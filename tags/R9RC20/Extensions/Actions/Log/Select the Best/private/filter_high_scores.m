function [ BestEvent, new_len, msg, status ] = filter_high_scores( event, N, mode, KeepChan, log )
% Select N highest scoring events using unit indicated in mode

status = 0;
msg = '';
fn = log_name( log );
% NumChan = length( KeepChan );

len = length( event );
if len < 1
    return;
end

%find event scores
score = [ event.score ];
num_scores = length( score );
if num_scores < length( event )
    msg = 'Not all events have scores ';
    return;
end

% keep N highest-scoring events in log
if     strcmp( mode, 'Log' )
    
    [ ~, idx ] = sort( score, 'descend' );
    N2 = min( N, len );
    idx = idx( 1 : N2 );
    BestEvent = event( idx );
    new_len = length( BestEvent );
    if new_len < N
        status = 2;
        fprintf( 'WARNING: Consider running detector with lower threshold.\n' )
        fprintf( 'Fewer than %.0f events:\n  %s\n\n', N, fn );
    else
        status = 1;
    end
    
% keep N highest-scoring events in each channel
elseif strcmp( mode, 'Channel' )
    
    flag = 0;
    BestEvent = empty( event_create );
    
    % for each selected channel
    for chan = KeepChan
        
        % filter events in current channel
        curr_event = event( ismember( [ event.channel ], chan ) );
        curr_score = [ curr_event.score ];        
        chan_len = length( curr_score );
        if chan_len < N
            fprintf( 'WARNING: Consider running detector with lower threshold.\n' )
            fprintf( 'Fewer than %.0f events in channel %.0f:\n  %s\n\n', ...
                N, chan, fn );
            flag = 1;
        end
        
        % filter N best events in current channel
        if chan_len > 0
            [ ~, idx ] = sort( curr_score, 'descend' );    
            N2 = min( N, chan_len );
            idx = idx( 1 : N2 );
            curr_event = curr_event( idx );
            BestEvent = [ BestEvent, curr_event ];      
        end
    end
    
    % find new log length
    new_len = length( BestEvent );
    
    %status 1 means nothing bad happened
    %       2 means fewer than N events per channel
    if flag
        status = 2;
    else
        status = 1;
    end
    
elseif strcmp( mode, 'Hour' )
    
    flag = 0;
    
    % set time-stamp and date-time attributes
    log.sound = PRBA_set_date_time( log.sound );
    log.sound = PRBA_set_time_stamps( log.sound );
    
    % find hour of each event
    t = zeros( len, 1 );
    hr0 = mod( log.sound.realtime, 1 ) * 24 ;
    for i = 1:len
        t( i ) = event( i ).time( 1 );
    end
    t_map = map_time( log.sound, 'real', 'record', t );
    hr =  floor(  hr0 + t_map ./ 3600 );
    
    %for each hour
    BestEvent = empty( event_create );
    for curr_hr = unique( hr' )
        
        % filter events in current hour
        curr_event = event( hr == curr_hr );        
        curr_score = [ curr_event.score ];        
        hr_len = length( curr_score );
        if hr_len < N
            fprintf( 'WARNING: Consider running detector with lower threshold.\n' )
            fprintf( 'Fewer than %.0f events in hour %.0f:\n  %s\n\n', ...
                N, curr_hr, fn );
            flag = 1;
        end
        
        % filter N best events in current hour
        if hr_len > 0
            [ ~, idx ] = sort( curr_score, 'descend' );
            N2 = min( N, hr_len );
            idx = idx( 1 : N2 );
            curr_event = curr_event( idx );
            BestEvent = [ BestEvent, curr_event ];
        end
    end
    
    % find new log length
    new_len = length( BestEvent );
    
    %status 1 means nothing bad happened
    %       2 means fewer than N events per channel
    if flag
        status = 2;
    else
        status = 1;
    end
    
elseif strcmp( mode, 'Channel-Hour' )
    
    flag = 0;
    
    % set time-stamp and date-time attributes
    log.sound = PRBA_set_date_time( log.sound );
    log.sound = PRBA_set_time_stamps( log.sound );
    
    % find hour of each event
    t = zeros( len, 1 );
    hr0 = mod( log.sound.realtime, 1 ) * 24 ;
    for i = 1:len
        t( i ) = event( i ).time( 1 );
    end
    t_map = map_time( log.sound, 'real', 'record', t );
    hr =  floor(  hr0 + t_map ./ 3600 );
    
    BestEvent = empty( event_create );
    
    % for each selected channel
    for chan = KeepChan
        
        % filter events in current channel
        chan_event = event( ismember( [ event.channel ], chan ) );
        ch_hr =     hr( ismember( [ event.channel ], chan ) );
        
        %for each hour
        for curr_hr = unique( ch_hr' )

            % filter events in current hour
            curr_event = chan_event( ch_hr == curr_hr );
            curr_score = [ curr_event.score ];
            ch_hr_len = length( curr_score );
            if ch_hr_len < N
                fprintf( 'WARNING: Consider running detector with lower threshold.\n' )
                fprintf( 'Fewer than %.0f events in channel %.0f and hour %.0f:\n  %s\n\n', ...
                    N, chan, curr_hr, fn );
                flag = 1;
            end

            % filter N best events in current hour
            if ch_hr_len > 0
                [ ~, idx ] = sort( curr_score, 'descend' );
                N2 = min( N, ch_hr_len );
                idx = idx( 1 : N2 );
                curr_event = curr_event( idx );
                BestEvent = [ BestEvent, curr_event ];
            end
        end    
    end
    
    % find new log length
    new_len = length( BestEvent );
    
    %status 1 means nothing bad happened
    %       2 means fewer than N events per channel
    if flag
        status = 2;
    else
        status = 1;
    end
end

% sort events by event id
id = [ BestEvent.id ];
[ ~, idx ] = sort( id, 'ascend' );
BestEvent = BestEvent( idx );