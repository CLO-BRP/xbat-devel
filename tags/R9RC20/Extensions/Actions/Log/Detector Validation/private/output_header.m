function M = output_header(parameter)

truth_flag = parameter.truth_flag;

%set header
M{1} = 'Detection Validation';
M{2,1} = 'Time Run:';
M{2,5} = datestr(now, 21);

M{4,1} = 'Truth Log Suffix:';
M{4,5} = parameter.truth_suffix;

M{5,1} = 'Test Log Suffix:';
M{5,5} = parameter.test_suffix;

M{6,1} = 'Time Overlap (% test event duration):';
M{6,5} = parameter.overlap;

M{7,1} = 'True Negatives Explicit?';

if parameter.truth_flag
  
  M{7,5} = 'Yes';
  
else
  
  M{7,5} = 'No';
  
end


%% set column heads

%if no negative truth events (TN not defined explicitly)
if ~truth_flag
  
  M{9,1}  = 'TPR';
  M{9,2} = sprintf('TPR (%.0f%% CI lower)', parameter.plevel * 100);
  M{9,3} = sprintf('TPR (%.0f%% CI upper)', parameter.plevel * 100);
  
  M{9,4}  = 'TPR';
  M{9,5}  = 'FP/hr';
  M{9,6}  = 'PPV';

  M{9,7}  = 'Positive Truth Matches';
  M{9,8}  = 'Positive Truth Misses';
  M{9,9}  = 'Positive Truth Total';

  M{9,10}  = 'Test Matches';
  M{9,11}  = 'Test Misses';
  M{9,12}  = 'Test Total';
  
  M{9,13}  = 'Truth Log';
  M{9,14} = 'Test Log';

  M{9,7}  = 'TP';
  M{9,8}  = 'FN';
  M{9,11} = 'FP';
  

%if negative truth events (TN defined explicitly)
else

  M{9,1} = 'TPR';
  M{9,2} = 'TPR (CI lower)';
  M{9,3} = 'TPR (CI upper)';  
  
  M{9,4} = 'TPR';
  M{9,5} = 'FPR';
  M{9,6} = 'FP/hr';
  M{9,7} = 'PPV';

  M{9,8} = 'Positive Truth Matches';
  M{9,9} = 'Positive Truth Misses';
  M{9,0} = 'Positive Truth Total';

  M{9,11} = 'Negative Truth Matches';
  M{9,12} = 'Negative Misses';
  M{9,13} = 'Negative Truth Total';

  M{9,14} = 'Test Matches Positive Truth';
  M{9,15} = 'Test Matches Negative Truth';
  M{9,16} = 'Test Matches Positive or Negative Truth';
  M{9,17} = 'Test Total';
  
  M{9,18} = 'Truth Log';
  M{9,19} = 'Test Log';

  M{9,8}  = 'TP';
  M{9,9}  = 'FN';
  M{9,12} = 'TN';
  M{9,15} = 'FP';

end
