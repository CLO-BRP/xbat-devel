function [result, context] = prepare(parameter, context)

% XBAT2RAVEN V3-1 - prepare

result = struct;

%% REFRESH log.file and log.path

disp('********************************************************************')
disp('*                                                                  *')
disp('*  XBAT2Raven v3.1                                                 *')
disp('*                                                                  *')
disp('*  Makes Raven Selection Tables for all selected logs              *')
disp('*                                                                  *')
disp('*  Note: Refreshes log.file and log.path to current log name and   *')
disp('*        current path                                              *')
disp('*                                                                  *')
disp('********************************************************************')

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end

% ask user for output directory

out_path = uigetdir(xbat_root, 'Choose output directory');

if out_path == 0
  context.state.kill = 1;
else
  context.state.kill = 0;
end

context.state.output = out_path;