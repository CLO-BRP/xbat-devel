function CheckDateTime(log)

% CHECK DATE-TIME ATTRIBUTE
%
% Reports which logs where realtime, date-time attribute, time stamps of
% first sound file, and log name do not all agree

%initializations
fn = log.file;
fn = fn(1:end-4);
realtime = log.sound.realtime;

if ~isequal(length(log.sound.attributes),0) && strcmp(log.sound.attributes(1).name,'date_time')
  datetime = log.sound.attributes(1).value.datetime;
else
  datetime = [];
end

indexfile = log.sound.file{1};
indexfiledate = file_datenum(indexfile);

%get date from log name
idx = findstr(fn,'_');      % find all hyphens in log name

i = 1;

if length(idx) >= 2         % if there are more than 2 hyphens
  
  fnDate = fn(idx(i)+1:idx(i+1)-1);  %attempt to extract datestr from fn
  
  while i<=length(idx) && isnan(str2double(fnDate))
    
    fnDate = fn(idx(i)+1:idx(i+1)-1);  %attempt to extract datestr from fn    
  
    i = i + 1;
    
  end
end

if i > length(idx)
  
  fnDate = fn(idx(end)+1:idx(end));  %attempt to extract datestr from fn  
  
end

if ~isnan(str2double(fnDate))
  
  logdate = datenum(fnDate, 'yyyymmdd'); %convert datestr to datenum
  
else
  
  logdate = [];
  
end
  
% if length(idx) >= 2         % if there are more than 2 hyphens
%   fnDate = fn(idx(1)+1:idx(2)-1);  %attempt to extract datestr from fn
%   
%   if ~isnan(str2double(fnDate))  %if datestr is a number
%     logdate = datenum(fnDate, 'yyyymmdd');  %convert datestr to datenum
%   elseif length(idx)>=3          %if datestr is not a number, but there is more log name to check
%     fnDate = fn(idx(2)+1:idx(3)-1); %attempt to extract another datestr from fn
%     logdate = datenum(fnDate, 'yyyymmdd'); %convert datestr to datenum
%   elseif length(idx)>=4          %if datestr is not a number, but there is more log name to check
%     fnDate = fn(idx(3)+1:idx(4)-1); %attempt to extract another datestr from fn
%     logdate = datenum(fnDate, 'yyyymmdd'); %convert datestr to datenum
%   end
% else
%   logdate = [];
% end

%test if date-time agrees with realtime, AIFF timestamp, log name
if isequal(datetime, indexfiledate)
  datetimeOK = 1;
  
elseif isempty(datetime)
  datetimeOK = 2;
  
else  %try to recover another date-time from index file
  
  datetimeOK = 0;
  idx2 = findstr(indexfile,'_');      % find all hyphens in index file
  
  if length(idx2) >= 3         % if there are more than 3 hyphens
    fnDate2 = indexfile(idx2(end-1)+1:idx2(end)-1);  %attempt to extract datestr from index file

    if ~isnan(str2double(fnDate2))  %if datestr is a number
      indexfiledate = datenum(fnDate2, 'yyyymmdd');  %convert datestr to datenum
      
      if isequal(datetime, indexfiledate)
        datetimeOK = 1;
      end      
    end
  end
end

realtimeOK = isequal(realtime, indexfiledate);
logdateOK = isequal(logdate, floor(indexfiledate));

if datetimeOK && realtimeOK && logdateOK && ~isequal(datetimeOK, 2)
  fprintf(1,'Date-time attribute is OK in %s\n', fn);
  
else
  fprintf(2,'%s has date-time problem\n',fn);
  
  if isequal(datetimeOK,2)
    if logdateOK
      fprintf(2,'  embedded date-time is empty\n');
    else
      fprintf(2,'  embedded date-time is empty and date in log name bad  ***callcount killer***\n');
    end
    
  elseif isequal(datetimeOK,0)
    fprintf(2,'  embedded date-time does not agree with index AIFF       ***callcount killer***\n');
  end
  
  if ~realtimeOK
    fprintf(2,'  embedded realtime does not agree with index AIFF\n');
  end
  
  if ~logdateOK
    fprintf(2,'  date in log name does not agree with index AIFF\n');
  end
  
end

fprintf(1,'\n');