function M_out = out_matrix(M, count, duration, parameter, fn_truth, fn_test)
  
[tpr, ci] = binofit(count.truth.TP(1), count.truth.TP(1) + count.truth.FN(1), 1 - parameter.plevel);

%if no negative truth events (TN not defined explicitly)
if ~parameter.truth_flag
  
  %if writing summary line
  if strcmp(fn_truth, 'TOTAL')
    
    idx = 2;
    
    M_new{idx,13} = 'COMBINED LOGS';
    M_new{idx,14} = ' ';
    
  else
    
    idx = 1;

    M_new{idx,13} = fn_truth;
    M_new{idx,14} = fn_test;    
    
  end
  
  M_new{idx,1} = tpr;
  M_new{idx,2} = ci(1);
  M_new{idx,3} = ci(2);

  M_new{idx,4} = count.truth.TP(1) / (count.truth.TP(1) + count.truth.FN(1));

  M_new{idx,5} = count.test.FP(1) / (duration / 3600);
  M_new{idx,6} = count.test.TP(1) / (count.test.TP(1) + count.test.FP(1));

  M_new{idx,7} = count.truth.TP(1);
  M_new{idx,8} = count.truth.FN(1);
  M_new{idx,9} = count.truth.TP(1) + count.truth.FN(1);

  M_new{idx,10} = count.test.TP(1);
  M_new{idx,11} = count.test.FP(1);
  M_new{idx,12} = count.test.TP(1) + count.test.FP(1);

  
%if negative truth events (TN defined explicitly)  
else
  
  %if writing summary line
  if strcmp(fn_truth, 'TOTAL')
    
    idx = 2;
    
    M_new{idx,20} = 'COMBINED LOGS';
    M_new{idx,21} = ' ';
    
  else
    
    idx = 1;

    M_new{idx,20} = fn_truth;
    M_new{idx,21} = fn_test;    
    
  end
  
  M_new{idx,1} = tpr;
  M_new{idx,2} = ci(1);
  M_new{idx,3} = ci(2);

  M_new{idx,4} = count.truth.TP(1) / (count.truth.TP(1) + count.truth.FN(1));
  M_new{idx,5} = count.truth.FP(1) / (count.truth.FP(1) + count.truth.TN(1));

  M_new{idx,6} = count.test.FP(1) / (duration / 3600);
  M_new{idx,7} = count.test.TP(1) / (count.test.TP(1) + count.test.FP(1));

  M_new{idx,8} = count.truth.TP(1);
  M_new{idx,9} = count.truth.FN(1);
  M_new{idx,10} = count.truth.TP(1) + count.truth.FN(1);
  M_new{idx,11} = count.truth.FP(1);
  M_new{idx,12} = count.truth.TN(1);
  M_new{idx,13} = count.truth.FP(1) + count.truth.TN(1);

  M_new{idx,14} = count.truth.TP(1) + count.truth.FN(1) + count.truth.FP(1) + count.truth.TN(1);
  M_new{idx,15} = count.truth.total;    

  M_new{idx,16} = count.test.TP(1);
  M_new{idx,17} = count.test.FP(1);
  M_new{idx,18} = count.test.TP(1) + count.test.FP(1);

  M_new{idx,19} = count.test.total;
  
end

M_out = [M; M_new];

