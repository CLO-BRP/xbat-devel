function parameter = parameter__create(context)

%% Make RL-TD Events and Guide Logs - parameter__create

parameter = struct;

% parameter.RemoveAutoXC = [];
parameter.KeepChannels = '';
parameter.NumBest = 5;

% parameter.RemoveZeros = [];
% parameter.RemoveNegatives = [];
% parameter.RemoveChannels = [];