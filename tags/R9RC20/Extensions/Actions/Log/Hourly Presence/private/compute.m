function [result, context] = compute(log, parameter, context)
result = struct;

% HOURLY PRESENCE - compute


%%
%Log Action "Hourly Presence"

%Creates CSV with hourly presence of events by channel

%%


if log_is_open(log)
    disp(['Close log ''', log_name(log), ...
      ' before calculating hourly presense.']); return;
end

nChannels = log.sound.channels;
len = length(log.event);
count=zeros(nChannels,24);

for i = 1:len
  hh = ceil(log.event(1,i).time(1)/3600);
  channel = log.event(1,i).channel;
  count(channel,hh) = count(channel,hh)+1;
end

% for i=1:nChannels
%   for j=1:24
%     if count(i,j)==0
%       count(i,j)=NaN;
%     end
%   end
% end

for i = 1:nChannels
  ChannelLabel(i,1) = i;
end

M = [ChannelLabel count];

HourLabel(1,1) = NaN;
for i = 2:25
  HourLabel(1,i) = i-2;
end
  
M = [HourLabel; M];

fn = log.file;
fn = fn(1:end-4);
fn = [fn '_HourlyPresence'];
xlswrite(fn,M)

context.state.count = context.state.count + sum(sum(count));