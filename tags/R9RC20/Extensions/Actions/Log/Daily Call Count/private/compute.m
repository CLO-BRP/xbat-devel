function [result, context] = compute(log, parameter, context)
result = struct;

%% DAILY CALL COUNT - compute
%
% Tabulates number of events with a user-definable tag for each channel of
% each selected sound.
%
% Useful for determining daily presence/absence of Fin Whales and other species.
%
% Note: If 'tag' parameter is left blank, all events will be counted.
%
%%
%terminate if fatal condition encountered
if context.state.kill
  return;
end

%initializations
len = log.length;
event = log.event;
LogName = log.file;
LogName = LogName(1:end-4);
fprintf(1,'Read into daily call count matrix: %s\n', LogName);

NumChannels = context.sound.channels;
DailyPresence = zeros(1,NumChannels);
TargetTag = parameter.tag;
day = floor(log.sound.realtime) - context.state.FirstDate + 1;

%Count # events in each channel for current log
if isequal(numel(TargetTag),0)  %if no target tag, count all events
  for i = 1:len
    DailyPresence(event(i).channel) = DailyPresence(event(i).channel) + 1;
  end
  
else                            %if target tag, only count target events
  for i = 1:len
    %convert tag to string if necessary
    tag = event(i).tags;
    if isequal(length(tag),0)
      tag='';
    elseif iscell(tag)
      tag = tag{1};
    end

    %count event only if proper tag
    if strcmpi(tag,TargetTag)
      DailyPresence(event(i).channel) = DailyPresence(event(i).channel) + 1;
    end
  end
end

%Warn if previous log had events on the same day
if any(context.state.DailyPresence(day,:))
  fprintf(2,'WARNING: Previous log had events on same day as %s\n', LogName);
end

%add callcount for current log to callcount for all days
context.state.DailyPresence(day,:) = ...
  context.state.DailyPresence(day,:) + DailyPresence;
