function [presets,f] = get_palette_presets( str, ext )

% get_palette_presets
% -------------------
%
% presets = get_palette_presets
%
% Input:
% -------
%  str - palette name
%
% Output:
% -------
%  presets - array of presets
%  f - array of preset names

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

% History
%   msp2 - 25 May 2014
%       Accommodate presets from control palettes as well as browser.
%       Allow ASCII as well as MAT presets.

%--
% set (possibly create) palette preset directory
%--
p = fullfile( xbat_root, 'Presets', str );
if ( ~exist( p, 'dir' ) )
	mkdir( p );
end

%--
% load presets
%--
f = what_ext( p, ext );
f = eval( [ 'f.' ext '( : )' ] );

% NOTE: return if there are no preset files
if ( isempty( f ) )
	presets = [];
    return;
end
j = 0;
for k = length(f):-1:1
    URI = fullfile( p, f{ k } );
	
	% NOTE: failure does not increment counter and removes file from list
	try
        switch ext
            case 'mat'
                tmp = load( URI , 'state' );
            case 'csv'
                state = txtread( URI, '%s%s', ',\t' );
                m = size( state, 2 );
                if m > 2
                    state = state( :, 1 : 2 );
                elseif m < 2
                    fail( sprintf( '%s is not a preset', f{ k } ) );
                    continue;
                end
                tmp.state = { state };
        end
	catch
		f( k ) = [];
		continue;
    end		
	j = j + 1;
	presets( j ) = tmp.state;	
end
presets = fliplr( presets( : ) );
% presets = flipud( presets( : ) );
