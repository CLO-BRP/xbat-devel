function str = get_grid_time_string(grid,time,realtime)

% get_grid_time_string - get time string according to grid
% --------------------------------------------------------
%
% str = get_grid_time_string(grid,time,realtime)
%
% Input:
% ------
%  grid - grid options
%  time - time
%  realtime - date offset
%
% Output:
% -------
%  str - time string according to grid

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4695 $
% $Date: 2006-04-20 11:22:26 -0400 (Thu, 20 Apr 2006) $
%--------------------------------

% History
%   msp2 - 21 Mar 2014
%       Construct time strings to the nearest 0.01 seconds for Event
%       palette, etc.
%   msp2 - 5 May 2014
%       Report times to 0.001 ms.  Was 0.01 ms, except for "Date and Time",
%         which was 1 s.
%       Don't insert leading blanks when time grid is "seconds.
%   msp2 - 26 May 2014
%       Round (rather than truncate) to 0.001 seconds when time grid labels
%         set to "seconds" and "clock".

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% set default empty realtime
%--

if nargin < 3
	realtime = []; 
end

%--
% handle multiple times recursively
%--

if numel(time) > 1
	
	str = cell(size(time));
	
	for k = 1:numel(time)
		str{k} = get_grid_time_string(grid,time(k),realtime);
	end 
	
	return; 

end

%------------------------------
% COMPUTE TIME STRINGS
%------------------------------

%--
% compute time string based on grid settings, time, and realtime
%--

% TODO: consider a 'get_grid_label_types'


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% disp( time )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch grid.time.labels
	
	case 'seconds'
		
		% NOTE: the rounding preserves two decimal digits if needed
		
		str = sprintf( '%.3f sec', time );
        
	case 'clock'
        str = datestr( time / 86400 , 'HH:MM:SS.FFF' );	
% 		str = sec_to_clock( time, 3 );
%         if time < 36000
%             str = ['0' str];
%         end
		
	case 'date and time'
		if isempty(realtime)
            str = datestr( time / 86400 , 'HH:MM:SS.FFF' );
% 			str = sec_to_clock( time, 3 );
%             if time < 36000
%                 str = ['0' str];
%             end
		else
			str = datestr( offset_date( realtime, time ), 'yyyy-mmm-dd HH:MM:SS.FFF' );
		end
		
end
