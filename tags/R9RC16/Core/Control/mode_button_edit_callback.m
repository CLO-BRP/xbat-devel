function mode_button_edit_callback( obj, ~, par_button )
    % mode_button_callback - button callbacks for Browse mode Edit palette
    
    %History
    %   msp2 - 21 May 2014
    %       Set ready bit in buttons depending on whether log is selected.
    %       Get duration and frequency from sliders rather than edit boxes
    %         for better precision.
    %       Add callback for bandwidth control
    %       Enable/disable frequency controls depending on mode.
    %   msp2 - 23 May 2014
    %       Set cursor icon of mode selected into Browse palette button.
    %       Set color of selected log into each Browse palette button.
    %   msp2 - 25 May 2014
    %       Clean up code.
    %   msp2 - 11 June 2014
    %       When saving and opening presets, unset duration and frequency is
    %         set to [] or '' rather than NaN.
    %   msp2 - 13 June 2014
    %       Tweak contrasting font color for buttons.

    %---
    % initializations
    %---
    str = get( obj, 'Tag' );
    pal = get( obj, 'Parent' );

    %--
    % Switch action based on control tag
    %--
    switch str 

        %--
        % Label edit box
        %--
        case 'label'
            % nothing to do

        %--
        % Logs list box
        %--
        case 'logs'
            % nothing to do
            
        %--
        % Mode list box
        %--
        case 'mode'
            
            % enable/disable frequency controls
            set_mode_slider( pal )

        %--
        % Tag edit box - condition tag
        %--
        case 'tag'
            
            %deblank tag if needed
            h = findobj( pal, 'Type', 'uicontrol', 'Style', 'edit', 'Tag', 'tag' );
            tag = get( h, 'String' );
            if any( isspace( tag ) )
                tag = strtok( tag );
                set( h, 'String', tag  )
            end

        %--
        % Duration (s) slider - do nothing
        %--
        case 'duration'
            % nothing to do

        %--
        % Frequency sliders
        %--
        case { 'min_freq', 'max_freq', 'bandwidth' }
            
            % get handles
            h_min_freq_edit =    findobj( pal, 'Type', 'uicontrol', 'Style', 'edit',   'Tag', 'min_freq' );
            h_min_freq_slider =  findobj( pal, 'Type', 'uicontrol', 'Style', 'slider', 'Tag', 'min_freq' );
            h_max_freq_edit =    findobj( pal, 'Type', 'uicontrol', 'Style', 'edit',   'Tag', 'max_freq' );
            h_max_freq_slider =  findobj( pal, 'Type', 'uicontrol', 'Style', 'slider', 'Tag', 'max_freq' );
            h_bandwidth_edit =   findobj( pal, 'Type', 'uicontrol', 'Style', 'edit',   'Tag', 'bandwidth' );
            h_bandwidth_slider = findobj( pal, 'Type', 'uicontrol', 'Style', 'slider', 'Tag', 'bandwidth' );
            
            % if callback is from Minimum Freq (Hz) or Maximum Frequency (Hz) control
            if any( strcmp( str, { 'min_freq', 'max_freq' } ) )
            
                % ensure minimum frequency lower than maximum frequency
                min_freq = get( h_min_freq_slider, 'Value' );
                max_freq = get( h_max_freq_slider, 'Value' );            

                if min_freq > max_freq
                    set( h_min_freq_edit, 'String', num2str( max_freq ) )
                    set( h_min_freq_slider, 'Value', max_freq )
                    set( h_max_freq_edit, 'String', num2str( min_freq ) )
                    set( h_max_freq_slider, 'Value', min_freq )
                end

                % set bandwidth
                bandwidth = abs( max_freq - min_freq );
                set( h_bandwidth_edit, 'String', num2str( bandwidth ) )
                set( h_bandwidth_slider, 'Value', bandwidth )
                    
            % if callback is from Bandwidth (Hz) control
            elseif strcmp( str, 'bandwidth' )
                
                % reset Min Freq and Max Freq controls
                bandwidth = get( h_bandwidth_slider, 'Value' );
                min_freq = 0;
                max_freq = bandwidth;
                set( h_min_freq_edit, 'String', num2str( min_freq ) )
                set( h_min_freq_slider, 'Value', min_freq )
                set( h_max_freq_edit, 'String', num2str( max_freq ) )
                set( h_max_freq_slider, 'Value', max_freq )                
            end
            
        %--
        % Cancel button - deletes Edit palette
        %--
        case( 'Cancel' )
            
            % delete Edit palette
            if ishandle( pal )
                delete( pal )
            end
            return;

        %--
        % OK button - sets tool tip in Browse palette button and deletes Edit palette
        %--
        case 'OK'

            % set Browse palette button label, icon and tool tip
            tooltip = set_button( pal, par_button );
            
            % set Browse palette button ready/unready state
            set_button_ready( tooltip, par_button )

            % delete Edit palette
            if ishandle( pal )
                delete( pal )
            end
            return;

        otherwise
            fail( sprintf( 'Unknown control tag from Browse Edit palette: "%s".', tag ) );
            return;
    end
end

%% --------------------------
function tooltip = set_button( pal, obj )

    %--
    % Set label in button
    %--

    % get values of controls in Edit palette
    [ label, logname, mode_str, tag, dur, min_freq, max_freq ] = get_browse_tooltip( pal );
    
    % set Browse palette button label
    set( obj, 'String', label )
    
    %--
    % Set tooltip in button
    %--
    tooltip = make_tooltip( logname, mode_str, tag, dur, [ min_freq, max_freq ] );
    
    % set Browse palette button tooltip
    set( obj, 'TooltipString', tooltip )
    
    %--
    % set bitmap for mode-specific cursor image in button
    %--
    switch mode_str
        case 'Select + Log'
            tmp = ptr_add;
        case 'One Click';
            tmp = ptr_ibeam;
        case 'One Click Frequency Tracking'
            tmp = ptr_cross;
        case 'Tag'
            tmp = ptrT;
    end
    
    %convert bitmap to Truecolor
    tmp( tmp == 1 ) = 0;
    tmp( tmp == 2 ) = 1;
    [ m, n ] = size( tmp );
    tmp = [ NaN( m, 88 - n ), tmp ];  % position on right side of button
    cdata = repmat( tmp, [ 1, 1, 3 ] );
    
    % set pointer into button
    set( obj, 'cdata', cdata )
      
end

%% --------------------------
function set_button_ready( tooltip, obj )

    %--
    % Determine if Browse button is ready to be used
    %--
    logname = parse_tooltip( tooltip );
    if ~isempty( logname )
        isready = 1;
    else
        isready = 0;
    end

    %--
    % set button label font to indicate if button isready / ~isready
    %--
    if isready

        % trip ready bit
        set( obj, 'Userdata', struct( 'ready', 1 ) )

        
        %--
        % Create box around icon whose color is log color
        %--

        % find name of selected log
        logname = parse_tooltip( tooltip );

        % find color of selected log
        data = get( get_active_browser, 'Userdata' );
        log_list = log_name( data.browser.log );
        idx = strcmp( log_list, logname );
        c_back = data.browser.log( idx ).color;

        % set log color into button
        set( obj, 'BackgroundColor', c_back )
        
        % set button font color to white, if background color dark
        test = ( c_back( 1 ) < 0.75 ) && ( c_back( 2 ) < 0.97 );
        c_fore = test * ones( 1, 3 );
        set( obj, 'ForegroundColor', c_fore )
            
    else

        % untrip ready bit
        set( obj, 'Userdata', struct( 'ready', 0 ) )

        % make button same gray as window
        set( obj, 'BackgroundColor', get(0, 'DefaultFigureColor' ) )
    end
end

%% -----------------------
function [ label, fn, mode_str, tag, dur, min_freq, max_freq ] = get_browse_tooltip( pal )

    % get selected label
    label = get( findobj( pal, 'Style', 'edit', 'Tag', 'label' ), 'String' );

    % get selected log name
    fn = get_list_str( pal, 'logs' );

    % get selected mode name
    mode_str = get_list_str( pal, 'mode' );

    % get selected tag
    tag = get( findobj( pal, 'Style', 'edit', 'Tag', 'tag' ), 'String' );

    % get selected duration
    dur = get( findobj( pal, 'Style', 'slider', 'Tag', 'duration' ), 'Value' );

    % get selected minimum frequency
    min_freq = get( findobj( pal, 'Style', 'slider', 'Tag', 'min_freq' ), 'Value' );

    % get selected maximum frequency
    max_freq = get( findobj( pal, 'Style', 'slider', 'tag', 'max_freq' ), 'Value' );

end

%% ------------------------
function str = get_list_str( pal, tag )

    log_h = findobj( pal, 'Style', 'listbox', 'Tag', tag );
    str = get( log_h, 'String' );
    if iscellstr( str )
        val = get( log_h, 'Value' );
        str = str{ val };
    end
    if strcmp( str, '(No Open Logs)' )
        str = '';
    end
end
   
% %------------------------
% function str = html( str, fsize, bold )
% % fontize - adds html to string so that it shows up in bigger font
% %
% %   input
% %       str   - string to be formatted
% %       fsize - fontsize
% %       bold  - 0/1 = normal/bold font weight
% %   output
% %       str - string with html font size tag
% 
% if bold
%     str = sprintf( '<html><b><font size=%.0f>%s</font></b></html>', fsize, str );
% else
%     str = sprintf( '<html><font size=%.0f>%s</font></html>', fsize, str );
% end
%
% -------------------------------
% Example of use:
%         tag1 = sprintf( '<html><font size=%.0f><b>', fsize );
%         tag2 = '</b>';
%         tag3 = '</font></html>';
%         selection = get( h_menu, 'Label' );
%         new_label = sprintf( '%s%s %s%s%s', tag1, type, tag2, selection, tag3 );
%         set( get( h_menu, 'Parent' ), 'Label', new_label )
% -------------------------------

% %------------------------
% function str = strip_html( str )
% 
% str = regexprep( str, '<.*?>', '' );
% % str = regexprep( str, '<[^>]*>', '' );
