function update_browse_palette( par, data )

% update_browse_palettes - update browse palette when logs change
% ----------------------------------------------------------------------
%
% pals = update_browse_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

% History
%   msp2 - 27 May 2014
%       Clean up code.

%----------------------
% HANDLE INPUT
%----------------------

%--
% check browser handle input
%--
if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--
if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%----------------------
% TEST CODE
%----------------------

%--
% get listener control values
%--
none = isempty(data.browser.log);
if none
	list = {'(No Open Logs)'}; 
%     ix = 1;
else
	list = file_ext(struct_field(data.browser.log, 'file')); 
%     ix = data.browser.log_active;
end
		
%--
% get listener names and palette handles
%--
% NOTE: not all of these listeners are currently in use
% % listener = {'output_log', 'noise_log', 'source_log', 'template_log'}; 
browse_listener = { 'button_1', 'button_2', 'button_3', 'button_4', 'button_5', 'button_6' };

% NOTE: this is critical, 'get_palette' only gets registered palettes, is should use this function!
pal = get_xbat_figs('type', 'palette', 'parent', par);

%--
% look for listener controls in available palettes
%--
name = get( pal, 'Name' );
idx = strcmp( name, 'Browse' );
pal = pal( idx );
if ~isempty( pal )
    
    %---
    % Process listener for Browse palette
    %---    
	for j = 1:length(browse_listener)
        
        %--
        % look for listener in Browse palette
        %--
        h_control = get_control( pal, browse_listener{ j }, 'handles' );
        if isempty( h_control )
            continue;
        end

        %--
        % find log name currently in button tooltip
        %--
        h_button = h_control.obj;
        tooltip = get( h_button, 'TooltipString' );
        [ tooltip_log, mode_str, tag, dur, freq ] = parse_tooltip( tooltip );

        %--
        % if tooltip log is no longer available, delete it in tooltip
        %--
        if ~isempty( tooltip_log ) && ~ismember( tooltip_log, list )
            
            % remove log name from tooltip
            tooltip_log = '';
            tooltip_new = make_tooltip( tooltip_log, mode_str, tag, dur, freq( 1 ), freq( 2 ) );
            set( h_button, 'TooltipString', tooltip_new )
            
            % disable button
            set( h_button, 'UserData', struct( 'ready', 0 ) )
            
            %---
            % enable select mode in browser
            %---
            
            % if button has bold font (means active button)
            if strcmp( get( h_button, 'FontWeight' ), 'bold' )
                
                % put browser in select mode
                set_browser_mode( par, 'select' ) ;
                
                % set button font weight to normal
                set( h_button, 'FontWeight', 'normal' )
                
                % set checkbox for Select mode in Browse palette
                pal2 = get_palette( par, 'Browse', data );
                h = findobj( pal2, 'Style', 'checkbox', 'Tag', 'select_mode' );
                set( h, 'Value', 1 )
            end
                
            % set button font color to black
            set( h_button, 'ForegroundColor', [ 0 0 0 ] )
            
            % make button same gray as window (may be white, green, or gray)
            set( h_button, 'BackgroundColor', get(0, 'DefaultFigureColor' ) );
            
        end
	end
end