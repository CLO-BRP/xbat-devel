function mode_button_callback( obj, ~ )
% mode_callback - right mouse click on Browse palette button calls Edit palette
%
%   input
%       obj: handle of clicked button in Browse palette

% History
%   msp2 - 29 Mar 2014
%       Create.
%	msp2 - 30 Mar 2014
%		Make button light green if ready.
%		Get callbacks for "Tag" mode working.
%		Remove references to "Delete" mode.
%   msp2 - 31 Mar 2014
%       Disable delete mode when editing button context menus.
%   msp2 - 27 Apr 2014
%       Prevent frequency from being set to less than zero.
%   msp2 - 29 Apr 2014
%       Fix minor bug in tag-setting code.
%   msp2 - 7 May 2014
%       Do not reset settings when changing log or mode in button.
%       Remove code converting tag "(No Tag)" to '', since not needed.
%   msp2 - 18 May 2014
%       Replace context menu with Edit palette for setting button parameters
%	msp2 - 21 May 2014
%		Browse palette
%           Accommodate html tags in tooltip.
%           Make Edit palette modal.
%           Make Edit palette edit boxes uninterruptible to avoid error.
%           Add bandwidth slider control
%           Enable/disable frequency controls depending on mode.
%   msp2 - 23 May 2014
%       Browse Edit palette: Make modal sooner to avoid mischief.
%   msp2 - 29 May 2014
%       Browse Edit palette: Position better on narrow portrait monitors.

%-------------------------------------------------------------------------
% Initializations
%-------------------------------------------------------------------------
h = get_active_browser;
data = get( h, 'userdata' );

%---
% Enable "Select" mode
%---
par = get( obj, 'Parent' );
children = get( par, 'Children' );
ch = findobj( children, 'Style', 'checkbox' );
set( ch, 'Value', 0 )
sh = findobj( ch, 'Tag', 'select_mode' );
set( sh, 'Value', 1 )
    
% Set selected button to normal font to indicate it is NOT selected
set( obj, 'FontWeight', 'normal' )

% Set browser to "select" mode
set_browser_mode( h, 'select', data )

%---
% Stop if no callback
%---
fun = get( obj, 'Callback' );
if isempty( fun )
    return;
end

%-------------------------------------------------------------------------
% Open control palette for editing parameters
%-------------------------------------------------------------------------

%--
% "Edit" header
%--
control( 1 ) = control_create( ...
    'style',  'separator', ...
    'string', 'Edit', ...
    'type',   'header', ...
    'space',  0.75 ...
);

%---
% Button label edit box
%--

% Recover button settings from button tooltip
tooltip = get( obj, 'TooltipString' );          
[ logname, mode_str, tag, duration, freq ] = parse_tooltip( tooltip );
label = get( obj, 'String' );

% Add control
control(end + 1) = control_create( ...
    'name', 'label', ...
    'style', 'edit', ...
    'color', ones(1,3), ...
    'string', label ...
);

%--
% Logs listbox
%--

% find index of currently-selected log
h = get_active_browser;
data = get_browser( h );
if ~isempty( data.browser.log )
    log_names = log_name( data.browser.log );
else
    log_names = '(No Open Logs)';
end
idx = find( strcmp( log_names, logname ) );
if isempty( idx )
    idx = 1;
end

% Add control
control(end + 1) = control_create( ...
    'name', 'logs', ...
    'label', 1, ...
    'style', 'listbox', ...
    'lines', 3, ...
    'string', log_names, ...
    'value', idx ...
);

%--
% Mode listbox
%--

% find mode list and index of currently-selected mode
[ ~, modes ] = get_browser_modes;
modes = modes( ~ismember( modes, { 'Select', 'Hand', 'Delete' } ) );
idx = find( strcmp( modes, mode_str ) );
if isempty( idx )
    idx = 1;
end

% Add control
control(end + 1) = control_create( ...
    'name', 'mode', ...
    'alias', 'Mode', ...
    'label', 1, ...
    'style', 'listbox', ...
    'string', modes, ...
    'value', idx ...
);

%--
% Tag edit box
%--
control(end + 1) = control_create( ...
    'name', 'tag', ...
    'style', 'edit', ...
    'color', ones(1,3), ...
    'string', tag ...
);

%--
% Duration (s) slider
%--

% If not previously set, set to 1
if isnan( duration )
    duration = 1;
end

% Add control
control(end + 1) = control_create( ...
    'name','duration', ...
    'alias', 'Duration (s)', ...
    'style', 'slider', ...
    'space', 1.25, ...
    'min',   0.001, ...
    'max',   60, ...
    'value', duration ...
);

%--
% Minimum Frequency (Hz) slider
%--

% If not previously set, set to 0
nyquist = data.browser.sound.samplerate / 2;
if isnan( freq( 1 ) )
    freq( 1 ) = 0;
end

% Add control
control(end + 1) = control_create( ...
    'name','min_freq', ...
    'alias', 'Minimum Frequency (Hz)', ...
    'style', 'slider', ...
    'min',   0, ...
    'max',   nyquist, ...
    'value', freq( 1 ) ...
);

%--
% Minimum Frequency (Hz) slider
%--

% If not previously set, set to Nyquist limit
if isnan( freq( 2 ) )
    freq( 2 ) = nyquist;
end

% Add control
control(end + 1) = control_create( ...
    'name','max_freq', ...
    'alias', 'Maximum Frequency (Hz)', ...
    'style', 'slider', ...
    'min',   0, ...
    'max',   nyquist, ...
    'value', freq( 2 ) ...
);

%--
% Bandwidth (Hz) slider
%--
control(end + 1) = control_create( ...
    'name','bandwidth', ...
    'alias', 'Bandwidth (Hz)', ...
    'style', 'slider', ...
    'min',   0, ...
    'max',   nyquist, ...
    'space', 1.5, ...
    'value', diff( freq ) ...
);

%--
% OK and Cancel buttons
%--
name = { 'OK', 'Cancel' };
control(end + 1) = control_create( ... 
    'name', name, ...
    'lines', 1.75, ...
    'space', 0.75, ...
    'style', 'buttongroup' ...
);

%--
% Create Edit palette
%--
opt = control_group;		
opt.width = 18; 
opt.top = 0; 
opt.bottom = 0;		
opt.header_color = get_extension_color( 'sound_browser_palette' );		
opt.handle_to_callback = 1;
pal = control_group( h, { @mode_button_edit_callback, obj }, 'Edit', control, opt );
set( pal, 'WindowStyle', 'modal' )

%--
% Enable/disable frequency sliders as appropriate for mode
%--
set_mode_slider( pal )

%--
% Make edit boxes uninterruptible to avoid error when click on button after
% typing in edit box
%--
edit_h = findobj( pal, 'Type', 'uicontrol', 'Style', 'edit' );
set( edit_h, 'Interruptible', 'off' )
        
%--
% Position Edit palette
%--

% initializations
margin = 0;
pos_browse = get( par, 'Position' );
pos_edit = get( pal, 'Position' );
ScreenSize = get( 0, 'ScreenSize' );

% position below the Browse palette, if it will fit
if 1 == margin
% if ( pos_browse( 2 ) - pos_edit( 4 ) - margin ) > 0
    pos_edit( 2 ) = pos_browse( 2 ) - pos_edit( 4 ) - margin;
    
% or else position above the Browse palette, if it will fit
elseif pos_browse( 2 ) + pos_browse( 4 ) + pos_edit( 4 ) + margin <= ScreenSize( 4 )
    pos_edit( 2 ) = pos_browse( 2 ) + pos_browse( 4 ) + margin;
    
% or else position on top of the Browse palette
else
    pos_edit( 2 ) = pos_browse( 2 ) + pos_browse( 4 ) - pos_edit( 4 );
end

% move the Edit palette
pos_edit( 1 ) = pos_browse( 1 );
set( pal, 'Position', pos_edit )
