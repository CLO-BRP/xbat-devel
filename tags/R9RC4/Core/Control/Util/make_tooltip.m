function tooltip = make_tooltip( logname, mode_str, tag, dur, min_freq, max_freq )
% make_tooltip: Makes tooltip with html tags for button in Mode palette
%
%   input
%       logname (char)
%       mode (char)
%       tag (char)
%       duration (double)
%       freq (1x2 double)
%   output
%       tooltip (char matrix)

% History
%   msp2 - 21 May 2014
%       Change input parameters for duration and frequency from string to double.

    tooltip = sprintf( ...
        [ '<html><pre><font face="courier new" size="4">' ...
            '<b>           Log: </b>%s <br />' ...
            '<b>          Mode: </b>%s <br />' ...
            '<b>           Tag: </b>%s <br />' ...
            '<b>  Duration (s): </b>%.3f <br />' ...
            '<b> Min Freq (Hz): </b>%5.0f <br />' ...
            '<b> Max Freq (Hz): </b>%5.0f' ...
          '</font>' ...
        ], ...
        logname, mode_str, tag, dur, min_freq, max_freq );
