function [ logname, mode, tag, duration, freq ] = parse_tooltip( tooltip )

% parse_tooltip: Unpacks tooltip from button in Mode palette
%
%   input
%       tooltip (char matrix)
%   output
%       logname (char)
%       mode (char)
%       tag (char)
%       duration (double)
%       freq (1x2 double)

% History
%   msp2 - 28 Mar 2014
%       Create.
%   msp2 - 28 Mar 2014
%       Remove output parameter for bandwidth.
%   msp2 - 21 May 2014
%       Accommodate html tags in tooltip.
%       Trim leading and trailing blanks from parameters in tooltip.
%   msp2 - 29 May 2014
%       Enable processing of multiple tooltips in one call.

% separate function calls for each tooltip
len = length( tooltip );
logname = cell( len, 1 );
mode = logname;
tag = logname;
duration = zeros( len, 1 );
freq = zeros( len, 2 );
if iscellstr( tooltip )
    for i = 1 : len
        [ logname{ i }, mode{ i }, tag{ i }, duration( i ), freq( i, 1:2 ) ] = parse_tooltip( tooltip{ i } );
    end
    return;
end

% initializations
logname = '';
mode = '';
tag = '';
duration = [];
freq = [];

if isempty( tooltip )
    return;
end

% unpack tooltip into individual parameters
C = regexp( tooltip, '<.*?>', 'split' );

logname = strtrim( C{ 6 } );
mode = strtrim( C{ 9 } );
tag = strtrim( C{ 12 } );
duration = str2double( strtrim( C{ 15 } ) );
freq = [ str2double( strtrim( C{ 18 } ) ), str2double( strtrim( C{ 21 } ) ) ];
