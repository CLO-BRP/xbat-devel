function control = parameter__control__create(parameter, context)

% DETECT - parameter__control__create

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 24 Sep 2014
%		Support for Data Management system and more control of log file
%		location and size.
%	msp2 -  2 Nov 2014
%		Add support for Raven selection table output.
%	msp2 - 10 Mar 2014
%		Improve GUI layout
%		Add support for whiten filter
%   msp2 - 12 Mar 2013
%       Add controls for spectrogram parameters
%   msp2 - 14 May 2014
%       Add support for a unique identifier in log names.
%   msp2 - 15 May 2014
%       Add multithreading support.
%       Cap number of process threads at 8, per MATLAB license.
%   msp2 - 3 June 2014
%       Move Window Parameter control to right of Window Type control.
%   msp2 - 5 June 2014
%       Change layout to no tabs, three sections, two columns.
%       Enable/disabled filter order control with whiten filter checkbox.
%       Disable specgram parameters and threshold controls, to enforce threshold settings.
%       Allow OK button to be enabled if at least one preset is present.
%       Disable threshold and poolsize sliders by default.
%   msp2m - 10 June 2014
%       Disable checkbox enabling threshold.  Now threshold is controlled
%         by preset only.

control = empty(control_create);

% %-------------------------------------------------------------------------
% % DETECTOR SEPARATOR
% %-------------------------------------------------------------------------
% 
% control(end + 1) = control_create( ...
% 	'string','Detector', ...
% 	'style','separator', ...
% 	'type','header' ...
% );

%---
% Detector Popup
%---
names = get_detector_names;

ix = find(strcmp(parameter.detector, names));

if isempty(ix) 
	ix = min(length(ix), 3);
end

% NOTE: the callback for this control updates the preset control

control(end + 1) = control_create( ...
	'name', 'detector', ...
	'width', 0.46, ...
    'align', 'left', ...
	'style', 'popup', ...
	'space', -2, ...
	'string', names, ... 
	'onload', 1, ...
	'value', ix ...
);

%---
% Preset Popup
%---

control(end + 1) = control_create( ...
	'name', 'preset', ...
	'width', 0.46, ...
    'align', 'right', ...
	'style', 'popup', ...
    'onload', 1, ...
	'space', 1.5, ...
	'string', { '(No presets available)' }, ... 
	'value', 1 ...
);

% control(end + 1) = control_create( ...
% 	'name', 'preset', ...
% 	'style', 'listbox', ...
%     'initialstate', 0, ...
%     'space', 1.5, ...
% 	'max', 2, ...
% 	'lines', 3 ...
% );

% Add maximum output log size CHECKBOX
control(end + 1) = control_create( ...
    'name', 'threshold_flag', ...
    'alias','Threshold', ...
    'style', 'checkbox', ...
    'width', 0.46, ...
    'align', 'left', ...
    'initialstate', '__DISABLE__', ...
	'space', -0.8, ...
    'value', parameter.threshold_flag ...
    );

%---
% Threshold Slider
%---
control(end + 1) = control_create( ...
    'name', 'threshold', ...
    'alias', ' ', ...
    'style', 'slider', ...
	'space', -2.17, ...
    'onload', 1, ...
    'width', 0.46, ...
    'align', 'left', ...
    'initialstate', '__DISABLE__', ...
    'min', 0, ...
    'max', 1, ...
    'value', parameter.threshold ...
    );

%---
% Multithreading Checkbox
%---
control(end + 1) = control_create( ...
    'name', 'multithread', ...
    'alias','Threads', ...
    'style', 'checkbox', ...
    'width', 0.46, ...
    'align', 'right', ...
    'space', -0.8, ...
    'value', parameter.multithread ...
    );

%---
% Multithreading Slider
%---

% Select number of threads
num_cores = getenv( 'NUMBER_OF_PROCESSORS' );
if isempty( num_cores )
    num_cores = 1;
else
    num_cores = str2double( num_cores );
end
max_setting = min( max( num_cores - 1, 1 ), 8 );

control(end + 1) = control_create( ...
    'name', 'poolsize', ...
    'alias', ' ', ...
    'style', 'slider', ...
    'type', 'integer', ...
    'space', 0.5, ...
    'width', 0.46, ...
    'align', 'right', ...
    'onload', 1, ...
    'space', 1.5, ...
    'initialstate', '__DISABLE__', ...
    'min', 1, ...
    'max', num_cores - 1, ...
    'value', max_setting ...
    );

%-------------------------------------------------------------------------
% FFT WINDOW PARAMETERS
%-------------------------------------------------------------------------

control(end + 1) = control_create( ...
	'string','FFT Window', ...
	'style','separator', ...
	'type','header' ...
);

%---
% FFT Size slider (samples)
%---
control(end + 1) = control_create( ...
    'name', 'fft', ...
	'alias', 'Size (samples)', ...
    'style', 'slider', ...
	'type', 'integer', ...
	'space', -2, ...
    'width', 0.46, ...
    'align', 'left', ...
    'initialstate', '__DISABLE__', ...
    'min', 16, ...
    'max', 4096, ...
    'value', parameter.fft ...
    );

% %---
% % FFT Size slider (ms)
% %---
% control(end + 1) = control_create( ...
%     'name', 'fft_ms', ...
% 	'alias', 'Size (ms)', ...
%     'style', 'slider', ...
% 	'type', 'integer', ...
% 	'space', 1.5, ...
%     'width', 0.46, ...
%     'align', 'right', ...
%     'min', 16, ...
%     'max', 4096, ...
%     'value', parameter.fft ...
%     );

%---
% FFT Advance slider
%---
control(end + 1) = control_create( ...
    'name', 'hop', ...
	'alias', 'Advance', ...
    'style', 'slider', ...
	'space', 1.5, ...
    'width', 0.46, ...
    'align', 'right', ...
    'initialstate', '__DISABLE__', ...
    'space', 1.75, ...
    'min', 0.01, ...
    'max', 1, ...
    'value', parameter.hop ...
    );
% 	'initialstate', parameter.hop_auto, ... 

%---
% Auto Advance slider
%---
% control(end + 1) = control_create( ...
%     'name', 'hop_auto', ...
% 	'alias', 'Auto Advance', ...
%     'style', 'checkbox', ...
%     'initialstate', '__DISABLE__', ...
% 	'space', 1.5, ...
%     'value', parameter.hop_auto ...
%     );

%---
% Smoothing checkbox
%---
% control(end + 1) = control_create( ...
%     'name', 'smoothing', ...
% 	'alias', 'Smoothing', ...
%     'style', 'checkbox', ...
%     'initialstate', '__DISABLE__', ...
% 	'space', 1.5, ...
%     'value', parameter.smoothing ...
%     );

%---
% Window Type popup
%---
[ window_types, ~, def_ix ] = window_to_fun;
ix = find( strcmpi( window_types, parameter.win_type ) );
if isempty( ix )
	ix = def_ix;
end

control(end + 1) = control_create( ...
    'name', 'win_type', ...
	'alias', 'Type', ...
    'style', 'popup', ...
	'onload', 1,  ...
    'width', 0.46, ...
	'space', -2.17, ...
    'initialstate', '__DISABLE__', ...
	'string', window_types, ...
    'value', ix ...
    );

%---
% Window Parameter slider
%---
param = window_to_fun( parameter.win_type, 'param' );
if ~isempty( param )
	value = param.value;
else
	value = [];
	param.min = 0;
	param.max = 1;
end
	
control(end + 1) = control_create( ...
    'name', 'win_param', ...
	'alias', 'Parameter', ...
    'style', 'slider', ...
	'space', 1.5, ...
    'width', 0.46, ...
    'align', 'right', ...
    'space', 1.75, ...
    'initialstate', '__DISABLE__', ...
    'min', param.min, ...
    'max', param.max, ...
    'value', value ...
    );

%---
% Window Length slider
%---
control(end + 1) = control_create( ...
    'name', 'win_length', ...
	'alias', 'Length', ...
    'style', 'slider', ...
	'space', -2.17, ...
    'width', 0.46, ...
    'align', 'left', ...
    'initialstate', '__DISABLE__', ...
    'min', 1 / 64, ...
    'max', 1, ...
    'value', parameter.win_length ...
    );

%---
% Signal Filter checkbox
%---
control(end + 1) = control_create( ...
    'name', 'filter_flag', ...
    'alias','Whiten', ...
    'style', 'checkbox', ...
    'width', 0.46, ...
    'align', 'right', ...
    'space', -0.8, ...
    'initialstate', '__DISABLE__', ...
    'onload', 1, ...
    'value', parameter.filter_flag ...
    );

%---
% Filter Order slider
%---
control(end + 1) = control_create( ...
    'name', 'order', ...
	'alias', ' ', ...
    'style', 'slider', ...
    'type', 'integer', ...
    'onload', 1, ...
    'space', 1.5, ...
    'width', 0.46, ...
    'align', 'right', ...
    'initialstate', '__DISABLE__', ...
    'min', 1, ...
    'max', 512, ...
    'value', parameter.order ...
    );

%-------------------------------------------------------------------------
% OUTPUT PARAMETERS SEPARATOR
%-------------------------------------------------------------------------

control(end + 1) = control_create( ...
	'string','Output', ...
	'style','separator', ...
	'type','header' ...
);

%---
% Output Type popup
%---
str = {'log', 'selection table'};
control(end + 1) = control_create( ...
  'name', 'output_type', ...
  'alias', 'Type', ...
  'style', 'popup', ...
  'width', 0.46, ...
  'align', 'left', ...
  'onload', 1, ...
	'space', -2.17, ...
  'string', str, ...
  'value', parameter.output_type_idx ...
);

%---
% Maximum Size checkbox
%---
control(end + 1) = control_create( ...
    'name', 'chop_flag', ...
    'alias','Max Size', ...
    'style', 'checkbox', ...
    'width', 0.46, ...
    'align', 'right', ...
    'space', -0.8, ...
    'value', parameter.chop_flag ...
    );

%---
% Maximum Size slider
%---
control(end + 1) = control_create( ...
    'name', 'max_num_events', ...
    'alias', ' ', ...
    'style', 'slider', ...
    'type', 'integer', ...
    'onload', 1, ...
    'space', 0.5, ...
    'width', 0.46, ...
    'align', 'right', ...
    'min', 1, ...
    'max', 100000, ...
    'value', parameter.max_num_events ...
    );

%---
% Add Token popup
%---
str = { 'Sound Name', 'Detector Name', 'Preset Name', 'Time', 'Unique Identifier' };

control(end + 1) = control_create( ...
    'name', 'add_token', ...
    'alias', ' ', ...
    'style', 'popup', ...
    'align', 'right', ...
    'width', 0.4, ...
    'space', -0.75, ...
    'string', str, ...
    'value', 1 ...
);

%---
% File Name edit box
%---
control(end + 1) = control_create( ...
	'name', 'output', ...
	'alias', 'File Name', ...
	'style', 'edit', ...
	'type', 'filename', ...
    'space', 1.5, ...
	'string', '%SOUND_NAME%_%PRESET_NAME%_%TIME%' ...
);

%--
% Output Path edit box
%--
control(end + 1) = control_create( ...
	'name', 'out_path', ...
	'style', 'edit', ...
	'space', 1.5 ...
);

% TODO: add some controls to display preset info, include tags and notes?