function [result, context] = prepare(parameter, context)

%% refresh log name and path

% Copyright (C) 2002-2012 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%initializations
result = struct;
context.state.kill = 0;

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end

