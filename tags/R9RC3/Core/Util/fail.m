function fail(txt, dlg_name)
% Displays text in warning dialog and in red font in MATLAB Desktop
%
% Input:
% ------
%	txt - string to display
%	dlg_name - title of warning dialog and text message to MATLAB Desktop

% History
%   msp2 - 27 Apr 2014
%       If dlg_name is not included in function call, set to "WARNING".

if ~exist( 'dlg_name', 'var' )
    dlg_name = 'WARNING';
end

fprintf(2,'\n\n%s\n%s\n', dlg_name, txt);
h = warndlg(txt, dlg_name);
movegui(h, 'center')
