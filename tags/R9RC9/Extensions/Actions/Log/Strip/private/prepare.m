function [result, context] = prepare(parameter, context)
% EXAMPLE LOG ACTION - prepare

% History
%   msp2 - 19 Mar 2014
%       Refresh log.name and log.path.
%       Avoid memory leak by skipping compute.

% prepare.m contents here
result = struct;
tstart = tic;

%Stop if any open browsers
if close_browsers
  return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
  fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
  return;
end

% for each log
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );
NumLogs = length( logs );

for i = 1:NumLogs

  t0 = tic;
  
  %indicate log being processed in waitbar
  waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...'] );
  
  %load and refresh path in log
  [ log, ~, status ] = load_log( logs{ i }, context );
  if status
    return;
  end    
 
    %--
    % strip log events
    %--

    if parameter.all

        for k = 1:length(log.event)
            log.event(k).measurement = empty(measurement_create);
        end

    else

        for k = 1:length(log.event)

            for j = length(log.event(k).measurement):-1:1

                if ~ismember(log.event(k).measurement(j).name, parameter.measures)
                    continue;
                end

                log.event(k).measurement(j) = [];

            end

        end

    end

    %--
    % save stripped log
    %--

    % NOTE: if we are modifying the input log file backup 

    if parameter.inplace || isempty(parameter.suffix)
        log_backup(log);
    else
        log.file = [file_ext(log.file), parameter.suffix, '.mat'];
    end

    log_save(log);
  
  %indicate log processed in waitbar
  result.target = log;
  result.status = 'done';
  result.message = '';
  result.error = [];
  result.elapsed = toc( t0 );
  result.output = struct( [] );
  action_waitbar_update( wh(1), i/NumLogs, result, 'log' ); 
    
end

d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc(tstart) );
waitbar_update( wh(1), 'PROGRESS', 'message', d );

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m
