function browser_kpfun(par, key)

% browser_kpfun - key press function for browser figure
% -----------------------------------------------------
%
% browser_kpfun(par, key)
%
% Input:
% ------
%  par - browser
%  key - keypress information

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 23 Nov 2013
%       Add "one-click" mode which pastes event and logs it with left mouse click.
%       Activated by "!" key.  Has cross pointer.
%	msp2 - 18 Dec 2013
%		Add "quick" mode which commits selections made by dragging the 
%		mouse pointer in browser spectrogram.  Activated by "q" key.  
%		Has addpole pointer.
%	msp2 - 20 Dec 2013
%		Callback to F9 generates left mouse click
%		Callback to F10 generates right mouse click
%		Delete callbacks to 'b' and 'n', which duplicate '[' and ']'
%		Delete callbacks to 'f' and 'g', since callbacks are broken and never used
%		Change lower case colormap callbacks to upper case 'A', 'C', 'I'
%	msp2 - 28 Dec 2013
%		Add "one_click_freq" mode similar to "one-click" mode, but tracks
%		frequency of mouse pointer, and has crosshair pointer.
% msp2 - 31 Dec 2013
%   Add "quick_delete" mode, which deletes events with a left mouse click.
%   Activated by "d" key, and has "X" pointer.
% msp2 - 10 Mar 2014
%	Disable "<" (Previous View) and ">" (Next View) because callback
%	doesn't work.
% msp2 - 30 Mar 2014
%   Add "tag" mode that adds tags to existing events.
% msp2 - 30 Mar 2014
%	Disable keyboard shortcuts for browser mode if "Browse" palette open.
% msp2 - 30 Apr 2014
%   Remove "!" as keyboard shortcut for "one-click" mode.
%   Open Event palette if not open when "e" or "E" key are pressed.
%   Change keyboard shortcuts to set "Tag" mode and "Delete" mode to "T" and "D"
%   Set "t" key to add tag from browser selection clipboard to selected event.
%   Set "d" key to delete selected event.
%   Make "t" and "d" keys active only if Browse palette is not open.
% msp2 - 6 Apr 2014
%   Put focus on Browse palette and create warning popup box if user attempts 
%    to use keyboard shortcuts for browse mode when Browse palette is open.
% msp2 - 7 May 2014
%   Allow keyboard control of browse mode if "Control Mode From Keyboard"
%    is enabled in Browse palette.
% msp2 - 28 May 2014
%   If Event palette is not open when user presses "e" or "E" key, open
%    Event palette *without* going to next or previous event.
%   Add keyboard shortcuts for opening several more palettes.
% msp2 - 30 May 2014
%   (Esc) key toggles Go/Stop button in Autoadvance palette.
%   When opening palettes, remember previous position.
% msp2 - 12 June 2014
%   "d" key / "D" key set quick_delete mode if not already set, and otherwise
%     deletes current event before jumping to next / previous event.
%   "t" key / "T" key set tag mode if not already set, and otherwise
%     tag current event (if active log) before jumping to next / previous event.
% msp2 - 20 June 2014
%    Warn the user if they attempt to control browser mode using keyboard
%      shortcuts when the Browse palette is open.
% msp2 - 29 July 2014
%    Warn the user if they attempt to control browser mode using keyboard
%      shortcuts when the Browse palette is open.  (Up until now, all
%      keyboard shortcuts were blocked when Browse palette was open.)

% To Do
%	F7 and F8 callbacks break when Hide Silence is enabled

% NOTE: we could use the 'shift', 'return', and 'tab' keys for modal or focal changes

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% check handle input
%--

if ~is_browser(par)
	return;
end

%--
% get keypress struct
%--

if nargin < 2
	key = get_keypress(par);
end

% NOTE: build keypress struct from character input

if ischar(key)
	key.char = key; key.code = double(key.char); key.key = [];
end

%--------------------------------------------
% KEYPRESS CALLBACK
%--------------------------------------------

if strcmp(key.key, 'space')
	
	if isempty(get_player_buffer)
		
		sel = get_browser_selection(par);
		
		if isempty(sel.event)
			browser_sound_menu(par, 'Page');
		else 
			browser_sound_menu(par, 'Selection');
		end
		
	end
	
end

%--
% handle function keys
%--

% NOTE: at the moment we use these for various visibility commands

if length( key.key ) > 1  && ~isempty( regexp( key.key, 'f\d', 'match' ) )
    
    data = get(par, 'userdata');
	
	switch key.key

		%--
		% toggle palette display
		%--
		
		case 'f1'
			browser_window_menu(par, 'Hide Palettes');
		
		%--
		% toggle other sounds display
		%--
		
		case 'f2'
			browser_window_menu(par, 'TOGGLE_OTHERS');
		
		%--
		% toggle desktop display
		%--
		
		case 'f3'
			browser_window_menu(par, 'TOGGLE_DESKTOP');
		
		%--
		% cycle through open sounds, previous and next sound
		%--
		
		% NOTE: this is particularly useful when hiding other sounds
		
		% NOTE: 'f6' is back because forward is more intuitive and 'f5' more accesible
		
		case 'f5'
			show_browser(par, 'next'); 
		
		case 'f6'
			show_browser(par, 'previous');
            
        % 'f7' is Previous Time-Stamp
        case 'f7'
            sessions = get_sound_sessions(data.browser.sound, 0);
            ix = get_current_session(data.browser.sound);
            if ix > 1
                time = map_time( ...
                    data.browser.sound, 'slider', 'real', sessions(ix - 1).start);
                set_time_slider(par, 'value', time);
            end

        % 'f8' is Next Time-Stamp
        case 'f8'
            sessions = get_sound_sessions(data.browser.sound, 0);
            ix = get_current_session(data.browser.sound);
            if ix < length(sessions)
                time = map_time( ...
                    data.browser.sound, 'slider', 'real', sessions(ix + 1).start);
                set_time_slider(par, 'value', time);
            end
		
		% 'f9' is left mouse click
		case 'f9'
			robot = java.awt.Robot;
			robot.mousePress  (java.awt.event.InputEvent.BUTTON1_MASK);
			robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);	
		
		% 'f10' is right mouse click
		case 'f10'
			robot = java.awt.Robot;
			robot.mousePress  (java.awt.event.InputEvent.BUTTON3_MASK);
			robot.mouseRelease(java.awt.event.InputEvent.BUTTON3_MASK);	
	end
	
	return;
	
end 
	
%--
% return on empty characters
%--

if isempty(key.code)
	return;
end

%--
% handle keystroke commands
%--

switch key.code
					
	%--------------------------------------------
	% AUTOADVANCE COMMANDS
	%--------------------------------------------
    
    % escape key
    case 27
        obj = findobj( par, 'Tag', 'jog_toggle' );
        browser_controls( par, 'jog_toggle', get( obj, 'String' ) )
    
    
	%--------------------------------------------
	% SOUND COMMANDS
	%--------------------------------------------
	
	%--
	% Play Page
	%--

	case double('P')
		browser_sound_menu(par, 'Page');
		
	%--
	% Play Selection
	%--
	
	case double('p')
		
		%--
		% get availability of selection from menu state
		%--
		
		data = get(par, 'userdata');
		
		% NOTE: return if current figure is not a proper browser figure
		
		if ~isfield(data, 'browser')
			return;
		end
		
		tmp = get_menu(data.browser.sound_menu.play, 'Selection');
		
		%--
		% play selection or page
		%--
				
		if strcmp(get(tmp, 'enable'), 'on')
			browser_sound_menu(par, 'Selection');
		else
			browser_sound_menu(par, 'Page');
		end
		
	%--
	% Other Rate
	%--
	
	case double('r')
		browser_sound_menu(par, 'Other Rate ...');
		
	%--
	% Volume Control
	%--
	
	case double('v')
		browser_sound_menu(par, 'Volume Control ...');
		
	%--------------------------------------------
	% NAVIGATE COMMANDS
	%--------------------------------------------
	
	%--
	% Previous Event
	%--	
	case double('E')
        navigate_events( par, key )
		
	%--
	% Next Event
	%--
	case double('e')
        navigate_events( par, key )
		
% 	% NOTE: the view navigation is currently unstable under the new
% 	% navigation framework	
% 	%--
% 	% Previous View
% 	%--
% 	
% 	case double('<')
% 		browser_view_menu(par, 'Previous View');
% 		
% 	%--
% 	% Next View
% 	%--
% 	
% 	case double('>')
% 		browser_view_menu(par, 'Next View');
		
	%--
	% Previous Page (back arrow)
	%--
	
	case 28
		browser_view_menu(par, 'Previous Page');
		
	%--
	% Next Page (forward arrow)
	%--
	
	case 29
		browser_view_menu(par, 'Next Page');
		
	%--
	% First Page (up arrow)
	%--
	
	case 30
		browser_zoom('out', par);
		
	%--
	% Last Page (down arrow)
	%--
	
	case 31
		browser_zoom('in', par);
		
	%--
	% Previous File
	%--
	
	% NOTE: perhaps this should be an idempotent commmand, look through the code
	
	case double('[')
		browser_view_menu(par, 'Previous File');
		
	%--
	% Next File
	%--
	
	case double(']')
		browser_view_menu(par, 'Next File');

	%--------------------------------------------
	% COLORMAP COMMANDS
	%--------------------------------------------
	
	%--
	% Gray 
	%--
	
	% NOTE: the trailing space in the name of the menu command
	
	case double('G')
		browser_view_menu(par, 'Gray ');
	
	%--
	% Hot 
	%--
	
	case double('H')
		browser_view_menu(par, 'Hot');
		
	%--
	% Jet
	%--
	
	case double('J')
		browser_view_menu(par, 'Jet');
		
	%--
	% Bone
	%--
	
	case double('B')
		browser_view_menu(par, 'Bone');
		
	%--
	% HSV
	%--
	
	case double('V')
		browser_view_menu(par, 'Jet');
		
	%--
	% Colorbar
	%--
	
	case double('C')
		browser_view_menu(par, 'Colorbar');
		
	%--
	% Auto Scale
	%--
	
	case double('A')
		browser_view_menu(par, 'Auto Scale');
			
	%--
	% Invert
	%--
	
	case double('I')
		browser_view_menu(par, 'Invert');
		
	%--------------------------------------------
	% SELECTION COMMANDS
	%--------------------------------------------
	
	%--
	% Delete Selection (delete), and Stop Play
	%--
	
	case 127
		
		%--
		% check for sound player
		%--
		
		% NOTE: eventually there may be multiple players, one per parent
		
		player = timerfind('name', 'PLAY_TIMER');
		
		if ~isempty(player)
			stop(player); return;
		end
		
		%--
		% delete selection from current figure
		%--
		
		delete_selection(par);
	
	%--
	% Log Selection To
	%--
	
	% NOTE: there are problems with the name of this command
	
	case double('l')
		
		handle = get_menu(par, 'Log Selection To');
		
		handles = get(handle, 'children');
		
		if strcmp(get(handles, 'enable'), 'on')
			browser_edit_menu(par, 'Log Selection To');
		end
		
	%--
	% Selection Grid
	%--
	
	case double('''')
		browser_view_menu(par, 'Grid');
				
	%--
	% Selection Labels
	%--
	
	case double('"')
		browser_view_menu(par, 'Labels');
				
	%--
	% Control Points
	%--
	
	case double('.')
		browser_view_menu(par, 'Control Points');
		
	%--
	% Selection Zoom
	%--
	
	% NOTE: this is a mode, however it sometimes behaves as an action
	
	case double('z')
		browser_view_menu(par, 'Selection Zoom');
			
	%--------------------------------------------
	% GRID COMMANDS
	%--------------------------------------------
	
	%--
	% Time Grid (used to be 'Grid')
	%--
	
	case double(';')
		browser_view_menu(par, 'Time Grid');
				
	%--
	% File Grid
	%--
	
	case double(':')
		browser_view_menu(par, 'File Grid');
			
	%--------------------------------------------
	% MISCELLANEOUS COMMANDS
	%--------------------------------------------
	
	%--
	% Actual Size
	%--
	
	case double('=')
		browser_window_menu(par, 'Actual Size');
		
	%--
	% Half Size
	%--
	
	case double('-')
		browser_window_menu(par, 'Half Size');
		
	%--------------------------------------------
	% PALETTE COMMANDS
	%--------------------------------------------
    case double( 'a' )
         browser_window_menu( par, 'Autoadvance' );
    case double( 'b' )
         browser_window_menu( par, 'Browse' );
    case double( 'c' )
         browser_window_menu( par, 'Colormap' );
    case double( 'g' )
         browser_window_menu( par, 'Grid' );
    case double( 'L' )
         browser_window_menu( par, 'Log' );
    case double( 'n' )
         browser_window_menu( par, 'Navigate' );
	case double( 'x' )
		xbat_palette;
		
	%--------------------------------------------
	% BROWSE MODE COMMANDS
	%--------------------------------------------
	otherwise

        %---------------------------------------------------------------
        % Set flag to execute Browse Mode shortcuts if Browse palette
        % closed or Browse palette has "Keyboard Shortcuts" selected
        %---------------------------------------------------------------
        pal = get_palette( par, 'Browse' );

        % If Browse palette is not open, set flag
        if isempty( pal )
            flag = 1;

        % If Browse palette is open, set flag only if "Keyboard Shortcuts" is selected.
        else
            obj = findobj( pal, 'Tag', 'keyboard_mode' );
            value = get( obj, 'Value' );
            flag = value;
        end

        if flag
            switch key.code

                %--
                % Tag Mode
                %--
                case { 't' }
                    
                    % if browser not in tag mode, set to tag mode
                    data = get_browser;
                    if ~strcmp( data.browser.selection.mode, 'tag' )
                        set_browser_mode(par, 'tag');
                        
                    % if in tag mode, tag selected event and go to next event
                    else
                        event_menu( 'next_event', 'Tag Event ...' )
                    end

                case { 'T' }
                    
                    % if browser not in tag mode, set to tag mode
                    data = get_browser;
                    if ~strcmp( data.browser.selection.mode, 'tag' )
                        set_browser_mode(par, 'tag');
                        
                    % if in tag mode, tag selected event and go to previous event
                    else
                        event_menu( 'previous_event', 'Tag Event ...' )
                    end

                %--
                % Delete Mode
                %--
                case( 'd' )
                    
                    %if browser not in delete mode, set to delete mode
                    data = get_browser;
                    if ~strcmp( data.browser.selection.mode, 'quick_delete' )
                        set_browser_mode(par, 'quick_delete');
                        
                    % if browser in delete mode, delete selected event and jump to next event
                    else
                        event_menu( 'next_event', 'Delete Event ...' )
                    end

                case( 'D' )
                    
                    %if browser not in delete mode, set to delete mode
                    data = get_browser;
                    if ~strcmp( data.browser.selection.mode, 'quick_delete' )
                        set_browser_mode(par, 'quick_delete');
                        
                    % if browser in delete mode, delete selected event and jump to previous event
                    else
                        event_menu( 'previous_event', 'Delete Event ...' )
                    end

                %--
                % Hand Mode
                %--

                case double('h')
                    % NOTE: this command should also be put into some other place
                    set_browser_mode(par, 'hand');

                %--
                % Select Mode
                %--
                case double('s')
                    set_browser_mode(par, 'select');

                %--
                % Quick Mode
                %--
                case { 'S' }
                    set_browser_mode(par, 'quick');

                %--
                % One-click Mode
                %--
                case { 'o' }
                    set_browser_mode(par, 'one_click');

                %--
                % One-click Mode with frequency tracking
                %--
                case { 'O' }
                    set_browser_mode(par, 'one_click_freq');

            end
        else
            t1 = 'To control browser modes from the keyboard, you must either';
            t2 = '     1. close the Browse palette or';
            t3 = '     2. select "Keyboard Shortcuts" in the Browse palette';
            t = sprintf( '%s\n%s\n%s', t1, t2, t3 );
            fail( t );
        end
		
end

% NOTE: consider if we want the browser to keep focus

%----------------------------------------------------------------------
function navigate_events( par, key )

    %--
    % get Event palette, if not found open
    %--
    [ g, flag ] = get_event_palette( par );
    
    %--
    % if Event palette not just opened, advance to next or previous event
    %--
    if ~flag
        if isequal( key.code, double('e') )
            control_callback([],g, 'next_event');
        elseif isequal( key.code, double('E') )
            control_callback([],g, 'previous_event');
        end
    end
        
