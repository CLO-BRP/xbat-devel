function state = get_palette_state(pal, data)

% get_palette_state - get position, toggle, and tab state
% -------------------------------------------------------
%
% state = get_palette_state(pal, data)
%
% Input:
% ------
%  pal - palette figure handle
%  data - palette userdata
%
% Output:
% -------
%  state - palette state

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 19 May 2014
%       Save state of popup and checkbox uielements to palette state.
%   msp2 - 21 May 2014
%       Record palette state for buttons in browser data.
%   msp2 - 22 May 2014
%       Browse palette
%           Fix bug that caused failure with buttons in Page and Navigate palette.
%           Recover backgroundcolor and fontweight of buttons.
%   msp2 - 23 May 2014
%       Browse palette
%           Fix bug that caused problems with Log palette.
%           Save button font color to browser state.
%           Save cursor icon on each button to browser state.
%   msp2 - 27 May 2014
%       Event palette: save state of sort order popup menu

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

%--
% get palette userdata if needed
%--

if (nargin < 2) || isempty(data)
	data = get(pal, 'userdata');
end

%--
% get palette-specific properties
%--
switch data.name
    case 'Browse'
        data = browse_state( data, pal );
    case 'Event'
        data = event_state( data, pal );        
end

%----------------------------------------------------
% GET NAME, TAG, AND POSITION
%----------------------------------------------------

% NOTE: use absolute position, relative position causes problems

state.name = get(pal, 'name');

state.tag = get(pal, 'tag');

state.position = get(pal, 'position');

state.data = data;

%----------------------------------------------------
% GET TOGGLE STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'toggle') && ~isempty(data.toggle)
	
	%--
	% loop over toggles
	%--
	
	for k = 1:length(data.toggle)
		
		tmp = data.toggle(k).toggle;
		
		%--
		% get toggle name
		%--
		
		state.toggle(k).name = get(get(tmp, 'parent'), 'tag');
		
		%--
		% get toggle state
		%--
		
		if strcmp(get(tmp, 'string'), '+')
			state.toggle(k).state = 'close';
		else
			state.toggle(k).state = 'open';
		end
		
	end
	
else
	
	%--
	% there are no toggles
	%--
	
	state.toggle = [];
	
end

%----------------------------------------------------
% GET TAB STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'tabs') && ~isempty(data.tabs)
	
	%--
	% loop over tabs
	%--
	
	for k = 1:length(data.tabs)
		
		%--
		% get text children of tabs axes
		%--
				
		% NOTE: problems arise when we have elements with no name
		
		label = findobj(get(findobj(pal, 'tag', data.tabs(k).name), 'children'), 'type', 'text');
				
		%--
		% select darkest color label to be the active tab 
		%--
		
		% NOTE: get colors, convert to matrix, sum along columns, and get the minimum value index
		
		[~, ix] = min(sum(cell2mat(get(label, 'color')), 2));
				
		%--
		% store active tab name
		%--
		
		state.tabs(k).tab = get(label(ix), 'string');
		
	end
	
else
	
	%--
	% there are no tabs
	%--
	
	state.tabs = [];
	
end

%------------------------------------
function data = browse_state( data, pal )

    %--
    % update Browse palette userdata
    %--
    style_list = { 'popup', 'popupmenu', 'checkbox', 'buttongroup', 'pushbutton' };

    % prop_state = data.control;
    data.control = data.control( ismember( { data.control( : ).style }, style_list ) );

    obj = findobj( pal, 'Type', 'uicontrol' );
    style = get( obj, 'Style' );
    obj = obj( ismember( style, style_list ) );
    name = get( obj, 'Tag' );

    for i = 1 : length( data.control );
        curr_obj = obj( ismember( name, data.control( i ).name ) );
        if ~isempty( curr_obj )
            curr_style = get( curr_obj, 'Style' );

            % save state of uicontrols on style_list, except pushbuttons
            if ~any( strcmp( curr_style, 'pushbutton' ) )
                data.control( i ).value = get( curr_obj, 'Value' );

            % save state of pushbuttons in "Browse" palette
            else

                % initialize control userdata
                if isempty( data.control( i ).userdata )
                    data.control( i ).userdata = cell( size( data.control( i ).name ) );
                end

                % set alias, tooltip and userdata in data.control
                button_name = get( curr_obj, 'Tag' );
                for j = 1 : length( button_name )
                    idx = strcmp( data.control( i ).name, button_name( j ) );
                    data.control( i ).alias{ idx } = get( curr_obj( j ), 'String' );
                    data.control( i ).tooltip{ idx } = get( curr_obj( j ), 'TooltipString' );
                    data.control( i ).backgroundcolor{ idx } = get( curr_obj( j ), 'BackgroundColor' );
                    data.control( i ).foregroundcolor{ idx } = get( curr_obj( j ), 'ForegroundColor' );
                    data.control( i ).fontweight{ idx } = get( curr_obj( j ), 'FontWeight' );
                    data.control( i ).cdata{ idx } = get( curr_obj( j ), 'cdata' );
                    data.control( i ).userdata{ idx } = get( curr_obj( j ), 'UserData' );
                end
            end
        end
    end

%------------------------------------
function data = event_state( data, pal )

%--
% update Event palette userdata
%--
    style_list = { 'popup', 'popupmenu' };

    % prop_state = data.control;
    data.control = data.control( ismember( { data.control( : ).style }, style_list ) );

    obj = findobj( pal, 'Type', 'uicontrol' );
    style = get( obj, 'Style' );
    obj = obj( ismember( style, style_list ) );
    name = get( obj, 'Tag' );

    curr_obj = obj( strcmp( name, data.control.name ) );
    if ~isempty( curr_obj ) && strcmp( name, 'sort_order' )
        data.control.value = get( curr_obj, 'Value' );
    end
    
