function [result, context] = compute(sound, ~, context)
% function [result, context] = compute(sound, parameter, context)

%% REFRESH LOGS - compute
%
%
%**************************************************************************
% THIS VERSION IS NOT CONFUSED BY HAVING MULTIPLE LOGS WITH THE SAME FILE
% NAME IN MULTIPLE XBAT LIBRARY FOLDERS
%**************************************************************************
%
% Refreshed structure variable name and file field for XBAT logs, using the
% file name of the log
%
% Allows XBAT actions to be run on XBAT logs whose names have been changed
% without having to first open them in XBAT. 
%
%%
% initializations
result = struct;

% list names of logs for sound
logs = get_library_logs('info',[],sound);


%--- start pitz 20121212

% get date-time attribute for sound from library
realtime_lib = file_datenum(sound.file{1});

%--- end pitz 20121212


% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  NewLog = get_library_logs('logs',[],sound,CurrentLog);
  NewLog = NewLog(1);
  
  
  %--- start pitz 20121212
  
  realtime_log = file_datenum(NewLog.sound.file{1});
  
  if ~isequal(realtime_lib, realtime_log)
    
    d=sprintf('\nLog: %s\nwas last used with\nsound: %s\nthat starts at a different time.\n', ... 
              log_name(log), sound_name(sound));
            
    fail(d, 'ERROR')
    
    return;
    
  end
  
  if ~isequal(length(sound.file), length(NewLog.sound.file))
    
    d=sprintf('\nLog: %s\nwas last used with\nsound: %s\nthat had a different number of sound files.\n', ... 
              log_name(log), sound_name(sound));
            
    fail(d, 'ERROR')
    
    return;
    
  end
  
  %--- end pitz 20121212
  
  % rename file field in log
  [SoundPath fn] = fileparts(CurrentLog);
  fnExt = [fn '.mat'];
  NewLog.file = fnExt;
  
  % rename path field in log
  LogPath = context.library.path;
  LogPath = [LogPath SoundPath '\Logs\'];
  NewLog.path = LogPath;
  
  %--- start pitz 20111028
  
  NewLog.sound = sound;
  
  %--- end pitz 20111028

  % save log
  log_save(NewLog);
end
