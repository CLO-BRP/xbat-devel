function [result, context] = prepare(parameter, context)

% Copy Attributes from First Selected Sound - prepare

% History
%   msp2 - 08 July 2014
%       Copy "Sensor Geometry Uncertainty" and "Sound Speed Uncertainty"
%         attributes, if they exist.

%initializations
result = struct;

%terminate if fewer than 2 sounds are selected
if length(context.target)<2
  fprintf(2,'Action failed. Select 2 or more sounds and try again.\n');
  return;
end

%get paths of attribute CSVs from 1st sound
SndSource = context.target(1).path;
GeoPath = '__XBAT\Attributes\sensor_geometry.csv';
SpeedPath = '__XBAT\Attributes\sound_speed.csv';
CalPath = '__XBAT\Attributes\sensor_calibration.csv';
GeoUncPath = '__XBAT\Attributes\sensor_geometry_uncertainty.csv';
SpeedUncPath = '__XBAT\Attributes\sound_speed_uncertainty.csv';

% %% --- pitz 20090924
% 
% FlagGeo = 0;
% 
% FlagSpeed = 0;
% 
% FlagCal = 0;
% 
% missing = {};
% 
% %terminate if Sensor Geometry is missing in first sound
% if ~exist([SndSource GeoPath], 'file')
%   
%   FlagGeo = 1;
%   
%   missing = [missing; 'Sensor Geometry Attribute'];
%   
% end
% 
% %terminate if Sound Speed is missing in first sound
% if ~exist([SndSource SpeedPath], 'file')
%   
%   FlagSpeed = 1;
%   
%   missing = [missing; 'Sound Speed Attribute'];
%   
% end
% 
% %terminate if Sensor Calibration is missing in first sound
% if ~exist([SndSource CalPath], 'file')
%   
%   FlagCal = 1;
%   
%   missing = [missing; 'Sensor Calibration Attribute'];
%   
% end
% 
% if FlagGeo || FlagSpeed || FlagCal
%   
%   txt1 = 'Future analysis is at risk because these are missing:';
%   
%   txt2 = sprintf('%s\n', missing);
%   
%   txt3 = sprintf('\nProceed anyway?');
%   
%   txt = sprintf('%s%s%s', txt1, txt2, txt3);
%   
%   button = questdlg(txt, 'WARNING', 'Yes', 'No', 'Yes');
%   
% end
% 
% if strcmp(button, 'No')
%   
%   return;
%   
% end
% 
% %% add code that prevents copy errors when attributes are missing
% %%
% 
% 
% %% --- end



%terminate if Sensor Geometry, Sound Speed, and/or Sensor Calibration are
%missing in first sound
if ~exist( fullfile( SndSource, GeoPath ), 'file') ...
|| ~exist( fullfile( SndSource, SpeedPath ), 'file') ...
|| ~exist( fullfile( SndSource, CalPath ), 'file')
  
  txt1 = 'The first selected sound must have these three attributes set:';
  
  txt2 = '     Sensor Geometry';
  
  txt3 = '     Sound Speed';
  
  txt4 = '     Sensor Calibration';
  
  txt5 = sprintf('\n%s\n%s\n%s\n%s\n', txt1, txt2, txt3, txt4);
  
  h1 = warndlg(txt5, 'ERROR');

  movegui(h1, 'center')
  
  fprintf(2,'%s', txt5);
  
  return;
  
end

%--
%for 2nd through end sound
%--
for i = 2:length(context.target)
    
  %--
  % copy attributes
  %--
  
  SndDest = context.target(i).path;
  
  % if no '__XBAT\Attributes\' directory, then create
  if ~exist( fullfile( SndDest, '__XBAT\Attributes' ), 'dir')
    mkdir( fullfile( SndDest, '__XBAT\Attributes' ) );
  end

  %if user requests overwrite, overwrite without checking to see what's there
  if parameter.overwrite
    copyfile( fullfile( SndSource, GeoPath ),  fullfile( SndDest, GeoPath ) );
    copyfile( fullfile( SndSource, SpeedPath ),  fullfile( SndDest, SpeedPath ) );
    copyfile( fullfile( SndSource, CalPath ),  fullfile( SndDest, CalPath ) );
    
    % if uncertainty attributes exist in source directory, copy without checking
    if exist( fullfile( SndSource, GeoUncPath ), 'file' )
        copyfile( fullfile( SndSource, GeoUncPath ),  fullfile( SndDest, GeoUncPath ) );
    end
    if exist( fullfile( SndSource, SpeedUncPath ), 'file' )
        copyfile( fullfile( SndSource, SpeedUncPath ),  fullfile( SndDest, SpeedUncPath ) );
    end
  
  %--
  %otherwise check to see if attribute exists before overwriting  
  %--
  
  %terminate if sensor geometry file already exists for current sound
  else
      if exist( fullfile( SndDest, GeoPath ), 'file')
        fprintf(2,'Action terminated. Sensor geometry attribute already set for %s\n', SndDest);
        return;

      %terminate if sound speed file already exists for current sound
      elseif exist( fullfile( SndDest, SpeedPath ), 'file')
        fprintf(2,'Action terminated. Sound speed attribute already set for %s\n', SndDest);
        return;

      %terminate if sensor calibration file already exists for current sound
      elseif exist( fullfile( SndDest, CalPath ), 'file')
        fprintf(2,'Action terminated. Sensor Calibration attribute already set for %s\n', SndDest);
        return;

      %terminate if sensor calibration file already exists for current sound
      elseif exist( fullfile( SndDest, GeoUncPath ), 'file')
        fprintf(2,'Action terminated. Sensor geometry uncertainty attribute already set for %s\n', SndDest);
        return;

      %terminate if sensor calibration file already exists for current sound
      elseif exist( fullfile( SndDest, SpeedUncPath ), 'file')
        fprintf(2,'Action terminated. Sensor speed uncertainty attribute already set for %s\n', SndDest);
        return;

      %no sensor geometry or sound speed attributes exist, so copy them from
      %first sound
      else
        copyfile( fullfile( SndSource, GeoPath ),  fullfile( SndDest, GeoPath ) );
        copyfile( fullfile( SndSource, SpeedPath ),  fullfile( SndDest, SpeedPath ) );
        copyfile( fullfile( SndSource, CalPath ),  fullfile( SndDest, CalPath ) );
    
        % if uncertainty attributes exist in source directory, copy
        if exist( fullfile( SndSource, GeoUncPath ), 'file' )
            copyfile( fullfile( SndSource, GeoUncPath ),  fullfile( SndDest, GeoUncPath ) );
        end
        if exist( fullfile( SndSource, SpeedUncPath ), 'file' )
            copyfile( fullfile( SndSource, SpeedUncPath ),  fullfile( SndDest, SpeedUncPath ) );
        end
      end
  end  
end
