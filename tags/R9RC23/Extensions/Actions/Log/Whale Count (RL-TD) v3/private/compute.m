function [result, context] = compute(log, parameter, context)
result = struct;

% WHALE COUNT (RL-TD) v3 - compute

% Counts minimum number of whales using RL-TD measurements, comparing the
% calculated maximum speed a whale could be moving (if it were directly
% between the sensors swimming directory toward one of the sensors) to the 
% maximum known speed for that type of whale to determine the minimum 
% number of whales present
%
% Counts tracks on one channel pair at a time, then used the maximum count 
% for each log.

%% TO DO
%
% 1. Skip events with no RL-TD measurements, rather than crashing
% 2. Output event data to CSV
% 3. Better (or user-selectable) method(s) for deciding which track to
%    assign each time delay, when a choice is available.

%%
%initializations
fn = log.file;
fn = fn(1:end-4);

latency = parameter.latency*60; %if no track in this time, start looking for new whale
SpeedLimit = parameter.SpeedLimit;  %km/hr
TDtolerance = parameter.TDtolerance;         %tolerance for error in TD measurement
TDlimit = parameter.TDlimit;         %tolerance for error in TD measurement
HourChop = parameter.HourChop; %count whales only within outs
IndexChOnly = parameter.IndexChOnly; %use only ch-pairs which include index channel
MinTrackLen = parameter.MinTrackLen; %minimum TDs before track counts
check = parameter.check; %check if date-time attribute in log is correct
plots = parameter.plots; %create time delay plot figures?
LeftHour = parameter.LeftHour;           %plotting limits for x-axis (hours)
RightHour = parameter.RightHour;        %plotting limits for x-axis (hours)
csvTrack = parameter.csvTrack;     %create csv output for track summaries
csvEvent = parameter.csvEvent;     %create csv output for event summaries
TrackSummary = parameter.TrackSummary; %output summary data for each track
TrackDetails = parameter.TrackDetails; %output data for each event

if HourChop
  HourChopStr='Y';
else
  HourChopStr='N';
end
  
if IndexChOnly
  IndexChOnlyStr='Y';
else
  IndexChOnlyStr='N';
end

if check
  checkStr='Y';
else
  checkStr='N';
end

%initialize number of channels
NumChan = log.sound.channels;
if ~isscalar(NumChan)
  fprintf(2,'Number of channels field missing in %s\n', fn);
  return;
end

%initialize SoundSpeed
SoundSpeed = log.sound.speed;

if isempty(SoundSpeed)

  %ask user for sound speed
  SoundSpeedStr = inputdlg( ...
    'Enter speed of sound (m/s)', ...
    sprintf('Sound Speed attribute not set for %s',fn), ...
    1, ...
    {'1500'} ...
  );

  if isempty(SoundSpeedStr)
    return;
  else
    SoundSpeed = str2double(SoundSpeedStr{1});
  end

end

SoundSpeed = SoundSpeed / 1000;  %convert from m to km per second

if check
  fprintf(2,'\nCheck date-time attribute utility under construction\n\n');  
end

%output header to MATLAB Command Window
fprintf(1,'%s\n\n',fn);

fprintf(1,'Check Date-time Attribute in Log:          %s\n\n',checkStr);
fprintf(1,'Number of channels:                        %.0f\n',NumChan);
fprintf(1,'Speed of Sound (m/s):                      %.0f\n\n',SoundSpeed*1000);

fprintf(1,'Maximum Speed of Target (km/hr):           %f\n',SpeedLimit);
fprintf(1,'Length of Sliding Window (min):            %.f\n',latency/60);
fprintf(1,'Time Delay tolerance (s):                  %f\n',TDtolerance);
fprintf(1,'Ignore Time Delays greater than � seconds: %f\n',TDlimit);
fprintf(1,'Minimum Time Delays in Track:              %.0f\n',MinTrackLen);
fprintf(1,'Count within hours only:                   %s\n',HourChopStr);
fprintf(1,'Count only channel-pairs with index channel: %s\n',IndexChOnlyStr);

if TrackDetails
  
  %write headers for time delay details to MATLAB Command Window, if user selected
  fprintf(2,'         Temp   Running Running    Min Vel            dTD/dt                            \n');
  fprintf(2,'Channel  Track   Num     Track    (re:last)   TD    (re:last)  Event   Arrival   Expire \n');
  fprintf(2,' A   B     ID   Events  Length     (km/h)     (s)               ID      Time      Time  \n\n');
  
  %format for writing data
  f2 = '%2.0f  %2.0f  %5.0f  %5.0f  %9s  %9.3f  %7.4f  %7.4f  %5.0f  %9s  %9s  \n';
  
end

if TrackSummary

  %write headers for track details and summary to MATLAB Command Window, if user selected
  fprintf(1,'\n');
  fprintf(1,'                                   Mean      Mean                                           \n');
  fprintf(1,'Channel  Track   Num    Track     Min Vel     TD       Mean   Resid       p-                \n');
  fprintf(1,' A   B     ID   Events  Length    (km/h)      (s)     dTD/dt   Var      value    R2       F \n\n');
  
  %format for writing data
  f = '%2.0f  %2.0f  %5.0f  %5.0f  %9s  %9.3f  %7.4f  %7.4f  %5.3f  %8.6f  %5.3f  %11.6f \n';
  
end

% find date-time attribute and time offset in seconds for day
realtime = log.sound.realtime;
if ~isempty(realtime)
  offset = 86400 * mod(realtime,1);  %recording start time (seconds from midnight)
else
  offset = 0;
  fprintf(2,'\nNo date-time attribute set. Assume sound starts at midnight for %s\n\n',fn);
end

%count whales
if log.length < 2
  WhaleCount = log.length;
  
else
  %initializations
  WhaleCount = 1;
  
%   %find date-time attribute
%   realtime = log.sound.realtime;
%   if ~isempty(realtime)
%     offset = 86400 * mod(realtime,1);  %recording start time (seconds from midnight)
%   else
%     offset = 0;
%     fprintf(2,'\nNo date-time attribute set. Assume sound starts at midnight for %s\n\n',fn);
%   end
  
  %set color rotation for plots  
  lcolor = [   1   0   1;
               0 .75 .75;
               1   0   0;
               0 .75   0;
             .75 .75   1;
               1  .1 .55;
               0   1   1;
              .75   0 .75;
               1 .75   0;
               0  .5  .5;
             .75 .25 .25;
              .5  .5   1  ];
          
  ncolor = length(lcolor);

%% create CSV and write header, if user selected
  if csvTrack

    %Find output file path (Logs folder of selected sound)
    LibPath = context.library.path;
    SoundName = sound_name(get_active_sound);
    SoundPath = [LibPath SoundName '\Logs\'];

    fn = [fn '_TracksSummary_' datestr(now,30) '.csv'];

    %Open output file
    fid = fopen([SoundPath fn], 'w');

    %output header
    fprintf(fid,'Summary Data for RL-TD Tracks\n');
    fprintf(fid,'%s\n\n',fn);

    fprintf(fid,'SETTINGS:\n');
    fprintf(fid,'%s,Check Date-time Attribute in Log\n\n',checkStr);
    fprintf(fid,'%.0f,Number of channels\n',NumChan);
    fprintf(fid,'%.0f,Speed of Sound (m/s)\n\n',SoundSpeed*1000);

    fprintf(fid,'%f,Maximum Speed of Target (km/hr)\n',SpeedLimit);
    fprintf(fid,'%f,Length of Sliding Window (min)\n',latency/60);
    fprintf(fid,'%f,Time Delay tolerance (s)\n',TDtolerance);
    fprintf(fid,'%f,Ignore Time Delays greater than � seconds\n',TDlimit);
    fprintf(fid,'%.0f,Minimum Time Delays in Track\n',MinTrackLen);
    fprintf(fid,'%s,Count within hours only\n\n',HourChopStr);
    fprintf(fid,'%s,Count only channel-pairs with index channel\n\n',IndexChOnlyStr);

    fprintf(fid,'1st Channel,2nd Channel,Track ID,Num Events,Track Length,Minimum Velocity (km/h),Mean TD (s),dTD/dt,Variance of Residual,p-value,R2,F\n\n');

    %format for output lines
    fCSV = '%f,%f,%f,%f,%s,%f,%f,%f,%f,%f,%f,%f\n';

  end

%% CALCULATE GUIDLINES FOR PLOTS, IF PLOTS REQUESTED BY USER

  if plots

    %slope for dTD/dt
    slope = (2/3600)*(SpeedLimit/SoundSpeed);

    %x data for guidelines
    xlimit = 86400;
    x = 0:xlimit;   
    lenx = xlimit + 1;

    %y data for guidelines
    y = zeros(1,lenx);
    y2 = y;
    current = TDlimit/2;
    current2 = -current;

    for i = 1:lenx

      if current > TDlimit
        current = -TDlimit;
      end

      y(i) = current;
      current = current + slope;

      if current2 > TDlimit
        current2 = -TDlimit;
      end

      y2(i) = current2;
      current2 = current2 + slope;

    end
  end
  
%% cycle through channel pairs, where m is the 1st channel in the pair and the second

  j = 0;         %j is index for channel pair in pairwise_delta_t
  NumPlots = 0;  %NumPlots is index for channel pairs plotted

  for m = 1 : NumChan-1
    for n = m+1 : NumChan
  
      j = j + 1;
      track = [];
      expired = [];
      NumTracks = 0;
      NumExpired = 0;

%% READ IN DATA FROM ENTIRE LOG FOR CHANNEL PAIR m-n

      %initializations
      TD = zeros(1,log.length);
      all_time = TD;
      ExpireTime =  TD;
      id =  TD;
      t =  TD;
      k = 0;  %counter for events for this channel pair

      %loop through all events in log, extract data into ch-pr-specific array
      for i = 1:log.length
        try
          
          %if time delay exists and is within TDlimit 
          if isfinite(log.event(i).measurement.value.pairwise_delta_t(j)) ...
            && abs(log.event(i).measurement.value.pairwise_delta_t(j)) < TDlimit
          
            %if user choses, use only time delays that have index channel 
            %in current channel pair
            if ~IndexChOnly || (IndexChOnly && ...
              (isequal(log.event(i).channel,m) || isequal(log.event(i).channel,n)))
              
              k = k + 1;

              TD(k) = log.event(i).measurement.value.pairwise_delta_t(j);
              all_time(k) = log.event(i).measurement.value.all_time(m) + offset;
              ExpireTime(k) = all_time(k) + latency + offset;
              id(k) = log.event(i).id;
              t(k) = log.event(i).time(1) + offset;
            
            end
          end
          
        catch
          fprintf(2,'\nFatal error reading measurements for %s\n', fn);
          fprintf(2,'Does event %.0f have measurements?\n\n', log.event(i).id);
          return;
        end
        
      end
      
      %trim unused members of each aray
      NumEvent = k;
      TD = TD(1:NumEvent);
      all_time = all_time(1:NumEvent);
      ExpireTime =  ExpireTime(1:NumEvent);
      id =  id(1:NumEvent);
      t =  t(1:NumEvent);
      
      %sort all arrivals by all_time
      [all_time idx] = sort(all_time);
      TD = TD(idx(:));
      ExpireTime = ExpireTime(idx(:));
      id = id(idx(:));
      t = t(idx(:));      

%% COUNT WHALES

      %only count whales if more than one event with channel pair
      if NumEvent > 1
        
        %initialize structure array track, which has last data for each
        %known whale
        
        NumTracks = 1;
        
        track = struct('length',1, ...
                        'event',struct( 'TD',TD(1), ...
                                        'all_time',all_time(1), ...
                                        'ExpireTime',ExpireTime(1), ...
                                        'id',id(1), ...
                                        't',t(1)));
        
        NumExpired = 0;
        expired = [];

        %loop through remaining events
        for i = 2:NumEvent

          %if user requested, eliminate any tracks from previous hours
          if HourChop
            k = 1;  
            while k <= NumTracks
              if floor(track(k).event(track(k).length).t/3600) < floor(t(i)/3600) %last track event in past hour
                
                %move all tracks to expired
                NumExpired = NumExpired + 1;
%                 NumExpired = NumExpired + NumTracks;
                expired = [expired track(k)];              
                
                %empty track
                track(k) = [];
                NumTracks = NumTracks - 1;
%                 NumTracks = 0;  %human counters start count over each hour
%                 track = [];
%                 k = 1;
                
              else
                k = k + 1;
              end
            end
          end
          
          %eliminate any whales whose time is past expiration
          k = 1;  
          while k <= NumTracks
            if track(k).event(track(k).length).ExpireTime < t(i)    %NOT A GREAT COMPARISON DUE TO MISALIGNMENT
              
              NumExpired = NumExpired + 1;
              expired = [expired track(k)];
              
              track(k) = [];
              NumTracks = NumTracks - 1;  
              
            else
              k = k + 1;
            end
          end

          %if there are not any known whales over Expiration Lag
          if ~NumTracks
            
            %current event starts track
            NumTracks = 1;
            NewTrack = -1;
            
            track = struct('length',1, ...
                           'event',struct('TD',TD(i), ...
                                          'all_time',all_time(i), ...
                                          'ExpireTime',ExpireTime(i), ...
                                          'id',id(i), ...
                                          't',t(i)));
            
          %if there are any known whales over Expiration Lag, check to see if
          %current whale is new whale
          else

            vMax = zeros(NumTracks,1);

            for k = 1:NumTracks

              dTD = TD(i) - track(k).event(track(k).length).TD;
              d_all_time = all_time(i) - track(k).event(track(k).length).all_time;

              %reduce dTD by tolerance
              if TDtolerance < abs(dTD)
                if dTD > 0
                  dTD = dTD-TDtolerance;

                else
                  dTD = dTD+TDtolerance;

                end

              else
                dTD = 0;

              end

              %velocity = change in distance / change in time
              % = change in TD * speed sound / (2 * change in time)
              %(3600 is seconds in hour, cuz speed sound is km/hr and TD is sec)
              %(2 cuz moving closer to one sensor results in moving farther from the other sensor, when whale in between)
              v = 1800 * SoundSpeed * dTD / d_all_time;

              %update maximum velocity for current track
              if abs(vMax(k)) < abs(v)
                vMax(k) = v;
              end
            end

            %if maximum velocity compared to all other tracks is over speed limit
            [minV minIDX] = min(abs(vMax));

            if minV > SpeedLimit
              
              NewTrack = 1;

              %create new track
              NumTracks = NumTracks + 1;
              track = [track ...
                       struct('length',1, ...
                              'event',struct( 'TD',TD(i), ...
                                              'all_time',all_time(i), ...
                                              'ExpireTime',ExpireTime(i), ...
                                              'id',id(i), ...
                                              't',t(i)))];
              
%               WhaleCount = max(WhaleCount, NumTracks);
              WhaleCount = max(WhaleCount, sum([track(:).length] >= MinTrackLen));
              
            %not over speed limit
            else
              
              NewTrack = 0;

              %update track with minimum abs velocity calculated with
              %current event
              track(minIDX) = struct( 'length',track(minIDX).length + 1, ...
                                      'event',[track(minIDX).event ...
                                               struct('TD',TD(i), ...
                                                      'all_time',all_time(i), ...
                                                      'ExpireTime',ExpireTime(i), ...
                                                      'id',id(i), ...
                                                      't',t(i))]);

            end
          end
          
%% output time delay data, if user selected and if any in channel pair
  
          if TrackDetails
            
            %if time delay is first in new track, other tracks don't exist
            if isequal(NewTrack,-1)
              
              TDtime = track(NumTracks).event(1).all_time;
              TDtimeStr = datestr(datenum(TDtime/86400),'HH:MM:SS');
              
              ExpireTimeCurrent = track(NumTracks).event(1).ExpireTime;
              ExpireTimeStr = datestr(datenum(ExpireTimeCurrent/86400),'HH:MM:SS');
                          
              fprintf(2,f2, ...
                m, ...
                n, ...
                NumTracks, ...
                1, ...
                '00:00:00', ...
                NaN, ...
                track(NumTracks).event(1).TD, ...
                NaN, ...
                track(NumTracks).event(1).id, ...
                TDtimeStr, ...
                ExpireTimeStr ...
              );
            
            %if time delay is first in new track, other tracks exist
            elseif isequal(NewTrack,1)
              
              TDtime = track(NumTracks).event(1).all_time;
              TDtimeStr = datestr(datenum(TDtime/86400),'HH:MM:SS');
              
              ExpireTimeCurrent = track(NumTracks).event(1).ExpireTime;
              ExpireTimeStr = datestr(datenum(ExpireTimeCurrent/86400),'HH:MM:SS');
              
              fprintf(2,f2, ...
                m, ...
                n, ...
                NumTracks, ...
                1, ...
                '00:00:00', ...
                vMax(minIDX), ...
                track(NumTracks).event(1).TD, ...
                vMax(minIDX) / (1800 * SoundSpeed), ...
                track(NumTracks).event(1).id, ...
                TDtimeStr, ...
                ExpireTimeStr ...
              );
            
            %if time delay was added to existing track
            else
              
              TrackTime = track(minIDX).event(end).all_time - track(minIDX).event(1).all_time;
              TrackTimeStr=datestr(datenum(TrackTime/86400),'HH:MM:SS');
              
              TDtime = track(minIDX).event(track(minIDX).length).all_time;
              TDtimeStr = datestr(datenum(TDtime/86400),'HH:MM:SS');
              
              ExpireTimeCurrent = track(minIDX).event(track(minIDX).length).ExpireTime;
              ExpireTimeStr = datestr(datenum(ExpireTimeCurrent/86400),'HH:MM:SS');
                   
              fprintf(2,f2, ...
                m, ...
                n, ...
                minIDX, ...
                track(minIDX).length, ...
                TrackTimeStr, ...
                vMax(minIDX), ...
                track(minIDX).event(track(minIDX).length).TD, ...
                vMax(minIDX) / (1800 * SoundSpeed), ...
                track(minIDX).event(track(minIDX).length).id, ...
                TDtimeStr, ...
                ExpireTimeStr ...
              );
              
            end
          end
          
          if csvEvent
            fprintf(2,'\nEvent output to CSV under construction\n')
          end
          
        end
      end
      
%% OUTPUT TRACK DATA, IF REQUESTED

      %output track summary data, if user selected
      if TrackSummary && (NumExpired || NumTracks)
        
        %output expired tracks, if any exist
        if NumExpired
          for i = 1:NumExpired

            %run regression only if multiple values of x (no dups!)
            if ~all([expired(i).event(:).all_time] == expired(i).event(1).all_time)
              
              TrackTime = expired(i).event(end).all_time - expired(i).event(1).all_time;
              TrackTimeStr=datestr(datenum(TrackTime/86400),'HH:MM:SS');
              
              %simple linear regression of TD on t
              [B Bint resid Rint stats] ...
                = regress([expired(i).event(:).TD]',...
                    [ones(1,expired(i).length); expired(i).event(:).all_time]');

            else
              TrackTimeStr = 'NaN';
              B(1:2) = NaN;
              stats(1:4) = NaN;

            end

            fprintf(1,f, ...
              m, ...
              n, ...
              i, ...
              expired(i).length, ...
              TrackTimeStr, ...
              1800 * SoundSpeed * B(2), ...
              mean([expired(i).event(:).TD]), ...
              B(2), ...
              stats(4), ...
              stats(3), ...
              stats(1), ...
              stats(2) ...
            );

          end
        end

        %output open tracks, if any exist
        if NumTracks
          for i = 1:NumTracks

            %run regression only if multiple values of x (no dups!)
            if ~all([track(i).event(:).all_time] == track(i).event(1).all_time)
              
              TrackTime = track(i).event(end).all_time - track(i).event(1).all_time;
              TrackTimeStr=datestr(datenum(TrackTime/86400),'HH:MM:SS');

              %simple linear regression of TD on t
              [B Bint resid Rint stats] ...
                = regress([track(i).event(:).TD]',...
                    [ones(1,track(i).length); track(i).event(:).all_time]');

            else
              TrackTimeStr = 'NaN';
              B(1:2) = NaN;
              stats(1:4) = NaN;

            end

            fprintf(1,f, ...
              m, ...
              n, ...
              i + NumExpired, ...
              track(i).length, ...
              TrackTimeStr, ...
              1800 * SoundSpeed * B(2), ...
              mean([track(i).event(:).TD]), ...
              B(2), ...
              stats(4), ...
              stats(3), ...
              stats(1), ...
              stats(2) ...
            );
          end
        end
        
        fprintf(1,'\n');  %blank line at end of output for channel pair
        
      end
      
%% output CSV, if user selected
      if csvTrack &&  (NumExpired || NumTracks)
      
        %output expired tracks, if any exist
        if NumExpired
          for i = 1:NumExpired

            %run regression only if multiple values of x (no dups!)
            if ~all([expired(i).event(:).all_time] == expired(i).event(1).all_time)
              
              TrackTime = expired(i).event(end).all_time - expired(i).event(1).all_time;
              TrackTimeStr=datestr(datenum(TrackTime/86400),'HH:MM:SS');
              
              %simple linear regression of TD on t
              [B Bint resid Rint stats] ...
                = regress([expired(i).event(:).TD]',...
                    [ones(1,expired(i).length); expired(i).event(:).all_time]');

            else
              TrackTimeStr = 'NaN';
              B(1:2) = NaN;
              stats(1:4) = NaN;

            end


            fprintf(fid,fCSV, ...
              m, ...
              n, ...
              i, ...
              expired(i).length, ...
              TrackTimeStr, ...
              1800 * SoundSpeed * B(2), ...
              mean([expired(i).event(:).TD]), ...
              B(2), ...
              stats(4), ...
              stats(3), ...
              stats(1), ...
              stats(2) ...
            );

          end
        end

        %output open tracks, if any exist
        if NumTracks
          for i = 1:NumTracks

            %run regression only if multiple values of x (no dups!)
            if ~all([track(i).event(:).all_time] == track(i).event(1).all_time)
              
              TrackTime = track(i).event(end).all_time - track(i).event(1).all_time;
              TrackTimeStr=datestr(datenum(TrackTime/86400),'HH:MM:SS');

              %simple linear regression of TD on t
              [B Bint resid Rint stats] ...
                = regress([track(i).event(:).TD]',...
                    [ones(1,track(i).length); track(i).event(:).all_time]');

            else
              TrackTimeStr = 'NaN';
              B(1:2) = NaN;
              stats(1:4) = NaN;

            end

            fprintf(fid,fCSV, ...
              m, ...
              n, ...
              i + NumExpired, ...
              track(i).length, ...
              TrackTimeStr, ...
              1800 * SoundSpeed * B(2), ...
              mean([track(i).event(:).TD]), ...
              B(2), ...
              stats(4), ...
              stats(3), ...
              stats(1), ...
              stats(2) ...
            );
          end
        end
        
        fprintf(fid,'\n');  %blank line at end of output for channel pair
        
      end
    
%% if user chose to plot tracks for each channel-pair AND if data to plot
      if plots && (NumTracks || NumExpired)
        
        NumPlots = NumPlots + 1;
        
        %set up figure and subplot axis
        cidx = 0;
        SubPlotNum = mod(NumPlots-1,5)+1;
        if SubPlotNum == 1
          figure('Position',[1 1 1200 1800],'menubar','figure')
        end
        
        LeftSecond = round(LeftHour*3600);
        RightSecond = round(RightHour*3600);
        
        subplot(5,1,SubPlotNum,   ...
          'color',[0,0,0], ...
          'xtick',LeftSecond:3600:RightSecond, ...
          'xticklabel',LeftHour:1:RightHour);
        
%         subplot(5,1,SubPlotNum,   ...
%           'color',[0,0,0], ...
%           'xtick',0:3600:86400, ...
%           'xticklabel',0:1:24);
        
        xlim([LeftSecond RightSecond])
        ylim([-TDlimit TDlimit])
        title(sprintf('Channel Pair: %.0f-%.0f for %s', m, n, fn))
        ylabel('TD(s)')
        xlabel('Time (hours)')
        hold on
        
        % draw guidelines for maximum velocity
        plot(x,y,'.','MarkerSize',0.1,'Color',[.25 .25 .25])
        plot(x,y2,'.','MarkerSize',0.1,'Color',[.25 .25 .25])
        plot(x,-y,'.','MarkerSize',0.1,'Color',[.25 .25 .25])
        plot(x,-y2,'.','MarkerSize',0.1,'Color',[.25 .25 .25])
        
        % plot expired tracks
        if NumExpired
          for i = 1:NumExpired
              
            cidx = cidx + 1;
            
            %chose plotting by line or 'x' depending on specified minimum path length
            if expired(i).length >= MinTrackLen
              LineSymbol = '-';
              MarkerSymbol = '.';
              MrkrSz = 5;
            else
              LineSymbol = ':';
              MarkerSymbol = 'x';
              MrkrSz = 5;
            end

            plot([expired(i).event(:).all_time], [expired(i).event(:).TD],...
              'LineStyle',LineSymbol,...
              'color',lcolor(mod(cidx-1,ncolor)+1,:),...
              'Marker',MarkerSymbol,...
              'MarkerSize',MrkrSz)
            
          end
        end
        
        %plot open tracks
        if NumTracks          
          for i = 1:NumTracks
              
            cidx = cidx + 1;
            
            %chose plotting by line or 'x' depending on specified minimum path length
            if track(i).length >= MinTrackLen
              LineSymbol = '-';
              MarkerSymbol = '.';
              MrkrSz = 5;
            else
              LineSymbol = ':';
              MarkerSymbol = 'x';
              MrkrSz = 5;
            end
            
            %plot events from current track with lines
            plot([track(i).event(:).all_time], [track(i).event(:).TD],...
              'LineStyle',LineSymbol,...
              'color',lcolor(mod(cidx-1,ncolor)+1,:),...
              'Marker',MarkerSymbol,...
              'MarkerSize',MrkrSz)
              
          end
        end
      end
      
    end
  end
end


fprintf(1,'\nWHALE COUNT: %.0f\n\n', WhaleCount);
fprintf(1,'****************************************\n\n');

if csvTrack
  fprintf(fid,'WHALE COUNT:,, %.0f\n', WhaleCount);
  fclose(fid);
end

fprintf(context.state.fid0, ...
  '%.0f,%s,%s\n', ...
  WhaleCount, ...
  datestr(floor(realtime)), ...
  fn);



