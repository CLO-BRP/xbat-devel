% 
% 
% Documentation for "Filter Events by Time v1-3"
% (28 Oct 2009)
% 
% 1. If there are no events in the specified time range, a warning is 
%    written to the MATLAB Command Window and no log is created.
% 
% 2. There is now the option to select between 2 log suffixes for the 
%    new log, "_Keep_<h>h-<h>h" and "_to_be_loc_<h>-<h>".
% 
% 3. If "Filter Events by Time v1-3" log action finds a log without the 
%    Date-time attribute embedded, it will display a warning in the MATLAB 
%    Command Window "WARNING: Date-time attribute is not set for <log name>", 
%    where <log name> is the name of the log in question.
% 
% (Note that any log that was last open in a sound for which the 
%   Date-time attribute was not set will not have the Date-time attribute 
%   embedded in it. In general, log actions that are designed to work on 
%   logs in their home "Logs" folder can get sound attributes from the 
%   sound itself rather than the selected log. However, log actions like 
%   this one which are designed to work on logs whose dates are mixed 
%   together in a single folder have to get the sound attributes from the 
%   logs they are working on.>
%   