function status = close_browsers
%offer to close XBAT Sound browsers, if any are open

status = 1;

fh = get_active_browser;
if ~isempty( fh )
  d1 = 'WARNING';
  d2 = sprintf( [ 'Cannot proceed.\n' ...
                  'Close all XBAT Sound windows?' ] );
  button = questdlg( d2, d1 );
  if strcmp( button, 'Yes' )
    while ~isempty( fh )
      browser_file_menu( fh, 'Close' );
      fh = get_active_browser;
    end
  else
    fprintf( '\n\nExecution terminated\n' );
    return;
  end
end

status = 0;