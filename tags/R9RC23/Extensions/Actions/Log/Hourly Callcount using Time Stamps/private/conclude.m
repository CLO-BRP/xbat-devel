function [result, context] = conclude(parameter, context)
result = struct;

% HOURLY PRESENCE - conclude
if context.state.kill
  return;
end

realtime = context.state.realtime;

first_day = floor(min(realtime));

last_day = floor(max(realtime));

num_days = last_day - first_day + 1;

count = zeros(num_days, 24);

fn = context.state.fn;

num_logs = size(fn, 1);

if isempty(realtime)
  fprintf(2,'No events to count.\n');
  return;
end

for i = 1:size(realtime, 1)
  
  day = floor(realtime(i) - first_day) + 1;
  
  hh = floor(24 * mod(realtime(i), 1)) + 1;

  count(day, hh) = count(day, hh) + 1;

end

line = cell(num_days + num_logs + 7, 27);

line(1,1) = {'Hourly Callcount'};

line(2,1) = {' '};

line(3,:) = {'Date','0h-1h','1h-2h','2h-3h','3h-4h','4h-5h','5h-6h','6h-7h',...
             '7h-8h','8h-9h','9h-10h','10h-11h','11h-12h','12h-13h',...
             '13h-14h','14h-15h','15h-16h','16h-17h','17h-18h','18h-19h',...
             '19h-20h','20h-21h','21h-22h','22h-23h','23h-24h','SUM','MaxHr'};
           
for i = 1:num_days

  line(i+3,1)    = {datestr(i + first_day - 1, 'mmm.dd,yyyy')};
  line(i+3,2:25) = num2cell(count(i, :));
  
  sum_count = sum(count(i,:));
  line(i+3,26)   = num2cell(sum_count);
  
  if sum_count
  
    MaxHr = max(count(i, :));
    MaxHrIDX = find(count(i, :) == MaxHr);
    lenMax = length(MaxHrIDX);

    line(i+3,27:26+lenMax) = line(3, MaxHrIDX  + 1);  
    
  end
end

line(i+4,1) = {' '};

ii = i + 5;

line(ii,1) = {'SUM'};

sum_count = zeros(1,24);

for j = 1:24
  sum_count(j) = sum(count(:,j));
%   line(ii,j+1) = {sum(count(:,j))};
end

for i = 1:24
  line{ii,i+1} = sum_count(i);
end

grand_sum_count = sum(sum_count);
% sum_count = sum(sum(count));

line(ii,26) = {grand_sum_count};
  
  if grand_sum_count
  
    MaxHr = max(sum_count);
    MaxHrIDX = find(sum_count == MaxHr);
    lenMax = length(MaxHrIDX);

    line(ii,27:26+lenMax) = line(3, MaxHrIDX  + 1);  
    
  end

line(ii+1,1) = {' '};

iii = ii + 2;

line(iii,1) = {'Logs counted:'};

for k = 1:num_logs
  line(iii + k,1) = fn(k);
end

xls_name = sprintf('callcount_%s', datestr(now,30));

[xls_name, xls_path] = uiputfile( ...
  '*.xls', ...
  'Hourly Callcount using Time Stamps: output', ...
  xls_name);

if isequal(xls_name, 0)
  return;
end

xlswrite([xls_path '\' xls_name], line);
% xlswrite([xls_path xls_name], line);


