function [result, context] = conclude(~, context)
result = struct;

return;

% % HISTOGRAM SCORES - conclude
% 
% %initializations
% ScoreVector = context.state.ScoreVector;
% MaxScoreVector = context.state.MaxScoreVector;
% len = length(ScoreVector);
% 
% inc = parameter.inc;
% 
% %% Report Summary Stats for Score
% fprintf(1,'SUMMARY STATS FOR SCORE\n');
% fprintf(1,'Mean Score: %f\n', mean(ScoreVector));
% fprintf(1,'Median Score: %f\n', median(ScoreVector));
% fprintf(1,'Score Variance: %f\n\n', var(ScoreVector));
% fprintf(1,'Min Score: %f\n\n', min(ScoreVector));
% fprintf(1,'Max Score: %f\n\n', max(ScoreVector));
% 
% %% HISTOGRAM OF Score Frequency
% 
% 
% %define centers of bars on histogram
% MinScore = floor(min(ScoreVector));
% MaxScore = ceil(max(ScoreVector));
% 
% x = MinScore+inc/2:inc:MaxScore-inc/2;
% 
% % save('test')
% 
% %plot histogram
% h1 = figure;
% set(h1,'Name','Score Frequency','MenuBar','figure')
% [n, xout] = hist(ScoreVector,x);
% bar(xout,n)
% cum_n = cumsum(n(end:-1:1));
% cum_n = cum_n(end:-1:1);
% 
% fprintf('\nScore, Freq, Cum Freq\n');
% 
% for i = length(n):-1:1
% % for i = 1:length(n)
%   
%   fprintf('%.3f,%10.0f,%12.0f\n',xout(i)-inc/2,n(i),cum_n(i));
% 
% end
% 
% 
% %% HISTOGRAM OF Square Root Score Frequency
% 
% if parameter.SqrtScore
% 
%   SqrtScoreVector = ...
%     (ScoreVector>=0).*sqrt(ScoreVector)-(ScoreVector<0).*sqrt(-ScoreVector);
% 
%   SqrtMinScore = min(SqrtScoreVector);
%   SqrtMaxScore = max(SqrtScoreVector);
% 
%   x2 = SqrtMinScore+0.005:0.01:SqrtMaxScore-0.0005;
% 
%   %plot histogram of square root of cross correlation
%   h2 = figure;
%   hist(SqrtScoreVector, x2)
%   set(h2, 'Name','Square Root Score Frequency','MenuBar','figure')
% end
% 
% %% HISTOGRAM OF MAXIMUM SCORE FOR EACH EVENT TAG (tags must be consecutive in log!
% 
% if parameter.MaxScore
%   h3 = figure;
%   hist(MaxScoreVector, x);
%   set(h3,'Name','Frequency for Maximum Score for Each Tag','MenuBar','figure');
% end
% 
% 
% %% SINGLE-CHANNEL HISTOGRAMS
% 
% if parameter.SeparateChannels
%   channel = context.state.channel;
%   NumChans = max(channel);
%   
%   ScoreVectorChannel = zeros(NumChans, len);
%   SqrtScoreVectorChannel = ScoreVectorChannel;
%   ChannelLength = zeros(NumChans);
%   
%   for i=1:len
%     ChannelLength(channel(i)) = ChannelLength(channel(i)) + 1;
%     ScoreVectorChannel(channel(i),ChannelLength(channel(i))) = ScoreVector(i);
%     SqrtScoreVectorChannel(channel(i),ChannelLength(channel(i))) = SqrtScoreVector(i);
%   end
%   
%   for i = 1:NumChans
%     h4 = figure;
%     set(h4,'Name',sprintf('Score Frequency for Channel %.0f', i),'MenuBar','figure');
%     hist(ScoreVectorChannel(i,1:ChannelLength(i)), x)
%   end
% 
%   if parameter.SqrtScore
%     for i = 1:NumChans
%       h5 = figure;
%       set(h5,'Name',sprintf('Square Root Score Frequency for Channel %.0f', i),'MenuBar','figure');
%       hist(SqrtScoreVectorChannel(i,1:ChannelLength(i)), x2)
%     end
%   end    
% end
% 
% %% output frequencies and bin centers, if requested
% if parameter.OutputFrequencies
%   [ScoreFreq ScoreBin] = hist(ScoreVector,x);
%   [MaxScoreFreq MaxScoreBin] = hist(MaxScoreVector,x);
%   save('HistogramScores', ...
%        'ScoreVector', ...
%        'ScoreFreq', ...
%        'ScoreBin', ...
%        'MaxScoreVector', ...
%        'MaxScoreFreq', ...
%        'MaxScoreBin');
% end