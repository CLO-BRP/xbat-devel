function [result, context] = compute(log, parameter, context)
result = struct;

% eXport CSV - compute

% Log action exports selected XBAT log into single CSV file, which is 
% created in the XBAT library for the currently-selected sound.

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. Make action independent of currently-selected sound

% Change Log
%
% 1. 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

%%
% if context.state.kill
%   return;
% end

%% initializations
fn = log_name(log);
line = cell(log.length,1);

% find date-time
realtime = log.sound.realtime;

if isempty(realtime)
  
  file = log.sound.file;
  if iscell(file)
    file = file{1};
  end
  
  realtime = file_datenum(file);
  
  if ~isempty(realtime)
    fprintf( ...
      2, ...
      '%s: Date-time attribute missing.  Date-time set to %s.\n', ...
      fn, ...
      datestr(realtime,0) ...
    );
  
  else
    
    fprintf( ...
      2, ...
      '%s: Date-time attribute missing and unavailable from time stamps. Log skipped.\n', ...
      fn ...
    );
  
  return;
    
  end
end
  
%find date in log name
idx = findstr(fn,'_');      % find all hyphens in log name
idx = [idx length(fn)+1];   % add fake delimiter at end so will check last string

if length(idx) >= 2         % if there are more than 2 hyphens
  fnDate = fn(idx(1)+1:idx(2)-1);  %attempt to extract datestr from fn

  if ~isnan(str2double(fnDate))  %if datestr is a number
    logdate = datenum(fnDate, 'yyyymmdd');  %convert datestr to datenum

  elseif length(idx)>=3          %if datestr is not a number, but there is more log name to check

    i = 2;
    while i < length(idx) && isnan(str2double(fnDate))

      fnDate = fn(idx(i)+1:idx(i+1)-1); %attempt to extract another datestr from fn
      if ~isnan(str2double(fnDate))  %if datestr is a number
        logdate = datenum(fnDate, 'yyyymmdd'); %convert datestr to datenum
      end

      i = i + 1;

    end
  end

  %check date-time attribute against date in log name
  if ~isequal(logdate, floor(realtime))
    
    fprintf( ...
      2, ...
      '%s: Date in log name is not in date-time attribute or time stamp.  Log skipped.\n', ...
      fn ...
    );
  
    return;
    
  end

else

  fprintf(2,'%s: Cannot get date from log name.  Log skipped.\n', fn);
  return;    

end


%% make output lines in cell array

for i = 1:log.length
  
  %prep score for output
  score = log.event(i).score;
  if isempty(score)
    score = 'NaN';
  else
    score = num2str(score);
  end
  
  %prep tags for output
  tags = log.event(i).tags;
  if isempty(tags)
    tags = {' '};
  elseif ~iscell(tags)
    tags = {tags};
  end
  tags = sprintf('%s|', tags{:});
  tags = tags(1:end-1);
  
  %prep tags for output
  notes = log.event(i).notes;
  if isempty(notes)
    notes = {' '};
  elseif ~iscell(notes)
    notes = {notes};
  end
  notes = sprintf('%s  ', notes{:});
  notes = notes(1:end-2);
  
  line{i} = sprintf( ...
    '%s, %.0f, %.0f, %s, %s, %14.4f, %14.4f, %14.4f, %5.4f, %5.4f, %5.4f, %s, %s, %s, %s', ...
    fn, ...
    log.event(i).id, ...
    log.event(i).channel, ...
    datestr(realtime + log.event(i).time(1)/86400, 0), ...
    datestr(realtime + log.event(i).time(2)/86400, 0), ...
    log.event(i).time(1), ...
    log.event(i).time(2), ...
    log.event(i).duration, ...
    log.event(i).freq(1), ...
    log.event(i).freq(2), ...
    log.event(i).bandwidth, ...
    score, ...
    num2str(log.event(i).rating), ...
    tags, ...
    notes ...
  );
    
end
    
context.state.line = [context.state.line; line];
    
