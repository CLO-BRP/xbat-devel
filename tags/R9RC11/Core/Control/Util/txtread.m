function txt_cell = txtread(URI, f, delim)
    %txtread - reads text file and creates mxn cell array of strings, 
    % here m is the number of records and n is the number of 
    % character-delimited fields
    %   
    %  input 
    %
    %    URI    - path of text file
    %
    %    f      - read format for textscan
    %
    %    delim  - delimiter(s) for field separation
    %
    %  output
    %
    %    txt_cell = mxn cell array of strings, where 
    %                 m is the number of records
    %                 n is the number of character-delimited fields

    % read text file
    fid = fopen(URI, 'r');
    txt_cell_in = textscan(fid, f, 'Delimiter', delim,'ReturnOnError',0);
    fclose(fid);

    % convert text file into cell array
    len = length(txt_cell_in);
    len2 = length(txt_cell_in{1,1});
    txt_cell = cell(len2,len);
    for i = 1:len
      txt_cell(1:len2,i) = txt_cell_in{1,i};
    end