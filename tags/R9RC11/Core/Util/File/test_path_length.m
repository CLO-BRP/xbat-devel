function flag = test_path_length( log )

% Tests if full path length, directory path length, or backup path length 
% for XBAT logs are too long.
%
% http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx

% History
%   msp2 - 22 Mar 2014
%       Create
%   msp2 - 25 Mar 2014
%       Allow multiple log input to accomodate multiselect in "Open" dialog
%       in Log palette.

flag = 0;

% call each log recursively
len = length( log );
if len > 1
    for i = 1 : length( log )
        flag = test_path_length( log( i ) );
        if flag
            return;
        end
    end
    flag = 1;
    return;
end

full_path = fullfile( log.path, log.file );

if length( full_path ) > 255
    txt = sprintf( 'Total path length for log > 255 characters:\n\n   %s', full_path );
    fail( txt, 'ERROR' )
    return;
end

if length( log.path) > 248
    txt = sprintf( 'Directory path length > 248 characters:\n\n   %s', log.path);
    fail( txt, 'ERROR' )
    return;
end

flag = 1;
    
% assert( length( full_path ) <= 255, '\n%s\nTotal path length for log > 255 characters:\n\n   %s\n\n%s\n\n\n', ast, full_path, ast )
% assert( length( log.path) <= 248, '\n%s\nDirectory path length > 248 characters:\n\n   %s\n\n%s\n\n\n', ast, fpath, ast )

backup_path = fullfile( full_path, '__BACKUP', [ log.file, ' 20140101T000000' ] );
if length( backup_path ) > 255
    txt = sprintf( 'Log written, but backup path may be too long to allow deletion in XBAT:\n   %s', log_name( log ) );
    fail( txt, 'WARNING' )
end