function duration = get_default_page_duration(sound)

% NOTE: perhaps consider some machine parameters and page mode

% Copyright (C) 2002-2012 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
%
% History
%   msp2  28 Oct 2013
%       Change default sound duration to 60 seconds for all sound sample
%       rates
%   msp - 9 Mar 2014
%     Restore wjh73 REV 3980, which changed hkf's 2^18 to 2^22, which should
%	  reduce the rate of duplicate detection 16x.  Default page duration of 
%     60 seconds was moved to browser_create.
%   msp2 - 22 mar 2014
%       Adjust page duration by dividing by number of channels.  This
%       effects page size for detectors, not the default browser duration.

%--
% estimate default page duration
%--
duration = (2^22) / get_sound_rate(sound) / sound.channels;

%--
% ensure integer duration or full sound duration
%--
duration = round(duration);

if duration < 1
	duration = 1;
end

if duration > sound.duration
	duration = sound.duration;
end
