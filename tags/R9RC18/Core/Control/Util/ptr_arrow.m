function cdata = ptr_arrow

cdata = [ 
       1   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   2   2   2   1 NaN NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   2   2   2   2   1 NaN NaN NaN NaN NaN NaN
       1   2   2   2   2   2   1   1   1   1   1 NaN NaN NaN NaN NaN
       1   2   2   1   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN NaN
       1   2   1 NaN   1   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN
       1   1 NaN NaN   1   2   2   1 NaN NaN NaN NaN NaN NaN NaN NaN
       1 NaN NaN NaN NaN   1   2   2   1 NaN NaN NaN NaN NaN NaN NaN
     NaN NaN NaN NaN NaN   1   2   2   1 NaN NaN NaN NaN NaN NaN NaN
     NaN NaN NaN NaN NaN NaN   1   1   1 NaN NaN NaN NaN NaN NaN NaN
];
