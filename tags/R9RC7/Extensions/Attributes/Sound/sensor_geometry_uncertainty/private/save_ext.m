function save_ext(attribute, store, context)

% SENSOR_GEOMETRY - save

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2  14 May 2014
%       Change name of function to avoid name conflict with MATLAB built-in function.

  %--
  % check for toolbox
  %--

  lines{1} = 'x-, x+, y-, y+, z-, z+';

  lines{2} = 'm';


  %--
  % write geometry uncertainty to lines
  %--
  
  uncertainty = attribute.userdata;
% %   ok = get_control( callback.pal.handle,'OK' );
% %   cb = get_callback( ok.handles.obj );
% %   id = cb.Callback{ 2 };
% %   uncertainty = get_env( id );
%   uncertainty = context.attribute.uncertainty;
  
  for ch = 1:size(uncertainty, 1)

    lines{ ch+2 } = sprintf( '%s, %s, %s, %s, %s, %s', ...
                             num2str( uncertainty( ch, 1, 1 ) ), ...
                             num2str( uncertainty( ch, 1, 2 ) ), ...
                             num2str( uncertainty( ch, 2, 1 ) ), ...
                             num2str( uncertainty( ch, 2, 2 ) ), ...
                             num2str( uncertainty( ch, 3, 1 ) ), ...
                             num2str( uncertainty( ch, 3, 2 ) )  ...
                           );

  end

  file_writelines(store, lines);

end

