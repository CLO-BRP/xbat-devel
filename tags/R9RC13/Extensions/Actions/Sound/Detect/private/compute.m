function [result, context] = compute(sound, parameter, context)

% DETECT - compute

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
%
% Change Log
%
% msp2 - 14 Nov 2012
%  1. Maximum output log size
%     -Add feature that allows user to specify maximum output log size in
%     GUI.
%     -The output will be divided among multiple logs.
%     -Changes were required in an XBAT core function, detector_scan.
% msp2 - 27 Mar 2013
%  1. Allow log to be output to a path specified in parameter.out_path.
%     -Calling function must insure that paths in "sound" are current.
%      Otherwise a WARNING will be output to MATLAB Desktop and all the
%      sound samples input into detector scan will be set to 0.
% msp2 - 23 September 2013
%  1. Allow extension to be called from XBAT or from DM.
%  2. Add dropdown list with log name tokens.
% msp2 - 2 Nov 2013
%	Add support for Raven selection table output.
% msp2 - 10 Mar 2013
%	Add support for whiten filter.
%	Fix bug that prevented maximum log size from being disabled.
% msp2 - 12 Mar 2013
%	Add controls for spectrogram parameters.
%   Make compatible with call from DM.
% msp2 - 13 Mar 2013
%   Simplify XBAT vs DM switching.
%   If no preset selected, disable OK button.
% msp2 - 13 May 2014
%   Change variable name "log_name" to "fn" to avoid conflict with XBAT
%   function "log_name".
%   msp2 - 14 May 2014
%       Use log sequence indexes in selection table names if maximum log size
%         invoked.
%       Add milliseconds to time stamp in output name to avoid name conflicts.
%       Add support for a unique identifier in log names.
%   msp2 - 15 May 2014
%       Enable multithreading by skipping calls to figures.
%   msp2 - 1 June 2014
%       Re-work parsing parameters to make compatible with changes to Data
%         Template Detector.
%   msp2 - 6 June 2014
%       Stop execution if any selected sound is open in a browser.

%-------------------------------------------------------------------------
% initializations
%-------------------------------------------------------------------------
result = [];
if ~isfield( context, 'state' ) || ~isfield( context.state, 'kill' )
    context.state.kill = 0;
end
if context.state.kill
    return;
end

%-------------------------------------------------------------------------
% stop execution if browser open for current sound
%-------------------------------------------------------------------------
if sound_is_open( context.target, context.library )
    context.state.kill = 1;
    fail( 'Detection failed.  Close all sounds and try again.' )
    return;
end

%-------------------------------------------------------------------------
% load detector extension
%-------------------------------------------------------------------------
ext = get_extension( 'sound_detector', parameter.detector{ 1 } );

%-------------------------------------------------------------------------
% find out of call to this extensions is by XBAT or by DM
%-------------------------------------------------------------------------
stack = dbstack;
from_xbat = strcmp( stack( 2 ).file, 'perform_action.m' );

%-------------------------------------------------------------------------
% load preset
%-------------------------------------------------------------------------
if from_xbat %called by XBAT
	preset = preset_load( ext, parameter.preset{ 1 } );  
else %called by DM
	preset_path = fullfile( parameter.preset_path, parameter.preset{ 1 } );
	preset = load( preset_path );
	preset = preset.preset;
	preset.name = parameter.preset{1};
	preset.ext.control.explain = 0;
end

%-------------------------------------------------------------------------
% get correct parameter
%-------------------------------------------------------------------------
if from_xbat %called by XBAT
	% nothing to do - parameter is correct
else %called by DM
    parameter = context.ext.parameter;
end

%-------------------------------------------------------------------------
% construct ext.parameter for call to detector
%-------------------------------------------------------------------------
ext.parameter.templates       = preset.ext.parameter.templates;
ext.parameter.thresh          = context.ext.parameter.threshold;
ext.parameter.thresh_test     = 1;
ext.parameter.deviation       = preset.ext.parameter.deviation;
ext.parameter.deviation_test  = preset.ext.parameter.deviation_test;
ext.parameter.mask            = preset.ext.parameter.mask;
ext.parameter.mask_percentile = preset.ext.parameter.mask_percentile;
ext.parameter.specgram.fft         = parameter.fft;
ext.parameter.specgram.hop         = parameter.hop;
ext.parameter.specgram.hop_auto    = 0;
ext.parameter.specgram.win_type    = parameter.win_type{ 1 };
ext.parameter.specgram.win_param   = parameter.win_param;
ext.parameter.specgram.win_length  = parameter.win_length;
ext.parameter.specgram.sum_type    = 'mean';
ext.parameter.specgram.sum_quality = 'low';
ext.parameter.specgram.sum_length  = 1;
ext.parameter.specgram.sum_auto    = 1;
ext.parameter.specgram.on          = 1;
ext.parameter.specgram.diff        = 0;
ext.parameter.specgram.filter      = 'None';
ext.parameter.specgram.smoothing   = 0;

%-------------------------------------------------------------------------
% update sound.specgram
%-------------------------------------------------------------------------
sound.specgram = ext.parameter.specgram;

%-------------------------------------------------------------------------
%set maximum number of events per log (empty means no limit)
%-------------------------------------------------------------------------
if ~isfield( parameter, 'max_num_events' ) || ( isfield( parameter, 'chop_flag' ) && isequal( parameter.chop_flag, 0 ) )
    max_num_events = [];
else
    max_num_events = parameter.max_num_events;
end

%-------------------------------------------------------------------------
% create new log
%-------------------------------------------------------------------------
fn = parameter.output;

fn = token_replace( fn, '%', ...
	'SOUND_NAME', sound_name( sound ), ...
	'PRESET_NAME', parameter.preset{ 1 }, ...
	'DETECTOR_NAME', ext.name, ...
	'TIME', datestr( now, 'yyyymmdd_HHMMSSpFFF' ), ...
	'UID', dec2hex( floor( rand( 1 ) * 10^6 ) ) ...
);

if ~isempty( max_num_events );
  fn = [ fn, '_000001' ];
end
  
%output Raven selection table
if isfield( parameter, 'output_type' ) ...
&& ( strcmpi( parameter.output_type, 'selection_table' ) ...
  || strcmpi( parameter.output_type, 'selection table' ) )

    %create empty Raven selection table
    log = raven_save( parameter.out_path, fn, 'DM', sound );
  
%output XBAT log
else

    sound.userdata.multithread = parameter.multithread;
    if from_xbat

      %create empty log
      log = new_log( fn, context.user, context.library, sound );

    else

      %create output directory, if needed
      if ~exist( parameter.out_path, 'dir' )
        mkdir( parameter.out_path )
      end

      %create empty log
      fn_full = fullfile( parameter.out_path, fn );
      log = log_create( fn_full, 'sound', sound, 'author', 'DM' );

    end
end

%-------------------------------------------------------------------------
% embed user parameters in log.userdata
%-------------------------------------------------------------------------
log.userdata.Whiten.filter_flag = parameter.filter_flag;
log.userdata.Whiten.order = parameter.order;
log.userdata.detect.max_num_events = max_num_events;

%-------------------------------------------------------------------------
% scan into log - NOTE: detector_scan saves the log so we don't need to
%-------------------------------------------------------------------------
if from_xbat
    preset.ext.parameter.multithread = parameter.multithread;
    detector_scan( ext, sound, [], [], log );
else
    preset.ext.parameter.multithread = parameter.multithread;
	detector_scan( ext, sound, [], [], log );
end

%-------------------------------------------------------------------------
% delete detector waitbar
%-------------------------------------------------------------------------
if ~parameter.multithread
    name = [ preset.ext.name, ' - ', sound_name( sound ) ];
    pal = find_waitbar( name );
    delete( pal );
end

%-------------------------------------------------------------------------
% TOKEN_REPLACE
%-------------------------------------------------------------------------
function str = token_replace( str, sep, varargin )

    %Replace tokens in log name with appropriate value
    [ field, value ] = get_field_value( varargin );
    for k = 1:length( field )
        tok = [ sep, field{k}, sep ];
        str = strrep( str, tok, value{ k } );
    end
    
