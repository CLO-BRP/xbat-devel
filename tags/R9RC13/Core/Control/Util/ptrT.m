function cdata = ptrT
% set "T" as a custom pointer in browser

o=NaN; w=2; k=1;
cdata = [...
w w w w w w w w w w w w w w w w
w k k k k k k k k k k k k k k w
w k k k k k k k k k k k k k k w
w k k k k k k k k k k k k k k w
w k k k k k k k k k k k k k k w
w w w w w w k k k k w w w w w w
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w k k k k w o o o o o
o o o o o w w w w w w o o o o o
    ];
