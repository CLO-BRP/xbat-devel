function [result, context] = conclude(parameter, context)
result = struct;

% SUM EVENT DURATION - conclude

duration = context.state.duration;

fprintf(1,'Cumulative duration of all events: %.0f s\n\n', sum(duration));

fprintf(1,'Minimum duration of all events:    %.2f s\n\n', min(duration));

fprintf(1,'Maximum duration of all events:    %.2f s\n\n', max(duration));

fprintf(1,'Median duration of all events:     %.2f s\n\n', median(duration));

fprintf(1,'Mean duration of all events:       %.2f s\n\n', mean(duration));

fprintf(1,'Standard deviation of all events:  %.3f s\n\n', std(duration));
