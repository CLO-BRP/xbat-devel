function [result, context] = compute(~, ~, context)

% HISTOGRAM SCORES - compute

result = struct;

% %terminate script if sound not selected
% if ~isfield(context.sound, 'type')
%   fprintf(2,'\n\n%s\n\n', 'No sound selected. Select sound in XBAT palette and try again.');
%   return;
% end

% len = log.length;
% fn = log.file;
% fn = fn(1:end-4);
% ScoreVector = zeros(1,len);
% channel = zeros(1,len);
% MaxScoreVector = zeros(1,len);
% CurrentTag = log.event(1).tags;
% if ~iscell(CurrentTag); CurrentTag = {CurrentTag}; end;
% MaxScore = -100;
% k = 0;
% j = 0;
% 
% for i=1:len
%   
%   ID = log.event(i).id;
%   score = log.event(i).score;
%   
%   if ~isempty(score)
%     k = k + 1;
%     ScoreVector(k) = score;
%     channel(k) = log.event(i).channel;
%     
%     %if requested, compile vector of maximum score for each tag
%     if parameter.MaxScore
%       LastTag = CurrentTag;
%       CurrentTag = log.event(i).tags{1};
%       if ~iscell(CurrentTag); CurrentTag = {CurrentTag}; end;
%       if strcmp(CurrentTag, LastTag)
%         MaxScore = max(MaxScore, score);
%       else
%         j = j + 1;
%         MaxScoreVector(j) = MaxScore;
%         MaxScore = score;    
%       end
%     end
% 
%     %display events with score zero or less
%     if score <= 0
%       fprintf(2, 'Score = %.4f,  ID = %5.0f,  Log = %s\n', score, ID, fn);
%     end
%   
%   %display events with missing scores
%   else
%     fprintf(2, 'Score = %s,  ID = %5.0f,  Log = %s\n', '   n/a', ID, fn);
%   end
% end
% 
% 
% %max score for last tag into MaxScoreVector
% if parameter.MaxScore && strcmp(CurrentTag, LastTag)
%   j = j + 1;
%   MaxScoreVector(j) = MaxScore;
% end
%   
%   
% %append score vector for log to score vector for all selected logs
% context.state.ScoreVector = [context.state.ScoreVector ScoreVector(1:k)];
% context.state.channel = [context.state.channel channel(1:k)];
% context.state.MaxScoreVector = [context.state.MaxScoreVector MaxScoreVector(1:j)];
