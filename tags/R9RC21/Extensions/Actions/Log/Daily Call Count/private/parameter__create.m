function parameter = parameter__create(context)

%% DAILY  CALL COUNT - parameter__create
%
% Tabulates number of events with a user-definable tag for each channel of
% each selected sound.
%
% Useful for determining daily presence/absence of Fin Whales and other species.
%
%%
parameter = struct;

parameter.tag = '';

parameter.refresh = 0;

parameter.check = 0;
