function [result, context] = prepare(parameter, context)
result = struct;

% WHALE COUNT (RL-TD) V3 - prepare

%% Initiate Whale Count Output

%initializations
latency = parameter.latency*60; %if no track in this time, start looking for new whale
SpeedLimit = parameter.SpeedLimit;  %km/hr
TDtolerance = parameter.TDtolerance;         %tolerance for error in TD measurement
TDlimit = parameter.TDlimit;         %tolerance for error in TD measurement
HourChop = parameter.HourChop; %count whales only within outs
IndexChOnly = parameter.IndexChOnly; %use only ch-pairs which include index channel
MinTrackLen = parameter.MinTrackLen; %minimum TDs before track counts
check = parameter.check; %check if date-time attribute in log is correct
% plots = parameter.plots; %create time delay plot figures?
% LeftHour = parameter.LeftHour;           %plotting limits for x-axis (hours)
% RightHour = parameter.RightHour;        %plotting limits for x-axis (hours)
% csvTrack = parameter.csvTrack;     %create csv output for track summaries
% csvEvent = parameter.csvEvent;     %create csv output for event summaries
% TrackSummary = parameter.TrackSummary; %output summary data for each track
% TrackDetails = parameter.TrackDetails; %output data for each event

if HourChop
  HourChopStr='Y';
else
  HourChopStr='N';
end
  
if IndexChOnly
  IndexChOnlyStr='Y';
else
  IndexChOnlyStr='N';
end

if check
  checkStr='Y';
else
  checkStr='N';
end

%initialize number of channels
NumChan = context.sound.channels;
% if ~isscalar(NumChan)
%   fprintf(2,'Number of channels field missing in %s\n', fn);
%   return;
% end

%initialize SoundSpeed
SoundSpeed = context.sound.speed;
SoundSpeed = SoundSpeed / 1000;

% if isempty(SoundSpeed)
% 
%   %ask user for sound speed
%   SoundSpeedStr = inputdlg( ...
%     'Enter speed of sound (m/s)', ...
%     sprintf('Sound Speed attribute not set for %s',fn), ...
%     1, ...
%     {'1500'} ...
%   );
% 
%   if isempty(SoundSpeedStr)
%     return;
%   else
%     SoundSpeed = str2double(SoundSpeedStr{1});
%   end
% 
% end

%Find output file path (Logs folder of selected sound)
LibPath = context.library.path;
SoundName = sound_name(get_active_sound);
SoundPath = [LibPath SoundName '\Logs\'];

fn = ['WhaleCount(RL-TD)v3_' datestr(now,30) '.csv'];

%Open output file
fid0 = fopen([SoundPath fn], 'w');

%output header
fprintf(fid0,'RL-TD Whale Count\n');
fprintf(fid0,'%s\n\n',fn);

fprintf(fid0,'SETTINGS:\n');
fprintf(fid0,'%s,Check Date-time Attribute in Log\n\n',checkStr);
fprintf(fid0,'%.0f,Number of channels\n',NumChan);
fprintf(fid0,'%.0f,Speed of Sound (m/s)\n\n',SoundSpeed*1000);

fprintf(fid0,'%f,Maximum Speed of Target (km/hr)\n',SpeedLimit);
fprintf(fid0,'%f,Length of Sliding Window (min)\n',latency/60);
fprintf(fid0,'%f,Time Delay tolerance (s)\n',TDtolerance);
fprintf(fid0,'%f,Ignore Time Delays greater than � seconds\n',TDlimit);
fprintf(fid0,'%.0f,Minimum Time Delays in Track\n',MinTrackLen);
fprintf(fid0,'%s,Count within hours only\n\n',HourChopStr);
fprintf(fid0,'%s,Count only channel-pairs with index channel\n\n',IndexChOnlyStr);

fprintf(fid0,'Whale Count, Date, Log Name\n');

context.state.fid0 = fid0;
