function result = make_result( log, t0 )
% Make result struct for action_waitbar_update call

result.target = log;
result.status = 'done';
result.message = '';
result.error = [];
result.elapsed = toc( t0 );
result.output = struct( [] );