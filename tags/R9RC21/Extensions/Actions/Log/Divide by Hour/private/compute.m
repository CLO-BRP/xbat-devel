function [result, context] = compute(log, parameter, context)
result = struct;

% Divide by Hour - compute

if context.state.kill
  return;
end

%abort if log open
if log_is_open(log)
    disp(['Close log ''', log_name(log), ' before randomizing it.']); return;
end

%initializations
fn = log_name(log);
t = zeros(1,log.length);

%sort log by time
for i = 1:log.length
  t(i) = log.event(i).time(1);
end

[t IDX] = sort(t);
event = log.event(IDX);
first = 1;
last = 1;

%for each hour of the day
for h = 0:23
  
  BeginTime = 3600 * h;
  EndTime = 3600 * (h+1);
  
  %for events in current hour
  while last<=log.length && t(last)>=BeginTime && t(last)<EndTime
    
    last = last + 1;
    
  end
  
  if last>log.length ...
    || ~(last==log.length && t(last)<EndTime) ...
    && (last<=log.length && t(first)<=EndTime) 
    
    last = last - 1;

  end
  
  %if any events in hour, create hour log
  if t(last)>=BeginTime && t(first)<EndTime
    
    %make new empty log
    HrStr = sprintf('_%02.0fh', h);
    NewLogName = [fn HrStr];  %output log name

    NewLog = new_log(NewLogName,context.user,context.library,context.sound);

    %get sound struct from parent log, if requested
    if strcmp(parameter.SoundSource,'Parent Log')
      NewLog.sound = log.sound;
    end

    %make selection of events and add to new log
    NewLog.event = event(first:last);

    NewLog.length = last-first+1;
    NewLog.curr_id = log.curr_id;

    %save new log
    log_save(NewLog);
    
    %stop if last event in log
    if last==log.length
      return;
    end
    
    last = last + 1;
    first = last;
    
%   %if no events in hour, make empty log if requested by user  
%   elseif parameter.empty_log
%         
%     %make new empty log
%     HrStr = sprintf('_%02.0fh', h);
%     NewLogName = [fn HrStr];  %output log name
% 
%     NewLog = new_log(NewLogName,context.user,context.library,context.sound);
% 
%     %get sound struct from parent log, if requested
%     if strcmp(parameter.SoundSource,'Parent Log')
%       NewLog.sound = log.sound;
%     end
% 
%     %make empty event, set length to zero
%     NewLog.event = empty(event_create);
% 
%     NewLog.length = 0;
%     NewLog.curr_id = 1;
% 
%     %save new log
%     log_save(NewLog);

  end  
end
