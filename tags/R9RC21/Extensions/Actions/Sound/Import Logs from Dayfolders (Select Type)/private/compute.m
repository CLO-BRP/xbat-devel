function [result, context] = compute(sound, parameter, context)

% IMPORT_LOGS FROM DAYFOLDERS (SELECT TYPE) - compute

result = struct;

%don't run, if user picked 'cancel' on previous sound
if context.state.kill
  
  return;
  
end
  
%find datenum for current sound
SoundName = sound_name(sound);

LenSoundName = length(SoundName);

SoundRealtime = file_datenum(SoundName, 'yyyymmdd');

% SoundDateString = datestr(realtime, 'yyyymmdd');

%find which subdirectory of selected directory corresponds to current sound
RootDir = dir(context.state.RootPath);

DirRealtime = '';

i = 0;

while i < length(RootDir) && ~isequal(SoundRealtime, DirRealtime)
  
  i = i + 1;

  CurrDir = RootDir(i);
  
  if CurrDir.isdir && ~strcmp(CurrDir.name(1), '.')
    
    DirRealtime = file_datenum(CurrDir.name, 'yyyymmdd');
  
  end
end

if isequal(SoundRealtime, DirRealtime)
  
  DayPath = fullfile(context.state.RootPath, CurrDir.name);
  
  DayDir = dir(DayPath);
    
%   DateString = datestr(DirRealtime, 'yyyymmdd');
  
else

  txt = 'No subdirectory in selected directory corresponds to selected sound:';

  fprintf(2, 'ERROR: %s\n   %s\n', txt, SoundName);

  h = warndlg(sprintf('%s\n%s', txt, SoundName), 'ERROR');
  
  movegui(h, 'center')
  
  return;
  
end
    
% DirDateStr = datestr(realtime, 'yyyymmdd');
  
% DayDir = dir(fullfile(context.state.RootPath, DirDateString));

LogList = '';

for i = 1:length(DayDir)

  CurrDir = DayDir(i);

  if ~CurrDir.isdir

    [~, fn, ext] = fileparts(CurrDir.name);

    if strcmp(ext, '.mat')

      if strcmp(SoundName, fn(1:LenSoundName))

        LogList = [LogList, {fn}];

      else

        txt = 'File name does not include sound name, and was disregarded:';

        fprintf(2, '%s\n%s\n', txt, fn);

        warndlg(sprintf('%s\n%s', txt, fn), 'WARNING')

      end
    end
  end
end

len = length(LogList);

if ~len
      
  fprintf(1, '\nWARNING: No matching logs for %s\n', SoundName);

  h = warndlg(sprintf('No matching logs for\n%s', SoundName), 'WARNING');

  movegui(h, 'center')
  
end
  
LogTypes = cell(1,len);

%--- 20110727

for i = 1:len
  
  suffix = LogList{i}(LenSoundName+2:end);
  
  IDXsuffix = strfind(suffix, '_');
  
  if ~isempty(IDXsuffix)
    
    LogTypes{i} = suffix(1:max(1, IDXsuffix(1)-1));
    
  else
    
    LogTypes{i} = IDXsuffix;
    
  end

%   LogTypes{i} = LogList{i}(LenSoundName+2:end);

end

%---

%if first sound processed, select list of log types to copy
if ~isempty(context.state.SelectedTypes)
  
  SelectedTypes = context.state.SelectedTypes;
  
else

[IDX, status] = listdlg( ...
  'PromptString', 'Select one or more file types to copy.', ...
  'SelectionMode', 'multiple', ...
  'InitialValue', 1:len, ...
  'Name', context.ext.name, ...
  'ListString', LogTypes, ...
  'OKString', 'Import' ...
);

  if ~status

    context.state.kill = 1;

    return;

  end
  
  SelectedTypes = LogTypes(IDX);
  
  context.state.SelectedTypes = SelectedTypes;

end

DestPath = fullfile(context.library.path, SoundName, 'Logs');

for i = 1:len
  
  if any(strcmp(SelectedTypes, LogTypes{i}))
  
    SourceLog = fullfile(DayPath, [LogList{i}, '.mat']);
%     SourceLog = fullfile(context.state.RootPath, DateString, [LogList{i},
%     '.mat']);

      if context.state.flag || ~exist(fullfile(DestPath, [LogList{i}, '.mat']), 'file')

        %copy log to appropriate directory in filer60
        [copy_status, message] = copyfile(SourceLog, DestPath);
    
        if ~copy_status

          fail(message, 'ERROR')

          context.state.kill = 1;

          return;

        end

      else
        
%% Four Button questdlg substitute
        QuestFig=dialog(                                    ...
          'Visible'         ,'off'                      , ...
          'Name'            ,'Copy'                     , ...
          'Pointer'         ,'arrow'                    , ...
          'Units'           ,'inches'                   , ...
          'Position'        ,[4, 4, 3.5, 1.5]           , ...
          'IntegerHandle'   ,'off'                      , ...
          'WindowStyle'     ,'normal'                   , ...
          'HandleVisibility','callback'                 , ...
          'Tag'             ,'copy_warning'             , ...
          'Name'           ,'WARNING'                    ...
          );        
                  
        str = {'Replace', ...
               'Replace All', ...
               'Skip', ...
               'Cancel'};              
             
        pos = [-0.75, 0.25, 0.75, 0.25];
        
        CBString='uiresume(gcbf)';
             
        for j = 1:4
          
          pos(1) = pos(1) + 0.85;
        
          bh(j) = uicontrol(...
            QuestFig, ...
            'style'              , 'pushbutton', ...
            'units'              , 'inches', ...
            'position'           , pos, ...
            'CallBack'           , CBString    , ...
            'string'             , str{j}, ...
            'HorizontalAlignment', 'center');
                                         
        end

        txt = sprintf('%s\nalready exists', LogList{i});
        set(QuestFig,'visible','on')
                
        uicontrol(QuestFig            , ...
          'Style'              ,'text'         , ...
          'Units'              ,'normalized'   , ...
          'Position'           ,[0.05 0.5 0.9 0.4]      , ...
          'String'             ,txt            , ...
          'Tag'                ,'Question'     , ...
          'HorizontalAlignment','left'           ...
        );
     
        movegui(QuestFig, 'center')

        set(QuestFig ,'WindowStyle','modal','Visible','on');
        drawnow;
        
        if ishghandle(QuestFig)
 
          uiwait(QuestFig);
          
        end
        
        % Check handle validity again since we may be out of uiwait because the
        % figure was deleted.
        if ishghandle(QuestFig)

          button = get(get(QuestFig,'CurrentObject'),'String');

          delete(QuestFig);
          
        else
          
          button = 'Cancel';
          
        end

%%
        if strcmp(button, 'Replace') || strcmp(button, 'Replace All')

          %copy log to appropriate directory in filer60
          [copy_status, message] = copyfile(SourceLog, DestPath);
    
          if ~copy_status

            fail(message, 'ERROR')

            context.state.kill = 1;

            return;

          end

        end

        if strcmp(button, 'Replace All')

          context.state.flag = 1;

        elseif strcmp(button, 'Skip')

          return;

        elseif strcmp(button, 'Cancel')
          
          context.state.kill = 1;

          return;

        end                  
      end  
  end
end

