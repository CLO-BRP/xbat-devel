function result = control__callback(callback, context)

% DATE_TIME - control__callback

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

result = [];

%--
% get uncertainty from fake axes "userdata"
%--
ax_han = get_control(callback.pal.handle, 'userdata', 'handles');
uncertainty = get( ax_han.obj, 'userdata' );
if uncertainty == 0
  uncertainty = context.attribute.uncertainty;
end

str = get_control( callback.pal.handle, 'channel', 'value' );		
ch = str2double( str{1} );


%--
% perform control callback
%--
if strcmp( callback.control.name, 'channel' )

  if ~isempty(ch)
    set_control( callback.pal.handle, 'x_lower', 'value', num2str( uncertainty( ch, 1, 1) ) );
    set_control( callback.pal.handle, 'x_upper', 'value', num2str( uncertainty( ch, 1, 2) ) );
    set_control( callback.pal.handle, 'y_lower', 'value', num2str( uncertainty( ch, 2, 1) ) );
    set_control( callback.pal.handle, 'y_upper', 'value', num2str( uncertainty( ch, 2, 2) ) );
    set_control( callback.pal.handle, 'z_lower', 'value', num2str( uncertainty( ch, 3, 1) ) );
    set_control( callback.pal.handle, 'z_upper', 'value', num2str( uncertainty( ch, 3, 2) ) );
  end

else

  str = get_control( callback.pal.handle, callback.control.name, 'value' );		
  value = str2double( str );
  
  switch callback.control.name    
    case 'x_lower'
      uncertainty( ch, 1, 1 ) = value;
    case 'x_upper'
      uncertainty( ch, 1, 2 ) = value;
    case 'y_lower'
      uncertainty( ch, 2, 1 ) = value;
    case 'y_upper'
      uncertainty( ch, 2, 2 ) = value;
    case 'z_lower'
      uncertainty( ch, 3, 1 ) = value;
    case 'z_upper'
      uncertainty( ch, 3, 2 ) = value;
  end
  
end

%--
% Stash uncertainty into fake axes "userdata" for recovery by "save"
%--
set( ax_han.obj, 'userdata', uncertainty );
