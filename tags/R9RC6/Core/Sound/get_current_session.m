function [ix, str] = get_current_session(sound, time)

%--
% handle input
%--

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 20 Mar 2014
%       Add tolerance for machine epsilon in time calculations to that
%       correct session can be found reliably.
%   msp2 - 21 Mar 2014
%       Use better machine epsilon figure.

if nargin < 2 || isempty( time )
	slider = get_time_slider( get_active_browser ); 
    time = slider.value;
end

if nargin < 1 || isempty( sound )
	sound = get_active_sound;
end

ix = []; str = '';

if ~has_sessions_enabled( sound )
	return;
end

%--
% get sessions in real time
%--
sessions = get_sound_sessions( sound, 0 );

if isempty(sessions)
	return;
end

starts = [ sessions( : ).start ];
stops = [ sessions( : ).end ];

%--
% get real time (using machine epsilon for tolerance)
%--
time = map_time( sound, 'real', 'slider', time + 2 * eps( stops( end ) ) );

%--
% find current session and output info string
%--
ix = find( [ time < stops ], 1, 'first' );
str = sec_to_clock( starts( ix ) );
