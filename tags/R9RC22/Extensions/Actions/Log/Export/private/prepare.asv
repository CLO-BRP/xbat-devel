function [result, context] = prepare(parameter, context)
result = struct;

% EXPORT - prepare

% Log action copies selected XBAT logs to a user-defined directory.

% Michael Pitrick
% msp2@cornell.edu
% 11 July 2011

% To Do
%
% 1. 

% Change Log
%
% 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.



%% initializations

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  return;
end

if ~iscell(logs)
  logs = {logs};
end

NumLogs = length(logs);

status = ASE_log_is_open(logs, NumLogs);

if status
  
  return;
  
end

% DestPath is the path you want to copy logs to
DestPath = set_dest_path(context);

%% Copy Logs to Date Folders in Root Destination Path

flag = 0;

for i = 1:NumLogs
    
  %determine log name
  CurrentLog = logs{i};

  %find source path  
  [SoundName, fn] = fileparts(CurrentLog);
    
  SourcePath = [context.library.path, SoundName, '\Logs\'];

  %check if a single valid log
  if ischar(SoundName) && ischar(fn) 

    % find full path of log to be copied
    SourceLog = [SourcePath, fn, '.mat'];  

    if flag || ~exist(fullfile(DestPath, [fn, '.mat']), 'file')

      %copy log to appropriate directory in filer60
      copyfile(SourceLog, DestPath, 'f')

    else

      button = questdlg(sprintf('%s\nalready exists', SourceLog), ...
                           'Copy', ...
                           'Replace', ...
                           'Replace All', ...
                           'Cancel', ...
                           'Cancel');

      if strcmp(button, 'Replace') || strcmp(button, 'Replace All')

        %copy log to appropriate directory in filer60
        copyfile(SourceLog, DestPath)

      end

      if strcmp(button, 'Replace All')

        flag = 1;

      elseif strcmp(button, 'Cancel')

        return;

      end                  
    end

  else

    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');

    return;

  end
end

%% REFRESH PATH

ASE_refresh(context);


%%
function DestPath = set_dest_path(context)

ExtPath = fullfile( ...
  xbat_root, 'Extensions', 'Actions', 'Log', context.ext.name, 'private', 'preset.mat');

if exist(ExtPath, 'file')
  
  load(ExtPath);
  
else
  
  DestPath = '';
  
end

NewDestPath = uigetdir(DestPath, 'Select Directory to Receive Logs');

if isequal(NewDestPath, 0);
  
  return;
  
end

if ~strcmp(DestPath, NewDestPath)
  
  DestPath = NewDestPath;
  
  save(ExtPath, 'DestPath')
  
end
%%