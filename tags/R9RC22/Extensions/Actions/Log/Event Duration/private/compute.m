function [result, context] = compute(log, parameter, context)
result = struct;

% SUM EVENT DURATION - compute

duration = [];

for i = 1:log.length
  
  duration = [duration log.event(i).duration];

  
end

context.state.duration = [context.state.duration duration];

