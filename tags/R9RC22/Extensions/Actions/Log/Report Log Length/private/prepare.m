function [result, context] = prepare(~, context)
result = struct;

% Report Log Length - prepare


%% wrapper for prepare.m

out_dir = uigetdir(pwd, 'Output Directory');

if isequal(out_dir, 0)
  
  return;
  
end

cd(out_dir)

header = {'Log Length', ' '; ...
          ' ', ' '; ...
          'XBAT User:', context.user.name; ...
          'XBAT Library:', context.library.name; ...
          'Run Date and Time:', datestr(now); ...
          ' ', ' '; ...
          'Log Name', 'Log Length'};

xlsname = sprintf('Log_Length_%s_%s.xlsx', context.library.name, datestr(now,'yyyymmddTHHMMSSFFF'));

xlsname_full = fullfile(out_dir, xlsname);


%% wrapper for compute.m

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  
  logs = context.target;
  
else
  
  fprintf(2,'API supplied no context.target in prepare.m \n');
  
  return;
  
end

if ~iscell(logs)
  
  logs = {logs};
  
end

% for each log
NumLogs = length(logs);

body = cell(NumLogs,2);

h = waitbar(0, ...
            sprintf('Log %.0f of %.0f', 0, NumLogs), ...
            'Name', context.ext.name);

for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  
  log = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ~(ischar(SoundName) && ischar(fn) && isequal(length(log),1))
    
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');

    return;
    
  end
    


  %% compute.m

  body(i,1:2) = {fn, log.length};
  
  waitbar(i/NumLogs, h, sprintf('Log %.0f of %.0f', i, NumLogs))
  
 
end


%% wrapper for conclude.m

M = [header ; body];

xlswrite(xlsname_full, M);

close(h)

% error('***** IGNORE THIS ERROR! *****')
