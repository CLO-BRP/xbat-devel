function parameter = parameter__create(context)

% PRESENCE - parameter__create

parameter = struct;

parameter.check = 1;

parameter.refresh = 1;

parameter.tag = 'BWHD';

parameter.unit = 1;

parameter.output = 1;

parameter.csv = 1;
