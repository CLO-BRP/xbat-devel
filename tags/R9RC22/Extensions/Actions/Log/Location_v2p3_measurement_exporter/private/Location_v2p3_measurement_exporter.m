function varargout = Location_v2p3_measurement_exporter(calling_mode, varargin)

%%
%% 'Location_v2p3_measurement_exporter'
%%
%% Measurement export description for the Location_v2p3_measurement
%%
%% Kathryn A. Cortopassi, Oct 2005
%%
%% syntax:
%% -------
%%   column_headers = Location_v2p3_measurement_exporter('export_headers', browser_log);
%%
%%   values = Location_v2p3_measurement_exporter('export_values', browser_log, event_inx, meas_inx);
%%
%% input:
%% ------
%%  browser_log = XBAT log structure
%%  event_inx = index for specific event
%%  meas_inx = index for measurement for specific event
%%
%% output:
%% -------
%%  column_headers = column header label strings (as a cell array)
%%                     for the values to be exported
%%  values = measurement values for specific event in order specified
%%           by 'column_headers'

% History
%   msp2 - 8 July 2012
%       Remove "Decimal Date-Time" value, which has the following problems:
%           Date reference incorrect.  Should be 00-Jan-1900 00:00:00, not 01-Jan-1900 00:00:00
%           Not enough significant digits.  %.3f is not even to nearest minute
%           Relies on date-time attribute being set correctly in log.
%       Add event "ID"
%       Add "Reference Channel"
%       Add "Real Begin Date (yyyy-mm-dd)"
%       Add "Real Begin Time (hh:mm:ss)"
%       Add "Real End Date (yyyy-mm-dd)"
%       Add "Real End Time (hh:mm:ss)"
%       Add "Recording Begin (hh:mm:ss)"
%       Add "Recording End (hh:mm:ss)"
%   msp2 - 12-28 September 2012
%       Accommodate Locatation v2p2 measurements.
%       Handle events with no measurements gracefully.
%       Controls for selecting output columns.
%       Separate sensor geometry output into separate worksheet.
%       Remember parameter settings between sessions.
%       Add sound attributes with error estimates to output.
%       Add columns for UTM and lat-long.
%       Add columns for event tag, event rating, event score, and event freq.
%       Add columns for Loc Total Errror, Error Due to Noise, Error Due to
%       Sound Speed, and Error Due to Sensor Position.
%       Omit columns for z axis, since these are not currently calculated.
%   msp2 - 1 December 2012
%       Accomodate second dimensions in "statictical error".
%   msp2 - 1 Feb 2013
%       Add "summary output".
%   msp2 - 17 July 2013
%       Accomomdate Location v2p3 measurements, which can have errors set
%         to [] and NaN.
%   msp2 - 14 April 2014
%       Accommodate log names with multiple ".".

switch (calling_mode)


  case ('export_headers')
    %% get column header labels and format control string
    %% for writing to comma-delimited output file

    browser_log = varargin{1};
    
    num_chan = browser_log.sound.channels;
    
    
%--- begin pitz 20120912
    
    zone = [];
    
    hem = '';
  
    if isfield(browser_log.sound.geometry, 'global') ...
    && ~isempty(browser_log.sound.geometry.global) ...
    && isfield(browser_log.sound.geometry, 'ellipsoid') ...
    && ~isempty(browser_log.sound.geometry.ellipsoid)
        
      latlon_ref = browser_log.sound.geometry.global(1,:);

      ellipsoid = browser_log.sound.geometry.ellipsoid;

      [zone,~,~,hem] = deg2utm(latlon_ref(2),latlon_ref(1),ellipsoid);
      
    end
    
%--- end pitz 20120912
    

    corr_peaks = [];
    for inx = 1:num_chan
      corr_peaks = [corr_peaks, {sprintf('Corr Pk %dxRef', inx)}];
    end

    sm_peaks = [];
    for inx = 1:num_chan
      sm_peaks = [sm_peaks, {sprintf('Pk Sm %dxRef', inx)}];
    end

    peak_lags = [];
    for inx = 1:num_chan
      peak_lags = [peak_lags, {sprintf('Pk Lag %dxRef (sec)', inx)}];
    end

    loc_lags = [];
    for inx = 1:num_chan
      loc_lags = [loc_lags, {sprintf('Loc Lag %dxRef (sec)', inx)}];
    end

    dist2sens = [];
    for inx = 1:num_chan
      dist2sens = [dist2sens, {sprintf('Dist to %d (m)', inx)}];
    end

    
%--- begin pitz 20120912
    
    column_headers = [...
      {'ID'},...
      {'Tag'},...
      {'Rating'},...
      {'Score'},...
      {'Ref Chan'},...
      {'Lower Frequency'},...
      {'Upper Frequency'},...
      {'Begin Time (sec)'},...
      {'End Time (sec)'},...
      {'Decimal Day'},...
      {'Date-Time (yyyy-mm-dd hh:mm:ss)'},...
      {'Julian Date'}, ...
      {'X Location (m)'},...
      {'Y Location (m)'},...
      {'Loc Total Error (X,m)'},...
      {'Loc Total Error (Y,m)'},...
      {'Error Due to Noise (X,m)'},...
      {'Error Due to Noise (Y,m)'},...
      {'Error Due to Sound Speed (X,m)'},...
      {'Error Due to Sound Speed (Y,m)'},...
      {'Error Due to Sensor Position (X,m)'},...
      {'Error Due to Sensor Position (Y,m)'},...
      {sprintf('Easting (zone %.0f%s)', zone, hem)},...
      {sprintf('Northing (zone %.0f%s)', zone, hem)},...
      {'Latitude'},...
      {'Longitude'},...
      {'Loc Avg Corr Sum'},...
      {'All Locations'},...
      {'Med Cent Dist (m)'},...
      {'Iterations'},...
      {'Channels'},...
      corr_peaks,...
      {'Channel Wts'},...
      sm_peaks,...
      peak_lags,...
      loc_lags,...
      dist2sens, ...
      {'Range (m)'},...
      {'Bearing (�)'},...
      {'Loc Range Error (m)'},...
      {'Loc Bearing Error (�)'}...
      ];
       
% % Kathy's original    
% %     column_headers = [...
% %       {'Decimal Date-Time'},...
% %       {'X Location'},...
% %       {'Y Location'},...
% %       {'Z Location'},...
% %       {'Loc Avg Corr Sum'},...
% %       {'All Locations'},...
% %       {'Med Cent Dist'},...
% %       {'Iterations'},...
% %       {'Channels'},...
% %       corr_peaks,...
% %       {'Channel Wts'},...
% %       sm_peaks,...
% %       peak_lags,...
% %       loc_lags,...
% %       {'Range'},...
% %       {'Bearing'},...
% %       {'Elevation'},...
% %       dist2sens
% %       ];


%--- end pitz 20120916
    
    
    %% make a column vector
    column_headers = column_headers';


    varargout{1} = column_headers;



  case ('export_values')
    %% get the values corresponding to the column header labels defined above

    browser_log = varargin{1};
    
    event_inx = varargin{2};
    
    meas_inx = varargin{3};
    
    utc_offset = varargin{4};

    num_chan = browser_log.sound.channels;
    
    ref_chan = browser_log.event(event_inx).channel;
    
    
%--- begin pitz 20120912
    
    tag = browser_log.event(event_inx).tags;
    
    if iscell(tag)
      
      if ~isempty(tag)
      
        tag = tag{1};
        
      else
        
        tag = '';
        
      end
      
    end    
    
    if isempty(browser_log.event(event_inx).measurement)
        
        null_output = cell(varargin{5},1);
        
        varargout{1} = null_output;
        
        return;
        
    end
    
%--- end pitz 20120912
    

    value_struc = browser_log.event(event_inx).measurement(meas_inx).value;

    values = [];


%--- begin pitz 20120714

    curr_event = browser_log.event(event_inx);


    % event id
    values = [values; {browser_log.event(event_inx).id}];
    
    

    % event tag
    values = [values; {tag}];
    

    % event rating
    values = [values; {browser_log.event(event_inx).rating}];

    % event score
    values = [values; {browser_log.event(event_inx).score}];
    
    % reference channel
    values = [values; {ref_chan}];
    
    %lower frequency
    values = [values; {browser_log.event(event_inx).freq(1)}];
    
    %upper frequency
    values = [values; {browser_log.event(event_inx).freq(2)}];
    
    %recording time of beginning of event
    values = [values; {curr_event.time(1)}];
    
    %recording time of end of event
    values = [values; {curr_event.time(2)}];


    % real date and time

    %if date-time attribute is set, set values for real date and time
    if isscalar(browser_log.sound.realtime)

      [date_str,time_str,excel_date,julian_day] = ...
        real_time(curr_event.time(1), browser_log.sound, utc_offset);

      values = [values; {excel_date}];

      values = [values; {['''',date_str,'   ',time_str]}];

      values = [values; {julian_day}];


    %if date-time attribute is not set, set value to 'NA'
    else

      values = [values; {'NA'}; {'NA'}; {'NA'}];

    end


% % % Kathy's code, problems with this listed in header to this function
% %
% %     if ~isempty(browser_log.sound.realtime)
% %       %% return decimal time with 1-Jan-1900, 00:00:00 as reference
% %       dectime = (browser_log.sound.realtime + (browser_log.event(event_inx).time(1)) / (86400)) - datenum('01-Jan-1900, 00:00:00');
% %       values = [values; {num2str(dectime, '%.3f')}];
% %     else
% %       values = [values; {'NA'}];
% %     end


    if ~isempty(value_struc.xlocation)
      values =  [values; {num2str(value_struc.xlocation, '%.2f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.ylocation)
      values =  [values; {num2str(value_struc.ylocation, '%.2f')}];
    else
      values = [values; {' '}];
    end

%     if ~isempty(value_struc.zlocation)
%       values =  [values; {num2str(value_struc.zlocation, '%.2f')}];
%     else
%       values = [values; {' '}];
%     end

    if isfield(value_struc, 'total_error') ...
    && ~isempty(value_struc.total_error)   ...
    && all(~isnan(value_struc.total_error))
      values =  [values; {num2str(value_struc.total_error(1), '%.2f')}];
      values =  [values; {num2str(value_struc.total_error(2), '%.2f')}];
    else
      values = [values; {' '}];
      values = [values; {' '}];
    end
    
    if isfield(value_struc, 'statistical_error') ...
    && ~isempty(value_struc.statistical_error)   ...
    && all(~isnan(value_struc.statistical_error))
      
      statistical_error = cell(2,1);
      
      statistical_error(:) = {''};
      
      for i = 1:length(value_struc.statistical_error)
        
        statistical_error(i) = {num2str(value_struc.statistical_error(i), '%.2f')};
        
      end     
      
      values =  [values; statistical_error];
      
%       values =  [values; {num2str(value_struc.statistical_error, '%.2f')}];

    else
      values = [values; {' '}];
      values = [values; {' '}];
    end

    if isfield(value_struc, 'sound_speed_error') ...
    && ~isempty(value_struc.sound_speed_error) ...
    && all(~isnan(value_struc.sound_speed_error))
      values =  [values; {num2str(value_struc.sound_speed_error(1), '%.2f')}];
      values =  [values; {num2str(value_struc.sound_speed_error(2), '%.2f')}];
    else
      values = [values; {' '}];
      values = [values; {' '}];
    end

    if isfield(value_struc, 'cummulative_error_sensor_pos') ...
    && ~isempty(value_struc.cummulative_error_sensor_pos)   ...
    && all(~isnan(value_struc.cummulative_error_sensor_pos))
      values =  [values; {num2str(value_struc.cummulative_error_sensor_pos(1), '%.2f')}];
      values =  [values; {num2str(value_struc.cummulative_error_sensor_pos(2), '%.2f')}];
    else
      values = [values; {' '}];
      values = [values; {' '}];
    end
     
    
    % Easting
    if isfield(browser_log.sound.geometry,'offset') ...
    && ~isempty(browser_log.sound.geometry.offset) ...
    && ~isempty(value_struc.xlocation)
    
      offset = browser_log.sound.geometry.offset;

      utm_east = value_struc.xlocation + offset(1);

      values = [values; {num2str(utm_east, '%.2f')}];

      bad_utm = 0;
        
    else
        
      values =  [values; {''}];

      bad_utm = 1;
        
    end

    
    % Northing
    if ~bad_utm && ~isempty(value_struc.ylocation)
        
      utm_north = value_struc.ylocation + offset(2);

      values = [values; {num2str(utm_north, '%.2f')}];

    else
        
      values =  [values; {''}];

      bad_utm = 1;
        
    end
    
  
    % Latitude and Longitude
  
    if ~bad_utm ...
    && isfield(browser_log.sound.geometry, 'global') ...
    && ~isempty(browser_log.sound.geometry.global) ...
    && isfield(browser_log.sound.geometry, 'ellipsoid') ...
    && ~isempty(browser_log.sound.geometry.ellipsoid)
        
      latlon_ref = browser_log.sound.geometry.global(1,:);

      ellipsoid = browser_log.sound.geometry.ellipsoid;

      [zone,~,~,hem] = deg2utm(latlon_ref(2),latlon_ref(1),ellipsoid);

      [lon,lat] = utm2deg(zone, ...
                          utm_east, ...
                          utm_north, ...
                          hem, ...
                          ellipsoid);
          
      values =  [values; {num2str(lat, '%.9f')}];
          
      values =  [values; {num2str(lon, '%.9f')}];

    else
          
      values =  [values; {''}];

      values =  [values; {''}];
        
    end


%--- end pitz 20120914






    if ~isempty(value_struc.xyz_corr_sum)
      values =  [values; {num2str(value_struc.xyz_corr_sum, '%.3f')}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.all_locations)
      values =  [values; {'MAT'}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.cent_dist)
      values =  [values; {num2str(value_struc.cent_dist, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.iterations)
      values =  [values; {num2str(value_struc.iterations)}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.channels_used)
      values =  [values; {num2str(value_struc.channels_used, '%d;')}];
    else
      values = [values; {' '}];
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_vals)
          values = [values; {num2str(value_struc.peak_vals(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    if ~isempty(value_struc.channels_wts)
      values =  [values; {num2str(value_struc.channels_wts, '%.3f;')}];
    else
      values = [values; {' '}];
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_vals_sm)
          values = [values; {num2str(value_struc.peak_vals_sm(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_lags)
          values = [values; {num2str(value_struc.peak_lags(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.loc_lags)
          values =  [values; {num2str(value_struc.loc_lags(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end

%--- begin pitz 20120928
    
%     if ~isempty(value_struc.elevation)
%       values =  [values; {num2str(value_struc.elevation, '%.2f')}];
%     else
%       values = [values; {' '}];
%     end

%--- end pitz 20120928

    counter = 0;
    for inx = 1:num_chan
      if ismember(inx,  value_struc.channels_used)
        counter = counter + 1;
        if ~isempty(value_struc.dist2channels)
          values = [values; {num2str(value_struc.dist2channels(counter), '%.2f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    if ~isempty(value_struc.range)
      values =  [values; {num2str(value_struc.range, '%.2f')}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.bearing)
      values =  [values; {num2str(value_struc.bearing, '%.2f')}];
    else
      values = [values; {' '}];
    end
    
    
    %--- begin pitz 20120924

    if isfield(value_struc, 'range_error') && ~isempty(value_struc.range_error)
      values =  [values; {num2str(value_struc.range_error, '%.2f')}];
    else
      values = [values; {' '}];
    end
    
    if isfield(value_struc, 'bearing_error') && ~isempty(value_struc.bearing_error)
      values =  [values; {num2str(value_struc.bearing_error, '%.2f')}];
    else
      values = [values; {' '}];
    end
    
    %--- end pitz 20120924


    varargout{1} = values;



end %% switch


return;


%--- begin pitz 20120708


function [date_str,time_str,excel_date,julian_day] = real_time(t,snd,utc_offset)

%calculate MATLAB datenum
date_num = snd.realtime + map_time(snd,'real','record',t)/86400;

%calculate Excel decimal day
excel_date = date_num - datenum('30-Dec-1899, 00:00:00');

%calculate date string and time string
date_str = datestr(date_num,'yyyy-mm-dd');

time_str = datestr(date_num,'HH:MM:SS.FFF');

%calculate Julian day
julian_day = juliandate(datevec(date_num - (utc_offset / 24)));


% t0 = datenum(1968, 5, 23) - 2440000 + .5;  
% 
% julian_day = date_num - t0 - (utc_offset / 24);


%--- end pitz 20120708
