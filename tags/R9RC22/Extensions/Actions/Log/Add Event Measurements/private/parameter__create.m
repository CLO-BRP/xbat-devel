function parameter = parameter__create(context)

% ADD EVENT MEASUREMENTS - parameter__create

% parameter = struct;

%load parameter preset
preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'preset.mat');		

% load preset, if readible                    
try    
  load(preset_path)
  
  %warn user if user parameters have changed
  warn_parameter_change( parameter, preset_path)

% if preset not readible, create one
catch
  parameter = make_parameter;
  save(preset_path,'parameter');
% %   fail( 'preset.mat initialized because it is missing or corrupt.', 'WARNING' )
end


function warn_parameter_change( parameter, preset_path)
%warn user if parameter has changed sine last invocation

%re-initialize preset.mat if parameter fields have changed
newfields = setdiff( fieldnames( make_parameter ), fieldnames( parameter ) );
if ~isempty( newfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been added to GUI.';
  d2 = sprintf( '   %s\n', newfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end

oldfields = setdiff( fieldnames( parameter ), fieldnames( make_parameter ) );
if  ~isempty( oldfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been removed from GUI.';
  d2 = sprintf( '   %s\n', oldfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end


function parameter = make_parameter
%initialize parameter if extension has never been invoked

%MEASUREMENT EXTENSION
parameter.measurements = {''};


%% CSE LOCATION (v2.3)

%MEASUREMENT-SPECIFIC PARAMETERS - part 1
parameter.corr_type = '';
parameter.corr_type_idx = 1;
parameter.corr_num = 'N(N-1)/2';
parameter.abs_pk_thresh = 0.01;
parameter.rel_pk_thresh = 75;
parameter.min_channels = 3;
parameter.max_channels = 11;
parameter.channels = '';
parameter.channels_idx = 1;

%MEASUREMENT-SPECIFIC PARAMETERS - part 2
parameter.search_type = 'grid';
parameter.search_radius = 20000;
parameter.theta_min = [];
parameter.theta_max = [];
parameter.term_crit = 500;
parameter.mean_type = 'arithmetic';
parameter.search_dim = '';
parameter.search_dim_idx = 1;
parameter.display_plots = '';
parameter.display_plots_idx = 1;
parameter.error_est = '';
parameter.error_est_idx = 1;
parameter.author = '';

%SPECTROGRAM TAB
parameter.spc_fft = 512;
parameter.spc_hop = 0.25;
parameter.spc_win_type = '';
parameter.spc_win_type_idx = 1;
% parameter.spc_win_type_idx = 10;
% parameter.win_param = 7;
parameter.spc_win_length = 1;

%FILTER TAB
parameter.filter_name = '';
parameter.filter_name_idx = 17;

parameter.test = {''};
parameter.test_idx = 0;  %not used - listed here to avoid re-initialing preset.mata


%% RL-TD (only parameters not already listed for CSE LOCATION (v2.3)
parameter.fftsz = '';
parameter.fftsz_idx = 9;
parameter.winfunc = '';      %default 'Hann'
parameter.winfunc_idx = 9;
parameter.noisebuff = 5;
parameter.noiseparam = 0.5;
parameter.sensor_calib_db = []; %not exposed?
parameter.sensor_bits = 12;
parameter.ref_Pa = 20;
parameter.sensor_geom = [];     %not exposed?
parameter.sound_speed = [];     %not exposed?
parameter.medium_density = 1.2;
parameter.use_stdFreq = '';     %default 'no'
parameter.use_stdFreq_idx = 1;
parameter.flo = 0;
parameter.fhi = 1000;
parameter.author = '';
