function [result, context] = compute(~, ~, context)

% RANDOM SAMPLE (log action) - compute

% Randomizes order of events in XBAT log, then breaks up the log into
% multiple logs with user-definable maximum size. Useful for randomly
% sampling the events in large logs.

% WON'T RUN IF LOGS FROM MULTIPLE SOUNDS SELECTED
% "reference to non-existent field 'type'"

%Mike Pitrick
%msp2@cornell.edu
%24 Feb 09

%Changes
% msp2   1 Nov 2011  move code to prepare so can run on multiple days

result = struct;


% %abort if log open
% if log_is_open(log)
%     disp(['Close log ''', log_name(log), ' before randomizing it.']); return;
% end
% 
% %initializations
% n = parameter.n;
% nStr = num2str(n);
% % nStr = context.state.nStr;
% len = log.length;
% curr_id = log.curr_id;
% 
% %randomize order of events. store in Event
% nNewLogs=ceil(len/n);
% idx = randperm(len);
% Event = log.event;
% 
% %find log name
% fnLog = log.file;
% fnLog = fnLog(1:end-4);
% 
% %break randomized log into nNewLogs logs of size n
% for i=1:nNewLogs
% 
%   %make new empty log
%   iStr = num2str(i);
%   NewLogName = [fnLog '_RANDOM' nStr '-' iStr];  %output log name
% %  RandomLog = struct([]);
%   RandomLog = new_log(NewLogName,context.user,context.library,context.sound);
% 
%   %make random selection of events and add to new log
% 
%   %new_log.event = empty(event_create);
%   first = n*(i-1)+1;
%   last = n*i;
%   if last > len
%     last = len;
%   end
%   RandomLog.event = Event(idx(first:last));
%   RandomLog.length = last-first+1;
%   RandomLog.curr_id = curr_id;
%   %RandomLog.curr_id = log.event(first).id;
% 
%   %save new log
%   log_save(RandomLog);
% end