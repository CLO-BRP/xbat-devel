function [result, context] = conclude(parameter, context)

% HOURLY PRESENCE - conclude

result = struct;

fprintf(1,'%.0f channel-hours with events in selected logs\n', ...
  context.state.count);

fprintf(1,'(NOTE: ONLY CONSIDERS BEGINNING OF EVENT, NOT END)\n\n');
