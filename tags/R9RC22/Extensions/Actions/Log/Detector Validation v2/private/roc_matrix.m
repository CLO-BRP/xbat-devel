function M_out = roc_matrix(M, count, duration, parameter)

%if no negative truth events (TN not defined explicitly)
if ~parameter.truth_flag
  
%   %if writing summary line
%   if strcmp(fn_truth, 'TOTAL')
%     
%     idx = 2;
%     
%     M_new{idx,13} = 'COMBINED LOGS';
%     M_new{idx,14} = ' ';
%     
%   else
%     
%     idx = 1;
% 
%     M_new{idx,13} = fn_truth;
%     M_new{idx,14} = fn_test;    
%     
%   end

  threshold = 0 : 0.05 : 1;
  
  M_new = cell(20,13);
  
  for i = 1:20
    
    curr_truth_TP = count.truth.TP(i);
    
    curr_truth_FN = count.truth.FN(i);
    
    
    curr_test_TP = count.test.TP(i);
    
    curr_test_FP = count.test.FP(i);
    
  
    [tpr, ci] = binofit(curr_truth_TP, curr_truth_TP + curr_truth_FN, 1 - parameter.plevel);
    
    M_new{i,1} = threshold(i);
 
    M_new{i,2} = tpr;
    M_new{i,3} = ci(1);
    M_new{i,4} = ci(2);

    M_new{i,5} = curr_truth_TP / (curr_truth_TP + curr_truth_FN);

    M_new{i,6} = curr_test_FP / (duration / 3600);
    M_new{i,7} = curr_test_TP / (curr_test_TP + curr_test_FP);

    M_new{i,8} = curr_truth_TP;
    M_new{i,9} = curr_truth_FN;
    M_new{i,10} = curr_truth_TP + curr_truth_FN;

    M_new{i,11} = curr_test_TP;
    M_new{i,12} = curr_test_FP;
    M_new{i,13} = curr_test_TP + curr_test_FP;
    
  end

  
%if negative truth events (TN defined explicitly)  
else
  
%   %if writing summary line
%   if strcmp(fn_truth, 'TOTAL')
%     
%     idx = 2;
%     
%     M_new{idx,20} = 'COMBINED LOGS';
%     M_new{idx,21} = ' ';
%     
%   else
%     
%     idx = 1;
% 
%     M_new{idx,20} = fn_truth;
%     M_new{idx,21} = fn_test;    
%     
%   end

  threshold = 0 : 0.05 : 1;
  
  M_new = cell(20,20);
  
  for i = 1:20
    
    curr_truth_TP = count.truth.TP(i);
    
    curr_truth_FP = count.truth.FP(i);
    
    curr_truth_FN = count.truth.FN(i);
    
    curr_truth_TN = count.truth.TN(i);
    
    
    curr_test_TP = count.test.TP(i);
    
    curr_test_FP = count.test.FP(i);
    
  
    [tpr, ci] = binofit(curr_truth_TP, curr_truth_TP + curr_truth_FN, 1 - parameter.plevel);
    
    M_new{i,1} = threshold(i);
  
    M_new{i,2} = tpr;
    M_new{i,3} = ci(1);
    M_new{i,4} = ci(2);

    M_new{i,5} = curr_truth_TP / (curr_truth_TP + curr_truth_FN);
    M_new{i,6} = curr_truth_FP / (curr_truth_FP + curr_truth_TN);

    M_new{i,7} = curr_test_FP / (duration / 3600);
    M_new{i,8} = curr_test_TP / (curr_test_TP + curr_test_FP);

    M_new{i,9} = curr_truth_TP;
    M_new{i,10} = curr_truth_FN;
    M_new{i,11} = curr_truth_TP + curr_truth_FN;

    M_new{i,12} = curr_truth_FP;
    M_new{i,13} = curr_truth_TN;
    M_new{i,14} = curr_truth_FP + curr_truth_TN;

    M_new{i,15} = curr_truth_TP + curr_truth_FN + curr_truth_FP + curr_truth_TN;
    M_new{i,16} = count.truth.total;    

    M_new{i,17} = curr_test_TP;
    M_new{i,18} = curr_test_FP;
    M_new{i,19} = curr_test_TP + curr_test_FP;

    M_new{i,20} = count.test.total;
    
  end  
end

M_out = [M; M_new];

