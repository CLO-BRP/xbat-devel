function [result, context] = compute(log, parameter, context)
result = struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XBAT2RAVEN V3-1 - compuate
%
% XBAT2Raven v3.2                                                 
%                                                                 
% Makes Raven Selection Tables for all selected logs              
%                                                                 
% Note: Refreshes log.file and log.path to current log name and   
%       current path                                              
%                                                                 
% designed for use in XBAT Pallet Log window
%
% Exports selected XBAT log to Raven selection table
%
% Ignores "Date Time" and "Time Stamp" attributes
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Michael Pitzrick
%  msp2@cornell.edu
%  21 Sep 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Changes:
%
%  1. All Raven selection tables go to the same directory (the default 
%     directory for each log is log.path as XBAT logs)
%
%  2. All Raven selection tables output to the same directory.
%
%  3. 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%

% initializations
if context.state.kill
  return;
end

out_path = context.state.out_path;
out_file = fullfile(out_path, [log_name(log) '.txt']);

% open exported Raven Selection Table
if exist(out_file,'file')==2
  
  dial = 'Raven Selection Table already exists. Pick a different name or folder.';

  [fn, out_path] = uiputfile('*.txt', dial, out_file);
    
  out_file = fullfile(out_path, fn);
  
end


fid = fopen(out_file, 'w');
% fid = fopen([log.path log.file(1:end-4) '.txt'], 'w');

% if isequal(fid,-1)
%   fprintf(2,'\n\nA Raven Selection Table with this name already exists. Please pick a new one.');
% end

% print headers in Raven Selection Table
header1 =  'Selection	View	Channel	Begin Time (s)	End Time (s)';
header2 = '	Low Freq (Hz)	High Freq (Hz)	score	rating	tags	notes';
fprintf(fid, '%s%s\n', header1, header2);

% loop through events in log
for j = 1:log.length

    % structure to array
    id = log.event(j).id;
    channel = log.event(j).channel;
    t(1) = log.event(j).time(1);
    t(2) = log.event(j).time(2);
    freq(1) = log.event(j).freq(1);
    freq(2) = log.event(j).freq(2);
    score = log.event(j).score;
    scores = num2str(score);
    rating = log.event(j).rating;
    ratings = num2str(rating);
    tagsCell = log.event(j).tags;
    lengthCell = length(tagsCell);
    tags = '';
    if isempty(tagsCell)
        tags = ' ';
    else
        tags = tagsCell{1};
        for i = 2:lengthCell
            tags = [tags ' | ' tagsCell{i}];
        end
    end
    notesCell = log.event(j).notes;
    if isempty(notesCell)
        notes = ' ';
    elseif iscell(notesCell)
        notes = notesCell{1};
    else
        notes = notesCell;
    end

    hh = zeros(1,2);
    tt = zeros(1,2);
    mm = zeros(1,2);
    ss = zeros(1,2);
    hhs = cell(2);
    mms = cell(2);
    sss = cell(2);
    time = cell(2);

    for i=1:2      %loop through begin and end time of event
        % divide into hh, mm, ss
        hh(i) = floor(t(i)/3600);
        tt(i) = mod(t(i), 3600);
        mm(i) = floor(tt(i)/60);
        ss(i) = mod(t(i),60);

        % format hh:mm:ss
        hhs{i} = int2str(hh(i));
        if length(hhs{i})==1
            hhs{i} = ['0' hhs{i}];
        end
        mms{i} = int2str(mm(i));
        if length(mms{i})==1
            mms{i} = ['0' mms{i}];
        end
        sss{i} = num2str(ss(i),'%5.2f');
        if length(mms{i})==1
            mms{i} = ['0' mms{i}];
        end

        if length(sss{i})==4
            sss{i} = ['0' sss{i}];
        end
        time{i}=sprintf('%2s:%2s:%5s', hhs{i}, mms{i}, sss{i});
    end

    f1='%.0f\t%s\t%.0f\t%.3f\t%.3f\t%.1f\t%.1f\t%s\t%s\t%s\t%s\n';
    ln1=sprintf(f1, id, 'Spectrogram 1', channel, t(1),t(2),freq(1),freq(2),scores,ratings,tags,notes);
    fprintf(fid, '%s%s', ln1);
end

fclose(fid);



