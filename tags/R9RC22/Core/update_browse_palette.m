function update_browse_palette( par, data, delete_flag )

% update_browse_palettes - update browse palette when logs change
% ----------------------------------------------------------------------
%
% pals = update_browse_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

% History
%   msp2 - 27 May 2014
%       Clean up code.
%   msp2 - 11 June 2014
%       When saving and opening presets, unset duration and frequency is
%         set to [] or '' rather than NaN.
%   msp2 - 12 June 2014
%       If log in tooltip is open, set in button color and ready state.
%       Remove unused output parameters from call to parse_tooltip.
%   msp2 - 13 June 2014
%       Tweak contrasting font color for buttons.
%       Update selection clipboard display from selection clipboard in browser.
%   msp2 - 15 June 2014
%       If selection clipboard in browser is empty, set it using values in
%         Browse palette.

%--
% check browser handle input
%--
if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--
if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%--
% list open logs
%--
none = isempty(data.browser.log);
if none
	list = {'(No Open Logs)'}; 
%     ix = 1;
else
	list = file_ext(struct_field(data.browser.log, 'file')); 
%     ix = data.browser.log_active;
end
		
%--
% list uicontrols whose state needs updating
%--
control_name = { 'button_1', 'button_2', 'button_3', 'button_4', 'button_5', 'button_6' };

%--
% update uicontrols
%--
pal = get_xbat_figs('type', 'palette', 'parent', par);
name = get( pal, 'Name' );
idx = strcmp( name, 'Browse' );
pal = pal( idx );
if ~isempty( pal )
	for j = 1:length(control_name)
        
        %--
        % get control struct
        %--
        h_control = get_control( pal, control_name{ j }, 'handles' );
        if isempty( h_control )
            continue;
        end        

        %--
        % find log name currently in button tooltip
        %--
        h_button = h_control.obj;
        tooltip = get( h_button, 'TooltipString' );
        tooltip_log = parse_tooltip( tooltip );
        
        %--
        % find ready state of button
        %--
        S = get( h_button, 'Userdata' );
        ready = S.ready;
        
        %--
        % find index of tooltip log
        %--
        idx = strcmp( tooltip_log, list );

        %--
        % if tooltip log is open and button state is unready, set it to ready
        %--
        if ready && any( idx )

            % recover color for log associated with button
            log_color = data.browser.log( idx ).color;
            
            % choose color (black/white) which contrasts log color
            test = ( log_color( 1 ) < 0.75 ) && ( log_color( 2 ) < 0.97 );
            c_fore = test * ones( 1, 3 );

            % set button color and ready state
            set( h_button, ...
                'BackgroundColor', log_color, ...
                'ForegroundColor', c_fore, ...
                'UserData',        struct( 'ready', 1 ) )
            
        %--
        % if tooltip log is not open and button state is ready, set button to to unready state
        %--
        elseif ~isempty( tooltip_log ) && ~any( idx ) && delete_flag
            
            % reinitialize button
            tooltip_new = make_tooltip( '', '', '', [], [] );
            set( h_button, ...
                'TooltipString',   tooltip_new, ...
                'String',          '', ...
                'UserData',        struct( 'ready', 0 ), ...
                'BackgroundColor', get(0, 'DefaultFigureColor' ), ...
                'ForegroundColor', [ 0 0 0 ], ...
                'Cdata', [], ...
                'UserData',        struct( 'ready', 0 ) ...
            )            
        end
    end
    
    %--
    % if update selection clipboard is empty, initialize from Browse palette
    %--
    event = data.browser.selection.copy;
    if isempty( event )
        
        % create empty event
        event = event_create;
        event.level = 1;
        
        % retrieve control values from Browse palette
        event.tags = get_control( pal, 'clipboard_tag', 'string' );
        event.duration = get_control( pal, 'clipboard_duration', 'value' );
        event.freq( 1 ) = get_control( pal, 'clipboard_min_freq', 'value' );
        event. freq( 2 ) = get_control( pal, 'clipboard_max_freq', 'value' );
        event.bandwidth = get_control( pal, 'clipboard_bandwidth', 'value' );
        
        % update selection clipboard in browser
        data.browser.selection.copy = event;
        set( par, 'Userdata', data );
    
    %--
    % update selection clipboard display from selection clipboard in browser
    %--
    else
        set_control( pal, 'clipboard_tag', 'string', event.tags );
        set_control( pal, 'clipboard_duration', 'value', event.duration );
        set_control( pal, 'clipboard_min_freq', 'value', event.freq( 1 ) );
        set_control( pal, 'clipboard_max_freq', 'value', event. freq( 2 ) );
        set_control( pal, 'clipboard_bandwidth', 'value', event.bandwidth );
    end    
end
