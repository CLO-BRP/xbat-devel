function [result, context] = compute(sound, parameter, context)

% DETECT - compute

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
%
% Change Log
%
% msp2 - 14 Nov 2012 - 
%
%  1. Maximum output log size
%     -Add feature that allows user to specify maximum output log size in
%     GUI.
%     -The output will be divided among multiple logs.
%     -Changes were required in an XBAT core function, detector_scan.
%
% msp2 - 27 Mar 2013
%
%  1. Allow log to be output to a path specified in parameter.out_path.
%     -Calling function must insure that paths in "sound" are current.
%      Otherwise a WARNING will be output to MATLAB Desktop and all the
%      sound samples input into detector scan will be set to 0.
%
% msp2 - 23 September 2013
%
%  1. Allow extension to be called from XBAT or from DM
%
%  2. Add dropdown list with log name tokens


result = [];

%--
% set flag if call is from XBAT
%--
stack = dbstack;
from_xbat = strcmp( stack( 2 ).file, 'perform_action.m' );


%--
% get detector and preset
%--

ext = get_extension( 'sound_detector', parameter.detector{ 1 } );

if from_xbat
  
  %load preset
  if isempty( parameter.preset )
    fprintf( 2, '\nWARNING: No preset selected\n' );
    return;
  end
  
  preset = preset_load( ext, parameter.preset{ 1 } );
  
  %override threshold in preset, if requested
  if parameter.threshold_flag
    preset.ext.parameter.thresh = context.ext.parameter.threshold;
  end
  
  chop_flag = parameter.chop_flag;
  
else
  
  %load preset
  preset_path = fullfile( parameter.preset_path, parameter.preset{ 1 } );
  preset = load( preset_path );
  preset = preset.preset;
  preset.name = parameter.preset{1};
  preset.ext.control.explain = 0;

  %override threshold in preset
  preset.ext.parameter.thresh = context.ext.parameter.threshold;
  ext.parameter = preset.ext.parameter;
  
  %if user requested maximum log size, set chop_flag
  chop_flag = ~isempty( parameter.max_num_events );
  
end
%--

%--
% turn off time stamps
%-- 

% NOTE: in the branch this is not necessary because scans happen in real
% time, but here it is because they still happen in record time.

sound.time_stamp = [];

%--
% create new log
%--
log_name = parameter.output;

log_name = token_replace( log_name, '%', ...
	'SOUND_NAME', sound_name( sound ), ...
	'PRESET_NAME', parameter.preset{ 1 }, ...
	'DETECTOR_NAME', ext.name, ...
	'TIME', datestr( now, 'yyyymmdd_HHMMSS' ) ...
);

if chop_flag  
  log_name = [ log_name, '_000001' ];
end

if from_xbat
  
  %create empty log
  log = new_log( log_name, context.user, context.library, sound );
  
else
  
  %create output directory, if needed
  if ~exist( parameter.out_path, 'dir' )
    mkdir( parameter.out_path )
  end
  
  %create empty log
  fn_full = fullfile( parameter.out_path, log_name );
  log = log_create( fn_full, 'sound', sound, 'author', 'DM' );
  
end
  
log.userdata.detect.max_num_events = parameter.max_num_events;

%--
% scan into log
%--

% NOTE: detector_scan saves the log so we don't need to

if from_xbat
  detector_scan( preset.ext, sound, [], [], log );
else
  detector_scan( ext, sound, [], [], log );
end

%--
% delete detector waitbar
%--

name = [ preset.ext.name, ' - ', sound_name( sound ) ];

pal = find_waitbar( name );

delete( pal );

%-----------------------------------------------
% TOKEN_REPLACE
%-----------------------------------------------

function str = token_replace( str, sep, varargin )

[ field, value ] = get_field_value( varargin );

for k = 1:length( field )
	
	tok = [ sep, field{k}, sep ];
	
	str = strrep( str, tok, value{ k } );
	
end
