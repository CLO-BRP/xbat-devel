function result = parameter__control__callback(callback, context)

% DETECT - parameter__control__callback

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 16 Sep 2013
%		Add control for adding string tokens to output log names.
%		Enable controls for threshold and max log size wtih checkboxes.
%	msp2 - 2 Nov 2013
%		Add support for Raven selection table output.
%	msp2 - 6 Mar 2013
%		Remove control for disabling threshold reset.
%		Update control for Window Parameter.
%   msp2 - 13 May 2014
%       Fix bug active when multiple presets selected.
%   msp2 - 14 May 2014
%       Add support for a unique identifier in log names.
%   msp2 - 15 May 2014
%       Add multithreading support.
%   msp2 - 19 May 2014
%       Disable multithreading controls if Parallel Processing toolbox is
%       not installed.
%   msp2 - 28 May 2014
%       Copy threshold, FFT parameters, and whiten filter state into GUI 
%         when loading presets.
%   msp2 - 3 June 2014
%       Make win_param invisible when not needed by win_type.
%   msp2 - 5 June 2014
%       Make threshold checkbox enable/disable threshold slider.
%       Make threads checkbox enable/disable threads slider.
%       Warn user if they attempt to enable multithreading without MATLAB
%         Parallel Processing Toolbox installed.
%       Add unique identifier to log name to avoid log name collisions if multithreading invoked.
%       Check for threshold checkbox state only when it is touched.
%   msp2 - 10 June 2014
%       Stop enabling filter order control when loading a preset with
%         Whiten filter engaged.  Filter should be set by preset only.
%       Stop enabling Window Parameter control.  Window Parameter should be
%         controlled by preset only.

result = [];
pal = callback.pal.handle;

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
switch callback.control.name
  case 'add_token'  
    %--
    % add token to log name
    %--
    if strcmp(callback.control.name, 'add_token')
      str = get_control(pal, 'output', 'string');
      value = get_control(pal, 'add_token', 'value');
      if strcmp(value{1}, 'Sound Name')
        value = '%SOUND_NAME%';
      elseif strcmp(value{1}, 'Detector Name')
        value = '%DETECTOR_NAME%';
      elseif strcmp(value{1}, 'Preset Name')
        value = '%PRESET_NAME%';
      elseif strcmp(value{1}, 'Time')
        value = '%TIME%';
      elseif strcmp(value{1}, 'Unique Identifier')
        value = '%UID%';
      end

      if isempty(str)
          str = value;
      else
          str = [str, '_', value];
      end

      set_control(pal, 'output', 'string', str);

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( pal, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( pal, 'OK', 'enable', 1 );
        else
           set_control( pal, 'OK', 'enable', 0 );
        end

        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( pal, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( pal, 'OK', 'enable', 0 );
        end

    end
    
	case 'detector'

        %--
        % get detector extension
        %--		
        detector = get_control(pal, 'detector', 'value');		
        ext = get_extensions('sound_detector', 'name', detector{1});

        %--
        % get preset names
        %--		
        presets = {};

        if ~isempty( ext )
            presets = get_preset_names( ext );
        end

        % NOTE: this may happen under various conditions		
        if isempty(presets)
            presets = { '(No Presets Found)' };
        end

        %--
        % unselect preset
        %--		
        handles = get_control( pal, 'preset', 'handles' );		
        set( handles.obj, 'string', presets, 'value', 1 );
        
        %--
        % disable OK button
        %--        
        set_control( pal, 'OK', 'enable', 0 );
		
	case 'preset'
		
		% TODO: this will update the preset info controls in the future

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( pal, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( pal, 'OK', 'enable', 1 );

            %--
            % get detector preset
            %--		
            detector = get_control(pal, 'detector', 'value');		
            ext = get_extensions('sound_detector', 'name', detector{1});
            preset = preset_load( ext, preset{ 1 } );
            
            %-----------------------------------------
            % Update threshold using preset
            %-----------------------------------------
            thresh = preset.ext.parameter.thresh;
            set_control( pal, 'threshold', 'value', thresh );
            
            %-----------------------------------------
            % Update specgram parameters using preset
            %-----------------------------------------
            param = preset.ext.parameter.specgram;
            
            % FFT Size
            set_control( pal, 'fft', 'value', param.fft );
            
            % FFT Advance
            set_control( pal, 'hop', 'value', param.hop );
            
            % FFT Window Type
            [ window_types, ~, def_ix ] = window_to_fun;
            ix = find( strcmpi( window_types, param.win_type ) );
            if isempty( ix )
                ix = def_ix;
            end
            win_type_h = findobj( pal, 'Tag', 'win_type', 'Style', 'popupmenu' );
            set( win_type_h, 'Value', ix )
                        
            % FFT Window Parameter
            update_win_param( pal );
            if ~isempty( param.win_param )
                set_control( pal, 'win_param', 'value', param.win_param );
            end
            
            % FFT Window Length
            set_control( pal, 'win_length', 'value', param.win_length );
            
            %-----------------------------------------
            %Whiten filter
            %-----------------------------------------
            try
                sf_ext = preset.active.signal_filter.ext;
            catch
                sf_ext = [];
            end
            if isfield( sf_ext, 'name' ) && strcmp( sf_ext.name, 'Whiten' )
                set_control( pal, 'filter_flag', 'value', 1 );
                set_control( pal, 'order', 'value', sf_ext.parameter.order );
%                 set_control( pal, 'order', 'enable', 1 );
            else
                set_control( pal, 'filter_flag', 'value', 0 );
%                 set_control( pal, 'order', 'enable', 0 );
            end           
        else
            
           % enable OK button
           set_control( pal, 'OK', 'enable', 0 );
        end
		
        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( pal, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( pal, 'OK', 'enable', 0 );
        end
        
    case 'filter_flag'
        filter_flag = get_control( pal, 'filter_flag', 'value' );
        if filter_flag
            set_control( pal, 'order', 'enable', 1 );            
        else
            set_control( pal, 'order', 'enable', 0 );
        end
		
	case 'output'

    % NOTE: consider having a type validation callback and extending validation

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( pal, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( pal, 'OK', 'enable', 1 );
        else
           set_control( pal, 'OK', 'enable', 0 );
        end
		
        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( pal, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( pal, 'OK', 'enable', 0 );
        end
        
    case 'output_type'  
        
        %--
        % ask user for output path if Raven selection table output selected
        %--
        output_type = get_control( pal, 'output_type', 'value' );

		if strcmp( output_type, 'selection table' )
            out_path = get_control( pal, 'out_path', 'string' );            
            out_path = uigetdir( out_path, 'Select folder for Raven selection tables');            
            if out_path == 0
                return;
            end
            set_control( pal, 'out_path', 'string', out_path );
            set_control( pal, 'out_path', 'enable', 1 );            
		else  %XBAT log
            set_control( pal, 'out_path', 'enable', 0 );
        end
        
	case 'win_type'
        update_win_param( pal );
        
    case 'multithread'

        % if Parallel Processing Toolbox not installed
        if isempty( ver( 'distcomp' ) )
            
            % disable slider and warn user
            set_control(pal, 'multithread', 'enable', 0);
            fail( 'Please install MATLAB Parallel Processing Toolbox to activate this control.' )
            
        % if Parallel Processing Tooxbox installed
        else
            
            % if user selects multithreading
            multithread = get_control(pal, 'multithread', 'value');
            if multithread
                
                % enable threads slider
                set_control(pal, 'poolsize', 'enable', 1);
        
                % add UID to output log name to avoid name collisions.
                str = get_control(pal, 'output', 'string');
                str = [str, '_%UID%' ];
                set_control(pal, 'output', 'string', str);
                
            else
                % disable threads slider
                set_control(pal, 'poolsize', 'enable', 0);
            end
        end
        
    case 'threshold_flag'
        
        %--
        % enable/disable threshold slider depending on state of checkbox
        %--
        threshold_flag = get_control(pal, 'threshold_flag', 'value');
        if threshold_flag
            set_control(pal, 'threshold', 'enable', 1);  
        else
            set_control(pal, 'threshold', 'enable', 0);
        end
        
    case 'chop_flag'

        %---
        % enable/disable maximum log size slider depending on state of checkbox
        %---
        chop_flag = get_control(pal, 'chop_flag', 'value');

        if chop_flag
            set_control(pal, 'max_num_events', 'enable', 1);    
        else
            set_control(pal, 'max_num_events', 'enable', 0);    
        end
end
%--
% update window parameter control
%--

function update_win_param( pal )

    win_type = get_control( pal, 'win_type', 'value');
    win_param = window_to_fun( win_type,'param' );
    if ~isempty( win_param )
%         set_control( pal, 'win_param', 'enable', 1 );
        handles = get_control( pal, 'win_param', 'handles' );
        set( handles.uicontrol.slider, 'min', win_param.min, 'max', win_param.max );
        set_control( pal, 'win_param', 'value', win_param.value );
        set_control( pal, 'win_param', 'label', win_param.name );
        set( handles.uicontrol.all, 'Tooltip', win_param.tip );
        set( handles.uicontrol.all, 'Visible', 'on' );
    else
%         set_control( pal, 'win_param', 'enable', 0 );
        handles = get_control( pal, 'win_param', 'handles' );
        hit_test = get( handles.uicontrol.all, 'HitTest' );
        set( handles.uicontrol.all, 'HitTest', 'on' )
        set( handles.uicontrol.all, 'Visible', 'off' );
        for i = 1 : length( hit_test )
            set( handles.uicontrol.all( i ), 'HitTest', hit_test{ i } )
        end
    end
    
