function [ g, flag ] = get_event_palette( par )

    flag = 0;
    g = get_palette(par, 'Event');		
    if isempty(g)
        flag = 1;
        g = browser_window_menu( par, 'Event' );

        %--
        % palette was recreated update last state if available
        %--
        if isempty( g )

            %--
            % update state of palette if available
            %--
            data = get( par, 'userdata' );
            if ~isempty(data.browser.palette_states)
                names = struct_field( data.browser.palette_states, 'name' );
                ix = find(strcmp( names, 'Event' ) );
                if ~isempty( ix )
                    set_palette_state( g, data.browser.palette_states( ix ) );
                end
            end
        end
    end
