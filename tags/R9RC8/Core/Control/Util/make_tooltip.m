function tooltip = make_tooltip( logname, mode_str, tag, dur, freq )
% make_tooltip: Makes tooltip with html tags for button in Mode palette
%
%   input
%       logname (char)
%       mode (char)
%       tag (char)
%       duration (double)
%       freq (1x2 double)
%   output
%       tooltip (char matrix)

% History
%   msp2 - 21 May 2014
%       Change input parameters for duration and frequency from string to double.
%   msp2 - 11 June 2014
%       When saving and opening presets, unset duration and frequency is
%         set to [] or '' rather than NaN.
%   msp2 - 12 June 2014
%       If duration and frequency are unset, put '' in tooltip rather than NaN.

    %--
    % Convert duration from double to string
    %--
    if isfinite( dur )
        dur = sprintf( '%.3f', dur );
    else
        dur = '';
    end
    
    %--
    % Convert frequency from double to string
    %--
    if all( isfinite( freq ) ) && isequal( size( freq ), [ 1, 2 ] )
        min_freq = sprintf( '%5.0f', freq( 1 ) );
        max_freq = sprintf( '%5.0f', freq( 2 ) );
    else
        min_freq = '';
        max_freq = '';
    end
    
    %--
    % Make tooltip string with embedded html tags
    %--
    tooltip = sprintf( ...
        [ '<html><pre><font face="courier new" size="4">' ...
            '<b>           Log: </b>%s <br />' ...
            '<b>          Mode: </b>%s <br />' ...
            '<b>           Tag: </b>%s <br />' ...
            '<b>  Duration (s): </b>%s <br />' ...
            '<b> Min Freq (Hz): </b>%s <br />' ...
            '<b> Max Freq (Hz): </b>%s' ...
          '</font>' ...
        ], ...
        logname, mode_str, tag, dur, min_freq, max_freq );
