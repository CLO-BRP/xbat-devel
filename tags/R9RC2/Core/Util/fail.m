function fail(txt, dlg_name)
% Displays text in warning dialog and in red font in MATLAB Desktop
%
% Input:
% ------
%	txt - string to display
%	dlg_name - title of warning dialog and text message to MATLAB Desktop

fprintf(2,'\n\n%s\n%s\n', dlg_name, txt);
h = warndlg(txt, dlg_name);
movegui(h, 'center')
