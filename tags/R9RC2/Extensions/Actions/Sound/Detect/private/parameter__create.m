function parameter = parameter__create(context)

% DETECT - parameter__create

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 24 Sep 2013
%		Support for Data Management system and more control of log file location and size
%	msp2 - 2 Nov 2013
%		Add support for Raven selection table output.
%	msp2 - 10 Mar 2013
%		Add support for whiten filter
% msp2 - 12 Mar 2013
%	Add controls for spectrogram parameters

parameter = struct;

parameter.detector = 'Data Template';

parameter.preset = '';

% parameter.threshold_flag = 0;

parameter.threshold = 0.4;

parameter.order = 512;

parameter.filter_flag = 0;

parameter.output_type = {''};

parameter.output_type_idx = 1;

parameter.chop_flag = 0;

parameter.max_num_events = 10000;

parameter.output = '';

parameter.out_path = pwd;


parameter.fft = 512;
parameter.hop = 0.5;
% parameter.hop_auto = 0;
% parameter.smoothing = 0;
parameter.win_type = 'Hann';
parameter.win_param = [];
parameter.win_length = 1;
