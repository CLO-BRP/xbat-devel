function control = parameter__control__create(parameter, context)

% DETECT - parameter__control__create

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

control = empty(control_create);

%--
% output log name
%--

str = { 'Sound Name', 'Detector Name', 'Preset Name', 'Time' };

control(end + 1) = control_create( ...
    'name', 'add_token', ...
    'alias', ' ', ...
    'style', 'popup', ...
    'align', 'right', ...
    'width', 0.4, ...
    'space', -0.75, ...
    'string', str, ...
    'value', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'output', ...
	'style', 'edit', ...
	'type', 'filename', ...
	'string', '%SOUND_NAME%_%PRESET_NAME%_%TIME%' ...
);

%--
% detector selection
%--

names = get_detector_names;

ix = find(strcmp(parameter.detector, names));

if isempty(ix) 
	ix = min(length(ix), 3);
end

% NOTE: the callback for this control updates the preset control

control(end + 1) = control_create( ...
	'name', 'detector', ...
	'width', 2/3, ...
	'style', 'popup', ...
	'string', names, ... 
	'onload', 1, ...
	'value', ix ...
);

%--
% preset selection
%--

control(end + 1) = control_create( ...
	'name', 'preset', ...
	'style', 'listbox', ...
	'space', 1.5, ...
	'max', 2, ...
	'lines', 3 ...
);

%-- pitz 16 September 2013

% Add maximum output log size CHECKBOX
control(end + 1) = control_create( ...
    'name', 'threshold_flag', ...
    'alias','Reset Threshold', ...
    'style', 'checkbox', ...
    'space', -0.75, ...
    'value', parameter.threshold_flag ...
    );

% Choose maximum output log size format slider
control(end + 1) = control_create( ...
    'name', 'threshold', ...
    'alias', ' ', ...
    'style', 'slider', ...
    'onload', 1, ...
    'space', 2, ...
    'align', 'right', ...
    'min', 0, ...
    'max', 1, ...
    'value', parameter.threshold ...
    );

%-- pitz 14 November 2012

% Add maximum output log size CHECKBOX
control(end + 1) = control_create( ...
    'name', 'chop_flag', ...
    'alias','Maximum Log Size', ...
    'style', 'checkbox', ...
    'space', -0.75, ...
    'value', parameter.chop_flag ...
    );

% Choose maximum output log size format slider
control(end + 1) = control_create( ...
    'name', 'max_num_events', ...
    'alias', ' ', ...
    'style', 'slider', ...
    'type', 'integer', ...
    'onload', 1, ...
    'space', 2, ...
    'align', 'right', ...
    'min', 1, ...
    'max', 100000, ...
    'value', parameter.max_num_events ...
    );
  
%-- end pitz




% TODO: add some controls to display preset info, include tags and notes?
