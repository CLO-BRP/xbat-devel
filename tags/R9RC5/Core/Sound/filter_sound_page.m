function page = filter_sound_page(page, context)

% filter_sound_page - filter a sound page using context
% -----------------------------------------------------
%
% page = filter_sound_page(page, context)
%
% Input:
% ------
%  page - page to filter
%  context - context
%
% Output:
% -------
%  page - filtered page

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 11 Mar 2014
%		Allow whiten filter to be invoked in the absence of a browser

% initializations
page.filtered = [];
if isempty( context )
	return;
end

%-----------------------------------------------------------------------
% switch between filtering for browser or not filtering for browser
%-----------------------------------------------------------------------
if ~isempty( context.active.signal_filter )
	
	% filter browser page
	context.page = page;
	page.filtered = apply_signal_filter(page.samples, context.active.signal_filter, context);
	
elseif isfield( context, 'log' ) ...
	&& isfield( context.log, 'userdata' ) ...
	&& isfield( context.log.userdata, 'Whiten' ) ...
	&& isfield( context.log.userdata.Whiten, 'filter_flag' )
		if context.log.userdata.Whiten.filter_flag
			page = whiten_filter( page, context );
		end
end

%--
function page = whiten_filter( page, context )

%initialiations
parameter.order = context.log.userdata.Whiten.order;
parameter.r = 0;

X = page.samples;

if isfloat(X)
	X = double(X);
end

Y = X;

form = get_form(X);


for i = 1:length(page.channels)
  
  %-----------------------------------------------------------------------
  %compile parameters for Whiten filter
  %-----------------------------------------------------------------------

  data = X(:,page.channels(i));
  
  %--
  % get model from data and update parameter filter
  %--

  % NOTE: this is the background model filter

  model.a = aryule(data, parameter.order);

  % NOTE: mitigate filtering if using regularization

  model.a = model.a .* ((1 - parameter.r) .^ (0:parameter.order));

  model.b = 1;

  % NOTE: our filter is actually the inverse filter

  parameter.filter.b = model.a;

  parameter.filter.a = model.b;
  
  %-----------------------------------------------------------------------
  %apply Whiten filter to page
  %-----------------------------------------------------------------------
  N = size(data, 1); 
  
  n = impzlength(parameter.filter.b, parameter.filter.a) - 1;
  
  data = [flipud(data(1:n, :)); data; flipud(data(end - n + 1:n, :))];
  
  data = fast_conv(data, parameter.filter.b);
  
  data = data(n + 1: n + N, :);  
  
  Y(:,i) = data;

end
	
%--
% check output form
%--

try
	Y = set_form(Y, form);
catch
	Y = X; extension_warning(ext, 'Incompatible output form.', lasterror);
end

page.filtered = Y;
