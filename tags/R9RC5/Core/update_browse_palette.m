function update_browse_palette( par, data, delete_flag )

% update_browse_palettes - update browse palette when logs change
% ----------------------------------------------------------------------
%
% pals = update_browse_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

% History
%   msp2 - 27 May 2014
%       Clean up code.
%   msp2 - 11 June 2014
%       When saving and opening presets, unset duration and frequency is
%         set to [] or '' rather than NaN.
%   msp2 - 12 June 2014
%       If log in tooltip is open, set in button color and ready state.

%--
% check browser handle input
%--
if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--
if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%--
% list open logs
%--
none = isempty(data.browser.log);
if none
	list = {'(No Open Logs)'}; 
%     ix = 1;
else
	list = file_ext(struct_field(data.browser.log, 'file')); 
%     ix = data.browser.log_active;
end
		
%--
% list uicontrols whose state needs updating
%--
control_name = { 'button_1', 'button_2', 'button_3', 'button_4', 'button_5', 'button_6' };

%--
% update uicontrols
%--
pal = get_xbat_figs('type', 'palette', 'parent', par);
name = get( pal, 'Name' );
idx = strcmp( name, 'Browse' );
pal = pal( idx );
if ~isempty( pal )
	for j = 1:length(control_name)
        
        %--
        % get control struct
        %--
        h_control = get_control( pal, control_name{ j }, 'handles' );
        if isempty( h_control )
            continue;
        end        

        %--
        % find log name currently in button tooltip
        %--
        h_button = h_control.obj;
        tooltip = get( h_button, 'TooltipString' );
        [ tooltip_log, mode_str, tag, dur, freq ] = parse_tooltip( tooltip );
        
        %--
        % find ready state of button
        %--
        S = get( h_button, 'Userdata' );
        ready = S.ready;
        
        %--
        % find index of tooltip log
        %--
        idx = strcmp( tooltip_log, list );

        %--
        % if tooltip log is open and button state is unready, set it to ready
        %--
        if ready && any( idx )

            % recover color for log associated with button
            log_color = data.browser.log( idx ).color;
            % choose color (black/white) which contrasts log color
            c_fore = ( sum( log_color ) <= 1.5 ) * ones( 1, 3 );

            % set button color and ready state
            set( h_button, ...
                'BackgroundColor', log_color, ...
                'ForegroundColor', c_fore, ...
                'UserData',        struct( 'ready', 1 ) )
            
        %--
        % if tooltip log is not open and button state is ready, set button to to unready state
        %--
        elseif ~isempty( tooltip_log ) && ~any( idx ) && delete_flag
            
            % reinitialize button
            tooltip_new = make_tooltip( '', '', '', [], [] );
            set( h_button, ...
                'TooltipString',   tooltip_new, ...
                'String',          '', ...
                'UserData',        struct( 'ready', 0 ), ...
                'BackgroundColor', get(0, 'DefaultFigureColor' ), ...
                'ForegroundColor', [ 0 0 0 ], ...
                'Cdata', [], ...
                'UserData',        struct( 'ready', 0 ) ...
            )            
        end
	end
end