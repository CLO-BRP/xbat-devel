function [result, context] = prepare(parameter, context)

% IMPORT_LOGS FROM DAYFOLDERS - prepare

result = struct;

%if context.state.kill = 1, then action will stop execution
context.state.kill = 0;

%set root path for source logs
ExtPath = fullfile( ...
  xbat_root, 'Extensions', 'Actions', 'Sound', context.ext.name, 'private', 'preset.mat');

if exist(ExtPath, 'file')
  
  load(ExtPath);
  
else
  
  RootPath = '';
  
end

NewRootPath = uigetdir(RootPath);

if isequal(NewRootPath, 0);
  
  %stop execution of compute.m
  context.state.kill = 1;
  
  return;
  
end

if ~strcmp(RootPath, NewRootPath)
  
  RootPath = NewRootPath;
  
  save(ExtPath, 'RootPath')
  
end

context.state.RootPath = RootPath;

%create empty field for log types
% context.state.SelectedTypes = '';

context.state.flag = 0;