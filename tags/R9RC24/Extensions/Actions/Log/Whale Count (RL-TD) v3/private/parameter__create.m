function parameter = parameter__create(context)

% WHALE COUNT (RL-TD) - parameter__create

parameter = struct;

parameter.SpeedLimit = 8;  %km/hr

parameter.latency = 60;     %minutes

parameter.TDtolerance = 0.1;  %seconds

parameter.TDlimit = 100;  %seconds

parameter.MinTrackLen = 1;

parameter.HourChop = 0;     %0/1

parameter.IndexChOnly = 1;   %0/1

parameter.check = 0;        %0/1

parameter.TrackSummary = 0;  %0/1

parameter.TrackDetails = 0; %0/1

parameter.csvTrack = 0;        %0/1

parameter.csvEvent = 0;        %0/1

parameter.plots = 0;        %0/1

parameter.LeftHour = 0;  %hours

parameter.RightHour = 24;%hours

