function [status, target, NumLogs, order_flag] = list_logs(context, parameter)

target = {''};

NumLogs = 0;

order_flag = [];


%% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  
  target = context.target;
  
else
  
  fprintf(2,'API supplied no context.target in prepare.m \n');
  
  status = 1;
  
  return;
  
end

if ~iscell(target)
  
  target = {target};
  
end

NumLogs = length(target);


%% test that logs are in matching pairs

%return if even number of logs is not selected
if mod(NumLogs, 2)
  
  txt = sprintf('Action terminated.\nOdd number of logs selected.\nSelect truth/test log pairs and try again.');
  
  fprintf(2,'ERROR:\n%s\n\n', txt);
  
  h = warndlg(txt, 'ERROR');
  
  movegui(h, 'center')
  
  status = 1;
  
  return;
  
end

%determine whether test or truth log come first in pair
[~, fn1] = fileparts(target{1});

[~, fn2] = fileparts(target{2});

if any(strfind(fn1, parameter.test_suffix)) && any(strfind(fn2, parameter.truth_suffix))
  
  order_flag = 0;
  
  status = 0;
  
elseif any(strfind(fn2, parameter.test_suffix)) && any(strfind(fn1, parameter.truth_suffix))

  order_flag = 1;
  
  status = 0;
  
else
  
  txt = sprintf('Action terminated.\nSelect logs with unique truth and\ntest identifiers in log name and try again.');
  
  fprintf(2,'ERROR:\n%s\n\n', txt);
  
  h = warndlg(txt, 'ERROR');
  
  movegui(h, 'center')
  
  status = 1;
  
end
