function [result, context] = conclude(parameter, context)

% LOCATION_V2P3_MEASUREMENT_EXPORTER - conclude

result = struct;

if context.state.kill
  return;
end

xlswrite(context.state.OutPath, context.state.M_summary, 'SUMMARY');