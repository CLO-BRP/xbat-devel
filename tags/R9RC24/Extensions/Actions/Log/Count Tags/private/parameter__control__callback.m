function result = parameter__control__callback(callback, context)

% ADD EVENT MEASUREMENTS - parameter__control__callback

result = struct;

%-------------------------------------------------------------------------
% load saved user parameter settings
%-------------------------------------------------------------------------

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

%load prest
load( preset_path )

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
name = callback.control.name;
style = get( callback.control.handle, 'style' );
	
if any( strcmp(style, { 'checkbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name ' = value;' ] );

% elseif any( strcmp( style, { 'popupmenu' 'listbox' } ) )
%   value = get( callback.control.handle, 'Value' );
%   eval( [ 'parameter.' name '_idx = value;' ] );
  
elseif any( strcmp( style, { 'edit' } ) )
  value = get( callback.control.handle, 'String' );
  eval( [ 'parameter.' name ' = value;' ] );  
end

%-------------------------------------------------------------------------
% Browse button modified edit control
%-------------------------------------------------------------------------
if strcmp( callback.control.name, 'browse' )
  out_dir = get_control( callback.pal.handle, 'out_dir', 'value' );
  out_dir = uigetdir( out_dir );  
  if out_dir == 0    
    return;    
  end
  set_control( callback.pal.handle, 'out_dir', 'value', out_dir );
  parameter.out_dir = out_dir;
else  
  out_dir = context.ext.parameter.out_dir;
end

%-------------------------------------------------------------------------
% set tooltip for Output Directory
%-------------------------------------------------------------------------
control = get_control( callback.pal.handle, 'out_dir' );
if isempty( control )
  return;
end
handles = control.handles.all;
for k = 1:length( handles )
  style = get( handles( k ), 'style' );
  if isequal( style, 'edit' )
    set( handles( k ), 'TooltipString', out_dir );
  end
end

%-------------------------------------------------------------------------
% enable/disable output path controls
%-------------------------------------------------------------------------
csv = get_control( callback.pal.handle, 'xlsx', 'value' );
set_control( callback.pal.handle, 'out_dir', 'enable', csv );   
set_control( callback.pal.handle, 'browse', 'enable', csv );

%-------------------------------------------------------------------------
% load saved user parameter settings
%-------------------------------------------------------------------------
save( preset_path, 'parameter' );