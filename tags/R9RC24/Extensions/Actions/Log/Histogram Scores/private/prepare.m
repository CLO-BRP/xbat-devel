function [result, context] = prepare(parameter, context)

% HISTOGRAM XC NORM - prepare

result = struct;


ScoreVector = [];
channel = [];
MaxScoreVector = [];


%% wrapper for compute.m

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
%   kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  log = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ~(ischar(SoundName) && ischar(fn) && isequal(length(log),1)    )
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
%     kill = 1;
    return;
  end
    
    % rename file field in log
    fnExt = [fn '.mat'];
    log.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    log.path = LogPath;


  %% compute.m

  len = log.length;
  fn = log.file;
  fn = fn(1:end-4);
  curr_ScoreVector = zeros(1,len);
  curr_channel = zeros(1,len);
  curr_MaxScoreVector = zeros(1,len);
  CurrentTag = log.event(1).tags;
  if ~iscell(CurrentTag); CurrentTag = {CurrentTag}; end;
  MaxScore = -100;
  k = 0;
  j = 0;

  for i=1:len

    ID = log.event(i).id;
    score = log.event(i).score;

    if ~isempty(score)
      k = k + 1;
      curr_ScoreVector(k) = score;
      curr_channel(k) = log.event(i).channel;

      %if requested, compile vector of maximum score for each tag
      if parameter.MaxScore
        LastTag = CurrentTag;
        CurrentTag = log.event(i).tags{1};
        if ~iscell(CurrentTag); CurrentTag = {CurrentTag}; end;
        if strcmp(CurrentTag, LastTag)
          MaxScore = max(MaxScore, score);
        else
          j = j + 1;
          curr_MaxScoreVector(j) = MaxScore;
          MaxScore = score;    
        end
      end

      %display events with score zero or less
      if score <= 0
        fprintf(2, 'Score = %.4f,  ID = %5.0f,  Log = %s\n', score, ID, fn);
      end

    %display events with missing scores
    else
      fprintf(2, 'Score = %s,  ID = %5.0f,  Log = %s\n', '   n/a', ID, fn);
    end
  end


  %max score for last tag into curr_MaxScoreVector
  if parameter.MaxScore && strcmp(CurrentTag, LastTag)
    j = j + 1;
    curr_MaxScoreVector(j) = MaxScore;
  end


  %append score vector for log to score vector for all selected logs
  ScoreVector = [ScoreVector curr_ScoreVector(1:k)];
  channel = [channel curr_channel(1:k)];
  MaxScoreVector = [MaxScoreVector curr_MaxScoreVector(1:j)];

 
end


%% conclude

% HISTOGRAM SCORES - conclude

%initializations
% ScoreVector = ScoreVector;
% MaxScoreVector = MaxScoreVector;
len = length(ScoreVector);

inc = parameter.inc;

%% Report Summary Stats for Score
fprintf(1,'SUMMARY STATS FOR SCORE\n');
fprintf(1,'Mean Score: %f\n', mean(ScoreVector));
fprintf(1,'Median Score: %f\n', median(ScoreVector));
fprintf(1,'Score Variance: %f\n\n', var(ScoreVector));
fprintf(1,'Min Score: %f\n\n', min(ScoreVector));
fprintf(1,'Max Score: %f\n\n', max(ScoreVector));

%% HISTOGRAM OF Score Frequency


%define centers of bars on histogram
MinScore = floor(min(ScoreVector));
MaxScore = ceil(max(ScoreVector));

x = MinScore+inc/2:inc:MaxScore-inc/2;

% save('test')

%plot histogram
h1 = figure;
set(h1,'Name','Score Frequency','MenuBar','figure')
[n, xout] = hist(ScoreVector,x);
bar(xout,n)
cum_n = cumsum(n(end:-1:1));
cum_n = cum_n(end:-1:1);

fprintf('\nScore, Freq, Cum Freq\n');

for i = length(n):-1:1
% for i = 1:length(n)
  
  fprintf('%.3f,%10.0f,%12.0f\n',xout(i)-inc/2,n(i),cum_n(i));

end


%% HISTOGRAM OF Square Root Score Frequency

if parameter.SqrtScore

  SqrtScoreVector = ...
    (ScoreVector>=0).*sqrt(ScoreVector)-(ScoreVector<0).*sqrt(-ScoreVector);

  SqrtMinScore = min(SqrtScoreVector);
  SqrtMaxScore = max(SqrtScoreVector);

  x2 = SqrtMinScore+0.005:0.01:SqrtMaxScore-0.0005;

  %plot histogram of square root of cross correlation
  h2 = figure;
  hist(SqrtScoreVector, x2)
  set(h2, 'Name','Square Root Score Frequency','MenuBar','figure')
end

%% HISTOGRAM OF MAXIMUM SCORE FOR EACH EVENT TAG (tags must be consecutive in log!

if parameter.MaxScore
  h3 = figure;
  hist(MaxScoreVector, x);
  set(h3,'Name','Frequency for Maximum Score for Each Tag','MenuBar','figure');
end


%% SINGLE-CHANNEL HISTOGRAMS

if parameter.SeparateChannels
%   channel = channel;
  NumChans = max(channel);
  
  ScoreVectorChannel = zeros(NumChans, len);
  SqrtScoreVectorChannel = ScoreVectorChannel;
  ChannelLength = zeros(NumChans);
  
  for i=1:len
    ChannelLength(channel(i)) = ChannelLength(channel(i)) + 1;
    ScoreVectorChannel(channel(i),ChannelLength(channel(i))) = ScoreVector(i);
    SqrtScoreVectorChannel(channel(i),ChannelLength(channel(i))) = SqrtScoreVector(i);
  end
  
  for i = 1:NumChans
    h4 = figure;
    set(h4,'Name',sprintf('Score Frequency for Channel %.0f', i),'MenuBar','figure');
    hist(ScoreVectorChannel(i,1:ChannelLength(i)), x)
  end

  if parameter.SqrtScore
    for i = 1:NumChans
      h5 = figure;
      set(h5,'Name',sprintf('Square Root Score Frequency for Channel %.0f', i),'MenuBar','figure');
      hist(SqrtScoreVectorChannel(i,1:ChannelLength(i)), x2)
    end
  end    
end

%% output frequencies and bin centers, if requested
if parameter.OutputFrequencies
  [ScoreFreq ScoreBin] = hist(ScoreVector,x);
  [MaxScoreFreq MaxScoreBin] = hist(MaxScoreVector,x);
  save('HistogramScores', ...
       'ScoreVector', ...
       'ScoreFreq', ...
       'ScoreBin', ...
       'MaxScoreVector', ...
       'MaxScoreFreq', ...
       'MaxScoreBin');
end

%%
error('This is the one error you can ignore!')
