function parameter = parameter__create(context)

% SET PARAMETERS - parameter__create

%% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 

% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

%%

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

% load preset, if readible                    
try    
  load(preset_path)
  
  %warn user if user parameters have changed
  warn_parameter_change( parameter, preset_path)

% if preset not readible, create one
catch
  parameter = make_parameter;
  save(preset_path,'parameter');
  fail( 'preset.mat initialized because it is missing or corrupt.', 'WARNING' )
end


function warn_parameter_change( parameter, preset_path)
%warn user if parameter has changed since last invocation

%re-initialize preset.mat if parameter fields have changed
newfields = setdiff( fieldnames( make_parameter ), fieldnames( parameter ) );
if ~isempty( newfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been added to GUI.';
  d2 = sprintf( '   %s\n', newfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end

oldfields = setdiff( fieldnames( parameter ), fieldnames( make_parameter ) );
if  ~isempty( oldfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been removed from GUI.';
  d2 = sprintf( '   %s\n', oldfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end


function parameter = make_parameter
%initialize parameter if extension has never been invoked

parameter.fft = 2048;
parameter.hop = 0.25;
parameter.hop_auto = 0;
parameter.win_type = 'Hann';
parameter.win_param = [];
parameter.win_length = 1;

parameter.channels = [];
parameter.duration = 360;
parameter.overlap = 0;
parameter.minfreq = 0;
parameter.maxfreq = [];

parameter.ColorName = '';
parameter.invert = 1;
parameter.auto_scale = 0;
parameter.brightness = 0.6;
parameter.contrast = 0.7;

parameter.TimeSpacing = 1;
parameter.TimeLabels = '';

parameter.FileBoundaries = 0;
parameter.FileLabels = 0;

parameter.TimeStampEnable = 1;
parameter.TimeStampCollapse = 1;

units_orig = get( 0, 'Units' );
set( 0, 'Units', 'Inches' )
ScreenSize = get( 0, 'ScreenSize' );
parameter.ScreenSize = ScreenSize;
set( 0, 'Unit', units_orig )

parameter.p1 = ScreenSize( 1 );
parameter.p2 = ScreenSize( 2 );
parameter.p3 = ScreenSize( 1 ) + ScreenSize( 3 );
parameter.p4 = ScreenSize( 2 ) + ScreenSize( 4 );

parameter.position_enable = 0;

parameter.oneclick_enable = 0;
parameter.event_duration = 0.2;
parameter.event_minfreq  = 0;
parameter.event_maxfreq = [];
parameter.event_tag = '';
