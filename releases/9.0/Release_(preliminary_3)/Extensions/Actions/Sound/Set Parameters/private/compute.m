function [result, context] = compute(sound, parameter, context)
result = struct;

% SET PARAMETERS - compute

% Sound Action allows user to set spectrogram parameters and page duration
% in preparation for running the Data Template Detector.

%% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 

% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

% Terminate if previous failure
if context.state.kill  
  return;  
end

% Terminate if sound is open
SoundName=sound_name(sound);
if sound_is_open(sound)  
  fprintf('\n%s is open, so parameters were not altered. Please close and try again.', SoundName);
  context.state.kill = 1;  
  return;  
end

% Set spectrogram parameters
sound.specgram.fft = parameter.fft;
sound.specgram.hop_auto = parameter.hop_auto;
sound.specgram.hop = parameter.hop;
sound.specgram.win_type = parameter.win_type;
sound.specgram.win_param = parameter.win_param;
sound.specgram.win_length = parameter.win_length;

% Set page parameters
channel_binary = zeros(max(sound.channels, length(parameter.channels)) ,1);

for i = 1:length(parameter.channels)
  
  channel = str2double(strrep(parameter.channels{i},'Channel ', ''));
  
  channel_binary(channel) = 1;
  
end

channel_binary = channel_binary(1:sound.channels);

channel_list = (1:sound.channels)';

sound.view.channels = [channel_list , channel_binary];

% if isempty(sound.view.time)
  
  sound.view.time = 0;
  
% end

sound.view.page.duration = parameter.duration;
sound.view.page.overlap = parameter.overlap;

maxfreq = min(parameter.maxfreq, 0.5 * sound.samplerate);
minfreq = parameter.minfreq;

if minfreq>=maxfreq
  fn = sound_name(sound);
  fprintf(2,'\nMinimum frequency set at or above maximum frequency.\n');
  fprintf(2, 'No change made in state of %s.\n\n', fn);
  return;
end

% Set colormap parameters
if iscell(parameter.ColorName)
  sound.view.colormap.name = lower(parameter.ColorName{1});
else
  sound.view.colormap.name = lower(parameter.ColorName);
end

sound.view.colormap.invert = parameter.invert;
sound.view.colormap.auto_scale = parameter.auto_scale;
sound.view.colormap.brightness = parameter.brightness;
sound.view.colormap.contrast = parameter.contrast;

%set grid parameters
sound.view.grid.time.spacing = parameter.TimeSpacing;

if iscell(parameter.TimeLabels)
  sound.view.grid.time.labels = lower(parameter.TimeLabels{1});
else

  sound.view.grid.time.labels = lower(parameter.TimeLabels);
end


sound.view.grid.file.on = parameter.FileBoundaries;
sound.view.grid.file.labels = parameter.FileLabels;

%   if isfield(sound.time_stamp, 'enable')
%       sound.time_stamp.enable = parameter.TimeStampEnable;
%       sound.time_stamp.collapse = parameter.TimeStampCollapse;
%   else
%       fprintf(2,'\n%s\n', sound_name(sound));
%       fprintf(2,'  Hide Silences not enabled because Time-Stamps attribute not set\n\n');
%   end

%-------------------------------------------------------------------------
% Set sound state if not yet intialized, or if user requested it
%-------------------------------------------------------------------------

%find current soundstate
sound.view.page.freq = [minfreq maxfreq];
lib = get_active_library;
file = get_library_sound_file(lib, SoundName);
state = get_sound_state(sound,lib);
  
  %set initial browser sound state if uninitialized
if ~isstruct(state)
  state.screensize = get( 0, 'ScreenSize' );
  state.position = [100 100 900 900];
  state.palette = [];
  state.palette_states = [];
  state.log = [];
end

%reposition browser, if requested
if parameter.position_enable
  state.position = context.state.position;
end

%set size of selection scrapbook for "One-click Mode', if requested
if parameter.oneclick_enable
	event = event_create;
	event = set_tags( event, parameter.event_tag );
	event.time = [ 0, parameter.event_duration ];
	event.freq = [ parameter.event_minfreq, parameter.event_maxfreq ];
	event.duration = parameter.event_duration;
	event.bandwidth = diff( event.freq );
	event.level = 1;
	
	state.selection.on = 1;
	state.selection.mode = 2;
	state.selection.zoom = 0;
	state.selection.event = struct( zeros( 1, 0 ) );
	state.selection.log = [];
	state.selection.handle = [];
	state.selection.color = [ 1 0 0.05 ];
	state.selection.linestyle = '-';
	state.selection.linewidth = 1;
	state.selection.patch = 0;
	state.selection.grid = 1;
	state.selection.label = 0;
	state.selection.control = 1;
	state.selection.copy = event;
end

save(file, 'sound', 'state');
