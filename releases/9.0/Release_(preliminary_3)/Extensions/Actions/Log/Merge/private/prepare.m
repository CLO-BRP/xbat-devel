function [result, context] = prepare(parameter, context)
% EXAMPLE LOG ACTION - prepare

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prepare.m contents here
result = struct;
tstart = tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Stop if any open browsers
if close_browsers
    return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
    fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
    return;
end

% for each log
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );
NumLogs = length( logs );
k = 0;

%retrieve list of sound from which logs were selected
sound_names = cellfun( @fileparts, logs, 'UniformOutput', false );
unique_snd_names = unique( sound_names );

%loop through XBAT sounds
for snd_idx = 1:length( unique_snd_names )
    curr_sound_name = unique_snd_names{ snd_idx };
    out = sound_load( context.library, curr_sound_name );
    snd = out.sound;
    mask = strcmp( sound_names,  curr_sound_name );
    curr_logs = logs( mask );
    event = [];
    len = 0;

    for i = 1:length( curr_logs )

        t0 = tic;

        %indicate log being processed in waitbar
        k = k + 1;
        waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ k }, ' ...'] );

        %load and refresh path in log
        [ log, ~, status ] = load_log( curr_logs{ i }, context );
        if status
        return;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % compute.m contents here

        %add events in current log to pool of combined events
        event = [ event, log.event ];

        %add number of events in current log to running total
        len = len + log.length;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %indicate log processed in waitbar
        result.target = log;
        result.status = 'done';
        result.message = '';
        result.error = [];
        result.elapsed = toc( t0 );
        result.output = struct( [] );
        action_waitbar_update( wh(1), k/NumLogs, result, 'log' );
    end
    
    % create name for merged log
    fn =  [ curr_sound_name, '_merged' ];

    % create new empty log
    MergedLog = new_log( fn, context.user, context.library, snd );

    %reset event id to sequence number
    for i = 1:len
        event( i ).id = i;
    end

    MergedLog.event = event;
    MergedLog.length = len;
    MergedLog.curr_id = len + 1;

    %save 'MergedLog.mat'
    log_save( MergedLog );

end

d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc( tstart ) );
waitbar_update( wh( 1 ), 'PROGRESS', 'message', d );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% conclude.m contents here




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m
