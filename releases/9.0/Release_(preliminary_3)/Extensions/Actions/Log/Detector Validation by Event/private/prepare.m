function [result, context] = prepare(parameter, context)
% Detector Validation by Event - prepare

%initialization
result = struct;
tstart = tic;

%stop if any open browsers
if close_browsers
    return;
end

%stop if get_schedule_from_files_export not available
if ~helper_available
    return;
end

%-------------------------------------------------------------------------
%process selected sounds
%-------------------------------------------------------------------------
logs = get_logs( context );

[ snd_names, unique_snd_names ] = find_sound_names( logs );
wh = waitbar2( context );

%loop initialization
NumLogs = length( logs );
log_names = {};
M_cum = [];
M_MARKER_cum = [];
snd_MARKER_cum = {};
tags_cum = {};
count_cum = [];
snd_duration_cum = 0;
count_vector = [];
k = 0;

MARKER_flag = ~isempty( parameter.MARKER_tag );
xls_snd_ROC = sprintf( '%s_SoundROC_%s.xlsx', context.library.name, datestr( now, 'yyyymmdd_HHMMSSFFF' ) );
xls_snd_ROC = fullfile( parameter.out_dir, xls_snd_ROC );
xls_summary = sprintf( '%s_SoundSummary_%s.xlsx', context.library.name, datestr( now, 'yyyymmdd_HHMMSSFFF' ) );
xls_summary = fullfile( parameter.out_dir, xls_summary );
xls_time_ROC = sprintf( '%s_MarkerROC_%s.xlsx', context.library.name, datestr( now, 'yyyymmdd_HHMMSSFFF' ) );
xls_time_ROC = fullfile( parameter.out_dir, xls_time_ROC );

%-------------------------------------------------------------------------
%loop through XBAT sounds
%-------------------------------------------------------------------------
for i = 1:length( unique_snd_names )
    
    % initialize sound struct
    curr_snd_name = unique_snd_names( i );
    snd = make_snd( unique_snd_names, i, context );
    snd = PRBA_set_date_time( snd );
    snd = PRBA_set_time_stamps( snd );
    context.sound = snd;
    
    % other initializations
    M_snd = [];
    M_MARKER_snd = [];
    tags_snd = {};
    notes_snd = [];
    log_names_snd = {};
    
    % list logs corresponding to current sound
    curr_logs = filter_logs_by_sound( logs, snd_names, curr_snd_name{ 1 } );

    %----------------------------------------------------------------------
    %loop through selected logs corresponding to current sound
    %----------------------------------------------------------------------
    for j = 1:length( curr_logs )

        %update waitbar
        [ t0, k ] = waitbar2( wh, logs, k );

        %load log, refresh file name and path
        log = load_log( curr_logs{ j }, context );
        
        %find log name
        fn = log_name( log );
        
        %Make matrix with event time, freq, and score and cell vector with tags
        [ M, tags, notes, parameter ] = extract_events( log, parameter );
        
        %concatenate events
        M_snd = [ M_snd ; M ];
        tags_snd  = [ tags_snd  ; tags ];
        notes_snd = [ notes_snd ; notes ];
        
        %add log name to list
        log_names_snd = [ log_names_snd ; { fn } ];
        
        %update waitbar
        waitbar2( log, t0, wh, k, NumLogs )
    end
    
    %add MATLAB datenum indicating date and time of begin time of events
    if parameter.xls_MARKER_ROC_flag
        M_snd = add_realtime( M_snd, snd );
    end
        
    %Filter events by MARKER event times
    if MARKER_flag
        [ M_snd, M_MARKER_snd, tags_snd, notes_snd ] = filter_by_MARKER_time( M_snd, tags_snd, notes_snd, parameter.MARKER_tag, curr_snd_name );
    end
    
    %compare truth events to test events
    [ count, matches ] = compare_sets( M_snd, tags_snd, notes_snd, parameter );
    
    %set sound duration
    snd_duration = cum_duration( M_snd, tags_snd, M_MARKER_snd, context, parameter );
    snd_duration_cum = snd_duration_cum + snd_duration;
    
    %output Excel worksheets
    if parameter.xls_sound_ROC_flag
        sheet_name = sprintf( 'Sound #%.0f', i );
        output_xls( count, curr_snd_name, snd_duration, log_names_snd, xls_snd_ROC, parameter, sheet_name )
    end
        
    %output annotated logs
    if parameter.log_flag
        output_log( matches, context, parameter )
    end
    
    %concatenate with cumulative matrices
    M_cum = [ M_cum ; M_snd ];
    M_MARKER_cum = [ M_MARKER_cum ; M_MARKER_snd ];
    snd_MARKER( 1:size( M_MARKER_snd, 1 ), 1 ) = curr_snd_name;
    snd_MARKER_cum = [ snd_MARKER_cum ; snd_MARKER ];
    tags_cum = [ tags_cum ; tags_snd ];
      
    %increment cumulative stats for sounds
    count_cum = sum_fields( count_cum, count );
    count.sound_duration = snd_duration;
    count_vector = [ count_vector , count ];
    
    %add log names from current sound to list of all log names
    log_names = [ log_names ; log_names_snd ];
end

%summary output
if parameter.xls_sound_ROC_flag
	output_xls( count_cum, unique_snd_names, snd_duration_cum, log_names, xls_snd_ROC, parameter, 'Mean ROC' )
end
if parameter.xls_summary_flag
	output_xls( count_vector, unique_snd_names, snd_duration_cum, log_names, xls_summary, parameter, 'Sound Summary' )
end
    
if parameter.xls_MARKER_ROC_flag
	output_MARKER_ROC_xls( M_cum, M_MARKER_cum, tags_cum, snd_MARKER_cum, snd_duration_cum, log_names, xls_time_ROC, parameter )
end

%-------------------------------------------------------------------------
%update waitbar
%-------------------------------------------------------------------------
d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc( tstart ) );
waitbar_update( wh( 1 ), 'PROGRESS', 'message', d );

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m


%--------------------------------------------------------------------------
function status = helper_available
               
status = exist( 'get_schedule_from_files_export.m', 'file' );
if ~status
    fail( 'Install "Export Gateway" log action', 'ERROR' )
    return;
end

%--------------------------------------------------------------------------
function[ snd_names, unique_snd_names ] = find_sound_names( logs )

    snd_names = cellfun( @fileparts, logs, 'UniformOutput', false );
    unique_snd_names = unique( snd_names );

%--------------------------------------------------------------------------
function varargout = waitbar2( varargin )

    switch nargin
        case 1
            context = varargin{ 1 };
            waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
            wh = findobj( 'tag', waitbar_name );
            varargout = { wh };
        case 3
            [ wh, logs, k ] = varargin{ : };
            t0 = tic;
            k = k + 1;
            waitbar_update( wh( 1 ), 'PROGRESS', 'message', ['Processing ', logs{ k }, ' ...'] );
            varargout = { t0, k };
        case 5
            [ log, t0, wh, k, NumLogs ] = varargin{ : };
            result.target = log;
            result.status = 'done';
            result.message = '';
            result.error = [];
            result.elapsed = toc( t0 );
            result.output = struct( [] );
            action_waitbar_update( wh( 1 ), k / NumLogs, result, 'log' );
    end
        
%--------------------------------------------------------------------------
function snd = make_snd( unique_snd_names, snd_idx, context  )

    curr_sound_name = unique_snd_names{ snd_idx };
    out = sound_load( context.library, curr_sound_name );
    snd = out.sound;

%--------------------------------------------------------------------------
function curr_logs = filter_logs_by_sound( logs, snd_names, curr_sound_name )

    mask = strcmp( snd_names,  curr_sound_name );
    curr_logs = logs( mask );

%--------------------------------------------------------------------------
function M = add_realtime( M, snd )

%map event begin times to a MATLAB datenum
t = M( :, 1 );
t_real = map_time( snd, 'real', 'record', t ) ./ 86400 + snd.realtime;

%concatenate datenum to M as column 7
M = [ M , t_real ];