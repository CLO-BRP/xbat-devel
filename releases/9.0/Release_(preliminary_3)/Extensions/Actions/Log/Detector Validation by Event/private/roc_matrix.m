function M = roc_matrix( count, sound_duration, parameter )

%if no negative truth events (TN not defined explicitly)
if isempty( parameter.neg_truth_tag )  

	threshold_vector = 0 : 0.05 : 1;  
    num_thresholds = length( threshold_vector );
	M = cell( num_thresholds, 13 ); 
    for i = 1:num_thresholds
        
        threshold = threshold_vector( i );
        
        %calculate TP and FN for truth logs, TP and FP for test logs
        TP_truth = sum( count.P_truth_score >= threshold  );
        FN_truth = count.FN_truth + count.TP_truth - TP_truth;
        TP_test = sum( count.TP_test_score >= threshold  );
        FP_test = sum( count.FP_test_score >= threshold  );
  
        %find confidence intervals for TPR
        [tpr, ci] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );  
        
        %make data for ROC table
        M{ i, 1 }  = threshold; 
        M{ i, 2 }  = tpr;
        M{ i, 3 }  = ci( 1 );
        M{ i, 4 }  = ci( 2 );
        M{ i, 5 }  = TP_truth / ( TP_truth + FN_truth );
        M{ i, 6 }  = FP_test / ( sound_duration / 3600 );
        M{ i, 7 }  = TP_test / ( TP_test + FP_test );
        M{ i, 8 }  = TP_truth;
        M{ i, 9 }  = FN_truth;
        M{ i, 10 } = TP_truth + FN_truth;
        M{ i, 11 } = TP_test;
        M{ i, 12 } = FP_test;
        M{ i, 13 } = TP_test + FP_test;
    end
  
%if negative truth events ( TN defined explicitly )  
else
	threshold_vector = 0 : 0.05 : 1;  
    num_thresholds = length( threshold_vector );
	M = cell( num_thresholds, 20 );  
	for i = 1:num_thresholds
        
        threshold = threshold_vector( i );
    
        %calculate TP, FN, FP, and TN for truth logs, TP and FP for test logs
        TP_truth = sum( count.P_truth_score >= threshold  );
        FN_truth = count.FN_truth + count.TP_truth - TP_truth;
        FP_truth = sum( count.N_truth_score >= threshold  );
        TN_truth = count.TN_truth + count.FP_truth - FP_truth; 
        TP_test = sum( count.TP_test_score >= threshold  );
        FP_test = sum( count.FP_test_score >= threshold  );

        %find confidence intervals for TPR
        [ tpr, ci ] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );

        %make data for ROC table
        M{ i, 1 }  = threshold;  
        M{ i, 2 }  = tpr;
        M{ i, 3 }  = ci( 1 );
        M{ i, 4 }  = ci( 2 );
        M{ i, 5 }  = TP_truth / ( TP_truth + FN_truth );  %TPR
        M{ i, 6 }  = FP_truth / ( FP_truth + TN_truth );  %FPR
        
        %FP/hr - count only sound duration in negative truth events
        M{ i, 7 }  = FP_test / ( sum( sound_duration ) / 3600 );
        M{ i, 8 }  = TP_test / ( TP_test + FP_test );     %PPV
        M{ i, 9 }  = TP_truth;
        M{ i, 10 } = FN_truth;
        M{ i, 11 } = TP_truth + FN_truth;
        M{ i, 12 } = FP_truth;
        M{ i, 13 } = TN_truth;
        M{ i, 14 } = FP_truth + TN_truth;
        M{ i, 15 } = TP_truth + FN_truth + FP_truth + TN_truth;
        M{ i, 16 } = TP_test;
        M{ i, 17 } = FP_test;
        M{ i, 18 } = TP_test + FP_test;
	end
end