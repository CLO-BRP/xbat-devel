function sound_duration = cum_duration( M, tags, M_MARKER_cum, context, parameter )

%if no negative truth events
if isempty( parameter.neg_truth_tag )

    %if no MARKER events, sound duration is entire recording duration for sound
    if isempty( parameter.MARKER_tag )
        sound_duration = context.sound.duration;

    %if MARKER events, sound duration is some of durations of MARKER events
    else
        time = M_MARKER_cum( :, 1:2 );
        sound_duration = sum( diff( time, 1, 2 ) );

        % make sure that MARKER events aren't overlapping
        [ ~, idx ] = sort( time( :, 1 ) );
        time = time( idx, : );
        m = size( time, 1 );

        for i = 2:m
            if time( i - 1, 2 ) > time( i, 1 )
                d = sprintf( 'Overlapping MARKER events found for\n  %s', ...
                    sound_name( context.sound ) );
                fail( d, 'WARNING' )
                error( 'No error' )
            end
        end
    end
    
%negative truth events, sound duration is sum of truth event durations
else
    mask_pos_truth = strcmp( tags, parameter.pos_truth_tag );
    time_pos_truth = M( mask_pos_truth, 1:2 );
    duration_pos_truth = sum( diff( time_pos_truth, 1, 2 ) );
    
    mask_neg_truth = strcmp( tags, parameter.neg_truth_tag );
    time_neg_truth = M( mask_neg_truth, 1:2 );
    duration_neg_truth = sum( diff( time_neg_truth, 1, 2 ) );
    
    sound_duration = [ duration_pos_truth, duration_neg_truth ];

    % make sure that MARKER events aren't overlapping
    time_truth = [ time_pos_truth ; time_neg_truth ];
    [ ~, idx ] = sort( time_truth( :, 1 ) );
    time_truth = time_truth( idx, : );
    m = size( time_truth, 1 );

    for i = 2:m
        if time_truth( i - 1, 2 ) > time_truth( i, 1 )
            d = sprintf( 'Overlapping truth events found for\n  %s', ...
                sound_name( context.sound ) );
            fail( d, 'WARNING' )
            error( 'No error' )
        end
    end
end