function output_log( matches, context, parameter )
% output annotated truth log

%------------------------------------------------------------------
% make annotated truth log
%------------------------------------------------------------------

if isempty( parameter.neg_truth_tag ) % no negative truth events    

    % initializations
    M_pos_truth = matches.M_pos_truth;
    notes_pos_truth = matches.notes_pos_truth;
    pos_truth_match = matches.pos_truth_match;
    M_test = matches.M_test;
    notes_test = matches.notes_test;
    pos_test_match = matches.pos_test_match;

    % make truth events
    TP_truth_events = make_events( M_pos_truth,  pos_truth_match, notes_pos_truth , 'TP_truth' );
    FN_truth_events = make_events( M_pos_truth, ~pos_truth_match, notes_pos_truth , 'FN_truth' );
    truth_events = [ TP_truth_events, FN_truth_events ];
    
else                                  % negative truth events

    % initializations
    M_pos_truth = matches.M_pos_truth;
    notes_pos_truth = matches.notes_pos_truth;
    M_neg_truth = matches.M_neg_truth;
    pos_truth_match = matches.pos_truth_match;
    neg_truth_match = matches.neg_truth_match;
    notes_neg_truth = matches.notes_neg_truth;
    M_test = matches.M_test;
    notes_test = matches.notes_test;
    pos_test_match = matches.pos_test_match;
    neg_test_match = matches.neg_test_match;

    % make events
    TP_truth_events = make_events( M_pos_truth,  pos_truth_match, notes_pos_truth, 'TP_truth' );
    FN_truth_events = make_events( M_pos_truth, ~pos_truth_match, notes_pos_truth, 'FN_truth' );
    FP_truth_events = make_events( M_neg_truth,  neg_truth_match, notes_neg_truth, 'FP_truth' );
    TN_truth_events = make_events( M_neg_truth, ~neg_truth_match, notes_neg_truth, 'TN_truth' );     
    truth_events = [ TP_truth_events, FN_truth_events, FP_truth_events, TN_truth_events ];
end

% write log
write_log( truth_events, context, 'annot_truth');

%------------------------------------------------------------------
% make annotated test log
%------------------------------------------------------------------

% make test events
if isempty( parameter.neg_truth_tag ) % no negative truth events
    TP_test_events = make_events( M_test, pos_test_match, notes_test, 'TP_test' );
    FP_test_events = make_events( M_test, ~pos_test_match, notes_test, 'FP_test' );
    test_events = [ TP_test_events, FP_test_events ];
else                                  % negative truth events
    TP_test_events = make_events( M_test, pos_test_match, notes_test, 'TP_test' );
    FP_test_events = make_events( M_test,  neg_test_match, notes_test, 'FP_test' );
    test_events = [ TP_test_events, FP_test_events ];
end

% write log
write_log( test_events, context, 'annot_test' );

%-----------------------------------------------------------------------
function e = make_events( M,  match_vector, notes, tag )

if ~any( match_vector )
    e = [];
    return;
end

%create vector of XBAT events with tag
e = event_create;
e = set_tags( e, tag );
e.level = 1;    
num_events = sum( match_vector );
e( 1:num_events ) = e;

%filter event vector
M = M( match_vector, : );
notes = notes( match_vector );

for i = 1:num_events

    %add event times, frequencies and channel from matrix to XBAT events
    e( i ).time = M( i, 1:2 );
    e( i ).duration = diff( M( i, 1:2 ) );
    e( i ).freq = M( i, 3:4 );
    e( i ).bandwidth = diff( M( i, 3:4 ) );
    e( i ).channel = M( i, 5 );
    e( i ).notes = notes{ i };

    %add event score from matrix to XBAT event, if it isn't NaN
    if ~isnan( M( i, 6 ) )
        e( i ).score = M( i, 6 );
    end
end

%-----------------------------------------------------------------------
function write_log( events, context, suffix )

%reset event id to sequence number
len = length( events );

%if no events, initialize empty event field
if ~len
    events = empty( event_create);
    
%if events, replace event ids with consecutive integers beginning with 1
else
    for i = 1:len
        events( i ).id = i;
    end
end

%output annotated truth log, chopping into sublogs of 10,000 events
for i = 0:ceil( len / 10000 )
    
    %if there are events, don't write a log on the first pass
    if len && ~i
       continue; 
    end

    timestamp = datestr( now, 'yyyymmdd_HHMMSSFFF' );

    %name log
    if len <= 10000
        fn = sprintf( '%s_%s_%s', sound_name( context.sound ), suffix, timestamp );
    else
        fn = sprintf( '%s_%s_%03f_%s', sound_name( context.sound ), suffix, i, timestamp );
    end

    %make log
    log = new_log( fn, context.user, context.library, context.sound );
    if isempty( log )
        d = sprintf( 'Stop execution.  A log named "%s" already exists.', fn );
        fail( d, 'WARNING' )
        error( 'No error' )
    end
    log.event = events;
    log.length = len;
    log.curr_id = len + 1;
    log_save( log );
end
