function output_xls( count, snd_names, sound_duration, log_names, xls_name, parameter, sheet_name )
    
% make XLS worksheet header
M_header = roc_header( sound_duration, snd_names, sheet_name, parameter );

% make XLS worksheet body
if strcmp( sheet_name, { 'Sound Summary' } )
    M = summary_matrix( count, snd_names, parameter, 'Sound Summary' );
else
    M = roc_matrix( count, sound_duration, parameter );
end

% make XLS footer
M_footer = roc_footer( log_names, parameter, sheet_name );

% concatenate header and body
M = [ M_header ; M ; M_footer ];

% write XLS
xlswrite( xls_name, M, sheet_name );