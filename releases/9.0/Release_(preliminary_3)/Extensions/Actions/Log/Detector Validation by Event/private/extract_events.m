function [ M, tags, notes, parameter ] = extract_events( log, parameter )
% Make matrix with event time, freq, and score, and cell vector with tags.

% initializations
M = [];
tags = {};
notes = {};
if isequal( log.length, 0 )
    return;
end

%-------------------------------------------------------------------------
% make <1 x n> cell array of strings with event tags
%-------------------------------------------------------------------------
e = log.event;

% extract tags from events
tags = get_tags( e )';

% stop execution if event has multiple tags
idx = cellfun( @length, tags, 'UniformOutput', false );
idx = find( [ idx{ : } ] > 1 );
if ~isempty( idx )
    d1 = sprintf( '%s\n has multiple tags in events\n', log_name( log ) );
    d2 = sprintf( '  %.0f\n', e( idx ).id );
    fail( sprintf( '%s%s', d1, d2 ), 'WARNING' )
    error( 'No error' )
end

% remove unwanted level from tags
tags = cellfun( @(x) x{ 1 }, tags, 'UniformOutput', false );

%-------------------------------------------------------------------------
% make matrix of times, freqs and scores
%-------------------------------------------------------------------------
time  = struct_field( e, 'time' );
freq  = struct_field( e, 'freq' );
chan  = struct_field( e, 'channel' );
score = struct_field( e, 'score' );
M = [ time, freq, chan, score ];

%-------------------------------------------------------------------------
% make events notes cell vector, using only first cell in notes for each event
%-------------------------------------------------------------------------
notes = { e.notes }';

%-------------------------------------------------------------------------
% if any test events have no score, disable ROC XLS output
%-------------------------------------------------------------------------
if parameter.xls_sound_ROC_flag
    
    test_mask = strcmp( tags, parameter.test_tag );
    isnan_mask = isnan( score );
    scoreless_test_mask = test_mask & isnan_mask;
    if any( scoreless_test_mask )
        parameter.xls_sound_ROC_flag = 0;
        d = sprintf( '%s has events with no score, so ROC XLS output suppressed', log_name( log ) );
        fail( d, 'WARNING' );
    end
end