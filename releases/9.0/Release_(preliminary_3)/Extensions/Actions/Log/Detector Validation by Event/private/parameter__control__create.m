function control = parameter__control__create(parameter, context)
% Detector Validation by Event - parameter__control__create

control = empty(control_create);

%-------------------------------------------------------------------------
% Event Tags SEPARATOR
%-------------------------------------------------------------------------
control(end + 1) = control_create( ...
	'string','Event Tags', ...
	'style','separator', ...
	'type','header', ...
	'space', 0.75 ...
);

%Test Tag
control(end + 1) = control_create( ...
	'name', 'test_tag', ...
	'alias', 'Test',...
	'style', 'edit', ...
	'string', parameter.test_tag ...
);

% Positive Truth Tag
control(end + 1) = control_create( ...
	'name', 'pos_truth_tag', ...
	'alias', 'Positive Truth',...
	'style', 'edit', ...
	'string', parameter.pos_truth_tag ...
);

%Negative Truth Tag
control(end + 1) = control_create( ...
	'name', 'neg_truth_tag', ...
	'alias', 'Negative Truth (blank means none)',...
	'style', 'edit', ...
	'string', parameter.neg_truth_tag ...
);

%MARKER Tag
control(end + 1) = control_create( ...
	'name', 'MARKER_tag', ...
	'alias', 'MARKER (blank means none)',...
	'style', 'edit', ...
	'string', parameter.MARKER_tag ...
);

%-------------------------------------------------------------------------
% Matching Criteria SEPARATOR
%-------------------------------------------------------------------------
control(end + 1) = control_create( ...
	'string','Matching', ...
	'style','separator', ...
	'type','header', ...
	'space', 0.75 ...
);

% Matching Method
str = { 'Time domain overlap' };
control(end + 1) = control_create( ...
    'name', 'match_method', ...
    'alias', 'Matching Method', ...
    'style', 'popup', ...
    'string', str, ...
    'value', parameter.match_method_idx ...
);

% Time overlap
control(end + 1) = control_create( ...
  'name', 'overlap', ...
  'alias', 'Overlap', ...
  'style', 'slider', ...
  'min', 0, ...
  'max', 1, ...
  'value', parameter.overlap ...
  );

%-------------------------------------------------------------------------
% Confidence Intervals SEPARATOR
%-------------------------------------------------------------------------
control(end + 1) = control_create( ...
	'string','Confidence Intervals', ...
	'style','separator', ...
	'type','header', ...
	'space', 0.75 ...
);

% p-Level slider
control(end + 1) = control_create( ...
  'name', 'plevel', ...
  'alias', 'p-level', ...
  'style', 'slider', ...
  'min', 0, ...
  'max', 1, ...
  'value', parameter.plevel ...
  );

%-------------------------------------------------------------------------
% Output Selection SEPARATOR
%-------------------------------------------------------------------------
control(end + 1) = control_create( ...
	'string','Output', ...
	'style','separator', ...
	'type','header', ...
	'space', 0.75 ...
);

% Separate excel file and figure output in separate tabs
tabs = { 'XLS', 'Logs', 'ROC Plots', 'Other Plots' };

control(end + 1) = control_create( ...
        'style', 'tabs', ...
        'tab', tabs ...
);

% % Summary Spreadsheet CHECKBOX
control(end + 1) = control_create( ...
    'name', 'xls_summary_flag', ...
    'tab', tabs{1}, ...
    'alias', 'Summary for Sounds', ...
    'style', 'checkbox', ...
    'value', parameter.xls_summary_flag ...
    );

% ROC Spreadsheet CHECKBOX
control(end + 1) = control_create( ...
    'name', 'xls_sound_ROC_flag', ...
    'tab', tabs{1}, ...
    'alias', 'ROC for each sound', ...
    'style', 'checkbox', ...
    'value', parameter.xls_sound_ROC_flag ...
    );

% ROC for MARKER Events Spreadsheet CHECKBOX
control(end + 1) = control_create( ...
    'name', 'xls_time_ROC_flag', ...
    'tab', tabs{1}, ...
    'alias', 'ROC for MARKER Events', ...
    'style', 'checkbox', ...
    'value', parameter.xls_MARKER_ROC_flag ...
    );

% Annotated log output check box
control(end + 1) = control_create( ...
    'name', 'log_flag', ...
    'tab', tabs{2}, ...
    'alias', 'Annotated Logs', ...
    'style', 'checkbox', ...
    'value', parameter.log_flag ...
    );

% ROC figrure CHECKBOX
control(end + 1) = control_create( ...
    'name', 'fig_flag', ...
    'tab', tabs{3}, ...
    'alias', 'ROC Curve figure', ...
    'style', 'checkbox', ...
    'value', parameter.fig_flag ...
    );

% ROC curves for each template CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'ROC_by_template', ...
    'tab', tabs{3}, ...
    'alias', 'ROC curves for all preset templates', ...
    'style', 'checkbox', ...
    'value', parameter.ROC_by_template ...
    );

%  ROC curve for subsets of templates CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'ROC_by_template_subset', ...
     'tab', tabs{3}, ...
    'alias', 'ROC curves for template subset (e.g. PIPL1a,PIPL1b,PIPL1c)', ...
    'style', 'checkbox', ...
    'value', parameter.ROC_by_template_subset ...
    );

% negative ROC curves for each template (with replacement) CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'negative_ROC_by_template', ...
    'tab', tabs{3}, ...
    'alias', 'Negative ROC curve for each template', ...
    'style', 'checkbox', ...
    'value', parameter.negative_ROC_by_template ...
    );

% TPR vs DTD threshold CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'TPR_vs_thresh', ...
    'tab', tabs{4}, ...
    'alias', 'TPR vs threshold', ...
    'style', 'checkbox', ...
    'value', parameter.TPR_vs_thresh ...
    );

% TPR vs DTD threshold for each template CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'TPR_vs_thresh_by_template', ...
    'tab', tabs{4}, ...
    'alias', 'TPR vs threshold for each template', ...
    'style', 'checkbox', ...
    'value', parameter.TPR_vs_thresh_by_template ...
    );


% FP/hr vs DTD threshold CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'FP_hr_vs_thresh', ...
    'tab', tabs{4}, ...
    'alias', 'FP per hour vs threshold', ...
    'style', 'checkbox', ...
    'value', parameter.FP_hr_vs_thresh ...
    );

% FP/hr vs DTD threshold for each template CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'FP_hr_vs_thresh_by_template', ...
    'tab', tabs{4}, ...
    'alias', 'FP per hour vs threshold for each template', ...
    'style', 'checkbox', ...
    'value', parameter.FP_hr_vs_thresh_by_template ...
    );
    
% F1 Score plot and precision/accuracy plot CHECK BOX 
control(end + 1) = control_create( ...
    'name', 'F1_flag', ...
    'tab', tabs{4}, ...
    'alias', 'F1 score vs threshold', ...
    'style', 'checkbox', ...
    'value', parameter.F1_flag ...
    );

%% Output Separator

control(end + 1) = control_create( ...
	'string','Output', ...
	'style','separator', ...
	'type','header' ...
);


%-------------------------------------------------------------------------
% Event Tags SEPARATOR
%-------------------------------------------------------------------------
control(end + 1) = control_create( ...
	'name', 'out_dir', ...
	'alias', 'Output Path', ...
	'style', 'edit', ...
	'string', parameter.out_dir ...
);

%% Browse Button

control(end + 1) = control_create( ...
	'name', 'browse', ...
	'style', 'buttongroup', ...
	'width', 0.4, ...
	'lines', 1.5, ...
	'space', 2, ...
	'align', 'right' ...
);
