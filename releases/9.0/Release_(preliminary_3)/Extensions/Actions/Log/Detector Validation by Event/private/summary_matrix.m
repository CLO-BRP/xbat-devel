function M = summary_matrix( count, snd_names, parameter, mode )
        
len = length( count );
sound_duration = struct_field( count, 'sound_duration' );
if strcmp( mode, 'MARKER ROC' )
    sound_MARKER_time = struct_field( count, 'MARKER_time' );
end

%--------------------------------------------------------------------------
% no negative truth events (TN not defined explicitly)
%--------------------------------------------------------------------------
if isempty( parameter.neg_truth_tag )

    M = cell( len + 2, 13 );
    
    for i = 1:len
        
        TP_truth = count( i ).TP_truth;
        FN_truth = count( i ).FN_truth;
        TP_test  = count( i ).TP_test;
        FP_test  = count( i ).FP_test;
        curr_sound_duration = sound_duration( i );

        %find confidence intervals for TPR
        [tpr, ci] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );  
        
        %------------------------------------------------------------------
        %make data for summary table
        %------------------------------------------------------------------
        
        % First column is sound name for "Sound Summary" workbook, and
        % Begin Day, Begin Time, End Day, and End Time for "MARKER ROC" workbook
        if strcmp( mode, 'Sound Summary' )
            M{ i, 1 }  = snd_names{ i };
            j = 0;
        elseif strcmp( mode, 'MARKER ROC' )
            M{ i, 1 } = snd_names{ i };
            dn = make_dn( sound_MARKER_time( i, : ) );            
            M{ i, 2 } = dn.begin_day;
            M{ i, 3 } = dn.begin_time;
            M{ i, 4 } = dn.end_time;
            M{ i, 5 } = dn.begin_excel_date;
            M{ i, 6 } = dn.end_excel_date;
            j = 5;
        end
        
        %other columns
        M{ i, j + 2 }  = tpr;
        M{ i, j + 3 }  = ci( 1 );
        M{ i, j + 4 }  = ci( 2 );
        M{ i, j + 5 }  = TP_truth / ( TP_truth + FN_truth );
        M{ i, j + 6 }  = FP_test / ( curr_sound_duration / 3600 );
        M{ i, j + 7 }  = TP_test / ( TP_test + FP_test );
        M{ i, j + 8 }  = TP_truth;
        M{ i, j + 9 }  = FN_truth;
        M{ i, j + 10 } = TP_truth + FN_truth;
        M{ i, j + 11 } = TP_test;
        M{ i, j + 12 } = FP_test;
        M{ i, j + 13 } = TP_test + FP_test;
        M{ i, j + 14 } = curr_sound_duration;
    end
        
    %Summary line
    TP_truth = sum( struct_field( count, 'TP_truth' ) );
    FN_truth = sum( struct_field( count, 'FN_truth' ) );
    TP_test  = sum( struct_field( count, 'TP_test' ) );
    FP_test  = sum( struct_field( count, 'FP_test' ) );
    sound_duration = sum( struct_field( count, 'sound_duration' ) );  

    %find confidence intervals for TPR
    [tpr, ci] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );  

    %make data for summary table
    i = i + 2;

    % First column is sound names for "Sound Summary" workbook, and
    % Begin Day, Begin Time, End Day, and End Time for "MARKER ROC" workbook
    M{ i, 1 }  = 'SUMMARY'; 
    if strcmp( mode, 'Sound Summary' )
        j = 0;
    elseif strcmp( mode, 'MARKER ROC' )
        j = 4;
    end

    M{ i, j + 2 }  = tpr;
    M{ i, j + 3 }  = ci( 1 );
    M{ i, j + 4 }  = ci( 2 );
    M{ i, j + 5 }  = TP_truth / ( TP_truth + FN_truth );
    M{ i, j + 6 }  = FP_test / ( sound_duration / 3600 );
    M{ i, j + 7 }  = TP_test / ( TP_test + FP_test );
    M{ i, j + 8 }  = TP_truth;
    M{ i, j + 9 }  = FN_truth;
    M{ i, j + 10 } = TP_truth + FN_truth;
    M{ i, j + 11 } = TP_test;
    M{ i, j + 12 } = FP_test;
    M{ i, j + 13 } = TP_test + FP_test;
    M{ i, j + 14 } = sound_duration;
    
%--------------------------------------------------------------------------
% if negative truth events ( TN defined explicitly )  
%--------------------------------------------------------------------------
else
    M = cell( len + 2, 20 );
    
	for i = idx
        
        TP_truth = count( i ).TP_truth;
        FN_truth = count( i ).FN_truth;
        TN_truth = count( i ).TN_truth;
        FP_truth = count( i ).FP_truth;
        TP_test  = count( i ).TP_test;
        FP_test = count( i ).FP_test;
        curr_sound_duration = sound_duration( i, : );

        %find confidence intervals for TPR
        [ tpr, ci ] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );
        
        %------------------------------------------------------------------
        %make data for summary table
        %------------------------------------------------------------------
        
        % First column is "SUMMARY" for "Sound Summary" workbook, and
        % Begin Day, Begin Time, End Day, and End Time for "MARKER ROC" workbook
        if strcmp( mode, 'Sound Summary' )
            M{ i, 1 }  = snd_names{ i };
            j = 0;
        elseif strcmp( mode, 'MARKER ROC' )
            dn = make_dn( sound_MARKER_time( i, : ) );            
            M{ i, 1 } = dn.begin_day;
            M{ i, 2 } = dn.begin_time;
            M{ i, 3 } = dn.end_time;
            M{ i, 4 } = dn.begin_excel_date;
            M{ i, 5 } = dn.end_excel_date;
            j = 4;
        end
        
        %other columns
        M{ i, j + 2 }  = tpr;
        M{ i, j + 3 }  = ci( 1 );
        M{ i, j + 4 }  = ci( 2 );
        M{ i, j + 5 }  = TP_truth / ( TP_truth + FN_truth );  %TPR
        M{ i, j + 6 }  = FP_truth / ( FP_truth + TN_truth );  %FPR
        
        %FP/hr - count only sound duration in negative truth events
        M{ i, j + 7 }  = FP_test / ( sum( curr_sound_duration ) / 3600 );        
        M{ i, j + 8 }  = TP_test / ( TP_test + FP_test );     %PPV
        M{ i, j + 9 }  = TP_truth;
        M{ i, j + 10 } = FN_truth;
        M{ i, j + 11 } = TP_truth + FN_truth;
        M{ i, j + 12 } = FP_truth;
        M{ i, j + 13 } = TN_truth;
        M{ i, j + 14 } = FP_truth + TN_truth;
        M{ i, j + 15 } = TP_truth + FN_truth + FP_truth + TN_truth;
        M{ i, j + 16 } = TP_test;
        M{ i, j + 17 } = FP_test;
        M{ i, j + 18 } = TP_test + FP_test;
        M{ i, j + 19 } = curr_sound_duration( 1 );
        M{ i, j + 20 } = curr_sound_duration( 2 );
	end
            
    % summary line
    TP_truth = sum( struct_field( count, 'TP_truth' ) );
    FN_truth = sum( struct_field( count, 'FN_truth' ) );
    TN_truth = sum( struct_field( count, 'TN_truth' ) );
    FP_truth = sum( struct_field( count, 'FP_truth' ) );
    TP_test  = sum( struct_field( count, 'TP_test' ) );
    FP_test  = sum( struct_field( count, 'FP_test' ) );
    sound_duration = sum( struct_field( count, 'sound_duration' ) );

    %find confidence intervals for TPR
    [ tpr, ci ] = binofit( TP_truth, TP_truth + FN_truth, 1 - parameter.plevel );

    %make data for summary table
    i = i + 1;

    % First column is sound names for "Sound Summary" workbook, and
    % Begin Day, Begin Time, End Day, and End Time for "MARKER ROC" workbook
    M{ i, 1 }  = 'SUMMARY'; 
    if strcmp( mode, 'Sound Summary' )
        j = 0;
    elseif strcmp( mode, 'MARKER ROC' )
        j = 4;
    end

    M{ i, j + 2 }  = tpr;
    M{ i, j + 3 }  = ci( 1 );
    M{ i, j + 4 }  = ci( 2 );
    M{ i, j + 5 }  = TP_truth / ( TP_truth + FN_truth );  %TPR
    M{ i, j + 6 }  = FP_truth / ( FP_truth + TN_truth );  %FPR

    %FP/hr - count only sound duration in negative truth events
    M{ i, j + 7 }  = FP_test / ( curr_sound_duration( 2 ) / 3600 );        
    M{ i, j + 8 }  = TP_test / ( TP_test + FP_test );     %PPV
    M{ i, j + 9 }  = TP_truth;
    M{ i, j + 10 } = FN_truth;
    M{ i, j + 11 } = TP_truth + FN_truth;
    M{ i, j + 12 } = FP_truth;
    M{ i, j + 13 } = TN_truth;
    M{ i, j + 14 } = FP_truth + TN_truth;
    M{ i, j + 15 } = TP_truth + FN_truth + FP_truth + TN_truth;
    M{ i, j + 16 } = TP_test;
    M{ i, j + 17 } = FP_test;
    M{ i, j + 18 } = TP_test + FP_test;
    M{ i, j + 19 } = curr_sound_duration( 1 );
    M{ i, j + 20 } = curr_sound_duration( 2 );
end

function dn = make_dn( sound_MARKER_time )

    begin_datenum = sound_MARKER_time( 1, 1 );
    end_datenum = sound_MARKER_time( 1, 2 );
    dn.begin_day = sprintf( '''%s', datestr( begin_datenum , 'yyyy-mm-dd' ) );
    dn.begin_time = sprintf( '''%s', datestr( begin_datenum , 'HH:MM:SS.FFF' ) );
    dn.end_time = sprintf( '''%s', datestr( end_datenum, 'HH:MM:SS.FFF' ) );
    dn.begin_excel_date = begin_datenum - datenum( '30-Dec-1899, 00:00:00' );
    dn.end_excel_date = end_datenum - datenum( '30-Dec-1899, 00:00:00' );