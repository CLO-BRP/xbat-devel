function [ log, snd_name ] = load_log( str, context )
% function [ log, snd_name, status ] = load_log( str, context )
%load XBAT log and refresh internal reprentation of file name and path

% status = 1;

%load log
[snd_name, fn] = fileparts(str);  
log = get_library_logs('logs',[],snd_name,str);
if ~(ischar(snd_name) && ischar(fn) && isequal(length(log),1))    
  fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');
  error( 'No error' )
%   return;
end

%refresh log name and path
fnExt = [fn '.mat'];
log.file = fnExt;
LogPath = context.library.path;
LogPath = [LogPath snd_name '\Logs\'];
log.path = LogPath;

% status = 0;