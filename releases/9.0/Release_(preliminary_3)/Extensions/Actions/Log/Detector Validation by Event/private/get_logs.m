function logs = get_logs( context )
%retrieve list of selected logs

% logs = {};

if isfield(context, 'target')   %check if context.target is available
	logs = context.target;
else
    fail( 'API supplied no context.target in prepare.m', 'WARNING' )
	error( 'No error' )
%   fprintf(2,'API supplied no context.target in prepare.m \n');
%   return;
end

if ~iscell(logs)
  logs = {logs};
end

if isempty( logs )
    fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
    error( 'No error' )
%     return;
end