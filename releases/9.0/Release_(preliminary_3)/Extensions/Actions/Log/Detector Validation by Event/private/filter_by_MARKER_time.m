function [ M, M_MARKER, tags, notes ] = filter_by_MARKER_time( M, tags, notes, MARKER_tag, snd_name )
% Filter events by MARKER event times

% separate MARKER events from other events
mask = strcmp( tags, MARKER_tag );
M_MARKER = M(  mask, : );
M = M( ~mask, : );
tags = tags( ~mask );
notes = notes( ~mask );

% stop execution if no MARKER events
if isempty( M_MARKER )
    d = sprintf( 'No MARKER events for %s', snd_name{ 1 } );
    fail( d, 'WARNING' )
    error( 'No error' )
end

%stop execution if MARKER event spans midnight
t_real = M_MARKER( :, 7 );
duration = diff( M_MARKER( :, 1:2 ), 1, 2 );
span_midnight = ( floor( t_real + duration ./ 86400 ) - floor( t_real ) ) == 1;
if any( span_midnight )
    d1 = sprintf( 'MARKER event(s) span midnight in "%s" starting at these times:', snd_name );
    d2 = sprintf( '  %s\n', datestr( t_real( span_midnight, 1 ) ) );
    fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
    error( 'No Error' )
end

% find which events have time bounds within MARKER events
L = M_MARKER( :, 1 );  %lower time bounds of MARKER events
U = M_MARKER( :, 2 );  %upper time bounds of MARKER events
match = M( :, 1 ) >= L( 1 ) & M( :, 2 ) <= U( 1 ) ;
for i = 2:size( L, 1 );
    match = match | ( M( :, 1 ) >= L( i ) & M( :, 2 ) <= U( i ) ) ;
end

% filter events by MARKER event time
M = M( match, : );
tags = tags( match );
notes = notes( match );