function M = roc_footer( log_names, parameter, mode )

M = {};
num_logs = length( log_names );

if ~num_logs
    return;
end

% figure out how many columns there are in the cell array
if isempty( parameter.neg_truth_tag )
    j = 13;
else 
    j = 18;
end
if strcmp( mode, 'Sound Summary' )
    j = j + 1;
elseif strcmp( mode, { 'MARKER ROC' } )
    j = j + 6;
end

% make footer
M = cell( num_logs + 2, j );
M{ 2, 1 } = 'Log Names:';
M( 3:end, 1 ) = log_names;