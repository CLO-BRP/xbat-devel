function cum_count = sum_fields( cum_count, count )

if isempty( cum_count )
    cum_count = count;
else
    cum_count.TP_truth = cum_count.TP_truth + count.TP_truth;
    cum_count.FN_truth = cum_count.FN_truth + count.FN_truth;
    cum_count.FP_truth = cum_count.FP_truth + count.FP_truth;
    cum_count.TN_truth = cum_count.TN_truth + count.TN_truth;
    cum_count.P_truth_score = [cum_count.P_truth_score  ; count.P_truth_score ];
    cum_count.N_truth_score = [ cum_count.N_truth_score ; count.N_truth_score ];
    cum_count.TP_test = cum_count.TP_test  + count.TP_test;
    cum_count.FP_test = cum_count.FP_test  + count.FP_test;
    cum_count.TP_test_score = [ cum_count.TP_test_score ; count.TP_test_score ];
    cum_count.FP_test_score = [ cum_count.FP_test_score ; count.FP_test_score ];
    cum_count.FN_test_score = [ cum_count.FN_test_score ; count.FN_test_score ];
    cum_count.TN_test_score = [ cum_count.TN_test_score ; count.TN_test_score ];
end