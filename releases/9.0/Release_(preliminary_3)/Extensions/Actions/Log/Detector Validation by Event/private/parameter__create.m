function parameter = parameter__create(context)
% Detector Validation by Event - parameter__create

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

% load preset, if readible                    
try    
  load(preset_path)
  
  %warn user if user parameters have changed
  warn_parameter_change( parameter, preset_path)

% if preset not readible, create one
catch
  parameter = make_parameter;
  save(preset_path,'parameter');
  fail( 'preset.mat initialized because it is missing or corrupt.', 'WARNING' )
end


function warn_parameter_change( parameter, preset_path)
%warn user if parameter has changed since last invocation

%re-initialize preset.mat if parameter fields have changed
newfields = setdiff( fieldnames( make_parameter ), fieldnames( parameter ) );
if ~isempty( newfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been added to GUI.';
  d2 = 'Cancel and try again.';
  d3 = sprintf( '   %s\n', newfields{ : } );
  fail( sprintf( '%s%s\n%s', d1, d2, d3 ), 'WARNING' )
end

oldfields = setdiff( fieldnames( parameter ), fieldnames( make_parameter ) );
if  ~isempty( oldfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been removed from GUI.';
  d2 = 'Cancel and try again.';
  d3 = sprintf( '   %s\n', oldfields{ : } );
  fail( sprintf( '%s%s\n%s', d1, d2, d3 ), 'WARNING' )
end


function parameter = make_parameter
%initialize parameter if extension has never been invoked

parameter = struct;

parameter.pos_truth_tag = 'clk';
parameter.neg_truth_tag = '';
parameter.MARKER_tag = 'MARKER';
parameter.test_tag = 'Sperm_Click';

parameter.match_method = '';
parameter.match_method_idx = 1;
parameter.overlap = 0.5;

parameter.plevel = 0.9;

parameter.xls_summary_flag = 1;
parameter.xls_sound_ROC_flag = 1;
parameter.xls_MARKER_ROC_flag = 1;
parameter.log_flag = 1;

parameter.fig_flag = 0;
parameter.ROC_by_template = 0;

parameter.ROC_by_template_subset = 0;
parameter.negative_ROC_by_template = 0;

parameter.TPR_vs_thresh = 0;
parameter.FP_hr_vs_thresh_by_template = 0;
parameter.FP_hr_vs_thresh = 0;
parameter.TPR_vs_thresh_by_template = 0;

parameter.F1_flag = 0;

parameter.out_dir = pwd;