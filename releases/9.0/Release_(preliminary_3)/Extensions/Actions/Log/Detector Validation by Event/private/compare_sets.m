function [ count, matches ] = compare_sets( M, tags, notes, parameter )
% Classify events as TP, FP, FN and TN, counting and creating XBAT events
% for each event class.

% filter positive truth events by tag
pos_truth = strcmp( tags, parameter.pos_truth_tag );
M_pos_truth = M( pos_truth , : );

% filter test events by tag
test = strcmp( tags, parameter.test_tag );
M_test = M( test, : );

% if no negative truth events
if isempty( parameter.neg_truth_tag )

    %classify positive and truth events and test events as matches or misses
    [ pos_test_match, pos_truth_match, P_truth_score, TP_test_score, FP_test_score ] = match( M_pos_truth, M_test, parameter );

    %count events
    TP_truth = sum( pos_truth_match );
    FN_truth = size( M_pos_truth, 1 ) - TP_truth;   
    TP_test = sum( pos_test_match );

    %put counts into struct
    count = struct( 'TP_truth',      TP_truth, ...
                    'FN_truth',      FN_truth, ...
                    'FP_truth',      [], ...
                    'TN_truth',      [], ...
                    'P_truth_score', P_truth_score, ...
                    'N_truth_score', [], ...
                    'TP_test',       TP_test, ...
                    'FP_test',       [], ...
                    'TP_test_score', TP_test_score, ...
                    'FP_test_score', FP_test_score, ...
                    'FN_test_score', [], ...
                    'TN_test_score', [] );
    
    %count events
    FP_test = sum( ~pos_test_match );
    
    %put counts into struct
    count.FP_test = FP_test;

    % put results in struct for making annotated logs
    matches.M_pos_truth = M_pos_truth;
    matches.pos_truth_match = pos_truth_match;
    matches.M_test = M_test;
    matches.pos_test_match = pos_test_match;
    
% if negative truth events
else
    
    disp('***************************************************************')
    disp('* This section of compare_sets.m is a mess and needs work!    *')
    disp('***************************************************************')    

    %classify positive and truth events and test events as matches or misses
    [ pos_test_match, pos_truth_match, P_truth_score, TP_test_score ] = match( M_pos_truth, M_test, parameter );

    %count events
    TP_truth = sum( pos_truth_match );
    FN_truth = size( M_pos_truth, 1 ) - TP_truth;   
    TP_test = sum( pos_test_match );

    %put counts into struct
    count = struct( 'TP_truth',      TP_truth, ...
                    'FN_truth',      FN_truth, ...
                    'FP_truth',      [], ...
                    'TN_truth',      [], ...
                    'P_truth_score', P_truth_score, ...
                    'N_truth_score', [], ...
                    'TP_test',       TP_test, ...
                    'FP_test',       [], ...
                    'TP_test_score', TP_test_score, ...
                    'FP_test_score', [], ...
                    'FN_test_score', [], ...
                    'TN_test_score', [] );
    
    % filter events by tag
    neg_truth = strcmp( tags, parameter.neg_truth_tag );
    M_neg_truth = M( neg_truth, : );
   
    
    %classify positive and truth events and test events as matches or misses
    [ neg_test_match, neg_truth_match, N_truth_score, FP_test_score ] = match( M_neg_truth, M_test, parameter );

    %count events
    FP_truth = sum( neg_truth_match );
    FP_test = sum( neg_test_match );
    TN_truth = size( M_neg_truth, 1 ) - FP_truth; 

    %put counts into struct
    count.FP_truth = FP_truth;
    count.TN_truth = TN_truth;
    count.FP_test = FP_test;  
    count.N_truth_score = N_truth_score;
    count. FP_test_score = FP_test_score;

    % put results in struct for making annotated logs
    matches.M_pos_truth = M_pos_truth;
    matches.M_neg_truth = M_neg_truth;
    matches.pos_truth_match = pos_truth_match;
    matches.neg_truth_match = neg_truth_match;
    matches.M_test = M_test;
    matches.pos_test_match = pos_test_match;
    matches.neg_test_match = neg_test_match;
end

% put notes in match, if any
if isempty( notes )
    matches.notes_pos_truth = {};
    matches.notes_neg_truth = {};
    matches.notes_test = {};
else
    notes_pos_truth = notes( pos_truth );
    notes_test = notes( test );
    if isempty( parameter.neg_truth_tag )
        matches.notes_test = notes_test;
        matches.notes_pos_truth = notes_pos_truth;
    else
        notes_neg_truth = notes( neg_truth );
        matches.notes_pos_truth = notes_pos_truth;
        matches.notes_neg_truth = notes_neg_truth;
        matches.notes_test = notes_test;
    end
end


function [ test_match, truth_match, truth_score, test_score_hit, test_score_miss ] = match( M_truth, M_test, parameter )

    %initializations
    size_truth = size( M_truth, 1 );
    truth_match_tentative = zeros( size_truth, 1 );
    truth_match = false( size_truth, 1 );
    size_test  = size( M_test, 1 );
    test_match_tentative = zeros( size_test, 1 );
    test_match = false( size_test, 1 );
    all_score = M_test( :, 6 );
    truth_score = NaN( size_truth, 1 );

    % for each truth event, record all test matches (idx and overlap duration)
    for i = 1:size_truth
        
        %find overlap of current truth event with test events
        overlap = min( M_truth( i, 2 ), M_test( :, 2 ) ) ...
                - max( M_truth( i, 1 ), M_test( :, 1 ) );
        if isempty( overlap )
            continue;
        end
        [ max_overlap, max_idx ] = max( overlap );
        
        %find overlap threshold
        overlap_threshold = parameter.overlap * ...
            min( diff( M_truth( i, 1:2 ) ), diff( M_test( max_idx, 1:2 ) ) );        
        
        %record index of test event with greatest overlap, if over treshold
        if max_overlap > overlap_threshold
            truth_match_tentative( i, 1 ) = max_idx;
        end
    end
    
    % for each test event, record index of truth event with greatest overlap duration
    for i = 1:size_test        
        
        %find overlap of current test event with truth events
        overlap = min( M_test( i, 2 ), M_truth( :, 2 ) ) ...
                - max( M_test( i, 1 ), M_truth( :, 1 ) );
        if isempty( overlap )
            continue;
        end
        [ max_overlap, max_idx ] = max( overlap );
        
        %find overlap threshold
        overlap_threshold = parameter.overlap * ...
            min( diff( M_test( i, 1:2 ) ), diff( M_truth( max_idx, 1:2 ) ) );        
        
        %record index of truth event with greatest overlap, if over treshold
        if max_overlap > overlap_threshold
            test_match_tentative( i, 1 ) = max_idx;
        end 
    end
    
    %for each truth event, match if test event it overlaps with most has 
    % its greatest overlap with same truth event
    %(remove test event from truth_match_tentative not yet processed)
    for i = 1:size_truth
        
        %if test event it overlaps with most has its greatest overlap with same truth event
        curr_truth_match_tentative = truth_match_tentative( i );
        if ~isequal( curr_truth_match_tentative, 0 ) ...
        &&  isequal( test_match_tentative( curr_truth_match_tentative ), i )
            
            %classify current truth event as TP
            truth_match( i ) = true;
                
            %record test event score for matching truth event
            truth_score( i ) = all_score( curr_truth_match_tentative );
            
            %record test event as a match
            test_match( curr_truth_match_tentative ) = true;
            
            %remove test event idx as tentative match for other truth events
            for j = i:size_truth
               if isequal( truth_match_tentative( j ), curr_truth_match_tentative )
                   truth_match_tentative( j ) = 0;
               end
            end
        end
    end
    
    %delete scores for truth events that did not have test match
    truth_score( isnan( truth_score ) ) = [];
    
    %scores for matching test events depends on whether multiple matches allowed
    test_score_hit = all_score( test_match );
    test_score_miss = all_score( ~test_match );