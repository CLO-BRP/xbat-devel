function M = roc_header( sound_duration, snd_names, mode, parameter )

%--------------------------------------------------------------------------
%set header
%--------------------------------------------------------------------------
M{ 1 } = 'Detection Validation';
M{ 2, 1 } = 'Time Run:';
M{ 2, 5 } = datestr(now,  21);
M{ 4, 1 } = 'Test Tag:';
M{ 4, 5 } = parameter.test_tag;
M{ 5, 1 } = 'Positive Truth Tag:';
M{ 5, 5 } = parameter.pos_truth_tag;
M{ 6, 1 } = 'Negative Truth Tag:';
M{ 6, 5 } = parameter.neg_truth_tag;
M{ 7, 1 } = 'MARKER Tag:';
M{ 7, 5 } = parameter.MARKER_tag;
M{ 8, 1 } = 'Matching Method:';
M{ 8, 5 } = parameter.match_method{ 1 };
M{ 9, 1 } = 'Time Overlap (% test event duration):';
M{ 9, 5 } = parameter.overlap;

% sound duration
if isempty( parameter.neg_truth_tag )
    if isempty( parameter.MARKER_tag )
        M{ 11, 1 } = 'Sound Duration (s):';
        M{ 11, 5 } = sound_duration;
    else
        M{ 11, 1 } = 'MARKER Duration (s):';
        M{ 11, 5 } = sound_duration;
    end
    m = 11;
else
    M{ 11, 1 } = 'Positive Truth Duration (s):';
    M{ 11, 5 } = sound_duration( 1 );
    M{ 12, 1 } = 'Negative Truth Duration (s):';
    M{ 12, 5 } = sound_duration( 2 );
    m = 12;
end

%--------------------------------------------------------------------------
% add sound names to header
%--------------------------------------------------------------------------

% skip sound names in "Sound Summary" worksheet
if ismember( mode, { 'Sound Summary', 'MARKER ROC' } )
    m = m + 2;
    
% add sound names for other worksheets
else
    M{ m + 2, 1 } = 'Sound Names';
    for i = 1:length( snd_names )
        M( m + 1 + i, 2 ) = snd_names( i );
    end
    m = i + m + 3;
end

%--------------------------------------------------------------------------
% set column heads
%--------------------------------------------------------------------------

% column head for column A depends on workbook
if strcmp( mode, 'Sound Summary' )
    M{ m, 1 } = 'Sound Name';
    n = 1;
elseif strcmp( mode, 'MARKER ROC' )
    M( m, 1:6 ) = { 'Sound Name', 'Begin Date', 'Begin Time', 'End Time', 'Begin Excel Date-time', 'End Excel Date-time' };
    n = 6;
else
    M{ m, 1 } = 'Threshold';
    n = 1;
end

%if no negative truth events (TN not defined explicitly)
if isempty( parameter.neg_truth_tag )  
    M{ m, n + 1 }      = 'TPR';
    M{ m, n + 2 }      = sprintf('TPR (%.0f%% CI lower)', parameter.plevel * 100);
    M{ m, n + 3 }      = sprintf('TPR (%.0f%% CI upper)', parameter.plevel * 100);  
    M{ m, n + 4 }      = 'TPR';
    M{ m, n + 5 }      = 'FP/hr';
    M{ m, n + 6 }      = 'PPV';
    M{ m, n + 7 }      = 'Positive Truth Matches';
    M{ m + 1, n + 7 }  = 'TP (truth)';
    M{ m, n + 8 }      = 'Positive Truth Misses';
    M{ m + 1, n + 8 }  = 'FN (truth)';
    M{ m, n + 9 }     = 'Positive Truth Total';
    M{ m, n + 10 }     = 'Test Matches';
    M{ m, n + 11 }     = 'Test Misses';
    M{ m + 1, n + 11 } = 'FP (test)';
    M{ m, n + 12 }     = 'Test Total';

    %if header is for a summary worksheet, add columns for sound duration
    if any( strcmp( mode, { 'Sound Summary', 'MARKER ROC'} ) )
        if isempty( parameter.MARKER_tag )
            M{ m, n + 13 } = 'Sound Duration (s):';
        else
            M{ m, n + 13 } = 'MARKER Duration (s):';
        end
    end
  
%if negative truth events (TN defined explicitly)
else  
    M{ m, n + 1 }      = 'TPR';
    M{ m, n + 2 }      = 'TPR (CI lower)';
    M{ m, n + 3 }      = 'TPR (CI upper)';  
    M{ m, n + 4 }      = 'TPR(check)';
    M{ m, n + 5 }      = 'FPR';
    M{ m, n + 6 }      = 'FP/hr';
    M{ m, n + 7 }      = 'PPV';
    M{ m, n + 8 }      = 'Positive Truth Matches';
    M{ m, n + 9 }     = 'Positive Truth Misses';
    M{ m, n + 10 }     = 'Positive Truth Total';
    M{ m, n + 11 }     = 'Negative Truth Matches';
    M{ m, n + 12 }     = 'Negative Truth Misses';
    M{ m, n + 13 }     = 'Negative Truth Total';  
    M{ m, n + 14 }     = 'Truth Total';
    M{ m, n + 15 }     = 'Test Matches Positive Truth';
    M{ m, n + 16 }     = 'Test Matches Negative Truth';
    M{ m, n + 17 }     = 'Test Total';
    M{ m + 1, n + 8 }  = 'TP (truth)';
    M{ m + 1, n + 9 } = 'FN (truth)';
    M{ m + 1, n + 11 } = 'FP (truth)';
    M{ m + 1, n + 12 } = 'TN (truth)';
    M{ m + 1, n + 15 } = 'TP (test)';
    M{ m + 1, n + 16 } = 'FP (test)';
    M{ m + 1, n + 17 } = 'Positive Truth Duration (s)';
    M{ m + 1, n + 18 } = 'Negative Truth Duration (s)';    

    %if header is for a summary worksheet, add columns for sound duration
    if any( strcmp( mode, { 'Sound Summary', 'MARKER ROC'} ) )
        M{ m, n + 19 } = 'Positive Truth Duration (s)';
        M{ m, n + 20 } = 'Negative Truth Duration (s)';
    end
end