function output_MARKER_ROC_xls( M, M_MARKER, tags, snd_names, sound_duration, log_names, xls_time_ROC, parameter )
%Write XLS reporting ROC for each MARKER event, with one worksheet for each 
%score threshold, and one line for each MARKER event

if ~isempty( parameter.neg_truth_tag )
    fail( 'ROC for MARKER Events code is not available for negative truth events yet.', 'WARNING' )
    return;
end

% make header and footer for all worksheets
unique_snd_names = unique( snd_names );
M_header = roc_header( sound_duration, unique_snd_names, 'MARKER ROC', parameter );
M_footer = roc_footer( log_names, parameter, 'MARKER ROC' );

%time_MARKER-row for each MARKER event
%col 1 = begin datenum, cl2 = end, datenum, col3 = duration (s)
sec_per_day = 86400;
time_MARKER = M_MARKER( :, 7 );
duration_MARKER = diff( M_MARKER( :, 1:2 ), 1, 2 );
time_MARKER = [ time_MARKER , time_MARKER + duration_MARKER ./ sec_per_day, duration_MARKER ];

% time = M( :, 7 );
% time = [ time , time + diff( M( :, 1:2 ), 1, 2 ) ./ sec_per_day ];
mask_truth = strcmp( tags, parameter.pos_truth_tag ); %mask for filtering truth events
mask_test = strcmp( tags, parameter.test_tag );           %mask for filtering test events

%tolerance to allow for rounding errors handling MATLAB datenums
tol = 2 * eps( datenum( '1-Jan-2100' ) ); 
    
% find threshold vector for ROC analysis
inc = 0.05;
score = M( :, 6 );
lo_threshold = min( score );
hi_threshold = min( max( score ) + inc, 1 );
threshold_vector = ( floor( lo_threshold ./ inc ) : floor( hi_threshold ./ inc ) ) .* inc;

%---------------------------------------------------------------------
% output worksheet for each threshold
%---------------------------------------------------------------------
num_MARKERs = size( M_MARKER, 1 );
for  threshold = threshold_vector
    
    count_vector = [];

    %filter test events with score above threshold and truth events
    mask_thresh = mask_truth | ( mask_test & ( M( :, 6 ) >= threshold ) );
    M_thresh = M( mask_thresh, : );
    time = M_thresh( :, 7 );
    time = [ time , time + diff( M_thresh( :, 1:2 ), 1, 2 ) ./ sec_per_day ];
    tags_thresh = tags( mask_thresh );

    %---------------------------------------------------------------------
    % output ROC by MARKER event
    %---------------------------------------------------------------------
    for i = 1:num_MARKERs

        %find time bounds of current MARKER event
        curr_time_MARKER = time_MARKER( i, 1:2 );
        curr_duration_MARKER = time_MARKER( i, 3 );

        %filter truth and test events contained by current MARKER event
        mask_MARKER = ( ( time( :, 2 ) + tol ) >= curr_time_MARKER( 1, 1 ) ) ...
                    & ( ( time( :, 1 ) - tol ) <= curr_time_MARKER( 1, 2 ) );
        M_curr = M_thresh( mask_MARKER, : );
        tags_curr = tags_thresh( mask_MARKER );
        count = compare_sets( M_curr, tags_curr, [], parameter );
    
        %add fields for MARKER event time and duration
        count.MARKER_time = curr_time_MARKER;
        count.sound_duration = curr_duration_MARKER;
        
        count_vector = [ count_vector , count ];
    end

    % make worksheet for threshold    
    M_body = summary_matrix( count_vector, snd_names, parameter, 'MARKER ROC' );

    % concatenate header and body
    M_out = [ M_header ; M_body ; M_footer ];

    % write XLS
    threshold_str = sprintf( '%.2f', threshold );
    xlswrite( xls_time_ROC, M_out, threshold_str );
    
end