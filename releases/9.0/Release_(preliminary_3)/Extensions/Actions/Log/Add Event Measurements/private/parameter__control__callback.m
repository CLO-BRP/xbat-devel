function result = parameter__control__callback(callback, context)

% ADD EVENT MEASUREMENTS - parameter__control__callback

result = struct;

%% update saved user parameter settings

preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'preset.mat');
load( preset_path )
name = callback.control.name;
style = get( callback.control.handle, 'style' );
	
if any( strcmp(style, { 'slider' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name ' = value;' ] );

elseif any( strcmp( style, { 'popupmenu' 'listbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name '_idx = value;' ] );
  
elseif any( strcmp( style, { 'edit' } ) )
  value = get( callback.control.handle, 'String' );
  
  %update slider if edit is part of slider control
  if length( callback.control.handles ) > 1
    style2 = get( callback.control.handles( 2 ), 'style' );    
    if strcmp( style2, 'slider')
      value = str2double( value );
    end    
  end
  
  eval( [ 'parameter.' name ' = value;' ] );
  
end

save(preset_path,'parameter');
