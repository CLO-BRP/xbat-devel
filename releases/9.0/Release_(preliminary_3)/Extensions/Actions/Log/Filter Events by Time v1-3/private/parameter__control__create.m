function control = parameter__control__create(parameter, context)

% FILTER EVENTS BY TIME v1-3 - parameter__control__create

% Log action creates new log with only events in user-specified time range
%
% New logs has suffix "_<hh>h-<hh>h"
%
% *** NOTE: Events only partly in the selected time range are included!!! ***

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. 

% Change Log
%
% 1. If there are no events in time range, gives warning and generates no
% log instead of making defective log and generating MATLAB errors
%
% 2. Gives the user the option to choose between 2 suffixes:
%    a. "_Keep_<h>h-<h>h"
%    b. "_to_be_locs_<h>-<h>" 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

control = empty(control_create);


% Select Time of Beginning of Keep Interval
control(end + 1) = control_create( ...
  'name', 'BeginTime', ...
  'alias', 'Keep Events After Hour', ...
  'style', 'slider', ...
  'type', 'integer', ...
  'min', 0, ...
  'max', 23, ...
  'value', 0 ...
  );

% Select Time of Beginning of Keep Interval
control(end + 1) = control_create( ...
  'name', 'EndTime', ...
  'alias', 'Keep Events Before Hour', ...
  'style', 'slider', ...
  'type', 'integer', ...
  'min', 1, ...
  'max', 24, ...
  'value', 24 ...
  );

%LIST (select multiple items)
control(end + 1) = control_create( ...
	'name','suffix', ...
  'alias','Log Name Suffix', ...
	'style','listbox', ...
	'lines',2, ...
  'value',1, ...
	'string', {'_Keep_<h>h-<h>h' ...
             '_to_be_loc_<h>-<h>'}, ...
	'space',0.5, ...
	'min',0, ...
	'max',2 ...
);
