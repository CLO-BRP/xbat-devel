function [result, context] = prepare(parameter, context)
result = struct;

% PRESENCE - prepare


%% FIND FIRST DATE AND LAST DATE IN LOGS
%% INITIALIZE NumEvents
%% REFRESH LOG NAME AND PATH AND/OR CHECK DATE-TIME ATTRIBUTE IF REQUESTED

%initializations
context.state.kill = 0;

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
FirstDate = inf;
LastDate = 0;

for i = 1:NumLogs

  %determine new log name and display
  CurrentLog = logs{i};

  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);

  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)

    %refresh log name and path if requested
    if parameter.refresh
      % rename file field in log
      fnExt = [fn '.mat'];
      NewLog.file = fnExt;

      % rename path field in log
      LogPath = context.library.path;
      LogPath = [LogPath SoundName '\Logs\'];
      NewLog.path = LogPath;

      % save log
      log_save(NewLog);
    end

    %check date-time attribute if requested
    if parameter.check
      CheckDateTime(NewLog);
    end
    
    %look for first and last date in logs
    FirstDate = min(FirstDate, NewLog.sound.realtime);
    LastDate = max(LastDate, NewLog.sound.realtime);
    
  if isempty(FirstDate) || isempty(LastDate)
    fprintf(2,'Action terminated. Date-time attribute is not set for %s\n\n', fn);
    context.state.kill = 1;
    return;
  end

  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end


%save FirstDate and Last Date
FirstDate = floor(FirstDate);
LastDate = floor(LastDate);
NumDays = LastDate - FirstDate + 1;
context.state.FirstDate = FirstDate;
context.state.LastDate = LastDate;

%initialize NumChannels and stuff into context.state
if isfield(context.sound,'channels')
  NumChan = context.sound.channels;
  context.state.NumChan = NumChan;

  %if multiple sounds selected, context.sound.channels does not exist
else
  fprintf(2,'\nAction failed. Select single sound in XBAT palette and try again.\n');
  context.state.kill = 1;
  return;
end

%cell bug
if iscell(parameter.unit)
  parameter.unit = parameter.unit{1};
end

%if time unit is day, NumBins per day is 1
if strcmp(parameter.unit, 'Daily')
  context.state.NumBins = 1;
  context.state.BinLen = 86400;
  
% if time unit is hour, NumBins per day is 24
elseif strcmp(parameter.unit, 'Hourly')
  context.state.NumBins = 24;
  context.state.BinLen = 3600;
  
%if time unit is minute, NumBins per day is 1440
else
  context.state.NumBins = 1440;
  context.state.BinLen = 60;
  
end
  
%intialize NumEvents
context.state.NumEvents = zeros(NumDays, NumChan, context.state.NumBins);

