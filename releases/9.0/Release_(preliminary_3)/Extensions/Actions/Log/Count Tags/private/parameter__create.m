function parameter = parameter__create(context)

% ADD EVENT MEASUREMENTS - parameter__create

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

% load preset, if readible                    
try    
  load(preset_path)
  
  %warn user if user parameters have changed
  warn_parameter_change( parameter, preset_path)

% if preset not readible, create one
catch
  parameter = make_parameter;
  save(preset_path,'parameter');
  fail( 'preset.mat initialized because it is missing or corrupt.', 'WARNING' )
end


function warn_parameter_change( parameter, preset_path)
%warn user if parameter has changed since last invocation

%re-initialize preset.mat if parameter fields have changed
newfields = setdiff( fieldnames( make_parameter ), fieldnames( parameter ) );
if ~isempty( newfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been added to GUI.';
  d2 = sprintf( '   %s\n', newfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end

oldfields = setdiff( fieldnames( parameter ), fieldnames( make_parameter ) );
if  ~isempty( oldfields )
  parameter = make_parameter;
  save(preset_path,'parameter');
  d1 = 'preset.mat re-intialized because these controls have been removed from GUI.';
  d2 = sprintf( '   %s\n', oldfields{ : } );
  fail( sprintf( '%s\n%s', d1, d2 ), 'WARNING' )
end

function parameter = make_parameter
%initialize parameter if extension has never been invoked

parameter.out_dir = pwd;
parameter.xlsx = 1;