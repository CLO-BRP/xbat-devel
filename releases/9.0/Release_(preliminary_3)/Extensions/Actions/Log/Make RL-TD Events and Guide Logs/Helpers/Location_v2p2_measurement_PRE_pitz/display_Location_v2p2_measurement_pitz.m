function brows_graph_h = display_Location_v2p2_measurement_pitz(h, m, event, ixa, description, data)

%%
%% 'display_Location_v2p2_measurement'
%%
%% display the results of the Location v2.2 Measurement in the browser window
%%
%% Kathryn A. Cortopassi, April 2005
%% --------------------------------------------
%%
%% syntax:
%% ------
%% brows_graph_h = display_Location_v2p2_measurement(h, m, event, ixa, description, data)
%%
%% input:
%% ------
%%  h - handle to the browser figure
%%  m - log index
%%  event - event
%%  ixa - measurement index
%%  description - measurement description
%%  data - figure userdata context
%%
%% output:
%% -------
%%  brows_graph_h - handles to created graphic objects
%%

%%
%% K.A. Cortopassi, July 2006
%% base plot axis extent on search area
%%


version2 = 'v2.2';


%% first plot ghost boxes for corr fun peaks
peak_lags = event.measurement(ixa).value.peak_lags;

%% and then for final location
loc_lags = event.measurement(ixa).value.loc_lags;

if isempty(peak_lags) || isempty(loc_lags)
  %% don't try to plot shadow boxes, and drop out
  brows_graph_h = [];
  return;
end


%% else, plot shadow boxes

%% get frequency scaling
if (strcmp(data.browser.grid.freq.labels, 'kHz'))
  kHz_flag = 1;
else
  kHz_flag = 0;
end

%% get the display frequency range
nyquist = data.browser.sound.samplerate/2;
disp_freq = data.browser.page.freq;
if isempty(disp_freq)
  disp_freq = [0, nyquist];
end

%% handle to the browser axes
browseraxes = data.browser.axes;

%% get the channels being displayed
brows_channels = data.browser.channels((data.browser.channels(:,2) == 1), 1);
brows_channels_off = data.browser.channels((data.browser.channels(:,2) == 0), 1);
num_brows_chan = length(brows_channels);

%% set colors for the ghost boxes
color1 = [.7 .3 .8]; %% purple
color1_highlight = [.95 .95 1];
color2 = [.2 .8 .75]; %% cyan
color2_highlight = [.95 1 .95];

%---pitz

%set color for dotted lines and boxes based on event id

BoxColor = [ ...
  0.7, 0.05, 0.05; ...
	0, 0.7, 0.7 ;...
	0.7, 0, 0.7; ...
	0.8, 0.8, 0; ...
	0, 0, 0; ...
	0.25, 0.25, 0.25; ...
	1, 0, 0.05; ...
	0, 1, 0.05; ...
	0, 0.05, 1; ...
	0.5, 0.05, 0.05; ...
	0.05, 0.5, 0.05; ...
	0, 0.05, 0.5; ...
	1, 0.45, 0.45; ...
	0.45, 1, 0.45; ...
	0.45, 0.45, 1 ...
];

% 	0.75, 0.75, 0.75; ... light gray
% 	0.5, 0.5, 0.5; ...    gray
% 	0.05, 0.7, 0.05; ...  green
% 	0, 0.05, 0.7; ...   blue

NumBoxColor = size(BoxColor, 1);

BoxColor0 = BoxColor(mod(event.id,NumBoxColor) + 1, :);

%---pitz


%% get ref chan and channels used in location
ref_chan = event.channel;
channels_used = event.measurement(ixa).value.channels_used;

%% make a lag vec for all channels used including ref
lags = zeros(size(channels_used));
lags(~(channels_used == ref_chan)) = peak_lags;
peak_lags = lags;
lags(~(channels_used == ref_chan)) = loc_lags;
loc_lags = lags;

%% get rid of the lags for the channels that aren't being displayed
peak_lags(ismember(channels_used, brows_channels_off)) = [];
loc_lags(ismember(channels_used, brows_channels_off)) = [];


arr_inx = 0;
brows_graph_h = [];


regular = 0;
%% plot the graphics in the browser window
for inx = 1:num_brows_chan

  if ismember(brows_channels(inx), channels_used)
    %% plot the ghost event

    axes(browseraxes(inx));
    arr_inx = arr_inx + 1;

    if inx < num_brows_chan
      %       %% draw line connecting bottom of ghost box to bottom of channel pane
      %       %% for corr fun peaks (in purple)
      %       t1 = event.time(1) - peak_lags(arr_inx) + event.duration/2;
      %       t2 = t1;
      %       f1 = event.freq(1);
      %       f2 = disp_freq(1);
      %       x = [t1, t2];
      %       y = [f1, f2];
      %       if (kHz_flag)
      %         y = y / 1000;
      %       end
      %
      %       if regular
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', color1...
      %           );
      %       else
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', 'r'...
      %           );
      %       end
      %       brows_graph_h = [brows_graph_h, lh1];

      %% draw line connecting bottom of ghost box to bottom of channel pane
      %% for calculated location (in cyan)
      t1 = event.time(1) - loc_lags(arr_inx) + event.duration/2;
      t2 = t1;
      f1 = event.freq(1);
      f2 = disp_freq(1);
      x = [t1, t2];
      y = [f1, f2];
      if (kHz_flag)
        y = y / 1000;
      end

      if regular
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', color2...
          );
      else
        
        %---pitz
        
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', BoxColor0 ...
          );
        
%         lh1 = line('xdata', x, ...
%           'ydata', y, ...
%           'linestyle', '--', ...
%           'linewidth', 2.5, ...
%           'color', 'b'...
%           );

        %---pitz

      end
      brows_graph_h = [brows_graph_h, lh1];

    end



    if arr_inx > 1
      %% draw line connecting top of ghost box to top of channel pane
      %       %% for corr fun peaks (in purple)
      %       t1 = event.time(1) - peak_lags(arr_inx) + event.duration/2;
      %       t2 = event.time(1) - peak_lags(arr_inx-1) + event.duration/2;
      %       f1 = event.freq(2);
      %       f2 = disp_freq(2);
      %       x = [t1, t2];
      %       y = [f1, f2];
      %       if (kHz_flag)
      %         y = y / 1000;
      %       end
      %
      %       if regular
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', color1...
      %           );
      %       else
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', 'r'...
      %           );
      %       end
      %
      %       brows_graph_h = [brows_graph_h, lh1];

      %% draw line connecting top of ghost box to top of channel pane
      %% for calculated location (in cyan)
      t1 = event.time(1) - loc_lags(arr_inx) + event.duration/2;
      t2 = event.time(1) - loc_lags(arr_inx-1) + event.duration/2;
      f1 = event.freq(2);
      f2 = disp_freq(2);
      x = [t1, t2];
      y = [f1, f2];
      if (kHz_flag)
        y = y / 1000;
      end

      if regular
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', color2...
          );
      else
        
        %---pitz
        
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', BoxColor0...
          );
        
%         lh1 = line('xdata', x, ...
%           'ydata', y, ...
%           'linestyle', '--', ...
%           'linewidth', 2.5, ...
%           'color', 'b'...
%           );

        %---pitz

      end
      brows_graph_h = [brows_graph_h, lh1];

    end


    if  (brows_channels(inx) ~= ref_chan)
      %       %% plot delay boxes corresponding to corr fun peaks (in purple)
      %       t1 = event.time(1) - peak_lags(arr_inx);
      %       t2 = t1 + event.duration;
      %       f1 = event.freq(1);
      %       f2 = event.freq(2);
      %       if (kHz_flag)
      %         f1 = f1 / 1000;
      %         f2 = f2 / 1000;
      %       end
      %       x = [t1, t1, t2, t2, t1];
      %       y = [f1, f2, f2, f1, f1];
      %
      %       if regular
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', color1...
      %           );
      %       else
      %         lh1 = line('xdata', x, ...
      %           'ydata', y, ...
      %           'linestyle', '--', ...
      %           'linewidth', 2.5, ...
      %           'color', 'r'...
      %           );
      %       end
      %
      %       brows_graph_h = [brows_graph_h, lh1];
      %
      %       %% label the box
      %       if regular
      %         th1 = text(t2, f1, 'Peak');
      %         set(th1, 'color', color1, 'backgroundcolor', color1_highlight, 'fontweight', 'demi');
      %       else
      %         th1 = text(t2, f1, 'PK');
      %         set(th1, 'color', 'r', 'backgroundcolor', [1, .95, .95], 'fontweight', 'demi');
      %       end
      %       brows_graph_h = [brows_graph_h, th1];
      %
      %% plot delay boxes corresponding to calculated location (in cyan)
      t1 = event.time(1) - loc_lags(arr_inx);
      t2 = t1 + event.duration;
      f1 = event.freq(1);
      f2 = event.freq(2);
      if (kHz_flag)
        f1 = f1 / 1000;
        f2 = f2 / 1000;
      end
      x = [t1, t1, t2, t2, t1];
      y = [f1, f2, f2, f1, f1];

      if regular
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', color2...
          );
      else
        
        %---pitz
        
        lh1 = line('xdata', x, ...
          'ydata', y, ...
          'linestyle', '--', ...
          'linewidth', 2.5, ...
          'color', BoxColor0 ...
          );
        
%         lh1 = line('xdata', x, ...
%           'ydata', y, ...
%           'linestyle', '--', ...
%           'linewidth', 2.5, ...
%           'color', 'b'...
%           );

        %---pitz
        
      end
      brows_graph_h = [brows_graph_h, lh1];

      %       %% label the box
      %       if regular
      %         th1 = text(t2, f2, 'BF Loc');
      %         set(th1, 'color', color2, 'backgroundcolor', color2_highlight, 'fontweight', 'demi');
      %       else
      %         th1 = text(t2, f2, 'CSE');
      %         set(th1, 'color', 'b', 'backgroundcolor', [.95, .95, 1], 'fontweight', 'demi');
      %       end
      %       brows_graph_h = [brows_graph_h, th1];

    end


    %     if (~regular) & (brows_channels(inx) == ref_chan)
    %       %% plot delay boxes corresponding to corr fun peaks (in purple)
    %       t1 = event.time(1);
    %       t2 = t1 + event.duration;
    %       f1 = event.freq(1);
    %       f2 = event.freq(2);
    %       if (kHz_flag)
    %         f1 = f1 / 1000;
    %         f2 = f2 / 1000;
    %       end
    %       x = [t1, t1, t2, t2, t1];
    %       y = [f1, f2, f2, f1, f1];
    %
    %       lh1 = line('xdata', x, ...
    %         'ydata', y, ...
    %         'linestyle', '--', ...
    %         'linewidth', 3, ...
    %         'color', [0, .85, .25]...
    %         );
    %       brows_graph_h = [brows_graph_h, lh1];
    %
    %       %% label the box
    %       th1 = text(t2, f1, 'REF');
    %       set(th1, 'color', [0, .75, .25], 'backgroundcolor', [.95, 1, .95], 'fontweight', 'demi');
    %       brows_graph_h = [brows_graph_h, th1];
    %
    %     end


  end


end

%---pitz

return;

%---pitz


%% make the display non-interfering with the interface
set(brows_graph_h,'hittest','off');







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% now plot the locations map

% %% get location and ID info for current event
% x_coord = event.measurement(ixa).value.xlocation;
% y_coord = event.measurement(ixa).value.ylocation;
% z_coord = event.measurement(ixa).value.zlocation;
% %% if these are empty, code will just plot the array with no location showing
% ID = event.id;

%% get array geometry and search area
brows_log = data.browser.log(m);
%arr_geom = brows_log.sound.geometry.local; %% use built-in function to get array geom
%arr_geom = get_geometry(brows_log.sound, 'local'); %% just forget it and use the browser sound
%% since the log sound and browser sound are often out of synch (with the log sound being the
%% one not getting consistently updated)
arr_geom = get_geometry(data.browser.sound, 'local');
search_radius = event.measurement(ixa).parameter.search_radius;

if isempty(arr_geom) || isempty(search_radius)
  %% drop out without plotting
  return;
end

%% determine the xyz limits for the plot based on the array geometry and
%% search radius (the array should come out of the browser sound variable
%% as 3D regardless of how the user entered the attribute data; i.e., if
%% no z-dim was entered, xbat should make z = 0)
arr_center = mean(arr_geom);
x_lim(1) = arr_center(1) - search_radius;
x_lim(2) = arr_center(1) + search_radius;
y_lim(1) = arr_center(2) - search_radius;
y_lim(2) = arr_center(2) + search_radius;
z_lim(1) = arr_center(3) - search_radius;
z_lim(2) = arr_center(3) + search_radius;


%% plot the locations for all events currently showing in the browser screen
eventinx = get_page_events(data, m);
measure_name = event.measurement(ixa).name;
event_inx = 0;
for loop_inx = eventinx' %% notice the transpose to convert to row vector
  meas_inx = find(strcmpi({brows_log.event(loop_inx).measurement.name}', measure_name));
  if (~isempty(meas_inx) && ...
      ~isempty(brows_log.event(loop_inx).measurement(meas_inx).value.xlocation) && ...
      ~isempty(brows_log.event(loop_inx).measurement(meas_inx).value.ylocation))
    %% event has loc measure and at least an x-y location estimate
    event_inx = event_inx + 1;
    all_IDs(event_inx) = brows_log.event(loop_inx).id;
    all_x_coords(event_inx) = brows_log.event(loop_inx).measurement(meas_inx).value.xlocation;
    all_y_coords(event_inx) = brows_log.event(loop_inx).measurement(meas_inx).value.ylocation;
    %% watch for empty z values
    if (isempty(brows_log.event(loop_inx).measurement(meas_inx).value.zlocation))
      all_z_coords(event_inx) = arr_center(3);
    else
      all_z_coords(event_inx) = brows_log.event(loop_inx).measurement(meas_inx).value.zlocation;
    end
    %% flag with 1 any 3D locations
    all_search_dim3D(event_inx) = strcmpi('3d', brows_log.event(loop_inx).measurement(meas_inx).parameter.search_dim);
  end
end


%% plot the locations in a common plot, with browser and log specific tag
lbwh = get(0, 'screensize');
fh1 = findobj(0, 'tag', ['Location_', version2, '_Array&Location_Measurment_Display_BROWS', num2str(h), '_LOG', num2str(m)]);
if event_inx && isempty(fh1)
  fh1 = figure;
  fpos = [5, 38, 550, 500];
  %fpos = [5, lbwh(4) - 380, 400, 350];
  %fpos = [5, lbwh(4) - 580, 600, 550]; %% for demo!!!!

  set(fh1, 'tag', ['Location_', version2, '_Array&Location_Measurment_Display_BROWS', num2str(h), '_LOG', num2str(m)],...
    'numbertitle', 'off',...
    'name', 'Array & Location',...
    'units', 'pixels',...
    'position', fpos,...
    'color', [.975 .975 .975],...
    'menubar', 'none',...
    'resize', 'on');
  %'menubar', 'figure');
elseif ~event_inx && ~isempty(fh1)
  %% get rid of the display
  %% hey, this is useless; this will never be seen
  %% cause display_measure won't be called if there
  %% are no located events on the screen
  close(fh1);
  return;
end

%% set up axes
ah1 = findobj(fh1, 'tag', ['Location_', version2, '_Array&Location_Measurment_Display_AX1']);
if event_inx && isempty(ah1)
  %if strcmpi(brows_log.sound.type, 'File Stream') %% just forget it and use the browser sound
  if strcmpi(data.browser.sound.type, 'File Stream')
    %sound_name = basenameof(brows_log.sound.path(1:end-1)); %% just forget it and use the browser sound
    sound_name = basenameof(data.browser.sound.path(1:end-1));
  else
    %sound_name = brows_log.sound.file; %% just forget it and use the browser sound
    sound_name = data.browser.sound.file;
  end
  apos = [0.2, 0.14, 0.7, 0.675];
  %apos = [0.11, 0.09, 0.8, 0.825]; %% for demo!!
  %% make sure axes go into right figure, and font is as we like, etc
  ah1 = axes('parent', fh1,...
    'tag', ['Location_', version2, '_Array&Location_Measurment_Display_AX1'],...
    'units', 'normal',...
    'position', apos,...
    'color', [.95 .95 .95],...
    'box', 'on',...
    'fontname', 'arial',...
    'fontweight', 'demi',...
    'fontsize', 10);
  th1(1) = get(ah1, 'title');
  th1(2) = get(ah1, 'xlabel');
  th1(3) = get(ah1, 'ylabel');
  th1(4) = get(ah1, 'zlabel');
  set(th1(1), 'string', sprintf('Source Location Measurement %s\nSound : ''%s''\nLog : ''%s''\n', version2, sound_name, brows_log.file));
  set(th1(2), 'string', 'X (meters)');
  set(th1(3), 'string', 'Y (meters)');
  set(th1(4), 'string', 'Z (meters)');
  set(th1, 'parent', ah1, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);
  set(th1(1), 'fontsize', 10);
  clear th1;
end
delete(get(ah1, 'children'));


%% plot element numbers
for loop_inx = 1:size(arr_geom, 1)
  th1(loop_inx) = text('parent', ah1, 'string', [' ', num2str(loop_inx)], 'position', [arr_geom(loop_inx, 1), arr_geom(loop_inx, 2), arr_geom(loop_inx, 3)]);
end
%set(th1, 'color', 'k', 'backgroundcolor', [1 .8 .8], 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11, 'horizontalalignment', 'left', 'verticalalignment', 'bottom');
set(th1, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11, 'horizontalalignment', 'left', 'verticalalignment', 'bottom');
clear th1;

%% plot array
lh1 = line(arr_geom(:, 1), arr_geom(:, 2), arr_geom(:, 3), 'parent', ah1);
%set(lh1, 'marker', 'p', 'markersize', 11, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');
set(lh1, 'marker', 'o', 'markersize', 9, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');
clear lh1;

%% plot line through array center
lh1(1) = line([arr_center(1), arr_center(1)], [y_lim(1), y_lim(2)], [arr_center(3), arr_center(3)], 'parent', ah1);
lh1(2) = line([arr_center(1), arr_center(1)], [arr_center(2), arr_center(2)], [z_lim(1), z_lim(2)], 'parent', ah1);
lh1(3) = line([x_lim(1), x_lim(2)], [arr_center(2), arr_center(2)], [arr_center(3), arr_center(3)], 'parent', ah1);
set(lh1, 'linestyle', ':', 'marker', 'none', 'color', 'k');
clear lh1;



if event_inx

  %% plot event IDs and locations
  for loop_inx = 1:event_inx
    th1(loop_inx) = text('parent', ah1, 'string', [' ', num2str(all_IDs(loop_inx))], 'position', [all_x_coords(loop_inx), all_y_coords(loop_inx), all_z_coords(loop_inx)]);
  end
  %set(th1, 'color', 'k', 'backgroundcolor', [.9 .9 1], 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 9, 'horizontalalignment', 'left', 'verticalalignment', 'top');
  set(th1, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 9, 'horizontalalignment', 'left', 'verticalalignment', 'top');
  clear th1;

  %% plot locations
  lh1 = line(all_x_coords, all_y_coords, all_z_coords, 'parent', ah1);
  set(lh1, 'marker', 'p', 'markersize', 9, 'markerfacecolor', 'r', 'markeredgecolor', 'b', 'linestyle', 'none');
  %   lh2 = line(all_x_coords, all_y_coords, all_z_coords, 'parent', ah1);
  %   set(lh1, 'marker', '*', 'linestyle', 'none', 'color', 'b');
  %   set(lh2, 'marker', 'o', 'linestyle', 'none', 'color', 'r');
  %   clear lh1;
  %   clear lh2;

end

%% set the xy limits
set(ah1, 'xlim', x_lim, 'ylim', y_lim, 'zlim', z_lim);

if any(all_search_dim3D)
  %% make plot 3D
  pad = search_radius/5;
  set(ah1, 'cameratarget', arr_center,...
    'cameraposition', [x_lim(1)+pad, y_lim(1), z_lim(2)+pad],...
    'cameraupvector', [0, 0, 1]);
  rotate3d(fh1, 'on');
else
  %% make plot 2D
  set(ah1, 'cameratarget', arr_center,...
    'cameraposition', arr_center+[0, 0, 1],...
    'cameraupvector', [0, 1, 0]);
  rotate3d(fh1, 'off');
end


%% finish
figure(fh1);
drawnow;


return;