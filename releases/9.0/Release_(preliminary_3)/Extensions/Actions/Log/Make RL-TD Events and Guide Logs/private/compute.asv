function [result, context] = compute(log, parameter, context)
result = struct;

%% Make RL-TD Events and Guide Logs - compute
%
% Purpose: Exports time delays in RL-TD measurements as XBAT events so that 
% they can be selectively deleted (�cleaned�).
% 
% Input: one or more RL-TD logs.  These can be in a single �Logs� folder in 
% an XBAT library, or spread over many �Logs� folders in an XBAT library.  
% 
% Output 1: One �bestNevents� log for each RL-TD log processed.  These logs
% 
% have the top-scoring N time delays for each event in the RL-TD log exported 
% as XBAT events.
% 
% Output 2: One �bestNguide� log for each RL-TD log processed.  These logs 
% have RL-TD measurements for the top-scoring N time delays for each event 
% in the RL-TD log converted to CSE Locator Tool measurements.
% 
% User Parameter 1: �Include Events In Channels� � Select all channels you 
% want to use for analysis.  Do not select channels with no sound or bad sound.
% 
% User Parameter 2: �Number of Events to Keep� � Select number of time delays 
% to be exported per RL-TD event.  Note that this number does not include 
% the RL-TD event itself.
% 
% Tip 1: CSE Locator Tool must be installed for the �bestNguide� logs to 
% display time delays.  
% 
% Tip 2: The �bestNevents� logs must be modified only be tagging and/or deleting 
% events.  Any events that are added to the log, or any events for which 
% the Notes field has been altered, will be discarded.


%%
if context.state.kill
  return;
end

%initializations
len = log.length;
fn = log.file;
fn = fn(1:end-4);

%skip log if it has no events
if isequal(log.length, 0)
  fprintf(1,'WARNING: %s skipped because it has no events.\n', fn);
  return;
end

KeepChannels = parameter.KeepChannels; %ignore channels not in vector
NumBest = parameter.NumBest;     %Number of events to be saved for each notes

NumChans = length(KeepChannels);
KeepChannelsVector = str2double(KeepChannels);
KeepChannelsVector = KeepChannelsVector';

% create empty event array template for RLTD_event log
EventTemplate = event_create;
EventTemplate.level = 1;
User = get_active_user;
EventTemplate.author = User.name;

num_events = NumChans * len;

RLTD_event(1:num_events) = EventTemplate;

%copy events for guide log
guide_log_event = RLTD_event;

loc_template = fake_loc_create(KeepChannels);

%for each event in log from RL_TD log
k = 0;
ii = 0;

for i = 1:len
  
  ID = log.event(i).id;
  
  %if event has RL-TD measurement, export time delays as events in XBAT log   
%   if isfield(log.event(i), 'measurement') ...
%       && isfield(log.event(i).measurement, 'value') ...
%       && isfield(log.event(i).measurement.value, 'all_pk_xcorr_norm') ...
%       && ~isempty(log.event(i).measurement.value.all_pk_xcorr_norm)
  try
    
    %stop processing if multiple measurements
    if length(log.event(i).measurement) > 1
      
      disp(' ')
      fprintf(2,'Log skipped: "%s"\n', log_name(log));
      fprintf(2,'Event %.0f has more than one type of measurement.\n',log.event(i).id);
      disp(' ')
      
      h = warndlg(sprintf('Log skipped: "%s"', log_name(log)), 'WARNING');
      
      movegui(h, 'center');
      
      return;
      
    end
    
    %stop processing if event has CSE Location measurement
    if ~strcmp(log.event(i).measurement.name, 'RL-TD Measurement')
      
      disp(' ')
      fprintf(2,'Log skipped: "%s"\n', log_name(log));
      fprintf(2,'Event %.0f has a %s measurement rather than an RL-TD measurement.\n\n', ...
        log.event(i).id ...
        log.event(i).measurement.name,);
      disp(' ')
      
      h = warndlg(sprintf('Log skipped: "%s"', log_name(log)), 'WARNING');
      
      movegui(h, 'center');
      
      return;
      
    end
      
      
    %stop processing if already processed with "Cull RLTD Measurements" log action
    if any(isnan(log.event(i).measurement.value.all_time))
      
      disp(' ')
      fprintf(2,'Log skipped: "%s"\n', log_name(log));
      fprintf(2,'Event %.0f already has some time delays deleted.\n',log.event(i).id);
      disp(' ')
      
      h = warndlg(sprintf('Log skipped: "%s"', log_name(log)), 'WARNING');
      
      movegui(h, 'center');
      
      return;
      
    end
    
    %if there are any RLTD time delays to export
    if ~isempty(log.event(i).measurement.value.all_time)
    
      duration = log.event(i).duration;
      freq = log.event(i).freq;
      bandwidth = log.event(i).bandwidth;
      channel = log.event(i).channel;

      %find index for N channels with highest XC
      [temp IDX] = sort(log.event(i).measurement.value.all_pk_xcorr_norm,'descend');
      temp = temp(ismember(IDX, KeepChannelsVector));
      IDX = IDX(ismember(IDX, KeepChannelsVector));
      IDX = IDX(~isnan(temp));
      channels_used = IDX(1:NumBest+1);  %add 1 because index channel doesn't count

      %create new event on each channel use time delays
      for j = channels_used(2:end)

        k = k + 1;
        all_time = log.event(i).measurement.value.all_time(j);

        %ID of new event
        RLTD_event(k).id = k;

        %find time bounds of time delay events
        RLTD_event(k).time = [all_time, all_time + duration];
        RLTD_event(k).duration = duration;

        %frequency bounds same as index event
        RLTD_event(k).freq = freq;
        RLTD_event(k).bandwidth = bandwidth;

        %channel is index j
        RLTD_event(k).channel = j;

        %event score is normalized cross-correlation for time delay events
        RLTD_event(k).score = log.event(i).measurement.value.all_pk_xcorr_norm(j);

        %tag events with ID of index event and time delay relative to index
        %event
        IDs = ['Ref: ID ' num2str(ID)];

        ChanPair = ['Ch ' num2str(j) '-' num2str(channel)];

        TD = all_time - log.event(i).measurement.value.all_time(channel);

        if j>channel
          TD = - TD;
        end
        TDs = ['TD ' num2str(TD)];

        notes = {[IDs ', ' ChanPair ', ' TDs]};
        RLTD_event(k).notes(1,1) = notes;

      end


      %create event for guide log
      ii = ii + 1;
      guide_log_event(ii) = log.event(i);

      loc = loc_template;
      loc.value.channels_used = sort(channels_used);

      t = log.event(i).time(1);
      all_time_used = log.event(i).measurement.value.all_time(sort(channels_used(2:end)));
      loc.value.peak_lags = t - all_time_used; 
      loc.value.loc_lags = loc.value.peak_lags';

      guide_log_event(ii).measurement = loc;
    end

  catch err

    if strcmp(err.identifier, 'MATLAB:extraneousStrucRefBlocks')

      fprintf(2,'**********************************************************\n\n');
      fprintf(2,'%s\n\n',fn);
      fprintf(2,'ERROR: More than one type of measurement in log.\n\n');
      fprintf(2,'Run this action on logs with only RL-TD measurements.\n\n');      
      fprintf(2,'**********************************************************\n\n');

      context.state.kill = 1;

      return;

    elseif strcmp(err.identifier, 'MATLAB:minrhs')

      fprintf(1,'\nWARNING: Event %.0f in %s has no measurements and was skipped.\n',ID,fn);

    else

      rethrow(err);

    end
  end
        
%   else
    
%     fprintf(2,'Event %.0f in %s has no measurements and was skipped.\n',ID,fn);
    
%   end
end
 
%%

if ~k

  fprintf(1,'\n**********************************************************\n\n');
  fprintf(1,'WARNING: %s has no measurements.\n\n', fn);
  fprintf(1,'Therefore, output logs have no events.\n\n');      
  fprintf(1,'**********************************************************\n\n');
  
end

%create RL-TD_events log
fnRLTD = sprintf('%s_best%.0fevents', fn, NumBest);
RLTD_events_log = new_log(fnRLTD, context.user, context.library, context.sound);
% RLTD_events_log = new_log(fnRLTD, context.user, context.library, log_sound);
RLTD_events_log.event = RLTD_event(1:k);
RLTD_events_log.length = k;
RLTD_events_log.curr_id = k + 1;
RLTD_events_log.color = [0.05, 0.7, 0.05];   %green
RLTD_events_log.sound = log.sound;
log_save(RLTD_events_log);

%create guide log
fnGUIDE = sprintf('%s_best%.0fguide', fn, NumBest);
guide_log = new_log(fnGUIDE, context.user, context.library, context.sound);
% guide_log = new_log(fnGUIDE, context.user, context.library, log_sound);
guide_log.event = guide_log_event(1:ii);
guide_log.length = ii;
guide_log.curr_id = ii + 1;
guide_log.color = [0 0 1];   %blue
guide_log.sound = log.sound;
log_save(guide_log);


function loc_template = fake_loc_create(KeepChannels)

%populate fake loc measurement for guide log
loc_template.name = 'Source Location (v2.2)';
loc_template.fun = @Location_v2p2_measurement_PRE_pitz;
User = get_active_user;
loc_template.author = User.name;
loc_template.created = now;
loc_template.modified = [];
loc_template.value = [];
loc_template.parameter = [];
loc_template.userdata = [];
loc_template.value.xlocation = 1;
loc_template.value.ylocation = 1;
loc_template.value.zlocation = [];

loc_template.value.xyz_corr_sum = [];
loc_template.value.all_locations = [];

loc_template.value.cent_dist = [];
loc_template.value.iterations = [];

loc_template.value.channels_used = [];  %needs unique vector for each event

loc_template.value.peak_vals = [];
loc_template.value.channels_wts = [];
loc_template.value.peak_vals_sm = [];

loc_template.value.peak_lags = []; %needs unique vector for each event
loc_template.value.loc_lags = []; %needs unique vector for each event

loc_template.value.range = [];
loc_template.value.bearing = [];
loc_template.value.elevation = [];
loc_template.value.dist2channels = [];


parameter.corr_type = 'Time Waveform';
parameter.corr_num = 'N(N-1)/2';
parameter.abs_pk_thresh = 0.1000;
parameter.rel_pk_thresh = 50;
parameter.min_channels = 3;
parameter.max_channels = length(KeepChannels);
parameter.channels = KeepChannels;
parameter.search_type = 'grid';
parameter.search_radius = 40000;
parameter.term_crit = 100;
parameter.mean_type = 'arithmetic';
parameter.search_dim = '2D';
parameter.display_plots = 'None';
parameter.spc_fft = 512;
parameter.spc_hop = 0.2500;
parameter.spc_win_type = 'Hann';
parameter.spc_win_length = 1;

loc_template.parameter = parameter;
  
