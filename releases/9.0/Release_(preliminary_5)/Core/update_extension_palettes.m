function pals = update_extension_palettes(par, data)

% update_extension_palettes - update extension palettes when logs change
% ----------------------------------------------------------------------
%
% pals = update_extension_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 29 Mar 2014
%		Add listener for "Browse" palette.
%   msp2 - 21 May 2014
%       Browse palette:
%           Adapt to html tags in tooltip.
%           Remove log name from tooltip when log is closed in browser.
%   msp2 - 23 May 2014
%       Move Browse palette update into update_browse_palette.
%   msp2 - 1 June 2014
%       Enable detector scan button only if both log and preset are loaded.
%   msp2 - 10 June 2014
%       Get rid of listener for "user_log", since that messes up filter extensions.

%----------------------
% HANDLE INPUT
%----------------------

%--
% check browser handle input
%--
if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--
if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%----------------------
% TEST CODE
%----------------------

%--
% get listener control values
%--
no_log = isempty(data.browser.log);
if no_log
	lognames = {'(No Open Logs)'}; 
    ix = 1;
else
	lognames = file_ext(struct_field(data.browser.log, 'file')); 
    ix = data.browser.log_active;
end
		
%--
% get listener names and palette handles
%--
% NOTE: not all of these listeners are currently in use
listener = {'output_log', 'noise_log', 'source_log', 'template_log' };

% NOTE: this is critical, 'get_palette' only gets registered palettes, is should use this function!
pal = get_xbat_figs('type', 'palette', 'parent', par);

%--
% look for listener controls in available palettes
%--
pals = [];
for k = 1:length(pal)
    
    update_flag = 0;
	
	for j = 1:length(listener)
		
		%--
		% look for listener in palette
		%--
		control = get_control(pal(k), listener{j});		
		if isempty(control)
			continue;
		end
		
		%--
		% update listening control values and state
		%--
		old_value = get_control(pal(k), listener{j}, 'value');		
		set(control.handles.obj, 'String', lognames, 'Value', ix );
		
		%--
		% try to keep old value for noise log
		%--		
        if strcmp(listener{j}, 'noise_log');
			set_control(pal(k), listener{j}, 'value', old_value);
        end      
        update_flag = 1;		
	end
    
    %---
    % Switch source palettes
    %---
    info = parse_tag( get( pal( k ), 'tag' ), '::' ,{ 'type', 'subtype', 'name' } );
    
    %--
    % Sound Detectors: set string and enable/disable for scan button
    %--
    if strcmp( info.subtype, 'SOUND_DETECTOR' )
        
        % set string and enable/disable for scan button in sound detector palettes

        % determine whether or not a preset is loaded
        templates = get_control( pal( k ), 'template_select', 'value' );
        no_preset = isempty( templates ) || strcmp( templates, '(No Available Templates)' );
        
        % determine whether button should be enabled or disabled
        if no_log || no_preset
            scan_enable = 'off';
            scan_str = 'Open Log and Preset to Activate';
        else
            scan_enable = 'on';
            scan_str = 'Start';
        end
    
        % set label and enable/disable
		control = get_control( pal( k ), 'scan' );
        if isempty( control )
			continue;
        end
		set( control.handles.obj, 'String', scan_str, 'Enable', scan_enable );
    end
    
	if update_flag
		%--
		% add palette to list of updated palettes
		%--
		pals(end + 1) = pal(k); 
	end
end
