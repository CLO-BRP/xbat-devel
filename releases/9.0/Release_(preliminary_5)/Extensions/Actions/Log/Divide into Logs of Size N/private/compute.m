function [result, context] = compute(log, parameter, context)
result = struct;

% Divide Log into Logs of Size N - compute

if context.state.kill
  return;
end

%abort if log open
if log_is_open(log)
    disp(['Close log ''', log_name(log), ' before randomizing it.']); return;
end

%initializations
n = parameter.n;
% nStr = num2str(n);

len = log.length;
curr_id = log.curr_id;

nNewLogs=ceil(len/n);
Event = log.event;

%find log name
fnLog = log.file;
fnLog = fnLog(1:end-4);

%log name format
LogNameFormat = sprintf('%%s_%%.0f_%%0%.0f.0f', ceil(log10(nNewLogs+1)));

%break randomized log into nNewLogs logs of size n
for i=1:nNewLogs

  %make new empty log
  NewLogName = sprintf(LogNameFormat, fnLog, n, i);  %log name
  
%   iStr = num2str(i);
%   NewLogName = [fnLog '_' nStr '-' iStr];  %og name

  NewLog = new_log(NewLogName,context.user,context.library,context.sound);

  %get sound struct from parent log, if requested
  if strcmp(parameter.SoundSource,'Parent Log')
    NewLog.sound = log.sound;
  end

  %make selection of events and add to new log

  first = n*(i-1)+1;
  last = n*i;
  if last > len
    last = len;
  end
  NewLog.event = Event(first:last);
  NewLog.length = last-first+1;
  NewLog.curr_id = curr_id;

  %save new log
  log_save(NewLog);
end
