function [result, context] = prepare(parameter, context)
result = struct;

% Filter Channel - prepare

% list names of selected logs
context.state.kill = 0;

if isfield(context, 'target')   %check if context.target is available  
  logs = context.target;  
else  
  fprintf(2,'API supplied no context.target in prepare.m \n');  
  context.state.kill = 1;  
  return;  
end

if ~iscell(logs)
  logs = {logs};
end

%initialize waitbar
tstart = tic;
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );

% for each log
NumLogs = length(logs);

for i = 1:NumLogs
    
    %indicate log being processed in waitbar
    t0 = tic;
    waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...'] );
  
    %determine new log name and display  
    CurrentLog = logs{i};

    % load log structure  
    [SoundName, fn] = fileparts(CurrentLog);  
    log = get_library_logs('logs',[],SoundName,CurrentLog);

    %check if a single, valid log is specified
    if ~(ischar(SoundName) && ischar(fn) && isequal(length(log),1))    
        fprintf(2,'\n\nAPI supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');    
        context.state.kill = 1;    
        return;    
    end

    %check log is associated with correct sound in library
    if ~strcmp(sound_name(log.sound), SoundName)    
        fprintf(2,'\n\nLog is not in correct sound in XBAT library \n');    
        fprintf(2,'   Log Name:      %s\n', fn);    
        fprintf(2,'   Correct Sound: %s\n', sound_name(log.sound));    
        fprintf(2,'   Library Sound: %s\n\n', SoundName);    
        context.state.kill = 1;    
        return;    
    end

    % rename file field in log
    fnExt = [fn '.mat'];
    log.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    log.path = LogPath;
    compute_substitute(log, parameter, context)

    %display log processed in waitbar
    result.target = log;
    result.status = 'done';
    result.message = '';
    result.error = [];
    result.elapsed = toc( t0 );
    result.output = struct( [] );
    action_waitbar_update( wh(1), i/NumLogs, result, 'log' ); 
end

%waitbar complete
d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc(tstart) );
waitbar_update( wh(1), 'PROGRESS', 'message', d );

%avoid memory leak by prevent execution of compute
error( 'No error' )

%%
function compute_substitute( log, parameter, context )

% Makes new log which includes only events from user-selected channels
if context.state.kill  
  return;  
end

% initializations
KeepChannels = parameter.KeepChannels;
KeepChannelsVector = str2double( KeepChannels );
KeepChannelsVector = KeepChannelsVector';

%initializations
len = log.length;
fn = log.file;
fn = fn( 1:end-4 );

%skip log if it has no events
if isequal( log.length, 0 )  
  fprintf (2, '%s has no events\n\n', fn );  
  return;  
end

if strcmp( parameter.single_channel, 'Selected channels output to same log' )

    %for each event in log
    j = 0;
    NewEvent = event_create;
    NewEvent( 1:len ) = NewEvent;

    for i = 1:len

      %copy events in selected channels  
      if any( KeepChannelsVector == log.event( i ).channel )    
        j = j + 1;    
        NewEvent(j) = log.event(i);
      end
    end

    % log name for new log
    suffix = num2str( sprintf( '%.0f-', KeepChannelsVector ) );
    suffix = suffix( 1:end-1 );
    if strcmp( parameter.suffix, 'Change Log in Place (no suffix)' )  
      NewLog = log;  
    else
      fnNew = [ fn '_KeepChans-' suffix ];
      NewLog = new_log( fnNew, context.user, context.library, log.sound );  
    end

    %add events to log
    NewLog.event = NewEvent( 1:j );
    NewLog.length = j;
    NewLog.curr_id = j+1;
    NewLog.sound = log.sound;

    %save log
    log_save( NewLog );
    
elseif strcmp( parameter.single_channel, 'Selected channels output to single-channel log(s)' )
    
    %for each selected channel
    for chan = KeepChannelsVector
        
        %find which events are in current channel
        chan_mask = ( [ log.event.channel ] == chan );
        
        %if there are no events in current channel, continue to next channel
        num_events = sum( chan_mask );
        if ~num_events
            fprintf( '\nWARNING: No events in channel %.0f for %s\n', chan, fn );
            continue;
        end
        
        %create log
        fnNew = sprintf( '%s_KeepChan-%02.0f', fn, chan );
        NewLog = new_log( fnNew, context.user, context.library, log.sound );  
        NewLog.event = log.event( chan_mask );
        NewLog.length = num_events;
        NewLog.curr_id = max( [ NewLog.event.id ] ) + 1;
        log_save( NewLog );
    end    
end




