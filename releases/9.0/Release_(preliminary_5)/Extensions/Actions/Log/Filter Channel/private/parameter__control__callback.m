function result = parameter__control__callback(callback, context)
% FILTER CHANNEL - parameter__control__callback
result = struct;
name = callback.control.name;
if strcmp( name, 'single_channel' )
   value = get( callback.control.handle, 'Value' );
   if isequal( value, 1 )
       set_control( callback.pal.handle, 'suffix', 'Enable', 1 );
   else
       set_control( callback.pal.handle, 'suffix', 'Value', 1 );
       set_control( callback.pal.handle, 'suffix', 'Enable', 0 );
   end
end