function [result, context] = conclude(parameter, context)
result = struct;

% PRESENCE - conclude

%% DAILY CALL COUNT - conclude
%
% Tabulates number of events with a user-definable tag for each channel of
% each selected sound.
%
% Useful for determining daily presence/absence of Fin Whales and other species.
%
%%
%terminate if fatal condition encountered
if context.state.kill
  return;
end

%intializations
NumEvents = context.state.NumEvents;
NumChannels = context.sound.channels;
FirstDate = context.state.FirstDate;
LastDate = context.state.LastDate;
NumDays = LastDate - FirstDate + 1;
NumBins = context.state.NumBins;
BinLen = context.state.BinLen;
binDTint = BinLen / 86400;  

output = parameter.output;
%fix cell bug
if iscell(output)
  output = output{1};
end

unit = parameter.unit;
%cell bug
if iscell(unit)
  unit = unit{1};
end

%% OUTPUT TO MATLAB COMMAND WINDOW

%Write header to Command Window
disp(' ')
if ~isempty(parameter.tag)
  disp(sprintf('Number of events with tag (case insensitive): %s',parameter.tag))
end

fprintf(1,'Number of channels: %.0f\n\n\n',NumChannels);

%% output number of events per unit of analysis, if requested
if strcmp(output, 'NumEvents') || strcmp(output,'Presence_and_NumEvents')

  %display header
  fprintf(1,'Date         Time Bin  ');
  for channel = 1:NumChannels
    fprintf(1,'%6.0f', channel)
  end
  fprintf(1,'     SUM\n\n');  

  binDT = 0;
  
  %display data
  for day = 1:NumDays
    for bin = 1:NumBins

      fprintf(1,'%s', datestr(FirstDate + day - 1));
      fprintf(1,'  %s-%s',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
      fprintf(1,'%6.0f', NumEvents(day,:,bin));
      fprintf(1,'%8.0f', sum(NumEvents(day,:,bin)));
      fprintf(1,'\n');

      binDT = binDT + binDTint;

    end
    
    if ~strcmp(unit, 'Daily')
      fprintf(1,'\n');
    end
    
  end
  
  %display sums
  fprintf(1,'SUM                     ');
  ChanSum = sum(sum(NumEvents,1),3);
  fprintf(1,'%6.0f', ChanSum);
  fprintf(1,'%8.0f', sum(ChanSum));
  fprintf(1,'\n\n\n');
  
end

%% output summary of number of events per unit of analysis, if requested
if strcmp(output, 'NumEvents') || strcmp(output,'Presence_and_NumEvents')

  %display header
  fprintf(1,'Date Range                 Time Bin  ');
  for channel = 1:NumChannels
    fprintf(1,'%6.0f', channel)
  end
  fprintf(1,'     SUM\n\n');  

  binDT = 0;
  
  %display data
  for bin = 1:NumBins

    fprintf(1,'%s - %s', datestr(FirstDate), datestr(LastDate));
    fprintf(1,'  %s-%s',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
    fprintf(1,'%6.0f', sum(NumEvents(:,:,bin),1));
    fprintf(1,'%8.0f', sum(sum(NumEvents(:,:,bin),1),2));
    fprintf(1,'\n');

    binDT = binDT + binDTint;

  end
  
  %display sums
  fprintf(1,'\n');
  fprintf(1,'SUM                                   ');
  ChanSum = sum(sum(NumEvents,1),3);
  fprintf(1,'%6.0f', ChanSum);
  fprintf(1,'%8.0f', sum(ChanSum));
  fprintf(1,'\n\n\n');
  
end

%% output presence per unit of analysis, if requested
if strcmp(output, 'Presence')  || strcmp(output,'Presence_and_NumEvents')

  %display header
  fprintf(1,'Date         Time Bin  ');
  for channel = 1:NumChannels
    fprintf(1,'%6.0f', channel)
  end
  fprintf(1,'     SUM\n\n');  

  %display data
  binDT = 0;
  
  
  for day = 1:NumDays
    for bin = 1:NumBins

      fprintf(1,'%s', datestr(FirstDate + day - 1));
      fprintf(1,'  %s-%s',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
      fprintf(1,'%6.0f', NumEvents(day,:,bin) > 0);
      fprintf(1,'%8.0f', sum(NumEvents(day,:,bin) > 0));
      fprintf(1,'\n');

      binDT = binDT + binDTint;

    end
    
    if ~strcmp(unit, 'Daily')
      fprintf(1,'\n');
    end
    
  end

  %display sums  
  fprintf(1,'SUM                     ');
  ChanSum = sum(sum(NumEvents > 0,1),3);
  fprintf(1,'%6.0f', ChanSum);
  fprintf(1,'%8.0f', sum(ChanSum));
  fprintf(1,'\n\n\n');
   
end

%% output summary of presence per unit of analysis, if requested
if strcmp(output, 'Presence')  || strcmp(output,'Presence_and_NumEvents')

  %display header
  fprintf(1,'Date Range                  Time Bin  ');
  for channel = 1:NumChannels
    fprintf(1,'%6.0f', channel)
  end
  fprintf(1,'     SUM\n\n');  

  %display data
  binDT = 0;
  
  
  for bin = 1:NumBins

    fprintf(1,'%s - %s', datestr(FirstDate), datestr(LastDate));
    fprintf(1,'  %s-%s',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
    fprintf(1,'%6.0f', sum(NumEvents(:,:,bin) > 0,1));
    fprintf(1,'%8.0f', sum(sum(NumEvents(:,:,bin) > 0,1),2));
    fprintf(1,'\n');

    binDT = binDT + binDTint;

  end

  %display sums
  fprintf(1,'\n');
  fprintf(1,'SUM                                   ');
  ChanSum = sum(sum(NumEvents > 0,1),3);
  fprintf(1,'%6.0f', ChanSum);
  fprintf(1,'%8.0f', sum(ChanSum));
  fprintf(1,'\n\n\n');
   
end

%% output NumEvents to CSV if requested

if parameter.csv && ...
    (strcmp(output, 'NumEvents') || strcmp(output,'Presence_and_NumEvents'))

  %Find output file path (Logs folder of selected sound)
  LibPath = context.library.path;
  SoundName = sound_name(get_active_sound);
  SoundPath = [LibPath SoundName '\Logs\'];

  %Output File Name = <LibraryName>_DailyPresence.csv
  LibraryName = context.library.name;
  fn = [LibraryName '_' unit 'NumEvents_' datestr(now,30) '.csv'];

  %Open output file
  fid = fopen([SoundPath fn], 'w');

  if strcmp(unit, 'Daily')
    fprintf(fid,'Number of Events for Each Channel-Day\n');
    
  elseif strcmp(unit, 'Hourly')
    fprintf(fid,'Number of Events for Each Channel-Hour\n');
    
  elseif strcmp(unit, 'Minutely')
    fprintf(fid,'Number of Events for Each Channel-Minute\n');
    
  end
  
  if ~isempty(parameter.tag)
    fprintf(fid,'Include tag (case insensitive): %s',parameter.tag);
  end
  fprintf(fid,'\n');
  
  fprintf(fid,'Run Date and Time:  %s\n\n',datestr(now));
  
  fprintf(fid,'Number of channels: %.0f\n\n\n',NumChannels);

  fprintf(fid,',,,Channel\n');
  fprintf(fid,'Date,Begin Time,End Time,');
  for channel = 1:NumChannels
    fprintf(fid,'%.0f,', channel);
  end
  fprintf(fid,'SUM\n\n');  
  
  binDT = 0;
  
  
  %display data
  for day = 1:NumDays
    for bin = 1:NumBins

      fprintf(fid,'%s,', datestr(FirstDate + day - 1));
      fprintf(fid,'%s,',datestr(binDT,'HH:MM'));     
      fprintf(fid,'%s,',datestr(binDT + binDTint,'HH:MM'));
      fprintf(fid,'%.0f,', NumEvents(day,:,bin));
      fprintf(fid,'%.0f', sum(NumEvents(day,:,bin)));
      fprintf(fid,'\n');

      binDT = binDT + binDTint;

    end
    
    if ~strcmp(unit, 'Daily')
      fprintf(fid,'\n');
    end
    
  end
  
  %display sums
  fprintf(fid,'SUM,,,');
  ChanSum = sum(sum(NumEvents,1),3);
  fprintf(fid,'%.0f,', ChanSum);
  fprintf(fid,'%.0f,', sum(ChanSum));
  fprintf(fid,'\n');
 
  %close output file
  fclose(fid);
  
end

%% output Presence to CSV if requested

if parameter.csv && ...
    (strcmp(output, 'Presence') || strcmp(output,'Presence_and_NumEvents'))

  %Find output file path (Logs folder of selected sound)
  LibPath = context.library.path;
  SoundName = sound_name(get_active_sound);
  SoundPath = [LibPath SoundName '\Logs\'];

  %Output File Name = <LibraryName>_DailyPresence.csv
  LibraryName = context.library.name;
  fn = [LibraryName '_' unit 'Presence_' datestr(now,30) '.csv'];

  %Open output file
  fid = fopen([SoundPath fn], 'w');

  if strcmp(unit, 'Daily')
    fprintf(fid,'Presence for Each Channel-Day\n');
    
  elseif strcmp(unit, 'Hourly')
    fprintf(fid,'Presence for Each Channel-Hour\n');
    
  elseif strcmp(unit, 'Minutely')
    fprintf(fid,'Presence for Each Channel-Minute\n');
    
  end
  
  if ~isempty(parameter.tag)
    fprintf(fid,'Include tag (case insensitive): %s',parameter.tag);
  end
  fprintf(fid,'\n');
  
  fprintf(fid,'Run Date and Time:  %s\n\n',datestr(now));
  
  fprintf(fid,'Number of channels: %.0f\n\n\n',NumChannels);

  fprintf(fid,',,,Channel\n');
  fprintf(fid,'Date,Begin Time,End Time,');
  for channel = 1:NumChannels
    fprintf(fid,'%.0f,', channel);
  end
  fprintf(fid,'SUM\n\n');  
  
  binDT = 0;
  
  
  %display data
  for day = 1:NumDays
    for bin = 1:NumBins

      fprintf(fid,'%s,', datestr(FirstDate + day - 1));
      fprintf(fid,'%s,',datestr(binDT,'HH:MM'));
      fprintf(fid,'%s,',datestr(binDT+binDTint,'HH:MM'));
      fprintf(fid,'%.0f,', NumEvents(day,:,bin)>0);
      fprintf(fid,'%.0f', sum(NumEvents(day,:,bin)>0));
      fprintf(fid,'\n');

      binDT = binDT + binDTint;

    end
    
    if ~strcmp(unit, 'Daily')
      fprintf(fid,'\n');
    end
    
  end
  
  %display sums
  fprintf(fid,'SUM,,,');
  ChanSum = sum(sum(NumEvents > 0,1),3);
  fprintf(fid,'%.0f,', ChanSum);
  fprintf(fid,'%.0f,', sum(ChanSum));
  fprintf(fid,'\n');
 
  %close output file
  fclose(fid);
  
end

%% output Summary of NumEvents to CSV if requested

if parameter.csv && ...
    (strcmp(output, 'NumEvents') || strcmp(output,'Presence_and_NumEvents'))

  %Find output file path (Logs folder of selected sound)
  LibPath = context.library.path;
  SoundName = sound_name(get_active_sound);
  SoundPath = [LibPath SoundName '\Logs\'];

  %Output File Name = <LibraryName>_DailyPresence.csv
  LibraryName = context.library.name;
  fn = [LibraryName '_' unit 'NumEventsSummary_' datestr(now,30) '.csv'];

  %Open output file
  fid = fopen([SoundPath fn], 'w');

  %write header
  if strcmp(unit, 'Daily')
    fprintf(fid,'SUMMARY - Number of Events for Each Channel-Day\n');
    
  elseif strcmp(unit, 'Hourly')
    fprintf(fid,'Number of Events for Each Channel-Hour\n');
    
  elseif strcmp(unit, 'Minutely')
    fprintf(fid,'Number of Events for Each Channel-Minute\n');
    
  end
  
  if ~isempty(parameter.tag)
    fprintf(fid,'Include tag (case insensitive): %s',parameter.tag);
  end
  fprintf(fid,'\n');
  
  fprintf(fid,'Run Date and Time:  %s\n\n',datestr(now));
  
  fprintf(fid,'Number of channels: %.0f\n\n\n',NumChannels);

  fprintf(fid,',,Channel\n');
  fprintf(fid,'Date Range,Time Bin,');
  for channel = 1:NumChannels
    fprintf(fid,'%.0f,', channel);
  end
  fprintf(fid,'SUM\n\n');  
  
  binDT = 0;
  
  %write data
  for bin = 1:NumBins

    fprintf(fid,'%s - %s,', datestr(FirstDate), datestr(LastDate));
    fprintf(fid,'%s-%s,',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
    fprintf(fid,'%6.0f,', sum(NumEvents(:,:,bin),1));
    fprintf(fid,'%8.0f,', sum(sum(NumEvents(:,:,bin),1),2));
    fprintf(fid,'\n');

    binDT = binDT + binDTint;

  end
  
  %display sums
  fprintf(fid,'\n');
  fprintf(fid,'SUM,,');
  ChanSum = sum(sum(NumEvents,1),3);
  fprintf(fid,'%.0f,', ChanSum);
  fprintf(fid,'%.0f,', sum(ChanSum));
  fprintf(fid,'\n');
 
  %close output file
  fclose(fid);
  
end

%% output Summary of Presence to CSV if requested

if parameter.csv && ...
    (strcmp(output, 'Presence') || strcmp(output,'Presence_and_NumEvents'))

  %Find output file path (Logs folder of selected sound)
  LibPath = context.library.path;
  SoundName = sound_name(get_active_sound);
  SoundPath = [LibPath SoundName '\Logs\'];

  %Output File Name = <LibraryName>_DailyPresence.csv
  LibraryName = context.library.name;
  fn = [LibraryName '_' unit 'PresenceSummary_' datestr(now,30) '.csv'];

  %Open output file
  fid = fopen([SoundPath fn], 'w');

  if strcmp(unit, 'Daily')
    fprintf(fid,'SUMMARY - Presence for Each Channel-Day\n');
    
  elseif strcmp(unit, 'Hourly')
    fprintf(fid,'SUMMARY - Presence for Each Channel-Hour\n');
    
  elseif strcmp(unit, 'Minutely')
    fprintf(fid,'SUMMARY - Presence for Each Channel-Minute\n');
    
  end
  
  if ~isempty(parameter.tag)
    fprintf(fid,'Include tag (case insensitive): %s',parameter.tag);
  end
  fprintf(fid,'\n');
  
  fprintf(fid,'Run Date and Time:  %s\n\n',datestr(now));
  
  fprintf(fid,'Number of channels: %.0f\n\n\n',NumChannels);

  fprintf(fid,',,Channel\n');
  fprintf(fid,'Date Range,Time Bin,');
  for channel = 1:NumChannels
    fprintf(fid,'%.0f,', channel);
  end
  fprintf(fid,'SUM\n\n');  
  
  binDT = 0;
  
  %display data
  for bin = 1:NumBins

    fprintf(fid,'%s - %s,', datestr(FirstDate), datestr(LastDate));
    fprintf(fid,'  %s-%s,',datestr(binDT,'HH:MM'),datestr(binDT+binDTint,'HH:MM'));
    fprintf(fid,'%.0f,', sum(NumEvents(:,:,bin) > 0,1));
    fprintf(fid,'%.0f,', sum(sum(NumEvents(:,:,bin) > 0,1),2));
%     fprintf(fid,'%.0f,', NumEvents(day,:,bin)>0);
%     fprintf(fid,'%.0f', sum(NumEvents(day,:,bin)>0));
    fprintf(fid,'\n');

    binDT = binDT + binDTint;

  end
    
  %display sums
  fprintf(fid,'SUM,,');
  ChanSum = sum(sum(NumEvents > 0,1),3);
  fprintf(fid,'%.0f,', ChanSum);
  fprintf(fid,'%.0f,', sum(ChanSum));
  fprintf(fid,'\n');
 
  %close output file
  fclose(fid);
  
end

