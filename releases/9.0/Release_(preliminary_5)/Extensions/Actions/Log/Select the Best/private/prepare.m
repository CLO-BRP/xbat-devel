
function [result, context] = prepare(parameter, context)
% SELECT THE BEST - prepare

% initializations
result = struct;
tstart = tic;
N = parameter.N;
KeepChan = str2double( parameter.KeepChan );
KeepChan = KeepChan';
mode = parameter.mode;
if iscell( mode )
    mode = mode{ 1 };
end

%Stop if any open browsers
if close_browsers
  return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
    fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
    return;
end

% for each log
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );
NumLogs = length( logs );

for i = 1:NumLogs

    t0 = tic;

    %indicate log being processed in waitbar
    waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...'] );

    %load and refresh path in log
    [ log, snd_name, status ] = load_log( logs{ i }, context );
    if status
        return;
    end
    
    fn = log_name( log );
    
    % filter events by channel 
    BestEvent = filter_events_by_channel( log.event, KeepChan );
    
    % filter highest-scoring events
     [ BestEvent, len, msg, status ] = filter_high_scores( BestEvent, N, mode, KeepChan, log );
     if ~status
         fail( sprintf( '%s\n  %s', msg, log_name( log ) ), 'WARNING' )
         return;
     end

    % output log
    chan_str = sprintf( '%.0f-', KeepChan );
    chan_str = chan_str( 1:end-1 );
    if isequal( status, 1)
      fn = sprintf( '%s_BEST%.0f-%s-ch%s', fn, N, mode, chan_str );
    elseif isequal( status, 2)
      fn = sprintf( '%s_BEST%.0f-%s-ch%s_INCOMPLETE', fn, N, mode, chan_str );
    end
    
    % make sound struct
    lib = get_active_library;
    S = sound_load( lib, snd_name );
    snd = S.sound;

    % create empty new log
    NewLog = new_log( fn, context.user, context.library, snd );

    % save filtered events to log
    NewLog.event = BestEvent;    
    NewLog.length = len;
    NewLog.curr_id = log.curr_id;
    log_save( NewLog );

    %indicate log processed in waitbar
    result = make_result( log, t0 );
    action_waitbar_update( wh(1), i/NumLogs, result, 'log' ); 
    
end

d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc( tstart) );
waitbar_update( wh(1), 'PROGRESS', 'message', d );

disp('**********************************************************************')
disp('Do not forget to update documentation and history in header of compute')
disp('**********************************************************************')

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m
