function result = parameter__control__callback(callback, context)

% ADD EVENT MEASUREMENTS - parameter__control__callback

result = struct;

%-------------------------------------------------------------------------
% update saved user parameter settings
%-------------------------------------------------------------------------

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

%load prest
load( preset_path )

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
name = callback.control.name;
if strcmp( name, 'all_tags' )
    all_tags = get_control(callback.pal.handle, 'all_tags', 'value');
    if strcmp( all_tags, 'Copy events with all tags' )
        set_control( callback.pal.handle, 'tag_list', 'enable', 0 );
    else
        set_control( callback.pal.handle, 'tag_list', 'enable', 1 ); 
    end
end

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
name = callback.control.name;
style = get( callback.control.handle, 'style' );
	
if any( strcmp(style, { 'slider' 'checkbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name ' = value;' ] );

elseif any( strcmp( style, { 'popupmenu' 'listbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name '_idx = value;' ] );
  
elseif any( strcmp( style, { 'edit' } ) )
  value = get( callback.control.handle, 'String' );
  
  %update slider if edit is part of slider control
  if length( callback.control.handles ) > 1
    style2 = get( callback.control.handles( 2 ), 'style' );    
    if strcmp( style2, 'slider')
      value = str2double( value );
    end    
  end  
  eval( [ 'parameter.' name ' = value;' ] );  
end

%-------------------------------------------------------------------------
% add suffix token to suffix
%-------------------------------------------------------------------------
pal = callback.pal.handle;
if strcmp(callback.control.name, 'add_token')        
  str = get_control(pal, 'suffix', 'string');
  value = get_control(pal, 'add_token', 'value');
  if strcmp(value{1}, 'sample bounds')
    value = '%SAMPLE_BOUNDS%';
  elseif strcmp(value{1}, 'sample index')
    value = '%SAMPLE_INDEX%';
  end
  if isempty(str)
      str = value;
  else
      str = [str, '_', value];
  end
  set_control(pal, 'suffix', 'string', str);  
  parameter.suffix = str;
end

%-------------------------------------------------------------------------
% save parameters to preset
%-------------------------------------------------------------------------
save(preset_path,'parameter');

