function [result, context] = compute(log, parameter, context)
% DELETE OVERLAPPING DUPLICATE EVENTS - compute

%%
% Delete Duplicate Events deletes duplicate events, leaving only 
% higher-scoring event at each time. "Duplicate" means time bounds overlap 
% more than 50%.

% Useful for cases where automated detection software places multiple
% events at the same time.
%
%%

% Mike Pitzrick
% msp2@cornell.edu
% 15 Mar 08

%% 

result = struct;

if context.state.kill
  
  return;
  
end

%Abort if log open

if log_is_open(log)
  
  fprintf(2,'ERROR: Close log before attempting to delete duplicate events.\n');
  
  fprintf(2,'   %s\n', log_name(log));
  
  return;
  
end


%%
% initializations
OverlapThreshold = parameter.OverlapThreshold;
% OverlapThreshold = 0.5;
  
len = log.length;

t1 = zeros(1,len);

t2 = t1;

score = t1;

channel = t1;

%sort events by time of beginning
for i=1:len
  t1(i) = log.event(i).time(1);
end

[t1 idx] = sort(t1);

%make variables for end time, score and channel, sorted by begin time
for i=1:len
  
  curr_event = log.event(idx(i));
  
  t2(i) = curr_event.time(2);
  
  if ~isempty(curr_event.score)
  
    score(i) = curr_event.score;
    
  end
  
  channel(i) = curr_event.channel;
  
end

% make NewEvent w/o dups, move dups to deleted events
keep_idx = [];

nDeleted = 0;

deleted(1,len) = event_create;

% idx = 1:len;

% % empty trash
% log.deleted_event = empty(event_create);
  

for chan = 1:log.sound.channels
  
  chan_filter = (channel == chan);
  
  chan_t1 = [-1, t1(chan_filter), -1];
  
  chan_t2 = [-1, t2(chan_filter), -1];
  
  chan_score = [-1, score(chan_filter), -1];
  
  chan_len = sum(chan_filter);
  
  chan_idx = [-1, idx(chan_filter), -1];
  
  i = 3;

  while i < chan_len+2

    MaxOverlap = OverlapThreshold * min(chan_t2(i) - chan_t1(i), chan_t2(i-1) - chan_t1(i-1));

    overlap = chan_t2(i-1) - chan_t1(i);

    if overlap < MaxOverlap                     % are not dups
      
      i = i + 1;

    else                                        %are dups
      
        nDeleted = nDeleted + 1;
        
        chan_len = chan_len - 1;

      if chan_score(i-1) >= chan_score(i)                  % 1st dup better
        
        deleted(nDeleted) = log.event(chan_idx(i));
        
        chan_idx = [chan_idx(1:i-1) chan_idx(i+1:end)];
        
        chan_t1 = [chan_t1(1:i-1) chan_t1(i+1:end)];
        
        chan_t2 = [chan_t2(1:i-1) chan_t2(i+1:end)];
        
        chan_score = [chan_score(1:i-1) chan_score(i+1:end)];

      else                                      % 2nd dup better
        
        deleted(nDeleted) = log.event(chan_idx(i-1));
        
        chan_idx = [chan_idx(1:i-2) chan_idx(i:end)];
        
        chan_t1 = [chan_t1(1:i-2) chan_t1(i:end)];
        
        chan_t2 = [chan_t2(1:i-2) chan_t2(i:end)];
        
        chan_score = [chan_score(1:i-2) chan_score(i:end)];

      end
    end  
  end
  
  if length(chan_idx) > 2
    
    keep_idx = [keep_idx, chan_idx(2:end-1)];
    
  end  
end

% save log
fn = log.file(1:end-4);
fn = [fn '_NoOverlappingEvents'];
NewLog = new_log(fn,context.user,context.library,context.sound);

NewLog.event = log.event(keep_idx);
% idx = idx(2:end-1);
% i = i - 2;
% NewLog.event = log.event(idx(1:i));
NewLog.deleted_event = deleted(1:nDeleted);

NewLog.length = length(keep_idx);
% NewLog.length = i;
NewLog.curr_id = log.curr_id;

log_save(NewLog);
