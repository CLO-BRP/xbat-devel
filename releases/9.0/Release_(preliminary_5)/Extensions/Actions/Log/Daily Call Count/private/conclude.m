function [result, context] = conclude(parameter, context)
result = struct;

%% DAILY CALL COUNT - conclude
%
% Tabulates number of events with a user-definable tag for each channel of
% each selected sound.
%
% Useful for determining daily presence/absence of Fin Whales and other species.
%
%%
%terminate if fatal condition encountered
if context.state.kill
  return;
end

%intializations
DailyPresence = context.state.DailyPresence;
NumChannels = context.sound.channels;
FirstDate = context.state.FirstDate;
LastDate = context.state.LastDate;
NumDays = LastDate - FirstDate + 1;


%% OUTPUT TO MATLAB COMMAND WINDOW
%Write header to Command Window
disp(sprintf('\n\n'))
disp(sprintf('Number of events with tag (case insensitive): %s',parameter.tag))
disp(sprintf('Number of channels: %.0f\n\n',NumChannels))

fprintf(1,'Date       ');
for channel = 1:NumChannels
  fprintf(1,'%6.0f', channel)
end
fprintf(1,'     SUM\n\n');

%output Daily Call Count
for day = 1:NumDays
  
  fprintf(1,'%s', datestr(FirstDate + day - 1));
  fprintf(1,'%6.0f', DailyPresence(day,:));
  fprintf(1,'%8.0f', sum(DailyPresence(day,:)));
  fprintf(1,'\n');
  
end

fprintf(1,'SUM        ');
fprintf(1,'%6.0f', sum(DailyPresence));
fprintf(1,'%8.0f', sum(sum(DailyPresence)));
fprintf(1,'\n\n');


%% OUTPUT TO CSV

%Find output file path (Logs folder of selected sound)
LibPath = context.library.path;
SoundName = sound_name(get_active_sound);
SoundPath = [LibPath SoundName '\Logs\'];

%Output File Name = <LibraryName>_DailyPresence.csv
LibraryName = context.library.name;
fn = [LibraryName '_DailyCallCount' datestr(now,30) '.csv'];

%Open output file
fid = fopen([SoundPath fn], 'w');

%Write header of output file
fprintf(fid,'Number of Events for Each Channel-Day\n');
fprintf(fid,'Include tag (case insensitive): %s\n',parameter.tag);
fprintf(fid,'RunTime:  %s\n\n',datestr(now));
fprintf(fid,',');
for channel = 1:NumChannels
  fprintf(fid,'Ch %.0f,',channel);
end
fprintf(fid,'SUM\n\n');

%output Daily Call Count
for day = 1:NumDays
  
  fprintf(fid,'%s,', datestr(FirstDate + day - 1));
  fprintf(fid,'%.0f,', DailyPresence(day,:));
  fprintf(fid,'%.0f,', sum(DailyPresence(day,:)));
  fprintf(fid,'\n');
  
end

fprintf(fid,'SUM,');
fprintf(fid,'%.0f,', sum(DailyPresence));
fprintf(fid,'%.0f,', sum(sum(DailyPresence)));
fprintf(fid,'\n\n');

%close output file
fclose(fid);
