function [result, context] = prepare(parameter, context)

% RANDOM SAMPLE - prepare

result = struct;

%initializations
context.state.kill = 0;

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

%initializations
result = struct;
n = parameter.n;

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  log = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(log),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    log.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    log.path = LogPath;
    
    % save log
    log_save(log);
    
    
    %abort if log open
    if log_is_open(log)
        disp(['Close log ''', log_name(log), ' before randomizing it.']); return;
    end

    len = log.length;
    curr_id = log.curr_id;

    %randomize order of events. store in Event
    nNewLogs=ceil(len/n);
    idx = randperm(len);
    Event = log.event;

    %find log name
    fnLog = log.file;
    fnLog = fnLog(1:end-4);

    %log name format
    LogNameFormat = sprintf('%%s_RAND%%.0f_%%0%.0f.0f', ceil(log10(nNewLogs+1)));

    %break randomized log into nNewLogs logs of size n
    for j=1:nNewLogs

      %make new empty log
      NewLogName = sprintf(LogNameFormat, fnLog, n, j);  %log name

      %make new empty log
      
      RandomLog = new_log(NewLogName,context.user,context.library,log.sound);

      %make random selection of events and add to new log

      %new_log.event = empty(event_create);
      first = n*(j-1)+1;
      last = n*j;
      if last > len
        last = len;
      end
      
      %sort events into original order
      keepIDX = idx(first:last);
      
      sample_size = last - first + 1;
      
      t = zeros(sample_size, 1);
      
      curr_events = Event(keepIDX);
      
      if parameter.sort
        
        for k = 1:sample_size
        
          t(k) = curr_events(:).time(1);
        
        t1 = t(:,1);
        
        [~, tIDX] = sort(t1);
        
        curr_events = curr_events(tIDX);        
        
%         keepIDX = sort(keepIDX);
        
      end
      
      RandomLog.event = curr_events;

      RandomLog.length = last-first+1;
      RandomLog.curr_id = curr_id;

      %save new log
      log_save(RandomLog);
    end

  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');
    context.state.kill = 1;
    return;
  end
end
