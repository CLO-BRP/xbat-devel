function output = Location_v2p2_measurement_pitz(mode, varargin)

%%
%% 'Location_v2p2_measurement'
%%
%% A VERY modified version of the KATLAB (K.M.F. / K.J.D.) Locator
%% fully re-written for XBAT
%%
%% There are some significant algorithmic changes from the original
%% KATLAB code.  All code except for 'fxcorr' and 'freqfilt' has been
%% re-worked and re-written; code has been streamlined where possible,
%% and approaches altered to simplify calculation.  Many new post
%% measurement visuals and diagnostic plots have been added
%%
%% Main modifications to the location algorithm include:
%% 1) the scaling applied to the correlation functions is limited to
%% have a divisor 100x down from the peak value (i.e., the scaling
%% functions have been thresholded to have values no lower than 100x
%% down from their peak value); this addresses the 'mote' around the
%% peak problem
%% 2) the search for location has been modified to use a stochastic
%% search instead of the previously used genetic search; the search still
%% relies on delay-sum beamforming of the correlation functions to find
%% the best location in space; two starting points for the search are
%% available:
%%   1. the search starts out along the hyperbolic trajectories defined
%%   by the peaks (i.e., pairwise time delays) of the correlation
%%   functions; points are more closely spaced near the foci of the
%%   hyperboli than out on the arms (the length of the arms are defined
%%   by the search range parameter)
%%   2. the search starts out on a regular grid defined by the search
%%   area (search area == search range meters beyond the rectangle
%%   defined by the extent of the array)
%% in both cases, new guesses are generated randomly from the top ~1000
%% points; the user defines the amount of point jitter allowed in
%% generating new search points (x & y jitter independently)
%%
%% By Kathryn A. Cortopassi, June 2005
%%
%% October 2005
%% start to modify existing process...
%%
%% July 2006
%% 1) change search area calculation to a given radius from the center of
%% the array (change parts of search code accordingly)
%% 2) add choice of using only N-1 correlation functions for making location
%% estimate
%% 3) add version number variable to prevent typos
%%
%% April 2011
%% 1) mods by Mike Pitzrick so that plotting callback function can be used
%% for RLTD_track



%%
%% syntax for call from XBAT:
%% --------------------------
%%  output = Location_v2p2_measurement(mode, varargin)
%%
%%  measurement = Location_v2p2_measurement('create', h)
%%  description = Location_v2p2_measurement('describe', h)
%%
%%  log = Location_v2p2_measurement('batch', log, ix, measurement)
%%  event = Location_v2p2_measurement('batch', sound, event, measurement)
%%
%%  flag = Location_v2p2_measurement('events', h, m, ix, measurement)
%%
%%  g = Location_v2p2_measurement('menu', h, m, ix)
%%  g = Location_v2p2_measurement('display', h, m, ix)
%%
%% addition for KAC batch measurement
%%  flag = Location_v2p2_measurement('batch_available')
%%
%% input:
%% ------
%%  h - handle to parent figure
%%  log - log structure
%%  ix - indices of events in log
%%  measurement - measurement structure (contains current parameters)
%%  event - event structure array
%%  m - index of log
%%
%% output:
%% -------
%%  measurement - measurement structure
%%  description - description structure
%%  log - log structure
%%  event - event structure array
%%  flag - update success indicator
%%  g - handles to objects created
%%



version1 = 'v2p2';
version2 = 'v2.2';



%%***************************************************************
%% STATIC CODE
%%***************************************************************

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------
%  STATIC MAIN CODE FOR GENERIC MEASUREMENT CONTROL
%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%--------------------------------------------------------
% SET DEFAULT MODE
%--------------------------------------------------------

if (~nargin)
  mode = 'create';
end


%--
% set context flag for create and describe
%--
if ((strcmp(mode, 'create') | strcmp(mode, 'describe')) & length(varargin))
  h = varargin{1};
  context_flag = 1;
else
  context_flag = 0;
end


%--------------------------------------------------------
% CREATE MEASUREMENT DESCRIPTION
%--------------------------------------------------------

persistent DESCRIPTION_PERSISTENT;

if (context_flag)
  DESCRIPTION_PERSISTENT = measurement_description(version2, h);
else
  if (isempty(DESCRIPTION_PERSISTENT))
    DESCRIPTION_PERSISTENT = measurement_description(version2);
  end
end

description = DESCRIPTION_PERSISTENT;

%--------------------------------------------------------
% CREATE EMPTY MEASUREMENT
%--------------------------------------------------------

%--
% create measurement structure
%--

measurement = measurement_create;

%--
% set name of measurement
%--

measurement.name = description.name;

%--
% create function handle to main annotation function
%--

persistent FUNCTION_HANDLE_PERSISTENT;

if (isempty(FUNCTION_HANDLE_PERSISTENT))
  FUNCTION_HANDLE_PERSISTENT = eval(['@' mfilename]);
end

measurement.fun = FUNCTION_HANDLE_PERSISTENT;

%--
% set measurement parameter
%--

persistent PARAMETER_PERSISTENT;

if (context_flag)
  PARAMETER_PERSISTENT = parameter_fun('create', h, version1);
else
  if (isempty(PARAMETER_PERSISTENT))
    PARAMETER_PERSISTENT = parameter_fun('create', [], version1);
  end
end

measurement.parameter = PARAMETER_PERSISTENT;

%--
% set value
%--

measurement.value = value_fun('create');

%--------------------------------------------------------
% COMPUTE ACCORDING TO MODE
%--------------------------------------------------------

switch (mode)

  %--------------------------------------------------------
  % OUTPUT SAMPLE MEASUREMENT
  %--------------------------------------------------------

  case ('create')

    output = measurement;

    %--------------------------------------------------------
    % OUTPUT MEASUREMENT DESCRIPTION
    %--------------------------------------------------------

  case ('describe')

    output = description;

    %--------------------------------------------------------
    % BATCH ANNOTATE LOG EVENTS OR EVENT SEQUENCE (THESE ARE MEANT FOR THE COMMAND LINE)
    %--------------------------------------------------------

  case ('batch')


    %%*****************************************************
    %%*****************************************************
    %% K.A. Cortopassi, May 2004
    %% put in error condition checking for KAC batch measurement tool run mode

    error_flag = 0;

    %% end KAC change
    %%*****************************************************
    %%*****************************************************



    %--
    % rename input
    %--

    tmp = varargin{1};

    %--
    % handle sound or log
    %--

    if (strcmp(tmp.type,'log'))

      %--------------------------------------------------------
      % (log,ix,measurement)
      %--------------------------------------------------------

      log = tmp;
      ix = varargin{2};

      sound = log.sound;
      event = log.event(ix);

      flag_log = 1;

    else

      %--------------------------------------------------------
      % (sound,event,measurement) THIS IS A SEQUENCE OF ORPHANED EVENTS
      %--------------------------------------------------------

      sound = tmp;
      event = varargin{2};

      flag_log = 0;

    end

    measurement = varargin{3};

    %--
    % check name of measurement to add
    %--

    if (~strcmp(measurement.name,description.name))
      disp(' ');
      warning(['Input measurement is ''' measurement.name ''' not ''' description.name '''.']);
      disp(' ');
      return;
    end




    %%*****************************************************
    %%*****************************************************
    %% K.A. Cortopassi, May 2004
    %% put in error condition checking for KAC batch measurement tool run mode
    %%

    %--
    % check for the sound attributes
    %--
    if isempty(sound.geometry.local)
      if strcmpi(sound.type, 'file stream')
        snd_label =[sound.path, '*.*'];
      elseif strcmpi(sound.type, 'file')
        snd_label = [sound.path, sound.file];
      end
      warndlg(sprintf('Please enter the channel geometry in sound attributes for sound:\n ''%s''!!', snd_label));
    end

    if isempty(sound.speed)
      if strcmpi(sound.type, 'file stream')
        snd_label =[sound.path, '*.*'];
      elseif strcmpi(sound.type, 'file')
        snd_label = [sound.path, sound.file];
      end
      warndlg(sprintf('Please enter speed of sound in sound attributes for sound:\n ''%s''!!', snd_label));
    end

    if isempty(sound.geometry.local) | isempty(sound.speed)
      return;
    end

    %% end KAC change
    %%*****************************************************
    %%*****************************************************





    %--
    % get current date string
    %--

    curr = now;

    %--
    % add or update measurement in event sequence
    %--





    %*********************************************************************
    %*********************************************************************
    %--
    % special addition for KAC batch measurements tool run mode!!!
    % K.A. Cortopassi, May 2004
    %--

    numevents = length(event);
    wh_batch = waitbar(0, 'Processing events...');
    set(wh_batch, 'units', 'normalized'); %% KAC add
    wbpos = get(wh_batch, 'position'); %% KAC add
    set(wh_batch, 'position', [.5, .5, wbpos(3), wbpos(4)]); %% KAC add

    % for k = 1:length(event)
    for k = 1:numevents

      try
        waitbar(k/numevents, wh_batch, sprintf('Processing event number %d of %d', k, numevents));
      catch
        %% cancel requested
        fprintf(1, 'Cancel requested, Location %s Measurement terminated %s.\n\n', version2, datestr(now));
        if error_flag
          fprintf(fid, 'Cancel requested, Location %s Measurement terminated %s.\n\n', version2, datestr(now));
          %% close error output file
          fclose(fid);
        end
        %% throw and error to any calling function
        error(['Cancel requested, Location ', version2, ' Measurement terminated.']);
      end

      %% end KAC change
      %*********************************************************************
      %*********************************************************************





      %--
      % get index of measurement
      %--

      tmp = struct_field(event(k).measurement,'name');
      ixa = find(strcmp(tmp,description.name));

      %--
      % compute measurement
      %--

      if (isempty(ixa))

        len = length(event(k).measurement);
        if ((len == 1) & isempty(event(k).measurement(1).name))
          len = 0;
        end

        measurement.created = curr;
        measurement.modified = [];





        %                 %% KAC removal November 2003 for easier debugging
        %                 %         try
        %
        %                 measurement.value = ...
        %                     measurement_compute(sound,event(k),measurement.parameter,version1,version2);
        %
        %                 %% KAC addition 7 April 2004
        %                 if isempty(measurement.value)
        %                     %% there has been an error!
        %                     return;
        %                 end


        %*********************************************************************
        %*********************************************************************
        %--
        % special addition for KAC batch measurements tool run mode!!!
        % put in error condition checking
        % K.A. Cortopassi, May 2004
        %--

        try
          %fprintf(1, 'at line 417/n');
          measurement.value = measurement_compute(sound, event(k), measurement.parameter, version1, version2);
        catch
          if ~error_flag
            %% set up error output directory, change to matlabroot May 2005
            error_output_path = [matlabroot, '\work\Location_', version1, '_measurement_ERROR OUTPUT'];
            if ~exist(error_output_path);
              mkdir(error_output_path);
            end
            %% set-up error reporting to file
            error_output = [error_output_path, '\Location_', version1, '_measurement_ERROR_D' datestr(now,30), '.txt'];
            fid = fopen(error_output, 'w');
            if flag_log
              fprintf(1,'Error computing Location %s Measurement for log ''%s'' %s\n', version2, log.file, datestr(now));
              fprintf(fid,'Error computing Location %s Measurement for log ''%s'' %s\n', version2, log.file, datestr(now));
            else
              fprintf(1,'Error computing Location %s Measurement %s\n', version2, datestr(now));
              fprintf(fid,'Error computing Location %s Measurement %s\n', version2, datestr(now));
            end
            error_flag = 1;
          end
          fprintf(1,'Error computing Location %s Measurement for event %d: %s : continuing...\n', version2, k, lasterr);
          fprintf(fid,'Error computing Location %s Measurement for event %d: %s : continuing...\n', version2, k, lasterr);
        end

        %% end KAC change
        %*********************************************************************
        %*********************************************************************



        event(k).measurement(len + 1) = measurement;

        %         catch
        %
        %           disp(' ');
        %           %% KAC fix to index November 2003
        %           warning(['Failed to compute ''' description.name ''' for event ' num2str(event(k).id)]);
        %           % warning(['Failed to compute ''' description.name ''' for event ' num2str(event.id)]);
        %           disp(' ');
        %
        %         end

        %--
        % recompute measurement
        %--

      else

        measurement.created = event(k).measurement(ixa).created;
        measurement.modified = curr;

        %                 %% KAC removal November 2003 for easier debugging
        %                 %         try
        %
        %                 measurement.value = ...
        %                     measurement_compute(sound,event(k),measurement.parameter,version1,version2);
        %
        %
        %                 %% KAC addition 7 April 2004
        %                 if isempty(measurement.value)
        %                     %% there has been an error!
        %                     return;
        %                 end


        %*********************************************************************
        %*********************************************************************
        %--
        % special addition for KAC batch measurements tool run mode!!!
        % put in error condition checking
        % K.A. Cortopassi, May 2004
        %--

        try
          %fprintf(1, 'at line 493\n');
          measurement.value = measurement_compute(sound, event(k), measurement.parameter, version1, version2);
        catch
          if ~error_flag
            %% set up error output directory, change to matlabroot May 2005
            error_output_path = [matlabroot, '\work\Location_', version1, '_measurement_ERROR OUTPUT'];
            if ~exist(error_output_path);
              mkdir(error_output_path);
            end
            %% set-up error reporting to file
            error_output = [error_output_path, '\Location_', version1, '_measurement_ERROR_D' datestr(now,30), '.txt'];
            fid = fopen(error_output, 'w');
            if flag_log
              fprintf(1,'Error computing Location %s Measurement for log ''%s'' %s\n', version2, log.file, datestr(now));
              fprintf(fid,'Error computing Location %s Measurement for log ''%s'' %s\n', version2, log.file, datestr(now));
            else
              fprintf(1,'Error computing Location %s Measurement %s\n', version2, datestr(now));
              fprintf(fid,'Error computing Location %s Measurement %s\n', version2, datestr(now));
            end
            error_flag = 1;
          end
          fprintf(1,'Error computing Location %s Measurement for event %d: %s : continuing...\n', version2, k, lasterr);
          fprintf(fid,'Error computing Location %s Measurement for event %d: %s : continuing...\n', version2, k, lasterr);
        end

        %% end KAC change
        %*********************************************************************
        %*********************************************************************


        event(k).measurement(ixa) = measurement;

        %         catch
        %
        %           disp(' ');
        %           %% KAC fix to index November 2003
        %           warning(['Failed to compute ''' description.name ''' for event ' num2str(event(k).id)]);
        %           % warning(['Failed to compute ''' description.name ''' for event ' num2str(event.id)]);
        %           disp(' ');
        %
        %         end

      end

    end

    %--
    % output event sequence
    %--

    if (flag_log)

      %--------------------------------------------------------
      % (log,ix,measurement)
      %--------------------------------------------------------

      log.event(ix) = event;
      output = log;

    else

      %--------------------------------------------------------
      % (sound,event,measurement)
      %--------------------------------------------------------

      output = event;

    end



    %*********************************************************************
    %*********************************************************************
    %--
    % special addition for KAC batch measurements tool run mode!!!
    % K.A. Cortopassi, May 2004
    %--

    %% close error output file
    if error_flag
      fclose(fid);
    end

    if exist('wh_batch')
      close(wh_batch);
    end


    %% end KAC change
    %*********************************************************************
    %*********************************************************************




    %--------------------------------------------------------
    % COMPUTE MEASUREMENT OF BROWSER DISPLAYED EVENTS
    %--------------------------------------------------------

  case ('events')

    %--
    % rename input variables
    %--

    h = varargin{1};
    m = varargin{2};
    ix = varargin{3};

    %--
    % get figure userdata
    %--

    data  = get(h,'userdata');

    %--
    % handle log browser get parent userdata
    %--

    if (gcf == h)
      flag = 0;
    else
      flag = 1;
    end

    %--
    % set default parameter for parameter edit
    %--

    nix = length(ix);

    if (nix == 1)

      %--
      % get measurement index for event
      %--

      event = data.browser.log(m).event(ix);
      ixa = find(strcmp(struct_field(event.measurement,'name'),description.name));

      %--
      % get current default parameter or current measurement parameter
      %--

      if (isempty(ixa))
        param = PARAMETER_PERSISTENT;
      else
        param = event.measurement(ixa).parameter;
      end

      %% KAC add 15 February 2007
      if ~isempty(ixa)
        author = data.browser.log(m).event(ix).measurement(ixa).author;
        recompute_flag = 1;
      else
        author = data.browser.author;
        recompute_flag = 0;
      end

    else

      %--
      % get current default parameter
      %--

      param = PARAMETER_PERSISTENT;
      author = data.browser.author;
      recompute_flag = 0;

    end

    %--
    % allow for batch computation within browser
    %--

    % this modality of the call to the measurement interface is used to
    % update the value of a measurement upon event editing

    flag_batch = (length(varargin) > 3);

    if (flag_batch)

      %--
      % use input parameter
      %--

      param = varargin{4}.('parameter');
      % author = data.browser.author; %% KAC comment out 15 Feb 2007

    else

      %--
      % edit parameter
      %--

      file = file_ext(data.browser.log(m).file);

      % this title should be changed to something more descriptive

      title_str = file;
      % 		title_str = 'Selected Set';

      %       [param,author] = parameter_edit(description, title_str, param, data.browser.author, version1); %% KAC comment out 15 Feb 2007
      [param,author] = parameter_edit(description, title_str, param, author, version1, recompute_flag);

    end

    %--
    % update persistent parameters and browser author
    %--

    if (~isempty(param))

      PARAMETER_PERSISTENT = param;
      data.browser.author = author;

    else

      output = 0;
      return;

    end

    %--
    % create waitbar and get start time
    %--

    %% KAC change this to have wait bar always
    %% May 2004
    if nix
      %if (nix > 6)
      % if (nix > 20)

      hw = wait_bar(0,'');
      set(hw,'name',[description.name '  -  ' file]);
      set(hw, 'units', 'normalized'); %% KAC add
      wbpos = get(hw, 'position'); %% KAC add
      set(hw, 'position', [.5, .5, wbpos(3), wbpos(4)]); %% KAC add

      start_time = clock;

      flag_wait = 1;

    else

      flag_wait = 0;

    end

    %--
    % set common values of measurements including parameters
    %--

    measurement.author = author;

    curr = now;
    measurement.created = curr;
    measurement.modified = [];

    measurement.parameter = param;

    %--
    % compute measurement for all indicated events
    %--

    for k = 1:nix

      %--
      % copy event from log and get measurement index
      %--

      event = data.browser.log(m).event(ix(k));

      ixa = find(strcmp(struct_field(event.measurement,'name'),description.name));

      %--
      % compute measurement
      %--

      if (isempty(ixa))

        len = length(event.measurement);
        if ((len == 1) & isempty(event.measurement(1).name))
          len = 0;
        end

        %%**** KAC changed this so de-bug is easier
        % 			try

        %fprintf(1, 'at line 775\n');
        measurement.value = measurement_compute(data.browser.sound, event, measurement.parameter, version1, version2);

        event.measurement(len + 1) = measurement;

        % 			catch
        %
        % 				disp(' ');
        % 				warning(['Failed to compute ''' description.name ''' for event ' num2str(event.id)]);
        % 				disp(' ');
        %
        % 			end

        %--
        % recompute measurement
        %--

      else

        measurement.created = event.measurement(ixa).created;
        measurement.modified = curr;

        %%**** KAC changed this so de-bug is easier
        %         try

        %fprintf(1, 'at line 800\n');
        measurement.value = measurement_compute(data.browser.sound, event, measurement.parameter, version1, version2);

        event.measurement(ixa) = measurement;

        % 			catch
        %
        % 				disp(' ');
        % 				warning(['Failed to compute ''' description.name ''' for event ' num2str(event.id)]);
        % 				disp(' ');
        %
        % 			end

      end

      %--
      % put measured event into log
      %--

      data.browser.log(m).event(ix(k)) = event;

      %--
      % update waitbar
      %--

      %% change back...
      %         %%**** KAC changed this to get wait bar all the time
      % if (flag_wait & ~mod(k,10))
      if (flag_wait & ~mod(k,1))
        %         if (flag_wait)
        waitbar((k / nix),hw, ...
          ['Events Processed: ' int2str(k) ', Elapsed Time: ' num2str(etime(clock,start_time),'%5.2f')]);
      end

      %% KAC ADDITION 13 March 2006
      %% save log every 100 calculations
      if (mod(k, 100) == 0)
        fprintf(1, 'Updating and saving log ''%s'' ...\n', file);
        waitbar(1,hw,['Updating and saving log ''', file, ''' ...']);
        if (data.browser.log(m).autosave)
          data.browser.log(m).saved = 1;
        else
          data.browser.log(m).saved = 0;
        end
        set(h,'userdata',data);
        if (data.browser.log(m).autosave)
          log_save(data.browser.log(m));
        end
      end

    end

    %--
    % indicate that we are updating and saving log
    %--

    if (flag_wait)
      waitbar(1,hw, ...
        ['Updating and saving log ''' file ''' ...'] ...
        );
    end

    %--
    % update save state of log
    %--

    if (data.browser.log(m).autosave)
      data.browser.log(m).saved = 1;
    else
      data.browser.log(m).saved = 0;
    end

    %--
    % update userdata
    %--

    set(h,'userdata',data);

    %--
    % save log if needed
    %--

    if (data.browser.log(m).autosave)
      log_save(data.browser.log(m));
    end

    %--
    % close waitbar
    %--

    if (flag_wait)
      close(hw);
    end

    %--
    % update event display
    %--

    if (flag == 1)

      %--
      % log browser display update for a single or for multiple events
      %--

      if (nix == 1)

        %--
        % delete previous event display objects
        %--

        % we do this object type delete because the axes are tagged as
        % well and we do not need to remove these

        type = {'uimenu','uicontextmenu','line','patch','text'};

        for k = 1:length(type)
          delete(findall(gcf,'tag',[int2str(m) '.' int2str(ix)],'type',type{k}));
        end

        %--
        % rebuild event display and selection
        %--

        event_view('log',gcf,m,ix);
        event_bdfun(gcf,m,ix);

        drawnow;

      else

        % this is not implemented yet

        % 			log_browser_display('events');

      end

    else

      %--
      % sound browser display update for a single or for multiple events
      %--

      if (nix == 1)

        %--
        % delete previous event display objects
        %--

        delete(findall(h,'tag',[int2str(m) '.' int2str(ix)]));

        %--
        % rebuild even display and selection
        %--

        event_view('sound',h,m,ix);
        event_bdfun(h,m,ix);

        drawnow;

      else

        %browser_display(h,'events');

      end

    end

    %--------------------------------------------------------
    % DISPLAY MEASUREMENT INFORMATION IN MENU OR GRAPHICALLY
    %--------------------------------------------------------

  case ({'menu','display'})

    %--
    % rename input
    %--

    h = varargin{1};
    m = varargin{2};
    ix = varargin{3};

    %--
    % get userdata if needed
    %--

    if (length(varargin) > 3)
      data = varargin{4};
    else
      data = get(h,'userdata');
    end

    %--
    % handle both sound and log browser display
    %--

    if (~isempty(data.browser.parent))

      flag_log = 1;

      %--
      % get parent handle and update data
      %--

      g = h;
      h = data.browser.parent;
      data = get(h,'userdata');

    else

      flag_log = 0;

    end

    %--
    % get event and log filename
    %--

    event = data.browser.log(m).event(ix);
    file = file_ext(data.browser.log(m).file);

    %--
    % check for existence of measurement
    %--

    ixa = find(strcmp(struct_field(event.measurement,'name'),description.name));

    if (isempty(ixa))
      output = [];
      return;
    end

    %--
    % compute depending on display mode
    %--

    switch (mode)

      %--------------------------------------------------------
      % DISPLAY MEASUREMENT INFORMATION MENU
      %--------------------------------------------------------

      case ('menu')

        %--
        % get event display parent handle
        %--

        if (~flag_log)
          g = findobj(h,'type','patch','tag',[int2str(m) '.' int2str(ix)]);
        else
          g = findobj(g,'type','axes','tag',[int2str(m) '.' int2str(ix)]);
          g = findobj(g,'type','image');
        end

        if (isempty(g))

          output = [];
          return;

        else

          %--
          % get contextual menu of parent
          %--


          %%~~~
          %% fix to deal with 'API' change in xbat PRE_DEVEL version
          %%~~~

          %% comment out this
          %           g = get(g,'uicontextmenu');
          %           g = get_menu(g,'Measurement');

          %% change to this
          g_tmp = get(g,'uicontextmenu');
          g = get_menu(g_tmp,'Measurement');

          %% add this
          if isempty(g)
            g = get_menu(g_tmp,'Measure');
          end

          %%~~~
          %% end fix
          %%~~~


          %--
          % create measurement scheme menu and edit option menu
          %--

          % 			mg = menu_group(g,'',{description.name,['Compute ' description.name ' ...']});

          mg = menu_group(g,'',{ ...
            description.name, ...
            'Recompute ...' ...
            });

          tmp = functions(event.measurement(ixa).fun);
          tmp = tmp.function;

          set(mg(2),'callback', ...
            [tmp '(''events'',' int2str(h) ',' int2str(m) ',' int2str(ix) ');']);

          if (get(mg(1),'position') > 1)
            set(mg(1),'separator','on');
          end

          %--
          % measurement information display menu
          %--

          output = measurement_menu(mg(1),m,event,ixa,description,data);

          %--
          % add parent menu handles to output
          %--

          output = [mg, output];

        end

        %--------------------------------------------------------
        % DISPLAY MEASUREMENT GRAPHICALLY
        %--------------------------------------------------------

      case ('display')

        %--
        % measurement information graphical display
        %--

        output = measurement_display(h,m,event,ixa,description,data, version1);

        %--
        % tag objects with log and event indices string
        %--

        set(output,'tag',[int2str(m) '.' int2str(ix)]);

    end






    %%--
    %% special addition for batch measurements!!!
    %% KAC May 2004
    %%--
  case('edit_parameters')

    if length(varargin)
      measurement = varargin{1};
    else
      eval(['measurement = ', mfilename, '(''create'');']);
    end
    eval(['description = ', mfilename, '(''describe'');']);

    dlg_title = 'parameter edit dialog';

    [measurement.parameter, measurement.author] = ...
      parameter_edit(description, dlg_title, measurement.parameter, measurement.author, version1);

    output = measurement;



  case('batch_available')

    output = 1;




end


%%***************************************************************
%% END STATIC CODE
%%***************************************************************







%%***************************************************************
%%***************************************************************
%% MEASUREMENT SPECIFIC CODE
%% by K.A. Cortopassi, April 2004
%%***************************************************************
%%***************************************************************

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  EDITABLE SUB-FUNCTIONS FOR MEASUREMENT
%%  note, however, function names must be static!
%%  because these functions are called as named by the
%%  static control code above
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%% measurement_description
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function description = measurement_description(version2, h);

%%
%% 'measurement_description'
%%
%% function to create measurement description
%%
%% --------------------------------------------------------
%%
%% syntax:
%% -------
%%   description = measurement_description(version2);
%%   description = measurement_description(version2, h);
%%
%%
%% input:
%% ------
%%  h = figure handle for calling context
%%
%% output:
%% -------
%%   description = measurement description structure
%%       .name = name of measurement
%%       .parameter = measurement parameter description
%%       .value = measurement value description
%%


%% set name of measurement
description.name = ['Source Location (', version2, ')'];


%% create measurement parameter description
%% take calling context into account
if (nargin == 2)
  description.parameter = parameter_fun('describe', h);
else
  description.parameter = parameter_fun('describe');
end


%% create measurement value description
description.value = value_fun('describe');


return;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------
%  parameter_fun
%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = parameter_fun(mode, varargin);

%%
%% 'parameter_fun'
%%
%% measurement parameter handling function
%% with four calling modes
%%
%% --------------------------------------------
%%
%%
%% syntax:
%% -------
%%
%%   param_struc = parameter_fun('create');
%%   param_struc = parameter_fun('create', h);
%%
%%   param_struc = parameter_fun('pack' ,param_cell);
%%
%%   param_cell = parameter_fun('unpack' ,param_struc);
%%
%%   param_descrip = parameter_fun('describe');
%%   param_descrip = parameter_fun('describe', h);
%%
%%
%% input:
%% ------
%%   mode = string describing function calling mode
%%   varargin:
%%       h = figure handle for calling context
%%       param_cell = parameter values in parameter cell array
%%       param_struc = parameter values in parameter structure
%%
%%
%% output:
%% -------
%%   varargout:
%%       param_struc = parameter values in parameter structure
%%       param_cell = parameter values in parameter cell array
%%       param_descrip = parameter description structure
%%           .field = field names
%%           .type = field types
%%           .default = default field value
%%           .input = input fields
%%           .line = display size of input fields
%%           .tip = field tips
%%           .menu = menu fields
%%           .separator = field separators in menu
%%


if ~nargin
  %% set default calling mode
  mode = 'create';
end



switch (mode)
  %% compute depending on mode


  case ('create')
    %% create measurement parameter structure

    %% Xcorr Method
    %% (time waveform, complex envelope, or (eventually) spectrogram)
    param_struc.corr_type = [];

    %% Xcorr Number
    %% (use all (N(N-1)/2) or N-1 correlation functions)
    param_struc.corr_num = [];

    %% Absolute Correlation-Peak Threshold
    %% (for channel discard; enter as fraction from zero to 1 for normalized xcorr functions)
    param_struc.abs_pk_thresh = [];

    %% Relative Correlation-Peak Threshold
    %%  (for peak selection in calculating xchannel events & weights; enter as percent of global peak)
    param_struc.rel_pk_thresh = [];

    %% Minimum Number Channels
    param_struc.min_channels = [];

    %% Maximum Number Channels
    param_struc.max_channels = [];

    %% Sound Channels to Use
    param_struc.channels = [];

    %% set this automatically
    %     %% Smoothing Filter Length
    %     param_struc.filter_length = [];

    %% Search Type
    %% (start to search, grid or hyperbolic)
    param_struc.search_type = [];

    %% Search Radius
    %% (part of setting search range)
    param_struc.search_radius = [];  %% (meters)

    %% Termination Criterion
    %% (median centroid distance)
    param_struc.term_crit = [];  %% (meters)

    %% Method for Mean Calculation
    %% (chose static to use for mean estimate, arithmetic or trimmed)
    param_struc.mean_type = [];

    %% select 2 or 3D search
    param_struc.search_dim = [];

    %% Display Result Plots
    %% (display? what level of diagnostic?)
    param_struc.display_plots = [];


    %% Spectrogram Generation Parameters
    %% (loaded if spectrogram correlation is used)
    param_struc.spc_fft = []; %% FFT length in points
    param_struc.spc_hop = []; %% fraction of FFT length
    param_struc.spc_win_type = []; %% window function
    param_struc.spc_win_length = []; %% fraction of FFT length



    if length(varargin) && ~isempty(varargin{1})
      %% figure handle was passed in; use figure handle to get figure data
      data = get(varargin{1}, 'userdata');
      num_chan = data.browser.sound.channels;
    else
      %% no data passed in
      num_chan = 21;
    end



    %% fill in parameter structure with defaults
    try
      %% get parameters from a saved defaults file
      version1 = varargin{2};

      %% get directory and name of the measurement, and ultimately location of the parameter defaults
      param_defaults = [pathof(mfilename('fullpath')), '__IGNORE\Location_', version1, '_measurement_PRE\param_defaults.mat'];

      %% load up defaults
      param_defaults = load(param_defaults);
      fields = fieldnames(param_defaults);
      param_struc = param_defaults.(fields{1});

      %% remove the saved author field
      param_struc = rmfield(param_struc, 'author');

      %       %% fix this for now, and remove as an option
      %       param_struc.search_type = 'grid';

      %       fprintf(1, 'Loading parameter defaults file\n');

    catch
      %% manually enter defaults

      if length(varargin) && ~isempty(varargin{1})
        %% figure handle was passed in; use figure handle to get figure data

        sensor_geom = data.browser.sound.geometry.local;
        %% get min, max of x
        %xrange = minmax(sensor_geom(:, 1)'); %% not part of standard tool box, so replace with
        xrange(1) = min(sensor_geom(:, 1));
        xrange(2) = max(sensor_geom(:, 1));
        %% get min, max of y
        %yrange = minmax(sensor_geom(:, 2)'); %% not part of standard tool box, so replace with
        yrange(1) = min(sensor_geom(:, 2));
        yrange(2) = max(sensor_geom(:, 2));
        param_struc.search_radius = sqrt(diff(xrange).^2 + diff(yrange).^2);
        param_struc.term_crit = max(10, min(1e5, param_struc.search_radius / 100));

        %% get spec params from browser
        param_struc.spc_fft = data.browser.specgram.fft;
        param_struc.spc_hop = data.browser.specgram.hop;
        param_struc.spc_win_type = data.browser.specgram.win_type;
        param_struc.spc_win_length = data.browser.specgram.win_length;

      else
        %% no data passed in

        param_struc.search_radius = 10e3;
        param_struc.term_crit = max(10, min(1e5, param_struc.search_radius / 100));

        param_struc.spc_fft = 512;
        param_struc.spc_hop = 0.5;
        param_struc.spc_win_type = 'Hanning';
        param_struc.spc_win_length = 1;

      end

      param_struc.corr_type = 'time waveform';
      param_struc.corr_num = 'N(N-1)/2';
      param_struc.abs_pk_thresh = .5;
      param_struc.rel_pk_thresh = 75;

      param_struc.min_channels = 3;

      %param_struc.filter_length = 1;
      param_struc.search_type = 'grid';
      param_struc.mean_type = 'arithmetic';
      param_struc.search_dim = '2D';
      param_struc.display_plots = 'none';


      %       fprintf(1, 'Defaults file not found! Resetting parameters.\n');


    end


    %% set channels and max_channels to match the number detected from
    %% the browser
    if isempty(param_struc.max_channels) || (param_struc.max_channels ~= num_chan)
      param_struc.max_channels = num_chan;
      param_struc.channels = 1:num_chan;
    end


    %% assign the parameter structure to the output
    varargout{1} = param_struc;




    %% pack parameter cell array into structure
  case ('pack')

    %% rename the variable input argument to
    %% something more descriptive
    param_cell = varargin{1};


    %% create and fill the parameter structure
    param_struc = parameter_fun('create');

    i = 1;
    param_struc.corr_type = param_cell{i};
    i = i + 1;
    param_struc.corr_num = param_cell{i};
    i = i + 1;
    param_struc.abs_pk_thresh = param_cell{i};
    i = i + 1;
    param_struc.rel_pk_thresh = param_cell{i};

    i = i + 1;
    param_struc.min_channels = round(param_cell{i}); %% force to int because of input dialog bug
    i = i + 1;
    param_struc.max_channels = round(param_cell{i}); %% force to int because of input dialog bug

    %% do a check of range here cause no where else to check/fix it
    if param_struc.max_channels < param_struc.min_channels
      param_struc.max_channels = param_struc.min_channels;
    end

    i = i + 1;
    for cell_inx = 1:length(param_cell{i}(:))
      channels(cell_inx) = str2num(param_cell{i}{cell_inx});
    end
    param_struc.channels = channels;

    %     i = i + 1;
    %     param_struc.filter_length = param_cell{i};
    i = i + 1;
    param_struc.search_type = param_cell{i};
    i = i + 1;
    param_struc.search_radius = param_cell{i};
    i = i + 1;
    param_struc.term_crit = param_cell{i};

    i = i + 1;
    param_struc.mean_type = param_cell{i};
    i = i + 1;
    param_struc.search_dim = param_cell{i};
    i = i + 1;
    param_struc.display_plots = param_cell{i};

    i = i + 1;
    param_struc.spc_fft = param_cell{i};
    i = i + 1;
    param_struc.spc_hop = param_cell{i};
    i = i + 1;
    param_struc.spc_win_type = param_cell{i};
    i = i + 1;
    param_struc.spc_win_length = param_cell{i};



    %% assign the parameter structure to the output
    varargout{1} = param_struc;




    %% unpack parameter structure to cell array
  case ('unpack')

    %% rename the variable input argument to
    %% something more descriptive
    param_struc = varargin{1};


    %% unpack the parameter structure
    i = 1;
    param_cell{i} = param_struc.corr_type;
    i = i + 1;
    param_cell{i} = param_struc.corr_num;
    i = i + 1;
    param_cell{i} = param_struc.abs_pk_thresh;
    i = i + 1;
    param_cell{i} = param_struc.rel_pk_thresh;

    %% do a check of range here cause no where else to check/fix it
    if param_struc.max_channels < param_struc.min_channels
      param_struc.max_channels = param_struc.min_channels;
    end

    i = i + 1;
    param_cell{i} = round(param_struc.min_channels); %% force to int because of input dialog bug
    i = i + 1;
    param_cell{i} = round(param_struc.max_channels); %% force to int because of input dialog bug

    i = i + 1;
    param_cell{i} = param_struc.channels;

    %     i = i + 1;
    %     param_cell{i} = param_struc.filter_length;
    i = i + 1;
    param_cell{i} = param_struc.search_type;
    i = i + 1;
    param_cell{i} = param_struc.search_radius;
    i = i + 1;
    param_cell{i} = param_struc.term_crit;

    i = i + 1;
    param_cell{i} = param_struc.mean_type;
    i = i + 1;
    param_cell{i} = param_struc.search_dim;
    i = i + 1;
    param_cell{i} = param_struc.display_plots;

    i = i + 1;
    param_cell{i} = param_struc.spc_fft;
    i = i + 1;
    param_cell{i} = param_struc.spc_hop;
    i = i + 1;
    param_cell{i} = param_struc.spc_win_type;
    i = i + 1;
    param_cell{i} = param_struc.spc_win_length;



    %% assign the parameter cell array to the output
    varargout{1} = param_cell;




    %% create parameter description
  case ('describe')

    if length(varargin)
      param_struc = parameter_fun('create', varargin{1});
      data = get(varargin{1}, 'userdata');
      num_chan = data.browser.sound.channels;
    else
      param_struc = parameter_fun('create');
      num_chan = 21;
    end


    %% set parameter field names (text strings)
    %% set parameter field unit types (text strings)
    i = 1;
    param_field{i} = 'Correlation Type';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Correlation Number';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Abs Corr Peak Thresh (frac)';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Rel Corr Peak Thresh (%)';
    param_type{i} = '';

    i = i + 1;
    param_field{i} = 'Min Channels';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Max Channels';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Channels to Use';
    param_type{i} = '';

    %     i = i + 1;
    %     param_field{i} = 'Filter Length Multiplier';
    %     param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Search Start';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Search Radius (m)';
    param_type{i} = 'm';
    i = i + 1;
    param_field{i} = 'Termination Criterion (m)';
    param_type{i} = 'm';

    i = i + 1;
    param_field{i} = 'Mean Estimator';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Search Dimensionality';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Diagnostic Plots';
    param_type{i} = '';

    i = i + 1;
    param_field{i} = 'FFT Length (pts)';
    param_type{i} = 'pts';
    i = i + 1;
    param_field{i} = 'Data Increment (frac)';
    param_type{i} = 'frac';
    i = i + 1;
    param_field{i} = 'Window Type';
    param_type{i} = '';
    i = i + 1;
    param_field{i} = 'Data Length (frac)';
    param_type{i} = 'frac';



    %% set parameter field prompts for input dialog, must be
    %% a subset of 'param_field'
    %param_input = param_field; %%([1:7, 9:16]);
    param_input = param_field([1,3:7,9:10,12:13]);



    %% set parameter field default values for input_dialog function
    corr_type_def{1} = 'Time Waveform';
    corr_type_def{2} = 'Time Waveform w/ Complex Envelope';
    %corr_type_def{3} = 'spectrogram';
    %corr_type_def{4} = find(strcmpi(corr_type_def, param_struc.corr_type));
    corr_type_def{3} = find(strcmpi(corr_type_def, param_struc.corr_type));

    corr_num_def{1} = 'N-1';
    corr_num_def{2} = 'N(N-1)/2';
    corr_num_def{3} = find(strcmpi(corr_num_def, param_struc.corr_num));

    abs_pk_thresh_def = [param_struc.abs_pk_thresh, 0, 1, 0.25, 0.1, 0];
    rel_pk_thresh_def = [param_struc.rel_pk_thresh, 0, 100, 0.25, 0.1, 0];

    min_channels_def = [param_struc.min_channels, 3, max(4, num_chan), 0.25, 0.1, 2];
    max_channels_def = [param_struc.max_channels, 3, max(4, num_chan), 0.25, 0.1, 2];

    for i=1:num_chan
      channels_def{i} = num2str(i);
    end
    channels_def{end+1} = param_struc.channels';

    %     filter_length_def = [param_struc.filter_length, 1, 20, 0.25, 0.1, 0];

    search_type_def{1} = 'Grid';
    search_type_def{2} = 'Hyperbolic';
    search_type_def{3} = find(strcmpi(search_type_def, param_struc.search_type));

    search_radius_def = [max(0, min(param_struc.search_radius, 1e5)), 0.1, 1e5, 0.25, 0.1, 0];
    term_crit_def = [max(10, min(param_struc.term_crit, 1e5)), 0.1, 1e5, 0.25, 0.1, 0];

    mean_type_def{1} = 'Arithmetic';
    mean_type_def{2} = 'Trimmed';
    mean_type_def{3} = find(strcmpi(mean_type_def, param_struc.mean_type));

    search_dim_def{1} = '2D';
    search_dim_def{2} = '3D';
    search_dim_def{3} = find(strcmpi(search_dim_def, param_struc.search_dim));

    display_plots_def{1} = 'None';
    display_plots_def{2} = 'Text Only';
    display_plots_def{3} = 'Standard';
%     display_plots_def{4} = 'Standard Plus';
%     display_plots_def{5} = 'Deep';
%     display_plots_def{6} = 'Deep Plus';
%     display_plots_def{7} = find(strcmpi(display_plots_def, param_struc.display_plots));
    display_plots_def{4} = find(strcmpi(display_plots_def, param_struc.display_plots));

    spc_fft_def = [param_struc.spc_fft, 8, 65536, 0.25, 0.1, 2];
    spc_hop_def = [param_struc.spc_hop, 0, 1, 0.25, 0.1, 0];
    spc_win_type_def = window_to_fun2;
    spc_win_type_def{end + 1} = find(strcmpi(spc_win_type_def, param_struc.spc_win_type));
    spc_win_length_def = [param_struc.spc_win_length, 0, 1, 0.25, 0.1, 0];




    %% set parameter field defaults,
    %% set parameter field tips, and
    %% set parameter field display size for input_dialog
    i = 1;
    param_default{i} = corr_type_def;
    param_tip{i} = 'Choose cross-correlation method to use.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = corr_num_def;
    param_tip{i} = 'Choose number of correlation functions to use in location estimation.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = abs_pk_thresh_def;
    param_tip{i} = 'Set absolute correlation peak threshold (0-1 of normalized xcorr fun) for retaining channels.';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = rel_pk_thresh_def;
    param_tip{i} = 'Set relative correlation peak threshold (0-100% of global corr peak) for finding putative cross-channel signals.';
    param_line(i,:) = [1, 55];

    i = i + 1;
    param_default{i} = min_channels_def;
    param_tip{i} = 'Set minimum number of channels to use.';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = max_channels_def;
    param_tip{i} = 'Set maximum number of channels to use.';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = channels_def;
    param_tip{i} = 'Select channels to use for location calculation.';
    param_line(i,:) = [4, 55];

    %     i = i + 1;
    %     param_default{i} = filter_length_def;
    %     param_tip{i} = 'Set filter length multiplier for correlation smoothing';
    %     param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = search_type_def;
    param_tip{i} = 'Choose starting arrangement of points to begin location search.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = search_radius_def;
    param_tip{i} = 'Set radius of search area from the center of the array (m).';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = term_crit_def;
    param_tip{i} = 'Set search termination criterion as median centroid distance (m) of top locations.';
    param_line(i,:) = [1, 55];

    i = i + 1;
    param_default{i} = mean_type_def;
    param_tip{i} = 'Select mean estimator to use.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = search_dim_def;
    param_tip{i} = 'Choose dimensionality for search.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = display_plots_def;
    param_tip{i} = 'Choose level of diagnostic plots to show.';
    param_line(i,:) = [1, 45];

    i = i + 1;
    param_default{i} = spc_fft_def;
    param_tip{i} =  'Enter FFT size in points.';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = spc_hop_def;
    param_tip{i} =  'Enter data increment as a fraction of FFT length.';
    param_line(i,:) = [1, 55];
    i = i + 1;
    param_default{i} = spc_win_type_def;
    param_tip{i} =  'Select windowing function to use.';
    param_line(i,:) = [1, 45];
    i = i + 1;
    param_default{i} = spc_win_length_def;
    param_tip{i} =  'Enter data length as a fraction of FFT length.';
    param_line(i,:) = [1, 55];




    %% set parameter fields for menu, must be
    %% a subset of 'pfield'
    param_menu = param_field;


    %% set separators for menu
    param_separator = [];


    %% fill the parameter description structure
    param_descrip.field = param_field;
    param_descrip.type = param_type;

    param_descrip.default = param_default;
    param_descrip.input = param_input;
    param_descrip.line = param_line;
    param_descrip.tip = param_tip;

    param_descrip.menu = param_menu;
    param_descrip.separator = param_separator;



    %% assign the parameter description to the output
    varargout{1} = param_descrip;



end  % switch(mode)

return;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  value_fun
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = value_fun(mode,varargin)

%%
%% 'value_fun'
%%
%% measurement value handling function
%% with four calling modes
%%
%%
%% --------------------------------------------
%%
%%
%% syntax:
%% -------
%%
%%   value_struc = value_fun('create');
%%
%%   value_struc = value_fun('pack',value_cell);
%%
%%   value_cell = value_fun('unpack',value_struc);
%%
%%   value_descrip = value_fun('describe');
%%
%%
%% input:
%% ------
%%   mode = string describing function calling mode
%%   varargin:
%%       value_cell = measurement values in value cell array
%%       value_struc = measurement values in value structure
%%
%%
%% output:
%% -------
%%   varargout:
%%       value_struc = measurement values in value structure
%%       value_cell = measurement values in value cell array
%%       value_descrip = measurement value description structure
%%           .field = value field names
%%           .type = value field types
%%           .menu = menu fields
%%           .separator = field separators in menu
%%


%% set default calling mode
if ~nargin
  mode = 'create';
end



%% compute according to mode
switch (mode)


  %% create measurement value structure
  case ('create')

    %% create value structure
    value_struc.xlocation = []; %% x-coord of top location
    value_struc.ylocation = []; %% y-coord of top location
    value_struc.zlocation = []; %% z-coord of top location
    value_struc.xyz_corr_sum = []; %% correlation sum associated with top location

    value_struc.all_locations = []; %% x,y,z,corr-sum for all N top locations

    value_struc.cent_dist = []; %% median centroid distance for all N top locations
    value_struc.iterations = []; %% number of iterations used in search

    value_struc.channels_used = []; %% channels finally used
    value_struc.peak_vals = []; %% peaks of ~N-1 correlation functions used (with ref chan)
    value_struc.channels_wts = []; %% channel weights used

    value_struc.peak_vals_sm = []; %% peaks of ~N-1 correlation functions used (with ref chan) after smoothing
    value_struc.peak_lags = []; %% lags at peaks of ~N-1 correlation functions used (with ref chan)

    value_struc.loc_lags = []; %% lags corresponding to top location for ~N-1 channels (with ref chan)

    value_struc.range = []; %% location range in m from array (0, 0)
    value_struc.bearing = []; %% location bearing angle in degrees clockwise from y-axis
    value_struc.elevation = []; %% location altitude angle in degrees counter-clockwise from xy-plane

    value_struc.dist2channels = []; %% dist from source loc to channels finally used

    %% assign the value structure to the output
    varargout{1} = value_struc;




    %% pack value cell array into value structure
  case ('pack')

    %% rename the input argument
    value_cell = varargin{1};


    %% create and fill the value structure
    value_struc = value_fun('create');

    i = 1;
    value_struc.xlocation = value_cell{i};
    i = i + 1;
    value_struc.ylocation = value_cell{i};
    i = i + 1;
    value_struc.zlocation = value_cell{i};
    i = i + 1;
    value_struc.xyz_corr_sum = value_cell{i};

    i = i + 1;
    value_struc.all_locations = value_cell{i};

    i = i + 1;
    value_struc.cent_dist = value_cell{i};
    i = i + 1;
    value_struc.iterations = value_cell{i};

    i = i + 1;
    value_struc.channels_used = value_cell{i};
    i = i + 1;
    value_struc.peak_vals = value_cell{i};
    i = i + 1;
    value_struc.channels_wts = value_cell{i};

    i = i + 1;
    value_struc.peak_vals_sm = value_cell{i};
    i = i + 1;
    value_struc.peak_lags = value_cell{i};

    i = i + 1;
    value_struc.loc_lags = value_cell{i};

    i = i + 1;
    value_struc.range = value_cell{i};
    i = i + 1;
    value_struc.bearing = value_cell{i};
    i = i + 1;
    value_struc.elevation = value_cell{i};

    i = i + 1;
    value_struc.dist2channels = value_cell{i};

    %% assign the value structure to the output
    varargout{1} = value_struc;




    %% unpack the value structure into cell array
  case ('unpack')

    %% rename the input argument
    value_struc = varargin{1};


    %% unpack structure into cell array
    i = 1;
    value_cell{i} = value_struc.xlocation;
    i = i + 1;
    value_cell{i} = value_struc.ylocation;
    i = i + 1;
    value_cell{i} = value_struc.zlocation;
    i = i + 1;
    value_cell{i} = value_struc.xyz_corr_sum;

    i = i + 1;
    value_cell{i} = value_struc.all_locations;

    i = i + 1;
    value_cell{i} = value_struc.cent_dist;
    i = i + 1;
    value_cell{i} = value_struc.iterations;

    i = i + 1;
    value_cell{i} = value_struc.channels_used;
    i = i + 1;
    value_cell{i} = value_struc.peak_vals;
    i = i + 1;
    value_cell{i} = value_struc.channels_wts;

    i = i + 1;
    value_cell{i} = value_struc.peak_vals_sm;
    i = i + 1;
    value_cell{i} = value_struc.peak_lags;

    i = i + 1;
    value_cell{i} = value_struc.loc_lags;

    i = i + 1;
    value_cell{i} = value_struc.range;
    i = i + 1;
    value_cell{i} = value_struc.bearing;
    i = i + 1;
    value_cell{i} = value_struc.elevation;

    i = i + 1;
    value_cell{i} = value_struc.dist2channels;

    %% assign the value cell array to the output
    varargout{1} = value_cell;




    %% create measurement value description
  case ('describe')


    %% set value field names (text strings)
    %% set value field unit types (text strings)
    i = 1;
    vfield{i} = 'loc x-coord';
    vtype{i} = '(meters)';
    i = i + 1;
    vfield{i} = 'loc y-coord';
    vtype{i} = '(meters)';
    i = i + 1;
    vfield{i} = 'loc z-coord';
    vtype{i} = '(meters)';
    i = i + 1;
    vfield{i} = 'loc avg corr sum';
    vtype{i} = '';

    i = i + 1;
    vfield{i} = 'all top locs and corr sums';
    vtype{i} = '';

    i = i + 1;
    vfield{i} = 'med cent dist of top locs';
    vtype{i} = '(meters)';
    i = i + 1;
    vfield{i} = '# of search iterations';
    vtype{i} = '';

    i = i + 1;
    vfield{i} = 'channels used';
    vtype{i} = '';
    i = i + 1;
    vfield{i} = 'N-1 correlation peaks';
    vtype{i} = '(frac)';
    i = i + 1;
    vfield{i} = 'channel wts';
    vtype{i} = '';

    i = i + 1;
    vfield{i} = 'N-1 peaks smoothed';
    vtype{i} = '(frac)';
    i = i + 1;
    vfield{i} = 'N-1 peak lags';
    vtype{i} = '(secs)';

    i = i + 1;
    vfield{i} = 'N-1 loc lags';
    vtype{i} = '(secs)';

    i = i + 1;
    vfield{i} = 'loc range';
    vtype{i} = '(meters)';
    i = i + 1;
    vfield{i} = 'loc bearing';
    vtype{i} = '(degrees)';
    i = i + 1;
    vfield{i} = 'loc elevation';
    vtype{i} = '(degrees)';

    i = i + 1;
    vfield{i} = 'dist to sensors';
    vtype{i} = '(meters)';



    %% set value fields for menu, must be
    %% a subset of 'vfield'
    %% I want to change the order of how these display
    vmenu = [vfield(1:7), vfield(9), vfield(8), vfield(10:17)];


    %% set separators for menu
    vseparator = [5, 6, 8, 11, 13, 14, 17];


    %% fill the value description structure
    value_descrip.field = vfield;
    value_descrip.type = vtype;
    value_descrip.menu = vmenu;
    value_descrip.separator = vseparator;


    %% output value description
    varargout{1} = value_descrip;


end

return;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  MEASUREMENT_COMPUTE
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function value = measurement_compute(sound, event, param, version1, version2)

%%
%% 'measurement_compute'
%%
%% compute the actual RL measurement
%%
%% Kathryn A. Cortopassi, April 2004
%% --------------------------------------------
%%
%% syntax:
%% -------
%%   value = measurement_compute(sound, event, param, version1, version2)
%%
%%
%% input:
%% ------
%%   sound = sound associated with event
%%   event = event to measure, a single event
%%   param = measurement parameter structure
%%   version2 = version number of measurement
%%
%%
%% output:
%% -------
%%   value = measurement value structure
%%


%% start by packing memory
%pack;

%% create measurement value structure
value = value_fun('create');

%% get the handle to any open waitbars
wh = findall(0, 'type', 'figure');
wh = wh(find(mod(wh, 1)));


%% get active browser handle to pass into calculate function
brows_h = get_active_browser;


try
  %% to get values

  fprintf(1, '\n\nLocating Event # %d (using ''Source Location (%s)'').\n\n', event.id, version2);
  eval(['value = calc_Location_', version1, '_measurement(brows_h, sound, event, param, value);']);

  if isempty(value)
    fprintf(1, '\nProblem encountered. Event # %d not located.\n', event.id);
  else
    fprintf(1, '\nFinished Locating Event # %d.\n\n', event.id);
  end

catch
  %% bow-out gracefully

  fprintf(1, '\nLocation %s Measurement computation: LASTERR == %s\n', version2, lasterr);
  fprintf(1, '\nProblem encountered. Event # %d not located.\n', event.id);

  %% find any new open waitbars that were left dangling by the measurement
  %% computation and close them
  wh_new = findall(0, 'type', 'figure');
  wh_new = wh_new(find(mod(wh_new, 1)));
  wh_new = wh_new(~ismember(wh_new, wh));
  close(wh_new);

end


return;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  MEASUREMENT_DISPLAY
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function g = measurement_display(h, m, event, ixa, description, data, version1)

%%
%% 'measurement_display'
%%
%% create graphical display of measurement data in browser window
%% ------------------------------------------------------------------
%%
%% syntax:
%% ------
%% g = measurement_display(h,m,event,ixa,description,data,version1)
%%
%% input:
%% ------
%%  h - handle to figure
%%  m - log index
%%  event - annotated event
%%  ixa - measurement index
%%  description - measurement description
%%  data - figure userdata context
%%
%% output:
%% -------
%%  g - handles to created graphic objects
%%


%g = display_Location_v2p2_measurement(h, m, event, ixa, description, data);
eval(['g = display_Location_', version1, '_measurement_pitz(h, m, event, ixa, description, data);']);

return;







%*********************************************************
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  OPTIONAL EDITABLE SUB-FUNCTIONS FOR MEASUREMENT
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%*********************************************************




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------
%%  parameter_edit
%%--------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [param, author] = parameter_edit(description, title, param, author, version1, recompute_flag)

%% 'parameter_edit'
%%
%% update and edit measurement parameters using input_dialog
%%
%% -----------------------------------------------------------------
%%
%% syntax:
%% ------
%% [param,author] = parameter_edit(description,title,param,author)
%%
%% input:
%% ------
%%  description = measurement description structure
%%  title = title for input edit dialog
%%  param = measurement parameter structure
%%  author = current author
%%
%% output:
%% -------
%%  param = parameter structure
%%  author = current author of parameters


%% get measurement name and parameter description
name = description.name;
parameter = description.parameter;


%% unpack parameter into cell array
param_cell = parameter_fun('unpack', param);


%% get indices of the editable parameter fields
n = length(parameter.input);

for k = 1:n
  ix(k) = find(strcmp(parameter.field, parameter.input(k)));
  %% finds index of parameter.input(k) in the cell array parameter.field
end


%% KAC no longer use this Feb 2007 !!
% %% fixed this! KAC, 7 April 2004
% % %% ensure that author field is editable
% % ix = [ix, n + 1];
% nn = length(parameter.field);
% ix = [ix, nn+1];

%% tack on author field at the end of parameter structure
nn = length(parameter.field);
ix = [ix, nn+1];



%% update parameter default values using current values
for k = 1:n

  %% update according to input type
  switch (class(parameter.default{ix(k)}))

    %% editable text box
    case ('char')

      parameter.default{ix(k)} = param_cell{ix(k)};

      %% slider input
    case ('double')

      parameter.default{ix(k)}(1) = param_cell{ix(k)};

      %% popup or listbox input
    case ('cell')

      %% KAC mod to this for listbox function 28 Feb 2005
      %% change 'strcmp' to 'ismember'
      if isnumeric(param_cell{ix(k)})
        for index = 1:length(param_cell{ix(k)})
          temp_cell{index} = num2str(param_cell{ix(k)}(index));
        end
        ixl = find(ismember(parameter.default{ix(k)}(1:end-1), temp_cell));
      else
        ixl = find(ismember(parameter.default{ix(k)}(1:end-1), param_cell{ix(k)}));
      end

      %% fixed this! KAC, 9 April 2004
      if ~isempty(ixl)
        % if ~isempty(ix)
        parameter.default{ix(k)}{end} = ixl;
      else
        parameter.default{ix(k)}{end} = 1;
      end

  end

end


%% KAC no longer use this Feb 2007 !!
% %% fixed this! KAC, 7 April 2004
% % %% add author field to input dialog
% % parameter.field{n + 1} = 'Author';
% % parameter.default{n + 1} = author;
% % parameter.line(n + 1,:) = [1,26];
% % parameter.tip{n + 1} = 'Author of current measurement parameters';
% parameter.field{nn+1} = 'Author';
% parameter.input{n+1} = parameter.field{end};
% parameter.default{nn+1} = author;
% parameter.line(nn+1,:) = [1, 45];
% parameter.tip{nn+1} = 'Author of current measurement parameters';

%% tack on an author field
if ~recompute_flag
  %% get author from saved defaults file
  try

    %% get directory and name of the measurement, and ultimately location of the parameter defaults
    param_defaults = [pathof(mfilename('fullpath')), '__IGNORE\Location_', version1, '_measurement_PRE\param_defaults.mat'];

    %% load up defaults
    param_defaults = load(param_defaults);
    fields = fieldnames(param_defaults);
    param_struc = param_defaults.(fields{1});

    %% get the default author field
    author = param_struc.author;

    %   fprintf(1, 'Loading default author\n');

  end
end

parameter.field{nn+1} = 'Run Author';
parameter.input{n+1} = parameter.field{end};
parameter.default{nn+1} = author;
parameter.line(nn+1,:) = [1, 45];
parameter.tip{nn+1} = 'Enter author of measurement run / parameter set';


%% use 'input_dialog' to allow for parameter editing

%% KAC April 2005
%% KAC put back in split Feb 2007 !!
%% split the input dialog into two to ease viewing

group1len = 6;

input_values_1 = input_dialog( ...
  parameter.field(ix(1:group1len)), ...
  [name '  -  ' title ' Input I'], ...
  parameter.line(ix(1:group1len),:), ...
  parameter.default(ix(1:group1len)), ...
  parameter.tip(ix(1:group1len)) ...
  );


if isempty(input_values_1)
  input_values = [];

else
%   input_values_2 = input_dialog( ...
%     parameter.field(ix([group1len+1:end-5, end])), ...
%     [name '  -  ' title ' Input II'], ...
%     parameter.line(ix([group1len+1:end-5, end]),:), ...
%     parameter.default(ix([group1len+1:end-5, end])), ...
%     parameter.tip(ix([group1len+1:end-5, end])) ...
%     );
  input_values_2 = input_dialog( ...
    parameter.field(ix([group1len+1:end])), ...
    [name '  -  ' title ' Input II'], ...
    parameter.line(ix([group1len+1:end]),:), ...
    parameter.default(ix([group1len+1:end])), ...
    parameter.tip(ix([group1len+1:end])) ...
    );


  %% check for a cancel requested in either dialog
  if isempty(input_values_2)
    input_values = [];
  else
    input_values = [input_values_1; input_values_2];
  end

end


%% KAC put back in split Feb 2007 !!
%% now this all goes away Feb 2007 !!
%%
% %% KAC May 2005
% %% split the input dialog; let second dialog be triggered
% %% by input into the first
%
% % %% for demo!!!
% % input_values_1 = input_dialog( ...
% %   parameter.field(ix), ...
% %   [name ' - Input (Log : ' title ')'], ...
% %   parameter.line(ix,:), ...
% %   parameter.default(ix), ...
% %   parameter.tip(ix) ...
% %   );
%
% group1len = 13;
%
% %author_inx = group1len + 1;
%
% input_values_1 = input_dialog( ...
%   parameter.field(ix([1:group1len, end])), ...
%   [name ' - Input I  (Log : ' title ')'], ...
%   parameter.line(ix([1:group1len, end]),:), ...
%   parameter.default(ix([1:group1len, end])), ...
%   parameter.tip(ix([1:group1len, end])) ...
%   );

input_values_1 = input_values;

if ~isempty(input_values_1) && strcmpi(input_values_1{1}, 'spectrogram')
  %% get spectrogram parameters

  input_values_2 = input_dialog( ...
    parameter.field(ix(end-5:end-1)), ...
    [name ' - Input II  (Log : ' title ')'], ...
    parameter.line(ix(end-5:end-1),:), ...
    parameter.default(ix(end-5:end-1)), ...
    parameter.tip(ix(end-5:end-1)) ...
    );

  %% check for a cancel requested in 2nd dialog
  if isempty(input_values_2)
    input_values = [];
  else
    input_values = [input_values_1(1:end-1); input_values_2; input_values_1(end)];
  end

  %else
  %% update length of params to save, and save inputs

  % n = group1len;
  % input_values = input_values_1;

end


%% KAC May 2005

% input_values = input_dialog( ...
%   parameter.input, ...
%   [name '  -  ' title], ...
%   parameter.line(ix, :), ...
%   parameter.default(ix), ...
%   parameter.tip(ix) ...
%   );

%% KAC April 2005


%% update parameter structure
if (isempty(input_values))

  param = [];
  author = '';

else

  %% KAC no longer use this Feb 2007 !!
  %author = author_validate(input_values{n + 1});
  %author = author_validate(input_values{n});
  %   %% KAC change
  %   author = author_validate(input_values{author_inx});


  %   if isempty(author)
  %
  %     param = [];
  %
  %   else

  %% debug line for weird input_dialog behavior re: slider/edit
  %% controls which are set to return integer display ==>
  %% they do not when clicking in bar off an integer position
  %% or sliding slider to a non-integer position (even though
  %% it pops to an integer position and shows an integer in the
  %% edit window), in this case they actually return the floating
  %% point values of the non-integer positions
  %       %% want to see entries returned
  %       input_values

  n = length(input_values);
  for k = 1:n-1
    param_cell{ix(k)} = input_values{k};
  end
  %   param_cell{ix(end)} = input_values{end};

  param = parameter_fun('pack', param_cell);
  author = input_values{end};

  %% debug line for weird input_dialog behavior re: slider/edit
  %% controls which are set to return integer display ==>
  %% they do not when clicking in bar off an integer position
  %% or sliding slider to a non-integer position (even though
  %% it pops to an integer position and shows an integer in the
  %% edit window), in this case they actually return the floating
  %% point values of the non-integer positions
  %       %% want to see results of 'pack'
  %       param


  %% KAC ADDITION, 31 March 2005
  %% save default parameters file
  try
    %% get directory and name of the measurement, and ultimately location of the parameter defaults
    param_defaults = [pathof(mfilename('fullpath')), '__IGNORE\Location_', version1, '_measurement_PRE\param_defaults.mat'];
    param_copy = param;
    param_copy.author = input_values{end};
    save(param_defaults, 'param_copy', '-mat');
    %     fprintf(1, 'Saving parameter default file\n');
  catch
    %% do nothing
    %     fprintf(1, 'NOT saving parameter defaults\n');
  end
  %% KAC ADDITION, 31 March 2005


  %   end

end






%--------------------------------------------------------
%  MEASUREMENT_MENU (DEFAULT)
%--------------------------------------------------------

function g = measurement_menu(h,m,event,ixa,description,data)

% measurement_menu - create description generated menu display
% ------------------------------------------------------------
%
% g = measurement_menu(h,m,event,ixa,description,data)
%
% Input:
% ------
%  h - handle to parent
%  m - log index
%  event - measured event
%  ixa - measurement index in event
%  description - measurement description
%  data - figure userdata context
%
% Output:
% -------
%  g - handles to created menus

%--
% unpack measurement structures to cell arrays
%--

param = parameter_fun('unpack',event.measurement(ixa).parameter);

value = value_fun('unpack',event.measurement(ixa).value);

%--
% create value menu
%--

name = description.name;

description = description.value;

n = length(description.menu);
L = cell(1,n);
j = 1;

for k = 1:n

  %--
  % get field index
  %--

  ixf = find(strcmp(description.field,description.menu{k}));

  %--
  % create menu label depending on value type
  %--

  if (isempty(value{ixf}))

    L{k} = [title_caps(description.field{ixf},'_'), ':  (Empty)'];

  else

    L{k} = [title_caps(description.field{ixf},'_'), ':  '];

    %--
    % display according to value class and type
    %--

    switch (class(value{ixf}))

      %--
      % string value
      %--

      case ('char')

        %                 if (length(value{ixf}) <= 32)
        %                     L{k} = [L{k} value{ixf}];
        %                 else
        %                     L{k} = [L{k} value{ixf}(1:28) ' ...'];
        %                 end

        %% KAC addition to default measurement display
        if size(value{ixf}, 1) == 1
          if (length(value{ixf}) <= 32)
            L{k} = [L{k} value{ixf}];
          else
            L{k} = [L{k} value{ixf}(1:28) ' ...'];
          end
        else
          L{k} = [L{k} 'matrix of values'];
        end


        %--
        % number value
        %--

      case ('double')

        %--
        % get type of value field
        %--

        type = description.type{ixf};

        %--
        % create menu according to value type
        %--

        switch (type)

          %--
          % datetime and time display
          %--

          case ('datetime')

            tmp = data.browser.grid.time.labels;

            %                         if (strcmp(tmp,'seconds'))
            %                             L{k} = [L{k} num2str(value{ixf})];
            %                         else
            %                             if (data.browser.grid.time.realtime & ~isempty(data.browser.sound.realtime))
            %
            %                                 date = datevec(data.browser.sound.realtime);
            %                                 time = date(4:6)*[3600,60,1]';
            %                                 L{k} = [L{k} sec_to_clock(time + value{ixf})];
            %
            %                             else
            %                                 L{k} = [L{k} sec_to_clock(value{ixf})];
            %                             end
            %                         end

            %% KAC addition to default measurement display
            if size(value{ixf}, 1) == 1
              if (strcmp(tmp,'seconds'))
                L{k} = [L{k} num2str(value{ixf})];
              else
                if (data.browser.grid.time.realtime & ~isempty(data.browser.sound.realtime))

                  date = datevec(data.browser.sound.realtime);
                  time = date(4:6)*[3600,60,1]';
                  L{k} = [L{k} sec_to_clock(time + value{ixf})];

                else
                  L{k} = [L{k} sec_to_clock(value{ixf})];
                end
              end
            else
              L{k} = [L{k} 'matrix of values'];
            end

          case ('time')

            tmp = data.browser.grid.time.labels;

            %                         if (strcmp(tmp,'seconds'))
            %                             %% KAC addition default measurement display
            %                             %L{k} = [L{k} num2str(value{ixf})];
            %                             L{k} = [L{k} num2str(value{ixf}) ' sec'];
            %
            %                         else
            %                             L{k} = [L{k} sec_to_clock(value{ixf})];
            %                         end

            %% KAC addition to default measurement display
            if size(value{ixf}, 1) == 1
              if (strcmp(tmp,'seconds'))
                L{k} = [L{k} num2str(value{ixf}) ' sec'];
              else
                nn = length(value{ixf});
                if nn == 1
                  times = sec_to_clock(value{ixf});
                  L{k} = [L{k} times ' clock time'];
                else
                  times = sec_to_clock(value{ixf});
                  for i = 1:nn
                    times{i} = [times{i} '   '];
                  end
                  times = [times{:}];
                  L{k} = [L{k} times ' clock time'];
                  %% notice transpose in case values are a
                  %% vector
                end
              end
            else
              L{k} = [L{k} 'matrix of values'];
            end


            %--
            % frequency display
            %--

          case ('freq')

            tmp = data.browser.grid.freq.labels;

            %% KAC addition to default measurement display
            %                         if (strcmp(tmp,'kHz'))
            %                             L{k} = [L{k} num2str(value{ixf} / 1000) ' kHz'];
            %                         else
            %                             L{k} = [L{k} num2str(value{ixf}) ' Hz'];
            %                         end

            %% KAC addition to default measurement display
            if size(value{ixf}, 1) == 1
              if (strcmp(tmp,'kHz'))
                L{k} = [L{k} num2str(value{ixf} / 1000) ' kHz'];
              else
                L{k} = [L{k} num2str(value{ixf}) ' Hz'];
              end

            else
              L{k} = [L{k} 'matrix of values'];
            end

            %--
            % miscellaneous unit or no unit display
            %--

          otherwise

            %% KAC addition to default measurement display
            if size(value{ixf}, 1) == 1 | size(value{ixf}, 2) == 1
              %L{k} = [L{k} num2str(value{ixf}) ' ' type];
              my_tmp_str = num2str(value{ixf}(1), '%.2f');
              for str_inx = 2:length(value{ixf})
                my_tmp_str = [my_tmp_str, num2str(value{ixf}(str_inx), ', %.2f')];
              end
              L{k} = [L{k} my_tmp_str ' ' type];
            else
              L{k} = [L{k} 'matrix of values'];
            end

        end

        %--
        % cell array value (this is not fully supported, currently only strings)
        %--

      case ('cell')

        if (length(value{ixf}) == 1)
          L{k} = [L{k} value{ixf}{1}];
        else
          ixc(j) = ixf;
          M{j} = L{k};
          j = j + 1;
        end

    end

  end

end

L{n + 1} = [name ' Info:'];

%--
% set separators
%--

S = bin2str(zeros(1,n + 3));

for k = 1:length(description.separator)
  S{description.separator(k)} = 'on';
end

S{n + 1} = 'on';

%--
% create menu items with empty callbacks
%--

g1 = menu_group(h,'',L,S);

%--
% disable empty field menus to de-emphasize
%--

for k = 1:length(L)
  if (~isempty(findstr(L{k},'(Empty)')))
    set(g1(k),'enable','off');
  end
end

%--
% add author, creation and modification date information fields
%--

L = { ...
  ['Author:  ' event.measurement(ixa).author], ...
  ['Created:  ' datestr(event.measurement(ixa).created)] ...
  };

if (~isempty(event.measurement(ixa).modified))
  L{3} = ['Modified:  ' datestr(event.measurement(ixa).modified)];
end

S = bin2str(zeros(1,3));
S{2} = 'on';

g2 = menu_group(get_menu(g1,[name ' Info:']),'',L,S);

%--
% put all handles together
%--

g = [g1,g2];

%--
% create submenus for cell arrays if needed (only strings are currently supported)
%--

if (j > 1)

  for k = 1:length(M)
    L = cell(0);
    tmp = value{ixc(k)};
    for j = 1:length(tmp)
      L{j} = tmp{j};
    end
    gk{k} = menu_group(get_menu(h,M{k}),'',L);
  end

  %--
  % append submenu handles to other menu handles
  %--

  for k = 1:length(M)
    g = [g, gk{k}];
  end

end