function [result, context] = compute(log, parameter, context)
result = struct;

%% PRESENCE - compute
%
% Tabulates number of events in user-defined time bins on each channel, 
% combining events from multiple logs on same day if they occur in multiple
% logs.
%
% Filters events with a user-definable tag for each channel of
% each selected sound.
%
% Note: If 'tag' parameter is left blank, all events will be counted.
%
% *************************************************************************
% Tabulates time based on log.event().time(1)  !!!!
%
% TO DO
%
% 1. Use entire span of time(1) and time(2) to determine presence and
% count!!!
%
% 2. DailyPresenceSummary - SUM line OK, preceding line has no meaningful
% data, other than the actual date range. GET RID OF IT!!!
%
% 3. List names of all logs processed to make each CSV.
%
% *************************************************************************

%%
%terminate if fatal condition encountered
if context.state.kill
  return;
end

%initializations
len = log.length;
event = log.event;
fn = log.file;
fn = fn(1:end-4);

TargetTag = parameter.tag;
day = floor(log.sound.realtime) - context.state.FirstDate + 1;

%check Number of channels same in log and context
NumChan = context.state.NumChan;
if ~isequal(log.sound.channels, NumChan)
  fprintf(2,'\nNum Channels not consistant. Log skipped: %s\n', fn);
  return;
  
elseif any(any(context.state.NumEvents(day,:,:)))
  fprintf(1,'WARNING: Previous log had events on same day as %s\n', fn);
  
else
  fprintf(1,'Log read: %s\n', fn);

end

%initialize NumEvents matrix for current log
NumBins = context.state.NumBins;  %number of time bins each day
BinLen = context.state.BinLen;    %length of time bins in seconds
NumEvents = zeros(1, NumChan, NumBins); %matrix for counting events

%Count # events in each channel for current log
if isempty(TargetTag)  %if no target tag, count all events
% if isequal(numel(TargetTag),0)  %if no target tag, count all events

  for i = 1:len
    
    bin = ceil(event(i).time(1) / BinLen);
    
    %set bin = 1 if time = 0
    if ~bin
      bin = 1;
    end
    
    NumEvents(1,event(i).channel,bin) = NumEvents(1,event(i).channel,bin) + 1;
    
  end
  
else                            %if target tag, only count target events
  for i = 1:len
    %convert tag to string if necessary
    tag = event(i).tags;
    if isequal(length(tag),0)
      tag='';
    elseif iscell(tag)
      tag = tag{1};
    end

    %count event only if proper tag
    if strcmpi(tag,TargetTag)
      bin = ceil(event(i).time(1) / BinLen);
      NumEvents(1,event(i).channel,bin) = NumEvents(1,event(i).channel,bin) + 1;
    end
  end
end


%add callcount for current log to callcount for all days
context.state.NumEvents(day,:,:) = context.state.NumEvents(day,:,:) + NumEvents;

return;

% HOURLY PRESENCE - compute


%%
%Log Action "Hourly Presence"

%Creates CSV with hourly presence of events by channel

%%


if log_is_open(log)
    disp(['Close log ''', log_name(log), ...
      ' before calculating hourly presense.']); return;
end

nChannels = log.sound.channels;
len = length(log.event);
count=zeros(nChannels,24);

for i = 1:len
  hh = ceil(log.event(1,i).time(1)/3600);
  channel = log.event(1,i).channel;
  count(channel,hh) = count(channel,hh)+1;
end

% for i=1:nChannels
%   for j=1:24
%     if count(i,j)==0
%       count(i,j)=NaN;
%     end
%   end
% end

for i = 1:nChannels
  ChannelLabel(i,1) = i;
end

M = [ChannelLabel count];

HourLabel(1,1) = NaN;
for i = 2:25
  HourLabel(1,i) = i-2;
end
  
M = [HourLabel; M];

fn = log.file;
fn = fn(1:end-4);
fn = [fn '_HourlyPresence'];
xlswrite(fn,M)

context.state.count = context.state.count + sum(sum(count));