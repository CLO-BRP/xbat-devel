function M = make_sensor_sheet(log)


%% read attributes from log

sound_speed = [];

sound_speed_uncertainty = [];

local = [];

global_geo = [];

offset = [];

ellipsoid = [];

sensor_geometry_uncertainty = [];


for i = 1:length(log.sound.attributes)
    
    curr_attribute = log.sound.attributes(i);
    
    if strcmp(curr_attribute.name, 'sound_speed')
        
        sound_speed = curr_attribute.value.speed;
        
        
    elseif strcmp(curr_attribute.name, 'sound_speed_uncertainty')
        
        sound_speed_uncertainty = curr_attribute.value.uncertainty.uncertainty;
        
        
    elseif strcmp(curr_attribute.name, 'sensor_geometry')
        
        local = curr_attribute.value.local;
        
        latlon = curr_attribute.value.global;
        
%         offset = curr_attribute.value.offset;
        
        ellipsoid = curr_attribute.value.ellipsoid;
        
    elseif strcmp(curr_attribute.name, 'sensor_geometry_uncertainty')
        
        sensor_geometry_uncertainty = curr_attribute.value.uncertainty;
        
    end
end


%% create output matrices

M = cell(0,0);

num_columns = max(11, log.sound.channels+2);


%% sound speed

M_sound_speed = cell(5,num_columns);

M_sound_speed{1,1} = 'Sound Speed (M/s)';

M_sound_speed{3,2} = num2str(sound_speed, '%.2f');
 
M = [M ; M_sound_speed];


%% sound speed uncertainty

M_sound_speed_uncertainty = cell(6,num_columns);

M_sound_speed_uncertainty{1,1} = 'Sound Speed Uncertainty (M/s)';

M_sound_speed_uncertainty(3,2:3) = {'speed-', 'speed+'};

if isequal(size(sound_speed_uncertainty,2),2)

    M_sound_speed_uncertainty{4,2} = num2str(sound_speed_uncertainty(1),'%.2f');

    M_sound_speed_uncertainty{4,3} = num2str(sound_speed_uncertainty(2),'%.2f');
    
end
 
M = [M ; M_sound_speed_uncertainty];


%% sensor geometry header

M_sensor_geometry_header = cell(4,num_columns);

M_sensor_geometry_header{1,1} = 'Sensor Geometry';


% stop if no sensor geometry is present in log

if isempty(local) || isempty(latlon) || isempty(ellipsoid)
    
    M_sensor_geometry_header{3,2} = sprintf('Not present in %s',log_name(log));
    
    M = [M ; M_sensor_geometry_header];
    
    return;
    
end


%find UTMs

idxL = find(local(:,1)==0);

llref = latlon(idxL,:);

[zone,utm_east,utm_north,hem] = deg2utm(llref(2),llref(1),ellipsoid);

UTMsensor = [local(:,1)' + utm_east ; local(:,2)' + utm_north]';



M_sensor_geometry_header{1,6} = 'UTM Zone';

M_sensor_geometry_header{1,7} = [num2str(zone), hem];

M_sensor_geometry_header{2,6} = 'UTM Datum';

M_sensor_geometry_header{2,7} = ellipsoid;


%column headings for sensor geometry table

M_sensor_geometry_header{4,2} = 'Channel';

M_sensor_geometry_header{4,3} = 'X Location';

M_sensor_geometry_header{4,4} = 'Y Location';

M_sensor_geometry_header{4,6} = 'Easting';

M_sensor_geometry_header{4,7} = 'Northing';

M_sensor_geometry_header{4,9} = 'Latitude';

M_sensor_geometry_header{4,10} = 'Longitude';


M = [M ; M_sensor_geometry_header];


%% sensor geometry

num_chan = size(local,1);

M_sensor_geometry = cell(num_chan+2,num_columns);


for i = 1:num_chan

  M_sensor_geometry{i,2} = num2str(i, '%.0f');

  M_sensor_geometry{i,3} = num2str(local(i,1), '%.2f');

  M_sensor_geometry{i,4} = num2str(local(i,2), '%.2f');

  M_sensor_geometry{i,6} = num2str(UTMsensor(i,1), '%.2f');

  M_sensor_geometry{i,7} = num2str(UTMsensor(i,2), '%.2f');

  M_sensor_geometry{i,9} = num2str(latlon(i,1), '%.9f');

  M_sensor_geometry{i,10} = num2str(latlon(i,2), '%.9f');
  
end


M = [M ; M_sensor_geometry];


%% Uncertainty in inter-MARU distance header

M_distance_uncertainty_header = cell(3,num_columns);

M_distance_uncertainty_header{1,1} = 'Sensor Geometry Uncertainty (M)';

M_distance_uncertainty_header(3,2:6) = {'Channel','x-','x+','y-','y+'};

M = [M ; M_distance_uncertainty_header];


%% Uncertainty in inter-MARU distance

M_distance_uncertainty = cell(num_chan+2,num_columns);

[m0,n0] = size(sensor_geometry_uncertainty);

M_distance_uncertainty(1:num_chan,2) = num2cell(1:num_chan)';

for m = 1:m0
    
    for n = 1:n0
        
        M_distance_uncertainty{m,n+2} = ...
          num2str(sensor_geometry_uncertainty(m,n), '%.2f');
        
    end
    
end

M = [M ; M_distance_uncertainty];


%% inter-MARU distances headers

M_distance_header = cell(3,num_columns);

M_distance_header{1,1} = 'Inter-sensor Distance (M)';

M_distance_header{3,2} = 'Channel';

  
for n = 1:num_chan

  M_distance_header{3,n+2} = sprintf('Dist to %.0f (m)', n);

end


M = [M ; M_distance_header];


%% inter-MARU distances

M_distance = cell(num_chan+2,num_columns);

d_matrix = zeros(num_chan);

M_distance(1:num_chan,2) = num2cell(1:num_chan)';


for m = 1:num_chan    
  
  for n = 1:num_chan
    
    d = sqrt((local(n,1)-local(m,1))^2 + (local(n,2)-local(m,2))^2);
    
    d_matrix(m,n) = d;

    M_distance{m,n+2} = num2str(d, '%.2f');

  end
  
end


M = [M ; M_distance];


%% inter-MARU maximum lag headers

M_lag_header = cell(3,num_columns);

M_lag_header{1,1} = 'Maximum Inter-sensor Lag (s)';

M_lag_header{3,2} = 'Channel';

  
for n = 1:num_chan

  M_lag_header{3,n+2} = sprintf('Max Lag %.0fxRef (sec)', n);

end


M = [M ; M_lag_header];


%% inter-MARU maximum lag

M_lag = cell(num_chan,num_columns);

M_lag(1:num_chan,2) = num2cell(1:num_chan)';


max_lag = d_matrix ./ sound_speed;

for m = 1:num_chan
  
  for n = 1:num_chan

    M_lag{m,n+2} = max_lag(m,n);

  end
  
end


M = [M ; M_lag];

