function [result, context] = compute(log, parameter, context)

% ADD EVENT MEASUREMENTS - compute

% History
%   19 Oct 2014 - Remove warning if preset.mat is missing.

result = struct;

% if context.state.kill
%   
%   return;
%   
% end
% 
% set(context.state.fh, 'Tag','XBAT_SOUND_BROWSER')
% 
% 
% %may need to add 4th input parameter, "measurement"
% log = Location_v2p2_measurement_PRE('batch', log, 1:log.length, context.state.measurement);
% 
% set(context.state.fh, 'Tag','figure1')
% 
% log_save(log);