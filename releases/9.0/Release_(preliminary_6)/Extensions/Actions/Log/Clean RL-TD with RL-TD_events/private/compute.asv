function [result, context] = compute(log, parameter, context)
%
% Clean RL-TD with RL-TD_events - compute
% 
% Purpose: Uses cleaned �best5events� logs to clean time delays in RL-TD log 
% in preparation for whale tracking/counting.
% 
% Input: Pairs of logs, each pair including one cleaned �best5events� log and 
% the RL-TD log it was created from.
% 
% Output: �HandCulled� logs which have RL-TD measurements cleaned using 
% cleaned �best5events� logs.
% 
% User Parameter 1: Log name suffix for RL-TD logs.  
% 
% User Parameter 2: Log name suffix for �best5events� logs.
% 
% Tip: Don�t use suffix pairs where one suffix is the same as the end of the 
% other suffix.  For example, don�t use �RL-TD� and �cleaned_RL-TD�.

%%
%initializations
result = struct;
fnA = log.file;
fnA = fnA(1:end-4);  
% KeepTag = parameter.KeepTag;
RLTDsuffix = parameter.RLTDsuffix;
RLTD_eventsSuffix = parameter.RLTD_eventsSuffix;

% %find datenum of beginning of recording
% file = log.sound.file;
% 
% if iscell(file)
%   file = file{1};
% end

% realtime = file_datenum(file);

% %skip log if sound file name cannot be read
% if isempty(file)
% % if isempty(realtime)
%   
%   fprintf(2,'Log skipped. Sound file names cannot be read for %s\n',fnA);
%   context.state.LastLog = [];
%   context.state.file = [];
%   return;  

%if no previous log waiting for processing with this log
if isempty(context.state.LastLog.sound.file{1})
% elseif isempty(context.state.file)
  
  %save current log to process with next log selected
  context.state.LastLog = log;
  
%if sound does not agree between previous log and current log,
%dump previous log and save current log
elseif ~strcmp(log.sound.file{1}, context.state.LastLog.sound.file{1})
% elseif ~isequal(log.sound.realtime, context.state.LastLog.sound.realtime)
 
  fnB = context.state.LastLog.file;
  fnB = fnB(1:end-4);
  
  context.state.LastLog = [];
  context.state.LastLog = log;
  
  fprintf(2,'%s skipped because sound for\n',fnB);
  fprintf(2,'does not agree with sound for %s\n\n', fnA);

  return;
  
%sound is the same as last log loaded, cull events in RL-TD
%log with info in RL-TD_events log
else
  
  fnB = context.state.LastLog.file;
  fnB = fnB(1:end-4);

  %copy RL-TD log to "RLTDlog" and RL-TD_events log to "EventsLog"
  if strcmp(RLTDsuffix, fnA(end-length(RLTDsuffix)+1:end)) && ...
     strcmp(RLTD_eventsSuffix, fnB(end-length(RLTD_eventsSuffix)+1:end))
   
     RLTDlog = log;
     EventsLog = context.state.LastLog;
     fnRLTD = fnA;
   
  elseif strcmp(RLTDsuffix, fnB(end-length(RLTDsuffix)+1:end)) && ...
         strcmp(RLTD_eventsSuffix, fnA(end-length(RLTD_eventsSuffix)+1:end))
   
     RLTDlog = context.state.LastLog;
     EventsLog = log;
     fnRLTD = fnB;
     
  else
    
    fprintf(2,'Logs skipped because they are improperly named.\n');
    fprintf(2,'  %s\n  %s\n\n', fnA, fnB);

    context.state.LastLog.sound.file = '';
%     context.state.LastLog.sound.realtime = [];
    
    return;

  end
  
  %test if RLTD log has any RLTD measurements
  len = RLTDlog.length;
  isRLTD = 0;
  i = 0;
  
  while i <= len
    
    i = i + 1;
    
    try      
      if ~isempty(RLTDlog.event(i).measurement.value.all_time)

        isRLTD = 1;
        
%         fprintf(1,'Event %.0f has no RLTD measurement in %s\n', ...
%           RLTDlog.event(i).id, ...
%           fnRLTD);

      end
      
    catch      
      %don't do anything      
    end
  end
  
  if ~isRLTD
    fprintf(2,'\n**********************************************************\n\n');
    fprintf(2,'WARNING: Log skipped.  No RLTD measurements in %s\n\n', fnRLTD);
    fprintf(2,'**********************************************************\n\n');
    
    return;
  end
    
  %reset data for LastLog
  context.state.LastLog.sound.file = '';
%   context.state.LastLog.sound.realtime = [];
  
%   len = RLTDlog.length;
  lenA = EventsLog.length;
  numchan = RLTDlog.sound.channels;
  culled_event(1:lenA) = event_create;
  k = 0;
  
  %make vector of RL-TD event ID's referenced in RL-TD_events log
  RefID = zeros(1,lenA);
  for i = 1:lenA

    notes = EventsLog.event(i).notes;
    delim = strfind(notes,',');
    
    if iscell(delim) && ~isempty(delim)
      delim = delim{1};
    end

    %fix cell bug
    if isempty(notes) || isempty(delim)
      fprintf(2,'\nRL-TD_event skipped due to protocol violation.\n');
      fprintf(2,'Notes altered in event %.0f in %s\n', ...
        EventsLog.event(i).id, log_name(EventsLog));
%       return;
      
    else
      if ~iscell(notes)
        notes = {notes};
        notes = notes{1};
      else
        notes = notes{1};
      end

      %extract reference ID from notes
      j = delim(1) - 1;

      while j > 0 && isfinite(str2double(notes(j)))
        j = j - 1;
      end

      RefID(i) = str2double(notes(j+1:delim(1)-1));
      
      k = k + 1;
      culled_event(k) = EventsLog.event(i);
    
    end
  end
  
  EventsLog.event = culled_event(1:k);
  
  %get rid of unused RefID, which correspond to RL-TD events with notes altered
  RefID = RefID(RefID~=0);

  %sort RL-TD_events log by RefID
  [RefID idx] = sort(RefID);
  EventsLog.event = EventsLog.event(idx(:));
  lenA = length(EventsLog.event);

  %make sort index for RL-TD log event ID's in RL-TD_events log
  uniqueRefID = unique(RefID);
  logIDidx = NaN(1,RLTDlog.event(len).id);

  i = 1;
  j = 1;
  LenUniqueRefID = length(uniqueRefID);
  
  % index through events in RL-TD log
  while i<=len  

    %find first RL-TD_event with reference ID matching current RL-TD event
    while j<=LenUniqueRefID && uniqueRefID(j)<RLTDlog.event(i).id  % index through uniqueRefID

      j = j + 1;

    end

    if j > LenUniqueRefID
      fprintf(2,'\n*********************************************************\n\n');
      fprintf(2,'WARNING: Event %.0f in RL-TD log has no corresponding event in RL-TD_events log.\n', ...
              RLTDlog.event(i).id);
      fprintf(2,'   RL-TD log:       %s\n', fnRLTD);
      fprintf(2,'   RL-TD_event log: %s\n', log_name(EventsLog));
      fprintf(2,'\n*********************************************************\n\n');
      break;    % stop looking, you're done!
      
%       RLTDlog = log;
%       EventsLog = context.state.LastLog;
%       fnRLTD = fnA;
      
    elseif isequal(uniqueRefID(j), RLTDlog.event(i).id)
      logIDidx(RLTDlog.event(i).id) = i;
    end

    i = i + 1;
    
  end

  %copy log events and move out measurements
  NewEvent = RLTDlog.event;
  measurement(len) = measurement_create;

  %create time delay index
  TDidx = zeros(numchan-1);
  k = 0;
  for i = 1:numchan
    for j = i+1:numchan
      k = k + 1;
      TDidx(i,j-1) = k;
    end
  end

  for i = 1:len

    measurement(i) = RLTDlog.event(i).measurement;

    if isempty(measurement(i).value.all_time)   % if no measurements
      NewEvent(i).measurement = measurement(i);     % initialize measurements as empty

    else                                            % if measurements, initialize as NaN    
      NewEvent(i).measurement.value.all_time(:) = NaN;
      NewEvent(i).measurement.value.pairwise_delta_t(:) = NaN;

      NewEvent(i).measurement.value.all_ssPressure(:) = NaN;
      NewEvent(i).measurement.value.all_rmsPressure(:) = NaN;
      NewEvent(i).measurement.value.all_rmsIntensity(:) = NaN;
      NewEvent(i).measurement.value.all_pk_xcorr_raw(:) = NaN;
      NewEvent(i).measurement.value.all_pk_xcorr_norm(:) = NaN;  

      NewEvent(i).measurement.value.all_time(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_time(RLTDlog.event(i).channel);

      NewEvent(i).measurement.value.all_ssPressure(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_ssPressure(RLTDlog.event(i).channel);

      NewEvent(i).measurement.value.all_rmsPressure(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_rmsPressure(RLTDlog.event(i).channel);

      NewEvent(i).measurement.value.all_rmsIntensity(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_rmsIntensity(RLTDlog.event(i).channel);

      NewEvent(i).measurement.value.all_pk_xcorr_raw(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_pk_xcorr_raw(RLTDlog.event(i).channel);

      NewEvent(i).measurement.value.all_pk_xcorr_norm(RLTDlog.event(i).channel) ...
         = measurement(i).value.all_pk_xcorr_norm(RLTDlog.event(i).channel);
    end

  end

%   %create time delay index
%   TDidx = zeros(numchan-1);
%   k = 0;
%   for i = 1:numchan
%     for j = i+1:numchan
%       k = k + 1;
%       TDidx(i,j-1) = k;
%     end
%   end

  %set time delays in RL-TD log to NaN if they were not labelled with
  %user-defined tag in the RL-TD_events log
%   ChPair = zeros(1,2);
  LastRefID = ' ';
  NumChans = 0;
  ChanList = [];
  for i = 1:lenA

%     %get RL-TD_event tag and fix cell bug
%     tag = EventsLog.event(i).tags;
%     if isempty(tag)
%       tag = '';
%     elseif ~iscell(tag)
%       tag = {tag};
%       tag = tag{1};
%     else
%       tag = tag{1};
%     end

    %copy arrivals times and time delays when tag is TP
%     if isequal(tag, KeepTag)

      %find channel pair indicated by current event in EventsLog
      id = logIDidx(RefID(i));
      CurrentCh = EventsLog.event(i).channel;
      IndexCh = RLTDlog.event(id).channel;
      ChPair1 = min(CurrentCh, IndexCh);
      ChPair2 = max(CurrentCh, IndexCh);

      NewEvent(id).measurement.value.all_time(EventsLog.event(i).channel) = ...
        measurement(id).value.all_time(EventsLog.event(i).channel);
      
      NewEvent(id).measurement.value.pairwise_delta_t(TDidx(ChPair1,ChPair2-1)) = ...
        measurement(id).value.pairwise_delta_t(TDidx(ChPair1,ChPair2-1));


      NewEvent(id).measurement.value.all_ssPressure(CurrentCh) = ...
        measurement(id).value.all_ssPressure(CurrentCh);
      
      NewEvent(id).measurement.value.all_rmsPressure(CurrentCh) = ...
        measurement(id).value.all_rmsPressure(CurrentCh);
      
      NewEvent(id).measurement.value.all_rmsIntensity(CurrentCh) = ...
        measurement(id).value.all_rmsIntensity(CurrentCh);
      
      NewEvent(id).measurement.value.all_pk_xcorr_raw(CurrentCh) = ...
        measurement(id).value.all_pk_xcorr_raw(CurrentCh);
      
      NewEvent(id).measurement.value.all_pk_xcorr_norm(CurrentCh) = ...
        measurement(id).value.all_pk_xcorr_norm(CurrentCh);


    % if there are previous arrivals, add time delays for all combinations
      if isequal(RefID(i), LastRefID)

        for j =  1:NumChans
          AddCh = ChanList(j);
          ChPair1 = min(CurrentCh, AddCh);
          ChPair2 = max(CurrentCh, AddCh);

          NewEvent(id).measurement.value.pairwise_delta_t(TDidx(ChPair1,ChPair2-1)) = ...
            measurement(id).value.pairwise_delta_t(TDidx(ChPair1,ChPair2-1));

        end

        NumChans = NumChans + 1;
        ChanList = [ChanList CurrentCh];

      else
        NumChans = 1;
        ChanList = CurrentCh;
      end

      LastRefID = RefID(i);
%     end

  end

  %create new log _RL-TD_culled
  fnNew = [fnRLTD '_HandCulled'];
  NewLog = new_log(fnNew, context.user, context.library, context.sound);
%   NewLog = new_log(fnNew, context.user, context.library, RLTDlog.sound);  Can't write!!!
  NewLog.event = NewEvent;
  NewLog.length = RLTDlog.length;
  NewLog.curr_id = RLTDlog.curr_id;

  % save RL-TD_culled
  log_save(NewLog);

end
