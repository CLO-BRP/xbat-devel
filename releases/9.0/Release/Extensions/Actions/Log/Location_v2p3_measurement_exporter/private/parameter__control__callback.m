function result = parameter__control__callback(callback, context)

% LOCATION_V2P3_MEASUREMENT_EXPORTER - parameter__control__callback

result = struct;


%% disable UTC Offset control if Julian Day control set to 0

julian_day = get_control(callback.pal.handle, 'julian_day', 'value');

if julian_day
  
  %enable event_duration control

  set_control(callback.pal.handle, 'utc_offset', 'enable', 1);

else
  
  %disable event_duration control

  set_control(callback.pal.handle, 'utc_offset', 'enable', 0);
  
end


%% update saved user parameter settings

preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'preset3.mat');

load(preset_path)

name = callback.control.name;

value = get(callback.control.handle,'Value');

eval(['parameter.' name ' = ' num2str(value) ';']);

save(preset_path,'parameter');
