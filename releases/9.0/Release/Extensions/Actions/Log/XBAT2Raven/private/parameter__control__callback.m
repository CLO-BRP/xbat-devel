function result = parameter__control__callback(callback, context)

% XBAT2RAVEN - parameter__control__callback
%
% History
%   msp2 - 11 Mar 2015
%       Add "Standard Output" option disables other controls.

result = struct;

%switch action depending on control being touched
switch callback.control.name
    case 'datetime_flag'
        dt_flag = get_control(callback.pal.handle, 'datetime_flag', 'value');

        if dt_flag
             %enable choice for datetime format
        %     set_control(callback.pal.handle, 'datetime_format', 'enable', 1);
            set_control(callback.pal.handle, 'datetime_format_out', 'enable', 1);   
        else
            %disable choice for datetime format
        %     set_control(callback.pal.handle, 'datetime_format', 'enable', 0);
            set_control(callback.pal.handle, 'datetime_format_out', 'enable', 0);
        end

    case 'realtime_flag'
        rt_flag = get_control(callback.pal.handle, 'realtime_flag', 'value');

        if rt_flag
             %enable choice for real time format
            set_control(callback.pal.handle, 'realtime_format_out', 'enable', 1);  
        else
            %disable choice for real time format
            set_control(callback.pal.handle, 'realtime_format_out', 'enable', 0);
        end

    case 'realdate_flag'
        rd_flag = get_control(callback.pal.handle, 'realdate_flag', 'value');

        if rd_flag
             %enable choice for real date format
            set_control(callback.pal.handle, 'realdate_format_out', 'enable', 1);  
        else
            %disable choice for real date format
            set_control(callback.pal.handle, 'realdate_format_out', 'enable', 0);
        end

    case 'standard_output'
        %--
        % Standard Output enabled freezes other controls
        %--
        standard_output = get_control(callback.pal.handle, 'standard_output', 'value');
        empty_logs_ok = get_control(callback.pal.handle, 'empty_logs_ok', 'value');
        checkbox_h = findobj(callback.pal.handle, 'Type', 'uicontrol', 'Style', 'checkbox' );
        popupmenu_h = findobj(callback.pal.handle, 'Type', 'uicontrol', 'Style', 'popupmenu' );

        if standard_output
            
            %set all checkboxes to 0
            set_control(callback.pal.handle, 'standard_output', 'value', 0);
            set( checkbox_h, 'Value', 0 )

            %disable all checkboxes
            set( checkbox_h, 'Enable', 'off' )
            set( popupmenu_h, 'Enable', 'off' )
            
            %set standard_output checkbox to 1
            set_control(callback.pal.handle, 'standard_output', 'value', 1);

            %enable standard output and empty log controls
            set_control(callback.pal.handle, 'standard_output', 'enable', 1);
            set_control(callback.pal.handle, 'empty_logs_ok', 'enable', 1);
            
            %set convert empty log control to original state
             set_control(callback.pal.handle, 'empty_logs_ok', 'value', empty_logs_ok);
             
            %set Real Date and Real Time formats
            set_control( callback.pal.handle, 'realdate_format_out', 'value', 2 );
            set_control( callback.pal.handle, 'realtime_format_out', 'value', 2 );

        else

            %enable all checkboxes
            set( checkbox_h, 'Enable', 'on' )
            
            %--
            % enable/disable date-time format controls
            %--
            dt_flag = get_control(callback.pal.handle, 'datetime_flag', 'value');
            if dt_flag
                set_control(callback.pal.handle, 'datetime_format_out', 'enable', 1);   
            else
                set_control(callback.pal.handle, 'datetime_format_out', 'enable', 0);
            end

            rt_flag = get_control(callback.pal.handle, 'realtime_flag', 'value');
            if rt_flag
                set_control(callback.pal.handle, 'realtime_format_out', 'enable', 1);  
            else
                set_control(callback.pal.handle, 'realtime_format_out', 'enable', 0);
            end

            rd_flag = get_control(callback.pal.handle, 'realdate_flag', 'value');
            if rd_flag
                set_control(callback.pal.handle, 'realdate_format_out', 'enable', 1);  
            else
                set_control(callback.pal.handle, 'realdate_format_out', 'enable', 0);
            end
        end
end


    %%
    % if strcmp(dt_flag,'Recalculate real event times and dates using sound name (must select format below)')
    %     
    %     %enable option to save newly recalculated datetime format
    %     set_control(callback.pal.handle, 'modify_logs', 'enable', 1);
    % else
    %     %disable option to save newly recalculated datetime format
    %     set_control(callback.pal.handle, 'modify_logs', 'enable', 0);
    % end