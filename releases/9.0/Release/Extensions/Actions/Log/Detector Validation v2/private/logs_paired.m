function status = logs_paired(target, NumLogs, parameter, order_flag)

status = 0;

%for each log pair
for i = 1:2:NumLogs/2
    
  if order_flag

    [sound_truth, fn_truth] = fileparts(target{i});

    [sound_test, fn_test] = fileparts(target{i+1});

  else

    [sound_truth, fn_truth] = fileparts(target{i+1});

    [sound_test, fn_test] = fileparts(target{i});

  end

  %test if log pair is in correct order
  if ~any(strfind(fn_test, parameter.test_suffix)) || ~any(strfind(fn_truth, parameter.truth_suffix))

    txt1 = sprintf('Action terminated.\nLog pair is not in correct order.\nRename logs and try again.');

    txt2 = sprintf('%s\n', target{i:i+1});

    txt =sprintf('%s\n%s', txt1, txt2);

    fprintf(2,'ERROR:\n%s\n\n', txt);

    h = warndlg(txt, 'ERROR');

    movegui(h, 'center')
    
    status = 1;

    return;

  end
  
%--- this code makes test work if date formats in sound names are
%    different, but depends on the sounds being dayfoldered
  
  %test if log pair have same date and deployment name in log name
%   datenum_truth = file_datenum(sound_truth, 'yyyymmdd');
% 
%   datenum_test = file_datenum(sound_test, 'yyyymmdd');

%   IDX_truth = strfind(sound_truth, '_');
% 
%   IDX_test = strfind(sound_test, '_');

%   deployment_truth = sound_truth(1:IDX_truth(1));
% 
%   deployment_test = sound_test(1:IDX_test(1));

%   if ~isequal(datenum_truth, datenum_test) || ~strcmp(deployment_truth, deployment_test)    

%---

  if ~strcmp(sound_truth, sound_test)
    
    %no problem if date format different, but date and deployment same

      txt1 = sprintf('Action terminated.\nLog pair is not in the same sound.\nSelect truth/test log pairs and try again.');

      txt2 = sprintf('%s\n', target{i:i+1});

      txt =sprintf('%s\n%s', txt1, txt2);

      fprintf(2,'ERROR:\n%s\n\n', txt);

      h = warndlg(txt, 'ERROR');

      movegui(h, 'center')

      status = 1;

      return;

  end

end