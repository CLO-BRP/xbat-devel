function [result, context] = compute(log, parameter, context)

% DIVIDE CHANNELS - compute

% Divides multi-channel log into multiple single-channel logs

% Michael Pitzrick
% msp2@cornelel.edu
% 19 September 2008

%% TO DO
%
% Needs log.file refreshed so output log has predicatable name
% results.

result = struct;

% initializations
numChannels = log.sound.channels;
numEvents = log.length;
LogName = log.file;
LogName = LogName(1:end-4);
Event = log.event;

% don't run if selected log has multiple channels
if numChannels == 1
    disp(['Log ''', log_name(log), ' has only 1 channel.']); 
    return;
end

% for each channel, create a single-channel log
for i = 1:numChannels
  NewEvent = [];
  k = 0;
  
  for j = 1:numEvents    
    %stuff events for channel i into Event structure
    CurrentChannel = Event(1,j).channel;
    if CurrentChannel == i
      NewEvent = [NewEvent Event(1,j)];
      k = k + 1;
    end    
  end
  
  %if events in channel, create new single-channel log
  if k>0
    iStr = num2str(i);
    NewLogName = [LogName '_Ch' iStr];
    NewLog = new_log(NewLogName,context.user,context.library,context.sound);
    NewLog.event = NewEvent;
    NewLog.length = k;
    log_save(NewLog);
  end
  
  end
end