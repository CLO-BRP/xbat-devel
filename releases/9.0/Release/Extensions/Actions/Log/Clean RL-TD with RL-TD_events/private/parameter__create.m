function parameter = parameter__create(context)

% Clean RL-TD with RL-TD_events - parameter__create

parameter = struct;

% parameter.KeepTag = '';

parameter.RLTDsuffix = 'h';
% parameter.RLTDsuffix = 'RL-TD';

parameter.RLTD_eventsSuffix = '1';
% parameter.RLTD_eventsSuffix = 'browsed';
