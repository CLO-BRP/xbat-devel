function [result, context] = compute(log, parameter, context)

% MONTAGE TOOL - compute

result = struct;

if context.state.kill
  
  return;
  
end


%% change MATLAB path to log path
log_path = fullfile(context.library.path, ...
                    sound_name(log.sound),...
                    'Logs');

cd(log_path)

%% change sound path to the one in the XBAT library

% find sound name for XBAT log
sound_name_log = sound_name(log.sound);

% find sound name in XBAT library
sound_name_lib = sound_name(context.sound);

% fail if sound names don't match
if ~strcmp(sound_name_log, sound_name_lib)
  
  fail(sprint('Log was made with different sound:\n  %s',log_name(log), ...
       'WARNING'));
     
end

% replace sound path in XBAT log with sound path in XBAT library
log.sound.path = context.sound.path;


%% open log in Montage Tool
if ~isempty(which('MontageTool.m'))
% if exist('MontageTool.m', 'file')
  
  %open MontageTool.fig
  MontageTool

  %load log into GUI
  MontageTool('open_log', log, 0)

else
  
  txt = 'Action failed.  Launch ASE Sedna and try again.';
  
  fprintf(2, '\nERROR: %s\n', txt);
  
  h = warndlg(txt, 'ERROR');
  
  movegui(h, 'center')
  
end

  
%% don't open any other selected logs
context.state.kill = 1;