function [result, context] = compute(sound, parameter, context)

% RAVEN - MIGRATE SOUND SELECTION TABLE - compute

% Changes Begin Time and End Time in Raven Sound Selection Table so that it
% can be used with a different sound stream.
%
% Input
%   Raven Sound Selection Tables
%
% Outputs
%   Raven Sound Selection Tables with updated Begin Time and End Time
%   Raven Listfile corresponding to output Raven Sound Selection Tables
%
% Note that Outputs are collated into folders named after the folder that
% the sound files are in.

% History
%   msp2  9 Apr 2015
%       Add file extension to "Begin File" values.
%       Renumber selections when merging tables.

%---
% Initializations
%---
result = struct;

if context.state.kill
    return;
end

path_out = context.state.path_out;
path_tsv = context.state.path_tsv;
fn_tsv = context.state.fn_tsv;
if ~iscell( fn_tsv )
    fn_tsv = { fn_tsv };
end

%---
% Create output directory for current sound
%---
snd_name = sound_name( sound );
path_out = fullfile( path_out, snd_name );
if exist( path_out, 'dir' )
    fail( sprintf( 'Output path already exists:\n  %s', path_out ), 'WARNING' );
    context.state.kill = 1;
    return;
end
mkdir( path_out )

%---
% Output list file for current sound
%---
sound_path = fullfile( sound.path, sound.file );
sound_path_char = char( sound_path );
path_out_full = fullfile( path_out, [ snd_name, '.listfile.txt' ] );
casewrite( sound_path_char, path_out_full );

%---
% For each selected Raven Sound Selection Table
%---
for i = 1 : length( fn_tsv )

    %---
    % Read Raven Sound Selection Table
    %---
    try
      table = caseread( fullfile( path_tsv, fn_tsv{ i } ) );  
    catch
      fail( 'The selected Raven Sound Selection Tables could not be read.', 'WARNING' )
      context.state.kill = 1;
      return;  
    end

    %---
    % Migrate Raven Sound Selection Table by updating Begin Time and End
    % Time to make compatible with new sound stream
    %---
    [ table_out, headers, selection_idx, status ] = migrate_table( table, sound );
    if status
        context.state.kill = 1;
        return;
    end
    
    %---
    % If user choses, output migrated Raven Sound Selection Table
    %---
    if ~parameter.combine_tables
        table_out = [ headers ; table_out ];
        [ ~, fn_tsv2 ] = fileparts ( fn_tsv{ i } );
        fn = strtok( fn_tsv2, '.' );
        fn_out = sprintf( '%s_(migrated_to)_%s.selections.txt', fn, snd_name );
        fn_out_full = fullfile( path_out, fn_out );
        fid = fopen( fn_out_full, 'w' );
        for j = 1:size( table_out, 1 )
            fprintf( fid, '%s\t', table_out{ j, : } );
            fprintf( fid, '\n' );
        end
        fclose( fid );
        
    %---
    % Otherwise, merge all tables for this sound
    %---
    else
        
        % Start merged table with first table
        if isequal( i, 1 )
            table_merged = [ headers ; table_out ];
            first_headers = headers;
            
        % Append table to previous tables, unless headers don't match
        else
            if all( strcmp( headers, first_headers ) )
                table_merged = [ table_merged ; table_out ];
                
            % Otherwise, stop execution of columns not same as previous Selection Table
            else
                fail( 'Headers are not the same as previous tables in %s', fn_tsv{ i } );
                context.state.kill = 1;
                return;
            end
        end
    end
end

%---
% If user chooses, output merged Raven Sound Selection Table
%---
if parameter.combine_tables
    
    %---
    % Renumber selections
    %---
    len = size( table_merged, 1 );
    if len > 1 && ~isempty( selection_idx )
        id = 1:len-1;
        id_mat = num2str( id', 17 );
        id_cell = cellstr( id_mat );
        table_merged( 2:end, 1 ) = id_cell;
    end
    
    %---
    % find name and path of output
    %---
    fn_out = sprintf( 'mergedST_(migrated_to)_%s.selections.txt', snd_name );
    fn_out_full = fullfile( path_out, fn_out );
    
    %---
    % Output selection table
    %---
    fid = fopen( fn_out_full, 'w' );
    for j = 1:size( table_merged, 1 )
        fprintf( fid, '%s\t', table_merged{ j, : } );
        fprintf( fid, '\n' );
    end
    fclose( fid );
end
