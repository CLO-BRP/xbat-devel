function [result, context] = compute(sound, parameter, context)
result = struct;

% SET DATE-TIME ATTRIBUTE - compute
%
% Sound Action sets Date-Time attribute using time stamps in sound file
% names
%
% This action consists of modified XBAT code activated with
%   XBAT palette > Attributes > Add Attribute > Date Time
% which was written by Harold Figueroa (hkf1@cornell.edu) and 
% Matthew Robbins (mer34@cornell.edu)
%
% Michael Pitzrick
% msp2@cornell.edu
% 23 Dec 08

%list AIFF's in sound file stream
file = sound.file;

%select first file in file stream
if iscell(file)
  file = file{1};
end

%calculate datenum from time stamp in name of first AIFF
num = file_datenum(file);

if isempty(num)
  
  txt = 'Unable to read time stamp in sound file names in sound:';
  
  txt2 = sprintf('%s\n  %s\n', txt, sound_name(sound));
  
  fprintf(2,'\nERROR:\n%s\n', txt2);
  
  h = warndlg(txt2, 'ERROR');
  
  movegui(h,'center')
  
  return;
  
end


% if ~isempty(num)
%   set_control(callback.pal.handle, 'datetime', 'string', datestr(num))
% end

lines{1} = 'Date and Time';
lines{2} = '(MATLAB date vector)';

%convert datenum of first AIFF into date vector
vec = datevec(num);

%convert date vector into CSV string
line = '';
for k = 1:length(vec)
	line = [line, num2str(vec(k)), ', '];
end
line(end-1:end) = '';
lines{3} = line;

% if no '__XBAT\Attributes\' directory, then create
sPath = sound.path;
if ~exist([sPath '__XBAT\Attributes'], 'dir')
  mkdir([sPath '__XBAT\Attributes']);
end

%write new date-time.csv
store = [sound.path '__XBAT\Attributes\date_time.csv'];
file_writelines(store, lines);
