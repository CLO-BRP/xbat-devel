function result = parameter__control__callback(callback, context)

% RAVEN - CREATE LISTFILE - parameter__control__callback

result = struct;
max_path_length = 255;

%% Browse to new Output Directory when "Browse" button clicked

if strcmp(callback.control.name, 'browse')
  out_dir = get_control(callback.pal.handle, 'out_dir', 'value');
  out_dir = uigetdir(out_dir);  
  if out_dir == 0    
    return;
  end
  while length( out_dir ) > max_path_length
    d = sprintf( 'Select output path with %.0f or fewer characters.', max_path_length );
    out_dir = uigetdir( out_dir, d );  
    if out_dir == 0    
      return;
    end
  end
  set_control(callback.pal.handle, 'out_dir', 'value', out_dir);  
elseif strcmp(callback.control.name, 'out_dir')  
  out_dir = get_control(callback.pal.handle, 'out_dir', 'value');
  while length( out_dir ) > max_path_length
    d = sprintf( 'Select output path with %.0f or fewer characters.', max_path_length );
    fail( d, 'WARNING' )
    return;
  end
  set_control(callback.pal.handle, 'out_dir', 'value', out_dir);  
else
  out_dir = context.ext.parameter.out_dir;
end

%% set tooltip for Output Directory (makes out_dir easier to read if long)
control = get_control(callback.pal.handle, 'out_dir');
if isempty(control)
  return;
end
handles = control.handles.all;

for k = 1:length(handles)
  style = get(handles(k), 'style');
  if isequal(style, 'edit')
    set(handles(k), 'TooltipString', out_dir);
  end
end

%% update saved user parameter settings
preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Sound', ...
                       context.ext.name, ...
                       'private', ...
                       'preset.mat');

load(preset_path)
parameter.out_dir = out_dir;
name = callback.control.name;
style = get(callback.control.handle, 'style');
if strcmp(style, 'edit')
  value = get(callback.control.handle,'String');	
	if strcmp( name, 'pvalue' )
		value = str2double( value );
  end
  eval(['parameter.' name ' = value;']);	
elseif strcmp(style, 'slider')
  value = get(callback.control.handle,'Value');
  eval(['parameter.' name ' = value;']);  
elseif strcmp(style, 'popupmenu')
  value = get(callback.control.handle,'Value');
  eval(['parameter.' name '_idx = value;']);  
end
save( preset_path, 'parameter' );