function hr = file_hour( fn_ext )
% Calculate which hour of a recording each sound file is
%
%   Input
%     fn_ext = cell vector of sound file names with date embedded in name as
%            '<fn_root>_yyyymmdd_HHMMSS.<ext>'
%
%   Output
%     hr = whole hour of recording starting with 1

%---
% Error checking
%---
assert( iscellstr( fn_ext ), 'WARNING\nInput must be cellstring' );

%---
% Find hour of beginning of each file
%---

% find MATLAB datenum for each file
file_date_cell = cellfun( @file_datenum, fn_ext, 'UniformOutput', 0 );
file_date = cell2mat( file_date_cell );

% find whole hours starting at 1
tolerance = eps( file_date( 1 ) );  %tolerance prevents rounding errors
hr = floor( 24 .* ( file_date - file_date( 1 ) + tolerance ) ) + 1;




% % strip file extension from name
% [ ~, fn ] = cellfun( @fileparts, fn_ext, 'UniformOutput', 0 );
% 
% % find hour in file names
% hr_cell = cellfun( @(x) x(end-5:end-4), fn, 'UniformOutput', 0 );
% hr = str2double( hr_cell );
% 
% %---
% % Add 24 hours for each day from beginning to yield unique hour index
% %---
% day0 = file_datenum( fn_ext{ 1 } );
% day_cell = cellfun( @(x) x(end-14:end-7), fn, 'UniformOutput', 0 );
% day = datenum( day_cell