function control = parameter__control__create(parameter, context)

% Set Time Stamps Attribute - parameter__control__create

control = empty(control_create);

% this is the default DATE pattern used to parse the time stamps
control(end + 1) = control_create( ...
	'name',  'DatePattern', ...
	'alias', 'Date-Time pattern', ...
	'style', 'edit', ...
  'string', 'yyyymmdd_HHMMSS' ...
);