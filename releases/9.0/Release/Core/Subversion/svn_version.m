function [version, exported, info] = svn_version(file)

% svn_version - get svn version string
% ------------------------------------
%
% [version, exported, info] = svn_version(file)
%
% Input:
% ------
%  file - file or directory
%
% Output:
% -------
%  version - string
%  exported - exported test, directory is not a working copy
%  info - svn info

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 23 Dec 2014
%       Enhance handling of error conditions.
%   msp2 - 11 Mar 2015
%       Handle case of directory not a working copy in most recent SVN.

%--
% set default file
%--
if nargin < 1
	file = pwd;
end 

%--
% get exported state and version string from info
%--
info = get_svn_info( file );
if isempty( info );
    exported = 1;
    version = 'SVN error';
elseif ischar( info ) && ...
      ( strfind( info, 'Subversion command line client not found' ) || ...
        strfind( info, 'subversion_command_line_client_not_found' ) )
    exported = 1;
    version = 'SVN not found';
elseif isfield( info, 'svn' ) && ~isempty( strfind( info.svn, 'E200009' ) )
    exported = 1;
    version = 'Not a working copy';
elseif isfield( info, 'svn' ) && ~isempty( strfind( info.svn, 'E155021' ) )
    exported = 1;
    version = '*Please update SubVersion client*';
    fail( version, 'WARNING' );
else
    exported = 0;
    version = int2str( info.revision );
end
