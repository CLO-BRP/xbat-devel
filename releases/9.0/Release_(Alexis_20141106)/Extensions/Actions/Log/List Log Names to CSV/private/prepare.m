function [result, context] = prepare(parameter, context)
result = struct;

% LIST LOG NAMES TO CSV - prepare

% get list of names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
line = cell(NumLogs,1);

for i = 1:NumLogs

  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  
  %check if a single, valid log
  if ischar(SoundName) && ischar(fn)
    
    % add extension to log name
    line(i) = {[fn '.mat']};

  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end

%find path to write text file to
WritePath = context.library.path;
WritePath = [WritePath SoundName '\Logs\LogNames_' datestr(now,30) '.csv'];

%write text file
file_writelines(WritePath, line);