function [result, context] = prepare(parameter, context)
result = struct;

%% FILTER EVENTS BY TIME v1-3 - prepare

% Log action creates new log with only events in user-specified time range
%
% New logs has suffix "_<hh>h-<hh>h"
%
% *** NOTE: Events only partly in the selected time range are included!!! ***

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. 

% Change Log
%
% 1. If there are no events in time range, gives warning and generates no
% log instead of making defective log and generating MATLAB errors
%
% 2. Gives the user the option to choose between 2 suffixes:
%    a. "_Keep_<h>h-<h>h"
%    b. "_to_be_locs_<h>-<h>" 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

%% 

%initializations
context.state.kill = 0;

%terminate script if sound not selected
context.state.kill = 0;
if ~isfield(context.sound, 'type')
  fprintf(2,'\n\n%s\n\n', 'No sound selected. Select sound in XBAT palette and try again.');
  context.state.kill = 1;
  return;
end

%terminate script if end of selected interval is at or before the beginning
if parameter.BeginTime >= parameter.EndTime
  fprintf(2,'\n\nBeginning of selected interval is at or after end!\n');
  context.state.kill = 1;
  return;
end

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end

% write header for MATLAB Command Window output
disp('*** THIS LOG ACTIONS ASSUMES DATE-TIME ATTRIBUTE WAS SET CORRECTLY ***')
disp('*** THE LAST TIME EACH LOG WAS OPEN.  IF YOU ARE NOT SURE, PLEASE  ***')
disp('*** RUN CHECK DATE-TIME ATTRIBUTE LOG ACTION FIRST!!!              ***')
disp(' ')
disp('Filter Original  New     New')
disp('Range  Length    Length  Log')