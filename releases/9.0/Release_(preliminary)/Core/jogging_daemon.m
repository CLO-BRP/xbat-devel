function daemon = jogging_daemon

% jogging_daemon - create a timer object to handle jogging display
% ----------------------------------------------------------------
%
% daemon = jogging_daemon
%
% Output:
% -------
%  daemon - timer object

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------
%
% History
%	msp2  1 November 2013
%		1. Advance timer by overlap rather than by slider trough.
%		2. Count delay from *after* specgram is rendered 
%		3. When looping, go back to beginning of sound.

%--
% create timer object
%--

daemon = timer;

%--
% configure timer object
%--

set(daemon, ...
	'name','XBAT Jogging Daemon', ...
	'timerfcn',@jog_display, ...
	'executionmode','fixedRate', ...
	'period',1.0 ...
);


%---------------------------------------------------
% JOG_DISPLAY
%---------------------------------------------------

function jog_display(obj,eventdata)

% jog_display - timer callback to update jogging display
% -------------------------------------------------------
%
% jog_display(obj,eventdata)
%
% Input:
% ------
%  obj - callback object
%  eventdata - currently not used

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%----------------------------------------------------
% UPDATE CURRENT BROWSER
%----------------------------------------------------

% NOTE: this could be slightly more efficient, get it working first

persistent CURR_FIG CURR_BROWSER FIG_CHANGE;
% persistent CURR_FIG CURR_BROWSER FIG_CHANGE ELAPSED;

%--
% set current figure
%--

if (isempty(CURR_FIG))
	CURR_FIG = gcf;
	FIG_CHANGE = 1;
end

%--
% update current figure
%--

if (CURR_FIG ~= gcf)
	CURR_FIG = gcf;
	FIG_CHANGE = 1;
else
	FIG_CHANGE = 0;
end

%--
% update current sound browser
%--

if (FIG_CHANGE && strncmp(get(CURR_FIG,'tag'),'XBAT_SOUND_BROWSER',18))
	CURR_BROWSER = CURR_FIG;
end

%----------------------------------------------------
% GET JOG STATE PARAMETERS
%----------------------------------------------------

%--
% get jog palette handle
%--

% NOTE: assume only parent palettes are visible to make selection faster

pal = findobj( ...
	findobj(get(0,'children'),'flat','visible','on'), ...
	'flat','name','Autoadvance' ...
);

% NOTE: jogging does not happen when palette is hidden

if (isempty(pal))
%         ELAPSED = 0;
	return;
end 

%--
% get jog speed from ui
%--

[ignore,speed] = control_update([],pal,'jog_speed');

% NOTE: the state value is obtained from the button label string

%--
% get jog state
%--

toggle = control_update([],pal,'jog_toggle');

state = get(toggle,'string');
	
% NOTE: when the button displays 'Go' the state is 'Stop' and viceversa

if (strcmp(state,'Go'))
%         ELAPSED = 0;
	return;
end

%----------------------------------------------------
% JOG
%----------------------------------------------------

% If we are jogging slowly, wait for more interupts

% % ELAPSED = ELAPSED + 1;
% if ELAPSED < speed
if toc < speed
   return;
end

% Otherwise, reset count then fall through to advance
% ELAPSED = 0;

%--
% get parent slider
%--

par = getfield(get(pal,'userdata'),'parent');

slider = findobj(par,'tag','BROWSER_TIME_SLIDER');

% NOTE: it is not clear when this would happen

if (isempty(slider))
	return;
end

%--
% compute time update and state update flag
%--

% update by multiple of real time
% time = get(slider,'value') + (get(obj,'period') * speed);

% update by page (slider step size)
timeStep = get(slider,'SliderStep');
min_time = get(slider,'min');
max_time = get(slider,'max');
time = get(slider,'value') + ((max_time-min_time) * timeStep(1));

flag = 0;

% NOTE: this condition only happens when going forward in time

if (time > max_time)
	
	% NOTE: we only need the loop state during edge conditions
	
	[~,loop] = control_update([],pal,'jog_loop');

	if (loop)
		time = min_time;
% 		time = mod(time,max_time);
	else
		time = max_time;
		flag = 1;
	end

end

% NOTE: this condition only happens when going backwards in time

if (time < 0)
	
	% NOTE: we only need the loop state during edge conditions
	
	[~,loop] = control_update([],pal,'jog_loop');
	
	if (loop)
		time = max_time;
% 		time = mod(time,max_time);
	else
		time = 0;
		flag = 1;
	end
	
end

% NOTE: the resulting behavior is not intuitive

% %--
% % turn off jog on direct slider update
% %--
% 
% if (gco == slider)
% 	set(toggle,'string','Go');
% end 

%--
% update slider position
%--
	
set(slider,'value',time);
if (CURR_FIG ~= CURR_BROWSER)
	set(0,'currentfigure',CURR_BROWSER);
end

%--
% update jog state when time goes off sound edges
%--

if (flag)
	set(toggle,'string','Go');
end 

