function cdata = ptr_add

cdata = [ 
       2   2 NaN NaN NaN NaN NaN NaN NaN NaN   2 NaN NaN NaN NaN NaN
       2   1   2 NaN NaN NaN NaN NaN NaN   2   1   2 NaN NaN NaN NaN
       2   1   1   2 NaN NaN NaN NaN   2   2   1   2   2 NaN NaN NaN
       2   1   1   1   2 NaN NaN   2   1   1   1   1   1   2 NaN NaN
       2   1   1   1   1   2 NaN NaN   2   2   1   2   2 NaN NaN NaN
       2   1   1   1   1   1   2 NaN NaN   2   1   2 NaN NaN NaN NaN
       2   1   1   1   1   1   1   2 NaN NaN   2 NaN NaN NaN NaN NaN
       2   1   1   1   1   1   1   1   2 NaN NaN NaN NaN NaN NaN NaN
       2   1   1   1   1   1   1   1   1   2 NaN NaN NaN NaN NaN NaN
       2   1   1   1   1   1   2   2   2   2   2 NaN NaN NaN NaN NaN
       2   1   1   2   1   1   2 NaN NaN NaN NaN NaN NaN NaN NaN NaN
       2   1   2 NaN   2   1   1   2 NaN NaN NaN NaN NaN NaN NaN NaN
       2   2 NaN NaN   2   1   1   2 NaN NaN NaN NaN NaN NaN NaN NaN
       2 NaN NaN NaN NaN   2   1   1   2 NaN NaN NaN NaN NaN NaN NaN
     NaN NaN NaN NaN NaN   2   1   1   2 NaN NaN NaN NaN NaN NaN NaN
     NaN NaN NaN NaN NaN NaN   2   2   2 NaN NaN NaN NaN NaN NaN NaN
];
