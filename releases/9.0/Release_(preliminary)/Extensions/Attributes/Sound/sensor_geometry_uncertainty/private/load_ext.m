function attribute = load_ext(store, context)

% SENSOR_GEOMETRY - load

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2  14 May 2014
%       Change name of function to avoid name conflict with MATLAB built-in function.

attribute = sensor_geometry_uncertainty(store);

%--
% add z-dimension if needed
%--

dim = size( attribute.uncertainty, 2 );

if dim == 2
  
  num_chan = size( attribute.uncertainty, 1 );
  
  uncertainty = zeros( num_chan, 3, dim );
  
  for ch = 1:num_chan
  
    uncertainty( ch, 1:3, 1 ) = [ attribute.uncertainty( ch, 1:2, 1 ), 0 ];
  
    uncertainty( ch, 1:3, 2 ) = [ attribute.uncertainty( ch, 1:2, 2 ), 0 ];
  
  end

  attribute.uncertainty = uncertainty;
  
end
