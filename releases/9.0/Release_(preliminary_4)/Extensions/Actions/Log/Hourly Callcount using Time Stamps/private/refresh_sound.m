function  [snd, context] = refresh_sound(snd, fn, context)


snd_str = snd.path;

[snd_path snd_name] = fileparts(snd_str);

if isempty(snd_name)    
  [snd_path snd_name] = fileparts(snd_path);
end

%if sound does not exist, see if sound root path same as in last log selected
if ~isdir(snd_str)

  snd_str = [context.state.snd_lib_root, '\', snd_name];

end

while ~isdir(snd_str)

  %ask user for sound path
  new_snd_str = uigetdir(pwd, ...
      sprintf('Log has invalid sound path %s\nFind sound for %s', ...
      snd_str, fn));

  %if user cancelled
  if isequal(new_snd_str, 0)
    context.state.kill = 1;
    return;
  end

  %check to see if sound name agrees with that in log
  [new_snd_path new_snd_name] = fileparts(new_snd_str);

  if isempty(new_snd_name)    
    [new_snd_path new_snd_name] = fileparts(new_snd_path);
  end

  if ~strcmp(new_snd_name, snd_name)
    fprintf(2,'\nName of selected directory does not match sound name:\n');
    fprintf(2,'  Original sound name: %s\n', snd_name);
    fprintf(2,'  Selected sound name: %s\n', new_snd_name);
    context.state.kill = 1;
    return;
  end

  snd_str = new_snd_str;

  context.state.snd_lib_root = new_snd_path;

end

%check to see if sound file stream agrees with that in log
new_sound = sound_create('file stream', snd_str);

try
  if ~all(strcmp(new_sound.file, snd.file))
    fprintf(2,'\nSound files do not match\n');
    fprintf(2,'  Log last open in:    %s\n', snd_str);
    fprintf(2,'  Sound just selected: %s\n', new_snd_str);
    context.state.kill = 1;
    return;
  end
catch
    fprintf(2,'\nSound files do not match\n');
    fprintf(2,'  Log last open in:    %s\n', snd_str);
    fprintf(2,'  Sound just selected: %s\n', new_snd_str);
    context.state.kill = 1;
    return;
end

if ~strcmp(snd_str(end), '\')
  snd_str = [snd_str '\'];
end

snd.path = snd_str;

