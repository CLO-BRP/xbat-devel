function [result, context] = prepare(parameter, context)

% SET PARAMETERS - prepare

%initializations
result = struct;
context.state.kill = 0;

%find pixel density on monitor
units_orig = get( 0, 'Units' );
set( 0, 'Units', 'inches' )
ScreenSize_inches = get( 0, 'ScreenSize' );
set( 0, 'Units', 'pixels' )
ScreenSize_pixels = get( 0, 'ScreenSize' );
pitch_x = ScreenSize_pixels( 3 ) / ScreenSize_inches( 3 );
pitch_y = ScreenSize_pixels( 4 ) / ScreenSize_inches( 4 );
set( 0, 'Units', units_orig );

%save user-selected browser position in context.state
context.state.position = [ pitch_x * parameter.p1, ...
                           pitch_y * parameter.p2, ...
                           pitch_x * ( parameter.p3 - parameter.p1 ), ...
                           pitch_y * ( parameter.p4 - parameter.p2 ) ...
                         ];
