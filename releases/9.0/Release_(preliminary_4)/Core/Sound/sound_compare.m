function [flag,flags] = sound_compare(f,g)

% sound_compare - compare sound structures
% ----------------------------------------
%
% [flag,flags] = sound_compare(f,g,mode)
%
% Input:
% ------
%  f,g - sound structures to compare
%
% Output:
% -------
%  flag - comparison flag
%  flags - detailed comparison

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5824 $
% $Date: 2006-07-21 15:52:41 -0400 (Fri, 21 Jul 2006) $
%--------------------------------
%
% History
%   msp2  28 October 2013
%       Strengthen test determining whether log is being opened in correct
%       sound.

% NOTE: this function is very primitive, it is really a simple sanity check

%-------------------------------------------------
% COMPARE RELEVANT FIELDS
%-------------------------------------------------

%--
% check type
%--
flags = ones( 1, 7 );

if ( strcmp( f.type, g.type ) == 0 )
	flags( 1 ) = 0;
end

%--
% number of files
%--
if ( length( f.file ) ~= length( g.file ) )
	flags( 2 ) = 0;
end

%--
% file names
%--
if ( flags( 2 ) == 0 ) || ~isequal( f.file, g.file )
	flags( 3 ) = 0;
end

%--
% sound duration
%--
if ~isequal( (f.cumulative( end ) / f.samplerate ), ( g.cumulative( end ) / g.samplerate ) )
	flags( 4 ) = 0;
end

%--
% date-time attribute
%--
realtime_log = file_datenum(  f.file{ 1 }  );
realtime_sound = file_datenum(  g.file{ 1 }  );
if ~isequal( realtime_log, realtime_sound )
	flags( 5 ) = 0;
end

%--
% time-stamps attribute
%--
datetime_log = get_schedule_from_files(  f, 'yyyymmdd_HHMMSS'  );
datetime_sound = get_schedule_from_files(  g, 'yyyymmdd_HHMMSS'  );

if ~(  isequal(  size(  datetime_log  ), size(  datetime_sound  )  ) && isequal(  datetime_log, datetime_sound  )  )
	flags( 6 ) = 0;
end

%--
% channels
%--
if ~isequal( f.channels, g.channels )
	flags( 7 ) = 0;
end

flag = all( flags );
