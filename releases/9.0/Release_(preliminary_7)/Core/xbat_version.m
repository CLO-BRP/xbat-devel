function str = xbat_version(name)

% xbat_version - output XBAT version string
% -----------------------------------------
%
% str = xbat_version(name)
%
% Input:
% ------
%  name - version name
%  info - subversion info
%
% Output:
% -------
%  str - version string

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6971 $
% $Date: 2006-10-09 15:34:23 -0400 (Mon, 09 Oct 2006) $
%--------------------------------

% History
%   msp2 - 29 Aug 2014 - Increment version from "R9" to "R9 RC18".
%   msp2 -  8 Sep 2014 - Increment version to "R9 RC19".
%   msp2 -  6 Oct 2014 - Increment version to "R9".
%   msp2 - 10 Oct 2014 - Increment version to "R9 DEV".
%   msp2 - 10 Oct 2014 - Increment version to "R9a DEV".
%   msp2 - 23 Dec 2014 - Rationalize and automate version string construction.

%-----------------------
% HANDLE INPUT
%-----------------------

[ copy, ~, version ] = is_working_copy(xbat_root);

%--
% set default name
% NOTE: if we are not a working copy, surely we are using the release
% TODO: consider subversion details, to handle a working copy of the release branch!

%-----------------------
% VERSION STRING
%-----------------------

%--
% get matlab version info
%--

rel = ver('matlab'); 
rel = rel.Release;

%--
% build version string
% NOTE: this is used in figure titles and structure version fields
%--

% if XBAT is not a working copy
if ~copy
    str = sprintf( 'Unknown version (%s), MATLAB %s', version, rel );

% if XBAT is a working copy
else
    %--
    % get svn info
    %--
    info = get_svn_info(xbat_root);
    if isempty(info) % || ~isfield(info, 'rev')
        return;
    end

    %--
    % create string with svn info 
    %--
    url = info.url;
    [ url2, folder ]  = fileparts( url );
    
    % if this is a working copy of a release
    if strcmp( folder, 'Release' )
        [ ~, vers_base, vers_sub ] = fileparts( url2 );
        version = [ vers_base, vers_sub ];
        
    % if this is anything else
    else
        version = folder;        
    end
    str = sprintf( '%s (REV %.0f), MATLAB %s', version, info.last_changed_rev, rel );
end
