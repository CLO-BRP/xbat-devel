function log = raven_save( varargin )
%Create Raven selection table in XBAT
%
%This function has two modes:
%
%  1. "set_up" mode outputs a stripped-down XBAT log variable with no events,
%     which can be used to call this function in "create" mode.
%
%     Input
%
%       outpath - path that selection table will be written to in "create" mode
%
%       fn - name of selection table will be written to in "create" mode
%
%       author - string with author name
%
%       sound - XBAT sound struct
%
%     Output
%
%       log - stripped down XBAT that can be used by this function in "create_mode".
%
%
%
%  2. "create mode" writes a Raven selection table using an XBAT log variable.  
%
%     Input
%
%       log - An XBAT log struct
%
%     Note: "log" can be a log struct created by any XBAT function.
%     If the XBAT log struct was created by raven_save in "set_up" mode, 
%     a struct vector of XBAT events must be added to log.event if the 
%     output Raven selection table is to have selections in it, log.length
%     must be set to number of elements in event vector, and log.curr_id 
%     must be set to to one higher than the maximum log.event().id.

%--------------------------------------------------------------------------
%determine whether raven_save is being called in "set_up" mode or "create" mode
%--------------------------------------------------------------------------
if nargin == 1
    log = varargin{ 1 };
    create( log );
elseif nargin == 4
    log = [];
    outpath = varargin{ 1 };
    fn      = varargin{ 2 };
    author  = varargin{ 3 };
    sound   = varargin{ 4 };
    log = set_up( outpath, fn, author, sound );
else
    error( 'Incorrect number of input parameters in call to "raven_save"' );
end


function log = set_up( outpath, fn, author, sound )
  
    %make stripped-down XBAT log with minimal information for making Raven selection table
    log.type = 'selection table';
    log.path = outpath;
    log.file = fn;
    log.author = author;
    log.sound = sound;
    log.event = empty( event_create );
	log.length = 0;                          
    log.curr_id = 1;
    log.userdata = [];
% 	log.userdata.detect.max_num_events = []; %disable maximum log size feature


function create( log )
    %--------------------------------------------------------------------------
    %set sound attributes for real time calculations
    %--------------------------------------------------------------------------

    %deal with 'file' type sounds
    if iscell( log.sound.file )
        file = log.sound.file{ 1 };
    else
        file = log.sound.file;
    end

    %make sound struct with added date-time and time-stamps attributes
    log.sound.realtime = file_datenum( file );
    time_stamps.table = get_schedule_from_files( log.sound, 'yyyymmdd_HHMMSS' );
    [ ~, ixs ] = sort( time_stamps.table( :, 1 ) );
    time_stamps.table = time_stamps.table( ixs, : );
    time_stamps.enable = 1;
    time_stamps.collapse = 1;
    log.sound.time_stamp = time_stamps;

    %make sound struct with time attributes unset (useful for finding file offsets)
    fixed_sound = log.sound;
    fixed_sound.time_stamp = [];
    fixed_sound.attributes = [];

    %--------------------------------------------------------------------------
    %open selection table
    %--------------------------------------------------------------------------
    path_full = fullfile( log.path, [ log.file '.selections.txt' ] );
    fid = fopen( path_full, 'w' );

    %--------------------------------------------------------------------------
    %write selection table header
    %--------------------------------------------------------------------------
    field_names = { 'Selection', ...
                    'View', ...
                    'Channel', ...
                    'Begin Time (s)', ...
                    'End Time (s)', ...
                    'Low Freq (Hz)', ...
                    'High Freq (Hz)', ...
                    'Begin Path', ...
                    'File Offset (s)', ...
                    'End Path', ...
                    'End File Offset', ...
                    'Real Start Datetime (yyyymmdd_HHMMSS)', ...
                    'Real Stop Datetime (yyyymmdd_HHMMSS)', ...
                    'XBAT Score', ...
                    'XBAT Tags' ...
                  };
    fprintf( fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n', field_names{ : } );

    %--------------------------------------------------------------------------
    %write selection table body
    %--------------------------------------------------------------------------
    for i = 1:log.length
        curr_event = log.event( i );

        %calculate MATLAB datenum for event start and event stop
        real_start =  log.sound.realtime + map_time( log.sound, 'real', 'record', curr_event.time(1)) ./ 86400;
        real_stop  =  log.sound.realtime + map_time( log.sound, 'real', 'record', curr_event.time(2)) ./ 86400;

        %error if sound files do not have readable time stamp
        if iscell( log.sound.file )
          file1 = log.sound.file{1};
        else
          file1 = log.sound.file;
        end
        file1 = fullfile( log.sound.path, file1 );
        assert( ~isempty( real_start ) && ~isempty( real_stop ) , ...
          'WARNING: Time stamp in sound file name is unreadible:\n  %s' , file1 );

        %calculate date string for event start and stop time
        real_start_datetime = datestr( real_start, 'yyyymmdd_HHMMSS' );
        real_stop_datetime  = datestr( real_stop,  'yyyymmdd_HHMMSS' );   

        %find sound file name and offset for event start time
        offset_start_file = get_current_file( fixed_sound, curr_event.time( 1 ) );
        start_offset =  curr_event.time( 1 ) - get_file_times( fixed_sound, offset_start_file );

        %find sound file name and offset for event stop time
        currfile = find( strcmp( log.sound.file, offset_start_file ) );
        sound_lens = log.sound.cumulative / log.sound.samplerate;
        curr_sound_stop = sound_lens( currfile );     

        %if event spans multiple sound files
        if curr_event.time( 2 ) > curr_sound_stop

           %if not last sound file
            if currfile < length( log.sound.file )            
                stopfile = find( ( curr_event.time(2) < sound_lens ), 1, 'first' );
                offset_stop_file = log.sound.file{ stopfile };
                stop_offset =  curr_event.time( 2 ) - sound_lens( stopfile - 1 );

            %if last sound file, event must fall within file bounds
            else
                offset_stop_file = log.sound.file{ currfile };
                stop_offset =  log.sound.samples( currfile ) / log.sound.samplerate;
            end

        %event is contained within one file
        else
            offset_stop_file = offset_start_file;

            if isempty(curr_event.duration)
                curr_event.duration = curr_event.time( 2 ) - curr_event.time( 1 );
            end

            stop_offset = start_offset + curr_event.duration;
        end

        %deal with multiple event tags
        tags = curr_event.tags{ 1 };
        for j = 2:length( curr_event.tags )
            tags = [tags ' | ' curr_event.tags{ j }];
        end
        
        %make path of sound files
        begin_file = fullfile( log.sound.path, offset_start_file );
        end_file   = fullfile( log.sound.path, offset_stop_file );

        %output line for current event to selection table
        line =  { i, ...
                  'Spectrogram 1', ...
                  curr_event.channel, ...
                  curr_event.time( 1 ), ...
                  curr_event.time( 2 ), ...
                  curr_event.freq( 1 ), ...
                  curr_event.freq( 2 ), ...
                  begin_file, ...
                  start_offset, ...
                  end_file, ...
                  stop_offset, ...
                  real_start_datetime, ...
                  real_stop_datetime, ...
                  curr_event.score, ...     
                  tags ...              
                };
        fprintf( fid, '%.0f\t%s\t%.0f\t%.17f\t%.17f\t%.17f\t%.17f\t%s\t%.17f\t%s\t%.17f\t%s\t%s\t%.17f\t%s\n', line{ : } );
    end

    %--------------------------------------------------------------------------
    %close selection table
    %--------------------------------------------------------------------------
    fclose( fid );
