function [result, context] = compute(sound, ~, context)

% Import Logs - compute


result = struct;

%don't run, if user picked 'cancel' on previous sound
if context.state.kill
  return;  
end

%initializations
SoundName = sound_name(sound);
LenSoundName = length(SoundName);
DestPath = fullfile(context.library.path, SoundName, 'Logs');
copy_flag = 0;

for i = 1:context.state.NumLogs
  
  fn = context.state.LogList{i};

  %if log name begins with sound name        
  if strcmp(SoundName, fn(1:LenSoundName))

    % log has passed checks, copy to local library
    SourceLog = fullfile(context.state.RootPath, fn);

    if context.state.flag || ~exist(fullfile(DestPath, fn), 'file')

      %copy log
      [copy_status, message] = copyfile(SourceLog, DestPath);

      if ~copy_status
        fail(message, 'ERROR')
        context.state.kill = 1;
        return;
      end
      copy_flag = 1;
      
    else

      %% Four Button questdlg substitute
      QuestFig=dialog(                                    ...
        'Visible'         ,'off'                      , ...
        'Name'            ,'Copy'                     , ...
        'Pointer'         ,'arrow'                    , ...
        'Units'           ,'inches'                   , ...
        'Position'        ,[4, 4, 3.5, 1.5]           , ...
        'IntegerHandle'   ,'off'                      , ...
        'WindowStyle'     ,'normal'                   , ...
        'HandleVisibility','callback'                 , ...
        'Tag'             ,'copy_warning'             , ...
        'Name'           ,'WARNING'                    ...
        );        

      str = {'Replace', ...
             'Replace All', ...
             'Skip', ...
             'Cancel'};              

      pos = [-0.75, 0.25, 0.75, 0.25];

      CBString='uiresume(gcbf)';

      for j = 1:4

        pos(1) = pos(1) + 0.85;

        bh(j) = uicontrol(...
          QuestFig, ...
          'style'              , 'pushbutton', ...
          'units'              , 'inches', ...
          'position'           , pos, ...
          'CallBack'           , CBString    , ...
          'string'             , str{j}, ...
          'HorizontalAlignment', 'center');

      end
% 
      txt = sprintf('%s\nalready exists', fn);
%             txt = sprintf('%s\nalready exists', LogList{i});
      set(QuestFig,'visible','on')

      uicontrol(QuestFig            , ...
        'Style'              ,'text'         , ...
        'Units'              ,'normalized'   , ...
        'Position'           ,[0.05 0.5 0.9 0.4]      , ...
        'String'             ,txt            , ...
        'Tag'                ,'Question'     , ...
        'HorizontalAlignment','left'           ...
      );

      movegui(QuestFig, 'center')
      set(QuestFig ,'WindowStyle','modal','Visible','on');
      drawnow;
      if ishghandle(QuestFig)
        uiwait(QuestFig);
      end

      % Check handle validity again since we may be out of uiwait because the
      % figure was deleted.
      if ishghandle(QuestFig)
        button = get(get(QuestFig,'CurrentObject'),'String');
        delete(QuestFig);
      else
        button = 'Cancel';
      end
      
      if strcmp(button, 'Replace') || strcmp(button, 'Replace All')
        [copy_status, message] = copyfile(SourceLog, DestPath);
        if ~copy_status
          fail(message, 'ERROR')
          context.state.kill = 1;
          return;
        end
        copy_flag = 1;
      end

      if strcmp(button, 'Replace All')
        context.state.flag = 1;
      elseif strcmp(button, 'Skip')
        return;
      elseif strcmp(button, 'Cancel')
        context.state.kill = 1;
        return;
      end
    end
  end
end

if ~copy_flag
  fprintf( 'No XBAT log was selected for sound named:\n   %s', SoundName );
%   txt = sprintf('No XBAT log was selected for sound named:\n   %s', SoundName);
%   fail(txt,'WARNING')
end
