function [result, context] = compute(log, parameter, context)
% LIST TAGS - compute
result = struct;
%%
% Add Tag log action
%
% Replaces tag for every event in selected log with user-specified tag
%
% Michael Pitzrick
% Cornell Lab of Ornithology
% msp2@cornell.edu

%%
if context.state.kill || log.length == 0
  return;
end

% Abort if log open
if log_is_open(log)
  disp(' ')
  disp(' ')
  disp(['Please close log ', log_name(log)]); 
  disp(' ')
  return;
end

if isempty( parameter.tag )
	tag = '';
elseif iscell( parameter.tag )
	tag = parameter.tag{ 1 };
elseif ischar( parameter.tag )
	tag = parameter.tag;
else
	fprintf( 2, 'Tag entered in GUI is not a string: %s\n', parameter.tag );
	context.state.kill = 1;
	return;
end

% Find and display name of current log in Command Window
fn = log_name( log );
fprintf('\n\n''%s'' tag added to %s\n', tag, fn);

% Add tag to each event in log
for i=1:log.length
  log.event(i) = set_tags( log.event(i), tag );
end

% Rename log and report to console
fn = log_name( log );
log.file = [fn '_' tag];
fprintf('\n\n''%s'' tag added to %s\n', tag, fn);

log_save( log );