function [status] = log_is_open(logs, NumLogs)

status = 0;


%% make list of open logs (very quick, because logs already loaded!!!)
open_logs = cell(0,0);

h = get_xbat_figs('type','sound');

for k = 1:length(h)
  
  data = get(h(k),'userdata');
  
  curr_logs = data.browser.log;
  
  if ~isempty(curr_logs)
  
    open_logs = [open_logs, log_name(curr_logs)];
    
  end
  
end

%% if any selected logs are open, warn user and set status

for i = 1:NumLogs

  CurrentLog = logs{i};

  %find source path  
  [SoundName, fn] = fileparts(CurrentLog);
  
  %skip log if log is open    
  if any(strcmp(fn, open_logs))
    
    status = 1;
    
    txt = sprintf('Close log before copying:\n   %s', fn);
    
    fprintf('\nWARNING: %s\n', txt);
    
    wh = warndlg(txt, 'WARNING');
    
    movegui(wh, 'center')
    
  end
end

if status
    
  txt = sprintf('Action terminated.  No logs copied.');

  fprintf('\nWARNING: %s\n', txt);

  wh = warndlg(txt, 'WARNING');

  movegui(wh, 'center')
  
end