function [result, context] = compute(log, parameter, context)

% FILTER EVENTS BY TIME v1-3 - compute

% Log action creates new log with only events in user-specified time range
%
% New logs has suffix "_<hh>h-<hh>h"
%
% *** NOTE: Events only partly in the selected time range are included!!! ***

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. 

% Change Log
%
% 1. If there are no events in time range, gives warning and generates no
% log instead of making defective log and generating MATLAB errors
%
% 2. Gives the user the option to choose between 2 suffixes:
%    a. "_Keep_<h>h-<h>h"
%    b. "_to_be_locs_<h>-<h>" 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

result = struct;

if context.state.kill; return; end

%initializations
fn = log.file;
fn = fn(1:end-4);

%realtime is the fractional day of date-time attribute converted to seconds
realtime = log.sound.realtime;
if isempty(realtime)
  realtime = 0;
  fprintf(2,'WARNING: Date-time attribute is not set for %s\n', fn);
else
  realtime = mod(realtime,1);
  realtime = 86400 * realtime;
end 

BeginTime = parameter.BeginTime;
EndTime = parameter.EndTime;
BeginTimeSec = 3600 * BeginTime - realtime;
EndTimeSec = 3600 * EndTime - realtime;

len = log.length;
NewEvent = event_create;
NewEvent(1,len) = NewEvent;
NewLength = 0;

%for each event
for i = 1:len
  
  CurrentEvent = log.event(i);
  
  % include event only if one of time bounds is in user-defined interval
  if CurrentEvent.time(2) > BeginTimeSec ...
  && CurrentEvent.time(1) < EndTimeSec
    
    NewLength = NewLength + 1;
    NewEvent(NewLength) = CurrentEvent;
    
  end
end

%% if there are events in time range, save them in a new log
if NewLength
  
  % make new log name
  if strcmp(parameter.suffix, '_Keep_<h>h-<h>h')
    NEWfn = [fn '_Keep_' num2str(BeginTime) 'h-' num2str(EndTime) 'h'];
  else
    NEWfn = [fn '_to_be_locs_' num2str(BeginTime) '-' num2str(EndTime)];
  end

  %create filtered log
  NewLog = new_log(NEWfn,context.user,context.library,context.sound);
  NewLog.event = NewEvent(1:NewLength);
  NewLog.length = NewLength;
  NewLog.curr_id = log.curr_id;
  NewLog.sound = log.sound;   %takes sound data from original log

  %save new log
  log_save(NewLog);

  %Display Number of Events in Log
  fprintf(1,'%.0fh-%.0fh  %5.0f  %5.0f     %s\n', ...
    BeginTime, EndTime, log.length, NewLog.length, NEWfn);

%% if there are no events in time range, display warning, don't make a log
else
  
  fprintf(2,'\nWARNING: No events between %.0f:00 and %.0f:00 in %s. No log created.\n\n', ...
    BeginTime, EndTime, fn);
    
end