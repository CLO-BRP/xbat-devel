function [result, context] = prepare(parameter, context)

% DETECTOR VALIDATION BY ARRAY-HOUR - prepare

result = struct;

%% initializations
truth_str = parameter.truth_str;
pos_truth_tag = parameter.pos_truth_tag;
neg_truth_tag = parameter.neg_truth_tag;
test_str = parameter.test_str;
pos_test_tag = parameter.pos_test_tag;
no_pos_test_tag = isempty( pos_test_tag );
out_dir = parameter.out_dir;
      
test_day = [];
test_hour = [];
presence_day = [];
presence_hour = [];
MARKER_day = [];
MARKER_hour = [];

% list names of selected logs
if ~isfield(context, 'target')   %check if context.target is available
	fprintf(2,'API supplied no context.target in prepare.m \n');
	return;
end

logs = context.target;

if ~iscell(logs)
	logs = {logs};
end

NumLogs = length(logs);

%% stop execution if log is open in an XBAT browser
status = ASE_log_is_open(logs, NumLogs);
if status  
	return;  
end

%% find list of log dates, based on log name
truth_log_date = [];
test_log_date = [];
skipped_log_name = '';
for i = 1:NumLogs
    CurrentLog = logs{i};
    [~, fn] = fileparts(CurrentLog);
    date_str = regexp( fn, '_\d\d\d\d\d\d\d\d_', 'match' );
    date_str = date_str{ 1 }( 2 : end - 1 );
    dn = datenum( date_str, 'yyyymmdd' );
    if ~isempty(strfind(fn, truth_str))
        truth_log_date  = [truth_log_date , dn ];
    elseif ~isempty(strfind(fn, test_str))
        test_log_date = [ test_log_date, dn ];
    else
    skipped_log_name = [ skipped_log_name ; { fn } ];
    d = sprintf( 'Log skipped because it is not a truth log or a test log\n  %s', fn );
    fail( d, 'WARNING' );
    end
end
truth_log_date = unique( truth_log_date  );
first_day = min( truth_log_date  );
last_day = max( truth_log_date  );

date_match = ismember( truth_log_date, test_log_date );

if ~any( date_match )
	fail( 'No test logs on days with truth logs', 'WARNING' )
	return;
elseif ~all( date_match )
	
	%-----------------------------------------------------------------------
	%Give user choice about what to do with unmatched truth logs
	%-----------------------------------------------------------------------
	
	%title
	title_txt = 'Unmatched truth logs';
	
	%dialog message
	d1 = 'Truth logs on these dates to not have matching test logs.';
	temp = cellstr( datestr( truth_log_date( ~date_match )));
	d2 = sprintf( '    %s\n', temp{ : } );
	d  = sprintf( '%s\n%s\n', d1, d2 );
	
	%buttons messages
	b = { 'Keep truth logs';
		    'Discard truth logs';
				'Cancel' 
				'Keep truth logs' };
	
	%draw button
	button = questdlg( d, title_txt, b{:} );
	
	%user response
	if strcmp( button, 'Keep truth logs' )		
		discard_missing_flag = false;		
	elseif strcmp( button, 'Discard truth logs' )		
		discard_missing_flag = true;		
	else
		fail( 'Canceled by user', 'Detector Validation by Array-Hour' )
		return;
	end
		
% 	d1 = 'Truth logs on these dates to not have matching test logs.';
% 	temp = cellstr( datestr( truth_log_date( ~date_match )));
% 	d2 = sprintf( '    %s\n', temp{ : } );
% 	d3 = 'Missing test logs will be treated as absence';
% 	d4 = 'Proceed anyway?';
% 	d  = sprintf( '%s\n%s\n%s\n\n%s\n', d1, d2, d3, d4 );
% 	button = questdlg( d );
% 	if ~strcmp( button, 'Yes' )
% 		fail( 'Canceled by user', '' )
% 		return;
% 	end

else   %perfect match between truth log dates and test log dates
	discard_missing_flag = false;
end

%% read logs
truth_log_name = '';
test_log_name = '';

for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  %load log structure
  [SoundName, fn] = fileparts(CurrentLog);
  
  log = get_library_logs('logs',[],SoundName,CurrentLog);
  
  %stop execution if a single, valid log is not specified
  if ~(ischar(SoundName) && ischar(fn) && isequal(length(log),1))
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');
    context.state.kill = 1;
    return;
  end
	
  %% set time-stamps and date-time attributes	
  log.sound.realtime = file_datenum( log.sound.file{1} );	
  log.sound.time_stamp.table = get_schedule_from_files_export( log.sound, 'yyyymmdd_HHMMSS' );	
  
  %% find event date, hour, and primary tag  
  if isempty(log.sound.realtime)    
    d = sprintf('Unable to find begin date of %s', sound_name(log.sound));    
    fail(d, 'ERROR')    
    return;    
  end
	
  t0 = 86400 * ( log.sound.realtime - fix( log.sound.realtime ));	
  day0 = floor( log.sound.realtime );  
  day = zeros(log.length,1);  
  hour = day;  
  tag = cell(log.length,1);
  
  for j = 1:log.length    
    curr_event = log.event(j);    
	t = t0 + get_session_time( log.sound, curr_event.time(1) );		
    curr_hour = floor( t / 3600 );    
    day(j) = day0 + floor(curr_hour / 24);    
    hour(j) = rem(curr_hour, 24);    
    curr_tag = get_tags(curr_event);    
    len = length(curr_tag);    
    if isequal(len,1)      
      tag(j) = curr_tag;      
    elseif len > 1      
      curr_tag = {strrep(curr_tag{1},'*')};      
      tag(j) = curr_tag{1};      
    end
  end  
  
  %% classify log as truth log or test log, then tabulate event times  
  truth_flag = ~isempty(strfind(fn, truth_str));  
  test_flag =  ~isempty(strfind(fn, test_str));
  
  if truth_flag && test_flag    
    d = sprintf('Log name qualifies both as truth log and test log:\n  %s', fn);    
    fail(d, 'ERROR')    
    return    
  elseif ~(truth_flag || test_flag)    
    d = sprintf('Log name does not qualify as truth log or test log:\n  %s', fn);    
    fail(d, 'ERROR')    
    return;    
	elseif test_flag   %test log    
		if no_pos_test_tag
			presence_filter = true( size( tag, 1), 1);
		else
			presence_filter = strcmp(tag, pos_test_tag);
		end
		
		match = presence_filter & ismember( day, truth_log_date );
		
		if any( match )
			test_day = [ test_day ;  day( match ) ];
			test_hour = [ test_hour ; hour( match ) ];
			test_log_name = [ test_log_name ; { fn }];    
		end
		
	else             %truth log
		
		if discard_missing_flag
			date_filter = ismember( day, test_log_date );
		else
			date_filter = true( size( tag, 1), 1);
		end
		
		if any( date_filter )
		
			presence_filter = strcmp( tag, pos_truth_tag) & date_filter ;
			absence_filter  = strcmp( tag, neg_truth_tag) & date_filter ;

			presence_day = [presence_day ;  day(presence_filter)];
			presence_hour = [presence_hour ; hour(presence_filter)];

			MARKER_filter = absence_filter | presence_filter;
			MARKER_day = [MARKER_day ; day(MARKER_filter)];
			MARKER_hour = [MARKER_hour ; hour(MARKER_filter)];
			
			truth_log_name = [truth_log_name ; {fn}];
    
		end
  end
    
end

%% tabulate hourly presence, hourly MARKER, hourly test

num_days = last_day - first_day + 1;

presence_matrix = zeros(num_days, 24);

MARKER_matrix = presence_matrix;

test_matrix = presence_matrix;

for i = 1:length(presence_day)
  
  presence_matrix(presence_day(i)-first_day+1, presence_hour(i)+1) = 1;
  
end

for i = 1:length(MARKER_day)
  MARKER_matrix(MARKER_day(i)-first_day+1, MARKER_hour(i)+1) = 1;
end

for i = 1:length(test_day)
  
  test_matrix(test_day(i)-first_day+1, test_hour(i)+1) = 1;
  
end

%% calculations for 2 x 2 contingiency table
P_matrix = presence_matrix;
N_matrix = MARKER_matrix & ~presence_matrix;
try  
  TP_matrix = P_matrix & test_matrix;
catch    
    d = 'Remove test logs outside the date range covered by truth logs and try again.';
        fail(d, 'ERROR')
        return;
end

FP_matrix = N_matrix & test_matrix;
FN_matrix = P_matrix & ~test_matrix;
TN_matrix = N_matrix & ~test_matrix;

%calculate counts
TP = sum(sum(TP_matrix));
FP = sum(sum(FP_matrix));
FN = sum(sum(FN_matrix));
TN = sum(sum(TN_matrix));

ACC = (TP + TN ) / ( TP + FN + FP + TN ); %Accuracy
F1  = 2 * TP / (2 * TP + FP + FN );       %F1 Score
MCC = ( ( TP * TN ) - ( FP * FN ) ) / ... %Matthews Correlation Coefficient
	sqrt( (TP + FP) * (TP + FN) * ( TN + FP ) * (TN + FN ) );


%-------------------------------------------------------------------------
%estimate summary statistics and confidence intervals
%-------------------------------------------------------------------------

%if statistics toolbox is installed
pat = '(?<=^.+[\\/]toolbox[\\/])[^\\/]+';
result = regexp( which( 'binofit' ), pat, 'match', 'once' );
if strcmp( result, 'stats' )
	[ TPR, ciTPR ] = binofit( TP, ( TP + FN ), 1 - parameter.pvalue );
	[ FPR, ciFPR ] = binofit( FP, ( FP + TN ), 1 - parameter.pvalue );
	[ PPV, ciPPV ] = binofit( TP, ( TP + FP ), 1 - parameter.pvalue );
	[ NPV, ciNPV ] = binofit( TN, ( TN + FN ), 1 - parameter.pvalue );
	
%if statistics toolbox not installed
else
	TPR = TP / ( TP + FN );
	FPR = FP / ( FP + TN );
	PPV = TP / ( TP + FP );
	NPV = TN / ( TN + FN );
	ciTPR = [ NaN, NaN ] ;
	ciFPR = [ NaN, NaN ] ;
	ciPPV = [ NaN, NaN ] ;
	ciNPV = [ NaN, NaN ] ;
	
	d = sprintf('\nInstall MATLAB Statistics Toolbox \nto allow calculation of confidence intervals.');
	fail( d, 'WARNING' );
	
end
	


%% make output table for Compare worksheet

array_hour_matrix = cell(num_days, 24);

if TP

  array_hour_matrix(TP_matrix) = {'TP'};
  
end

if FP

  array_hour_matrix(FP_matrix) = {'FP'};
  
end

if FN

  array_hour_matrix(FN_matrix) = {'FN'};
  
end

if TN
  
  array_hour_matrix(TN_matrix) = {'TN'};
  
end


%% make output table for Truth worksheet
           
[m,~] = size(array_hour_matrix);

truth_out = cell(m,24);

truth_out(strcmp(array_hour_matrix,'TP') | strcmp(array_hour_matrix, 'FN')) = {1};

truth_out(strcmp(array_hour_matrix,'FP') | strcmp(array_hour_matrix, 'TN')) = {0};


%% make output table for Test worksheet

test_out = cell(m,24);

test_out(strcmp(array_hour_matrix,'TP') | strcmp(array_hour_matrix, 'FP')) = {1};

test_out(strcmp(array_hour_matrix,'FN') | strcmp(array_hour_matrix, 'TN')) = {0};


%% make cell matrix with log names for output
num_truth = size(truth_log_name,1);
C_truth = cell(num_truth+3,25);
C_truth(1,1) = {'Truth Log Names'};
if isempty( truth_log_name )
	txt = 'No truth logs were found';
	fail( txt, 'WARNING' )
	return;
end
C_truth(2:num_truth+1,1) = truth_log_name;

num_test = size(test_log_name,1);
C_test = cell(num_test+3,25);
C_test(1,1) = {'Test Log Names'};
if isempty( test_log_name )
	txt = 'No test logs were found';
	fail( txt, 'WARNING' )
	return;
end
C_test(2:num_test+1,1) = test_log_name;

num_skipped = size(skipped_log_name,1);
C_skipped = cell(num_skipped+3,25);
C_skipped(1,1) = {'Skipped Log Names'};
if ~isempty( skipped_log_name )
  C_skipped(2:num_skipped+1,1) = skipped_log_name;
end
C_logs = [ C_truth ; C_test ; C_skipped ];

%% make generic worksheet header

C_HEADER             = cell( 14, 25 );
C_HEADER(  1, 1    ) = { 'Detector Evaluation by Array-Hour' };
C_HEADER(  4, 1:2  ) = { 'Run:', datestr( now ) };

C_HEADER(  6,  1   ) = { 'USER PARAMETERS'                                                          };
C_HEADER(  7,  1:2 ) = { 'alpha level',   parameter.pvalue                                              };
C_HEADER(  8,  1:5 ) = { 'Truth Log', '', 'Filter Log Names By',        '', parameter.truth_str     };
C_HEADER(  9,  1:5 ) = { '',          '', 'Event Tag When Presence',    '', parameter.pos_truth_tag };
C_HEADER( 10,  1:5 ) = { '',          '', 'Event Tag When No Presence', '', parameter.neg_truth_tag };
C_HEADER( 11,  1:5 ) = { 'Test Log',  '', 'Filter Log Names By',        '', parameter.test_str      };
C_HEADER( 12,  1:5 ) = { '',          '', 'Event Tag When Presence',    '', parameter.pos_test_tag  };


%% make SUMMARY worksheet

C_HEADER( 2,1 )    = {'SUMMARY'};

C_BODY1            = cell( 20, 25 );
C_BODY1(  1, 1:4 ) = { '',    'Estimate', ...
	                    sprintf( '%.1f%% CI (lower)', 100 * ( 1 - parameter.pvalue ) ), ...
						sprintf( '%.1f%% CI (upper)', 100 * ( 1 - parameter.pvalue ) ) };
C_BODY1(  2, 1:4 ) = { 'TPR', TPR, ciTPR( 1 ), ciTPR( 2 ) };
C_BODY1(  3, 1:4 ) = { 'FPR', FPR, ciFPR( 1 ), ciFPR( 2 ) };
C_BODY1(  4, 1:4 ) = { 'PPV', PPV, ciPPV( 1 ), ciPPV( 2 ) };
C_BODY1(  5, 1:4 ) = { 'NPV', NPV, ciNPV( 1 ), ciNPV( 2 ) };

C_BODY1(  8, 1:2 ) = { 'Accuracy', ACC };
C_BODY1(  9, 1:2 ) = { 'F1 Score', F1 };
C_BODY1( 10, 1:2 ) = { 'Matthews Correlation Coefficient', MCC };

C_BODY1( 13, 1:2 ) = { '',   'COUNT' };
C_BODY1( 14, 1:2 ) = { 'TP', TP };
C_BODY1( 15, 1:2 ) = { 'FP', FP };
C_BODY1( 16, 1:2 ) = { 'FN', FN };
C_BODY1( 17, 1:2 ) = { 'TN', TN };
C_BODY1( 18, 1:2 ) = { 'Array-Hours', sum( [ TP, FP, FN, TN] ) };

C_SUMMARY          = [ C_HEADER ; C_BODY1 ; C_logs ];


%% make Compare worksheet

C_HEADER( 2, 1 )    = { 'Compare Test Logs with Truth Logs' };

[ m, ~ ]            = size( array_hour_matrix );
C_BODY2             = cell( m+7, 25 );
C_BODY2( 1, 2:25 )  = mat2cell( datestr( ( 0:23 ) ./ 24, 'HH:MM' ), ones( 24, 1 ), 5 )';
C_BODY2( 2:1+m, 1 ) = mat2cell( datestr( first_day:last_day, 1 ), ...
                        ones( num_days, 1 ), ...
                        11 );
C_BODY2(2:1+m,2:25) = array_hour_matrix;

C_Compare           = [ C_HEADER ; C_BODY2 ; C_logs ];


%% make Truth worksheet

C_HEADER( 2, 1 )    = { 'Truth Log Presence' };
           
[m,~]               = size( array_hour_matrix );
C_BODY3             = cell( m+7, 25 );
C_BODY3( 1, 1 )     = { 'Detector Evaluation by Array-Hour' };
C_BODY3( 2, 1 )     = { 'Truth Log Presence' };
C_BODY3( 4, 1:2 )   = { 'Run:', datestr( now )};
C_BODY3( 7, 2:25 )  = mat2cell( datestr( ( 0:23 ) ./ 24, 'HH:MM' ), ones( 24, 1 ), 5 )';
C_BODY3( 8:7+m, 1 ) = mat2cell( datestr( first_day:last_day, 1 ), ...
                        ones( num_days, 1 ), ...
                        11 );
C_BODY3( 8:7+m, 2:25 ) = truth_out;

C_Truth = [ C_HEADER ; C_BODY3 ; C_logs];


%% make Test worksheet
C_HEADER( 2, 1 )    = { 'Test Log Presence' };
           
[ m ,~ ]            = size( array_hour_matrix);
C_BODY4             = cell( m+11 ,25 );
C_BODY4( 4, 1:2 )   = { 'Run:', datestr( now ) };
C_BODY4( 7, 2:25 )  = mat2cell( datestr( (0:23) ./ 24, 'HH:MM' ), ones( 24, 1 ), 5 )';
C_BODY4( 8: 7+m,1 ) = mat2cell( datestr( first_day:last_day, 1 ), ...
                        ones( num_days, 1 ), ...
                        11);
C_BODY4(8:7+m,2:25) = test_out;
C_Test              = [ C_HEADER ; C_BODY4 ; C_logs];

%% output XLSX
fn_out = sprintf('Detector_Eval_by_Array-Hour_%s.xlsx', datestr(now,30));
fn_out_full = fullfile(out_dir, fn_out);
xlswrite(fn_out_full, C_SUMMARY, 'SUMMARY');
xlswrite(fn_out_full, C_Compare, 'Compare');
xlswrite(fn_out_full, C_Truth, 'Truth');
xlswrite(fn_out_full, C_Test, 'Test');
fprintf('\nOutput to %s\n', fn_out_full);