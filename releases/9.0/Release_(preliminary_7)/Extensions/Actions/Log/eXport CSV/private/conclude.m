function [result, context] = conclude(parameter, context)
result = struct;

% eXport CSV - conclude

% Log action exports selected XBAT log into single CSV file, which is 
% created in the XBAT library for the currently-selected sound.

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. Make action independent of currently-selected sound

% Change Log
%
% 1. 

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

%% open output CSV
fnCSV = context.library.name;
fnCSV = [fnCSV '.csv'];

d = 'OUTPUT CSV';

[fnCSV, pathCSV] = uiputfile('*.csv', d, fnCSV);

if isequal(fnCSV,0) || ~exist(pathCSV,'dir')
  fprintf(2, 'Action canceled by user\n');
  return;
end

% open exported CSV file and print header
fid = fopen([pathCSV fnCSV], 'w');

%terminate if output file failed to open
if isequal(fid, -1)
  fprintf(2,'Output CSV could note be opened\n');
  return;
end
  
%output header line
h1 =  'log, id, channel, real_start, real_stop, record_start, record_stop, ';
h2 = 'duration, min_freq, max_freq, bandwidth, score, rating, tags, notes';
fprintf(fid, '%s%s\n', h1, h2);

%output body
fprintf(fid, '%s\n', context.state.line{:});

%close exported csv
fclose(fid);