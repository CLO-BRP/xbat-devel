function [result, context] = prepare(parameter, context)
% ADD EVENT MEASUREMENTS - prepare

% To Do

%initializations
result = struct;
tstart = tic;

%Stop if any open browsers
if close_browsers
  return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
  return;
end

%Stop if sounds in library have differing number of channels
if test_channels( logs )
  return;
end

%make handle function for selected event measurement
if strcmp( parameter.measurements{ 1 }, 'CSE Location (v2.3)' )
  measurement_function = str2func( 'Location_v2p3_measurement_PRE' );
elseif strcmp( parameter.measurements{ 1 }, 'RL-TD' ) 
  measurement_function = str2func( 'RL_TD_measurement' );
else
  return;
end

%make empty measurement struct with correct parameters
measurement = make_empty_measurement( measurement_function, parameter );

%--------------------------------------------------------------------------
% for each log
%--------------------------------------------------------------------------
library = get_active_library;
NumLogs = length(logs);
  wh = findobj('tag','XBAT_WAITBAR::Add Event Measurementsing Logs ...');

for i = 1:NumLogs

  t0 = tic;
  
  %load and refresh path in log
  [ log, snd_name, status ] = load_log( logs{ i }, context );
  if status
    return;
  end
  
  %indicate log being processed in waitbar
  waitbar_update(wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...']);
  
%   %set sound state per parameter
  [ snd_struct_path, orig_sound_struct ] = set_sound_state( snd_name, parameter, context );
  
  %Open browser
  fh = open_library_sound(snd_name, library);
  
  %set signal filter into browser, if specified by user
  data = set_filter( fh, i, parameter, context );
  
  %update sound struct in log
  log.sound = data.browser.sound;

  %add event measurements (log cannot be open in browser!)
  log_out =  measurement_function( 'batch', log, 1:log.length, measurement );

  %write measurements to log
  if isfield( log_out, 'type' ) && strcmp( log_out.type, 'log' )
    log_save(log_out);
  else
    d = sprintf( 'Measurement algorithm failed for\n   %s\n', log_name( log ) );
    fail( d, 'WARNING' )
  end
  
  %close browser
  browser_file_menu( fh, 'Close' );
  
  %return XBAT sound struct to original state
  save( snd_struct_path, '-struct', 'orig_sound_struct' );
  
  %indicate log processed in waitbar
  result.target = log;
  result.status = 'done';
  result.message = '';
  result.error = [];
  result.elapsed = toc(t0);
  result.output = struct([]);
  action_waitbar_update(wh(1), i/NumLogs, result, 'log'); 
    
end

d = sprintf('%.0f logs processed in %f sec', NumLogs, toc(tstart));
waitbar_update(wh(1), 'PROGRESS', 'message', d);
% close(h)    %close waitbar

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error('No Error')
