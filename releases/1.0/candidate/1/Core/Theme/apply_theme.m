function apply_theme(handle, theme, finder)

% TODO: use set header image for headers

% set_header_image(handles.axes, color.separator.back);

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default finder
%--

% NOTE : the findall will modify axes 'title', 'xlabel', and 'ylabel'

if nargin < 3
	finder = @findall; 
end

%--
% set default theme
%--

% TODO: store default theme to file perhaps, consider using text

if nargin < 2
	
	theme.alpha = 0.2; 
	
	theme.figure.color = 0.2 * ones(1, 3); 
	
	theme.axes.color = [1, 1, 0.8]; 
	
	theme.text.color = 'white';
	
end

%--
% deal with handle input
%--

type = {'axes', 'text', 'patch', 'rectangle', 'uicontrol'};

if numel(handle) == 1 && strcmp(get(handle, 'type'), 'figure')
	
	par = handle; clear handle;
	
	for k = 1:numel(type)
		handle.(type{k}) = finder(par, 'type', type{k});
	end
	
else
	
	par = get_parent_figure(handle); in = handle; clear handle; 

	for k = 1:numel(type)
		handle.(type{k}) = in(strcmp(get(in, 'type'), type{k}));
	end

end

%-------------------
% APPLY THEME
%-------------------

%--
% set base colors
%--

base = theme.figure.color;

set(par, 'color', base);

if has_handles(handle, 'axes') 
	set(handle.axes, 'color', theme.axes.color, 'xcolor', theme.text.color, 'ycolor', theme.text.color);
end

if has_handles(handle, 'text') 
	set(handle.text, 'color', theme.text.color);
end

% NOTE: these are really intended for patches used in building interface elements, not display patches

% TODO: factor function that skips handles that have a color property set to 'none'

if has_handles(handle, 'patch') 
	
	for k = 1:numel(handle.patch)
		
		current = handle.patch(k);
		
		if ~ischar(get(current, 'facecolor'))
			set(current, 'facecolor', theme.axes.color);
		end
		
		if ~ischar(get(current, 'edgecolor'))
			set(current, 'edgecolor', theme.text.color);
		end
		
	end

end

if has_handles(handle, 'rectangle') 
	
	for k = 1:numel(handle.rectangle)
		
		current = handle.rectangle(k);
		
		if ~ischar(get(current, 'facecolor'))
			set(current, 'facecolor', theme.axes.color);
		end
		
		if ~ischar(get(current, 'edgecolor'))
			set(current, 'edgecolor', theme.text.color);
		end
		
	end
	
end

% TODO: implement theming of 'uicontrol' elements

if has_handles(handle, 'uicontrol')
	
	control = handle.uicontrol;
	
	for k = 1:numel(control)
		
		switch get(control(k), 'style')
			
			case {'text', 'checkbox'}
				set(control(k), 'foregroundcolor', theme.text.color, 'backgroundcolor', base);
				
			case {'edit', 'listbox', 'popupmenu', 'slider'}
				set(control(k), 'foregroundcolor', theme.text.color, 'backgroundcolor', theme.axes.color);
				
		end
		
	end
	
end

%--
% blend colors as needed
%--

if theme.alpha < 1
	blend_colors(par, theme.alpha);
end


%--------------------------------
% BLEND_COLORS
%--------------------------------

% TODO: this function wants to be factored, it can be used for other purposes

function blend_colors(par, alpha, finder)

%--
% handle input and setup
%--

if nargin < 3
	finder = @findall;
end

if nargin < 2
	alpha = 0.2;
end

base = get(par, 'color'); beta = 0.5 * (alpha + 1);

%--
% update axes
%--

ax = finder(par, 'type', 'axes');

for k = 1:numel(ax)
	
	back = get(ax(k), 'color'); grid = get(ax(k), 'xcolor'); 
	
	new_back = alpha * back + (1 - alpha) * base; new_grid = beta * grid + (1 - beta) * base;
	
	set(ax(k), ...
		'color', new_back, 'xcolor', new_grid, 'ycolor', new_grid ...
	);
	
end

%--
% update text
%--

tx = finder(par, 'type', 'text');

for k = 1:numel(tx)
	grid = get(tx(k), 'color'); set(tx(k), 'color', beta * grid + (1 - beta) * base);
end

%--
% update patches and rectangles
%--

pt = union(finder(par, 'type', 'patch'), finder(par, 'type', 'rectangle'));

for k = 1:numel(pt)
	
	face = get(pt(k), 'facecolor'); 
	
	if ~ischar(face)
		new_face = alpha * face + (1 - alpha) * base; set(pt(k), 'facecolor', new_face);
	end
	
	edge = get(pt(k), 'edgecolor');
	
	if ~ischar(edge)
		new_edge = beta * edge + (1 - beta) * base; set(pt(k), 'edgecolor', new_edge);
	end
	
end

%--
% update controls
%--

ui = finder(par, 'type', 'uicontrol');

for k = 1:numel(ui)

	control = ui(k);
	
	switch get(control, 'style')
		
		case {'text', 'edit', 'listbox', 'popupmenu', 'checkbox', 'slider'}
			
			back = get(control, 'backgroundcolor'); set(control, 'backgroundcolor', alpha * back + (1 - alpha) * base);
			
			fore = get(control, 'foregroundcolor'); set(control, 'foregroundcolor', beta * fore + (1 - beta) * base);
			
	end
	
end


%--------------------------------
% GET_PARENT_FIGURE
%--------------------------------

function par = get_parent_figure(handle)

par = [];

for k = 1:numel(handle)
	par(k) = ancestor(handle(k), 'figure');
end

par = unique(par);

% TODO: this should lead to a warning or error, consider input option

if numel(par) > 1
	par = par(1);
end


%--------------------------------
% HAS_HANDLES
%--------------------------------

function value = has_handles(handle, type)

value = isfield(handle, type) && ~isempty(handle.(type));

