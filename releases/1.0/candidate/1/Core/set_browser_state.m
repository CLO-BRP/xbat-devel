function state = set_browser_state(par, state, opt)

% set_browser_state - set browser display state
% ---------------------------------------------
%
% state = set_browser_state(par, state, opt)
%
%   opt = set_browser_state
% 
% Input:
% ------
%  par - browser figure handle
%  state - browser display state
%  opt - state loading options
%
% Output: 
% -------
%  state - browser display state
%  opt - state loading options

%-------------------
% HANDLE INPUT
%-------------------

%--
% set and possibly output default set options
%--

if (nargin < 3) || isempty(opt)
	
	opt.position = 1;
	
	opt.palettes = 1;
	
	opt.log = 1;
	
	opt.selection = 1;
	
	if ~nargin
		state = opt; return;
	end
	
end
	
%-------------------
% SETUP
%-------------------

%--
% turn off sounds while we are doing this
%--

palette_sounds = get_env('palette_sounds');

if isempty(palette_sounds)
	palette_sounds = 'on';
end

set_env('palette_sounds', 'off');

%--
% turn off palette daemon if needed
%--

% NOTE: this can be encapsulated

timer_stop('XBAT Palette Daemon');

%--
% check screensize
%--

% NOTE: the screensize check could be problematic

screensize = get(0, 'screensize');

if ~isequal(screensize, state.screensize)
	
	disp(' ');
	disp('WARNING: Screen-size changed between sessions, problems may arise.');
	disp(' ');
	
end

%-------------------
% OUT OF SCREEN
%-------------------

% NOTE: this may not work, but we try

%--
% consider offset for figures off the screen
%--

offset = 0;

if (state.position(1) <= 0)
	
	offset = -state.position(1) + 32;
	
elseif (state.position(1) >= screensize(3))
	
	offset = -state.position(3) - 32;
	
end

%--
% update children positions based on offset
%--

if offset

	state.position(1) = state.position(1) + offset;

	if isfield(state, 'palette')
		for k = 1:length(state.palette)
			state.palette(k).position(1) = state.palette(k).position(1) + offset;
		end
	end
	
	if isfield(state, 'children')
		for k = 1:length(state.children)
			state.children(k).position(1) = state.children(k).position(1) + offset;
		end	
	end
	
end

%----------------------------------
% SET BROWSER POSITION
%----------------------------------

if opt.position
	
	set(par, 'position', state.position);

end

%----------------------------------
% OPEN PALETTES
%----------------------------------

if opt.palettes && (isfield(state, 'palette') || isfield(state, 'children'))
	
	%---------------------
	% NEW
	%---------------------
	
	if isfield(state, 'children')
		
		for k = 1:length(state.children)

			child = state.children(k);
            
            if isempty(child.parent.type)
                continue;
            end
            
			switch child.parent.type

				case 'core'
					handle = browser_palettes(par, child.name);

				otherwise
					handle = extension_palettes(par, child.parent.name, child.parent.type);

			end
			
			if ~isempty(handle)
				
				if strcmpi(child.type, 'XBAT_PALETTE')
					set_palette_state(handle, child);
				else
					set(handle, 'position', child.position);
				end
				
			end

		end

	%---------------------
	% OLD
	%---------------------
	
	else

		for k = 1:length(state.palette)

			try

				pal = [];

				%--
				% try to open palette using tag if available
				%--

				if isfield(state.palette(k), 'tag')

					info = parse_tag(state.palette(k).tag, '::', {'ignore', 'type', 'name'});

					info.type = lower(info.type);

					switch info.type

						case 'core'
							pal = browser_palettes(par, state.palette(k).name);

						otherwise
							pal = extension_palettes(par, info.name, info.type);

					end

				end

				%--
				% try to open palette by beating with a stick in the dark
				%--

				if isempty(pal)

					pal = browser_palettes(par, state.palette(k).name);

					% NOTE: in this case the function tries to infer the type

					if isempty(pal)
						pal = extension_palettes(par, state.palette(k).name);
					end

				end

				%--
				% set palette state
				%--

				% TODO: this step fails for detector palettes

				if ~isempty(pal)
					set_palette_state(pal, state.palette(k));
				end

			catch

				nice_catch(lasterror, ['WARNING: Failed to open ''', state.palette(k).name, ''' palette.']);

			end

		end

	end

end

%----------------------------------
% RESTORE PALETTE STATES
%----------------------------------

if opt.palettes && (isfield(state, 'palette_states') || isfield(state, 'widget_states'))

	data = get(par, 'userdata');

	if isfield(state, 'palette_states')
		data.browser.palette_states = state.palette_states;
	end
	
	if isfield(state, 'widget_states')
		data.browser.widget_states = state.widget_states;
	end
	
	set(par, 'userdata', data);

end
	
%----------------------------------
% SET OPEN LOGS STATE
%----------------------------------

if opt.log && isfield(state, 'log') && ~isempty(state.log)

	%--
	% get library content from figure tag
	%--
	
	info = parse_tag(get(par, 'tag')); 
	
	lib = get_libraries([], 'name', get_library_file_name(info.library));
	
	%--
	% construct library log locations
	%--
	
% 	db_disp;
	
	logs = get_logs(lib, data.browser.sound, state.log.names);
	
	%--
	% separate active log from other logs in list
	%--
	
	active = logs(state.log.active);
	
	logs(state.log.active) = [];
	
	%--
	% try to open logs from state
	%--
	
	% NOTE: we open active log last, and log open is given a no warning flag

    for k = 1:length(logs)       
		log_open(par, logs(k), 0);       
    end
    
	log_open(par, active, 0);
	
	% TODO: consider notifying user if some log failed to load
	
end

%----------------------------------
% RESTORE SELECTION STATES
%----------------------------------

if opt.selection && isfield(state, 'selection') && ~isempty(state.selection)

	%--
	% update browser selection state
	%--
	
	% TODO: browsers should become objects
	
	data = get(par, 'userdata');

	data.browser.selection = state.selection;

	set(par, 'userdata', data);

	%--
	% make sure that controls are in sync with state
	%--
	
	% NOTE: we recreate the palette so that current selection values will be loaded
	
	pal = get_palette(par, 'Selection', data);
	
	if ~isempty(pal)
		
		close(pal); browser_window_menu(par, 'Selection'); % create and set state
	
	end	
	
	%--
	% update selection display
	%--
	
	selection_update(par, data);
	
end

if isfield(state, 'marker')
	
	set_browser_marker(par, state.marker, 1, data);
	
end

%-------------------
% CLEANUP
%-------------------

%--
% set palette sounds to previous state
%--

% NOTE: this is another problem related to failure to load state

set_env('palette_sounds', palette_sounds);

%--
% restart daemon if needed
%--

timer_run('XBAT Palette Daemon');