function info = get_whois(ip, server)

% TODO: add caching

%--
% get date from service
%--

url = ['http://ws.arin.net/whois/?queryinput=', ip]; % 128.253.113.118

response = curl_rest('get', url);

lines = {}; state = 0;

%--
% get relevant lines from response content
%--

for k = 1:numel(response.content.lines)
	
	current = strtrim(response.content.lines{k});
	
	if state
		if strcmpi(current, '</pre>')
			break;
		else
			if ~isempty(current) && current(1) ~= '#'
				lines{end + 1} = current;
			end
		end
	else
		state = strcmpi(current, '<pre>');
	end
	
end

%--
% parse lines to get info struct
%--

% NOTE: the names used in this format are proper field names

% NOTE: the format may have multiple entries per field, these appear on separate lines

info = struct;

for k = 1:numel(lines)
	
	% TODO: the split function should handle a single split!
	
	disp(lines{k})
	
	part = str_split(lines{k}, ':'); 
	
	field = part{1}; 
	
	if numel(part) < 2
		part{2} = '';
	else
		value = part{2};
	end
	
	% NOTE: handle field creation, first append to field, and further appends
	
	if ~isfield(info, field)
		
		info.(field) = value;
		
	else
		
		if ischar(info.(field))
			info.(field) = {info.(field), value};
		else
			info.(field){end + 1} = value;
		end
		
	end

end