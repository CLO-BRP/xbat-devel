function content = get_connection_headers(connection) 

%--
% get string representation of java map object
%--

str = char(get(connection, 'HeaderFields'));

%--
% parse string to get header elements
%--

element = str_split(str(2:end - 1), ','); 

for k = numel(element):-1:1
	
	if sum(ismember(element{k}, '[]')) == 1
		
		element{k - 1} = [element{k - 1}, element{k}]; element(k) = [];
		
	end
	
end

iterate(@disp, element);

%--
% parse header element strings into field value pairs and pack in struct
%--

content = struct;

for k = 1:numel(element) 

	part = str_split(element{k}, '='); 
	
	field = lower(strrep(part{1}, '-', '_')); value = part{2}(2:end - 1);
	
	content.(field) = value;

end