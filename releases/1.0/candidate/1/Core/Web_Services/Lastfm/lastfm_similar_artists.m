function result = lastfm_similar_artists(artist)

% TODO: the content of this function will be nearly fully factored, leaving only 'url' construction

%--
% create and check persistent cache
%--

% TODO: centralize cache, this ties well with the database idea

persistent SIMILAR;

if isempty(SIMILAR)
	SIMILAR = struct;
end

% NOTE: we normalize the name at the start, this holds for all names

artist = lastfm_name(artist); 

% NOTE: get result from cache if available

% TODO: move cache to database, set an expiration date, implement refresh option

field = genvarname(artist);

if isfield(SIMILAR, field)
	result = SIMILAR.(field); return;
end

%--
% try to get data from service and create struct from XML, note this also works for RSS
%--

url = [lastfm_root, 'artist/', artist, '/similar.xml'];

try
	result = url_read(url);
catch
	nice_catch;
end

result = xml_to_struct(result);

% NOTE: we update the cache

SIMILAR.(field) = result;


