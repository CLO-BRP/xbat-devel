function [info, response] = get_hostip(ip, opt)

% get_hostip - use hostip service to get estimated location
% ---------------------------------------------------------
%
% 
% TODO: add caching to this service, clearly this is slow changing information

%--
% handle input
%--

if nargin < 2
	opt = 'full';
end

if ~nargin
	ip = get_ip;
end

%--
% get resource 
%--

switch opt
		
	case 'country'
		
		url = ['http://api.hostip.info/country.php?ip=', ip];
		
	case 'basic'
		
		url = ['http://api.hostip.info/get_html.php?ip=', ip];
		
	case 'full'
		
		% NOTE: the first url should work according to http://www.hostip.info/use.html
		
% 		url = ['http://api.hostip.info/get_html.php?ip=', ip, '&position=true'];

		url = ['http://api.hostip.info/?ip=', ip];

end

done = 0;

% TODO: the 'curl_rest' call is hanging, we need to be able to set a timeout on the request

for k = 1:3
	try
		response = curl_rest('get', url); done = 1; break;
	catch
		disp('Hostip access failed, trying again ...'); pause(0.25);
	end
end

% NOTE: if we failed we silently return trivial output

if ~done
	info = struct; response = struct; return;
end

%--
% parse resource
%--

lines = response.content.lines;

switch opt

	case 'country'
		
		info.country = lines{1};
		
	case 'basic'
		
		for k = 1:numel(lines)
			part = str_split(lines{k}, ':'); info.(lower(part{1})) = part{2};
		end
		
	case 'full'

		% NOTE: we can use a simple approach to parsing the very simple output, currently we are using this to debug 'xml_to_struct'
		
		% NOTE: removing the tag namespacing makes the output fields simpler
		
		lines = iterate(@strrep, lines, 'gml:', '');
		
		% NOTE: the 'pop' gets rid of the root element
		
		result = struct_pop(xml_to_struct(lines));
		
		try
			
			branch = result.featureMember.Hostip; 
			
			% NOTE: the next three fields are the basic 'full' fields, we also pack the full output as 'full'
			
			info.country = branch.countryName; 
			
			info.city = branch.name; 
			
			info.coordinates = branch.ipLocation.PointProperty.Point.coordinates;
			
			part = str_split(info.coordinates, ','); 
			
			info.lon = eval(part{1}); info.lat = eval(part{2});
			
			info.full = result;
			
		catch 
			
			% NOTE: this should only happen if the service format changes
			
			info.full = result;
			
		end
		
end



