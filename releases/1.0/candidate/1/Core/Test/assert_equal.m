function [result, reason] = assert_equal(x1, x2, tol)

% assert_equal - compare and perhaps explain result
% -------------------------------------------------
%
% [result, reason] = assert_equal(x1, x2, tol)
%
% Input:
% ------
%  x1 - value
%  x2 - value
%  tol - tolerance for maximum difference between numeric arrays
%
% Output:
% -------
%  result - of equality test
%  reason - for inequality

% TODO: consider 'nan' values

% TODO: consider using the variable names in messages we produce

%--
% initialize trivial reason to trivial
%--

reason = struct; 

%--
% check for numeric equality
%--

if isnumeric(x1) || isnumeric(x2)
	
	% NOTE: this helps discriminate the two types of numeric equality failures
	
	accuracy = 0;
	
	%--
	% check size and compatibility
	%--
	
	try
		d = x1 - x2; d = max(abs(d(:))); result = 1;
	catch
		result = 0; reason = lasterror;
	end
	
	%--
	% check for numerical equality to tolerance if needed
	%--
	
	if result
		
		if nargin < 3
			tol = eps;
		end

		result = d <= tol;

		if ~result
			accuracy = 1; reason.message = ['Maximum array element difference ', num2str(d), ' exceeds tolerance ', num2str(tol), '.'];
		end
	
	end

	%--
	% throw an error if output is not captured
	%--
	
	if ~nargout && ~result

		if accuracy
			error(reason.message);
		else
			rethrow(reason);
		end

	end

else
	
	result = isequal(x1, x2);
	
	if ~nargout && ~result
		error('Input values are not equal.');
	end
	
end




















%--------------------------------
% TEST_
%--------------------------------

% NOTE: this may be used somewhere ... find out where

function pass = test_(in_1, in_2, threshold, mode, names) %#ok<DEFNU>

value = 0;

if nargin < 5 || isempty(names)
    names = {'value 1', 'value 2'};
end

if nargin < 4 || isempty(mode)
    mode = 'max';
end

if nargin < 3 || isempty(threshold)
    threshold = eps;
end

if threshold > 0
    disp(['checking that ', names{1}, ' has ', mode, ' difference from ', names{2}, ' less than or equal to ', num2str(threshold), '...']); 
else
    disp(['checking that ', names{1}, ' is equal to ', names{2}, '...']);
end

if size(in_1) ~= size(in_2)
    disp('failed. vectors are not the same size'); disp(' '); return;
end

d = abs(in_1 - in_2);

switch mode
    
    case 'max'
        
        value = max(d(:));
        
    case 'rms'
        
        value = sqrt(mean(d(:).^2));
        
end

pass = value > threshold;

if pass
    disp(['failed. ' mode ' difference = ', num2str(max(d(:)))]);
else
    disp('passed.'); 
end

disp(' ');