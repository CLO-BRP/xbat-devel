function result = map(value, range, limits)

% map - scalars to output range
% -----------------------------
%
% result = map(value, range, limits)
%
% Input:
% ------
%  value - to map
%  range - for output
%  limits - for input values
%
% Output:
% -------
%  result - of value map

%--
% handle input
%--

if nargin < 3
	limits = fast_min_max(value);
end

if nargin < 2
	range = [0, 1];
end

%--
% map values
%--

scale = diff(range) / diff(limits);

result = scale * (value - limits(1)) + range(1);