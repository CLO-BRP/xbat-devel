function sites_cache_clear

% SITES_CACHE_CLEAR clear sites assets cache
%
% sites_cache_clear

clear get_assets;
