function [value, styles] = has_styles(varargin)

% HAS_STYLES check that site has styles
%
% [value, styles] = has_styles(site, update)

[value, styles] = has_assets('styles', varargin{:});
