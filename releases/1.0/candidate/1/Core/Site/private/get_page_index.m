function [k, page] = get_page_index(pages, page)

% GET_PAGE_INDEX get page index from page
%
% [k, page] = get_page_index(pages, page)

% NOTE: the second output could be used if the input was a duck

[page, select] = struct_select(pages, 'id', page.id);

k = find(select, 1);