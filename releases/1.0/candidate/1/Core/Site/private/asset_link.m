function link = asset_link(data, type, name, varargin)

% ASSET_LINK link asset from page
%
% link = asset_link(data, type, name, varargin)

%--
% get asset to link
%--

site = data.model.site;

asset = get_asset(site, type, name);
		
%--
% handle missing asset
%--

if isempty(asset)

	if strcmp(type, 'images')
		
		% NOTE: the image name is used by the template
		
		data.name = name;
		
		link = file_readlines( ...
			which('missing_image_warning.html'), {@process_callback, data} ...
		);
	
	else
		
		link = {}; disp(['WARNING: ', title_caps(type) , ' ''', name, ''' not found in ''', site, '''.']);
	
	end

end

%--
% copy asset to build asset directory
%--

linked = is_url(name);

if linked
	
	url = name;

else
	
	root = [build_root(site), filesep, type];
	
	file = [root, filesep, strrep(name, '/', filesep)];

	create_dir(fileparts(file));
	
	% TODO: compare file modification dates

	if ~exist(file, 'file') && ~isempty(create_dir(root))

		disp(['Copying ', type(1:end - 1), ' ''', name, ''' to build ...']);

		switch type

			% NOTE: processing the style file allows us to use variables in the CSS

			case 'styles'
				file_process(file, asset, {@process_callback, data});

			otherwise
% 				db_disp; asset, file
				
				copyfile(asset, file);

		end

	end
	
	%--
	% create url assets
	%--

	page = data.page;

	url = [level_prefix(page.level), type, '/', name];

end

%--
% create attribute string for asset
%--

[attr, opt] = parse_tokens(type, varargin);

%--
% create link
%--

switch type
	
	case 'styles'
		
		link = ['<link rel="stylesheet" href="', url, '"', attr, '/>'];
		
	case 'scripts'
		
		link = ['<script type="text/javascript" src="', url, '"', attr, '></script>'];
	
	case 'images'
		
		%--
		% handle case of linked to images
		%--
		
		if linked	
			
			link = ['<img alt="', '" src="', url, '"', attr, '/>']; return;
		
		end
		
		% TODO: improve caption processing, add license processing
		
		if ~isfield(opt, 'thumb')
			
			%--
			% simple image display
			%--
			
			% NOTE: this means no resize
			
			if ~isfield(opt, 'image')
				
				link = ['<img alt="', name,'" src="', url, '"', attr, '/>']; 
				
				if isfield(opt, 'short_caption')
					link = [link, '<p>', opt.short_caption, '</p>'];
				end
				
				if isfield(opt, 'license')
					db_disp(['adding license to: ', name]);
					link = append_license(link, opt);
				end
				
				return;
				
			end
			
			%--
			% resized image display
			%--
			
			resized = get_resized_image(site, name, opt.image);
			
			url = [level_prefix(page.level), type, '/', resized];
			
			link = ['<img alt="', name,'" src=', url, '"', attr, '/>']; 
			
			if isfield(opt, 'short_caption')
				link = [link, '<p>', opt.short_caption, '</p>'];
			end
				
			if isfield(opt, 'license')
				link = append_license(link, opt);
			end
				
			return;
			
		end
		
		%--
		% zoomable thumb image display
		%--
		
		[thumb, ignore, info] = get_resized_image(site, name, opt.thumb);
		
		if isfield(opt, 'image')
			name = get_resized_image(site, name, opt.image);
		end
		
		%--
		% create zoom page
		%--
		
		% NOTE: the zoom page is named according to the resized image
		
		out = [fileparts(page_file(site, page)), filesep, name, '-zoom.html'];
		
		file_process(out, which('image_zoom_page.html'));
		
		data.image = name;
		
		data.short_caption = get_field(opt, 'short_caption', '');
		
		data.caption = get_field(opt, 'caption', '');
		
		if isfield(opt, 'license')
			data.license = append_license('', opt, 1);
		else
			data.license = '';
		end
		
		% NOTE: this is the width of the initial image
		
		data.width = info.Width;
		
		process_page_file(out, data);
		
		%--
		% link thumb image and then link to zoom page
		%--
		
		linki = asset_link(data, 'images', thumb);
		
		link = ['<a href="', name, '-zoom.html', '">', linki, '</a>'];
		
		if isfield(opt, 'short_caption')
			link = [link, '<p>', opt.short_caption, '</p>'];
		end
		
		if isfield(opt, 'license')
			link = append_license(link, opt);
		end
		
end



function value = is_url(str)

value = strmatch('http://', str);


%---------------------------
% APPEND_LICENSE
%---------------------------

function link = append_license(link, opt, full)

%--
% be paranoid
%--

if ~isfield(opt, 'license')
	return;
end

%--
% set full license display default
%--

if nargin < 3
	full = 0;
end

%--
% persistently cache license content
%--

% TODO: add others, in particular there are licenses for various media types?

persistent LICENSE;

if isempty(LICENSE);
	
	cc = file_readlines('cc_full.html'); LICENSE.cc_full = [cc{:}];
	
	cc = file_readlines('cc_small.html'); LICENSE.cc_small = [cc{:}];
	
end

%--
% parse license directive
%--

% NOTE: the first part if the license short name, second part author, last part optional contact email

part = str_split(opt.license, '::');

% TODO: consider adding subject line to message, append this '?subject=SUBJECT'

if numel(part) == 3
	author = ['<a href="mailto:', part{3}, '">', part{2}, '</a>'];
else
	author = part{2};
end

switch part{1}
	
	case 'cc'
		
		if full
			field = 'cc_full';
		else
			field = 'cc_small';
		end

		link = [link, '<p class="license byline">', strrep(LICENSE.(field), '$AUTHOR', author), '</p>'];
		
end
