function assets = get_assets(site, type, update)

% GET_ASSETS get site assets of given type
%
% assets = get_assets(site, type, update)
% 
% Input:
% ------
%  site - name
%  type - of asset
%  update - list
%
% Output:
% -------
%  assets - list

%-----------------
% HANDLE INPUT
%-----------------

%--
% set no update default
%--

if nargin < 3
	update = 0;
end

%--
% check site exists and asset type is known
%--

assets = {};

if ~exist_site(site) || ~is_asset_type(type)
	return;
end 

%-----------------
% GET ASSETS
%-----------------

%--
% try to get assets from cache
%--

persistent ASSETS_CACHE; 

if in_cache(ASSETS_CACHE, site, type) && ~update
	assets = ASSETS_CACHE.(site).(type); return;
end

%--
% get assets of given type from site
%--
	
% NOTE: get assets from site asset root according to asset extensions

source = assets_root(site, type); ext = get_asset_extensions(type);

content = get_extension_content(source, ext, '', 1);

if ~isempty(content)
	assets = {content.name}';
end

ASSETS_CACHE.(site).(type) = assets;


%-----------------
% IN_CACHE
%-----------------

function value = in_cache(cache, site, type)

% NOTE: check cache is not empty and has asset type cache for site

value = ~isempty(cache) && isfield(cache, site) && isfield(cache.(site), type);



