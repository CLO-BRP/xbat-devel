function ch = get_channels(C)

% get_channels - get channels in order from channel representation
% ----------------------------------------------------------------
%
% ch = get_channels(C)
%
% Input:
% ------
%  C - channel display matrix
%
% Output:
% -------
%  ch - displayed channels

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1730 $
% $Date: 2005-09-02 14:34:46 -0400 (Fri, 02 Sep 2005) $
%--------------------------------

%--
% get size and minimally check channel display matrix
%--

% NOTE: this is meant as a low level function so error checking will not be used

% [m,n] = size(C);
% 
% if (n ~= 2)
% 	disp(' ');
% 	error('Improper channel display matrix.');
% end
% 
% if (sum(C(:,2)) == 0)
% 	disp(' ');
% 	error('No channels displayed according to matrix.');
% end

%--
% get displayed channels from channel display matrix
%--

ch = C(find(C(:,2)),1); % get displayed channels list