function handles = selection_handles

% selection_handles - container struct for selection display handles
% ------------------------------------------------------------------
%
% handles = selection_handles
%
% Output:
% -------
%  handles - container struct for handles

%--
% callback object handle
%--

% NOTE: this is a duplicate of one of the display handles

handles.obj = [];

%--
% selection display handles
%--

handles.patch = [];

handles.control = [];

handles.grid.x = [];

handles.grid.y = []; 

handles.label.x = []; 

handles.label.y = [];
