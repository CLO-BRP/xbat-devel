function channel_control_strings(h,data)

% channel_control_strings - update channel control strings
% --------------------------------------------------------
%
% channel_control_strings(h,data)
%
% Input:
% ------
%  h - figure handle
%  data - figure userdata

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% the constraints based on the play channels should be reconsidered and
% removed, a simple rule can be devised to update the play channel to one
% of the remaining display channels

%--
% get channel control handles
%--

g = control_update(h,'Page','Channels');

if (~isempty(g))
	
	%--
	% create channel strings from channel matrix
	%--
	
	[L,value] = channel_strings(data.browser.channels);
	
	%--
	% update strings to contain play information
	%--
		
	% display of play channel information in the listbox control is not
	% consistent with the current approach of reassiging the play channels
	
% 	chp = data.browser.play.channel;
% 	
% 	if (~diff(chp))
% 		ix = find(data.browser.channels(:,1) == chp(1));
% 		L{ix} = [L{ix} '  (LR)'];
% 	else
% 		ix = find(data.browser.channels(:,1) == chp(1));
% 		L{ix} = [L{ix} '  (L)'];
% 		ix = find(data.browser.channels(:,1) == chp(2));
% 		L{ix} = [L{ix} '  (R)'];
% 	end
	
	%--
	% update strings and value of listbox control
	%--
		
	set(findobj(g,'style','listbox'), ...
		'string',L, ...
		'value',value ...
	);

	%--
	% disable cancel and apply buttons
	%--

	set(findobj(g,'style','pushbutton'),'enable','off');
	
end