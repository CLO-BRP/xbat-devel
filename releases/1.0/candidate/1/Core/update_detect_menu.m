function par = update_detect_menu

% update_detect_menu - update detect menus for open browsers
% ----------------------------------------------------------
%
% update_detect_menu
%
% Output:
% -------
%  par - updated browser handles

%--
% re-create detect menus for open browsers
%--

par = get_xbat_figs('type', 'sound');

for k = 1:length(par)
	
	%--
	% find and delete existing menu
	%--
	
	menu = findobj(par(k), 'type', 'uimenu', 'label', 'Detect');
	
	% NOTE: we don't re-create the menu if is was not available
	
	if isempty(menu)
		continue;
	end
	
	% NOTE: we delete the children then the parent
	
	try
		delete(allchild(menu)); delete(menu); 
	catch 
		nice_catch(lasterror, 'Failed to delete all previous menus.');
	end
		
	%--
	% re-create menu updating detect stores
	%--

	browser_detect_menu(par(k));
	
end
