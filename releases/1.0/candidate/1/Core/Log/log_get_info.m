function [info, tags] = log_get_info(log, search, order)

% log_get_info - generate SQL and query log cache files for info
% --------------------------------------------------------------
%
% [info, tags] = log_get_info(log, search, order)
%
% Input:
% ------
%  log - log or logs
%  search - search string
%  order - top sorting order
%
% Output:
% -------
%  info - the log info in a cell array of strings
%  tags - all of the extant tags

% TODO: consider how to simplify the sorting distinction between 'name' and not

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default score order and select all 
%--

if nargin < 3
	order = 'score'; 
end 

if nargin < 2
	search = '';
end

%--
% test older code
%--

% NOTE: it might be interesting to check the old code, the new code can do more however

% try
% 	aqwerq, [info, tags] = fast_log_get_info(log, search, order); return; 
% catch
% 	nice_catch(lasterror);
% end

%--------------------------
% GET TAGS
%--------------------------

% NOTE: this is simple and independent of the rest, why is this here?

if nargout > 1
	
	switch numel(log)
		
		case 0, tags = {};
			
		case 1, tags = get_log_tags(log);
			
		otherwise, tags = aggregate(get_log_tags(log), 1);
			
	end

end

%--------------------------
% GET INFO
%--------------------------

%--
% get relevant events from each log
%--

sql = get_search_sql(search);

selected = cell(size(log));

for k = numel(log):-1:1
	
	% NOTE: get events without extension data
	
	part = log_get_events(log(k), [], 0);
	
% 	part = sqlite(log_cache_file(log(k)), 'prepared', sql);

	if ~isstruct(part) || isempty(part)
		log(k) = []; selected(k) = [];
	else
		selected{k} = part;
	end
	
end

if isempty(log)
	info = {}; return;
end

%--
% order events as requested
%--

% NOTE: if we need to order by name, we first order the logs

if strcmp(order, 'name')
	[ignore, ix] = sort(log_name(log)); log = log(ix);
end

[field, column] = get_order_description(order);

data = [];

for k = 1:numel(log)

	part = zeros(numel(selected{k}), numel(field));

	% NOTE: 'struct_field' will return nan when the field value is empty! this is good

	for j = 1:numel(field)
		
		try
			value = struct_field(selected{k}, field{j});
		catch
			db_disp('exception'); value = {selected{k}.(field{j})};
		end
		
		if strcmp(field{j}, 'time')
			value = value(:, 1);
		end
		
		part(:, j) = value;
		
	end

	% NOTE: for the sake of ordering we replace nan values with zero

	part(isnan(part)) = 0;

	if strcmp(order, 'name')
		ix{k} = sortrows(part, column);
	else
		data = [data; part];
	end
	
end

% NOTE: we only sort the larger collection when 'name' does not dominate

if ~strcmp(order, 'name')
	
	if size(data, 1) > 1
		[ignore, ix] = sortrows(data, column);
	else
		ix = 1;
	end

end
	
%--
% produce info strings
%--

info = {};

for k = 1:numel(log)
	
	% TODO: the time should be the time as described by the grid time, did the other approach solve this? 
	
	score = strcat(iterate(@num2str, 100 * struct_field(selected{k}, 'score'), 3), {'%'});
	
% 	rating = iterate(@str_line, struct_field(selected{k}, 'rating'), '*');
	
	channel = int_to_str(struct_field(selected{k}, 'channel'));
	
	time = struct_field(selected{k}, 'time'); time = sec_to_clock(time(:, 1)); 
	
	part = strcat( ...
		{log_name(log(k))}, {' # '}, int_to_str([selected{k}.id])', {': '}, score, {' '}, ... % rating, ...
		{', Ch = '}, channel, ...
		{', T = '}, time ...
	);

	% NOTE: here we order the log event blocks, below we skip the full permutation
	
	if strcmp(order, 'name')
		part = part(ix{k});
	end
	
	info = {info{:}, part{:}};
	
end

if ~strcmp(order, 'name')
	info = info(ix);
end


%---------------------------------
% GET_ORDER_DESCRIPTION
%---------------------------------

function [field, column] = get_order_description(order)

%--
% set fields to order and direction
%--

% NOTE: we have removed the 'name' field for now, since the above code is not as simple or efficient in that case

persistent FIELD COLUMN

if isempty(FIELD)
	FIELD = {'time', 'score', 'channel', 'id'}; COLUMN = [1, -1, 1, 1];
end

% NOTE: the goal of this function is to prioritize the provided order field

ix = strmatch(order, FIELD); 

if isempty(ix)
	field = FIELD; column = COLUMN; return;
end

field = FIELD; field(ix) = []; field = {FIELD{ix}, field{:}}; 

column = COLUMN; column(ix) = []; column = [COLUMN(ix), column]; column = column .* (1:4);


%---------------------------------
% GET_SEARCH_SQL
%---------------------------------

function sql = get_search_sql(search)

%--
% build select string
%--

output = str_implode(search_fields, ', ');

sql = ['SELECT ', output, ' FROM event'];

%--
% parse search string to build filter string
%--

clause = parse_search(search);

% NOTE: when there is no filter finalize command and output as string

if isempty(clause)
	sql = [sql, ';']; return;
end

% TODO: this line will change if we allow other logical combinations

filter = str_implode({clause.string}, ' AND ');

sql = [sql, ' WHERE (', filter, ');' ];


%---------------------------------
% PARSE_SEARCH
%---------------------------------

function clause = parse_search(search)

% NOTE: the goal here is to support a query language that allows simple clause AND composition

%--
% handle input
%--

clause = [];

if isempty(search)
	return;
end

%--
% parse search string
%--

% TODO: consider extending this to support AND, OR, and perhaps NOT

part = str_split(search, ',');

for k = 1:numel(part)
	
	current = parse_clause(part{k});
	
	if ~isempty(current)
		
		if isempty(clause)
			clause = current;
		else
			clause(end + 1) = current;
		end
		
	end

end


%---------------------------------
% PARSE_CLAUSE
%---------------------------------

function clause = parse_clause(str)

% NOTE: a parsed clause string is normalized in this function

%--
% declare known relations
%--

% NOTE: the ordering ensures we check for the complex relation first, resolving possible ambiguity

relation = {'<=', '=>', '<', '>', '='};

%--
% try to parse known relations
%--

for k = 1:numel(relation)
	
	%--
	% split clause using relation character
	%--
	
	part = str_split(str, relation{k});

	% NOTE: split did not work, relation does not apply here
	
	if numel(part) ~= 2
		continue;
	end
	
	%--
	% check clause elements and produce parsed clause
	%--
	
	% NOTE: the atomic clause consists of field name relation and test value
	
	value = num2str(part{2});
	
	if isempty(value)
		continue;
	end
	
	clause.field = get_matching_search_field(part{1});
	
	clause.relation = relation{k}; clause.value = value;

	clause.string = [clause.field, ' ', relation{k}, ' ', part{2}];
	
	return;
	
end

%--
% consider non-clause elements as text probes for tags and notes
%--

% NOTE: a string without the relation characters is assumed part of tags OR notes

clause.field = {'tags', 'notes'}; clause.relation = 'like'; clause.value = part{1};

clause.string = ['(', str_implode(strcat(clause.field, ' LIKE ''%', clause.value, '%'''), ' OR '), ')'];


%---------------------------------
% GET_MATCHING_SEARCH_FIELD
%---------------------------------

function field = get_matching_search_field(str)

fields = search_fields;

ix = strmatch(str, fields);

if isempty(ix)
	field = ''; 
else
	field = fields{ix};
end


%---------------------------------
% SEARCH_FIELDS 
%---------------------------------

function fields = search_fields

% NOTE: the 'tags' field is handled in a separate way, although it does not appear here

% NOTE: conveniently each field starts with a different character, field matching then allows for concise expression

persistent FIELDS

if isempty(FIELDS)
	FIELDS = {'id', 'channel', 'start', 'low', 'duration', 'high'};
end

fields = FIELDS;



%--------------------------------------------------------------------

% NOTE: the code below may be faster, but is currently mysteriously failing in various installations

%--------------------------------------------------------------------



function [info, tags] = fast_log_get_info(log, search, order)

if nargin < 3
	order = [];
end

if nargin < 2
	search = [];
end

file = [tempname, '.db'];

sql_attach = {}; sql_bigtable = {}; sql_tags = {};

for k = 1:length(log)

	% TODO: make sure this is a proper identifier, so we can have spaces in log names

	name = log_name(log(k));

	db_name = sqlite_database_name(name);

	%--
	% attach database for this log
	%--

	sql_attach{end + 1} = ['ATTACH DATABASE ''', log_cache_file(log(k)), ''' AS ''', db_name, '''; '];

	%--
	% build union select statement
	%--

	sql_bigtable{end + 1} = ['SELECT "', name, '" name,* FROM ', db_name, '.events'];

	if ~isempty(search)
		sql_bigtable{end} = [sql_bigtable{end}, ' ', search_str(name, search)];
	end

	if k < length(log)
		sql_bigtable{end} = [sql_bigtable{end}, ' UNION ALL '];
	end

	%--
	% build union tags statement
	%--

	sql_tags{end + 1} = ['SELECT tags FROM ', db_name, '.tags'];

	if k < length(log)
		sql_tags{end} = [sql_tags{end}, ' UNION '];
	end

end

sql_bigtable = ['(', sql_bigtable{:}, ')'];

%--
% get info strings
%--

sql = { ...
	sql_attach{:}, ...
	select_str, ...
	sql_bigtable ...
};

if nargin > 2 && ~isempty(order)
	sql{end + 1} = [' ORDER BY ', get_order_str(order)];
end

sql{end + 1} = ';';

[status, info] = sqlite(file, 'exec', sql);

%--
% get tags
%--

sql = {sql_attach{:}, sql_tags{:}, ' ORDER BY tags;'};

[status, tags] = sqlite(file, 'exec', sql);

delete(file);

% TODO: consider storing complex SQL to text files an loading them into persistent variables


%--------------------------
% SELECT_STR
%--------------------------

function str = select_str

str = [ ...
    'SELECT(', ...
    'name || '' # '' || id || '': '' || ', ...
    'IFNULL(((round(score*1000)/10) || ''% ''), '''') || ', ...
    'IFNULL((substr(''*****'', 0, rating) || '' ''),'''') || ', ...
    '''Ch = '' || channel || '', '' || ', ...
    '''T = '' || time((time/86400)-0.5)) ', ...
    'info FROM ' ...
];


%--------------------------
% SEARCH_STR
%--------------------------

function str = search_str(name, search)

str = 'WHERE ';

[tok, r] = strtok(search); 

while ~isempty(tok)
    
    str = [str, '(''', name, ' # '' || id || '': '' || IFNULL (tags, '''') || IFNULL (notes, '''') || IFNULL ((round(score*1000)/10 || ''%''), '''') LIKE ''%', tok, '%'''];

    [tok, r] = strtok(r); %#ok<STTOK>
    
    if ~isempty(tok) 
        str = [str, ' AND '];      
    end
    
end


%--------------------------
% GET_ORDER_STR
%--------------------------

function order_str = get_order_str(order)

persistent ORDER;

if isempty(ORDER)
    ORDER  = {'time ASC', 'score DESC', 'rating DESC', 'channel ASC', 'name ASC, id ASC'};
end

ix = strmatch(order, ORDER);

if ix == 1
    
    
    % NOTE: switch direction of top-level order
%     return;
    
%     if regexp(ORDER{1}, 'ASC$')
%         ORDER{1} = [strtok(ORDER{1}), ' DESC'];
%     else
%         ORDER{1} = [strtok(ORDER{1}), ' ASC'];
%     end
    
else
    
    tmp = ORDER{ix};
    
    ORDER(ix) = [];
    
    ORDER = {tmp, ORDER{:}};
    
end

order_str = '';

for k = 1:length(ORDER)
    
    order_str = [order_str, ORDER{k}];
    
    if k < length(ORDER)
        order_str(end + 1) = ',';
    end
    
end

