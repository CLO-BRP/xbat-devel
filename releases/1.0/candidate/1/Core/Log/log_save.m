function result = log_save(log, event)

% log_save - save log to file
% ---------------------------
%
% result = log_save(log, event)
%
% Input:
% ------
%  log - log
%  event - event
%
% Output:
% -------
%  result - result

%----------------------
% SAVE CONNECTION
%----------------------

%--
% update modification date and save to file
%--

log.modified = now;

% NOTE: it is not clear which is a better error message, consider truly handling exception

try
	save(log.file, 'log'); 
catch
	nice_catch(lasterror, 'Failed to save log file.'); rethrow(lasterror);
end

if nargin < 2
	result = log; return;
end 

%----------------------
% SAVE EVENTS
%----------------------

% TODO: check whether and when we need this

%--
% get log format extension
%--

format = get_log_format(log);

%--
% save events using format
%--

try
	result = format.fun.event.save(log, event);
catch
	extension_warning(format, 'Failed to save events.', lasterror);
end


