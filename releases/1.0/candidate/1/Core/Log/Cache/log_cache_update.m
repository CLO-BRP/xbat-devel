function log_cache_update(log, mode, input)

% log_cache_update - update log cache
% -----------------------------------
%
% log_cache_update(log, mode, input)

%--
% handle input
%--

% NOTE: the simple call behavior is to regenerate the cache database

if nargin < 2
	mode = 'update';
end

if nargin < 3
	input = [];
end

%--
% update log cache database
%--

file = log_cache_file(log);

switch mode

	case {'update', 'insert'}

		% NOTE: on 'update' or 'insert' the input is typically events
		
		% TODO: allow identifier input as well

		if isempty(input)
			event = log_get_events(log, [], 0); % NOTE: we only get basic event, not extension information
		else
			event = input;
		end

		if isempty(event)
			return;
		end

		% TODO: this can be slow for large event collections, we should page through
		
		% NOTE: we need to determine what large is
		
		update_event(file, event);

	case 'delete'

		% NOTE: on 'delete' the input is event identifiers

		id = input; delete_event(file, id);

	otherwise

		error('Unrecognized log cache update mode.');

end


%---------------------------
% UPDATE
%---------------------------

function update_event(file, event)

%--
% get basic events
%--

basic = get_table_event(event);

%--
% insert or replace events
%--

sql = cell(length(basic) + 2, 1);

sql{1} = 'BEGIN;';

for k = 1:length(basic)
	sql{k + 1} = ['INSERT OR REPLACE INTO event VALUES ', value_str(basic(k)), ';'];
end

sql{end} = 'COMMIT;';

% NOTE: it is possible that the cache schema changes, in this case delete the file it will be recreated

try
	sqlite(file, sql);
catch
	if exist(file, 'file')
		delete(file);
	end
end

%--
% update tags
%--

% TODO: make use of the SQLite log format code


%----------------------------
% DELETE
%----------------------------

function delete_event(file, id)

%--
% delete events
%--

% TODO: this seems faster than the general 'str_implode', consider factoring

id = sprintf('%d, ', id); id(end - 1:end) = [];

sqlite(file, ['DELETE FROM event WHERE event.id IN (', id, ');']);

%--
% delete tag relations
%--

% NOTE: we don't need to delete tags, we can do this ocasionally













