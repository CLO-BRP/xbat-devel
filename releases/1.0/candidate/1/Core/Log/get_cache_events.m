function event = get_cache_events(log, id)

% get_cache_events - using identifiers
% ------------------------------------
%
% event = get_cache_events(log, id)
%
% Input:
% ------
%  log - source
%  id - for event
%
% Output:
% -------
%  event - from cache

%--
% select all events
%--

if nargin < 2
	id = [];
end

%--
% build query
%--

sql = 'SELECT * FROM event;'; 

if ~isempty(id)
	sql = [sql(1:end - 1), ' WHERE id IN (', str_implode(id, ', ', @int2str), ');'];
end

%--
% get events from cache
%--

[status, event] = sqlite(log_cache_file(log), sql);