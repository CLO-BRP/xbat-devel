function [event, tag, ix] = get_events_by_tag(log, tags)

% get_events_by_tag - select events from logs by tags
% ---------------------------------------------------
%
% [id, tag, log] = get_events_by_tags(logs, tags)
%
% Input:
% ------
%  logs - logs
%  tags - tags
%
% Output:
% -------
%  events - events
%  tag - tag indicator matrix
%  log - log index array

% NOTE: the tag indicator matrix is binary with event row index and tag column index

% WARNING: the above is not actually true, to make it true call 'to_mat' on the 'tag' output

%--
% handle multiple logs with iterator
%--

% TODO: develop an 'iterate_with_index' function that can assist us in keeping track if index

persistent LOG_IX;

if numel(log) > 1
	LOG_IX = 1; [event, tag, ix] = iterate(mfilename, log, tags); LOG_IX = []; return;
end

%--
% make sure tags is a cell array
%--

if ischar(tags)
    tags = {tags};
end

%--
% build sql for getting ids and indicator table
%--

like = {}; ind = {};

for k = 1:numel(tags)   
    like{k} = ['(events.tags LIKE ''%', tags{k}, '%'')']; ind{k} = [like{k}, ' ', tags{k}];      
end

sql = [ ...
    'SELECT id,', str_implode(ind, ', '), ' FROM events WHERE ', str_implode(like, ' OR '), ';' ...
];

%--
% get stuff from cache
%--

[status, result] = sqlite(log_cache_file(log), 'prepared', sql);

if isempty(result)
    event = empty(event_create); tag = []; ix = []; return;
end

%--
% get events from id column and use the rest as the indicator table
%--

id = [result.id];

event = log_get_events(log, id, 0);

if isempty(event)
    tag = []; ix = []; return;
end

tag = rmfield(result, 'id');

% NOTE: this helps us keep track of the index output while we 'iterate'

ix = ones(size(event));

if isempty(LOG_IX)
    return;
end

ix = LOG_IX * ix;

LOG_IX = LOG_IX + 1;






