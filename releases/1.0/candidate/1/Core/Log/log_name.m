function name = log_name(log, lib, display) 

% log_name - get name of log
% --------------------------
%
% name = log_name(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  name - name of log

%----------------------
% HANDLE INPUT
%----------------------

% TODO: add 'is_log' check

if nargin < 3
    display = 0;
end

if nargin < 2
    lib = [];
end

%--
% handle multiple and empty log input
%--

if numel(log) > 1
	name = iterate(mfilename, log, lib, display); return;
end

if isempty(log)
	name = ''; return;
end

%----------------------
% GET NAME
%----------------------

% NOTE: temporary and dummy logs may have no file

no_file = ~isfield(log, 'file') || isempty(log.file);

if no_file
	name = log.name;
else
	[ignore, name] = fileparts(fileparts(log.file));
end

% NOTE: the display flag prefixes the name with the sound name

if display
    name = [sound_name(log.sound), filesep, name];
end

% NOTE: when we don't have a file, we also don't belong to a library

if no_file
	return; 
end

% NOTE: when library input is also provided we further prefix with the library name

if ~isempty(lib)
    name = [get_library_name(lib), filesep, name];
end
