function str = latex_escape(str)

% latex_escape - special characters
% ---------------------------------
% 
% str = latex_escape(str)
%
% Input:
% ------
%  str - input
%
% Output:
% -------
%  str - escaped
%
% NOTE: based on XSL from http://db2latex.sourceforge.net/reference/rn42re484.html

if iscellstr(str)
	for k = 1:numel(str) 
		str{k} = latex_escape(str{k});
	end
	
	return;
end

% BASIC

pat = {'#', '_', '$', '%'}; rep = strcat('\', pat);

for k = 1:numel(pat)
	str = strrep(str, pat{k}, rep{k});
end

% str = strrep(str, '{' , '\{');
% str = strrep(str, '}' , '\}');


