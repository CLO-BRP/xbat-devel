function units = get_type_units(type)

% NOTE: 'part' is another unit, but it is not part of a typical hierarchy

units = {'chapter', 'section', 'subsection', 'subsubsection', 'paragraph', 'subparagraph'};

switch type
	case 'article'
		units = units(2:end);
		
	case {'report', 'book'}

	otherwise
		error(['Unrecognized document type ''', type, '''.']);
		
end