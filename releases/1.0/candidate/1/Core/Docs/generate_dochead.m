function lines = generate_dochead(title, author, email)

% generate_dochead - lines injecting title and author info
% --------------------------------------------------------
%
% lines = generate_dochead(title, author, email)
%
% Input:
% ------
%  title - for doc
%  author - of doc
%  email - for author
%
% Output:
% -------
%  lines - with values injected

%--
% handle input
%--

if nargin < 3
	email = '';
end

if nargin < 2 || isempty(author)
	author = '';
end

if nargin < 1 || isempty(title)
	error('Title is required input.');
end

%--
% load and inject lines
%--

persistent HEAD;

if isempty(HEAD)
	HEAD = file_readlines(fullfile(fileparts(mfilename('fullpath')), 'dochead.tex'));
end

lines = HEAD;

% NOTE: this depends on the content of 'dochead.tex'

for k = 1:numel(lines)
	if strmatch('%% title =', lines{k})
		lines{k} = ['%% title = ', title]; continue;
	end
	
	if ~isempty(author) && ~isempty(strmatch('%% author =', lines{k}))
		lines{k} = ['%% author = ', author]; continue;
	end
	
	if ~isempty(author) && ~isempty(strmatch('%% email =', lines{k}))
		lines{k} = ['%% email = ', email]; continue;
	end
end

%--
% display if output not captured
%--

if ~nargout
	disp(' '); iterate(@disp, lines); disp(' '); clear lines;
end
