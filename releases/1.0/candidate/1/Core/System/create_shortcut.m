function [status, result, str] = create_shortcut(target, par, link)

% create_shortcut - create a windows shortcut
% -------------------------------------------
%
% [status, result, str] = create_shortcut(target, par, link)
%
% Input:
% ------
%  target - target of shortcut
%  par - location of shortcut (def: windows desktop)
%  link - name of shortcut (def: same as target)
%
% Output:
% -------
%  status - system call status output
%  results - system call results output
%  str - command string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%------------------------------------------------------------
% HANDLE INPUT
%------------------------------------------------------------

%--
% set default name same as target
%--

if (nargin < 3) || isempty(link)
	[ignore, link] = path_parts(target);	
end

%--
% set default directory
%--

if (nargin < 2) || isempty(par)
	par = '~$folder.desktop$';
end

%------------------------------------------------------------
% BUILD AND EXECUTE COMMAND
%------------------------------------------------------------

if isunix
    system(['ln -s ', target, ' ', par]); return;
end

% TODO: move this to use the UnxUtils 'ln.exe' rather than NirCmd

% NOTE: remember that NirCmd supports other features such as hiding the desktop

%--
% get tool file
%--

tool = nircmd;

if isempty(tool)
    return;
end

%--
% create command string
%--

% NOTE: there are a series of functions with this pattern

str = ['"', tool.file , '" shortcut "', target, '" "', par, '" "', link, '"'];

[status, result] = system(str);

% NOTE: this is a little filesystem assertion to save us from possible threading problems

while ~exist(fullfile(par, [link, '.lnk']), 'file')
	
	disp('Waiting on create shortcut.'); 
	
	pause(0.1);
	
end