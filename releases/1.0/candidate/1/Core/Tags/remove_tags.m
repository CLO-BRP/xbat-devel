function obj = remove_tags(varargin)

% remove_tags - remove tags from tagged objects
% -------------------------------------------------
%
% obj = remove_tags(obj, tags)
%
% Input:
% ------
%  obj - tagged objects
%  tags - tags to subtract
%
% Output:
% -------
%  obj - updated tagged objects
%
% NOTE: this is the same as 'subtract_tags'

obj = update_tags('subtract', varargin{:});