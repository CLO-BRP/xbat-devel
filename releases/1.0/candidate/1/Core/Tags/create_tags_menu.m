function handles = create_tags_menu(top, tags, known, callback)

% create_tags_menu - create a menu for tag display and editing
% ------------------------------------------------------------
%
% handles = create_tags_menu(top, tags, known, callback)
%
% Input:
% ------
%  top - parent
%  tags - current tags
%  known - known tags
%  callback - callback

% TODO: consider the conventional asterisk used for labels

% TODO: make the various sections of this menu optional, 'tags', 'apply', and 'remove'

% TODO: add an 'Edit Tags ...' menu item

%---------------
% HANDLE INPUT
%---------------

if nargin < 4
	callback = @tag_edit_trace;
end

if nargin < 3
	known = {};
end

if nargin < 2
	tags = {};
end 

%---------------
% SETUP
%---------------

% NOTE: relevant tags to add are tags known but not currently used

no_known = isempty(known);

if ~no_known
	known = setdiff(known, tags);
end

%---------------
% CREATE MENU
%---------------

%--
% set parent callback
%--

% TODO: this callback should check if the known tags have changed, and update if so

% NOTE: this function must access the database

set(top, 'callback', []);

%--
% update tag menus
%--

% TAGS

append_menu(top, '(Tags)', [], 1); % NOTE: here we initialize the append position

if isempty(tags)
	
	append_menu(top, 'No Tags');
	
else
	
	handle = zeros(numel(tags), 1);
	
	for k = 1:numel(tags)
		handle(k) = append_menu(top, tags{k}, 'label');
	end
	
	set(handle, 'callback', callback); 
	
end

% APPLY TAG

append_menu(top, '(Apply)');
	
if isempty(known)
	
	append_menu(top, ['No ', ternary(no_known, '', 'Relevant '), 'Known Tags']);
	
else
	
	handle = zeros(numel(known), 1);
	
	for k = 1:numel(known)
		handle(k) = append_menu(top, known{k}, 'apply');
	end
	
	set(handle, 'callback', callback); 
	
end
	
% REMOVE TAG

append_menu(top, '(Remove)');
	
if isempty(tags)
	
	append_menu(top, 'No Tags To Remove');
	
else
	
	for k = 1:numel(tags)
		handle(k) = append_menu(top, tags{k}, 'remove'); 		
	end
	
	set(handle, 'callback', callback);
	
end

%--
% remove excess menus and set further menu properties 
%--

last = append_menu; handles = get(top, 'children'); pos = cell2mat(get(handles, 'position'));

remove = handles(pos > last); handles(pos > last) = []; delete(remove);

% NOTE: dim menus with parenthetical labels

parenthetical_menus(get(top, 'children'));


%------------------------------
% APPEND_MENU 
%------------------------------

% TODO: factor this to 'Util'

function [handle, created] = append_menu(par, label, action, pos)

%--
% create persistent store for append position 
%--

persistent LAST_POSITION;

if isempty(LAST_POSITION)
	LAST_POSITION = 1; 
end

% NOTE: output last position when called with no input

if ~nargin
	handle = LAST_POSITION; return;
end

%--
% get current append position, from input or using last position
%--

if nargin < 4
	pos = LAST_POSITION + 1;
end

LAST_POSITION = pos;

%--
% set null action 
%--

if nargin < 3
	action = 'null';
end 

%--
% update menu
%--

% NOTE: a unique object tag is produced from the action and label

[handle, created] = create_menu(par, [action, '::', label], 'label', label, 'position', pos);


%------------------------------
% TAG_EDIT_TRACE
%------------------------------

function tag_edit_trace(obj, eventdata)

disp(get(obj, 'tag'));

