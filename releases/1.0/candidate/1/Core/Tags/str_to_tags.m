function tags = str_to_tags(str)

% str_to_tags - convert string to tags
% ------------------------------------
%
% tags = str_to_tags(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  tags - tags in string

%--
% check for string input
%--

if ~ischar(str)
	error('String input is required.');
end

%--
% parse string to cell array
%--

tags = strread(str, '%s', -1, 'delimiter', ' ');

% NOTE: return input string when parse to valid tags fails, does this ever happen?

if ~is_tags(tags)
	tags = str;
end

% NOTE: this is the way the tags will be stored, consider using 'update_tags'

if iscell(tags) 
	tags = unique(tags(:));
end
