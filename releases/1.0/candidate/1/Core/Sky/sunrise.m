function time = sunrise(varargin)

% time = sunrise(lon, lat, cal, zen)
%
% Input:
% ------
%  lon - longitude
%  lat - latitude
%  cal - date
%  zen - zenith or 'official', 'civil', 'nautical', or 'astronomical'
%
% Output:
% -------
%  time - of sunrise

time = sunbase('rise', varargin{:});

if ~nargout
	disp(' '); disp(sec_to_clock(3600 * time)); disp(' '); clear time;
end