function time = sunset(varargin)

% time = sunset(lon, lat, cal, zen)
%
% Input:
% ------
%  lon - longitude
%  lat - latitude
%  cal - date
%  zen - zenith or 'official', 'civil', 'nautical', or 'astronomical'
%
% Output:
% -------
%  time - of sunset

time = sunbase('set', varargin{:});

if ~nargout
	disp(' '); disp(sec_to_clock(3600 * time)); disp(' '); clear time;
end