function ix1 = get_closest_point(ix0, ix, data)

% NOTE: compute distances from base point to node points and get min

d = distance(get_points(ix0, data), get_points(ix, data));

[d1, ix1] = min(d); 

% NOTE: we need to dereference local index to get data index

ix1 = ix(ix1);