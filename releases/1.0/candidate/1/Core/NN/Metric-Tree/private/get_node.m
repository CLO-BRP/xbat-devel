function [node, ix] = get_node(tree, id)

ix = find([tree.node.id] == id);

if isempty(ix)
	node = [];
else
	node = tree.node(ix);
end