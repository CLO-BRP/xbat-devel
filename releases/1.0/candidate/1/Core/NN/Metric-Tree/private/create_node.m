function node = create_node(parent, pivot, points)

% create_node - create metric tree node

global tree

%--
% node content fields
%--

if isempty(tree)
	node.id = [];
else
	node.id = tree.id; tree.id = tree.id + 1;
end

% NOTE: the various point fields typically store point indices, not actual points

if nargin < 2
	node.pivot = [];
else
	node.pivot = pivot; 
end

if nargin < 3
	node.points = [];
else
	node.points = points;
end

% NOTE: these are essential cached statistics for the node, the center is a center

node.decision = [];

node.center = [];

node.radius = [];

% NOTE: this will contain various other fields of cached information about the points

node.cached = struct;

%--
% node family
%--

% NOTE: we store empty values instead of empty nodes

if nargin < 1
	node.parent = [];
else
	node.parent = parent;
end

node.child.left = [];

node.child.right = [];
