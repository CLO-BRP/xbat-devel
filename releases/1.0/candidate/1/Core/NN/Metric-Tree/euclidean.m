function D = euclidean(X, Y)

% euclidean - euclidean distance computation
% ------------------------------------------
%
% D = euclidean(X, Y)
%
% Input:
% ------
%  X - points
%  Y - points
%
% Output:
% -------
%  D - distances

% NOTE: this function should be optimized for large input

% TODO: factor out to compute for other exponents and mahalanobis

switch nargin
	
	case 1
		
		%--
		% get input size
		%--
		
		mx = size(X, 1);
		
		%--
		% initialize matrix and fill upper triangle, then symmetrize 
		%--
		
		D = zeros(mx);
		
		for k = 1:mx
			Z = X(k + 1:end, :) - repmat(X(k, :), mx - k, 1); 
			D(k, k + 1:end) = sqrt(sum(Z.^2, 2))';
		end
		
		D = D + D';
		
	case 2
		
		%--
		% get and check input matrix sizes
		%--

		[mx, nx] = size(X); [my, ny] = size(Y);

		if nx ~= ny
			error('Input matrices must have the same number of columns.');
		end

		%--
		% initialize matrix and fill one row at a time
		%--

		D = zeros(mx, my);

		for k = 1:mx
			Z = Y - repmat(X(k, :), my, 1); 
			D(k, :) = sqrt(sum(Z.^2, 2))';
		end

end






