function daemon = scrolling_daemon

% scrolling_daemon - timer that implements scrolling display
% ----------------------------------------------------------
%
% daemon = scrolling_daemon
%
% Output:
% -------
%  daemon - configured timer

% TODO: update to use 'get_time_slider' and 'set_time_slider'

%-------------------------------
% SETUP
%-------------------------------

name = 'XBAT Scrolling Daemon';

mode = 'fixedRate'; 

rate = 0.1; 

%-------------------------------
% GET SINGLETON TIMER
%-------------------------------

%--
% try to find timer
%--

daemon = timerfind('name', name);

if ~isempty(daemon)
	return;
end

%--
% create and configure timer if needed
%--
	
daemon = timer;

set(daemon, ...
	'name', name, ...
	'executionmode', mode, ...
	'busymode', 'drop', ...
	'period', rate, ...
	'timerfcn', @scroll_callback ...
);



