function out = specgram_compare(param, opt)

% specgram_compare - compare several spectrogram computation options with bar chart
% -------------------------------------------------------------------------
% 
% opt = specgram_compare()
% out = specgram_compare(param, opt)
%
% Input:
% ------
% param - spectrogram paramter structure
% opt   - configuration options, defaults generated
%
% Output:
% -------
% out - structure array of scalar performance metrics
% opt - default configurations structure
%

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 3512 $
% $Date: 2006-02-13 17:19:46 -0500 (Mon, 13 Feb 2006) $
%--------------------------------


%--
% configure options structure with defaults
%--

if (nargin < 2 || isempty(opt))
	
	opt.samples = 1000000;
	
	opt.rate = 44100;
	
	opt.speed_units = 'x_realtime';
	
	opt.compare = {'xbat_double', 'xbat_single', 'matlab_double'};
	
    if nargin > 0 && ~isstruct(param)
        out = opt; return;
    end
	
end

%--
% get default spectrogram parameters if none are specified
%--

if (nargin < 1 || isempty(param))
	
	param = specgram_parameter;
	
end

%--
% generate test signal
%--

X = generate_signal(opt);

%--
% get reference output
%--

B_ref = matlab_double(X, param);

%--
% verify that power spectrum is squared magnitude
%--

Bn = fast_specgram(X, [], 'norm', param);
Bp = fast_specgram(X, [], 'power', param);

disp('checking that power spectrum is correctly computed (i.e. that ''norm'' == sqrt(''power'')) ...');

d = Bn(:) - sqrt(Bp(:));

if any(Bn(:) - sqrt(Bp(:)))
    disp(['failed max difference = ', num2str(max(d))]);
else
    disp('passed.');
end

%--
% iterate over functions to compare
%--

measure_handle = str2func(opt.speed_units);

for func_name = opt.compare
	
	func_handle = str2func(func_name{1});
		
	%--
	% time spectrogram computation
	%--
	
	[B, t] = func_handle(X, param);
	
	out.(func_name{1}).speed = measure_handle(t, opt);
    
    try
        out.(func_name{1}).accuracy.rms = sqrt(mean((B(:) - B_ref(:)).^2));
        out.(func_name{1}).accuracy.max = max(abs(B(:) - B_ref(:)));
    catch
        disp('output isn''t the right size, can''t check for accuracy.');
    end
	
end

if nargout
    return
end

%--
% create bar plot using data specified in opt.compare
%--

speed = [];
max_error = [];
rms_error = [];

disp('checking error relative to MATLAB spectrogram(), ''norm'' mode');

for k = opt.compare
	
	speed = [speed ; out.(k{1}).speed];
    
    max_error = [max_error; out.(k{1}).accuracy.max];
    rms_error = [rms_error; out.(k{1}).accuracy.rms];
    
    disp([k{1}, ': max error = ', num2str(max_error(end)), ' rms error = ', num2str(rms_error(end))]);
	
end

disp(['eps = ', num2str(eps)]);

fig;

h = bar(speed, .4);

ax = gca;

set(ax, 'XTickLabel', title_caps(opt.compare));

ylabel_str = ['speed: (' title_caps(opt.speed_units) ')'];

if strcmp(opt.speed_units, 'x_realtime')
	
	ylabel_str = [ylabel_str ' (at ', num2str(opt.rate), ' Hz Sample Rate)'];
	
end

ylabel(ylabel_str);

xlabel('Computation Function');

title_str = {'Spectrogram Speed Comparison: ', ...
 	['FFT size = ', num2str(param.fft), ' samples'], ...
	['Overlap = ', num2str(floor(param.hop*param.fft)), ' samples'], ...
	['Window Length = ', num2str(floor(param.win_length*param.fft)), ' samples'] ...
};

title(title_str);




%-------------------------------------
% XBAT_SINGLE
%-------------------------------------

function [B, t] = xbat_single(X, param)

X = single(X);

[B, t] = xbat_double(X, param);

B = double(B);


%-------------------------------------
% XBAT_DOUBLE
%-------------------------------------

function [B, t] = xbat_double(X, param)

tic;

B = fast_specgram(X, [], 'norm', param);

t = toc;


%-------------------------------------
% MATLAB_DOUBLE
%-------------------------------------

function [B, t] = matlab_double(X, param)

%--
% extract parameters
%--

[B, freq, time, t] = matlab_specgram(X, [], 'norm', param);


%--------------------------------------
% MATLAB_SINGLE
%--------------------------------------

function [B, t] = matlab_single(X, param)

[B, t] = matlab_double(X, param);

	
%--------------------------------------
% XREALTIME
%--------------------------------------

function x = x_realtime(t, opt)

actual_time = opt.samples / opt.rate;

x = actual_time / t;


%--------------------------------------
% MSPS
%--------------------------------------

function x = msps(t, opt)

x = (opt.samples / t) / 10^6;


%--------------------------------------
% GENERATE_SIGNAL
%--------------------------------------

function x = generate_signal(opt)

x = randn(opt.samples, 1);
