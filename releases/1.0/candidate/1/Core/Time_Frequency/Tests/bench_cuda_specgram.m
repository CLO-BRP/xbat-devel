function result = bench_cuda_specgram(varargin)

% bench_cuda_specgram - benchmark CUDA specgram against FFTW
% ----------------------------------------------------------
% 
% bench_cuda_specgram(varargin)
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'exhaustive' - run extensive benchmark instead of fast benchmark
%  'single' - run with single precision data
%  'double' - run with double precision data
%  'verbose' - print status output
%  'silent' - print nothing
%
% Output:
% -------
% result - array of times and data rates


% NOTE: default is fast test, double data type, and silent output

fast = 1;
float = 0;
verbose = 0;

% Input signal

[X, r] = sound_file_read(which('LarkSparrow.flac')); % NOTE: this is an XBAT sample sound

% Parse arguments

for i = 1:length(varargin)
	switch varargin{i}
		
		case 'quick'
			fast = 1;
		case 'exhaustive'
			fast = 0;
		case 'single'
			float = 1;
		case 'double'
			float = 0;
		case 'silent'
			verbose = 0;
		case 'verbose'
			verbose = 1;
	end
end

% Convert data if needed

if float
	X = single(X);
	leps = eps('single');
else
	leps = eps('double');
end

% FFT sizes

if fast
	n = [512, 1044, 2048];
else
	n = better_fft_sizes(128, 4096, [2, 3, 5, 7, 11]);
end

% Overlaps

if fast
	overlap = [0.33, 0.5, 0.875];
else
	overlap = 0.5:0.05:0.975;
end

% Iteration count

% NOTE: Calculations repeated to account for intermittent events that effect timing

count = 10;

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
	
	warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
	level = get_cuda_capability;
	
	if strcmp(class(X), 'double') && level < 1.3
		warning('CUDA hardware capability not available, test not run.'); return;
	end
end

%--
% run tests
%--

parameter = fast_specgram;

% NOTE: for each transform size and overlap pair we run the MEX and CUDA
% code a 'count' number of times and collect these

samples = numel(X);

start = clock;

for i = 1:numel(n)
	
	if verbose
		fprintf('FFT size: %d\n', n(i));
	end
	
	for j = 1:numel(overlap)
		
		if verbose
			fprintf('%2.1f,', 100 * overlap(j));
		end
		
        h = hann(n(i));
        o = round(n(i) * overlap(j));
		output = 0;
        
		for k = 1:count
			tic; h0 = cuda_specgram(X, h, n(i), o, output); cuda_time(i,j,k) = toc / samples;
		end
		
		parameter.fft = n(i); parameter.win_length = n(i) / n(i);
		
		parameter.win_type = 'Hann'; parameter.hop = 1 - overlap(j); % NOTE: these do not appear explicitly above
		
        for k =1:count
            tic; h1 = fast_specgram(X, 1, 'norm', parameter); mex_time(i,j,k) = toc / samples;
		end
		
		if verbose
			fprintf('%1.2f  ', mex_time(i,j,k) / cuda_time(i,j,k));
		end
	end
	
	if verbose
		fprintf('\n\n');
	end
end

elapsed = etime(clock, start)

result.cuda_time = cuda_time;

result.mex_time = mex_time;

%--
% report results
%--

% NOTE: Graph all collected performance data
	
if verbose

	cuda_time = mean(cuda_time, 3); mex_time = mean(mex_time, 3);
	
	speedup = mex_time ./ cuda_time;
	
	fig; handle = plot(n, speedup); title SPECGRAM; 
	
	set(handle, 'linewidth', 2);
	
	for k = 1:numel(handle)
		temp = text(n(end), speedup(end, k), num2str(overlap(k)));
		
		set(temp, 'color', get(handle(k), 'color'), 'fontweight', 'bold');
	end
	
	% Display results of last specgram configuration
	
	fig; imagesc(log(h0 + 1)); title CUDA; colormap(jet);
	
	fig; imagesc(log(h1 + 1)); title MEX; colormap(jet);
	
end
 	
