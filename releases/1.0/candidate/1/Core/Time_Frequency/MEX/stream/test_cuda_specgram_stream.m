function result = test_cuda_specgram(X, n, w, overlap)

% test_cuda_specgram - for correctness and speed
% ----------------------------------------------
% 
% test_cuda_specgram(X, n, w, overlap)
%
% Input:
% ------
%  X - input (default rand(1024) )
%  n - size of FFT (default 256)
%  w - size of window (default 256)
%  overlap - as fraction of transform size (default: 0)

%--
% handle input
%--

if ~nargin
	X = sound_file_read(which('LarkSparrow.flac')); % NOTE: this is an XBAT sample sound
end

X = single(X);

if nargin < 2
	n = better_fft_sizes(128, 2048, [2, 3, 5, 7]);
end 

if nargin < 3
	w = n;
end

if nargin < 4
	overlap = 0:0.125:0.875;
end

count = 5;

%--
% run tests
%--

fig;

parameter = fast_specgram;

% NOTE: for each transform size and overlap pair we run the MEX and CUDA
% code a 'count' number of times and collect these

start = clock;

for i = 1:numel(n)
	
	disp(int2str(n(i)));
	
	for j = 1:numel(overlap)
		
		fprintf('%2.1f,', 100 * overlap(j));
		
		for k = 1:count
			tic; h0 = cuda_specgram_stream_mex(X, n(i), w(i), round(n(i) * overlap(j))); cuda_time(i,j,k) = toc;
		end
		
		parameter.fft = n(i); parameter.win_length = w(i) / n(i);
		
		parameter.win_type = 'box'; parameter.hop = 1 - overlap(j); % NOTE: these do not appear explicitly above
		
		for k = 1:count
			tic; h1 = fast_specgram(X, 1, 'norm', parameter); mex_time(i,j,k) = toc;
		end
				
% 		hist(vec(h0 - h1), 51); drawnow;
		
% 		imagesc(h0 - h1); colorbar; drawnow;
		
		fprintf('%1.2f  ', mex_time(i,j,k) / cuda_time(i,j,k));
	end
	
	fprintf('\n\n');
end

elapsed = etime(clock, start)

%--
% report results
%--

% NOTE: output all collected performance data

result.cuda_time = cuda_time; result.mex_time = mex_time;

cuda_time = mean(cuda_time, 3); mex_time = mean(mex_time, 3);

speedup = mex_time ./ cuda_time;

fig; pcolor(speedup); colorbar;

% fig; imagesc(log(h0 + 1)); title CUDA; colormap(jet);
% 
% fig; imagesc(log(h1 + 1)); title MEX; colormap(jet);
% 
% disp('   Mex Time  CUDA Time Speedup');
% disp([mexEnd', cudaEnd', mexEnd./cudaEnd']);
	
