function [files, destination] = build

%--
% clear MEX files so that we can overwrite them
%--

clear fast_specgram_mex_double;

clear fast_specgram_mex_single;

%--
% build single and double precision DLL
%--

opt = build_mex;

opt.cflags = '-DFLOAT'; opt.outname = 'fast_specgram_mex_single';

build_mex( ...
    '../fast_specgram_mex.c', ...
    '-lfftw3f', ...
    opt ...
);

opt.cflags = ''; opt.outname = 'fast_specgram_mex_double';

build_mex( ...
    '../fast_specgram_mex.c', ...
    '-lfftw3', ...
    opt ...
);

%--
% move files to private
%--

movefile(['*.', mexext], '../../private');

%--
% report created files and destination
%--

% TODO: the destination is redundant, it should not be needed as output

par = fileparts(fileparts(pwd));

destination = [par, filesep, 'private'];

% NOTE: the files to consider are files after they've been installed

files = { ...
	[destination, filesep, 'fast_specgram_mex_double.', mexext], ...
	[destination, filesep, 'fast_specgram_mex_single.', mexext] ...
};



