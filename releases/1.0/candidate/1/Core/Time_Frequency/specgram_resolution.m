function [time, freq] = specgram_resolution(parameter, rate, pack)

% specgram_resolution - spectrogram time and frequency resolution
% ---------------------------------------------------------------
%
% [time, freq] = specgram_resolution(parameter, rate, pack)
%
% Input:
% ------
%  parameter - for spectrogram
%  rate - of signal
%  pack - resolution struct
%
% Output:
% -------
%  time - resolution
%  freq - resolution
%
% NOTE: this computes based on the 'fast_specgram' parameter struct

%--
% handle input
%--

if nargin < 3
	pack = 0;
end

% NOTE: the following compuations rely on the 'fft', 'hop', and 'sum_length' fields

%--
% time frequency resolution
%--

% NOTE: the hop is expressed as a fraction of the transform length

% TODO: why are we not simply computing the hop 'samples' from the 'hop' parameter?

overlap = round(parameter.fft * (1 - parameter.hop));

samples = parameter.fft - overlap;

time = (samples * parameter.sum_length) / rate;

if (nargout < 2) && ~pack
	return;
end

% NOTE: this may not be exact

bins = floor(parameter.fft / 2) + 1;

freq = 0.5 * rate / (bins - 1);

% freq = rate / parameter.fft;

%--
% pack if needed
%--

if pack
	out.time = time; out.freq = freq; time = out;
end
