function [B, A, Z, t] = saber(X, alpha, start)

% saber - simple adaptive background estimator
% --------------------------------------------
%
% [B, A, Z] = saber(X, alpha, start)
%
% Input:
% ------
%  X - column sequence matrix
%  alpha - mixing constant
%  start - starting background estimate
%
% Output:
% -------
%  B - adaptive background estimate
%  A - average background estimate
%  Z - background indicator
%  t - background threshold

% TODO: consider if we want a single slice offset in the computation

%--------------------
% HANDLE INPUT
%--------------------

%--
% set no initial state for background
%--

if nargin < 3
	start = [];
end

%--------------------
% COMPUTE
%--------------------

%--
% compute simple background estimate
%--

A = raber(X, alpha, start);

n = size(X, 2); A = zeros(size(X));

if ~isempty(start)
	A(:, 1) = (1 - alpha) * start + alpha * X(:, 1);
end

for k = 2:n
	A(:, k) = (1 - alpha) * A(:, k - 1) + alpha * X(:, k);
end
	
%--
% compute adaptive background estimate
%--

if parameter.adaptive
	
	%--
	% compute signal mask using simple background estimate
	%--
	
	% NOTE: this should be callback input for a threshold function
	
	t = fast_rank(A(:), 0.90);
	
	Z = (X - A) <= t; Z = logical(morph_erode(uint8(Z), ones(3)));
	
	%--
	% compute adaptive background estimate
	%--
	
	if ~isempty(start)
		B(:, 1) = (1 - alpha) * start + alpha * X(:, 1);
	end

	for k = 2:n
		
		B(~Z(:, k), k) = B(~Z(:, k), k - 1);
		
		B(Z(:, k), k) = (1 - alpha) * B(Z(:, k), k - 1) + alpha * X(Z(:, k), k);
		
	end

	
end