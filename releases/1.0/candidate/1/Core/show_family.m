function show_family(obj, eventdata)

% show_family - show parent and siblings of a figure
% --------------------------------------------------
%
% show_family(obj);
%
% Input:
% ------
%  obj - figure

% NOTE: this only shows the ancestral family

%--
% check handle input
%--

if ~ishandle(obj) || strcmp(get(obj, 'beingdeleted'), 'on')
	return; 
end

figure(obj);

%--
% check for double click
%--

if ~double_click(obj)
	return;
end

%--
% get parent and siblings
%--

par = get_xbat_figs('child', obj);

% NOTE: return since no parent implies no siblings

if isempty(par)
	return;
end

sib = setdiff(get_xbat_figs('parent', par), obj);

%--
% display siblings, parent and keep focus
%--

for k = 1:length(sib)
	figure(sib(k));
end

figure(par); figure(obj);

