function par = get_parent_browser(handle)

if numel(handle) > 1
	par = iterate(mfilename, handle); return;
end

if is_browser(handle)
	par = handle; return;
end

% NOTE: this works on 'widgets' and 'palettes'

par = get_widget_parent(ancestor(handle, 'figure'));