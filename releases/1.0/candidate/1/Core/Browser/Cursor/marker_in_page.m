function [value, page] = marker_in_page(marker, par, data)

% marker_in_page - test if marker is in page
% ------------------------------------------
%
% [value, page] = marker_in_page(marker, par, data)
%
% Input:
% ------
%  marker - marker
%  par - parent
%  data - state
%
% Output:
% -------
%  value - indicator
%  page - current browser page

% NOTE: this function is at a higher level than 'event_in_page' reconsider

%--
% handle input
%--

if nargin < 2
	par = get_active_browser;
end

if nargin < 3
	data = get_browser(par);
end

if ~nargin || isempty(marker)
	marker = get_browser_marker(par, data);
end 

%--
% check we have a non-empty marker
%--

if isempty(marker) || isempty(marker.time)
	
	value = 0; 
	
	if nargout > 1
		page = get_browser_page(par, data);
	end
	
	return;

end 

%--
% create marker event
%--

event = event_create;

% NOTE: this is a zero duration event, use this approach to store cues

event.time = marker.time * ones(1, 2); event.channel = marker.channel; 

%--
% check marker event is in page
%--

page = get_browser_page(par, data); 

sound = data.browser.sound;

value = event_in_page(event, page, sound);
