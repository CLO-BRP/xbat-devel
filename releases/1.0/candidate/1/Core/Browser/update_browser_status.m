function handle = update_browser_status(par, target, varargin)

% update_browser_status - update status bar display
% -----------------------------------------
%
% [handle, value] = update_browser_status(par, target, 'property', value, ... )
%
% Input:
% ------
%  par - parent figure
%  target - update target ('left','right', or empty)
%  field - property to update
%  value - value of property
%
% Output:
% -------
%  handle - handles of objects updated

% TODO: generalize to handle status specific targets. consider uniform tags and a collection of display objects, think of text and waitbars

%--
% set default output
%--

handle = [];

%--
% get status bar axes
%--

% TODO: use better tagging for status bars

ax = findobj(par, 'tag', 'Status');

% return if figure has no status bar, there is no warning for this event

if isempty(ax)
	return;
end

%--
% get field value pairs
%--

[field, value] = get_field_value(varargin);

% return if there is nothing to update

if isempty(field)
	return;
end

%--
% get text handles
%--

left = findobj(ax, 'tag', 'Status_Text_Left');

right = findobj(ax, 'tag', 'Status_Text_Right'); 

%--
% update properties
%--

for k = 1:length(field)
	
	%--
	% property update switch
	%--
	
	switch field{k}
		
		%--
		% status bar background
		%--
		
		case 'backgroundcolor'
			
			switch target
				
				% axes background color
				
				case ''
					handle = ax; set(handle, 'color', value{k});
					
				% text background color
				
				case 'left'
					handle = left; set(handle, 'backgroundcolor', value{k});
					
				case 'right'
					handle = right; set(handle, 'backgroundcolor', value{k});
					
			end
			
		%--
		% status bar text color
		%--
		
		case 'color'
			
			switch target
				
				% all text color
				
				case ''
					handle = [left, right]; set(handle, 'color', value{k});
					
				% message specific text color
				
				case 'left'
					handle = left; set(handle, 'color', value{k});
					
				case 'right'
					handle = right; set(handle, 'color', value{k});
					
			end

		%--
		% message update
		%--
		
		case 'message'
		
			switch target
				
				case 'left'
					handle = left; set(handle, 'string', value{k});
					
				case 'right'
					handle = right; set(handle, 'string', value{k});
					
					% text is repositioned by the resize function
					
					browser_resizefcn(par);
					
			end
			
	end
	
end