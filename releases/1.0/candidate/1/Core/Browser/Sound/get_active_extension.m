function [ext, ix, context] = get_active_extension(type, par, data, fast)

% get_active_extension - get browser active detector
% -------------------------------------------------
%
% [ext, ix, context] = get_active_extension(type, par, data)
%
% Input:
% ------
%  type - extension type
%  par - parent browser
%  data - parent state
%
% Output:
% -------
%  ext - active detector extension
%  ix - index of extension in browser store
%  context - context

% [caller, str] = get_caller; disp(['ACTIVE EXT: ', str]);

context = [];

%-------------------------
% HANDLE INPUT
%-------------------------

if nargin < 4
	fast = 0;
end 

%--
% get active browser
%--

if nargin < 2
	par = get_active_browser;
end

% NOTE: return empty when there is no browser to be active in =

if isempty(par)
	ext = []; ix = []; return;
end

%--
% check browser and get state if needed
%--

if ~is_browser(par)
	error('Input handle is not browser handle.');
end

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%--
% check extension type and existence of browser store for type
%--

passed = is_extension_type(type);

% NOTE: not perfect english, but correct and much better than before

if any(~passed)
	error(['Unrecognized extension type ''', str_implode(type(~passed), ''', '''), '''.']);
end

%--
% handle multiple types recursively
%--

if iscellstr(type)
	
	% NOTE: the output structure for the recursive case is reasonable but quirky
	
	% TODO: match the output structure for both calls
	
	for k = 1:length(type)
		
		[ext, ix, context] = get_active_extension(type{k}, par, data, fast);
		
		if ~isempty(ext)
			
			active.(type{k}).ext = ext; active.(type{k}).ix = ix; 
			
			active.(type{k}).context = context;
		
		end
		
	end
	
	if ~exist('active', 'var')
		active = [];
	end
	
	ext = active; ix = []; return;
	
end

%-------------------------
% GET ACTIVE EXTENSION
%-------------------------

%--
% consider trivial empty return conditions
%--

ix = [];

% NOTE: there is no browser registry for this extension type

if ~isfield(data.browser, type)
	ext = []; return;
end

% NOTE: there are no extensions of this type to make active

if isempty(data.browser.(type).ext)
	ext = []; return;
end 

%--
% get current active extension
%--

active = data.browser.(type).active;

% NOTE: there is no active extension of this type

if isempty(active)
	ext = []; return;
end 

if ischar(active)
	active = {active};
end

clear context;

for k = 1:length(active)
	
	% TODO: consider how to handle failure here
	
	[ext(k), ix(k), context(k)] = get_browser_extension(type, par, active{k}, data, fast);
	
end

