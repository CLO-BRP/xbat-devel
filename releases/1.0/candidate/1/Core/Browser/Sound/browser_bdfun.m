function event = browser_bdfun(event, obj, eventdata)

% browser_bdfun - enable selection in spectrogram display axes
% ------------------------------------------------------------
% 
% event = browser_bdfun(event)
% 
% Input:
% ------
%  event - event to display as selection
%
% Output:
% -------
%  event - selection event structure

% TODO: add parent figure input to avoid problems setting selection programmaticaly

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% set empty callback input
%--

if nargin < 3
	evendata = struct;
end

if nargin < 2
	obj = [];
end

%--
% return if selection is off
%--

par = get_active_browser;

data = get_browser(par); 

if ~data.browser.selection.on
	return;
end

% NOTE: this test solves a strange problem updating the selection grid from the palette

%--
% actual browser button down function
%--

% NOTE: the second clause of the statement is related to the 'browser_bdfun_callback' adaptation

if ~nargin || ((nargin == 3) && isempty(event))
	
	user = get_active_user;
	
	%--
	% check for double click
	%--
		
	% NOTE: this should only happen for actual clicks, hence the 'gco' test
	
	if ~isempty(get(gco, 'type'))
		
		if double_click(gco)
			browser_sound_menu(par, 'Play Page'); return;
		end
		
	end
	
	%--
	% pause marching ants
	%--
	
	ants_timer = timerfind('name', 'XBAT Marching Ants');
	
	if strcmp(get(ants_timer, 'running'), 'on')
		stop(ants_timer); ants_flag = 1;
	else
		ants_flag = 0;
	end
	
	%--
	% check for play timer and toggle play state
	%--
		
	% TODO: this behavior can also be effected through a control
	
% 	toggle_play_state;
	
	%--
	% get current axes
	%--
	
	dax = gca;
	
	%--
	% check pointer state to determine action
	%--
	
	% NOTE: new mode framework has been introduced after this code 
	
	ptr = get(par, 'pointer');
	
	if strcmp(ptr, 'watch')
		ptr = 'arrow';
	end
	
	%--
	% get coordinates for current marker or selection selection
	%--
	
	if strcmp(ptr, 'arrow')
	
		%--
		% selection behavior
		%--
		
		set(par, 'pointer', 'arrow');
		
		try	
			p1 = get(dax,'CurrentPoint'); rbbox; p2 = get(dax,'CurrentPoint'); p1 = p1(1, 1:2); p2 = p2(1, 1:2);
		catch	
			set(par, 'pointer', ptr); return;
		end
		
		set(par, 'pointer', ptr);
		
	else
	
		return;
		
	end
	
	%--
	% restart marching ants if needed
	%--
	
	if ants_flag
		start(ants_timer);
	end
	
	%--
	% sort points and compute anchor point and offset representation
	%--
	
	p3 = max(p1, p2); p1 = min(p1, p2); p2 = p3; dp = p2 - p1;
	
	%--
	% check that selection is contained in axes and not too small
	%--
	
	% TODO: develop marker and cues, cues may be stored in FLAC cue sheets
	
	% NOTE: allow for label and time display on marker and cues, we may select using associated text
		
	% TODO: this test should use relative size of selection within the page
	
	xl = get(dax, 'xlim'); yl = get(dax, 'ylim');
	
	%--
	% find session that selection is in
	%--
	
% 	session_boundaries = [];
	
	if has_sessions_enabled(data.browser.sound)
		
		sessions = get_sound_sessions(data.browser.sound);
		
		session_ix = ([sessions.end] > p2(1)) & ([sessions.start] < p1(1));
		
		% NOTE: return if the selection is not contained in a session
		
		if ~any(session_ix)
			return;
		else
% 			session_boundaries = [sessions(session_ix).start, sessions(session_ix).end];
		end
		
	end	
	
	%--
	% permit selection of events with zero starting frequency or nyquist end
	%--
	
	if (p1(1) < xl(1)) || (p2(1) > xl(2))
		return;
	end

	%--
	% check size of selection to determine if we place marker or selection
	%--
	
	% TODO: make this test relative to the page under consideration
	
	% NOTE: we consider frequency scaling in size test

	if strcmp(data.browser.grid.freq.labels, 'Hz')
		too_small = (dp(2) < 1) || dp(1) < 0.001;
	else
		too_small = (dp(1) < 0.001) || dp(1) < 0.001;
	end
	
	if too_small
				
		set_marker(par, dax, p1(1), data); return;
		
		% NOTE: the below experiment fails because the 'key' is not updated sufficiently
		
		% TODO: try to implement behavior using java handle
		
% 		db_disp 'two-marker selection behavior'
% 		
% 		key = get_keypress(par);
% 		
% 		switch key.key
% 			
% 			case 'shift'
% 				
% 				% TODO: this currently ignores sessions! it will need a fix
% 				
% 				marker = get_browser_marker(par); current = p1(1);
% 				
% 				if current < marker.time
% 					time = [current, marker.time];
% 				else
% 					time = [marker.time, current];
% 				end
% 				
% 				% TODO: consider using the page display frequency boundaries
% 				
% 				event = event_create( ...
% 					'time', time, ...
% 					'freq', [0, 0.5 * get_sound_rate(data.browser.sound)], ... 
% 					'channel', str2int(get(dax, 'tag')), ...
% 					'author', user.name ... 
% 				);
% 				
% 				browser_bdfun(event); return;
% 				
% 			otherwise
% 				
% 				set_marker(par, dax, p1(1), data); 
% 		end
% 
% 		return;

	end 

	%--
	% fix selection boundaries at frequency limits
	%--
	
	if p1(2) < yl(1)
		p1(2) = 0; dp(2) = p2(2);
	end
	
	if p2(2) > yl(2)
		p2(2) = yl(2); dp(2) = p2(2) - p1(2);
	end
	
	%--
	% create event structure for selection
	%--
	
	event = event_create;
	
	% NOTE: this may not be needed in the future
	
	if ~ischar(event.created)
		event.created = get_current_timestamp;
	end
	
	event.channel = str2int(get(dax, 'tag'));
	
	event.time = [p1(1), p1(1) + dp(1)];
	
	event.duration = dp(1);
	
	event.level = 1; % the setting of level will have to be reconsidered when we move to hierarchical events
	
	if strcmp(data.browser.grid.freq.labels, 'Hz')
		event.freq = [p1(2), p1(2) + dp(2)]; event.bandwidth = dp(2);
	else
		event.freq = 1000 * [p1(2), p1(2) + dp(2)]; event.bandwidth = 1000 * dp(2);
	end

	% NOTE: this may not be the only thing we store in the database, we should also include an email as identifier
	
	event.author = user.name;
	
%--
% display event based on input event
%--

else
	
	%--
	% get current axes
	%--
	
	% NOTE: this is valid in the case that we are clicking on a logged event and
	% we are displaying it as a selection, however this should get the axes
	% handle by finding the axes with the right tag
	
	% this is only slighly better
	
	dax = findobj(par, 'type', 'axes', 'tag', int2str(event.channel));
	
end

%----------------------------
% DELETE PREVIOUS SELECTION
%----------------------------

% TODO: consider removing one of the currently used lines, and using patch border

% TODO: consider tag-based deletion of display

clear_selection_display(data.browser.selection);

%----------------------------
% UPDATE MENUS AND CONTROLS
%----------------------------

%--
% turn on play selection menu 
%--

set(get_menu(data.browser.sound_menu.play,'Selection'),'enable','on');

%--
% turn on selection commands in edit menu
%--

tmp = data.browser.edit_menu.edit;

set(get_menu(tmp,'Cut Selection'),'enable','on');

set(get_menu(tmp,'Copy Selection'),'enable','on');

set(get_menu(tmp,'Delete Selection'),'enable','on'); 

set(get_menu(tmp,'Log Selection To'),'enable','on');

%--
% update controls
%--

control_update(par,'Sound','Selection','__ENABLE__',data);
	
control_update(par,'Navigate','Previous Event','__DISABLE__',data);

control_update(par,'Navigate','Next Event','__DISABLE__',data);

% NOTE: the idea of having a play selection button is not a bad one

% hp = get_palette(par, 'Sound', data);
% 
% if ~isempty(hp)
% 	set(get_button(hp, 'Play Selection'), 'enable', 'on');
% end

if isempty(dax)
	return;
end

%----------------------------
% DISPLAY SELECTION
%----------------------------

handle = selection_event_display(event, dax, data);

if ~ishandle(handle)
	return;
end

set(handle, 'tag', 'SELECTION_HANDLES');

refresh(par);

%----------------------------
% SELECTION MENU
%----------------------------

%--
% create basic selection menu
%--

menus = selection_menu(handle(2), event, data);

%--
% compute active measures and display menus
%--

top = get_menu(menus, 'Measurement');

% TODO: the signature can be improved by packing the selection earlier, and improving the 'handle' packing as well

[event, handles] = compute_active_event_measures(par, event, data, dax, top);

%----------------------------
% SET BROWSER SELECTION
%----------------------------

%--
% update selection in browser structure
%--

data.browser.selection.event = event;

data.browser.selection.handle = handle;

data.browser.selection.handles = handles;

data.browser.selection.log = [];

set(par, 'userdata', data);

%----------------------------
% SELECTION CREATED EVENT
%----------------------------

%--
% update selection zoom
%--

% TODO: implement this as a widget extension

opt = selection_display; opt.show = 1;

selection_display(par, event, opt, data);

%--
% update selection controls
%--

set_selection_controls(par, event, 'start', data);

%--
% update widgets
%--

% TODO: we don't want to update the active measure widgets here, they should be updated when we get here

update_widgets(par, 'selection__create', data);

if ~nargout
	clear('event');
end

%----------------------------
% BRING MARKER TO FRONT
%----------------------------

% NOTE: we want to do this as the marker does not affect the editability of the selections

marker = get_browser_marker(par);

if ~isempty(marker.handles)
	
	try %#ok<TRYNC>
		uistack(marker.handles, 'top');
	end

end




