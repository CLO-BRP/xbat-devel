function [log, ix] = set_browser_log(par, log, ix)

% set_browser_log - update browser log
% ------------------------------------
%
% [log, ix] = set_browser_log(par, log, ix)
%
% Input:
% ------
%  par - browser
%  log - log
%  ix - log browser position
%
% Output:
% -------
%  log - log
%  ix - log browser position

%--
% get browser state
%--

data = get_browser(par);

%--
% get log store index if needed
%--

if nargin < 3 || isempty(ix)
	ix = get_log_index(par, log, data);
end

if isempty(ix)
	return;
end 

%--
% udpate browser state
%--

data.browser.log(ix) = log;

set(par, 'userdata', data);


%--------------------
% GET_LOG_INDEX
%--------------------

function [ix, data] = get_log_index(par, log, data)

if nargin < 3
	data = get_browser(par);
end

ix = find(strcmp(log_name(log), log_name(data.browser.log)), 1);


