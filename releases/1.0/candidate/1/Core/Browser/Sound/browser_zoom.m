function par = browser_zoom(type, par)

% browser_zoom - zoom browser page
% --------------------------------
%
% par = browser_zoom('in', par), zoom in
%
%     = browser_zoom('out', par), zoom out
%
%     = browser_zoom('sel', par), zoom to selection
%
% Output:
% -------
%  par - updated browser

%---------------
% HANDLE INPUT
%---------------

%--
% get active browser if needed
%--

if nargin < 2
	par = get_active_browser;
end

%--
% check browser input
%--

if ~is_browser(par)
	return;
end

%---------------
% SETUP
%---------------

data = get_browser(par);

page = data.browser.page;

sel = data.browser.selection;

%---------------
% ZOOM
%---------------

switch type
	
	case 'in'
		
		set_browser_page(par, 'slider', ...
			'time', data.browser.time + page.duration / 4, ...
			'duration', page.duration / 2 ...
		);
		
	case 'out'
		
		set_browser_page(par, 'slider', ...
			'time', data.browser.time - page.duration / 2, ...
			'duration', page.duration * 2 ...
		);
		
	case 'sel'
		
		if isempty(sel.event)
			return;
		end

		pad = sel.event.duration / 20;
		
		set_browser_page(par, 'slider', ...
			'time', sel.event.time(1) - pad, ...
			'duration', sel.event.duration + 2 * pad ...
		);
		
	otherwise

		error('Unrecognized zoom command.');
		
end