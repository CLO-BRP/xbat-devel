function spectrogram_controls_callback(callback)

% spectrogram_controls_callback - callbacks for spectrogram palette controls
% --------------------------------------------------------------------------
%
% spectrogram_controls_callback(callback)
%
% Input:
% ------
%  callback - callback context

%--
% unpack callback
%--

control = callback.control; pal = callback.pal; par = callback.par;

%---------------------------
% PERFORM CALLBACKS
%---------------------------

%--
% get control state
%--

% NOTE: the callback control is not as rich as the get control

value = get_control(pal.handle, control.name, 'value');

handles = get_control(pal.handle, control.name, 'handles');

%--
% update interface elements
%--

switch control.name

	case 'fft'

		% NOTE: we display fft size factorization
		
		if handles.uicontrol.edit, set(handles.uicontrol.edit, 'tooltipstring', factor_str(value)); end

	case 'hop_auto'
	
		set_control(pal.handle, 'Advance', 'enable', ~value);


	case 'sum_auto'

		set_control(pal.handle, 'sum_length', 'enable', ~value);

	case 'win_type'

		window_plot([], pal, data);
		
		% NOTE: check for parametrized window and update parameter value and control
		
		param = window_to_fun(value, 'param');

		if isempty(param)
			
			g = set_control(pal.handle, 'Parameter', 'enable', 0);

			set(findobj(g, 'flat', 'style', 'slider'), ...
				'min', 0, ...
				'max', 1, ...
				'value', 0 ...
			);

			set(findobj(g, 'flat', 'style', 'edit'), 'string', []);

		else

			set_control(pal.handle, 'Parameter', 'enable', 1);
			
			handles = get_control(pal.handle, 'Parameter', 'handles');
			
			set(handles.uicontrol.slider, 'min', param.min, 'max', param.max);
			
			set_control(pal.handle, 'Parameter', 'value', param.value);

		end

end