function str = get_grid_time_string(grid, time, sound)

% get_grid_time_string - get time string according to grid
% --------------------------------------------------------
%
% str = get_grid_time_string(grid, time, sound)
%
% Input:
% ------
%  grid - grid options
%  time - time
%  sound - provides date offset and rate information
%
% Output:
% -------
%  str - time string according to grid

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% handle missing grid and sound input
%--

if trivial(grid)
	grid.time.labels = 'date and time';
end

if nargin < 3
	sound = struct;
end

%--
% handle multiple times recursively
%--

if numel(time) > 1
	
	str = cell(size(time));
	
	for k = 1:numel(time)
		str{k} = get_grid_time_string(grid, time(k), sound);
	end 
	
	return; 

end

%--
% set default realtime and rate
%--

if trivial(sound)
	realtime = []; rate = 10000;
else
	realtime = sound.realtime; rate = get_sound_rate(sound);
end 

%------------------------------
% COMPUTE TIME STRINGS
%------------------------------

%--
% get precision conmesurate with rate
%--

% NOTE: in 'event_labels' we use one more digit than here

n = floor(log10(rate)) - 1;

%--
% compute time string based on grid settings, time, and realtime
%--

% TODO: consider a 'get_grid_label_types'

switch grid.time.labels
	
	case 'seconds'
		
		% NOTE: the rounding preserves two decimal digits if needed
		
		str = [num2str(round(10^n * time) / 10^n), ' sec'];
		
	case 'clock'
		
		str = sec_to_clock(time, n);
		
	case 'date and time'
		
		if isempty(realtime)
			str = sec_to_clock(time, n);
		else
			str = datestr(offset_date(realtime, time));
		end
		
end
