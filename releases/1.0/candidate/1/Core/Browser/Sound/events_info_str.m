function [info, known_tags] = events_info_str(par, search, opt)

% events_info_str - create info string for list of browser events
% ---------------------------------------------------------------
%
% [info, known_tags] = events_info_str(par, fields, opt)
%
% Input:
% ------
%  par - browser handle
%  fields - fields to display
%  opt - list options
%
% Output:
% -------
%  info - cell array of event strings

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% set and possibly output default options
%--

if (nargin < 3) || isempty(opt)
	
	opt.page = 0; opt.visible = 0; opt.order = 'time';
	
	if ~nargin
		info = opt; return;
	end
	
end

if nargin < 2
    search = [];
end

%--
% check sort order value
%--

orders = {'id', 'score', 'time', 'rating', 'name'};

if ~string_is_member(opt.order, orders)
	error(['Unrecognized sort order ''', opt.order, '''.']);
end

%--
% check browser input
%--

if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%----------------------------
% SETUP
%----------------------------

%--
% get browser state
%--

data = get_browser(par); 

%--
% create convenience variables for parts of state
%--

log = data.browser.log; 

%----------------------------
% KEEP VISIBLE
%----------------------------

%--
% select only displayed channels
%--

% NOTE: it makes sense to use visibility filtering all the time

if opt.visible
	
	%--
	% select remove invisible logs
	%--
	
	log(~[log.visible]) = [];
	
    if isempty(log)
		info = []; known_tags = {}; return;
    end
	
end

%----------------------------
% CREATE STRINGS
%----------------------------

%--
% get log info and known tags
%--

[info, known_tags] = log_get_info(log, search, opt.order);


