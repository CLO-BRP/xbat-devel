function [ext, context, ix] = get_active_image_filter(varargin)

[ext, ix, context] = get_active_extension('image_filter', varargin{:});