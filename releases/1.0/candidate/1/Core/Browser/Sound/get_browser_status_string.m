function [str, info] = get_browser_status_string(par, start, data)

%--
% handle input
%--

if nargin < 3
	data = get_browser(par);
end

%--
% build status info
%--

info.elapsed = etime(clock, start);

N = data.browser.specgram.fft; 

info.row = floor(0.5 * N) + ~mod(N, 2);

info.col = size(get(data.browser.images(1), 'CData'), 2);

info.stack = sum(data.browser.channels(:, 2));

if isempty(data.browser.page.freq)
	info.view_rows = info.row;
else
	info.view_rows = size(get(data.browser.images(1), 'CData'), 1);
end

%--
% build status string
%--

row = int2str(info.row); col = int2str(info.col); stack = int2str(info.stack);

elapsed = [num2str(info.elapsed,4) ' sec'];

if isempty(data.browser.page.freq)
	str = [row, ' x ', col, ' x ', stack, '   (', elapsed, ')'];
else
	str = ['(', int2str(info.view_rows), '/', row, ') x ', col, ' x ', stack, '   (', elapsed, ')'];
end



