function [next, event, log] = goto_next_event(varargin)

[next, event, log] = goto_closest_event(1, varargin{:});
