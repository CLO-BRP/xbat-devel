function handle = spectrum_tool

% TODO: we need a shorter way of declaring interfaces, this should be an even more concise internal DSL

% NOTE: part of this may be assisted by type specific constructors

% NOTE: the previous comment would seem counter to the idea of extensibility, perhaps

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = header_control('Spectrum', 0);

control(end + 1) = control_create( ...
	'name', 'display', ... 
	'style', 'axes', ...
	'lines', 10 ...
);

% NOTE: the controls and callbacks present a driver not device oriented interface

str = get_playrec_drivers; str = {'All', str{:}}; value = 1;

control(end + 1) = control_create( ...
	'name', 'driver', ...
	'style', 'popup', ...
	'string', str, ...
	'value', value ...
);

device = get_playrec_input_devices; str = {device.name};

control(end + 1) = control_create( ...
	'name', 'device', ...
	'style', 'popup', ...
	'string', str, ...
	'value', value, ...
	'width', 0.75, ...
	'space', -2 ...
);

control(end + 1) = control_create( ...
	'name', 'id', ...
	'alias', 'ID', ...
	'style', 'edit', ...
	'string', int2str(device(value).deviceID), ...
	'width', 0.2, ...
	'align', 'right', ...
	'space', 2, ...
	'initialstate', '__DISABLE__' ...
);

% NOTE: here we sort of use the alias as a value for the control, consider this in 'get_control'

control(end + 1) = control_create( ...
	'name', 'listen', ...
	'alias', 'START', ...
	'style', 'buttongroup', ...
	'lines', 2 ...
);

%--
% render tool interface
%--

opt = control_group; opt.width = 18; opt.bottom = 0;

handle = control_group(0, @spectrum_tool_callback, 'Spectrum Tool', control, opt);


%--------------------------------
% SPECTRUM_TOOL_CALLBACK
%--------------------------------

function spectrum_tool_callback(obj, eventdata)  %#ok<INUSD>

%--
% collect callback context
%--

callback = get_callback_context(obj, eventdata); pal = callback.pal;

%--
% get context devices
%--

value = get_control(pal.handle, 'driver', 'value'); value = value{1};

if strcmpi(value, 'all')
	device = get_playrec_input_devices;
else
	device = get_playrec_input_devices('driver', value);
end
		
%--
% dispatch callback
%--

switch callback.control.name
	
	case 'driver'
		
		% NOTE: we are assuming that there is at least one device
		
		set_control(pal.handle, 'device', 'string', {device.name});
		
		% NOTE: we set the device to the first device in the list
		
		set_control(pal.handle, 'device', 'value', 1);
		
		device = device(1);
		
		set_control(pal.handle, 'id', 'value', device.deviceID);
		
		%--
		% initialize selected device and set samplerate display
		%--
		
		if playrec('isInitialised')
			disp(' '); playrec('reset');
		end
		
		rate = device.defaultSampleRate;
		
		playrec('init', rate, -1, device.deviceID);
		
		set_control(pal.handle, 'samplerate', 'value', rate);
		
	case 'device'
		
		%--
		% get device and update identifier display
		%--
		
		device = device(get(callback.obj, 'value'));
		
		set_control(pal.handle, 'id', 'value', device.deviceID);
		
		%--
		% initialize selected device and set samplerate display
		%--
		
		if playrec('isInitialised')
			disp(' '); playrec('reset');
		end
		
		rate = device.defaultSampleRate;
		
		playrec('init', rate, -1, device.deviceID);
		
		set_control(pal.handle, 'samplerate', 'value', rate);

	case 'listen'
		
		parameter = compile_parameters(callback.pal.handle);
		
		% NOTE: we are using the button label as a control value
		
		switch lower(get(callback.obj, 'string'))
			
			case 'start'
				start_listening(pal, parameter); set(callback.obj, 'string', 'PAUSE');
				
			case 'pause'
				stop_listening(pal, parameter); set(callback.obj, 'string', 'START');
				
		end
		
	otherwise

		return;
	
end


%--------------------------------
% COMPILE_PARAMETERS
%--------------------------------

function parameter = compile_parameters(pal)

parameter = get_control_values(pal);


%--------------------------------
% START_LISTENING
%--------------------------------

function start_listening(pal, parameter)

initialize_display(pal);

playrec('delPage');




%--------------------------------
% STOP_LISTENING
%--------------------------------

function stop_listening(pal, parameter)

initialize_display(pal);

playrec('delPage');



%--------------------------------
% INITIALIZE_DISPLAY
%--------------------------------

function initialize_display(pal)

%--
% check for container
%--

handle = get_control_handles(pal.handle, 'display');

if isempty(handle)
	return;
end

%--
% get device info and initialize display
%--

device = get_record_device















function [ ] = spectrum_analyser( recDeviceID, chanList)

%SPECTRUM_ANALYSER Uses Playrec to implement a basic spectrum analyser.
%
%   spectrum_analyser( recDeviceID, chanList) creates two figures, one
%   displaying the spectrum and another the waveform recorded from the
%   specified playrec device, recDeviceID, on the channels chanList.
%
%   The script runs until either interrupted or both figures are closed.

%--------------------
% SETUP
%--------------------

% NOTE: increase these values to ensure output stability at the expense of a longer latency

pageSize = 2048;    % size of each page processed

pageBufCount = 5;   % number of pages of buffering

fftSize = pageSize * 2;

Fs = 44100;

% NOTE: when true, the processor is used much more heavily but the chance of glitches is reduced without increasing latency

runMaxSpeed = false; 

if (ndims(chanList) ~= 2) || (size(chanList, 1) ~= 1)
	error 'chanList must be a row vector';
end

%--
% test if current initialisation is ok
%--

if playrec('isInitialised')
	
	if playrec('getSampleRate')~=Fs
		
		fprintf('Changing playrec sample rate from %d to %d\n', playrec('getSampleRate'), Fs);
		playrec('reset');
		
	elseif(playrec('getRecDevice')~=recDeviceID)
		
		fprintf('Changing playrec record device from %d to %d\n', playrec('getRecDevice'), recDeviceID);
		playrec('reset');
		
	elseif(playrec('getRecMaxChannel')<max(chanList))
		
		fprintf('Resetting playrec to configure device to use more input channels\n');
		playrec('reset');
		
	end
	
end

%--
% initialise if not initialised
%--

if ~playrec('isInitialised')
	fprintf('Initialising playrec to use sample rate: %d, recDeviceID: %d and no play device\n', Fs, recDeviceID);
	playrec('init', Fs, -1, recDeviceID)
end

if ~playrec('isInitialised')
	error 'Unable to initialise playrec correctly';
elseif playrec('getRecMaxChannel') < max(chanList)
	error ('Selected device does not support %d output channels\n', max(chanList));
end

% NOTE: clear all previous pages as part of initialization stage

playrec('delPage');

%--
% create display figures
%--

fftFigure = figure;

fftAxes = axes('parent', fftFigure, 'xlimmode', 'manual', 'ylimmode', 'manual', 'xscale', 'log', 'yscale', 'linear', 'xlim', [10 Fs/2], 'ylim', [-60, 60]);

for i=1:length(chanList)
	fftLine(i) = line('XData', (0:(fftSize/2))*Fs/fftSize,'YData', ones(1, fftSize/2 + 1));
end

timeFigure = figure;

timeAxes = axes('parent', timeFigure, 'xlimmode', 'manual', 'ylimmode', 'manual', 'xscale', 'linear', 'yscale', 'linear', 'xlim', [1 pageSize], 'ylim', [-1, 1]);

for i=1:length(chanList)
	timeLine(i) = line('XData', 1:pageSize,'YData', ones(1, pageSize));
end

drawnow;

recSampleBuffer = zeros(fftSize, length(chanList));

%--
% create vector to act as FIFO for page numbers
%--

pageNumList = repmat(-1, [1 pageBufCount]);

firstTimeThrough = true;

while(ishandle(fftFigure) || ishandle(timeFigure))
	
	pageNumList = [pageNumList playrec('rec', pageSize, chanList)];

	if firstTimeThrough
		%This is the first time through so reset the skipped sample count
		playrec('resetSkippedSampleCount');
		firstTimeThrough = false;
	else
		if playrec('getSkippedSampleCount')
			fprintf('%d samples skipped!!\n', playrec('getSkippedSampleCount'));
			%return
			%Let the code recover and then reset the count
			firstTimeThrough = true;
		end
	end

	% runMaxSpeed==true means a very tight while loop is entered until the
	% page has completed whereas when runMaxSpeed==false the 'block'
	% command in playrec is used.  This repeatedly suspends the thread
	% until the page has completed, meaning the time between page
	% completing and the 'block' command returning can be much longer than
	% that with the tight while loop
	
	if runMaxSpeed
		while(playrec('isFinished', pageNumList(1)) == 0)
		end
	else
		playrec('block', pageNumList(1));
	end

	lastRecording = playrec('getRec', pageNumList(1));
	
	if ~isempty(lastRecording) 
		
		%very basic processing - windowing would produce a better output
		
		recSampleBuffer = [recSampleBuffer(length(lastRecording) + 1:end, :); lastRecording];
		
		recFFT = fft(recSampleBuffer)';
		
		if ishandle(fftFigure)
			for i=1:length(chanList)
				set(fftLine(i), 'YData', 20*log10(abs(recFFT(i, 1:fftSize/2 + 1))));
			end
		end

		if ishandle(timeFigure)
			for i=1:length(chanList)
				set(timeLine(i), 'YData', lastRecording(:,i));
			end
		end
		
	end

	drawnow;

	playrec('delPage', pageNumList(1));
	
	% NOTE: we pop page number from FIFO
	
	pageNumList = pageNumList(2:end);

end

%--
% delete all pages now loop has finished
%--

playrec('delPage');

