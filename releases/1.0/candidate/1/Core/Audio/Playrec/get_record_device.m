function device = get_record_device

id = playrec('getRecDevice');

if id == -1
	device = []; return;
end

device = get_playrec_input_devices; device = device([device.deviceID] == id);

if ~nargout
	disp(device); clear device;
end