function [file, test] = get_library_sound_file(lib, name)

% get_library_sound_file - get location of sound file in library
% --------------------------------------------------------------
%
% [file, test] = get_library_sound_file(lib, name)
%
% Input:
% ------
%  lib - library 
%  name - name of sound
%
% Output:
% -------
%  file - location of sound file in library
%  test - file existence test

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

if nargin < 2
	error('Sound name input is required.');
end

%--
% set library to active library
%--

if (nargin < 1) || isempty(lib)
	lib = get_active_library;
end
	
%--
% handle multiple sounds recursively
%--

if iscellstr(name)
	file = iteraten(mfilename, 2, lib, name); return;
end

%--
% check sound name
%--

if ~ischar(name)
	error('Sound name input must be string of string cell array.');
end

%-----------------------------------
% BUILD SOUND FILE LOCATION
%-----------------------------------

%--
% build sound file name
%--

file = [lib.path, name, filesep, name, '.mat'];

%--
% test for file existence
%--

if nargout > 1
	test = exist(file, 'file');
end
