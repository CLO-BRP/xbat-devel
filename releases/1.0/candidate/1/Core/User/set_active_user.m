function user = set_active_user(user)

% set_active_user - set active user
% ---------------------------------
% 
% user = set_active_user(user)
%
% Input:
% ------
%  user - user to make active
%
% Output:
% -------
%  user - active user

%-------------------------------
% HANDLE INPUT
%-------------------------------

%--
% output active user when given no input
%--

if nargin < 1
	user = get_active_user; return;
end

%--
% check input and get user from input
%--

switch class(user)

	case 'char', user = get_users('name', user);
		
	case 'cell', user = get_users('name', user{1});
	
	case 'struct'

		if ~isfield(user, 'name')
			user = [];
		end

	otherwise, user = []; % NOTE: no proper way to set using input

end

%--
% return active user if no proper way to update
%--

if isempty(user)
	user = get_active_user; return;
end

% TODO: consider warning when we don't find the requested user

%-------------------------------
% SET ACTIVE USER
%-------------------------------

%--
% check if user is changing
%--

change = ~is_active_user(user);

%--
% update user store
%--

set_env('xbat_user', user);

if ~change
	return;
end

%--
% update XBAT palette on user change
%--

% NOTE: the user can be modified if there is a missing library!

[lib, user] = set_active_library([], user);

% TODO: use update controls when developer state has not changed

close(xbat_palette); xbat_palette;

% update_controls(user);

%--
% update pcode state
%--

pcode_refresh(ternary(xbat_developer, 'clear', 'generate'));

% NOTE: this allows developers to see undocumented handle graphic properties

% set(0, 'HideUndocumented', ternary(xbat_developer, 'off', 'on'));


%-------------------------------
% UPDATE CONTROLS
%-------------------------------

function update_controls(user) %#ok<DEFNU>

% update_controls - find and update all controls dealing with active user
% -----------------------------------------------------------------------

%-----------------------------------------------
% UPDATE XBAT PALETTE CONTROLS
%-----------------------------------------------

pal = xbat_palette;

if isempty(pal) 
	return;
end

if isempty(user)
	return;
end

%--
% update 'User' control
%--

handles = get_control(pal, 'User', 'handles');

names = user_name_list; ix = find(strcmp(user.name, names));

% %--
% % disable delete user for default user
% %--
% 
% set_control(pal, 'edit_user', ...
% 	'enable', ~strcmp(user.name, 'Default') ...
% );

set(handles.obj, ...
	'string', names, 'value', ix ...
);


%----------------------------------
% UPDATE XBAT USER MENUS
%----------------------------------

session_menu(pal, user);

user_menu(pal);
