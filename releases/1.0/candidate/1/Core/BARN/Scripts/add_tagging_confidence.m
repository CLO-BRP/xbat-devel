function add_tagging_confidence(store)

% add_tagging_confidence - column, along with score and review

column = {'confidence', 'score', 'review'};

description = strcat({'TINYINT DEFAULT '}, {'0', 'NULL', 'NULL'});

for k = 1:numel(column)
	
	if ~is_table_column(store, 'tagging', column{k}),
		add_column(store, 'tagging', column{k}, 'sql', description{k});
	end
end