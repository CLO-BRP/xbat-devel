function add_event_sound_id(store)

% add_event_sound_id - migration
% ------------------------------
%
% add_event_sound_id(store)
%
% Input:
% ------
%  store - database

%--
% handle input
%--

if ~nargin
	store = local_barn;
end 

%--
% inspect and possibly update schema
%--

% NOTE: here we describe the candidate columns to add

column = {'sound_id', 'standard_start', 'standard_type'};

type = {'INTEGER NOT NULL', 'VARCHAR(255)', 'VARCHAR(255)'};

for k = 1:numel(column)
	if is_table_column(store, 'event', column{k})
		continue; 
	end
	
	add_column(store, 'event', column{k}, 'sql', type{k});
end

%--
% set sound foreign-key value
%--

% NOTE: this update may not be typical, the sub-select may be useful in more places 'reveal_transitive_relation'

% NOTE: we may also want a 'discover_transitive_relations' function, would be useful in the 'views' inference function

query(store, [ ...
	'UPDATE event AS e INNER JOIN', ...
	' (SELECT log.sound_id AS sid, log.id AS lid, event.id AS eid FROM log LEFT JOIN event ON event.log_id = log.id)' ...
	' AS logevent ON e.id = logevent.eid SET e.sound_id = logevent.sid' ...
]); 

