function table = reestablish_barn_table(store, table, type, sql)

% reestablish_barn_table - for core tables
% ----------------------------------------
%
% table = reestablish_barn_table(store, table)
%
% Input:
% ------
%  store - description
%  table - name
%
% Output:
% -------
%  table - in master

%--
% handle input
%--

if nargin < 4
	sql = create_barn_table(table);
end 

if nargin < 3
	type = 'core';
end

%--
% remove table references and drop table
%--

query(store, ['DELETE FROM barn_master WHERE name = ''', table, '''; DROP TABLE IF EXISTS ', table, ';']);

% NOTE: we call create barn table with 'force', whish first deletes the table if it exists

table = establish_barn_table(store, table, type, sql);