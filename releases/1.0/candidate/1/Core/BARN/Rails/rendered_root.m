function [root, exists, created] = rendered_root(obj, create)

% rendered_root - root for rendered assets
% ----------------------------------------
%
% [root, exists, created] = rendered_root(obj, create)
%
% Input:
% ------
%  obj - rendered
%  create - root directory if needed
%
% Output:
% -------
%  root - directory
%  exists - indicator
%  created - now

%--
% get root of all rendered asset files
%--

% NOTE: creation does not apply to the parent

root = fullfile(public_root, 'files', 'rendered'); 

if ~nargin
	exists = exist(root, 'dir'); created = []; return;
end

%--
% get asset directory, possibly create
%--

% NOTE: we set no create default

if nargin < 2
	create = 0; 
end 

% NOTE: input may be the model name or an object

if ischar(obj)
	model = obj;
else
	model = inputname(1);
end

if isempty(model)
	error('Unable to determine model name from input.');
end

% NOTE: we know we have a non-empty model at least

if ischar(obj)
	root = fullfile(root, model);
else
	root = fullfile(root, model, int2str(obj.id));
end

if create
	[out, created] = create_dir(root); exists = ~isempty(out);
else
	exists = exist(root, 'dir'); created = 0;
end
