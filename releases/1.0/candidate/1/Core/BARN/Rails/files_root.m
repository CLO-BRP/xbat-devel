function root = files_root(server)

% files_root - for rails server
% -----------------------------
%
% root = files_root(server)
%
% Input:
% ------
%  server - description
% 
% Output:
% -------
%  root - of files

%--
% handle input
%--

if ~nargin
	[ignore, server] = local_barn;
end

%--
% get files root by inspecting server description
%--

if ~isempty(server.files)
	
	root = server.files;
	
elseif ~isempty(server.root)
	
	root = fullfile(server.root, 'public', 'files');
	
else
	root = '';
end