function routes = parse_routes(root, pat)

if nargin < 2
	pat = '';
end

if ~nargin
	root = rails_root;
end 

%--
% get rake lines, parse, and possibly filter
%--

routes = iterate(@parse_route, get_rake_lines(root));

if ~isempty(pat)
	routes = routes(string_contains({routes.line}, pat));
end
	
%--
% display routes if needed
%--

if ~nargout
	disp(routes); clear routes;
end


%--------------------
% GET_RAKE_LINES
%--------------------

function lines = get_rake_lines(root)

%--
% setup and check persistent line store
%--

persistent LINES; 

if isempty(LINES)
	LINES = struct;
end

key = genvarname(md5(root));

if isfield(LINES, key)
	lines = LINES.(key); return;
end

start = pwd; cd(root)

[status, result] = system('rake routes'); lines = file_readlines(result); 

for k = numel(lines):-1:1
	
	if ~isstrprop(lines{k}(1), 'wspace') || ~string_contains(lines{k}, '{')
		lines(k) = [];
	end
	
end

LINES.(key) = lines;

cd(start);


%--------------------
% PARSE_ROUTE
%--------------------

function route = parse_route(line)

%--
% parse route line
%--

line = str_implode(str_split(line));

% route.line = line;

split = find(line == '{', 1);

part = str_split(line(1:split - 1)); params = line(split:end);

%--
% pack
%--

route = route_create; route.params = params; route.line = line;

switch numel(part)
	
	case 2 
		
		if string_is_member(part{1}, {'GET', 'POST', 'PUT', 'DELETE'})
			route.name = ''; route.method = part{1}; 
		else
			route.name = part{1}; route.method = '';
		end

		route.path = part{2};
		
	case 3
		
		route.name = part{1}; route.method = part{2}; route.path = part{3};
		
end


%--------------------
% ROUTE_CREATE
%--------------------

function route = route_create

field = {'name', 'method', 'path', 'params', 'line'};

route = struct;

for k = 1:numel(field)
	route.(field{k}) = []; 
end
