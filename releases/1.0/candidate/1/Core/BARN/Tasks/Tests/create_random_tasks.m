function task = create_random_tasks(count)

% create_random_tasks - to test task queue
% ----------------------------------------
%
% task = create_random_tasks(count)
%
% Input:
% ------
%  count - of random tasks
%
% Output:
% -------
%  task - array

if nargin < 2
	count = 10;
end

%--
% create a persistent counter to name tasks
%--

persistent TOTAL_TASKS;

if isempty(TOTAL_TASKS)
	TOTAL_TASKS = 1;
end

%--
% create numbered tasks with random descriptions
%--

task = empty(create_task); 

for k = 1:count
	
	description = because;
	
	if numel(description) > 255
		description = description(1:255);
	end
	
	task(end + 1) = create_task( ...
		'name', ['Task-', int2str(TOTAL_TASKS)], 'description', description ...
	); %#ok<AGROW>

	TOTAL_TASKS = TOTAL_TASKS + 1;

end

if ~nargout
	disp(task); clear task; 
end

