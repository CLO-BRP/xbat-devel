function [task, field] = create_task(varargin)

% create_task - struct
% --------------------
% 
% task = create_task(field, value, ... )
%
% Input:
% ------
%  field - of task
%  value - for field
% 
% Output:
% -------
%  task - struct

persistent PROTOTYPE;

if isempty(PROTOTYPE)
	PROTOTYPE = struct_empty(get_barn_prototype('task'));
end

[task, field] = parse_inputs(PROTOTYPE, varargin{:});
