function result = execute_task(task, store, server)

db_disp executing; task


task.daemon = ''; task.completed_at = datestr(now, 31);

set_barn_objects(store, task);

result = struct;