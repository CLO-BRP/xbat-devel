function daemon = task_daemon(store, server)

% task_daemon - used to poll BARN store queued tasks
% --------------------------------------------------
%
% daemon = task_daemon(store, server)
%
% Input:
% ------
%  store - to poll
%  server - to interact with
%
% Output:
% -------
%  daemon - timer

%--
% set local barn store default
%--

if ~nargin
	[store, server] = local_barn;
end

%--
% create barn daemon to check on store
%--

guid = generate_uuid;

daemon = create_timer('BARN task daemon', {@on__timer, guid, store, server}, 2, 'fixedRate', true);

set(daemon, 'busymode', 'drop');


%------------------------
% ON__TIMER
%------------------------

function on__timer(obj, eventdata, guid, store, server) %#ok<INUSL>

% NOTE: the two lines below are a sketch of where we are going

task = pick_task(store, guid);

if isempty(task)
	return;
end

db_disp implement-execution

result = execute_task(task, store, server); %#ok<NASGU>

return;





















%--
% select tasks from store
%--

task = get_tasks(store);

if isempty(task)
	return;
end

% TODO: update task board when we pick up a task, so that others will not go for the same task

% NOTE: thus we want to 'pick' a task more than 'get' tasks

%--
% dispatch tasks
%--

for k = 1:numel(task)
	
	current = task(k);
	
	%--
	% handle task
	%--
	
	% TODO: given that actions may have quite a bit of code we need to encapsulate, perhaps extensions
	
	switch current.task.action
		
		case 'create_sound'
			task_create_sound(store, current);
			
		case 'scan_sound'
			task_scan_sound(store, current);
			
		case 'render_event'
			
		case 'measure_log';
			
		case 'measure_event'
			
		case 'classify_event'
			
	end
	
end

% TODO: we want to rely on 'get_ip', the current module for this is very obscure


