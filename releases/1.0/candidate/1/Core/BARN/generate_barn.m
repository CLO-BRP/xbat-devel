function [result, created] = generate_barn

% generate_barn - create directory skeleton for BARN repository
% -------------------------------------------------------------
%
% [result, created] = generate_barn
%
% Output:
% -------
%  result - created directories
%  created - indicator

%--
% create root
%--

[root, created] = barn_root; 

disp(' '); display_status(root, created);

%--
% create children
%--

child = {'Database', 'Assets', 'Server'};

result{1} = root;

for k = 1:numel(child)
	
	[result{end + 1}, created(end + 1)] = create_dir(root, child{k});
	
	display_status(result{end}, created(end));
	
end

disp(' ');

if ~nargout, clear result; end


%----------------------------
% DISPLAY_STATUS
%----------------------------

function display_status(result, created)

if created
	disp(['Created ', result]);
else
	disp(['Exists ', result]);
end