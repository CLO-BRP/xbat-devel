function root = ruby_root

% ruby_root - get root of ruby executables
% ----------------------------------------
% 
% root = ruby_root
%
% Output:
% -------
%  root - directory

% NOTE: this will get the root of the system ruby installation

%--
% keep and check persistent store
%--

persistent ROOT; 

if ~isempty(ROOT)
	root = ROOT; return;
end

%--
% look in windows path for match
%--

path = get_windows_path; root = '';

for k = 1:numel(path)
	
	if isempty(strfind(path{k}, 'ruby'))
		continue;
	end
	
	root = path{k}; break;

end

% NOTE: we set the persistent store here

ROOT = root;