function root = mex_checkout_root(root)

% mex_checkout_root - where we've checked out the MEX branch
% ----------------------------------------------------------
%
% root = mex_checkout_root(root)
%
% Input:
% ------
%  root - to set
%
% Output:
% -------
%  root - currently set

persistent MEX_CHECKOUT_ROOT

if isempty(MEX_CHECKOUT_ROOT)
	candidate = fullfile(fileparts(app_root), 'XBAT_MEX');
	
	if exist_dir(candidate)
		MEX_CHECKOUT_ROOT = candidate;
	end
end

root = MEX_CHECKOUT_ROOT;