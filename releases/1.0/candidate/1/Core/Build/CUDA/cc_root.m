function [bin, include] = cc_root(bin)

% cc_root - where the C compiler tools live
% -----------------------------------------
%
% [bin, include] = cc_root
%
% Output:
% -------
%  bin - directory
%  include - directory

persistent ROOT

% NOTE: we set the root when it is empty or when we are asked to

if isempty(ROOT) || nargin
	
	if nargin
		% NOTE: setting the root with input allows us to handle any odd configuration
		
		ROOT = bin;
	else
		if ~ispc
			[ignore, ROOT] = system('which gcc');  %#ok<ASGLU>
		else
			% NOTE: these are the default installation directories
			
			candidate = { ...
				'C:\Program Files\Microsoft Visual Studio 9.0\VC\bin', ...
				'C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin' ...
			};
			
			ROOT = [];
			
			for k = 1:numel(candidate)
				if ~exist(candidate{k}, 'dir')
					continue;
				end
				
				ROOT = candidate{k}; break;
			end
		end
	end
	
	if isempty(ROOT) || ~exist(ROOT, 'dir')
		ROOT = []; error('Proposed compiler bin directory does not exist.');
	end
end

bin = ROOT;

include = fullfile(fileparts(bin), 'include');
