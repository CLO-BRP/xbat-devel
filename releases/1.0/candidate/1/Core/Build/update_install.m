function update_install(file)

% update_install - tree for platform-specific MEX files
% -----------------------------------------------------
%
% update_install(file)
%
% Input:
% ------
%  file - to add to MEX install tree

%--
% setup
%--

% NOTE: keep track of code root and platform-specific MEX install bundle root

persistent ROOT1 ROOT2;

if isempty(ROOT1)
    ROOT1 = app_root;
    
    ROOT2 = create_dir(fullfile(app_root, 'MEX', [upper(computer), '_', mexext]));
    
    if isempty(ROOT2)
        error('Unable to create install root.');
    end
end

%--
% append file move commands to file
%--

for k = 1:length(file)
    
    % NOTE: we are moving files from where they are installed to the MEX install root
    
    output = strrep(file{k}, ROOT1, ROOT2);
    
    if isempty(create_dir(fileparts(output)))
        continue;
    end
    
    try
        copyfile(file{k}, output);
    catch
        continue;
    end
end
