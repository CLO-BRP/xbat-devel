function dirs = get_mex_dirs

% get_mex_dirs - get MEX directories within XBAT root
% ---------------------------------------------------
%
% dirs = get_mex_dirs
%
% Output:
% -------
%  dirs - MEX dirs within XBAT root

%----------------
% GET MEX DIRS
%----------------

%--
% scan directories with mex selection callback
%--

dirs = scan_dir(app_root, @get_mex);

if ~nargout
	xml_disp(dirs);
end


%----------------
% GET_MEX
%----------------

function file = get_mex(file)

if strmatch('XEM', fliplr(file))
	
else 
	file = [];
end