function [files, destination] = build(debug)

% build - generic build for files in a 'MEX' directory
% ----------------------------------------------------
%
% build(debug)
%
% Input:
% ------
%  debug - flag
%
% NOTE: Move to a 'MEX' directory and call build to build contents

% TODO: consider adding compiler preference as input

% TODO: consider integrating the CUDA compilation here

%--
% handle input
%--

if ~nargin
    debug = 0;
end

%--
% check we are in MEX directory
%--

here = pwd;

if here(end) == filesep
    here(end) = [];
end

if ~strncmp(fliplr(here), 'XEM', 3)
    disp(' ');
    disp('This is not a ''MEX'' directory, there is nothing to build!');
    disp(' ');
    
    return;
end

% NOTE: remove platform-specific existing files from MEX directory

delete(['*.', mexext]);

%--
% yield to custom build script
%--

private = fullfile(here, 'private');

if exist(fullfile(private, 'build.m'), 'file')
    str_line(64, '_');
    disp(' ');
    disp(['CUSTOM build of ''', pwd, ''' ...']);
    str_line(64, '_');
    disp(' ');
    
    try
        cd(private); [files, destination] = build; cd(here); return;
    catch
        cd(here); nice_catch(lasterror, ['Custom build script failed for ''', here, '''.']); %#ok<LERR>
    end
end

%--
% build generically
%--

content = no_dot_dir; names = {content.name};

if isempty(names)
    return;
end

%--
% get library dependences for linker from file if possible
%--

linkflags = {};

if any(strcmp(names, 'deps.txt'))
	
	libs = file_readlines('deps.txt');
	
	for k = 1:length(libs)
		linkflags{k} = strrep(libs{k}, 'lib', '-l');
	end
end

if isempty(linkflags) && string_is_member('deps.m', names)
	libs = deps();
	
	for k = 1:length(libs)
		linkflags{k} = strrep(libs{k}, 'lib', '-l');
	end
end

disp(' ');

for ix = length(names):-1:1
    
    % NOTE: this enforces two naming conventions for MEX files, the first is the current
    
    if ~any(strfind(names{ix}, '_mex.c')) && ~any(strfind(names{ix}, '_.c'))
        names(ix) = [];
    end
    
end

str_line(64, '_');
disp(' ');
disp(['GENERIC build of ''', pwd, ''' ...']);
str_line(64, '_');

opt = build_mex();

if debug
    opt.cflags = '-DDEBUG';
end

for ix = length(names):-1:1
    
    disp(' ');
    disp(['- Building ''', names{ix}, ''' ...']);
    
    fun = file_ext(names{ix}); clear(fun);
    
    try
        % TODO: we use the built-in 'mex' command when linking is not needed
		
        build_mex(names{ix}, linkflags, opt);
    catch
        nice_catch(lasterror); names(ix) = []; %#ok<LERR>
    end
    
end

disp(' ');

%--
% generate install directory and script
%--

destination = [fileparts(pwd), filesep, 'private'];

files = strcat(destination, filesep, file_ext(names, mexext))';

%--
% move compiled files to private folder
%--

if isempty(names)
    files = {}; disp('Nothing to build.'); return;
end

disp('Moving files to private folder.');

movefile(['*.', mexext], destination); delete(['*.', mexext]);


