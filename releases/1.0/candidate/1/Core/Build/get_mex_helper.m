function helper = get_mex_helper(fun, type, name)

% get_mex_helper - function according to convention
% -------------------------------------------------
%
% helper = get_mex_helper(fun, type)
%
% Input:
% ------
%  fun - requesting help
%  type - of help 'mex' or 'cuda' (also 'mex_old')
%
% Output:
% -------
%  helper - handle
%
% NOTE: the convention includes names and path locations
% 
% NOTE: look at the code for a way of overriding the conventional names

%--
% setup persistent store for helpers
%--

persistent HELPERS

if isempty(HELPERS)
	HELPERS = struct;
end

if nargin < 2 || isempty(type)
	field = hash_varname(fun);
else
	field = hash_varname([fun, '::', type]);
end

if isfield(HELPERS, field)
	helper = HELPERS.(field); return;
end

%--
% get location of asking function
%--

[root, fun] = fileparts(fun);

% NOTE: we do this in the case where we are given the simple name

if isempty(root)
	fun = which(fun); [root, fun] = fileparts(fun);
	
	if isempty(root)
		error('Unable to determine function location.');
	end
end

%--
% return helper of specified type
%--

% NOTE: the third input to this function will allow us to override conventional names, it should rarely be used

if nargin < 3
	% NOTE: these are the conventional MEX helper function names
	
	name.mex = [fun, '_mex']; name.cuda = ['cuda_', name.mex]; name.mex_old = [fun, '_'];
end

if nargin > 1 && ~isempty(type)
	% NOTE: the struct packing by type and retrieval of the name implicitly checks type input
	
	helper = get_helper_handle(root, name.(type)); 
	
	if ~isempty(helper)
		HELPERS.(field) = helper;
	end
	
	return;
end

%--
% find best helper available
%--

% NOTE: we first look for a CUDA MEX (when it is available), then a plain MEX, then an old MEX

% NOTE: the types match the fiels of the 'name' struct

if has_cuda
	types = {'cuda', 'mex', 'mex_old'};
else
	types = {'mex', 'mex_old'};
end

for k = 1:numel(types)
	helper = get_helper_handle(root, name.(types{k}));
	
	if ~isempty(helper)
		break;
	end
end

if ~isempty(helper)
	HELPERS.(field) = helper;
end 


%---------------------
% GET_HELPER_HANDLE
%---------------------

function helper = get_helper_handle(root, name)

% when we start we don't have a helper handle

helper = [];

% NOTE: this is the case of a public function, simple name gets handle

if ~isempty(which(name))
	helper = str2func(name); return; 
end

% NOTE: we see whether the function is private, full name is needed to get handle

% NOTE: that the root is part of the code layout convention

curr = pwd; cd(fullfile(root, 'private'));

if ~isempty(which(name))
	helper = str2func(name);
end

cd(curr);
	
	