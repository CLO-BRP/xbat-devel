function types = get_event_extension_data_types

% TODO: this name is too long

persistent TYPES;

if ~isempty(TYPES), types = TYPES;
	
else	
	types = get_extension_types(0, 'target', 'event');

	% NOTE: the above fails to detect 'detector' data, since the 'target' is 'sound' for 'sound_detector'

	types = union(types, 'sound_detector');

	% NOTE: we remove 'action' data from consideration, actions should not store

	types = setdiff(types, {'event_action'}); % NOTE: 'annotation' here is temporary, until it is implemented
	
	TYPES = types;
end