function field = event_table_fields

persistent FIELD;

if ~isempty(FIELD)
	field = FIELD; return; 
end 

% NOTE: these are older names used throughout the code, the translation also requires some value conversion

field.name = {'id', 'guid', 'time', 'duration', 'freq', 'bandwidth', 'channel', 'score', 'created', 'modified'};

% NOTE: the goals here are short and readable names

% NOTE: the order is somewhat in order of relevance

field.column = {'id', 'guid', 'start', 'duration', 'low', 'high', 'channel', 'score', 'created_at', 'modified_at'};

hint = column_type_hints;

field.hint = {hint.integer, hint.string, hint.real, hint.real, hint.real, hint.real, hint.integer, hint.real, hint.timestamp, hint.timestamp};

FIELD = field;