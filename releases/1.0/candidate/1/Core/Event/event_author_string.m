function author = event_author_string(event)

% event_author_string - for menu display
% ---------------------------------------
% 
% author = event_author_string(event)
%
% Input:
% ------
%  event - to consider
% 
% Output:
% -------
%  author - string

%--
% handle multiple events
%--

if numel(event) > 1
	author = iterate(mfilename, event); return;
end

%--
% compute author string
%--

if ischar(event.author)
	author = event.author; return;
end

% NOTE: the first author is the human

author = event.author{1};

if numel(event.author) < 2
	return;
end

% NOTE: the second author is the robot

robot = get_extension_by_classname(event.author{2});	

if isempty(robot)
	return; 
end

author = [author, '  (', robot.name, ')'];


%----------------------------------------
% GET_EXTENSION_BY_CLASSNAME
%----------------------------------------

function ext = get_extension_by_classname(name)

ext = get_extensions('sound_detector');

ix = string_is_member(extension_classname(ext, 1), name);

ext = ext(ix);
