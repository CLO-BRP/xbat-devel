function event = event_specgram(event, sound, context)

% event_specgram - compute event spectrogram
% ------------------------------------------
%
% event = event_specgram(event, sound)
%
% Input:
% ------
%  event - event
%  sound - parent sound
%
% Output:
% -------
%  event - event with spectrogram information

% NOTE: note events are orphaned, we need the sound as input if case we need to get the samples

% TODO: generalize to get event features

%--
% handle multiple events if needed
%--

if numel(event) > 1
	
	event = iterate(mfilename, event, sound, context); return;

end

%--
% get samples if needed
%--

has_samples = isfield(event, 'samples') && ~isempty(event.samples);

if ~has_samples
	
	switch nargin

		case 1, error('Sound input is required if samples are not available.');

		case 2, event = get_event_samples(event, sound);
			
		case 3, event = get_event_samples(event, sound, context);

	end
	
end

%--
% compute spectrogram
%--

% db_disp(['SAMPLES:  ', int2str(numel(event.samples))]);
% 
% stack_disp;
% 
% sound.specgram

[value, freq, time] = fast_specgram(event.samples, event.rate, 'norm', sound.specgram);

%--
% filter image if needed
%--

% NOTE: not sure if we should do this

%--
% get row indices that correspond to event
%--

if isempty(event.freq)
	
	start = 1; stop = size(event.specgram.value, 1);
	
else
	
	start = find(freq >= event.freq(1), 1); stop = find(freq <= event.freq(2), 1, 'last');

	if isempty(start)
		start = 1;
	end

	if isempty(stop)
		stop = size(event.specgram.value, 1);
	end

end

%--
% pack spectrogram information in event
%--

event.specgram.value = value;

event.specgram.freq = freq; event.specgram.time = time;

event.specgram.rows = [start, stop];


