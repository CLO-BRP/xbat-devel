function file = get_event_file(event, sound)

% get_event_file - get file event starts in and when
% --------------------------------------------------
%
% [file, start] = get_event_file(event, sound)
%
% Input:
% ------
%  event - struct
%  sound - containing event
% 
% Output:
% -------
%  file - event starts in
%  start - of event in file

%--
% handle multiple events
%--

if numel(event) > 1
	file = iterate(mfilename, event, sound); return;
end

%--
% get file event starts and file offset in seconds
%--

[file.name, file.position] = get_current_file(sound, event.time(1));

file.start = event.time(1) - get_file_times(sound, file.name);