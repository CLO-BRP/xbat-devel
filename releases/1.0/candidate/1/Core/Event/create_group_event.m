function group = create_group_event(event)

% create_group_event - create hierarchical event from event array
% ---------------------------------------------------------------
%
% event = create_group_event(children)
%
% Input:
% ------
%  children - event

% TODO: consider how this might be integrated into 'event_create'

%--
% check for channel consistency
%--

% NOTE: at the moment we are assuming all children events are in the same channel

if numel(unique([event.channel])) > 1
	error('All events in group must share channel.');
end

%--
% use a duck to get time and frequency bounds
%--

group.time = []; group.freq = []; group.children = event;

time = event_time(group); freq = event_freq(group);

%--
% create group event
%--

group = event_create( ...
	'time', time, ...
	'freq', freq, ...
	'children', event ...
);
	