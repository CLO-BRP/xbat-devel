function field = event_relation_fields

field.name = {'tags', 'notes', 'rating', 'author'}; 

field.column = field.name;

hint = column_type_hints;

field.hint = {hint.string, hint.text, hint.integer, hint.string};
