function page = get_event_page(event, sound, context)

% get_event_page - get event equivalent page along with samples 
% -------------------------------------------------------------
%
% page = get_event_page(event, sound, context)
%
% Input:
% ------
%  event - event
%  sound - parent sound
%  context - context
%
% Output:
% -------
%  page - event equivalent page

%--
% get event page
%--

% NOTE: this essentially consists in mapping event time from 'record' time to 'real' time

event.time = map_time(sound, 'real', 'record', event.time);

page.start = event.time(1); page.duration = event_duration(event);

%--
% read sound page
%--

page = read_sound_page(sound, page, event.channel);

%--
% filter page if needed
%--

if nargin > 2
	page = filter_sound_page(page, context);
end
