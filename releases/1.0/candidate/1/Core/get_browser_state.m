function state = get_browser_state(par, data)

% get_browser_state - get browser display state
% ---------------------------------------------
%
% state = get_browser_state(par, data)
%
% Input:
% ------
%  par - browser
%  data - userdata
%
% Output: 
% -------
%  state - display state

% NOTE: browser state is 'split' between this display state and the sound 'state'

% NOTE: sounds and logs try to behave as annotated books in a library

%---------------------------------
% HANDLE INPUT
%---------------------------------
	
%--
% get active browser
%--

if ~nargin
	
	par = get_active_browser;
	
	if isempty(par)
		state = []; return;
	end
	
end

%--
% get browser state if needed
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end
	
%---------------------------------
% GET SCREEN-SIZE POSITION
%---------------------------------

% NOTE: the handling of the screensize in 'set' could use some work

state.screensize = get(0, 'screensize');

state.position = get(par, 'position');

%---------------------------------
% GET CHILDREN STATES
%---------------------------------

%--
% get open children states
%--

[state.children, widget_states] = get_browser_children(par);

%--
% get stored children states
%--

% NOTE: these are states for palettes and widgets that are not currently open

state.palette_states = data.browser.palette_states;

state.widget_states = update_widget_states(data.browser.widget_states, widget_states);

%---------------------------------
% GET OPEN LOGS STATE
%---------------------------------

switch length(data.browser.log)

	case 0
		
		state.log = [];

	case 1
		
		state.log.names = log_name(data.browser.log);
        
        if ~iscell(state.log.names)
            state.log.names = {state.log.names};
        end
		
		state.log.ids = data.browser.log.id;
		
		state.log.active = data.browser.log_active;

	otherwise
		
		state.log.names = file_ext(struct_field(data.browser.log, 'file'));
		
		state.log.ids = struct_field(data.browser.log, 'id');
		
		state.log.active = data.browser.log_active;
		
end

% TODO: save and load extension states when presets mature

%---------------------------------
% GET SELECTION AND MARKER
%---------------------------------

% TODO: the selection event is possibly linked to a log, problems?

state.selection = data.browser.selection;

state.marker = data.browser.marker;


%---------------------------------
% GET_BROWSER_CHILDREN
%---------------------------------

function [children, widget_states] = get_browser_children(par)

% get_browser_children - get browser children and their state
% -----------------------------------------------------------
%
% [children, widget_states] = get_browser_children(par)
%
% Input:
% ------
%  par - browser
%
% Output:
% -------
%  children - children states
%  widget_states - states of non-widget widgets

%--
% get children figures
%--

handles = get_xbat_figs('parent', par);

%--
% get required state info
%--

children = [];

for k = 1:length(handles)
	
	handle = handles(k);
	
	child.handle = handle;
	
	%--
	% get name and tag
	%--
	
	% NOTE: these are typically sufficient to recreate the child
	
	child.name = get(handle, 'name');
	
	tag = get(handle, 'tag');
	
	child.tag = tag;
	
	%--
	% get state information
	%--
	
	% NOTE: these are used to position the child
	
	info = parse_tag(tag, '::', {'type', 'subtype', 'name'});

	child.type = info.type;
	
	child.parent.name = info.name;
	
	child.parent.type = lower(info.subtype);
	
	if strcmpi(info.type, 'XBAT_PALETTE')
		
		state = get_palette_state(handle);
		
		child.position = state.position;
		
		child.toggle = state.toggle;
		
		child.tabs = state.tabs;
		
	else
		
		child.position = get(handle, 'position');
		
		child.toggle = [];
		
		child.tabs = [];
		
	end
	
	if isempty(children)
		children = child;
	else
		children(end + 1) = child;
	end
	
end

%--
% remove non-widget widgets from child list
%--

widget_states = [];

for k = length(children):-1:1
	
	% NOTE: we get widget states and remove non-widget widgets, these depend on active states
	
	if strcmpi(children(k).type, 'XBAT_WIDGET') && ~strcmpi(children(k).parent.type, 'WIDGET')
		
		widget_state = get_widget_state(children(k).handle);
		
		if isempty(widget_states)
			widget_states = widget_state;
		else
			widget_states(end + 1) = widget_state;
		end
		
		children(k) = [];
		
	end
	
end

%--
% remove handles from children states
%--

if ~isempty(children)
	children = rmfield(children, 'handle'); 
end


%---------------------------------
% UDPATE_WIDGET_STATES
%---------------------------------

function widget_states = update_widget_states(widget_states, update)

%--
% set states to update if initial array is empty
%--

if isempty(widget_states)
	widget_states = update; return;
end

%--
% get widget names in current states array
%--

names = {widget_states.name};

%--
% consider update states, adding new ones and updating existing ones
%--

for k = 1:length(update)
	
	ix = find(strcmp(names, update(k).name));
	
	% NOTE: we silently select the first index if we get multiple hits
	
	if isempty(ix)
		widget_states(end + 1) = update(k);
	else
		widget_states(ix(1)) = update(k);
	end
	
end

