function root = splash_root(root)

if nargin
	if ~exist(root, 'dir')
		error(['Proposed root for splash images ''', root, ''' does not exist.']);
	end
	
	set_env('splash_root', root);
else
	root = get_env('splash_root');
end