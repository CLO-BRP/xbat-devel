function y = vario_linear(h,c0,b)

% vario_linear - linear variogram model
% -------------------------------------
%
% y = vario_linear(h,c0,b)
%   = vario_linear(h,p)
%
% Input:
% ------
%  h - points of evaluation
%  c0,b - model parameters 
%  p - model parameters (p.c0,p.b)
%
% Output:
% -------
%  g - variogram values
%

%--
% unpack parameters
%--

if (nargin < 3)
	b = c0.b;
	c0 = c0.c0;
end

%--
% compute function
%--

y = c0 + (b * h);
