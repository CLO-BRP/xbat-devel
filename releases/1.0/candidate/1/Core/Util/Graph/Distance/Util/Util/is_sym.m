function result = is_sym(A, tol)

% is_sym - test for matrix symmetry
% ---------------------------------
%
% result = is_sym(A, tol)
%
% Input:
% ------
%  A - candidate matrix
%  tol - tolerance for approximate test
%
% Output:
% -------
%  result - of test

if size(A, 1) ~= size(A, 2)
	
	result = false;
else
	
	if nargin > 1
		result = max(max(abs(A - A'))) < tol;
	else
		result = all(all(A == A'));
	end
end

