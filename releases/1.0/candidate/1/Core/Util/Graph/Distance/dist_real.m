function D = dist_real(x, y, fun)

% dist_real - distance matrix for reals
% -------------------------------------
%
% D = dist_real(x, y, fun)
%
% Input:
% ------
%  x - real numbers, row index
%  y - real numbers, column index
%  fun - distance defining difference map (def: @abs)
%
% Output:
% ------
%  D - distance matrix

%--
% handle input
%--

if nargin < 3
	fun = @abs;
end

if nargin < 2 || isempty(y)
	y = x;
end

%--
% compute differences
%--

% NOTE: this is simple special case of the difference based computations

D = repmat(x(:), [1, numel(y)]) - repmat(y(:)', [numel(x), 1]);

%--
% apply distance defining difference map if available
%--

if isempty(fun)
	return;
end

% NOTE: we allow for callback type parametrized maps

if iscell(fun)
	D = fun{1}(D, fun{2:end});
else
	D = fun(D);
end

