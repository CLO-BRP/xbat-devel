function test_fast_nearest(type)

%--
% handle input
%--

if ~nargin
	type = {'hamming', 'absolute', 'euclidean'};
end 

if ischar(type)
	type = {type};
end

%--
% run tests
%--

for j = 1:numel(type)
	
	disp(' ');
	disp(upper(type{j}));
	disp(' ');
	
	for k = 2:5:100
		
		X = randn(k, 400); Y = randn(k, 600); failure = false;
		
		try
			start = clock;
			
			D = fast_distance(X, Y, type{j});
			
			% NOTE: this is how we 'collapse' the full distance matrix to get the smallest distance
			
			[Dx(:, 1), Dx(:, 2)] = min(D, [], 2); [Dy(:, 1), Dy(:, 2)] = min(D, [], 1);
			
% 			db_disp; Dx, Dy
			
			elapsed1 = etime(clock, start);
		catch
			failure = true; elapsed1 = inf; nice_catch;
		end
		
		try
			start = clock;
			
			[Nx, Ny] = fast_nearest(X, Y, type{j});
			
% 			db_disp; Nx, Ny

			elapsed2 = etime(clock, start);
		catch
			failure = true; elapsed2 = inf; nice_catch;
		end
		
		% report
		
		passed = ~failure && (max(abs(vec(Nx - Dx))) < eps) && (max(abs(vec(Ny - Dy))) < eps);
		
		disp(['d = ', int2str(k), ', passed = ', int2str(passed), ', elapsed = [', sec_to_clock(elapsed1), ', ', sec_to_clock(elapsed2) '], speed = ', num2str(elapsed1/elapsed2)]);
	end
end

disp(' ');


