function z = circle(n, r, t)

% circle - compute positions on circle
% ------------------------------------
% 
% z = circle(n, r)
%
% Input:
% ------
%  n - number of positions
%  r - radius
%
% Output:
% -------
%  z - complex points on circle

if nargin < 3
	t = [0, 2 * pi];
end

%--
% compute angle sequence
%--

t = linspace(t(1), t(2), n + 1); t = t(1:n)';

%--
% compute points
%--

z = r * exp(i * t);
