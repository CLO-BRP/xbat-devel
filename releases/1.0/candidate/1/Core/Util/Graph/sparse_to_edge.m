function [E, W] = sparse_to_edge(A)

% sparse_to_edge - sparse matrix to edge list representation of graph
% -------------------------------------------------------------------
%
% [E, W] = sparse_to_edge(A)
%
% Input:
% ------
%  A - sparse matrix
%
% Output:
% -------
%  E - vertex edge list
%  W - vertex edge weights

%--
% check proposed adjacency matrix
%--

[m, n] = size(A);

if m ~= n
	error('Proposed adjacency matrix is not square.');
end

%--
% consider whether weight output was requested
%--

if nargout < 2
	
	E = cell(n, 1);

	for k = 1:n
		E{k} = find(A(k, :));
	end

else
	
	E = cell(n, 1); W = cell(n, 1);

	for k = 1:n
		[ignore, E{k}, W{k}] = find(A(k, :));
	end
	
end













