function handle = components_view(G, label, ax)


% TODO: factor and generalize. essentially we have 'positions', 'values', and 'edges' to consider

%--
% set up axes if not provided
%--

% TODO: provide axes as input

if nargin < 3
	par = fig; ax = axes('parent', par);
end

%--
% scatter vertex points
%--

for k = 1:size(G.X, 1)
	handle.point(k) = line(G.X(k, 1), G.X(k, 2), 'marker', 'o', 'linestyle', 'none', 'parent', ax);
end

% NOTE: set 'axes' properties for display

axis('equal');

set(ax, 'visible', 'off');

if nargin < 3
	set(ax, 'units', 'normalized', 'position', [0, 0, 1, 1]);
end

hold on;

%--
% compute connected components
%--

% NOTE: the number of components is the maximum componenent number

[step, comp, branch] = graph_dfs(G); nc = max(comp);

C = hsv(nc); C = C(randperm(nc), :);

for k = 1:size(G.X, 1)
	
	% NOTE: display labels here if we have them, otherwise display step in DFS
	
	if nargin > 1 && ~isempty(label)
		
		if isnumeric(label)
			handle.text(k) = text(G.X(k, 1) + 0.05, G.X(k, 2), int2str(label(k)));
		else
			handle.text(k) = text(G.X(k, 1) + 0.05, G.X(k, 2), label{k});
		end
		
	else
		
		handle.text(k) = text(G.X(k, 1) + 0.05, G.X(k, 2), int2str(step(k)));
		
	end
	
	set(handle.text(k), 'fontsize', 9, 'color', 0.6 * ones(1, 3));
	
	% NOTE: we display branchpoints in the DFS as larger nodes
	
	if branch(k) > 0
		set(handle.point(k), 'markersize', 12);
	end
	
end

set(handle.text, 'hittest', 'off');

%--
% display components
%--

A = edge_to_sparse(G.E);

for k = 1:nc
	ix = find(comp == k); [x, y] = gplot(A(ix, ix), G.X(ix, :)); handle.line(k) = line('parent', ax, 'xdata', x, 'ydata', y, 'color', C(k, :));
end

set(handle.line, 'hittest', 'off');

for k = 1:length(handle.point)
	
	set(handle.point(k), ...
		'markersize', get_scaled_size(sum(A(:, k))), ...
		'markerfacecolor', C(comp(k), :), ...
		'markeredgecolor', ones(1, 3) ...
	);

	set(handle.point(k), 'buttondownfcn', {@highlight, handle.text(k)});
	
end

%--
% set figure color, doing this when we are done serves as 'refresh'
%--

set(gcf, 'color', 0 * ones(1, 3));


%----------------------
% GET_SCALED_SIZE
%----------------------

function value = get_scaled_size(value, scale, base) %#ok<DEFNU>

%--
% handle input
%--

if issparse(value)
	value = full(value);
end

if nargin < 3
	base = 8;
end

% TODO: we should scale based on the size of the graph

if nargin < 2
	scale = 6;
end 

%--
% get size using value
%--

value = base * (1 + 2 * (value.^2 ./ (scale^2 + value.^2)));


%----------------------
% HIGHLIGHT
%----------------------

function highlight(obj, eventdata, label)

data = get(label, 'userdata'); selected = ~isempty(data);

if selected
	set(obj, 'markersize', str2int(data)); set(label, 'backgroundcolor', 'none', 'userdata', ''); uistack(label, 'top');
else
	base = get(obj, 'markersize'); set(obj, 'markersize', 1.5 * base); set(label, 'backgroundcolor', [1 1 0], 'userdata', int2str(base));
end
	

