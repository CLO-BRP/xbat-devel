function result = append_resize(handle, fun)

% append_resize - append figure resize function array
% ---------------------------------------------------
%
% result = append_resize(handle, fun)
%
% Input:
% ------
%  handle - figure
%  fun - resize function
%
% Output:
% -------
%  result - length of resize function array

result = add_callback_function(handle, 'resizefcn', fun);

