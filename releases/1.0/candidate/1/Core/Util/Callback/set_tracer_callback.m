function [tracer, wrapped] = set_tracer_callback(handle, wrap)

% set_tracer_callback - set tracer callback for object
% ----------------------------------------------------
%
% [tracer, wrapped] = set_tracer_callback(handle, wrap) 
%
% Input:
% ------
%  handle - handle
%  wrap - modify non-trivial callbacks
%
% Output
% ------
%  tracer - callbacks added or modified
%  wrapped - indicator

% TODO: extend to wrap callback and provide full tracing

% TODO: factor callback wrapping

% TODO: develop a way of removing tracer callbacks

%------------------
% HANDLE INPUT
%------------------

%--
% set no wrap default, this leaves existing callbacks unchanged
%--

if nargin < 2
	wrap = 0;
end

%--
% handle multiple handles
%--

if numel(handle) > 1

	if nargout < 2
		tracer = iterate(mfilename, handle, wrap); 
	else
		[tracer, wrapped] = iterate(mfilename, handle, wrap);
	end 

	return; 
	
end

%--
% get callbacks
%--

[ignore, field, value] = get_callbacks(handle);

%--
% set tracer callback on callbacks not used
%--

tracer = {}; wrapped = [];

for k = 1:numel(field)
	
	if isempty(value{k})
		set(handle, field{k}, {@tracer_callback, field{k}}); tracer{end + 1} = field{k};
	end 

end

%--
% output wrapped indicator
%--

if ~wrap && nargout > 1
	wrapped = zeros(size(tracer));
end





