function value = is_parametrized(callback) 

% is_parametrized - shortcut to indicate parametrized callback 
% ------------------------------------------------------------
%
% value = is_parametrized(callback)
%
% Input:
% ------
%  callback - callback
%
% Output:
% -------
%  value - indicator

% NOTE: the function that actually evaluates the type in 'is_callback' is named similarly

[value, type] = is_callback(callback);

if ~value
	return;
end

value = strcmp(type, 'parametrized'); 