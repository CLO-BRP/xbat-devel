function value = is_m2str(str)

% NOTE: this is a cheap weak test

value = strcmp(str(1:3), '<ma') || strcmp(str(1:3), '<ce');