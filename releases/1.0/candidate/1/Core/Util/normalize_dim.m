function A = normalize_dim(A, k, opt)

% normalize_dim - center and normalize dimension
% ----------------------------------------------
%
% A = normalize_dim(A, k, opt)
%
% Input:
% ------
%  A - array to normalize
%  k - dimension to normalize
%
% Output:
% -------
%  A - normalized array
%
% NOTE: for the sum normalization (opt.norm = 1) we assume that array values are positive

%--
% set default normalization options
%--

if nargin < 3
	
	opt.center = 1; opt.normalize = 1; opt.norm = 2;
	
	if ~nargin
		A = opt; return;
	end
	
end

%--
% set default dimension to normalize
%--

if (nargin < 2) || isempty(k)
	k = 1;
end

%--
% get dimensions and check input
%--

d = ndims(A); 

if k > d
	error('Requested dimension to normalize is not available.');
end

%--
% get repeat vector to reconstitute collapsed array
%--

m = size(A, k); rep = ones(1, d); rep(k) = m;

%--
% normalize array dimension
%--

% NOTE: centering is typical of the euclidean normalization

if opt.center
	
	A = A - repmat(mean(A, k), rep);

end

if opt.normalize
	
	switch opt.norm
		
		% NOTE: we assume that for the sum normalization the values are positive
		
		case 1
			N = sum(A, k);
			
		case 2
			N = sqrt(sum(A.^2, k));
			
		case inf
			N = max(A, [], k);
			
		otherwise
			N = sum(abs(A).^opt.norm, k).^(1 / opt.norm);
			
	end
	
	A = A ./ repmat(N, rep);
	
end
