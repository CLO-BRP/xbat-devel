function store = app_db(app, store)

% app_db - set and get application DB store
% -----------------------------------------
%
% store = app_db(app, store)
%
% Input:
% ------
%  app - name
%  store - description
% 
% Output:
% -------
%  store - current

% TODO: consider some further output

% NOTE: ensure that the store is proper when setting, something more sophisticated is needed

if nargin
	helper = app_helper(app, 'establish_schema');
	
	if ~isempty(helper)
		helper(store);
	end
end

%--
% set or get
%--

switch nargin
	case 0
		store = get_env('local_barn_store');

	case 1
		set_env('local_barn_store', store);
end

%--
% display if output not captured
%--

if ~nargout
	disp(store); clear store;
end
