function test_cuda_window(n, i, w, o)

% test_cuda_window
% ----------------
% 
% test_cuda_window(n, i, w, o)
%
% Input:
% ------
%  n - size of 'rand' matrix, default = 100
%  i - number of iterations, default = 10
%  w - window width, default = 10
%  o - overlap, default = 0
%

%--
% handle input
%--
if nargin < 1
	n = 100;
end

if nargin < 2
	i = 10;
end 

if nargin < 3
	w = 10;
end

if nargin < 4
	o = 0;
end

if w < 0
	die;
end

if o >= w
	die;
end

X = single(rand(n, 1));

l = (n - w) / (w - o) + 1;

%%--
%% Compute in Matlab
%%--

h0 = zeros(l, 1);
for k = 1:l
	lo = int32((k - 1) * (w - o)) + 1;
	hi = int32((k - 1) * (w - o) + w);
	h0(k, 1) = sum(X(lo:hi, 1));
end
disp(h0);
disp(' ');

%%--
%% Compute in CUDA MEX
%%--

for k = 1:i
	h1 = cuda_window_mex(X, w, o);
end
disp(h1);

%%--
%% Compare results
%%-- 

if max(abs(h0 - h1)) > 0.01
	disp(upper(['Failed test !!!!!!!!!!!']));
else
	disp(['Passed test']);
end

disp(' ');