function [U, L] = real_envelope(X, opt)

% real_envelope - compute real envelope of real signal
% ----------------------------------------------------
%
% [U, L] = real_envelope(X, opt)
%
%    opt = real_envelope
%
% Input:
% ------
%  X - signal 
%  opt - envelope computation options
%
% Output:
% -------
%  U - upper envelope
%  L - lower envelope
%  opt - default envelope
%
% NOTE: currently being used as helper for Scope widget, not a proper envelope computation!

% NOTE: each column is interpreted as a signal channel

%--
% set and possibly output default options
%--

if nargin < 2

	opt.smooth = 0; opt.method = 'cubic'; opt.pad = 0.1;

	% NOTE: return default options if asked to
	
	if ~nargin && nargout
		U = opt; return;
	end
	
end

%--
% align row input 
%--

% NOTE: this kind of flexibility could lead to confusion, but it is convenient

flip = size(X, 1) == 1;

if flip
	X = X(:);
end

%--
% compute upper envelope
%--

n = size(X, 2);

for k = 1:n	
	U(:, k) = get_envelope(X(:, k), 1, opt);
end

if flip
	U = U';
end

%--
% compute lower envelope
%--

if nargout < 2
	return;
end

for k = 1:n	
	L(:, k) = get_envelope(X(:, k), -1, opt);
end

if flip
	L = L';
end


%-------------------------------
% GET_ENVELOPE
%-------------------------------

function e = get_envelope(y, type, opt)

%--
% get signal length
%--

n = length(y);

%--
% get extrema indices of smoothed signal
%--

if opt.smooth
	yy = linear_filter(y, [0.25, 0.5, 0.25]'); ix = fast_peak_valley(yy, type); r = fast_range(yy);
else
	ix = fast_peak_valley(y, type); r = fast_range(y);
end

%--
% expand extrema if needed and interpolate to native rate
%--

if opt.pad
	y(ix) = y(ix) + opt.pad * r * type;
end

% NOTE: the transpose makes the output a column vector

% e = interp1(ix, y(ix), 1:n, opt.method)';

ix = [1, ix, n];
	
e = interp1(ix, y(ix), 1:n, opt.method)';


