function [A, M] = fast_amplitude(X, window, overlap, mode)

% fast_amplitude - computation
% ----------------------------
%
% [A, M] = fast_amplitude(X, window, overlap, mode)
%
% Input:
% ------
%  X - signal, one channel per column
%  window - for frame, determines frame size
%  overlap - for frame in samples
%  mode - 'abs' or 'rms' (def: 'abs')
%
% Output:
% -------
%  A - positive and negative means
%  M - positive and negative extremes
%
% NOTE: for a single channel A and M are two column matrices

%--
% handle input
%--

if nargin < 4
	mode = 'abs';
end

if nargin < 3 || isempty(overlap)
	overlap = 0;
end

if nargin < 2 || isempty(window)
	window = ones(1, 100);
end

if isscalar(window)
	window = ones(1, window);
end

%--
% compute amplitude
%--

% TODO: consider how we pack the MEX output, try to make layout relatively uniform for both modes

nch = size(X, 2);

switch mode
	
	case 'rms'
		
		% NOTE: square signal to compute RMS, this should happen in-place
		
		X = X.^2;
		
		A = cell(1, nch); M = cell(1, nch);
		
		% NOTE: the MEX file computes block-averages for positive and negative values
		
		% NOTE: in this case only one column from each cell is informative
		
		for k = 1:nch
			
			% TODO: we should also take care of the double to single conversion for the input
			
			if cuda_enabled
				[A{k}, M{k}] = cuda_amplitude_mex(X(:, k), length(window), overlap);
			else
				[A{k}, M{k}] = fast_amplitude_mex(X(:, k), window, overlap);
			end
			
			A{k} = A{k}(:, 2); M{k} = M{k}(:, 2);
		end
		
		% NOTE: the column index becomes the channel index
		
		A = sqrt([A{:}]); M = sqrt([M{:}]);
		
	case 'abs'
		
		% NOTE: the 'sheet' index becomes the channel index
		
		% NOTE: nomenclature for three dimensional matrices: rows, columns, and sheets
		
		for k = 1:nch
			if cuda_enabled
				[A(:,:,k), M(:,:,k)] = cuda_amplitude_mex(X(:, k), length(window), overlap);
			else
				[A(:,:,k), M(:,:,k)] = fast_amplitude_mex(X(:, k), window, overlap);
			end
		end
		
	otherwise
		
		error('Unrecognized amplitude computation mode.')
		
end

% TODO: we should definitely take care of this in the CUDA mex

if cuda_enabled
	
	A = double(A); M = double(M);
end
