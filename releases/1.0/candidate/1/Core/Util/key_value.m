function value = key_value(key, value)

% key_value - persistent key value store
% --------------------------------------
%
% value = key_value(key, value)
%
% Input:
% ------
%  key - value
%  value - to store
%
% Output:
% -------
%  value - stored

%--
% setup persistent store
%--

persistent CACHE;

if isempty(CACHE)
	CACHE = struct;
end

%--
% get key and retrieve or store
%--

% NOTE: the prefix 'K' is required to ensure a proper fieldname

field = ['K', md5(key)];

switch nargin
	
	case 1
		if isfield(CACHE, field)
			value = CACHE.(field);
		else
			value = [];
		end
		
	case 2
		CACHE.(field) = value;
		
end