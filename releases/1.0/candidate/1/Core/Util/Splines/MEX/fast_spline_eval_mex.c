#include "mex.h"

#include "fast_spline_eval.c"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    double * s, * b, *t; 
    
    int nB, nT;
    
    /*
     * get input coefficients and length
     */
    
    b = (double *) mxGetPr(prhs[0]);
    
	nB = (int) (mxGetM(prhs[0]) * mxGetN(prhs[0]));
    
    
    /*
     * get input time grid and length
     */
    
    t = (double *) mxGetPr(prhs[1]);
    
	nT = (int) (mxGetM(prhs[1]) * mxGetN(prhs[1]));    
    
    /*
     * allocate output
     */
    
    s = mxCalloc(nT, sizeof(double));
    
    /*
     * compute spline coefficients
     */
    
    fast_spline_eval(s, t, nT, b, nB);
    
    /*
     * write output data
     */
    
    plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);
    
    mxSetM(plhs[0],nT); mxSetN(plhs[0],1);
    
    mxSetPr(plhs[0],s);
    
}
