function s = fast_spline_eval(b, t)

s = fast_spline_eval_mex(b, t - 1);