function str = isodate(value, fractional)

% isodate - iso 8601 date string for numerical date value
% -------------------------------------------------------
%
% str = isodate(value, fractional)
%
% Input:
% ------
%  value - valid input to 'datestr'
%  fractional - include fractional second information (def: 0)
% 
% Output:
% -------
%  str - date strings in some ISO 8601 type format

% NOTE: the formats produced plays well with SQLite

% http://www.sqlite.org/cvstrac/wiki/wiki?p=DateAndTimeFunctions

%--
% handle input
%--

if nargin < 2
	fractional = 0; 
end

if ~ischar(value) && (numel(value) > 1)
	str = iterate(mfilename, value, fractional); return;
end

%--
% get desired date string
%--

str = datestr(value, isodate_format(fractional));


	