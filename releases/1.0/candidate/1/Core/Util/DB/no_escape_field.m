function value = no_escape_field(field)

% TODO: consider which fields, and whether we are escaping in the right place

value = string_is_member(field, {'email', 'url', 'organization', 'path', 'date', 'created', 'modified', 'created_at', 'modified_at', 'updated'});