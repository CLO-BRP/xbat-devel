function set_taggings(store, obj, tags, user, table)

% set_taggings - tag objects in database
% --------------------------------------
%
% set_taggings(store, obj, tags, user, table)
%
% Input:
% ------
%  store - database
%  obj - to tag
%  tags - to set
%  user - tagging
%  table - name
%
% See also: get_taggings

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% typically reflect table from input name
%--

if nargin < 5
	table = inputname(2);
end

if nargin < 4
	user = set_barn_user(store);
end

% NOTE: the tests for empty 'obj' result as this function is used to simply add tags to the 'tag' table

if isempty(table) && ~isempty(obj)
	error('Table to tag is not available explicitly or implicitly from input.');
end

%--
% match tags and objects if needed
%--

if ~isempty(obj) && ~ischar(tags) && ~iscellstr(tags) && (numel(tags) ~= numel(obj))
	error('Tags input must be ''scalar'' or match the number of objects to tag.');
end

%---------------------------
% SETUP
%---------------------------

%--
% make sure all proposed tags are in database
%--

[proposed, type] = unique_tags(tags); known = get_taggings(store);

missing = setdiff(proposed, {known.name});

if ~isempty(missing)
	
	for k = 1:numel(missing)
		append(k).name = missing{k};
	end
	
	set_database_objects(store, append, [], {'name'}, 'tag');
	
	if isempty(obj)
		return;
	end
	
	known = get_taggings(store); 
	
end

%--
% get object identifiers
%--

% NOTE: we accept identified object array or identifier array input

if isstruct(obj)
	id = get_id_field(obj);
else
	id = obj;
end

%---------------------------
% SET TAGGINGS
%---------------------------

%--
% get current object tags
%--

current = get_taggings(store, id, user, table);

%--
% compare proposed and current tags
%--

% NOTE: in this case the string contains some tags to be proposed for all

if strcmp(type, 'char')
	tags = proposed; 
end

if ~strcmp(type, 'batch')
	tags_id = get_tag_id(tags, known);
end

for k = 1:numel(obj)
	
	switch type
		
		case 'char'
			
			if isempty(current{k})
				add{k} = tags_id; remove{k} = [];
			else
				add{k} = get_tag_id(setdiff(tags, current{k}), known); remove{k} = get_tag_id(setdiff(current{k}, tags), known);
			end
			
		case 'cell'
			
			if isempty(current{k})
				add{k} = tags_id; remove{k} = [];
			else
				add{k} = get_tag_id(setdiff(tags, current{k}), known); remove{k} = get_tag_id(setdiff(current{k}, tags), known);
			end
			
		case 'batch'
			
			if isempty(current{k})
				add{k} = get_tag_id(tags{k}, known); remove{k} = [];
			else
				add{k} = get_tag_id(setdiff(tags{k}, current{k}), known); remove{k} = get_tag_id(setdiff(current{k}, tags{k}), known);
			end
			
	end
	
end

%--
% insert and delete taggings as needed
%--

% INSERT

t.tag_id = 1; t.taggable_type = 'string'; t.taggable_id = 1;

t = empty(t); i = 1;

for j = 1:numel(id)
	
	for k = 1:numel(add{j}) 
		
		t(i).tag_id = add{j}(k); 
		
		t(i).taggable_type = table; 
		
		t(i).taggable_id = id(j);
		
		t(i).user_id = user.id; 
		
		i = i + 1;
		
	end
	
end

set_database_objects(store, t, [], fieldnames(t), 'tagging');

% DELETE

sql{1} = 'BEGIN;'; 

for j = 1:numel(id)
	
	if isempty(remove{j})
		continue;
	end
	
	sql{end + 1} = delete_taggings_as_relation([], id(j), table, remove{j}, user.id);
	
end

sql{end + 1} = 'COMMIT;';

if numel(sql) < 3
	return;
end

query(store, sql);

