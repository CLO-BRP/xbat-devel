function [tags, type] = unique_tags(tags)

% TODO: use 'str_to_tags' to validate and convert string input

%--
% consider tag string and tags cell input
%--

type = 'cell';

% NOTE: we allow splitting by space or comma

if ischar(tags)
	if any(tags == ',')
		tags = str_split(tags, ',');
	else
		tags = str_split(tags, ' '); 
	end

	type = 'char';
end

if iscellstr(tags)
	tags = unique(tags); return;
end

%--
% consider cell of tag strings or tag cells input 
%--

if ~iscell(tags)
	error(str);
end

type = 'batch';

batch = tags; tags = {};

for k = 1:numel(batch)
	
	current = batch{k};
	
	if ischar(current)
		current = str_split(current, ' ');
	end
	
	tags = union(tags, current);
	
end