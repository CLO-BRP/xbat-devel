function [value, column] = table_has_column(file, table, name)

column = get_table_columns(file, table);

value = string_is_member(name, column);
