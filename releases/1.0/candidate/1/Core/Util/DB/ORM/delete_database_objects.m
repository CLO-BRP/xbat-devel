function [status, result] = delete_database_objects(store, obj, table, opt)

% delete_database_objects - by identifier and table
% -------------------------------------------------
%
% [status, result] = delete_database_objects(store, obj, table, opt)
%
% Input:
% ------
%  store - database
%  obj - set or identifiers
%  table - name
%  opt - options
%
% Output:
% -------
%  status - of query
%  result - of query
%  opt - default options
%
% See also: get_database_objects

% TODO: allow this function to remove related objects, like 'load_relations' in 'get_database_objects

% NOTE: we can do this in a transaction for integrity and efficiency

if nargin < 4
	opt.delete_relations = {};
	
	opt.foreign_key = {};
	
	if ~nargin
		status = opt; return;
	end
end

%--
% typically inspect table name
%--

if nargin < 3
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%--
% delete objects using identifier
%--

% NOTE: we expect objects or an identifier vector

if isstruct(obj)
	id = [obj.id]; 
else
	id = obj;
end 

if numel(id) == 1
	condition = [' = ', int2str(id), ';'];
else
	condition = [' IN (', str_implode(id, ', ', @int2str), ');'];
end

if isempty(opt.delete_relations)

	sql = ['DELETE FROM ', table, ' WHERE id ', condition];
else
	% TODO: consider factoring this foreign key bit below
	
	% NOTE: if foreign keys in relation tables are not provided we set a generator
	
	if isempty(opt.foreign_key)
		opt.foreign_key = @(table, related)([table, '_id']);
	end
	
	% NOTE: if the foreign key description is a helper, we apply it here
	
	% NOTE: the helper takes the current table and related table as input
	
	if isa(opt.foreign_key, 'function_handle')
		
		helper = opt.foreign_key; opt.foreign_key = cell(size(opt.delete_relations)); 
		
		for k = 1:numel(opt.delete_relations)
			opt.foreign_key{k} = helper(table, opt.delete_relations{k});
		end
	end 
	
	% NOTE: by the time we get here, foreign keys are strings
	
	sql = {'BEGIN;'};
	
	for k = 1:numel(opt.delete_relations)
		sql{end + 1} = ['DELETE FROM ', opt.delete_relations{k}, ' WHERE ', opt.foreign_key{k}, condition]; %#ok<AGROW>
	end
	
	sql{end + 1} = {'COMMIT;'};
end

%--
% submit query
%--

[status, result] = query(store, sql);

