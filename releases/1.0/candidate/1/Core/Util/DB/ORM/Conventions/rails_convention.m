function [foreign, relation] = rails_convention(columns, candidate)

% rails_convention - for foreign key discovery
% --------------------------------------------
%
% [foreign, relation] = rails_convention(columns, candidate)
%
% Input:
% ------
%  columns - to consider
%  candidate - tables referenced
%
% Output:
% -------
%  foreign - key declarations
%  relation - description for keys
%
% NOTE: the Rails foreign-key convention is configurable, this implements the default convention
% 
% See also: create_table

%--
% handle input
%--

if nargin < 2
	candidate = {};
end

foreign = {}; relation = empty(struct('foreign_key', [], 'table', [], 'column', []));

for k = 1:numel(columns)
	
	current = columns{k};
	
	if numel(current) < 3 || ~strcmp(current(end - 2:end), '_id')
		continue;
	end
	
	table = current(1:end - 3);
	
	if ~isempty(candidate) && ~string_is_member(table, candidate)
		continue;
	end
	
	foreign{end + 1} = ['FOREIGN KEY (', current, ') REFERENCES ' table, '(id)']; %#ok<AGROW>
	
	if nargout > 1
		relation(end + 1) = struct('foreign_key', current, 'table', table, 'column', 'id'); %#ok<AGROW>
	end
end

if ~nargout
	
	for k = 1:numel(foreign)
		disp(foreign{k});
	end
end