function obj = set_database_objects(file, obj, id, fields, table, resolve)

% set_database_objects - store struct array to database
% -----------------------------------------------------
%
% obj = set_database_objects(file, obj, id, fields, table, resolve)
%
% Input:
% ------
%  file - database
%  obj - to store
%  id - identifier
%  fields - to store
%  table - name
%  resolve - conflict algorithm
%
% Output:
% -------
%  obj - retrieved from database

%-------------------
% HANDLE INPUT
%-------------------

%--
% set replace default for conflict resolution
%--

% NOTE: this is the most aggressive conflict resolution algortithm 

if nargin < 6
	resolve = 'REPLACE';
end

%--
% reflect for table name if needed
%--

if nargin < 5 || isempty(table)
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%--
% set full objects default
%--

if nargin < 4 || isempty(fields)
	fields = fieldnames(obj);
end

%--
% set new objects default
%--

if nargin < 3
	id = []; 
end

new = isempty(id);

%-------------------
% SETUP
%-------------------

% TODO: we should be able to turn off these checks

%--
% check for objects table if needed
%--

% NOTE: the first clause in the test check for old SQLite centered usage

if ischar(file) && ~isempty(file) && ~database_has_table(file, table)
	
	error(['Database does not have a ''', table, ''' table.']);
end

% TODO: check for columns 

%-------------------
% SET AND GET
%-------------------

opt = sqlite_array_insert; opt.resolve = resolve;

%--
% append or update objects based on identifier availability 
%--

% NOTE: when we create new objects we do not have an identifier

if ~isempty(id)
	
	if numel(obj) ~= numel(id) 
		error('Number of objects must match number of identifiers.');
	end
	
	if ~string_is_member('id', fields)
		fields{end + 1} = 'id';
	end
	
	for k = 1:numel(obj)
		obj(k).id = id(k);
	end
	
	% NOTE: when the objects have more fields than fields are requested to set we update
	
	if ~isempty(setdiff(fieldnames(obj), fields))
		opt.update = 1;
	end
end

% NOTE: this function outputs a query, it should perhaps be renamed

sql = sqlite_array_insert(table, obj, fields, opt); 

% NOTE: when there is nowhere to put the objects we output the query and return

if isempty(file)

	obj = sql;
	
	if ~nargout
		disp(' '); sql_display(obj); disp(' '); clear obj;
	end
	
	return;
end

%--
% write objects to database and get objects from database if requested
%--

if ~nargout
	query(file, sql); return;
end

%--
% write objects to database and get objects from database if requested
%--

% TODO: consider an option to only output the identifiers, not the full object

if ~new
	
	query(file, sql);

	obj = get_database_objects_by_column(file, table, 'id', id);
else
	% TODO: generalize this 'id' retrieval, multiple connections could make this fail

	% NOTE: it seems like we want to wrap the whole thing in a transaction

	% NOTE: we assume an AUTOINCREMENT 'id' but otherwise use a standard SQL query

	[status, before] = query(file, ['SELECT max(id) AS max_id FROM ', table ,';']);

	query(file, sql);

	[status, after] = query(file, ['SELECT max(id) AS max_id FROM ', table ,';']);
	
	obj = get_database_objects_by_column(file, table, 'id', (before.max_id + 1):after.max_id);
end

