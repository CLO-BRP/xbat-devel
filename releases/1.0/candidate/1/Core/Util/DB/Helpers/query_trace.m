function state = query_trace(state)

% query_trace - state
% -------------------
%
% state = query_trace(state)
%
% Input:
% ------
%  state - to set (opt), 0 or 1
%
% Output:
% -------
%  state - currently

persistent STATE;

if nargin
	if ischar(state) 
		state = str2int(state);
	end
	
	STATE = state;
else
	if isempty(STATE)
		STATE = 0;
	end
	
	state = STATE;
end