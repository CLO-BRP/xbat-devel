function obj = set_modified_at(varargin)

% set_modified_at - field of struct array
% -------------------------------------
%
% obj = set_modified_at(obj, stamp)
%
% Input:
% ------
%  obj - array
%  stamp - time
%
% Output:
% -------
%  obj - updated array

obj = set_timestamp('modified_at', varargin{:});
