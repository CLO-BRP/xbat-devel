function obj = set_existing_field(obj, varargin)

% set_existing_field - of struct array
% ------------------------------------
%
% obj = set_existing_field(obj, field, value, ... )
%
% Input:
% ------
%  obj - array
%  field - name
%  value - for field
%
% Output:
% -------
%  obj - updated array

% NOTE: this function probably belongs in the 'Core/Datatypes/Struct' module

% TODO: add options to handle various conditions, mainly missing field

%--
% handle input
%--

[field, value] = get_field_value(varargin);

%--
% set fields
%--

for k = 1:numel(field)
	obj = set_single_field(obj, field{k}, value{k});
end


%-----------------------------
% SET_SINGLE_FIELD
%-----------------------------

function obj = set_single_field(obj, field, value)

%--
% check for field to set
%--

if nargin < 3 || ~isfield(obj, field)
	return;
end

%--
% count values
%--

% NOTE: we don't set single-character values, a string is a single value

if ischar(value)
	count = 1;
else
	count = numel(value);
end

%--
% check for multiple or single field values to set
%--

if count > 1

	% NOTE: make sure we match values to objects in the multiple value case
	
	if numel(value) ~= numel(obj)
		error(['Multiple values (', int2str(numel(value)), ') for ''', field, ''' must match number of objects (', int2str(numel(obj)), ').']);
	end
	
	if iscell(value)
		for k = 1:numel(obj)
			obj(k).(field) = value{k};
		end
	else
		for k = 1:numel(obj)
			obj(k).(field) = value(k);
		end
	end

else

	for k = 1:numel(obj)
		obj(k).(field) = value;
	end

end


