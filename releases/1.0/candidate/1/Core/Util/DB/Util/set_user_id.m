function obj = set_user_id(obj, value)

% set_user_id - foreign key
% -------------------------
%
% obj = set_user_id(obj, id)
%
% Input:
% ------
%  obj - array
%  id - user
%
% Output:
% -------
%  obj - updated array

obj = set_foreign_key(obj, 'user', value);