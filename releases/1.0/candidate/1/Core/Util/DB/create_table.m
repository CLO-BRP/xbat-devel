function [str, lines] = create_table(prototype, name, opt)

% create_table - create table for struct array
% --------------------------------------------
%
% [str, lines] = create_table(prototype, name, opt)
%
%          opt = create_table
%
% Input:
% ------
%  prototype - struct
%  name - table name
%  opt - create options
%
% Output:
% -------
%  str - statement as string
%  lines - statement as lines
%  opt - default create options

% TODO: consider renaming to 'create_object_table', also organize related module

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set and possibly output create options
%--

if nargin < 3 
	
	% NOTE: force new table and use hints to establish type affinity
	
	opt.flatten = 1;
	
	opt.force = 0; 
	
	opt.hint = 1; 

	% NOTE: this contains known type and constraints information to be used for a column
	
	opt.column = empty(column_description);
	
	% NOTE: apply a unique constraint over all columns
	
	opt.unique = 0;
	
	opt.constraints = '';
	
	opt.foreign_key.explicit = false;
	
	opt.foreign_key.convention = @rails_convention;
	
	if ~nargin
		str = opt; return;
	end

end

if iscell(prototype)
    
	% TODO: figure out if this form is used
	
    column = prototype; opt.hint = 0;
    
elseif isstruct(prototype)

	%--
	% use flat field names as column names if needed
	%--

	% NOTE: ensure that we are working with a scalar struct, this makes sense
	
	prototype = prototype(1);
	
	if opt.flatten
		prototype = flatten_struct(prototype);
	end
	
	column = fieldnames(prototype);
	
else
	
	error('Prototype input must be cell array or struct.');
	
end

%--
% make sure we have table name
%--

if (nargin < 2) || isempty(name)	
	name = inputname(1);
end

if isempty(name)
	error('Table name is not available from input, explicitly or implicitly.');
end

%---------------------------------
% CREATE STATEMENT
%---------------------------------

%--
% initialize statement and consider opt.force
%--

lines = {};

if opt.force
	lines{end + 1} = ['DROP TABLE IF EXISTS ', name, '; '];
end

%--
% build create table statement
%--

lines{end + 1} = ['CREATE TABLE IF NOT EXISTS ', name];


if isempty(prototype)
	
	% NOTE: we have no way of inferring columns, this is not typical
	
	lines{end} = [lines{end}, ';'];
	
else
	
	%--
	% continue statement
	%--
	
	lines{end} = [lines{end}, ' ('];

	for k = 1:length(column)

		%--
		% check for column description
		%--
		
		if isempty(opt.column)
			described = 0;
		else

			[described, ix] = string_is_member(column{k}, {opt.column.name});
			
			if described
				description = opt.column(ix);
			end
			
		end
		
		%--
		% put together column description
		%--
		
		% NOTE: this is a generic untyped unconstrained column 
		
		if ~opt.hint && ~described
			lines{end + 1} = ['  ', column{k}, ',']; continue;  %#ok<AGROW>
		end
		
		% NOTE: in this case we create a description from the hint
		
		if opt.hint && ~described 
			description = get_type_and_constraints(column{k}, prototype.(column{k}));
		end

		lines{end + 1} = ['  ', column{k}, ' ', description.type , ' ', description.constraints, ',']; %#ok<AGROW>

	end

    %--
    % add table constraints
    %--
    
	if ~isempty(opt.constraints) 
		
		if iscell(opt.constraints)
			lines{end + 1} = ['  ', str_implode(opt.constraints, ', ')];
		else
			lines{end + 1} = ['  ', strtrim(opt.constraints)];
		end
		
	end
	
	% NOTE: map an empty list of unique fields to the not unique indicator
	
	if iscell(opt.unique) && isempty(opt.unique)
		unique = 0;
	else
		unique = 1;
	end

	% NOTE: a unique indicator means unique across all columns, a list of columns establishes a subset
	
	if unique
		
		if isnumeric(opt.unique)
			
			if opt.unique
				opt.unique = column;
			else
				opt.unique = {};
			end
			
		end
		
		if ischar(opt.unique)
			opt.unique = {opt.unique};
		end
			
		% NOTE: available columns should match all requested columns
		
		if ~isempty(opt.unique)

			if ~isempty(setdiff(opt.unique, column))
				error('Requested unique columns are not available.');
			end

			if ~isempty(opt.constraints) 
				lines{end} = [lines{end}, ', '];
			end
			
			lines{end + 1} = ['  UNIQUE(', str_implode(opt.unique, ', '), ')'];

		end
		
		%--
		% add explicit foreign key declarations based on provided convention
		%--
		
		if opt.foreign_key.explicit
% 			db_disp; column, opt.foreign_key.convention(column)
			
			foreign = opt.foreign_key.convention(column);
			
			if ~isempty(foreign)
				lines = [lines, strcat({'  '}, foreign, ',')];
			end
		end
		
	end
	
	%--
	% close statement
	%--
	
	if lines{end}(end) == ','
		lines{end}(end) = [];
	end
	
	lines{end + 1} = ');';
	
end

lines = strrep(lines(:), ' ,', ',');

%--
% output sql as string as well as lines or display output
%--

if nargout
	str = sql_string(lines);
else
	disp(' '); sql_display(lines); disp(' ');
end


