function out = get_type_and_constraints(column, value)

% get_type_and_constraints - get type and constraint information
% --------------------------------------------------------------
%
% out = get_type_and_constraints(column, value)
%
% Input:
% ------
%  column - column name
%  value - column instance values
%
% Output:
% -------
%  out - struct output

% TODO: use a sequence of values to infer types

%--
% get base type and constraints
%--

[out.constraints, timestamp] = get_constraints(column);

% NOTE: we had done this a different way before, reconsider

if ~timestamp
	out.type = get_type(value);
else
	out.type = 'TIMESTAMP'; % out.type = 'DATETIME';
end

%--
% handle conventional cases more forcefully
%--

% NOTE: if a column is a key, try to consider it as an integer

if ~isempty(strfind(out.constraints, 'KEY'))
	
	if isempty(value) || all(round(value) == value)
		out.type = 'INTEGER';
	end
	
end

% NOTE: if a column is a creation date try to consider it as a datetime

% NOTE: something like this should be optional, since it may require further conversion code when we insert

% if string_is_member(column, {'created', 'modified', 'timestamp'})
% 	out.type = 'DATETIME';
% end


%-----------------------------------------------------
% GET_CONSTRAINTS
%-----------------------------------------------------

function [str, timestamp] = get_constraints(column)

% get_constraints - get constraints based on naming conventions
% -------------------------------------------------------------
%
% [str, timestamp] = get_constraints(column)
%
% Input:
% ------
%  column - field name
%
% Output:
% -------
%  str - constraint description
%  timestamp - indicator

%--
% initialize constraints and normalize column name
%--

str = cell(0); column = lower(column);

%--
% consider not null conventions
%--

% timestamp_words = {'created', 'created_at', 'modified', 'modified_at', 'timestamp'};

not_null_timestamp_words = {}; % {'timestamp', 'created', 'created_at'};

nullable_timestamp_words = {'created', 'created_at', 'modified', 'modified_at'}; % {'modified', 'modified_at'};

timestamp_words = [not_null_timestamp_words, nullable_timestamp_words];

not_null_words = {not_null_timestamp_words{:}, 'author'};
	
if string_is_member(column, not_null_words)
	str{end + 1} = 'NOT NULL';
end

%--
% consider timestamp conventions
%--
	
timestamp = string_is_member(column, timestamp_words);

if string_is_member(column, not_null_timestamp_words);
	str{end + 1} = 'DEFAULT CURRENT_TIMESTAMP';
end

%--
% consider key conventions
%--

if strcmp(column, 'id')
	
	str{end + 1} = 'NOT NULL PRIMARY KEY AUTOINCREMENT'; 
	
else
		
	key_words = {'_id'};

	for k = 1:length(key_words)
		
		if ~isempty(strfind(column, key_words{k}))
			str{end + 1} = 'NOT NULL'; break;
		end
		
	end
	
end

if strcmp(column, 'hash')
    
    str{end + 1} = 'UNIQUE';
    
end
    
%--
% concatenate constraints
%--

str = strcat(str, {' '}); str = [str{:}]; str(end) = [];



%-----------------------------------------------------
% GET_TYPE
%-----------------------------------------------------

function type = get_type(value)

if ischar(value)
	
	type = 'VARCHAR(255)';
	
	% NOTE: we try to convert the string to a date to check for datetime
	
	if ~isempty(value)
		try
			datenum(value); type = 'TIMESTAMP'; % type = 'DATETIME';
		catch
			% NOTE: there is nothing to catch
		end
	end

	return;

end

if islogical(value)
	type = 'TINYINT'; return;
end 

if isnumeric(value) && isscalar(value) && isreal(value)
	
	% NOTE: the big integer option is MySQL specific, it is currently not used
	
	if value == floor(value)
		if isa(value, 'int64')
			type = 'BIGINT';
		else
			type = 'INTEGER';
		end
	else
		type = 'REAL';
	end
	
	return;
	
end

%--
% handle complex value cases
%--

% NOTE: this is so because we store complex types as XML

type = 'TEXT(65535)';
