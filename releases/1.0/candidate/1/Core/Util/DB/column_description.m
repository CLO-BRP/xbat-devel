function description = column_description(name, type, constraints)

%--
% set empty type and constraints default
%--

if nargin < 3 || isempty(constraints)
	constraints = '';
end

if nargin < 2 || isempty(type)
	type = '';
end

if ~nargin
	name = '';
end

%--
% build column description
%--

description.name = name;

description.type = type;

description.constraints = constraints;