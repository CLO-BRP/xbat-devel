function query = delete_related(file, focus, related)

% TODO: this function is to generate queries to delete relation entries when an object is deleted

% NOTE: the queries should then be used as triggers

% NOTE: this should lead to some refactoring of 'select_related' as well

query = struct;