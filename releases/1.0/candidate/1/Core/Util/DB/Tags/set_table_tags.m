function [done, status, result] = set_table_tags(file, table, id, tag, user)

% set_table_tags - set tags for objects in table
% ----------------------------------------------
%
% [done, status, result] = set_table_tags(file, table, id, tag, user)
%
% Input:
% ------
%  file - file
%  table - name
%  id - for objects
%  tag - for objects
%  user - user
%
% Output:
% -------
%  done - indicator
%  status - status
%  result - result

% TODO: refactor to use 'update_relation', note we may have to add and delete edges in this case

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% check table has tags
%--

if ~database_has_tags(file, table)
	error(['Table ''', table, ''' is not currently taggable.']);
end

%--
% check tags input
%--

% NOTE: in this case we have one set of tags with a single tag in the set

if ischar(tag)
	tag = {{tag}};
end

if numel(tag) ~= numel(id)
	error('Tag and identifier arrays must have matching elements.');
end

%--
% handle user input
%--

% NOTE: at the moment this couples this simple function with the rest of the system

if nargin < 5
	
	active = get_active_user; user = get_database_user(file, active.name);
	
	if isempty(user)
		user = set_database_user(active);
	end 
	
	user_id = user.id;
	
end

% NOTE: we trust that the input is a uset struct or identifier

if isstruct(user) && isfield(user, 'id')
	user_id = user.id; 
else
	user_id = user;
end

%-------------------------
% SETUP
%-------------------------

%--
% append all proposed tags so they may have identifiers, and get these
%--

append_database_tags(file, get_append_tags(tag));

[tags, tid] = get_database_tags(file);

%--
% get current tags so we may determine which objects are being updated
%--

current = get_table_tags(file, table, id);

% TODO: the section and comments below are not correct, we may be deleting tags

% TODO: consider deleting tags matching the various identifiers to start!

%--
% check how many tags we will be updating per object and total 
%--

count = iterate(@numel, tag); total = sum(count);

% NOTE: this is a special case of no tags, typical when we first log events

if total == 0
	done = 1; status = 0; result = ''; return;
end

%-------------------------
% SET TAGS
%-------------------------

%--
% encode tag strings using identifiers
%--

% NOTE: the encoding is a two-part process, we get the index for the known tag, then we map it

code = {};

for k = 1:numel(tag)
	
	if ~count(k)
		continue;
	end 
	
	% NOTE: we are sure to find the tag in the list of tags, because we appended
	
	for j = 1:count(k)
		code{k}(j) = find(strcmp(tag{k}{j}, tags), 1);
	end
	
	% NOTE: we have an index for each tag name and we map to retrieve the corresponding identifier
	
	code{k} = tid(code{k});
	
end

%--
% update table tags
%--

% NOTE: this is the number of edges to be drawn between the two tables

count = sum(iterate(@numel, tag));

sql = cell(count + 2, 1);

sql{1} = 'BEGIN;'; ix = 2;

% NOTE: the outer loop is over objects to be tagged, the inner loop over the tags for this object

relation = tag_relation_table(table);

for k = 1:numel(tag)
	
	for j = 1:numel(code{k})
	
		sql{ix} = [ ...
			'INSERT OR IGNORE INTO ', relation, ...
			' (', str_implode(strcat({table, 'tag', 'user'}, '_id'), ', '), ') VALUES (', ...
			int2str(id(k)), ', ', int2str(code{k}(j)), ',', int2str(user_id), ');' ...
		]; 
	
		ix = ix + 1;
	
	end
	
end

sql{end} = 'COMMIT;';

[status, result] = sqlite(file, sql); done = status == 101;


%-------------------------
% GET_APPEND_TAGS
%-------------------------

function append = get_append_tags(tag)

% NOTE: input is a cell array of strings or string cell arrays

if ischar(tag{1})
	append = {tag{1}};
else
	append = tag{1};
end

for k = 2:numel(tag)
	
	if isempty(tag{k})
		continue;
	end
	
	if ischar(tag{k})
		append = unique({append{:}, tag{k}});
	else
		append = unique({append{:}, tag{k}{:}});
	end
	
end 

% TODO: there is a problem with the empty string sneaking into the tags consideration

append = setdiff(append, {''});

