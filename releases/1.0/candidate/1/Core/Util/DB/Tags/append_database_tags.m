function [count, tags, id] = append_database_tags(store, tags)

% append_database_tags - add tags to database
% -------------------------------------------
%
% [count, tag, id] = append_database_tags(store, tags)
%
% Input:
% ------
%  store - database
%  tags - tags 
%
% Output:
% -------
%  count - added
%  tag - name
%  id - identifier
%
% See also: get_database_tags, rename_database_tags

% NOTE: we read known tags, if we have to add then we write

%--
% handle input
%--

if ischar(tags)
	tags = {tags};
end

%--
% check whether we need to append tags
%--

% TODO: do this evaluation using the database eventually

append = setdiff(tags, get_database_tags(store)); count = numel(append);

if ~count
	tag = {}; id = []; return;
end 

%--
% build query to append tags
%--

% NOTE: we ensure valid tags, this is a bit paranoid

[ignore, append] = iterate(@valid_tag, append);

% NOTE: we will always use the 'ignore' resolution in this case

sql = cell(numel(append) + 2, 1); sql{1} = 'BEGIN;';

for k = 1:numel(append)
	sql{k + 1} = ['INSERT INTO tag VALUES (NULL, ''', append{k}, ''', CURRENT_TIMESTAMP);'];
end

sql{end} = 'COMMIT;';

%--
% append tags and output count indicator
%--

[status, result] = query(store, sql); 

% NOTE: this is how we indicate failure

if status == 1
	count = -1; return;
end

%--
% get tags with their corresponding id values
%--

if nargout < 2
	return;
end

[tag, id] = get_tag_id(store, append);


%-------------------
% GET_TAG_ID
%-------------------

function [tag, id] = get_tag_id(store, tag)

result = query(store, ['SELECT name, id FROM tag WHERE name IN (''', str_implode(tag, ''', '''), ''');']);

tag = {result.name}; id = [result.id];
