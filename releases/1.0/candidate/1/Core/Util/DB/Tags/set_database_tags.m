function [count, tag, id] = set_database_tags(file, tag, id, created)

% set_database_tags - set tags along with identifiers
% ---------------------------------------------------
%
% count = set_database_tags(file, tag, id, created)
%
% Input:
% ------
%  file - file
%  tag - tags
%  id - identifiers
%  created - time stamps
%
% Output:
% -------
%  count - tags set
%  tag - name
%  id - identifier

% TODO: this will set tags with specified it values, it is dangerous

% TODO: use this to keep a coherent set of tag identifiers across many files

%--------------------
% HANDLE INPUT
%--------------------

if nargin < 4
	created = [];
end

%--
% simply append tags if we are not concerned about identifier values
%--

if nargin < 3
	
	count = append_database_tags(file, tag);
	
	if nargout > 2
		[tag, id] = get_database_tags(file, 'in', tag);
	end

	return;
	
end

%--
% make sure tag is a cell and identifiers match
%--

if ischar(tag)
	tag = {tag};
end 

if numel(tag) ~= numel(id)
	error('Tag and identifier arrays must have matching elements.');
end

%--------------------
% SET TAGS
%--------------------

%--
% build query to append tags
%--

% NOTE: we ensure valid tags, this is a bit paranoid

[ignore, tag] = iterate(@valid_tag, tag);

% NOTE: using the replace resolver will clobber tag names with matching identifier

sql = cell(numel(tag) + 2, 1); sql{1} = 'BEGIN;';

if isempty(created)

	for k = 1:numel(tag)
		sql{k + 1} = ['INSERT OR REPLACE INTO tag VALUES (', int2str(id), ', ''', tag{k}, ''', CURRENT_TIMESTAMP);'];
	end

else
	
	% TODO: make sure that created time stamps are properly expressed as strings
	
	for k = 1:numel(tag)
		sql{k + 1} = ['INSERT OR REPLACE INTO tag VALUES (', int2str(id), ', ''', tag{k}, ''', ', created{k}, ');'];
	end
	
end

sql{end} = 'COMMIT;';

%--
% append tags and output count indicator
%--

[status, result] = sqlite(file, sql); 

% NOTE: this is how we indicate failure

if status == 1
	count = -1; return;
end

%--
% get tags with their corresponding id values
%--

if nargout < 2
	return;
end

result = sqlite(file, ['SELECT name, id FROM tag WHERE name IN (''', str_implode(tag, ''', '''), ''');']);

tag = {result.name}; id = [result.id];


