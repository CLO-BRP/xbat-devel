function root = jdbc_driver_root(adapter)

% jdbc_driver_root - get install location for drivers
% ---------------------------------------------------
%
% root = jdbc_driver_root(adapter)
%
% Input:
% ------
%  adapter - name
% 
% Output:
% -------
%  root - directory for driver install 

root = fullfile(fileparts(mfilename('fullpath')), 'Drivers');

if nargin
	
	in = adapter; adapter = known_jdbc_adapter(adapter);
	
	if isempty(adapter)
		error(['Unknown JDBC adapter ''', in, '''.']);
	end
	
	root = fullfile(root, adapter);
	
end 
