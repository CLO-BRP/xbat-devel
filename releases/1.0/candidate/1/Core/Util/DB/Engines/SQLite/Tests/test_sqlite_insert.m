function test_sqlite_insert(value)

% TODO: finish this test by adding reading back and comparing

% TODO: develop a second test here that uses 'create_table' and other such functions

%--
% check input
%--

if ~isstruct(value)
	error('Input values should be a struct.');
end

%--
% create database and table
%--

file = fullfile(fileparts(mfilename('fullpath')), 'test.db');

% NOTE: a more sophisticated schema is used by 'create_table'

columns = fieldnames(value);

sql = ['DROP TABLE if EXISTS my_table; CREATE TABLE if NOT EXISTS my_table (''', str_implode(columns, ''','''), ''');']; 

% NOTE: turn on tracing

trace = sqlite_trace; sqlite_trace(1);

sqlite(file, 'exec', sql);

%--
% insert values
%--

if numel(value) < 1
	sql = ['INSERT INTO my_table VALUES ', value_str(value), ';'];
else
	sql = sqlite_array_insert('my_table', value);
end

sqlite(file, 'exec', sql);

%--
% reset tracing
%--

sqlite_trace(trace);
