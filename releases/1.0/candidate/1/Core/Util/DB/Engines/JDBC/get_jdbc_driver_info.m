function info = get_jdbc_driver_info(adapter)

% get_jdbc_driver_info - get info for known drivers
% -------------------------------------------------
%
% info = get_jdbc_driver_info(adapter)
%
% Input:
% ------
%  adapter - name
%
% Output:
% -------
%  info - for JDBC driver

root = 'http://xbat.org/downloads/jdbc/';

%---------------------
% DRIVER INFO'
%---------------------

info = empty(create_info);

%--
% mysql
%--

info(end + 1) = create_info( ...
	'name', 'MySQL', ...
	'url', [root, 'mysql/mysql-connector-java-5.1.10.zip'], ...
	'driver', 'com.mysql.jdbc.Driver' ...
);

%--
% postgres
%--

if strfind(version('-java'), 'Java 1.6')
	url = [root, 'postgresql/postgresql-8.4-701.jdbc4.jar'];
else
	url = [root, 'postgresql/postgresql-8.4-701.jdbc3.jar'];
end

info(end + 1) = create_info( ...
	'name', 'PostgreSQL', ...
	'url', url, ...
	'driver', 'org.postgresql.Driver' ...
);

%--
% sqlite
%--

info(end + 1) = create_info( ...
	'name', 'SQLite', ...
	'url', [root, 'sqlite/sqlite-jdbc-3.6.20.1.jar'], ...
	'driver', 'org.sqlite.JDBC' ...
);
	
%--
% sqlserver
%--

info(end + 1) = create_info( ...
	'name', 'SQLServer', ...
	'url', [root, 'sqlserver/jtds-1.2.5-dist.zip'], ...
	'driver', 'net.sourceforge.jtds.jdbc.Driver' ...
);

%---------------------
% SELECT INFO
%---------------------

if ~nargin
	return; 
end

ix = find(iterate(@strcmpi, {info.name}, adapter));

if isempty(ix)
	info = empty(info);
else
	info = info(ix);
end


%-----------------------
% CREATE_INFO
%-----------------------

function info = create_info(varargin)

info.name = ''; info.url = ''; info.driver = '';

if ~nargin
	return;
end

info = parse_inputs(info, varargin{:});





