function current = get_current_timestamp(store)

% get_current_timestamp - from database
% -------------------------------------
%
% current = get_current_timestamp(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  current - timestamp

% TODO: move this elsewhere

sql = 'SELECT CURRENT_TIMESTAMP as current;';

% NOTE: the simple SQLite query returns the UTC timestamp, particular stores can be configured differently

if ~nargin
	result = sqlite('', sql);
else
	[ignore, result] = query(store, sql); %#ok<ASGLU>
end

current = result.current;