function id = get_last_id(statement)

% TODO: this should probably be a private function

resultset = statement.getGeneratedKeys(); id = [];

if ~isempty(resultset)
	
    while resultset.next()
        id(end + 1) = resultset.getInt(1); %#ok<AGROW>
    end
end

resultset.close();

% FROM http://java.sun.com/products/jdbc/faq.html

% Q: there is a JDBC API method 'getColumnCount'. is there a method to find the number of rows?

% A: No. For a scrollable 'resultset' the methods 'last' and 'getRow' can be used to find out how many rows.

% A: If the 'resultset' is not scrollable, count the rows by iterating through the set.

% A: Get the number of rows by submitting a query with a COUNT column in the SELECT clause.
