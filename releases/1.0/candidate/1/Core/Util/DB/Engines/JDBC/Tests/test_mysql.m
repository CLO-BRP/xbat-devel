function result = test_mysql

tic;

config = test_mysql_setup;

%--
% SINGLE STATEMENT QUERY
%--

jdbc(config, 'INSERT into users (name) VALUES (''David'')');
jdbc(config, 'INSERT into users (name) VALUES (''Harold'')');

[status, result] = jdbc(config, 'SELECT count(*) as count FROM users');

if isequal(result.count, 2);
    result = 'Passed';
else
    result = ['Failed. 2 users were expected. ', num2str(result.count), ' were found.'];
end

%-----
% BATCH MODE
%-----

jdbc(config, 'INSERT into users (name) VALUES (''Joey''); INSERT into users (name) VALUES (''Joey''); INSERT into users (name) VALUES (''Billy'');');

[status, result] = jdbc(config, 'SELECT count(*) as count FROM users');

if isequal(result.count, 5);
    result = 'Passed';
else
    result = ['Failed. 5 users were expected. ', num2str(result.count), ' were found.'];
end

toc


%--------------------
% SETUP
%--------------------

function config = test_mysql_setup

config = mysql_test_config;

jdbc(config, 'DROP table IF EXISTS users ;');

jdbc(config, 'CREATE TABLE users ( id int(11) default NULL, name varchar(255) default NULL, bool tinyint(1) default 0) ENGINE=InnoDB DEFAULT CHARSET=latin1;');

