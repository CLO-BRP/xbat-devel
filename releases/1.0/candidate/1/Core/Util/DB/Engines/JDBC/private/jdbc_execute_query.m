function [status, result] = jdbc_execute_query(connection, sql)

% NOTE: typically these are SELECT queries

statement = get_jdbc_statement(connection);

resultset = statement.executeQuery(sql);

% NOTE: 'autocommit' is default, only 'jdbc_execute_batch' turns it off temporarily

% connection.commit();

result = format_jdbc_resultset(resultset); % now a private function

status = 0;

% NOTE: this cleans up the transaction

resultset.close();