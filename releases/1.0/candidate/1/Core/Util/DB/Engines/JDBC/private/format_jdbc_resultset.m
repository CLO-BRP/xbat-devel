function result = format_jdbc_resultset(resultset)

%---------------------------
% SETUP
%---------------------------

metadata = resultset.getMetaData(); % NOTE: what is this magical creature 'metadata'?

%--
% get number of columns, create result struct prototype, and get column names
%--

% NOTE: apparently we need both the 'label' ('alias') for a column and the original 'name

count = metadata.getColumnCount;

result = struct;

name = cell(1, count);

for k = 1:count

	% NOTE: we assume that the column labels are proper field names

	result.(char(metadata.getColumnLabel(k))) = [];

	name{k} = char(metadata.getColumnName(k));

end

result = empty(result);

label = fieldnames(result);

%---------------------------
% GET RESULT
%---------------------------

% The following are JDBC interface notes:

% column_type_name = metadata.getColumnTypeName(k) % database specific

% column_type = metadata.getColumnType(k) % database agnostic

% classname = metadata.getColumnClassName(k); % returns Java class as a String

%--
% get size of result set, return quickly if empty, this may not be needed
%--

% resultset.last();
% 
% if ~resultset.getRow()
% 	return;
% end
% 
% resultset.beforeFirst(); 

%--
% loop over the resultset and pack the result struct
%--

j = 1;

while resultset.next()

	for k = 1:count

		%--
		% get column content 'object' as MATLAB data
		%--

		% NOTE: this call should return the proper value in MATLAB data type

		try
			value = resultset.getObject(name{k});
		catch
			db_disp; nice_catch; value = '';
		end

		%--
		% append result array
		%--

		result(j).(label{k}) = java2matlab(value);

	end

	j = j + 1;

end


%--------------------------
% JAVA2MATLAB
%--------------------------

% TODO: perhaps this function can be combined with the other java conversion functions as necessary (see load_yaml)

function value = java2matlab(value)

if isa(value, 'java.sql.Timestamp')
	value = get_string_from_java(value);
end
