function test_sharing

%--
% setup test database
%--

store = fullfile(fileparts(mfilename('fullpath')), 'test.sqlite');

if exist(store, 'file')
	delete(store);
end

%--
% add objects and users to database
%--

% NOTE: these are all public objects, they have no 'user_id', which we understand as owner

for k = 1:8
	obj(k).id = k; obj(k).content = 'content';
end

query(store, create_table(obj));

obj = set_database_objects(store, obj); % disp(obj);

for k = 1:4
	user(k).id = k; user(k).name = ['name_', int2str(k)];
end

query(store, create_table(user));

user = set_database_objects(store, user); % disp(user);

%--
% establish sharing and share objects
%--

establish_sharing(store);

% NOTE: we can shared all objects a priori to make the results easier to inspect

get_shared_objects(store, obj);

% NOTE: selecting objects requires we are explicit with object type

% matching objects and recipients

share_objects(store, obj([4,3,1]), user([1,3,4]), user(2), 'obj');

% single object to multiple recipients

share_objects(store, obj(1), user(2:4), user(1), 'obj');

% multiple objects to single recipient

share_objects(store, obj, user(4), user(3));

%--
% check for shared objects
%--

% TODO ... 

