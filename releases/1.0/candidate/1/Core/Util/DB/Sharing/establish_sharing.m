function [status, result] = establish_sharing(store)

% establish_sharing - add tables for sharing
% ------------------------------------------
%
%     [sql, lines] = establish_sharing
%
% [status, result] = establish_sharing(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  status - of query request
%  result - of query

%--
% describe shared objects table
%--

hint = column_type_hints;

% NOTE: these are the shared objects

shared.id = hint.i; 

shared.shareable_type = hint.s; 

shared.shareable_id = hint.i;

% shared.user_id = hint.i; % NOTE: this is the first person who shares?

shared.forwardable = hint.i;

shared.created_at = hint.ts; 

shared.modified_at = hint.ts;

opt = create_table; opt.constraints = {'UNIQUE (shareable_id)'};

%--
% describe sharing table
%--

% NOTE: each row is an act of sharing

sharing.id = hint.i;

sharing.shared_id = hint.i; 

sharing.user_id = hint.i;

% TODO: allow create table to work with complex hints in prototype, we can include constraints

sharing.recipient_id = hint.i; % {hint.i, 'null'};

sharing.created_at = hint.ts; 

%--
% generate create table queries
%--

sql = {'BEGIN;', create_table(shared, 'shared', opt), create_table(sharing), 'COMMIT;'};

%--
% OUTPUT OR EXECUTE QUERY
%--

if ~nargin
	store = [];
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql);
end
