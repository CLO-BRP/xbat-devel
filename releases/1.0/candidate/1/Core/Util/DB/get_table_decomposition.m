function table = get_table_decomposition(in, name, table)

% get_table_decomposition - represent struct array as collection of tables
% ------------------------------------------------------------------------
%
% table = get_table_decomposition(in, name)
%
% Input:
% ------
%  in - struct array
%  name - table name
%
% Output:
% -------
%  table - struct of tables

%--
% handle input
%--

if nargin < 3
	table = struct;
end

if nargin < 2
	name = inputname(1);
end

if isempty(name)
	error('Unable to determine table name.');
end

%--
% add primary id to input objects if needed
%--

% NOTE: a named parent 'id' column will be available if this is a recursive call

if ~isfield(in, 'id')
	
	% NOTE: the 'id' values are particularly important if we have children
	
	if ~isfield(table, name)
		values = num2cell(1:length(in));   
	else
		start = max([table.(name).id]) + 1; values = num2cell(start:start + length(in) - 1);
	end
	
	% NOTE: this line deals the 'id' values to the various input elements
	
	[in(:).id] = values{:}; 
	
end

%--
% inspect and element of the input to learn about children
%--

element = in(1); field = fieldnames(element); child = {}; child_name = {};

for k = 1:length(field)
	
	if isstruct(element.(field{k}))
		
		child{end + 1} = field{k}; 
		
		if ismember(child{end}, {'value', 'parameter'})
			child_name{end + 1} = [name, '_', child{end}];
		else
			child_name{end + 1} = child{end};
		end
		
	end
	
end

%--
% get table decomposition
%--

for j = 1:length(in)
	
	element = in(j);
	
	%--
	% handle child table fields
	%--
	
	for k = 1:length(child)
		
		% NOTE: get child values, and give the children the parent id
		
		value = element.(child{k}); 
		
		if isempty(value)
			continue; 
		end
		
		[value.([name, '_id'])] = deal(element.id);
		
		table = get_table_decomposition(value, child_name{k}, table);
		
	end

	%--
	% remove struct fields from element and append to current table
	%--

	if ~isempty(child)
		element = rmfield(element, child);
	end
	
	if ~isfield(table, name)
		table.(name) = element;
	else
		try
			table.(name)(end + 1) = element;
		catch
			k = 1;
			while 1
				alternative = [name, int2str(k)];
				if isfield(table, alternative)
					try
						table.(alternative)(end + 1) = element; break;
					catch
						k = k + 1;
					end
				else
					table.(alternative) = element; break;
				end
			end
		end
	end
	
end

