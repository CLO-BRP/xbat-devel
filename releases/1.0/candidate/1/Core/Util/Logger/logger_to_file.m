function status = logger_to_file(logname, level, message)

filename = get_logger_file_fullfile(logname);

fid = fopen(filename,'a');

fprintf(fid, '[%s] %s \n%s \n\n', upper(level), datestr(now), message);

status = fclose(fid);