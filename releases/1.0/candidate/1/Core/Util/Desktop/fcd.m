function par = fcd(name)

% fcd - get or move to function or file parent directory
% ------------------------------------------------------
%
% fcd name; par = fcd(name)
%
% Input:
% ------
%  name - function or file name
%
% Output:
% -------
%  par - parent directory

%--
% return current directory if no input
%--

% NOTE: in this case there is no need to move

if nargin < 1
	par = pwd; return;
end
	
%--
% get function or file location using which, or back location from store
%--

switch name
	
	case 'back'
		
		par = get_env('fcd_back');
		
		if isempty(par)
			par = pwd;
		end
		
	otherwise

		par = which(name);

		prefix = 'built-in ('; % NOTE: this step is fragile, since MATLAB could change the prefix

		if strmatch(prefix, par)
			par(end) = []; par(1:length(prefix)) = [];
		end

		par = fileparts(par);

end

%--
% get parent directory, move to it if needed
%--



if ~isempty(par) && ~nargout
	
	set_env('fcd_back', pwd); cd(par); 
	
end