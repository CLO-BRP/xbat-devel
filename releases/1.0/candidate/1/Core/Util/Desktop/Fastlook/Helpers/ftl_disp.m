function ftl_disp(result)

% ftl_disp - result
% -----------------
%
% ftl_disp(result)
%
% Input:
% ------
%  result - from 'ftl'
%
% See also: ftl

% TODO: consider what the display is like, in 'svn' paths are relative	

% TODO: consider various display options, default should display: file, line numbers, and line content

disp(' '); 

if result.total > result.limit
	disp(['  ', int2str(result.total) ' matching lines found. Showing first ', int2str(result.limit), '.']);
else
	switch result.total
		case 0
			disp('  No matching lines found.'); disp(' '); return;
			
		case 1
			disp(['  ', int2str(result.total) ' matching line found.']);
		
		otherwise
			disp(['  ', int2str(result.total) ' matching lines found.']);
	end
end

% NOTE: this is to simplify the relative path display code below

if ~isempty(result.root) && result.root(end) ~= filesep
	result.root(end + 1) = filesep;
end

for k = 1:numel(result.file)
	
	current = result.file(k); file = fullfile(current.location.location, current.name);
	
	file = strrep(file, result.root, '');
	
	disp(' '); disp(['  ', file]);
	
	limit = max([current.line.number]);
	
	for j = 1:numel(current.line)
		
		anchor = ['at line ', int_to_str(current.line(j).number, limit, ' ')];

		line = strtrim(current.line(j).content);
		
		link = ['  <a href="matlab:opentoline(''', file, ''',', int2str(current.line(j).number), ')">', anchor, '</a>  ', line]; 
		
		disp(link);
	end
end

disp(' ');