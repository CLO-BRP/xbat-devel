function cleaned = ftl_clean(location, verb)

% ftl_clean - delete missing files
% --------------------------------
%
% cleaned = ftl_clean(location, verb)
%
% Input:
% ------
%  location - string
%  verb - flag
%
% Output:
% -------
%  added - info
%
% See also: ftl, ftl_add, ftl_del, ftl_sync

if nargin < 2
	verb = false;
end

% NOTE: the third input is a 'clean' flag which indicates only delete missing files

cleaned = ftl_del(location, verb, true);
