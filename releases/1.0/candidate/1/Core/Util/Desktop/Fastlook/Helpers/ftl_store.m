function store = ftl_store

% ftl_store - connection
% ----------------------
%
% store = ftl_store
%
% Output:
% -------
%  store - for fast look
% 
% See also: ftl

% TODO: allow for this to be a set and get helper for a general database connection

%--
% compute store file 
%--

here = fileparts(mfilename('fullpath')); 

store = fullfile(here, 'ftl.sqlite3');

%--
% establish store if it does not exist
%--

if exist(store, 'file')
	return;
end

%--
% establish ftl schema
%--

% TODO: conventionally consider 'hash' like columns unique in 'create_table'

location = ftl_model('location');

query(store, create_table(location));

file = ftl_model('file');

query(store, create_table(file));

line = ftl_model('line');

query(store, create_table(line));



	
