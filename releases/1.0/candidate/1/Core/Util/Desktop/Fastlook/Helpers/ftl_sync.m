function result = ftl_sync(location, verb)

% ftl_sync - location content and store
% -------------------------------------
%
% result = ftl_sync(location, verb)
%
% Input:
% ------
%  location - string
%  verb - flag
%
% Output:
% -------
%  added - info
%
% See also: ftl, ftl_add, ftl_clean, ftl_del
%
% NOTE: 'sync' simply cleans and adds

result.cleaned = ftl_clean(location, verb);

result.added = ftl_add(location, verb);