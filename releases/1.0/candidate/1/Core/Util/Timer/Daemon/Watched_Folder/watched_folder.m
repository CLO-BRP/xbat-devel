function daemon = watched_folder(name, list, callback, rate, varargin)

% watched_folder - create daemon
% ------------------------------
%
% daemon = watched_folder(name, list, callback, rate, varargin)
%
% Input:
% ------
%  name - for daemon
%  list - of folders
%  callback - for daemon
%  varargin - timer options
%
% Output:
% -------
%  daemon - timer
%
% See also: set_watched_folder, content_monitor

% TODO: allow simple callback or struct callback input so that we may register 'start' and 'stop callback' to inform about state

%--
% handle input
%--

if nargin < 4
	rate = 1;
end

if nargin < 3 || isempty(callback)
	callback = @content_monitor;
end

if nargin < 2 || isempty(list)
	list = {pwd};
end

if ~nargin || isempty(name)
	name = 'watched_folder';
end

%--
% create singleton timer and configure
%--

daemon = create_timer(name, {@callback_helper, list, callback}, rate, 'fixedRate', varargin);


%-------------------------------------
% CALLBACK_HELPER
%-------------------------------------

function callback_helper(obj, eventdata, list, callback) %#ok<INUSL>

% TODO: consider using a stronger convention with the callback helpers so that we may use the 'obj' and 'eventdata' inputs

% NOTE: another thing we could do is offer the content monitoring to the callback!

% TODO: add some tracer code here

for k = 1:numel(list)
	try
		eval_callback(callback, list{k});
	catch
		nice_catch;
	end
end


