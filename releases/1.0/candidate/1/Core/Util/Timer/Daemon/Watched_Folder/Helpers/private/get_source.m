function [source, new, status] = get_source(store, location)

% get_source - representation in database
% ---------------------------------------
%
% [source, new] = get_source(store, location)
%
% Input:
% ------
%  store - for content monitor
%  location - of source
%
% Output:
% -------
%  source - representation in database
%  new - source indicator

[status, source] = query(store, ['SELECT * FROM source WHERE location = ''', location, '''']);

% TODO: handle failure status

new = isempty(source);

if new
	clear source; source.location = location;
	
	source = set_database_objects(store, source);
end