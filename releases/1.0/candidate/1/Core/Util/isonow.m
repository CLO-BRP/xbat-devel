function str = isonow

% isonow - result of 'now' as iso 8601 date string
% -------------------------------------------------
%
% str = isonow(value)
%
% Output:
% -------
%  str - result of 'now' in ISO 8601 format

% NOTE: since this function never operates on array we call 'datestr' directly instead of 'isodate'

str = datestr(now, 30);
