function root = toolbox_data_root(name)

% toolbox_data_root - root for toolbox data and files
% ---------------------------------------------------
%
% root = toolbox_data_root(name)
%
% Input:
% ------
%  name - toolbox name
%
% Output:
% -------
%  root - toolbox data root

if ~nargin
	name = '';
end

root = fullfile(fileparts(mfilename('fullpath')), 'Data', name); 