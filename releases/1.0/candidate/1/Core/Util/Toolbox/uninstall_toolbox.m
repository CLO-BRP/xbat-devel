function uninstall_toolbox(name)

% uninstall_toolbox - remove toolbox from path and filesystem
% -----------------------------------------------------------
%
% uninstall_toolbox(name)
%
% Input:
% ------
%  name - toolbox name

%--
% get toolbox root by name
%--

root = toolbox_root(name);

% NOTE: if the root does not exist we assume the toolbox is not installed

if ~exist_dir(root)
	return;
end

%--
% remove toolbox from path
%--

% TODO: update 'append_path.m' to also perform path removal

dirs = scan_dir(root); rmpath(dirs{:}); 

%--
% remove from filesystem
%--

% NOTE: we clear functions so that any MEX files are not held in memory

try
	clear functions; rmdir(root);
catch 
	disp(['Please remove ''', root, ''' manually.']);
end
