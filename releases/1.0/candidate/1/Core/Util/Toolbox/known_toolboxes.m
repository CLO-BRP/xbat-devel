function known = known_toolboxes

% known_toolboxes - simple check to see if a toolbox is known
% -----------------------------------------------------------
%
% known = known_toolboxes
%
% Output:
% -------
%  known - names of known toolboxes

content = no_dot_dir(toolbox_data_root); 

content = content([content.isdir]); 

known = {content.name};
