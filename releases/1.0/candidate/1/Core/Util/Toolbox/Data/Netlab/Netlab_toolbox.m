function toolbox = Netlab_toolbox

%------------------------
% DATA
%------------------------

toolbox.name = 'Netlab';

toolbox.home = 'http://www.ncrg.aston.ac.uk/netlab/index.php';

toolbox.url = { ...
	'http://www.ncrg.aston.ac.uk/netlab/downloads/3_3/netlab.zip', ...
	'http://www.ncrg.aston.ac.uk/netlab/downloads/3_3/nethelp.zip', ...
	'http://www.ncrg.aston.ac.uk/netlab/downloads/foptions.m' ...
};

toolbox.install = @install;

%------------------------
% INSTALL
%------------------------

function install

