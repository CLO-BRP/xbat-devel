function installed = installed_toolboxes

% installed_toolboxes - simple check to see if a toolbox is available
% -------------------------------------------------------------------
%
% installed = installed_toolboxes
%
% Output:
% -------
%  installed - names of installed toolboxes

content = no_dot_dir(toolbox_root);

content = content([content.isdir]); 

installed = {content.name};
