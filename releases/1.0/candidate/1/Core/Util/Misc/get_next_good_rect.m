function [n, R] = get_next_good_rect(n0, M)

if nargin < 2
	M = 0.75;
end

[n, R] = get_good_rect(4 * n0, M);

ix = find(n >= n0, 1, 'first'); 

if ~isempty(ix)
	
	n = n(ix); R = R(ix, :);
		
end