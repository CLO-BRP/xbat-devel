function str = int_to_str(in, large, pad)

% int_to_str - convert integers to strings
% ----------------------------------------
%
% str = int_to_str(in, large, pad)
%
% Input:
% ------
%  in - integers 
%  large - largest integer to consider
%  pad - character to use for prefix
%
% Output:
% -------
%  str - string conversion

%------------------------
% FAST CONVERSION
%------------------------

%--
% round and real
%--

in = real(round(in));

%--
% convert to string
%--

% NOTE: outputs a cell array for matrix inputs and a string for scalars

str = int_to_str_(in);

%------------------------
% PADDING
%------------------------

% NOTE: check if padding is requested or possible

if (nargin < 2) || isempty(large)
	return;
end

% NOTE: pack single string in cell for convenience

pack = ischar(str);

if pack
	str = {str};
end

%--
% set default padding character
%--

if nargin < 3
	pad = '0';
end

%--
% set default large
%--

% NOTE: an empty large value requests we compute this automatically

if isempty(large)
	large = max(in(:));
end

%--
% pad integers
%--

pad = double(pad); width = length(int2str(large));

for k = 1:numel(in)
	
	if length(str{k}) < width
		str{k} = [char(pad * ones(1, width - length(str{k}))), str{k}];
	end
	
end

if pack
	str = str{1};
end
