function [R, ix, AR, d] = rect_factor(n, ar)

% rect_factor - compute rectangle factorizations
% ----------------------------------------------
%
% [R, ix] = rect_factor(n, ar)
%
% Input:
% ------
%  n - positive integer sequence
%
% Output:
% -------
%  R - rectangle factorizations for all elements of sequence

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 990 $
% $Date: 2005-04-28 20:06:31 -0400 (Thu, 28 Apr 2005) $
%--------------------------------

%-----------------------------------------
% HANDLE INPUT
%-----------------------------------------

%--
% get screen aspect ratio
%--

if nargin < 2
	ar = get_screen_aspect_ratio;
end

%--
% consider integer sequences recursively
%--

% TODO: reconsider the behavior of this function for multiple inputs. what is the current behavior about?

if numel(n) > 1
	
	R = [];
	
	for k = 1:length(n)
		
		Rk = rect_factor(n(k));
		
		if ~isempty(Rk)
			R = unique([R; Rk], 'rows');
		end
		
	end
	
	return;
	
end

%-----------------------------------------
% COMPUTE RECTANGLE FACTORIZATIONS
%-----------------------------------------

%--
% factor number
%--

f = factor(n);

% NOTE: return when the number of factors is too large

if length(f) > 7
	error('There is a limit of 7 factors for this computation.');
end

%--
% compute permutations of factors
%--

pf = perms(f);

%--
% compute unique possible rectangles
%--

R = [];

% NOTE: for primes, this loop is never executed, and empty rectangles are output

for k = 1:(length(f) - 1)
	
	Rk = unique( ...
		[prod(pf(:, 1:k), 2), prod(pf(:, (k + 1):end), 2)], 'rows' ...
	);

	R = unique([R; Rk], 'rows');
	
end

%--
% compute preferred rectangle with respect to screen if needed
%--

if nargout < 2
	return; 
end

if isempty(R)
	AR = []; ix = []; d = inf; return;
end

AR = R(:, 2) ./ R(:, 1);

% NOTE: consider refinements of this function

[d, ix] = min(abs(AR - ar));


