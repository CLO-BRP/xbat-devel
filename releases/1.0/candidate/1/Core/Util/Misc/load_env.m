function load_env(file)

% load_env - from file
% --------------------
%
% load_env(file)
%
% Input:
% ------
%  file - name
%
% See also: persist_env, set_env, get_env

load(file); 

if ~exist('env', 'var')
	error(['No ''env'' variable found in file ''', file, '''.']);
end

data = get(0, 'userdata');

data.env = env;

set(0, 'userdata', data);
