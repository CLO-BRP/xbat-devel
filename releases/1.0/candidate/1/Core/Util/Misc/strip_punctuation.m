function str = strip_punctuation(str)

% strip_punctuation - strip punctuation from string
% -------------------------------------------------
% 
% str = strip_punctuation(str);
%
% Input:
% ------
%  str - input string
% 
% Output:
% -------
%  str - output string

% TODO: consider implementing this using regular expressions

if iscellstr(str)
	
	for k = 1:length(str)
		str{k} = strip_punctuation(str{k});
	end

	return;

end

%--
% create array of punctuation
%--

punct = double('`~!@#$%^&*()-=+[{]}\|;:''",<.>/?');

%--
% remove punctuation from string
%--

str = double(str);

for k = 1:length(punct)
	str(str == punct(k)) = [];
end

str = char(str);

%--
% convert spaces to underscore
%--

% TODO: check whether this is used by calling functions, it does not fit semantics

str = strrep(str, ' ', '_');
