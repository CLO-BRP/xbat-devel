function str = sec_to_clock(t, n)

% sec_to_clock - convert seconds to time string
% ---------------------------------------------
%
% str = sec_to_clock(t, n)
%
% Input:
% ------
%  t - time in seconds
%  n - fractional second digits (def: 2)
%
% Output:
% -------
%  str - time string

%--
% use rounding to get desired precision
%--

% NOTE: we are only generating strings up to six digits of precision, this is considered here

if nargin > 1 && n < 6
	t = round(10^n * t) / 10^n;
end

%--
% compute clock string using mex
%--

str = sec_to_clock_(t);

% NOTE: this is a little hack to deal with infinity

fix = isinf(t); pix = fix & t > 0; nix = fix & t < 0;

str(pix) = {'Inf'}; str(nix) = {'-Inf'};

%--
% remove trailing zeros if needed
%--

str = iterate(@regexprep, strcat(str, {' '}), '(0+)\s', '');

for k = 1:numel(str)

	% NOTE: this is needed for when there are no trailing zeros

	if str{k}(end) == ' ', str{k}(end) = ''; end

	% NOTE: this is needed for when we have integers

	if str{k}(end) == '.', str{k}(end + 1) = '0'; end

end

%--
% output string for single number
%--

% NOTE: we output a string for a single time

if length(str) == 1
	str = str{1};
end