function visible = is_visible(handle)

% is_visible - test for handles
% -----------------------------
%
% visible = is_visible(handle)
%
% Input:
% ------
%  handle - array
%
% Output:
% -------
%  visible - indicator
%
% NOTE: output is 'nan' when visibility is not a property

%--
% handle multiple handles
%--

if numel(handle) > 1
	visible = iterate(mfilename, handle); return; 
end

%--
% check for property and test
%--

prop = get(handle);

if isfield(prop, 'Visible')
	visible = strcmpi(prop.Visible, 'on');
else
	visible = nan;
end