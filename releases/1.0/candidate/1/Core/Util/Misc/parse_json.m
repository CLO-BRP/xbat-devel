function out = parse_json(json, nested)

% parse_json - reconstruct json representation of struct
% ------------------------------------------------------
%
% out = parse_json(str)
%
% Input:
% ------
%  str - json representation
% 
% Output:
% -------
%  out - struct

json = strtrim(json);

%--
% handle nested objects
%--

if nargin < 2
	
	% NOTE: this peels off one nesting level at a time, not efficient for highly nested structures
	
	chi = cumsum((json(2:end - 1) == '{') - (json(2:end - 1) == '}')) > 0;

	start = find(chi(1:end - 1) < chi(2:end)) + 2; stop = find(chi(1:end - 1) > chi(2:end)) + 2;

	nested = {};
	
	for k = numel(start):-1:1
		nested{k} = json(start(k):stop(k)); json = [json(1:start(k) - 1), '$OBJECT-', int2str(k), json(stop(k) + 1:end)];
	end
	
end

%--
% parse json
%--

switch json(1)
	
	case '{' % hash

		out = struct; part = str_split(json(2:end - 1), ',');
		
		% NOTE: this undoes the excessive split that happens when we have an array of objects
		
		for k = numel(part):-1:1
			
			if part{k}(end) == ']' && all(part{k} ~= '[')
				part{k - 1} = [part{k - 1}, ', ', part{k}]; part(k) = [];
			end
			
		end
		
		for k = 1:numel(part)
			
			% NOTE: this matches fieldnames that may be quoted
			
			[start, stop, match] = regexp(part{k}, '^"?(\w+)"?:'); 
			
			field = part{k}(match{1}(1):match{1}(2)); value = part{k}((stop + 1):end);
			
			% NOTE: 'genvarname' ensures that hash keys are proper fields
			
			out.(genvarname(field)) = parse_json(value, nested);
			
		end
		
	case '[' % array

		out = iterate(@parse_json, str_split(json(2:end - 1), ','), nested);
			
	otherwise % values (string, number, nested objects, and special values)

		if json(1) == '"'
			
			if strcmp(json, '""')
				out = '';
			else
				out = json(2:end - 1);
			end
			
		% NOTE: we don't start with + (this is implied) and we always include a zero before the decimal
		
		elseif isstrprop(json(1), 'digit') || json(1) == '-' 
		
			out = str2double(json);
			
		elseif numel(json) > 7 && strcmp(json(1:8), '$OBJECT-')
			
			k = str2double(json(9:end));
			
			out = parse_json(nested{k}); 
			
		else
			switch json
				
				case 'true', out = true;
					
				case 'false', out = false;
					
				case 'null', out = [];
				
			end
		end
	
end