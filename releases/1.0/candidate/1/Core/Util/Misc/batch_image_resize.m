function batch_image_resize(source, max_dim, destination)

%--
% handle input
%--

if ~nargin || isempty(source)
	source = pwd;
end

if nargin < 3
	[destination, created] = create_dir(fullfile(source, 'thumbs'));
	
	if isempty(destination)
		error('Unable to create default output destination.');
	end
	
	if ~created
		disp(' '); warning('''thumbs'' directory contents will be overwritten as needed.');
	end
end

if nargin < 2
	max_dim = [1024, 512, 256, 128];
end

%--
% get images to process from source
%--

% TODO: make recursive scan an option

in = scan_dir(source);

for j = 1:numel(in)

	if string_contains(in{j}, 'thumbs')
		continue;
	end
	
	%--
	% get input directory content
	%--
	
	disp(' '); disp(['Scanning ''', in{j}, ''' ... ']);
	
	content = what_ext(in{j}, 'JPG');
	
	% NOTE: continue if no content of interest
	
	if isempty(content.JPG)
		disp(' '); continue;
	end
	
	disp(' '); disp(['Processing ''', in{j}, ''' ... ']); disp(' ');
	
	%--
	% create corresponding output directory
	%--
	
	out = create_dir(strrep(content.path, source, destination));
	
	if isempty(out)
		error('Unable to create output directory.');
	end
	
	%--
	% resize images
	%--
	
	for k = 1:numel(content.JPG)
	
		%--
		% get image file and info
		%--
		
		last_resized = fullfile(content.path, content.JPG{k}); info = imfinfo(last_resized);
		
		disp(['Processing ''', strrep(last_resized, source, '.'), ''' ... (', int2str(k), ' of ', int2str(numel(content.JPG)), ')']);
		
		start = clock;
		
		%--
		% determine scale, resize and write output
		%--

		for l = 1:numel(max_dim)
			
			out_dim{l} = create_dir(fullfile(out, int_to_str(max_dim(l), 9999)));
			
			resized = fullfile(out_dim{l}, content.JPG{k});

			if info.Width < max_dim
				copyfile(file, resized); continue;
			end

			scale = max_dim(l) / info.Width;

			disp(['Creating ''', strrep(resized, source, '.'), ''' ... ']);
			
			imwrite(imresize(imread(last_resized), scale, 'bicubic'), resized);
			
			last_resized = resized; info = imfinfo(last_resized);
			
		end
		
		disp(['Elapsed ', sec_to_clock(etime(clock, start))]);
		
		disp(' ');
		
	end
	
	for k = 1:numel(out_dim)
		disp(['Archiving ''', out_dim{k}, ''' ... ']);
		
		zip(fullfile(out, [int_to_str(max_dim(k), 9999), '.zip']), out_dim{k});
	end
	
	disp(' ');
	
end