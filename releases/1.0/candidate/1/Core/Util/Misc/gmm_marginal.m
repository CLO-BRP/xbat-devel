function out = gmm_marginal(in,d)

% gmm_marginal - create marginal mixture model
% --------------------------------------------
%
% out = gmm_marginal(in,d)
%
% Input:
% ------
%  in - input mixture density model
%  d - marginal dimensions
%
% Output:
% -------
%  out - marginal mixture density model

%---------------------------------------------
% HANDLE INPUT
%---------------------------------------------

%--
% check marginal dimensions
%--

if (round(d) ~= d)
	disp(' ');
	error('Marginal dimensions must be integer values.');
end

if (any(d > in.nin) | any(d < 1))
	disp(' ');
	error('Desired marginal dimensions are out of bounds.');
end

if (~isequal(d,unique(d)))
	disp(' ');
	error('Marginal dimensions must be unique and ordered.');
end

nin = length(d);

%---------------------------------------------
% CREATE MARGINAL MODEL
%---------------------------------------------

%--
% create marginal mixture model
%--

out = gmm(nin,in.ncentres,in.covar_type);

%--
% update centres and covariance
%--

out.centres = in.centres(:,d);

switch (in.covar_type)

	%--
	% spherical covariance
	%--

	case ('sperical')

		% NOTE: no dimension specific covariance implies no changes
		
	%--
	% diagonal covariance
	%--

	case ('diagonal')
		
		out.covars = in.covars(:,d);

	%--
	% full covariance
	%--

	case ('full')

		%--
		% create temporary covariance structure
		%--
		
		% NOTE: select covariance rows and columns
		
		for k = 1:out.ncentres
			
			tk = in.covars(:,:,k); 
			tk = tk(:,d); 
			tk = tk(d,:);
			
			out.covars(:,:,k) = tk;
			
		end
				
	case ('ppca')

	otherwise

end