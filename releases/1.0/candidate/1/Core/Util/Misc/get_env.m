function value = get_env(name, verb)

% get_env - get value of environment variable
% -------------------------------------------
%
% value = get_env(name, verb)
%
% Input:
% ------
%  name - variable name
%  verb - flag (def: false)
%
% Output:
% -------
%  value - variable value

% $Date: 2006-10-09 15:34:23 -0400 (Mon, 09 Oct 2006) $

%--
% handle input and setup
%--

if (nargin < 2) || isempty(verb)
	verb = false;
end

% TODO: update framework to use application data

data = get(0, 'userdata');

if ~isfield(data, 'env')
	value = [];

	if verb
		disp('There is no environment variable structure available.');
	end

	return;
end

%--
% display named variable or existing environment variables
%--

if nargin && ~isempty(name)

	field = genvarname(name); 
	
	if ~isfield(data.env, field)	
		value = [];

		if verb
			disp(['Environment variable ''' name ''' is not currently defined.']);
		end
		
		return;
	end

	value = data.env.(field);
else

	value = data.env;
end
