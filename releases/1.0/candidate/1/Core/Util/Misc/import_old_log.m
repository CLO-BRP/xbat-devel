function log = import_old_log(name)

%--
% get file to import
%--

[file, path] = uigetfile; file = fullfile(path, file);

if ~file
	return;
end

%--
% get old log from file 
%--

old = load(file);

if numel(fieldnames(old)) == 1
	old = struct_pop(old);
end

%--
% create new log
%--

% TODO: get matching sound in library

sound = old.sound;

log = new_log(sound, name);

%--
% copy old log events to new log
%--

% TODO: check that tags are appropiately stored, etc

for k = 1:numel(old.event)
		
end



