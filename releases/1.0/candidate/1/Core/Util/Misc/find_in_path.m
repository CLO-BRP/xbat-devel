function out = find_in_path(pat)

% find_in_path - find path components containing pattern
% ------------------------------------------------------
%
% out = find_in_path(pat)
%
% Input:
% ------
%  pat - pattern to look for, look at 'filter_strings'
%
% Output:
% -------
%  out - path elements containing pattern

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1160 $
% $Date: 2005-07-05 14:55:22 -0400 (Tue, 05 Jul 2005) $
%--------------------------------

%--
% filter path strings to get elements containing pattern
%--

out = filter_strings(str_split(path, pathsep), pat);

%--
% display results if no output was required
%--

if ~nargout
		
	% NOTE: display nothing if nothing ws found
	
	if isempty(out)
		return;
	end
	
	%--
	% prepare display string
	%--
	
	str = strrep(out, '\', '\\'); % escape windows filesep
	
	str = strcat(str, pathsep, '\n'); % append pathsep and newline
	
	str = strcat(str{:}); % concatenate cells
	
	%--
	% display
	%--
	
	disp(' '); disp(sprintf(str));
	
end