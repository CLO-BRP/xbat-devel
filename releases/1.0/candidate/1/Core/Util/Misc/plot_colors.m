function out = plot_colors(n, intensity)

if nargin < 2 || isempty(intensity)
	intensity = 0.85;
end

switch n
	
	case 1, colors = [0, 0, 1];
		
	case 2, colors = [0, 0, 1; 0, 1, 0];
		
	case 3, colors = [0, 0, 1; 0, 1, 0; 1, 0, 0];
		
	otherwise, colors = [plot_colors(3); flipud(hsv(n - 3))];
		
end
	
out = intensity * colors;