function date = offset_date(origin, offset)

% date_time_str - get date string from date and time offset
% ---------------------------------------------------------
%
% date = offset_date(origin, offset)
%
% Input:
% ------
%  origin - base date
%  offset - date offset in seconds
%
% Output:
% -------
%  date - date at offset

%--
% offset date number
%--

% NOTE: the date number is a decimal day representation, offset is day normalized

date = origin + (offset / 86400);
