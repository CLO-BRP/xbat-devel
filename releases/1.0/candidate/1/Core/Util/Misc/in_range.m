function value = in_range(value, low, high)

% in_range - test whether value is in range
% -----------------------------------------
%
% result = in_range(value, low, high)
%
% Input:
% ------
%  value - to test
%  low - boundary
%  high - boundary
%
% Output:
% -------
%  result - test of range 

%--
% set default range to unit interval
%--

switch nargin
	case 1
		low = 0; high = 1;
		
	case 2
		if numel(low) == 2
			high = low(2); low = low(1);
		else
			high = 1;
		end
end

%--
% perform test
%--

value = (value >= low) & (value <= high);