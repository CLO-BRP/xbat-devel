function test_line_numbers(file)

%--
% handle full file or function name input
%--

file = which(file);

if isempty(file)
	disp('Unable to find file.'); return;
end

%--
% test line numbers with various options
%--

opt = file_readlines;

[lines, nos] = file_readlines(file, [], opt);

disp(' '); disp([upper('full file ('), int_to_str(numel(lines)), ')']); disp(' '); display_lines(lines, nos);

opt.pre = '%';

[lines, nos] = file_readlines(file, [], opt);

disp(' '); disp([upper('no comments ('), int_to_str(numel(lines)), ')']); disp(' '); display_lines(lines, nos);

opt.skip = true;

[lines, nos] = file_readlines(file, [], opt);

disp(' '); disp([upper('no comments, no empty ('), int_to_str(numel(lines)), ')']); disp(' '); display_lines(lines, nos);


%----------------------
% DISPLAY_LINES
%----------------------

function display_lines(lines, nos)

if numel(lines) ~= numel(nos)
	error('Lines and numbers do not match.');
end

total = numel(lines);

for k = 1:total
	disp([int_to_str(nos(k), total), '.  ', lines{k}]);
end
