function result = hash(in, type, file)

% hash - compute input hash of specific type
% ------------------------------------------
%
% result = hash(in, type)
%
% Input:
% ------
%  in - to hash
%  type - of hash
%
% Output:
% -------
%  result - of hashing
%
% NOTE: based on http://www.mathworks.com/matlabcentral/fileexchange/8944

% TODO: this 'md5' does not match the output of our MEX version, look at test_hash.m

% NOTE: the MEX file seems wrong look at the C code, comparing with http://www.miraclesalad.com/webtools/md5.php

%-----------------
% HANDLE INPUT
%-----------------

if nargin < 3
	file = 0;
end 

if nargin < 2 || isempty(type)
	type = 'SHA1';
end

%--
% get input byte-stream representation
%--

% NOTE: convert strings and logicals into uint8 format, 'typecast' everything else into uint8

if ~file
	in = in(:);

	if ischar(in) || islogical(in)
		in = uint8(in);
	else
		% NOTE: we are not able to typecast a struct, first we convert to string
		
		% NOTE: this is probably better done outside keeping this function pure, this is convenient
		
		if ~ischar(in)
			in = uint8(to_str(in));
		end
		
		in = typecast(in, 'uint8');
	end
end

%--
% check hash type and normalize
%--

type = upper(type);

switch type
	
    case 'SHA1'
        type = 'SHA-1';
		
    case 'SHA256'
        type = 'SHA-256';
		
    case 'SHA384'
        type = 'SHA-384';
		
    case 'SHA512'
        type = 'SHA-512';
		
end

types = {'MD2','MD5','SHA-1','SHA-256','SHA-384','SHA-512'};

if ~string_is_member(type, types)
    error('Hash type must be MD2, MD5, SHA-1, SHA-256, SHA-384, or SHA-512');
end

%-----------------
% CREATE HASH
%-----------------

%--
% compute input byte-stream hash of given type
%--

digest = java.security.MessageDigest.getInstance(type);

if ~file
	digest.update(in);
else
	fid = fopen(in); chunk = fread(fid, 8192);
	
	while ~isempty(chunk)
		digest.update(chunk); chunk = fread(fid, 8192);
	end
	
	fclose(fid);
end

%--
% create hex representation of hash
%--

% NOTE: this may not be the representation we want

result = typecast(digest.digest, 'uint8'); result = dec2hex(result)';

% NOTE: handle the remote possibility that all hash bytes < 128, in this case we pad

if size(result, 1) == 1 
    result = [repmat('0', [1, size(result, 2)]); result];
end

result = lower(result(:)');

% clear digest;


% HASH - Convert an input variable into a message digest using any of
%        several common hash algorithms
%
% USAGE: result = hash(in,'type')
%
% in  = input variable, of any of the following classes:
%        char, uint8, logical, double, single, int8, uint8,
%        int16, uint16, int32, uint32, int64, uint64
% result    = hash digest output, in hexadecimal notation
% type = hash algorithm, which is one of the following:
%        MD2, MD5, SHA-1, SHA-256, SHA-384, or SHA-512 
%
% NOTES: (1) If the input is a string or uint8 variable, it is hashed
%            as usual for a byte stream. Other classes are converted into
%            their byte-stream values. In other words, the hash of the
%            following will be identical:
%                     'abc'
%                     uint8('abc')
%                     char([97 98 99])
%            The hash of the follwing will be different from the above,
%            because class "double" uses eight byte elements:
%                     double('abc')
%                     [97 98 99]
%            You can avoid this issue by making sure that your inputs
%            are strings or uint8 arrays.
%        (2) The name of the hash algorithm may be specified in lowercase
%            and/or without the hyphen, if desired. For example,
%            result = hash('my text to hash','sha256');
%        (3) Carefully tested, but no warranty. Use at your own risk.
%        (4) Michael Kleder, Nov 2005
%
% EXAMPLE:
%
% algs = {'MD2','MD5','SHA-1','SHA-256','SHA-384','SHA-512'};
% for n = 1:6
%     result = hash('my sample text',algs{n});
%     disp([algs{n} ' (' num2str(length(result)*4) ' bits):'])
%     disp(result)
% end



