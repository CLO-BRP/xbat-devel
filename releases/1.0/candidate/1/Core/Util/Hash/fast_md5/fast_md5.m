function value = fast_md5(in, file)

% fast_md5 - computation using java library
% -----------------------------------------
%
% value = fast_md5(in, file)
%
% Input:
% ------
%  in - string 
%  file - input indicator
%
% Output:
% -------
%  value - of md5 hash

% NOTE: a shortcut to hash files is 'file_md5', it also tests the consistency of the fast and built-in java version

%--
% handle input
%--

if nargin < 2
	file = false;
end

%--
% compute hash
%--

obj = javaObject('com.twmacinta.util.MD5');

if file
	in = javaObject('java.io.File', in);
	
	value = obj.asHex(obj.getHash(in));
else
	if ~ischar(in)
		in = to_str(in);
	end
	
	obj.Update(in);
	
	value = obj.asHex();
end 

value = char(value);


