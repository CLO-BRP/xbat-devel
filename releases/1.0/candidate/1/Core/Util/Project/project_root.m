function [root, exists] = project_root(name, author)

% project_root - base directory for project
% -----------------------------------------
%
% root = project_root(project)
%
% [root, exists] = project_root(name, author)
%
% Input:
% ------
%  project - struct
%  name - of project
%  author - name
%
% Output:
% -------
%  root - directory of project
%  exists - indicator
%
% NOTE: if it does not exist we do not create this directory without being asked to

%--
% handle input
%--

% NOTE: we get project names from project struct

if nargin < 2 && isstruct(name)
	project = name;	
	
	author = project.author; name = project.name;
end

%--
% get project root from author and name, this convention constraints naming
%--

if ~proper_filename(author) || ~proper_filename(name) 
	error('Project name and author must be proper filenames.');
end

root = fullfile(projects_root, author, name);

if nargout > 1
	exists = exist_dir(root);
end
