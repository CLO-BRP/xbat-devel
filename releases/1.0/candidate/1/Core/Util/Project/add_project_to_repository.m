function [added, info] = add_project_to_repository(project, repository)

% add_project_to_repository - using name and possibly repository
% --------------------------------------------------------------
%
% [added, info] = add_project_to_repository(project, repository)
%
% Input:
% ------
%  project - struct or name
%  repository - address
%
% Output:
% -------
%  added - indicator
%  info - for working copy
%
% NOTE: we output 1 on adding, zero if we are already under version control, -1 if we failed

% TODO: add 'repository' input and check for repository when we get the project

if ischar(project)
	project = get_project(project);	
end

if isempty(project.repository) 
	
	if (nargin < 2 || isempty(repository))
		error('Unable to determine repository to add to.');
	end 
	
	project.repository = repository;
end

%--
% check if we are in the repository
%--

[done, info] = project_in_repository(project.name, project.author);

% NOTE: if no output was requested display subversion info

if done
	added = 0; if ~nargout, disp(info); clear; end; return;
end

%--
% regenerate project function
%--

generate_project_function(project);

%--
% import project
%--

root = project_root(project.name, project.author);

repo = project_repository_root(project);

[status, result] = svn('import', '-m import', root, repo);

if status
	disp(result); added = -1; return;
end

% TODO: consider making a call to 'install_repository_projects' to be more DRY

% TODO: backup project before deleting

remove_path(root); rmdir(root, 's');

[status, result] = svn('checkout', repo, root);

if status
	disp(result); added = -1; return;
end

append_path(root);

% TODO: implement adding to repository, we have not used these functions much

added = 1;

%--
% get project version subversion info
%--

[done, info] = project_in_repository(project.name, project.author); %#ok<ASGLU>

%--
% display result
%--

if ~nargout
	disp(info); clear;
end

