function [added, project] = install_project(repository, project)

% install_project - install project from repository
% -------------------------------------------------
%
% [added, project] = install_project(repository, project)
%
% Install:
% --------
%  repository - address
%  project - name
%
% Output:
% -------
%  added - project names
%  project - descriptions

%--
% handle input
%--

% NOTE: get repository and project information through dialog if needed

switch nargin
	
	case 0
		[repository, project] = install_project_dialog;

	case 1
		
		% NOTE: just a little paranoia and convenience

		if iscell(repository)
			repository = repository{1};
		end

		[repository, project] = install_project_dialog(repository);
		
end
	
% NOTE: consider whether we cancelled dialog

if isempty(repository)
	added = 0; project = []; return;
end

%--
% check for availability of requested project or projects
%--

% NOTE: this is a bit paranoid in the case where we went through the dialog

available = get_repository_projects(repository); unknown = setdiff(project, available);

% TODO: consider using the 'added' output to handle the 'unknown' projects

if ~isempty(unknown)
	% TODO: display that some projects are not available, perhaps throw error
end

project = setdiff(project, unknown);

%--
% install projects
%--

% NOTE: we do not know the project author until after installation, later we organize these

author = 'Unknown';

for k = 1:numel(project)
	
	root = project_root(project{k}, author);

	repo = project_repository_root(project{k}, repository);

	start = clock;
	
	disp(' '); disp(['Installing ''', project{k}, ''' from ''', repository, '''.']);
	
	[status, result] = svn('checkout', repo, root);
	
	if status
		disp(['Failed. (', sec_to_clock(etime(clock, start)), ')']); disp(' '); disp(result); added(k) = -1; continue;
	end

	append_path(root); added(k) = 1; 
	
	disp(['Done. (', sec_to_clock(etime(clock, start)), ')']);
	
end

organize_projects;

%--
% little display details
%--

if any(added == 1)
	disp(' ');
end

if ~nargout
	clear;
end


%--------------------------------
% INSTALL_PROJECT_DIALOG
%--------------------------------

function [repository, project] = install_project_dialog(repository)

%--
% handle input and setup
%--

known = get_project_repositories; 

if ~nargin
	repository = known{1};
end

%--
% create controls
%--

control = empty(control_create);

ix = find(strcmp(known, repository));

if isempty(ix)
	ix = 1;
end

control(end + 1) = header_control('Project', 0);

control(end + 1) = control_create( ...
	'name', 'repository', ...
	'style', 'popup', ...
	'onload', 1, ...
	'string', known, ...
	'value', ix ...
);

control(end + 1) = control_create( ...
	'name', 'project', ...
	'alias', 'name', ...
	'style', 'listbox', ...
	'lines', 4, ...
	'space', 1.5, ...
	'string', '(No Projects Found)', ...
	'value', 1 ...
);

%--
% render dialog and consider result
%--

opt = dialog_group; opt.width = 15;

out = dialog_group('Install Project ...', control, opt, @install_dialog_callback);

if isempty(out.values)
	repository = ''; project = ''; return;
end

repository = out.values.repository{1};

project = out.values.project{1};


%--------------------------------
% INSTALL_DIALOG_CALLBACK
%--------------------------------

function install_dialog_callback(obj, eventdata)

%--
% setup callback context
%--

callback = get_callback_context(obj, eventdata); 

pal = callback.pal;

%--
% callback based on context
%--

switch callback.control.name
	
	case 'repository'
		
		% NOTE: we set the pointer to watch since the callback involves an uncertain network operation 
		
		set(pal.handle, 'pointer', 'watch');
		
		repository = callback.value{1};
		
		project = get_repository_projects(repository);
		
		handle = get_control_handles(pal.handle, 'project');
		
		% TODO: the call to 'all_projects' should be 'installed = all_projects(repository);'
		
		installed = all_projects('repository', repository);
		
		if isempty(project)
			project = '(No Projects Found)';
		else
			installed = all_projects; project = setdiff(project, {installed.name});
		end
		
		if isempty(project)
			project = '(All Projects Installed)';
		end
		
		set(handle.obj, 'string', project, 'value', 1);
		
		set(pal.handle, 'pointer', 'arrow');
		
end
