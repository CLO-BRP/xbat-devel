function repos = add_project_repository(repo)

% add_project_repository - to list of known repositories
% ------------------------------------------------------
%
% repos = add_project_repository(repo)
%
% Input:
% ------
%  repo - to add
%
% Output:
% -------
%  repos - full list

[repos, file] = get_project_repositories;

if string_is_member(repo, repos)
	return;
end 

repos{end + 1} = repo;

file_writelines(file, repos);

repos = get_project_repositories;