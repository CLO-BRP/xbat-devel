function value = project_exists(name, author)
	
value = exist(project_root(name, author), 'dir');

% NOTE: calling this function without capturing output asserts project existence

if ~nargout && ~value
	error(['Project ''', name, ''' by ''', author, ''' does not exist.']);
end