function project = project_create(name, description, author, repository)

if nargin < 4
	repository = '';
end

if nargin < 3
	author = '';
end

if nargin < 2
	description = '';
end

if nargin < 1
	name = '';
end

% TODO: enforce filename constraints for name and author

% NOTE: trim paranoia

project.name = strtrim(name);

project.description = strtrim(description);

project.author = strtrim(author);

project.repository = strtrim(repository);

project.fun = [];