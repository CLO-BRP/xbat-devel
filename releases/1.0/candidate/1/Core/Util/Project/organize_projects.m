function organize_projects

% organize_projects - using directory convention
% ----------------------------------------------
%
% organize_projects
%
% NOTE: all available projects locations are checked to be conventional
% 
% NOTE: conventional project locations are Projects/AUTHOR/PROJECT_NAME

%--
% check all projects are properly installed
%--

project = all_projects;

for k = 1:numel(project)
	%--
	% get currrent and conventional locations for project
	%--
	
	% NOTE: we get the current location by examining the project function location
	
	fun = functions(project(k).fun); current = fileparts(fun.file);

	if isempty(project(k).author)
		project(k).author = 'Unknown';
	end
	
	root = project_root(project(k).name, project(k).author);

	%--
	% move project to conventional location if needed
	%--
	
	% NOTE: we take care to remove and readd the project directories to the path
	
	if ~strcmp(current, root)
		remove_path(current);
	
		% NOTE: this statement creates the author parent directory as needed
		
		movefile(current, fileparts(root), 'f');
		
		append_path(root);
	end
end

%--
% check for and remove empty author directories
%--

author = no_dot_dir(projects_root, true);

for k = 1:numel(author)
	current = fullfile(projects_root, author(k).name);
	
	content = no_dot_dir(current, true);
	
	if isempty(content)
		remove_path(current); rmdir(current, 's');
	end
end 
