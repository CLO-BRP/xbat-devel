function project = organize_projects_by_author

% organize_projects_by_author - utility to update project organization
%
% NOTE: make sure that all currently installed projects have a non-empty author
%
% NOTE: make sure that project names do not clash with author names

%--
% load projects using the former convention
%--

content = no_dot_dir(projects_root, 1); project = [];

for k = 1:numel(content) 
	
	name = content(k).name; fun = str2func([genvarname(name), '__project']);
	
	try
		if isempty(project)
			project = fun();
		else
			project(end + 1) = fun();
		end 
	catch
		nice_catch(lasterror, ['Failed to load project ''', content(k).name, '''.']);
	end
	
end

%--
% create author directories
%--

% NOTE: we have a list of each author, and known authors

author = {project.author}; known = unique(author);

disp(' ');

for k = 1:numel(known)
	container = fullfile(projects_root, known{k});
	
	create_dir(container);
	
	disp(['Created ', container]);
end

%--
% copy projects to new organization
%--

disp(' ');

for k = 1:numel(project)
	source = fullfile(projects_root, project(k).name);
	
	destination = fullfile(projects_root, project(k).author, project(k).name);
	
	copyfile(source, destination, 'f');
	
	disp(['Source: ', source]); disp(['Destination: ', destination]); disp(' ');
end



