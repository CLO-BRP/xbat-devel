function initial = initial_enable(value)

% initial_enable - convert binary value to enable state string
% ------------------------------------------------------------
%
% initial = initial_enable(value)
%
% Input:
% ------
%  value - enable indicator
%
% Output:
% -------
%  initial - initial state enable string

if value
	initial = '__ENABLE__';
else
	initial = '__DISABLE__';
end
