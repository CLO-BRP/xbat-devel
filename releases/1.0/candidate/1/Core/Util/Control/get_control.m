function [out, controls] = get_control(pal, name, opt, controls)

% get_control - get control from palette
% --------------------------------------
%
%     out = get_control(pal, name)
%
%         = get_control(pal, name, 'all')
%
% control = get_control(pal, name, 'control')
%
% handles = get_control(pal, name, 'handles')
%
%   value = get_control(pal, name, 'value')
%
%   range = get_control(pal, name, 'range')
%
%   label = get_control(pal, name, 'label')
%
% Input:
% ------
%   pal - parent palette handle
%  name - control name
%
% Output:
% -------
%      out - output container
%  control - control
%  handles - handles
%    value -  value
%    label - label

%--------------------------------
% HANDLE INPUT
%--------------------------------

if nargin < 4
	controls = get_palette_controls(pal);
end

%--
% set default output option
%--

if (nargin < 3) || isempty(opt)
	opt = 'all';
end

%--
% check output option
%--

opts = {'all', 'control', 'handles', 'value', 'range', 'label', 'string', 'index', 'userdata'};

if ~string_is_member(opt, opts)
	error(['Unknown output option ''', opt, '''.']);
end

%--
% check palette handle
%--

if ~is_palette(pal)
	out = []; return; % error('Input handle is not a palette.');
end

%--------------------------------
% GET CONTROL
%--------------------------------

%--
% get control
%--

control = get_control_by_name(pal, name, controls);

% NOTE: return simple empty when control is not found , consider some kind of message here

if isempty(control)
	out = []; return;
end

%--
% get further control info and pack output
%--

% NOTE: the lower level functions return simple empty on empty

switch opt
	
	case 'control', out = control;
	
	case 'userdata' % NOTE: this will not work as expected on headers and waitbars, possibly others
		
		handles = get_control_handles(pal, name, control);
		
		out = get(handles.uicontrol.text, 'userdata'); 
		
	case {'handles', 'label', 'string', 'index'}
		
		% NOTE: this does not get all header handles
		
		%--
		% handles code
		%--
		
		handles = get_control_handles(pal, name, control);
		
		if isempty(handles.all)
			out = []; return;
		end
		
		if strcmp(opt, 'handles')
			out = handles; return;
		end

		if strcmp(opt, 'string')
			out = get(handles.obj, 'string'); return;
		end

		if strcmp(opt, 'index')
			
			out = get(handles.obj, 'value');
			
			if ~isnumeric(out)
				out = []; 
			end
			
			return;
		end
			
		%--
		% label code
		%--
		
		out = get_control_label(handles);
	
	case {'value', 'range', 'all'}
		
		%--
		% required for value and hence for all
		%--
		
		out.control = control;
		
		out.handles = get_control_handles(pal, name, control);
		
		% NOTE: discard control and handles from output if not 'all'
		
		switch opt
			case 'value'
				out = get_value(out); return;
				
			case 'range'
				out = get_range(out); return;
		end
		
		out.value = get_value(out);
		
		out.range = get_range(out);
		
		out.label = get_control_label(out.handles);
end


%----------------------------------------------
% GET_CONTROL_LABEL
%----------------------------------------------

function label = get_control_label(handles)

% TODO: consider the case of multiple text handles, mark label in some way

if ~isfield(handles, 'uicontrol') || ~isfield(handles.uicontrol, 'text') || isempty(handles.uicontrol.text)
	label = []; return;
end

label = get(handles.uicontrol.text, 'string');


%----------------------------------------------
% GET_VALUE
%----------------------------------------------


function value = get_value(control)

value = get_value_from_handles(control);

% NOTE: here we consider 'set' callback availability

value = get_get_value(control.control, value);



function value = get_value_from_handles(control)

%--
% return quickly if there are no handles
%--

% TODO: implement hidden control here, the value is stored in the palette

if isempty(control.handles)
	value = []; return;
end

% TOGGLEGROUP

if strcmp(control.control.style, 'togglegroup')
	
	handles = control.handles.uicontrol.togglebutton;
	
	string = get(handles, 'string'); state = cell2mat(get(handles, 'value'));
	
	% NOTE: the 'string' and 'state' field include information about all labels
	
	value.string = string; value.state = state; 
	
	% NOTE: the 'selected' field indicates which strings are selected
	
	value.selected = value.string; value.selected(~value.state) = [];
	
	return;
end

% PASSWORD

if strcmp(control.control.style, 'password')
	
	value = get(control.handles.jobj, 'password')'; return;
end

% HEADER

if strcmp(control.control.style, 'separator') && strcmp(control.control.type, 'header')

	value = get(control.handles.header_toggle, 'string'); return;
end

% TODO: make use of control struct for efficiency and robustness

%--
% get control handles
%--

% TODO: use the handle struct and be smarter

handles = control.handles.all;

%--
% get control value
%--

%------------------------------
% AXES
%------------------------------

% NOTE: this also gets the value from a rating control

ix = find(strcmp(get(handles, 'type'), 'axes'));

if ~isempty(ix)
	value = get(handles(ix), 'userdata'); return;
end

%------------------------------
% SLIDER
%------------------------------

% NOTE: slider first yields the correct result for controls composed of a slider and edit box

ix = find(strcmp(get(handles, 'style'), 'slider'));

if ~isempty(ix)
	value = get(handles(ix), 'value'); return;
end

%------------------------------
% EDIT
%------------------------------

% NOTE: we convert multiple row character arrays to string cell arrays

ix = find(strcmp(get(handles, 'style'), 'edit'));

if ~isempty(ix)

	value = get(handles(ix), 'String');

	if size(value, 1) > 1
		value = cellstr(value);
	end

	return;
end

%------------------------------
% POPUPMENU
%------------------------------

% TODO: the output value of popup menus should be a string, change with care

ix = find(strcmp(get(handles, 'style'), 'popupmenu'));

if ~isempty(ix)

	% NOTE: we get value by dereferencing string with value

	strings = get(handles(ix), 'string');
    
    if ~iscell(strings)
        strings = {strings};
    end
	
	value = strings(get(handles(ix), 'value')); % results in cell

	return;
end

%------------------------------
% LISTBOX
%------------------------------

% NOTE: result is a cell array, of strings if not empty

ix = find(strcmp(get(handles, 'style'), 'listbox'));

if ~isempty(ix)
	
	% NOTE: we get value by dereferencing string with value
	
	strings = get(handles(ix), 'string');
		
	if isempty(strings)
		value = {}; return;
	end
	
	if ischar(strings)
		strings = {strings};
	end
	
	ix = get(handles(ix), 'value');
	
	if isempty(ix)
		value = {}; return;
	end
	
	value = strings(ix); return;
end

%------------------------------
% CHECKBOX
%------------------------------

ix = find(strcmp(get(handles, 'style'), 'checkbox'));

if ~isempty(ix)
	value = get(handles(ix), 'value'); return;
end

%------------------------------
% PUSHBUTTON
%------------------------------

ix = find(strcmp(get(handles, 'style'), 'pushbutton'));

if ~isempty(ix)
	value = get(handles(ix), 'userdata'); return;
end


%----------------------------------------------
% GET_RANGE
%----------------------------------------------

function range = get_range(control)

range = [];

%--
% return quickly if there are no handles
%--

if isempty(control.handles)
	return;
end

%--
% get main control handle
%--

obj = control.handles.obj;

if isempty(obj) || ~ishandle(obj)
	return;
end

switch get(obj, 'type')
	
	case 'axes'	
		range.xlim = get(obj, 'xlim'); range.ylim = get(obj, 'ylim');
		
	case 'uicontrol'
		
		switch get(obj, 'style')
			
			case {'listbox', 'popupmenu'}, range = get(obj, 'string');
				
			case {'edit', 'text'}, range = '';
				
			otherwise, range = [get(obj, 'min'), get(obj, 'max')];	
		end	
end
