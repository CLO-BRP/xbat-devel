function pal = find_waitbar(name)

% find_waitbar - find named waitbar 
% ---------------------------------
%
% pal = find_waitbar(name)
%
% Input:
% ------
% name - waitbar name
%
% Output:
% -------
%  pal - waitbar handle

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

%--
% get all waitbars
%--

pal = get_xbat_figs('type', 'waitbar');

%--
% return quickly if there are no waitbars
%--

if isempty(pal)
	return;
end

%--
% find named waitbar
%--

tags = get(pal, 'tag');

ix = find(strcmp(['XBAT_WAITBAR::', name], tags));

if isempty(ix)
	pal = []; return;
end

pal = pal(ix);
