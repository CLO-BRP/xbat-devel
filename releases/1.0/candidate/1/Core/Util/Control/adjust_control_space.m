function control = adjust_control_space(control, next)

% adjust_control_space - adjust control spacing considering next
% --------------------------------------------------------------
%
% control = adjust_control_space(control, next)
%
% Input:
% ------
%  control - to adjust
%  next - next control
%
% Output:
% -------
%  control - adjusted

%--
% handle input
%--

if nargin < 2
	return;
end 

% NOTE: this is the ugliest duckling ever, why even bother

if ~isfield(control, 'space')
	error('Control must have space field to adjust.');
end

if ~isfield(next, 'style')
	error('Next control does not have style field.');
end

%--
% adjust control
%--

switch next.style
		
	case 'axes'
		if next.label
			control.space = 0.75;
		else
			control.space = 1.5;
		end

	case 'buttongroup'
		control.space = 1;

	case {'edit', 'popup'}
		control.space = 0.75;

	case 'separator'
		control.space = 1.5;

	case 'tabs'
		control.space = 0.1;
		
	otherwise
		control.space = 1;

end


% FEATURE MENU

% switch (ext_controls(1).style)
% 	
% 	case ('separator')
% 
% 		if (~isempty(ext_controls(1).string))
% 			control(end).space = 1.25;
% 		end
% 
% 	case ('tabs'), control(end).space = 0.10;
% 
% 	case ('popup'), control(end).space = 0.75;
% 
% end


