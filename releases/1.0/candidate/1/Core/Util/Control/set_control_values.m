function count = set_control_values(pal, value)

% set_control_values - set control values
% ---------------------------------------
% 
% count = set_control_values(pal, value)
%
% Input:
% ------
%  pal - palette figure
%  value - value struct
%
% Output:
% -------
%  count - number of controls updated

% TODO: update to handle hierarchical value structs using 'flatten'

% TODO: consider performing onload callbacks

count = 0;

%--
% handle trivial case
%--

% NOTE: this should not happen eventually, but it still does ocasionally

if isempty(value)
	return;
end

%--
% fieldnames are control names
%---

field = fieldnames(value); 

for k = 1:length(field)
	
	control = set_control(pal, field{k}, 'value', value.(field{k})); 
	
	count = count + ~isempty(control);
	
end
