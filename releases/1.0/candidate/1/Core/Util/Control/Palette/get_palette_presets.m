function [preset, file] = get_palette_presets(user, varargin)

% get_palette_presets - for user
% ------------------------------
%
% [preset, file] = get_palette_presets(user)
%
% 
% Input:
% ------
%  user - in context
%
% Output:
% -------
%  preset - list
%  file - name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%--
% handle input
%--

if ~nargin || isempty(user)
	user = get_active_user;
end

preset = [];

if isempty(user)
	return;
end

%--
% make sure we have palette preset directory
%--

root = create_dir(palette_preset_root(user));

if isempty(root)
	error('Unable to create palette preset root.');
end

%--
% load presets from matfiles in directory
%--

content = what_ext(root, 'mat'); file = content.mat(:);

% NOTE: return if there are no preset files

if isempty(content)
	return;
end

for k = numel(file):-1:1
	
	% NOTE: failure removes file name from list
	
	try
		loaded = load(fullfile(root, file{k}), 'state');
	catch
		file(k) = []; continue;
	end
		
	if isempty(preset)
		preset = loaded.state;
	else
		preset(end + 1) = loaded.state;
	end
	
end

% NOTE: we should reverse presets so they match files

preset = flipud(preset(:));


