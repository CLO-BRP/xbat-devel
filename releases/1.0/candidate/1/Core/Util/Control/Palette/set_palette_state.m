function set_palette_state(pal, state)

% set_palette_state - set position, toggle, and tab state
% -------------------------------------------------------
%
% flag = set_palette_state(pal, state)
%
% Input:
% ------
%  pal - palette to apply state to
%  state - state of palette (previously obtained using get_palette_state)
%
% Output:
% -------
%  flag - success flag
%
% See also: get_palette_state, palette_toggle

%-------------------------
% HANDLE INPUT
%-------------------------

if (nargin < 3) || isempty(slow)
	slow = 0;
end

%-------------------------
% SET TOGGLE STATES
%-------------------------

if ~isempty(state.toggle)
	%--
	% get current palette toggles
	%--
	
	% NOTE: use of the toggle handles appears in 'min_max_palette'
	
	toggle = findobj(pal, 'tag', 'header_toggle');
	
	par = get(toggle, 'parent');
	
	if iscell(par)
		par = cell2mat(par);
	end
	
	name = get(par, 'tag');
	
	if slow
		stop(timerfind);
	end

	for k = 1:length(state.toggle)

		ix = find(strcmp(state.toggle(k).name, name));

		if ~isempty(ix)
			palette_toggle(pal, toggle(ix), state.toggle(k).state);
			
			if slow
				drawnow; pause(0.025);
			end
		end
	end

	if slow
		start(timerfind);
	end
end

%----------------------------------------------------
% SET TAB STATES
%----------------------------------------------------

% NOTE: tab selection only works when palette tabs have unique names

if ~isempty(state.tabs)
	
	for k = 1:length(state.tabs)
		tab_select([], pal, state.tabs(k).tab);
	end
	
end

%----------------------------------------------------
% SET POSITION
%----------------------------------------------------

if ~ishandle(pal) || strcmp(get(pal, 'beingdeleted'), 'on')
	return;
end 

pos = get(pal, 'position');
	
pos(1:2) = state.position(1:2);

set(pal, 'position', pos);

% NOTE: this helps reduce display problems on multiple monitors

drawnow;


