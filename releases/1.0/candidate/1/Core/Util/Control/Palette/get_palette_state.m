function state = get_palette_state(pal, data)

% get_palette_state - get position, toggle, and tab state
% -------------------------------------------------------
%
% state = get_palette_state(pal, data)
%
% Input:
% ------
%  pal - palette figure handle
%  data - palette userdata
%
% Output:
% -------
%  state - palette state
%
% See also: set_palette_state, palette_toggle

% TODO: factor the reading of the toggle state from this function

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

%--
% get palette userdata if needed
%--

if (nargin < 2) || isempty(data)
	data = get(pal, 'userdata');
end

%----------------------------------------------------
% GET NAME, TAG, AND POSITION
%----------------------------------------------------

% NOTE: use absolute position, relative position causes problems

state.name = get(pal, 'name');

state.tag = get(pal, 'tag');

state.position = get(pal, 'position');

%----------------------------------------------------
% GET TOGGLE STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'toggle') && ~isempty(data.toggle)
	%--
	% loop over toggles
	%--
	
	for k = 1:length(data.toggle)
		
		tmp = data.toggle(k).toggle;
		
		%--
		% get toggle name
		%--
		
		state.toggle(k).name = get(get(tmp, 'parent'), 'tag');
		
		%--
		% get toggle state
		%--
		
		if strcmp(get(tmp, 'string'), '+')
			state.toggle(k).state = 'close';
		else
			state.toggle(k).state = 'open';
		end
	end
	
% 	db_disp toggle-state; disp(state.toggle)
else
	%--
	% there are no toggles
	%--
	
	state.toggle = [];
end

%----------------------------------------------------
% GET TAB STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'tabs') && ~isempty(data.tabs)
	%--
	% loop over tabs
	%--
	
	for k = 1:length(data.tabs)
		%--
		% get text children of tabs axes
		%--
				
		% NOTE: problems arise when we have elements with no name
		
		label = findobj(get(findobj(pal, 'tag', data.tabs(k).name), 'children'), 'type', 'text');
				
		%--
		% select darkest color label to be the active tab 
		%--
		
		% NOTE: get colors, convert to matrix, sum along columns, and get the minimum value index
		
		if numel(label) == 1
			ix = 1;
		else
			[ignore, ix] = min(sum(cell2mat(get(label, 'color')), 2));
		end 
		
		%--
		% store active tab name
		%--
		
		state.tabs(k).tab = get(label(ix), 'string');
	end
else
	%--
	% there are no tabs
	%--
	
	state.tabs = [];
end
