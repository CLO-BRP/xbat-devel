function name = get_palette_name(pal)

% get_palette_name - get name of palette
% --------------------------------------
%
% name = get_palette_name(pal)
%
% Input:
% ------
%  pal - palette handle
%
% Output:
% -------
%  name - palette name

%--------------------------
% HANDLE INPUT
%--------------------------

if ~is_palette(pal)
	error('Input is not palette handle.');
end

%--------------------------
% GET NAME FROM TAG
%--------------------------

% NOTE: palette's are missing an alias, store the name is in the tag not the figure name

fields = {'type', 'subtype', 'name'};

info = parse_tag(get(pal, 'tag'), '::', fields);

% NOTE: at the moment palette tags consist of two or three parts

if isempty(info.name)
	info.name = info.subtype;
end

name = info.name;
