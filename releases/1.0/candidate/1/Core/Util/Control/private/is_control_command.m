function value = is_control_command(command)

% is_control_command - check whether input is control command
% -----------------------------------------------------------
%
% value = is_control_command(command)
%
% Input:
% ------
%  command -  command
%
% Output:
% -------
%  value - command indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3297 $
% $Date: 2006-01-29 12:34:00 -0500 (Sun, 29 Jan 2006) $
%--------------------------------

% TODO: consider using 'ismember' and discarding this function altogether

value = ~isempty(find(strcmp(get_control_commands,command)));