function handles = get_control_handles(pal, name, control)

% get_control_handles - get control handles by name
% -------------------------------------------------
%
% handles = get_control_handles(pal, name, control)
%
% Input: 
% ------
%  pal - palette handle
%  name - control name
%  control - control
%
% Output:
% -------
%  handles - palette handle structure

%-------------------------------------------
% CHECK FOR HANDLES
%-------------------------------------------

% NOTE: because 'buttongroups' are groups, we cannot reliably store handles

if nargin > 2 && isfield(control, 'handles') && ~trivial(control.handles) && ~strcmp(control.style, 'buttongroup')
	
	handles = control.handles; return;
end

%-------------------------------------------
% CREATE STRUCT OF HANDLES
%-------------------------------------------

%--
% field for main callback control and all control related handles
%--

% NOTE: these get filled at the end, it just makes the struct more readable to have them first

handles.obj = []; 

handles.jobj = []; 

handles.all = [];

%-------------------------------------------
% EXPERIMENT

% NOTE: we are interested in handling header controls uniformly

if strcmp(control.style, 'separator') && strcmp(control.type, 'header')
	
	handles.all.axes = findobj(pal, 'type', 'axes', 'tag', control.name);

	child = get(handles.all.axes, 'children');
	
	for k = 1:numel(child)
		current = child(k); handles.(get(current, 'tag')) = current;
	end
	
	if isfield(handles, 'header_toggle')
		handles.obj = handles.header_toggle;
	end
	
	return;
end

%-------------------------------------------

%--
% struct for uicontrol handles
%--

% NOTE: we get and sort all 'uicontrol' handles

handles.uicontrol.all = findobj(pal, 'type', 'uicontrol', 'tag', name);

for k = 1:length(handles.uicontrol.all)
	
	%--
	% copy control handle and get style for convenience
	%--
	
	handle = handles.uicontrol.all(k); style = get(handle, 'style');
	
	%--
	% store handle in style bin, creating bin if needed
	%--
	
	if ~isfield(handles.uicontrol, style)
		handles.uicontrol.(style) = handle;
	else
		handles.uicontrol.(style)(end + 1) = handle;
	end
end

%--
% get hybrid control handles, collect all handles
%--

handles = get_hybrid_handles(pal, name, control, handles); 

handles = collect_all_handles(handles);

% NOTE: this is probably not a good idea 

if isempty(handles.all)
	handles = []; return;
end

%--
% get java handles
%--

% NOTE: this code is contingent on the 'uicomponent' packing, add exception handler

handles.java = findobj(pal, 'type', 'hgjavacomponent', 'tag', name);

%--
% select main object handle
%--

handles = select_obj_handle(handles, control); 
		
% NOTE: we also get the main object java handle 

% NOTE: we only take advantage of the 'edit' box text updating

% if isfield(control, 'style') && strcmp(control.style, 'edit')
% 	
% 	handles.jobj = findjobj(handles.obj);
% 
% end

%--
% store results for future use
%--

if nargin > 2 && ~strcmp(control.style, 'buttongroup')
	
	control.handles = handles; set_control_by_name(pal, control);
end


%-------------------------------------------
% COLLECT_ALL_HANDLES
%-------------------------------------------

function handles = collect_all_handles(handles)

handles.all = handles.uicontrol.all;

type = hybrid_types;

for k = 1:length(type)	
	
	if ~isempty(handles.(type{k}))
		handles.all = [handles.all; handles.(type{k})];
	end
end


%-------------------------------------------
% SELECT_OBJ_HANDLE
%-------------------------------------------

function handles = select_obj_handle(handles, control)

switch control.style
	
	% NOTE: these controls are named as the uicontrol
	
	case {'checkbox', 'edit', 'listbox', 'slider'}
		
		handles.obj = handles.uicontrol.(control.style);
	
	% NOTE: these controls store value information in axes
	
	case {'axes', 'rating'}
		
		handles.obj = handles.axes;
	
	% NOTE: these controls have a shorthand name
	
	case 'popup'
		
		handles.obj = handles.uicontrol.popupmenu;
		
	case 'buttongroup'
		
		handles.obj = handles.uicontrol.pushbutton;
		
	% NOTE: these are the 'uicomponent' based java controls
	
	case 'password'
		
		handles.obj = handles.java; handles.jobj = get(handles.obj, 'userdata');
end


%-------------------------------------------
% PURE_CONTROL
%-------------------------------------------

function value = pure_control(control)

% pure = {'checkbox', 'edit', 'listbox', 'slider', 'popup', 'buttongroup'};

hybrid = {'axes', 'rating', 'separator', 'tabs'};

value = ~string_is_member(control.style, hybrid);


%-------------------------------------------
% GET_HYBRID_HANDLES
%-------------------------------------------

function handles = get_hybrid_handles(pal, name, control, handles)

% NOTE: there are the elements that can be found in hybrid controls

type = hybrid_types;

%--
% return quickly for pure controls
%--

if pure_control(control)
	
	for k = 1:length(type)
		handles.(type{k}) = [];
	end
	
	return;
end

%--
% get hybrid control element handles
%--

for k = 1:length(type)
	handles.(type{k}) = findobj(pal, 'type', type{k}, 'tag', name);
end

%--
% handle special cases for axes and text objects
%--

% NOTE: most of these can be solved by careful choice of control names

if ~isempty(handles.axes)
	
	%--
	% remove palette toggle axes from control axes
	%--
	
	obj = findobj(handles.axes, 'buttondownfcn', 'palette_toggle'); 

	if ~isempty(obj)
		handles.axes = setdiff(handles.axes, obj); 
	end
	
	%--
	% get rating markers
	%--
	
	% NOTE: consider a more general way to get axes children
	
	if strcmp(control.style, 'rating')
		
		markers = findobj(handles.axes, 'type', 'line'); pos = cell2mat(get(markers, 'xdata'));
		
		[ignore, ix] = sort(pos); markers = markers(ix); %#ok<ASGLU>
		
		handles.markers = markers;
	end
end

if ~isempty(handles.text)
	
	obj = findobj(handles.text, 'backgroundcolor', 'none'); 
	
	if ~isempty(obj)
		handles.text = setdiff(handles.text, obj); 
	end
end


%-------------------------------------------
% HYBRID_TYPES
%-------------------------------------------

function type = hybrid_types

type = {'axes', 'text'};
