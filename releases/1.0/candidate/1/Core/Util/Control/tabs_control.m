function control = tabs_control(tabs, varargin)

% tabs_control - create shortcut
% ------------------------------
%
% control = tabs_control(tabs, field, value, ... )
%
% Input:
% ------
%  tabs - labels
%  field - name
%  value - for field
%
% Output:
% -------
%  control - struct

%--
% create control using provided information
%--

% NOTE: the way we pass the variable arguments tests that these are proper within 'control_create'

control = control_create( ...
	'style', 'tabs', ...
	'tab', tabs, ...
	varargin{:} ...
);