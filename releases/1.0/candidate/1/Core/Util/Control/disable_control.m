function disable_control(pal, control)

% disable_control - disable control
% ---------------------------------
%
% disable_control(pal, name)
%
% disable_control(control)
%
% Input:
% ------
%  pal - palette
%  name - of control
%  control - control

if nargin < 2
	control = pal; pal = []; 
end

if ~ischar(control) && numel(control) > 1
	iteraten(mfilename, 2, pal, control); return; 
end

set_control(pal, control, 'enable', 0);