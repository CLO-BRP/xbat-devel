function control = update_control_values(control, value, flat) 

% update_control_values - update values of controls in a control array
% --------------------------------------------------------------------
%
% control = update_control_values(control, value, flat) 
%
% Input:
% ------
%  control - control array
%  value - value struct
%  flat - flatten flag (def: 1)
%
% Output:
% -------
%  control - updated control array

%--
% set flatten default
%--

if nargin < 3
	flat = 1;
end

if flat
	value = flatten(value);
end

%--
% get field names and control names
%--

field = fieldnames(value); name = {control.name};

%--
% loop over controls or fields to update control values
%--

if numel(name) < numel(field) 
	
	for k = 1:length(name)
	
		if ~string_is_member(name{k}, field)
			continue;
		end
		
		control(k) = set_control_value(control(k), value.(name{k}));
		
	end
	
else
	
	for k = 1:length(field) 
		
		ix = find(strcmp(field{k}, name), 1);
		
		if isempty(ix)
			continue;
		end
		
		control(ix) = set_control_value(control(ix), value.(field{k}));
		
	end
	
end


%-----------------------------
% SET_CONTROL_VALUE
%-----------------------------

function control = set_control_value(control, value)

% NOTE: this is a fairly lazy function, it should not be relied on, 'control_create' will make sure things are consistent

switch control.style

	case 'edit'
		
		if ischar(value)
			control.string = value;
		else
			error('Update value for ''edit'' control is not a string.');
		end
		
	case {'listbox', 'popup'}
		
		% TODO: make this a bit smarter and cleaner
		
		% NOTE: when the proposed value is a string we find the string index
		
		if ischar(value)
			control.value = find(strcmp(value, control.string));
		end
		
		% NOTE: when the proposed value is a string cell array we find all string indices
		
		if iscellstr(value)
			
			control.value = [];
			
			for k = 1:length(value)
				control.value(end + 1) = find(strcmp(value{k}, control.string));
			end
			
		end
		
	otherwise

		% NOTE: when the value is a scalar we at least map it to the control range
		
		if numel(value) == 1 && isnumeric(value)
			value  = clip_to_range(value, [control.min, control.max]);
		end
		
		control.value = value;
		
end