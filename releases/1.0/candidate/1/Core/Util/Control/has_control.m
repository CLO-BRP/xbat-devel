function [value, ix] = has_control(pal, name)

% has_control - check for named control in palette or control array
% -----------------------------------------------------------------
%
% [value, ix] = has_control(in, name)
%
% Input:
% ------
%  in - control palette or cotnrol array
%  name - control name
%
% Output:
% -------
%  value - result of test
%  ix - index of control in array

%--
% control palette input
%--

if ishandle(pal)
	
	ix = []; value = ~isempty(get_control(pal, name, 'handles'));

	if value 
		data = get(pal, 'userdata'); control = data.control; ix = find(strcmp(name, {control.name}));
	end

%--
% control array input
%--

else
	
	control = pal; ix = find(strcmp(name, {control.name}));  value = ~isempty(ix);

end