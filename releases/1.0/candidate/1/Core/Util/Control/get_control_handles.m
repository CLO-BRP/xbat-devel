function handles = get_control_handles(pal, name)

handles = get_control(pal, name, 'handles');
