function pal = test_toggle_group(type, known)

%--
% handle input
%--

if nargin < 2
	known = {'red', 'green', 'blue', 'black', 'violet'; 'whale', 'cat', 'dog', 'bird', 'moose'};
end

if ~nargin
	type = 'palette';
end

%--
% create edit and toggle-group controls
%--

% NOTE: this test is a simple tags implementation

control = empty(control_create);

control(end + 1) = header_control('Test');

control(end + 1) = edit_control('tags');

control(end + 1) = control_create( ...
	'name', 'known_tags', ...
	'style', 'togglegroup', ...
	'lines', 2 * 1.75, ...
	'callback', @callback, ...
	'string', known ...
);

%--
% create palette or dialog
%--

switch type
	
	case 'palette'
		opt = control_group; opt.width = 24;
		
		pal = control_group([], [], 'Text Tags 1', control, opt);
		
	case 'dialog'
		
end


%-------------------------
% CALLBACK
%-------------------------

function callback(obj, evendata)

% TODO: what do we do when the parsing fails? use the 'tags_to_str' and 'str_to_tags' helpers

callback = get_callback_context(obj, eventdata); pal = callback.pal;

switch callback.control.name
	
	case 'tags'
		
		%--
		% get list of tags
		%--
		
		current = str_split(callback.value, ',')
		
		%--
		% update buttons if needed
		%--
		
	case 'known_tags'
		
		%--
		% get list of tags after toggle
		%--
		
		current = get_control_value(pal.handle, 'tags'); current = str_split(current, ','); 
		
		value = get_control_value(pal.handle, 'known_tags');
		
		current = setdiff(current, value.string);

		current = union(current, value.selected);
		
		%--
		% update tags control
		%--
		
		value = str_implode(current, ', ');
		
		set_control_value(pal.handle, 'tags', value);
		
end