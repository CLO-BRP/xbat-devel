function pal = test_text_control

control = empty(control_create);

control(end + 1) = header_control('Test');

control(end + 1) = control_create( ...
	'name', 'text', ...
	'style', 'text', ...
	'string', {'first line', 'second line', 'third and last line'} ...
);

opt = control_group; opt.width = 16;

pal = control_group([], [], 'Text Test 1', control, opt);