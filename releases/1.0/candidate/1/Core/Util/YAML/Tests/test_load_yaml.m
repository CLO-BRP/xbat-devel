function test_load_yaml

out_struct = load_yaml(which('database.example.yml'))
out_cell = load_yaml(which('cell.yml'));
% This should be a Struct

assert( isstruct(out_struct) );
assert( iscell(out_cell) );