function out = load_yaml(file)

% load_yaml - loads a YAML file and returns a cell array, struct, or value
% depending upon the struture of the YAML doc. For most applications, like
% Ruby on Rails database.yml files, the returned object will be a struct or structs.
% ---------------------------------------------------------------
% 
% out = load_yaml(file)

% Input:
% -------
% file - absolute path to a YAML file to be parsed
%
% Output:
% -------
%  out - returns a cell array, struct, or value
% depending upon the struture of the YAML doc

% Perform installations of the JYAML lib as necessary

install_jyaml;

% Load the file

file = javaObject('java.io.File', file);

% This call returns a Java object - presumably either a classic datatype
% such as an Integer or String, or a collection (ArrayList or HashMap) - 
% there could be others dependin on the input, i.e. you could feed it a Java object type.

yaml = org.ho.yaml.Yaml.load( file );

out = process_jyaml_node(yaml);


%% process_jyaml_node - private helper function called recursively
%-----------------------
 
function out = process_jyaml_node(node)

% Hash - there may be a better way to process a Java HashMap, but his works

if isa(node, 'java.util.HashMap')
    
    child = struct;
    
    iterator = node.keySet().iterator(); 
    while iterator.hasNext()
        key   = iterator.next();
        value = node.get(key);
        child.(key) = process_jyaml_node(value);
    end
    
    out = child; return;
end

% Array - process array nodes

if isa(node, 'java.util.ArrayList')

    child = cell(node.size(),1);

    for j=0:node.size()-1
        value = node.get(j);
        child{j+1} = process_jyaml_node( value );
    end
    out = child; return;
end

% The only thing that could be left here is a value (or something else unaccounted for)

out = node;
