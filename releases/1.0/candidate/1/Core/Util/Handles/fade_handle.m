function fade_handle(handle, base, alpha, varargin)

% fade_handle - properties
% ------------------------
%
% fade_handle(handle, base, alpha, 'field', value, ... )
%
% Input:
% ------
%  handle - list
%  base - color to blend with
%  alpha - for blending

%--
% handle input
%--

if nargin < 3
	alpha = 0.4;
end

if nargin < 2
	base = 0.8 * ones(1, 3);
end

if numel(handle) > 1
	
	for k = 1:numel(handle)
		fade_handle(handle(k), base, alpha, varargin);
	end
	
	return;
end

%--
% update handle
%--

% TODO: extend this to other properties, consider various types, look at 'rotate_handle'

% NOTE: we also have something along this lines elsewhere?

fields = get_color_fields(handle);

for k = 1:numel(fields)
	field = fields{k};
	
	color = get(handle, field) ;
	
	if ischar(color)
		continue;
	end
	
	blend = alpha * color + (1 - alpha) * base; 
	
	set(handle, field, blend);
end
	
	
	