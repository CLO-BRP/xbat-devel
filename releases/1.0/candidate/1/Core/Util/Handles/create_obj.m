function [handle, created]  = create_obj(type, par, tag, varargin)

% create_obj - create tagged object of specific type within parent
% ----------------------------------------------------------------
%
% [handle, created] = create_obj(type, par, tag, field, value, ... )
%
% Input:
% ------
%  type - type
%  par - parent
%  tag - tag
%  field - field name
%  value - field value
%
% Output:
% -------
%  handle - object handle
%  created - creation indicator

% TODO: consider exteding the 'create_obj' function to use multiple properties to determine uniqueness

% NOTE: for example, if 'tag' input is actually a cell array of field-value pairs

% TODO: consider some form of caching, to skip 'findobj'

% TODO: extend this function to handle the creation of 'text' as an object

%---------------------
% HANDLE INPUT
%---------------------

%--
% check type
%--

% NOTE: is is not clear what we are gaining in this check

% types = {'figure', 'axes', 'line', 'patch', 'rectangle', 'uicontrol'}; 
% 
% if ~string_is_member(type, types), error('Unrecognized object type.'); end

%---------------------
% GET OBJECT HANDLE
%---------------------

%--
% get tagged object of type from parent, create if needed
%--

handle = findobj(par, 'type', type, 'tag', tag);

% handle = find_handles_with_tag(par, tag, 'type', type);

if ~isempty(handle)
	created = 0;
	
	if numel(handle) > 1
		db_disp; disp(['''', tag, ''' tag returns multiple handles!']);
	end
else
	handle = feval(type, 'parent', par, 'tag', tag); created = 1;
end

%--
% set object properties
%--

% NOTE: we at least check that fields and values are paired

if ~isempty(varargin) && ~mod(length(varargin), 2)
	
	try
		set(handle, varargin{:});
	catch %#ok<CTCH>
		nice_catch(lasterror, ['Failed to set ', type, ' (', tag, ') properties.']); %#ok<LERR>
	end

end
