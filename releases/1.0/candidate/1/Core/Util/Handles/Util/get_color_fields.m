function fields = get_color_fields(handle)

%--
% check persistent cache
%--

persistent COLOR_FIELDS;

if isempty(COLOR_FIELDS)
	COLOR_FIELDS = struct;
end

type = get(handle, 'type');

if isfield(COLOR_FIELDS, type)
	fields = COLOR_FIELDS.(type); return;
end

%--
% determine fields and update cache
%--

fields = fieldnames(get(handle(1)));

fields = fields(string_contains(fields, 'color', false));

COLOR_FIELDS.(type) = fields;