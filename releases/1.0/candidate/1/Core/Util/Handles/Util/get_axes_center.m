function center = get_axes_center(ax)

% get_axes_center - as complex number
% -----------------------------------
%
% center = get_axes_center(ax)
%
% Input:
% ------
%  ax - handle
%
% Output:
% -------
%  center - as complex number

prop = get(ax);

center = mean(prop.XLim) + 1i * mean(prop.YLim);