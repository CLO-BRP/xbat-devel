function level = dir_level(dir)

% dir_level - level of directory from root
% ----------------------------------------
%
% level = dir_level(dir)
%
% Input:
% ------
%  dir - path
%
% Output:
% -------
%  level - of directory

%--
% handle input
%--

if ~nargin
	dir = pwd;
end

if iscellstr(dir) && numel(dir) > 1
	level = iterate(mfilename, dir); return;
end

%--
% compute level of directory
%--

% NOTE: we first remove duplicate 'filesep' sequences

dir = strrep(dir, [filesep, filesep], '');

level = sum(dir == filesep);