function result = jar_in_classpath(jar, type)

% jar_in_classpath - check whether jar is in classpath
% ----------------------------------------------------
%
% value = jar_in_classpath(jar, type)
%
% Input:
% ------
%  jar - file
%  type - of classpath ('-static', '-dynamic', or '-all' (default))
%
% Output:
% -------
%  result - of test

%--
% set full classpath search
%--

if nargin < 2
	type = '-all';
end

%--
% select file part of jar input
%--

if ischar(jar)
	jar = {jar};
end

for k = 1:numel(jar)
	
	start = find(jar{k} == filesep, 1, 'last');

	if ~isempty(start)
		jar{k} = jar{k}(start + 1:end);
	end

end

%--
% check whether jar is in classpath
%--

result = string_is_member(jar, get_classpath_jars(type));
