function icon = sys_tray

%--
% check for tray support
%--

[has, tray] = has_tray;

if ~has
	icon = []; return;
end

%--
% create icon and add to tray
%--

file = which('matlabicon.gif');

image = java.awt.Toolkit.getDefaultToolkit.createImage(file)

icon = java.awt.TrayIcon(image, 'initial tooltip')

icon.setToolTip('click this icon for applicative context menu')

tray.add(icon);