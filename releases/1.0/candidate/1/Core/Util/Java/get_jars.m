function jars = get_jars(root)

% get_jars - scac directories looking for jars
% --------------------------------------------
%
% jars = get_jars(root)
%
% Input:
% ------
%  root - of scan
%
% Output:
% -------
%  jars - found

if ~nargin
	root = pwd;
end 

dirs = scan_dir(root); jars = {};

for k = 1:numel(dirs)
	
	content = what_ext(dirs{k}, 'jar');
	
	if ~isempty(content.jar)
		content.jar = strcat(content.path, filesep, content.jar); jars = {jars{:}, content.jar{:}};
	end
	
end

if ~nargout
	disp(' '); iterate(@disp, jars); disp(' '); clear jar;
end