function out = buffer_index(buf, ix)

% buffer_index - get and set buffer index
% ---------------------------------------
%
%  ix = buffer_index(buf)
%
% buf = buffer_index(buf, ix)
%
% Input:
% ------
%  buf - buffer
%
% Output:
% -------
%  ix - buffer index
%  buf - updated buffer

% get buffer index

if nargin < 2
	out = buf.index; return;
end

% set buffer index

if (ix < 1) || (ix > buf.length)
	error('Index is out of range.');
end

buf.index = ix; out = buf;
