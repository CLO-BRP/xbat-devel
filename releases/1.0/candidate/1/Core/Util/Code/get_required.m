function [required, called] = get_required(source, pack)

% get_required - directories, with called functions
% -------------------------------------------------
%
% [required, called] = get_required(source, pack)
%
% Input:
% ------
%  source - file or directory
%  pack - as struct
%
% Output:
% -------
%  required - directories
%  called - functions from directory
% 
% See also: get_called, getcallinfo, deprpt

%--
% handle input
%--

if nargin < 2
	pack = false;
end

if ~nargin 
	source = pwd;
end

%--
% get called and parse to get required
%--

called = get_called(source); file = {called.file};

required = cell(size(file));

for k = 1:numel(called)
	required{k} = fileparts(file{k});
end 

[required, ignore, index] = unique(required);

if nargout > 1 || ~nargout
	content = cell(size(required));
	
	for k = 1:numel(required)
		content{k} = file(index == k);
	end
	
	called = content;
end 

%--
% display output if not captured
%--

% TODO: consider linking the files to the edit

if ~nargout
	disp(' ');
	
	for k = 1:numel(required)
		disp(required{k});
		
		for j = 1:numel(called{k})
			disp(['    ', strrep(called{k}{j}, [required{k}, filesep], '')]);
		end
		
		disp(' ');
	end
	
	clear required;
end

