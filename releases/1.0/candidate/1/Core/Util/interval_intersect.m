function interval = interval_intersect(varargin)

% interval_intersect - intersect a collection of intervals
% --------------------------------------------------------
%
% interval = interval_intersect(interval)
%
% Input:
% ------
%  interval - interval struct
% 
% Output:
% -------
%  interval - modified interval

%--
% allow for variable input
%--

switch numel(varargin)
	
	case 1, interval = varargin{1};
		
	% NOTE: we assume each input is an interval description row
	
	otherwise, interval = reshape([varargin{:}], 2, [])';
		
end

%--
% allow for interval matrix input
%--

% NOTE: the interval matrix is assumed to contain an interval per row with start and stop

% TODO: a function to ensure proper interval matrix

matrix = ~isstruct(interval);

if matrix
	data = interval; clear interval; interval.start = data(:, 1); interval.stop = data(:, 2);
end

%--
% sort interval edges
%--

[edges, ix] = sort([interval.start; interval.stop]);

labels = [ones(size(interval.start)); -ones(size(interval.stop))];

labels = labels(ix);

%--
% get new block starts from indicator sum
%--

ix = find(cumsum(labels) == 2);

%--
% get intersection scan ingredients
%--

if isempty(ix)
	interval.start = []; interval.stop = [];
else
	interval.start = edges(ix, 1); interval.stop = edges(ix + 1, 1);
end

%--
% output matrix
%--

if matrix
	interval = [(interval.start)', (interval.stop)'];
end