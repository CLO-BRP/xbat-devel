function [status, result] = ssh(host, varargin)

% ssh - start ssh session to host
% -------------------------------
%
% [status, result] = ssh(host, varargin)
%
% Input:
% ------
%  host - host
%
% NOTE: this is a shortcut to putty, arguments are imploded as strings

[status, result] = putty(['-ssh ', host], varargin{:}, ' &');


