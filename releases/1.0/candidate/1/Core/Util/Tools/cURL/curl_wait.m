function value = curl_wait(file)

% curl_wait - indicate that curl is currently waiting on a file
% -------------------------------------------------------------
%
% value = curl_wait(file)
%
% Input:
% ------
%  file - file
%
% Output:
% -------
%  value - wait indicator

value = 0;

%--
% check for daemon
%--

daemon = curl_daemon;

if isempty(daemon)
	return;
end

%--
% check if daemon is considering file
%--

timerfcn = get(daemon, 'timerfcn');

if ~iscell(timerfcn) || (length(timerfcn) < 2)
	return;
end

listens = timerfcn{2};

value = ismember(file, {listens.file.name});

