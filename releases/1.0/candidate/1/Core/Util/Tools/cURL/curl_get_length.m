function total = curl_get_length(url, tool)

% curl_get_length - get context length for url
% --------------------------------------------
%
% total = curl_get_length(url, tool)
%
% Input:
% ------
%  url - to get length for
%  tool - curl tool
%
% Output:
% -------
%  total - url length in bytes

% NOTE: this indicates an indefinite wait

total = inf;

%--
% get curl tool
%--

if nargin < 2
	
	tool = get_curl;

	if isempty(tool)
		error('curl is not available.');
	end

end

%--
% get header
%--

[status, result] = system(['"', tool.file, '" -I ', url]);

if status
    return;
end

%--
% parse header to get total number of bytes
%--

parse = regexpi(result, 'content-length: (?<size>\d+)', 'names');

if isempty(parse)
	return;
end

total = str2double(parse.size);

