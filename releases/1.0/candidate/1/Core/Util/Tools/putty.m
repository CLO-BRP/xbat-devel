function [status, result] = putty(varargin)

% putty - use free implementation of Telnet and SSH
% -------------------------------------------------
%
% [status, result] = putty(varargin)
% 
% NOTE: arguments are imploded as strings

name = 'PuTTY'; file = 'putty.exe'; url = 'http://the.earth.li/~sgtatham/putty/latest/x86/putty.zip';

[status, result] = generic_tool(name, file, url, 'urlwrite', varargin{:});
