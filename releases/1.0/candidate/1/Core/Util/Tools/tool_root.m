function root = tool_root(name)

if ~nargin
	error('Tool name input is required.');
end

root = create_dir(fullfile(tools_root, name));
