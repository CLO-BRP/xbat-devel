function [result, status] = markdown(in, out)

% markdown - process markdown file
% --------------------------------
% 
% [result, status] = markdown(in, out)
%
% Input:
% ------
%  in - input file
%  out - output file
%
% Output:
% -------
%  result, status - of script

if nargin < 2
	out = file_ext(in, 'html');
end

[result, status] = perl('Markdown.pl', in, out);