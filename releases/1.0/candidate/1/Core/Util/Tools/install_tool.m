function success = install_tool(name, url, helper)

% install_tool - download tool and add to path
% --------------------------------------------
%
% success = install_tool(name, url, helper)
%
% Input:
% ------
%  name - tool name
%  url - download url
%  helper - download helper name 'curl' or 'urlwrite'
%
% Output:
% -------
%  success - indicator
% 
% See also: uninstall_tool

%--
% set default helper
%--

if nargin < 3
	helper = 'curl';
end

%--
% set list of application we should get natively
%--

persistent BOOT;

if isempty(BOOT)
	BOOT = {'NirCmd', 'cURL'};
end

%--
% install tool
%--

try
	
	%--
	% get destination directory
	%--

	root = create_dir(fullfile(tools_root, name));

	if isempty(root)
		error(['Failed to create root directory for ''', name, ''' tool.']);
	end

	%--
	% download tool
	%--
	
	% TODO: inspect 'url' in case 'unzip' is not the required action!
	
	disp(['Downloading and installing ''', name, ''', this may take some time ...']);
	
	start = clock;
	
	file = fullfile(root, 'temp.zip');
	
	if string_is_member(name, BOOT) || strcmpi(helper, 'urlwrite')
		
		urlwrite(url, file);
		
	else

		% NOTE: this provides download progress indicators

		curl_get(url, file, ['Downloading ''', name, '''']);

		while curl_wait(file)
			pause(0.25);
		end
		
	end
	
	unzip(file, root); delete(file);
	
	%--
	% add to path
	%--
	
	append_path(root);
	 
	elapsed = etime(clock, start);
	
	disp(['Done. (', num2str(elapsed), ' sec)']);
	disp(' ');
	
	success = 1;
	
catch
	
	success = 0; nice_catch(lasterror, ['Failed to get ''', name, '''.']);
	
end


