function [result, status] = texify(in, out, varargin)

% texify - prepares code for tex
% ------------------------------

%--
% setup and handle input
%--

persistent helper;

if isempty(helper)
	helper = which('texify.pl');

	if isempty(helper)
		error('Unable to find ''texify.pl'' helper script.');
	end
end

if nargin < 2 || isempty(out)
	out = [in, '.tex'];
end

%--
% call helper perl script
%--

% NOTE: when we use which we assume the file is a full path or in MATLAB path

[result, status] = perl(helper, 'matlab', '-i', which(in), '-o', out, varargin{:});