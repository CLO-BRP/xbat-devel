function varargout = use_tool(tool, varargin)

% use_tool - in a generic way
% ---------------------------
%
% [status, result] = use_tool(tool, varargin)
%
% Input:
% ------
%  tool - struct, as returned by 'get_tool'
%  varargin - arguments for tool
%
% Output:
% -------
%  status - of call
%  result - of call
%
% NOTE: this is used to call command-line tools, it results in a system call
%
% See also: get_tool

%--
% build tool command string
%--

str = ['"', tool.file, '" ', str_implode(varargin)];

% disp(' '); disp(str); disp(' ');

%--
% perform system call to use tool
%--

switch nargout
	case 0
		system(str);

	case 1
		varargout{1} = system(str);
		
	case 2
		[varargout{1}, varargout{2}] = system(str);
end