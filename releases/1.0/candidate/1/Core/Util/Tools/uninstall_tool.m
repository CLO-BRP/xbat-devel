function success = uninstall_tool(name)

% uninstall_tool - from Tools
% ---------------------------
%
% success = uninstall_tool(name)
% 
% Input:
% ------
%  name - of tool
% 
% Output:
% -------
%  success - indicator
%
% See also: install_tool

%--
% check for tool root directory
%--

root = tool_root(name);

if ~exist(root, 'dir')
	return;
end

%--
% try to remove from path, remove from system
%--

try
	remove_path(root); rmdir(root, 's'); success = true;
catch
	success = false;
end