function url = latest_icecast_setup

% latest_icecast_setup - get url of latest icecast setup
% ------------------------------------------------------
%
% url = latest_icecast_setup
%
% Output:
% -------
%  url - latest icecast setup location

%--
% try to get release listing, otherwise return recent hard-coded result 
%--

root = 'http://downloads.xiph.org/releases/icecast/';

try
	listing = urlread(root);
catch
	url = [root, 'icecast2_win32_v2.3.1_setup.exe']; return; 
end

%--
% parse listing as lines and select some for further consideration
%--

url = '';

lines = file_readlines(listing);

% TODO: clearly we need a filtered read function

for k = numel(lines):-1:1
	
	consider = string_contains(lines{k}, '.exe');

	if ~consider
		lines(k) = [];
	end

end

%--
% consider lines to get latest setup url
%--

% TODO: parse the lines to get the latest setup, consider getting properties such as 'beta' or 'release-candidate'

if ~nargout
	
	disp(' '); iterate(@disp, lines); disp(' ');

end