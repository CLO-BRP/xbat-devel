function tool = shortcut

% shortcut - command-line tool to create windows shortcuts
% --------------------------------------------------------
%
% tool = shortcut
%
% Output:
% -------
%  tool - shortcut tool

%--
% set executable and human name and source url
%--

name = 'Shortcut'; exe = 'Shortcut.exe'; url = 'http://www.optimumx.com/download/Shortcut.zip';

% TODO: factor what follows, the previous line along with windows-only indication are input

% TODO: replace factored function calls in relevant places

% NOTE: this is a windows only tool, other systems have reasonable ways of doing this

if ~ispc
    tool = []; return;
end

%--
% get tool
%---

tool = get_tool(exe);

% NOTE: we try to install tool to get tool if needed

if isempty(tool) && install_tool(name, url);
	tool = get_tool(exe);
end