function value = install_hstart

% NOTE: this may allow us to hide the prompt created when using 'curl'

url = 'http://www.ntwind.com/download/hstart.zip';

value = install_tool('hstart', url, 'urlwrite');
