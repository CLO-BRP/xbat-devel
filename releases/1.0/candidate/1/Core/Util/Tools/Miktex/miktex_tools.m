function tools = miktex_tools(pat, types)

% miktex_tools - get miktex tools
% -------------------------------
%
% tools = miktex_tools(pat, types)
%
% Input:
% ------
%  pat - name pattern
%  types - tool types
%
% Output:
% -------
%  tools - tools

%--------------
% SETUP
%--------------

%--
% get miktex tool source directory
%--

source = miktex_root;

if isempty(source)
	tools = []; return;
end

hint = miktex_root('doc');

%--------------
% HANDLE INPUT
%--------------

%--
% set types
%--

if nargin < 2
	types = get_tool_types;
end

%--
% set name pattern
%--

if nargin < 1
	pat = '';
end

% NOTE: consider specific tool name input, this will ignore types input

if ~isempty(pat) && ~any(pat == '*')
	
	[p1, p2, p3] = fileparts(pat);
	
	if ~isempty(p3)
		pat = p2; types = p3;
	end

end

%--------------
% GET TOOLS
%--------------

tools = get_tools(source, pat, types, hint);
