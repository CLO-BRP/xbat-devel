function out = xml_to_struct(in)

% NOTE: the following code should parse flat line-based xml, typical from many sources

% TODO: get closing tag position while parsing nodes!

%--
% get lines from string input
%--

if ischar(in)
	lines = file_readlines(in, @strtrim);
else
	lines = in;
end

% NOTE: remove format line

if strncmp('<?xml', lines{1}, 5)
	lines(1) = [];
end

% NOTE: remove comments

% TODO: improve removal of comments, consider those spanning multiple lines

for k = numel(lines):-1:1
	
	if strncmp('<!--', strtrim(lines{k}), 4)
		lines(k) = [];
	end
	
end

%--
% parse lines into nodes
%--

[node.name, node.attr, node.content, node.type] = parse_line(lines{1});

% NOTE: this should allocate the struct array

node(numel(lines)).name = '';

for k = 2:numel(lines)	
	[current.name, current.attr, current.content, current.type] = parse_line(lines{k}); node(k) = current;
end

%--
% parse node structure and build struct
%--

out = build_struct(struct, node(1), node(2:end));


%-----------------------
% BUILD_STRUCT
%-----------------------

function in = build_struct(in, par, node)

%--
% return if there is nothing to build with
%--

if isempty(node)
	return;
end

%--
% check that we are starting
%--

if strcmp(par.type, 'open')
	
	%--
	% append parent field or array
	%--
	
	if ~isfield(in, par.name)

		in.(par.name) = struct;

		if ~trivial(par.attr)
			in.(par.name).ATTR = par.attr;
		end

	else

		in.(par.name)(end + 1) = in.(par.name)(end);
	
	end
	
	%--
	% append field nodes
	%--
	
	k = 1; current = node(k);
	
	while ~strcmp(current.type, 'open') && k < numel(node)
		
		if strcmp(current.type, 'full')
			
			in.(par.name)(end).(current.name) = current.content;
			
			% NOTE: this puts the attributes as the last field, which is not the most intuitive for display
			
			if ~trivial(current.attr)
				in.(par.name)(end).(current.name).ATTR = current.attr;
			end
			
		end
		
		k = k + 1; current = node(k);
		
	end
	
	%--
	% partial recursive
	%--
	
	last = find_close(par, node);
	
	in.(par.name) = build_struct(in.(par.name), node(k), node(k + 1:last));
	
end

%--
% full recursion
%--

if numel(node) >= last + 2
	in = build_struct(in, node(last + 1), node(last + 2:end));
end


%-----------------------
% FIND_CLOSE
%-----------------------

% TODO: this is one of the most expensive steps in the code, improve this

function k = find_close(par, node)

% NOTE: the find first element arguments is critical

k = find(strcmp({node.name}, par.name) & strcmp({node.type}, 'close'), 1);


%-----------------------
% PARSE_LINE
%-----------------------

function [name, attr, content, type] = parse_line(line)

% NOTE: we make sure lines are trimmed of whitespace

line = strtrim(line);

%--
% get line type and content if available
%--

[type, content, ix1] = get_line_type(line);

if ~isempty(content) && all(isstrprop(content, 'digit')) %#ok<TRYNC>
	content = eval(content);
end

%--
% get name and attributes based on type
%--

attr = struct;

switch type
	
	case 'close'
		
		name = content; content = ''; 
		
	case {'full', 'open'}
		
		% NOTE: attributes are space separated and the first part of the opening tag is the name

		part = str_split(line(2:(ix1 - 1)), ' '); name = part{1};

		for k = numel(part):-1:1
			
			% NOTE: we join segments that contain a single double-quote character, this protects quoted strings
			
			if sum(part{k} == '"') == 1
				part{k - 1} = [part{k - 1}, ' ', part{k}]; part(k) = [];
			end
			
		end
		
		% NOTE: attributes are name value pairs separated by an equal sign, with the value in quotes

		for k = 2:numel(part)
			pair = str_split(part{k}, '='); attr.(genvarname(pair{1})) = pair{2}(2:end - 1);
		end
		
end


%-----------------------
% GET_LINE_TYPE
%-----------------------

function [type, content, ix1] = get_line_type(line)

% NOTE: note a closing line starts with '</', in this case the content is the name

if strcmp(line(1:2), '</')
	type = 'close'; ix1 = []; content = line(3:end - 1); return;
end

% NOTE: a full line contains a '</' not at the start

ix2 = strfind(line, '</'); 

if ~isempty(ix2)
	type = 'full'; ix1 = find(line == '>', 1); content = line(ix1 + 1:ix2 - 1); return;
end

% NOTE: otherwise we are dealing with an opening line

type = 'open'; ix1 = find(line == '>', 1); content = line(ix1 + 1:end - 1);

