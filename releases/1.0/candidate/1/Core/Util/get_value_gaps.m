function [start, duration, value] = get_value_gaps(value, k)

% get_value_gaps - compute top gaps in a collection of values
% -----------------------------------------------------------
%
% [start, duration, value] = get_value_gaps(value, k)
%
% Input:
% ------
%  value - values
%  k - number of desired gaps (def: 1)
%
% Output:
% -------
%  start, duration - of gap
%  value - sorted values

% TODO: develop function that uses this and sampling from a collection to estimate gaps 

%--
% set number of output gaps
%--

% TODO: develop automatic criterion for selecting the number of gaps considering the gap magnitude ratios

if nargin < 2
	k = 1;
end

%--
% compute gaps
%--

% NOTE: sort values, compute differences, and sort jumps keeping track of position

value = sort(value(:)); jump = diff(value); [jump, position] = sort(jump, 'descend');

%--
% pack gap start and duration
%--

start = value(position(1:k)); duration = jump(1:k);





