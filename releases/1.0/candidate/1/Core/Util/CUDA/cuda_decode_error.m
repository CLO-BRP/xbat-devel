function s = cuda_decode_error(e)

% cuda_decode_error - Convert numeric CUDA return code into error string
%
% --------------------------------------------------------------------
%
% Input:
% ------
%  e - Numeric CUDA return code
%
% Output:
% -------
%  s - Error description string
%
% Returns codes are defined in CUDA api file 'driver_types.h'
%

switch e
    case 0
        s = 'CUDA success';
    case 1
        s = 'Missing configuration error';
    case 2
        s = 'Memory allocation error';
    case 3
        s = 'Initialization error';
    case 4
        s = 'Launch failure';
    case 5
        s = 'Prior launch failure';
    case 6
        s = 'Launch timeout error';
    case 7
        s = 'Launch out of resources error';
    case 8
        s = 'Invalid device function';
    case 9
        s = 'Invalid configuration';
    case 10
        s = 'Invalid device';
    case 11
        s = 'Invalid value';
    case 12
        s = 'Invalid pitch value';
    case 13
        s = 'Invalid symbol';
    case 14
        s = 'Map buffer object failed';
    case 15
        s = 'Unmap buffer object failed';
    case 16
        s = 'Invalid host pointer';
    case 17
        s = 'Invalid device pointer';
    case 18
        s = 'Invalid texture';
    case 19
        s = 'Invalid texture binding';
    case 20
        s = 'Invalid channel descriptor';
    case 21
        s = 'Invalid memcpy direction';
    case 22
        s = 'Address of constant error';
    case 23
        s = 'Texture fetch failed';
    case 24
        s = 'Texture not bound error';
    case 25
        s = 'Synchronization error';
    case 26
        s = 'Invalid filter setting';
    case 27
        s = 'Invalid norm setting';
    case 28
        s = 'Mixed device execution';
    case 29
        s = 'CUDA runtime unloading';
    case 30
        s = 'Unknown error condition';
    case 31
        s = 'Function not yet implemented';
    case 32
        s = 'Memory value too large';
    case 33
        s = 'Invalid resource handle';
    case 34
        s = 'Not ready error';
    case 35
        s = 'CUDA runtime is newer than driver';
    case 36
        s = 'Set on active process error';
    case 38
        s = 'No available CUDA device';
    case 39
        s = 'Uncorrectable ECC error detected';
    case 127 % 0x7f
        s = 'Startup failure';
    case 10000
        s = 'API failure base';
    otherwise
        s = 'Unknown CUDA error';
end
