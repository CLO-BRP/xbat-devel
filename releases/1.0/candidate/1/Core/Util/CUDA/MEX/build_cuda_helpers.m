function build_cuda_helpers(arch)

% build_cuda_helpers - basic functions to inspect the device hardware
%
% build_cuda_helpers(arch)
%
% Input:
% ------
%  arch - level
%
% NOTE: the build cannot ask the hardware the architecture ('arch') level before these are built

%--
% set build options
%--

opt = build_cuda_mex; opt.arch = arch;

%--
% try to build all helpers
%--

fun = {'get_attributes', 'get_capability', 'get_count', 'get_name', 'get_totalmem', 'get_freemem'};

for k = 1:numel(fun)
	
	try
		build_cuda_mex(fun{k}, opt);
	catch
		nice_catch(lasterror, ['Failed to build ''', fun{k}, '''.']);
	end 
end