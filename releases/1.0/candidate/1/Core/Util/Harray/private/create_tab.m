function handles = create_tab(par, bar, str)

% create_tab - create tab to attach to bar
% ----------------------------------------
%
% create_tab(par, bar, str)
%
% Input:
% ------
%  par - parent
%  bar - bar
%  str - tab string
%
% Output:
% -------
%  handles - created object handles

%---------------------
% HANDLE INPUT
%---------------------

%--
% set default no string, use marker
%--

if nargin < 3
	str = '';
end

%--
% check bar
%--

bars = {'tool', 'status'};

if ~ismember(bar, bars)
	error('Unrecognized bar.');
end 

%--
% check and get data from parent
%--

data = harray_data(par);

if isempty(data)
	error('Parent does not contain hierarchical arrays.');
end

%--
% get bar handle
%--

switch bar
	
	case 'status', ax = findobj(par, 'tag', 'HARRAY_STATUSBAR');
		
	case 'tool', ax = findobj(par, 'tag', 'HARRAY_TOOLBAR');
		
end

if isempty(ax)
	return;
end

%---------------------
% CREATE TAB
%---------------------

%--
% get bar colors
%--

color.control = get(ax, 'color'); color.line = get(ax, 'xcolor');

%--
% compute tab element positions
%--

[pos, marker] = get_tab_positions(ax, bar);

%--
% tab elements
%--

handles = rectangle( ...
	'tag', 'TOGGLE_TAB', ...
	'position', pos.tab, ...
	'curvature', [0 0], ...
	'parent', ax, ...
	'edgecolor', color.line, ...
	'facecolor', color.control, ...
	'clipping', 'off' ...
);

set_rectangle_curvature(handles, 10); 

handles(end + 1) = rectangle( ...
	'tag', 'TOGGLE_PATCH', ...
	'position', pos.patch, ...
	'parent', ax, ...
	'edgecolor', 'none', ...
	'facecolor', color.control, ...
	'clipping', 'off' ...
);

%--
% tab symbol
%--

if isempty(str)

	handles(end + 1) = line( ...
		'tag', 'TOGGLE_MARKER', ...
		'parent', ax, ...
		'xdata', pos.marker(1), ...
		'ydata', pos.marker(2), ...
		'clipping', 'off', ...
		'hittest', 'off', ...
		'linestyle', 'none', ...
		'marker', marker, ...
		'markerfacecolor', 'none', ...
		'markeredgecolor', color.line ...
	);

%--
% tab string
%--

else
	
	handles(end + 1) = text(pos.marker(1), pos.marker(2), str, ...
		'parent', ax, ...
		'color', color.line, ...
		'hittest', 'off', ...
		'horizontalalignment', 'center', ...
		'verticalalignment', 'middle' ...
	);

end

%---------------------
% TAB CALLBACK
%---------------------

set(handles(1), 'buttondownfcn', {@tab_callback, par, bar, handles(end)});
