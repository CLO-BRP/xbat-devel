function [pos, marker] = set_tab_positions(ax, bar, wide, data)

%--
% handle input
%--

par = ancestor(ax, 'figure');

if nargin < 4
	data = harray_data(par);
end

if nargin < 3
	wide = 1.75;
end

%--
% get tab positions
%--

[pos, marker] = get_tab_positions(ax, bar, wide, data);

%--
% update tab elements
%--

tab = findobj(ax, 'tag', 'TOGGLE_TAB');

set(tab, ...
	'position', pos.tab ...
);

patch = findobj(ax, 'tag', 'TOGGLE_PATCH');

set(patch, ...
	'position', pos.patch ...
);

marker = findobj(ax, 'tag', 'TOGGLE_MARKER');

set(marker, ...
	'xdata', pos.marker(1), 'ydata', pos.marker(2) ...
);
