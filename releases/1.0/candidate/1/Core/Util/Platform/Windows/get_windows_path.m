function elements = get_windows_path(match, first, refresh)

% get_windows_path - elements matching a string
% ---------------------------------------------
%
% elements = get_windows_path(match, first, refresh)
%
% Input:
% ------
%  match - to look for in path element (def: '', all elements returned)
%  first - match return (def: true)
%  refresh - path from system (def: false)
%
% Output: 
% -------
%  elements - of windows path matching

if ~ispc
	error('This function is only available for Windows.');
end

%--
% handle input
%--

if nargin < 3
	refresh = false; 
end

if nargin < 2
	first = true;
end

%--
% get full path string
%--

% NOTE: we get path string and parse elements into cells

persistent ELEMENTS;

if isempty(ELEMENTS) || refresh
	[status, result] = system('path');
	
	[ignore, result] = strtok(result, '=');
	
	ELEMENTS = strread(result(2:end), '%s', -1, 'delimiter', ';');
end

if ~nargin || isempty(match)
	elements = ELEMENTS; return;
end

%--
% filter if needed
%--

ix = [];

for k = 1:length(ELEMENTS)
	if isempty(strfind(lower(ELEMENTS{k}), match))
		continue;
	end
	
	ix(end + 1) = k;  %#ok<AGROW>
	
	if first
		break;
	end
end

elements = ELEMENTS(ix);
