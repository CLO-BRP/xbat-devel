function n = get_next_even(n, strict)

% get_next_even - get next even number
% ------------------------------------
% 
% n = get_next_even(n, strict)
%
% Input:
% ------
%  n - numbers
%  strict - requires next not current even in such case
%
% Output:
% -------
%  n - next even numbers

if nargin < 2
	strict = 0;
end

n = ceil(n); 

if strict
	n = n + mod(n, 2) + 2 * ~mod(n, 2);
else
	n = n + mod(n, 2);
end