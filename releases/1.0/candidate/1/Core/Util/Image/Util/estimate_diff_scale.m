function scale = estimate_diff_scale(X)

% estimate_diff_scale - used in anisotropic diffusion
% ---------------------------------------------------
%
% scale = estimate_diff_scale(X)
%
% Input:
% ------
%  X - image
% 
% Output:
% -------
%  scale - estimate, for each plane if needed

% TODO: implement using diff and compare

h = [-1, 1, 0];

switch ndims(X)
	case 2
		tic
		D = [vec(diff(X, 1, 1)), vec(diff(X, 1, 2))];
		t1 = toc;
		
		tic
		D = [linear_filter(X, h); linear_filter(X, h')];
		t2 = toc
		
		scale = 1.4826 * fast_mad(D);
		
	case 3
		K = size(X, 3); scale = zeros(1, K);
		
		for k = 1:K
			D = [linear_filter(X(:, :, k), h); linear_filter(X(:, :, k), h')];
		
			scale(k) = 1.4826 * fast_mad(D);
		end
end


