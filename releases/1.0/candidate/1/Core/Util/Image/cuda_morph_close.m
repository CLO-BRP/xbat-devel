function Y = cuda_morph_close(X, SE, m, n, s, i, b)

% cuda_morph_close - compute close in neighborhood defined by SE
% -------------------------------------------------------------
% 
% Y = cuda_morph_close(X, SE, m, n, s, i, b)
% 
% Input:
% ------
%  X - input image
%  SE - structuring element
%  m - SE m size
%  n - SE n size
%  s - separable boolean, default false
%  i - number of iterations, default 1
%  b - boundary behavior, default reflecting
%
%   -2 - cyclic boundary
%   -1 - reflecting boundary
%    n - n padding for n >= 0
%
% Output:
% -------
%  Y - closed image
%  e - CUDA error code

% NOTE: Only reflective boundary is implemented

%--
% grayscale or rgb image
%--

if nargin < 2
	error 'required argument missing';
end

if nargin < 3
    m = size(SE, 1);
end

if nargin < 4
    n = size(SE, 2);
end

SE = uint8(SE);

if nargin < 5
    s = 0;
end

if nargin < 6
    i = 1;
end

if nargin < 7
    b = -1;
end

if n >= size(X, 2) || m >= size(X, 1)
	b = 0;
end

d = ndims(X);

switch (d)

	case (2)
	
		[Y, status] = cuda_morph_filter_mex(X, SE, m, n, s, 4, i, b, true);
        
        handle_cuda_failure(status)

		
	case (3)
	
		%--
		% initialize output depending on datatype
		%--
			
		Y = X;
		
		%--
		% pad plane by plane
		%--
		
		for k = 1:3
			[Y(:,:,k), status] = cuda_morph_filter_mex(X(:,:,k), SE, m, n, s, 4, i, b, true);
            
            handle_cuda_failure(status)
		end

end
