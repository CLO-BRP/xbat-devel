function t = is_gray(X)

% is_gray - check for grayscale image
% -----------------------------------
%
% t = is_gray(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  t - grayscale indicator
% 
% See also: is_binary, is_rgb, is_tricolor

t = (ndims(X) == 2) && ~(any(X(:) < 0));
