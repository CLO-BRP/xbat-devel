#include "mex.h"

#include <stdlib.h>

#include "cuda_runtime.h"

#include "cuda_image_pad.h"

//--------------------------------------------------
// Create a Matlab scalar value
//--------------------------------------------------

mxArray* mxCreateScalar(double x)
{
    mxArray* p = mxCreateDoubleMatrix(1,1,mxREAL);
    double*  ptr = mxGetPr(p);
    ptr[0] = x;
    return p;
}

//--------------------------------------------------
// MEX FUNCTION
//--------------------------------------------------

// cuda_image_pad - pad 2D image
// -------------------------------------------------
//
// Implements 2D image padding
//

//
// Y = cuda_image_pad(X,SE)
//
// Input:
// ------
//  X - input data
//  pq - support parameters
//
// Output:
// -------
//  Y - Padded image

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	void  *h_Data;				// Host source
	void  *d_Data;				// Device source
	
	void  *h_Result;			// Host result
	void  *d_Result;			// Device result

	unsigned int	mX, nX;     // Source dimensions
	unsigned int	N;			// Source sample count
	unsigned int	p = 0, q = 0; // Support parameters
	unsigned int	resultSize;	// Result size

	cudaError_t		cudaErr = cudaSuccess;

	// Get input data
	mX     = mxGetM(prhs[0]);
	nX     = mxGetN(prhs[0]);
	N      = mX * nX;
	h_Data = (void *) mxGetPr(prhs[0]);
	
	// Get support parameters
    if (nrhs > 1 && (mxGetM(prhs[1]) * mxGetN(prhs[1])) > 1)
    {
        p = mxGetPr(prhs[1])[0];
		q = mxGetPr(prhs[1])[1];
    }
	
	// Size of result image
	resultSize = (mX + 2 * p) * (nX + 2 * q);

	switch (mxGetClassID(prhs[0])) {
		// BEGIN-EXPAND-TYPES
		
		case MEX_TYPE_CLASS:
			// Create result variable
			h_Result = (void  *) mxGetPr(plhs[0] = mxCreateNumericMatrix(mX + 2 * p, nX + 2 * q, MEX_TYPE_CLASS, mxREAL));
			
			// Allocate GPU global memory for input data
			if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **)&d_Data, N * sizeof(MEX_TYPE)); }
			
			// Allocating GPU global memory for result
			if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **)&d_Result, resultSize * sizeof(MEX_TYPE) ); }
			
			// Copy input data from host to GPU
			if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(d_Data, h_Data, N * sizeof(MEX_TYPE), cudaMemcpyHostToDevice); }
			
			// Compute median filter on GPU
			cuda_image_pad_MEX_TYPE_NAME((MEX_TYPE *) d_Result, (MEX_TYPE *) d_Data, mX, nX, p, q);
			
			// Copy results data from GPU to host
			if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(h_Result, d_Result, resultSize * sizeof(MEX_TYPE), cudaMemcpyDeviceToHost); }
			
			break;
			
			// END-EXPAND-TYPES
	}
	
	// Return CUDA error if requested
	if (nlhs > 1) {
		plhs[1] = mxCreateScalar((double) cudaErr);
	}
	
	// Free GPU global memory
	if (d_Result != NULL) { cudaFree(d_Result); }
	if (d_Data != NULL) { cudaFree(d_Data); }
	
	
}
