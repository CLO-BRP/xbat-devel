function build_cuda_image_custom(global_opt)

% build_cuda_image_custom - non-standard filters
% ----------------------------------------------
%
% build_cuda_image_custom(global_opt)
%
% Input:
% ------
%  global_opt - build options struct, obtained from build_cuda_mex

%--
% handle input
%--

if ~nargin
	global_opt = build_cuda_mex;
end

%--
% always rebuild padding code in the event that build options have changed
%--

obj = 'cuda_image_pad.obj';

opt = global_opt; opt.link = false;
	
build_cuda_mex('image_pad', opt);

if exist('cuda_image_pad.cu.obj', 'file')
	% HACK: this ugly block resolves a discrepancy in the naming of output files between compilers in 32 and 64 bit environments 

	movefile('cuda_image_pad.cu.obj', 'cuda_image_pad.obj');
end

%--
% build custom filters
%--

% summed area table

clear('cuda_sat');

build_cuda_mex('sat');

% anisotropic diffusion

opt = global_opt;

opt.mex = {obj};

clear('cuda_anisodiff');

build_cuda_mex('anisodiff', opt);

% mean and deviation

opt = global_opt;

opt.mex = {obj};

clear('cuda_mean_dev_mex');

build_cuda_mex('mean_dev', opt);




