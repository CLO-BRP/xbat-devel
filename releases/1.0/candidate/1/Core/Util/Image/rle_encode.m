function R = rle_encode(X, T)

% rle_encode - binarization and run-length encoding of images
% -----------------------------------------------------------
%
% R = rle_encode(X, T)
%
% Input:
% ------
%  X - input image
%  T - binarization threshold (def: [], no binarization)
%
% Output:
% -------
%  R - run-length code

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

% NOTE: some error checking would be needed for general use

%--
% set default thresold
%--

if nargin < 2
	T = [];
end

%--
% convert input to double if needed
%--

% TODO: MEX should handle a variety of data types transparently

if ~strcmp(class(X), 'double')
	X = double(X);
end

%--
% MEX for encoding with binarization if needed
%--

if isempty(T)
	R = rle_encode_(X);
else
	R = rle_encode_(X, T);
end

