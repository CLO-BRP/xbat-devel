function test_linear_filter(X, f)

% test_linear_filter - for variable filter size and separable vs direct
% ---------------------------------------------------------------------
% 
% test_linear_filter(X, f)
%
% Input:
% ------
%  X - input image (def: random image)
%  f - filter sizes (def: 1:2:13)

%--
% handle input and setup
%--

if nargin < 2
	f = 1:2:21;
end

if ~nargin || isempty(X);
	
	X = 255 * rand(1024);
end

% NOTE: this makes sure that 'linear_filter' does not call 'cuda_linear_filter'

cuda_enabled false;

%--
% run tests
%--

test_count = 1;

disp(' ');

for i = 1:2
    
    if i == 2
        disp('SEPARABLE-FILTER');
    else
        disp('DIRECT-FILTER');
    end
    
    disp(' ');
    
    for s = 0:1
        
        if s == 1
            disp('SAME TYPE');
        else
            disp('DOUBLE');
        end
        
        for j = 1:numel(f)
            
            for k = 1:numel(f)
                
                % for t = 1:numel(types)
                
                F = 100 * randn(f(j), f(k)); failure = false;
                
                if i == 2
                    F = filt_decomp(F);
                end
                
                try
                    start = tic; Y1 = linear_filter(X, F, 1, -1, s); elapsed1 = toc(start);
                catch
                    failure = true; elapsed1 = inf; nice_catch;
                end
                
                try
                    start = tic; Y2 = cuda_linear_filter(X, F, 1, -1, s); elapsed2 = toc(start);
                catch
                    failure = true; elapsed2 = inf; nice_catch;
                end
                
                max_error = max(vec(abs(Y1(:) - Y2(:))));
                
                passed = ~failure && (max_error < 10^-8);
                
                % NOTE: this would probably be better using 'fprintf'
                
                disp([
                    'size = [', int2str(f(j)), ', ', int2str(f(k)), '], passed = ', int2str(passed), ', max_error = ' num2str(max_error), ...
                    ', elapsed = [', sec_to_clock(elapsed1), ', ', sec_to_clock(elapsed2), '], speed = ', num2str(elapsed1/elapsed2) ...
                    ]);
                
                speedup(test_count) = elapsed1/elapsed2;
                test_count = test_count + 1;
                
            end
            
            % end
            
        end
        
    end
    
end

disp(['Median speedup: ', num2str(median(speedup(:)))]);
disp(['Max speedup: ', num2str(max(speedup(:)))]);

