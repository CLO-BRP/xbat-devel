function [result, info] = test_cuda_morph(varargin)

% test_cuda_image_morph - test cuda image morphological filtering for
%   agreement with C implementation
% --------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'exhaustive' - run extensive benchmark instead of fast benchmark
%  'display' - display CUDA padded image for each test
%  'verbose' - print status output
%  'silent' - print nothing
%  anything else - assumed to be type conversion function handle ie. 'double'
%
% Output
% ------
%  result = 1 if passed, 0 if failed

% NOTE: default is fast test on double precision data with no messages
% and no display, equivalent to
% 'test_cuda_image_min('quick', 'silent', 'double')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0; display = 0;
type_func = @double;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case {'exhaustive' 'full'}, fast = 0;
            
        case 'silent', verbose = 0;
            
        case 'verbose', verbose = 1;
            
        case 'display', display = 1;
            
        otherwise, type_func = str2func(varargin{i});
    end
end

%----------------------
% SETUP
%----------------------

%--
% initialize output
%--

result = 1;

%--
% load test data
%--

% NOTE: we load data and cast for type we are testing

X = 100 * rand(1024);
% load clown;

X = type_func(X);

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if isa(X, 'double') && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end

%--
% set parameter ranges
%--

% Padding sizes

if fast
    p = 3:4:15; q = p;
else
    p = 1:2:25; q = p;
end


%----------------------
% RUN TEST
%----------------------

%--
% Compare CUDA image min to Matlab implementation over range of parameters
%--

if display
    par = fig;
end

% Disable transparent CUDA acceleration

cuda_state = cuda_enabled;
cuda_enabled false;

test_count = 1;

for separable = 0:1
    
    if separable
        disp('SEPARABLE');
    else
        disp('NEIGHBORHOOD');
    end
    
    for k = 1:10 % operation
        
		switch k
			case 1
				disp('Erode');
			case 2
				disp('Dilate');
			case 3
				disp('Open');
			case 4
				disp('Close');
			case 5
				disp('Tophat, White');
            case 6
                disp('Tophat, Self');
			case 7
				disp('Tophat, Black');
			case 8
				disp('Gradient');
			case 9
				disp('Gradient Inner');
			case 10
				disp('Gradient Outer');
		end
        
        for i = 1:numel(p)
            
            for j = 1:numel(q)
                
                if separable
                    SE = ones(p(i), q(j));
                else
                    SE = double(fix(2 * rand(p(i), q(j))));
                end
                
                    
                    % CUDA morphological function
                    try
                        start = tic;
						switch k
							case 1
								Y0 = cuda_morph_filter('erode', X, uint8(SE), -1, 1);
							case 2
								Y0 = cuda_morph_filter('dilate', X, uint8(SE), -1, 1);
							case 3
								Y0 = cuda_morph_filter('open', X, uint8(SE), -1, 1);
							case 4
								Y0 = cuda_morph_filter('close', X, uint8(SE), -1, 1);
							case 5
                                Y0 = cuda_morph_filter('tophat-white', X, uint8(SE), -1, 1);
							case 6
                                Y0 = cuda_morph_filter('tophat-self', X, uint8(SE), -1, 1);
                            case 7
                                Y0 = cuda_morph_filter('tophat-black', X, uint8(SE), -1, 1);
                            case 8
                                Y0 = cuda_morph_filter('gradient', X, uint8(SE), -1, 1);
                            case 9
                                Y0 = cuda_morph_filter('gradient-inner', X, uint8(SE), -1, 1);
                            case 10
                                Y0 = cuda_morph_filter('gradient-outer', X, uint8(SE), -1, 1);
						end
                        elapsedc = toc(start);
                    catch
                        passed = false; elapsedc = inf; nice_catch;
                    end
                    
                    if display
                        figure(par);
                        
                        image_view(double(Y0));
                    end
                    
                    % Matlab morphological function
                    
                    start = tic;
										
                    switch k
                        case 1
                            Y1 = morph_erode(X, SE);
                        case 2
                            Y1 = morph_dilate(X, SE);
                        case 3
                            Y1 = morph_open(X, SE);
                        case 4
                            Y1 = morph_close(X, SE);
                        case 5
                            Y1 = morph_tophat(X, SE, 1);
                        case 6
                            Y1 = morph_tophat(X, SE, -1);
                        case 7
                            Y1 = morph_tophat(X, SE, 0);
                        case 8
                            Y1 = morph_gradient(X, SE, 0);
                        case 9
                            Y1 = morph_gradient(X, SE, -1);
                        case 10
                            Y1 = morph_gradient(X, SE, 1);
                    end
					
                    elapsedm = toc(start);
                    
                    % Set return code if we have an error
                                        
                    if any(Y0(:) - Y1(:))
                        passed = false;
                    else
                        passed = true;
                    end
                                        
                    if ~passed
                        result = false;
                    end
                    
                    % NOTE: this would probably be better using 'fprintf'

                    disp([
                        'size = [', int2str(p(i)), ', ', int2str(q(j)), '], passed = ', int2str(passed), ...
                        ', elapsed = [', sec_to_clock(elapsedm), ', ', sec_to_clock(elapsedc), '], speed = ', num2str(elapsedm/elapsedc) ...
                        ]);
                    
                    speedup(test_count) = elapsedm/elapsedc;
                    test_count = test_count + 1;
                    
                end
                
            end
                   
    end
    
end

disp(['Median speedup ', num2str(median(speedup(:)))]);
disp(['Max speedup ', num2str(max(speedup(:)))]);

% Restore CUDA state

cuda_enabled(cuda_state);
