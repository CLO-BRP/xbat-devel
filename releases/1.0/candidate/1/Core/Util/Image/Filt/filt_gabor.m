function F = filt_gabor(sigma, theta, omega, tol)

% filt_gabor - create gabor filters
% ---------------------------------
%
% F = filt_gabor(sigma, theta, omega, tol)
%
% Input:
% ------
%  sigma - standard deviation (def: 1)
%  theta - orientation angle (def: 0)
%  omega - normalized frequency (def: 0.5)
%  tol - relative filter value cutoff tolerance (def: 10^-2)
%
% Output:
% -------
%  F - normalized gabor filter mask

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5223 $
% $Date: 2006-06-11 21:57:45 -0400 (Sun, 11 Jun 2006) $
%--------------------------------

%--
% handle input
%--

if (nargin < 4) || isempty(tol)
	tol = 10^-2;
end

if (nargin < 3) || isempty(omega)
	omega = 0.5;
end
	
if (nargin < 2) || isempty(theta)
    theta = 0;
end     

if (nargin < 1) || isempty(sigma)
    sigma = 1;
end

% NOTE: duplicate scalar deviation for convenience

if numel(sigma) == 1
	sigma = [sigma, sigma];
end

%--
% compute axis-aligned or rotated evaluation grid
%--

if ~theta	
	px = -3*ceil(sigma(1)):3*ceil(sigma(1));
	
	py = -3*ceil(sigma(2)):3*ceil(sigma(2));
	
	[X, Y] = meshgrid(px, py);
else	
	sigma(:) = max(sigma(1), sigma(2));
	
	px = -4*sigma:4*sigma; py = px;
		
	[X, Y] = meshgrid(px, py);

    % NOTE: we create rotation matrix, then vectorize positions and rotate

    A = [ ...
		cosd(theta), -sind(theta); ...
		sind(theta), cosd(theta) ...
	];
        
    xy = [X(:)'; Y(:)'];
	
    xy = A * xy;
    
    % reshape positions
    
    X = reshape(xy(1,:), size(X));
	
    Y = reshape(xy(2,:), size(Y));
end 

%--
% evaluate function on grid
%--

A = exp(-((X.^2 ./ (2*sigma(1)^2)) + (Y.^2 ./ (2*sigma(2)^2))));

F = A .* cos((2*pi) * (omega / sigma(1)) * X); 

%--
% normalize and tighten mask support
%--

F = F / max(abs(F(:)));

F = filt_tight(F, tol);

%--
% euclidean normalization
%--

% TODO: is this the right normalization?

F = F / sqrt(sum(F(:).^2));
