function Y = rgb_linear(X,V)

% rgb_linear - linear transform of colorspace
% -------------------------------------------
%
% Y = rgb_linear(X,V)
%
% Input:
% ------
%  X - input image
%  V - linear transformation
%
% Output:
% -------
%  Y - linear transform colorspace image

%--
% get image size
%--

[m,n,d] = size(X); 

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% check size of transform
%--

if (any(size(V) ~= 3))
	error('Colorspace transformation matrix must be 3 by 3.');
end

%--
% apply linear transform
%--

Y = rgb_reshape(rgb_vec(X)*V,m,n);


