function info = conversion_test(X)

% conversion_test - test colorspace conversions
% ---------------------------------------------
%
%  info = conversion_test(X)
%
% Input:
% ------
%  X - image to use in tests
%
% Output:
% -------
%  info - error reporting on the various transformations

%--
% create list of conversion pairs
%--

C = { ...
	'rgb_to_hsv','hsv_to_rgb'; ...
	'rgb_to_lab','lab_to_rgb'; ...
	'rgb_to_opp','opp_to_rgb'; ...
	'rgb_to_xyz','xyz_to_rgb'; ...
	'rgb_to_rgbnl','rgbnl_to_rgb'; ...
	'rgb_to_luv','luv_to_rgb' ...
};

%--
% create conversion information structure
%--

tmp.conversion = '';
tmp.time = [];
tmp.error = [];

info = tmp;

%--
% run color conversion tests
%--

% get number of available conversions

n = length(C);

% loop over conversions

for k = 1:n
	
	%--
	% compute and time identity transformation
	%--
	
	disp(' ');
	disp(['Computing ''' C{k,1} ''' identity ...']);
	disp('------------------------------------');
		
	try
		
		tic;
		Y = feval(C{k,2},feval(C{k,1},X));
		t = toc;
		
	catch
		nice_catch(lasterror, ['Problems computing ''' C{k,1} ''' type identity.']);
		
		info(k) = tmp;
		
		info(k).conversion = C{k,1};
		info(k).time = 'ERROR';
		info(k).error = 'ERROR';
		
		continue;
		
	end
	
	%--
	% compute error
	%--
	
	e = fast_min_max(double(Y) - double(X));
	
	%--
	% collect info
	%--
	
	info(k) = tmp;
	
	info(k).conversion = C{k,1};
	info(k).time = t;
	info(k).error = e;
	
	%--
	% display info
	%--
	
	disp(info(k));
	
% 	disp(['time:  ' num2str(t) ' sec']);
% 	disp(['error: ' mat2str(e,4)]);
% 	disp(['error: ' mat2str(e./eps,4)]);
% 	disp(' ');
	
end
	
if ~nargout
	clear info;
end

	
	
	