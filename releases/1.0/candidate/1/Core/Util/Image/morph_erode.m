function Y = morph_erode(X, SE, n, b)

% morph_erode - morphological erosion
% -----------------------------------
% 
% Y = morph_erode(X, SE, n, b)
%   = morph_erode(X, SE, Z, b)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  n - iterations of operation (def: 1)
%  Z - computation mask image (def: [])
%  b - boundary behavior (def: -1)
%
% Output:
% -------
%  Y - eroded image
%
% See also: morph_dilate, morph_gradient, image_extrema

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% check for handle input
%--

[X, N, tag, flag, g, h] = handle_input(X, inputname(1));

%--
% set boundary behavior
%--

if (nargin < 4)
	b = -1;
end

%--
% iteration or mask
%--

if (nargin < 3) || isempty(n)
	
	n = 1; Z = [];
else
	Z = [];
	
	% TODO: match the logical consideration of the 'tag' here with the one below
	
	switch tag
		
        case {'IMAGE_GRAY', 'IMAGE_GRAY_U8'}, [r, c] = size(X);		
		
        case {'IMAGE_RGB', 'IMAGE_RGB_U8', 'IMAGE_NDIM', 'IMAGE_NDIM_U8'}, [r, c, s] = size(X);
		
        case 'IMAGE_STACK', [r, c] = size(X{1});
        
        otherwise, [r, c] = size(X);
		
	end
		
	if all(size(n) == [r, c])
		Z = n; n = 1;
	end
end

%--
% check for trivial processing
%--

% convert structuring element to matrix and check for scalar

if length(se_mat(SE)) == 1
	Y = X; return;
end  

% invoke CUDA implementation if possible

% NOTE: the code below will remember where CUDA morph erode failed and not delegate

B = se_mat(SE); state.data = {class(X), size(B)};

if isempty(Z) && cuda_enabled && ~cuda_has_failed(mfilename, state)
	try		
		Y = cuda_morph_filter('erode', X, B, b, n); return;
	catch		
		cuda_has_failed(mfilename, state, lasterror);
		
		clear cuda_morph_filter_mex; % TODO: could this be integrated into 'cuda_has_failed'? probably not simple
	end
end

%--
% process depending on image type
%--

% NOTE: these are multiple plane images packed into a three-dimensional array

if string_contains(tag, 'NDIM') || string_contains(tag, 'RGB')
	
	Y = zeros(size(X), class(X));
	
	for k = 1:size(X, 3)
		if isempty(Z)
			Y(:, :, k) = morph_erode(X(:, :, k), SE, n, b);
		else
			Y(:, :, k) = morph_erode(X(:, :, k), SE, Z, b);
		end
	end
	
% NOTE: these are images packed into a cell array

elseif string_contains(tag, 'STACK')
	
	Y = cell(size(X));
	
	for k = 1:size(X)
		if isempty(Z)
			Y{k} = morph_erode(X{k}, SE, n, b);
		else
			Y{k} = morph_erode(X{k}, SE, Z, b);
		end
	end

% NOTE: this is where we process single planes

else
	% NOTE: decompose structuring element and use decomposed computation if worthwhile
	
	SEQ = se_decomp(SE);
	
	if SEQ.ratio > 1.5
		
		if isempty(Z)
			
			for j = 1:n
				Y = erode_decomp(X, SEQ, b); X = Y;
			end
		else
			Y = erode_decomp(X, uint8(SEQ), b, uint8(Z));
		end
		
	else
		B = se_mat(SE); pq = se_supp(SE);
		
		if isempty(Z)
			for j = 1:n
				X = image_pad(X, pq, b);
				
				Y = morph_erode_mex(X, uint8(B)); X = Y;
			end
		else
			X = image_pad(X, pq, b); Z = image_pad(Z, pq, 0);
			
			Y = morph_erode_mex(X, uint8(B), uint8(Z));
		end
	end
	
end

%--
% display output
%--

if flag && view_output
	
	switch view_output
		
		case 1
			figure(h);
			set(g, 'cdata', Y);
			set(gca, 'clim', fast_min_max(Y));
			title_edit(['\Er \pb{' N '}']);
			
		otherwise
			fig;
			image_view(Y);
			title_edit(['\Er \pb{' N '}']);		
	end
	
end

%---------------------------------------
% GET_FILTER_KEY
%---------------------------------------

function key = get_filter_key(F)

if isstruct(F)
	key = ['separable', int2str(size(F.H, 1)), 'x', int2str(size(F.H, 2))];
else
	key = ['direct', int2str(size(F, 1)), 'x', int2str(size(F, 2))];
end

%----------------------------------------------
% ERODE_DECOMP
%----------------------------------------------

function Y = erode_decomp(X, SEQ, b, Z)

% erode_decomp - erosion using decomposed structuring element
% -----------------------------------------------------------
%
% Y = erode_decomp(X, SEQ, b, Z)
%
% Input:
% ------
%  X - input image
%  SEQ - decomposed structuring element
%  b - boundary condition flag
%  Z - computation mask
%
% Output:
% -------
%  Y - eroded image

%--
% set mask
%--

if (nargin < 4)
	Z = [];
end

%--
% compute according to mask
%--

if isempty(Z)
	
	%--
	% erode with first line if needed
	%--
	
	if length(SEQ.line{1}) > 1
		
		pq = se_supp(SEQ.line{1});
		
		Y = morph_erode_mex( ...
			image_pad(X, pq, b), ...
			uint8(SEQ.line{1}) ...
		);
	else
		Y = X;
	end
	
	%--
	% erode with second line if needed
	%--
	
	if length(SEQ.line{2}) > 1
		
		pq = se_supp(SEQ.line{2});
		
		Y = morph_erode_mex( ...
			image_pad(Y, pq, b), ...
			uint8(SEQ.line{2}) ...
		);
	end
	
	%--
	% apply residual and compute minimum if needed
	%--
	
	if ~isempty(SEQ.rest)
		
		% NOTE: the minimum function is defined for relevant input classes

		pq = se_supp(SEQ.rest);
		
		Y = min( ...
			Y, ...
			morph_erode_mex( ...
				image_pad(X, pq, b), ...
				uint8(SEQ.rest) ...
			) ...
		);
	end

else
	
	%--
	% erode with first line if needed
	%--
	
	if length(SEQ.line{1}) > 1
	
		pq = se_supp(SEQ.line{1});
		
		Y = morph_erode_mex( ...
			image_pad(X, pq, b), ...
			uint8(SEQ.line{1}), ...
			uint8(image_pad(Z, pq, 0)) ...
		);
	else
		Y = X;
	end
	
	%--
	% erode with second line if needed
	%--
	
	if length(SEQ.line{2}) > 1
	
		pq = se_supp(SEQ.line{2});
		
		Y = morph_erode_mex( ...
			image_pad(Y, pq, b), ...
			uint8(SEQ.line{2}), ...
			uint8(image_pad(Z, pq, 0)) ...
		);
	end
	
	%--
	% apply residual and compute minimum if needed
	%-- 
	
	if ~isempty(SEQ.rest)
				
		pq = se_supp(SEQ.rest);
		
		Y = min( ...
			Y, ...
			morph_erode_mex( ...
				image_pad(X, pq, b), ...
				uint8(SEQ.rest), ...
				uint8(image_pad(Z, pq, 0)) ...
			) ...
		);
	end

end
	
