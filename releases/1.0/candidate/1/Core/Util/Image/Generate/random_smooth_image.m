function X = random_smooth_image(N, r, b)

% random_smooth_image - generation
% --------------------------------
%
% X = random_smooth_image(N, r, b)
%
% Input:
% ------
%  N - size (def: 512)
%  r - radius for smoothing, or radii (def: [16, 16, 16])
%  b - bounds for values (def: [0, 255])
%
% Output:
% -------
%  X - random smooth image

%--
% handle input
%--

if nargin < 3
	b = [0, 255];
end 

if nargin < 2
	r = [32, 32, 32];
end

if ~nargin
	N = 256;
end

if length(N) == 1
	N = [N, N];
end

%--
% generate image
%--

X = rand(N(1), N(2)); r = next_odd(r);

for k = 1:numel(r)
	f = filt_binomial(r(k));
	
	X = linear_filter(linear_filter(X, f), f');
end

X = lut_range(X, b);

%--
% display if output not captured
%--

if ~nargout
	fig; imagesc(X); colormap(gray(256)); axis image; clear X;
end
