function Y = image_clip(X,b)

% image_clip - clip range of an image 
% -----------------------------------
%
% Y = image_clip(X,b)
% 
% Input:
% ------
%  X - input image
%  b - image value bounds (def: [0,255])
%
% Output:
% -------
%  Y - clipped image

%--
% set bounds
%--

if (nargin < 2)
	b = [0,255];
end
 
%--
% initialize output image
%--

Y = X;

%--
% clip lower bound pixels
%--

ix = find(X < b(1));
Y(ix) = b(1);

%--
% clip upper bound pixels
%--

ix = find(X > b(2));
Y(ix) = b(2);
