function figs_cycle(t, h)

% figs_cycle - cycle through open figure windows
% ----------------------------------------------
%
% figs_cycle(t, h)
%
% Input:
% ------
%  t - time between figures (def: -1, pause)
%  h - figure handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.1 $
% $Date: 2003-11-05 16:00:57-05 $
%--------------------------------

%--
% set figure handles
%--

if nargin < 2
	h = get(0, 'children'); h = sort(h);
end

%--
% set and check pause interval
%--

if ~nargin
	t = -1;
end

tn = length(t); hn = length(h);

% NOTE: this is not currently in use, consider removing 

if tn == 1
	for k = 1:hn
		v(k) = t;
	end
	t = v;
elseif tn ~= hn
	error('Pause interval input must be scalar or match the number of figures.');
end
	
%--
% cycle through windows
%--

for k = 1:length(h)
	
	figure(h(k)); 
	
	if t > 0
		pause(t);
	else
		pause;
	end
end
