function test_radial_grid

% setup axes

fig; ax = axes; set(ax, 'xlim', [-2, 2], 'ylim', [-2, 2], 'box', 'on'); 

props = {'linewidth', 1};

% default grid

opt = set_radial_grid; opt.ray_start = 0.05;

set_radial_grid(ax, 0.25:0.25:1, 0:45:359, opt, props{:});

% another configured grid

opt = set_radial_grid; 

opt.color = [1, 0.5, 0]; opt.prefix = 'another-grid:'; opt.ray_start = 0.1;

opt.center = 1 + 1i;

a = 0:60:359; a = a + 6;

set_radial_grid(ax, [opt.ray_start, 0.33 * [1, 2]], a, opt, props{:}, 'linestyle', '--');

opt.center = 1i; opt.color = 'red'; opt.prefix = 'test1';

set_radial_grid(ax, 0.5 * [opt.ray_start, 0.33 * [1, 2]], [], opt, props{:}, 'linestyle', '--');

opt.center = -1 - 1i; opt.color = 'blue'; opt.prefix = 'test2';

set_radial_grid(ax, [], a + 18, opt, props{:}, 'linestyle', '--');

axis equal;

% set(ax, 'xlim', [-1, 1], 'ylim', [-1, 1]); 