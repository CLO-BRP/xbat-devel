function corner = get_complex_corners(xlim, ylim)

% get_complex_corners - of box spanning ranges
% --------------------------------------------
%
% corner = get_complex_corners(xlim, ylim)
%
% Input:
% ------
%  xlim - range
%  ylim - range 
%
% Output:
% -------
%  corner - points

corner = [xlim, fliplr(xlim)] + 1i * kron(ylim, ones(1, 2));