function test_complex_scatter

ax = test_axes;

%--
% using points on circle, display values with size and color, then display spiral
%--

z = circle(0, 1, 16); value = 1:16; 

map = complex_scatter; map.range = [12, 36];

complex_scatter(ax, z, value, [], 'markerfacecolor', 0.75 * ones(1, 3));

map.prop = 'markerfacecolor'; 

handle = complex_scatter(ax, z, value, map, 'markersize', 8);

uistack(handle, 'top');

w = z .* linspace(1.25, 2, numel(z));

complex_scatter(ax, w, value, map, 'markersize', 12);
