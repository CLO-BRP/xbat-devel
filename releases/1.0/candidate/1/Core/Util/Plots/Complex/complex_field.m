function handle = complex_field(par, z, w, varargin)

% complex_field - plot using complex data
% ---------------------------------------
%
% handle = complex_field(par, z, w, 'field', value)
%
% Input:
% ------
%  par - axes
%  z - position points
%  w - field points
%  field, value - pairs
% 
% Output:
% -------
%  handle - 
%
% See also: complex_plot, complex_scatter, complex_text