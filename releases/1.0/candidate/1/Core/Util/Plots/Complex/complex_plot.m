function handle = complex_plot(par, z, varargin)

% complex_plot - plot with complex points
% ---------------------------------------
%
% handle = complex_plot(par, z, 'field', value, ... )
%
% Input:
% ------
%  par - axes 
%  z - points to plot, one line per column for matrices
%  field, value - pairs
%
% Output:
% -------
%  handle - array to lines created
%
% NOTE: this function plots point sets as lines, for point plots use 'scatter_complex'
%
% See also: complex_field, complex_scatter, complex_text

%--
% plot complex vector or set of columns
%--

if min(size(z)) == 1
	
	handle = line('parent', par, 'xdata', real(z(:)), 'ydata', imag(z(:)));
else
	n = size(z, 2); handle = zeros(1, n);
	
	for k = 1:n
		handle(k) = line( ...
			'parent', par, ...
			'xdata', real(z(:, k)), ...
			'ydata', imag(z(:, k)) ...
		);
	end
end

%--
% set any requested properties
%--

if ~isempty(varargin)
	
	set(handle, varargin{:});
end

