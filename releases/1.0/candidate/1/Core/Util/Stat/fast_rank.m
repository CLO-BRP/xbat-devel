function y = fast_rank(X, k, Z)

% fast_rank - fast ranked value computation
% -----------------------------------------
% 
% y = fast_rank(X, k, Z)
%   = fast_rank(X, f, Z)
%
% Input:
% ------
%  X - input image
%  k - rank parameter
%  f - fraction of data element is above or below (when negative)
%  Z - select indicator (def: [], consider all data)
%
% Output:
% -------
%  y - ranked value

%--
% set mask
%--

if nargin < 3
	Z = [];
end

%--
% apply mask
%--

if ~isempty(Z)
	X = X(logical(Z));
end

%--
% check for rank or fraction
%--

if nargin < 2
	error('Rank or fraction parameter is missing.');
end
	
%--
% handle multiple rank requests
%--

if numel(k) > 1
	y = iteraten(mfilename, 2, X, k, Z); return;
end

%--
% convert fraction to rank 
%--

N = numel(X);

if (abs(k) > 0) && (abs(k) < 1)

	% size of array
	
	N = numel(X);

	% rank from lowest and highest
	
	if (k > 0)
		k = ceil(N * k);
	else
		k = N + floor(N * k) + 1;
	end
	
end

%--
% check rank is in range
%--

if (k < 1) || (k > N)
	error('Rank or fraction parameter out of range.');
end

%--
% consider special cases or compute using mex
%--

switch k
	
case 1
	y = min(X(:));
	
case N
	y = max(X(:));
	
otherwise
	y = fast_rank_(X, k);
	
end