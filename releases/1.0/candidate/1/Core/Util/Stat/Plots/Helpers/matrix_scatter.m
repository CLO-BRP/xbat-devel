function [handles, opt, groups] = matrix_scatter(ax, data, opt)

% matrix_scatter - plot helper
% ----------------------------
%
% handles = matrix_scatter(ax, data, opt)
%
% Input:
% ------
%  ax - container
%  data - to plot
%  opt - for plot
%
% Output:
% -------
%  handles - created
%
% NOTE: group is given as index 
%
% See also: matrix_plot

% NOTE: using the outputs we can match color to group, perhaps tag and control

% TODO: implement the use of 'size' and 'color' vars

%--
% handle input
%--

if ~has_type(data, 'group')
	group = [];
else
	groups = unique(data.group);
end

if nargin < 3
	
	% NOTE: we generate as many colors as there are groups
	
	if isempty(group)
		opt.color = 0.75 * [1 0 0];
	else
		opt.color = 0.75 * group_colors(numel(groups));
	end
	
	opt.linestyle = 'none';
	
	opt.marker = 'o';
	
	opt.markersize = 8;
	
	if ~nargin
		handles = opt; return;
	end
end

%--
% plot data
%--

handles = [];

if isempty(data.y)
	
	% NOTE: for the diagonal plots we display a single variable distribution
	
	if isempty(group)
		handles = [handles, kernel_density_plot(ax, data.x)];
		
		set(handles, 'color', opt.color);
	else
		for k = 1:numel(groups)
			part = kernel_density_plot(ax, data.x(data.group == groups(k)));
			
			set(part, 'color', opt.color(k, :));
			
			handles = [handles, part]; %#ok<AGROW>
		end
	end
	
else
	
	% NOTE: for off-diagonal plots we display a two-variable scatter
	
	if isempty(group)
		handles(end + 1) = line( ...
			'parent', ax, ...
			'xdata', data.x, ...
			'ydata', data.y, ...
			'color', opt.color ...
		);
	else
		for k = 1:numel(groups)
			select = group == groups(k);
			
			handles(end + 1) = line( ...
				'parent', ax, ...
				'xdata', data.x(select), ...
				'ydata', data.y(select), ...
				'color', opt.color(k, :) ...
			); %#ok<AGROW>
		end
	end
	
	set(handles, ...
		'linestyle', opt.linestyle, ...
		'marker', opt.marker, ...
		'markersize', opt.markersize ...
	);
	
end


%----------------------
% GROUP_COLORS
%----------------------

% NOTE: many more colors are difficult to read, consider using marker

function color = group_colors(total)

% NOTE: this only work for six groups or less

color = 0.9 * [eye(3); 1 1 0; 0 1 1; 1 0 1];

color = color(1:total, :);


%----------------------
% HAS_TYPE
%----------------------

function value = has_type(data, type)

value = isfield(data, type) && ~isempty(data.(type));



