function [r, l] = fast_range(varargin)

% fast_range - get range of values in array
% -----------------------------------------
%
% [r, l] = fast_range(X, Z) 
%
% Input:
% ------
%  X - input data
%  Z - select indicator (def: [], consider all data)
%
% Output:
% -------
%  r - range
%  l - limits min and max

l = fast_min_max(varargin{:}); r = diff(l);
