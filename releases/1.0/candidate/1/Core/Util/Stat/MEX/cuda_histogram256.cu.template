#ifndef CUDA_COMMON_CODE
#define CUDA_COMMON_CODE

//---------------------------------------------
// DEFINITIONS
//---------------------------------------------

#define BIN_COUNT 256
#define MERGE_THREADBLOCK_SIZE 256

typedef unsigned int histogram_t;

// GPU-specific common definitions

#define LOG2_WARP_SIZE 5U
#define WARP_SIZE (1U << LOG2_WARP_SIZE)

//Both map to single instructions on G8x / G9x / G10x
#define UMUL(a, b)      __umul24( (a), (b) )
#define UMAD(a, b, c) ( UMUL((a), (b)) + (c) )

//Warps == subhistograms per threadblock
#define WARP_COUNT 6

//Threadblock size
#define THREADBLOCK_SIZE (WARP_COUNT * WARP_SIZE)

//Shared memory per threadblock
#define THREADBLOCK_MEMORY (WARP_COUNT * BIN_COUNT)

//// Partial histogram
//extern const unsigned int PARTIAL_COUNT;
//
//extern unsigned int *d_PartialHistograms;

static const unsigned int PARTIAL_COUNT = 240;

static unsigned int *d_PartialHistograms;

//---------------------------------------------
// MERGE HISTOGRAM KERNEL
//---------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Merge histogram256() output
// Run one threadblock per bin; each threadblock adds up the same bin counter
// from every partial histogram. Reads are uncoalesced, but mergeHistogram256
// takes only a fraction of total processing time
////////////////////////////////////////////////////////////////////////////////

__global__ void mergeHistogram256Kernel(
    unsigned int *d_Histogram,
    unsigned int *d_PartialHistograms,
    unsigned int histogramCount
);

__global__ void mergeHistogram256Kernel(
    unsigned int *d_Histogram,
    unsigned int *d_PartialHistograms,
    unsigned int histogramCount
)
{
    unsigned int sum = 0;
    for(unsigned int i = threadIdx.x; i < histogramCount; i += MERGE_THREADBLOCK_SIZE)
    {
        sum += d_PartialHistograms[blockIdx.x + i * BIN_COUNT];
    }

    __shared__ unsigned int data[MERGE_THREADBLOCK_SIZE];
    data[threadIdx.x] = sum;

    for(unsigned int stride = MERGE_THREADBLOCK_SIZE / 2; stride > 0; stride >>= 1)
    {
        __syncthreads();
        if(threadIdx.x < stride)
            data[threadIdx.x] += data[threadIdx.x + stride];
    }

    if(threadIdx.x == 0)
    {
        d_Histogram[blockIdx.x] = data[0];
    }
}

//---------------------------------------------
// INITIALIZE AND CLOSE
//---------------------------------------------



extern "C" void initHistogram256(void);

void initHistogram256(void)
{
    cudaMalloc((void **)&d_PartialHistograms, PARTIAL_COUNT * BIN_COUNT * sizeof(unsigned int));
}

extern "C" void closeHistogram256(void);

void closeHistogram256(void)
{
    cudaFree(d_PartialHistograms);
}

#endif

//---------------------------------------------
// INCREMENT BIN
//---------------------------------------------

#include <cuda.h> 
#include "cuda_histogram256.h"

inline __device__ void add_CUDA_TYPE_NAME(histogram_t *s_WarpHist, CUDA_TYPE *data, double a, double f)
{
	register unsigned int idx = (int) ((*data - a) * f);

	if (idx < BIN_COUNT)
    {
		atomicAdd(s_WarpHist + idx, 1);
	}
    else if (idx == BIN_COUNT)
    {
		atomicAdd(s_WarpHist + idx - 1, 1);
	}
}

//---------------------------------------------
// PARTIAL HISTOGRAM
//---------------------------------------------

__global__ void histogram256Kernel_CUDA_TYPE_NAME(unsigned int *d_PartialHistograms, CUDA_TYPE *d_Data, unsigned int N, double a, double b)
{
    //Per-warp subhistogram storage
    __shared__ histogram_t s_Hist[THREADBLOCK_MEMORY];
    histogram_t *s_WarpHist= s_Hist + (threadIdx.x >> LOG2_WARP_SIZE) * BIN_COUNT;

	// Compute bin size and scaling factor
	double d = (b - a) / (256 - 1);
	double f = 256 / ((b - a) + d);

    //Clear shared memory storage for current threadblock before processing
    #pragma unroll
    for(unsigned int i = 0; i < (THREADBLOCK_MEMORY / THREADBLOCK_SIZE); i++)
	{
       s_Hist[threadIdx.x + i * THREADBLOCK_SIZE] = 0;
	}
    __syncthreads();


    //Cycle through the entire data set, update subhistograms for each warp
    for(unsigned int pos = UMAD(blockIdx.x, blockDim.x, threadIdx.x); pos < N; pos += UMUL(blockDim.x, gridDim.x))
    {
        add_CUDA_TYPE_NAME(s_WarpHist, (CUDA_TYPE *) d_Data + pos, a, f);
    }
    __syncthreads();


    //Merge per-warp histograms into per-block and write to global memory
    for(unsigned int bin = threadIdx.x; bin < BIN_COUNT; bin += THREADBLOCK_SIZE)
    {
        unsigned int sum = 0;

        for(unsigned int i = 0; i < WARP_COUNT; i++)
        {
            sum += s_Hist[bin + i * BIN_COUNT];
        }
        d_PartialHistograms[blockIdx.x * BIN_COUNT + bin] = sum;
    }
}

//---------------------------------------------
// FULL HISTOGRAM
//---------------------------------------------

extern "C" void histogram256_CUDA_TYPE_NAME(unsigned int *d_Histogram, CUDA_TYPE *d_Data, unsigned int dataCount, double lo, double hi);

void histogram256_CUDA_TYPE_NAME(unsigned int *d_Histogram, CUDA_TYPE *d_Data, unsigned int dataCount, double lo, double hi)
{
    //--
	// compute partial histograms
	//--
	
	histogram256Kernel_CUDA_TYPE_NAME<<<PARTIAL_COUNT, THREADBLOCK_SIZE>>>(
        d_PartialHistograms,
        d_Data,
        dataCount, lo, hi
    );
	
	//--
	// merge partial histograms, this uses a common kernel
	//--
	
    mergeHistogram256Kernel<<<BIN_COUNT, MERGE_THREADBLOCK_SIZE>>>(
        d_Histogram,
        d_PartialHistograms,
        PARTIAL_COUNT
    );
}

