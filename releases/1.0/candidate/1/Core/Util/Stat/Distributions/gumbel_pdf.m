function p = gumbel_pdf(x,a,b)

% gumbel_pdf - gumbel extreme value distribution
% ----------------------------------------------
% 
% p = gumbel_pdf(x,a,b)
%
% Input:
% ------
%  x - points to evaluate
%  a - location
%  b - scale
%
% Output:
% -------
%  p - gumbel distribution

%--
% compute shape of probability density function of gumbel
%--

z = (-x + a) ./ b;
p = (1 / b) .* exp(z) .* exp(-exp(z));