function y = gauss_pdf(x, p)

% gauss_pdf - compute generalized gaussian pdf
% ---------------------------------------------
%
% y = gauss_pdf(x, p)
%
% Input:
% ------
%  x - points to evaluate
%  p - generalized gaussian parameters [mean, deviation, shape]
%
% Output:
% -------
%  y - pdf values

y = gauss_pdf_(x, p(3), p(2), p(1));
