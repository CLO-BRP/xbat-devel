function [a, b] = fast_min_max(X, Z)

% fast_min_max - fast min and max value computation
% -------------------------------------------------
%
% [a, b] = fast_min_max(X, Z)
%      b = fast_min_max(X, Z)
%
% Input:
% ------
%  X - input data
%  Z - select indicator (def: [], consider all data)
%
% Output:
% -------
%  a - min value or min and max values
%  b - max value

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set mask
%--

if nargin < 2
	Z = [];
end

%--
% apply mask if needed
%--

if ~isempty(Z)
	X = X(logical(Z));
end

%-----------------------
% COMPUTE MIN AND MAX
%-----------------------

if ~numel(X)
	
	% NOTE: clearly if there are no elements the limits are not defined

	a = [nan, nan];
	
elseif islogical(X)
	
	% NOTE: handle logical as a special case, simply find one one
	
	ix = find(X, 1);
	
	if isempty(find(X, 1))
		a = [0, 0];
	else
		a = [0, 1];
	end
		
else
	
	% NOTE: we just compute min and max jointly for efficiency
	
	a = fast_min_max_(double(X));
	
end

%--
% separate min and max if needed
%--

if (nargout == 2)
	b = a(2); a = a(1);
end
		
