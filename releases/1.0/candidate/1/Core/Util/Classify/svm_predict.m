function [value, info] = svm_predict(data, model, varargin)

% svm_predict - use SVM model to predict values given data
% --------------------------------------------------------
%
% [value, info] = svm_predict(data, model, arg1, arg2, ... )
%
% Input:
% ------
%  data - to use
%  model - trained
%  arg1, arg2, ... - other arguments used by predict (depends on helper)
%
% Output:
% -------
%  value - predictions
%  info - suplemental data

if isa(model.predict, 'function_handle')

	[value, info] = model.predict(data, model, varargin{:});
else	
	error('Missing ''predict'' helper.');
end