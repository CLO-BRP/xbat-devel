function test = load_test_data

load heart_scale.mat;

test.data = heart_scale_inst; test.value = heart_scale_label;

% 
% % Split Data
% train_data = heart_scale_inst(1:150,:);
% train_label = heart_scale_label(1:150,:);
% test_data = heart_scale_inst(151:270,:);
% test_label = heart_scale_label(151:270,:);