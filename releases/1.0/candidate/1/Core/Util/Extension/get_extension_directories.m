function child = get_extension_directories

% get_extension_directories - names of extension children directories
% -------------------------------------------------------------------
%
% names = get_extension_directories
% 
% Output:
% -------
%  names - children directory names

child = { ...
	'Docs', ...
	'Helpers', ...
	'Presets', ...
	'private' ...
};