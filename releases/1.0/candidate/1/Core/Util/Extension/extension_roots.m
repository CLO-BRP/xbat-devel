function [roots, names] = extension_roots(type, project)

% extension_roots - get root directories for extensions
% -----------------------------------------------------
%
% [roots, names] = extension_roots(type, project)
%
% Input:
% ------
%  type - of extensions
%  project - name
%
% Output:
% -------
%  roots - of extensions
%  names - of extensions

%--------------------
% HANDLE INPUT
%--------------------

%--
% set empty defaults for 'type' and 'project'
%--

if nargin < 2 || isempty(project)
	project = '';
end

if ~nargin || isempty(type)
	type = '';
end

%--------------------
% GET ROOTS
%--------------------

% TODO: there is currently a discrepancy of 1 between this and the extensions in cache

%--
% get representative directories under the extensions root
%--

roots = scan_dir(extensions_root(type, project));

% NOTE: we filter to get representatives of extension directories

roots = roots(string_ends(roots, [filesep, 'private']));

if isempty(roots)
	names = {}; return;
end

roots = roots(~string_contains(roots, [filesep, 'Helpers', filesep]));

if isempty(roots)
	names = {}; return;
end

% NOTE: we get the extension roots and possibly the names

roots = iterate(@fileparts, roots);

if nargout > 1
	[ignore, names] = iterate(@fileparts, roots);
end

%--
% filter results based on type
%--

if isempty(type)
	return;
end

select = string_contains(roots, type_to_dir(type));

roots = roots(select); 

if nargout > 1
	names = names(select);
end