function ext = get_sound_format_ext(varargin)

ext = get_extensions('sound_format', 'name', varargin{:});
