function ext = get_signal_filter_ext(varargin)

ext = get_extensions('signal_filter', 'name', varargin{:});
