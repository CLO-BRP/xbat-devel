function ext = get_sound_annotation_ext(varargin)

ext = get_extensions('sound_annotation', 'name', varargin{:});
