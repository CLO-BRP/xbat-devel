function ext = get_extension_type_ext(varargin)

ext = get_extensions('extension_type', 'name', varargin{:});
