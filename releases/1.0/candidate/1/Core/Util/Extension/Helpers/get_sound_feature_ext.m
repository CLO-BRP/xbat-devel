function ext = get_sound_feature_ext(varargin)

ext = get_extensions('sound_feature', 'name', varargin{:});
