function ext = get_log_annotation_ext(varargin)

ext = get_extensions('log_annotation', 'name', varargin{:});
