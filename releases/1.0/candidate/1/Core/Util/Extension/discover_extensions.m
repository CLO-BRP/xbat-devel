function exts = discover_extensions(type, name, project)

% discover_extensions - discover extensions from filesystem
% ---------------------------------------------------------
%
% exts = discover_extensions(type, name)
%
% Input:
% ------
%  type - type of extensions to discover
%  name - names of extensions to discover
%
% Output:
% -------
%  exts - discovered extensions

%--
% handle input
%--

if nargin < 3
	project = '';
end

if nargin < 2 || isempty(name) 
	name = {};
end

if ischar(name)
	name = {name};
end

if ~iscellstr(name)
	error('Extension names must be a string or string cell array.');
end

if ~nargin || isempty(type)
	type = get_extension_types;
end 

if ischar(type)
	type = {type};
end

%--
% discover extensions of requested types
%--

exts = [];

if isempty(project)
	
	for k = 1:length(type)
		exts = [exts, discover_type_extensions(type{k}, name)];
	end

end

%--
% discover project extensions
%--

if isempty(project)
	project = all_projects;
else
	project = get_project(project);
end

for j = 1:numel(project)
	
	for k = 1:length(type)
		
		current = discover_type_extensions(type{k}, name, project(j));
		
		if isempty(current)
			continue;
		end
		
		for l = 1:numel(current)
			current(l).project = project(j);
		end
		
		exts = [exts, current];
		
	end
	
end


%--------------------------------
% DISCOVER_TYPE_EXTENSIONS
%--------------------------------

function ext = discover_type_extensions(type, name, project)

% persistent CACHED_EXTENSION; 
% 
% if isempty(CACHED_EXTENSION)
% 	CACHED_EXTENSION = load_extensions_cache;
% end

%--------------------------------
% HANDLE INPUT
%--------------------------------

ext = [];

%--
% set no project default, this means consider the core extensions
%--

if nargin < 3
	project = [];
end

%--
% get extensions root and check it is in path
%--

type_dir = extensions_root(type, project);

% NOTE: we return if the project does not contain type directory

if ~isempty(project) && ~exist(type_dir, 'dir')
	return;
end

% NOTE: we return if the type directory is empty

if isempty(what(type_dir))
	return;
end

%--
% get extension root directories
%--

root = get_field(what_ext(type_dir), 'dir');
	
% NOTE: remove known non-extension directories 

for k = length(root):-1:1
	
	if ( ...
		strcmp(root{k}, 'CVS') || ...
		strcmp(root{k}, 'private') || ...
		strcmp(root{k}, '__IGNORE') || ...
		strcmp(root{k}, '.svn') ...
	)
		root(k) = [];
	end
	
end

% NOTE: return if there are no extension directories

if isempty(root)
	ext = []; return;
end

%--------------------------------
% DISCOVER EXTENSIONS
%--------------------------------

%--
% discover only named extensions
%--

if ~isempty(name)
	root = intersect(root, name);
end

%--
% keep current directory
%--

curr_dir = pwd;

%--
% loop over extension directories
%--

splash = get_splash;

for k = 1:length(root)

% 	temp = [type_dir, filesep, root{k}]; 
% 	
% 	info = dir(temp); {info.name}
% 	
% 	disp(temp);
% 	disp(['  ', info(1).date]);

	if strcmp(root{k}, 'DOCS')
		continue;
	end
	
	%--
	% get extension file
	%--

	% NOTE: we are looking for a single M or then P code file in the extension directory

	files = what_ext([type_dir, filesep, root{k}], 'm', 'p');

	main = get_field(files, 'm');

	if isempty(main)
		main = get_field(files, 'p');
	end

	% NOTE: this is where we enforce the single file condition

	if length(main) > 1

		disp(' ');
		disp( ...
			['WARNING: Extension directory ''', root{k}, ''' must contain a single M-file or P-file.'] ...
		);

		continue;

	end

	%--
	% try to create extension structure
	%--

	% TODO: improve this exception handler block to provide an informative display

	try

		splash_wait_update(splash, ['loading ''', root{k}, ''' ', strrep(type, '_', ' '), ' ...']);
		
		cd([type_dir, filesep, root{k}]); 
		
		fun = file_ext(main{1});
		
		extension = feval(fun);

	catch
		
		str = ['<a href="matlab:show_file(''', [type_dir, filesep, root{k}], ''');">Show</a>'];
		
		nice_catch(lasterror, ['WARNING: Failed to get extension from ''', root{k}, '''. ', str]); continue;

	end

	%--
	% update extension registry array
	%--

	if isempty(ext)
		ext = extension;
	else
		ext(end + 1) = extension;
	end

end

if isempty(ext)
	return;
end

%--
% sort by name if needed
%--

if length(ext) > 1
	[ignore, ix] = sort(struct_field(ext, 'name')); ext = ext(ix);
end

%--
% return to current directory
%--

cd(curr_dir);
