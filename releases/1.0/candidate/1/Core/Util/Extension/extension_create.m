function ext = extension_create(fun)

% extension_create - create extension
% -----------------------------------
% 
% ext = extension_create(fun)
%
%
% Input:
% ------
%  fun - extension function
%
% Output:
% -------
%  ext - extension

% TODO: update to require non-empty 'fun' input and simplify setup section of function

%------------------------
% HANDLE INPUT
%------------------------

%--
% set empty extension function
%--

% NOTE: 'get_caller' will try to discover this

if nargin < 1
	fun = '';
end

%------------------------
% SETUP
%------------------------

%--
% check whether input is proposed extension or extension type
%--

if isempty(fun) || ~is_extension_type(fun)
	caller = get_caller(fun);
else
	caller = [];
end 

%--
% get basic extension info
%--

if ~isempty(caller)
	
	% NOTE: 'get' functions are private because of the fragility of stack use
	
	type = get_type(caller);
	
	name = get_name(caller);
	
% 	info = get_info(caller); modified = info.date;
	
	info = dir2(fileparts(caller.file)); 
	
	% NOTE: in this case, loaded will always exceed modified
	
	modified = info.datenum; loaded = now;
	
	main = get_main(caller);

else
	
	% NOTE: this allows testing from command line
	
	type = fun; 
	
	name = '';
	
	modified = now; loaded = now;
	
	main = [];
	
end

%--
% check extension type
%--

% NOTE: return empty for unrecognized type

if ~is_extension_type(type)
	ext = [];  return;
end

%------------------------
% CREATE EXTENSION
%------------------------

%--
% ADMIN AUTO
%--

% NOTE: the type field went unused, so we should now use it, this has not been tested extensively

% TODO: consider removing subtype altogether

ext.type = type;

ext.subtype = type;			% actual type of extension (available types above)

ext.name = name; 			% name of extension

ext.guid = generate_uuid;

ext.fieldname = extension_fieldname(name);

% ext.classname = extension_classname(name);

% TODO: these fields should contain, the modification date for the parent directory and last time loaded

ext.modified = modified; 	% modification date of current version

ext.loaded = loaded;

ext.parent = [];

%--
% ADMIN USER
%--

ext.version = '';			% version string for extension

ext.short_description = '';	% short description of extension

ext.category = {};			% extension category (used in the production of menus)

ext.project = '';

ext.author = '';			% author of extension

ext.email = '';

ext.url = '';

ext.links = {};

ext.enable = 1;				% enable extension in interface

% TODO: develop allowed modes, perhaps 'user', 'developer', 'simple', 'advanced'

ext.mode = 'simple';

%------------------------
% FUNCTION HANDLES
%------------------------

% NOTE: we have the type, but it could be inferred if we have a main

ext.fun = attach_fun(main, type);

ext.fun.main = main;

%------------------------
% STORES
%------------------------

%--
% parameters store
%--

% NOTE: we keep parameter values when a palette is closed

% NOTE: we are creating two plausible stores for 'compute' parameters

ext.parameter = struct; 

ext.parameters = struct;

types = get_preset_types(ext);

for k = 1:length(types)
	
	ext.parameters.(types{k}) = struct;

end

%--
% full palette controls store
%--

% NOTE: we keep control values when a palette is closed

ext.control = struct; 

%--
% extension value description
%--

% NOTE: calling 'value__create' with no input should produce a description

% TODO: consider similar behavior for 'parameter__create' functions ?

try
	ext.value = ext.fun.value.create();
catch
	ext.value = struct;
end

%--------------------------------
% SYSTEM 
%--------------------------------

ext.accelerator = '';

ext.state = struct; % NOTE: this is not currently used

ext.debug = 0;

ext.multichannel = 0;

ext.fade = 1;

%--
% sound format extension fields
%--

ext.ext = {};

ext.seek = 1;

ext.compression = 0;

%--
% requests
%--

% TODO: design a variety of requests and their corresponding handlers

% ext.requests.event.page = 0;

%--
% required 
%--

% NOTE: declare required sound attributes	

ext.required.sound = []; 

% NOTE: retrieve availability information using 'ver' function 

ext.required.toolbox = cell(0);

%--
% behaviors
%--

% NOTE: these are not thoroughly implemented

% NOTE: extension can be made active

if findstr(type, 'action')
	ext.active = 0;
else
	ext.active = 1; 
end

% NOTE: the goal here is to indicate to the action dispatcher whether to get samples or not

% TODO: this field can be used by various dispatchers not just action

ext.uses.samples = 1;


% NOTE: extension can be paused an restarted

ext.restart = 0; 	

%--
% other
%--

% NOTE: these are type dependent at the moment

ext.estimate = 1; % NOTE: indicate whether filter output is a signal estimate

ext.waitbar = 1; % NOTE: use waitbar for array actions

%--
% interface configuration
%--

% NOTE: it is possible that we can use typical palette extensions in dialog mode

ext.palette.opt = control_group;

ext.dialog.opt = dialog_group;

%--------------------------------
% USERDATA FIELDS
%--------------------------------

ext.userdata = [];
