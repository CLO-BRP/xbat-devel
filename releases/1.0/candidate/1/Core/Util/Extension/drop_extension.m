function [status, result] = drop_extension(store, ext)

% drop_extension - drop extension tables from log store
% -----------------------------------------------------
%
% [status, result] = drop_extension(store, ext)
%
% Input:
% ------
%  store - database
%  ext - extension
%
% Output:
% -------
%  status - of transaction
%  result - of transaction

% TODO: extend to drop all extension tables

%--
% handle multiple extensions recursively
%--

% NOTE: this approach leads to one transaction per extension

if numel(ext) > 1
	[status, result] = iterate(mfilename, ext, 0); return;
end

%--
% get extension table names
%--

table = extension_table_names(ext);

%--
% build drop query
%--

lines = { ...
	'BEGIN;', ...
	['  DROP IF EXISTS ', table.value, ';'], ...
	['  DROP IF EXISTS ', table.parameter, ';'], ...
	'COMMIT;' ...
};

%--
% drop tables from store
%--

sql = sql_string(lines);

if isempty(store)
	
	if nargout
		status = sql; result = lines;
	else
		disp(' '); sql_display(lines); disp(' ');
	end
	
else 
	[status, result] = query(store, sql);
end


