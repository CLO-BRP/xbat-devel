function dispatch_extension_request(request, ext, context)

% NOTE: extensions place request to their type handlers

% db_disp; request, ext, context

%--
% get and check type handler extension
%--

type = get_extensions('extension_type', 'name', title_caps(ext.type));

if isempty(type)
	error('Unable to find extension type handler.'); 
end

% NOTE: for now we display this message, eventually we throw an error

if isempty(type.fun.dispatch)
	db_disp('Type does not have a dispatcher.'); return; % error('Type does not have a dispatcher.');
end

%--
% get dispatcher parameters
%--

% NOTE: we reuse the context, it should not have changed. we only declare ourselves as the context extension

context2 = context; context2.ext = type;

parameter = struct;

if ~isempty(type.fun.parameter.create)
	
	try
		parameter = type.fun.parameter.create(context2);
	catch
		extension_warning(type, 'Parameter creation failed.', lasterror);
	end
	
end

if ~isempty(type.fun.parameter.compile)
	
	try
		parameter = type.fun.parameter.compile(parameter, context2);
	catch
		extension_warning(type, 'Parameter compile failed.', lasterror);
	end
	
end

%--
% dispatch request
%--

try
	[result, context] = type.fun.dispatch(request, parameter, context);
catch
	extension_warning(type, 'Dispatch failed.', lasterror);
end