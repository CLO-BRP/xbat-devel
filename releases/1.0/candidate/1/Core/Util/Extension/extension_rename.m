function ext = extension_rename(ext, name, force)

% extension_rename - rename extension
% -----------------------------------
%
% ext = extension_rename(ext, name)
%
% Input:
% ------
%  ext - extension
%  name - name
%
% Output:
% -------
%  ext - renamed extension

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% consider name input
%--

if nargin < 3
	force = 0;
end

% NOTE: return quickly if we cannot rename, throw an error for an improper name

if nargin < 2 || strcmp(ext.name, name)
	return;
end

if ~proper_filename(name)
	error('Extension name must be proper filename.');
end

%--
% check for children
%--

child = get_extension_children(ext);

% NOTE: this is not really an error, rather a dangerous operation that we may force

if ~force && ~isempty(child)
	error('Rename is not supported for extensions with children, however you can force this operation.');
end

%-----------------------
% RENAME
%-----------------------

%--
% get current root and main name, compute new root, and get subversion status
%--

disp(' ');

root = extension_root(ext); disp(root);

[ignore, current, ignore] = fileparts(get_extension_main(ext));

db_disp; current

new = [fileparts(root), filesep, name]; disp(new);

in_svn = is_working_copy(root);

%--
% rename directory considering repository status
%--

try
	
	%--
	% remove old directories from path
	%--
	
	% NOTE: removing previous directories from path allows deletion
	
	warning('off', 'MATLAB:rmpath:DirNotFound');
	
	dirs = scan_dir(root); rmpath(dirs{:});
	
	warning('on', 'MATLAB:rmpath:DirNotFound');
	
	%--
	% rename directory
	%--
	
	% NOTE: both of these operations are move, not copy operations
	
	if in_svn
		svn('rename', '--force', root, new);
	else
		movefile(root, new);
	end
	
	%--
	% add new directories to path
	%--
	
	% NOTE: in fact we do a little bit more
	
	refresh_extensions_path(ext.subtype);
	
catch
	
	nice_catch(lasterror, ...
		['Extension rename from ''', ext.name, ''' to ''', name, ''' failed.'] ...
	);

end

%--
% discover renamed extension and add to cache
%--

ext = discover_extensions(ext.subtype, name);

if ~isempty(ext)
	extensions_cache(ext);
end

%--
% regenerate main to normalize main
%--

% NOTE: the second input requests a normal name

[main, old] = regenerate_main(ext, 1);

if in_svn && ~strcmp(main, old)
	
	% NOTE: 'regenerate_main' deletes the old main, this removes it from the repository. we also add the new main 
	
	svn('delete', old); svn('add', main);
	
end

%--
% re-discover renamed extension and add to cache
%--

ext = discover_extensions(ext.subtype, name);

if ~isempty(ext)
	extensions_cache(ext);
end

%--
% adopt children under new name
%--

for k = 1:numel(child)
	
	% NOTE: the zero says we are not pretending
	
	extension_adopt(child, ext, 0, current);
	
end

% TODO: update browser extension stores effectively

% NOTE: this should be simple once 'update_extension_store' is properly implemented







