function check_required_extensions(ext)

% check_required_extensions - simple check for required extensions
% ----------------------------------------------------------------
%
% req = check_required_extensions(ext)
%
% Input:
% ------
%  ext - extension we want to check required extensions for

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3213 $
% $Date: 2006-01-18 17:53:34 -0500 (Wed, 18 Jan 2006) $
%--------------------------------

% NOTE: at the moment this function is a simple sanity check

% TODO: add toolbox dependencies

% TODO: move required sound metadata to context ??

%--
% check for existence of required extension directories
%--

for k = 1:length(ext.required)
	
	%--
	% parse required string
	%--
	
	[type,name] = strtok(ext.required{k},'::'); name = name(3:end);
		
	%--
	% check for extension directory
	%--
	
	% NOTE: this requires the name to match the extension directory
		
	tmp_info = what(['Extensions', filesep, type_to_dir(type), filesep, name]);
	
	tmp = tmp_info.path;
		
	if (~exist(tmp,'dir'))
		
		% NOTE: eventually check for, download, and install required extensions
		
		disp(' ')
		error(['Extension ''' ext.name ''' requires extension''' ext.required{k} '''.']);
		
	end
	
end