function main_name = get_main_name(name, type, simple)

% get_main_name - compute main function name from extension name
% --------------------------------------------------------------
%
% main_name = get_main_name(name, type, simple)
%
% Input:
% ------
%  name - extension name
%  type - extension type
%  simple - request
%
% Output:
% -------
%  main_name - main function name

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set no namespace clash protection default
%--

if nargin < 3
	simple = 0;
end 

%--
% handle extension input
%--

if (nargin < 2) && isstruct(name)
	ext = name; name = ext.name; type = ext.subtype;
end

%---------------------------
% GET NORMALIZED NAME
%---------------------------

%--
% lower and strip punctuation
%--

name = lower(strip_punctuation(name)); type = lower(type);

%--
% get simple name
%--

% NOTE: we are safe, but we hope a clash will not happen

simple_name = [name, '_', lower(type)];

main_name = simple_name; 

if simple
	return;
end

%--
% consider name clashes
%--

k = 2;

while ~isempty(which(main_name))	
	main_name = [name, '_', int_to_str(k, 10, '0'), '_', type];
end

% TODO: generate a warning if a clash happens

if ~strcmp(simple_name, main_name)
	
end
