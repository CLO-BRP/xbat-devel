function handles = action_menu(par, type)

% action_menu - attach actions to actions menu
% --------------------------------------------
%
% handles = action_menu(par, type)
%
% Input:
% ------
%  par - action menus parent 
%  type - action type
%
% Output:
% -------
%  handles - handles

%------------------
% HANDLE INPUT
%------------------

if ~is_action_type(type)
	error('Unrecognized action type.');
end

%------------------
% CREATE MENU
%------------------

%--
% prepare parent by clearing children and callback
%--

delete(allchild(par)); set(par, 'callback', []);

%--
% get actions
%--

ext = get_extensions([type, '_action']);

% NOTE: return early creating informative menu

handles = [];

if isempty(ext)
	handles(end + 1) = uimenu(par, 'label', '(No Actions Found)', 'enable', 'off'); return;
end

%--
% create action menus
%--

projects = [ext.project];

if isempty(projects)
	
	% TODO: create categorical action menus, perhaps further organization

	head = uimenu(par, 'label', '(Core)', 'enable', 'off');
	
	for k = 1:length(ext)

		% NOTE: add menus to control framework, tag information helps for now

		handles(end + 1) = uimenu(par, ...
			'label', [ext(k).name, ' ...'], ...
			'tag', ext(k).name, ...
			'callback', {@action_dispatch, type} ...
		);

	end

else
	
	% NOTE: we get unique projects to present interface
	
	[ignore, index] = unique({projects.name}); projects = projects(index);
	
	%--
	% project actions
	%--
	
	head = uimenu(par, 'label', '(Projects)', 'enable', 'off');
	
	for j = 1:numel(projects)
		
		project = projects(j); 
		
		part = empty(ext);
		
		for k = 1:numel(ext)
			
			% FIXME: currently core extensions have '' as project
			
			if isstruct(ext(k).project) && strcmp(ext(k).project.name, project.name)
				part(end + 1) = ext(k);  %#ok<AGROW>
			end 
			
		end

		if isempty(part)
			
			% NOTE: using continue means that projects without actions do not appear in menu
			
% 			continue;
			
			child = uimenu(par, 'label', project.name);
			
			handles(end + 1) = uimenu(child, 'label', '(No Actions Found)', 'enable', 'off');
			
		else
			
			child = uimenu(par, 'label', project.name);
			
			for k = 1:length(part)

				% NOTE: add menus to control framework, tag information helps for now

				handles(end + 1) = uimenu(child, ...
					'label', [part(k).name, ' ...'], ...
					'tag', part(k).name, ...
					'callback', {@action_dispatch, type} ...
				);

			end
			
		end

	end
	
	%--
	% core actions
	%--
	
	part = ext(iterate(@isempty, {ext.project}));
	
	head = uimenu(par, 'label', '(Core)', 'enable', 'off', 'separator', 'on');
	
	if isempty(part)
		
		handles(end + 1) = uimenu(par, 'label', '(No Actions Found)', 'enable', 'off');
		
	else
		
		for k = 1:length(part)

			% NOTE: add menus to control framework, tag information helps for now

			handles(end + 1) = uimenu(par, ...
				'label', [part(k).name, ' ...'], ...
				'tag', part(k).name, ...
				'callback', {@action_dispatch, type} ...
			);

		end

	end

end




