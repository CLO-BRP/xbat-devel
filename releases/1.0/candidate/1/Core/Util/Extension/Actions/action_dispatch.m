function result = action_dispatch(obj, eventdata, type, varargin)

% action_dispatch - dispatch action extension
% -------------------------------------------
%
% result = action_dispatch(obj, eventdata, type)
%
% Input:
% ------
%   obj - callback object
%   eventdata - callback event data
%   type - action type
%
% Output:
% -------
%  result - results of action

% NOTE: consider not updating the results section during prepare and conclude

% NOTE: the passing of type around is quirky and should be considered

%-------------------------------
% HANDLE INPUT
%-------------------------------

%--
% check for valid type input
%--

% NOTE: sometimes 'type' can be inferred at considerable code cost, error is better

if (nargin < 3) || isempty(type)	
	error('Action type is required.');
end

if ~is_action_type(type)
	error('Unrecognized action type.');
end

%-------------------------------
% SETUP
%-------------------------------

%--
% get action ingredients from callback context or input
%--

if isstruct(obj)
	
	ext = obj.ext; context = obj.context; targets = obj.target;
	
	callback.par.handle = obj.par;
	
else

	%--
	% get callback context
	%--

	% NOTE: dispatch starts in a menu, the menu is not 'control' but this works

	% NOTE: the name comes from the 'tag' not the 'label'

	callback = get_callback_context(obj, eventdata);

	%--
	% get action extension
	%--

	% NOTE: the action 'type' is stored in the callback, the menu label is the action 'name'

	% NOTE: when the parent is the root browser, we get the extension from the cache

	[ext, ix, context] = get_browser_extension([type, '_action'], callback.par.handle, callback.control.name);

	if isempty(ext)
		return;
	end

	%--
	% get action targets
	%--

	targets = get_action_target(type, callback);

	if isempty(targets)
		return;
	end

end

%--
% append action context
%--

% NOTE: extensions could declare context fields required and context could be compiled

context.type = type;

context.target = targets;

% NOTE: this is a convenient alias for the targets, to bridge specific and general action code

switch type
	case 'log'
		context.log = iterate(@get_log_from_name, targets);
		
	otherwise
		context.(type) = targets;
end

context.library = get_active_library;

context.callback = callback;

%-------------------------------
% DISPATCH ACTION
%-------------------------------

%--
% edit parameters through dialog if needed
%--

% NOTE: the developer test makes sure we can always access the developer menus

if ~isempty(ext.fun.parameter.control.create) % || xbat_developer
	
	%--
	% present dialog
	%--
	
	out = action_dialog(targets, ext, ext.parameter, context);

	if ~strcmp(out.action, 'ok')
		return;
	end
	
	%--
	% update and compile parameters if needed
	%--
	
	if ~trivial(out.values)
		
		% TODO: there is a lack of DRY in this code, two extension copies
		
		ext.parameter = struct_update(ext.parameter, out.values);
		
		ext.control = out.values;
		
		context.ext = ext;
		
		if ~isempty(ext.fun.parameter.compile)
			
			try
				[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
			catch 
				extension_warning(ext, 'Parameter compilation failed.', lasterror);
			end
			
		end
		
		% NOTE: makes sure context extension is not stale, the system does not guarantee this
		
		context.ext = ext;
		
	end
	
	%--
	% store compiled action parameters
	%--
	
	set_browser_extension(callback.par.handle, ext);
	
end

%--------------------
% START
%--------------------

%--
% create waitbar
%--

% NOTE: the default extension behavior is to have a waitbar

if ext.waitbar
	
	pal = action_waitbar(context); 
	
	% NOTE: the key thing here is the number of operations
	
	progress.sequence = linspace(0, 1, ...
		length(targets) - 1 + ~isempty(ext.fun.prepare) + ~isempty(ext.fun.conclude) ...
	);
	
	progress.sequence(end + 1) = 1;
	
	progress.index = 1;
	
end

%--
% start time of action execution
%--

started = datestr(now); start = clock;

%--------------------
% PREPARE
%--------------------

if isempty(ext.fun.prepare)
	
	prepare = empty(action_result);
	
else
	
	%--
	% indicate we are preparing
	%--
	
	waitbar_update(pal, 'PROGRESS', 'message', ['Preparing ...']);

	%--
	% try to prepare
	%--
	
	try
		[output, context] = ext.fun.prepare(ext.parameter, context); status = 'done';
	catch
		output = lasterror; status = 'failed'; extension_warning(ext, 'Prepare failed.', output);
	end
	
	prepare = action_result(status, 'prepare', output, '', start);
	
	%--
	% update with result of prepare
	%--
	
	action_waitbar_update(pal, progress.sequence(progress.index), [], type); 
	
	progress.index = progress.index + 1;
	
	% NOTE: returning orphans the waitbar, so we can see that prepare failed
	
	if strcmp(prepare.status, 'failed');
		return;
	end
	
end

%--------------------
% PERFORM ACTION
%--------------------

%--
% get actual targets from target names
%--

switch type
	
	case 'event'
		
		% NOTE: get required state for event action
		
		par = callback.par; data = get_browser(par.handle);
		
% 		[targets, context.log] = get_str_event(par.handle, targets, data);
		
		% NOTE: what follows is the paged version of the previous line
		
		str = targets; targets = [];
		
		start = 1; page = 20; stop = min(numel(str), start + page);
		
		db_disp 'reading events from log is slow'
		
		while start < numel(str)
			
			start
			
			[part1, part2] = get_str_event(par.handle, str(start:stop), data);
		
			if isempty(targets)
				targets = part1; logs = part2;
			else
				targets = [targets; part1]; logs = [logs; part2];
			end 
			
			start = stop + 1; stop = min(numel(str), start + page);
			
		end
		
	case 'log'
		
		targets = get_target_log(targets, context);

end

%--
% loop over action targets
%--

status = 'done'; 

% NOTE: these are lists of events to discard and update

discard = []; update = [];

for k = 1:length(targets)
	
	%--
	% cancel on closing of waitbar
	%--
	
	% NOTE: this is not working right now
	
	if ~ishandle(pal) 
		status = 'cancel'; break;
	end
	
	%--
	% update waitbar while we get actual target from targets
	%--
	
	switch type
	
		case 'event'
		
			target = targets(k); 
			
			target_name = get_action_target_name(target, type);
		
			% NOTE: the 'get_event_samples' provides system 'samples' and 'unfiltered' samples
			
			if ext.uses.samples
				
				waitbar_update(pal, 'PROGRESS', ...
					'message', ['Reading ', target_name, ' ...'] ...
				);
			
				target = get_event_samples(target, context.sound, context);
				
			end
			
		case 'sound'
			
			target = targets(k);

			target_name = get_action_target_name(target, type);
			
		case 'log'

			target = targets(k);
			
			target_name = log_name(target);
			
	end
	
	%--
	% indicate processing
	%--
	
	waitbar_update(pal, 'PROGRESS', 'message', ['Processing ', target_name, ' ...']);

	%--
	% perform action on target
	%--
	
	[result(k), context] = perform_action(target, ext, ext.parameter, context);
	
	%--
	% update waitbar results
	%--
	
	% TODO: implement interrupt behavior using waitbar closing
	
	if ext.waitbar && ishandle(pal)
		
		action_waitbar_update(pal, progress.sequence(progress.index), result(k), type); 
		
		progress.index = progress.index + 1;
		
	end
	
	%--
	% consider result side-effects
	%--
	
	% NOTE: currently we are interested in updating and deleting events as part of the framework
	
	if strcmp(type, 'event') && isfield(result(k).output, 'event')
		
		if isempty(result(k).output.event)
			discard = append_event_list(discard, context.log, target);
		else
			update = append_event_list(update, context.log, clean_up_event(result(k).output.event));
		end

	end
		
end

%--------------------
% CONSIDER REQUESTS
%--------------------

if ~isempty(discard) || ~isempty(update)
	
	%--
	% delete events
	%--

	% NOTE: this means that delete can be implemented as an action
	
	for k = 1:numel(discard)
		log_delete_events(discard(k).log, [discard(k).event.id]);
	end

	%--
	% update events
	%--
	
	for k = 1:numel(update)
		log_append(update(k).log, update(k).event);
	end
	
	%--
	% update browser display and event palette
	%--

	browser_display(context.par, 'events');

	update_find_events(context.par, []);
	
end

%--------------------
% CONCLUDE
%--------------------

if isempty(ext.fun.conclude)
	
	conclude = empty(action_result);
	
else
	
	%--
	% indicate we are concluding
	%--
	
	waitbar_update(pal, 'PROGRESS', 'message', ['Concluding ...']);

	%--
	% try to conclude
	%--
	
	try
		output = ext.fun.conclude(ext.parameter, context); status = 'done';
	catch	
		output = lasterror; status = 'failed'; extension_warning(ext, 'Conclude failed.', lasterror);	
	end
	
	conclude = action_result(status, 'conclude', output, '', start);
	
	%--
	% update with result of prepare
	%--
	
	action_waitbar_update(pal, progress.sequence(progress.index), [], type); 
	
	progress.index = progress.index + 1;
	
	if strcmp(conclude.status, 'failed');
		set_control(pal, 'close_on_completion', 'value', 0);
	end
	
end

%--------------------
% OUTPUT RESULT
%--------------------

% NOTE: 'perform_action' wraps action instance info, wrap again to add action info

last_action.name = ext.name;

% NOTE: consider using when we implement interrupt behavior

last_action.status = status;

last_action.started = started;

elapsed = sum([result.elapsed]);

last_action.elapsed = elapsed;

last_action.result.prepare = prepare;

last_action.result.compute = result;

last_action.result.conclude = conclude;

% NOTE: a partial content assignment could happen in the loop

assignin('base', 'last_action', last_action);

%-------------------------------------
% FINISH
%-------------------------------------

%--
% update xbat palette if needed
%--

switch ext.subtype
	
	case 'event_action'
		
		% NOTE: this is probably going to update an event palette

	% TODO: we should fix the selection preservation behavior for listboxes
	
	case 'log_action'
		
		xbat_palette('find_logs');
		
	case 'sound_action'
		
		xbat_palette('find_sounds');

end

%--
% indicate completion in waitbar
%--

if ext.waitbar && ishandle(pal)
	
	%--
	% close waitbar if needed
	%--
	
	if get_control(pal, 'close_after_completion', 'value')
		close(pal); return;
	end
		
	%--
	% present completion message
	%--

	if elapsed > 2
		time = sec_to_clock(elapsed);
	else 
		time = [num2str(elapsed, 4), ' sec'];
	end

	message = [integer_unit_string(numel(targets), type), ' processed in ', time];

	waitbar_update(pal, 'PROGRESS', ...
		'message', message ...
	);
	
end


%--------------------------------------
% APPEND_EVENT_LIST
%--------------------------------------

function list = append_event_list(list, log, event)

%--
% initialize list
%--

if isempty(list)
	list.log = ''; list.event = empty(event_create); list = empty(list);
end

%--
% check whether we have a list for this log and add the event to the list
%--

if isempty(list)
	ix = [];
else
	ix = find(strcmp(log_name(log), log_name([list.log])));
end

if isempty(ix)
	list(end + 1).log = log; list(end).event = event;
else
	list(ix).event(end + 1) = event;
end


%--------------------------------------
% CLEAN_UP_EVENT
%--------------------------------------

% NOTE: this function is also used in 'detector_scan'

function event = clean_up_event(event)

%--
% remove appended fields
%--

valid = fieldnames(event_create); current = fieldnames(event);

remove = setdiff(current, valid);

if ~isempty(remove)
	event = rmfield(event, remove);
end

%--
% clear samples and rate
%--

event.samples = []; event.rate = [];


