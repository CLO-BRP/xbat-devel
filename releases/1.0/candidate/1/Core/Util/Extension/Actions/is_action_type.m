function value = is_action_type(type)

% is_action_type - test if string is action type
% ----------------------------------------------
%
% value = is_action_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - type indicator

%--
% handle input
%--

if ~ischar(type)
	error('Action type must be string.');
end

%--
% check if proposed string is action type
%--

value = string_is_member(type, get_action_types);