function name = get_action_target_name(target, type) 

% get_action_target_name - get name of target
% -------------------------------------------
%
% name = get_action_target_name(target, type)
%
% Input:
% ------
%  target - action target
%  type - action type
%
% Output:
% -------
%  name - target name

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% handle multiple targets recursively
%--

if ~ischar(target) && (numel(target) > 1)
	
	name = iterate(mfilename, target, type); return;
	
end

%------------------------------------------
% COMPUTE TARGET NAME
%------------------------------------------

% NOTE: we compute target name based on type

switch type
	
	% NOTE: we try to pack the parent log name in the event userdata
	 
	case 'event'
		name = event_name(target);
		
	case 'log'
		name = log_name(target);
		
	case 'sound'
		name = sound_name(target);
		
	otherwise
		error('Unrecognized action type.');
		
end