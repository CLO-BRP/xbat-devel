function [control, opt] = get_action_controls(ext, parameter, context)

% get_action_controls - get controls for action dialog
% ----------------------------------------------------
%
% [control, opt] = get_action_controls(ext, parameter, context)
%
% Input:
% ------
%  ext - action extension
%  parameter - extension parameter
%  context - context
%
% Output:
% -------
%  control - action controls
%  opt - action control options

%-------------------
% GET CONTROLS
%-------------------

%--
% create common controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'string', 'Parameters', ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'min', 1 ...
);

%--
% create extension specific controls
%--

specific = empty(control_create);

if ~isempty(ext.fun.parameter.control.create)
	
	try
		specific = ext.fun.parameter.control.create(parameter, context);
	catch
		extension_warning(ext, 'Control creation failed.', lasterror);
	end
	
end

%--
% append extension specific controls
%--

if ~isempty(specific)
	
	control(end) = adjust_control_space(control(end), specific(1));

	for k = 1:length(specific)
		control(end + 1) = specific(k);
	end

end

%-------------------
% GET OPTIONS
%-------------------

% NOTE: return if options were not requested

if nargout < 2
	return;
end

%--
% get default options and add color
%--

opt = dialog_group;

% NOTE: add color according to parent context

if strcmp(ext.subtype, 'event_action')
	header_type = 'sound_browser_palette';
else
	header_type = 'root';
end 

opt.header_color = get_extension_color(header_type);

%--
% update using extension specific options if needed
%--

if ~isempty(ext.fun.parameter.control.options)
	
	try
		opt = struct_update(opt, ext.fun.parameter.control.options(context));
	catch
		extension_warning(ext, 'Control configuration options failed.', lasterror);
	end
	
end

opt.ext = ext;