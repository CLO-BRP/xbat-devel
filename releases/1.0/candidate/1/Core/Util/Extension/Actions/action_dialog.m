function out = action_dialog(target, ext, parameter, context) %#ok<INUSL>

% action_dialog - present action dialog
% -------------------------------------
%
% out = action_dialog(target, ext, parameter, context)
%
% Input:
% ------
%  target - action target array
%  ext - action extension
%  parameter - action parameters
%  context - action context
%
% Output:
% -------
%  out - dialog output

% TODO: consider the 'target' input to this function

%--
% get action extension controls
%--

[control, opt] = get_action_controls(ext, parameter, context);

%--
% present dialog
%--

par = context.callback.par.handle;

out = dialog_group(ext.name, ...
	control, opt, {@callback_router, ext, context}, par ...
);


%----------------------------------------
% CALLBACK_ROUTER
%----------------------------------------

function callback_router(obj, eventdata, ext, context) %#ok<INUSL>

%--
% handle input
%--

% NOTE: return if there is no extension callback

if isempty(ext.fun.parameter.control.callback)
	return;
end

%--
% get callback context and call extension specific callback
%--

callback = get_callback_context(obj, eventdata);

try
	ext.fun.parameter.control.callback(callback, context);
catch
	extension_warning(ext, 'Control callback failed.', lasterror);
end

