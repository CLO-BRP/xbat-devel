function pal_cli(varargin)

% pal_cli - extension development pal for the CLI
% -----------------------------------------------
%
% pal_cli(ext)
%
% pal_cli(type, name)
%
% Input:
% ------
%  ext - extension
%  type - type
%  name - name

% TODO: provide code stats in this context, no palette loading

%--
% handle input
%--

switch length(varargin)
	
	case 1, ext = varargin{1};
		
	% NOTE: this is all we need to get the actual extension from the system
	
	case 2, ext.subtype = varargin{1}; ext.name = varargin{2};
		
end

%--
% get fresh extension and check subversion status
%--

% NOTE: 'discover' outputs the same extension we later 'get'

extensions_cache(discover_extensions(ext.subtype, ext.name));

ext = get_extensions(ext.subtype, 'name', ext.name);

% NOTE: this is a coarse test of revision control status

root = extension_root(ext); in_svn = is_working_copy(root);

% TODO: decide if we add to get missing files if we are under version control

% if in_svn
% 	svn('add', root);
% end

%--
% build header
%--

type = upper(strrep(ext.subtype, '_', ' ')); 

name = upper(ext.name);

description = ext.short_description;

if isempty(description)
	description = '(Short description is not available)';
end

header = [' ', name, ' ', ext.version, ' - ', description, ' (', type, ')'];

% NOTE: this is not very effective consider something a bit more sophisticated

header = strrep(header, '  ', ' ');

%--
% display header
%--

n = max(72, length(header) + 1); sep = str_line(n, '_');

disp(' ');
disp(sep);
disp(' ');
disp(header);
disp(sep);
disp(' ');

%--
% display API section
%--

fun = flatten(ext.fun); field = fieldnames(fun); % n = max(cellfun('prodofsize', field));

disp(' API:');
disp(' ');
disp('   FUNCTIONS:');

for k = 1:length(field)
	
	if isempty(fun.(field{k}));
		continue;
	end
	
	info = functions(fun.(field{k})); file = info.file;
	
	str = ['     ', info.function, '  ', edit_link(file), ' ', show_link(file)];
	
	if in_svn
		str = [str, ' ', diff_link(file)];
	end
	
	disp(str);
	
end

disp(' ');
disp('   ADD:');

count = 0; str = '     ';

for k = 1:length(field)
	
	handle = fun.(field{k});
	
	if ~isempty(handle)
		continue;
	end
	
	count = count + length(field{k}) + 1;
	
	if count > 72
		disp(str); count = 0; str = '     '; continue; 
	end
	
	str = [str, add_link(ext, field{k}, field{k}), ',  '];

	if k == length(field)
		str(end - 1:end) = ''; disp(str);
	end

end

disp(' ');

%--
% display Helpers
%--

disp(' HELPERS:');
disp(' ');

disp('   FUNCTIONS:');

[helper, root] = get_helpers(ext);

for k = 1:length(helper)
	
	file = [root, filesep, helper{k}, '.m'];
	
	str = ['     ', helper{k}, '  ', edit_link(file)];
	
	if in_svn
		str = [str, ' ', diff_link(file)];
	end
	
	disp(str);
	
end

disp(' ');

disp('   ADD:');

str = ['     ', helper_link(ext, 0), ',  ', helper_link(ext, 1)]; disp(str);

disp(' ');
disp(sep);
disp(' ');

%----------------
% ADD_LINK
%----------------

function str = add_link(ext, name, label)

if nargin < 3
	label = 'Add';
end

str = [ext.subtype, '::', ext.name, '::', ext.parent];

str = ['<a href="matlab:generate_function(''', str , ''', ''', name, ''');">', label, '</a>'];


%----------------
% EDIT_LINK
%----------------

function str = edit_link(file)

str = ['<a href="matlab:opentoline(''', file, ''',', int2str(1), ');">Edit</a>'];


%----------------
% SHOW_LINK
%----------------

function str = show_link(file)

str = ['<a href="matlab:show_file(''', file, ''');">Show</a>'];


%----------------
% DIFF_LINK
%----------------

function str = diff_link(file)

str = ['<a href="matlab:tsvn(''diff'', ''', file, ''');">Diff</a>'];


%----------------
% HELPER_LINK
%----------------

function str = helper_link(ext, private)

if nargin < 2
	private = 0;
end 

tag = ext_tag(ext);

if private
	str = ['<a href="matlab:add_helper_dialog(''', tag , ''', 1);">private_helper</a>'];
else
	str = ['<a href="matlab:add_helper_dialog(''', tag , ''', 0);">public_helper</a>'];
end 


%----------------
% EXT_TAG
%----------------

% NOTE: this function has use outside of this function

function tag = ext_tag(ext)

tag = [ext.subtype, '::', ext.name, '::', ext.parent];
