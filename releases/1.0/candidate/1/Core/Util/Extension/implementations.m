function info = implementations(type, name)

% implementations - get implementations of a method for an extension type
% -----------------------------------------------------------------------
%
% fun = implementations(type, name)
%
% Input:
% ------
%  type - extension type
%  name - method name 
%
% Output:
% -------
%  info - implementation info

%--
% initialize output
%--

info.fun = []; info.ext = {}; info = empty(info);

%--
% get extensions of type
%--

ext = get_extensions(type);

if isempty(ext)
	return;
end

%--
% normalize method name and check method is available
%--

% NOTE: accept underscore, space, and dot notation

if any(name == ' ')
	name = strrep(name, ' ', '__');
end

if any(name == '.')
	name = strrep(name, '.', '__');
end

if ~isfield(flatten(ext(1).fun), name)
	return;
end

%--
% check for implementations of method
%--

for k = 1:length(ext)
	
	%--
	% get method handle
	%--
	
	fun = flatten(ext(k).fun); handle = fun.(name);
	
	% NOTE: continue if there is no handle available
	
	if isempty(handle)
		continue;
	end
	
	%--
	% pack methods handle in output along with parent extension name
	%--

	if isempty(info)

		info(end + 1).fun = handle; info(end).ext = {ext(k).name};
		
	else
		
		handles = {info.fun}; ix = find(handles == handle);

		if isempty(ix)
			info(end + 1).fun = handle; info(end).ext = {ext(k).name};
		else
			info(ix).ext{end + 1} = ext(k).name;
		end

	end

end

%--
% display results
%--

if ~nargout
	
	for k = 1:length(info)
		
		str_line(64, '_');
		disp(' ');
		fun_disp(info(k).fun);
		str_line(64, '_');
	
		disp(' ');
		
		for j = 1:length(info(k).ext)
			disp([' ', info(k).ext{j}]);
		end
		
		disp(' ');
	end
	
	clear info;
	
end


% TODO: develop this into the overloaded display for function handles

function fun_disp(in)

info = functions(in); fields = fieldnames(info);

for k = 1:length(fields)
	disp([' ', fields{k}, ': ', to_str(info.(fields{k}))]);
end

