function ext = extension_here(varargin)

% extension_here - gets extension from current directory
% ------------------------------------------------------
%
% ext = extension_here
%
% Input:
% ------
%  root - directory to consider
%
% Output:
% -------
%  ext - in this directory

%--
% check whether we are in an extension directory
%--

[type, name] = dir_to_type(varargin{:});

if isempty(name)
	ext = []; return;
end

%--
% get corresponding extension
%--

ext = get_extensions(type, 'name', name);