function [file, root] = preset_file(ext, name, type)

% preset_file - get extension preset file name
% --------------------------------------------
%
% [file, root] = preset_file(ext, name, type)
%
% Input:
% ------
%  ext - extension
%  name - name
%  type - type
%
% Output:
% -------
%  file - file
%  root - root

%--
% handle input
%--

% NOTE: when this happens then 'preset_dir' will fill this with 'compute' for now

if nargin < 3
	type = '';
end

if ~proper_filename(name)
	error('Preset name must be a proper filename.');
end

%--
% get preset directory and full filename
%--

root = preset_dir(ext, type);

% NOTE: what we get is really the name of the preset, when it is MAT store, this will be appended, when DIR we are done

file = [root, filesep, name];