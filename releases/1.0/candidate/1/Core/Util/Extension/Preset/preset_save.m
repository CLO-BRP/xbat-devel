function [info, store] = preset_save(preset, name, type, tags, notes)

% preset_save - save preset to file
% ---------------------------------
%
% [info, store] = preset_save(preset, name, type, tags, notes)
%
% Input:
% ------
%  preset - preset
%  name - name
%  type - type
%  tags - tags
%  notes - notes
%
% Output:
% -------
%  info - store info
%  store - preset store

%------------------
% HANDLE INPUT
%------------------

%--
% set default empty tags and notes
%--

if nargin < 5
	notes = {};
end

if nargin < 4
	tags = {};
end

% NOTE: this is the preset type

if nargin < 3
	type = 'compute';
end 

if nargin < 2
	% TODO: this is not where we store this value in other places
	
	if isfield(preset, 'name') && ~isempty(preset.name)
		name = preset.name;
	else
		error('Preset must be named.');
	end
end

%--
% handle tags
%--

if ischar(tags)
	tags = str_to_tags(tags);
end

if ~is_tags(tags)
	error('Input tags are not proper tags.');
end

%--
% check preset name
%--

if ~proper_filename(name)
	error('Preset name must be proper filename.');
end

%------------------
% SAVE PRESET
%------------------

%--
% get preset store location
%--

store = preset_file(preset.ext, name, type);

%--
% update preset
%--

if ~isempty(tags)
	preset.tags = tags;
end

if ~isempty(notes)
	preset.notes = notes;
end

%--
% check for helper
%--

ext = preset.ext;

if strcmp(type, 'compute')
	helper = ext.fun.parameter.save;
else
	helper = ext.fun.(type).parameter.save;
end 

%--
% save preset file and get info
%--

if isempty(helper)
	% NOTE: the default store simply stores the full preset to a MAT file
	
	store = [store, '.mat'];
	
	try
		save(store, 'preset'); info = dir(store);
	catch
		info = dir(store);
	end
	
% 	barn_preset_save(ext, preset);

else
	% TODO: the dynamic field name below reveals how 'type' parameters are stored
	
	try	
		% NOTE: helpers are expected to create a directory store and put preset content inside it
		
		if strcmp(type, 'compute')
			preset = ext.parameter;
		else
			preset = ext.parameters.(type);
		end
		
		% TODO: consider what may be needed for 'context' in this context
		
		context.user = get_active_user; context.ext = ext;
		
		info = helper(preset, store, context);
	catch
		if isempty(type)
			str = 'Preset save failed.';
		else
			type(1) = upper(type(1)); str = [type, ' preset save failed.'];
		end
		
		extension_warning(ext, str, lasterror);
	end
	
% 	info = dir(store);
end


%----------------------------
% BARN_PRESET_SAVE
%----------------------------

function preset = barn_preset_save(ext, preset)

% TODO: we should factor some part of this

store = local_barn;

if isempty(store)
	return;
end

table = extension_table_names(ext);

if strcmp(type, 'compute')
	preset = ext.parameter;
else
	preset = ext.parameters.(type);
end

user = set_barn_user;

preset = flatten(preset); preset.user_id = user.id;

% NOTE: the extension that we get here has been obtained through a browser, therefore it has been compiled with a context

if ~has_barn_table(store, table.parameter)
	
	establish_extension_table(store, ext, 'parameter');
end

parameter = set_database_objects(store, preset, [], [], table.parameter);

extension_persist = get_database_objects_by_column(store, 'extension', 'guid', ext.guid);

% NOTE: we get an empty prototype 'extension_preset' from the database

obj = empty(get_database_objects(store, 'extension_preset', []));

obj(end + 1).guid = generate_uuid;

obj.name = name;

obj.user_id = user.id;

obj.parameter_id = parameter.id;

obj.extension_id = extension_persist.id;

preset = set_database_objects(store, obj, [], [], 'extension_preset');



