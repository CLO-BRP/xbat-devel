function [value, types] = has_any_presets(ext)

% has_any_presets - check extension has any presets
% -------------------------------------------------
%
% [value, types] = has_any_presets(ext) 
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - has presets indicator
%  types - available presets

%--
% get preset types
%--

% NOTE: 'get_preset_types' returns potential preset types

types = get_preset_types(ext); value = 0;

%--
% check at least one types has presets
%--

for k = length(types):-1:1

	if ~has_presets(ext, types{k})
		types(k) = []; continue;
	end

	value = 1;
	
	if nargout < 2
		return;
	end
	
end
