function child = extension_adopt(child, parent, pretend, current)

% extension_adopt - change the parent of an extension
% ---------------------------------------------------
%
% extension_adopt(child, parent, pretend, current)
%
% Input:
% ------
%  child - to adopt
%  parent - adopting
%  pretend - indicator (default: 1)
%  current - parent name
%
% Output:
% -------
%  child - adopted

%--
% handle input
%--

if nargin < 4
	current = '';
end

if nargin < 3
	pretend = 1;
end

if nargin < 2 || isempty(parent)
	return;
end

main = @get_extension_main; % NOTE: alias function for convenience

% TODO: we should check the extension type to see if inheritance is allowed

%--
% update reference to current parent in child main with new parent
%--

if isempty(current)

	ancestry = get_extension_ancestry(child);

	if numel(ancestry) == 1
		error('Orphan adoption is not implemented.');
	end

	[ignore, current, ignore] = fileparts(main(ancestry(2)));

end

[ignore, parent, ignore] = fileparts(main(parent));

opt = file_replace; opt.pretend = pretend;

file_replace(main(child), current, parent, [], opt);
	
%--
% re-discover child after adoption
%--

child = discover_extensions(child.subtype, child.name);




