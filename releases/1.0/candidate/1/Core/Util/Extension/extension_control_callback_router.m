function result = extension_control_callback_router(obj, eventdata, ext, context, type, fun) %#ok<INUSL>

% extension_control_callback_router - route extension control callback
% --------------------------------------------------------------------
%
% result = extension_control_callback_router(obj, eventdata, ext, context, type, fun)
%
% Input:
% ------
%  obj - callback object
%  eventdata - reserved
%  ext - extension
%  context - for extension
%  type - control parameter type
%  fun - callback
%
% Output:
% -------
%  result - result

%--
% compile callback context
%--

callback = get_callback_context(obj, eventdata);

%--
% get fresh context and extension
%--

par = callback.par.handle; pal = callback.pal.handle;

if is_tabs_control(callback.control)
	
	% NOTE: 'get_browser_extension' makes a 'tabs' callback too expensive, we use an incorrect 'light' alternative
	
	% NOTE: this is taken from 'get_browser_extension'
	
	context = get_extension_context(ext, par);

	context.pal = pal;
else
	
	[ext, ignore, context2] = get_browser_extension(ext.subtype, par, ext.name); %#ok<ASGLU>
	
	% NOTE: we merge contexts here, so that we keep any special context state input
	
	% NOTE: this assumes that context state is stored in new fields and 'state', not in other base fields
	
	% NOTE: state should be empty coming out of 'get_browser_extension'
	
	added = union(setdiff(fields(context), fields(context2)), 'state');
	
	for k = 1:numel(added)
% 		db_disp(['added: ', added{k}]);
		
		context2.(added{k}) = context.(added{k});
	end
end

%--
% call control callback
%--

% NOTE: the callback has a current extension in the context

try
	result = fun(callback, context2);
catch
	extension_warning(ext, ['''', title_caps(type), ''' control callback failed.'], lasterror); return;
end

%--
% try to update widget extension
%--

% TODO: if the context is wrong this could be a big problem

if ~isfield(context, 'widget') && ~isempty(context.widget)
	
	db_disp('updating-widget-extension, consider-context-used');
	
	[ext, context] = get_browser_extension(ext.type, context.par, ext.name);
	
	set_widget_extension(context.widget, ext, context);
end

%--
% consider callback result
%--

% NOTE: return if result indicate no need for update

if ~trivial(result) && isfield(result, 'update') && ~result.update
	return;
end

%--
% update display considering results
%--

% NOTE: there are callbacks that regenerate the palette, in this case the handle is stale

if ~ishandle(pal)
	pal = get_extension_palette(ext, par);
end

if isempty(pal)
	return;
end

update_parent_display(pal);
