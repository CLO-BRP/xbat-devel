function pal = delete_palette(pal, eventdata, par) %#ok<INUSL>

% delete_palette - delete palette from screen and update parent
% -------------------------------------------------------------
%
% pal = delete_palette(pal, evendata, par)
%
% Input:
% ------
%  pal - palette handle
%  par - parent handle
%
% Output:
% -------
%  pal - palette handle (no longer a handle)

% NOTE: this function does a lot more than just close the palette figure

%------------------
% SETUP
%------------------

%--
% get parent state
%--

data = get_browser(par);

%------------------
% STORE EXTENSION
%------------------

%--
% check palette belongs to extension
%--

% NOTE: the extension returned here, if any does not contain current state

[store, ext] = is_extension_palette(pal);

%--
% update store if needed
%--

if store
	
	[ext, eix] = get_browser_extension(ext.subtype, par, ext.name, data);

	if ~isempty(eix)
		data.browser.(ext.subtype).ext(eix) = ext;
	end

end

%------------------
% STORE STATE
%------------------

%--
% get palette state
%--

% NOTE: the palette state includes: position, toggle, and tab selection states

state = get_palette_state(pal);

%--
% append or update palette state registry
%--

if isempty(data.browser.palette_states)
	
	data.browser.palette_states = state;
	
else
	
	names = struct_field(data.browser.palette_states, 'name');
	
	ix = find(strcmp(state.name, names));
	
	% NOTE: this clears the state store of old strutures
	
	try
		if ~isempty(ix)
			data.browser.palette_states(ix) = state;
		else
			data.browser.palette_states(end + 1) = state;
		end
	catch
		data.browser.palette_states = state;
	end
	
end

%------------------
% UNREGISTER
%------------------

data.browser.palettes(data.browser.palettes == pal) = [];

%------------------
% UPDATE AND CLOSE
%------------------

set(par, 'userdata', data);

closereq;
