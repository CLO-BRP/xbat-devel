function value = is_tabs_callback(callback)

value = strcmpi(callback.control.kind, 'tabs');