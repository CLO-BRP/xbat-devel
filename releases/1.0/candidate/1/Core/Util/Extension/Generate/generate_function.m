function [file, created] = generate_function(ext, fun)

% generate_function - generate template function for extension
% ------------------------------------------------------------
%
% [file, created] = generate_function(ext, fun)
%
% Input:
% ------
%  ext - extension to generate function for
%  fun - name of function(s) to generate
%
% Output
% ------
%  file - file(s)
%  created - indicator

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

%--
% handle string input
%--

% NOTE: this is used in 'pal_cli'

% NOTE: this will create a duck from a string

if ischar(ext)
	ext = parse_tag(ext, '::', {'subtype', 'name', 'parent'});
end

%--
% get interface information for extension type
%--

type = ext.subtype;

% NOTE: we assume that the extension name is the simple normalized name

% NOTE: the following test handles ducks

if ~isfield(ext, 'fun')
	ext = get_extensions(ext.subtype, 'name', ext.name);
end

main = ext.fun.main;

% if isfield(ext, 'fun')
% 	main = ext.fun.main;
% else
% 	main = get_main_name(ext.name, type, 1);
% end

[name, args, sig, out] = extension_signatures(type, main);

%--
% set default functions to generate
%--

% NOTE: the default is to generate all

if nargin < 2
	fun = name;
end

% NOTE: return if there is nothing to generate

file = {};

if isempty(fun)
	return;
end

% NOTE: wrap fun string in cell

if ischar(fun)
	fun = {fun};
end

%-----------------------------------
% GENERATE FUNCTIONS
%-----------------------------------

root = extension_root(ext);

%--
% loop over requested functions
%--

file = cell(size(fun)); created = zeros(size(fun));

for k = 1:length(fun)
	
	%--
	% find function in interface
	%--
	
	ix = find(strcmp(name, fun{k}));
	
	if isempty(ix)
		continue;
	end
	
	%--
	% check for file, continue if it exists, create if needed
	%--
	
	file{k} = [root, filesep, 'private', filesep, name{ix}, '.m'];
	
	% TODO: ask if we want to regenerate file
	
	if exist(file{k}, 'file')
		continue;
	end
	
	fid = fopen(file{k}, 'w'); created(k) = 1; rel_path(file{k});
	
	%-- 
	% add function signature and indicator comment
	%--
	
	fprintf(fid, '%s\n\n%s\n\n', ...
		['function ', sig{ix}], ...
		['% ', upper(ext.name), ' - ', name{ix}] ...
	);

	if strcmp(name{ix}, 'value__create')
		fprintf(fid, 'hint = column_type_hints;\n\n');
	end
	
	%--
	% add trivial body to make function execute
	%--
	
	if isempty(ext.parent)
		
		fprintf(fid, '%s\n', out{ix});
	
	else
		
		% TODO: we need to get the parent type signature, and check for compatibility
		
		% NOTE: wrap all this into a function 
		
		par = ext.parent.main();
		
		par_fun = flatten(par.fun);
		
		% NOTE: if possible create code to evaluate parent function
		
		if isfield(par_fun, name{ix}) && ~isempty(par_fun.(name{ix}))
			
			if isequal(par.subtype, ext.subtype)
				
				% NOTE: this relies on the fact that the signatures are the completely the same

				fprintf(fid, '%s\n\n%s; %s;\n', ...
					['% ', out{ix}], ...
					'fun = parent_fun(mfilename(''fullpath''))', strrep(sig{ix}, name{ix}, 'fun') ...
				);
			
			else 
			
				% NOTE: in this case we have some further consideration when calling the parent
				
				% NOTE: it is OK to consider these cross-type cases individually
				
				if feature_detector_inheritance(par, ext) && strcmp(name{ix}, 'compute')
					
					% NOTE: what this does is output an empty event and turn the feature into the detector view data 
					
					% TODO: the second line should perhaps be dynamically generated
					
					fprintf(fid, '%s\n\n%s\n', ...
						out{ix}, ...
						'fun = parent_fun(mfilename(''fullpath'')); [feature, context] = fun(page, parameter, context); context.view.data = feature;' ...
					);
			
				elseif feature_measure_inheritance(par, ext) && strcmp(name{ix}, 'compute')
					
					fprintf(fid, '%s\n\n%s\n\n%s\n\n%s\n', ...
						['% ', out{ix}], ...
						'page = get_event_page(event, context.sound, context);', ...
						'fun = parent_fun(mfilename(''fullpath'')); [feature, context] = fun(page, parameter, context); context.state.feature = feature;', ...
						out{ix} ...
					);
					
				else

					fprintf(fid, '%s\n\n%s; %s;\n', ...
						['% ', out{ix}], ...
						'fun = parent_fun(mfilename(''fullpath''))', strrep(sig{ix}, name{ix}, 'fun') ...
					);
				
% 					event = empty(event_create);

				end
				
			end
		
		else
			
			fprintf(fid, '%s\n', out{ix});
		
		end
		
	end
	
	%--
	% create skeleton control and callback code
	%--
	
	% NOTE: do not try to make this code too smart, it will just make it stupid
	
	%--
	% close file
	%--
	
	fclose(fid);
	
end

%--
% update extension cache
%--

% NOTE: this does not update browsers, this function is unaware of browsers

extensions_cache(discover_extensions(ext.subtype, ext.name));

%--
% un-cell single file
%--

if length(file) == 1
	file = file{1};
end



function value = feature_detector_inheritance(par, ext)

value = strcmp(par.subtype, 'sound_feature') && strcmp(ext.subtype, 'sound_detector');


function value = feature_measure_inheritance(par, ext)

value = strcmp(par.subtype, 'sound_feature') && strcmp(ext.subtype, 'event_measure');

