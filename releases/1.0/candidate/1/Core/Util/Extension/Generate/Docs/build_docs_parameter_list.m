function [lines, file] = build_docs_parameter_list(ext, type, level, par)

% build_docs_parameter_list - text for docs
% -----------------------------------------
%
% [lines, file] = build_docs_parameter_list(ext, type, level, par)
%
% Input:
% ------
%  ext - to build for
%  type - of parameters
%  level - in document (def: 'subsubsection', as type is typically 'sub')
%  par - context browser
%
% NOTE: we need to be able to compile parameters in the context of a sound

%--
% handle input
%--

if nargin < 4
	par = get_active_browser;
end 

if isempty(par)
	error('We need a browser for context as we try to compile parameter information.'); 
end

if nargin < 3
	level = 'subsubsection';
end 

% TODO: check level value here

if nargin < 2
	type = 'compute';
end

%--
% get full parameters, discrepancy with parent, and with compiled parameters if available
%--

[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name);

if trivial(ext.parent)
	pext = [];
else
	pext = get_browser_extension(ext.parent.type, par, ext.parent.name);
end

parameter = ext.parameters.(type); fields = fieldnames(flatten(parameter));

%----------------

% uncompiled = get_uncompiled_parameters(ext, context, type);

% hidden = setdiff(fields, fieldnames(flatten(uncompiled)));

% fields = fieldnames(flatten(uncompiled));

% [fields, control] = get_relevant_controls(ext, context, type);

%----------------

if ~isempty(pext)
	pparameter = pext.parameters.(type); pfields = fieldnames(flatten(pparameter));
	
	fields = setdiff(fields, pfields);
end

for k = 1:numel(fields) 
	fields{k} = strrep(fields{k}, '__', '.');
	
	fields{k} = latex_escape(fields{k});
end

if isempty(fields)
	lines = {};
else
	lines = cell(4 * numel(fields), 1);

	% TODO: add labels to parameters consider parent-type-name, verbose but should allow global referencing

	for k = 1:numel(fields)
		lines{4 * k - 3} = sprintf('\t%s', ['% BEGIN-PARAMETER ', fields{k}]);

		lines{4 * k - 2} = sprintf('\t%s', ['\item \texttt{\large ', fields{k},'}\\']);

		lines{4 * k - 1} = sprintf('\t\t%s', ['Here we describe the ''', fields{k}, ''' parameter.']);

		lines{4 * k} = sprintf('\t%s', ['% END-PARAMETER ', fields{k}]);
	end

	lines = [
		{ ...
			['\', level, '{Defined by ', latex_escape(ext.name), ' (', latex_escape(ext.type), ')}'], ...
			'\begin{paramlist}' ...
		}'; ...
		lines; ...
		{'\end{paramlist}'} ...
	];
end

file = fullfile(extension_root(ext), 'Docs', 'DOCS', ['parameters__', type, '.tex']);

%--
% make sure parent file exists
%--

if ~isempty(pext)
	[ignore, pfile] = build_docs_parameter_list(pext, type, par);

	lines{end + 1} = ['\input{', pfile, '}'];
end

% DEBUG DISPLAY

% db_disp(upper(file)); iterate(@disp, lines);


%--------------------------------
% GET_RELEVANT_CONTROLS
%--------------------------------

% NOTE: we get controls that we should document

% NOTE: in the end we want to document controls and parameters, it would be useful to know which parameters require context for their default value

function [name, control] = get_relevant_controls(ext, context, type)

%--
% get current controls
%--

parameter = ext.parameters.(type);

switch type
	case 'compute'
		control = ext.fun.parameter.control.create(parameter, context);

	case 'view'
		control = ext.fun.view.parameter.control.create(parameter, context);
end

%--
% create auxiliary control array describing what we should document
%--

for k = numel(control):-1:1
	
	switch control(k).style
		
		% NOTE: these are not real controls
		
		case {'separator', 'tabs', 'axes'}
			control(k) = []; continue;
		
		% NOTE: this separates buttons in a group into a collection of controls and removes the group
		
		case 'buttongroup'
			for j = 1:numel(control(k).name)
				control(end + 1) = control(k);  %#ok<AGROW>
				
				control(end).name = control(k).name{j}; % TODO: cleanup other array fields
			end 
			
			control(k) = [];
	end 
end

name = {control.name};


%--------------------------------
% GET_UNCOMPILED_PARAMETERS
%--------------------------------

function uncompiled = get_uncompiled_parameters(ext, context, type)

uncompiled = struct;

switch type
	case 'compute'
		
		if ~isempty(ext.fun.parameter.compile)
			uncompiled = ext.fun.parameter.create(context);
		end
		
	otherwise
		
		if ~isempty(ext.fun.(type).parameter.compile)
			uncompiled = ext.fun.(type).parameter.create(context);
		end
end

