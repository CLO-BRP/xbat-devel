function varargout = extension_call(ext, name, varargin)

% extension_call - call extension method
% --------------------------------------
%
% varargout = extension_call(ext, name, varargin)
%
% Input:
% ------
%  ext - extension
%  name - method name
%  varargin - inputs required for method
%
% Output:
% -------
%  varargout - outputs provided by method

% NOTE: it is not clear that we want to do this because of the overhead

% NOTE: this will only be useful when it improves the resulting code

% TODO: consider how to handle failure to get handle

%--
% get function handle for call
%--

fun = get_extension_fun(ext, name);

%--
% make call to extension method
%--

% TODO: add option to wrap or not wrap, the latter should be faster during production

% NOTE: consider doing this based on an extension flag, with possible override

try
	[varargout{1:nargout}] = fun(varargin{:});
catch
	extension_warning(ext, get_call_error_string(name), lasterror);
end


%-----------------------------
% GET_EXTENSION_FUN
%-----------------------------

function get_extension_fun(ext, name)

%--
% parse name into field hierarchy
%--

field = parse_fun_name(name);

%--
% traverse extension functions structure looking for function
%--

current = ext.fun;

for k = 1:length(field)

	% NOTE: this indicates an improper name
	
	if ~isfield(current, field{k})
		field = {}; return;
	end
	
	current = current.(field{k});
	
end

%-----------------------------
% GET_CALL_ERROR_STRING
%-----------------------------

function str = get_call_error_string(name)

str = [str_implode(parse_fun_name(name)), ' failed.']; str(1) = upper(str(1));


%-----------------------------
% PARSE_FUN_NAME
%-----------------------------

function field = parse_fun_name(name)

%--
% handle space separated name
%--

if is_space_name(name)
	field = strread(name, '%s', -1); return;
end

%--
% handle dot separated name
%--

if is_dot_name(name)
	field = strread(strrep(name, ' ', ''), '%s', -1, 'delimiter', '.'); return;
end

%--
% handle double underscore separated name
%--

% NOTE: convert name to the first and faster of the previous cases and use a recursive call

field = parse_fun_name(strrep(name, '__', ' '));


%-----------------------------
% IS_SPACE_NAME
%-----------------------------

function value = is_space_name(name)

% NOTE: check for space

value = ~isempty(find(name == ' ', 1));


%-----------------------------
% IS_DOT_NAME
%-----------------------------

function value = is_dot_name(name)

% NOTE: check for period

value = ~isempty(find(name == '.', 1));