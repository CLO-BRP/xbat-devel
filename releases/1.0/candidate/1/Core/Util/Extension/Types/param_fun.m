function fun = param_fun(name)

% param_fun - parameter functions structure
% -----------------------------------------
%
% fun = param_fun(name)
%
% Output:
% -------
%  fun - function structure

if ~nargin || isempty(name)
	name = 'parameter';
end

% NOTE: the first cell contains output names, the second cell input names

fun.create = {{name}, {'context'}}; 

fun.compile = {{name, 'context'}, {name, 'context'}};

fun.control = control_fun(name);

% fun.menu.create = menu_args(name);
% 
% fun.menu.callback = {{'result'}, {'callback', 'context'}};

fun.menu = menu_fun(name);

% NOTE: the 'store' is the place where the parameters are stored

fun.load = {{name}, {'store', 'context'}};

fun.model = {{name}, 'context'};

fun.save = {{'info'}, {name, 'store', 'context'}};