function [protocol, host, path, ext] = urlparts(url)

% urlparts - separate url into component parts
% --------------------------------------------
%
% [protocol, host, path, ext] = urlparts(url)
%
% Input:
% ------
%  url - url
%
% Output:
% -------
%  protocol - name
%  host - name
%  path - location
%  ext - extension
%
% NOTE: asking for extension modifies the path output

% TODO: update to support all possible parts of a URL correctly, authentication information

% TODO: check for known protocol

%--
% get protocol from string
%--

ix1 = strfind(url, '://'); 

if numel(ix1) > 1
	error('Improper URL, contains more than one protocol separator.');
end

protocol = url(1:ix1 - 1);

%--
% get host and path if available
%--

ix2 = strfind(url(ix1 + 3:end), '/');

% NOTE: if we cannot find a separator we only have a host

if isempty(ix2)
	host = url(ix1 + 3:end); path = ''; return;
end 

host = url(ix1 + 3:ix1 + ix2 + 1);

path = url(ix1 + 3 + ix2 - 1:end);

if nargout > 3
	
	ix = find(path == '.', 1, 'last');
	
	if isempty(ix)
		ext = '';
	else
		ext = path(ix:end); path = path(1:ix - 1);
	end
	
end




