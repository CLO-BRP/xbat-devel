function [value, par] = sound_is_open(sound, lib)

% sound_is_open - test if a sound is open
% ---------------------------------------
%
% [value, par] = sound_is_open(sound, lib)
%
% sound_is_open(par)
% 
% Input:
% ------
%  sound - sound or name
%
% Output:
% -------
%  par - handle to open sound browser

%-------------------
% HANDLE INPUT
%-------------------

%--
% for browser tag input, declare sound to be opening
%--

if ischar(sound)
	open_sounds_cache(sound); value = 1; par = []; return;
end

%--
% parent handle is input declare sound open
%--

if ishandle(sound)
	
	par = sound; value = 1;
	
	open_sounds_cache(setdiff(open_sounds_cache, get(sound, 'tag'))); 
	
	return;

end

%--
% set library
%--

if nargin < 2
	lib = get_active_library;
end

%-------------------
% CHECK FOR OPEN
%-------------------

%--
% check for opening
%--

tag = browser_tag(sound, lib, get_active_user);

if ismember(tag, open_sounds_cache)
	value = 1; par = []; return;
end

%--
% get open sound browsers
%--

handles = get_xbat_figs('type', 'sound');

%--
% look for one with this sound
%--

par = [];

for k = 1:length(handles)
	
	handle = handles(k);
	
	info = parse_browser_tag(get(handle, 'tag')); 
	
	if ~strcmp(info.sound, sound_name(sound)) || ~strcmp(info.library, get_library_name(lib))
		continue;
	end
	
	par(end + 1) = handle;
	
end

%--
% answer question
%--

value = ~isempty(par);
