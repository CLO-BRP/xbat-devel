function [hash, obj] = get_sound_hash(sound)

% get_sound_hash - as data based unique identifier
% ------------------------------------------------
%
% [hash, obj] = get_sound_hash(sound)
%
% Input:
% ------
%  sound - to hash
%
% Output:
% -------
%  hash - value
%  obj - of intermediate computations

%--
% handle multiple sounds
%--

if numel(sound) > 1
	hash = iterate(mfilename, sound); return;
end 

%--
% hash a string constructed from the ordered file content hashes
%--

% HACK: this is a stop-gap to deal with problems storing the content hash

hash = get_cache_hash(sound);

if ~isempty(hash)
	obj = struct; return;
end

disp(['Computing content-based hash for ''', sound_name(sound), ''' this may take some time ...']);

% NOTE: we also pack the intermediate results for inspection

% TODO: consider reuse of the BARN data_file content_hash

if ischar(sound.file)
	obj.file = sound.file; obj.content_hash = file_md5(get_sound_file(sound));
else
	obj.file = iteraten(@fullfile, 2, sound.path, sound.file); obj.content_hash = iterate(@file_md5, obj.file);
end

obj.string = str_implode(obj.content_hash, ',');

hash = md5(obj.string);

obj.hash = hash;

set_cache_hash(sound, hash);


%-------------------
% CACHE HASH
%-------------------

function hash = set_cache_hash(sound, hash)

hash = key_value_store([], ['sound_hash::', sound_name(sound)], hash);


function hash = get_cache_hash(sound)

hash = key_value_store([], ['sound_hash::', sound_name(sound)]);



