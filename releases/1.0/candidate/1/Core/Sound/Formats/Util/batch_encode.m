function result = batch_encode(source, destination, format, opt)

% batch_encode - files to specified format
% ----------------------------------------
%
% result = batch_encode(source, destination, format, opt)
%
% Input:
% ------
%  source - directory
%  destination - for encoded files
%  format - to encode to
%  opt - for encoding
%
% Output:
% -------
%  result - summmary

%--
% handle input
%--

% TODO: check for known writeable format

if nargin < 3 || isempty(format)
	format = 'flac';
end

if nargin < 4 || isempty(opt)
	opt = sound_file_write(['test.', format]);
end

if ~nargin || isempty(source)
	source = pwd;
end

if nargin < 2 || isempty(destination)
	destination = fullfile(source, format);
end

%--
% scan for files to encode
%--

% TODO: consider allowing a filter for the input files, where does this
% come in, options?

roots = scan_dir(source);

disp(' ');

for k = 1:numel(roots)
	content = no_dot_dir(roots{k}, -1);
	
	disp(upper(roots{k})); disp(content)
	
	for j = 1:numel(content)
% 		sound_file_encode(
	end
end





