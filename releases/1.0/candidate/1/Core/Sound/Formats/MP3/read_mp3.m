function [X, opt] = read_mp3(info, ix, n, ch, opt)

% read_mp3 - read samples from sound file
% ---------------------------------------
%
% X = read_mp3(f, ix, n, ch)
%
% Input:
% ------
%  info - file info struct
%  ix - initial sample
%  n - number of samples
%  ch - channels to select
%  opt - conversion request options
%
% Output:
% -------
%  X - samples from sound file
%  opt - conversion request options

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 587 $
% $Date: 2005-02-22 23:28:55 -0500 (Tue, 22 Feb 2005) $
%--------------------------------

%--
% get samples from file
%--

X = sound_read_mex(info.file, ix, n, 2); 

X = X(:, ch);

	
