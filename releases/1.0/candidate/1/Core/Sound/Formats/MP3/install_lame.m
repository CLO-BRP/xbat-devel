function [installed, tool] = install_lame

% install_lame - command-line tool
% --------------------------------
%
% [installed, tool] = install_lame
%
% Output:
% -------
%  installed - indicator
%  tool - info

%--
% check whether tool is available
%--

tool = get_tool('lame.exe');

installed = ~isempty(tool);

if installed
    return;
end

%--
% download and install
%--

% TODO: we should consider mirroring some of these tools

url = 'http://xbat.org/downloads/installers/lame3.98.2-libsndfile.zip';

installed = install_tool('LAME', url, 'urlwrite');

if nargout > 1
    tool = get_tool('lame.exe');
end
