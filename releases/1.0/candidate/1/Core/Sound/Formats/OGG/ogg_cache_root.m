function root = ogg_cache_root

% ogg_cache_root - get root directory for decoded ogg files
% ---------------------------------------------------------
%
% root = ogg_cache_root
%
% Output:
% -------
%  root - directory

persistent ROOT

% TODO: consider a possible set, this is somewhat conflicts with the next idea

% TODO: develop this a bit more so that we can cache various things

if isempty(ROOT)
	ROOT = fullfile(tempdir, 'XBAT', 'OGG-CACHE');
end

root = create_dir(ROOT);

if isempty(root)
	error('Unable to create OGG-CACHE root.');
end
