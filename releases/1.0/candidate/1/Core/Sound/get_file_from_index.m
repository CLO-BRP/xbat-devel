function file = get_file_from_index(sound, ix, full)

% get_file_from_index - get file contained in sound from its index
% ----------------------------------------------------------------
%
% file = get_file_from_index(sound, ix)
%
% Input:
% ------
%  sound - sound
%  ix - file index
%
% Output:
% -------
%  file - file

%--
% handle file sound special case
%--

if ischar(sound.file)
	
	if ix == 1
		file = fullfile(sound.path, sound.file);
	else
		file = '';
	end
	
	return; 

end

%--
% get file name or key from sound file array
%--

% NOTE: this code will handle multiple indices

file = sound.file(ix);

% NOTE: if we are only dealing with a single file extract from cell

if iscell(file) && numel(file) == 1
	file = file{1};
end

%--
% make sure we have a file name
%--

% NOTE: if 'file' is a string or string cell array, we assume we have filenames and we are done

if ischar(file) || iscellstr(file)
	
	% NOTE: for non-playlist sounds we must attach the path to the file
	
	if lower(sound.type(1)) ~= 'p'
		file = strcat(sound.path, file);
	end
	
	return;

end

% NOTE: retrieve file from index in some way, database or callback








