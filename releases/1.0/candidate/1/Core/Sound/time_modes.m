function modes = time_modes

% time_modes - return list of possible time modes
% -----------------------------------------------
%
% modes = time_modes
%
% output:
% -------
%  modes - modes list


modes = {'record', 'slider', 'real'};