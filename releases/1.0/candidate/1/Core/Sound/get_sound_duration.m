function duration = get_sound_duration(sound)

% get_sound_duration - get duration using sessions
% ------------------------------------------------
%
% duration = get_sound_duration(sound)
%
% Inputs:
% -------
% sound - an XBAT sound structure
%
% Outputs:
% --------
% duration - the duration of the sound (in seconds)

%------------------
% HANDLE INPUT
%------------------

%--
% handle multiple sounds
%--

if numel(sound) > 1
	duration = iterate(mfilename, sound); return; 
end

%--
% an empty sound has no duration
%--

% NOTE: it is not clear why we consider this trivial case

if isempty(sound)
	duration = 0; return;
end

%------------------
% GET DURATION
%------------------

%--
% get recording duration if there are no sessions
%--

% NOTE: recording duration is exactly what it means, it refers to the data, not calendar time

if isempty(sound.time_stamp) || ~sound.time_stamp.enable
	duration = sound.duration; return;
end

%--
% get duration based on sessions state
%--

% NOTE: the result of this function is used as the slider duration

sessions = get_sound_sessions(sound);

duration = sessions(end).end;

