function [root, exists] = library_sound_root(sound, lib)

% library_sound_root - get root directory of sound given sound and library
% ------------------------------------------------------------------------
%
% [root, exists] = library_sound_root(sound, lib)
%
% Input:
% ------
%  sound - sound
%  lib - parent library
%
% Output:
% -------
%  root - path to sound in library
%  exists - existence indicator

%--
% set default active library 
%--

if nargin < 2
    lib = get_active_library;
end

%--
% compute sound root
%--

% NOTE: library path stores are unreliable with regards to final filesep

root = fullfile(lib.path, sound_name(sound));

if nargout > 1
    exists = exist_dir(root);
end 
