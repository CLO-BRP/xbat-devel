function sound = update_sound(sound)

%--
% update sound structure
%--

opt = struct_update; opt.flatten = 0;

sound = struct_update(sound_create(''), sound, opt);

%--
% update spectrogram and view parameters
%--

sound.specgram = struct_update(fast_specgram, sound.specgram);

sound.view = struct_update(view_create, sound.view);