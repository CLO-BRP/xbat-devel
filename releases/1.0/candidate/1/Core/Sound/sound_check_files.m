function out = sound_check_files(sound)

% sound_check_files - check that all files for a given sound exist
% ----------------------------------------------------------------
%  out = sound_check_files(sound)
%
% Input:
% ------
%  sound - the sound
%
% Output:
% -------
%  out - true if all files exist

out = 1;

if ismember(sound.type, {'variable', 'recording', 'synthetic'})
	return;
end

%--
% check that sound files exist in the same location
%--

if ischar(sound.file)
	
	test = exist([sound.path, sound.file], 'file');
	
else
	
	% NOTE: we check existence of all files in sound, this can be expensive
	
	for k = 1:length(sound.file)		
		test(k) = exist(get_file_from_index(sound, k), 'file');
	end

end

%--
% find the sound files and reassign if needed
%--

out = all(test == 2);

	
