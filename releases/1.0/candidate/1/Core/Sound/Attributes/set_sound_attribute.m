function value = set_sound_attribute(sound, name, value, store)

% set_sound_attribute - in barn store
% -----------------------------------
%
% value = set_sound_attribute(sound, name, value, store)
%
% Input:
% ------
%  sound - parent
%  name - of attribute
%  value -of attribute
%  store - description
%
% Output:
% -------
%  value - stored

%--
% handle input
%--

if nargin < 4
	store = local_barn;
end

% NOTE: in this case no value is stored

if isempty(store)
	value = struct; return;
end

%--
% make sure we have a valid extension and the relevant extension table
%--

% NOTE: we try to get the extension by name or fieldname

ext = get_attribute_extension(name);

if isempty(ext)
	error(['Unavailable sound attribute ''', name, '''.']);
end

table = extension_table_names(ext);

if ~has_table(store, table.value)
	
	prototype = struct; field = fieldnames(value); hint = column_type_hints;
		
	prototype.id = 1; prototype.sound_id = 1;
		
	for k = 1:numel(field)
		prototype.(field{k}) = value.(field{k});
	end
		
	prototype.user_id = hint.integer;
	
	obj.created_at = hint.timestamp; obj.modified_at = hint.timestamp;
	
	establish_barn_table(store, table.value, 'extension', create_table(prototype, table.value));
	
end

%--
% store value
%--

% NOTE: here we get some needed foreign keys

sound = set_barn_sound(store, sound); user = set_barn_user;

value.sound_id = sound.id; value.user_id = user.id;

% NOTE: this slight duplication saves a single database access when output is not requested, probably not worthwhile

if nargout
	value = set_database_objects(store, value, [], {}, table.value);
else
	set_database_objects(store, value, [], {}, table.value);
end
