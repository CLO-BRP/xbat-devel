function out = read_compare(file, opt)

%--
% set and possibly output default options
%--

if (nargin < 2) || isempty(opt)
	
	opt.trials = 8;
	
	opt.ch_read = 1;
	
	opt.compare = {'xbat_single', 'matlab_double'};
	
	opt.functions = {'xbat_single', 'xbat_double', 'matlab_single', 'matlab_double'};
	
	% TODO: should this not be 'get_readable_formats'
	
	opt.formats = get_writeable_formats;
	
	% NOTE: the default reads five minute files at whatever rate
	
	opt.channels = 1; opt.rate = 44100; opt.length = 60 * opt.rate; opt.format = 'wav';
	
	if nargout && ~nargin
		out = opt; return;
	end
end

%--
% generate test file if needed
%--

if (nargin < 1) || isempty(file) || ~exist(file, 'file')
	file = generate_file(opt);
	
	disp(' '); disp('TEST'); disp(sound_file_info(file));
end

%--
% get samples from file so we may write them to several formats
%--

opt_read = sound_file_read; 

X = sound_file_read(file, [], [], [], 1:opt.channels, opt_read);

%--
% loop over formats, read single and double precision, and possibly compare to MATLAB
%--

formats = opt.formats;

for ix = 1:length(formats)
	
	%--
	% create test file for reading
	%--
	
	temp_file = ['temp.', formats(ix).ext{1}];
	
	info = sound_file_write(temp_file, X, opt.rate);
	
	% NOTE: we display the format and the test file info
	
	disp(upper(formats(ix).ext{1})); disp(info);
	
	%--
	% bench the XBAT function
	%--
	
	for data_class = {'single', 'double'}
	
		opt_read.class = data_class{1};
	
		for j = 1:opt.trials
			
			start = clock;
			
			Y = sound_file_read(temp_file, 0, length(X), [], 1:opt.ch_read, opt_read);
			
			elapsed(j) = etime(clock, start);
			
		end
	
		out(ix).(['xbat_' data_class{1}]) = (numel(Y) / 10^6) ./ elapsed;
	end
	
	%--
	% try to bench the MATLAB function
	%--
	
	% NOTE: we assume  MATLAB read functions are named '[ext]read.m', and that they all work the same way as 'wavread'
	
	name = [formats(ix).ext{1} 'read'];
	
	if ~exist(name, 'file')
		
		% NOTE: we output NaNs for formats that MATLAB does not support
		
		out(ix).matlab_single = NaN;
		
		out(ix).matlab_double = NaN;
		
	else
		
		fun = str2func(name);
		
		%--
		% single precision
		%--
	
		for j = 1:opt.trials
			
			start = clock;
			
			Y = fun(temp_file, length(X)); Y = single(Y);
			
			elapsed(j) = etime(clock, start);
			
		end
		
		out(ix).matlab_single = (numel(Y) / 10^6) ./ elapsed;
		
		%--
		% double precision
		%--
		
		for j = 1:opt.trials
			
			start = clock;
			
			Y = fun(temp_file, length(X));
			
			elapsed(j) = etime(clock, start);
			
		end
		
		out(ix).matlab_double = (numel(Y) / 10^6) ./ elapsed;
		
	end
	
end

%--
% create figure and produce box plots
%--

switch numel(out)
		
	case {1, 2, 3, 4}, r = 1; c = numel(out);
		
	case {5, 6}, r = 2; c = 3;
		
end

field = fieldnames(out);
	
label = upper(field);

for k = 1:numel(label)
	
	if string_contains(label{k}, 'single', 0)
		label{k} = 'S';
	else
		label{k} = 'D';
	end

end

par = fig;

for k = 1:numel(out)
	
	for j = 1:numel(field)
		A{k}(:, j) = out(k).(field{j});
	end
	
	figure(par); ax = subplot(r, c, k); 
	
	boxplot(A{k}, 'labels', label);
	
	title(upper(formats(k).ext{1})); ylabel('MS/S');

end

for k = 1:numel(out)
	ax(k) = subplot(r, c, k); ylim(k, :) = get(ax(k), 'ylim');
end

xlim = get(ax(1), 'xlim'); 

if numel(ax) > 1
	ylim = [min(ylim(:, 1)), max(ylim(:, 2))];
end

set(ax, 'ylim', ylim);

for k = 1:numel(out)
	
	line( ... 
		'parent', ax(k), ...
		'xdata', mean(xlim) * ones(1, 2), ... 
		'ydata', ylim ... 
	);

	text(mean(xlim) - 0.25 * diff(xlim), ylim(1) + 0.95 * diff(ylim), 'XBAT', ...
		'parent', ax(k), ...
		'horizontalalignment', 'center' ...
	);
	
	text(mean(xlim) + 0.25 * diff(xlim), ylim(1) + 0.95 * diff(ylim), 'MATLAB', ...
		'parent', ax(k), ...
		'horizontalalignment', 'center' ...
	);

end

%--
% create bar plot using data specified in opt.compare
%--

y = zeros(length(opt.compare), length(formats));

for k = 1:length(opt.compare)
	
	yy = [out.(opt.compare{k})]; y(k, :) = yy(x);
	
end

h = fig;

set(h, 'numbertitle', 'off', 'name', 'Sound Read Benchmark');

if length(formats) > 1

	h = bar(1:length(formats), y');
	
	%--
	% get legend names and create legend
	%--

	names = upper(opt.compare);

	names = strrep(names,'_DOUBLE',' D');
	
	names = strrep(names,'_SINGLE',' S');

	legend(h, names{:}, 'location', 'best');

	%--
	% set labels and stuff
	%--

	temp = strfind(opt.compare, 'matlab');

	matlab_flag = ~isempty([temp{:}]);

	ax = gca;

	xt = cell(size(x));

	for k = x;

		ext = formats(x(k)).ext{1};

		label = upper(ext);

		%--
		% mark formats that are not supported in MATLAB
		%--

		if ~strcmpi(ext, {'wav', 'au'})
			
			if matlab_flag
				label = [label ' (*)'];
			end
			
		end

		xt{k} = label;

	end

	set(ax, 'XTickLabel', xt);	
	
else
	
	h = bar(y);
	
	ax = gca;
	
	names = upper(opt.compare);

	names = strrep(names,'_DOUBLE',' D');
	
	names = strrep(names,'_SINGLE',' S');
	
	set(ax, 'XTickLabel', names);
	
end

%--
% get label handles
%--

xlb = get(ax, 'XLabel');

ylb = get(ax, 'YLabel');

titleh = get(ax, 'Title');

%--
% write labels
%--

set(ylb, 'String', 'MS/SEC');

set(titleh, 'String', 'SOUND FILE READ SPEED'); % (' num2str(opt.ch_read) ' out of ' num2str(opt.channels) ' channels)']); 




%--------------------------------------------------
% GENERATE_FILE
%--------------------------------------------------

function file = generate_file(opt)

%--
% generate file name and samples
%--

file = ['temp.', opt.format];

X = 2 * rand(opt.length, opt.channels) - 1;

%--
% write file
%--

rate = opt.rate; opt = sound_file_write(file); opt.rate = rate;

sound_file_write(file, X, opt.rate);

	
	
	
	
