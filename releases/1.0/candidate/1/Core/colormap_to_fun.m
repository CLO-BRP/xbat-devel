function [fun, S, T] = colormap_to_fun(str)

% colormap_to_fun - get function for named colormap
% -------------------------------------------------
%
% [fun, S, T] = colormap_to_fun(str)
%
% Input:
% ------
%  str - colormap name
%
% Output:
% -------
%  fun - function name or list of available colormaps
%  S - menu separators
%  T - tooltip strings

%--
% create colormap function table
%--

% NOTE: the only place where the space is used is in the interface, by 'browser_view_menu' I believe

% FIXME: there should be no space in 'Gray'

T = { ...
	'Gray ', 'gray'; ...
	'Bone', 'bone'; ...
	'Copper','copper'; ...
	'Pink','pink'; ...
	'Hot', 'hot'; ...
	'Sunset','cmap_pseudo'; ...
	'HSV', 'hsv'; ...
	'Jet', 'jet'; ...
	'Label', 'cmap_label'; ...
};
		

%--
% look up function name
%--

if nargin
	
	ix = find(strcmpi(strtrim(str), strtrim(T(:, 1))));
	
	if ~isempty(ix)
		fun = T{ix, 2};
	else
		fun = [];
	end

%--
% output available colormaps
%--

else
	
	fun = T(:, 1);
	
	%--
	% output menu separators
	%--
	
	if nargout > 1
		n = length(fun); S = bin2str(zeros(1, n)); S{2} = 'on'; S{5} = 'on';
	end
	
	%--
	% output tooltips
	%--
	
	if nargout > 2
		
		T = cell(1, n); 
		
		for k = 1:n
			T{k} = '';
		end
		
		T = { ...
			'Linear gray-scale colormap (Shift + G)'; ...
			'Gray-scale with a tinge of blue colormap (Shift + B)'; ...
			'Linear copper-tone colormap'; ...
			'Pastel shades of pink colormap'; ...
			'Black-red-yellow-white colormap (Shift + H)'; ...
			'Hue-saturation-value colormap'; ...
			'Variant of HSV colormap (Shift + J)'; ...
			'Permuted HSV colormap'; ...
		};
	
	end
			
end


