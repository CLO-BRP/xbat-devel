function x = spcharout(x)

%SPCHAROUT replaces special character codes by their values
%
%Syntax y = spcharout(x)
%
%Description
%   x is a character or 2D cell array of characters
%   y is the corresponding version with special character codes 
%     '%ascii;' replaced by their values
%
%See also: spcharin
%
% Jonas Almeida, almeidaj@musc.edu 8 Oct 2002, MAT4NAT Tbox
% 
% Harold Figueroa, hkf1@cornell.edu, 06-Sep-2007

%--
% handle input
%--

% NOTE: we assume a cell array is a string cell array

if iscell(x)
	y = iterate(mfilename, x); return;
end

if ~ischar(x)
	error(['String expected, ', upper(class(x)), ' input found instead.']);
end

%--
% recover string
%--

% NOTE: no hash means no replacements

if no_hash(x)
	return;
end

% NOTE: we capture the special character tokens build a string that evaluates character codes

x = eval(['[''',regexprep(x,'\#(\d+);',''',char($1),'''),''']']);


%-----------------
% NO_HASH
%-----------------

function value = no_hash(x)

value = isempty(find(x == '#', 1));

