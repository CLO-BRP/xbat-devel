function y = spcharin(x)

%SPCHARIN replaces special characters by their codes
%
% Syntax y = spcharin(x)
%
% Description
%   x is a character or 2D cell array of characters
%   y is the corresponding version with reserved (special) characters
%   replaced by '%ascii;' codes
%
% See also: SPCHAROUT
%
% Jonas Almeida, almeidaj@musc.edu, 8 Oct 2002, MAT4NAT Tbox
%
% Harold Figueroa, hkf1@cornell.edu, 06-Sep-2007

%--
% handle input
%--

% NOTE: we assume a cell array is a string cell array

if iscell(x)
	y = iterate(mfilename, x); return;
end

if ~ischar(x)
	error(['String expected, ', upper(class(x)), ' input found instead.']);
end

%--
% encode string
%--

x = ['AAA', x, 'AAA']; % add polyA header and tail

% NOTE: replace delimiters first '%' and ';'

x = strrep(x, ';', '*59;');

x = strrep(x, '#', '#35;');

x = strrep(x, '*59;', '#59;');

% NOTE: find special characters

ascii = x * 1; 

sp = find(~(((x > 47) & (x < 58)) | ((x > 96) & (x < 123)) | ((x > 64) & (x < 91)) | (x == 59) | (x == 35)));

% NOTE: replace special characters by ASCII code delimited by '%' and ';'

for i = length(sp):-1:1
	x = [x(1:(sp(i)-1)), '#', num2str(ascii(sp(i))), ';', x((sp(i)+1):end)];
end

y = x(4:end-3); % note polyA head and tail are removed
