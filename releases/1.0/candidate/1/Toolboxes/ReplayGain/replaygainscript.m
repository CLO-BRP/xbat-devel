% replaygainscript
% Asks user for name of wavefiles (or folders containing wavefiles)
% User gives null response to indicate all files entered
% Calculates replay gain of file using "replaygain" function

% To enter entire folder, append / or \ (i.e. "maskers/" processes all files in directory "maskers")

% David Robinson, July 2001. http://www.David.Robinson.org/

clear Vrms filenamematrix

% Get filter co-efs for 44100 kkHz Equal Loudness Filter
[a1,b1,a2,b2]=equalloudfilt(44100);

% Calculate perceived loudness of -20dB FS RMS pink noise
% This is the SMPTE reference signal. It calibrates to:
% 0dB on a studio meter / mixing desk
% 83dB SPL in a listening environment (THIS IS WHAT WE'RE USING HERE)
[ref_Vrms]=replaygain('ref_pink.wav',a1,b1,a2,b2);

filename='not empty';
filenumber=0;
filenumber=filenumber+1;
% Ask user for filename to process
filename=input(['enter filename ',num2str(filenumber),' ? '],'s');

% do this loop while ever the user is entering files (user hits enter to proceed to calculation)
while length(filename)>0,
   % Check if user has entered folder name
   if filename(length(filename))=='/' | filename(length(filename))=='\',	% If we're parsing a directory
      % Get directory listing of requested folder
      d=dir([filename '*.wav']);
      % If the folder exists and contains .wav files in teh directory
      if length(d)>0,
         % Store each wavefilename for processing later
         for loop=1:length(d)
            realfilename=d(loop).name;
            filenamematrix(filenumber).name=[filename realfilename(1:length(realfilename)-4)];
            filenumber=filenumber+1;
         end
         filename=input(['enter filename ',num2str(filenumber),' ? '],'s');
      else
         % If the folder does nto exist or contains not .wavs, ask the user for another name
         filename=input(['NOT FOUND. enter filename ',num2str(filenumber),' ? '],'s');
      end
      % If the user has entered a file name (rather than folder)
   else
      	% Add .wav to end if user failed to include it
         if isempty(findstr(filename,'.wav')), filename=[filename '.wav']; end
         % Check the file exists
      if ~exist(filename,'file'),
         filename=input(['NOT FOUND. enter filename ',num2str(filenumber),' ? '],'s');
      else
         % If it does, store the file name
         filename=filename(1:length(filename)-4);	% Strip .wav from end
         filenamematrix(filenumber).name=filename;
         filenumber=filenumber+1;
         filename=input(['enter filename ',num2str(filenumber),' ? '],'s');
      end
   end
end

disp(char(13));

% If no files entered, end the program
if filenumber==1, error('Program Aborted: You must type something!!!'); end

% Start a timer to find out how long this takes!
tic;

% Loop through all the files
for loop=1:filenumber-1,
   % Calculate the perceived loudness of the file using "replaygain" function
   % Subtract this from reference loudness to give actual replay gain relative to 83 dB level
Vrms(loop)=ref_Vrms-replaygain(filenamematrix(loop).name,a1,b1,a2,b2);
% Output the result on screen
disp([filenamematrix(loop).name '.wav: ' num2str(Vrms(loop)) ' dB']);
end

disp(char(13));
disp('== ReplayGainScript complete ==');

% Stop timer and display elapsed time
toc
