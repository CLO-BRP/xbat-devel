function result = control__callback(callback, context)

% SENSOR_CALIBRATION - control__callback

result = struct;

%--
% get calibration from fishbowl
%--

calibration = get_control(callback.pal.handle, 'calibration', 'value');

if isempty(calibration)
    calibration = context.attribute.calibration;
end

%--
% get current channel index
%--

channel = get_control(callback.pal.handle, 'channel', 'index');

switch callback.control.name
    
    case 'channel'
        
        set_control(callback.pal.handle, 'offset', 'value', calibration(channel));
        
    case 'offset'
        
        %--
        % modify value for channel
        %--

        calibration(channel) = get_control(callback.pal.handle, 'offset', 'value');
        
        %--
        % store the result
        %--
        
        set_control(callback.pal.handle, 'calibration', 'value', calibration);
           
end

result.calibration = calibration;

result.reference = get_control(callback.pal.handle, 'reference', 'value') * 10^-6;


        
