function attribute = load(store, context)

% SENSOR_CALIBRATION - load

attribute = struct;

attribute = sensor_calibration(store);

if numel(attribute.calibration) ~= context.sound.channels
    attribute = [];
end