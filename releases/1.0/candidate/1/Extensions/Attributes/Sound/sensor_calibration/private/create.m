function attribute = create(context)

% SENSOR_CALIBRATION - create

attribute = struct;

attribute.calibration = zeros(context.sound.channels, 1);

attribute.reference = 20;