function save(attribute, store, context)

% SENSOR_CALIBRATION - save

lines{1} = 'Calibration';

lines{2} = ['Reference, ', num2str(attribute.reference)];

lines{3} = 'dB';

for k = 1:numel(attribute.calibration)
    lines{end + 1} = num2str(attribute.calibration(k));
end

file_writelines(store, lines);