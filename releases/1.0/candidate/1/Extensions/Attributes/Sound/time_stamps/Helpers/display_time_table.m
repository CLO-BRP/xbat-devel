function display_time_table(sound, n)

%--
% handle input
%--

if nargin < 2
    n = 20;
end

if ~nargin
    sound = get_active_sound;
end

if isempty(sound)
    return;
end

%--
% display time table
%--

table = sound.time_stamp.table; 

% if ~isempty(table)
%     table = table + sound.realtime;
% end

disp(' '); 

disp(sound_name(sound)); 
disp(' ');

disp(datestr(sound.realtime));
disp(' ');

N = size(table, 1);

for k = 1:min(n, size(table, 1))
    
    disp(str_implode(sec_to_clock(table(k, :)), ', ')); 
end

if N > n
    disp('...');
end

disp(' ');