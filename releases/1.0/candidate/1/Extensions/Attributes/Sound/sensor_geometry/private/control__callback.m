function result = control__callback(callback, context)

% SENSOR_GEOMETRY - control__callback

result = [];

%--
% get geometry
%--

handles = get_control(callback.pal.handle, 'sensor_geometry', 'handles');

geometry_ax = handles.obj;

geometry = get(geometry_ax, 'userdata');

%--
% get selected channel and reference channel from axes
%--

ch = get_selected_channel(geometry_ax);

if isempty(ch)
    ch = 1;
end

ref = get_reference_channel(geometry_ax);

if isempty(ref)
	[ignore, ref] = min(sum(geometry.local.^2, 2));
end

%--
% perform control callback
%--

value = get_control(callback.pal.handle, callback.control.name, 'value');

switch callback.control.name	
		
	case 'channel'
		
		ch = str2double(value{1});
		
		set_selected_channel(geometry_ax, ch);
		
	case 'reference'
		
		if value	
			ref = ch; set_reference_channel(geometry_ax, ch);
		end
	
	case 'x'
		
		geometry.local(ch, 1) = str2double(value);
			
	case 'y'
		
		geometry.local(ch, 2) = str2double(value);
		
	case 'z'
		
		geometry.local(ch, 3) = str2double(value);
        
	case 'lat'
		
		geometry.global(ch, 1) = str2double(value);
			
	case 'lon'
		
		geometry.global(ch, 2) = str2double(value);
		
	case 'alt'
		
		geometry.global(ch, 3) = str2double(value);  
        
    case 'add'
        
        [geometry, ref, cancel] = georeference_geometry(geometry, ref);
        
        if cancel
            return;
        end
        
        names = {'lon', 'lat', 'alt', 'ellipsoid'};
        
        for k = 1:length(names) 
            set_control(callback.pal.handle, names{k}, 'enable', 1); 
        end
           
        set_control(callback.pal.handle, 'add', 'enable', 0);
			
end

%--
% sync according to what has changed
%--

if ~isempty(geometry.global)

    switch callback.control.name

        case {'x', 'y', 'z'}

            type = 'local';

        case {'lat', 'lon', 'alt'}

            type = 'global';

        otherwise

            type = 'local';

    end

    geometry = sync_geometry(geometry, type, ref);
    
end

%--
% store geometry in 'fishbowl'
%--

set(geometry_ax, 'userdata', geometry);

%--
% plot geometry
%--

geometry_plot(geometry.local, callback.pal.handle, geometry_ax, ch, ref);

%--
% update reference indicators
%--

ch = get_selected_channel(geometry_ax); ref = get_reference_channel(geometry_ax);

set_control(callback.pal.handle, 'reference', 'value', ref == ch);

%--
% update position controls
%--		

set_control(callback.pal.handle, 'x', 'value', num2str(geometry.local(ch, 1)));

set_control(callback.pal.handle, 'y', 'value', num2str(geometry.local(ch, 2)));

set_control(callback.pal.handle, 'z', 'value', num2str(geometry.local(ch, 3)));

if isempty(geometry.global)
    return;
end

set_control(callback.pal.handle, 'lat', 'value', num2str(geometry.global(ch, 1)));

set_control(callback.pal.handle, 'lon', 'value', num2str(geometry.global(ch, 2)));

set_control(callback.pal.handle, 'alt', 'value', num2str(geometry.global(ch, 3)));    
	
	

%-----------------------------------
% GET_SELECTED_CHANNEL
%-----------------------------------

function ch = get_selected_channel(ax)

ch = [];

selection = findobj(ax, 'tag', 'SELECT_LINE');

array = findobj(ax, 'tag', 'ARRAY_LINE');

if isempty(get(selection, 'xdata'))
	return;
end

compare =  ...
	get(selection, 'xdata') == get(array, 'xdata') & ...
	get(selection, 'ydata') == get(array, 'ydata') ...
;

ch = find(compare, 1, 'first');


%----------------------------------
% SET_SELECTED_CHANNEL
%----------------------------------

function set_selected_channel(ax, ch)

if nargin < 2 
	ch = [];
end

selection = findobj(ax, 'tag', 'SELECT_LINE');

array = findobj(ax, 'tag', 'ARRAY_LINE');

x = get(array, 'xdata'); y = get(array, 'ydata');

set(selection, 'xdata', x(ch), 'ydata', y(ch));


%----------------------------------
% GET_REFERENCE_CHANNEL
%----------------------------------

function ch = get_reference_channel(ax)

ch = [];

array = findobj(ax, 'tag', 'ARRAY_LINE');

ref = findobj(ax, 'tag', 'REFERENCE_LINE');

if isempty(get(ref, 'xdata'))
	return;
end

compare = [ ...
	[get(ref, 'xdata') == get(array, 'xdata')] & ...
	[get(ref, 'ydata') == get(array, 'ydata')] ...
];

ch = find(compare, 1, 'first');

%-----------------------------------
% SET_REFERENCE_CHANNEL
%-----------------------------------

function set_reference_channel(ax, ch)

if nargin < 2 
	ch = [];
end

ref = findobj(ax, 'tag', 'REFERENCE_LINE');

array = findobj(ax, 'tag', 'ARRAY_LINE');

x = get(array, 'xdata'); y = get(array, 'ydata');

set(ref, 'xdata', x(ch), 'ydata', y(ch));
