function ext = clock_widget

ext = extension_create(mfilename);

ext.short_description = 'A clock';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'fe8c3380-0ef6-45c8-a63d-05908f82a084';

ext.author = 'Harold Figueroa';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

