function [handles, context] = on__page(widget, data, parameter, context)

% HISTOGRAM - on__page

%--
% get image data
%--

images = get_image_handles(context.par); 

X = [];

for k = 1:length(images)
	Xk = get(images(k), 'cdata'); X = [X; Xk];
end

%--
% get and clear axes
%--

ax = findobj(widget, 'tag', '1d_histogram'); cla(ax);

%--
% compute and display histogram
%--

[h, c] = hist_1d(X, 51); 

if parameter.normalize
	h = h / sum(h);
end

handles = line( ...
	'parent', ax, ...
	'xdata', c, ...
	'ydata', h ...
);

set(ax, ...
	'xlim', [c(1), c(end)], ...
	'ygrid', parameter.grid, ...
	'yscale', parameter.scale ...
);

%--
% compute and display frequency conditional power distribution
%--

ax = findobj(widget, 'tag', '2d_histogram'); cla(ax);

Y = repmat(get_freq_grid(context.sound), 1, size(X, 2));

[H, c] = hist_2d(X, Y);

imagesc([c.x(1), c.x(end)], [c.y(1), c.y(end)], H, 'parent', ax);

% NOTE: the resetting of the tag is needed because we are using 'imagesc'

set(ax, 'ydir', 'normal', 'tag', '2d_histogram');
