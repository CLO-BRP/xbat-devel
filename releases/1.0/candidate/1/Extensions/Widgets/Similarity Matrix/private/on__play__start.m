function [handles, context] = on__play__start(widget, data, parameter, context)

% SIMILARITY MATRIX - on__play__start

handles = [];

%--
% get relevant times
%--

page = data.page;

time = [page.start, page.start + page.duration];

current = get_play_time(context.par); 

%--
% create and update cursors
%--

ax = similarity_matrix_axes(widget)

if isempty(ax)
	return;
end

player = sound_player;

get_cursor(ax, 1, ...
	'parent', ax, ...
	'color', player.color, ...
	'marker', 's', ...
	'xdata', current * ones(1,2), ...
	'ydata', time ...
);

get_cursor(ax, 2, ...
	'parent', ax, ...
	'color', player.color, ...
	'marker', 's', ...
	'ydata', current * ones(1,2), ...
	'xdata', time ...
);

