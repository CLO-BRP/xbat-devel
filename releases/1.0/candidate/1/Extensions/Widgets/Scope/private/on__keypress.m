function [handles, context] = on__keypress(widget, data, parameter, context)

% SCOPE - on__keypress

handles = [];

%--
% check for modifiers
%--

if string_is_member('shift', data.Modifier)
	frac = 0.5;
else
	frac = 0.2;
end 

%--
% check key 
%--

switch data.Key

	%--
	% marker move events
	%--
	
	case 'leftarrow'
		
		offset_marker(context.par, -frac * parameter.duration);
		
	case 'rightarrow'
		
		offset_marker(context.par, frac * parameter.duration);

	case 'uparrow'
		
		start_marker(context.par);
		
	case 'downarrow'
		
		end_marker(context.par);
		
	%--
	% hand off to parent
	%--
	
	% NOTE: this hands off the keypress event to the browser keypress
	
	otherwise

		browser_keypress_callback(widget, data, context.par);
		
end 


%-----------------------
% OFFSET_MARKER
%-----------------------

function offset_marker(par, offset)

m = get_browser_marker(par); m.time = m.time + offset; set_browser_marker(par, m); 


%-----------------------
% START_MARKER
%-----------------------

function start_marker(par)

page = get_browser_page(par);

m = get_browser_marker(par); m.time = page.start; set_browser_marker(par, m); 


%-----------------------
% END_MARKER
%-----------------------

function end_marker(par)

page = get_browser_page(par);

m = get_browser_marker(par); m.time = page.start + page.duration; set_browser_marker(par, m); 


