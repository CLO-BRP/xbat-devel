function [handles, context] = on__page(widget, data, parameter, context)

% BIG TIME - on__page

handles = display_big_time(widget, context.page.start, context);

set(widget, 'name', [context.ext.name, ' (Page)']);
