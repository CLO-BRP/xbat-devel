function [handles, context] = on__play__update(widget, data, parameter, context)

% BIG TIME - on__play__update

%--
% get play time
%--

time = get_play_time(context.par); 

%--
% display play time
%--

handles = display_big_time(widget, time, context);
