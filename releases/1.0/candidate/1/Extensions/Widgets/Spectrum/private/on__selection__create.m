function [handles, context] = on__selection__create(widget, data, parameter, context)

% SPECTRUM - on__selection__create

%--
% clear previous selection widget display
%--

on__selection__delete(widget, data, parameter, context);

%--
% get selection data and widget display axes
%--

% NOTE: create convenient names

selection = data.selection; event = selection.event; 

ax = spectrum_axes(widget, event.channel); handles = [];

if isempty(ax)
	return;
end

%--
% get selection spectrogram axes
%--

% NOTE: here we get the browser displayed spectrogram axes, from these we will get the contained image

if isempty(selection.handle)
	event.axes = get_channel_axes(context.par, event.channel);
else
	event.axes = get(selection.handle(1), 'parent');
end

%--
% get selection slice data
%--

% NOTE: the current code uses persistent storage of the spectrogram integral to compute mean efficiently

% [slices, freq] = get_spectrogram_slices(event, context); slice = mean(slices, 2);

[slice, freq] = get_spectrogram_slice_mean(event, context);

freq = linspace(freq(1), freq(2), numel(slice));

%--
% display spectrum
%--

handles(end + 1) = selection_line(ax, event.channel, ...
	'color', selection.color, ...
	'xdata', freq, ...
	'ydata', slice ...
);

%--
% display spectral peaks
%--

% TODO: add configuration parameters, whether to display for example

% NOTE: also configure the display of guides and the number of peaks to display

opt = peak_display; opt.color = selection.color; opt.peaks = parameter.peaks;

handles = [handles(:)', peak_display(ax, freq, slice, 'selection_peaks', opt)];

% NOTE: we could 'find' this handle, however for performance we control and depend on packing in 'peak_display'

%--
% update label to contain time
%--

value = get(handles(end), 'userdata');

if isempty(value)
	return;
end 

str = get_peak_value_string(mean(event.time), value, context);

set(handles(end), 'string', str);

%--
% display frequency guides
%--

handles(end + 1) = frequency_patch(ax, selection);








