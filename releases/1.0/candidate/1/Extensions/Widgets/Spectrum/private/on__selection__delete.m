function [handles, context] = on__selection__delete(widget, data, parameter, context)

% SPECTRUM - on__selection__delete

handles = [];

%--
% delete selection lines and frequency guides
%--

channels = context.page.channels;

for k = 1:length(channels)
	
	channel = channels(k);
	
	ax = spectrum_axes(widget, channel);

	delete([selection_line(ax, channel), frequency_guides(ax, channel)]);

	delete_obj(ax, 'selection_peaks');
	
	delete_obj(ax, 'selection_band');
	
end
