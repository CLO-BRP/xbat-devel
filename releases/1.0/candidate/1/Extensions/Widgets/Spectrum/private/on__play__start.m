function [handles, context] = on__play__start(widget, data, parameter, context)

% SPECTRUM - on__play__start

handles = [];

%--
% get player buffer and display options
%--

buffer = get_player_buffer;

if isempty(buffer)
	return;
end

player = sound_player;

%--
% hide peak display
%--

label = findobj(widget, 'tag', 'PEAK_LABEL_TEXT');

if ~isempty(label)
	set(label, 'visible', 'off');
end

%--
% create play lines in channels
%--

for k = 1:length(buffer.channels)
	
	channel = buffer.channels(k);
	
	ax = spectrum_axes(widget, channel);
	
	if isempty(ax)
		continue;
	end
	
	handles(end + 1) = play_line(ax, channel, ...
		'color', player.color ...
	);
	
end

%--
% create peak display
%--

opt = peak_display; opt.color = player.color; opt.peaks = parameter.peaks;

peak_display(ax, [], [], 'play_peaks', opt);

