function [slice, freq] = get_spectrogram_slice_mean(event, context)

% get_spectrogram_slices - get slices from spectrogram
% ----------------------------------------------------
%
% [slice, freq] = get_spectrogram_slices(event, context)
%
% Input:
% ------
%  event - event
%  time - context
%
% Output:
% -------
%  slice - spectrogram slice mean
%  freq - frequency limits

% TODO: allow for the slice getting to use the cache as well? we should not store two images

%-------------------
% SETUP
%-------------------

%--
% get slice times from event
%--

time = get_field(event, 'time', []);

%--
% get the channel axes and the contained image
%--

% ax = get_channel_axes(context.par, event.channel);

ax = event.axes;

im = findobj(ax, 'type', 'image');

if isempty(im)
	return;
end

%--
% get spectrogram image integral
%--

integral = get_integral(im);

%--
% get frequency bounds in Hz from image
%--

freq = get(im, 'ydata');

if context.display.grid.freq.labels(1) == 'k'
	freq = 1000 * freq;
end

%--
% get slice indices from event and image times
%--

cols = size(integral.value, 2); times = get(im, 'xdata'); 

if isempty(time)
	ix = [];
else
	ix = round(cols * (time - times(1)) / diff(times));
end

% NOTE: this is a strange condition, we don't want to infer start and end

if any(ix < 1) || any(ix > cols)
	return;
end
	
%--
% compute slice mean using integral
%--

% TODO: consider fast computation of deviation 

switch length(ix)
	
	% NOTE: if no selection criteria are proposed we will the full mean
	
	case 0
		
		slice = integral.value(:, end) / size(integral.value, 2);
		
	% NOTE: the following two cases handle the starting boundary condition
	
	case 1
		
		if ix == 1
			slice = integral.value(:, 1);
		else
			slice = integral.value(:, ix) - integral.value(:, ix - 1);
		end 
		
	case 2
		
		n = ix(2) - ix(1) + 1; 
		
		ix(1) = ix(1) + (ix(1) == 1); slice = (integral.value(:, ix(2)) - integral.value(:, ix(1) - 1)) / n;
		
end


%--------------------
% GET_INTEGRAL
%--------------------

function integral = get_integral(handle)

%--
% try to get from cache
%--

integral = integral_cache(handle); 

if ~isempty(integral)
	return;
end

%--
% compute and store in cache
%--

integral = compute_integral(handle);

integral_cache(handle, integral);


%--------------------
% INTEGRAL_CACHE
%--------------------

function integral = integral_cache(handle, integral)

%--
% create persistent store 
%--

persistent INTEGRAL_STORE

if isempty(INTEGRAL_STORE)
	INTEGRAL_STORE = empty(struct); 
end

%--
% compute key from handle
%--

% NOTE: we assume that the source image is updated not recreated

if ~trivial(INTEGRAL_STORE)
	ix = find(handle == [INTEGRAL_STORE.handle], 1);
else 
	ix = [];
end

%------------
% SET
%------------

if nargin > 1
	
	if isempty(ix)
		INTEGRAL_STORE(end + 1).handle = handle; INTEGRAL_STORE(end).integral = integral; return;
	else
		INTEGRAL_STORE(ix).integral = integral;
	end
	
end
	
%------------
% GET
%------------
	
%--
% check for integral using key
%--

if isempty(ix)
	integral = []; return;
end

%--
% get integral and check it is current
%--

% NOTE: we assume that the image 'xdata' and 'ydata' are faithful indicators of current content

integral = INTEGRAL_STORE(ix).integral;

if ~isequal(integral.xdata, get(handle, 'xdata')) || ~isequal(integral.ydata, get(handle, 'ydata'))
	integral = []; return;
end


%--------------------
% COMPUTE_INTEGRAL
%--------------------

function integral = compute_integral(handle)

integral.value = cumsum(get(handle, 'cdata'), 2);

integral.xdata = get(handle, 'xdata'); 

integral.ydata = get(handle, 'ydata');

