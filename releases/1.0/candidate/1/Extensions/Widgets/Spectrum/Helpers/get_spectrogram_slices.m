function [slices, freq] = get_spectrogram_slices(event, context)

% get_spectrogram_slices - get slices from spectrogram
% ----------------------------------------------------
%
% [slices, freq] = get_spectrogram_slices(event, context)
%
% Input:
% ------
%  event - event
%  time - context
%
% Output:
% -------
%  slices - spectrogram slices
%  freq - frequency limits

slices = []; freq = [];

%-------------------
% HANDLE INPUT
%-------------------

%--
% get slice times from event
%--

% NOTE: the sort is just a little healthy paranoia

if isfield(event, 'time')
	time = sort(event.time);
else
	time = [];
end

%-------------------
% SETUP
%-------------------

%--
% get spectrogram data from image
%--

% TODO: make sure we always have the axes

if isfield(event, 'axes')
	ax = event.axes;
else
	ax = get_channel_axes(context.par, event.channel);
end

im = findobj(ax, 'type', 'image');

if isempty(im)
	return;
end

X = get(im, 'cdata'); cols = size(X, 2); times = get(im, 'xdata'); 

%--
% get frequency bounds in Hz
%--

freq = get(im, 'ydata');

if context.display.grid.freq.labels(1) == 'k'
	freq = 1000 * freq;
end

%--
% compute slice indices from slice and image times
%--

if isempty(time)
	ix = [];
else
	ix = round(cols * (time - times(1)) / diff(times));
end

% NOTE: this is a strange condition, we don't want to infer start and end

if any(ix < 1) || any(ix > cols)
	return;
end
	
%--
% get slices
%--

switch length(ix)
	
	% NOTE: if no selection criteria are proposed we will get all slices
	
	case 0, slices = X;
		
	case 1, slices = X(:, ix);
		
	case 2, slices = X(:, ix(1):ix(2));
		
end


