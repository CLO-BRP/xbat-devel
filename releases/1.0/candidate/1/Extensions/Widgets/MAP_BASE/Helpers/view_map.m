function [par, handle] = view_map(map, par)

%--
% create figure or set focus
%--

% NOTE: passing a figure is very destructive right now

if nargin < 2
	par = fig;
else
	clear(par); figure(par);
end

%--
% display map image
%--

handle.image = image(map.image);

handle.axes = get(handle.image, 'parent');

axis(handle.axes, 'image'); set(handle.axes, 'ydir', 'normal');

set(par, 'color', zeros(1, 3));

set(handle.axes, 'xcolor', 0.5 * ones(1, 3), 'ycolor', 0.5 * ones(1, 3));