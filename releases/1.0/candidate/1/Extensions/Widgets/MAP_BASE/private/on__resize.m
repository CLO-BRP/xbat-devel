function [handles, context] = on__resize(widget, data, parameter, context)

% MAP_BASE - on__resize

%--
% get map axes and size in pixels
%--

handles = harray_select(widget, 'level', 1);

% NOTE: this means that this callback gets called twice, and manually here

harray_resize(widget);

pix = get_size_in(handles, 'pixels'); 

% disp(' '); disp(mat2str(pix));

%--
% update map display
%--

% TODO: this will be a function, it will also be called from the keypress function as the various parameters change

parameter

map = get_virtual_earth_map(parameter.lon, parameter.lat, parameter.level)

