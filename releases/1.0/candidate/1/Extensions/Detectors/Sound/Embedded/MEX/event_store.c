#include <sqlite3.h>
#include <sndfile.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/stat.h>
#include "detect.h"

#ifndef LINE_MAX
   #define LINE_MAX 256
#endif

#define LOCK_FILE	"/tmp/detect_write.lock"

/*
 * the event buffer
 */

event_t events[100];

static int n_events = 0;

static time_t buffer_time;

static float report_thresh = 8.;

static float clip_thresh = 6.;

float store_thresh = 4.;

/*
 * various event accessors
 */

int get_n_events()
{
	return n_events;	
}

void set_n_events(int n){
	n_events = n;
}

void inc_n_events(int n){
	n_events+=n;
}

event_t * get_events(){
	return events;
}

time_t get_time(){
	return buffer_time;
}

void set_time(time_t time){
	buffer_time = time;
}

float get_report_thresh(){
	return report_thresh;
}

void set_report_thresh(float t){
	report_thresh = t;
}

float get_clip_thresh(){
	return clip_thresh;
}

void set_clip_thresh(float t){
	clip_thresh = t;
}

float get_store_thresh(){
	printf("getting store threshold: %f\n", store_thresh);
	return store_thresh;
}

void set_store_thresh(float t){
	printf("setting store threshold: %f\n", t);
	store_thresh = t;
}

/*
 * notify - put a file in the filesystem to tell the reporting process to go now
 */

void notify(float score){
	
	FILE * fp;
	
	fp = fopen("/tmp/report", "w");
	fprintf(fp, "%f\n", score);
	
	fclose(fp);
	
}

/*
 * fexist - check for file existence using stat()
 *------------------------------------------------*/

static int fexist(char * filename) {
	
	struct stat buffer;
		
	if (stat(filename, &buffer))
		return 0;
	else
		return 1;
	
}

/*
 * get_id - get the hardware id associated to this machine
 *---------------------------------------------------------*/

static void get_id(char * id_str){
	
	FILE * fp;
	
	char line[LINE_MAX];
	
	char * sp, * id;
	
	id = NULL;
	
	fp = fopen("/proc/cpuinfo", "r");
	
	if (fp == NULL){
		sprintf(id_str, "unknown"); return;
	}
	
	while (fgets(line, LINE_MAX, fp) != NULL){ 
		
		if (strlen(line) < 7) continue;
		
		if (strncmp(line, "Serial", 6) != 0){
			continue;
		}
		
		sp = strrchr(line, ':');
		
		if (sp)
			id = sp+2;
		
		/*
		 * replace carriage return with string terminator
		 */
		
		sp = strrchr(line, '\n');
	
		if (sp)
			* sp = '\0';
		
		break;
		
	}
	
	fclose(fp);
	
	if (id == NULL)
		id = "unknown";
	
	memcpy(id_str, id, strlen(id));
	
}


/*
 * database_init - initialize sqlite database with our schema
 *-----------------------------------------------------------*/

static sqlite3 * database_init(char * filename) {
	
	char *errmsg;
	
	char sql[512];
	
	char id[64];
	
	int init, status, format;
	
	static sqlite3 * db;
	
	/*
	 * sql for creating an event table
	 */

	static char event_table[] = 
	"CREATE TABLE IF NOT EXISTS events( \
	id INTEGER NOT NULL PRIMARY KEY, tags VARCHAR, \
	rating INT, notes TEXT, score REAL, channel INT, \
	time DATETIME, time_str TEXT, freq REAL, duration REAL, bandwidth REAL, \
	file TEXT, samples TEXT, rate REAL, data TEXT, author VARCHAR NOT NULL, \
	created DATETIME NOT NULL, modified DATETIME );";
	
	/* 
	 * sql for creating meta data table
	 */
	
	static char meta_table[] = 
	"CREATE TABLE IF NOT EXISTS meta( \
	format REAL, hw_id TEXT, gps_time_used INTEGER, \
	gps_lat_dec_deg REAL, gps_long_dec_deg REAL, \
	batt_volts REAL );";
	
	format = 1;
	
	/* 
	 * create database if it doesn't exist
	 */
	
	init = !fexist(filename);
	
	status = sqlite3_open(filename, &db);
	
	if (status){
		printf("can't open database file!\n"); 
	}
	
	if (!init)
		return db;
	
	/* 
	 * create the tables
	 */
	
	status = sqlite3_exec(db, event_table, NULL, NULL, &errmsg);
	
	if (status){
		printf("can't create event table! %s\n", errmsg);
	}
	
	status = sqlite3_exec(db, meta_table, NULL, NULL, &errmsg);
	
	if (status){
		printf("can't create metadata table! %s\n", errmsg); 
	}
	
	/*
	 * populate the meta data table
	 */
	
	get_id(id);
	
	sprintf(sql, "INSERT INTO meta VALUES (%d, '%s', 0, 0, 0, 0);", format, id);
	
	status = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
	
	if (status){
		printf("can't write metadata! %s\n", errmsg); 
	}
	
	return db;
	
}

/*
 * write_events - write events to sqlite database
 *---------------------------------------------------------*/
 
void write_events(int rate){

	int status, k, n, ix;
	float report;
	
	static int file_count = 0;
	
	sqlite3 *db;
	
	time_t event_time;
	
	char * errmsg;
	char fname[64];
	char db_fname[64];
	char sql[4096];
	
	char * sql_p, * fname_trim;
	
	event_t * events;
	
	/*
	 * Check for lockout on writing.  Note that we don't reset the event pointer.
	 */
	
	if (fexist(LOCK_FILE)){
		printf("can't write events, locked!\n"); return;
	}
	
	/*
	 * open and initialize database
	 */
	
	db = database_init("/tmp/save/dump.db");
	
	if (db == NULL){
		set_n_events(0); return;
	}
	
	/* 
	 * get the events
	 */
	
	events = get_events(); report = report_thresh;
	
	/* 
	 * start an SQLite transaction string
	 */
	
	sql_p = sql;
	
	n = sprintf(sql_p, "BEGIN;");
	
	if (n >= 0)
		sql_p += n;
	
	for (k = 0; k < get_n_events(); k++){
	
		/*
		 * NOTE: currently time is discretized to seconds
		 */
		
		event_time = buffer_time + events[k].t_in / rate;

		if (events[k].score < store_thresh)
			continue;
		
		/*
		 * get the event file name and store a file if the event is good enough
		 */
		
		if (events[k].score > clip_thresh){
			
			sprintf(fname, "/tmp/save/file%010d.aiff", file_count);
			
			ix = get_sample_buffer_index() - BUFFER_LATENCY;
			
			write_sample_buffer_file(fname, rate, events[k].t_in, events[k].t_in + events[k].t_dt, ix); 
			
			file_count++;
			
			/*
			 * trim file path
			 */
			
			fname_trim = strrchr(fname, '/');
			
			if (fname_trim == NULL)
				fname_trim = fname;
			
			sprintf(db_fname, "'%s'", fname_trim + 1);
			
		} else {
			
			sprintf(db_fname, "NULL");
		
		}
		
		/*
		 * generate the SQL to insert this event
		 */
		
		sql_p += sprintf(sql_p, 
			"INSERT INTO events VALUES (\
			NULL, NULL, NULL, NULL, %f, \
			1, datetime(%d, 'unixepoch'), \
			'%s', %f, %f, %f, %s, NULL, %d, \
			'%s', 'autobuoy', date('now'), NULL);",
			events[k].score,
			(int) event_time,
			asctime(gmtime(&event_time)),
			events[k].f_lo,
			((float) events[k].t_dt / (float) rate),
			events[k].f_df,
			db_fname,
			rate,
			events[k].data ? events[k].data : ""
		);
		
		if (events[k].score > report) {
			report = events[k].score;
		}
		
		/*
		 * free extension data associated to this event
		 */
		
		if (!events[k].data)
			free(events[k].data);
		
	}
	
	/*
	 * terminate the sql string with "COMMIT" and run it on the database
	 */
	
	sql_p += sprintf(sql_p, "COMMIT;");
	
	status = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
		
	if (status){
		printf("%s\n", errmsg);
	}
	
	sqlite3_close(db);

	/*
	 * set event pointer back to zero
	 */
	
	set_n_events(0);
	
	/*
	 * set reporting flag if necessary
	 */
	
	if (report > report_thresh)
		notify(report);
	
}


