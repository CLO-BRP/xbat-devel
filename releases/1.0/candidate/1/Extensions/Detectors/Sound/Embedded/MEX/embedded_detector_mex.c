#include <mex.h>
#include <detect.h>

#define N_EVENT_FIELDS 4
#define N_EVENT_ALLOCATE 100


void mex_disp_sg(sample_t * sg, int slices, int nbins, int hi)
{
	
	int j, k;
	
	char sym;
	
	for (j = 0; j < slices; j++){
		
		for (k = 0; k < nbins; k++){
			
			if (sg[j * nbins + k] < 10){
				sym = ' ';
			}
			
			else if (sg[j * nbins + k] < 100){
				sym = '.';
			}
			
			else if (sg[j * nbins + k] < 1000){
				sym = '~';
			}		

			else
				sym = '#';
			
			mexPrintf("%c", sym);
			
		}
		
		mexPrintf("\n");
		
	}
	
	
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    int nSamples, nName, result, i, j, k, nEvents, nEventsAllocated, n_slices;
    
    sample_t * in;
    sound_setup * setup;
    
    char * libname;
    char * config_file;
    
    void * lib_handle;
    
    mxArray * value, * out; 
    double * mat_pr;
    
    const char * fieldnames[N_EVENT_FIELDS] = {"time", "freq", "score", "data"};
    event_t * events;
    sample_t * sg;
    
    /*
     * get input data
     */
    
    nSamples = mxGetN(prhs[0])*mxGetM(prhs[0]);
    in = (sample_t *) mxGetData(prhs[0]);
    
    /*
     * get plugin name
     */
    
    nName = mxGetM(prhs[1]) * mxGetN(prhs[1]);
    libname = malloc(nName + 1);
    mxGetString(prhs[1], libname, nName + 1);
    
    /*
     * get configuration file
     */
    
    nName = mxGetM(prhs[2]) * mxGetN(prhs[2]);
    config_file = malloc(nName + 1);
    mxGetString(prhs[2], config_file, nName + 1);
    
    /*
     * get point position
     */
   
    if (nrhs > 3){ 
        set_point_pos((int) mxGetScalar(prhs[3]));
    }
    
    /*
     * create output event array and allocate room for 100 events
     */
    
    plhs[0] = mxCreateStructMatrix(0, 1, N_EVENT_FIELDS, fieldnames); nEventsAllocated = 0;
    
    /*
     * initialize detector
     */
    
    result = initialize_gateway(libname, config_file);
    
    if (result){
        mexPrintf("initialization failed with code %d!\n", result); return;
    }
    
    setup = setup_sound();
    
    if (setup->rate == 0)
        mexPrintf("un oh\n");
    
    /*
     * iterate over samples using the detector's preferred buffer size
     */
    
    nEvents = 0;
    
    for (k = 0; k < nSamples - setup->N; k += setup->N){
        
        result = detect_gateway(in + k, setup->N, 0);
        
        /*
         * make room for more events as necessary
         */
        
        if ((nEvents + result) > nEventsAllocated){
            
            nEventsAllocated += N_EVENT_ALLOCATE;
            
            mxSetData(
                plhs[0], mxRealloc(mxGetData(plhs[0]), nEventsAllocated * sizeof(mxArray *))
            );
            
        }
        
        /*
         * do something with the events
         */
        
        events = get_events();
        
        for (j = 0; j < result; j++){
        
            if (events[j].score < 1.0)
                continue;
            
            /*
             * time 
             */
            
            value = mxCreateDoubleMatrix(1, 2, mxREAL); mat_pr = mxGetPr(value);
            
            /*
             * NOTE: we need to add the buffer time and convert from samples to seconds
             */
            
            mat_pr[0] = (double) (k + events[j].t_in) / (double) setup->rate; 
            mat_pr[1] = mat_pr[0] + (double) events[j].t_dt / (double) setup->rate;
            
            mxSetField(plhs[0], nEvents, "time", value);
            
            /*
             * frequency 
             */
            
            value = mxCreateDoubleMatrix(1, 2, mxREAL); mat_pr = mxGetPr(value);
            
            /*
             * No conversion is necessary since events report real frequencies
             */
            
            mat_pr[0] = (double) events[j].f_lo; 
            mat_pr[1] = (double) events[j].f_lo + events[j].f_df;
            
            mxSetField(plhs[0], nEvents, "freq", value);
            
            /*
             * score
             */
            
            value = mxCreateDoubleScalar((double) events[j].score);
            
            mxSetField(plhs[0], nEvents, "score", value);
            
            /*
             * data
             */
            
            value = mxCreateString((const char *) events[j].data);
            
            free(events[j].data);
            
            //value = mxCreateDoubleMatrix(0, 0, mxREAL);
            
            mxSetField(plhs[0], nEvents, "data", value);
            
             
            /* increment event counter */
            
            nEvents++;
        
        } 
            
        set_n_events(0);
        
    }
    
    cleanup_detector(parameters_access(1, NULL), state_access(1));
    
    mxSetM(plhs[0], nEvents);
    
    
    
}
