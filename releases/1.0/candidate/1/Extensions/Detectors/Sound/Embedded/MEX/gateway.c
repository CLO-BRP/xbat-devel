
#ifdef PCWIN
    #include <edll/edll.h>
    #define LOAD_PLUGIN load_plugin_windows
    #define PLUGIN edll_module
#else
    #include <dlfun.h>
    #define LOAD_PLUGIN load_plugin_linux
    #define PLUGIN void
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <time.h>

#include "detect.h"
#include "detect_methods.h"


PLUGIN * lib_handle;

void * parameters_access(int mode, const char * config_file){
	
	static void * parameters = 0;
	
	if (mode && parameters != 0)
		return parameters;
	
	if (parameter_initialize)
		parameters = (void *) parameter_initialize(config_file);
	
	return parameters;
	
}

void * state_access(int mode){
	
	static void * state = 0;
	
	if (mode && state != 0)
		return state;
	
	if (state_initialize)
		state = (void *) state_initialize(parameters_access(1, NULL));
	
	return state;
	
}

sound_setup * setup_sound(){
	
	sound_setup * p = 0;
	
	if (setup)
		p = setup(parameters_access(1, NULL));
	
	return p;
	
}

int load_plugin_windows(const char * lib_name)
{
    char * lib_path, * lib_name_trim;
    
	/*
	 * get extension name
	 */
	
	if (!lib_name)
		lib_name = "";
    
    lib_path = strdup(lib_name);
    lib_name_trim = strrchr(lib_path, '\\');
    
    if (!lib_name_trim)
        lib_name_trim = lib_name;
    else
        *(lib_name_trim++) = '\0';
    
    /*
     * Initialize the edll library 
     */
    
	if(edll_init() != 0) {
		return -1;
	}
    
	if(edll_setsearchpath (lib_path) != 0) {
		edll_exit(); return -2;
	}
    
	/*
	 * try to open extension
	 */
    
	lib_handle = edll_open (lib_name_trim);
	
	if (!lib_handle){
		mexPrintf("%s\n", edll_strerror()); edll_exit(); return -3;
	}
    
    free(lib_path);
    
	/*
	 * populate functions
	 */
	
	parameter_initialize = edll_sym(lib_handle, "parameter_initialize");
	state_initialize = edll_sym(lib_handle, "state_initialize");
	detect = edll_sym(lib_handle, "detect");
	setup = edll_sym(lib_handle, "setup");
	data_to_str = edll_sym(lib_handle, "data_to_str");
    cleanup = edll_sym(lib_handle, "cleanup");
    
}

int unload_plugin_windows(void)
{
    if (NULL != lib_handle)
        edll_close(lib_handle);
    
    edll_exit();
}

int initialize_gateway(const char * lib_name, const char * config_file)
{
	
    LOAD_PLUGIN(lib_name);
    
	/*
	 * actually initialize the detector
	 */
	
	parameters_access(0, config_file);
	
	state_access(0);
	
	return 0;
	
}

int cleanup_detector(void * parameters, void * state)
{
    
    cleanup(parameters, state);
    
    if (NULL != lib_handle)
        edll_close(lib_handle);
    
    edll_exit();
    
}

int detect_gateway(sample_t * buf, int nbuf, time_t tbuf)
{
	
	int result;
	void * parameters;
	void * state;
	sound_setup * setup;
	
	/*
	 * get parameters and state from persistent storage
	 */
	
	parameters = parameters_access(1, NULL);
	state = state_access(1);
	setup = setup_sound();
	
	/*
	 * call the detector
	 */
	
    if (NULL == detect)
        return 0;
    
    result = detect(get_events(), buf, nbuf, parameters, state, setup); 
	
	return result;
	
}
