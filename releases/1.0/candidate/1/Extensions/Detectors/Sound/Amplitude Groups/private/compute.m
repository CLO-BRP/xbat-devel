function [event, value, context] = compute(page, parameter, context)

% AMPLITUDE GROUPS - compute

%--
% call parent to perform band amplitude detection
%--

% event = empty(event_create);

fun = parent_fun(mfilename('fullpath')); [event, value, context] = fun(page, parameter, context);

% NOTE: for single events or no events there is nothing to do, we are done

if numel(event) < 2
	return;
end

%--
% configure spectrogram correlation
%--

% TODO: set spectrogram parameters in some reasonable way for the correlation computation

% NOTE: we should be able to set these based on the existing time and frequency related parameters

% NOTE: this should move to parameter compile as well

sound = context.sound; sound.specgram = fast_specgram; 

sound.specgram.fft = 768; sound.specgram.hop = 0.125; sound.specgram.win_length = 0.5;

opt = image_corr; opt.pad_row = 0; opt.pad_col = 1; parameter.opt = opt;

%--
% separate and process each chain
%--

% NOTE: we partition the events into chains and clear the event array

chain = get_event_chains(event); event = empty(event_create);

for i = 1:numel(chain)
	
	%--
	% simply output short chains
	%--
	
	if numel(chain(i).element) < parameter.min_chain_length

		for k = 1:numel(chain(i).element)
			event(end + 1) = chain(i).element(k);
		end
		
		continue; 
	
	end
	
	%--
	% perform correlation analysis on chain
	%--
	
	% NOTE: this may update the initial segmentation, however it will not break the chain
	
	chain(i) = chain_correlation(chain(i), page, sound, parameter, context);
	
	%--
	% output chain events
	%--
	
	for k = 1:numel(chain(i).element)
		event(end + 1) = chain(i).element(k);
	end
	
% 	event(end + 1) = create_group_event(chain(i).element);
	
end

%--
% pack chains in state
%--

% NOTE: this can be used in another detector to make further decisions for event construction and annotation

context.state.chain = chain;


value = repmat(struct('test', 1, 'another', 2), size(event));


%----------------------------------
% CHAIN_CORRELATION
%----------------------------------

function chain = chain_correlation(chain, page, sound, parameter, context)

%--
% make sure that we have all required spectrograms
%--

% NOTE: this is important for the recursive computation

for k = 1:numel(chain.element)
	
	if ~trivial(chain.element(k).specgram)
		continue; 
	end
	
	chain.element(k) = get_event_specgram(chain.element(k), page, sound, parameter, context);
		
end

%--
% initialize correlation, position, and anomaly indicator matrices
%--

C = zeros(numel(chain.element)); P = zeros(size(C)); A = zeros(size(C));

%--
% compute element correlations for chain
%--

% NOTE: we only compute the upper triangle, the lower is part is the same

for j = 1:numel(chain.element)
	
	%--
	% select outer loop element and check spectrogram size
	%--
	
	outer = chain.element(j);

	% NOTE: we ensure that one of the images has an odd number of columns to fix the grid position

	if ~mod(size(outer.specgram.value, 2), 2), outer.specgram.value(:, end + 1) = 0; end
	
	%--
	% loop over upper part of element chain
	%--
	
	for k = j:numel(chain.element)

		%--
		% select inner loop element and compute correlation
		%--
		
		inner = chain.element(k);
		
		[C(j, k), P(j, k)] = max(image_corr(outer.specgram.value, inner.specgram.value, parameter.opt));

		% NOTE: we normalize the row considering the maximum diagonal correlation
		
		C(j, k) = C(j, k) / C(j, j);
		
		%--
		% check for anomalous correlation
		%--
		
		% TODO: this should be factored to allow for adaptive similarity threshold
		
		% NOTE: the correlation is high and the durations disparate means the initial segmentation missed something
		
		if C(j, k) > parameter.similar
			
			% NOTE: positive (negative) values indicate that the row (column) index event needs splitting
			
			if (outer.duration > inner.duration)
				
				A(j, k) = (outer.duration / inner.duration);
			
			elseif (outer.duration < inner.duration)
			
				A(j, k) = -(inner.duration / outer.duration);
			
			end
		
		end
		
	end

end

% DEBUG DISPLAY

if context.debug

	figure(create_figure('test_groups'));
	
	C, D = (C > parameter.similar) .* C
	
	subplot(1,2,1); imagesc(C); colormap(gray);
	
	subplot(1, 2, 2); spy(D);
	
end

%--
% consider anomalous correlations and update chain
%--

% TODO: check for large correlations and disparate duration, find smaller in larger, update element array and correlation matrix

cutter = [];

for k = numel(chain.element):-1:1
	
	% NOTE: we should not cut cutters in the same iteration
	
	% TODO: THIS IS NOT THE CORRECT WAY TO DO THIS AND IS A CRITICAL PART OF THE ALGORITHM
	
	if ismember(k, cutter)
		continue;
	end
	
	%--
	% check if chain element should be cut
	%--
	
	cut = find(A(1:k - 1, k) < -1 * parameter.stretch);
	
	% NOTE: the element does not need to be cut, continue
	
	if isempty(cut)
		continue;
	end

	%--
	% select the best cutter to perform the cut with
	%--
	
	[ignore, best] = max(C(cut, k)); cut = cut(best); cutter(end + 1) = cut;

	if context.debug
		disp([ ...
			'cut ', int2str(k), ' (', num2str(chain.element(k).duration), ...
			') using ', int2str(cut(end)), ' (', num2str(chain.element(cut(end)).duration), ')' ...
		]);
	end
	
	%--
	% cut element and update chain
	%--
	
	% TODO: implement the cut element function, remember that it should not create slivers
	
	parts = cut_element(chain.element(k), chain.element(cut), P(cut, k), parameter);
	
	% NOTE: the cut had no effect, continue;
	
	if numel(parts) == 1

		disp('rejected');
		
		continue;
	
	end
		
	if context.debug
		
		offset = 1000; parts(1).freq = parts(1).freq + offset; parts(2).freq = parts(2).freq - offset; 
		
		disp(mat2str(chain.element(k).time));
		disp(mat2str(parts(1).time));
		disp(mat2str(parts(2).time));
		
		disp(' ');
		
	end
	
	%--
	% update chain to incorporate cut parts
	%--
	
	chain.element = [chain.element(1:k - 1), parts, chain.element(k + 1:end)];
	
end

%--
% recompute the correlations if needed
%--

% chain.correlation.matrix = C; 

return;

% NOTE: this happens when new cuts were introduced, this is recursion

if isempty(cutter)
	
	chain.correlation.matrix = C; 
	
else
	
	chain = chain_correlation(chain, page, sound, parameter, context);
	
end


%---------------------------------------
% CUT_ELEMENT
%---------------------------------------

function parts = cut_element(element, cut, position, parameter)

%--
% compute position of short element 
%--

% NOTE: we should not have to compute this over and over

step = diff(element(1).specgram.time(1:2));

center = element.time(1) + step * position; time = center + 0.5 * [-1, 1] * cut.duration;

%--
% compute left and right margins
%--

% NOTE: we compute the signed distance to the edge of the element to cut, negative values indicate we overflow

left = time(1) - element.time(1); right = element.time(2) - time(2);

%--
% cut long element with the short
%--

% TODO: develop the right shift threshold here

if abs(left) < 16 * step
	
	parts(1:2) = element; parts(1).time(2) = time(2); parts(2).time(1) = time(2); disp('left');
	
elseif abs(right) < 16 * step

	parts(1:2) = element; parts(1).time(2) = time(1); parts(2).time(1) = time(1); disp('right');
	
else
	
	% TODO: currently we skip the cut when it happens in the center,develop this
	
	parts = element; return;
	
end

%--
% check cut is consistent with amplitude
%--

% TODO: there are various problems here, some bugs and the other using the spectrogram amplitude instead of the initial amplitude feature

[ignore, cut] = min(abs(element.specgram.time - parts(1).time(2)));

[sum(element.specgram.value(:, cut)), max(sum(element.specgram.value, 1))]

if sum(element.specgram.value(:, cut)) > 0.25 * median(sum(element.specgram.value, 1))
	
	parts = element; return;
	
end

%--
% accept cut, update event durations and discard samples and spectrogram data
%--

parts = update_event_duration(parts);

% NOTE: this is not the most economical way to get this right, we clear samples and spectrogram to force recomputation

for k = 1:2
	
	parts(k).samples = []; parts(k).specgram = struct;

end


%---------------------------------------
% GET_EVENT_CHAINS
%---------------------------------------

function chain = get_event_chains(event)

%--
% get link information and initialize first chain
%--

previous = [event.previous]; chain(1).element(1) = event(1);

%--
% append or create new chains until we run out of events
%--

for k = 2:numel(event)
	
	% NOTE: no previous event starts a new chain, otherwise extend current chain
	
	% NOTE: we also initialize the last chain's correlation matrix before starting a new chain
	
	if ~previous(k)
		chain(end).correlation.matrix = zeros(numel(chain(end).element)); chain(end + 1).element(1) = event(k);
	else
		chain(end).element(end + 1) = event(k);		
	end

end

% NOTE: in the case of a single chain we need to append the correlation matrix field

if numel(chain) == 1
	chain(end).correlation.matrix = zeros(numel(chain(end).element));
end


%---------------------------------------
% GET_EVENT_SPECGRAM
%---------------------------------------

function event = get_event_specgram(event, page, sound, parameter, context)

%--
% compute event spectrograms for all events
%--

for k = 1:numel(event)

	%--
	% compute spectrogram using 'event_specgram'
	%--
	
	% NOTE: the event spectrogram function needs the event to have proper record time

	event(k).time = event(k).time + page.start;

	event(k) = event_specgram(event(k), sound, context);

	event(k).time = event(k).time - page.start;
	
	%--
	% perform band selection updates to spectrogram data
	%--
	
	% TODO: consider this as an option in 'event_specgram'
	
	rows = event(k).specgram.rows; start = rows(1); stop = rows(2);
	
	event(k).specgram.freq = event(k).specgram.freq(start:stop);
	
	event(k).specgram.value = event(k).specgram.value(start:stop, :);
	
	rows  = [1, stop - start + 1]; event(k).specgram.rows = rows;
	
	% NOTE: we ensure that the number of rows is also odd, the 'rows' field remains valid
	
	if ~mod(rows(2), 2)
		event(k).specgram.value(end + 1, :) = 0;
	end
	
end
