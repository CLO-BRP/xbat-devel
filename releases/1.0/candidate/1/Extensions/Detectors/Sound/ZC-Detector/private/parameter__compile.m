function [parameter, context] = parameter__compile(parameter, context)

% ZC-DETECTOR - parameter__compile

% 

fun = parent_fun(mfilename('fullpath')); [parameter, context] = fun(parameter, context);

context.scan.page.duration = 30;
