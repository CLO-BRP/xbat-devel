function [Xk, ix] = get_component_signal(data, k, rate)

% get_component_signal - synthesize model signal component
% --------------------------------------------------------

%--
% get component check and for model
%--

component = data.component(k);

if isempty(component.model)
	Xk = []; ix = []; return;
end

%--
% get event and component signal indices
%--

start = component.model.t(2);

stop = component.model.t(end - 1);

ix = [floor(start * rate):floor(stop * rate)]; 

n = length(ix);

%--
% create frequency and amplitude vectors
%--

[t, f] = spline_eval(component.model.y, component.model.t, n);

%--
% get linear-grid frequency signal
%--

f = interp1(t, f, linspace(min(t), max(t), length(f)), 'spline');

f = f * 1000;

a = component.amplitude; 

% NOTE: these are ad-hoc amplitude modifications to the amplitude

a(1) = 0; a(2) = 0.75 * a(2); a(end - 1) = 0.75 * a(end - 1); a(end) = 0;

a = interp1(a, linspace(1, length(a), length(f)), 'spline');

%--
% synthesize and add component to model signal
%--

Xk = fm_synth(f, a, rate, 2);
