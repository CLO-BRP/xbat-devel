function [outseg, outval] = simplify_segments(event, value);
%
% SAVI's WARBLER Verison
%
% This function combines and joins overlapping segments. The data in
% [value] is also rearranged and updated.
%

% ---
% TODO: make a generic version of this, implementing interfaces for segment weighting
% and update of the value struct
% ---

% return on single input
if size(segments,1) < 2
    
    outseg = segments;
    outval = value;
    return;
end

% ---
% sweep: 
%
% purpose: decide on overlapping segments' intervals
%
% -timepoints : start/stops of segments
% -stack: segment numbers of segments being active
%
% -compare: segment weights, policy: best segment wins shared timepoints
% -output: active segment for each interval
% ---

% ---
% define segment weights
% we begin using the segments domination
% ---
for i = 1:numel(value)
    segweight(i) = event(i).score;
end

% ---
% generate timepoints and related data
% ---
for i = 1:numel(event)
    segments(i,:) = event(i).time;
end

% the timesteps
% ---
% TODO: covert this to a unique representation, 
% transform fitting segment vector into segment sets
% ---
tsteps = [segments(:,1); segments(:,2)]'; 

% type (start/end of seg)
ttype = [ones(1,size(segments,1)) zeros(1,size(segments,1))]; 

% associated segment number
tseg = [1:size(segments,1) 1:size(segments,1)];

% sort acc. to time
[tsteps,idx] = sort(tsteps);
ttype = ttype(idx);
tseg = tseg(idx);

% ---
% do the sweep
% ---
seglst = [];
winseg = zeros(1,numel(tsteps));
for i = 1:numel(tsteps)
    if ttype(i) == 1     % segment starts
       % add seg
       seglst = [seglst tseg(i)];
       
       % determine best seg
       [null,idx] = max(segweight(seglst));
       
       % save to list
       winseg(i) = seglst(idx);
       
    else                 % segment ends
        % subst seg 
        idx = find(seglst == tseg(i));
        seglst(idx) = [];
        
        if ~isempty(seglst)
           % determine best seg
           [null,idx] = max(segweight(seglst));

           % save to list
           winseg(i) = seglst(idx);
        else
           % close segment
           winseg(i) = 0;
        end
    end
end

% ---
% now we generate a new, non-overlapping segmentation 
% by looking for the segments' nominations
% ---
[idx, class] = num_get_equal_segments(winseg);
idx(:,2) = min(idx(:,2)+1, numel(tsteps));

uniqseg = [];
uniqval = [];
for i = 1:size(idx,1)
    
    % check for trivial segments and save
    if class(i) ~= 0 && tsteps(idx(i,2)) > tsteps(idx(i,1))
        
        uniqseg(end+1,:) = [tsteps(idx(i,1)) tsteps(idx(i,2))];
        
        % tricky generation of the value struct
        if ~isempty(uniqval)
            
            uniqval(end+1) = value(class(i));
        else
            % initialize value struct's structure
            uniqval = value(class(i));
        end 
    end
end

% ---
% TODO: correct phrase analysis and statistics 
% ---


% ---
% get groups of temporally adjacent and similar segments
% check further segment properties
% ---
isadjTS = zeros(1,size(uniqseg,1));
for i = 1:numel(isadjTS)-1
    if similar_segment(uniqseg(i,:), uniqval(i), uniqseg(i+1,:), uniqval(i+1))

        % check in list as similar to following segment
        isadjTS(i) = 1;
    end
end

% ---
% get groups of segments to join,
% join these groups into single segments, 
% save both group and single segments
% ---
idx = num_get_equal_segments(isadjTS);

combseg = [];
combval = [];
i = 1;
while i <= size(uniqseg,1)
    
    % is segment within group or single ?
    if i < size(uniqseg,1) && isadjTS(i)
        
        % get segment's group
        seggrp = find(idx(:,1) <= i,1,'last');
        relsegs = idx(seggrp,1):idx(seggrp,2)+1;
        
        % combine the segments and save into new structure
        if ~isempty(combval)
        [combseg(end+1,:) combval(end+1)] = combine_segments(...
            uniqseg(relsegs,:), uniqval(relsegs));
        else
            % initialize combval
            [combseg(end+1,:) combval] = combine_segments(...
            uniqseg(relsegs,:), uniqval(relsegs));
        end
        
        % fast forward to next untouched segment
        i = i + numel(relsegs);
    else
        
        % copy single segment
        combseg(end+1,:) = uniqseg(i,:);
        
        if ~isempty(combval)
            combval(end+1) = uniqval(i);
        else
            combval = uniqval(i);
        end
        
        i = i + 1;
    end
    
end
% ---
% TODO: kill small segments which have not been joined before. 
% use min_sizeP parameter.
% ---

outseg = combseg;
outval = combval;

% ---
% TODO: generate new events from segmentation
% ---



% ---------------------------------------------------------------------- 
% PRIVATE HELPER FUNCTIONS
% ----------------------------------------------------------------------
function [seg, val] = combine_segments(segin, valin, weights)
% combines the given segments using the longest segment as a basis.

% save final segment's time
seg = [segin(1,1) segin(end,end)];

% ---
% decide on dominant segment,
% which will provide the basic statistics
% ---
tdiffs = diff(segin,1,2);

% get relative portions of time 
tdiffs = tdiffs/max(tdiffs);

[null, domseg] = max(tdiffs);

% copy values of dominant seg, except phrase stats
val = rmfield(valin(domseg), 'phrases');

% ---
% add each segment's phrases
% ---
for i = 1:size(segin,1)
    
    if isfield(valin(i),'phrases') && ~isempty(valin(i).phrases)
        
        for j = 1:numel(valin(i).phrases)
            
            % initialize phrase struct
            if ~isfield(val,'phrases')
                val.phrases = valin(i).phrases(j);
            else
                val.phrases(end+1) = valin(i).phrases(j);
            end
        end
    end
end
% ---
% TODO: correct phrase stats
% ---
val.pstat.pNum = numel(val.phrases);


