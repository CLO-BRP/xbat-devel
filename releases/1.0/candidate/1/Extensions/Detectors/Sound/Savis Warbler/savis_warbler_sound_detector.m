function ext = savis_warbler_basic_sound_detector

ext = extension_inherit(mfilename, periodicity_sound_detector);

ext.short_description = 'Detects the birds stereotypical song';

% ext.category = {};

% ext.version = '';

ext.guid = 'beb41906-fafb-4888-bff4-157183a6fc30';

ext.author = 'Daniel Wolff';

ext.email = 'wolffd.mail@googlemail.com';

ext.url = 'www.uni-bonn.de/~wolffd';