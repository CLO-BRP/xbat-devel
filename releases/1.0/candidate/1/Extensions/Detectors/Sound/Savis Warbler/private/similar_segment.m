function out = similar_segment(ta, vala, tb, valb)
%
% ta, tb: seg times in s,
% vala, valb: phrase stats
%
% compares two segments according to their common properties

% get parameters
% set_params;

% ---
% get temporal proximity
% ---
sim_t = min(abs(ta(2) - tb(1)), abs(tb(2) - ta(1))) < max_distP;


% ---
% get center frequency similarity
% ---
% we use the smallest distance between all pairs of frequency indices
[c, minv, maxv] = diff_all(vala.freq, valb.freq, 1);

sim_cf = 1 - min(centerFtol, minv.val) / centerFtol;

% ---
% get makro repetition rate's similarity
% ---
max_diff_mr = abs(diff(makroRepR));

sim_makrepf = 1 - min(max_diff_mr, abs(vala.pstat.freq - valb.pstat.freq)) / max_diff_mr;

% ---
% conversion from probability to binary decision
% ---
out = (sim_cf * sim_makrepf * sim_t) > 0;