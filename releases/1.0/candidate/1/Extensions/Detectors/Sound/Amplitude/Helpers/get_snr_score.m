function score = get_snr_score(snr, scale)

% NOTE: that the SNR is assumed to be a square to start with, the effect of the parameter is to scale the map

if nargin < 2
	scale = 2;
end

score = snr ./ (scale^2 + snr);

score(isnan(score)) = 1;