function result = parameter__control__callback(callback, context)

% AMPLITUDE DETECTOR I - parameter__control__callback

% result = struct;

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);

switch callback.control.name
	
	case 'join_contiguous' 
		
		set_control(callback.pal.handle, 'break', 'enable', ~get(callback.obj, 'value'));
		
end
