function parameter = parameter__create(context)

% COMPONENTS - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% NOTE: the user parameter should be available as time, the internal parameter as pixels

parameter.min_duration = 0.025;

% db_disp 'set reasonable default parameters'