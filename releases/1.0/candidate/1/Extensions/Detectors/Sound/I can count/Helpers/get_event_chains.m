function chain = get_event_chains(event)

%--
% get link information and initialize first chain
%--

previous = [event.previous]; chain(1).element(1) = event(1);

%--
% append or create new chains until we run out of events
%--

for k = 2:numel(event)
	
	% NOTE: no previous event starts a new chain, otherwise extend current chain
	
	if ~previous(k)
		chain(end + 1).element(1) = event(k);
	else
		chain(end).element(end + 1) = event(k);		
	end

end