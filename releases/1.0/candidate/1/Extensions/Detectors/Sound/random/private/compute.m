function [event, context] = compute(page, parameter, context)

% RANDOM - compute

event = empty(event_create);

%--
% create new random event in this page
%--

start = page.start + rand * page.duration;

duration = parameter.min_length + rand * (parameter.max_length - parameter.min_length);

fluke = event_create;

fluke.time = [start, start + duration];

% TODO: make this random as well

fluke.freq = [100, page.rate/4];

fluke.score = rand;

fluke.channel = floor(1 + rand * context.sound.channels);

% NOTE: we randomly create this event, we may not be lucky

if rand > parameter.probability
	event(end + 1) = fluke;
end