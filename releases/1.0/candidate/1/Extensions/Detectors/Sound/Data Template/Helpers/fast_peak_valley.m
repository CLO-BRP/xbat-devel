function [ix, h, w] = fast_peak_valley(x, opt)

% fast_peak_valley - fast extrema computation
% -------------------------------------------
%
% [ix, h, w] = fast_peak_valley(x, opt)
%
% Input:
% ------
%  x - input sequence
%  opt - type of extrema (def: 0)
%
% Output:
% -------
%  ix - extrema indices
%  h - extrema heights or depths
%  w - extrema widths

% TODO: align input and output for vectors

% TODO: generalize code to handle matrices and take dimension input

%--
% set output option
%--

if (nargin < 2) || isempty(opt)
	opt = 0;
end

%--
% compute peaks and valleys
%--

% TODO: include opt as part of the low level computation

switch nargout
	
	case 1
		
		ix = fast_peak_valley_(x);
		
		%--
		% select max or min extrema if needed
		%--
		
		% NOTE: this approach to separating peaks is not intuitive
		
		if opt
			if opt > 0
				ix = ix(ix > 0);
			else
				ix = -ix(ix < 0);
			end
		end
		
	case 2
		
		[ix, h] = fast_peak_valley_(x);
		
		%--
		% select max or min extrema if needed
		%--
		
		% NOTE: this approach to separating peaks is not intuitive
		
		if opt
			if opt > 0
				tmp = find(ix > 0);
				ix = ix(tmp);
				h = h(:, tmp);
			else
				tmp = find(ix < 0);
				ix = -ix(tmp);
				h = h(:, tmp);
			end
		end
		
	case 3
		
		[ix, h, w] = fast_peak_valley_(x);
		
		%--
		% select max or min extrema if needed
		%--
		
		% NOTE: this approach to separating peaks is not intuitive
		
		if opt
			if opt > 0
				tmp = find(ix > 0);
				ix = ix(tmp);
				h = h(:, tmp);
				w = w(:, tmp);
			else
				tmp = find(ix < 0);
				ix = -ix(tmp);
				h = h(:, tmp);
				w = w(:, tmp);
			end
		end
		
end


