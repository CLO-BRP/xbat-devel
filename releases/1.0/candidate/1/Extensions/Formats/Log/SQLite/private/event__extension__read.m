function [data, id] = event__extension__read(store, id, ext, context)

% SQLITE - event__extension__read

%-------------------------------
% SETUP
%-------------------------------

data = [];

%--
% get extension table names
%--

table = extension_table_names(ext);

%--
% return if table doesn't exist
%--

% TODO: we have to make these calls, we don't know a priori whether the log contains extension data

if ~has_table(store.file, table.value)
	return;
end

%-------------------------------
% GET EXTENSION DATA
%-------------------------------

%------------------
% VALUES
%------------------

%--
% build value select query
%--

sql = ['SELECT * FROM ', table.value, ' v'];

if ~isempty(id)
    sql = [sql, ' WHERE v.event_id IN (', str_implode(id, ', ', @int2str), ')'];
end

sql = [sql, ' ORDER BY v.event_id'];

%--
% execute query and extract retrieved event identifiers from result
%--

[status, value] = sqlite(store.file, 'prepared', sql); 

id = [value.event_id];

%------------------
% PARAMETERS
%------------------

%--
% get expanded parameter values
%--

parameter = struct;

if has_table(store.file, table.parameter)

	% NOTE: get needed parameter identifiers, get such parameters, later we join in MATLAB
	
	sub = ['SELECT DISTINCT parameter_id FROM ', table.value, ' WHERE event_id IN (', str_implode(id, ', ', @int2str), ')'];
	
	sql = ['SELECT * FROM ', table.parameter, ' p WHERE p.id IN (', sub, ');'];

	[status, parameter] = sqlite(store.file, sql);
	
	% NOTE: this was an atrocious!! query, it does not scale at all, why?
	
%     % NOTE: we join on values.parameter_id and parameter.id, the parameters struct comes out the same size as the values
%     
%     sql = [ ...
%         'SELECT p.*, v.parameter_id, v.event_id FROM ', table.parameter, ' p,', table.value, ' v ', ...
%         'WHERE v.parameter_id = p.id ', ...
%         'ORDER BY v.event_id;'
%     ];

end

%------------------
% JOIN AND OUTPUT
%------------------

%--
% setup for join and normalization
%--

persistent AUX;

if isempty(AUX)
	AUX.value = {'event_id', 'parameter_id'}; AUX.parameter = {'id', 'hash'};
end

parameter_id = [parameter.id];

%--
% join, normalize, and pack output
%--

% NOTE: what we are calling normalization consists or removal of auxiliary fields and struct collapse

for k = 1:length(value)
   
    data(k).value = collapse(rmfield(value(k), AUX.value));
    
	data(k).parameter = collapse(rmfield(parameter(parameter_id == value(k).parameter_id), AUX.parameter));
    
end

