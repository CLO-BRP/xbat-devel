function result = event__delete(store, id, context)

% SQLITE - event__delete

% TODO: consider whether we should VACUUM after deletion, this is required to make the file smaller

result = struct;

if nargin < 2 || isempty(id)
    return;
end

%--
% build and execute delete query
%--

query = ['DELETE FROM event WHERE id IN (', str_implode(id, ', ', @int2str), ');'];

sqlite(store.file, query);

%--
% delete corresponding measurement entries
%--

% TODO: this is not done, and rather tricky at the moment

% NOTE: this should perhaps be done using triggers

% TODO: we should create a table that stores the names of the measurement tables so that we can do this in a cross-database way.

sql = 'SELECT * FROM sqlite_master WHERE type=''table'''; 

[status, tables] = sqlite(store.file, sql);

names = {tables.name};

% %--
% % get measurement table names
% %--
% 
% names = names(strmatch('measure', names));
% 
% for k = 1:length(names)
%    
%     sql = ['DELETE FROM ', names{k}, 'WHERE event_id IN', select, ';'];
%     
%     sqlite(store.file, 'prepared', sql); 
%     
% end
