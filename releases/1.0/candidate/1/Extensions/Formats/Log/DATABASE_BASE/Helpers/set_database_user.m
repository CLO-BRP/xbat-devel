function user = set_database_user(file, user, id)

% NOTE: this is not the typical usage for 'user' in the database context

%--
% check for user table
%--

if ~database_has_table(file, 'user')
	error('Database does not have a ''user'' table.');
end

%--
% append or update user based, when we have identifier input
%--

fields = {'name', 'alias', 'email'};

if nargin > 2
	user.id = id; fields{end + 1} = 'id';
end

sqlite(file, sqlite_array_insert('user', user, fields));

%--
% get user from database if requested
%--

if nargout
	user = get_database_user(file, user.name);
end

%--
% clear users cache
%--

% TODO: improve 'get_database_users' so we may clear a single file from cache

clear get_database_users;