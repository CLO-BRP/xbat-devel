function [names, ix] = get_mat_formats

%--
% get list of possible options
%--

names = {'-V4', '-V6', '-V7', '-V7.3'}; 

%--
% prune list based on version
%--

str = version; major = eval(str(1)); minor = eval(str(3));

if major < 7
	names(3:end) = [];
else
	if minor < 3
		names(4) = [];
	end
end

%--
% get index of default option
%--

ix = min(length(names), 3);