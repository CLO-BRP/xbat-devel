function result = event__save(store, event)

% MOBYSOUND - event__save

result = struct;

lines = file_readlines(store.file);

for k = 1:length(event)
    
    if ~isfield(event(k).measure, 'snr')
        event(k).measure.snr.value = NaN;
    end
    
    str = sprintf('%f\t%f\t%f\t%f\t%f', event(k).time(1), event(k).time(2), event(k).freq(1), event(k).freq(2), event(k).measure.snr.value);
    
    if ~isempty(event.id)       
        lines{event.id} = str; continue;    
    end
   
    lines{end + 1} = str;
    
end

file_writelines(store.file, lines);
