function result = store__parameter__control__callback(callback, context)

% BARN - store__parameter__control__callback

% NOTE: this is used in the user dialog, set in 'new_user_dialog.m', where controls are loaded in a 'prefs' 'namespace'

if ~isfield(context, 'prefix')
	prefix = '';
else
	prefix = context.prefix;
end

% db_disp get-prefix-in-context; stack_disp; prefix, callback.control

result = struct; 

pal = callback.pal;

switch callback.control.name
	
	case {'adapter', [prefix, 'adapter']}
				
		adapter = cellfree(callback.control.value);
		
		%--
		% update server adapter controls
		%--
		
		enable = ~strcmpi(adapter, 'sqlite');
		
		toggle = strcat(prefix, {'hostname', 'port', 'username', 'password'});
		
		for k = 1:numel(toggle)
			
			set_control(pal.handle, toggle{k}, 'enable', enable);
			
			% NOTE: we also clear the values of the server fields for SQLite
			
			if ~enable
				set_control(pal.handle, toggle{k}, 'value', '');
			end	
		end
		
		%--
		% set some reasonable server defaults
		%--
		
		% TODO: this is where the current barn values are being clobbered!
		
		if enable
			set_control(pal.handle, [prefix, 'hostname'], 'value', 'localhost');
			
			set_control(pal.handle, [prefix, 'username'], 'value', 'root');
			
			% TODO: this belongs in the adapter info function, move it there somehow
			
			switch lower(adapter)
				
				case 'mysql'
					value = '3306'; 
					
				case 'postgresql'
					value = '5432';
					
				otherwise
					value = '1527';		
			end
			
			set_control(pal.handle, [prefix, 'port'], 'value', value);
		end
		
	case {'show_password', [prefix, 'show_password']}
		
		% TODO: develop some way of revealing the password
		
		db_disp not-currently-implemented
		
		control = get_control(pal.handle, [prefix, 'password']); 
		
end 