function user = set_barn_user(store, user)

% set_barn_user - from store
% --------------------------
%
% user = set_barn_user(store, user)
%
% Input:
% ------
%  store - for barn
%  user - to find
% 
% Output:
% -------
%  user - in barn

%--
% get active user default
%--

if nargin < 2
	user = get_active_user;
end

if ~nargin
	store = local_barn;
end

if numel(user) > 1
	user = iteraten(mfilename, 2, store, user); return; 
end

%--
% check for and existing user
%--

% NOTE: we use the email as guid for human users when possible, for other types of users we use the guid

if strcmp(user.type, 'user') && ~isempty(user.email)
	field = 'email';
else 
	field = 'guid';
end

current = get_database_objects_by_column(store, 'user', field, user.(field));

if ~isempty(current)
	user = current; return;
end

%--
% prepare user for store based on type, then set and get the user
%--

switch user.type
	
	case 'user'
		
		user.login = user.email;
		
		user = set_barn_objects(store, user);

	% NOTE: 'robots' are automated annotation extensions
	
	case barn_robot_types

		%--
		% prepare extension as user
		%--
		
		% NOTE: the user name is the AR classname used to wrap this extension's data
		
		% NOTE: we also clear values that have a different meaning

		ext = user;
		
		user.name = extension_classname(user, 1);

		user.email = []; user.state = [];
		
		%--
		% store user with related role, determined by extension type
		%--
		
		user = set_barn_objects(store, user);
		
		role = get_database_objects_by_column(store, 'role', 'name', ext.type);
		
		if isempty(role)
			role = struct; role.name = ext.type; role = set_barn_objects(store, role);
		end
		
		insert_relation(store, 'user', user.id, 'role', role.id); 
		
	otherwise
		
		error(['Unrecognized type of user ''', user.type, '''']);
		
end


% TODO: this was an experiment in server communication, this will be fully developed elsewhere

% NOTE: to use http to create user server must be running

% server = 'localhost:3000';
% 
% post_barn_user(user, server);
% 
% user = get_database_objects_by_column(store, 'user', 'email', user.email);


