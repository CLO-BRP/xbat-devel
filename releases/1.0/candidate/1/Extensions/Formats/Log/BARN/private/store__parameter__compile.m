function [parameter, context] = store__parameter__compile(parameter, context)

% BARN - store__parameter__compile

parameter.adapter = cellfree(parameter.adapter);

if ~strcmpi(parameter.adapter, 'sqlite')
	
	parameter = create_database_config(parameter);
	
else
	
	% NOTE: in the case of the SQLite adapter we turn the database name into a full filename
	
    parameter.database = barn_database_file(parameter.database); 

	parameter = create_database_config(parameter);
	
end