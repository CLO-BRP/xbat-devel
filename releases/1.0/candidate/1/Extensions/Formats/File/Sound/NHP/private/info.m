function info = info(file)

% NHP - info

info = struct;

[info.lines, len] = file_head(file, 18);

db_disp; iterate(@disp, info.lines)

% TODO: parse lines