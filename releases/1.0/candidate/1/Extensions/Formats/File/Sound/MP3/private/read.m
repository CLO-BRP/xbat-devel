function [samples, options] = read(file, start, duration, channels, options)

% MP3 - read

[samples, options] = read_mp3(file, start, duration, channels, options);