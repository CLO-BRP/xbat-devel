function ext = libsndfile_base_sound_file_format

ext = extension_create(mfilename);

ext.short_description = 'Base for file formats handled by libsndfile';

% ext.category = {};

% ext.ext = {};

ext.version = '0.1';

ext.guid = '90a30487-41b3-42ee-9e97-df7c9a81d5f6';

ext.author = 'Harold and Matt';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

