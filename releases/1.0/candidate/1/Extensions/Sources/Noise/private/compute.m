function X = compute(parameter, context)

% NOISE - compute

X = [];

rate = get_sound_rate(context.sound);

N = floor(context.page.duration * rate);

distribution = parameter.distribution{1};

if isempty(distribution)
	distribution = 'gaussian';
end

switch distribution
	
	case 'gaussian', X = randn(N, numel(context.page.channels));

	case 'uniform', X = 2 * rand(N, numel(context.page.channels)) - 1;
		
	case 'heavy', X = rand_tan(N, numel(context.page.channels));
		
	otherwise
		
end

X = X * parameter.variance;

if isempty(parameter.filter)
	return;
end

X = filter(parameter.filter.b, parameter.filter.a, X);
