function ext = configure_spectrogram_sound_action

ext = extension_create(mfilename);

ext.short_description = 'Set opening spectrogram parameters';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'c0138e2c-bd4a-46f6-bcae-bd48949e9b5d';

ext.author = 'Harold';

ext.email = '';

ext.url = '';

