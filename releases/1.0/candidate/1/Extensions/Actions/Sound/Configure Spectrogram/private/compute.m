function [result, context] = compute(sound, parameter, context)

% CONFIGURE BROWSER SPECTROGRAM - compute

result = struct;

%--
% check for open sound and close if needed
%--

[open, par] = sound_is_open(sound, context.library);

if open
	close(par); reopen.sound = sound; reopen.library = context.library;
end

%--
% update spectrogram parameters and save sound
%--

% TODO: make sure that these make sense for the current sound

sound.specgram = struct_update(sound.specgram, parameter);

sound_save(sound, context.library);

%--
% update reopen structure
%--

% NOTE: we will re-open any closed sounds during conclude

if open
	
	if ~isfield(context.state, 'reopen')
		context.state.reopen = struct;
	end

	context.state.reopen(end + 1) = reopen;
	
end