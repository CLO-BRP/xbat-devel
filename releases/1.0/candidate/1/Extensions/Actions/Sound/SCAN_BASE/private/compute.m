function [result, context] = compute(sound, parameter, context)

% SCAN_BASE - compute

% TODO: figure out how to use the 'news' result, and how to actually pack results

% NOTE: this is related to figuring out how these extensions related to measures

%-------------------------------
% SETUP
%-------------------------------

result = struct;

%--
% get short names
%--

callback = parameter.callback; 

%--
% create scan waitbar
%--

pal = scan_waitbar(sound, parameter);

% NOTE: the total duration is used in the waitbar value update

total = get_sound_duration(sound);

%--
% create a copy of the context local to the current sound in the iteration 
%--

local = context; local.sound = sound;

%-------------------------------
% SCAN
%-------------------------------

%--
% create scan using parameters
%--

% NOTE: a child extension should typically set this

if isempty(parameter.duration)
	
	parameter.duration = get_default_page_duration(sound);

end

scan = get_sound_scan(sound, parameter.duration, parameter.overlap);

%--
% perform prepare callback
%--

if ~isempty(callback.prepare.fun)
	
	if ~callback.page.parametrized
		[news, local] = callback.prepare.fun(parameter, local);
	else
		[news, local] = callback.prepare.fun{1}(parameter, local, callback.fun{2:end});
	end

end

%--
% scan through while performing page callback
%--

[page, scan] = get_scan_page(scan); 

while 1
	
	% NOTE: break if there are no more pages
	
	if isempty(page)
		break;
	end

	%--
	% read current page
	%--
	
	page = read_sound_page(sound, page, parameter.channels); 
	
	%--
	% perform page callback
	%--
	
	if ~callback.page.parametrized
		[news, local] = callback.page.fun(page, parameter, local);
	else
		[news, local] = callback.page.fun{1}(page, parameter, local, callback.fun{2:end});
	end
	
	%--
	% update waitbar
	%--
	
	stop = page.start + page.duration;
	
	waitbar_update(pal, 'PROGRESS', ...
		'value', stop / total, 'message', sec_to_clock(stop) ...
	);
	
	%--
	% get new page
	%--
	
	[page, scan] = get_scan_page(scan);
	
end

%--
% perform page callback
%--

if ~isempty(callback.conclude.fun)
	
	if ~callback.page.parametrized
		[news, local] = callback.conclude.fun(parameter, local);
	else
		[news, local] = callback.conclude.fun{1}(parameter, local, callback.fun{2:end});
	end

end

%-------------------------------
% CLEANUP
%-------------------------------

%--
% close waitbar if needed
%--

value = get_control(pal, 'close_on_completion', 'value');

if value
	close(pal);
end


%--------------------------------
% SCAN_WAITBAR
%--------------------------------

function pal = scan_waitbar(sound, parameter)

%--
% create controls
%--

control = empty(control_create); 

control(end + 1) = control_create( ... 
	'name', 'PROGRESS', ...
	'style', 'waitbar', ...
	'units', get_sound_duration(sound), ...
	'lines', 1.1, ...
	'confirm', 1, ...
	'space', 0.75 ...
);

control(end + 1) = control_create( ...
	'name', 'close_on_completion', ... 
	'style', 'checkbox', ...
	'value', 1, ...
	'space', 0.5 ...
);

%--
% check for parent waitbar
%--

par = get_xbat_figs('type', 'waitbar');

% NOTE: in this situation there should be only one, eventually check the name

if isempty(par)
	par = 0; pos = 'center';
else
	par = par(1); pos = 'bottom';
end

%--
% create waitbar
%--

pal = waitbar_group(['Scanning ', sound_name(sound)], control, par, pos);

