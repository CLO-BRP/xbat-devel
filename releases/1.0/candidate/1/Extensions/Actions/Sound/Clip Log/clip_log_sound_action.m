function ext = clip_log_sound_action

ext = extension_create(mfilename);

ext.short_description = 'Create log for clip-based sound.';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'fccc1448-4a2d-485a-b4b8-c2730d348d61';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

