function parameter = parameter__create(context)

% DIGEST - parameter__create

parameter = struct;

parameter.location = get_windows_desktop;

parameter.extension = 'flac';

parameter.add_to_library = 0;

parameter.pad = 0.5;