function ext = iterate_event_action_log_action

ext = extension_create(mfilename);

ext.short_description = 'Iterate event action over log events';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'b1806a16-06c0-41d2-b13f-187ea8ed9d82';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

