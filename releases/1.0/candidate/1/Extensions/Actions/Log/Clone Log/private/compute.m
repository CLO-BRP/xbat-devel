function [result, context] = compute(log, parameter, context)

% CLONE LOG - compute

result = struct;

%--
% create clone
%--

name = [log_name(log), '_CL'];

clone = new_log(log.sound, name, 'SQLite', struct);

% NOTE: create new user in close, since this author information is not available in cache

active = get_active_user; 

user = set_database_user(clone.store.file, active);

%--
% copy events to new log
%--

id = get_log_event_field(log, 'id');

start = 1; total = numel(id); part = 500; page = {};

while start < total
	stop = min(total, start + part - 1); page{end + 1} = id(start:stop); start = min(total, stop + 1);
end

for k = 1:numel(page)
	
	%--
	% copy cache events to clone and set author
	%--
	
	event = get_full_event(get_cache_events(log, page{k})); log_save_events(clone, event);
	
	insert_relation(clone.store.file, 'event', page{k}, 'user', user.id);
	
end



