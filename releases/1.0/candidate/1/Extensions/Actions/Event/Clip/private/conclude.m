function [result, context] = conclude(parameter, context)

% CLIP - conclude

%--
% show files if requested
%--

% NOTE: the result is whether we showed the files

result.showed = 0;

if parameter.show_files
	show_file(parameter.output); result.showed = 1;
end
