function file = create_event_thumb(sound, event, file, opt)

% create_event_thumb - create event thumb image
% ---------------------------------------------
%
% file = create_event_thumb(sound, event, file, opt)
%
% opt = create_event_thumb
%
% Input:
% ------
%  sound - containing event
%  event - to thumb
%  file - to output
%  opt - options
%
% Output:
% -------
%  file - thumb
%  opt - default options
%
% See also: create_event_clip

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default options and possibly output
%--

if nargin < 4
	
	opt.pad = 0; opt.taper = 0; % event reading options
	
	opt.band = [];
	
	opt.specgram = []; % spectrogram computation options
	
	opt.map.name = 'gray-light';
	
	opt.map.scale = 1;
	
	opt.map.matrix = flipud(gray(512));
	
	opt.filter = {@linear_filter, filt_binomial(3, 3)};
	
	opt.overlay = [];
	
	if ~nargin
		file = opt; return;
	end
end

if trivial(opt.specgram)
	
	opt.specgram = sound.specgram;
end

%--------------------------
% CREATE IMAGE FILES
%--------------------------
				
%--
% read samples if necessary
%--

% if opt.pad || isempty(event.samples)

	event.rate = get_sound_rate(sound);
	
	event.duration = required_duration(event, opt.specgram);

	event = event_sound_read(event, sound, opt.pad, opt.taper);

% end

%--
% compute spectrogram and create image
%--

X = fast_specgram(event.samples, event.rate, 'norm', opt.specgram);

if ~isempty(opt.filter)

	if iscell(opt.filter)
		fun = opt.filter{1}; args = opt.filter(2:end);

		X = feval(fun, X, args{:});
	else
		X = linear_filter(X, opt.filter);
	end
end

% NOTE: we flip and scale image values, these are conventional

X = lut_dB(flipud(X));

%--
% create requested colormap and convert image to RGB
%--

if opt.map.scale
	
	if isempty(opt.band)
		map = cmap_scale([], X, opt.map.matrix);
	else
		
		map = cmap_scale([], X, opt.map.matrix);
	end
else
	map = opt.map.matrix;
end

% NOTE: each spectrogram is scaled independently

X = gray_to_rgb(X, map);

% TODO: possibly display event as mask

% X = add_event_overlay(X, event, opt);

%--
% write image
%--

% NOTE: convert image to uint8 ourselves, 'imwrite' takes too long doing the same

imwrite(uint8(lut_range(X, [0, 255])), file);


%--------------------------
% ADD_EVENT_OVERLAY
%--------------------------

function X = add_event_overlay(X, event, opt)
	
% TODO: we need to integrate resolution and mask options

%--
% compute event mask rectangle in pixel coordinates
%--

% NOTE: the spectrogram resolution allows conversion of event bounds to pixel ranges

[dt, df] = specgram_resolution(opt.specgram, event.rate);

% NOTE: clip start time is zero

pad = opt.pad;

x1 = max(floor(pad / dt), 1); 

x2 = ceil((diff(event.time) + pad) / dt);

y1 = max(floor(event.freq(1) / df), 1); 

y2 = ceil(event.freq(2) / df);

% NOTE: here we assemble the mask

Z = zeros(size(X, 1), size(X, 2));

Z(y1:y2, x1:x2) = 1;

Z = flipud(Z);

%--
% configure and apply event mask to image
%--

mask_opt = mask_gray_color;

mask_opt.bound = 1;

mask_opt.color = [0.5, 1, 0.5];

X = mask_gray_color(X, Z, mask_opt);


%--------------------------
% REQUIRED_DURATION
%--------------------------

function duration = required_duration(event, parameter, context) %#ok<INUSL>

%--
% compute required page duration
%--

dt = specgram_resolution(parameter, event.rate);

hop = (parameter.fft - round((1 - parameter.hop) * parameter.fft));

duration = ((event.duration / dt) * hop * parameter.sum_length + parameter.fft) / event.rate;



