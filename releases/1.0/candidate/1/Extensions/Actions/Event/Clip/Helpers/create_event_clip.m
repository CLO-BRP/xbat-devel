function [file, event] = create_event_clip(sound, event, file, opt)

% create_event_clip - file
% ------------------------
%
% [file, event] = create_event_clip(sound, event, file, opt)
%
% Input:
% ------
%  sound - to read from
%  event - to read
%  file - to write
%  opt - struct
%
% Output:
% -------
%  file - name
%  event - with corresponding samples
% 
% See also: create_event_thumb, event_sound_read

%--
% set and possibly output default options
%--

if nargin < 4
	% NOTE: these control padding and tapering of pad
	
	opt.pad = 0; opt.taper = 0; 
	
	% NOTE: the rate can be 'player', 'input', or specified rate
	
	opt.rate = 'player';
	
	if ~nargin
		file = opt; return;
	end
end

%--
% setup
%--

% TODO: make sure we hit a format supported rate

switch opt.rate
	case 'player'
		sound.output.rate = get_player_resample_rate(sound.samplerate);

	case 'input'
		sound.output.rate = [];
		
	otherwise
		sound.output.rate = opt.rate;
end

%--
% read samples if necessary
%--

event = event_sound_read(event, sound, opt.pad, opt.taper);

%--
% write file
%--

if iscell(file)
	file = [file{:}];
end

% TODO: we should test this and also extend to handle callbacks

if isa(file, 'function_handle')
	file = file(event);
end

sound_file_write(file, event.samples, event.rate);





