function [result, context] = compute(event, parameter, context)

% CLIP - compute

%--
% compute file name
%--

time = map_time(context.sound, 'real', 'record', event.time); 

% NOTE: try to use full datetime if possible

if ~isempty(context.sound.realtime)
    timestr = datestr((time(1) / 86400) + context.sound.realtime, 30);
else
    timestr = datestr(time(1) / 86400, 'HHMMSS.FFF');
end

ext = lower(parameter.format);

%--
% get event id string, padded with zeros
%--

idstr = int_to_str(event.id, 999999);

% NOTE: the clip file is the result

if ischar(context.log)
	name = context.log; 
else
	name = log_name(context.log); 
end 

result.file = [ ...
	parameter.output, filesep, name, '_', idstr, '_', timestr, '.', ext ...
];

%--
% write clip to file
%--

opt = create_event_clip; opt.pad = parameter.padding; opt.taper = parameter.taper;

create_event_clip( ...
    context.sound, event, result.file, opt ...
);





	
	



