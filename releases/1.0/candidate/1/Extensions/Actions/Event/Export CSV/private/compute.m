function [result, context] = compute(event, parameter, context)

% EXPORT CSV - compute

result = struct;

event = parameter.prepare(event, context);

% TODO there is some vestigial code here

out = context.state.out;

if ~isfield(context.state, 'event')
	
	file_writelines(out.fid, generate_header(event));
	
	file_writelines(out.fid, generate_lines(event));
	
	context.state.event = event;

else

	file_writelines(out.fid, generate_lines(event));
	
% 	context.state.event(end + 1) = event;

end


%-------------------
% GENERATE_HEADER
%-------------------

function header = generate_header(in)

header = {str_implode(fieldnames(in), ', ')};


%-------------------
% GENERATE_LINES
%-------------------

function lines = generate_lines(in)

values = struct2cell(in);

lines = cell(size(values, 3), 1);

for k = 1:length(lines)
	lines{k} = str_implode(values(:, :, k), ', ', @escaped_str);
end


%-------------------
% EXCAPED_STR
%-------------------

function str = escaped_str(value)

% TODO: consider using another separator, perhaps semi-colon

str = to_str(value);

str = strrep(str, ',', ' -');
