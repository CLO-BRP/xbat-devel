function [handles, context] = view__on__event__display(widget, data, parameter, context)

% TIME-FREQUENCY MOMENTS - view__on__event__display

handles = [];

% NOTE: to create an overlay on the browser, we cannot rely on the widget handle

% NOTE: there is also, no good place to actually do this within the widget framework, we may need a hack

location = data.value.time.mean; scale = data.value.time.deviation;

start = location - scale; stop = location + scale; 

center = mean(get(data.event.axes, 'ylim'));

handles = line( ...
	'parent', data.event.axes, ...
	'xdata', [start, stop], ...
	'ydata', center * ones(1, 2), ...
	'linewidth', 2 ...
);
