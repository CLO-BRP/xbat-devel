function [value, context] = compute(event, parameter, context)

% ZC-RATE - compute

% value = struct;

page = get_event_page(event, context.sound, context);

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.state.feature = feature;

%--
% compute measure value from feature
%--

value = struct;

value.mean = mean(feature.rms.value);