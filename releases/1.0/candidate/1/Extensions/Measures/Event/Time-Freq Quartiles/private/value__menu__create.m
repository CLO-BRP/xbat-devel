function handles = value__menu__create(par, value, context)

% TIME-FREQUENCY QUARTILES - value__menu__create

db_disp; flatten(value)

handles = flat_struct_menu(par, value);
