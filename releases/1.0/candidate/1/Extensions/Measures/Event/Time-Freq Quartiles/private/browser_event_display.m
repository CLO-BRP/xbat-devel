function handles = browser_event_display(par, data, context)

% TIME-FREQUENCY QUARTILES - browser_display

% NOTE: this is where we get the display parameters

% context.ext.parameters.view

%-------------------
% SETUP
%-------------------

%--
% set short name for fields
%--

% NOTE: the axes are the display axes for the event

event = data.event; ax = event.axes;

% NOTE: this is the way to find the axes in the browser

% ax = findobj(data.browser.axes, 'tag', int2str(event.channel));

value = data.value;

%-------------------
% DISPLAY
%-------------------

%--
% set display options
%--

lw1 = 0.5 + eps; lw2 = 2;

rgb1 = [1 1 0]; rgb2 = [1 1 0];

%--
% get frequency scaling
%--

if strcmp(context.display.grid.freq.labels, 'kHz')
	kHz_flag = 1;
else
	kHz_flag = 0;
end

%--
% display time quartiles and fences (reduced box plot)
%--

% fixed for this part of the display

y = value.frequency.median * ones(1, 3);

if kHz_flag
	y = y / 1000;
end

% time concentration

x = value.time.median + [value.time.low, 0, value.time.high];

handles(1) = line( ...
	'parent', ax, ...
	'xdata', x, ...
	'ydata', y, ...
	'linestyle', '-', ...
	'marker', 'none', ... % 'marker', '+', ...
	'color', rgb1, ...
	'linewidth', lw2 ...
);	

% time fences

off = 1.5 * value.time.range;

x = x([1, 3]) + off * [-1, 1];

handles(2) = line( ...
	'parent', ax, ...
	'xdata', x, ...
	'ydata', y(1:2), ...
	'linestyle', '-', ...
	'marker', 'none', ... % 'marker','+', ...
	'color', rgb2, ...
	'linewidth', lw1 ...
);

%--
% display frequency quartiles and fences (reduced box plot)
%--

% fixed for this part of the display

x = value.time.median * ones(1, 3);

% frequency concentration

y = [value.frequency.low, value.frequency.median, value.frequency.high];

if kHz_flag
	y = y / 1000;
end

handles(3) = line( ...
	'parent', ax, ...
	'xdata', x, ...
	'ydata', y, ...
	'linestyle', '-', ...
	'marker', 'none', ... % 'marker','+', ...
	'color', rgb1, ...
	'linewidth', lw2 ...
);	

% frequency fences

off = 1.5 * value.frequency.range;

y = [value.frequency.low - off, value.frequency.high + off];

if kHz_flag
	y = y / 1000;
end

handles(4) = line( ...
	'parent', ax, ...
	'xdata', x(1:2), ...
	'ydata', y, ...
	'linestyle', '-', ...
	'marker', 'none', ... % 'marker','+', ...
	'color', rgb2, ...
	'linewidth', lw1 ...
);	

%--
% make the display non-interfering with the interface
%--

% TODO: this should be moved elsewhere along with some tagging for clean-up

set(handles, 'hittest', 'off');



