function handles = view__layout(widget, parameter, context)

% FORMANTS - view__layout

% handles = [];

harray(widget, layout_create(2, 1));

handles = harray_select(widget, 'level', 1);

set(handles(1), 'tag', 'top');

set(handles(2), 'tag', 'bottom');

% axes(handles(1)); ylabel('A');
% 
% axes(handles(2)); ylabel('B');