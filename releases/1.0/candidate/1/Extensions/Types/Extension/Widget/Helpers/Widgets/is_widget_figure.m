function test = is_widget_figure(handle)

% is_widget_figure - test whether a figure is a widget
% ----------------------------------------------------
%
% test = is_widget_figure(handle) 
%
% Input:
% ------
%  handle - to candidate figure
%
% Output:
% -------
%  test - result

% NOTE: how is this related to 'get_xbat_figs' and 'match_tag_prefix', why the multiplicity

%--
% get tags
%--

tag = get(handle, 'tag');

if ischar(tag)
	tag = {tag};
end

%--
% check they have widget prefix
%--

test = strncmp('XBAT_WIDGET', tag, 11);