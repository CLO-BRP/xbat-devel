function info = parse_widget_tag(tag, fields, sep)

% parse_widget_tag - parse widget tag to get info
% -----------------------------------------------
%
% info = parse_widget_tag(tag)
%
% Input:
% ------
%  tag - widget figure tag
% 
% Output:
% -------
%  info - widget info

% TODO: push caching all the way here, not just the 'listen' field, get sig directly using length

%--
% configure and parse tag
%--

% NOTE: the only reason to make these variables is that 'parse_listen is local, reconsider

if nargin < 3
	sep = '::';
end

if nargin < 2
	fields = {'header', 'type', 'name', 'user', 'library', 'sound', 'listen', 'sig'};
end

info = parse_tag(tag, sep, fields);

%--
% parse listen string to make event information explicit
%--

if ~isempty(info.listen)
	
	info.listen = parse_listen(info.listen, info.sig);

end


%-------------------------------
% PARSE_LISTEN
%-------------------------------

function listen = parse_listen(str, sig)

% parse_listen - parse listener binary indicator string
% -----------------------------------------------------
%
% listen = parse_listen(str, sig)
%
% Input:
% ------
%  str - indicator binary string
%
% Output:
% -------
%  listen - event listener indicator struct

%--
% create and check cache
%--

persistent CACHE

if isempty(CACHE)
	CACHE = struct;
end

if isfield(CACHE, sig)
	listen = CACHE.(sig); return;
end

%--
% setup
%--

event = get_widget_events;

% NOTE: does this break for other character sets?

value = double(str) - double('0');

%--
% parse listen into struct
%--

listen = struct; 

for k = 1:length(event)
	listen.(event{k}) = value(k);
end

% NOTE: this is currently the most expensive line

listen = collapse(listen);

%--
% update cache
%--

CACHE.(sig) = listen;
