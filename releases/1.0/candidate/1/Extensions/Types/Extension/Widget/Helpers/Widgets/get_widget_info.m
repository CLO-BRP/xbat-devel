function [info, data] = get_widget_info(widget)

% get_widget_info - from widget handle
% ------------------------------------
%
% [info, data] = get_widget_info(widget)
%
% Input:
% ------
%  widget - handle
%
% Output:
% -------
%  info - mostly from tag
%  data - from userdata

info = parse_widget_tag(get(widget, 'tag'));

data = get(widget, 'userdata');

info.parent = data.parent;