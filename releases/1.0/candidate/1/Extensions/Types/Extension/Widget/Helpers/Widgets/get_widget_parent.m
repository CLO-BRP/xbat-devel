function par = get_widget_parent(widget)

par = zeros(size(widget));

for k = 1:numel(widget)
	
	value = get_field(get(widget(k), 'userdata'), 'parent', -1);
	
	% NOTE: this is what is stored in the 'XBAT' palette field
	
	if isempty(value)
		value = 0;
	end
	
	par(k) = value;
	
end
