function fun = signatures

% WIDGET - signatures

% NOTE: a widget extension provides a parametrized display that listens to events

%--
% create and position
%--

fun.layout = {{'handles'}, {'widget', 'parameter', 'context'}};

fun.position = {{'widget'}, {'widget', 'parameter', 'context'}};

%--
% event handlers
%--

events = widget_events;

% NOTE: the context here will probably be used for paging

for k = 1:length(events)
	fun.on.(events{k}) = {{'handles', 'context'}, {'widget', 'data', 'parameter', 'context'}};
end

fun.on = collapse(fun.on);

%--
% parametrize
%--

fun.parameter = param_fun;
