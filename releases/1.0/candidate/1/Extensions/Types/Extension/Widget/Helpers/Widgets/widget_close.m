function widget_close(widget, eventdata, par)

% widget_close - close widget
% ---------------------------
%
% widget_close(widget, eventdata)
%
% Input:
% ------
%  widget - widget

% NOTE: set as 'closerequestfcn' of widget figure using function handle 

%--
% save widget extension in parent store
%--

ext = get_widget_extension(widget);

state = get_widget_state(widget);

switch ext.subtype
	
	% NOTE: in the case of widgets we store the extension state in the widget
	
	case 'widget', set_browser_extension(par, ext, state);
	
	% NOTE: for 'views' the extension 'palette' keeps track of the state
	
	otherwise
		
		set_browser_extension(par, [], state);
		
% 		super = get_browser_extension(ext.subtype, par, ext.name);
		
		% TODO: figure out what goes into the 'ext' and 'super' merge
		
% 		set_browser_extension(par, super);
		
end

%--
% close widget
%--

closereq;

