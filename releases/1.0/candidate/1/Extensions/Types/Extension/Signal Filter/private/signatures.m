function fun = signatures

% NOTE: for now context output is to indicate any mixing and type casting

fun.compute = {{'X', 'context'}, {'X', 'parameter', 'context'}};

fun.parameter = param_fun;

% fun.view = get_extension_type_signatures('widget');
