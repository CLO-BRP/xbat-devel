function [fun, info] = signatures

% EVENT CLASSIFIER - signatures

%--------------
% INFO
%--------------

info.version = '0.2';

info.short_description = '';

%--------------
% API
%--------------

fun.prepare = {{'result'}, {'parameter', 'context'}};

% NOTE: the event is a simple event at the moment

fun.compute = {{'tags', 'value', 'context'}, {'event', 'parameter', 'context'}};

% NOTE: in the 'train' mode, the data is the 'training' set: event, tags, possibly attributes
%       for the output the result is the trained classifier parameters
%       (model), the 'value' performance evaluation values

% NOTE: in the 'predict' mode the data is events, possibly attributes
%       for the output result contains tags, 'value' describes classifier
%       state and or confidence

fun.compute = {{'result', 'value', 'context'}, {'mode', 'data', 'parameter', 'context'}};

fun.conclude = {{'result'}, {'parameter', 'context'}};

fun.parameter = param_fun;

fun.view = get_extension_type_signatures('widget');

