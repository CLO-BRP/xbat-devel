function [root, created] = feature_render_root(sound, feature)

% feature_render_root - root for rendered feature assets
% ------------------------------------------------------
%
% [root, created] = feature_render_root(sound, feature)
%
% Input:
% ------
%  sound - being rendered
%  feature - extension
%
% Output:
% -------
%  root - for assets
%  created - indicator

%--
% handle input
%--

% NOTE: we also allow context input

if nargin == 1
	context = sound; sound = context.sound; feature = context.ext;
end

%--
% ensure existence of and report on root directory for assets
%--

[root, created] = create_dir(fullfile(get_desktop, 'RENDER', sound_name(sound), feature.name));