function out = render_sound_feature(sound, feature, context, scales)
	
%--------------------
% SETUP
%--------------------

%--
% set some default rendering scales
%--

if nargin < 4
	scales = [10, 30, 60, 300, 600]; scales(scales > get_sound_duration(sound)) = [];
end

%--
% get a default context
%--

% NOTE: currently the context depends on a browser, figure out how to this

% get_extension_context(ext, par, data)

%--------------------
% RENDER
%--------------------

%--
% setup output location and waitbar
%--

root = feature_render_root(sound, feature);

wait = render_waitbar(context.sound);

%--
% render for various scales
%--

for j = 1:numel(scales)
	
	scale = scales(j); 
	
	%--
	% get scan for this scale and pages
	%--
	
	scan = get_sound_scan(sound, scales(j));
	
	[page, scan] = get_scan_page(scan); pages = page;

	while ~isempty(page)
		pages(end + 1) = page; [page, scan] = get_scan_page(scan);
	end

	%--
	% PLACE THESE WHERE THEY BELONG
	%--
	
	if ~isempty(feature.fun.parameter.compile)
	
		try
			
		catch
			extension_warning();
		end
		
	end
	
	create_dir(fullfile(root, int_to_str(scale, 10^6)));
	
	%--
	% page through sound and render
	%--

	for k = 1:numel(pages)
		
		page = pages(k);
		
		%--
		% render each channel separately
		%--

		for l = 1:sound.channels

			%--
			% get current page samples, possibly filtered
			%--

			page = read_sound_page(context.sound, page, l);

			%--
			% compute feature on page channel and render
			%--

			value = context.ext.fun.compute(page, feature.parameters.compute, context);

			% NOTE: the rendered may share some code with the view, these should be factored as helpers

			% NOTE: the 'view' is the 'display output device', when we 'render' we are thinking about a filesystem representation

			% TODO: think about rendering in the context of computational caching as well

			% TODO: think about whether we want a render file name input, or whether some time information should be used

			prefix = fullfile(root, int_to_str(scale, 10^6), [int_to_str(k, 10^6), '_', int_to_str(l, 100)]);
			
			context.prefix = prefix;

			try
				result.render(k, l) = context.ext.fun.render(value, context);
			catch
				extension_warning(context.ext, 'Failed to render.', lasterror);
			end

			%--
			% add matching audio files to rendering if requested
			%--

			% TODO: the format will be obtained from parameters, and options updated

			file = [prefix, '.mp3']; opt = sound_file_write(file);

			result.audio{k, l} = sound_file_write(file, page.samples, context.sound.rate, opt);
			
		end
		
		%--
		% create simple rendering page
		%--

		% NOTE: at the moment this seems to specifically tied to this application

		% TODO: implement this is a helper, consider using javascript

		result.page = feature_render_page(result, context);

		waitbar_update(wait, 'PROGRESS', 'value', k / numel(pages));
		
	end

end

%--
% close waitbar and show output location if needed
%--

if get_control(wait, 'close_after_completion', 'value')
	close(wait);
end

show_file(root);


%-------------------------
% RENDER_WAITBAR
%-------------------------

function pal = render_waitbar(sound)

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Overall progress bar', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'update_rate', [], ...
	'space', 2 ...
);
	
control(end + 1) = control_create( ...
	'name', 'close_after_completion', ...
	'style', 'checkbox', ...
	'value', 1, ...
	'space', 1 ...
);

name = ['Rendering ', sound_name(sound)]; opt = waitbar_group;

pal = waitbar_group(name, control, [], [], opt);


%-------------------------
% FEATURE_RENDER_PAGE
%-------------------------

function file = feature_render_page(result, context)

file = [context.prefix, '.html'];

% % TODO: look at other html rendering code in the system ...
% 
% for k = 1:numel(result.render)
% 	
% 	if ~isfield(result.render, 'image')
% 		continue;
% 	end
% 
% end
