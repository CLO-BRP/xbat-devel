function log = active_detection(par, data)

% active_detection - perform active detection
% -------------------------------------------
%
% log = active_detection(par, data)
%
% Input:
% ------
%  par - handle to browser figure
%  data - browser figure userdata
%
% Output:
% -------
%  log - active detection log

%---------------------
% HANDLE INPUT
%---------------------

%--
% get parent state if needed
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%---------------------
% SETUP
%---------------------

%--
% set default output to cleared active detection log
%--

log = log_clear(get_active_detection_log(par, '', data));

%--
% get active detector
%--

ext = get_active_extension('sound_detector', par, data);

% NOTE: return if there is no active detector

if isempty(ext)
	return;
end

% TODO: the following line explores the extension to multiple active detectors

%--
% clear all relevant active detection logs
%--

% NOTE: the cell extraction of active detector names will work when there are multiple

% NOTE: the 'log_clear' function is local and only works with MAT format logs

% log = log_clear(get_active_detection_log(par, {ext.name}, data));

%---------------------
% BUILD SCAN
%---------------------

%--
% get sound and channels
%--

sound = data.browser.sound;

channels = get_channels(data.browser.channels);

%--
% get page start and duration
%--

% NOTE: we map the browser page boundaries to real time

edges = [data.browser.time, data.browser.time + data.browser.page.duration];

edges = map_time(sound, 'real', 'slider', edges);

% NOTE: this happens when there is no data in the displayed page, due to expansion

if diff(edges) == 0
	return;
end

page.start = edges(1); page.duration = diff(edges);

%--
% build scan for page considering sound
%--

scan = scan_intersect(get_page_scan(page), get_sound_scan(sound));

%%%% HACK %%%%

% NOTE: what we want is the scan page to be the browser page if possible

scan.page.duration = min(30, data.browser.page.duration);

%--
% pack context
%--

% NOTE: the detector context is a decorated extension context

% TODO: currently active detection is handled in a special way and uses the empty log input, change this

context = detector_context(ext, sound, scan, channels, [], par, data);

%-------------------------------------------------
% UPDATE ACTIVE DETECTION LOG
%-------------------------------------------------

%--
% scan using active detector
%--

log.event = detector_scan(context);

for k = 1:numel(log.event)
	log.event(k).id = k;
end

log.length = numel(log.event);

% TODO: eventually we should support multiple active detectors and this can be used to distinguish

% NOTE: not clear that this is the place where the distinguishing colors would be set

log.linestyle = '-'; log.linewidth = 1; log.color = 0.75 * ones(1, 3); 

% NOTE: this looks good, however for semantic reasons perhaps we should wait until we have multiple active detectors for color

% log.color = 0.8 * [1 1 0];


%----------------------------------------------
% CLEAR LOG
%----------------------------------------------

function log = log_clear(log)

log.event = empty(event_create);

log.length = 0;





