function log = get_active_detection_log(par, name, data) %#ok<INUSL>

% get_active_detection_log - get this from browser
% ------------------------------------------------
%
% log = get_active_detection_log(par, name, data)
%
% Input:
% ------
%  par - browser
%  name - detector name (ignored for now, until we can have multiple)
%  data - browser state
%
% Output:
% -------
%  log - active detection log

%------------------
% HANDLE INPUT
%------------------

%--
% try to get a browser if needed
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

% NOTE: there is nothing we can get if we don't have a browser

if isempty(par)
	log = []; return;
end 

%--
% get browser state if needed
%--

if nargin < 3
	data = get_browser(par);
end

%--
% set empty name default to get all active detection logs
%--

if nargin < 2
	name = '';
end

% NOTE: this consideration extracts confined strings and handles multiple names

if iscell(name)
	log = iteraten(mfilename, 2, par, name, data); return;
end

%--
% get active detection log
%--

log = data.browser.active_detection_log;