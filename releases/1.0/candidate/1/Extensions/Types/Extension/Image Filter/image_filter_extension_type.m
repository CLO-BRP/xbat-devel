function ext = image_filter_extension_type

ext = extension_create;

ext.short_description = 'Image to image transformation';

% ext.category = {};

ext.version = '0.2';

ext.guid = 'e97b84b7-4d31-4a9d-9cb8-8725fdba2d48';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

