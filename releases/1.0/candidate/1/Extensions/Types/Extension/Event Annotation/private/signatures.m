function [fun, info] = event_annotation

% event_annotation - function handle structure
% ---------------------------------------------
%
% [fun, info] = event_annotation
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun = value_fun;