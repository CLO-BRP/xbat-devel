function [ANNOT,ANNOT_NAME] = get_annotations(mode)

% get_annotations - get currently available annotations
% ------------------------------------------------------
%
% [ANNOT,ANNOT_NAME] = get_annotations
%                    = get_annotations('update')
%
% Output:
% -------
%  ANNOT - annotation structure array
%  ANNOT_NAME - annotation names cell array

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1421 $
% $Date: 2005-08-01 17:03:23 -0400 (Mon, 01 Aug 2005) $
%--------------------------------

%--
% set mode
%--

if (~nargin)
	mode = '';
end 

%--
% create persistent annotation structure array and names
%--

persistent PERSISTENT_ANNOT PERSISTENT_ANNOT_NAME;

%--
% get annotation information if needed or 'update requested'
%--

if (isempty(PERSISTENT_ANNOT) | strcmp(mode,'update'))
	
	%--
	% get functions in 'annotations' directory
	%--
	
	fun = what([extensions_root, filesep, 'Annotations']); 
	
	fun = what([fun.path, filesep, 'Event']);

	fun = unique(file_ext({fun.m{:}, fun.p{:}}));

	%--
	% get annotation information
	%--
	
	if (length(fun))
		
		j = 1;
		
		for k = 1:length(fun)
			
			if findstr(fun{k},'_annotation')
				
				tmp = eval(file_ext(fun{k}));
				
				if (j == 1)
					PERSISTENT_ANNOT = tmp;
				else
					PERSISTENT_ANNOT(j) = tmp;
				end
				
				PERSISTENT_ANNOT_NAME{j} = tmp.name;
				
				j = j + 1;
				
			end
			
		end
		
	end
	
	%--
	% sort annotations by names
	%--
	
	[PERSISTENT_ANNOT_NAME,ix] = sort(PERSISTENT_ANNOT_NAME);
	
	PERSISTENT_ANNOT = PERSISTENT_ANNOT(ix);
	
end

%--
% output annotation structure array and annotation names
%--

ANNOT = PERSISTENT_ANNOT;

ANNOT_NAME = PERSISTENT_ANNOT_NAME;
