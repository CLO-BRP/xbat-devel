function measurement_menu(menus, event, par, m, ix)

% measurement_menu - construct a measurement context menu for an event
% --------------------------------------------------------------------
%
% measurement_menu(m, event)
%
% Input:
% ------
%  m - parent context menu
%  event - event
%
% Output:
% -------
%  (none)
%

measurements = get_measurements(par);

measurement_names = {measurements.name};
	
%--
% Add measurement values
%--

meas_menu = get_menu(menus, 'Measure');

for k  = 1:length(event.measurement)

	%--
	% do nothing for no-name measurements
	%--
	
	if isempty(event.measurement(k).name)	
		continue;
	end

	%--
	% try to produce the measurement's context menu
	%--
	
	try
		
		feval(event.measurement(k).fun, 'menu', par, m, ix);
	
	catch
		
		nice_catch(lasterror, ['Measurement Menu Creation failed for ', event.measurement(k).name, '.']);
		
		%--
		% fall back on a struct_menu() of values
		%--

		h = uimenu(meas_menu, ...
			'Label', event.measurement(k).name ...
		);
		
		value_struct.value = event.measurement(k).value;
		
		struct_menu(h, value_struct);
		
		%--
		% create 'Edit ...' option
		%--	
		
		fh = event.measurement(k).fun;	
		
		callback = {@measurement_recompute_wrapper, fh, 'events', par, m, ix};
		
		uimenu(meas_menu, 'Label', 'Edit ...', 'callback', callback);

	end

end

%--
% Measures to Add ...
%--

L = setdiff(measurement_names,struct_field(event.measurement,'name'));

if ~isempty(L)
	
	L = strcat(L,' ...');

	tmp = menu_group(meas_menu, '', {'Add Measure'});

	if (get(tmp,'position') > 1)
		set(tmp,'separator','on'); 
	end

	n = length(L);

	S = bin2str(zeros(1,n));
	
	if (n > 1)
		mg = menu_group(tmp,'event_menu',L,S);
	else
		mg = menu_group(tmp,'event_menu',L);
	end

end

set(get_menu(menus,'Template'),'enable','off');

% TODO: this does not belong here

%--
% Hierarchy
%--

L = { ...
	['Level:  ' int2str(event.level)], ...
	'Children', ...
	'Parents' ...
};

n = length(L);

S = bin2str(zeros(1,n)); S{2} = 'on';

tmp = menu_group(get_menu(menus,'Hierarchy'),'',L,S);

%--
% Children
%--

if isempty(event.children)
	
	L = {'(No Children)'};
	
	set(menu_group(get_menu(tmp,'Children'),'',L),'enable','off');
	
else

end

%--
% Parents
%--

% TODO: update old logs that have events with 'parents' fields

if isempty(event.parent)
	
	L = {'(No Parents)'};
	
	set(menu_group(get_menu(tmp,'Parents'),'',L),'enable','off');
	
else

end

%--
% Copy Event To
%--

logs = get_browser(par, 'log'); logs(m) = [];

L = {};

for k = 1:length(logs)
	L{k} = log_name(logs(k));
end
	
if length(L)
	menu_group(get_menu(menus,'Copy Event To'),'event_menu',L);
end


%----------------------------------
% measurement_recompute_wrapper
%----------------------------------

function measurement_recompute_wrapper(obj, eventdata, fh, varargin)

fh(varargin{:});



