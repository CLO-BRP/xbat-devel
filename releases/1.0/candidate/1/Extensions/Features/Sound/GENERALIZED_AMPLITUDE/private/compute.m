function [feature, context] = compute(page, parameter, context)

% GENERALIZED_AMPLITUDE - compute

%-----------------------------
% APPLY MAP
%-----------------------------

%--
% apply generalized amplitude map to signal
%--

% NOTE: we always store the generalized amplitude in the filtered field

if isempty(page.filtered)
	page.filtered = page.samples;
end

% NOTE: the map info may contain range, unit information, or label information

[page.filtered, context.state.info] = parameter.map(page.filtered, parameter);

%-----------------------------
% COMPUTE AMPLITUDE
%-----------------------------

%--
% compute amplitude feature of mapped signal
%--

fun = parent_fun(mfilename('fullpath'));

if ~iscell(page.filtered)
	
	[feature, context] = fun(page, parameter, context); return;

end

%--
% compute amplitude feature of multiple map signals
%--

% TODO: move this to actually use a struct array for the output, this will also contain the label

filtered = page.filtered;

for k = 1:numel(page.filtered)
	
	page.filtered = filtered{k}; [feature(k), context] = fun(page, parameter, context);

end


