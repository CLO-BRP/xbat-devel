function ext = spectral_flux_sound_feature

ext = extension_inherit(mfilename, spectral_reduction_sound_feature);

ext.short_description = 'Various measures of temporal change in Spectrum';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '22c346f6-77db-402c-9472-426dce4cafd0';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

