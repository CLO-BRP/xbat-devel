function control = parameter__control__create(parameter, context)

% SPECTRAL FLUX - parameter__control__create

% control = empty(control_create);

fun = parent_fun(mfilename('fullpath')); control = fun(parameter, context);

owned = empty(control_create);

owned(end + 1) = control_separator();

control = [owned, control];