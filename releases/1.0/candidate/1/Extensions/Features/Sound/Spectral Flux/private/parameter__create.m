function parameter = parameter__create(context)

% SPECTRAL FLUX - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @map;

parameter.flux = 'euclidean';

parameter.onset = 0;

parameter.log = 0;


%------------------
% MAP
%------------------

% TODO: this should be part of the SPECTRAL_REDUCTION feature and not part of a child

% NOTE: the constraint is that the reduction preserve the time grid

function reduction = map(feature, parameter, context)

if ~iscell(feature.spectrogram.value)

	reduction.value = flux(feature.spectrogram.value, parameter);
	
else
	
	for k = 1:numel(feature.spectrogram.value)
       
		reduction.value{k} = flux(feature.spectrogram.value{k}, parameter);
	end
	
end

reduction.time = feature.time;

reduction.value;


%------------------
% FLUX
%------------------

function flux = flux(value, parameter)

% TODO: consider weighted sums, logarithmic scaling and perhaps onset consideration

%--
% compute difference
%--

if parameter.log
	value = log(value);
end

D = diff(value, 1, 2);

if parameter.onset
	D(D < 0) = 0;
end

%--
% compute flux
%--

type = cellfree(parameter.flux);

switch lower(type)

	% TODO: add logarithmic scaling and positive change only here
	
	% NOTE: both of these are equally sensitive to onset and offset
	
	case 'absolute'
		
		% NOTE: in the case of 'onset' we only have positive values, save the 'abs'
		
		if parameter.onset
			flux = [0, sum(D, 1)];
		else
			flux = [0, sum(abs(D, 1))];
		end
		
	case 'euclidean'
		
		flux = [0, sqrt(sum(D.^2, 1))];

end




