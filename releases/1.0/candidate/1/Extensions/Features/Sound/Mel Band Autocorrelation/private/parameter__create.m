function parameter = parameter__create(context)

% MEL BAND AUTOCORRELATION - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% overwrite standard values

parameter.num_lin = 4;

parameter.num_log = 2;


parameter.temp_res =  min(10, 200/ (context.page.duration*10));

parameter.max_lag = min(2,0.1 * context.page.duration);

