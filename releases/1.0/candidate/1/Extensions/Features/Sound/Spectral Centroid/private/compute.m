function [feature, context] = compute(page, parameter, context)

% SPECTRAL CENTROID - compute

% feature = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

feature.centroid = parameter.map(feature, parameter, context);