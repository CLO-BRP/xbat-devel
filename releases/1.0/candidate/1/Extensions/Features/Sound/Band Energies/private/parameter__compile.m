function [parameter, context] = parameter__compile(parameter, context)

% BAND ENERGY - parameter__compile

fun = parent_fun(mfilename('fullpath')); [parameter, context] = fun(parameter, context); 

% ---
% this is a workaround to help using integer frequencies as input
% ---
if mean(parameter.bands) > 1
    parameter.bands = parameter.bands ./ (context.sound.rate/2) ;
end

parameter.band_details = parameter.interpretfun(parameter);

[parameter.bank, parameter.bank_freqs] = generate_bank(parameter, context);
