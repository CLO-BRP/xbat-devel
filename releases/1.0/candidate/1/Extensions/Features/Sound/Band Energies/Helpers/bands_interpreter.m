function band_details = bands_interpreter(parameter)
% band parameter interpreter for BAND ENERGY extension
%
% adds field 'band_details' to parameter
% by interpreting the bands and type user interface
%
% -----band_details:
%    starf1 ...... startfn
%    centf1 ...... cenftn
%    stopf1 ...... stopfn

switch lower(cellfree(parameter.bank_type))

	case {'triangular', 'hann', 'generic'}

		%--
		% NOTE: here, we assume the definition of half-overlapping bands
		% via their center positions
		%--

		centerpos=parameter.bands;

		% check for seperated limits

		if isfield(parameter, 'start_freq')
			band_limits = [parameter.start_freq, parameter.stop_freq];
		else
			band_limits = [0, 1];
		end

		% triangular filters are defined by their center pos.
		% cutoff freqs are assumed using half-band overlaps
		band_details=zeros(3,numel(centerpos));
		band_details(1, 1)=band_limits(1);
		band_details(end, end)=band_limits(2);

		band_details(1,2:end)=centerpos(1:end-1); % start positions
		band_details(2,:)=centerpos; % center positions
		band_details(3,1:end-1)=centerpos(2:end); %end positions

	case 'rectangular'

		%--
		% NOTE: here, we assume the definition of successive NONOVERLAPPING rectang.
		% bands via the border frequencies
		%--

		band_limits=parameter.bands;

		band_details=zeros(3,numel(band_limits)-1);
		
		for k=1:numel(band_limits)-1
			band_details([1 3],k)= band_limits([k k+1]); %border frequencies
			band_details(2,k)= diff(band_limits([k k+1]))/2+band_limits(k); %center frequencies
		end

	otherwise
		
		error(['Unrecognized filter type ''', parameter.bank_type, '''.']);

end


