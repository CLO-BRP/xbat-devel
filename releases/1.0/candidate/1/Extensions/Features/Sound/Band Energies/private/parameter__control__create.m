function control = parameter__control__create(parameter, context)

% BAND ENERGY - parameter__control__create

% control = empty(control_create);

%--
% get inherited controls
%--

fun = parent_fun(mfilename('fullpath')); inherited = fun(parameter, context);

%--
% describe specific controls
%--

bank_types  = {'rectangular', 'triangular', 'hann'};%{'triangular', 'hann'}; % = {'rectangular', 'triangular', 'hann'};

[ignore, value] = string_is_member(cellfree(parameter.bank_type), bank_types, 1);

type = control_create( ...
	'name', 'bank_type', ...
	'alias', 'type', ...
	'label', 0, ...
	'style', 'popup', ...
	'width', 0.5, ...
	'space', -0.8, ...
	'color', get(0, 'defaultuicontrolbackgroundcolor'), ...
	'value', value, ...
	'string', title_caps(bank_types) ...
);

normalize = checkbox_control({'normalize', 'normalized'}, parameter.normalize, ...
	'width', 0.4, ...
	'space', 1.5, ...
	'align', 'right', ...
	'tooltip', 'Normalize by bandwidth' ...
);

% TODO: consider adding some space at the top for the center display

bank = control_create( ...
	'name', 'bank', ...
	'label', 0, ...
	'style', 'axes', ... 
	'onload', 1, ...
	'lines', 4.5, ...
	'space', 1.5 ...
);

%--------------------

% TODO: we want finer control over band definition with a simple interface

bands = empty(control_create);

% NOTE: these controls are for each band, the currently selected band

% nyq = 0.5 * context.sound.rate;
% 
% bands(end + 1) = edit_control({'band_name', 'name'});
% 
% bands(end + 1) = slider_control({'band_center', 'center'}, 0.25 * nyq, [0, nyq], 'real');
% 
% bands(end + 1) = checkbox_control('contiguous', 1);
% 
% bands(end + 1) = slider_control({'band_start', 'start'}, 0, [0, nyq]);
% 
% bands(end + 1) = slider_control({'band_stop', 'stop'}, nyq, [0, nyq]);

%--------------------

centers = edit_control({'bands', 'centers'}, parameter.bands, ...
    'tooltip', 'defining freqs in Hz, separate by ","', ...
	'onload', 1, ...
	'space', 1.5, ...
	'set', {@access_bands, 'set'}, ...
	'get', {@access_bands, 'get'} ...
);

%--
% pack all controls
%--

control = [type, normalize, bank, bands, centers, section_collapse_control(1), inherited];

% NOTE: the lines below pack the new controls in a tab for display clarity

tabs = {'Filter-Bank'};

for k = 1:4
	control(k).tab = tabs{1};
end

control = [control_create('style', 'tabs', 'tab', tabs), control];

