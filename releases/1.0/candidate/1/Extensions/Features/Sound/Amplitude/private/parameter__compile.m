function [parameter, context] = parameter__compile(parameter, context)

% AMPLITUDE - parameter__compile

%--
% consider adapt request
%--

% NOTE: we check whether adaptation is requested and possible in context

if ~parameter.auto || ~isfield(context, 'page')
	return;
end

%--
% adapt block
%--

rate = get_sound_rate(context.sound); 

block = get_block_range(rate);

% TODO: add a min block duration consideration to the auto

parameter.block = clip_to_range(context.page.duration / 500, block);

%--
% update block control 
%--

if isempty(context.pal)
	return; 
end

set_control(context.pal, 'block', 'value', parameter.block);
