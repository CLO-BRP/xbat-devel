function result = parameter__control__callback(callback, context)

% AMPLITUDE - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'auto'
		value = get_control(callback.pal.handle, 'auto', 'value');
		
		set_control(callback.pal.handle, 'block', 'enable', ~value);
end

feature_refresh(context);