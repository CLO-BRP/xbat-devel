function block = get_block_range(rate)

%--
% set block limits in samples
%--

samples = [2^4, 2^18];

%--
% convert to time
%--

block = samples ./ rate;