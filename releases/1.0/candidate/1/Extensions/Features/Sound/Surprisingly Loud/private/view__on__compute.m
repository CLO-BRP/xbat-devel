function [handles, context] = view__on__compute(widget, data, parameter, context)

% SURPRISINGLY LOUD - view__on__compute

%-------------------------
% SETUP
%-------------------------

handles = [];

page = context.page;

% NOTE: this code should not be needed

nyq = [0, 0.5 * context.sound.rate];

if isempty(page.freq)
	page.freq = nyq;
end

time = [page.start, page.start + page.duration]; freq = page.freq;

%-------------------------
% DISPLAY IMAGE
%-------------------------

for k = 1:length(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = get_channel_axes(widget, page.channels(k));
	
	if isempty(ax)
		continue;
	end
	
	im = findobj(ax, 'type', 'image');
	
	%--
	% display components
	%--
	
	switch class(data.spectrogram.value)
	
		case 'double', X = data.spectrogram.value;
			
		case 'cell',  X = data.spectrogram.value{k};
	
	end
	
	if isempty(im)
		
		imagesc(time, nyq, X, 'parent', ax); context.view.paging(k) = 1;
		
	else
		
		paging = isfield(context.view, 'paging') && numel(context.view.paging) >= k && context.view.paging(k);
		
		if ~paging
			
			set(im, 'xdata', time, 'ydata', nyq, 'cdata', X); context.view.paging(k) = 1;
			
		else
			
			X0 = get(im, 'cdata'); start = get(im, 'xdata'); start = start(1);
			
			X = [X0, X]; time = [start, time(2)];
			
			set(im, 'xdata', time, 'cdata', X);
			
		end
			
	end
	
	set(ax, ...
		'tag', int2str(page.channels(k)), ...
		'ylim', freq, ...
		'ydir', 'normal' ...
	);

end

%-------------------------
% DISPLAY COMPONENTS
%-------------------------

handles = zeros(size(data.component.value)); ix = 1;

for k = 1:length(page.channels)
	
	channel = page.channels(k);	ax = get_channel_axes(widget, channel);
	
	if isempty(ax)
		continue;
	end
	
	component = data.component.value([data.component.value.channel] == channel);
	
	for j = 1:length(component)
		handles(ix) = plot_component(ax, component(j), parameter, context); ix = ix + 1;
	end
	
end


%-------------------------
% PLOT_COMPONENT
%-------------------------

function handles = plot_component(ax, component, parameter, context)

%--
% display component box
%--

start = component.time(1); stop = start + component.duration;

low = component.freq(1); high = low + component.bandwidth;

x = [start, stop, stop, start, start]; y = [low, low, high, high, low];

handles = patch( ...
	x, y, 'b'); 

set(handles, ...
	'parent', ax, ...
	'edgecolor', 0.75 * ones(1, 3), ...
	'linestyle', '-', ...
	'facecolor', 'none' ...
);






