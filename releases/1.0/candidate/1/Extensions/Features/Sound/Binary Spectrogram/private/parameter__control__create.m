function control = parameter__control__create(parameter, context)

% BINARY SPECTROGRAM - parameter__control__create

% control = empty(control_create);

fun = parent_fun(mfilename('fullpath')); control = fun(parameter, context);

control = [slider_control('alpha', parameter.alpha, [1, 4]), control];
