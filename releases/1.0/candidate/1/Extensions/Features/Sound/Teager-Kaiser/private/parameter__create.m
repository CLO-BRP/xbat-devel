function parameter = parameter__create(context)

% TEAGER-KAISER - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @energy;

parameter.name = 'energy';


function [X, info] = energy(X, parameter) %#ok<INUSD>

info = struct;

X = teager_kaiser(X);
