function [handles, context] = view__on__compute(widget, data, parameter, context)

% FROST - view__on__compute

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% display centroid overlay
%--

percent = context.ext.parameter.percent;

page = context.page; 

for j = 1:length(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(j)));
	
	if isempty(ax)
		continue;
	end
	
	%--
	% display centroid
	%--
	
	% TODO: select a good color
	
	if iscell(data.frost.value)
		value = data.frost.value{j};
	else
		value = data.frost.value;
	end
	
	value = map_frequency(value, parameter, context, 0);
	
	for k = 1:size(value, 1)

		% NOTE: we visually code the percentile using linewidth and saturation
		
		p = percent(k); color = [1, 0.5, 0];
		
		if p >= 25 && p <= 75
			lw = 2; 
		else
			lw = 1; color = 0.5 * (color + 1);
		end
		
		handles(end + 1) = line( ...
			'parent', ax, ...
			'xdata', data.frost.time, ...
			'ydata', value(k, :), ...
			'color', color, ... 
			'linewidth', lw ...
		); %#ok<AGROW>
		
		% TODO: does it make sense tha 'p' would be 100?
		
		if p == 100
			set(handles(end), 'linestyle', 'none', 'marker', 'o', 'color', scale * [1 0.25 0.5], 'markersize', 4);
		end
		
	end

end

