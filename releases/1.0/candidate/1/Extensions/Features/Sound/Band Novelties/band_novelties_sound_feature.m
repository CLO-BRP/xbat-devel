function ext = band_novelties_sound_feature

ext = extension_inherit(mfilename, band_energies_sound_feature);

ext.short_description = 'Novelty curves analyzing spectral variations in selected frequency bands';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '8862222e-9acf-4a14-a2aa-94301b87a130';

ext.author = 'Daniel Wolff';

ext.email = 'wolffd.mail@googlemail.com';

ext.url = 'www.uni-bonn.de/~wolffd';
