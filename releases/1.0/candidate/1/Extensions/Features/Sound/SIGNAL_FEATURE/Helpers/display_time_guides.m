function handles = display_time_guides(widget, page, context)

%--
% get relevant handles and ensure proper limits
%--

handles = harray_select(widget, 'level', 1);

if isempty(handles) 
	return;
end 

% NOTE: this does not seem to follow from the name of this function 

set(handles, 'xlim', [page.start, page.start + page.duration]);

%--
% set time grid
%--

set_time_grid(handles, context.display.grid, [], context.sound.realtime, context.sound);

%--
% display file boundaries
%--

% NOTE: this duck parent state is used by the following two display functions

data.browser.sound = context.sound; data.browser.grid = context.display.grid;

[times, files] = get_file_boundaries(context.sound, page.start, page.duration);

% TODO: this value is not being properly stored 'context.display.grid.file', this display can be costly

display_file_boundaries(handles, times, files, data);

%--
% display session boundaries
%--

[times, start] = get_session_boundaries(context.sound, page.start, page.duration);

display_session_boundaries(handles, times, start, [], data);
