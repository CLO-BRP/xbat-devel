function [handles, context] = view__on__selection__edit__update(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__selection__edit__update

%--
% setup
%--

tag = 'selection_rubber_band';

handles = findobj(widget, 'tag', tag);

if isempty(handles) 
	return;
end 

sel = data.selection;

%--
% get various coordinates
%--

xlim = sel.event.time;

start = xlim(1); stop = xlim(2); 

%--
% display rectangle
%--

x = [start, start, stop, stop, start]; 

set(handles, 'xdata', x);


