function [feature, context] = reduce(feature, parameter, context)

% SIGNAL_FEATURE - reduce

% NOTE: a typical use of this method if for display reduction

% NOTE: features are assumed to be stored as columns, but there is some flexiblity

% NOTE: the 'parameter' input here is the feature compute parameter struct

% TODO: consider how this should be parametrized in the extension type API

%--
% check whether we consider reduction at all
%--

% NOTE: we only try to reduce when we have some indication of how to do so

try
	target = parameter.reduce.target;
catch
	try
		target = context.ext.parameters.compute.reduce.target;
	catch
		return;
	end
end

if isempty(target)
	return;
end 

%--
% iterate reduction if needed
%--

% NOTE: this currently happends for generalized amplitudes that are multiple

if numel(feature) > 1
	feature = iterate(@reduce, feature, parameter, context); return;
end

%--
% get common time and reduce
%--

if isfield(feature, 'time')
	common.time = feature.time;
else
	common.time = [];
end

if ~isempty(common.time)

	common.rate = get_reduction_rate(common.time, context, target);
	
	% NOTE: here we reduce the actual common feature time
	
	if common.rate == 1
		common.reduced = [];
	else
		common.reduced = reduce_time(common.time, common.rate); feature.time = common.reduced;
	end

end
	
%--
% reduce feature fields
%--

% NOTE: we remove time, and fields that are not structured

field = setdiff(fieldnames(feature), 'time');

for j = length(field):-1:1
	
	if ~isstruct(feature.(field{j}))
		field(j) = [];
	end
	
end

% NOTE: we consider structured non-time feature fields for reduction

for j = 1:length(field)
	
	%--
	% get field time and reduced time
	%--
	
	% NOTE: in this case use the common time
	
	if ~isfield(feature.(field{j}), 'time')
		
		time = common.time; rate = common.rate; reduced = common.reduced;
		
	else
		
		time = feature.(field{j}).time; 
		
		% NOTE: we consider this to be an interval description
		
		if numel(time) == 2
			rate = []; reduced = []; % TODO: something reasonable, we defer until we get to the data
		else
			rate = get_reduction_rate(time, context, target); reduced = reduce_time(time, rate);
		end
		
	end 
	
	% NOTE: continue if we cannot deduce time relation or no reduction is needed
	
	if isempty(time) || isempty(reduced) || rate <= 1.05
		
		feature.(field{j}).reduction = rate; continue;
		
	end
	
	%--
	% reduce feature value fields
	%--
	
	% TODO: consider other strategies and handle other packings
	
	% NOTE: this is maeant for a 'time', 'value' (other names can be used) packing in a feature field
	
	element = fieldnames(feature.(field{j}));
	
	for k = 1:length(element)
		
		% NOTE: if 'time' is a feature field element it has already been reduced
		
		if strcmp(element{k}, 'time')
			
			feature.(field{j}).time = reduced; continue;
			
		end
		
		% NOTE: we skip reduction when the time and value are of different lengths
		
		% NOTE: we record that the reduction did not make sense 
		
% 		db_disp 'need to make sure this works for all features'
		
		if length(feature.(field{j}).(element{k})) ~= length(time) && size(feature.(field{j}).(element{k}), 2) ~= length(time)
			
			% TODO: in this case we will not have 'time' for 'interp1' we want to use resample perhaps
			
			if length(time) == 2
				
				% NOTE: this is meant for the case where we start with a very fine grid
				
				value = feature.(field{j}).(element{k}); value(1) = time(1); value(end) = time(2);
				
				rate = get_reduction_rate(value, context, target); reduced = reduce_time(time, rate);
			
			else
				feature.(field{j}).reduction = -1; db_disp skip-reduction; continue;
			end
			
		end 
		
		% TODO: extend parametrization to include resampling function
		
		%--
		% interpolate considering possible transposition of matrices
		%--
		
		% NOTE: this could be tricky for higher-dimensional features, this works up to matrices
		
		% NOTE: there is no need to align vectors, that explains the first clause in 'flip'
		
		shape = size(feature.(field{j}).(element{k})); flip = (min(shape) > 1) && (shape(1) ~= length(time)) && (shape(2) == length(time));
		
		% NOTE: this asserts equal spacing assumption on the input time
		
		% NOTE: this is a 'v5' feature of the 'interp1' function not currently revealed in the documentation
		
		if diff(fast_min_max(diff(time))) < 10^-6 
			method = ['*', parameter.reduce.method];
		else
			method = parameter.reduce.method;
		end
		
		% NOTE: note the two transpose operations in the flip branch
		
		if ~flip
			feature.(field{j}).(element{k}) = interp1(time, feature.(field{j}).(element{k}), reduced , method);
		else
			feature.(field{j}).(element{k}) = interp1(time, feature.(field{j}).(element{k})', reduced , method)';
		end
		
		feature.(field{j}).reduction = rate;
		
	end
	
end


%-----------------------
% GET_REDUCTION_RATE
%-----------------------

% TODO: consider we may handle a simple time description

function rate = get_reduction_rate(time, context, target)

% NOTE: we cannot reduce without an associated time

if isempty(time)
	rate = []; return;
end

%--
% get number of pages in full page and total points at current rate
%--

pages = context.page.duration / (time(end) - time(1));
	
total = pages * length(time);

%--
% get reduction rate required to meet target
%--

rate = total / target;


%-----------------------
% REDUCE_TIME
%-----------------------

% TODO: consider forcing integer rates when we are close to them

% NOTE: reduce time may update the rate in rounding

function time = reduce_time(time, rate)

% NOTE: we conserve time and values if rate less than or equal to 1

if isempty(time) || isempty(rate) || rate <= 1.05
	return;
end

% NOTE: this suggests an equi-spaced grid

% NOTE: we use the ceiling to make sure we are always positive

time = linspace(time(1), time(end), ceil(length(time) / rate))';


