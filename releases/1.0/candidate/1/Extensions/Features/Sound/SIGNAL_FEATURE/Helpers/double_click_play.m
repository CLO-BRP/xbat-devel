function double_click_play(obj, eventdata, par, type)

% double_click_play - callback for double click play
% --------------------------------------------------
%
% double_click_play(obj, eventdata, par, type)
%
% Input:
% ------
%  obj - callback object
%  eventdata - reserved by MATLAB
%  par - browser handle
%  type - type of play

%--
% return if no parent
%--

if isempty(par)
	return;
end

%--
% check for double click
%--

if double_click(obj)
	
	%--
	% play according to type
	%--
	
	switch type
		
		case 'page', browser_sound_menu(par, 'Play Page');
			
		case 'selection', browser_sound_menu(par, 'Play Selection');
			
	end
	
else
	
	marker = get_browser_marker(par);
	
	ax = ancestor(obj, 'axes');
	
	marker.channel = str2int(get(ax, 'tag'));
	
	point = get(ax, 'currentpoint'); marker.time = point(1);
	
	set_browser_marker(par, marker);
	
end