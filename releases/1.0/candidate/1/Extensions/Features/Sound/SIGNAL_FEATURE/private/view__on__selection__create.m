function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__selection__create

%--
% clear previous selection display
%--

delete(findobj(widget, 'tag', 'selection_handles'));

%--
% get selection
%--

sel = data.selection;

handles = [];

if isempty(sel.event)
	return;
end

%--
% get axes
%--

ax = get_channel_axes(widget, sel.event.channel);

if isempty(ax)
	return;
end 

%--
% display selection
%--

opt = time_event_display; 

opt.color = sel.color; opt.control = 1; % opt.pad = 2 * opt.pad;

handles = time_event_display(ax, sel.event, opt);

set(handles, 'tag', 'selection_handles');

uistack(handles(2), 'bottom'); % NOTE: this relies on handle packing within 'time_event_display'

%--
% set double click play 
%--

handle = findobj(handles, 'type', 'patch');

if isempty(handle)
	return;
end

set(handle, 'hittest', 'on', 'buttondownfcn', {@double_click_play, context.par, 'selection'});


