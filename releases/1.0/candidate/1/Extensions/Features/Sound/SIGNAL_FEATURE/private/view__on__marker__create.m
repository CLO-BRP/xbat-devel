function [handles, context] = view__on__marker__create(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__marker__create

%--
% get channel axes
%--

ax = get_channel_axes(widget, data.marker.channel);

if isempty(ax) 
	handles = []; return;
end

%--
% display widget marker
%--

marker = display_marker(widget, ax, data.marker.time, data);

if isempty(marker)
	handles = []; return;
end 

handles = marker.handles;