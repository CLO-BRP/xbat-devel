function [handles, context] = view__on__slide__start(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__slide__start

%--
% update time guides
%--

page = context.page; page.start = data.slide.time;

handles = display_time_guides(widget, page, context);

%--
% display selection if needed
%--

% TODO: was this being done to provide a location reference during sliding?

% NOTE: we check that an unlogged selection is available

% if ~isempty(data.selection.event) && isempty(data.selection.log)
% 	part = view__on__selection__create(widget, data, parameter, context); handles = [handles, part];
% end
