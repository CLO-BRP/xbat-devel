function parameter = view__parameter__create(context)

% SPECTROGRAM - view__parameter__create

%--
% get parent parameters
%--

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% append parameters
%--

parameter.orientation = 'Horizontal';

% FREQUENCY

% NOTE: these determine the frequency display axes range and the display of a cursor

parameter.min_freq = 0;

parameter.max_freq = 0.5 * context.sound.rate;

% TODO: implement a simple as well as a harmonic cursor, the first is a special case of the latter

parameter.cursor = 0;

parameter.fundamental = 0.05 * context.sound.rate;

parameter.harmonics = 10;

parameter.track = 0;

% VALUE

% NOTE: these control the value range displayed and colormap

parameter.min_value = 0;

parameter.max_value = 0;

parameter.adapt_range = 1;

% COLOR

parameter.colormap = 'gray';

parameter.invert = 1;

parameter.brightness = 0.75;

parameter.contrast = 0.75;

parameter.adapt_contrast = 1;
