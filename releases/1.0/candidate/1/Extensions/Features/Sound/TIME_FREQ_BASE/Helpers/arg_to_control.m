function control = arg_to_control(arg)

switch arg.type
	
	case 'real'
		
		control = slider_control(arg.name, arg.default, [arg.range{:}]);
		
	case 'integer' 
		
		control = slider_control(arg.name, arg.default, [arg.range{:}], 'type', 'integer');
		
	case 'string'
	
		value = find(string_is_member(arg.value, arg.range));
		
		if isempty(value)
			error('Unable to determine control value from argument description.');
		end
		
		control = control_create( ...
			'name', arg.name, ...
			'style', 'popup', ...
			'string', arg.range, ... 
			'value', value ...
		);
	
end