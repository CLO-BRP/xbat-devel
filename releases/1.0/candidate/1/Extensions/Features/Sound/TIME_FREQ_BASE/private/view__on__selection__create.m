function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% SPECTROGRAM - view__on__selection__create

%--
% check for selection
%--

if isempty(data.selection.event)
	handles = []; return;
end

%--
% create parent display
%--

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% modify parent display
%--

freq = data.selection.event.freq;

% NOTE: the first handle is the patch, the remaining handles are the control points

ydata = get(handles(1), 'ydata'); 

ydata(ydata == min(ydata)) = freq(1); ydata(ydata == max(ydata)) = freq(2);

set(handles(1), 'ydata', ydata);

center = mean(freq); 

for k = 2:numel(handles)
	set(handles(k), 'ydata', center);
end
