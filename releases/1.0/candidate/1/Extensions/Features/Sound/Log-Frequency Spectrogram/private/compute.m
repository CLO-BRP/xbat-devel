function [feature, context] = compute(page, parameter, context)

% LOG-FREQUENCY SPECTROGRAM - compute

% feature = struct;

%--
% compute spectrogram
%--

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% compute log-frequency grid
%--

% TODO: consider if the frequencies we want are available

freq = feature.freq;

% feature.freq = parameter.low_freq * 2.^((0:(parameter.octaves * parameter.steps - 1)) / parameter.steps)';

feature.freq = 2.^linspace(log2(200), log2(4000), numel(freq))';

db_disp; format bank; freq', feature.freq'

%--
% interpolate spectra to get log-frequency values
%--

% TODO: improve quality and make faster

% NOTE: matrix pre-multiplication can take advantage of BLAS 3

if numel(page.channels) == 1
	
	feature.spectrogram.value = resample_spectrum(freq, feature.spectrogram.value, feature.freq, parameter);
	
else
	
	for k = 1:numel(feature.spectrogram.value)
		feature.spectrogram.value{k} = resample_spectrum(freq, feature.spectrogram.value{k}, feature.freq, parameter);
	end
	
end


%-------------------------------------
% RESAMPLE_SPECTRUM 
%-------------------------------------

function a1 = resample_spectrum(f0, a0, f1, parameter) 

% resample_spectrum
% -----------------
%
% a1 = resample_spectrum(f0, a0, f1, parameter)
%
% Input:
% ------
%  f0 - initial frequency grid
%  a0 - initial amplitudes
%  f1 - desired frequency grid
%  parameter - parameter
%
% Output:
% -------
%  a1 - amplitudes at desired grid

% NOTE: this is too slow

% a1 = interp1(f0, a0, f1, 'cubic');

% NOTE: these next two are equivalent and the second is twice as fast

% a1 = interp1(f0, a0, f1, 'linear');

a1 = interp1q(f0, a0, f1);

