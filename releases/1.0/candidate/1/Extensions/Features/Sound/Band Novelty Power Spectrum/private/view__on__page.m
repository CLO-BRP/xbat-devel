function [handles, context] = view__on__page(widget, data, parameter, context)

% MEL BAND AUTOCORRELATION - view__on__page

% handles = [];
ax = get(widget,'CurrentAxes');
labl = get(ax, 'YTickLabel');
tick =  get(ax, 'YTick');

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

set(ax, 'YTick', tick,'YTickLabel',labl);
set(widget, 'colormap', jet);

ylabel('Repetition Frequency [Hz]')