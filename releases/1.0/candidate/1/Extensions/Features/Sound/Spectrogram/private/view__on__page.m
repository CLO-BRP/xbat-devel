function [handles, context] = view__on__page(widget, data, parameter, context)

% SPECTROGRAM - view__on__page

%--
% perform parent update
%--

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% update colormap
%--

set_spectrogram_colormap(widget, parameter);

%--
% set guides and cursors
%--

display_freq_guides(widget, parameter, context);

display_harmonic_cursor(widget, parameter, context);

%--
% allow clicks to go through spectrogram images
%--

% TODO: this is very blunt, consider making it smarter

set(findobj(widget, 'type', 'image'), 'hittest', 'off');


%----------------------------
% DISPLAY_FREQ_GUIDES
%----------------------------

function display_freq_guides(widget, parameter, context)

%--
% setup, clean up display and set auxiliary variables
%--

delete(findobj(widget, 'tag', 'SPLIT_LINES'));

page = context.page;

[ytick, yticklabel] = get_freq_ticks(parameter, context);

ytick = map_frequency(ytick, parameter, context, 0);

split = cellfree(parameter.split); split = eval(split(1:2)) / 100;

%--
% update all page channel displays
%--

for j = 1:numel(page.channels)

	%--
	% select channel axes
	%--

	channel = page.channels(j); 

	ax = get_channel_axes(widget, channel);

	if isempty(ax)
		continue;
	end

	%--
	% display ticks
	%--
	
	set(ax, 'ytick', ytick, 'yticklabel', yticklabel);
	
	%--
	% display split line if needed
	%--

	if strcmpi(cellfree(parameter.scale), 'linear-log')

		ylim = get(ax, 'ylim'); freq = ylim(1) + split * diff(ylim);

		xtick = get(ax, 'xtick');

		line( ...
			'parent', ax, ...
			'marker', '+', ...
			'xdata', xtick, ...
			'ydata', freq * ones(size(xtick)), ...
			'color', context.display.grid.color, ...
			'tag', 'SPLIT_LINES' ...
		);

	end

end


%----------------------------
% GET_FREQ_TICKS
%----------------------------

function [ytick, yticklabel] = get_freq_ticks(parameter, context)

switch lower(cellfree(parameter.scale))

	case 'linear'

		base = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000];
		
		[ignore, select] = min(abs(base - 0.1 * (parameter.max_freq - parameter.min_freq)));
		
		ytick = 0:base(select):(0.5 * context.sound.rate);
		
		yticklabel = int_to_str(ytick); for k = 2:2:numel(ytick), yticklabel{k} = ''; end
		
	case 'log'

		ytick = 2.^(0:0.5:16);
		
		yticklabel = int_to_str(ytick); for k = 2:2:numel(ytick), yticklabel{k} = ''; end
		
	case 'linear-log'

		ytick1 = linspace(0, parameter.split_freq, 5); ytick2 = 2.^(0:0.5:15); ytick2 = ytick2(ytick2 > parameter.split_freq);

		ytick = [ytick1, ytick2];

		label1 = int_to_str(ytick1); label1{end} = ['[', label1{end}, ']']; 
		
		label2 = int_to_str(ytick2); for k = 2:2:numel(label2), label2{k} = ''; end

		yticklabel = {label1{:}, label2{:}};

end

select = ytick >= parameter.min_freq & ytick <= parameter.max_freq;

ytick = ytick(select);

yticklabel = yticklabel(select);

