function map = get_spectrogram_colormap(parameter, X)

% TODO: integrate the value range stuff here as well

%--
% compute and update colormap according to view parameters
%--

fun = colormap_to_fun(parameter.colormap);

if parameter.adapt_contrast

	%--
	% adaptive contrast
	%--
	
	map = eval(fun);

	if parameter.invert
		map = flipud(map);
	end

	% TODO: update 'cmap_scale' signature to take target figure as input, various other updates as well

	map = cmap_scale([], X, map);

else

	%--
	% brightness contrast controlled map
	%--
	
	map = brightness_contrast_map(fun, parameter.brightness, parameter.contrast, parameter.invert);

end
