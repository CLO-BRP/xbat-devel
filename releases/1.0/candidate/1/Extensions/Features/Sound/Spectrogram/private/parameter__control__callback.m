function result = parameter__control__callback(callback, context)

% SPECTROGRAM - parameter__control__callback

%--
% setup
%--

% range = resolution_ranges(context);

pal = callback.pal;

%--
% respond
%--

result = struct;

% NOTE: perform a quick check for tabs control

if is_tabs_control(callback.control)
	return; 
end

switch callback.control.name
	
	% NOTE: this is part of a service this extension provides for its children, it does not have a control of this name
	
	case 'use_common_spectrogram'
		
		% NOTE: the palette position manipulation may not be needed if we use a different state placement strategy

		%--
		% get initial palette position and close
		%--
		
		% NOTE: closing the palette stores the current extension state
		
		pos1 = get(pal.handle, 'position'); close(pal.handle); 
		
		%--
		% regenerate extension palette and position
		%--
		
		ext = context.ext; pal.handle = extension_palettes(context.par, ext.name, ext.subtype);
		
		pos2 = get(pal.handle, 'position'); pos2(2)= pos2(2) + (pos1(4) - pos2(4));
		
		set(pal.handle, 'position', pos2);
		
	%--
	% control of frame duration and advance
	%--
	
	case 'frame'
		
		%--
		% update samples based on frame and make sure we are consistent (set-get pattern)
		%--
		
		samples = set_samples_from_frame(pal, context);
		
		set_frame_from_samples(pal, context, samples);
		
	case 'samples'
		
		%--
		% check for proper samples input and rollback if needed
		%--
		
		samples = get_control_value(pal.handle, 'samples');
		
		% TODO: this does not allow us to evaluate expressions in this edit box
		
		% NOTE: factor as function if the test is more complex
		
		if any(~isstrprop(samples, 'digit'))
			set_samples_from_frame(pal, context); return;
		end
		
		samples = eval(samples);
		
		%--
		% set frame based on samples
		%--
		
		set_frame_from_samples(pal, context, samples);
		
	case 'advance'
		
		advance = get_control_value(pal.handle, 'advance');
		
		samples = eval(get_control_value(pal.handle, 'samples'));
		
		value = round(advance * samples);
		
		set_control(pal.handle', 'advance', 'value', value / samples);
	
	case 'adapt'
		
		set_control(pal.handle, 'advance', 'enable', ~callback.value);
		
	%--
	% control of transform length
	%--
	
	case 'fft'
		
		%--
		% make sure we use a better fft size and update the fft frames
		%--
		
		value = set_better_fft_size(pal);
		
		% NOTE: we evaluate directly we know the value is valid
		
		samples = eval(get_control_value(pal.handle, 'samples'));
		
		set_control(pal.handle, 'fft_frames', 'value', value / samples);
		
	case 'fft_frames'
		
		frames = get_control_value(pal.handle, 'fft_frames');
		
		samples = eval(get_control_value(pal.handle, 'samples'));
			
		value = better_fft_size(frames * samples);
		
		set_control(pal.handle, 'fft', 'value', value);
		
		set_control(pal.handle, 'fft_frames', 'value', value / samples);
			
	%--
	% smooth control
	%--
	
	case 'smooth'
		
		set_control(pal.handle, 'method', 'enable', context.ext.parameter.smooth);
		
end

%--
% update parameter display, always
%--

% TODO: this may not be the right way to get the parameter values
	
parameter = get_control_values(pal.handle);

parameter_plot(pal.handle, parameter, context);

%--
% update our own view, if we are active
%--

if get_control_value(pal.handle, 'active') && strcmp(context.ext.type, 'sound_feature')
	
	feature_refresh(context);
	
end
	
%--
% update active children extensions if we are the Spectrogram
%--

% TODO: figure out how to update active child detector and how to factor this

if strcmpi(context.ext.name, 'spectrogram')
	
	% TODO: eventually we should make use of the common spectrogram computation for efficiency

	%--
	% update children extensions as needed
	%--
	
	child = get_extension_children(context.ext);

	for k = numel(child):-1:1

		if ~extension_is_active(child(k), context.par) || ~strcmp(child(k).type, 'sound_feature')
			child(k) = [];
		end

	end

	for k = numel(child):-1:1
	
		 [child(k), ignore, child_context] = get_browser_extension('sound_feature', context.par, child(k).name);
		 
		 % NOTE: we assume all children have kept their inherited parameter
		 
		 if ~child(k).parameters.compute.use_common_spectrogram
			continue;
		 end
		 
		 feature_refresh(child_context);
		 
	end
	
end

%---------------------------------
% SET_FRAME_FROM_SAMPLES
%---------------------------------

function samples = set_samples_from_frame(pal, context, frame)

%--
% handle input
%--

if nargin < 3
	frame = get_control_value(pal.handle, 'frame');
end

%--
% set samples control
%--

samples = round(frame * context.sound.rate);
		
set_control(pal.handle, 'samples', 'value', int2str(samples));


%---------------------------------
% SET_FRAME_FROM_SAMPLES
%---------------------------------

function frame = set_frame_from_samples(pal, context, samples)

%--
% handle input
%--

if nargin < 3
	samples = get_control_value(pal.handle, 'samples');
end

%--
% set frame controls and update related fft controls
%--

frame = samples / context.sound.rate;

set_control(pal.handle, 'frame', 'value', frame);

set_control(pal.handle, 'fft', 'min', samples);

value = better_fft_size(get_control_value(pal.handle, 'fft'));

set_control(pal.handle, 'fft', 'value', value);

set_control(pal.handle, 'fft_frames', 'value', value / samples);


%---------------------------------
% SET_BETTER_FFT_SIZE
%---------------------------------

function value = set_better_fft_size(pal, value)

if nargin < 2
	value = get_control_value(pal.handle, 'fft');
end

value = better_fft_size(value);

set_control(pal.handle, 'fft', 'value', value);

