function [handles, context] = view__on__compute(widget, data, parameter, context)

% SPECTROGRAM - view__on__compute

% TODO: fix discrepancy between image display and axes, in frequency scale

%-----------------
% SETUP
%-----------------

handles = [];

%--
% set some variables for clarity
%--

% TODO: when we toggle the display, the page is not the same as when we page, we are missing the 'freq' field

page = context.page;

time = [data.time(1), data.time(end)]; 

% freq = [data.freq(1), data.freq(end)]; % NOTE: this is the feature freq range, not the display

%-----------------
% DISPLAY
%-----------------

for k = 1:length(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(k)));
	
	if isempty(ax)
		continue;
	end
	
	im = findobj(ax, 'type', 'image');
	
	%--
	% get spectrogram image data
	%--
	
	% TODO: factor contingent display code
	
	if ~isfield(data, 'spectrogram')
		
% 		handle = big_centered_text(ax, 'No spectrogram available at this resolution.'); set(handle, 'tag', 'no-data-message');
		
		if ~isempty(im)
			set(im, 'visible', 'off');
		end
		
		continue;	
		
	end
	
	switch class(data.spectrogram.value)

		case 'double', X = data.spectrogram.value;

		case 'cell',  X = data.spectrogram.value{k};

	end

	X = lut_dB(X);

	X = resample_spectrogram(X, data.freq, parameter);

	if ~isempty(im)
		set(im, 'visible', 'on');
	end
	
	%--
	% display spectrogram image
	%--

	if isempty(im)
		
		% NOTE: the 'get' and 'set' of callbacks is because 'imagesc' has a habit of clearing these
		
		callback = get_callbacks(ax);
		
		imagesc(time, [0, 1], X, 'parent', ax); context.view.paging(k) = 1;
		
		set_callbacks(ax, callback);
		
	else
		
		paging = isfield(context.view, 'paging') && numel(context.view.paging) >= k && context.view.paging(k);
		
		if ~paging
			
			set(im, 'xdata', time, 'cdata', X); context.view.paging(k) = 1;
			
			set(im, 'ydata', [0, 1]);
			
		else
			
			X0 = get(im, 'cdata'); start = get(im, 'xdata'); start = start(1);
			
% 			db_disp 'memory problems'; size(X0), size(X)
			
			X = [X0, X]; time = [start, time(2)]; 
			
			set(im, 'xdata', time, 'cdata', X);
			
		end
			
	end
	
	set(ax, ...
		'tag', int2str(page.channels(k)), 'xlim', time, 'ydir', 'normal' ...
	);

	set(ax, 'ylim', [parameter.min_freq, parameter.max_freq] / (0.5 * context.sound.rate));
	
end
