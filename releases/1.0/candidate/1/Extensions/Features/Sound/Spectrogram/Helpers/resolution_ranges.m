function [range, default] = resolution_ranges(context)

% NOTE: here we define resolution ranges and defaults for spectrogram

% TODO: we should consider using a simple frame duration rather than a simple transform length

% NOTE: set a simple frame duration and set a better fft size that relates to it

%--
% set ranges
%--

range.fft = [16, 32768];

range.time = range.fft / context.sound.rate;

range.freq = context.sound.rate ./ fliplr(range.fft);

%--
% set defaults
%--

default.fft = 512;

default.time = 0.5 * default.fft / context.sound.rate;

default.freq = context.sound.rate / default.fft;