function control = parameter__control__create(parameter, context)

% RENYI ENTROPY - parameter__control__create

%--
% create own controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'slider', ...
	'name', 'exponent', ...
	'space', 1.5, ...
	'max', 10, ...
	'value', parameter.exponent ...
);

control(end + 1) = section_collapse_control(1);

%--
% append parent controls
%--

fun = parent_fun(mfilename('fullpath'));

control = [control, fun(parameter, context)];

