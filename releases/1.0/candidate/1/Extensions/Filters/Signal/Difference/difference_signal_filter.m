function ext = difference_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Difference filter';

ext.category = {ext.category{:}, 'Enhance', 'Frequency'};

% ext.version = '';

ext.guid = '6e955877-ab4c-4ab9-bf26-8eb51c3f7028';

% ext.author = '';

% ext.email = '';

% ext.url = '';

