function [parameter, context] = parameter__compile(parameter, context)

% DIFFERENCE - parameter__compile

%--
% perform parent compilation if needed
%--

% NOTE: this is the parent that should go into the code generator

fun = parent_fun(mfilename('fullpath'));

if ~isempty(fun)
	parameter = fun(parameter, context);
end

%--
% compile filter from order
%--

% TODO: implement a binomial filter, how should these two share code

d = [-1, 1]; b = d;

for k = 1:(parameter.order - 1)
	b = conv(b, d);
end

% NOTE: filter normalization normalizes gain

if parameter.normalize
	b = b ./ sum(abs(b));
end

%--
% pack filter in parameter struct
%--

parameter.filter.b = b;

parameter.filter.a = 1;
