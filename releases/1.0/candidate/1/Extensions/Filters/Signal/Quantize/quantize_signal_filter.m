function ext = quantize_signal_filter

ext = extension_inherit(mfilename, point_operation_base_signal_filter);

ext.short_description = 'Quantize to number of bits';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '44d2be08-bd5d-4fff-9430-5235403e7abe';

% ext.author = '';

% ext.email = '';

% ext.url = '';

