function result = parameter__control__callback(callback, context)

% POINT_OPERATION_BASE - parameter__control__callback

result = struct;

% NOTE: this is a strange signature, also this should be a function

ext = get_browser_extension(context.ext.subtype, callback.par.handle, context.ext.name);

result.handles = display_point_operation(callback.pal.handle, ext.parameter);
