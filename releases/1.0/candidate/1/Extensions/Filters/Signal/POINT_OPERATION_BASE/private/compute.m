function [X, context] = compute(X, parameter, context)

% POINT_OPERATION_BASE - compute

if isempty(parameter.map)
	return;
end

% TODO: implement actual filtering using a lookup-table

X = parameter.map(X, parameter);
