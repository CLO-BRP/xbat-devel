function ext = butterworth_band_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Bandpass Butterworth Filtering';

ext.category = {ext.category{:}, 'Frequency', 'IIR'};

ext.version = '0.1';

ext.guid = 'e5fd8ecf-6b4d-4900-829b-0525a441d517';

% ext.author = '';

ext.email = 'harold.figueroa@gmail.com';

% ext.url = '';

