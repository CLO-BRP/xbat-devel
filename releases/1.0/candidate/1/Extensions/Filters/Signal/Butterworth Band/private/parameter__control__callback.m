function result = parameter__control__callback(callback, context)

% BUTTERWORTH BAND - parameter__control__callback

%--
% control callback
%--

switch callback.control.name

	case 'sel_config'

		[selection, count] = get_browser_selection(context.par);

		if ~count
			return;
		end

		freq = selection.event.freq; pal = callback.pal.handle;
		
		set_control(pal, 'min_freq', 'value', freq(1));

		set_control(pal, 'max_freq', 'value', freq(2));
		
end

%--
% update display
%--

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);
