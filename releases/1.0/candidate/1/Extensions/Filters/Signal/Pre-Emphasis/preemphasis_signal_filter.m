function ext = preemphasis_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = '';

% ext.category = {ext.category{:}};

ext.version = '0.1';

ext.guid = '423729d2-e236-4b95-abb8-a8fd09ebf8d6';

ext.author = 'Harold and Sara';

ext.email = 'harold.figueroa@gmail.com';

% ext.url = '';

