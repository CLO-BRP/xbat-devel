function [X, context] = compute(X, parameter, context)

% PRE-EMPHASIS - compute

fun = parent_fun(mfilename('fullpath')); [X, context] = fun(X, parameter, context);
