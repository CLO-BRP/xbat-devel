function ext = notch_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Notch a given frequency';

ext.category = {ext.category{:}, 'Frequency'};

% ext.version = '';

ext.guid = '8af88c1d-d1aa-4b3f-bb14-8c53c5b78647';

ext.author = 'Matt';

ext.email = '';

% ext.url = '';

