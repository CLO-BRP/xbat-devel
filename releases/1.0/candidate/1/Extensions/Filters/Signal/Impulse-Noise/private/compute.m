function [X, context] = compute(X, parameter, context)

% IMPULSE-NOISE - compute

%--
% get stateful random samples
%--

N = get_random_samples(X, parameter, context);

%--
% get uniform noise vector and probability of impulse
%--

p = parameter.percent / 100;

%--
% select positions and place impulses
%--

switch lower(parameter.type{1})
	
	case 'mixed', X(N > (1 - 0.5 * p)) = 1; X(N < 0.5 * p) = -1;
		
	case 'negative', X(N < p) = -1;
		
	case 'positive', X(N < p) = 1;
		
end


% TODO: this needs to be factored

%------------------------------
% GET_RANDOM_SAMPLES
%------------------------------

function N = get_random_samples(X, parameter, context)

%--
% setup
%--

% NOTE: get the seed for the generator and the sound and page fields required

if ischar(parameter.seed)
	parameter.seed = eval(parameter.seed);
end

seed = parameter.seed;

rate = get_sound_rate(context.sound); page = context.page;

%--
% get stateful samples for second-long tile-cover
%--

seeds = get_channel_seeds(seed, page, context);

N = rand_state_blocks(rate, ceil(page.duration) + 1, seed + floor(page.start) + seeds); % * page.channels);

%--
% select page samples from cover
%--

N = N(:); 

part = page.start - floor(page.start);

% NOTE: we remove trailing samples at start and end

N(1:(round(part * rate) - 1)) = [];

N = N(1:length(X));


%------------------------------
% GET_CHANNEL_SEEDS
%------------------------------

% NOTE: this gets a seed offset for each channel using the channel and seed

function seeds = get_channel_seeds(seed, page, context)

%--
% generate seed offsets for all channels
%--

nch = context.sound.channels;

seeds = 10^6 * rand_state(nch, seed);

%--
% select only the page channel seeds
%--

seeds = seeds(page.channels);

