function [parameter, context] = parameter__compile(parameter, context)

% FILTER CHAIN - parameter__compile

if isempty(context.pal)
	return;
end

% NOTE: this function is here to help with the unusual parameter store for this extension

control = get_control(context.pal, 'extensions');

parameter.selected = get(control.handles.obj, 'value');