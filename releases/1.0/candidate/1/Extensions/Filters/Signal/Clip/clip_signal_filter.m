function ext = clip_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Clip samples to percent of level';

ext.category = {'Nonlinear'};

ext.version = '0.1';

ext.guid = '0dfe5dcb-6cf1-492c-a9b9-f7e2443be7e3';

ext.author = 'Harold and Matt';

ext.email = '';

ext.url = 'http://xbat.org';

