function ext = resonant_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Resonate a given frequency';

ext.category = {ext.category{:}, 'Enhance', 'Frequency'};

ext.version = '0.1';

ext.guid = 'b8caefd8-66e2-4614-a51d-6ca39b6164d5';

ext.author = 'Matt';

ext.email = '';

% ext.url = '';

