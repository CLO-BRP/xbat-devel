function result = parameter__control__callback(callback, context)

% BUTTERWORTH - parameter__control__callback

% result = struct;

switch callback.control.name
	
	case 'sel_config'
	
		%--
		% get handles
		%--
		
		pal = callback.pal.handle; par = callback.par.handle;
		
		%--
		% get selection
		%--
		
		[selection, count] = get_browser_selection(par);
		
		if ~count
			return;
		end
		
		freq = selection.event.freq;
		
		%--
		% update cutoff based on type
		%--
		
		type = get_control(pal, 'type', 'value'); type = type{1};
		
		if strcmpi(type, 'low')
			value = freq(2);
		else
			value = freq(1);
		end
		
		% NOTE: we use this control name so the parent can draw a guide
		
		set_control(pal, 'max_freq', 'value', value);
		
end

%--
% update filter response display using parent callback
%--

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);
