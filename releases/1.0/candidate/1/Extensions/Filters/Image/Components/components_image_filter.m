function ext = components_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Label connected-components of binary image';

% ext.category = {};

ext.version = '';

ext.guid = 'bff72677-19c0-4122-92b9-7b628551a058';

ext.author = '';

ext.email = '';

ext.url = '';

