function ext = morphological_gradient_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Gradient via erosion, dilation, or both';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '8eb6f24f-338d-4a15-a1d3-213e3f32df6a';

% ext.author = '';

% ext.email = '';

% ext.url = '';

