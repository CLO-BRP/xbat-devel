function [X, context] = compute(X, parameter, context)

% MEDIAN FILTER - compute

SE = create_se(parameter);

X = median_filter(X, SE);