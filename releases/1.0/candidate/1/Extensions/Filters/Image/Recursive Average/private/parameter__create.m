function parameter = parameter__create(context)

% RECURSIVE AVERAGE - parameter__create

parameter = struct;

parameter.output = {'signal'}; 

parameter.half_life = 0.5;

parameter.adaptive = 1;

parameter.positive = 1;
