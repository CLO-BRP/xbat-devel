function result = parameter__control__callback(callback, context)

% LINEAR_BASE - parameter__control__callback

% NOTE: this filter is lazy so no update is needed

result = struct; result.update = 0;

plot_mask(callback.pal.handle, context.ext.parameter);