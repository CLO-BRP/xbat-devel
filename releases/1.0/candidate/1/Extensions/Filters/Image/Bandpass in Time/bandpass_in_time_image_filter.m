function ext = bandpass_in_time_image_filter

ext = extension_inherit(mfilename, time_fir_base_image_filter);

ext.short_description = 'Bandpass image along the time dimension';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '0c636b85-14a2-464e-91b9-89e994675037';

% ext.author = '';

% ext.email = '';

% ext.url = '';

