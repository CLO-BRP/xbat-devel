function [types,ix] = se_types

% se_types - structuring element type strings
% -------------------------------------------
%
% [types,ix] = se_types
%
% Output:
% -------
%  types - cell of type strings
%  ix - default value index

% NOTE: for lists that may change this formatting is convenient

types = { ...
	'Rectangle', ...
	'Disc', ...
	'Diamond', ... 
	'Star', ...
};

ix = 2;
