function parameter = parameter__create(context)

% RANK FILTER - parameter__create

%--
% get parent parameters
%--

fun = parent_fun(mfilename('fullpath'));

parameter = fun(context);

%--
% add rank parameter
%--

parameter.rank = 1;
