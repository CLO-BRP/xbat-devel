function ext = closing_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Morphological closing via dilation-erosion';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '450d7a9a-7174-4b8b-a79a-a29a04590c1b';

% ext.author = '';

% ext.email = '';

% ext.url = '';

