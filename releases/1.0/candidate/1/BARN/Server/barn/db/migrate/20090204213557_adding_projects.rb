class AddingProjects < ActiveRecord::Migration
  
  def self.up
    create_table :project, :force => true do |t|
      t.string :name, :null => false
      t.text :description
      t.boolean :visible, :default => true
      t.boolean :public, :default => true
      t.integer :user_id, :null => false
      t.timestamp :created_at, :modified_at
    end

    create_table :project_user, :force => true do |t|
      t.integer :project_id, :user_id
      t.string :state
      t.integer :role_id, :null => false
      t.timestamp :created_at
    end
  end

  def self.down
    drop_table :project_user
    drop_table :project
  end
  
end
