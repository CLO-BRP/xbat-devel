class RenameUpdatedAtToModifiedAtEvents < ActiveRecord::Migration
  def self.up
    rename_column :event, :updated_at, :modified_at
  end

  def self.down
    rename_column :event, :modified_at, :updated_at
  end
end
