class AddMtimeAndCtime < ActiveRecord::Migration
  def self.up
		add_column :data_file, :mtime, :datetime
		add_column :data_file, :ctime, :datetime
  end

  def self.down
		remove_column :data_file, :ctime
		remove_column :data_file, :mtime
  end
end
