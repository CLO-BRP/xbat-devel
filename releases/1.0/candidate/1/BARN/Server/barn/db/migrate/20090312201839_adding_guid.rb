require 'simple_uuid'

class AddingGuid < ActiveRecord::Migration

  @@tables = %w{user sound event log project recording image note task}

	def self.up
		@@tables.each do |table|
			begin remove_column table, :guid rescue true end
			add_column table, :guid, :string
		end
		
		@@tables.each do |table|
			table.capitalize.constantize.find(:all).each{ |record|
				begin
					record.guid = SimpleUUID.generate
					record.save
				rescue Exception => e
					puts e
				end
			}
		end
  end

  def self.down
		@@tables.each do |table|
			remove_column table, :guid
		end
  end
end
