class AddTypeToPeer < ActiveRecord::Migration
  def self.up
		add_column :peer, :type, :string
  end

  def self.down
		remove_column :peer, :type
  end
end
