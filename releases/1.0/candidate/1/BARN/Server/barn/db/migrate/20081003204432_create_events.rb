class CreateEvents < ActiveRecord::Migration
  def self.up
    begin drop_table "event" rescue true end
    
    create_table :event do |t|
      t.column :start, :float
      t.column :duration, :float
      t.column :low, :float
      t.column :high, :float
      t.column :channel, :integer
      t.column :score, :float
      t.column :log_id, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :event
  end
end
