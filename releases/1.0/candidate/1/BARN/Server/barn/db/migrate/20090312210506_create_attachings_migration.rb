class CreateAttachingsMigration < ActiveRecord::Migration
  def self.up
		# This table is used for creating loose associations or collections of things 
		# between things like Images and Documents (asset) and Projects (attachable). 
		# Since it is polymorphic, Images and Documents can also be attached to other things, like Users and Sounds
    create_table "attaching" do |t|
      t.integer  "attachable_id"
      t.string   "attachable_type"
      t.integer  "asset_id"
      t.string   "asset_type"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    # add_index "attaching", ["attachable_id"], :name => "index_attachings_on_attachable_id"
  end
  
  def self.down
    drop_table :attaching
  end
end