class AddSomeFields < ActiveRecord::Migration
  def self.up
		add_column :recording, :data_file_id, :integer
		add_column :data_file, :in_ftp, :boolean
  end

  def self.down
		remove_column :data_file, :in_ftp
		remove_column :recording, :data_file_id
  end
end
