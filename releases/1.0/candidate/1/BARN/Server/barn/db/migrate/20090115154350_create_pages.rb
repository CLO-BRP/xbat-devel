class CreatePages < ActiveRecord::Migration
  def self.up
    begin drop_table "page" rescue true end
    
    create_table :page do |t|
      t.string :title
      t.string :permalink
      t.text :body
      t.datetime :created_at
      t.datetime :modified_at

      t.timestamps
    end
    #Page.create :title => "Home", :permalink => 'home', :body => "homepage.. edit me."
  end

  def self.down
    drop_table :page
  end
end
