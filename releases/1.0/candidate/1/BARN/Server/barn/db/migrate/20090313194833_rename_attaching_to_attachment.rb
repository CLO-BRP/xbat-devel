class RenameAttachingToAttachment < ActiveRecord::Migration
  def self.up
		rename_table :attaching, :attachment
  end

  def self.down
		rename_table :attachment, :attaching
  end
end
