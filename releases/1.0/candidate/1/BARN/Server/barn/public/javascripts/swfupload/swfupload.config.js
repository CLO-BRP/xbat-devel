/* 
  http://demo.swfupload.org/Documentation
*/

function swfupload_create(options){
  

    var swfupload = new SWFUpload({
      upload_url: options.upload_url,
      flash_url:  "/javascripts/swfupload/swfupload.swf",
      
      post_params: {
        authenticity_token : options.authenticity_token,
        sound_id: options.sound_id
      },
      
      // Fucking button stylings for stupid ass Flash 10
      button_placeholder_id : "upload_button",
      button_width: 100,
      button_height: 20,
      button_text_left_padding : 0, 
      button_text_top_padding : -2,
      button_text_style : ".redText { color: #FF0000; }",
      button_cursor : SWFUpload.CURSOR.HAND,
      button_text : "<font face='Trebuchet MS' size='17px'>Browse</font>",
      button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
      //button_action : SWFUpload.BUTTON_ACTION.SELECT_FILES,
      
      // File specs
      file_types: "*.mp3; *.wav; *.aif; *.aiff; *.flac",
      file_types_description: "Audio Files Only",
      file_upload_limit: 0,    // No upload limit
      file_queue_limit:  0,     // No queue limit
      
      moving_average_history_size: 40,

      // The event handler functions are defined in handlers.js
      file_queued_handler : fileQueued,
      file_queue_error_handler : fileQueueError,
      file_dialog_complete_handler : fileDialogComplete,
      upload_start_handler : uploadStart,
      upload_progress_handler : uploadProgress,
      upload_error_handler : uploadError,
      upload_success_handler : uploadSuccess,
      upload_complete_handler : uploadComplete,
      queue_complete_handler : queueComplete, // Queue plugin event
      
      custom_settings : {
        progressTarget : "fsUploadProgress",
        cancelButtonId : "btnCancel",
        tdFilesQueued : document.getElementById("tdFilesQueued"),
        tdFilesUploaded : document.getElementById("tdFilesUploaded"),
        tdErrors : document.getElementById("tdErrors"),
        tdCurrentSpeed : document.getElementById("tdCurrentSpeed"),
        tdAverageSpeed : document.getElementById("tdAverageSpeed"),
        tdMovingAverageSpeed : document.getElementById("tdMovingAverageSpeed"),
        tdTimeRemaining : document.getElementById("tdTimeRemaining"),
        tdTimeElapsed : document.getElementById("tdTimeElapsed"),
        tdPercentUploaded : document.getElementById("tdPercentUploaded"),
        tdSizeUploaded : document.getElementById("tdSizeUploaded")
      }
      
    });
    
    return swfupload;  
  
  
}
