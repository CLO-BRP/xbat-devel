//-----------------------
// CONFIGURE
//-----------------------

// The BARN global namespace Object
BARN = {};

// TODO: consider adding a message to the activity indicator

$.ajaxSetup({
	beforeSend: function (request) {
		$('#activity-indicator').click( function() { $('#activity-indicator').fadeOut(250); });
		$('#activity-indicator').show();
		//console.log(request);
  },
	complete: function() {
		$('#activity-indicator').fadeOut(250);
	},
	success: function (request, status) {
		$('#activity-indicator').fadeOut(250); // NOTE: this is sometimes called, and complete is not ... for instance when a listing lazy loads
	},
  error: function (request, status) {
    // NOTE: this gets called erroneously ... how ironic
	}
});





//-----------------------
// FLASH NOTICES
//-----------------------

function flash(type, text){
    //var flash = $("#flash-" + type);
	var flash = $("#activity-indicator");
  
    // stop the running animations
    flash.stop(true);

    flash.html(text);
    // show the notice
	flash.show();

	flash.animate( {opacity:1.0} , 2500, function() { flash.fadeOut(250, function(){ $(this).html(""); $(this).css("background-color", "#FFFFCE"); }); } );
};

function flash_notice(text){ 
    flash('notice', text); 
};

function flash_error(text){
	$("#activity-indicator").css("background-color", "#FFCCCC");
    flash('error', text); 
};





function close_dialog(dialog_div){
    if (dialog_div.dialog('isOpen')) dialog_div.dialog('close');  
}

function refresh_container(container_id){
    try {
        $("#d-refresh-" + container_id).trigger('click'); 
    } catch(e) { 
        // console.log(e);
    }
}




//-----------------------
// RATING
//-----------------------

function enable_ratings(options) {
	options = (typeof options == 'undefined') ? {} : options;

	// Define the options we are using
	var callback = options.callback;
	  
	// NOTE: I have removed livequery for the moment, it was causing a delay in the rendering of the ratings so you would see options for a second, then ratings
    
	$('.render-ratings').each( function() {

		$(this).removeClass('render-ratings');
    
		$(this).rating({
			readOnly: $(this).parent().hasClass("read-only"), 
			 
			callback: function(value, link) {
				var form = this.form;
				form['rating[rating]'].value = value;
				
				var field_data = $("form#" + form.id + " input[name^='rating[']").fieldSerialize();        
				var base_uri   = form.action;
                
				// SAVE OR UPDATE RATING
                      
				if(value){
					$.post(base_uri + '?' + field_data, null, function(data) { 
						var rating = data.rating
						form['rating[id]'].value = rating.id;
						if(callback) callback(rating);
					}, 'json');
					return;
				}
                
				// DELETE RATING
				
				var rateable_id = $("form#" + form.id + " input[name='rating[rateable_id]']").fieldValue();        
				var rating_id   = $("form#" + form.id + " input[name='rating[id]']").fieldValue();
				var uri = '/ratings/' + rating_id + '?_method=delete'
                
				if(rating_id[0] != ''){
					$.post(uri, null, callback);
				}
				
				// Make sure the rating value is cleared from the form
				form['rating[id]'].value = null;
			}
        
		});   
		// $(this).rating
	});  
	// $('.render-ratings').each
}


function render_simple_ratings() {
    
	// NOTE: the first selector means that the element has both classes
    
	$('.render.simple-rating').removeClass('render').each( function () {
        
		//--
		// we get the rating target object and current value from the element
		//--
        
		var target = $(this).attr('target'); var current = $(this).html();
        
		//--
		// create rating markup and replace this element
		//--
        
		var rating = $('<div class="simple-rating">');
        
		for (k = 0; k <= 5; k++) {
			
			if (k == 0) {
				hook = 'class="delete"'; 
			} else if (k <= current) {
				hook = 'class="star-on"';
			} else {
				hook = '';
			}
			
			var link = $('<a ' + hook + '" href="' + target + '/ratings?value=' + k + '" title="' + k + '">' + k + '</a>'); 
						
			// Set the click behavior of the star
						
			$(link).click(function(event){

				$(this).siblings().removeClass('star-on');
				
				$(this).addClass('star-on').prevAll().addClass('star-on');
				
				$.post($(this).attr('href'), null, null, "json");
				
				return false;
			});
			
			// Add the newly created link element to the rating div

			rating.append(link);
			
		}
        
		$(this).replaceWith(rating);
        
		//--
		// set children hover and click behavior
		//--
        
		$('.simple-rating a').hover ( function () {
			$(this).prevAll().addClass('hover');
		}, function () {
			$(this).prevAll().removeClass('hover');
		});
        
	});   
	  
}


//-----------------------
// TABS
//-----------------------

// NOTE: the idempotency 'render' convention may be all we need, not 'livequery'

function render_ajax_tabs () {
    
    $('.render-ajax-tabs').removeClass('render-ajax-tabs').each ( function () {
        
        // NOTE: get tabs, update selected, load reference with load indication, and select first tab
        
        var id = this.id;
        
        var tabs = $("#" + id + " .tabs-list li a");
    
        $(tabs).click( function () {

            $(this).parent().addClass('selected').siblings().removeClass('selected');

			if ($(this).hasClass('iframe')) {
				$("#" + id + " > .tabs-target").html('<iframe frameborder="0" class="tab-content" src="' + this.href +'"></iframe>');
			} else {
				$("#" + id + " > .tabs-target").html('Loading ...').load(this.href);
			}    
            return false;
        });
                    
        $(tabs[0]).click();

    });
    
}


function ac_cleanup() {
	
	// $('#ac_results').hide(); // or remove both fail
}

//-----------------------
// VERBS
//-----------------------

/**
 * Configure the modal jQuery dialog. Requires jQuery dialog plugin.
 *
 * @param {jQuery} form 
 * This is a jQuery object containing the target form
 * @param {Object} options 
 * JSON object containing dialog options
 */
function configure_dialog(form, options){
  	  
	var defaults = {
		'closeOnEscape': true,
		'bgiframe': true,
		'height': 300,
		'width': 500,
		'modal': true,
		'title': 'BARN dialog'
	};
	
	var options = (options) ? options : defaults;
	
	// Loop through all the options to mark up, falling back to defaults as necessary
	$.each(['bgiframe', 'height', 'width', 'closeOnEscape', 'modal', 'title'], function(index, value){  
		if(options[value]){
			form.dialog('option', value, options[value]);
		}
		else{ 
			form.dialog('option', value, defaults[value]);
			options[value] = defaults[value];
		}
	});

	// This seems to actually execute the set options and open the dialog
	form.dialog(options);
}

function closeDialog( target )
{
	//$(target).dialog('close');
	$(".ui-dialog-titlebar-close").click();

}

/**
 * Enable the 'verbs' on the page
 *
 * @param {String} container_id 
 * The DOM id of the containing div where we can find the verbs
 */
function enable_verbs(container_id){

	$('a.verb', '#' + container_id ).click( function () {


		var link = $(this);
		// Options are held as a JSON string in a link attribute
		var options  = link.attr('dialog_options');
		options = (options) ? $.parseJSON(options) : {};
		// Add the title
		options['title'] = link[0].innerHTML;

		var is_independent = $(this).hasClass('independent');

		var selected = get_selected_elements('#' + container_id);
		var ids = [];

		$.each(selected, function(k, val){
			var split  = val.split("-");
			var id     = split[split.length - 1];
			ids.push(id);
		});

		var currSize = $('.griddata .size', '#' + container_id).html();
		var currPage = $('.griddata .page', '#' + container_id).html();

			// Construct the URI

		var path  = $.url.setUrl(this.href).attr("path");
		var query = $.url.setUrl(this.href).attr("query");

		//adding pagination
		//TODO this should be turned into a common client-side url generation function
		query += "&per_page=" + currSize;
		query += "&page=" + currPage;


		var set   = "{" + ids.join(',') + "}";
		var uri   = path + "/" + set + "?" + query;




			// There was nothing selected, return.

		if(set == "{}" && !is_independent ){
		  flash_error("Nothing selected.");
		  return false;
		}

		// Prepare the dialog window

		var form = $('#verb-form');




		var noview = ( getUriVars(uri)['noview'] == 'true' || options['nodialog'] );		

		// Now make the request for the intermediary form...

		if ( options['post'] ) {

			$.post(uri, {context: "'" + container_id + "'"}, function (data) {
				if (noview) {
					refresh_container( container_id );
					flash( 'notice', data );
				} else {
					form.html(data);
					configure_dialog(form, options);

					// open the form if its closed
					if ( form.dialog( 'isOpen' ) == false) {
						form.dialog( 'open' );
					}
				}
			}, 'text');

		} else {

			//TODO: refactor to pass along flash messages if errors occur
			//TODO: uri should tell controller to avoid rendering full response
			$.get(uri, {context: "'" + container_id + "'"}, function (data) {
				if (noview) {
					flash( 'notice', data );
				} else {
					form.html(data);
					configure_dialog(form, options);

					// open the form if its closed
					if( form.dialog( 'isOpen' ) == false) {
						form.dialog( 'open' );	
					}
				}
			}, 'text');
		}

		return false;
	});
}

BARN.clickTarget = null;

//function enable_review(container_id){
//
//	$('.sidebar a', '#' + container_id ).click( reviewAction );
//}
//
//function reviewAction() {
//
//	BARN.clickTarget = this;
//
//	$.post(this.href, {}, function (data) {
//		// refresh_container( container_id );
//		//TODO send back proper error
//		if ( data != 'ERROR' ){
//			var target = $(BARN.clickTarget).closest('.review_panel');
//			$(target).html(data);
//			$(target).children('a').click( reviewAction );
//		} else {
//			flash( 'notice', data );
//		}
//
//	}, 'text');
//
//	return false;
//}
//
//
//function toggleReviewLink( target ){
//
//	var reviewEnum = [ 'yes', 'no', 'maybe', 'yes', 'no' ]; // trick to make replacing easier
//
//	var currClass = null;
//
//	var currIndex = 2;  // default - will trigger 'yes' when class is none
//
//	if ( target.hasClass('none') ) {
//		target.removeClass('none');
//
//		currIndex = 2;
//	}
//
//	for (var i = 0; i < 3; i++) {
//
//		if ( target.hasClass(reviewEnum[i]) ) {
//			currIndex = i; target.removeClass(reviewEnum[i]); i = 4; // break
//		}
//	}
//
//	var currUrl = target.attr('href');
//
//	target.addClass(reviewEnum[currIndex + 1]);
//
//	target.attr('href', currUrl.replace('review:' + reviewEnum[currIndex + 1], 'review:' + reviewEnum[currIndex + 2]));
//}


//-----------------------
// FILTERING
//-----------------------

// NOTE: this code relies on a naming convention applied to the result target div

function enable_filter_forms ( context ) {

	$('.filter-form', '#' + context).submit( function () {

		//TODO: this clip hide should be refactored to move into the 'events.js' code, and exploit an 'onsubmit' hook in this code.
		
		try { hideClipTip(); } catch( err ){ /*boo hoo*/ }

		var action = $(this).attr('action');

		var selector = '#' + $(this)[0].id.split('-')[1];

		$.get(action, {query: $('.filter-query', this).val()}, function(response){
			$(selector).replaceWith(response)
		});

		return false;
	});

	$('.filter-query',  '#' + context).focus( function(){ BARN.gridTextEdit = true; /* alert(BARN.gridTextEdit);*/ } );
	
	$('.filter-query', '#' + context).blur( function(){ BARN.gridTextEdit = false; /* alert(BARN.gridTextEdit);*/ } );
}

//-----------------------
// TOGGLE AND SELECTION
//-----------------------

// NOTE: currently these rely on similar aspects of the markup

function enable_toggle_controls( context ) {
        
	$('.grid_toggle span', '#' + context).click( function () {
		
		var state = $(this).hasClass('ui-icon-plus');
		
		if (state) {
			 $(this).removeClass('ui-icon-plus').addClass('ui-icon-minus');
		} else {
			 $(this).removeClass('ui-icon-minus').addClass('ui-icon-plus');
		}
		
		// TODO: this can be simplified using attributes rather than the additional span markup

		var config = $(this).next();
								
		if (config.hasClass('targets')) {
				
			var targets = $('.' + config[0].innerHTML);
			
			var children = targets.children();
			
			if (state) {
				targets.show(); $(children[1]).load($(this).attr("uri"));
			} else {
				targets.hide();  $(children[1]).html("<p>Loading ...</p>");
			}
				
		}
            
    });

}


function bind_file_add( container_id ) {
	$("#add_form", "#" + container_id ).ajaxForm( function(){
		refresh_container( container_id );
	});
}


function add_to_project() {
		
    //--
    // get selected checkboxes, return if no selection
    //--
    
    var labels = get_selected_elements();
    
    if (labels.length == 0) {
        return false;
    }
    
    //--
    // build request template from selection
    //--
            
    // NOTE: the identifiers are current of the form '{type}-{type_id}'
    
    for (k = 0; k < labels.length; k++) {		
        var part = labels[k].split('-'); labels[k] = 'add_sounds[]=' + part[1];
    }
        
    uri = '/projects/{project_id}?_method=put&' + labels.join('&');
        
    //--
    // present form to get project
    //--
    
    $('body').append('<div style="display: block;" id="add-to-project" title="Add to project"></div>')
        	
    $.get('/projects?partial=projects/add_to_project', {uri: uri}, function (data) {
        $('#add-to-project').html(data); 
    }, 'text');
    
    $("#add-to-project").dialog({
        bgiframe: true,
        height: 300,
        width: 500,
        modal: true,
        closeOnEscape: true
    });
		
	return false;
}


//-----------------------
// QTIPS
//-----------------------

// NOTE: these are not displayed within the listing container

function enable_export_tips() {
	
	$('.export-links a').filter( function(index) {
		return $(this).attr('title').length && !$(this).hasClass('tipped')
	}).addClass('tipped').qtip({
		style: {
			name: 'dark', tip: true
		},
		show: {
			delay: 500
		},
		position: {
			corner: {
				target: 'topLeft',
				tooltip:'bottomRight'
			}
		}
	});
	
	// NOTE: this does not belong here, but it typically does work, in any case move.
	
	$('a.edit-link').filter( function(index) {
		return $(this).attr('title').length && !$(this).hasClass('tipped')
	}).addClass('tipped').qtip({
		style: {
			name: 'dark', tip: true
		},
		show: {
			delay: 500
		},
		position: {
			corner: {
				target: 'topRight',
				tooltip:'bottomLeft'
			}
		}
	});
}

function enable_tag_tips(context_id) {
	
	var context = $('#' + context_id);
	
	// NOTE: already tried: using context, adding a marker tag for bind idempotency, and filtering on empty title
	
	// NOTE: the solo show options resolves a lingering problem	
		
	$('div.tag-cloud span.tag, div.tag-cloud span.add').filter( function(index) {
		return $(this).attr('title').length && !$(this).hasClass('tipped')
	}).addClass('tipped').qtip({
		style: {
			name: 'dark', tip: true
		},
		show: {
			solo: true,
			delay: 500
		},
		position: {
			corner: {
				target: 'topRight',
				tooltip:'bottomLeft'
			}
		}
	});
	
	$('div.tags').filter( function(index) {
		return $(this).attr('title').length && !$(this).hasClass('tipped')
	}).addClass('tipped').qtip({
		style: {
			name: 'dark', tip: true
		},
		show: {
			solo: true
		},
		when: {
			event: 'click'
		},
		position: {
			corner: {
				target: 'topLeft',
				tooltip: 'bottomRight'
			}
		}
	});
	
	// TODO: this guy lingers when we delete the tag, and fails to bind after we edit tags
	
	//$('div.tags span.delete').filter( function(index) {
	//	return !$(this).hasClass('tipped')
	//}).addClass('tipped').qtip({
	//	content: "?",
	//	style: {
	//		name: 'red', tip: true
	//	},
	//	show: {
	//		solo: true
	//	},
	//	hide: {
	//		delay: 400,
	//		fixed: true
	//	},
	//	position: {
	//		corner: {
	//			target: 'topRight',
	//			tooltip:'bottomLeft'
	//		}
	//	}
	//});
}


//-----------------------
// UTILITY
//-----------------------

// NOTE: we user this to turn paths into proper identifiers that we can use in selectors

function dasherize_path(uri){
    
    // This is used because paths begining with / are interpreted with full protocol ('http://')
    var path_array = $.url.setUrl(uri).attr("path").split('/');
    
    // splice returns the removed element, so no chaining
    path_array.splice(0,1); 
    
    var path = path_array.join("-");
    
    // account for a possible format extension (.html)
    if( path.split(".").length > 1 ){
        path_array = path.split(".");
        path_array.pop();
        path = path_array.join("");
    }

    return path;
};

/*
    This function works specifically with the will_paginate helper method
    Looks for 2 options: 
    target_id - the DOM element to be replaced with the results of the page request, presumably a container of some sort
    partial - the partial to render, since we do not want the default view
*/

/*
function hijack_pagination_links(options){
    var target_id     = options.target_id;
    var partial_param = "&partial=" + options.partial;
    
    var selector  = '#' + target_id + ' .pagination a';

    $(selector).click( function(){
        // If the partial param is already attached, dont reattach
        if ($.url.setUrl(this.href).param("partial")){
            partial_param = ""
        }
        
        var id = '#' + dasherize_path(this.href);
        
        $.get(this.href + partial_param, null, function(response){
            $(id).replaceWith(response)
        });

    	return false;
    });
}
*/


// NOTE: this was formerly used as a poor man's template engine, very poor

function replace_with_data (template, data) {
		
    var output = template;
    
    for (var field in data) {
        var match = new RegExp('{' + field.toUpperCase() + '}', 'g'); output = output.replace(match, data[field]);
    }

    return output;

}

String.prototype.strip = function(){
  return this.replace(/^\s+/, '').replace(/\s+$/, '');
}


// Read a page's GET URL variables and return them as an associative array.
function getUriVars( uri )
{
    var vars = [], hash;

	if (!uri){
		uri = window.location.href;
	}
	
    var hashes = uri.slice(uri.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}



// commonKeyBindingHandlers contains all of the key-handling functions registered for this page.
//commonKeyBindingHandlers = new Array();

BARN.mode_key_handler = function(a,b){};
BARN.keyShiftMode = false;
BARN.keyCtlMode = false;
BARN.gridTextEdit = false;


$(document).ready(function() {

	$(document).keyup(function(e) {
		
		var code = (e.keyCode || e.which);
		if(code == 16) BARN.keyShiftMode = false;
		if(code == 17) BARN.keyCtlMode = false;

	}).keydown(function(e) {

		var code = (e.keyCode || e.which);
		if(code == 16) BARN.keyShiftMode = true;
		if(code == 17) BARN.keyCtlMode = true;

//		console.info("key = %s, code = %d", String.fromCharCode(code), code);

//		for ( i=0; i< commonKeyBindingHandlers.length; i++ ) {
//			commonKeyBindingHandlers[i](code, e);
//		}

		BARN.mode_key_handler(code, e);
	});

});
