//--
// inline player
//--

// NOTE: the various classes are targeted here, and in inlineplayer.css

function bind_inline_player( context ) {

//		$('a.render-inline-player').

//	$('a.sm2_link', '#' + context ).dblclick( function() {
//		toggleClipTip( $(this).parent() );
//		return false;
//	});

	$('a.sm2_link', '#' + context ).click( function() {
		//NOTE: this will set the mode_key_handler, though it could be abstracted to invoke a 'setUIContext' which
		//delegates to a ui-specific (possibly entity) specific function that sets the key handler among other things.
		//also, a setUIContext function would help to keep these kind of bindings clear when we wish to preserve
		//state from page to page
		//BARN.mode_key_handler = handle_grid_keys;

		
		invoke_inline_player(this);
		return false;
	});
}



function invoke_inline_player( target ) {

	set_cursor( $(target).closest('.element') );

	clipHeight = 129;
	clipMaxWidth = 800;

	// NOTE: if we have a player and the url has not changed toggle pause state

	if (inline_player && (inline_player.url == target.href)) {
		inline_player.togglePause(); return false;
	}

	// NOTE: cleanup and start a new sound

	soundManager.destroySound('inline-player');
	$('a.sm2_playing').removeClass('sm2_playing');
	$('a.sm2_paused').removeClass('sm2_paused');

	//--
	// create and play sound
	//--

	// NOTE: 'this' is tricky here, there are two. the 'parent' is the container dom node, this allows easy update of various properties

	inline_player = soundManager.createSound({

		id: 'inline-player',
		url: target.href,
		container: target,
		progressDiv: null,
		clipImage: $(target).children('img')[0],
		child: $(target).children(),

		onplay: function() {

			$(this.options.container).addClass('sm2_playing');
			$(this.options.container).prepend("<div class='progress' style='height: " + clipHeight + "px; width: 1px; left: 0px'></div>");
			this.options.progressDiv = $('.progress', this.options.container)[0];

		},
		whileplaying: function () {

			var progressVal = Math.round(this.options.clipImage.width * (this.position / this.duration));
			var progressStyle = progressVal + 'px';

			if (this.options.clipImage.width < clipMaxWidth) {

				this.options.progressDiv.style.width = progressStyle;

			} else {

				halfWidth = clipMaxWidth / 2;

				if (progressVal < halfWidth) {
					this.options.progressDiv.style.width = progressStyle;

				} else if ( progressVal > (this.options.clipImage.width - halfWidth)) {

					this.options.progressDiv.style.width = (clipMaxWidth - ((this.options.clipImage.width) - progressVal)) + 'px';

				} else {

					this.options.clipImage.style.marginRight = (progressVal - halfWidth) + 'px';
					this.options.clipImage.style.marginLeft = '-' + (progressVal - halfWidth) + 'px';

				}
			}
		},
		onpause: function() {
			$(this.options.container).addClass('sm2_paused');
		},
		onresume: function() {
			$(this.options.container).removeClass('sm2_paused');
		},
		onfinish: function() {
			$(this.options.container).removeClass('sm2_playing');
			$('.progress', this.options.container).remove();
			this.options.progressDiv = null;

			this.options.clipImage.style.marginLeft = '0px';

			if (this.options.clipImage.width > clipMaxWidth) {
				this.options.clipImage.style.marginRight = '0px';
			}
		}
	});

	inline_player.play(); return false;

}


// NOTE: this is a utility to pause execution

function lapse (interval) {
	var start = new Date();
	var current = null;
	do { current = new Date(); } while (current - start < interval);
}


function handle_tip_keys( code, event ){
//	console.info("TipKeys");
	if ( get_cursor() != null ){
		switch( code ) {
			case 37:
//				prevClip();
				break;

			case 39:
//				nextClip();
				break;
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
				console.log(code - 48);				
		        break;
			case 190:
				invoke_inline_player( $(get_cursor()).children('a')[0] );

				event.preventDefault(); break;
			case 191:
				var cursortip = $(get_cursor()).qtip("api").elements.tooltip;
				if ( $( cursortip ).hasClass('qtip-active') ) {
					$(get_cursor()).qtip("hide");
				} else {
					$(get_cursor()).qtip("show");	
				}

				event.preventDefault(); break;
			case 88:
				toggle_element_selection(get_cursor());
				break;

			default:
				break;
		}

		return false;
	}
}


function bind_clip_tips( context ){

	$( '.glider', '#' + context ).children('li').each( function(){ 


		// determine position and width, and adjust for the preset qtip width for positioning logic.

		var corner = adjustTipLocation( this );

		$(this).qtip({
			content: {
				title: {
					text: $(this).attr('title'),
					button: 'Close'
				},
				url: $(this).attr('rel')
			},			
			position:	{
				corner: {
				 tooltip: corner.tooltip,
				 target: corner.target
			    },
				adjust: {
					screen: false,
					scroll: false,
					resize: false
				}
			},
			show: {
				solo: true,
				when: { event: 'click' }
			},
			hide: {
				fixed: true,
				when: { target: $('h1:first'), event: 'click' }
			},
			style: {
			  name: 'light',
			  tip: true,
			  width: {
			  	min: 520,
				max: 520
			  }
			}
		});


		var qapi = $(this).qtip("api");

		qapi.onFocus = function() {
			var tooltip = this.elements.tooltip;

			$(".vote-popup").css("z-index", $(tooltip).css("z-index")+1);
		};

		qapi.onHide = function () {

			BARN.mode_key_handler = handle_grid_keys;
		};

		qapi.onShow = function () {
			BARN.mode_key_handler = handle_tip_keys;
		};

		qapi.onContentUpdate = function() {

			// NOTE: this code is hit twice on selection, for some reason.

			var target = this.elements.target;

			var tooltip = this.elements.tooltip;

			$(".tipBar .title", this).html( $(target).attr('title') );

			// $('#currentTip').attr('id', '');
			//$(target).attr('id','currentTip');

			$(".hmenu li", tooltip).click( function(){
				displayTipTab( $(this).html() );
			});

			$(".tipBar span", tooltip).click( function(){
				$('.qtip').qtip("hide");

				BARN.mode_key_handler = handle_grid_keys;
			});

			displayTipTab('tags');
			
			BARN.mode_key_handler = handle_tip_keys;

			//bindTagTips();

			this.updateWidth( 520 );

			//adjustTipLocation( target, tooltip );
			
		};





	});

}

function displayTipTab( tabName ){

	$("#currentTip .tip_tags").hide();
	$("#currentTip .tip_info").hide();	
	$("#currentTip .hmenu li").removeClass('selected');

	$("#currentTip .hmenu li").each( function() {
		if ( $(this).html() == tabName ){
			$(this).addClass('selected');
		} else {
			$(this).removeClass('selected');
		}
	});

	target = $("#currentTip .tip_" + tabName);
	if ( target.length > 0 ){
		target.show();
	}
}

function adjustTipLocation( target ) {

	//UNDER DEVELOPMENT

	// Accounts for tip versus screen edges
	var win_width = $(window).width();

	var targ_width = $(target).outerWidth();
	var targ_left = $(target).offset().left;
	var targ_right = $(target).offset().left + targ_width;
	var tip_width = 520; //$(tip).outerWidth();

	//var corner = $(tip).qtip("api").options.position.corner
	//var tipStyle = $(tip).qtip("api").options.style.tip


	var corner = {};
	corner.target = '';
	corner.tooltip = '';

	if ( targ_right + tip_width > win_width ) {
		if ( targ_left - tip_width < 0 ) {

			corner.target = 'bottomMiddle';
			corner.tooltip = 'topMiddle';
			tipStyle = 'topMiddle';

			//alert('center (L' + targ_left + ' W' + tip_width + ' R' + targ_right + ')\n' + ' left_diff:' + (targ_left - tip_width) + '\n || right_diff:' + (targ_right + tip_width) + '\n || window:' + win_width);
		} else {
			//disp left
			
			corner.target = 'bottomLeft';
			corner.tooltip = 'topRight';
			tipStyle = 'topRight';

			//alert('left (L' + targ_left + ' W' + tip_width + ' R' + targ_right + ')\n' + ' left_diff:' + (targ_left - tip_width) + '\n || right_diff:' + (targ_right + tip_width) + '\n || window:' + win_width);
		}
	} else {
		//disp right

		corner.target = 'bottomRight';
		corner.tooltip = 'topLeft';
		tipStyle = 'topLeft';

		//alert('right (L' + targ_left + ' W' + tip_width + ' R' + targ_right + ')\n' + ' left_diff:' + (targ_left - tip_width) + '\n || right_diff:' + (targ_right + tip_width) + '\n || window:' + win_width);
	}
	//$(tip).qtip("api").updateStyle();
	//$(tip).qtip("api").updatePosition();

	return corner;

//		var target = $('.cursor');
//
//		// move the clip to the screen center so it will resize to proper width
//		$(tip).css( 'left', ($('body').outerWidth() / 2));
//
//		$(tip).css( 'left', ($(target).offset().left - ($(tip).outerWidth() + $(target).outerWidth() )) + 'px' );
}

//function foo() {}