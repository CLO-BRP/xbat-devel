// BARN Log resource

BARN.Log = new BARN.Resource('Log', {
    response_format: 'xml',
    schema: {
        resultNode: "log",
        fields: [
            {key:'id', parser: "number"},
            {key:'name', parser: "string"},
            {key:'user-id', parser: "number"},
            {key:'created-at', parser: "string"},
            {key:'modified-at', parser: "string"}
        ],
        metaNode: 'metadata',
        metaFields: {
            total_entries:'total_entries'
        }
    }
});


var LogsController = {
  
  loadEvents: function(node, fnLoadComplete){
    EventsController.renderTable({log_id:node.data.log_id, caption:node.data.caption});
    node.data.response.argument.fnLoadComplete();
  },
  
  renderTreeNodes: function(node, fnLoadComplete){
    
    EventsController.renderTable({scope:'sounds', scope_id:node.data.sound_id, caption:node.data.label + ' Events'});
    
    //prepare URL for XHR request:
    var sUrl = "/sounds/" + node.data.sound_id + "/logs.js";
    
    //prepare our callback object
    var callback = {
      
        success: function(response) {
            YAHOO.log(response.responseText,'info');
            var results = YAHOO.lang.JSON.parse(response.responseText);
            
            //--
            // handle null response, we are done
            //--
            
            if(!results || results.length == 0) {
              response.argument.fnLoadComplete(); return;
            }
            
            //--
            // handle non-trivial response
            //--
            
            for (var i=0, j=results.length; i<j; i++) {
              var data = {
                label:results[i].log.name,
                log_id:results[i].log.id,
                caption: 'All events logged for ' + results[i].log.name,
                response: response
              };
              
              // NOTE: This is the right way to do this:
              
              //var tempNode = new YAHOO.widget.TextNode(data, node, false);
              //tempNode.setDynamicLoad(Log.loadEvents, 1);
              
              var tempNode = new YAHOO.widget.HTMLNode({
                html: '<a href="#" onclick="EventsController.renderTable({scope:\'logs\', scope_id:'+ results[i].log.id + ', caption:\'All events logged for '+ results[i].log.name + '\'});">' + results[i].log.name + '</a>', 
                key: i, 
                multiExpand:false
                }, node);
              
            }

            response.argument.fnLoadComplete();
            
        },
        
        failure: function(response) {
            YAHOO.log("Failed to process XHR transaction.", "info", "example");
            response.argument.fnLoadComplete();
        },
        
        argument: {
            "node": node,
            "fnLoadComplete": fnLoadComplete
        },

        timeout: 7000
    };
    
    //With our callback object ready, it's now time to 
    //make our XHR call using Connection Manager's
    //asyncRequest method:
    YAHOO.util.Connect.asyncRequest('GET', sUrl, callback);
  }
  
};