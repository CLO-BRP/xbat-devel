// This one is barely used
BARN.Rating = new BARN.Resource('Rating', {
    response_format: 'js',
    schema: {
        resultNode: "rating",
        fields: [
            {key:'id', parser: "number"},
            {key:'rateable_id', parser: "string"},
            {key:'rateable_type', parser: "number"},
            {key:'rate_id', parser: "number"},
            {key:'user_id', parser: "number"},
            {key:'created_at', parser: "string"},
            {key:'modified_at', parser: "string"}
        ],
        metaNode: 'metadata',
        metaFields: {
        }
    }
});


var RatingsController = {
    save: function(params){
        YAHOO.util.Connect.asyncRequest('POST', '/ratings.js', {
            timeout: 7000,
            success: function(response){
                var result = YAHOO.lang.JSON.parse(response.responseText);           
            },
            failure: function(response){
            }
        },
        'rating[rateable_id]=' + params.rateable_id + 
        '&rating[rateable_type]=' + params.rateable_type + 
        '&rating[rating]=' + params.rating +
        '&rating[user_id]=' + BARN.Application.user.id
        );
    }
}