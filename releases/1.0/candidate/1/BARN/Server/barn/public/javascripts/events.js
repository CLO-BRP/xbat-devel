// Create the Event resource

BARN.Event = new BARN.Resource('Event', {
    response_format: 'xml',
    schema: {
        resultNode: "event",
        fields: [
            {key:'id',          parser: "number"},
            {key:'start',       parser: "string"},
            {key:'duration',    parser: "number"},
            {key:'low',         parser: "number"},
            {key:'high',        parser: "number"},
            {key:'channel',     parser: "number"},
            {key:'score',       parser: "number"},
            {key:'created_at',  parser: "string"},
            {key:'modified_at', parser: "string"},
            {key:'rating', parser: "number"},
            {key:'tags'}
        ],
        metaNode: 'metadata',
        metaFields: {
            total_entries:'total_entries',
            score_min:'score_min',
            score_max:'score_max',
            score_mean:'score_mean'
        }
    }
});

// The Controller renders different views of the data from the resource.

var EventsController = {
	
    renderTable: function(params)
    {
        // Variables used in rendering the DataTable

        var caption = params.caption || 'Events';
        var target  = params.target  || 'events_table';
        var size    = params.size || 15;
        
        var columns = [  // YUI DataTable column definitions
            {key: "id",         sortable: true, width:15, resizeable:true},
            {key: "start",      sortable: true, width:120, resizeable:true, label:'Start'},
            {key: "duration",   sortable: true, width:120, resizeable:true, label:'Duration'},
            {key: "low",        sortable: true, width:40, resizeable:true, label:'Low'},
            {key: "high",       sortable: true, width:40, resizeable:true, label:'High'},
            {key: "channel",    sortable: true, width:45, resizeable:true, label:'Channel'},
            {key: "score",      sortable: true, width:120, resizeable:true, label:'Score'},
            {key: "rating",     sortable: true, width:85, resizeable:true, label:'Average Rating', formatter:YAHOO.widget.DataTable.formatRating},
            {key: "tags",     sortable: true, width:85, resizeable:true, label:'Tags', formatter: YAHOO.widget.DataTable.formatTags},
            {key: "created_at", sortable: true, width:170, resizeable:true, label:'Created at'}
        ];
        
        // YUI DataSource via the Events Model
        // - this var is also used by the initalRequest in the configs object sent to the DataTable constructor

        var datasource = BARN.Event.getDataSource(params);
        
        // YUI DataTable
        
        var datatable = new YAHOO.widget.DataTable( target, columns, datasource, {
            dynamicData:    true,
            draggableColumns: true,
            // caption:        caption,            
            sortedBy:       { key:"created_at", dir:"desc" },
            paginator: new YAHOO.widget.Paginator({ 
                rowsPerPage: size, 
                containers : ['my_pagination_top', 'my_pagination_bottom']
            }),
            
            /*  
                This is a tricky config for YAHOO.widget.DataTable.
                This method (initialRequest) is expected to be a string (not return one), and is not called like as a function, 
                but we need to do some work in order to get a useable initial request string. 
                So we create a function the executes itself when initialRequest is called.
            */          
            initialRequest: function(size, datasource){ 

                // For the Initial request of the YUI DataTable, we need to modify the base generated URI, 
                // which is something like 'events.xml' to a paged result in the format 'events/1-10.xml'

                var uri_split       = datasource.liveData.split('.');
                
                // the liveData field on our DataSource object is the default URI of where to find resources
                datasource.liveData = uri_split.slice(0, uri_split.length -1 );
                var uri = '/1-' + size + '.' + datasource.responseFormat; 
                return uri
                
            }(size, datasource),          
            
            /* 
                This config on the other hand, is called by YUI as a function,
                In this fuction we need to rewrite what YUI's DataTable assumes will be the request, 
                and what we really want it to be.
            */
            generateRequest:function(params, datatable){
                var datasource     = datatable.getDataSource();
                var base_uri       = datasource.liveData;
                
                var page  = params.pagination.page;
                var rows  = params.pagination.rowsPerPage;
                var order = params.sortedBy.key;
                var direction  = params.sortedBy.dir.split("-"); direction = direction[direction.length-1];
                 
                var last_record   = (page * rows);
                var first_record  = (last_record - rows) + 1;
                var records_range =  first_record.toString() + '-' + last_record.toString();        
                
                var parameters = '';
                if(order && direction){
                    parameters = '?order=' + order + '&direction=' + direction;
                }
                
                return '/' + records_range + '.' + datasource.responseFormat + parameters;
            }
            
        }); //datatable

        // The YUI DataTable now requires us to overwrite a few instance methods
        
        datatable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {

            // TODO: move this meta back to the Event instance!! 
            // ...or perhaps have an event triggered that the view can observe
        
            var meta = oResponse.meta;
            if(meta.score_mean){
                document.getElementById('events_table_metadata').innerHTML = '' + 
                    '<div style="margin-bottom:10px;font-weight:bold;">Event statistics</div>' +
                    "total entries: " + meta.total_entries + "<br />" +
                    "mean score: " + meta.score_mean + '<br />' +
                    "min score: " + meta.score_min + '<br />' +
                    "max score: " + meta.score_max + '<br />';    
            }
            else{
                document.getElementById('events_table_metadata').innerHTML = "";
            }
            // This line is needed for proper paging from Paginator
            oPayload.totalRecords = Number(meta.total_entries);
            return oPayload;
        };
        
        return datatable;
        
    } // EventsController.renderTable()
    
};