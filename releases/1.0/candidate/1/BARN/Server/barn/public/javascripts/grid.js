
//--
// DEFAULT SELECTION BINDINGS
//--

function bind_selection_elements( context ){
	$( '.element', '#' + context ).click( function() {

		BARN.mode_key_handler = handle_grid_keys;

		if ( get_cursor() != this ){
			try { hideClipTip(); } catch( err ){ /*boo hoo*/ }
		}

		// TODO - set_cursor invoke is repeated for a diff level in the invoke_inline_player.  Must Normalize.
		set_cursor( this );

		//console.log('click');
	});
}

function bind_selection_allnone( context, gridtype ){
	
	// NOTE: the 'return false;' means that the links are not followed

	$('a.select-all','#' + context).click( function () {
		checkbox_select (get_visible_checkboxes(context, gridtype), true );
		return false;
	});
	
	$('a.select-none','#' + context).click( function () {
		checkbox_select (get_visible_checkboxes(context, gridtype), false );
		return false;
	});

	// NOTE: this has been excluded from the interface until a decision is made on how to support multipage selects
	$('.select-multi', '#' + context).click( function () {
		checkbox_select (get_visible_checkboxes(context, gridtype), this.checked );
	})

	
}

//sets the check
function checkbox_select (checkboxes, setting) {

	$.each(checkboxes, function(k, item) {
		item.checked = setting;
		
		row_selection(item);
	});
}


function get_visible_checkboxes(context, gridtype) {

	// try getting table listing checkboxes

	if ( gridtype == 'glider' ){
		var selector = 'table.listing.' + context;
		var checkboxes = $(selector).next().find('.selection-checkbox');
	} else {
		var selector = 'table.listing.' + context + ' > tbody > tr > td > .selection-checkbox';
		var checkboxes = $(selector);
	}

	return checkboxes;
}


function enable_row_selection() {

	$("input:checkbox.render-select-row").livequery('click', function () {
			row_selection(this);
	});
}

function row_selection(cx) {

	var row = $(cx).closest('.element');

	// TODO: what is the purpose of 'related' ?
	//var related = '.' + $(cx).attr('targets');

	if (cx.checked) {
		row.addClass('selected');
		//$(related).addClass('selected');
	} else {
		row.removeClass('selected');
		//$(related).removeClass('selected');
	}
}

//--
// GET_SELECTED_ELEMENTS - return array of selected element identifiers
//--

function get_selected_elements(container_id) {

	var selected = $('.selected', container_id); var labels = [];

	$.each(selected, function(k, item) {
		if ( item.id ) {
			labels[labels.length] = item.id;
		}
	});

	return labels;
}



//--
//   LIST SELECTIONS
//--

// TODO: select state and checkbox sync approach is not proper

function enable_clip_selection() {
    
	$("input:checkbox.render-select-clip").livequery('click', function () {
		
		//element = $(this).parent()[0];
		element = $(this).closest('li')[0];
				
		if ($(this).is(':checked')) {
			select_element(element);
		} else {
			deselect_element(element);
		}
	});
}

// NOTE: we should conventionally class the checkbox we refer to below

function select_element( element ) {
	$(element).addClass("selected");
	
	//set_select_checkbox(element);
}

function deselect_element( element ) {
	$(element).removeClass("selected");
	
	//set_select_checkbox(element);
}

function toggle_element_selection(element) {

	if ($(element).hasClass("selected")) {
		
		$(element).removeClass("selected");
	} else {
		$(element).addClass("selected");
	}

	set_select_checkbox(element);
}

function set_select_checkbox(element) {
	checkBox =  $('.selection-checkbox', element)[0];

	if (checkBox) {
		$(checkBox).attr('checked', !$(checkBox).attr('checked'));
	}
}

function clear_selected_elements() {}


//--
// CURSOR MANIPULATION AND DISPLAY
//--

function set_cursor( element ) {

	if ( get_cursor() != null ) {
		$(get_cursor()).removeClass('cursor');
	}

	$(element).addClass('cursor');

  	adjust_window_scroll_for_cursor();
}

function get_cursor() {
	cursor = $('.cursor')[0];
	
	// NOTE: here we decorate the cursor with toggle information
	
	if (cursor != null) {
		cursor.toggle = get_toggle(cursor);
	}
	return cursor;
}





// TODO - these are brittle - update on grid refactor 

function get_cursor_container() {
	return $( get_cursor() ).parent().closest('.grid-container')[0];
}

function get_child_container() {
	var ajax_tabs = $( get_cursor() ).next().children('td.content-pane').children('div.ajax-tabs');
	var target = $( ajax_tabs ).children('div.tabs-target').children('div.grid-container')[0];
	return target;
}

function get_parent_element() {
	var my_container = get_cursor_container();
	var parent = $( my_container ).closest('.content-pane').parent().prev('.element')[0];
	return parent;
}




function get_toggle(cursor) {
	
	// NOTE: this creates an empty object that we can append 'state' to below
	
	var toggle = {};
	
	// NOTE: this relies on the current markup for toggle, it must change accordingly
	
	toggle.element = $('.grid_toggle span.ui-icon', cursor);
	
	if (toggle.element == null) {
		// NOTE: if we did not find a toggle element clear the whole thing, return 'null'
		
		toggle = null;
	} else {
		toggle.is_open = function(){ this.state == "open"; }
		toggle.state = toggle.element.hasClass('ui-icon-minus') ? 'open' : 'closed';
	}
	
	return toggle;
}

function adjust_window_scroll_for_cursor(){
	// check for offscreen adjustment

	if ( $(".cursor").length != 0 ) {

		var cursorTop = $(".cursor").offset().top;

		if ( cursorTop < $("body").scrollTop() ) {

			$("body").scrollTop( cursorTop - (0.5 * $("body").height()) );
		} else {

			var cursorBottom = $(".cursor").offset().top + $(".cursor").outerHeight();

			var screenBottom = $("body").scrollTop() + $("body").height();

		//	console.log($("body").scrollTop() + " " + $("body").height() + " " + $("#cursorElement").offset().top + " " + $("#cursorElement").outerHeight() );
		//	console.log( cursorBottom + " " + screenBottom );

			if ( cursorBottom > screenBottom ){
				$("body").scrollTop( cursorBottom - (0.5 * $("body").height()) );
			}
		}
		
	}
}

function prev_element( next_page ){
	// hideClipTip();

	var target = $(get_cursor()).prevAll('.element')[0];
	
	chooseSequentialElement('prev', target, next_page);
}

function next_element( next_page ){
	// hideClipTip();

	var target = $(get_cursor()).nextAll('.element')[0];
	
	chooseSequentialElement('next', target, next_page);
}

function next_child_element() {
	// hideClipTip();

	var target = $(get_cursor()).nextAll('.element')[0];

	set_cursor( target );
}

function chooseSequentialElement( direction, target, next_page ){
	if ( target == null || target.length == 0 ){

		// NOTE: this is the code that moves to another page triggered by cursor navigation

		var targetControl = get_pagination_control( direction );

		if ( targetControl && next_page ) {

			targetControl.attr('href', add_offset_parameter_to_url(  targetControl.attr('href'), direction ) );

			targetControl.click();
		}

		return;
	}

	set_cursor( target );
}

function get_pagination_control( direction ) {
	var paginationControl = $(get_cursor()).parent().prev().find('.pagination');

	return $(paginationControl).find('.' + direction + '_page');	
}

function add_offset_parameter_to_url( targetHref, direction ){
	// NOTE: the 'cursorOffset' parameter helps the server markup next page cursor
	
	var newHref = null;
	
	if (targetHref.indexOf("cursorOffset") != -1) {
		newHref = targetHref.replace(/cursorOffset=%27[A-Za-z]*%27/g, "cursorOffset=%27" + direction + "%27");
	} else {
		newHref = targetHref + "&" + "cursorOffset=%27" + direction + "%27";
	}
	
	return newHref;
}

var cursorOffset = null;

function set_cursor_for_grid( cursorOffset, containerId ){

	// NOTE: this sets the cursor to the proper element when we have moved from a previous page

	if ( cursorOffset != null ) {
		if ( cursorOffset == 'prev' ) {
			set_cursor( $(".glider li" , '#' + containerId )[$(".glider li" , '#' + containerId ).length - 1] );
			cursorOffset = null;
		} else if ( cursorOffset == 'next' ){
			set_cursor( $(".glider li" , '#' + containerId )[0] );
			cursorOffset = null;
		}
	}
}


function cycleReview(){

	//var containerId =

	//TODO establish containerId to avoid unpleasant surprises with multiple cursors....(or forbid them - exclusive only on cursor set)

	$('.cursor .sidebar a.review').click();
}


//--
//   DEFAULT KEYBOARD HANDLER FOR GRID CONTEXT
//--

function handle_grid_keys( code, event ){

	//console.info("GridKey: %d", code);

	cursor = get_cursor();
	
	// console.dir(cursor.toggle);
	
	// TODO: flatten this, return with 'null' cursor
	
	if (cursor != null){

		if ( !BARN.gridTextEdit ) {

			switch (code) {
				case 38: // up-arrow

					// if the list is a glider, and we are on the first element,
					// and keyShiftMode is not selected, jump out to the parent element on the parent list

					if ( $(get_cursor()).parent().hasClass('glider') && $(get_cursor()).prevAll('.element')[0] == null && !BARN.keyShiftMode ) {
						set_cursor( get_parent_element() );
					} else {
						prev_element( BARN.keyShiftMode );
					}

					event.preventDefault(); break;
	
				case 40: // down-arrow

					// if the list is a glider, and we are on the last element,
					// and keyShiftMode is not selected, jump out to the next element in the parent list

					if ( $(get_cursor()).parent().hasClass('glider') && $(get_cursor()).nextAll('.element')[0] == null && !BARN.keyShiftMode ) {
						set_cursor( $( get_parent_element() ).next().next() );
					} else {
						next_element( BARN.keyShiftMode );
					}

					event.preventDefault(); break;

				case 37: // left-arrow
					if (cursor.toggle.element.length == 0) {
						prev_element( BARN.keyShiftMode );
					} else {
						if (cursor.toggle.state == "open") cursor.toggle.element.click();
					}

					event.preventDefault(); break;
					break;


				case 39: //right-arrow
					if (cursor.toggle.element.length == 0) {
						next_element( BARN.keyShiftMode );
					} else {
						if (cursor.toggle.state == "closed") {
							cursor.toggle.element.click();
						} else {
							target = get_child_container().get_collection()[0];
							if ( target != null ) {
								set_cursor( target );
							}
						}
					}

					event.preventDefault(); break;
					break;

				case 32:
				case 190:
					// checking gridTextEdit will stop popup when enter is pressed after filter is modified
					invoke_inline_player( $(get_cursor()).find('a.sm2_link')[0] );
					event.preventDefault(); break;

				case 13:
				case 191:
					toggleClipTip(get_cursor());
					event.preventDefault(); break;

				case 82:	// r
					cycleReview(); break;

				default:
					break;
			}
		}

		switch (code){
			case 88:
				toggle_element_selection(get_cursor());
				break;

			case 84:
				// console.log("tag-me");
				break;

			default:
				break;

		}

		return false;
	}
	
	return false;
}


//--
// CONTAINER CONTENT LOAD
//--

function replaceContainerContents( response, containerId ){
	var parent = $('#' + containerId ).parent();
	$('#' + containerId ).replaceWith(response)
	return $(parent).find(".grid-container").attr('id');
}


function update_selected_tab( containerId ){
	var currCount = $('.griddata .size', '#' + containerId).html();
	var target = $('#'+ containerId).parent().prev().find('li.selected a')
	if (target.length > 0) {
		var value = target.html();
		target.html(value.replace(/\(\d+\)/, '(' + currCount + ')' ) );
	}
}

//--
// BIND LINK TARGETS target refresh, sort, and pagination links to container
//--

// NOTE: this is where we capture various listing links to update the listing

function bind_link_targets( context ){

	// TODO: express this as an array of selectors and combine into a string at the end
	
	var grid_refresh_links = ('#d-refresh-' + context + ', #refresh-' + context + ', .sort-controls a, a.sort-listing, .tag-cloud a, .pagination a, .per-page-controls a, .rating-controls a');

	$( grid_refresh_links , '#' + context).click( function () {
		
		$('.qtip').remove();
		
		$.get(this.href, null, function (response) {
			newId = replaceContainerContents( response, context );
			
			adjust_window_scroll_for_cursor();
			
			update_selected_tab(newId);
		});

		return false;
	});
}