=begin

THIS NEEDS TO BE REWRITTEN

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe EventsController do
	
	before :all do
		@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
		@ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')			
	end
  
  # Listing of events can happen in a number of contexts
  
  describe "GET" do 
    integrate_views
    
    before(:each) do
      #Sound.destroy_all
      SoundUser.destroy_all
      
			@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
			@ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
			
			# Make 3 sounds upon which to test
			@bills_public_sound  = default_sound(:name => "Bill's Public Sound", :public => 1, :user_id => @bill.id)
			@bills_private_sound = default_sound(:name => "Bill's Private Sound", :public => 0, :user_id => @bill.id)
	    @teds_private_sound   = default_sound(:name => "Ted's Private Sound", :public => 0, :user_id => @ted.id)
	    
	    login_as @bill
    end
  end
end
=begin	  
  describe "GET" do 
		fixtures :user, :event
    describe "with mime type of xml" do
      integrate_views
      
      before :each do
        login_as(:default) # authorize_as(:default)
      end

			it "should only show your own events" do
        get :index
				#puts event(:one)
			end
      
      it "HTML format should show the YUI browser" do
        # we need a user to be logged in
        login_as(:default)
        get :index
        response.should be_success
      end
      
      it 'should be a teapot' do
        login_as :default
        get :new, :format => 'html'
        response.headers["Status"].should =~ /418/ # I'm a Teapot
      end
      
      # This test is using mocks.
      # We use mocks here to test that we are hitting the correct basic methods
      it 'should render events as xml by testing using mocks' do
        #request.env["HTTP_ACCEPT"] = "application/xml"
        # THIS IS NOT WORKING RIGHT NOW, SOLVE LATER
        #Event.stub!(:rating_by_user).with(User.new).and_return(@event)
        #Event.should_receive(:find_by_set_notation).with({"action"=>"show", "id"=>"1-50", "controller"=>"events"}).and_return([mock_event(:id => 1), mock_event(:id => 2)])
        #get :show, :id => '1-50'

        # check the response
        #response.should be_success
        #response.body.should have_text("Event-in-XML")
        
        #assigns[:events].should_receive(:to_xml).and_return("Event-in-XML")
      end
      
      # This test will actually test the format of the reponse document
      # because it is using fixtures, rather than a mock object
      it "should render all events as xml using fixtures" do
        request.env["HTTP_ACCEPT"] = "application/xml"
        get :show, :id => '1-50'
        
        assigns[:events]
        
        # The following two are synonymous and are here for examples sake
        response.headers['Status'].should  == '200 OK'
        response.should be_success

        response.body.should have_text(/<?xml/)
        #response.body.should have_text(/<metadata>/)
      end
    end
  end
  
  describe "GET" do
		fixtures :user, :event
    before :each do
      request.env["HTTP_ACCEPT"] = "application/xml"
      login_as(:default) # authorize_as(:default)
    end
    
    it "should get a single Event" do
      get :show, :id => "1"
      response.should be_success
      assigns[:event].id.should equal(1)
    end
    
    it 'should return a set' do
      get :index, :id => "{1}"
      response.should be_success
      assigns[:events].first.id.should equal(1)
    end
    
    it 'should return a set' do
      get :index, :id => "{1,2,3}"
      response.should be_success
      assigns[:events].size.should equal(3)
    end    
  end
    # 
  # # describe "responding to GET new" do
  # #   
  # #     it "should expose a new event as @event" do
  # #       Event.should_receive(:new).and_return(mock_event)
  # #       get :new
  # #       assigns[:event].should equal(mock_event)
  # #     end
  # # 
  # #   end
  # 
  # describe "responding to GET edit" do
  # 
  #   it "should expose the requested event as @event" do
  #     Event.should_receive(:find).with("37").and_return(mock_event)
  #     get :edit, :id => "37"
  #     assigns[:event].should equal(mock_event)
  #   end
  # 
  # end
  # 
  
  # POST
  describe "POST" do
		fixtures :user, :event
    # Add views to the tests so we can also test the rendered response (view)
    # If this method call is not made, we get a mock response (i think)
    integrate_views
    
    # POST with valid params
    describe "with valid params in all response formats" do
      %w{json xml}.each do |format|
        it "should expose a newly created event as @event" do
          login_as(:quentin) # authorize_as(:default)
          create_event_with_mock format
          assigns[:event].should == @event
          
          response.headers["Status"].should =~ /201/ # Created
        end
  
        it "should return the newly created event, in the correct format" do
          login_as(:quentin) # authorize_as(:default)
          create_event_with_mock format
          response.headers["Status"].should =~ /201/ # Created
          # this is a brittle test... it fails if we begin to use the builder...
          response.body.should == mock_event.send("to_#{format}")
        end
        
        it "should require a login!" do
          # notice that i didnt login first...
          create_event_with_mock format
          response.headers["Status"].should =~ /401/ # Unauthorized
        end
      end # format loop
    end # describe
    
    # POST with INVALID params
    describe "with invalid params in all response formats" do
      %w{js xml}.each do |format|
        it "should expose a newly created but unsaved event as @event" do
          login_as(:default) # authorize_as(:default)
          post :create, :event => {:start => 0.0}
          response.headers["Status"].should =~ /422/ # Unprocessable Entity
        end
      end
    end # describe
    
    # a little helper method for these tasks
    def create_event_with_mock(format = 'html')
      @event = mock_event(:save => true)
      # Create a method stub on the Event model
      # this temporarily replaces the method 'new' on Event model with some fake parameters and assign the result to 
      # a Mock Model instance, @event
      Event.stub!(:new).with({'these' => 'params'}).and_return(@event)

      # This call actually hits the controller with the same 
      post :create, :event => {:these => 'params'}, :format => format
    end
    
  end #describe "POST"
  
  # PUT 
  describe "responding to PUT udpate" do
    fixtures :user, :event
    %w{json xml}.each do |format|
    it "should always require a login!" do
      put :update, :id => "37", :event => {:these => 'params'}, :format => format
      response.headers["Status"].should =~ /401/ # Unauthorized
    end
    end
    
    describe "with valid params" do
      before :each do
        login_as(:quentin)
      end

      %w{json xml}.each do |format|			
        it "should update the requested event" do
          @event = mock_event
          Event.should_receive(:find).with("37").and_return(@event)
          @event.should_receive(:update_attributes).with({'these' => 'params'})
          put :update, :id => "37", :event => {:these => 'params'}
        end
  
        it "should expose the requested event as @event" do
          @event = mock_event(:update_attributes => true)
          Event.stub!(:find).and_return(@event)
          put :update, :id => "1", :format => format
          response.headers["Status"].should =~ /200/ # OK
          assigns(:event).should equal(@event)
        end
  
        it "should return the updated representation of itself" do
          Event.stub!(:find).and_return(mock_event(:update_attributes => true))
          put :update, :id => "1", :format => format
          #puts response.body.inspect
          response.body.should == mock_event.send("to_#{format}")
          #response.should redirect_to(event_url(mock_event))
        end
      end # each format...

    end
    
    # describe "with invalid params" do
    #  
    #      it "should update the requested event" do
    #        Event.should_receive(:find).with("37").and_return(mock_event)
    #        mock_event.should_receive(:update_attributes).with({'these' => 'params'})
    #        put :update, :id => "37", :event => {:these => 'params'}
    #      end
    #  
    #      it "should expose the event as @event" do
    #        Event.stub!(:find).and_return(mock_event(:update_attributes => false))
    #        put :update, :id => "1"
    #        assigns(:event).should == mock_event
    #      end
    #  
    #      it "should re-render the 'edit' template" do
    #        Event.stub!(:find).and_return(mock_event(:update_attributes => false))
    #        put :update, :id => "1"
    #        response.body.should equal(mock_event.send("to_#{format}"))#render_template('edit')
    #      end
    #  
    #    end
  
  end
  # 
  # describe "responding to DELETE destroy" do
  # 
  #   it "should destroy the requested event" do
  #     Event.should_receive(:find).with("37").and_return(mock_event)
  #     mock_event.should_receive(:destroy)
  #     delete :destroy, :id => "37"
  #   end
  # 
  #   it "should redirect to the event list" do
  #     Event.stub!(:find).and_return(mock_event(:destroy => true))
  #     delete :destroy, :id => "1"
  #     response.should redirect_to(events_url)
  #   end
  # 
  # end


	describe "GET" do
		before :all do
      bill = User.find_by_login('bill')
	    bill.destroy if bill
	
			ted = User.find_by_login('ted')
	    ted.destroy if ted
	
			@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
			@ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
			
			@bills_event  = default_event(:user => @bill)
			@teds_event  = default_event(:user => @ted)
    end

		before :each do
      login_as(@bill)
    end

		it "should only show you your own events" do
			
		end
	end


end
=end