require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe TagsController do
  
  describe "route recognition" do
    it "should generate expected params" do
      %w[ sounds projects logs events users recordings notes ].each do |resource|
        params_from(:get, "/#{resource}/tags").should == {:controller => "tags", :action => "index", :parent => "#{resource}"}
        params_from(:get, "/#{resource}/1/tags").should == {:controller => "tags", :action => "index", :parent => "#{resource}", :parent_id => "1"}
      end
    end
    
    it "should NOT generate sons of shit" do
      params_from(:get, "/poop/tags").should == {:controller => "tags", :action => "index", :parent => "poop"}
      
    end
  end
  
end