=begin

State table for access to a Event resource.
In cells marked, additional conditions apply based on the state of another resource.

	
								|	no parent		| public parent	|	private parent	|
	---------------------------------------------------------------
	not logged in	|							|								|									|
	---------------------------------------------------------------
	logged in			|	+						|	++						|									|
	---------------------------------------------------------------
	
	+  User logged in and Event public
		
		* Owner of Event?
		
	++ User logged in and Event private

		* Owner of Event?
		* Event shared with User?
		* Accessed through a Project?
			- User a Project member?
		* Accessed through a parent?
			_ 

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe "Event access:" do
	
	before do
		@owner = mock_model(User)
	end
	
	before :each do
		Sound.destroy_all; Log.destroy_all; Event.destroy_all; Project.destroy_all
	end
	
	#----------
	# User NOT logged in
	#----------
	
	describe "User NOT logged in" do
		before do
			@user  = nil
		end
		
		it "should have the correct listing access" do
			Event.indexable_by?(@user).should be_false
			Event.creatable_by?(@user).should be_false
		end
		
		#----------
		# NO PARENT
		#----------
		
		describe "Event accessed with NO PARENT" do
			before do
				@event = default_event
			end
		
			it "should have the correct access" do
				@event.readable_by?(@user).should be_false
				@event.updatable_by?(@user).should be_false
				@event.deletable_by?(@user).should be_false
			end
		end

		#----------
		# PUBLIC PARENT
		#----------
		
		describe "Event accessed with PUBLIC PARENT" do
			before do
				@log	 = default_log(:public => 1)
				@sound 	 = default_sound(:public => 1, :log_id => @log.id)
				@event   = default_event
				@event.sound = @sound
				@parents = [@sound]
			end

			it "should have the correct access" do
				@event.readable_by?(@user, @parents).should be_true
				@event.updatable_by?(@user, @parents).should be_false
				@event.deletable_by?(@user, @parents).should be_false
			end
		end
		
		#----------
		# PRIVATE PARENT
		#----------
		
		describe "Event accessed with PRIVATE PARENT" do
			before do
				@event  = Event.new
				# parents is always sent as an array
				@parents = Sound.new(:user => @owner, :public => 0)
			end

			it "should have the correct access" do					
				@event.readable_by?(@user, @parents).should be_false
				@event.updatable_by?(@user, @parents).should be_false
				@event.deletable_by?(@user, @parents).should be_false
			end
		end
		
		#----------
		# PRIVATE PARENT, MULTIPLE PARENTS
		#----------
		
		describe "Event accessed with PRIVATE PARENT, with PROJECT" do
			before do
				# lets test with real objects
				@sound 	 = default_sound(:user => @owner, :public => 0)
				@log 	   = default_log(:sound_id => @sound.id, :user => @owner, :public => 0)
				@event   = default_event(:log_id => @log.id)
				@event.sound = @sound
				@project = default_project(:public => 1, :user_id => mock_model(User).id)
				@project.sounds << @sound
				#@project_sound = mock_model(ProjectAsset, :asset_id => @sound.id, :asset_type => "Sound", :project_id => @project.id)
				@parents = [@project, @sound]
			end
			
			it "should be accessible if accessed only through the PROJECT" do
				Event.indexable_by?(@user, [@project]).should be_true
			end
			
			it "should be accessible if PRIVATE PARENT and accessed through PUBLIC PROJECT" do
				Event.indexable_by?(@user, @parents).should be_true
				# these methods always expect an array of parents				
				@event.readable_by?(@user, @parents).should be_true
				@event.updatable_by?(@user, @parents).should be_false
				@event.deletable_by?(@user, @parents).should be_false
			end
			
			it "should NOT be accessible if PRIVATE PARENT and accessed through PRIVATE PROJECT" do
				@project.update_attribute(:public, 0)
				Event.indexable_by?(@user, @parents).should be_false
			end
		end
		
	end
	
	#----------
	# User logged in
	#----------
	
	describe "User logged in" do
		before :each do
			@user  = default_user
			@event = default_event
			@other_user = default_user(:login => '666', :email => "666@evilgoogle.com")
			@event.user = @user
		end
		
		it "should have the correct listing access" do
			Event.indexable_by?(@user).should be_true
			Event.creatable_by?(@user).should be_true
		end
		
		#----------
		# NO PARENT
		#----------
		describe "Event accessed with NO PARENT" do
			it "should have the correct access for owners" do
				@event.readable_by?(@user).should be_true
				@event.updatable_by?(@user).should be_true
				@event.deletable_by?(@user).should be_true
			end
			
			it "should have the correct access for non-owners" do
				@event.readable_by?(@other_user).should == false
				@event.updatable_by?(@other_user).should be_false
				@event.deletable_by?(@other_user).should be_false
			end
		end
	
		#----------
		# PARENTS
		#----------
		
		describe "Event accessed through PARENTS" do
			before do
				@log	 	 = default_log(:public => 1)
				@sound 	 = default_sound(:public => 1, :log_id => @log.id)
				@other_sound = default_sound(:public => 0, :log_id => default_log(:public => 1, :user => @other_user))
				
				@project = default_project
				@project.sounds << @sound
				
				@event   = default_event
				@event.sound = @sound
				@event.user = @user
			end
			
			# Here we test for 2 user accesses, one who owns the event, and one who does not
			describe "PUBLIC PARENTS" do
				before :each do
					@parents = [@project, @sound]
				end
				
				it "should handle correct access" do
					[@log, @sound].each{ |parent|
						@event.readable_by?(@other_user, parent).should be_true
						@event.readable_by?(@user, parent).should be_true
	
						@event.updatable_by?(@other_user, parent).should be_false
						@event.updatable_by?(@user, parent).should be_true
	
						@event.deletable_by?(@other_user, parent).should be_false
						@event.deletable_by?(@user, parent).should be_true
					}
				end
				
				it "should handle PUBLIC PROJECT" do
					# these methods always expect an array of parents
					Event.indexable_by?(@other_user, @parents).should be_true
					
					@event.readable_by?(@other_user, @parents).should be_true
					@event.readable_by?(@user, @parents).should be_true
					
					@event.updatable_by?(@other_user, @parents).should be_false
					@event.updatable_by?(@user, @parents).should be_true
	
					@event.deletable_by?(@other_user, @parents).should be_false	
					@event.deletable_by?(@user, @parents).should be_true
				end
				
				it "should handle PRIVATE PROJECT" do
					@project.update_attribute(:public, 0)
					
					Event.indexable_by?(@other_user, @parents).should be_false
					Event.indexable_by?(@user, @parents).should be_true #its yours

					@event.readable_by?(@other_user, @parents).should be_false
					@event.readable_by?(@user, @parents).should be_true
					
					@event.updatable_by?(@other_user, @parents).should be_false
					@event.updatable_by?(@user, @parents).should be_true
	
					@event.deletable_by?(@other_user, @parents).should be_false	
					@event.deletable_by?(@user, @parents).should be_true
				end
			end
			
			
			describe "PRIVATE PARENTS" do
				before :each do
					@log.update_attribute(:public, 0)
					@sound.update_attribute(:public, 0)
					@parents = [@project, @sound]
					@event.sound = @sound
				end
				
				it "should handle correct access" do		
					@log.update_attribute(:public, 0)
					@sound.update_attribute(:public, 0)
					
					[@log, @sound].each{ |parent|
						@event.readable_by?(@other_user, parent).should be_false
						@event.readable_by?(@user, parent).should be_true
						
						@event.updatable_by?(@other_user, parent).should be_false
						@event.updatable_by?(@user, parent).should be_true
	
						@event.deletable_by?(@other_user, parent).should be_false	
						@event.deletable_by?(@user, parent).should be_true
					}
				end
				
				it "should handle PUBLIC PROJECT" do
					# these methods always expect an array of parents
					Event.indexable_by?(@other_user, @parents).should be_true
					
					@event.readable_by?(@other_user, @parents).should be_true
					@event.readable_by?(@user, @parents).should be_true
					
					@event.updatable_by?(@other_user, @parents).should be_false
					@event.updatable_by?(@user, @parents).should be_true
	
					@event.deletable_by?(@other_user, @parents).should be_false	
					@event.deletable_by?(@user, @parents).should be_true
				end
				
				it "should handle PRIVATE PROJECT" do
					@project.update_attribute(:public, 0)
						
					Event.indexable_by?(@other_user, @parents).should be_false
					
					@event.readable_by?(@other_user, @parents).should be_false
					@event.readable_by?(@user, @parents).should be_true
					
					@event.updatable_by?(@other_user, @parents).should be_false
					@event.updatable_by?(@user, @parents).should be_true
	
					@event.deletable_by?(@other_user, @parents).should be_false	
					@event.deletable_by?(@user, @parents).should be_true
				end
			end
			
			
			
			
			
		end
		
	end
	
	
	
	#describe "User logged in (possibly as the Event's owner)" do
	#	before do
	#		@user = mock_model(User)
	#	end
	#	
	#	it "should have the correct access" do
	#		#it "should be creatable by the User" do
	#		Event.creatable_by?(@user).should be_true
	#		#it "should be indexable by the User" do
	#		Event.indexable_by?(@user).should be_true
	#	end
	#			
	#	#----------
	#	# Event is PUBLIC (x1,y2)
	#	#----------
	#	
	#	describe "Event is PUBLIC (x1,y2)" do
	#		before do
	#			@event = Event.new(:user => @owner, :public => 1)
	#		end
	#		
	#		it "should have the correct access" do
	#			# it "should be readable by the User" do
	#			@event.readable_by?(@user).should be_true
	#		
	#			# it "should NOT be updatable by the User, but updateable by the owner"
	#			@event.updatable_by?(@user).should be_false
	#			@event.updatable_by?(@owner).should be_true
	#		
	#			#it "should NOT be deletable by the User, but deletable by the owner"
	#			@event.deletable_by?(@user).should be_false
	#			@event.deletable_by?(@owner).should be_true
	#		end
	#	end
	#	
	#	#----------
	#	# Event is PRIVATE (x2,y2)
	#	#----------
	#	
	#	describe "Event is PRIVATE (x2,y2)" do
	#		before do
	#			@event = Event.new(:user => @owner, :public => 0)
	#		end
	#
	#		it "should have the correct access" do
	#			# it "should NOT be readable by the User, but readable by the owner" do
	#			@event.readable_by?(@user).should be_false
	#			@event.readable_by?(@owner).should be_true
	#		
	#			# it "should NOT be updatable by the User, but updateable by the owner" do
	#			@event.updatable_by?(@user).should be_false
	#			@event.updatable_by?(@owner).should be_true
	#	
	#			# it "should NOT be deletable by the User, but deletable by the owner" do
	#			@event.deletable_by?(@user).should be_false
	#			@event.deletable_by?(@owner).should be_true
	#		end
	#		
	#		#----------
	#		# accessed through a Project
	#		#----------
	#		
	#		describe "accessed through a Project" do
	#			before do
	#				@project_owner = mock_model(User)
	#				@project = mock_model(Project, :public => 1, :user => @project_owner)
	#			end
	#			
	#			it "should have the correct access" do
	#				#it "should be indexable by the User" do
	#				# HOW IS THIS TESTED?
	#				@project.stub!(:readable_by? => false)
	#				Event.indexable_by?(@user, [@project]).should be_false
	#				
	#				@project.stub!(:readable_by? => true)
	#				Event.indexable_by?(@user, [@project]).should be_true
	#			end
	#			
	#			describe "with membership or is public" do
	#				it "should be readable (even though its a private Event)" do
	#					# Here we are faking out the Project who will be tested elsewhere
	#					# so we are assuming that these are the responses from the Project!
	#					@project.stub!(:readable_by? => true)
	#					@project.stub!(:project_Events => [mock_model(ProjectAsset, :asset_id => @event.id, :asset_type => "Event")])
	#					# these methods always expect an array of parents
	#					@event.readable_by?(@user, [@project]).should be_true
	#					@event.updatable_by?(@user, [@project]).should be_false
	#					@event.deletable_by?(@user, [@project]).should be_false
	#
	#					# just to make sure
	#					@event.readable_by?(@user).should be_false
	#				end
	#			end
	#			
	#			describe "WITHOUT membership or is private" do
	#				it "should NOT be readable" do
	#					# Here we are faking out the Project who will be tested elsewhere
	#					# so we are assuming that these are the responses from the Project!
	#					@project.stub!(:readable_by? => false)
	#					@project.stub!(:project_Events => [])
	#
	#					# these methods always expect an array of parents
	#					@event.readable_by?(@user, [@project]).should be_false
	#					@event.updatable_by?(@user, [@project]).should be_false
	#					@event.deletable_by?(@user, [@project]).should be_false
	#					
	#					@event.readable_by?(@user).should be_false
	#				end
	#			end
	#		end
	#		
	#	end
	#end

end