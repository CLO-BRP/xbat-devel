=begin

State table for access to a Event resource.
In cells marked, additional conditions apply based on the state of another resource.

	
				|	public parent		|	private parent		| private parent, public project
	---------------------------------------------------------------
not logged in	|						|						|					|
	---------------------------------------------------------------
logged in		|	+					|	++					|					|
	---------------------------------------------------------------
	
	+  User logged in and Event public
		
		* Owner of Event?
		
	++ User logged in and Event private

		* Owner of Event?
		* Event shared with User?
		* Accessed through a Project?
			- User a Project member?

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe EventsController do
	
	#----------
	# User NOT logged in
	#----------
	describe "User NOT logged in" do
		
		#----------
		# Event is PUBLIC (x1,y1)
		#----------
		describe "Event's parent is PUBLIC (x1,y1)" do
			it "should have the correct authorization" do
				@user = default_user
				@event = default_event(:user => @user)
				
				# READ
				get :show, :id => @event.id
				response.should be_redirect
				# UPDATE
				put :update, :id => @event.id
				response.should be_redirect
				# DELETE
				put :destroy, :id => @event.id
				response.should be_redirect
			end
		end

		#----------
		# Event is PRIVATE (x2,y1)
		#----------		
		describe "Event is PRIVATE (x2,y1)" do
			it "should have the correct authorization" do
				#@Event = default_event(:user => mock_model(User), :public => 0)
				#
				## READ
				#get :show, :id => @Event.id
				#response.should be_redirect
				## UPDATE
				#put :update, :id => @Event.id
				#response.should be_redirect
				## DELETE
				#put :destroy, :id => @Event.id
				#response.should be_redirect
			end
		end
	end
	
	#----------
	# User logged in
	#----------
	describe "User logged in (possibly as the Event's owner)" do
		before do
			@user  = default_user
			@owner = default_user(:login => 'other', :email => "your@ss.com")
		end
		
		#----------
		# Event is PUBLIC (x1,y2)
		#----------
		describe "Event is PUBLIC (x1,y2)" do
			it "should have the correct authorization" do
				#@Event = default_event(:user => @owner, :public => 1)
				#login_as @user
				#
				## READ
				#get :show, :id => @Event.id
				#response.should be_success
				## UPDATE
				#put :update, :id => @Event.id
				#response.should be_redirect
				## DELETE
				#put :destroy, :id => @Event.id
				#response.should be_redirect
			end
		end
		
		#----------
		# Event is PRIVATE (x2,y2)
		#----------
		describe "Event is PRIVATE (x2,y2)" do
			before do
				#@Event = default_event(:user => @owner, :public => 0)
				#@project = default_project(:public => 1, :user_id => @user.id)			
				#@project.add_assets(Event, @Event)
			end
			
			it "logged in as user should have the correct authorization" do
				#login_as @user
				## READ
				#get :show, :id => @Event.id
				#response.should be_redirect
				## READ
				#get :show, :id => @Event.id, :project_id => @project.id
				#response.should be_success
			end
			
			it "logged in as owner should have the correct authorization" do
				#login_as @owner
				## READ
				#get :show, :id => @Event.id
				#response.should be_success
				## UPDATE
				#put :update, :id => @Event.id, :format => "js"
				## puts # response.headers["Status"]
				#response.should be_success
				## DESTROY
				#put :destroy, :id => @Event.id, :format => "js"
				#response.should be_success
			end
		end
	end
end

