require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Note do
	
	before :each do
		# make some users for the system
		@harry_ziegler = default_user
		@joey_ramone   = default_user(:login => 'joey', :email => 'joey@theramones.com')
		
		# make some notes
		@note = default_note(:user => @harry_zeigler)
	end

=begin	
  describe "#readable_by" do
		it "should return notes readable by the user" do 
			notes = Note.readable_by(@harry_ziegler)
			notes.size.should == 1
		end
		
		it "should NOT return notes that are not the user's" do 
			notes = Note.readable_by(@joey_ramone)
			notes.size.should == 0
		end
	end
=end 
	
	describe "#with_annotation" do
		it "should return notes readable by the user" do 
			note = Note.with_annotation
			note.first.should.respond_to? :annotated_id
		end
	end
	
	describe "#create_content_hash" do 
		it "should just work" do
			note = Note.new(:title=>"Let me tell you something", :body=>"There, I said it.")
			note.create_content_hash.should_not be_nil
			note.content_hash.should_not be_nil
			
			note = Note.create(:title=>"Let me tell you something", :body=>"There, I said it.")
			note.content_hash.should_not be_nil
		end
	end

end
