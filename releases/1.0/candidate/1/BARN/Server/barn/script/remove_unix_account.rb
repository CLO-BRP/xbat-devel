#! /usr/local/bin/ruby

=begin

		The userdel command exits with the following values:

		0  - success
		1  - can't update password file
		2  - invalid command syntax
		6  - specified user doesn't exist
		8  - user currently logged in
		10 - can't update group file
		12 - can't remove home directory
		
=end

if ARGV.size < 1
	puts "ERROR: You must have 1 arg: username"
else
	username = ARGV[0]
	
	puts cmd = "sudo userdel --force --remove #{username}"
	result = `#{cmd}`
	# TODO catch results... 
	
	puts "User #{username} deleted"
end

