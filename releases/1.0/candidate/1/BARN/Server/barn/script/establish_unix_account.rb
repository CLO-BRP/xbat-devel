#! /usr/local/bin/ruby

=begin
	
	READ THIS PLEASE
	
	To get this script to run, especially when called from the server, do these things:
		
	1) Allow the user who runs the webserver to run sudo commands without asking for password.
	Here's a hint: sudoers -> www ALL=NOPASSWD: ALL
	2) Create a group called 'ftp' on you target system
	3) $ sudo apt-get install makepasswd
	
	Disc quotas for users are set up via the OS, not useradd or this script...
	
	The system is currently using vsftp to server FTP to these users who are jailed in their home. 
		
=end

if ARGV.size < 3
	puts "ERROR: You must have 3 args: username, password, home directory"
	
else
	username = ARGV[0]
	password = ARGV[1]
	home_dir = ARGV[2]
	
	# Save the password to a file for later encryption
	puts cmd = "touch #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	puts cmd = "chmod 600 #{home_dir}/passwd.txt"
	result = puts `#{cmd}` # not sure if this is necessary
	
	puts cmd ="echo '#{password}' > #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	# Create the user on the system with no shell access in group ftp
	puts cmd = "sudo useradd -d #{home_dir} -p #{password} -g ftp #{username}"
	result = `#{cmd}`
	
	# Now for a little tomfoolery: use makepasswd to create an encrypted password from the one in the file,
	# and save it to a variable 'ph' - then, modify the users password to be the saved variable. 
	# This is done because Debian (and others) will not allow unencrypted passwords to be entered from stdin
	puts cmd = "ph=$(sudo makepasswd --clearfrom=#{home_dir}/passwd.txt --crypt-md5 | awk '{print $2}') && sudo usermod -p $ph #{username}"
	result = `#{cmd}`
	
	# Clean up the password
	puts cmd ="rm #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	# The home directory needs to have permissions changed
	puts cmd ="sudo chown #{username}:ftp #{home_dir}"
	result = `#{cmd}`
	
	puts "User #{username} added"
end

