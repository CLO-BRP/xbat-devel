# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :cron_log, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

if File.expand_path(__FILE__) =~ /dev/
  set :environment, :development
end

set :cron_log, "/apps/barn.xbat.org/shared/log/cron_log.log"

every 1.day, :at => '4:30 am' do
	rake "db:backup"
end

every 1.minutes do
	rake "files:process"
	
	rake "files:cleanup"
end

#0 3 * * * cd /apps/barn.xbat.org/current && /usr/bin/rake db:backup DIR=/apps/backups/barn.xbat.org RAILS_ENV=production >/dev/null 2>&1
#*/5 * * * * cd /apps/barn.xbat.org/current && /usr/bin/rake files:process RAILS_ENV=production >/dev/null 2>&1
#*/5 * * * * cd /apps/barn.xbat.org/current && /usr/bin/rake files:cleanup RAILS_ENV=production >/dev/null 2>&1
