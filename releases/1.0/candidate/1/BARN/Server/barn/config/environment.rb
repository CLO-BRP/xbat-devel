# NOTE: be sure to restart your server when you modify this file

# NOTE: uncomment below to force production mode when you don't control web/app server and can't set it the proper way

# ENV['RAILS_ENV'] ||= 'production'

# NOTE: here we specify gem version of Rails to use when vendor/rails is not present

RAILS_GEM_VERSION = '2.1.2' unless defined? RAILS_GEM_VERSION

# NOTE: bootstrap the Rails environment, frameworks, and default configuration

require File.join(File.dirname(__FILE__), 'boot')

require 'fileutils'

# NOTE: we use this to get information about the current installation of Ruby

require 'rbconfig'

Rails::Initializer.run do |config|
	
	# NOTE: settings in config/environments/* take precedence over those specified here
	
	# NOTE: application configuration should go into files in config/initializers
	
	# -- all .rb files in that directory are automatically loaded.
	# -- See Rails::Configuration for more options.

	# NOTE: skip unused frameworks, to use Rails without a database remove the Active Record framework.
	
	# config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

	#--
	# gems
	#--
	
	# NOTE: here we specify gems that this application depends on.
	# -- They can then be installed with "rake gems:install" on new installations
	
	config.load_paths += Dir["#{RAILS_ROOT}/vendor/gems/**"].map do |dir|
		
		File.directory?(lib = "#{dir}/lib") ? lib : dir
	end

	config.gem 'whenever'

	config.gem 'mysql'

	config.gem 'mongrel'

	config.gem "rubyist-aasm", :lib => 'aasm', :source => "http://gems.github.com"

	config.gem "fastercsv"

	config.gem 'mime-types', :lib => 'mime/types'

	config.gem 'hpricot'

	config.gem 'javan-whenever', :lib => false, :source => 'http://gems.github.com'
	
	config.gem 'geokit'

	config.after_initialize do

		#--
		# ensure certain installs on windows
		#--
		
		# NOTE: this is not a reliable approach, not all windows machines have a development environment
		
		# gem install sqlite3-ruby --version=1.2.2
		
		if Platform.is_windows?
			
			begin
				if `sqlite3 --version`
					# puts 'sqlite3 is installed'
				else
					# TODO: come up with a solution to sqlite3 libraries
					# modify windows path to include the lib/sqlite folder?
					# both the sqlite.exe and the sqlite.dll need to be in the environment path.
	
					# puts 'sqlite3 is NOT installed'
				end
	
				# NOTE: instructions for ImageScience for Windows can be found here:
				
				# -- http://thewebfellas.com/blog/2008/2/18/imagescience-on-windows-without-the-pain
				
				# NOTE: here we install FreeImage to local ruby path
					
				# TODO: this is not working on Windows 7, probably because of privilieges!
				
				bindir = Config::CONFIG['bindir']

				# puts "#{bindir}"
				
				if File.exist?(bindir + 'FreeImage.dll')
					
					puts "Freeimage found in '#{bindir}'"
				else
					source = "#{RAILS_ROOT}/vendor/bin/FreeImage.dll"

					FileUtils.cp(source, bindir + "FreeImage.dll")
					
					puts "FreeImage copied to '#{bindir}'"
				end
	
				config.gem 'image_science'
			rescue
				
				# TODO: say something, or do something here
			end
		else
			
			config.gem 'image_science'
		end
	end

	# config.gem 'restclient'
	# config.gem "dmichael-set-notation-helper", :lib => 'set_notation_helper', :source => "http://gems.github.com"

	# TODO - include the mysql.rb library for native Ruby MySQL access...

	# Only load the plugins named here, in the order given. By default, all plugins
	# in vendor/plugins are loaded in alphabetical order.
	# :all can be used as a placeholder for all plugins not explicitly named
	# config.plugins = [
	#	 :acts_as_rateable,
	#	 :acts_as_annotated,
	#	 :acts_as_taggable_on_steroids,
	#	 :restful_authentication,
	#	 :rspec,
	#	 'rspec-rails',
	#	 :validates_email_format_of,
	#	 :will_paginate
	# ]

	# TODO: make sure we can exclude some plugins, http://www.dcmanges.com/blog/15

	#config.plugins = Dir.entries(File.join(RAILS_ROOT, 'vendor', 'plugins'))
	#config.plugins -= 'thinking-sphinx'

	# Add additional load paths for your own custom dirs


	config.load_paths += %W(
		#{RAILS_ROOT}/app/models/extensions
		#{RAILS_ROOT}/lib/authorization_apocalypse
	)


	# Force all environments to use the same logger level
	# (by default production uses :info, the others :debug)
	# config.log_level = :debug

	# Make Time.zone default to the specified zone, and make Active Record store time values
	# in the database in UTC, and return them converted to the specified local zone.
	# Run "rake -D time" for a list of tasks for finding time zone names. Comment line to use default local time.
	config.time_zone = 'UTC'

	# Your secret key for verifying cookie session data integrity.
	# If you change this key, all old sessions will become invalid!
	# Make sure the secret is at least 30 characters and all random,
	# no regular words or you'll be exposed to dictionary attacks.
	config.action_controller.session = {
		:session_key 	=> '_barn_session',
		:secret				=> '1a72951ead92ea6e739efa07a4fcb2ca5a752f2e1143609d11a2d217eaa8cfa827d5f1c97af1470797db9fb417d0c6af0fe2b486f5c2760a5e258a8793c89294'
	}
	# Use the database for sessions instead of the cookie-based default,
	# which shouldn't be used to store highly confidential information
	# (create the session table with "rake db:sessions:create")
	# config.action_controller.session_store = :active_record_store

	# Use SQL instead of Active Record's schema dumper when creating the test database.
	# This is necessary if your schema can't be completely dumped by the schema dumper,
	# like if you have constraints or database-specific column types
	config.active_record.schema_format = :sql

	# Activate observers that should always be running

	# config.active_record.observers = :user_observer

end

#--
# application variables
#--

ActiveRecord::Base.pluralize_table_names = false

ActiveRecord::Base.include_root_in_json = false

$svn_revision = `svn info #{RAILS_ROOT}`.match('Revision.*')

$server_domain_name = 'barn.xbat.org'

domain = (RAILS_ENV == "development") ? "dev.barn.xbat.org" : "barn.xbat.org"

# NOTE: this could be defined in the /environments/* files

# NOTE: this is system dependent and currently only used in the Linux server deployments

USER_FTP_ROOT = File.join(RAILS_ROOT, "users")

#USER_FTP_ROOT = "/mnt/barn-share/#{domain}/users/"

UNIX_VOLUMES_ROOT = "/mnt/filer60"

UNIX = 0; WINDOWS = 1; AMIGAOS = 2 # ;)

FILESYSTEM = WINDOWS

#--
# libraries
#--

require 'fastercsv'

require 'mime/types'

# NOTE: these could be moved to initializers

require 'activerecord_extensions'

require 'hash_extensions'

require 'guid_generator'

require 'filebrowser'

require 'set_notation_helper'

require 'playlist/lib/playlist'

require 'whenever'

# require 'rbconfig'

require 'tag_extensions'

def trace
	return caller[0].split('/').last
end

def barn_logger(*args)
	level, trace, string  = :info, "", ""

	if args.size == 3
		level  = args[0]
		
 		trace  = args[1]
		
		string = args[2]
	
	elsif args.size == 2
		level  = args[0]
		
		string = args[1]
	else
		
		string = args[0]
	end

	logger.send(level, "\n|| #{level.to_s.upcase} -> #{trace}")
	
	logger.send(level, "||---------------------------------------------------")
	
	logger.send(level, "|| #{string}")
	
	logger.send(level, "||---------------------------------------------------\n\n")
end


#-------------
# setup
#-------------

# NOTE: make sure that the default user is in the database

# If the user table does not have a state field with a default value of 'passive'
# this will fail! with the message 'State initalpending does not exist' - if the db was created by XBAT, this could happen
#
# There are also some weird things happening the with the built-in mysql and the gem
# between Rails 2.1.2 and 2.2 ON WINDOWS ONLY

begin
	#--
	# setup default roles
	#--

	Role.default_roles.each do |role|
		Role.find_or_create_by_name(role)
	end

	#--
	# make sure we have a default user
	#--

	@user = User.find(:first, :conditions => "login = 'default'")

	if @user.blank?
		puts "Creating the default user, be sure to modify the password!"

		@user = User.create(
			:name => "default",
			:login => "default",
			:email => "default@email.com",
			:password => "password",
			:password_confirmation => "password"
		)

		@user.register

		@user.activate

		@user.save
	end

	# NOTE: we make sure to at least have a 'default' admin user

	unless @user.has_role?('administrator')
		@user.roles << Role.find_by_name('administrator')

		@user.save
	end

rescue Exception => e
	puts "BOOTSTRAPPING FAILED! Please check the logs and correct."

	puts e
end
