module ServerStatistics
  require 'socket'

	def local_ip
	  orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse DNS resolution temporarily

	  begin
		  UDPSocket.open do |s|
			s.connect '64.233.187.99', 1
			s.addr.last
		  end
	  rescue #Errno::ENOENT => e
		#barn_logger e
		#render :text => "Sorry, #{e}"
		'Local Access'
	  end

	ensure
	  Socket.do_not_reverse_lookup = orig
	end
	
end