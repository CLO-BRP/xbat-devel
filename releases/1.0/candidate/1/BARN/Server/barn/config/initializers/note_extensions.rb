# These methods are written in a module because they rely on a couple of files specific to this project
# Otherwise, acts_as_annotated should be fine.

# module NoteExtensions
#   
#   # Apparently this method is called when a module is included dynamically into ActiveRecord or a derived class
#   def self.included(base) # :nodoc:
#     super
#     
#     # include set notation methods
#     base.send(:include, WillPaginateHelper)
#     base.send(:include, NamedScopeHelper)
#     
#     # base.extend ClassMethods
#   end
#   
#   module ClassMethods
#   
#   end 
# 	# ClassMethods
#   
# end
# 
# Note.send(:include, NoteExtensions)
# 
# Annotation.send(:include, NoteExtensions)