ActionController::Routing::Routes.draw do |map|
	
	#---------------
	# Set notation routing
	#---------------

	# Match for set notation, this must be at the top of the routes
	# These 2 expressions account for URL escaped and non-escaped URLs
	
	# these do not account for spaces in the set => {10, 11}. Fix that.

	set_regex = /%7B\s*([\d\-,(%20)*]*)\s*%7D/ 
	set_regex2 = /\{\s*([\d\-,\s*]*)\s*\}/
	set_regexes = [set_regex, set_regex2]

	%w[ sounds projects logs events users recordings notes ratings tags data_files file_resources].each do |resource|
		set_regexes.each do |regex|
			map.with_options( :controller => resource, :action => 'index', :id => regex, :conditions => { :method => :get }) do |r|
				r.connect "/#{resource}/:id.:format"
				r.connect "/#{resource}/:id"
			end
		end
	end

	
	set_regex = /%7B\s*([\d\-,(%20)*]*)\s*%7D/
	set_regex2 = /\{\s*([\d\-,\s*]*)\s*\}/
	set_regexes = [set_regex, set_regex2]

	%w[ sounds events projects logs file_resources].each do |resource|
		set_regexes.each do |regex|
			map.with_options( :controller => resource, :action => 'update', :id => regex, :conditions => { :method => :post }) do |r|
				r.connect "/#{resource}/:id.:format"
				r.connect "/#{resource}/:id"
			end
		end
	end


	# have a look at to_param for set parsing - included in the SetNotationHelper

	#---------------
	# Sync routing
	#---------------

	map.connect 'sync/setup', :controller => 'sync', :action => 'index',  :conditions => { :method => :get } 
		
	map.connect 'sync/request', :controller => 'sync', :action => 'pull_requester',  :conditions => { :method => :get } 
	map.connect 'sync', :controller => 'sync', :action => 'pull_responder',  :conditions => { :method => :get } 

	map.connect 'sync/request', :controller => 'sync', :action => 'push_requester',  :conditions => { :method => :put } 
	map.connect 'sync', :controller => 'sync', :action => 'push_responder',  :conditions => { :method => :put } 

	# map.connect 'sync/gui', :controller => 'sync', :action => 'gui',  :conditions => { :method => :get } 
	# map.resources :sync, :only => [:index, :update] 	
	map.resources :peers do |peers|
		peers.resources :tags,	:requirements => { :type => 'peer' }
		peers.resources :notes, :requirements => { :type => 'peer' }
		peers.resources :ratings, :requirements => { :type => 'peer' }
	end

	#---------------
	# Silos routing
	#---------------
	
	map.connect 'silos/remote_request', :controller => 'silos', :action => 'remote_request', :conditions => {:method => :get}
	map.connect 'silos/:guid/request', :controller => 'silos', :action => 'deferred_request', :conditions => {:method => :get}
	map.connect 'silos/:guid/response', :controller => 'silos', :action => 'deferred_response', :conditions => {:method => :post}
	map.connect 'silos/:guid/:request', :controller => 'silos', :action => 'remote_request', :conditions => {:method => :get}
	
	map.resources :silos
					
	#---------------
	# User routing
	#---------------
	
	map.logout '/logout', :controller => 'sessions', :action => 'destroy'

	map.login '/login', :controller => 'sessions', :action => 'new'
	
	map.register '/register', :controller => 'users', :action => 'create'

	map.signup '/signup', :controller => 'users', :action => 'new'
	
	map.dashboard '/dashboard', :controller => 'home', :action => 'dashboard'
	
	map.activate '/activate/:activation_code', :controller => 'users', :action => 'activate', :activation_code => nil 
	
	map.error '/error', :controller => 'some_controller', :action => 'error_action'
	
	map.denied '/denied', :controller => 'some_controller', :action => 'denied_action'
	
	
	#---------------
	# Tags
	#---------------
	
	map.resources :tags do |tags|
		tags.resources :sounds, :logs, :events, :projects, :users
	end
	
	map.connect "tags/autocomplete", :controller => "tags", :action => "autocomplete"
  
	map.connect ":parent/tags", :controller => "tags", :action => "index"
	map.connect ":parent/tags.:format", :controller => "tags", :action => "index"
	
	map.connect ":parent/:parent_id/tags", :controller => "tags", :action => "index"	
	map.connect ":parent/:parent_id/tags.:format", :controller => "tags", :action => "index"	
  
	#---------------
	# DataFiles
	#---------------
	
	map.resources :data_files
	
	#---------------
	# User
	#---------------

	map.connect "/users/forgot_password", :controller => "users", :action => "forgot_password"
	
	map.resources :users, :member => {:suspend	 => :put, :unsuspend => :put, :purge => :delete} do |users|
		users.resources :projects
		users.resources :memberships, :controller => :projects
	end
	


	map.resource :session
	 
	# map.resources :users, :member => { :enable => :put } do |users|
	#	 users.resource :account
	#	 users.resources :roles
	# end

	#---------------
	# Sounds
	#---------------
	
	map.resources :sounds do |sounds|
		sounds.resources :logs
		sounds.resources :events
		sounds.resources :recordings
		sounds.resources :users				
		sounds.resources :tags,	:requirements => { :type => 'sound' }
		sounds.resources :notes, :requirements => { :type => 'sound' }
		sounds.resources :shares, :requirements => { :type => 'sound' }
		sounds.resources :ratings, :requirements => { :type => 'sound' }
		sounds.resources :file_resources, :requirements => { :type => 'sound' }
	end
	
	# Sad experimental implementation of *nested* set notation support - see the top of this file for set notation routing
	# there must be a better way to do this as this wont scale...
	set_regexes.each do |regex|
		['sounds', 'logs', 'users', 'file_resources'].each do |association|
			map.with_options(:controller => association, :action => 'index', :id => regex) do |resource|
				resource.connect "/projects/:project_id/#{association}/:id"
				resource.connect "/projects/:project_id/#{association}/:id.:format"
			end
		end
	end
	
	
    #---------------
    # Extensions
    #---------------

    map.resources :extensions, :has_many => :presets, :only => [:index]	

	#---------------
	# Events
	#---------------

	map.resources :events do |events|
		# Nested resources that are polymorphic, like these, should be defined in block form (rather than :has_many)
		# in order to retain 'type' params
		events.resources :tags,	:requirements => { :type => 'event' }
		events.resources :notes, :requirements => { :type => 'event' }
		events.resources :ratings, :requirements => { :type => 'event' }
	end

	map.connect "/events/:id/review.:format", :controller => "events", :action => "update_review", :method => :post

	#---------------
	# Logs
	#---------------
	
	map.resources :logs do |logs|
		logs.resources :events
		logs.resources :sounds
		logs.resources :tags,	:requirements => { :type => 'log' }
		logs.resources :notes, :requirements => { :type => 'log' }
		logs.resources :ratings, :requirements => { :type => 'log' }
	end

	#----------
	# Projects
	#----------

	#map.connect "/projects/file_widget", :controller => "projects", :action => "file_widget", :conditions => {:method => :get}

	map.connect "/projects/find_users", :controller => "projects", :action => "find_users", :conditions => {:method => :post}	

	map.resources :projects, :member => { :remove_member => :put} do |projects|

		projects.resources :images
		projects.resources :users
		projects.resources :sounds do |sounds|
			sounds.resources :events
		end
		projects.resources :logs do |logs|
			logs.resources :events
		end
		projects.resources :notes, :requirements => { :type => 'project' }
		projects.resources :ratings, :requirements => { :type => 'project' }
		projects.resources :file_resources, :requirements => { :type => 'project' }

	end

	#----------
	# Recordings
	#----------
	
	map.resources :recordings do |recordings|
		recordings.resources :sounds
		recordings.resources :notes, :requirements => { :type => 'recording' }
		recordings.resources :ratings, :requirements => { :type => 'recording' }
	end


	#----------
	# Search
	#----------

	map.connect "/search", :controller => 'search', :action => 'index', :conditions => { :method => :get }






	#----------
	# Shares
	#----------
	
	map.with_options :controller => 'shares' do |shares|
		shares.sharing '/sharing', :action => 'show', :id => "outbox", :conditions => { :method => :get }
		shares.shared '/shared', :action => 'show', :id => "inbox", :conditions => { :method => :get }
	end

    #----------
    # Tasks
    #----------

	map.resources :tasks    

#    set_regexes.each do |regex|
#      map.connect 'tasks/:verb/:noun/:id', :requirements => { :id =>regex }, :conditions => { :method => :post }
#    end

	#----------	
	# The polymorphic resources.
	#----------

	map.resources :ratings
	
	map.resources :notes
	
	map.resources :shares

	map.resources :file_resources

	#---------- 
	# Root routes
	#----------
	
	map.root :controller => "sounds", :action => 'index'
	
	map.connect ':controller/:action/:id'

	map.connect ':controller/:action/:id.:format'






	#---------- 
	# original notes
	#---------- 

	# See how all your routes lay out with "rake routes"

	# Install the default routes as the lowest priority.
	# Note: These default routes make all actions in every controller accessible via GET requests. You should
	# consider removing the them or commenting them out if you're using named routes and resources.

	# The priority is based upon order of creation: first created -> highest priority.

	# Sample of regular route:
	#	 map.connect 'products/:id', :controller => 'catalog', :action => 'view'
	# Keep in mind you can assign values other than :controller and :action

	# Sample of named route:
	#	 map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
	# This route can be invoked with purchase_url(:id => product.id)

	# Sample resource route (maps HTTP verbs to controller actions automatically):
	#	 map.resources :products

	# Sample resource route with options:
	#	 map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

	# Sample resource route with sub-resources:
	#	 map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
	
	# Sample resource route with more complex sub-resources
	#	 map.resources :products do |products|
	#		 products.resources :comments
	#		 products.resources :sales, :collection => { :recent => :get }
	#	 end

	# Sample resource route within a namespace:
	#	 map.namespace :admin do |admin|
	#		 # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
	#		 admin.resources :products
	#	 end

end
