xml.instruct!

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do

    xml.title       "#{$server_domain_name || local_ip}: #{feed_title('Sounds')}"
    xml.link        url_for(:only_path => false, :controller => 'sounds')
    xml.pubDate     CGI.rfc1123_date(@sounds.first.created_at) if !@sounds.blank? && !@sounds.first.created_at.blank?
    xml.description "Sounds recently added to BARN"

    @sounds.each do |sound|
      xml.item do
        xml.title       sound.name
        xml.link        url_for(:only_path => false, :controller => 'sounds', :action => 'show', :id => sound.id)
        xml.description "#{sound.samplerate}Hz, #{sec_to_clock(sound.duration)}, #{sound.channels} channel"
        xml.pubDate     CGI.rfc1123_date sound.modified_at if !sound.modified_at.blank?
        xml.guid        url_for(:only_path => false, :controller => 'sounds', :action => 'show', :id => sound.guid)
      end
    end

  end
end
