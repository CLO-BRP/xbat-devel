module ProjectsHelper
	
	def columns_default
      columns = super
      
      #columns.concat ListingColumn.assemble(%w[project access description rating tags])
      
      #NOTE: controlling width here may be outdated, since we now have columns classes?
      
	  columns.concat ListingColumn.assemble([
		  {:name => 'project', :width => '15%'},
		  {:name => 'access', :width => '4%'},
		  {:name => 'description', :width => '20%'},
		  {:name => 'rating', :width => '12%'},
		  {:name => 'tags', :width => ''}
	  ]);
	end
	

	# TODO: add number of members column to project

	def column_markup (row, column, project)
			
		case column.name
			
		when 'access'
			html = ""
			
			if logged_in?
				status = (project.public?)? 'unlocked' : 'locked'
				
				html << jquery_icon(status)
			end
			
		when 'project'
			# mark up role and publicity
			if logged_in?
				powner = project.owner
				role = (project.owner == User.current.id)? "Owner" : "Member";
				
				publicity = (project.public)? "Public" : "Private"
				
				status = (project.public?)? 'unlocked' : 'locked'

				icon = jquery_icon(status)
			else
				if project.owner.nil?
					role = "owner removed"
				else
					name = (project.owner.name.blank?)? project.owner.login : project.owner.name
					
					role = link_to(name, user_path(project.owner))
				end
				
				publicity = 'Public'
			end
		
			# TODO: write this using interpolated string syntax, that should be the standard here
			
			str	= '<b>%s %s</b> %s<br/><div style="display:none;margin: 0.75em 0em 1em"></div>'
			
			name = link_to(h(project.name), project_path(project))
			
			return name;
		
			## description = '('+ role + ', ' + publicity + ')'
			#description = " (#{role})"
			#
			#return sprintf(str, icon, name, description)
		
		else
			super
			
		end
	end
	
	 
	def column_sorting (column)
			
		case column.name

		when 'access'
			{:order => 'project.public', :direction => toggle_sort_direction('project.public')}
			
		when 'project'
			{:order => 'name', :direction => toggle_sort_direction('name')}
				
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating')}
			
		else
			return super
			
		end
	end
	
	
	def sub_menu

		element = [];

		case params[:action]

		when 'show'
			# TODO: implement adding project members and sounds

			# NOTE: consider a new interface metaphor where we can look through a list of sounds or users and add them to another list

			element = [
				#{:title => 'Add Member', :path => "#{params[:id]}/users"},
				#{:title => 'Add Sound', :path => "/some"},
				{:title => "New Scan", :path => "#{params[:id]}/tasks/new?type=scan"}
			];

		else
			element = [
				{:title => "New Project", :path => "projects/new"}
			];

		end

		return element;
	end

	#def sub_menu
	#	return [ { :title => "add project", :path => "/projects/new" }, { :title => "configure scan", :address => "/projects/configure_scan" } ]
	#end

end
