xml.recording {
  xml.id(recording.id, :type => 'integer')
  xml.bytes(recording.bytes, :type => 'integer')
  xml.channels(recording.channels, :type => 'integer')
  xml.content_type(recording.channels, :type => 'integer')
  xml.samplesize(recording.samplesize, :type => 'float')
  xml.samplerate(recording.samplerate, :type => 'float')
  xml.samples(recording.samples, :type => 'integer')
  xml.duration(recording.duration, :type => 'float')
  xml.file(recording.file, :type => 'string')  
  
  # rating
  rating = recording.rating_by_user(User.current)
  if !rating.blank?
    xml.user_rating(rating, :type => 'float')
    xml.user_rating_id(rating.id, :type => 'integer')
  else
    xml.user_rating(nil, :type => 'float')  
    xml.user_rating_id(nil, :type => 'integer')
  end
  
  xml.average_rating(recording.rating, :type => 'float')
  xml.number_of_ratings(recording.ratings.size, :type => 'float')
  
  xml.tag_list(recording.tag_list, :type => 'string')  
  xml.created_at(recording.created_at, :type => 'timestamp')
  xml.modified_at(recording.modified_at, :type => 'timestamp')
}