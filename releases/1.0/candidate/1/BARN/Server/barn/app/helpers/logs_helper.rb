module LogsHelper
  
  def columns_default
      columns = super
      
      # NOTE: this is a funny way of constructing the columns, can we do it all at once
      
      # NOTE: the columns and width descriptions are the same as for sounds
      
      columns.concat ListingColumn.assemble([
		{:name => 'log', :width => '15%'}
	  ]);
      
      columns.concat ListingColumn.assemble(%w[events rating])
      
      columns.concat ListingColumn.assemble([
			{:name => 'tags', :width => '60%'}
	  ]);
  end
  
  def column_markup (row, column, log)
	
		case column.name
			
		when 'log'
			link_to(log.name, log_path(log))
			
		when 'events'
			log.events_count
			
		else
			super
			
		end
  end
  
  def column_sorting(column)
	
		case column.name
			
		when 'log'
			{:order => 'name', :direction => toggle_sort_direction('name')}
		
		when 'events'
			{:order => 'log.events_count', :direction => toggle_sort_direction('log.events_count')}
			
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating')}
		
		else
			super
		
		end
  end
  
  def sub_menu
	  element = [];
	  
	  case params[:action]
	  
	  when 'show'
		# TODO: the 'show' action should show a different menu
		
	  else
		element.push({:title => "New Log", :path => "logs/new"});
		
	  end
  
	  return element;
  end	
  
end
