module SoundsHelper

	def header_markup ( column, params )
		
		case column.name
			
			when 'sound'
				#this sort of problem is where a viewmodel would help
				order = params.include?(:order) ? params[:order] : 'name'

				control = "<dl class='render-sort-menu dropdown'><dt><a>#{order}</a></dt>"
				control << "<dd><ul>"

				sound_sort.each { | sortcol |
					sorting = column_sorting(sortcol)
					control << "<li>" + link_to(title_caps(sortcol.name), params.merge(sorting).merge({:page => 1}), :class => "sort-listing") + "</li>"
				}

				control << "</ul></dd>"
				control << "</dl>"

				html = "<th width='#{column.width}' class='multisort'><span>#{title_caps('sort by')}</span>#{control}<span>#{params[:direction] == 'desc' ? 'descending' : 'ascending'}</span></th>"
		  	
#			when 'name'
#				html = "<th width='#{column.width}'>Sound</th>"
				
		  	when 'tags'
				html = "<th width='#{column.width}'>#{title_caps('tags')}</th>"
		    
		    else
		  		super
		end
	end

	def sound_sort
	
		sortcols = ListingColumn.assemble(%w[name duration channels events samplerate ratings tags])
	end

	def columns_default
	    # NOTE: from the parent we get selection and expansion columns
	    
	    columns = super

		#columns.concat ListingColumn.assemble([
		#	{ :name => 'sound', :width => '45%'},
		#	{ :name => 'events', :width => '10%'},
		#	{ :name => 'tags', :width => '45%'}
		#]);
		
		columns.concat ListingColumn.assemble([
			{:name => 'name', :width => '15%'},
			{:name => 'access', :width => '4%'},
			{:name => 'events', :width => '4%'},
			{:name => 'rating', :width => '12%'},
			{:name => 'tags', :width => ''}
		]);
 	end
  
	def column_markup( row, column, sound )
		  
		case column.name
  
		when 'access'
			html = ""
			
			if logged_in?
				status = (sound.public?)? 'unlocked' : 'locked'
				
				html << jquery_icon(status)
				
				# html << "<span class='icon #{status}'>&nbsp;&nbsp;</span>"
			end
  
		when 'name'
			html = ''
			
			if params[:project_id]
				html << link_to(sound.name, project_sound_path(params[:project_id],sound))
			else
				html << link_to(sound.name, sound_path(sound))
			end
			
		when 'duration'
			sec_to_clock(sound.duration)
		
		when 'contributor'
			sound.user.login if sound.user
  
		  when 'sound'
			html = "<span class='sound'>"
  
			if params[:project_id]
				html << link_to(sound.name, project_sound_path(params[:project_id],sound))
			else
			  html << link_to(sound.name, sound_path(sound))
			end
			html << "</span>"
  
			html << "<ul class='sounddata'>"
			html << "<li><em>T</em> #{sec_to_clock(sound.duration)}</li>"
			html << "<li><em>CH</em> #{sound.channels}</li>"
			html << "<li><em>R</em> #{rate_to_hertz(sound.samplerate)}</li>"
			html << "</ul>"
  
			html << "<span class='render simple-rating' target='#{send(sound.class.to_s.downcase + '_path', sound)}'>
				#{sound.rating}
			</span>"			
  
		  when 'events'
			sound.events_count
			
		else
			super
			
		end
		  
	end
	
	
	def column_sorting (column)		
		
		params = {}
		
		case column.name
			
		when 'access'
			params = {:order => 'sound.public', :direction => toggle_sort_direction('sound.public')}
			
		when 'name'
			params = {:order => 'sound.name', :direction => toggle_sort_direction('sound.name')}
		
		when 'log'	
			params = {:order => 'log.name', :direction => toggle_sort_direction('log.name')}
		
		when 'events'
			params = {:order => 'sound.events_count', :direction => toggle_sort_direction('sound.events_count')}
		
		else
			return super
		
		end
		
		return params
	end

	def post_row( options, columns, row, object )
	
		super
	end
	
	def sub_menu
		element = [];
			
		case params[:action]
			
		when 'show'
			element.push({:title => "New Scan", :path => "#{params[:id]}/tasks/new?type=scan"})	
		
		else
			element.push({:title => "New Sound", :path => "sounds/new"})
			
		end
		
		return element; 
	end	

end
