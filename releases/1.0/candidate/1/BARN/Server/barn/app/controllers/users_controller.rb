class UsersController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
	# Protect these actions behind an admin login		
	before_filter :login_required, :except => [:index, :show, :create, :new, :forgot_password]#, :only => [:edit, :update]
	before_filter :has_permission?, :except => [:forgot_password]
	
	before_filter :find_user, :only => [:suspend, :unsuspend, :destroy, :purge]
	
	# Example standard URIs
	# GET /users
	# GET /users.(html|xml|js|json)
	# GET /users/{1}
	# GET /users/{1-10, 15}
	# GET /users.xml?public=1&page=1&order=name

	# Example Nested URIs
	# GET /projects/1/users
	# GET /projects/{1,3,5-10}/users

	def index
		# special accounting for projects
		if params.include?(:project_id)
			@users = User.restrict_fields.in_project(params[:project_id]).in_set(params[:id]).filtered_by( params ).ordered_and_paginated(params)
			count = User.in_project(params[:project_id]).in_set(params[:id]).filtered_by( params ).count
			
			# this is incorrect for project tags....! there is an error in the tag_counts_rescoped for associations...!
			@tags = []#User.with_parent_in_set( params ).filtered_by( params ).tag_counts_rescoped
		else
			@users = User.restrict_fields.with_parent_in_set( params ).filtered_by( params ).ordered_and_paginated( params )
			count = User.with_parent_in_set( params ).filtered_by( params ).count
			@tags = User.with_parent_in_set( params ).filtered_by( params ).tag_counts_rescoped
		end
		
		@metadata	 = {
			:total_entries => count,
			:per_page => params[:per_page] || User.per_page,
			:page		 => params[:page] || 1
		}
		
		response_for(@users)
	end
	
	
	# GET /users/1
	
	def show
		@user = User.find(params[:id])
		 
		respond_to do |format|
			format.xml { render :xml => @user }
			format.any(:js, :json) { render :json => @user }
			format.html
		end
	end
	
	# render new.rhtml
		
	def new
		@user = User.new
		
		respond_to do |format|
			format.xml { render :xml => @user }
			format.any(:js, :json) { render :json => @user }
			format.html
		end
	end
		
	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])

		if @user.update_attributes(params[:user])
			update_roles(@user)
			@user.update_unix_password(params[:user][:password])
			flash[:notice] = 'User was successfully updated.'
			redirect_to user_path(@user)
		else
			render :action => "edit"
		end
	end
 
	# curl -H "Authorization:ZGVmYXVsdDpwYXNzd29yZA==" -X POST http://localhost:3000/users -d 'user[login]=xxx&user[password]=xxxxxx&user[password_confirmation]=xxxxxx&user[email]=x@x.com'
	
	def create
		# logout_keeping_session!
		@user				= User.new(params[:user])
		@user.name 	= @user.login if @user.name.blank?
				
		if @user && @user.valid? && @user.errors.empty?
			@user.register!
	
			# TODO: This should be moved to after the activation !!
			# This might require saving off the password someplace
	
			if logged_in? and User.current.is_admin?
				@user.activate!
				@user.establish_unix_account(params[:user][:password])	
				
				update_roles(@user)
				@user.save
			else				
#				UserMailer.deliver_signup_notification(@user)
#				UserMailer.deliver_activation(@user) if @user.recently_activated?
				flash[:notice] = "An email has been sent to #{@user.email} with an activation code."
			end
			
			respond_to do |format|
				format.html{ 
					redirect_back_or_default("/users/#{@user.id}")
				}
				format.xml{ render :xml => @user }
				format.js{ render :json => @user }
			end
			return
		end
		# flash[:error]	= "We couldn't set up that account, sorry.	Please try again, or contact an admin (link is above)."
		# render :action => 'new'
		respond_to do |format|
			format.xml	{ render :xml => @user.errors }
			format.js	 { render :json => @user.errors }
			format.html { render :action => 'new' }
		end
		
	end

	def activate
		logout_keeping_session!
		user = User.find_by_activation_code(params[:activation_code]) unless params[:activation_code].blank?
		
		case	
			when (!params[:activation_code].blank?) && user && !user.active?
				user.activate!
				user.establish_unix_account(params[:user][:password])
				
				flash[:notice] = "Signup complete! Please sign in to continue."
				redirect_to '/login'
			when params[:activation_code].blank?
				flash[:error] = "The activation code was missing.	Please follow the URL from your email."
				redirect_back_or_default('/')
			else 
				flash[:error]	= "We couldn't find a user with that activation code -- check your email? Or maybe you've already activated -- try signing in."
				redirect_back_or_default('/')
		end
	end

	def suspend
		@user.suspend! 
		redirect_to user_path
	end

	def unsuspend
		@user.unsuspend! 
		redirect_to user_path
	end

	def destroy
		@user.delete!
		flash[:notice] = "The User has been marked as deleted. This soft delete prevents orphaned resources."
		redirect_to users_path
	end

	def purge
		if User.current == @user
			flash[:error] = "It would be a very bad idea if you deleted yourself permanently."
		else
			@user.destroy
			@user.remove_unix_account
		end
		redirect_to users_path
	end
	
  def forgot_password
    if request.post?
      u = User.find_by_email(params[:user][:email])
      if u and u.send_new_password
        flash[:notice]  = "A new password has been sent by email."
        redirect_to login_path
      else
        flash[:error]  = "Sorry, I couldn't send a new password. Please make sure your email is correct."
      end
    end
  end

	# There's no page here to update or destroy a user.	If you add those, be
	# smart -- make sure you check that the visitor is authorized to do so, that they
	# supply their old password along with a new one to update it, etc.

protected

	def find_user
		@user = User.find(params[:id])
	end

	def update_roles(user)
		return false if !User.current.is_admin?
		
		if params[:user] && !params[:user][:role].blank?
			existing_roles = RoleUser.find(:all, :conditions=>"user_id = #{user.id}").map{|r| r.role_id}
			role = Role.find(params[:user][:role])
			
			if !existing_roles.include?(role.id)
				user.roles << role
			end
		end
	end
	
=begin
	def establish_unix_account(user, password)
		return unless Platform.is_linux?
		
		require 'fileutils'
		require 'lib/password_generator'		
		
		username	= user.email
		
		if RAILS_ENV == 'development'
			username << "-dev"
		end
		
		#password	= PasswordGenerator.new.password
		domain		= (RAILS_ENV == 'production') ? "barn.xbat.org" : "dev.barn.xbat.org"
		#users_dir = "/apps/#{domain}/shared/users"
		users_dir = "/apps/#{domain}/shared/users"
		home_dir	= "#{users_dir}/#{username}"
		#home_dir = "/apps/#{domain}/users/#{user.email}"
		
		#FileUtils.mkdir_p users_dir # make sure this exists...
		FileUtils.mkdir_p home_dir	# make sure this also exists...

		logger.info ""
		logger.info "Adding UNIX user account for #{username} ..." 
		
		unless RAILS_ENV == 'test'
			logger.info result = `sudo ruby /apps/#{domain}/current/script/establish_unix_account.rb #{username} #{password} #{home_dir}`
		end
		
		logger.info "done."
		logger.info ""
						
		UserMailer.deliver_ftp_account_notification(user, password)
		logger.info ""
	end
	
	def remove_unix_account(user)
		return unless Platform.is_linux?
		username = user.email
		domain		= (RAILS_ENV == 'production') ? "barn.xbat.org" : "dev.barn.xbat.org"
		logger.info ""
		logger.info "Deleting UNIX user account for #{username} ..." 
		logger.info ""
		unless RAILS_ENV == 'test'
			logger.info result = `sudo ruby /apps/#{domain}/current/script/remove_unix_account.rb #{username}`
		end
		logger.info "done."
		logger.info ""
	end
	
=end
end
