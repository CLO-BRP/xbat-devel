# NOTE: The 'edit' action has been removed since it has not relevance in the context of the api

class RecordingsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:index, :show]
	before_filter :has_permission?
	
	#skip_before_filter :verify_authenticity_token
	
	# Example standard URIs
	# GET /recordings
	# GET /recordings.(html|xml|js|json)
	# GET /recordings/{1}
	# GET /recordings/{1-10, 15}
	# GET /recordings.xml?public=1&page=1&order=name

	# Example Nested URIs
	# GET /users/1/recordings
	# GET /users/{1,3,5-10}/recordings
	
	def index	
		user = (logged_in? && params[:public].blank?) ? User.current : nil
		
		# Recordings and metadata
 		results = Recording.paginate_by_user_with_metadata( user, params )

		@recordings, @metadata, @tags = results[:collection], results[:metadata], results[:tags]

		if params.include? :sound_id
			locals = {
				:sound => Sound.find(params[:sound_id])
			}
		else
			locals = {}
		end
		
		response_for(@recordings, {:metadata => @metadata, :locals => locals}.merge(params))
	end


	# GET /recordings/1

	
	def show
		@recording = Recording.find(params[:id])
		
		# see application.rb for response_for_resource
		response_for( @recording, params )
	end


	# GET /recordings/new
	
	def new
		logger.debug params[:sound_id].blank?
		if params[:sound_id].blank?
			flash[:notice] = "For the moment, lets only upload recordings to a sound. Please create one - or <a href='/home/dashboard'>pick an existing one.</a>."
			redirect_to new_sound_path and return
		end
		
		@recording = Recording.new
		@sound = Sound.find params[:sound_id]
		response_for( @recording, params )
	end

	
	# POST /recordings
	# curl -X POST -H "Authorization:YWRtaW46cGFzc3dvcmQ=" -F Filedata=@/music.aif http://localhost:3000/sounds/{sound_id}/recordings
	
=begin
		http://www.beckshome.com/2007/07/19/AttachmentFuOnWindows.aspx
		
		When running Attachment_Fu on Windows, 
		the most commonly accounted problem is the “Size Is Not Included In List” validation error. 

		UPDATE: see also
		http://epirsch.blogspot.com/2008/01/fixing-attachmentfu-on-windows-like.html
		http://thewebfellas.com/blog/2008/11/2/goodbye-attachment_fu-hello-paperclip
=end	

	def create
		
		if params[:Filedata].blank? and params[:filedata].blank?
			flash[:notice] = 'Creating a recording without data is meaningless'
			respond_to do |format|
				# TODO: these are inappropriate responses
				format.html { redirect_to(@recording) }
				format.xml { render :xml => @recording, :status => :created, :location => @recording }
				format.all { render :text => @recording }
			end
			return
		end
		
		#params[:Filedata].content_type = File.mime_type?(params[:Filedata].original_filename)
		filedata = params[:Filedata] || params[:filedata]
		@recording = Recording.new :swf_uploaded_data => filedata
		
		# See the comment above this method!
		# NOTE: This is extraordinarily unreliable since the larger the file, the larger the wait time to get valid data
		# sleep 5 if RUBY_PLATFORM =~ /mswin/ 
		
		# we are using our own field names here
		@recording.file	= @recording.filename
		name = @recording.filename.split(".")
		name.pop
		@recording.name	=	name
		# it might be possible to avoid doing this by putting the sleep function inside attachement_fu
		@recording.bytes = File.size(@recording.temp_path)
		
		# Save the recording and associate it to the proper sound
		@recording.save!
		
		# We cannot get the location without saving it first
		@recording.location = "http://" << local_ip << @recording.public_filename
		@recording.save!

		@sound = Sound.find(params[:sound_id])
		@sound.recordings << @recording
		
		respond_to do |format|
			format.html{ render :text => @recording.public_filename }
			format.xml { render :nothing => true, :status => :success	}
			# Windows should really not be running this server
			# this is essentially a catch for Windows (Firefox at least...) where the response mime-type is 
			# being completely cleared - most likely by Flash
			format.all { render :text => @recording.public_filename }
		end		
		 
	end

	
	# PUT /recordings/1
	
	def update
		@recording = Recording.find(params[:id])

		respond_to do |format|
			if @recording.update_attributes(params[:recording])
				format.xml { head :ok }
				format.js	{ head :ok }
			else
				format.xml { render :xml => @recording.errors, :status => :unprocessable_entity }
				format.js { render :json => @recording.errors, :status => :unprocessable_entity }
			end
		end
	end

	
	# DELETE /recording/1
	# DELETE /recordings/{1}
	# DELETE /recordings/{1-10, 15}
	
	# TODO: need to write tests for this!
	def destroy
		@recordings = Recording.in_set(params[:id])
		
		@recordings.each do |recording|
			begin
				recording.destroy
			rescue Exception => e
				logger.error e
			end
		end
		
		respond_to do |format|
			format.html{ 
				flash[:notice] = "Your recordings have been deleted."
				redirect_to recordings_url
			}
			format.xml { head :ok }
			format.js	{ head :ok }
		end
	end
	
end
