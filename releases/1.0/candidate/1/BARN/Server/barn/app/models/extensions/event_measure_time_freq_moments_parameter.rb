class EventMeasureTimeFreqMomentsParameter < ActiveRecord::Base
  set_table_name "event_measure__time_freq_moments__parameter"
  
  has_many :event_measure_time_freq_moments, :foreign_key => :parameter_id, :class_name => "EventMeasureTimeFreqMoments"
  
  alias_method :values, :event_measure_time_freq_moments
end
