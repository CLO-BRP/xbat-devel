module FileResourceKeymaster
	
  module ClassMethods
  	def indexable_by?(user, parents = nil)
  		# If requested through a parent(s), check the parents readability
  		#(parents && parents_readable_by?(user, parents)) ? true : false
		true
    end

  	def creatable_by?(user, parent = nil)
  		true
    end
  end

  def readable_by?(user, parents = nil)
		true
  end

  def updatable_by?(user, parent = nil)
		true
  end

  def deletable_by?(user, parent = nil)
		false
  end
end