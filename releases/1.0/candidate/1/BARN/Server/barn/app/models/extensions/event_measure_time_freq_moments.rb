class EventMeasureTimeFreqMoments < ActiveRecord::Base
  set_table_name "event_measure__time_freq_moments__value"
  
  belongs_to :event
  belongs_to :event_measure_time_freq_moments_parameter, :foreign_key => :parameter_id
  
  alias_method :parameter, :event_measure_time_freq_moments_parameter
end
