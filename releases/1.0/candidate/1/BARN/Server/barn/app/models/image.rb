=begin
  ImageScience is currently the image library of choice.
  http://blog.eizesus.com/2008/11/attachmentfu-and-minimagick-resize-bug.html

  There is a resize bug when using attachment_fu and minimagick, so unfortunately we will not use MiniMagick
  Another alternative would be to use RMagick, but historically this installation has been an extreme challenge
  
  To install ImageScience on Windows, this article may help
  http://thewebfellas.com/blog/2008/2/18/imagescience-on-windows-without-the-pain
=end

class Image < ActiveRecord::Base
	include AccessControlHelper
		
	belongs_to :user
	#has_many :attachments, :as => :asset, :dependent => :destroy

	has_many :project_assets, :as => :asset#, :dependent => :destroy
	has_many :projects, :through => :project_assets#, :source => :attachable, :source_type => "Project", :class_name => "Project"  

	# attachment_fu
	has_attachment :content_type => :image, 
								 :path_prefix => 'public/files/uploaded/image',
                 :storage => :file_system, 
                 :max_size => 1.megabytes,
                 :size => 1..5.megabyte,
                 :thumbnails => { 
									:small  => '128',
									:medium => '256',
									:large  => '512'
								 },
								 :processor => 'ImageScience'
    
	validates_as_attachment

	before_save GUIDGenerator.new
	before_save {|record| record.user = User.current }
		    
    # this method is to make sure that only the owner of the parent object OR the user who uploaded the image can delete it
    def user_can_delete?(user)
      asset = asset_type.classify.constantize.send("find", asset_id) if asset_id
      if (asset and asset.user_id and asset.user_id == user.id) or self.user_id == user.id
        return true
      end
      return false
    end
end