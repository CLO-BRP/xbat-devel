class Peer < ActiveRecord::Base
	include WillPaginateHelper
	include NamedScopeHelper
	
	acts_as_rateable
	acts_as_taggable
	acts_as_annotated
	
	has_many :peer_users
	has_many :users, :through => :peer_users
  belongs_to :user
  
  # We should track at least who created the peer
  validates_presence_of :user_id, :guid
  
  def after_create
    # Add an entry to the peer_users table as well.
    # This will simplify searches (?) so that:
    # Peer.find(:first).users 
    # => [#<User>]
    PeerUser.create(:user_id => user_id, :peer_id => id)
	end
	
  before_validation GUIDGenerator.new
  
  def owner
    user
  end
end