module ProjectKeymaster
  
  module ClassMethods
    # Anyone can see a list of projects
  	def indexable_by?(user, parents = nil)
  		true
    end

  	# Only users can create projects
  	def creatable_by?(user, parent = nil)
  		(user)? true : false 
    end
  end
  
	# A project is readable by anyone if it is public, or by owners or members if not
  def readable_by?(user, parents = nil)
		#
		return true if self.public?
		return true if user && self.has_owner?(user)
		return true if user && self.has_member?(user)
		return false
  end

	# Only a user who is an admin or owner may update a project.
  def updatable_by?(user, parent = nil)
		if user && (self.has_owner?(user) || self.has_admin?(user))
			return true 
		end
		return false
  end

  def deletable_by?(user, parent = nil)
		if user && (self.has_owner?(user) || self.has_admin?(user))
			return true 
		end
		return false
  end
  
end