module FilesHelper
	
	def columns_default
        columns = super
        
		columns.concat ListingColumn.assemble(%w[file size created]) # score] # created modified]
	end
  
	def column_markup(row, column, file)
		
		case column.name
			
		when 'file'
			
			# NOTE: the directory is displayed as a header
			
			# NOTE: '.files-listing' means that the content will be loaded in the '#files-listing' container
			
			# TODO: the 'heading_selector' part is not working
			
			if file.directory?
				link_to(
					"<h3 style='border: none; margin-bottom: 0em; padding-bottom: 0em;'>#{file.basename}</h3>",
					"/files?partial=files/listing&dirname=#{u params[:dirname]}/#{u file.basename}&heading_selector=#files-header",
					:class => 'render files-listing'
				)
			else
				file.basename
			end
		
		when 'size'
			size_to_bytes file.size
			
		else
			super
			
		end
		
  end
	
	def column_sorting(column)
		
		case column.name
		
		when 'file'
			{:order => 'name', :direction => toggle_sort_direction("name")}
		
		else
			super
		
		end
	
	end

	def sub_menu
		element = [];
			
		case params[:action]
		
		when 'show'
			
		else
			element.push({:title => "Upload", :path => "files/new"}) 	
		end
		
	end
	
end
