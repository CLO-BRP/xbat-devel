class TagSet < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	

	acts_as_rateable
	
	acts_as_taggable
	
	acts_as_annotated
	
	
	logical_parents :project, :user
		
	before_save GUIDGenerator.new
	
	# TODO: this is where we establish the relations of the tag-sets to parent contexts
	
	# NOTE: the corresponsing models indicate 'has_* :tag_sets, :as => :context'
	
	belongs_to :project, :polymorphic => true
	
	belongs_to :user, :polymorphic => true
	

	# TODO: consider some conditions on these values
	
	validates_presence_of :prefix
	
	validates_presence_of :tags
	
	validates_presence_of :context_id
	
	validates_presence_of :context_type
end
