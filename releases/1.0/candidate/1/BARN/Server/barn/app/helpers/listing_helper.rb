module ListingHelper

end

class ListingColumn
  attr_accessor :name, :display, :width, :header

  def initialize(name,  display, width, header )
    @name = name
    @display = display.nil? ? true : display
    @width = width.nil? ? '' : width
    @header = header.nil? ? true : header
  end

  def self.assemble( columns )
     out = Array.new()

     #WARNING subsequent arrays must be carefully concatenated ( concat, not + ) so the original object is preserved for this to work
     def out.byname(colname);
        return select { |v| v.name == colname}[0];
     end

     columns.each { |val| name = val[:name].nil? ? val : val[:name]; out.push(ListingColumn.new( name, val[:display], val[:width],val[:header] ))}
     return out
  end

end