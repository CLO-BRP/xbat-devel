class Task < ActiveRecord::Base
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	before_save GUIDGenerator.new
	
	def before_create
		self.created_at = Time.now
	end
	
end
