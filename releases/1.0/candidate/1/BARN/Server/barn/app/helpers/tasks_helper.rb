module TasksHelper

	def columns_default

		columns = super

		if logged_in?
			columns.concat ListingColumn.assemble(%w[name priority progress started_at])
		else
			columns.concat ListingColumn.assemble(%w[name priority progress started_at])
		end
	end

	def column_markup(row, column, task)

		case column.name

		when 'started_at'
			task.started_at.nil? ? "Not started yet" : "Started #{time_ago_in_words(task.started_at)} ago."

		when 'progress'
			task.progress.nil? ? "No progress." : progress_display(task.progress)

		else
			super

		end
	end

  def column_sorting (column)
	params = {}

	case column.name 

	when 'log'
#		params = {:order => 'log.name', :direction => toggle_sort_direction('log.name')}
	else
		return super
	end

	return params
  end

  def progress_display ( progress )
    #TODO this could also use color to give an indication of 'doneness' at a glance

    "<div class='progress' style='width:100px;'><div class='progress_complete' style='width:#{progress}px;'><span class='progress_info'>#{progress} %</span></div></div>"
  end
  
  def sub_menu
	  
	  # TODO: what is the difference between 'path' and 'address'
	  
	  #return [
		#  {:title => "New Task", :path => "tasks/new"}
	  #]
  end	
  
end