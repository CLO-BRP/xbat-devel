# NOTE: this is necessary because somehow the namespace is getting polluted with Rake::Task

require 'task'

class TasksController < ApplicationController
	
	#include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	
	def index
		
    	user_id = (logged_in? && params[:public].blank?) ? User.current.id : 0

		@tasks = Task.find(:all, :conditions => { :user_id => user_id, :parent_id => 0 }, :order => "created_at" ).paginate(:page => params[:page] || 1, :per_page => params[:per_page] || 10)
        
		@metadata = {:total_entries => Task.count}

		response_for(@tasks, {:metadata => @metadata}.merge(params))
	end            

    def children
      @tasks = Task.find(:all, :conditions => {:user_id => user_id, :parent_id => :params[:id] }, :order => "created_at" ).paginate(:page => params[:page] || 1, :per_page => params[:per_page] || 10)

      @metadata = {:total_entries => Task.count}
      response_for(@tasks, {:metadata => @metadata}.merge(params))
    end

	def create
		
		#--
		# get objects to operate on from request
		#--

		ids = []

		if params[:id]
			ids = params[:id].split(",")

		elsif params[:task_target]
			ids = params[:task_target].split(",")

        end

		#--
		# set default null task description
		#--

		# TODO: why do we have this at all?

        @name = "Null task"

        @description = "Null task, nothing to do."

        @notice = "Null task Created"

		#--
		# build task description based on task action
		#--

		# TODO: consider renaming task_action to something else 'command', 'cmd', ...

		@xml = Builder::XmlMarkup.new

        @xml.comment! "M2XML"

        case params[:task_action]

		when "import_file"

			@xml.task(:class=>'struct', :size=> '1 1') do
				@xml.type(params[:type], :class=>'char', :size=> '1 1')

				@xml.action( params[:task_action], :class=>'char', :size=> '1 1')

				@xml.id(ids.join(" "), :class=>'double', :size=>"1 #{ids.size}")
			end

			@notice = "A task has been created to import files: #{ids.join(', ')}"

		when "scan_sound"

			#--
			# setup
			#--

			# NOTE: get extension and corresponding preset object

			extension = Extension.find(params[:extension][:id])

			preset = ExtensionPreset.find(params[:preset][:id])

			# NOTE: translate local ids into portable ids

			port_extension_id = extension.guid

			port_preset_id = preset.guid

			port_sound_ids = Sound.get_portable_id_hash(ids).values

			#--
			# build task description
			#--

			@name = "Sound Scan"

			@description = "Scanning selected sounds with detector %s, preset %s" % [extension.name, preset.name]

			@xml.task(:class=>'struct', :size=> '1 1') do
				@xml.action( params[:task_action], :class=>'char', :size=> '1 1')

				@xml.detector( port_extension_id, :class=>'char', :size=> '1 1')

				@xml.preset( port_preset_id, :class=>'char', :size=>'1 1')

				@xml.sound( port_sound_ids.join(" "), :class=>'char', :size=>"1 #{port_sound_ids.size}")
			end

			@notice = "A task has been created to scan sounds: #{port_sound_ids.join(', ')}"

		end

		# TODO: something is fishy, 'created_at' is not automatically set

		@task = Task.create(
			:name => @name,
			:description => @description,
			:task => @xml.target!,
			:parent_id => 0,
			:priority => 1,
			:user_id => User.current.id,
			:created_at => Time.now
		)
		
		flash[:notice] = @notice
		
		redirect_to "/#{params[:redirect]}" || '/'
	end
	
	def update
		
	end
	
	def destroy
	
	end
	
end