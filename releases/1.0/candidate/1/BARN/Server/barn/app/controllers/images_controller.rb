class ImagesController < ApplicationController
  include AuthorizationApocalypse::Gatekeeper
  
  before_filter :login_required
  before_filter :has_permission?
	
  # DELETE /images/1
  def destroy
    @image = Image.in_set(params[:id])
    # make sure you can delete this image
    if parent_resource_class and (parent_resource_class.user_id == User.current.id) or @image.user_id == @image.user_id
      @image.destroy
      respond_to do |format|
        format.html{
          redirect_to :back
        }
      end
      return
    end
    
    flash[:error] = "You cannot delete this image"
    respond_to do |format|
      format.html{
        redirect_to :back
      }
    end
  end
  
end
