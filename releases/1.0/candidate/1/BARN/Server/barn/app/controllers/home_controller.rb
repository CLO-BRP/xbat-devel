class HomeController < ApplicationController
	
  # before_filter :login_required
  
  # GET /users
  # GET /users.xml
  def index
		if logged_in?
			redirect_to :action => 'resources'
		end
  end
   
  def resources
  end
  
  # def portal
  #   render :layout => 'portal', :template => 'home/home'
  # end
  
  def dashboard
    @sounds = Sound.find(:all, :conditions => "user_id = #{User.current.id}")
    @recordings = Recording.find(:all)
  end
 
end