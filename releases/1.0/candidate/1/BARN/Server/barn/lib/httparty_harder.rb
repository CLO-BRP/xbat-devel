module HTTPartyHarder
  
  class Callbacks < Hash
    def initialize(params = {}) 
      self[:complete] = lambda{|response|}
      self[:error]   = lambda{|error|}
    end
  end
  
  def base_uri
    # this should be overwritten
    "http://localhost:80"
  end
  
  def get(address = '/', callbacks = HTTPartyHarder::Callbacks.new)	  
    begin
      response = self.class.get( File.join(base_uri, address) )
      callbacks[:complete].call(response)
      response
      
    rescue SocketError, Errno::ECONNREFUSED => error
      callbacks[:error].call(error)
      nil
    end
  end
  
end