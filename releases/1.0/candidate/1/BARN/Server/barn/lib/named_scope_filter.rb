# --------------
# NamedScopeFilter encapsulates the filter query scope and its attendant helper functions
# --------------

module NamedScopeFilter

	def self.included(base)

		base.extend(ClassMethods)

		base.class_eval do

			named_scope :filtered_by, lambda { |*args|

				params = args.first || {}

				query = params[:query]

				return {} if query.blank?

				tokens = raw_to_tokens( query )

				conditions = assemble_conditions( tokens, table_name )

				joins = assemble_joins( tokens, conditions, table_name )

				#--
				# return scope hash
				#--

				{
					:joins => joins.join(" "),
				}

			}

		end

	end

	module ClassMethods


		def raw_to_tokens( raw_input )

			# This accounts for a comma separated list coming from the tags input autocompleter,
			# in effect, ignoring all the previous entries - but could

			return raw_input.split(',').each {|part| part.strip!};

		end

		def key_filterable?( key )
			# define filterable keys in model
			# filterable_keys = %w[ channels samplerate duration ]
			return filterable_keys.include?( key )
		end

		def key_reserved?( key )
			return ( key == 'tag:' || key == 'name:' || key_filterable?( key.chop ) )
		end

		def assemble_conditions( tokens, table_name )

			conditions = ""

			tokens.each do |token|

				#TODO this common work should be factored out at an earlier stage so we are passing a hash of tokens
				#TODO also, must resolve when : is removed from keys in a consistent way
				key, modifier, value = extract_key_value( token )

				# reserved keys include
				# name: and tag:

				if key != nil
					modified_key = key.chop!

					if key_filterable?( modified_key )
						if conditions != ""
							conditions += " AND "
						end
						conditions += " #{table_name}.#{modified_key} #{modifier} '#{value}' "
					end
				end

				return conditions
			end

		end

		def assemble_joins( tokens, conditions, table_name )

			# first order parsing

			# reserved keywords
			# like: - returns items with tag or name like search term inclusive
			# tag: - returns items with tag
			# name: - returns items with name

			# nouns and modifiers
			# target - exact match
			# *target - match with wild prefix
			# target* - match with wild suffix
			# *target* - match 'like'

			clauses = []

			# TODO: this is insufficient - must be unique across all possible chainings
			unique_id = 0


			#preassemble tokens for conditions



			tokens.each do |token|


				# reserved tag:value
				# property:value
				# other:value


				#TODO refactor key handling to simplify key class ( reserved, property, or other) and chop in only one place
				key, modifier, value = extract_key_value( token )

				if key != nil
					if ( !(key_reserved?( key )) )
						value = key + value
						key = nil
					end
				end

				target = render_target( value )


				clause = ""


				if !column_names.include?('name')
					key = 'tag:'
				end

				case key

					when nil

						and_conditions = ""

						if conditions != ""
							and_conditions =  "AND (#{conditions})"
						end

						clause = "
							JOIN (
								SELECT DISTINCT #{table_name}.id AS id FROM #{table_name}
									LEFT JOIN tagging ON tagging.taggable_id = #{table_name}.id
									LEFT JOIN tag ON tagging.tag_id = tag.id
									WHERE (#{table_name}.name LIKE '#{target}' OR tag.name LIKE '#{target}')
									#{and_conditions}
									AND ( tagging.taggable_type = '#{table_name}' OR tagging.taggable_type IS NULL )
								) AS cl_#{unique_id}
							ON cl_#{unique_id}.id = #{table_name}.id
						"

					when "tag:"

						clause = "
							JOIN (
								SELECT DISTINCT tagging.taggable_id FROM `tagging`
									JOIN tag ON tagging.tag_id = tag.id
									WHERE tag.name LIKE '#{target}'
									AND tagging.taggable_type = '#{table_name}'
								) AS cl_#{unique_id}
							ON cl_#{unique_id}.taggable_id = #{table_name}.id
						"

					when "name:"

						clause = "
							JOIN (
								SELECT DISTINCT #{table_name}.id FROM #{table_name}
									WHERE #{table_name}.name LIKE '#{target}'
								) AS cl_#{unique_id}
							ON cl_#{unique_id}.id = #{table_name}.id
						"

				end

				clauses.push( clause )

				unique_id += 1
			end


			#TODO this will be redundant if there are already tokens without keys in the mix
			if ( conditions != "" )

				clause = "
					JOIN (
						SELECT DISTINCT #{table_name}.id AS id FROM #{table_name}
						WHERE #{conditions}
					) AS cl_#{unique_id}
					ON cl_#{unique_id}.id = #{table_name}.id
				"

				clauses.push( clause )
			end


			return clauses

		end

		def extract_key_value( token )

			#keyword: <=> value
			# includes modifier variants such as => <> <=

			key, modifier, value = token.scan(/([A-Za-z0-9-_]+:)?\s*([<=>]{1,2})?\s*(\*?[A-Za-z0-9-_:]+\*?)/)[0]

			if modifier == "=="
				modifier = "="
			end

			return key, modifier, value

		end

		def render_target( value )

			# *value* -> '%value%'

			value.gsub("*", "%")

		end
		
	end

end