namespace :files do
	
	desc "Create or update a record in the database for each file in the user's directory"
	
	task :process => [:environment] do
		puts "Raking for files to process :: #{Time.now} :: #{RAILS_ROOT}"
		
		BarnFileSystem.update_user_data_files
	end
	
	
	desc "Set in_ftp to false for datafiles that are no longer in FTP."
	
	task :cleanup => [:environment] do
		puts "Cleaning up data files :: #{Time.now}"
		
		BarnFileSystem.cleanup_user_data_files
	end
	
	
	desc "Remove database records of files that have been deleted from FTP."
	
	task :remove => [:environment] do
		puts "Removing data files not in FTP :: #{Time.now}"
		
		BarnFileSystem.remove_user_data_files
	end
	
	
	desc "Copy users uploaded data files from FTP to BARN Share."
	
	task :copy_user => [:environment] do
		puts "Copying files from FTP :: #{Time.now}"
		
		BarnFileSystem.copy_user_data_files
	end
	
	
	desc "Copy users uploaded data files from FTP to BARN Share."
	
	task :copy_project => [:environment] do
		puts "Copying files from FTP :: #{Time.now}"
		
		BarnFileSystem.copy_project_data_files
	end
	
end