module AccessControlHelper
	
	def self.included(base)
		base.class_eval do

			named_scope :readable_by, lambda { |*args| 
				return {} if args.nil? || args.first.nil?
				user = (args.first.is_a?(User))? args.first : User.find(args.first)
				{
					:include => [:user], 
					:conditions => ["#{table_name}.user_id = ?", user.id ]
				} 
			}

			# #----------
			# # updateable_by
			# #----------
			# 
			#			 named_scope :updateable_by, lambda { |*args| 
			#				 user = (args.first.is_a?(User))? args.first : User.find(args.first)
			#				 {
			#					 :include => :user, 
			#					 :conditions => ["#{table_name}.user_id = ?", user.id ]
			#				 } 
			#			 }
		end
	end
	
	# Overwrite this in the child models as necessary
	def readable_by?(user)

		if !user.blank?
			if user.is_admin? || 
				(self.respond_to?(:user) && (self.user.id == user.id)) || 
				(self.respond_to?(:public) && !self.public.blank?)
				return true
			else
				return false
			end
		end
		
		if user.blank?
			if (self.respond_to?(:public) && !self.public.blank?)
				return true
			else
				return false
			end
		end
	end
	
	def writeable_by?(user)
		
		if user.is_admin? || (self.respond_to?(:user) && (self.user.id == user.id))
			return true
		else
			return false
		end
	end
	
	def shareable_by?(user)
		return false
	end
	
end