# Tag extensions allow us to further shape the behavior of the taggable entities
# it is intended to be used wherever acts_as_taggable is used in model-land

module ActiveRecord #:nodoc:
  module Acts #:nodoc:
    module Taggable #:nodoc:

		module InstanceMethods

			def tag_add=(value)
				@mode = :add
				value.split(",").each do |val|
					if !added_tags.include?(val.strip)
						added_tags << val.strip
						self.tag_list.add( val.strip )
					end
				end
			end

			def tag_list=(value)
				@mode = :list
				@tag_list = TagList.from(value)
				@difference = @tag_list.reject { |addedtag| tags.map(&:name).include?( addedtag ) }
				unless @difference.empty?
					@difference.each { |tag| added_tags << tag.strip }
				end
			end

#			def tag_list_rated
#				if @tag_list_rated.nil?
#					@tag_list_rated = ""
#				end
#
#				@tag_list_rated = " { "
#				taggings.each { |tagging|
#					@tag_list_rated +=  "{" + tagging.tag.name + "," + tagging.vote + "},"
#				}
#				@tag_list_rated += " } "
#
#			end

			def tag_rated=(value)
				@mode = :rating				
				args = value.split(",")
				val = args[0]
				rating = args[1]

				added_tags << val.strip
				self.tag_list.add( val.strip )

				Tagging.vote = rating
			end

			def added_tags
				if @added_tags.nil?
					@added_tags = []		
				end

				@added_tags
			end



			def tagged_as?(value)
				self.tag_list.include?(value)
			end

			def tagged_as_count(value)
				self.tags.select{ |tag| tag.to_s == value }.length
			end

			def tagged_as_for_user?(value, user)
				taggings.find(:all, :conditions => [ "user_id = ?", user.id ]).map { |tagging| tagging.tag.to_s }.include?( value )
			end

			def my_tags()
				taggings.find(:all, :conditions => [ "user_id = ?", user.id ] ).map(&:tag)
			end

			def other_tags()
				# this is an exclusive or set of tags created by other users versus my own
				taggings.find(:all, :conditions => [ "user_id <> ? && tag_id not in (?)", user.id, my_tags.map(&:id) ] ).map(&:tag)
			end

			def namespace_tags( namespace )
				taggings.find(:all).map(&:tag).uniq.select { |tag| get_namespace( tag ) == namespace }
			end

			def find_namespace( tags, namespace )
				output = []
				tags.each do |tag|
					if (get_namespace( tag ) == namespace)
						return true
					end
				end
				false
			end

			def get_namespace( tag )
				set = tag.to_s.split(':')
				namespace = set[0..set.length-2].join(":")
			end


			#overrides default in acts_as_taggable
			def save_tags
			  return unless @tag_list


			  #TEMP CODE - doesn't handle rating from within command line
			  #!!!

			  existing_tags = my_tags().map { |tag| tag.name }

			  tags_to_remove = existing_tags - @tag_list

			  drop_tags = my_tags().select { |tag| tags_to_remove.include? tag.to_s }


			if ( @mode == :rating )
				# a rated tag - there will be no deletion in this case, bc it came from the tag_rated accessor above
				# however, we will have to drop the selected tag so it can be reentered with the new score
				drop_tags = my_tags().select { |tag| added_tags.include? tag.to_s }
			else
			    Tagging.vote = 1
			end


#			  tags_to_update = added_tags
#
#			  drop_tags_ids = tags.select { |tag|
#				  tags_to_update.include? tag.to_s
#			  }

			  #remove the extraneous tags from the tag list
			  #tags_to_drop.each { |tag| tags.delete_at(tags.map(&:name).index(tag))}
			  #tags_to_update.each { |tag_name| tags.delete_if { |tag| tag.name == tag_name } }
				

			  #remove preexisting associations if tags are not in the list
			  self.class.transaction do

				if drop_tags.any?
				  taggings.find(:all, :conditions => ["tag_id IN (?) AND user_id = ?", drop_tags.map(&:id), User.current]).each(&:destroy)
				  taggings.reset
				end

				#if any new tags are introduced, add them here
				#new_tag_names.each do |new_tag_name|
				added_tags.each do |new_tag_name|
				  tags << Tag.find_or_create_with_like_by_name(new_tag_name)
				end

			  end

			  true
			end
			
		end
	
	end
  end
end