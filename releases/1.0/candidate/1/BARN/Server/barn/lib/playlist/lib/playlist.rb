require File.join(File.dirname(__FILE__), 'playlist_parser')
Dir.glob(File.join(File.dirname(__FILE__), 'parsers', '*.rb')).each {|f| require f }

class Playlist
  include Enumerable
  
  attr_accessor :parser, :entries
  
  def initialize(path, parser = M3uParser.new)
    @files  = []
    @parser = parser
    
    open(path) {|file| 
      @entries = parser.parse(file) 
    }
  end
  
  def each
    paths = @entries.collect {|entry| entry.path }
    paths.each {|path| yield path}
  end
end
