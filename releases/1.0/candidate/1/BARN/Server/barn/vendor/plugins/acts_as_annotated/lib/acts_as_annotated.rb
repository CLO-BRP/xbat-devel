module ActiveRecord #:nodoc:
	module Acts #:nodoc:
		module Annotated #:nodoc:
			def self.included(base)
				base.extend(ClassMethods)
			end
			
			module ClassMethods
				def acts_as_annotated
					has_many :notings, :as => :annotated, :dependent => :destroy, :include => :note
					has_many :notes, :through => :notings
					
					extend ActiveRecord::Acts::Annotated::SingletonMethods
					include ActiveRecord::Acts::Annotated::InstanceMethods
					
				end
			end
			
			module SingletonMethods

			end
			
			module InstanceMethods
				private

					def sanitize_params(params)
						params[:page]			= params[:page] || 1
						params[:order]		 = params[:order] || "note.id"
						params[:direction] = params[:direction] || "asc"
						params[:sort]			= "#{params[:order]} #{params[:direction]}"
						return params
					end
				
				public
				
				#-------- 
				# paginate_notes - convienice method
				#--------
				
				def paginate_notes(params = {})
					params = sanitize_params(params)
					
					self.notes.paginate(:page => params[:page], :conditions => ["user_id = ?", user.id], :order => params[:sort] )
				end
				
				def note_count_for_user(user)
					self.notes.count(:conditions => ["user_id = ?", user.id])
				end
				
				#-------- 
				# find_notes_by_user
				#--------
				
				def find_notes_by_user(user, params = {})
					# This indifferent access is really going to bite us in the ass.. do we assume params is a string keyed hash or a symbol keyed hash
					order			 = params[:order] || "id"
					direction	= params[:direction] || "asc"
					sort				= "#{order} #{direction}"
					
					self.notes.find(:conditions => ["user_id = ?", user.id], :order => sort )
				end				

				#-------- 
				# paginate_notes_by_user
				#--------
				
				def paginate_notes_by_user(user, params = {})
					params = sanitize_params(params)
					
					self.notes.paginate(:page => page, :conditions => ["user_id = ?", user.id], :order => sort )
				end				
				
				#-------- 
				# paginate_notes
				#--------
				
				def paginate_notes(params = {})
					params = sanitize_params(params[:params])
					user	 = params[:user]
					if user
						self.notes.paginate(:page => params[:page], :conditions => ["user_id = ?", user.id], :order => params[:sort] )
					else
						self.notes.paginate(:page => params[:page], :order => params[:sort] )
					end
				end
				
				#-------- 
				# add_notes_by_user - responds with and array of notes added - could be only one
				#--------
				
				def add_notes_by_user(params, user)
					return nil if params.blank? || user.blank?
					
					# setup
					params = [params] if !params.is_a?( Array )

					@notes	= []
					@added	= []
					@exists = []
					@errors = []
					
					# pack the notes and examine the integrity of what is coming in
					params.each do |param|
						if param.is_a?( Hash )
							note = Note.new(param)
							note.create_content_hash
							@notes << note
						elsif param.is_a?( Note )
							param.create_content_hash # this may have already been done, but should happen on creation...
							@notes << param
						else
							# stop. we dont want to go any further
							raise "To add notes to resource, params can only be Note objects or param Hashs of the form: {:title => 'title', :body => 'body'}"
						end
					end
					
					# validate the notes

					@notes.each do |note|
						existing_note = Note.find_by_content_hash( note.content_hash )	
						
						# save the note if it isnt already in the DB
						if existing_note.blank?
							# call the validations on the note
							if note.valid?
								note.save
							else
								logger.error "Invalid Note: #{note.errors.inspect}. It has been ignored."
								@errors << note
								# keep iterating, but go no further on this loop
								next 
							end
						end	
						
						if !existing_note.blank?
							note = existing_note
						end

						# Now make the associations to the object
						
						# first check to see if the association has already been made
						noting = Noting.find(:first, :conditions => [
							"note_id = ? AND annotated_type = ? AND annotated_id = ? AND user_id = ?", note.id, self.class.to_s, self.id, user.id ]
						)
						#raise annotation.inspect						
						# If this association is not already made, make it
						if noting.blank?
							ann = Noting.create({
								:note_id				=> note.id,
								:user_id				 => user.id,
								:annotated_type => self.class.to_s,
								:annotated_id	 => self.id,
							}) 
							
							note.notings << ann
						end	
						
						#Finally, lets store what we added (even if it already existed)
						@added << note
						#return @added
					end 
					# @notes.each
					
					return @added
				end
				 # def add_notes
				alias :add_note_by_user :add_notes_by_user

			end
			# InstanceMethods
		end # module Annotated 
	end # module Acts
end # module ActiveRecord
