class Tagging < ActiveRecord::Base #:nodoc:
  belongs_to :tag
  belongs_to :taggable, :polymorphic => true

  cattr_accessor :vote

  #TODO so, this is now entangled with the user class.  Not very nice, but we can attack it once everything else is sorted out
  before_save {|tagging|
	  tagging.user_id = User.current.id
	  tagging[:vote] = Tagging.vote
  }
  
  def after_destroy
    if Tag.destroy_unused
      if tag.taggings.count.zero?
        tag.destroy
      end
    end
  end
end
