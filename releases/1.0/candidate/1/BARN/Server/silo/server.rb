#------------------------------
# SETUP
#------------------------------

root = File.dirname(File.expand_path(__FILE__)); vendor_root = File.join(root, 'vendor')

# NOTE: unshift method is used here because we would like the local libraries to load before RubyGem libraries

$LOAD_PATH.unshift File.join(root, 'lib')
$LOAD_PATH.unshift File.join(root, 'lib', 'worm')
$LOAD_PATH.unshift File.join(vendor_root, 'platform', 'lib')

# Then require the libraries
# NOTE: at the moment these are currently system gem requirements, we will probably package these

require 'rubygems'
require 'uri'
require 'net/http'
require 'json'
require 'erb'
require 'sinatra'
require 'worm'
require 'silo_scout'
require 'silo'
require 'helpers'

#--
# initialize
#--

# Now lets create some instance variables (in what context for Sinatra?)

@local_ip = UDPSocket.open { |s| 
	s.connect('64.233.187.99', 1)
	s.addr.last 
}

@config = Silo::Configuration.new(File.join(root, 'config.json'), :local_ip => @local_ip)

@worm 	= Worm.new(File.join(root, @config['silo']['database']))

@types 	= ['audio', 'image']

@types.each do |type|
  @worm.execute "
    CREATE VIEW IF NOT EXISTS #{type}_asset AS
    SELECT * FROM asset JOIN #{type} ON asset.asset_id = #{type}.id ORDER BY asset.asset_id
  "
end

# and make these available in the 'options' local variable in Sinatra actions

set :config, @config
set :worm, @worm
set :types, @types
set :local_ip, @local_ip

#------------------------------
# SILO SCOUT
#------------------------------

# Thread.new do
#   scout = SiloScout.new(
#     "http://#{config['scout']['host']}/silo/#{config['silo']['guid']}/request",
#     "http://localhost:#{config['silo']['port']}",
#     config['scout']['resolution'].to_f
#   )
#   
#   scout.start
# end

#------------------------------
# SILO SERVER
#------------------------------

#--
# filters
#--

before do
	# make these variables available to all views
	@local_ip = options.local_ip
	@config 	= options.config
	@message 	= params[:message] || nil
end

#--
# routes
#--

get '/' do
	#head("#{params[:message]}<br/>This is your silo. <a href='configure'>configure here.</a>") << head('request') << disp(request)
	erb :index
end

get '/configure' do
	erb :configure
end

post '/configure' do
	config = options.config.load
	config['scout']['user'] 			= params[:user]
	config['scout']['password'] 	= params[:password]
	config['scout']['host'] 			= params[:server]
	config['scout']['port'] 			= params[:port] || 80
	config['scout']['resolution'] = params[:resolution] || 1
	
	config.save
	options.config = config
	
	redirect '/?message=Configuation saved.'
end

get '/register' do
	erb :register
end


post '/register' do
	message = register_with_barn(options.config)
	redirect "/?message=#{message}"
end

# NOTE: this finds across resources, using asset columns

get '/find' do		
	if query.empty?
		disp(options.worm.find_all('asset'), 'all assets')
	else
		disp(options.worm.find_by('asset', query.keys.first, query.values.first), 'search results')
	end
end

['/info/:type.:format', '/info/:type'].each do |path|
	get path do
		check_resource_type
		
		disp(options.worm.table_info(params[:type] + '_asset'), params[:type] + ' table info')
	end
end

get '/:type.:format' do
	check_resource_type
		
	table = params[:type] + '_asset'
	
	if query.empty?
		disp(options.worm.find_all(table), params[:type])
	else
		disp(options.worm.find_by(table, query.keys.first, query.values.first))
	end
end

get '/:type/:id.:format' do
	check_resource_type
	
	disp(options.worm.find(params[:type] + '_asset', params[:id]).first, "#{params[:type]} #{params[:id]}")
end