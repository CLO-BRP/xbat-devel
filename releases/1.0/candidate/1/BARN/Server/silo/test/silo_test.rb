$LOAD_PATH.unshift File.join(File.expand_path(File.dirname(__FILE__)), '..')

require 'rubygems'
require 'test/unit'
require 'rack/test'
require 'silo'

set :environment, :test

class MyAppTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Silo::Application
  end

  def test_my_default
    get '/'
    assert_equal 'Silo', last_response.body
  end

  # def test_with_params
  #   get '/meet', :name => 'Frank'
  #   assert_equal 'Hello Frank!', last_response.body
  # end

  # def test_with_rack_env
  #     get '/', {}, 'HTTP_USER_AGENT' => 'Songbird'
  #     assert_equal "You're using Songbird!", last_response.body
  #   end
end