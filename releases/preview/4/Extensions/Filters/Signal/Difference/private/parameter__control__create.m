function control = parameter__control__create(parameter, context)

% DIFFERENCE - parameter__control__create

%--
% get parent controls
%--

fun = parent_fun; control = fun(parameter, context);

%--
% add order control
%--

control(end + 1) = control_create( ...
	'name', 'order', ...
	'style', 'slider', ...
	'type', 'integer', ...
	'slider_inc', [1, 2], ...
	'min', 1, ...
	'max', 5, ...
	'value', parameter.order, ... 
	'space', 1.5 ...
);