function control = parameter__control__create(parameter,context)

% HIGHPASS - parameter__control__create

fun = parent_fun; control = fun(parameter, context);

[remove, ix] = has_control(control, 'max_freq'); 

if remove
	control(ix) = [];
end

%--
% change minimum minimum frequency
%--

[ignore, ix] = has_control(control, 'min_freq');

control(ix).min = parameter.transition;
