function parameter = parameter__create(context)

% HIGHPASS - parameter__create

fun = parent_fun; parameter = fun(context);

rate = get_sound_rate(context.sound); nyq = rate / 2;

parameter.min_freq = 0.5 * nyq;

parameter.max_freq = nyq;
