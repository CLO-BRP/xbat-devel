function parameter = parameter__create(context)

% RANK FILTER - parameter__create

%--
% get parent parameters
%--

fun = parent_fun;

parameter = fun(context);

%--
% add rank parameter
%--

parameter.rank = 1;