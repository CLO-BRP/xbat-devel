function control = parameter__control__create(parameter, context)

% TOP-HAT - parameter__control__create

control = empty(control_create);

%--
% add leading tophat types control
%--

[types,ix] = tophat_types;

control(end + 1) = control_create( ...
    'name', 'type', ...
    'alias', 'Top-hat', ...
	'style', 'popup', ...
	'space', 1.25, ...
	'string', types, ...
	'value', ix ...
);

control(end + 1) = control_create( ...
	'style', 'separator' ...
);

%--
% append parent controls
%--

fun = parent_fun;

par_control = fun(parameter, context);

control = [control, par_control];