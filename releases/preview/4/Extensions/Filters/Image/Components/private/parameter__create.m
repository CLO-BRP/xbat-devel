function parameter = parameter__create(context)

% COMPONENTS - parameter__create

%--
% binarization parameters
%--

parameter.edge_scale = 3;

parameter.edge_filter = '';

parameter.edge_percent = 8;

%--
% component parameters
%--

% NOTE: this value must be 4 or 8

parameter.neighbors = 8;

parameter.component_percent = 50;
