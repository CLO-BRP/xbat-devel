function [handles, context] = on__play__start(widget, data, parameter, context)

% SPECTRUM - on__play__start

handles = [];

%--
% create play line in spectrum axes
%--

ax = spectrum_axes(widget);

% NOTE: get sound player display options

player = sound_player;

play_line(ax, ...
	'color', player.color ...
);