function scope = scope_line(ax, varargin)

scope = create_line(ax, 'scope_line', varargin{:});