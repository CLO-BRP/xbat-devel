function [handles, context] = on__play__update(widget, data, parameter, context)

% SCOPE - on__play__update

handles = [];

%--
% update axes time display
%--

ax = scope_axes(widget);

current = get_play_time(context.par); 

set(get(ax, 'title'), 'string', sec_to_clock(current));

%--
% get scope data
%--

% NOTE: return if we can't get data to display

if isempty(data.buffer)
	return;
end

% NOTE: the step seems like a parameter

step = 0.01; rate = get_sound_rate(context.sound); n = floor(step * rate);

time = linspace(current, current + step, n);

buffer = data.buffer;

try
	samples = buffer.samples(buffer.ix:(buffer.ix + n - 1));
catch
	samples = zeros(n, 1);
end

%--
% update display
%--

% NOTE: consider only updating 'ydata' here, and no update on axes

scope_line(ax, ...
	'xdata', time, ...
	'ydata', samples ...
);

% NOTE: automatic scaling could also be a parameter

scope_axes(widget, ...
	'xlim', [current, current + step], ...
	'ylim', [-1, 1] ...
);
