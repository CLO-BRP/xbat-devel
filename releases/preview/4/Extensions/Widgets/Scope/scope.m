function ext = scope

ext = extension_create;

ext.short_description = 'signal waveform scope display';

ext.category = {''};

ext.version = '0.1';

ext.author = 'Harold Figueroa';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

