function [handles, context] = on__page(widget, data, parameter, context)

% HISTOGRAM - on__page

%--
% get image data
%--

images = get_image_handles(context.par); 

X = [];

for k = 1:length(images)
	Xk = get(images(1), 'cdata'); X = [X; Xk(:)];
end

%--
% get and clear axes
%--

ax = findobj(widget, 'tag', 'histogram_axes'); cla(ax);

%--
% compute and display histogram
%--

[h, c] = hist_1d(X, 51);

handles = line( ...
	'parent', ax, ...
	'xdata', c, ...
	'ydata', h ...
);
