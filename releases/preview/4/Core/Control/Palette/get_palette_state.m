function state = get_palette_state(pal, data)

% get_palette_state - get position, toggle, and tab state
% -------------------------------------------------------
%
% state = get_palette_state(pal, data)
%
% Input:
% ------
%  pal - palette figure handle
%  data - palette userdata
%
% Output:
% -------
%  state - palette state

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3168 $
% $Date: 2006-01-11 16:54:20 -0500 (Wed, 11 Jan 2006) $
%--------------------------------

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

%--
% get palette userdata if needed
%--

if (nargin < 2) || isempty(data)
	data = get(pal, 'userdata');
end

%----------------------------------------------------
% GET NAME AND POSITION
%----------------------------------------------------

% NOTE: use absolute position, relative position causes problems

state.name = get(pal, 'name');

state.position = get(pal, 'position');

%----------------------------------------------------
% GET TOGGLE STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'toggle') && length(data.toggle)
	
	%--
	% loop over toggles
	%--
	
	for k = 1:length(data.toggle)
		
		tmp = data.toggle(k).toggle;
		
		%--
		% get toggle name
		%--
		
		state.toggle(k).name = get(get(tmp, 'parent'), 'tag');
		
		%--
		% get toggle state
		%--
		
		if strcmp(get(tmp, 'string'), '+')
			state.toggle(k).state = 'close';
		else
			state.toggle(k).state = 'open';
		end
		
	end
	
else
	
	%--
	% there are no toggles
	%--
	
	state.toggle = [];
	
end

%----------------------------------------------------
% GET TAB STATES
%----------------------------------------------------

% NOTE: this code depends on the way toggles are created in 'control_group'

if isfield(data, 'tabs') && length(data.tabs)
	
	%--
	% loop over tabs
	%--
	
	for k = 1:length(data.tabs)
		
		%--
		% get text children of tabs axes
		%--
				
		% NOTE: problems arise when we have elements with no name
		
		label = findobj(get(findobj(pal, 'tag', data.tabs(k).name), 'children'), 'type', 'text');
				
		%--
		% select darkest color label to be the active tab 
		%--
		
		% NOTE: get colors, convert to matrix, sum along columns, and get the minimum value index
		
		[ignore, ix] = min(sum(cell2mat(get(label, 'color')), 2));
				
		%--
		% store active tab name
		%--
		
		state.tabs(k).tab = get(label(ix), 'string');
		
	end
	
else
	
	%--
	% there are no tabs
	%--
	
	state.tabs = [];
	
end
