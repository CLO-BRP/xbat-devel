function unregister_palette(obj,event,fun)

% unregister_palette - remove palette handle from palette list
% ------------------------------------------------------------
%
% unregister_palette(obj,event,fun)
%
% Input:
% ------
%  obj - palette handle
%  event - reserved
%  fun - clean fun

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

% NOTE: function handle can be used as callback

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set default clean fun
%--

% NOTE: set empty fun to skip close

if (nargin < 3)
	fun = @closereq;
end

%---------------------------------
% UNREGISTER PALETTES
%---------------------------------

%--
% unregister sequence of palettes
%--

for k = 1:length(obj)

	%--
	% get parent information from palette
	%--

	% NOTE: this makes this function less efficient than register

	par = get_xbat_figs('child',obj(k));

	if (isempty(par))
		continue;
	end

	%--
	% unregister from parent
	%--

	% NOTE: assume palette has single parent
	
	data = get(par,'userdata');

	data.browser.palette.handle = setdiff(data.browser.palette.handle,obj(k));

	set(par,'userdata',data);

	%--
	% yield to fun
	%--
	
	if (~isempty(fun))
		fun();
	end

end

