function [fun,S] = window_to_fun(str,out)

% window_to_fun - get function for named window
% ---------------------------------------------
%
% [fun,S] = window_to_fun(str)
%
%   param = window_to_fun(str,'param')
%
% Input:
% ------
%  str - window name
%
% Output:
% -------
%  fun - function name or list of available functions
%  S - suggested menu separators

%--
% set output mode
%--

if ((nargin < 2) || isempty(out))
	out = 'name';
end

%--
% create window function table
%--

persistent WINDOW_FUN_TABLE;
	
if (isempty(WINDOW_FUN_TABLE))
	
	%--
	% create window function and parameters table
	%--
	
	WINDOW_FUN_TABLE = { ...
		'Bartlett', @bartlett, []; ...
		'Bartlett-Hanning', @barthannwin, []; ...
		'Blackman', @blackman, []; ...
		'Blackman-Harris', @blackmanharris, []; ...
		'Bohman', @bohmanwin, []; ...
		'Chebyshev', @chebwin, {'R','Sidelobe attenuation in dB',[100,500],100};
		'Flattop', @flattopwin, []; ...
		'Gaussian', @gausswin, {'Alpha','Reciprocal standard deviation',[0,5],2.5}; ...
		'Hamming', @hamming, []; ... 		
		'Hanning', @hann, []; ...
		'Kaiser', @kaiser, {'Beta','Bessel function parameter',[0,20],7}; ... 
		'Nutall', @nuttallwin, []; ...
		'Parzen', @parzenwin, []; 
		'Rectangular', @rectwin, []; ...
		'Triangular', @triang, []; ...
		'Tukey', @tukeywin, {'R','Hanning-Rectangular mix coefficient',[0,1],0.5} ...
	};

	%--
	% convert table parameter representation to structure
	%--

	% NOTE: why is this done this way ???
	
	for k = 1:size(WINDOW_FUN_TABLE,1)

		if (~isempty(WINDOW_FUN_TABLE{k,3}))

			tmp.name = WINDOW_FUN_TABLE{k,3}{1};
			tmp.tip = WINDOW_FUN_TABLE{k,3}{2};
			tmp.min = WINDOW_FUN_TABLE{k,3}{3}(1);
			tmp.max = WINDOW_FUN_TABLE{k,3}{3}(2);
			tmp.value = WINDOW_FUN_TABLE{k,3}{4};

			WINDOW_FUN_TABLE{k,3} = tmp;

		end

	end

end
		
%--
% look up function name or parameter description
%--

if (nargin)
	
	switch (out)
		
		%--
		% output window function handle
		%--
		
		case ('name')
			
			ix = find(strcmpi(str,WINDOW_FUN_TABLE(:,1)));
			
			if (~isempty(ix))
				fun = WINDOW_FUN_TABLE{ix,2};
			else
				fun = [];
			end
			
		%--
		% output window function parameter description
		%--
		
		case ('param')
			
			ix = find(strcmpi(str,WINDOW_FUN_TABLE(:,1)));
			
			if (~isempty(ix))
				fun = WINDOW_FUN_TABLE{ix,3};
			else
				fun = [];
			end
			
		%--
		% error
		%--
		
		otherwise, error(['Unrecognized output mode ''' out '''.']);
			
	end

%--
% output available window names
%--

else
	
	%--
	% output list of available windows
	%--
	
	fun = WINDOW_FUN_TABLE(:,1);
	
	%--
	% output menu separators
	%--
	
	% NOTE: the separators are not needed
	
	if (nargout > 1)
		n = length(fun); S = bin2str(zeros(1,n));
	end
	
end

