function out = sound_check_files(sound)

%--
% check that sound files exist in the same location
%--

if iscell(sound.file)
	
	%--
	% check all files in sound
	%--
	
	for k = 1:length(sound.file)		
		test(k) = exist([sound.path, sound.file{k}], 'file');
	end
	
else
	
	%--
	% check single sound file
	%--
		
	test = exist([sound.path, sound.file], 'file');
	
end

%--
% find the sound files and reassign if needed
%--

out = all(test == 2);

	
