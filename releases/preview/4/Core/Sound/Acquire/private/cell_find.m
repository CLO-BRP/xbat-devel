function [ixs] = cell_find(a, b, not)

% [ixs] = cell_find(a, b, not)
%
% return the indexes of *a* whose entries are contained in *b*
%
% *a* and *b* are cell arrays.  if NOT is specified, then the search is
% negative.


if nargin < 3
	
	not = [];
	
end

ixs = zeros(1, length(a));

for ii = 1:length(a)
	
	for jj = 1:length(b)
		
		
		eq = strcmp(num2str(a{ii}), num2str(b{jj}));
		
		flag = ~any(not) && eq || any(not) && ~eq;
		
		if eq
			
			ixs(ii) = 1;
			
		end
		
	end
	
end

if any(not)
	
	ixs = ~ixs;
	
end

ixs = find(ixs);
	
	