function X = get_data(player)

%--
% get sound, starting index and duration, and channels from player
%--

sound = player.sound;

ix = player.bix; n = min(player.buflen, (player.ix + player.n) - player.bix);

ch = player.ch;

%--
% get samples from source
%--
		
if isfield(sound, 'data')
	X = sound.data;
else
	X = sound_read(sound, 'samples', ix, n, ch);
end

%--
% filter samples
%--

if ~isempty(player.filter)
	X = apply_signal_filter(X, player.filter);
end

%--
% resample for player
%--

X = player_resample(X, player);
