function ext = get_write_ext(format)

% get_write_ext - get extensions we may write to
% ----------------------------------------------
%
% ext = get_write_ext(format)
%
% Input:
% ------
%  format - list of formats
%
% Output:
% -------
%  ext - extensions we may write to

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 563 $
% $Date: 2005-02-21 05:59:20 -0500 (Mon, 21 Feb 2005) $
%--------------------------------

%--
% get all formats if needed
%--

if (nargin < 1)
	format = get_formats;
end

%--
% get writeable extensions
%--

ext = cell(0);

for k = 1:length(format)
	if (~isempty(format(k).write))
		ext = {ext{:}, format(k).ext{:}};
	end
end

ext = ext';