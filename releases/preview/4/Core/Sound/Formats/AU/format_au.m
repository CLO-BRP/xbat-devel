function format = format_au

% format_au - create format structure
% -----------------------------------
%
% format = format_au
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% inherit format structure
%--

format = format_libsndfile;

%--
% fill name and extension fields of format structure (set)
%--

format.name = 'Audio File Format (AU)';

format.ext = {'au'};