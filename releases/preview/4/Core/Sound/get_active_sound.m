function [sound, lib] = get_active_sound(par, pal)

% get_active_sound - get the active sound
% ---------------------------------------
%
% [sound, lib] = get_active_sound(par, pal)
%
% Input:
% ------
%  par - browser handle (def:active browser)
%  pal - XBAT palette handle
%
% Output:
% -------
%  sound - sound
%  lib - parent library

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 5869 $
% $Date: 2006-07-28 12:32:05 -0400 (Fri, 28 Jul 2006) $
%--------------------------------

%--
% handle input
%--

lib = []; sound = [];

if nargin < 1 || isempty(par)
	par = get_active_browser;
end

if nargin < 2 || isempty(pal)
	pal = get_palette(0, 'XBAT');
end

%--
% get sound and library from open browser
%--

if ~isempty(par)
	
	sound = get_browser(par, 'sound'); info = parse_browser_tag(get(par, 'tag'));
	
	[ignore, libname] = strtok(info.library, filesep); libname = libname(2:end);   
	
	lib = get_libraries(get_users('name', info.user), 'name', libname);
	
	return;

end

%--
% otherwise, get sound from xbat palette if possible
%--

if isempty(pal)
	return;
end

%--
% get selected sounds in XBAT palette
%--

name = get_control(pal, 'Sounds', 'value')

if numel(name) > 1 || numel(name) == 0
	return;
end

lib = get_active_library;

sound = get_library_sounds(lib, 'name', name{1});




	
	
	