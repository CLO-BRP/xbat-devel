function sound = update_sound_attributes(sound, mode)

if nargin < 2
	mode = 'update';
end

attr = get_attributes(sound.path);

%--
% map attribute fields to sound fields
%--

% TODO: consider renaming to make this whole process dynamic

% NOTE: sound create should also do this dynamically

sound.realtime = attr.date_time;

sound.geometry = attr.sensor_geometry;

sound.speed = attr.sound_speed;

if strcmp(mode, 'create')
	sound.time_stamp = attr.time_stamps;
end

if (~isempty(attr.time_stamps))
	sound.time_stamp.table = attr.time_stamps.table;
end

if ~isfield(attr, 'sensor_calibration')
	sound.calibration = [];
else
	sound.calibration = attr.sensor_calibration;
end



