function flag = file_geometry(mode,h,in)

% file_geometry - handle geometry data file
% -----------------------------------------
% 
%     g = file_geometry('read',f)
%
%  flag = file_geometry('read',h,f)
%       = file_geometry('write',h,t)
%
% Input:
% ------
%  h - handle of figure modified
%  f - name of file to read
%  t - geometry data to write to file
%
% Output:
% -------
%  flag - execution success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set handle
%--

if ((nargin < 2) | isempty(h))
	h = gcf;
end

%--
% get userdata
%--

if (~strcmp(mode,'read'))
	data = get(h,'userdata');
end


%--
% read or write depending on mode
%--

switch (mode)
	
%--
% read geometry data from file
%--

case ('read')
	
	%--
	% rename input
	%--
	
	in = h;
	
	%--
	% try to read values from file
	%--
		
	try
		
		g = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Geometry Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
% 	nch = data.browser.sound.channels;
% 	
% 	if ((any(size(g) ~= [nch,3]) & any(size(g) ~= [nch,4])) | any(g(:,1) < 0) | any(round(g(:,1)) ~= g(:,1)))
			
	if (((size(g,2) ~= 3) & (size(g,2) ~= 4)) | any(g(:,1) < 0) | any(round(g(:,1)) ~= g(:,1)))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper geometry information.'], ...
			' XBAT Error  -  Geometry Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% output geometry
	%--
	
	flag = g(:,2:end);
	
	return;
	
%--
% read geometry into browser from file
%--

case ('load')
	
	%--
	% get file if needed
	%--
	
	if ((nargin < 3) | isempty(in))

		%--
		% go to figure sound directory
		%--
		
		pi = pwd;
		
		cd(data.browser.sound.path);
		
		%--
		% get file using dialog
		%--
		
		[in,p] = uigetfile( ...
			{'*.txt','Text Files (*.txt)'; '*.*','All Files (*.*)'}, ...
			'Select Geometry Data File: ' ...
		);  
		
		%--
		% return if file dialog was cancelled
		%--
		
		if (~in)
			
			flag = 0;
			cd(pi);	
			return;
			
		else
			
			in = [p,in];
			cd(pi);
			
		end

	end
	
	%--
	% try to read values from file
	%--
	
	try
		
		g = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Geometry Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	nch = data.browser.sound.channels;
	
	if ((any(size(g) ~= [nch,3]) & any(size(g) ~= [nch,4])) | any(g(:,1) < 0) | any(round(g(:,1)) ~= g(:,1)))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper geometry information.'], ...
			' XBAT Error  -  Geometry Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% update geometry in browser
	%--
	
	data.browser.sound.geometry = g(:,2:end);
	
	set(h,'userdata',data);
	
	%--
	% update geometry data file
	%--

	flag = file_geometry('write',h,g(:,2:end));
	
	if (~flag)
		
		tmp = error_dialog('Error updating geometry data file.', ...
			' XBAT Error  -  Geometry Data','modal');
		waitfor(tmp);
		
		return;
		
	end
		
%--
% write geometry to file
%--

case ('write')
		
	%--
	% set geometry values if needed
	%--
	
	if ((nargin < 3) | isempty(in))
		g = data.browser.sound.geometry;
	else
		g = in;
	end
	
	%--
	% get template string to write to file and add sound data
	%--
	
	str = file_to_str('file_geometry.txt');
	
	str = strrep(str,'$PATH',strrep(data.browser.sound.path,'\','\\'));
	
	if (~strcmp(data.browser.sound.type,'File'))
		str = strrep(str, ...
			'$FILE',[data.browser.sound.file{1} ' , ... , ' data.browser.sound.file{end}]);
	else
		str = strrep(str,'$FILE',data.browser.sound.file);
	end	
	
	%--
	% add geometry data to string
	%--
	
	nch = data.browser.sound.channels;
	
	if (size(g,2) == 2)
		for k = 1:nch
			str = [str, int2str(k) ', ' num2str(g(k,1)) ', ' num2str(g(k,2)) '\n'];
		end
	else
		for k = 1:nch
			str = [str, int2str(k) ', ' num2str(g(k,1)) ', ' num2str(g(k,2)) ', ' num2str(g(k,3)) '\n'];
		end
	end
	
	%--
	% write file
	%--

	if (~strcmp(data.browser.sound.type,'File'))
		pre = data.browser.sound.path;
		ix = findstr(pre,filesep);
		pre = pre(ix(end - 1):(end - 1));
	else
		pre = file_ext(data.browser.sound.file);
	end
	
	flag = str_to_file(str,[data.browser.sound.path pre '_Geometry.txt']);
	
	%--
	% update geometry related menus
	%--
	
	mg = data.browser.sound_menu.data(8)
	
	set(mg,'label','Geometry:  (Available)');
	
	delete(get(mg,'children'));
	
	if (size(g,2) == 2)
		L = cell(0);
		for k = 1:nch
			L{k} = ['Channel ' int2str(k) ':  [' int2str(k) ', ' num2str(g(k,1)) ', ' num2str(g(k,2)) '] m'];
		end
	else
		L = cell(0);
		for k = 1:nch
			L{k} = ['Channel ' int2str(k) ':  [' num2str(g(k,1)) ', ' num2str(g(k,2)) ', ' num2str(g(k,3)) '] m'];
		end
	end
	
	L{nch + 1} = 'Edit Geometry ...'; 
	L{nch + 2} = 'Import Geometry ...';
	
	n = length(L);

	S = bin2str(zeros(1,n));
	S{nch + 1} = 'on';
	
	tmp = ...
		menu_group(mg,'browser_view_menu',L,S);
	
	set(tmp(1:end - 2),'callback','');
	
end

%--
% set flag
%--

flag = 1;

	
	
	
	