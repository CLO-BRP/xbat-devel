function root = get_attributes_root(in)

% get_attributes_root - get attributes directory relative to input path
% ---------------------------------------------------------------------
%
% root = get_attributes(in)
% 
% Input:
% ------
%  in - input path
%
% Output:
% -------
%  root - attributes path

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 2201 $
% $Date: 2005-12-06 08:15:05 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

if in(end) == filesep
	in(end) = '';
end

root = [in, filesep, '__XBAT', filesep, 'Attributes'];