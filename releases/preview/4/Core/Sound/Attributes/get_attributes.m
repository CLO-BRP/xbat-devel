function attr = get_attributes(in,flag)

% get_attributes - get sound attributes from sound files
% ------------------------------------------------------
%
% attr = get_attributes(in,flag)
%
% Input:
% ------
%  in - parent directory
%  flag - attributes search flag
%
% Output:
% -------
%  attr - attributes struct

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 2201 $
% $Date: 2005-12-06 08:15:05 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

%------------------------------------------
% SETUP
%------------------------------------------

file_ext = '.csv'; % excel format (Comma-Separated Values)

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% create empty attributes struct
%--

attr = attributes_create;

%--
% set default attribute search flag and consider search
%--

if nargin < 2 || isempty(flag)
	flag = 1;
end

% NOTE: return empty attributes if no search

if ~flag
	return; 
end

%--
% check for attributes directory
%--

root = get_attributes_root(in);

if ~exist_dir(root)
	return;
end

%------------------------------------------
% LOAD AVAILABLE ATTRIBUTES
%------------------------------------------

attributes = get_attribute_fields;

for attribute = attributes
	
	attribute = attribute{1}; % un-cell
	
	%--
	% skip this attribute if no function exists to handle it
	%--
	
	parser = [attribute, '.m'];
	
	if isempty(which(parser))
		continue;
	end
	
	%--
	% apply parse function to get data from file
	%--
	
	fun = str2func(attribute);
	
	file = [root, filesep, attribute, file_ext];
	
	if (exist(file,'file'))
		attr.(attribute) = fun(file);
	end
	
end

	
%------------------------------------------
% ATTRIBUTES_CREATE
%------------------------------------------

function attr = attributes_create

fields = get_attribute_fields; values = cell(1,length(fields));

attr = cell2struct(values,fields,2);
