function types = sound_types

% sound_types - list of sound types
% ---------------------------------
%
% types = sound_types
%
% Output:
% -------
%  types - sound type strings

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2201 $
% $Date: 2005-12-06 08:15:05 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

types = { ...
	'file', ...
	'file stream', ...
	'stack', ...
	'variable', ...
	'recording' ...
};