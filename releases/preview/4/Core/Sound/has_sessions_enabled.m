function value = has_sessions_enabled(sound)

value = ~isempty(sound.time_stamp) && sound.time_stamp.enable;