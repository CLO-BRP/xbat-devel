function user = default_user

% default_user - defines and creates default user
% -----------------------------------------------
%
% user = default_user
%
% Output:
% -------
%  user - default user

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% create default user structure
%--

user = user_create( ...
	'id',0, ...
	'name','Default', ...
	'alias','Default' ...
);

%--
% add default user to file system
%--

user = add_user(user);