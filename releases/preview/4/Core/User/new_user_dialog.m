function user = new_user_dialog(user)

% new_user_dialog - dialog to create new users
% --------------------------------------------
%
% user = new_user_dialog(user)
%
% Input:
% ------
%  user - user to edit
%
% Output:
% -------
%  user - new user created

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1161 $
% $Date: 2005-07-05 16:25:08 -0400 (Tue, 05 Jul 2005) $
%--------------------------------

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set new user state
%--

if ~nargin
	new = 1;
else
	new = 0;
end

%----------------------------------
% CREATE CONTROLS
%----------------------------------

control = empty(control_create);

%-----------------
% INFO
%-----------------

if ~new
	str = ['User  (', user.name, ')'];
else
	str = 'User';
end

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', str ...
);

%--
% name
%--

% NOTE: the number of lines along with the dialog group options produce a square

control(end + 1) = control_create( ...
	'name', 'name', ...
	'space', 1, ...
	'onload', 1, ...
	'space', 0.75, ...
	'style', 'edit', ...
	'type', 'filename' ...
);

if ~new
	control(end).string = user.name; control(end).initialstate = '__DISABLE__';
end

% %--
% % organization
% %--
% 
% control(end + 1) = control_create( ...
% 	'name', 'organization', ...
% 	'space', 1, ...
% 	'style', 'edit' ...
% );
% 
% if ~new
% 	control(end).string = user.organization;
% end

%--
% email and url
%--

control(end + 1) = control_create( ...
	'name', 'email', ...
	'space', 0.75, ...
	'style', 'edit' ...
);

if ~new
	control(end).string = user.email;
end

control(end + 1) = control_create( ...
	'name', 'url', ...
	'alias', 'URL', ...
	'space', 0.75, ...
	'style', 'edit' ...
);

if ~new
	control(end).string = user.url;
end


control(end).space = 1.5;

%-----------------
% PREFERENCES
%-----------------

% control(end + 1) = control_create( ...
% 	'style', 'separator', ...
% 	'type', 'header', ...
% 	'min', 1, ...
% 	'string', 'Preferences' ...
% );
% 
% control(end + 1) = control_create( ...
% 	'style', 'checkbox', ...
% 	'name', 'developer' ...
% );
% 
% control(end + 1) = control_create( ...
% 	'style', 'checkbox', ...
% 	'name', 'palette_sounds' ...
% );
% 
% control(end + 1) = control_create( ...
% 	'style', 'checkbox', ...
% 	'name', 'palette_tooltips' ...
% );

%----------------------------------
% CREATE DIALOG
%----------------------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 12;

opt.header_color = get_extension_color('root');

opt.text_menu = 1;

%--
% create dialog
%--

if new
	name = 'New ...';
else 
	name = 'Edit ...';
end

out = dialog_group(name, control, opt, @new_user_callback);

% NOTE: return empty on cancel

if isempty(out.values)
	user = []; return;
end

%----------------------------------
% CREATE AND SUBSCRIBE USER
%----------------------------------

values = out.values;

%--
% create and add user
%--

if new
	
	user = struct_update(user_create, values);
	
	% NOTE: this creates the default user library and subscribes the user to it

	user = add_user(user);
	
else
	
	user = struct_update(user, values); 
	
	user_save(user);

end


%----------------------------------
% NEW_USER_CALLBACK
%----------------------------------

function new_user_callback(obj, eventdata)

[control, pal] = get_callback_context(obj);

switch control.name
	
	case 'name'
		set_control(pal.handle, 'OK', 'enable', proper_filename(get(obj, 'string')));
		
end
