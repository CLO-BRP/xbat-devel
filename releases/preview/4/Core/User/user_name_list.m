function names = user_name_list

% user_name_list - call array list of user names with 'Default' first
% -------------------------------------------------------------------
%
% names = user_name_list
%
% Output:
% -------
%  names - list of user names

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

names = struct_field(get_users, 'name');

%--
% put default on top
%--

ix = find(strcmp('Default', names));

if ~isempty(ix)
	names(ix) = []; names = {'Default', names{:}};
end