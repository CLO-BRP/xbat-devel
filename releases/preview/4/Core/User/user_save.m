function user = user_save(user)

% user_save - save user to file
% -----------------------------
%
% user = user_save(user)
%
% Input:
% ------
%  user - user structure
%
% Output:
% -------
%  user - saved user

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1631 $
% $Date: 2005-08-23 12:41:39 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

% TODO: update to update active user if needed

%--
% update modified date
%--

user.modified = now;

file = get_user_file(user);

%--
% save to file
%--

% NOTE: it is not clear which is a better error message, consider truly handling exception

try
	save(file, 'user'); 
catch
	error(['Failed to save user ''', user.name, '''.']);
end

	