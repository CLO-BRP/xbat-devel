function lib = set_active_library(lib, user)

% set_active_library - set the active library
% -------------------------------------------
%
%  lib = set_active_library(lib)
%
% Input:
% ------
%  lib - library to set active
%  user - the user for which to set 'lib' as the active library
%
% Output:
% -------
%  lib - the updated library

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------


%----------------------------
% HANDLE INPUT
%----------------------------

if nargin < 2 || isempty(user)
	user = get_active_user;
end

switch class(lib)
	
	case 'char', lib = get_libraries(user, 'name', lib);
		
	case 'cell', lib = get_libraries(user, 'name', lib{1});
		
	case 'struct'
		
		if ~isfield(user, 'name')
			user = [];
		end
		
	otherwise, lib = [];
		
end

if nargin < 1 || isempty(lib)	
	lib = get_active_library(user); 
end

%----------------------------
% SET ACTIVE LIBRARY
%----------------------------

%--
% set user's active library and store user if necessary
%--

libs = get_libraries(user);

ix = find(strcmp(lib.path, {libs.path}));

if ix ~= user.active
	user.active = ix; set_active_user(user);
end

%--
% update controls
%--

update_controls(lib);


%-----------------------------------------
% UPDATE CONTROLS
%-----------------------------------------

function update_controls(lib)

% update_controls - find and update all controls dealing with library
% -------------------------------------------------------------------

%-----------------------------------------------
% UPDATE XBAT PALETTE CONTROLS
%-----------------------------------------------

pal = xbat_palette;

if isempty(pal) 
	return;
end

if isempty(lib)
	return;
end

%--
% update 'Library' control
%--

handles = get_control(pal, 'Library', 'handles');

names = library_name_list; 

ix = find(strcmp(get_library_name(lib, get_active_user), names));

set(handles.obj, ...
	'string', names, ...
	'value', ix ...
);

%--
% disable unsubscribe for default library
%--

if length(names) < 2	
	set_control(pal, 'unsubscribe_user', 'enable', 'off');
else
	set_control(pal, 'unsubscribe_user', 'enable', 'on');
end

%--
% Update Sounds List
%--

set_control(pal, 'find_sounds', 'value', '');

xbat_palette('find_sounds');


