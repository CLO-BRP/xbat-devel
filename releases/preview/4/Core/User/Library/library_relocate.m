function [user, lib] = library_relocate(user, old_path, new_path, rec)

if nargin < 4 || isempty(rec)
	rec = 0;
end

%--
% duck a library and unsubscribe
%--

[root, name] = path_parts(old_path);

lib.name = name; lib.path = old_path; lib.author = user.name;

user_unsubscribe(lib, user);

user = get_users('name', user.name);

%--
% present question dialog. 
%--

if nargin < 3 || isempty(new_path) 
	
	new_path = [];
	
	str = {['Unable to find library.''' name ''', '], 'Would you like to find it?'};

	out = quest_dialog(double_space(str),' Missing Library','Cancel');

	if ~strcmp(out, 'Yes')
		lib = []; return;
	end	
	
end

while (1)

	%--
	% get new directory
	%--

	if isempty(new_path)
		new_path = uigetdir(pwd, 'Select location of missing library.');
	end

	[root, name] = path_parts(new_path); 

	%--
	% update library file
	%--
	
	file = get_library_file(root, name);
	
	% NOTE: keep old library path to remove from user library list
	
	lib = load_library(file); lib.path = [new_path, filesep]; 

	library_save(lib);
	
	%--
	% refresh library sound cache
	%--
	
	get_library_sounds(lib, 'refresh');

	if ~isempty(lib)
		break;
	end

end

user_subscribe(lib, user);

user = get_users('name', user.name);

%--
% do the rest
%--

if ~rec
	return;
end

other_users = lib.user(find(~strcmp(lib.user, user.name)));

for j = 1:length(other_users)

	this_user = get_users('name', other_users{j});

	library_relocate(this_user, old_path, new_path);

end