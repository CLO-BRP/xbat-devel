function str = tags_to_str(tags)

% tags_to_str - convert tags to string
% ------------------------------------
%
% str = tags_to_str(tags)
%
% Input:
% ------
%  tags - tags
%
% Output:
% -------
%  str - tag string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1953 $
% $Date: 2005-10-19 20:22:46 -0400 (Wed, 19 Oct 2005) $
%--------------------------------

%--
% check input for tags
%--

if ~is_tags(tags)
	error('Input is not proper tags.');
end

%--
% return empty string for empty tags
%--

if isempty(tags)
	str = ''; return;
end

%--
% create space separated tags string
%--

str = tags{1};

for k = 2:length(tags)
	str = [str, ' ', tags{k}];
end
