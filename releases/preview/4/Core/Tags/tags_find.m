function ix = tags_find(obj, tags, logic)

% tags_find - find objects based on tags
% --------------------------------------
%
% ix = tags_find(obj, tags, logic)
%
% Input:
% ------
%  obj - tagged objects
%  tags - tags to search for
%  logic - logic to user for combining search
%
% Output:
% -------
%  ix - objects found indices

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4286 $
% $Date: 2006-03-15 19:04:03 -0500 (Wed, 15 Mar 2006) $
%--------------------------------

% NOTE: consider an implementation that creates a parallel index array and reduces
