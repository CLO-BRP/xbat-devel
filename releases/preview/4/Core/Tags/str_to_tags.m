function tags = str_to_tags(str)

% str_to_tags - convert string to tags
% ------------------------------------
%
% tags = str_to_tags(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  tags - tags in string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1953 $
% $Date: 2005-10-19 20:22:46 -0400 (Wed, 19 Oct 2005) $
%--------------------------------

%--
% check for string input
%--

if ~ischar(str)
	error('String input is required.');
end

%--
% parse string to cell array
%--

tags = strread(str, '%s', -1, 'delimiter', ' ');

% NOTE: return input string when parse to valid tags fails, does this ever happen?

if ~is_tags(tags)
	tags = str;
end
