function value = is_tags(tags)

% is_tags - check that input is a valid set of tags
% -------------------------------------------------
%
% value = is_tags(tags)
%
% Input:
% ------
%  tags - candidate tags
%
% Output:
% -------
%  tags - valid tags test result

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1953 $
% $Date: 2005-10-19 20:22:46 -0400 (Wed, 19 Oct 2005) $
%--------------------------------

% TODO: this code can be refactored for conciseness and efficiency

% TODO: add coded output to distinguish between cell and string tags

%--
% check for empty cell
%--

% NOTE: an empty cell is the empty tags set

if iscell(tags) && isempty(tags)
	value = 1; return;
end 

%--
% check for a string or cell array of strings
%--

if ~ischar(tags) && ~iscellstr(tags)
	value = 0; return;
end

%--
% check string content
%--

if ischar(tags)
	value = valid_tag(tags); return;
end

%--
% check string cell array contents
%--

for k = 1:length(tags)
	
	% NOTE: we return quickly if we find a not valid tag
	
	if ~valid_tag(tags{k})
		value = 0; return;
	end 
	
end 

% NOTE: passed all tests

value = 1;
