function [value, tag] = valid_tag(tag, rep)

% valid_tag - check tag validity and enforce it
% ---------------------------------------------
%
% [value, tag] = valid_tag(tag, opt)
%
% Input:
% ------
%  tag - candidate tag
%  rep - space replacement (def: '-')
%
% Output:
% -------
%  value - tag validity test result
%  tag - derived valid tag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1953 $
% $Date: 2005-10-19 20:22:46 -0400 (Wed, 19 Oct 2005) $
%--------------------------------

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% check for string input
%--

if ~ischar(tag)
	error('Tags are strings.');
end

%--
% set default space replacement
%--

if (nargin < 2)
	rep = '-';
end 

%--
% check and enforce validity
%--

% NOTE: tags have no spaces, everything else goes

if any(isspace(tag))
	value = 0; tag = strrep(strtrim(tag), ' ', rep); return;
end

% NOTE: string with no spaces, we have a tag

value = 1;

