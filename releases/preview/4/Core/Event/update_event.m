function events = update_event(old_event)

% update_event - update event(s) to new structure form
% ----------------------------------------------------
% events = update_event(old_event)
%
% Input:
% ------
%  old_event - array of old events
%
% Output:
% -------
%  events - new events

opt = struct_update; opt.flatten = 0;

events = empty(event_create);

measurements = get_measurements; annotations = get_annotations;

for k = 1:length(old_event)	
	
	event = struct_update(event_create, old_event(k), opt);
	
	%--
	% trim off measurements and annotations not supported on this system
	%--
	
	for k = length(event.measurement):-1:1
		if ~ismember(event.measurement(k).name, {measurements.name})
			event.measurement(k) = [];
		end
	end
	
	for k = length(event.annotation):-1:1
			
		if ~ismember(event.annotation(k).name, {annotations.name})
			event.annotation(k) = []; continue;
		end
		
		break;
		
% 		if k > 1
% 			event.annotation(k) = [];
% 		end	
		
	end
	
	%--
	% write updated event to output array
	%--
	
	events(end + 1) = event;
	
	migrate_wait('Events');

end
