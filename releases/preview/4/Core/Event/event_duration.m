function duration = event_duration(event)

% event_duration - get event duration in seconds
% ----------------------------------------------
%
% duration = event_duration(event)
%
% Input:
% ------
%  event - event array
%
% Output:
% -------
%  duration - event durations

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 563 $
% $Date: 2005-02-21 05:59:20 -0500 (Mon, 21 Feb 2005) $
%--------------------------------

% TODO: implement event array case using 'struct_field'

% NOTE: the duration of a composite event should be set once?, think groups and containers

%--
% allocate and compute durations
%--

duration = zeros(size(event));

for k = 1:numel(event)

	switch (event.level)

		%--
		% simple event
		%--
		
		case (1)
			duration(k) = diff(event.time);
		
		%--
		% hierarchical event
		%--
		
		otherwise

			% NOTE: duration for composite event is still not defined
			
	end
	
end