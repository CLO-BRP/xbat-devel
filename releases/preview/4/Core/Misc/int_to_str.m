function str = int_to_str(x)

% int_to_str - convert integers to strings
% ----------------------------------------
%
% str = int_to_str(x)
%
% Input:
% ------
%  x - input matrix
%
% Output:
% -------
%  str - string conversion

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% round and real
%--

x = round(x);

%--
% convert to string
%--

% NOTE: outputs a cell array for matrix inputs and a string for scalars

str = int_to_str_(x);