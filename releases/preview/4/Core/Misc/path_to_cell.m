function out = path_to_cell(str, delimiter)

% path_to_cell - separate path parts into cell
% --------------------------------------------
%
% out = path_to_cell(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  out - cell array with path contents

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1752 $
% $Date: 2005-09-07 16:22:23 -0400 (Wed, 07 Sep 2005) $
%--------------------------------

%--
% handle file separator
%--

if nargin < 2 || isempty(delimiter)

	if (strcmp(filesep,'\'))
		delimiter = '\\';
	else
		delimiter = filesep;
	end

end

%--
% parse string into cell
%--

out = strread(str,'%s','delimiter',delimiter);