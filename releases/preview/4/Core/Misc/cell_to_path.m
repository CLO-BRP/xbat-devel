function out = cell_to_path(in, delimiter)

% cell_to_path - concatenate cell strings with a delimiter
% --------------------------------------------------------
%
% out = cell_to_path(in, delimiter)
%
% Input:
% ------
%  in - input cell string
%
% Output:
% -------
%  out - concatenated string

%--
% handle file separator
%--

if nargin < 2 || isempty(delimiter)

	if (strcmp(filesep,'\'))
		delimiter = '\\';
	else
		delimiter = filesep;
	end

end

%--
% concatenate
%--

out = '';

for k = 1:length(in)
	out = [out, in{k}, ';'];
end