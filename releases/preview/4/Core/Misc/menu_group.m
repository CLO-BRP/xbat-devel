function h = menu_group(g,fun,L,S,A,c)

% menu_group - append menu to figure or other menu
% ------------------------------------------------
%
% h = menu_group(g,fun,L,S,A,c)
%
% Input:
% ------
%  g - parent handle
%  fun - callback function
%  L - labels
%  S - separators (def: [])
%  A - accelerator keys (def: [])
%  c - controlled figure handle (def: gcf)
%
% Output:
% -------
%  h - handles to menu items

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1772 $
% $Date: 2005-09-09 18:34:37 -0400 (Fri, 09 Sep 2005) $
%--------------------------------

% TODO: update callbacks to use tags, the label can then be used to store state

% NOTE: get command from menu tag and further state from the label

% NOTE: calling update for menu commands will be worthwhile work

%--
% number of items
%--

n = length(L);

%--
% set accelerator keys
%--

if (nargin < 5)
	A = cell(1,n);
end

%--
% set separator flags
%--

if (nargin < 4)
	if (n > 1)
		S = bin2str(zeros(1,n));
	else
		S = {'off'};
	end
end

%--
% create menu items
%--

switch (get(g,'type'))

	case ({'figure'})

		%--
		% create callback strings
		%--
		
		if (~isempty(fun))
			
			if (isstr(fun))
				
				%--
				% toolbar formalism (focus controlled figure c, execute command, return to toolbar g)
				%--
				
				if (nargin > 5)
					
					for k = 1:n
						F{k} = ['figure(' num2str(c) '); ' ...
							fun '(gcf,''', L{k}, '''); figure(' num2str(g) ');'];
					end
				
				%--
				% standard menu (execute command on figure with menu)
				%--
				
				else 
					
					for k = 1:n
						F{k} = [fun '(gcf,''' L{k} ''');'];
					end
					
				end		
				
			elseif (isa(fun,'function_handle'))
				
				for k = 1:n
					F{k} = fun;
				end
				
			end
				
		%--
		% information menu items (no callback function)
		%--
		
		else
			
			for k = 1:n
				F{k} = '';
			end	
			
		end

		%--
		% create menu items (put label in label and tag, the latter helps with dynamic menus)
		%--
		
		% first item is the name of the menu, no callback
		
		h(1) = uimenu(g,'label',L{1},'tag',L{1},'callback','');
				
		for k = 2:n	
			
			h(k) = uimenu(h(1),'label',L{k},'separator',S{k},'callback',F{k});		
			
			if (~isempty(A{k}))
				set(h(k),'accelerator',A{k});
			end	

		end
	
	case ({'uimenu','uicontextmenu'})
	
		%--
		% empty parent callback
		%--
		
		set(g,'callback','');
		
		%--
		% create callback strings
		%--
		
		if (~isempty(fun))		
			
			if (isstr(fun))
				
				%--
				% toolbar formalism (focus controlled figure c, execute command, return to toolbar g)
				%--
				
				if (nargin > 5)
				
					for k = 1:n
						F{k} = ['figure(' num2str(c) '); ' ...
							fun '(gcf,''', L{k}, '''); figure(' num2str(g) ');'];
					end
				
				%--
				% standard menus (execute command on figure with menu)
				%--
				
				else 
				
					for k = 1:n
						F{k} = [fun '(gcf,''' L{k} ''');'];
					end
					
				end		
				
			elseif (isa(fun,'function_handle'))
				
				for k = 1:n
					F{k} = fun;
				end
				
			end
		
		%--
		% information menu items (no callback function)
		%--
		
		else
			
			for k = 1:n
				F{k} = '';
			end		
			
		end

		%--
		% create menu items
		%--
		
		for k = 1:n
		
			h(k) = uimenu(g,'label',L{k},'separator',S{k},'callback',F{k});
				
			if (~isempty(A{k}))
				set(h(k),'accelerator',A{k});
			end
			
		end
		
end
