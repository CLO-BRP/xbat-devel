function res = reg_exp(str, pat)

% reg_exp - match regular expression and pack result
% --------------------------------------------------
%
% res = reg_exp(str, pat);
%
% Input:
% ------
%  str - string to match
%  pat - regular expression
%
% Output:
% -------
%  res - results structure

%--
% add string and pattern to result
%--

res.str = str; 

res.pat = pat;

%--
% match regular expression and add outputs to result
%--

[res.start, res.end, res.extent, res.match, res.token, res.name] = regexp(str, pat);

%--
% display result if no output
%--

% NOTE: we display the result and clear to avoid actual output display

if ~nargout
	disp(' '); disp(show_reg_exp(res)); disp(' '); clear res;
end
