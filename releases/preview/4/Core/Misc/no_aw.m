function no_aw

% TODO: add exclusion rule for top sponsored links

% TODO: make rules important

%--
% create exclusion rule
%--

% NOTE: there should be a more concise way of doing this

rule = '';

for k = 1:99
	rule = [rule, '#aw', int2str(k), ','];
end	

rule = [rule, '.a {display: none;}'];

%--
% save file
%--

[file,path] = uiputfile('no_aw.css','Save ...');

if (~ischar(file))
	return;
end

fid = fopen([path, file],'wt')

fprintf(fid,'%s',rule); 

fclose(fid);