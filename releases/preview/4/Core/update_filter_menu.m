function par = update_filter_menu

% update_filter_menu - update filter menu for open browsers
% ---------------------------------------------------------
%
% par = update_filter_menu
%
% Output:
% -------
%  par - updated browser handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1814 $
% $Date: 2005-09-21 15:24:01 -0400 (Wed, 21 Sep 2005) $
%--------------------------------

%--
% get handles to open browser figures
%--

par = get_xbat_figs('type', 'sound');

% NOTE: return if there is nothing to update

if isempty(par)
	return;
end

%--
% destroy and create filter menus
%--

for k = 1:length(par)
	
	%--
	% find parent menu
	%--
	
	menu = findobj(par(k), 'type', 'uimenu', 'label', 'Filter');
	
	if isempty(menu)
		continue;
	end
	
	%--
	% kill all children then parent
	%--
	
	try
		delete(allchild(menu)); delete(menu); 
	end
		
	%--
	% recreate filter menu, this updates filter stores
	%--

	browser_filter_menu(par(k));
	
end
