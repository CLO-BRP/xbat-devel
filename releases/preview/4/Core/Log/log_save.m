function flag = log_save(log,p,title)

% log_save - save log to file
% ---------------------------
%
% flag = log_save(log,p,title)
%
% Input:
% ------
%  X - log structure
%  p - alternate location to save, or dialog if empty
%  title - title string used in file dialog
%
% Output:
% -------
%  flag - save success indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

%--
% set title string
%--

if ((nargin < 3) || isempty(title))
	title = 'Save XBAT Log File:';
end

%--
% check for save as option
%--

if (nargin > 1)
	
	%--
	% get log location interactively using file dialog
	%--
	
	if (isempty(p))
		
		%--
		% start in input log directory
		%--
		
		di = pwd; cd(log.path);
		
		%--
		% get log file location and filename
		%--
		
     	[fn,p] = uiputfile(log.file,title);
		
		%--
        % return on cancel and return to initial directory
     	%--
		
        if (~fn)
			
			disp(' ');
			warning(['Log file ''' file_ext(log.file) ''' was not saved, ''uiputfile'' was cancelled.']);
			
			cd(di); 
			
			flag = 0; return;
			
		end
		
		%--
		% update log fields
		%--
		
		log.file = fn;
		
		log.path = p;
		
		log.created = datestr(now);
		
		log.modified = '';
		
		%--
		% save log
		%--
		
		flag = log_save(log);
		
	%--
	% log location and filename provided
	%--
	
	else
		
		%--
		% parse input location to match uigetfile output
		%--
        
        [p,f1,f2] = fileparts(p);
		
		fn = [f1 f2];
		
		%--
		% update log fields
		%--
		
		log.file = fn;
		
		log.path = p;
		
		log.created = datestr(now);
		
		log.modified = '';
		
		%--
		% save log
		%--
		
		flag = log_save(log);
		
	end
	
%--
% save log according to current field values
%--

else 

	%--
	% update saved field
	%--
	
	log.saved = 1;
	
	log.modified = datestr(now);
	
	%--
	% create named log variable
	%--
	
	var = file_ext(log.file);
	
	var = strip_punctuation(var);
	
	var = ['Log_' var];
	
	eval([var ' = log;']);
	
	%--
	% save log variable to log file
	%--
	
	% NOTE: convert single quotes to double quotes to evaluate string
	
	log_path = strrep(log.path,'''','''''');
	
	log_file = strrep(log.file,'''','''''');
	
	eval(['save(''' log_path, log_file ''',''' var ''');']);
	
	%--
	% return flag
	%--
	
	flag =  1;
	
end
