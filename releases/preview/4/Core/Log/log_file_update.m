function log = log_file_update(par, oldlog)

% log_file_update - create new version of deprecated log file
% -----------------------------------------------------------
%
% [flag, g] = log_file_update(par, oldlog)
%
% Input:
% ------
%  par - parent browser handle
%  oldlog - old log structure
%
% Output:
% -------
%  log - new log structure

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 5809 $
% $Date: 2006-07-19 18:37:34 -0400 (Wed, 19 Jul 2006) $
%--------------------------------

%--
% setup
%--

data = get_browser(par);

file = oldlog.file;  

lib = get_active_library;

user = get_active_user;

%--
% test for existance of log
%--

str = [lib.path, sound_name(data.browser.sound), filesep, 'Logs', filesep, file];

if exist(str, 'file')
	log = []; return;
end

%--
% create new log
%--

log = log_create(str,'sound',data.browser.sound,'author',get_field(user,'name'));

%--
% update log events
%--

opt = struct_update; opt.level = 1; 

for event = oldlog.event	
	log.event(end + 1) = struct_update(event_create, event, opt);
end

%--
% update log ID numbers and index and save
%--

log.length = length(log.event); log.curr_id = max([log.event.id]) + 1;

log_save(log);



	
	
	
	