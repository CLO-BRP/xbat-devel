function out = create_table(in,name)

% create_table - create table for struct array
% --------------------------------------------
%
% out = create_table(in,name)
%
% Input:
% ------
%  name - table name
%  in - struct array
%
% Output:
% -------
%  out - struct output 

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% try to get name from input
%--

if (nargin < 2)
	
	name = inputname(1);
	
	if (isempty(name))
		error('Unable to get table name from input name.');
	end
	
end
	
%---------------------------------
% CREATE QUERIES
%---------------------------------

%--
% flatten struct and use field names as column names
%--

in = flatten_struct(in); 

column = fieldnames(in);

%--
% build create table statement
%--

sql{1} = ['CREATE TABLE ', name, ' ('];

for k = 1:length(column)	
	
	%--
	% get type and constraints
	%--
	
	temp = get_type_and_constraints(column{k},in.(column{k}));
	
	%--
	% build column description line
	%--
	
	sql{end + 1} = ['  ', column{k}, ' ', temp.type , temp.constraints, ',']
	
end

sql{end + 1} = ')';

out.sql = sql(:);