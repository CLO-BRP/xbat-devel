function type = get_type(value)

% get_type - get type description for value
% -----------------------------------------
%
% type = get_type(value)
%
% Input:
% ------
%  value - content of struct field
%
% Output:
% -------
%  type - fragment describing value type

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% DEFAULT
%--

% NOTE: return empty for container and object types

type = '';

%--
% NUMERIC
%--

if (isnumeric(value))
	
	% NOTE: array contents are stored as blobs
	
	if (numel(value) > 1)
		type = 'BLOB'; return;
	end
	
	if (isfloat(value))
		type = 'REAL'; return;
	end

	if (isinteger(value))
		type = 'INTEGER'; return;
	end
	
	type = 'BLOB';
	
end

%--
% LOGICAL
%--

if (islogical(value))
	
	if (numel(value) > 1)
		type = 'BLOB'; return;
	end
	
	type = 'BOOLEAN'; return;
	
end

%--
% STRINGS
%--

if (ischar(value))
	type = 'VARCHAR'; return;
end

if (iscellstr(value))
	type = 'TEXT'; return;
end