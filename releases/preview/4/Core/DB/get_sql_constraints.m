function str = get_constraints(column)

% get_constraints - get constraints based on naming conventions
% -------------------------------------------------------------
%
% str = get_constraints(column)
%
% Input:
% ------
%  column - field name
%
% Output:
% -------
%  str - constraint description

% TODO: develop index naming convention

str = '';

switch (lower(column))
	
	case ({'id','created','modified','author'})
		
		str = ' NOT NULL';
		
end