function sql = create_event_table

%--
% create event with type hint data
%--

int = int32(1);

event = event_create( ...
	'id', int, ...
	'channel', int, ...
	'level', int ...
);

%--
% remove children object fields
%--

fields = {'detection','annotation','measurement','userdata'};

for k = 1:length(fields)
	event = rmfield(event,fields{k});
end

%--
% generate create table sql
%--

sql = create_table('Events',event);
