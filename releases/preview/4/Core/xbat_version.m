function str = xbat_version(name)

% xbat_version - output XBAT version string
% -----------------------------------------
%
% str = xbat_version(name)
%
% Input:
% ------
%  name - version name
%  info - subversion info
%
% Output:
% -------
%  str - version string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6971 $
% $Date: 2006-10-09 15:34:23 -0400 (Mon, 09 Oct 2006) $
%--------------------------------

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set default name
%--

if nargin < 1
	name = 'DEVEL';
end
	
%-----------------------
% VERSION STRING
%-----------------------

%--
% get matlab version info
%--

rel = ver('matlab'); rel = rel.Version;

%--
% build version string
%--

% NOTE: this is used in figure titles and structure version fields

str = [name, '  (MATLAB ', rel, ')'];

%-----------------------
% SVN VERSION STRING
%-----------------------

%--
% check SVN info is relevant
%--

if ~is_working_copy(xbat_root) || isempty(tsvn_root)
	return;
end

%--
% get svn info
%--

info = get_svn_info;

if isempty(info) || ~isfield(info, 'rev')
	return;
end

%--
% create string with svn info 
%--

str = [name, '  (SVN ', int2str(info.rev), ')  (MATLAB ', rel, ')'];


