function log = log_menu_update(mode, data, m, ix)

% log_menu_update - update log information menu
% ---------------------------------------------
%
% flag = log_menu_update('meta', data, m, ix)
%
%      = log_menu_update('edit', data, m, ix)
%
%      = log_menu_update('event', data, m, ix)
%
% Input:
% ------
%  data - figure userdata
%  m - log index
%  ix - event index
%
% Output:
% -------
%  flag - update success flag

switch (mode)
	
	%--
	% modification date update
	%--
	
	case ('update')
		
		%--
		% get log info menu
		%--
		
		tmp = get_menu(data.browser.log_menu.log,file_ext(data.browser.log(m).file));
		tmp = get_menu(tmp,'Log');
		tmp = get_menu(tmp,'Log Info:');
		
		%--
		% recreate log info menu
		%--
		
		delete(get(tmp,'children'));
	
		L = { ...
			['Author:  ' data.browser.log(m).author], ...
			['Created:  ' datestr(data.browser.log(m).created)], ...
			['Modified:  ' datestr(data.browser.log(m).modified)] ...
		};
	
		menu_group(tmp,'',L);

	%--
	% full update
	%--
	
	case ('full')
		
		%--
		% get log menu
		%--
		
		tmp = get_menu(data.browser.log_menu.log,file_ext(data.browser.log(m).file));
		tmp = get_menu(tmp,'Log');
		
		g = get_menu(tmp,'Log Info:');
		tmp = get(g,'children');
		
		%--
		% recreate log menu
		%--
		

	
	%--
	% event addition and deletion update
	%--
	
	case ('event')
		
end