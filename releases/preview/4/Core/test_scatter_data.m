function [X, name] = test_scatter_data(par)

%--------------------
% HANDLE INPUT
%--------------------

if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--------------------
% SETUP
%--------------------

%--
% get logs from browser
%--

data = get_browser(par);

log = data.browser.log(1);

%--------------------
% SCATTER
%--------------------
	
%--
% get basic event data
%--

time = reshape([log.event.time]', 2, [])'; 

freq = reshape([log.event.freq]', 2, [])' / 1000;

X(:,1) = time(:, 1);

X(:,2) = freq(:, 1);

X(:,3) = diff(time, 1, 2);

X(:,4) = diff(freq, 1, 2);

%--
% set names
%--

name = { ...
	'Time', 'Freq', 'Duration', 'Bandwidth' ...
};

% NOTE: remove time variable

X = X(:, 2:end); name = name(2:end);


%--
% create scatter matrix
%--

scatter_matrix(fig, X, name);
