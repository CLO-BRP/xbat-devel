function Y = hist_match(X,T)

% hist_match - match image histograms
% -----------------------------------
%
% Y = hist_match(X,T)
%
% Input:
% ------
%  X - input image
%  T - target image
%
% Output:
% -------
%  Y - histogram matched image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-12-15 13:52:40 -0500 (Thu, 15 Dec 2005) $
% $Revision: 2304 $
%--------------------------------

%--
% compute histogram of target image
%--

[h,c] = hist_1d(T,256);

%--
% specify histogram of input image
%--

Y = hist_specify(X,h,c);

