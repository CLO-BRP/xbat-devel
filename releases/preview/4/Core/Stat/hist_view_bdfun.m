function hist_view_bdfun(h,str)

% hist_view_bdfun - button down function for histogram view
% ---------------------------------------------------------
%
% hist_view_bdfun(h,str)
%
% Input:
% ------
%  h - figure handle
%  str - command string

%--
% set command string
%--

if ((nargin < 2) | isempty(str))
	str = 'Initialize'; 
end

%--
% set figure handle
%--

if ((nargin < 1) | isempty(h))
	h = gcf;
end

%--
% main switch
%--

switch (str)
	
	%--
	% Initialize
	%--
	
	case ('Initialize')
		set(h,'windowbuttondownfcn','hist_view_bdfun(gcf,''action'')');
	
	%--
	% Action
	%--
	
	case ('Action')
		
		%--
		% get userdata
		%--
		
		data = get(h,'userdata');
		
		%--
		% get children and bring them to front
		%--
		
		ch = data.hist_menu.children;
		
		for k = 1:length(ch)
			figure(ch(k));
		end
		
% 		figure(h);
		
end