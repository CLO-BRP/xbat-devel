function line = apply_fun(line, fun, args)

% apply_fun - apply line processing function
% ------------------------------------------
%
% line = apply_fun(line, fun, args)
%
% Input:
% ------
%  line - line to process
%  fun - process line function handle
%  args - arguments for process line function
%
% Output:
% -------
%  line - processed line

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% return quickly on no processing
%--

if isempty(fun)
	return;
end

%--
% consider availability of arguments in evaluation
%--

if isempty(args)
	line = fun(line);
else
	line = fun(line, args{:});
end
