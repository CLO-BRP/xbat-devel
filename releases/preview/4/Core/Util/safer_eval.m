function fail = safer_eval(str)

% safer_eval - evaluate string with some concern
% ----------------------------------------------
%
% fail = safer_eval(str)
%
% Input:
% ------
%  str - string to evaluate
%
% Output:
% -------
%  fail - failure indicator
% 
% NOTE:
% -----
% This only works if string evaluated does not use or modify caller variables.

% NOTE: this is an experiment to see what may be possible

%--
% check string for unsafe commands
%--

censored = {'quit', 'close', 'delete', 'stop', 'set', 'figure', 'uicontrol'};

for k = 1:numel(censored)
	
	if strfind(str, censored{k})
		str = ''; break;
	end
	
end

%--
% evaluate string
%--

try
	eval(str);
catch
	if ~nargout
		xml_disp(lasterror);
	end
end
