function keep(varargin)

% keep - keeps a set of variables
% -------------------------------
% 
% keep x1 ... xN
%
% Input:
% ------
%  x1 ... xN - names of variables to keep (separated by spaces)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-07-06 13:36:06-04 $
%--------------------------------

%--
% get variables in base workspace
%--

v = evalin('base','who');
vlen = length(v);

%--
% create list of variables to delete
%--

ix = ones(vlen,1);

for k = 1:nargin
	ix = ix & ~strcmp(v,varargin{k});
end

d = find(ix);

dlen = length(d);

%--
% delete indicated variables if any
%--

if (dlen)

	str = 'clear';
	
	for k = 1:dlen
		str = [str,' ',v{d(k)}];
	end
	
	evalin('base',str);
	
end
