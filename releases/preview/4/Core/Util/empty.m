function out = empty(in)

% empty - create an empty array for a type
% ----------------------------------------
%
% out = empty(in)
%
% Input:
% ------
%  type - type prototype
%
% Output:
% -------
%  out - empty array of prototype type

% NOTE: why is this not a part of the language?

if (isempty(in))
	out = in; return;
end

out = in(1); out(1) = [];