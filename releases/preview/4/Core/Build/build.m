function build

clear('functions');

%--
% check we are in MEX directory
%--

here = pwd;

if ~strncmp(fliplr(here), 'XEM', 3)
	disp('this is not a MEX directory, there is nothing to build'); return;
end

%--
% build using build scipt
%--

if isdir('private') && exist('./private/build.m')
	
	cd('private'); build; cd(here); return;

end

%--
% build generically
%--

d = dir;

fnames = {d.name};

for ix = 1:length(fnames)

	if any(strfind(fnames{ix}, '_mex.c')) || any(strfind(fnames{ix}, '_.c'))

		disp(['Building "' fnames{ix} '" ...']); 

		build_mex(fnames{ix});

	end

end	 

