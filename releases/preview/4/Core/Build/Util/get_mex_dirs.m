function out = get_mex_dirs

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default root directory
%--

if ((nargin < 1) ||isempty(p))
	p = xbat_root;
end

%--------------------------------
% GET MEX DIRS
%--------------------------------

%--
% scan directories with mex selection callback
%--

p

out = scan_dir(p,@get_mex);

if (~nargout)
	xml_disp(out);
end

%--------------------------------
% GET_MEX
%--------------------------------

function out = get_mex(p)

if (strmatch('XEM',fliplr(p)))
	out = p;
else
	out = [];
end