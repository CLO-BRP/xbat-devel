function w = bspline_weights(f, h)

% bspline_weights - get spline matrix and solve for weights
% ---------------------------------------------------------
% w = bspline_weights(f, h)
%
% Input:
% ------
%  f - values of function on equi-spaced grid
%  h - grid space
%
% Output:
% -------
%  w - weights

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

if nargin < 2 || isempty(h)
	h = 1;
end

A_inv = bspline3_matrix(length(f), h);

w = A_inv * f(:);


