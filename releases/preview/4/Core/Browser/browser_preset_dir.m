function p = browser_preset_dir(in,type)

% browser_preset_dir - get browser presets directory
% --------------------------------------------------
%
% p = browser_preset_dir(in,type)
%
% Input:
% ------
%  in - browser handle or type
%  type - preset type
%
% Output:
% -------
%  p - browser preset type root

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% TODO: allow multiple roots to a preset type by pooling over browser types

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default figure and check for browser
%--

% TODO: develop function to test for browser figure

if ((nargin < 1) || isempty(in))
	in = gcf;
end

if (~is_browser(in))
	error('Input figure is not a browser.');
end

%--------------------------------
% GET DIRECTORY
%--------------------------------

if (all(ishandle(in)))

	if (numel(in) > 1)
		warning('Using first handle in list.'); in = in(1);
	end
	
	info = get_browser_info(in)
	
	in = browser_type_tag(info.type); 
	
	in(1) = upper(in(1)); type(1) = upper(type(1));
	
	% NOTE: reading of these paths requires some inversions
	
	p = [browsers_root, filesep, in, filesep, 'Presets', filesep, type];
	
end