function info = process_browser_events(par, ext, m, ix)

% process_browser_events - process events in browser
% --------------------------------------------------
%
% process_browser_events(par, ext, m, ix)
%
% Input:
% ------
%  par - browser
%  ext - extension
%  m - log store index 
%  ix - event store indices
%
% Output:
% -------
%  info - computation info

%-------------------------
% HANDLE INPUT
%-------------------------

if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%-------------------------
% PROCESS
%-------------------------

% NOTE: this is an archaic and very dangerous API

%--
% process browser events and collect some profiling info
%--

t0 = clock;

feval(ext.fun, 'events', par, m, ix);

info.events = length(ix);

info.elapsed = etime(clock, t0);




