function out = browser_measure_menu(h, str, flag)

% browser_measure_menu - browser measurement function menu
% --------------------------------------------------------
%
% flag = browser_measure_menu(h,str,flag)
%
% Input:
% ------
%  h - figure handle (def: gcf)
%  str - menu command string (def: 'Initialize')
%  flag - info flag (def: '')
%
% Output:
% -------
%  out - command dependent output

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-11-01 19:45:41 -0500 (Tue, 01 Nov 2005) $
% $Revision: 2056 $
%--------------------------------

%--
% set command string
%--

if (nargin < 2)
	str = 'Initialize';
end

%--
% set handle
%--

if (nargin < 1)
	h = gcf;
end

%--
% set default output
%--

out = [];

%--
% get userdata
%--

data = get(h, 'userdata');

%--
% get measurement information
%--

% NOTE: extensions from system not currently used

[SYSTEM_MEAS, SYSTEM_NAMES] = get_measurements(h, 'update');

% NOTE: extension info from browser

MEAS_NAME = data.browser.measurement.name;

MEAS_VIEW = data.browser.measurement.view;

%--
% main switch
%--

switch (str)

%-----------------------------------------------------------------
% INITIALIZE
%-----------------------------------------------------------------

case ('Initialize')
	
	%--
	% check for existing menu
	%--
		
	tmp = get(findobj(gcf, 'type', 'uimenu', 'parent', gcf), 'label');
	
	tmp = find(strcmp(tmp, 'Measure'));
	
	if ~isempty(tmp)
		return;
	end
	
	%--
	% Measure
	%--
	
	L = strcat(MEAS_NAME,' ...');
	
	L = { ...
		'Measure', ...
		'Display', ...
		L{:} ...
	};

% 	'Active', ...
	
	n = length(L);
	
	S = bin2str(zeros(1,n));
	S{3} = 'on';
	
	mg = menu_group(h,'browser_measure_menu',L,S);
	
	data.browser.measure_menu.measure = mg;
	
	set(mg(1),'position',10);
	
	% turn off measurements upon initialization, opening a log will
	% activate these
	
	set(mg(3:end), 'enable', 'off');
	
	%--
	% Display
	%--
	
	L = { ...
		'No Display', ...
		MEAS_NAME{:}, ...
		'Display All' ...
	};
	
	n = length(L);
	
	S = bin2str(zeros(1,n));
	
	S{2} = 'on';
	
	S{end} = 'on';
	
	tmp = menu_group(get_menu(mg, 'Display'), 'browser_measure_menu', L, S);
	
	data.browser.measure_menu.display = tmp;
	
	for k = 1:length(MEAS_VIEW)
		set(get_menu(tmp, MEAS_VIEW{k}), 'check', 'on');
	end
	
% 	%--
% 	% Active
% 	%--
% 	
% 	L = { ...
% 		'No Measure', ...
% 		MEAS_NAME{:} ...
% 	};
% 	
% 	n = length(L);
% 	
% 	S = bin2str(zeros(1,n));
% 	S{2} = 'on';
% 	
% 	tmp = menu_group(get_menu(mg,'Active'),'browser_measure_menu',L,S);
% 	data.browser.measure_menu.active = tmp
% 	
% 	L{1} = '';
% 	ix = find(strcmp(L,data.browser.measurement.active));
% 	set(tmp(ix),'check','on');
	
	%--
	% save userdata
	%--
	
	set(h, 'userdata', data);
	
%-----------------------------------------------------------------
% ACTIVE AND DISPLAY
%-----------------------------------------------------------------

%----------------------------------
% NO DISPLAY
%----------------------------------

case ('No Display')
	
	%--
	% update measure display list
	%--
	
	data.browser.measurement.view = cell(0);
	
	set(h, 'userdata', data);
	
	%--
	% update display state menu
	%--
	
	tmp = data.browser.measure_menu.display;
	
	set(tmp, 'check', 'off');	
	
	%--
	% update display
	%--
	
	browser_display(h, 'events', data);
	
%----------------------------------
% DISPLAY ALL
%----------------------------------

case ('Display All')
	
	%--
	% update measurement display list
	%--
	
	data.browser.measurement.view = MEAS_NAME;
	
	set(h, 'userdata', data);
	
	%--
	% update display state menu
	%--
	
	tmp = data.browser.measure_menu.display;
	
	set(tmp(2:(end - 1)), 'check', 'on');	
	
	%--
	% update display
	%--
	
	browser_display(h, 'events', data);

%----------------------------------
% TOGGLE MEASURE DISPLAY
%----------------------------------

case (MEAS_NAME)

	%--
	% update display state for measurement
	%--

	ixa = find(strcmp(MEAS_VIEW, str));

	if isempty(ixa)
		MEAS_VIEW = sort({MEAS_VIEW{:}, str}); state = 'on';
	else
		MEAS_VIEW(ixa) = []; state = 'off';
	end
	
	data.browser.measurement.view = MEAS_VIEW;
	
	set(h, 'userdata', data);

	%--
	% update display state menu
	%--

	set(get_menu(data.browser.measure_menu.display, str), 'check', state);

	%--
	% update display
	%--

	browser_display(h, 'events', data);
	
%-----------------------------------------------------------------
% MEASUREMENT ...
%-----------------------------------------------------------------

% NOTE: currently this initiates computation dialog in the future it opens palette

case (strcat(MEAS_NAME,' ...'))
	
	%----------------------------------------------
	% OLD INTERFACE (START)
	%----------------------------------------------
	
	%--
	% select measure from available measures
	%--
	
	MEAS = data.browser.measurement.measurement;
	
	ixa = find(strcmp(MEAS_NAME, str(1:end - 4)));
	
	ext = MEAS(ixa);
	
	%--
	% compute measurement for all events in active log
	%--
	
	m = data.browser.log_active; 
	
	ix = 1:data.browser.log(m).length;
	
	process_browser_events(h, ext, m, ix);
	
	%----------------------------------------------
	% OLD INTERFACE (END)
	%----------------------------------------------
	
	%--
	% update display
	%--
	
	browser_display(h, 'events')

end