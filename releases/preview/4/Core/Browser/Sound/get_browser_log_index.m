function m = get_browser_log_index(par, name, data)

% get_browser_log_index - get index of named log in browser log array
% -------------------------------------------------------------------
%
% m = get_browser_log_index(par, name, data)
%
% Input:
% ------
%  par - browser handle
%  name - log name
%  data - browser state
%
% Output:
% -------
%  m - log index

% NOTE: the log index affects display

%--
% get open log names
%--

% NOTE: the name is the file name without extension

names = file_ext(struct_field(data.browser.log, 'file'));

%--
% compare names to input to get index
%--

m = find(strcmp(names, name));