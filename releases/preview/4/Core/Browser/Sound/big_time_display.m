function handle = big_time_display(ax, time, grid, sound)

% big_time_display - big time display
% -----------------------------------
%
% handle = big_time_display(ax, time, limits, sound)
%
% Input:
% ------
%  ax - parent axes
%  time - time
%  grid - grid options
%  sound - related sound
%
% Output:
% -------
%  handle - big time handle

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4695 $
% $Date: 2006-04-20 11:22:26 -0400 (Thu, 20 Apr 2006) $
%--------------------------------

% TODO: expand to handle multiple axes for display

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set default empty sound and grid
%--

% NOTE: use duck decoys to solve the variable input problem

if nargin < 4
	sound.time_stamp = []; sound.realtime = [];
end

if nargin < 3
	grid.time.labels = 'seconds';
end

%---------------------------
% BIG TIME DISPLAY
%---------------------------

%--
% convert to session time from slider time
%--

time = map_time(sound, 'real', 'slider', time);

%--
% use grid options to produce string
%--

% NOTE: realtime translation if applied if needed, this is independent from sessions

string = get_grid_time_string(grid, time, sound.realtime);

%--
% display big time string
%--

handle = big_centered_text(ax, string);
