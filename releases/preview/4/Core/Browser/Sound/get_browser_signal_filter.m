function ext = get_browser_signal_filter(varargin)

% get_browser_signal_filter - get browser signal filter
% -----------------------------------------------------
%
% ext = get_browser_signal_filter(par,name,data)
%
% Input:
% ------
%  par - parent browser
%  name - signal filter name
%  data - parent state
%
% Output:
% -------
%  ext - browser signal filter

ext = get_browser_extension('signal_filter',varargin{:});

