function window_plot(h,pal,data)

% window_plot - create and update window display
% ----------------------------------------------
% 

%--
% get control handles
%--

handles = get_control(pal,'Window_Plot','handles');

% NOTE: return quickly if controls is not there

if (isempty(handles))
	return;
end

ax = handles.axes;

%--
% display window in plot axes
%--

axes(ax);

hold on;

%--
% remove previous lines
%--

kids = get(ax, 'children');

if (~isempty(kids))
	delete(get(ax,'children'));
end

%--
% draw new window line
%--

nfft = data.browser.specgram.fft;

nwin = floor(data.browser.specgram.win_length*nfft);

npad = nfft - nwin;

next = floor(data.browser.specgram.hop*nfft);

if (isempty(window_to_fun(data.browser.specgram.win_type,'param')))
	win = feval( ...
		window_to_fun(data.browser.specgram.win_type), ...
		nwin ...
	);
else
	win = feval( ...
		window_to_fun(data.browser.specgram.win_type), ...
		nwin, ...
		data.browser.specgram.win_param ...
	);
end

%--
% pad window with zeros
%--

padwin = [win(:); zeros(npad, 1)];

nextwin = [zeros(next,1); padwin(1:(nfft - next))];


%--
% display window and shifted window
%--
	
tmp = plot(padwin,'k');

set(tmp,'linewidth',1);

hold on;

tmp = plot(nextwin, ':k');

tmp = plot([1,nfft],[0 0],':');
set(tmp,'color',0.5*ones(1,3));

tmp = plot([1,nfft],[1 1],':');
set(tmp,'color',0.5*ones(1,3));

set(ax, ...
	'xlim',[1,nfft], ...
	'ylim',[-0.2,1.2] ...
);
