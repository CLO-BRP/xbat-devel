function set_migrate_wait_ticks(ticks)

% set_migrate_wait_ticks - set the total number of ticks
% ------------------------------------------------------
%
% set_migrate_wait_ticks(ticks)


%--
% get palette
%--

pal = migrate_wait;

%--
% get PROGRESS userdata
%--

handles = get_control(pal, 'PROGRESS', 'handles');

data = get(handles.axes, 'userdata');

%--
% return if the number of ticks has already been set
%--

if isfield(data, 'ticks') && data.ticks > 0
	return;
end

%--
% set the number of ticks
%--

data.ticks = ticks; data.tick = 0;

set(handles.axes, 'userdata', data);
