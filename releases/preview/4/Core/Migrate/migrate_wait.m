function pal = migrate_wait(type, ticks, message)

% migrate_wait - wait for migration to complete
% ---------------------------------------------
% pal = migrate_wait(type, ticks, message)
%
% Input:
% ------
%  type - migration type 
%  ticks - number of ticks
%  message - message to display on header
%
% Output:
% -------
%  pal - handle to figure

%--
% Singleton
%--

name = ['Migrating ...'];

pal = find_waitbar(name);

if ~nargin
	return;
end

if isstr(type) && strcmp(type, 'finish')
	migrate_wait_finish; return;
end

%--
% handle input
%--

if nargin < 3 || isempty(message)
	message = '';
end

if nargin < 2
	ticks = [];
end
	
%--
% update waitbar if it already exists
%--

if ~isempty(pal)
	migrate_wait_update(pal, type, ticks, message); return;
end

%------------------------------
% CREATE WAITBARS
%------------------------------

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'name', 'PROGRESS', ...
	'style', 'waitbar', ...
	'lines', 1, ...
	'align', 'center', ...
	'confirm', 1, ...
	'space', 2 ...
);

control(end + 1) = control_create( ...
	'name', 'close_after_completion', ... 
	'style', 'checkbox', ...
	'space', 0.75, ...
	'value', 0 ...
);

control(end + 1) = control_create( ...
	'name', 'details', ...
	'style', 'separator', ...
	'type', 'header', ...
	'string', 'Details' ...
);

control(end + 1) = control_create( ...
	'name', 'action', ...
	'style', 'listbox', ...
	'lines', 4 ...
);

control(end + 1) = control_create( ...
	'name', 'Events', ...
	'style', 'waitbar', ...
	'lines', 0.75, ...
	'align', 'center', ...
	'update_rate', 1, ...
	'confirm', 1, ...
	'space', 2 ...
);
	

%--
% render waitbar
%--

opt = waitbar_group; opt.show_after = 0; opt.bottom = 0; opt.update_rate = 0.25;

pal = waitbar_group(name, control, [], [], opt);

%--
% initialize progress
%--

migrate_wait_update(pal, 'PROGRESS', 0);


%-----------------------------------------------------------
% MIGRATE_WAIT_UPDATE
%-----------------------------------------------------------

function migrate_wait_update(pal, type, ticks, message)

if nargin < 4 || isempty(message)
	message = '';
end

%--
% reset waitbar
%--

if ~isempty(ticks)	
	reset_bar(pal, type, ticks, message); return;
end	

%--
% update waitbar(s)
%--

update_bar(pal, type, message);

if strcmpi(type, 'events')
	waitbar_update(pal, 'PROGRESS', 'value', []); return;
end

str = ['Migrating ', string_singular(type), ' "', message, '" '];

update_bar(pal, 'PROGRESS', str);

%--
% update listbox
%--

handles = get_control(pal, 'action', 'handles');

if isempty(handles)
	return;
end

list = get(handles.uicontrol.listbox(1), 'string');

if isempty(list)
	list = {};
end

list(1:end-30) = [];

list = {list{:}, [str, ' ...']};

set(handles.uicontrol.listbox(1), 'string', list, 'value', numel(list));


%-----------------------------------------
% UPDATE_BAR
%-----------------------------------------

function update_bar(pal, name, message)

update_message = 1;

persistent previous;

if isempty(previous)
	previous = struct();
end

if isempty(message) || (isfield(previous, name) && strcmp(message, previous.(name)))
	update_message = 0; 
else
	previous.(name) = message;
end

%--
% get waitbar axes userdata
%--

handles = get_control(pal, name, 'handles');

if isempty(handles)
	return;
end

data = get(handles.axes, 'userdata');

if ~isfield(data, 'ticks') || ~isfield(data, 'tick')
	return;
end

%--
% update or reset userdata
%--

data.tick = min(data.tick + 1, data.ticks); 

set(handles.axes, 'userdata', data);

%--
% update waitbar
%--

if data.ticks
	value = data.tick / data.ticks;
else
	value = 0;
end
	
waitbar_update(pal, name, ...
	'value', value ...
);

if nargin < 2 || ~update_message || isempty(message)
	return;
end

% NOTE: a simple message will suffice for the main progress bar

waitbar_update(pal, name, ...
	'message', message ...
);


%-----------------------------------------
% RESET_BAR
%-----------------------------------------

function reset_bar(pal, name, ticks, message)

%--
% get waitbar axes userdata
%--

handles = get_control(pal, name, 'handles');

if isempty(handles)
	return;
end

data = get(handles.axes, 'userdata');

%--
% initialize 'tick' and 'ticks' fields
%--

data.ticks = ticks;

data.tick = 0;

%--
% update waitbar
%--

% message = [name, ': ', message];

waitbar_update(pal, name, ...
	'value', 0, ...
	'message', message ...
);

set(handles.axes, 'userdata', data);


%-----------------------------------------
% GET_WAIT_TYPE
%-----------------------------------------

function types = get_wait_types

types = {'Events', 'Sounds', 'Libraries', 'Users', 'PROGRESS'}; 


%-----------------------------------------
% MIGRATE_WAIT_FINISH
%-----------------------------------------

function migrate_wait_finish

%--
% set progress string to done
%--

done = 'Done!';

waitbar_update(migrate_wait, 'PROGRESS', 'value', 1, 'message', done);

list_handles = get_control(migrate_wait, 'action', 'handles');

str = get(list_handles.obj, 'string'); 

if isempty(str)
	str = {done};
else
	str = {str{:}, done};
end

set(list_handles.obj, 'string', str, 'value', numel(str));

