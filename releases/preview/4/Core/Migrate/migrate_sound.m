function sound = migrate_sound(source, lib)

%--
% update waitbar ticks if necessary
%--

content = what_ext(fullfile(source, 'Logs'), 'mat'); 

set_migrate_wait_ticks(length(content.mat) + 1);

%--
% increment waitbar
%--

[ignore, name] = fileparts(source); migrate_wait('Sounds', [], name);

%--
% set default output
%--

sound = [];

%--
% handle input
%--

if nargin < 2 || isempty(lib)
	lib = get_active_library;
end

%--
% load old sound file
%--

file = get_sound_file(source);

if ~exist(file, 'file')
	return;
end

contents = load(file);

%--
% check contents of file
%--

if isfield(contents, 'snd')
	sound = contents.snd;
elseif isfield(contents, 'sound')
	sound = contents.sound;
else
	return;
end

if ~isempty(get_library_sounds(lib, 'name', sound_name(sound)))
	return;
end

% NOTE: state is not currently updated

if ~isfield(contents, 'state')
	state = [];
else
	state = contents.state;
end

%--
% update sound and add to library
%--

sound = update_sound(sound);

add_sounds(sound, lib);

%--
% migrate logs
%--

migrate_logs([source, filesep, 'Logs'], lib, sound);