function log = migrate_logs(source, lib, sound)

% migrate_logs - migrate library logs
% ------------------------------------------
% 
% logs = migrate_logs(source, lib, sound)
%
% Input:
% ------
%  source - source logs root
%  lib - library to which to migrate logs
%  sound - sound to which to migrate logs
%
% Output:
% -------
%  logs - migrated logs

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% try to get sound from library if not given
%--

if nargin < 3 || isempty(sound)
	
	[ignore, soundname] = fileparts(fileparts(source));

	sound = get_library_sounds(lib, 'name', soundname);
	
end
	
if isempty(sound)

	str = [
		'Could not add logs from ''', soundname, ...
		''' to library, sound ''', soundname, ...
		''' not found.' ...
		];

	log = []; warning(str); return;

end

%--
% get active library if none given
%--

if nargin < 2 || isempty(lib)
	lib = get_active_library;
end

%--
% migrate single log
%--

if exist(source) == 2
	log = migrate_log(source, lib, sound); return;
end

%--
% migrate logs
%--

content = what_ext(source, 'mat'); log = {};

%--
% set total migrate wait ticks if needed
%--

set_migrate_wait_ticks(length(content.mat));

for k = 1:length(content.mat)		
	
	migrate_wait('Logs', [], content.mat{k});

	log{end + 1} = migrate_log([source, filesep, content.mat{k}], lib, sound);	
	
end

log = [log{:}];


%--------------------------------
% MIGRATE_LOG
%--------------------------------

function log = migrate_log(source, lib, sound)

%--
% migrate and update the log
%--

if exist(source) ~= 2
	log = []; return;
end

%--
% update waitbar
%--

[ignore, log_name] = fileparts(source);

migrate_wait('Events', 0, ['Loading "', log_name, '" ...']);

%--
% load old log from file
%--

oldlog = log_load(source);

if isempty(oldlog)
	log = []; return;
end

%--
% prepare waitbar
%--

str = ['Migrating "', log_name, '" (', int2str(length(oldlog.event)), ' events) ...'];

migrate_wait('Events', length(oldlog.event), str);

%--
% get new log file, exit if it exists already
%--

logfile = get_library_log_file(lib, sound, log_name);

if exist(logfile, 'file')
	log = []; migrate_wait('Events', 0, 'Events'); return;
end

%--
% create new log
%--

log = log_create(logfile, ...
	'sound', sound, ...
	'author', lib.author ...
);

%--
% update events and append them
%--

log = log_append(log, update_event(oldlog.event));

%--
% clear waitbar
%--

migrate_wait('Events', 0, 'Events');


