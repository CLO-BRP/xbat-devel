function [status, result, str] = scite(file)

% scite - use scite to edit file
% ------------------------------
%
% [status, result, str] = scite(file)
%
% Input:
% ------
%  file - file to edit
%
% Output:
% -------
%  status - status
%  result - result
%  str - command string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3232 $
% $Date: 2006-01-20 18:00:37 -0500 (Fri, 20 Jan 2006) $
%--------------------------------

%--------------------
% SETUP
%--------------------

%--
% get tool
%--

tool = get_tool('Scite.exe');

%--------------------
% HANDLE INPUT
%--------------------

% TODO: this may be factored, it needs a name

if nargin

	%--
	% it is an error to use an unavailable tool
	%--
	
	if isempty(tool)
		error('Tool is not available.');
	end

else

	%--
	% output info or open scite
	%--

	if nargout
		status = tool;
	else
		system(['"', tool.file, '" &']);
	end
	
	return;

end

%--
% find file
%--

% NOTE: this may not be the right thing to do

if isempty(strfind(file, filesep))
	file = which(file);
end

%--------------------
% OPEN
%--------------------

%--
% open file for editing
%--

% NOTE: we quote tool and file to avoid space problems

str = ['"', tool.file, '" "', file, '" &'];

[status, result] = system(str);

% NOTE: don't output when called as command

if ~nargout
	clear status;
end
