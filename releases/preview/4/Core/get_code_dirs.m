function out = get_code_dirs(root,exclude)

% get_code_dirs - get relevant directories that contain code
% ----------------------------------------------------------
%
% out = get_code_dirs
%
% Output:
% -------
%  out - cell array of partial paths to directories

% NOTE: we remove root from output

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% set list of directories to exclude
%--

% NOTE: this is very specific to current state of code

if (nargin < 2)
	exclude = {'Users', 'Samples', 'Presets', 'Docs', 'Others', 'private', 'mex_source'};
end

%--
% set application root
%--

if (nargin < 1)
	root = xbat_root;
end

%------------------------------------------
% GET DIRECTORIES
%------------------------------------------

%--
% get directories under root and strip root string
%--

out = scan_dir(root);

out = strrep(out,root,'');

%--
% remove excluded directories
%--

for k = length(out):-1:1
	
	% NOTE: remove directory if it matches any of the excluded names
	
	for j = 1:length(exclude)
		if (findstr([filesep, exclude{j}],out{k}))
			out(k) = []; break;
		end
	end
	
	% NOTE: also remove empty directories
	
	if (isempty(out{k}))
		out(k) = [];
	end
	
end

%--
% sort output
%--

% NOTE: the directories come out in reverse alphabetical order, this helps sort

out = flipud(out);

out = sort(out);

% NOTE: consider further sort by depth