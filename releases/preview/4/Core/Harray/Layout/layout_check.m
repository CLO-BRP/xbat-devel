function [flag, str] = layout_check(layout)

% layout_check - check that layout is valid
% ------------------------------------------
%
% [flag, str] = layout_check(layout)
%
% Input:
% ------
%  layout - layout structure
%
% Output:
% -------
%  flag - validity flag
%  str - error message

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

%--
% create persistent units table
%--

persistent PAD_UNITS;

if isempty(PAD_UNITS)
	PAD_UNITS = {'centimeters', 'inches', 'pixels'};
end 

%----------------------------------------------
% HANDLE INPUT
%----------------------------------------------

%--
% handle layout arrays recursively
%--

if length(layout) > 1
	
	for k = 1:length(layout)
		[flag(k),str{k}] = layout_check(layout(k)); str{k} = [int2str(k), '. ', str{k}];
	end
	
	return;
	
end

%----------------------------------------------
% CHECK LAYOUT
%----------------------------------------------

% NOTE: the function returns on the first error found

flag = 1; str = 'Layout is valid.';

%--
% check layout units
%--

if isempty(find(strcmpi(layout.units ,PAD_UNITS), 1))
	flag = 0; str = ['Unrecognized layout units ''', layout.units, '''.']; return;
end

%--
% check layout margins
%--

%--
% check layout rows
%--

m = layout.row.size;

if ~isequal(size(layout.row.frac(:)), [m, 1])
	flag = 0; str = 'Row fraction layout length does not match row size.'; return;
end

if sum(layout.row.frac) ~= 1
	flag = 0; str = 'Row fraction layout does not sum to 1.'; return;
end

if ~isequal(size(layout.row.pad(:)), [m - 1, 1])
	flag = 0; str = 'Row padding layout length does not match row size.'; return;
end

%--
% check layout columns
%--

n = layout.col.size;

if ~isequal(size(layout.col.frac(:)), [n, 1])
	flag = 0; str = 'Column fraction layout length does not match column size.'; return;
end

if sum(layout.col.frac) ~= 1
	flag = 0; str = 'Column fraction layout does not sum to 1.'; return;
end

if ~isequal(size(layout.col.pad(:)), [n - 1, 1])
	flag = 0; str = 'Column padding layout length does not match column size.'; return;
end
