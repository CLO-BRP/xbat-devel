function desktop = get_windows_desktop

% TODO: figure out how to get this from the registry

%--
% get windows info
%--

info = get_windows_info;

if isempty(info)
	desktop = ''; return;
end

%--
% get user desktop folder
%--

desktop = ['C:\Documents and Settings\', info.user, '\Desktop'];

% NOTE: check the directory exists

if ~exist_dir(desktop)
	desktop = '';
end
