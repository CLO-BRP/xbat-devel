function in = remove_field(in,fields)

% remove_fields - remove existing fields from struct
% --------------------------------------------------
%
% out = remove_field(in,fields)
%
% Input:
% ------
%  in - input struct
%  fields - fields to remove
%
% Output:
% -------
%  out - updated struct

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% put string field in cell
%--

if ischar(fields)
	fields = {fields};
end

%-----------------------
% REMOVE FIELDS
%-----------------------

%--
% remove existing fields from struct
%--

for k = 1:numel(fields)
	
	if (isfield(in,fields{k}))
		in = rmfield(in,fields{k});
	end

end