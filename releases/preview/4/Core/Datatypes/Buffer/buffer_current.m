function [content,updated] = buffer_current(buf)

% buffer_current - get current buffer entry 
% ----------------------------------------
%
% [content,updated] = buffer_current(buf)
%
% Input:
% ------
%  buf - buffer
%
% Output:
% -------
%  content - content of current entry
%  updated - time current entry was updated

% get content at current index

content = buf.content{buf.index};

% get when content was updated if needed

if (nargout > 1) 
	updated = buf.updated(buf.index);
end