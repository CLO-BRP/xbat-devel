function value = xbat_developer(value)

% xbat_developer - xbat developer state
% -------------------------------------
%
% value = xbat_developer(value)
%
% Input:
% ------
%  value - value to set
%
% Output:
% -------
%  value - value of environment variable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1631 $
% $Date: 2005-08-23 12:41:39 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

%--
% set name and default
%--

name = mfilename; % NOTE: filename is used as variable name

if (~nargin)
	default = 0;
end

%--
% get or set value
%--

switch (nargin)
	
	case (0)
		
		%--
		% get value if not set set default
		%--
		
		value = get_env(name);
		
		if (isempty(value))
			value = default; set_env(name,value);
		end

	case (1)
		
		% NOTE: this is to allow the command form of the function call
		
		if (ischar(value))
			value = eval(value);
		end
		
		set_env(name,value);

end