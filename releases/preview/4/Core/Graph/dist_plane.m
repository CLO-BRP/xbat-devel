function D = dist_plane(x,y)

% dist_plane - distance matrix for plane points 
% ---------------------------------------------
%
% D = dist_plane(x,y)
%
% Input:
% ------
%  x - plane positions (row index)
%  y - plane positions (column index)
%
% Output:
% ------
%  D - distance matrix

%--
% put vectors into complex numbers
%--

if (isreal(x))
	x = x(:,1) + i*x(:,2);
end

if (isreal(y))
	y = y(:,1) + i*y(:,2);
end

%--
% compute distance matrix
%--

% non-hermitian transpose

x = x.';

m = length(x);
n = length(y);

D = abs(repmat(x,[n,1]) - repmat(y,[1,m]));
