function G = graph_distance(V,fun,b)

% graph_distance - create distance graph
% --------------------------------------
%
% G = graph_distance(V,fun,b)
%
% Input:
% ------
%  V - values
%  fun - distance function
%  b - distance linking activity interval
%
% Output:
% -------
%  G - distance network
%    .V - values
%    .E - edges
%

%--
% set values
%--

G.V = V;

%--
% compute distance matrix
%--

D = feval(fun,V,V);

%--
% threshold using activity interval graph
%--

A = thresh(D,b);

%--
% set graph
%--

G.E = sparse_to_edge(A);




