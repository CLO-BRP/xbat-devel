function y = vario_exp(h,c0,c1,a)

% vario_exp - exponential variogram model
% ---------------------------------------
%
% y = vario_exp(h,c0,c1,a)
%   = vario_exp(h,p)
%
% Input:
% ------
%  h - points of evaluation
%  c0,c1,a - model parameters
%  p - structure packed parameters (p.c0,p.c1,p.a)
%
% Output:
% -------
%  g - variogram values

%--
% unpack parameters
%--

if (nargin < 3)
	c1 = c0.c1;
	a = c0.a;
	c0 = c0.c0;
end

%--
% compute function values
%--

y = c0 + (c1 * (1 - exp(-h ./ a)));
