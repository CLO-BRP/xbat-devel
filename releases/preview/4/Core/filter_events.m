function [ix,id] = filter_events(event,varargin)

% filter_events - filter events based on field values
% ---------------------------------------------------
%
% [ix,id] = filter_events(event,'field',value, ...)
%
% Input:
% ------
%  events - array of events
%  field - field name
%  value - field values
%
% Output:
% -------
%  ix - event indices
%  id - event id numbers

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get and check field value pairs
%--

n = length(varargin);

if (mod(n,2))
	disp(' ');
	error('Input fields and values must come in pairs.');
end

p = n/2;

for k = 1:p
	field{k} = varargin{2*k - 1};
	value{k} = varargin{2*k};
end

for k = 1:p
	if (~isstr(field{k}))
		disp(' ');
		error('Field names must be strings.');
	end
end

%--
% apply event selection
%--

for k = 1:p
	
	switch (field{k})
		
		case ('channel')
		
		case ('start_time')
			
		case ('end_time')
			
		case ('duration')
			
		case ('start_freq')
			
		case ('end_freq')
						
		case ('bandwidth')
			
		otherwise
			
	end
	
end
