function scan = page_through(start, stop, duration, overlap)

%--
% handle input
%--

if (nargin < 1) || isempty(start)
	start = 10:11:111;
end

if (nargin < 2) || isempty(stop)
	stop = 15:12:125; stop(end) = 137;
end

if (nargin < 3) || isempty(duration)
	duration = 2;
end

if (nargin < 4) || isempty(overlap)
	overlap = 0;
end

%--
% create scan
%--

scan = scan_create(start, stop); 

scan.page.duration = duration; scan.page.overlap = overlap;
		
%--
% display scan blocks and pages
%--

disp(' '); disp('SCAN BLOCKS AND PAGES');

k = 1; display_block_header(scan, k);

[page, scan] = get_scan_page(scan); 
	
while ~isempty(page)

	disp(page_to_str(page));
	
	if ~page.full
		k = k + 1; display_block_header(scan, k);
	end
	
	[page, scan] = get_scan_page(scan); 
	
end

disp(' ');


%--
% display_block_header
%--

function display_block_header(scan, k)

if k > length(scan.start)
	return;
end

block = [scan.start(k), scan.stop(k)]; str = block_to_str(block); 

disp(' '); disp(str); str_line(str);