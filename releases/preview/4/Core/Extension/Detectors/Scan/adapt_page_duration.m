function scan = adapt_page_duration(scan,elapsed)

% adapt_page_duration - update context scan page duration to achieve goal
% ------------------------------------------------------------------------
%
% scan = adapt_page_duration(scan,elapsed)
%
% Input:
% ------
%  scan - scan
%  elapsed - elapsed time for last page
%
% Output:
% -------
%  scan - updated scan

% TODO: set percent tolerance

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% return if no elapsed or known adapt mode
%--

if (nargin < 2)
	return;
end

modes = {'response','speed'};

if (~ismember(scan.adapt,modes))
	return;
end

%----------------------------
% ADAPT
%----------------------------

%--
% update page duration to acheve adaptation goal
%--

switch (scan.adapt)

	%--
	% achieve response time
	%--
	
	case ('response')

		target = 0.25;
		
		if abs(elapsed - target) <= (scan.tol * target)
			return;
		end
		
		factor = 1.25;
		
		if (elapsed > target)
			factor = 1/factor;
		end
		
		scan.page.duration = factor * scan.page.duration;
	
	%--
	% achieve speed
	%--
	
	case ('speed')

		% NOTE: we need a sequence of relevant values, we evaluate change

end



