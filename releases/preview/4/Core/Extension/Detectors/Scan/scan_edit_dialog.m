function scan = scan_edit_dialog(sound, scan)

% scan_edit_dialog - manually edit a sound scan
% ---------------------------------------------
%
% scan = scan_edit_dialog(sound, scan)
%
% Input:
% ------
%  sound - sound
%  scan - scan
%
% Output:
% -------
%  scan - scan

%-----------------------------
% HANDLE INPUT
%-----------------------------

if nargin < 1
	sound = get_active_sound;
end

if isempty(sound)
	return;
end

%-----------------------------
% SETUP
%-----------------------------

%--
% get default scan
%--

if nargin < 2 || isempty(scan)
	scan = scan_create(0, get_sound_duration(sound), get_default_page_duration(sound));
end

str = scan_info_str(scan);

if ~iscell(str)
	str = {str};
end


%------------------------------
% CREATE DIALOG
%------------------------------

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', 'Scan' ...
);

control(end + 1) = control_create( ...
	'name', 'scan_view', ...
	'alias', 'time', ...
	'label', 1, ...
	'style', 'axes', ...
	'space', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'start', ...
	'style', 'slider', ...
	'type', 'time', ...
	'width', 7 / 15, ...
	'min', 0, ...
	'max', sound.duration, ...
	'space', -2, ...
	'value', 0 ...
);

control(end + 1) = control_create( ...
	'name', 'stop', ...
	'style', 'slider', ...
	'type', 'time', ...
	'width', 7 / 15, ...
	'align', 'right', ...
	'min', 0, ...
	'max', sound.duration, ...
	'value', sound.duration ...
);

control(end + 1) = control_create( ...
	'name', 'scan', ...
	'alias', 'tiles', ...
	'style', 'listbox', ...
	'string', str, ...
	'onload', 1, ...
	'value', 1, ...
	'max', 2, ...
	'space', 0, ...
	'lines', 2 ...
);

control(end + 1) = control_create( ...
	'style', 'buttongroup', ...
	'name', {'add', 'delete'}, ...
	'alias', {'+', '-'}, ...
	'width', 1/3, ...
	'align', 'right', ...
	'lines', 1.25 ...
);

%--
% create dialog
%--

opt = dialog_group; opt.width = 17;

result = dialog_group('Edit ...', control, opt, @scan_edit_callback);

values = result.values;

if isempty(values)
	scan = []; return;
end

scan = result.values.scan_view.scan;


%-------------------------------
% SCAN_EDIT_CALLBACK
%-------------------------------

function result = scan_edit_callback(obj, eventdata, sound)

[control,par] = get_callback_context(obj); 

result = [];

%--
% get scan string and scan
%--

han = get_control(par.handle, 'scan', 'handles');

scan_str = get(han.obj, 'string');

scan = parse_time_interval(scan_str);

%--
% find selected scan interval
%--

val = get_control(par.handle, 'scan', 'value');

ix = find(strcmp(scan_str, val{1}));

switch (control.name)
	
	case {'stop', 'start'}
		
		%--
		% set limits of this interval
		%--
		
		stop = get_control(par.handle, 'stop', 'value');
		
		start = get_control(par.handle, 'start', 'value');
		
		%--
		% enforce positive duration
		%--
		
		if stop < start
			
			switch control.name
				
				case 'start'
					
					start = stop;
					
					set_control(par.handle, 'start', 'value', start); 
					
				case 'stop'
					
					stop = start;
					
					set_control(par.handle, 'stop', 'value', stop);
					
			end
			
		end
		
		scan.start(ix) = start; scan.stop(ix) = stop;
				
	case 'add'
		
		%--
		% add a scan interval
		%--
		
		start = scan.stop(ix); stop = start;
		
		scan.start = [scan.start(1:ix), start, scan.start(ix+1:end)];
		
		scan.stop = [scan.stop(1:ix), stop, scan.stop(ix+1:end)];
		
		ix = ix + 1;
			
	case 'delete'
		
		%--
		% remove a scan interval
		%--
		
		if numel(scan.start) < 2
			return;
		end
		
		scan.start(ix) = []; scan.stop(ix) = [];	
		
		ix = max(ix - 1, 1);
				
end

%--
% sync sliders
%--

set_control(par.handle, 'start', 'value', scan.start(ix));

set_control(par.handle, 'stop', 'value', scan.stop(ix));

%--
% set scan string
%--

scan_str = scan_info_str(scan);

set(han.obj, 'string', scan_str);

set(han.obj, 'value', min(ix, numel(scan_str)));

%--
% store scan in display axes
%--

ax_han = get_control(par.handle, 'scan_view', 'handles');

data = get(ax_han.obj, 'userdata'); data = struct();

data.scan = scan; set(ax_han.obj, 'userdata', data);

%--
% display scan
%--

sli_han = get_control(par.handle, 'stop', 'handles'); duration = get(sli_han.obj, 'max');

set(ax_han.obj, ...
	'layer', 'top', ...
	'box', 'on', ...
	'xlim', [0, duration], ...
	'ylim', [0, 1], ...
	'xtick', [], ...
	'ytick', [] ... 
);

delete(get(ax_han.obj, 'children'));

for k = 1:length(scan.start)
	
	patch('parent', ax_han.obj, ...
		'xdata', [scan.start(k), scan.stop(k), scan.stop(k), scan.start(k), scan.start(k)], ...
		'ydata', [0, 0, 1, 1, 0], ...
		'facecolor', (4 * get(ax_han.obj, 'color') + [0, 0, 0.9]) / 5, ...
		'facealpha', 1 ...
	);

end

set(ax_han.obj, 'box', 'on');
	


