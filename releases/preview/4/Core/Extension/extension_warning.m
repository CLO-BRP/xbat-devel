function extension_warning(ext, str, info)

% extension_warning - produce warning for extension failure
% ---------------------------------------------------------
%
% extension_warning(ext, str, info)
%
% Input:
% ------
%  ext - extension producing message
%  str - message string
%  info - error info struct as provided by 'lasterror'

% TODO: consider adding author and modification date info

% TODO: add some form of logging

%--
% put warning together using extension
%--

type_str = upper(strrep(ext.subtype,'_',' ')); 

name_str = upper(ext.name);

str = [' WARNING: In ', type_str, ' extension ''', name_str, '''. ', str];

n = length(str) + 1; line = str_line(n, '_');

%--
% display warning
%--

disp(' '); 

disp(line);
disp(' '); 
disp(str);
disp(line);

disp(' '); 

if (nargin > 2)

	disp(' MESSAGE:'); 
	disp(' ');
	if isempty(info.identifier)
		disp(['   ', info.message]);
	else
		disp(['   ', info.message, ' (', info.identifier, ')']);
	end

	disp(' ');
	
	disp(' STACK:');
	disp(' ');
	for k = 1:length(info.stack)
		disp(['   ', int2str(k), '. ', stack_line(info.stack(k))]);
	end
	
end

disp(' ');
disp(line);
disp(' ');


function str = stack_line(stack) 

file = strrep(stack.file, xbat_root, '');

link = ~isequal(file, stack.file);

file = strrep(file, matlabroot, '(matlabroot)');

str = [file, ' at line ', int2str(stack.line)];

if link
	str = ['In <a href="matlab:opentoline(''', stack.file, ''',', int2str(stack.line), ')">', '(xbat_root)', str, '</a>'];
else
	str = ['In ', str];
end

	