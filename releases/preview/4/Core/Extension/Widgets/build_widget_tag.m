function tag = build_widget_tag(par, ext)

% build_widget_tag - build tag for widget figure
% ----------------------------------------------
%
% tag = build_widget_tag(par, ext)
%
% Input:
% ------
%  par - parent browser
%  ext - widget extension
%
% Output:
% -------
%  tag - widget figure tag

%---------------------
% HANDLE INPUT
%---------------------

%--
% check for browser and get info
%--

[proper, info] = is_browser(par);

if ~proper
	error('Input handle is not browser handle.');
end

%---------------------
% BUILD TAG
%---------------------

%--
% build tag with common information
%--

type = 'XBAT_WIDGET'; sep = '::';

% NOTE: we match user, library, and sound, change type and add widget name

tag = [type, sep, info.user, sep, info.library, sep, info.sound, sep, ext.name, sep, get_listen(ext)];


%-------------------------------------
% GET_LISTEN
%-------------------------------------

function listen = get_listen(ext)

%--
% get events and initialize listen
%--

event = get_widget_events; listen = '';

%--
% test if we know how to listen to each event
%--

for k = 1:length(event)
	
	% NOTE: this is effectively a 'collapse' on a string not a struct
	
	fun = eval(['ext.fun.on.', strrep(event{k}, '__', '.')]);
	
	if isempty(fun)
		listen(end + 1) = '0';
	else
		listen(end + 1) = '1';
	end
	
end


