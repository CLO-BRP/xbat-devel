function class_types = get_extension_class_types(types)

% get_extension_class_types - get extension class types
% -------------------------------------------------
%
% class_types = get_extension_class_types(types)
%
% Input:
% ------
%  types - list of extension types (def: all extension types)
%
% Output:
% -------
%  class_types - list of extension class types

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% set all available extension types default
%--

if (nargin < 1)
	types = get_extension_types;
else
	if (~iscellstr(types))
		error('Extension type input must be cell array of strings.');
	end
end

%------------------------------
% GET TARGET TYPES
%------------------------------

[ignore,class_types] = strtok(types,'_'); 

class_types = unique(class_types);

for k = length(class_types):-1:1
	
	if (isempty(class_types{k}))
		class_types(k) = []; continue;
	end

	class_types{k} = class_types{k}(2:end);
	
end

