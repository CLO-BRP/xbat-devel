function B = apply_image_filter(B, ext)

% apply_image_filter - apply active image filter
% -----------------------------------------------
% 
% B = apply_image_filter(B, ext)
%
% Input:
% ------
%  B - images to filter
%  ext - image filter extension
%
% Output:
% -------
%  B - filtered images

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2094 $
% $Date: 2005-11-08 19:44:04 -0500 (Tue, 08 Nov 2005) $
%--------------------------------

% NOTE: extension compute functions are expected to handle single images

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% check for opacity parameter
%--

% NOTE: no opacity parameter leads to pure filtered image output

if ~isfield(ext.control, 'opacity')
	alpha = 1;
else
	alpha = ext.control.opacity;
end

%--
% get number of images to process and storage format
%--

% NOTE: multiple images are stored in cell arrays or three dimensional arrays

if iscell(B)
	nch = length(B); type = 'cells';	
elseif (ndims(B) == 3)
	nch = size(B, 3); type = 'sheets';
else	
	nch = 1; type = 'single';
end

%--
% handle float to double conversion
%--

%--
% gather context
%--

% TODO: implement context representation and gathering

context = [];

%------------------------------------------
% APPLY FILTER
%------------------------------------------

%--
% apply filter depending on alpha
%--

% FIXME: there is a problem in making filters active before they are first opened

tic;

switch alpha
	
	%--
	% no filtering
	%--
	
	case 0, return;
		
	%--
	% pure filtered image
	%--
	
	case 1
		
		%--
		% handle multiple and single channel displays
		%--
		
		switch type
			
			case 'single', B = ext.fun.compute(B, ext.parameter, context);
				
			case 'cells'
				
				for k = 1:nch
					B{k} = ext.fun.compute(B{k}, ext.parameter, context);
				end
				
			case 'sheets'
				
				for k = 1:nch
					B(:,:,k) = ext.fun.compute(B(:,:,k), ext.parameter, context);
				end
				
		end
		
	%--
	% alpha blended image
	%--
	
	% TODO: implement and access lower level opacity computation

	otherwise
		
		%--
		% handle multiple and single channel displays
		%--

		switch type
			
			case 'single'
				
				B = combine_images('convex', ...
					B, ext.fun.compute(B, ext.parameter,context), alpha ...
				);
			
			case 'cells'
				
				for k = 1:nch
					B{k} = combine_images('convex', ...
						B{k}, ...
						ext.fun.compute(B{k}, ext.parameter,context), ...
						alpha ...
					);
				end
				
			case 'sheets'
				
				for k = 1:nch
					B(:,:,k) = combine_images('convex', ...
						B(:,:,k), ...
						ext.fun.compute(B(:,:,k), ext.parameter,context), ...
						alpha ...
					);
				end
				
		end
		
end

%--
% coarse profiling info
%--

% NOTE: this informormation could be logged in some way and/or displayed

% t = toc; 
% 
% switch (type)
% 	
% 	case ('single'), [m,n] = size(B); k = 1;
% 		
% 	case ('cells'), [m,n] = size(B{k}); k = length(B);
% 		
% 	case ('sheets'), [m,n,k] = size(B);
% 		
% end
% 
% r = (m * n * k) / (t * 10^6); 
% 
% disp([ ...
% 	'IMAGE FILTER: ', ext.name, ' = ', ...
% 	num2str(t), ' sec (', int2str(m), ' x ', int2str(n), ' x ', int2str(k), ') ', ...
% 	num2str(r), ' MP' ...
% ]);
% disp(' ');


%---------------------------------------------------------
% COMBINE_IMAGES
%---------------------------------------------------------

function Y = combine_images(method, X1, X2, param)

% combine_images - combine two images
% -----------------------------------
%
% Y = combine_images(method, X1, X2, param)
%
% Input:
% ------
%  method - combination method
%  X1, X2 - input images
%  param - parameters for combine
%
% Output:
% -------
%  Y - image combination

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2094 $
% $Date: 2005-11-08 19:44:04 -0500 (Tue, 08 Nov 2005) $
%--------------------------------

% TODO: implement coherent parameter passing

% TODO: implement other forms of combining filtered and input images

switch method
	
	case 'convex', Y = ((1 - param) * X1) + (param * X2);
		
	case 'min', Y = min(X1, X2);
		
	case 'max', Y = max(X1, X2);
		
	% NOTE: non-filtered image output for unrecognized methods, send a message
	
	otherwise, Y = X1;
		
end
