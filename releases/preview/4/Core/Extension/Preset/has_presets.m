function value = has_presets(ext)

% has_presets - test whether extension may have presets
% -----------------------------------------------------
%
% value = has_presets(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - result of test 

%--
% get potential preset types
%--

types = get_preset_types(ext);

%--
% check whether there are any implemented preset types
%--

% NOTE: implemented means extension has controllable parameters of a type

value = [];

for k = 1:length(types)
	
	type = types{k};
	
	% NOTE: there are no tests on whether these functions actually work, this could be done
	
	if isempty(type)
		value(end + 1) = ~isempty(ext.fun.parameter.create) && ~isempty(ext.fun.parameter.control.create);
	else
		value(end + 1) = ~isempty(ext.fun.(type).parameter.create) && ~isempty(ext.fun.(type).parameter.control.create);
	end
	
end

value = any(value);