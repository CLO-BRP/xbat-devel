function [info, file] = preset_save(preset, name, type, tags, notes)

% preset_save - save preset to file
% ---------------------------------
%
% [info, file] = preset_save(preset, name, type, tags, notes)
%
% Input:
% ------
%  preset - preset
%  name - name
%  type - type
%  tags - tags
%  notes - notes
%
% Output:
% -------
%  info - preset file info
%  file - preset file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1627 $
% $Date: 2005-08-23 10:01:37 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default empty tags and notes
%--

if nargin < 5
	notes = {};
end

if nargin < 4
	tags = {};
end

if nargin < 3
	type = '';
end 

if ~is_tags(tags)
	error('Input tags are not proper tags.');
end

%--
% check preset name
%--

if ~proper_filename(name)
	error('Preset name must be proper filename.');
end

%-------------------
% SAVE PRESET
%-------------------

%--
% get preset file location
%--

file = preset_file(preset.ext, name, type);

%--
% update preset
%--

% TODO: implement real tags and notes storage

if ~isempty(tags)
	preset.tags = tags;
end

if ~isempty(notes)
	preset.notes = notes;
end

%--
% save preset file and get info
%--

try
	save(file, 'preset'); info = dir(file);
catch 
	info = [];
end
