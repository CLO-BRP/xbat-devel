function [files, root] = get_preset_files(ext, type)

% get_preset_files - get preset files and root
% --------------------------------------------
%
% [files, root] = get_preset_files(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  files - preset filenames
%  root - presets root

%--
% handle input
%--

if nargin < 2
	type = '';
end

%--
% get preset directory
%--

root = preset_dir(ext, type);

if isempty(root)
	files = cell(0); return;
end

%--
% get preset files
%--

files = get_field(what_ext(root, 'mat'), 'mat');
	
if isempty(files)
	return;
end

files = sort(files);
