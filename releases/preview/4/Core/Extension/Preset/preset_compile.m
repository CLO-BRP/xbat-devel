function preset = preset_compile(pal)

% preset_compile - put together preset given palette handle
% ---------------------------------------------------------
%
% preset = = preset_compile(pal)
%
% Input:
% ------
%  pal - extension palette handle
%
% Output:
% -------
%  preset - compiled preset

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6317 $
% $Date: 2006-08-24 17:02:39 -0400 (Thu, 24 Aug 2006) $
%--------------------------------

% NOTE: this is not robust and needs work, limited use should not break it 

%----------------------------------------
% HANDLE INPUT
%----------------------------------------

%--
% check for extension palette
%--

% NOTE: this extension is not current, but it helps further on

[test, ext] = is_extension_palette(pal);

if ~test
	error('Input handle is not extension palette.');
end
		
%----------------------------------------
% SETUP
%----------------------------------------

%--
% get palette parent
%--

% NOTE: the parent provides the context for the preset

par = get_palette_parent(pal);

data = get_browser(par);

%--
% get preset extension
%--

% NOTE: this extension contains compiled parameters, a superset of the conrolled parameters

[ext, ignore, context] = get_browser_extension(ext.subtype, par, ext.name, data);

%--
% get context elements from parent
%--

sound = context.sound;

types = setdiff(get_extension_types, ext.subtype);

active = get_active_extension(types, par, data);

%----------------------------------------
% CREATE PRESET
%----------------------------------------

% TODO: preset info string for dialog box (?) consider full control preview

%--
% create preset with compiled information
%--

preset = preset_create(ext, ...
	'ext',		ext, ...
	'sound',	sound, ...
	'active',	active ...
);
