function preset = preset_load(ext, name, type)

% preset_load - load preset from file
% -----------------------------------
%
% preset = preset_load(ext, name, type)
%
% Input:
% ------
%  ext - extension
%  name - preset name
%  type - preset type
%
% Output:
% -------
%  preset - preset or name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1627 $
% $Date: 2005-08-23 10:01:37 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% set default preset type
%--

if (nargin < 3)
	type = '';
end

%-------------------------
% LOAD PRESET
%-------------------------

%--
% get preset file
%--

file = preset_file(ext, name, type);

if ~exist(file, 'file')
	error('Preset file does not seem to exist.');
end

%--
% load preset content based on type
%--

% NOTE: this loads the preset variable

load(file);

preset.name = name;
