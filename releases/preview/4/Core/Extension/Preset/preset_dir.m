function root = preset_dir(ext, type)

% preset_dir - compute presets directory for an extension
% -------------------------------------------------------
%
% root = preset_dir(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  root - presets directory

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6200 $
% $Date: 2006-08-17 16:01:12 -0400 (Thu, 17 Aug 2006) $
%--------------------------------

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set and check preset type
%--

if (nargin < 2)
	type = '';
end

types = get_preset_types(ext);

if ~ismember(type, types)
	error(['Unrecognized preset type ''', type, '''.']);
end

%---------------------------
% COMPUTE PRESET ROOT
%---------------------------

%--
% get root presets directory
%--

% NOTE: get type based parameter create function handle 

if isempty(type)
	create = ext.fun.parameter.create;
else
	create = ext.fun.(type).parameter.create;
end

% NOTE: preset root is based on create function or main function

if ~isempty(create)
	fun = functions(create); root = [path_parts(path_parts(fun.file)), filesep, 'Presets'];
else
	fun = functions(ext.fun.main); root = [path_parts(fun.file), filesep, 'Presets'];
end

%--
% append type to root if needed
%--

% NOTE: this leads to special presets being contained in a specially named directory

if ~isempty(type)
	root = [root, filesep, '__', upper(type)];
end

%--
% make sure presets directory exists
%--

root = create_dir(root);

if isempty(root)
	error(['Failed to create preset directory for extension ''', ext.name, '''.']);
end


