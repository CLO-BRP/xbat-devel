function pal = extension_palettes(par, name)

% extension_palettes - open an extension palette by name
% ------------------------------------------------------
%
% pal = extension_palettes(par, name)
%
% Input:
% ------
%  par - parent browser figure handle
%  name - name of palette (corresponds with extension name)
%
% Output:
% -------
%  pal - palette figure handle

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% TODO: consider multiple extensions with same name

%--
% try to get extension type from name
%--

% NOTE: this does not work at the moment

% types = get_extension_types;

types = { ...
	'signal_filter', ...
	'image_filter', ...
	'sound_detector' ...
};

type = '';

for j = 1:length(types)

	if ~isempty(get_extensions(types{j}, 'name', name))
		type = types{j}; break;
	end

end

% NOTE: return empty on failure to find matching extension

if isempty(type)
	pal = []; return;
end

%--
% open extension palette
%--

switch type

	% TODO: annotations, measures
	
	%--
	% filters
	%--
	
	case {'signal_filter', 'image_filter'}

		pal = browser_filter_menu(par, [name, ' ...']);

	%--
	% detectors
	%--
	
	case 'sound_detector'

		pal = browser_detect_menu(par, [name, ' ...']);

end