function [exts, names] = get_measurements(par, mode)

% get_measurements - get currently available measures
% -----------------------------------------------
%
% [exts, names] = get_measurements(par)
%
% Input:
% ------
%  par - browser handle (def: [])
%
% Output:
% -------
%  exts - measure 'extensions'
%  names - measure names

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% NOTE: 'mode' input is ignored, but kept momentarily for backwards compatibility

%--
% set handle
%--

if nargin < 1
	par = [];
end

%--
% get functions in measures directory
%--

fun = what([extensions_root, filesep, 'Measures']); 

fun = what([fun.path, filesep, 'Event']);

fun = unique(file_ext({fun.m{:}, fun.p{:}}));

%--
% get measure extensions
%--

exts = [];

for k = 1:length(fun)

	%--
	% skip non-extension files
	%--
	
	if ~findstr(fun{k}, '_measure')
		continue;
	end

	%--
	% try to get extension
	%--
	
	% NOTE: this handles extensions that fail to load altogether
	
	try
		ext = feval(file_ext(fun{k}), 'create', par);
	catch
		continue;
	end

	%--
	% add extension to list
	%--
	
	% NOTE: the exception here handles variations in extensions structure
	
	if isempty(exts)
		exts = ext;
	else
		try
			exts(end + 1) = ext;
		catch
			continue;
		end
	end

end

%--
% get measure names
%--

names = {ext.name};

