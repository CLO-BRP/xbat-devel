function fun = value_fun

% value_fun - value callback structure
% ------------------------------------
%
% fun = value_fun
%
% Output:
% -------
%  fun - callback function structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.create = {{'value'}, {'varargin'}};

fun.describe = {{'describe'}, {'context'}};

fun.display = display_fun;

fun.menu = menu_args('value');