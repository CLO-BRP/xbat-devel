function fun = param_fun

% param_fun - parameter functions structure
% -----------------------------------------
%
% fun = param_fun
%
% Output:
% -------
%  fun - function structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: the first cell contains output names, the second cell input names

fun.create = {{'parameter'}, {'context'}}; 

fun.compile = {{'parameter', 'context'}, {'parameter', 'context'}};

fun.control = control_fun('parameter');

fun.menu = menu_args('parameter');