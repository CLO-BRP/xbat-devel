function result = perform_action(target, ext, parameter, context)

% perform_action - perform action on target
% -----------------------------------------
%
% result = perform_action(target, ext, parameter, context)
%
% Input:
% ------
%  target - action target
%  ext - action extension
%  parameter - action parameters
%  context - action context
%
% Output:
% -------
%  result - result of action

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%-------------------------------- 

%--
% action is undefined
%--

if isempty(ext.fun.compute)
	
	result = action_result('undefined', target); return;
	
end

%--
% try to perform action on target
%--

started = clock;
	
try
	
	% NOTE: this line uses all the input variables
	
	output = ext.fun.compute(target, parameter, context);
	
	result = action_result('done', target, output, '', started);
	
catch
	
	result = action_result('failed', target, lasterror, '', started);

end


