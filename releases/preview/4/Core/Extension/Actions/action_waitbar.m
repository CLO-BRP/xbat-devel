function pal = action_waitbar(context, duration)

% action_waitbar - create multiple target action waitbar
% ------------------------------------------------------
%
% pal = action_waitbar(context)
%
% Input:
% ------
%  context - action context
%  duration - duration of target in time
%
% Output:
% -------
%  pal - waitbar handle

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%-----------------------------
% HANDLE INPUT
%-----------------------------

if nargin < 2
	duration = [];
end

%-----------------------------
% WAITBAR CONTROLS
%-----------------------------

%--
% progress waitbar
%--

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Perform Action ...', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'lines', 1.15, ...
	'space', 2 ...
);

% NOTE: this displays time realtime for sound actions

if ~isempty(duration)
	control.units = duration;
end

control(end + 1) = control_create( ...
	'name', 'close_after_completion', ... 
	'style', 'checkbox', ...
	'space', 0.75, ...
	'value', 1 ...
);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'string', 'Details' ...
);

control(end + 1) = control_create( ...
	'name', 'results', ...
	'alias', 'Results', ...
	'lines', 6, ...
	'space', 0, ...
	'confirm', 0, ...
	'style', 'listbox' ...
);

%-----------------------------
% CREATE WAITBAR
%-----------------------------

% TODO: put some string in here to indicate what we are doing

name = [string_ing(context.ext.name), ' ', title_caps(string_plural(context.type)), ' ...'];

opt = waitbar_group; % opt.show_after = 1;

pal = waitbar_group(name, control, [], [], opt);

