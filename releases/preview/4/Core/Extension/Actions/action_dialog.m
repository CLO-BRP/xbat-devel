function out = action_dialog(target, ext, parameter, context)

% action_dialog - present custom action dialog
% --------------------------------------------
%
% out = action_dialog(target, ext, parameter, context)
%
% Input:
% ------
%  target - action target array
%  ext - action extension
%  parameter - action parameters
%  context - action context
%
% Output:
% -------
%  out - dialog output

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'string', 'Parameters', ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'min', 1 ...
);

try
	ext_control = ext.fun.parameter.control.create(parameter, context);
catch
	ext_control = empty(control_create);
end

for k = 1:length(ext_control)
	control(end + 1) = ext_control(k);
end

%--
% configure dialog
%--

opt = dialog_group;

% NOTE: add color according to parent context

if strcmp(ext.subtype, 'event_action')
	header_type = 'sound_browser_palette';
else
	header_type = 'root';
end 

opt.header_color = get_extension_color(header_type);

if ~isempty(ext.fun.parameter.control.options)
	opt = struct_update(opt, ext.fun.parameter.control.options(context));
end

opt.ext = ext;

%--
% present dialog
%--

% TODO: route callback and pass context in the router

par = context.callback.par.handle;

out = dialog_group(ext.name, ...
	control, opt, {@callback_router, ext.fun.parameter.control.callback, context}, par ...
);


%----------------------------------------
% CALLBACK_ROUTER
%----------------------------------------

function callback_router(obj, eventdata, fun, context)

%--
% get callback context
%--

callback = get_callback_context(obj, 'pack');

%--
% call extension specific callback
%--

if ~isempty(fun)
	fun(callback, context);
end

