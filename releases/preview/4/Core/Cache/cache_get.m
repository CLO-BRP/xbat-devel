function cache = cache_get(label)

% cache_get - clear cache entry
% -------------------------------
%
% cache = cache_get(label)
%
% Input:
% ------
%  label - label of cache entry
%
% Output:
% -------
%  cache - cache structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% check for label and get cache entry if it exists
%--

if (nargin < 1)
	
	disp(' ');
	error('Cache label is empty.');

else 
	
	data = get(0,'userdata');
	
	if (isfield(data,'cache') && length(data.cache))
		
		labels = struct_field(data.cache,'label');
		
		ix = find(label == labels);
		if (~isempty(ix))
			cache = data.cache(ix);
		else
			cache = [];
		end
		
	else
		
		cache = [];
		
	end
	
end
		