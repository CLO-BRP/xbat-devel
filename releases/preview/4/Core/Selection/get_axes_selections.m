function [sel, opt] = get_axes_selections(ax)

% get_axes_selections - get selections from axes
% ----------------------------------------------
%
% [sel, opt] = get_axes_selections(ax)
%
% Input:
% ------
%  ax - parent axes
%
% Output:
% -------
%  sel - selections
%  opt - selection options

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%--
% find patch objects in axes
%--

obj = findobj(ax, 'type', 'patch');

if isempty(obj)
	sel = []; opt = []; return;
end

%--
% check for patches tagged as selections
%--

tags = get(obj, 'tag');

for k = length(tags):-1:1
	
	if isempty(strfind(tags{k}, '::SELECTION::'))
		tags(k) = []; obj(k) = [];
	end
	
end

if isempty(obj)
	sel = []; opt = []; return;
end

%--
% get selections from patches
%--

for k = 1:length(obj)
	
	% NOTE: we store options in cell to handle empty options
	
	callback = get(obj(k), 'buttondownfcn'); sel(k) = callback{2}; opt{k} = callback{3};
	
end
