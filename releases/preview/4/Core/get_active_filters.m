function active = get_active_filters(par,data)

% get_active_filters - get currently active filters
% -------------------------------------------------
%
% active = get_active_filters(par,data)
%
% Input:
% ------
%  par - parent browser
%  data - browser data
%
% Output:
% -------
%  active - filter extension structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%--
% check browser input
%--

if (~is_browser(par))
	error('Input handle is not browser handle.'); 
end

%--
% get browser userdata
%--

if ((nargin < 2) || isempty(data))
	data = get_browser(par); 
end

%--
% get active filters
%--

active.signal = get_active_extension('signal_filter',par,data);

active.image = get_active_extension('image_filter',par,data);