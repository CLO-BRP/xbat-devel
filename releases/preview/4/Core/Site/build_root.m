function root = build_root(site)

% BUILD_ROOT root for build output
%
% root = build_root(site)

root = [site_root(site), filesep, 'build'];
