function content = get_extension_content(source, ext)

% GET_EXTENSION_CONTENT get source directory content matching extensions
%
% content = get_extension_content(source, ext)

%--
% initialize content and record initial directory
%--

init = pwd; content = [];

try
	
	%--
	% move to source directory
	%--
	
	cd(source);
	
	%--
	% loop over extensions
	%--
	
	for k = 1:length(ext)

		%--
		% get extension content
		%--
		
		ext_content = dir(ext{k});

		if isempty(ext_content)
			continue;
		end
		
		%--
		% append extension content
		%--
		
		if ~isempty(content)
			content = [content; ext_content];
		else
			content = ext_content;
		end

	end
	
catch
	
	content = [];

end

%--
% return to initial directory 
%--

cd(init);
