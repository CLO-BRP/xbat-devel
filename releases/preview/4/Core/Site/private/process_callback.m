function lines = process_callback(line, data)

% PROCESS_CALLBACK callback to process file markup
%
% lines = process_callback(line, data)

%--
% split line to find segments to process
%--

[seg, process] = line_split(line, '<%.*%>');

%--
% return quickly if no processing is needed
%--

if ~any(process)
	lines = line; return;
end

%--
% loop over segments processing if needed
%--

lines = cell(0);

for k = 1:length(seg)
	
	%--
	% no process segment
	%--
	
	if ~process(k)
		lines{end + 1} = seg{k}; continue;
	end
	
	%--
	% parse command tokens
	%--

	% NOTE: the command is space separated, consider comma separated
	
	tok = str_split(strtrim(seg{k}(3:end - 2)), ' ');
	
	%--
	% switch on command
	%--
	
	switch tok{1}

		%--
		% links
		%--
		
		case 'link'
			
			%--
			% inject data
			%--
			
			% NOTE: the first two tokens cannot be injected
	
			for j = 3:length(tok)
				tok{j} = inject(tok{j}, data);
			end
			
			%--
			% get link info
			%--
			
			% NOTE: the data provides the context for the basic tokens
			
			site = data.model.site; page = data.page; 
			
			type = [tok{2}, 's']; name = tok{3};

			%--
			% find asset and link to it
			%--
			
			switch type

				case {'images', 'scripts', 'styles'}
					
					out = asset_link(data, type, name, tok{4:end});

				case 'pages'
					
					% TODO: move this into page link
					
					target = get_page_by_name(data.model.pages, name);
					
					if isempty(target)
						out = file_readlines( ...
							which('missing_page_warning.html'), {@process_callback, data} ...
						);
					else
						out = page_link(site, page, target, tok{4:end});
					end
				
				otherwise
					
					out = {}; disp(['WARNING: Unknown link type ''', type, '''.']);
					
			end
			
		%--
		% include
		%--
		
		case 'include'

			%--
			% inject data
			%--
			
			% NOTE: the first two tokens cannot be injected
	
			for j = 2:length(tok)
				tok{j} = inject(tok{j}, data);
			end
			
			%--
			% include and process 
			%--

			% NOTE: we give the file another chance by using 'which'

			out = file_readlines(which(tok{2}), {@process_callback, data});

		%--
		% simple variable injection
		%--
		
		otherwise
			
			tok = inject(tok, data);
			
			% NOTE: the first token is the value to inject
			
			out = tok{1};
			
			% NOTE: the second token is a string filter
			
			if length(tok) > 1
				fun = str2func(tok{2}); out = fun(out);
			end

	end

	%--
	% append processing output to output lines
	%--

	% TODO: indicate problem if 'out' is not available
		
	if ischar(out)
		out = {out};
	end
		
	for k = 1:length(out)
		lines{end + 1} = out{k};
	end
	
end