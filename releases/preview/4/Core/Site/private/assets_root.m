function root = assets_root(site, type)

% ASSETS_ROOT root for site assets of given type
%
% root = assets_root(site, type)

if ~is_asset_type(type) || ~exist_site(site)
	root = ''; return;
end 

root = [site_root(site), filesep, type];
