function children = site_children

% SITE_CHILDREN site children directory names
%
% children = site_children

assets = asset_types;

children = {assets{:}, 'build'};
