function file = page_file(site, page)

% PAGE_FILE compute name of output page file
% 
% file = page_file(site, page)

root = [site_root(site), filesep, 'pages'];

file = strrep(page.file, root, build_root(site));
