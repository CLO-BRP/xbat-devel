function tok = inject(tok, data)

% INJECT inject data into tokens
%
% tok = inject(tok, data)

if ischar(tok)
	tok = {tok}; extract = 1;
else
	extract = 0;
end 

for k = 1:length(tok)
	
	if strcmp(tok{k}(1), '$')
		tok{k} = to_str(get_field(data, tok{k}(2:end), []));
	end
	
end

if extract
	tok = tok{1};
end