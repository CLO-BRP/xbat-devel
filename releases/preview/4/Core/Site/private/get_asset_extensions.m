function ext = get_asset_extensions(type)

% GET_ASSET_EXTENSIONS get file extensions of assets of given type
%
% ext = get_asset_extensions(type)

ext = {};

if ~is_asset_type(type)
	return;
end

switch type
		
	case {'fragments', 'pages'}, ext = {'*.html', '*.php'};
			
	case 'images', ext = {'*.gif', '*.jpg', '*.png'};
		
	case 'scripts', ext = {'*.js', '*.php'};
		
	case 'styles', ext = {'*.css'};
		
	otherwise, disp(['WARNING: Asset type ''', type, ''' has no defined extensions.']);
		
end 
