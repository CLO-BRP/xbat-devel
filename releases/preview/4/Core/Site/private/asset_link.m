function link = asset_link(data, type, name, varargin)

% ASSET_LINK link asset from page
%
% link = asset_link(data, type, name, varargin)

%--
% get asset to link
%--

site = data.model.site;

asset = get_asset(site, type, name);
		
%--
% handle missing asset
%--

if isempty(asset)

	if strcmp(type, 'images')
		
		% NOTE: the image name is used by the template
		
		data.name = name;
		
		link = file_readlines( ...
			which('missing_image_warning.html'), {@process_callback, data} ...
		);
	
	else
		
		link = {}; disp(['WARNING: ', title_caps(type) , ' ''', name, ''' not found in ''', site, '''.']);
	
	end

end

%--
% copy asset to build asset directory
%--

root = [build_root(site), filesep, type];

file = [root, filesep, name];

% TODO: compare file modification dates

if ~exist(file, 'file') && ~isempty(create_dir(root))
	
	disp(['Copying ', type(1:end - 1), ' ''', name, ''' to build ...']);
	
	copyfile(asset, file);

end

%--
% create url and attribute strings for assets
%--

page = data.page;

url = [level_prefix(page.level), type, '/', name];

[attr, opt] = parse_tokens(type, varargin);

%--
% create link
%--

switch type
	
	case 'styles'
		
		link = ['<link rel="stylesheet" href="', url, '"', attr, '/>'];
		
	case 'scripts'
		
		link = ['<script type="text/javascript" src="', url, '"', attr, '/>'];
	
	case 'images'
		
		%--
		% image link 
		%--
		
		if ~isfield(opt, 'thumb')
			link = ['<img src="', url, '"', attr, '/>']; return;
		end
		
		% THUMB LINK
		
		%--
		% get thumb image name
		%--
		
		thumb = get_thumb(site, name, opt.thumb);
		
		%--
		% create zoom page
		%--
		
		out = [fileparts(page_file(site, page)), filesep, name, '-zoom.html'];
		
		file_process(out, which('image_zoom_page.html'));
		
		data.style = 'new.css';
		
		data.image = url;
		
		process_page_file(out, data);
		
		%--
		% link thumb image asset and link to zoom page
		%--
		
		linki = asset_link(data, 'images', thumb);
		
		link = ['<a href="', name, '-zoom.html', '">', linki, '</a>'];
		
end
