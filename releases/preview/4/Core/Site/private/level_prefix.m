function str = level_prefix(k) 

% LEVEL_PREFIX prefix to get to base from level
%
% str = level_prefix(k) 

str = ''; 

for j = 1:k
	str = [str, '../'];
end