function types = asset_types

% ASSET_TYPES asset type names
%
% types = asset_types

types = {'fragments', 'images', 'pages', 'scripts', 'styles'};
