function html = build_nav(model, k, fun)

% BUILD_NAV build top of page navigation
% 
% html = build_nav(model, k, fun)

%--
% set default label fun
%--

% TODO: this should allow for cell callbacks

if nargin < 3
	fun = @prepare_label;
end 

%--
% get page and family
%--

page = model.pages(k);

family = get_family(model, page);

%--
% create multiple level navigation
%--

html = ''; member = [];

for level = 0:max([model.pages.level])

	%--
	% get all visible pages from this level and ancestor
	%--

	if isempty(member)
		pages = model.pages([model.pages.level] == level);
	else
		ix = intersect(member.child, find([model.pages.level] == level)); pages = model.pages(ix);
	end 
	
	pages([pages.position]) = pages;
	
	member = family([family.level] == level);
	
	%--
	% build level navigation list
	%--
	
	html = [html, '<div id="nav-', int2str(level), '">\n<ul>\n'];

	for k = 1:length(pages)
		
		href = strrep(pages(k).file, [model.root, filesep, 'pages', filesep], '');
		
		href = [level_prefix(page.level), strrep(href, filesep, '/')];
		
		if isempty(member)
			html = [html, '\t<li><a href="', href, '">', fun(pages(k).name), '</a></li>\n'];
		else
			if ~strcmp(pages(k).name, member.name)
				html = [html, '\t<li><a href="', href,'">', fun(pages(k).name), '</a></li>\n'];
			else
				html = [html, '\t<li class="here"><a href="', href, '">', fun(pages(k).name), '</a></li>\n'];
			end
		end

	end

	html = [html, '</ul>\n</div>\n'];

end

html = sprintf(html);


%--------------------------------
% GET_FAMILY
%--------------------------------

function family = get_family(model, page)

family = page;

while page.parent
	ancestor = model.pages(page.parent); family(end + 1) = ancestor; page = ancestor;
end


%--------------------------------
% GET_CHILDREN
%--------------------------------

function pages = get_children(model, page)

if isempty(page.child)
	pages = []; return; 
end 

pages = model.pages(page.child);

% NOTE: we get the children in position order

pos = [pages.position]; 

if ~isempty(pos)
	pages(pos) = pages;
end




