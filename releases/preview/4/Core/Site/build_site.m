function  [files, updated] = build_site(site)

% BUILD_SITE build site
%
% [files, updated] = build_site(site)

% TODO: consider passing options here, these may include styles

sites_cache_clear;

%--
% get site model if needed
%--

% NOTE: we consider that input is either site name or model

if ischar(site)
	model = model_site(site);
else
	model = site;
end

%--
% build pages
%--

N = length(model.pages); files = cell(N, 1); updated = zeros(N, 1);

disp(' ');

for k = 1:N
	
	[files{k}, updated(k)] = build_page(model, model.pages(k));
	
	if updated(k)
		disp(['Updated <a href="', files{k}, '">', files{k}, '</a>']);
	end
	
end

disp(' ');

if ~nargout
	clear files;
end 
