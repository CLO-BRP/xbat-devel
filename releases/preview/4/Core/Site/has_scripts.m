function [value, scripts] = has_scripts(varargin)

% HAS_SCRIPTS check that site has scripts
%
% [value, scripts] = has_scripts(site, update)

[value, scripts] = has_assets('scripts', varargin{:});
