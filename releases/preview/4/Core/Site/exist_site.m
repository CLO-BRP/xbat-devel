function [exists, root] = exist_site(site)

% EXIST_SITE check site root directory exists
%
% [exists, root] = exist_site(site)

[root, exists] = site_root(site);
