function model = model_site(site)

% MODEL_SITE compile site model
%
% model = model_site(site)

%--
% create empty site model
%--

% NOTE: in this section we set the 'site' and 'root' fields

model = create_model(site);

if ~exist_site(site)
	return;
end

model.root = site_root(site);

% NOTE: in what follows we set the remaining 'fragments' and 'pages'

%--
% add fragments to model
%--

root = [model.root, filesep, 'fragments'];

model.fragments = model_pages(root);

%--
% add pages to model
%--

root = [model.root, filesep, 'pages'];

model.pages = model_pages(root);


%---------------------------
% CREATE_MODEL
%---------------------------

function model = create_model(site)

%--
% site name and root
%--

model.site = site;

model.root = '';

%--
% site fragments and pages
%--

model.fragments = empty(create_page);

model.pages = empty(create_page);


%---------------------------
% CREATE_PAGE
%---------------------------

function page = create_page(name, file)

if nargin < 2
	file = '';
end

if nargin < 1
	name = '';
end

page.name = name;

page.file = file;

page.id = [];

page.level = 0;

page.position = [];

page.parent = 0;

% NOTE: this will handle the list directory '@name'

page.list = [];

page.child = [];


%---------------------------
% MODEL_PAGES
%---------------------------

function pages = model_pages(root, par, pages)

% MODEL_PAGES compile pages model
%
% pages = model_pages(root, par, pages)

if nargin < 3
	pages = empty(create_page);
end

if nargin < 2
	par = [];
end

%--
% get root contents
%--

% NOTE: it seems like we could model other assets if we wanted to

ext = get_asset_extensions('pages');

content = get_extension_content(root, ext);

children = get_children(root);

order = get_order(root);

% TODO: put where it belongs and allow for partial specification

if isempty(order)
	order = sort(file_ext({content.name}));
end

%--
% model pages
%--

for k = 1:length(content)
	
	%--
	% create page
	%--
	
	% NOTE: create sets the page 'name' and 'file'
	
	name = file_ext(content(k).name);
	
	page = create_page(name, [root, filesep, content(k).name]);
	
	%--
	% add page
	%--
	
	% NOTE: add sets the page 'id', 'parent', and 'level'
	
	if isempty(par)
		pages = add_page(pages, page);
	else
		pages = add_page(pages, page, par);
	end
	
	% NOTE: in the remaining code we set the 'position' and 'child' fields
	
	%--
	% set page position
	%--
	
	pages(end).position = get_page_position(pages(end), order);
	
	%--
	% add list content
	%--
	
	% TODO: this will require improvements to order
	
	if ismember(['@', name], children)
		pages(end).list = model_list(pages(end));
	end
	
	%--
	% add children if we have any
	%--
	
	if ismember(name, children)
		pages = model_pages([root, filesep, name], length(pages), pages);
	end

end


%---------------------------
% ADD_PAGE
%---------------------------

function pages = add_page(pages, page, par)

%--
% compute new page id
%--

if isempty(pages)
	id = 1;
else
	id = pages(end).id + 1;
end

page.id = id;

%--
% add root page
%--

if nargin < 3
	pages(end + 1) = page; return;
end

%--
% add child page
%--

page.parent = par; page.level = pages(par).level + 1; 
	
pages(end + 1) = page;

pages(par).child(end + 1) = length(pages);


%---------------------------
% GET_CHILDREN
%---------------------------

function children = get_children(root)

children = get_folder_names(root);


%---------------------------
% GET_ORDER
%---------------------------

function order = get_order(root) 

file = [root, filesep, 'order.txt']; 

if exist(file, 'file')
	order = file_readlines(file); 
else
	order = {};
end


%---------------------------
% GET_PAGE_POSITION
%---------------------------

function pos = get_page_position(page, order)

pos = find(strcmp(page.name, order));


%---------------------------
% CREATE_LIST
%---------------------------

function list = create_list(page)

list.parent = page;

list.order = [];

list.transform = [];

list.elements = []; 


%---------------------------
% MODEL_LIST
%---------------------------

function list = model_list(page)

%--
% create list
%--

list = create_list(page);

%--
% get list order
%--

%--
% check for list transform
%--

%--
% get list elements
%--

