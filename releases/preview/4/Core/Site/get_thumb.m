function [thumb, file] = get_thumb(site, name, width)

% GET_THUMB get thumbnail image for site image
%
% [thumb, file] = get_thumb(site, name, width)

%--
% set default width 
%--

if nargin < 3
	width = 256;
end

%--
% get parent image
%--

file = get_image(site, name);

if isempty(file)
	return;
end

%--
% get thumb image, create if needed in the image assets directory
%--

[root, name, ext] = fileparts(file);

% TODO: reconsider the naming convention

thumb = [name, '_TH_', int2str(width), ext];

out = [root, filesep, thumb];

if ~exist(out, 'file')
	
	disp('Creating thumb image ...');
	
	info = imfinfo(file); M = width / info.Width;

	imwrite(imresize(imread(file), M, 'bicubic'), out);
	
	% NOTE: this will allow the system to find the new image
	
	sites_cache_clear;

end

while ~exist(out, 'file')
	disp('Waiting on thumb creation ...'); pause(0.1);
end

file = out;

