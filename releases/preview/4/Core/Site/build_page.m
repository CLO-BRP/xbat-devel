function [file, updated] = build_page(model, page)

% BUILD_PAGE build site page file
%
% [file, updated] = build_page(model, page)

%----------------------
% HANDLE INPUT
%----------------------

%--
% get page and index index from input
%--

% NOTE: we assume the page input is a page, name, or index

switch class(page)
	
	case 'struct'
		k = get_page_index(model.pages, page);
		
	case 'char'
		[page, k] = get_page_by_name(model.pages, page);
		
	otherwise
		k = page;
		
end

if isempty(k)
	error('Unable to find page in site model.');
end

if (k < 1) || (k ~= floor(k)) || (k > length(model.pages))
	error('Index must indicate a site model page.');
end

%----------------------
% BUILD PAGE
%----------------------

%--
% get output file handle
%--

file = page_file(model.site, model.pages(k));

[file, updated] = create_file(file);

out = get_fid(file, 'wt');

%--
% add header
%--

header = get_page_by_name(model.fragments, 'header');

if ~isempty(header)
	file_process(out.fid, header.file);
end

%--
% add navigation
%--

file_process(out.fid, build_nav(model, k));

%--
% add page content
%--

% NOTE: page names should not have spaces, these are bad for many reasons

file_process(out.fid, { ...
	' ', ...
	'<div class="clear content">', ...
	['<div id="', page.name, '">'] ...
});

lines = file_readlines(page.file);

if isempty(lines)
	file_process(out.fid, which('empty_page_warning.html'));
else
	file_process(out.fid, page.file);
end

file_process(out.fid, { ...
	'</div>', ...
	'</div>', ...
	' ' ...
});

%--
% add footer
%--

footer = get_page_by_name(model.fragments, 'footer');

if ~isempty(footer)
	file_process(out.fid, footer.file);
end 

%--
% close file
%--

fclose(out.fid);

%--
% process page file as template
%--

% NOTE: we get some basic data for template 

data.model = model;

data.page = page;

data.created = datestr(now);

process_page_file(file, data);


