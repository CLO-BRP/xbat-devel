function event_bdfun(h, m, ix, flag)

% event_bdfun - event display button down function
% ------------------------------------------------
%
% event_bdfun(h, m, ix, flag)
%
% Input:
% ------
%  h - figure handle
%  m - index of log
%  ix - index of event
%  flag - update flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6980 $
% $Date: 2006-10-10 14:23:26 -0400 (Tue, 10 Oct 2006) $
%--------------------------------

% TODO: factor selection drawing code from this function

%--------------------------------------------------
% HANDLE INPUT
%--------------------------------------------------

%--
% set default update
%--

if (nargin < 4) || isempty(flag)
	flag = 0;
end

%--
% get figure handle and log and event indices
%--

if ~nargin

	%--
	% hold current figure handle
	%--
	
	h = gcf;
	
	%--
	% get log and event indices looking at the patch tag
	%--
	
	tag = get(gco, 'tag');
	
	[m, ix] = strtok(tag, '.');
	
	m = str2num(m); ix = str2num(ix(2:end));
	
	%--
	% check for pointer state (this is where the state is stored)
	%--
	
% 	get_custom_pointer(h)
	
end

%--
% get current key to modify selection behavior
%--

% NOTE: selection on most GUI systems is modified by 'shift' and 'control'

% NOTE: should intervening events include events on other visible channels?

% key = get(h,'currentkey');

%--------------------------------------------------
% GET EVENT DATA
%--------------------------------------------------

%--
% get figure userdata
%--

data = get(h, 'userdata');

%--
% get event and linewidth
%--

if isempty(data.browser.parent)
		
	event = data.browser.log(m).event(ix);
	
	line_width = data.browser.log(m).linewidth;
	
else
		
	parent = get(data.browser.parent, 'userdata');
	
	event = parent.browser.log(m).event(ix);
	
	line_width = parent.browser.log(m).linewidth;
	
end

%--------------------------------------------------
% UPDATE EVENT PALETTE
%--------------------------------------------------

%--
% check for open event palette
%--

pal = get_palette(h, 'Event', data);

%--
% update palette display if needed
%--

% NOTE: the number of arguments and flag help distinguish code and GUI calls

if ~isempty(pal) && (~nargin || flag)
			
	%--
	% get event strings from palette
	%--
	
	handles = get_control(pal, 'event_display', 'handles');
	
	listbox = handles.uicontrol.listbox;
	
	str = get(listbox, 'string');
	
	%--
	% find event string among listbox strings
	%--
	
	% NOTE: we rely on strings containing log name and event id
	
	% NOTE: the first pattern is the log name the second the event id
	
	pat = ['"', file_ext(data.browser.log(m).file), ' #" "# ', int2str(event.id), ':"'];
	
	sel = filter_strings(str, pat, 'and');
	
	%--
	% get index of event string in string list if needed and update palette
	%--
	
	if ~isempty(sel)

		% TODO: consider how this may be part of control_update

		ix2 = strmatch(sel, str);

		set(listbox, ...
			'value', ix2, 'listboxtop', ix2 ...
		);		

		double_click('off');
		
		control_callback([], pal, 'event_display');
		
		double_click('on');
		
	end
	
end

%--------------------------------------------------
% UPDATE BROWSER SELECTION
%--------------------------------------------------

% NOTE: this includes browser state and display elements

%--
% update selection state
%--

tmp = data.browser.selection.handle;

if all(ishandle(tmp))
	delete(tmp);
end

% TODO: there is a bug lurking here

if isempty(data.browser.parent)
	
	ax = findobj(data.browser.axes, 'tag', num2str(event.channel));
	
% 	axes(ax);
	
else
	
	TAGS = get(data.browser.axes, 'tag');
	
	tag = [int2str(m) '.' int2str(ix)];
	
	tmp = find(strcmp(TAGS, tag));
	
	ax = data.browser.axes(tmp);
	
% 	axes(ax);

end

%--
% display selection
%--

g = selection_event_display(event, ax, data, m, ix);

%--
% turn on selection edit functions
%--

if isempty(data.browser.parent)
	
	%--
	% update menus
	%--
	
	set(get_menu(data.browser.sound_menu.play, 'Selection'), 'enable', 'on');
	
	tmp = data.browser.edit_menu.edit;
	
	set(get_menu(tmp,'Cut Selection'),'enable','on');
	set(get_menu(tmp,'Copy Selection'),'enable','on');
	set(get_menu(tmp,'Log Selection ...'),'enable','on');
	set(get_menu(tmp,'Delete Selection'),'enable','on');
	
	%--
	% update controls
	%--
	
	control_update(h,'Sound','Selection','__ENABLE__',data);
	
else
	
	
	tmp = data.browser.sound_menu.sound;
	
	set(get_menu(tmp,'Play Event'),'enable','on');
	set(get_menu(tmp,'Play Clip'),'enable','on');
	
	tmp = data.browser.edit_menu.edit;

	set(get_menu(tmp,'Delete Selection'),'enable','on');
	
end

%--
% refresh figure
%--

refresh(h);

%--
% make displayed event current selection in sound
%--

% TODO: check that this solves the double annotation problems

% NOTE: consider whether this is the right behavior. it seems like 'id',
% measurements, and annotations should be cleared

% NOTE: it seems that the 'id' is being used somewhere to distinguish
% logged event selection from simple selections

event.measurement = measurement_create;

event.annotation = annotation_create;

data.browser.selection.event = event;

% NOTE: this is the field that should be used to distinguish logged event
% selections from simple selections, perhaps an 'id' field separate from
% the one in the event is needed

data.browser.selection.log = m;

data.browser.selection.handle = g;

%--
% update userdata
%--

set(h, 'userdata', data);

%--
% update widgets
%--

% NOTE: the code above should be replaced by a 'set_browser_selection' call

update_widgets(h, 'selection__create');


%--------------------------------------------------
% UPDATE ZOOM DISPLAY
%--------------------------------------------------

opt = selection_display;

opt.show = 1;

selection_display(h,event,opt,data);

%--------------------------------------------------
% UPDATE VARIOUS PALETTES
%--------------------------------------------------

%--
% update selection palette controls
%--

set_selection_controls(h,event,'start',data);

%--
% update palettes
%--

% TODO: reconsider this condition

if (isempty(data.browser.parent))
		
	%--
	% update navigate palette
	%--
	
	if (ix > 1)
		control_update(h,'Navigate','Previous Event','__ENABLE__',data);
	else
		control_update(h,'Navigate','Previous Event','__DISABLE__',data);
	end
	
	if (ix < data.browser.log(m).length)
		control_update(h,'Navigate','Next Event','__ENABLE__',data);
	else
		control_update(h,'Navigate','Next Event','__DISABLE__',data);
	end
	
end
