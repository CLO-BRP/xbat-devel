function Y = image_mist(X,p,r)

% image_mist - probabilistically thin 'binary' image
% --------------------------------------------------
%
% Y = image_mist(X,p)
%
% Input:
% ------
%  X - input binary image
%  p - density for zero
%  r - ratio of one and zero density
%
% Output:
% -------
%  Y - thinned image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.2 $
% $Date: 2003-07-16 03:46:10-04 $
%--------------------------------

%--
% set default ratio
%--

if ((nargin < 3) | isempty(r))
	r = 2;
else
	if (r < 1)
		disp(' ');
		error('Ratio must be larger than one.');
	end
end

%--
% set default zero density
%--

if ((nargin < 2) | isempty(p))
	p = 0.1;
end

%--
% map image to fixed range
%--

X = lut_range(X,[0,(r - 1) * p]);

%--
% create uniform random image
%--

N = rand(size(X));

%--
% compute thinned image5
%--

Y = double((X + N) > p);
