function XX = rgb_create(f)

% rgb_create - create rgb image structure
% ---------------------------------------
%
% XX = rgb_create(f)
%
% Input:
% ------
%  f - image filename (def: file dialog)
%
% Output:
% -------
%  XX - image structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:31:28-04 $
%--------------------------------

%--
% use load rgb to get image and file data
%--

if ((nargin < 1) | isempty(f))
	[data,file] = load_rgb;
else
	[data,file] = load_rgb(f);
end

%--
% pack image and file data into structure
%--

XX.type = 'rgb';
XX.file = file.Filename;
XX.data = data;
XX.info = file;