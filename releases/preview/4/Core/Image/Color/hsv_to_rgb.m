function Y = hsv_to_rgb(X)

% hsv_to_rgb - hsv to rgb conversion
% ----------------------------------
%
% Y = hsv_to_rgb(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - hsv image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-02-11 06:56:06-05 $
% $Revision: 1.1 $
%--------------------------------

%--
% get size of input
%--

[m,n,d] = size(X);

%--
% compute according to input
%--

switch (d)
	
	case (1)
	
		%--
		% single color conversion
		%--
		
		if (m*n == 3)
			
			Y = hsv_to_rgb_(X(:)');
			
		%--
		% colormap conversion
		%--
		
		elseif (n == 3)	
			
			Y = hsv_to_rgb_(X);
			
		%--
		% improper input
		%-
		
		else
			
			disp(' ');
			error('Improper input size.');
			
		end
					
	case (3)
	
		%--
		% convert hue to degrees
		%--
		
		% this may not be the right place to do this
		
% 		r = (2 * pi) / 360;
% 		
% 		X(:,:,1) = X(:,:,1) .* r;
		
		%--
		% color image conversion
		%--
				
		X = rgb_vec(X);
				
		Y = hsv_to_rgb_(X);

% 		Y(Y < 0) = 0; this enforcing of positivity is probably related to the angle sign
				
		Y = rgb_reshape(Y,m,n);
		
end
