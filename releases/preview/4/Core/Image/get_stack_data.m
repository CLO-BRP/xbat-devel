function [X,N,h] = get_stack_data(g)

% get_stack_data - get stack data
% -------------------------------
%
% [X,N,h] = get_stack_data(g)
%
% Input:
% ------
%  g - handle to parent figure or axes (def: gcf)
%
% Output:
% ------
%  X - image stack
%  N - image names
%  h - handle to image containing stack

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:53-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set handle
%--

if (~nargin)
	g = gcf;
end

%--
% get stack image handle
%--

h = findobj(g,'tag','IMAGE_STACK');

if (isempty(h))
	error('Parent object does not contain image stack.');
end

%--
% get stack data
%--

X = getfield(get(h,'userdata'),'X');

if (nargout > 1)
	N = getfield(get(h,'userdata'),'N');
end
