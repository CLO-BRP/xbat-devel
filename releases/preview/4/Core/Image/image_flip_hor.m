function Y = image_flip_hor(X)

% image_flip_hor - flip an image along a horizontal axis
% ------------------------------------------------------
%
% Y = image_flip_hor(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  Y - flipped image

%--
% scalar or color  image
%--

d = ndims(X);

switch (d)

	case (2)
        
% 	    Y = X;
% 		Y = flipud(Y);

        Y = flipud(X);
			
	case (3)
		
		Y = X;
		for k = 1:3
			Y(:,:,k) = flipud(X(:,:,k));
		end
		
end
