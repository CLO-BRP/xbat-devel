% Fragment of code to link zooming of images in different figures
% linked zooming of images in same figure may also be useful,
% however generally multiple images in one figure is not a great idea


%--
% get figures with image menus
%--

h = findobj(0,'type','figure');

% keep figures with image menus

for k = length(h):-1:1
	if (~isfield(get(h(k),'userdata'),'image_menu'))
		h(k) = [];
	end
end

%--
% create name strings
%--

for k = 1:length(h)
	
	% standard figure name
	
	t = ['Figure No. ' num2str(h(k))];
	
	% append name
	
	n = get(h(k),'Name');
	if (~isempty(n))
		t = [t ': ' n];
	end
	
	L{k} = t;
	
end

%--
% create uimenu for
%--


