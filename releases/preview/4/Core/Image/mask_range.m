function Z = mask_range(X,r)

% mask_range - double threshold mask
% ----------------------------------
% 
% Z = mask_range(X,r)
%
% Input:
% ------
%  X - input image
%  r - value range interval strings
%
% Output:
% -------
%  Z - range mask

%--
% image or handle
%--

flag = 0;

if (ishandle(X))
    h = X;
    hi = get_image_handles(h);
    if (length(hi) == 1)
        X = get(hi,'cdata');
        flag = 1;
    else
        warning('Figure has more than one image. No computation performed.');
        return;
    end
    
end

%--
% put string in cell
%--

if (isstr(r))
	r = {r};
end

%--
% loop over interval strings
%--

Z = zeros(size(X));

for k = 1:length(r)
	
	%--
	% parse interval string
	%--
	
	[b,t] = parse_interval(r(k));
	
	%--
	% compute interval mask
	%--
	
	switch (t)
		
		case (0)
			Z = Z | ((X > b(1)) & (X < b(2)));
			
		case (1)
			Z = Z | ((X > b(1)) & (X <= b(2)));	
			
		case (2)
			Z = Z | ((X >= b(1)) & (X < b(2)));
			
		case (3)
			Z = Z | ((X >= b(1)) & (X <= b(2)));
			
	end

end

%--
% convert logical to uint8
%--

Z = uint8(Z);

%--
% update displayed image
%--

if (flag & ~nargout)
    figure(h);
    set(hi,'cdata',Z);
	set(gca,'clim',fast_min_max(Z));
end
		



	

