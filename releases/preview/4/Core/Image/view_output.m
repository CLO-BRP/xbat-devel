function flag = view_output(flag)

% view_output - get and set output display flag
% ---------------------------------------------
%
% flag = view_output(flag)
%
% Input:
% ------
%  flag - output display flag
% 
% Output:
% -------
%  flag - output display flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:40-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set environment variable
%--

if (~nargin)
	flag = get_env('view_output');
else
	set_env('view_output',flag);
end
