function test_mask(X,Z)

% test_mask - test mask display options
% -------------------------------------
% 
% test_mask(X,Z)
%
% Input:
% ------
%  X - input image
%  Z - mask image

opt = mask_gray_color;

alpha = {0.2, 0.4, 0.6};

color = { ...
	[1 1 0], ...
	[1 0 1], ...
	[0 1 1] ...
};

bound = {-1, 0, 1};

M = max(X(:));

for i = 1:length(color)
for j = 1:length(bound)
for k = 1:length(alpha)
	
	opt.color = color{i};
	opt.bound = bound{j};
	opt.alpha = alpha{k};
	
	Y = mask_gray_color((M - X),Z,opt);
	
	fig; image_view(Y);
	
end
end
end