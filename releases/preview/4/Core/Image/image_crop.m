function [Y,r,c] = image_crop(h,X)

% image_crop - crop a displayed image
% -----------------------------------
%
% [Y,r,c] = image_crop(h,X)
% 
% Input:
% ------
%  h - axes handle for displayed image (def: gca)
%  X - image to crop (def: displayed image)
%
% Output:
% -------
%  Y - cropped image
%  r - row limits from axes
%  c - column limits from axes

%--
% set handle
%--

if (~nargin)
	h = gca;
end 

%--
% set image
%--

if (nargin < 2)
	X = get(findobj(h,'type','image'),'CData');
end

%--
% get indexing from axes handle
%--

p = get(h);

r = p.YLim + [0.5 -0.5];
r = [ceil(r(1)), floor(r(2))];

c = p.XLim + [0.5 -0.5];
c = [ceil(c(1)), floor(c(2))];

%--
% create cropped image
%--

Y = X(r(1):r(2),c(1):c(2),:);
