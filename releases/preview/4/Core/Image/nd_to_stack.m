function Y = nd_to_stack(X)

% nd_to_stack - put multiple channel image planes in stack
% --------------------------------------------------------
%
% Y = nd_to_stack(X)
%
% Input:
% ------
%  X - multiple channel image
% 
% Output:
% -------
%  Y - stack of X data

%--
% get size of input data
%--

[m,n,d] = size(X);

%--
% put separate image planes into cells
%--

for k = 1:d
	Y{k} = X(:,:,k);
end