function root = mingw_root(in)

% mingw_root - set and get root of mingw compiler
% -----------------------------------------------
%
% root = mingw_root(root)
%
% Input:
% ------
%  root - root to set
%
% Output:
% -------
%  root - value of mingw root

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2319 $
% $Date: 2005-12-16 19:26:33 -0500 (Fri, 16 Dec 2005) $
%--------------------------------

%--
% handle set, get, and error
%--

switch (nargin)

	%--
	% get root
	%--

	case (0)

		%--
		% get environment variable
		%--
		
		root = get_env('mingw_root');

		%--
		% try to set default if empty
		%--
		
		if (isempty(root))
			
			% NOTE: this is the recommended installation path
			
			root = [xbat_root, filesep, 'Tools', filesep, 'MinGW'];
			
			if (exist(root,'dir'))
				set_env('mingw_root',root);
			else
				root = '';
			end
			
		end

	%--
	% set root
	%--

	% NOTE: this works like set verify

	case (1)

		%--
		% check input 
		%--
		
		if (~ischar(in))
			error('Proposed path must be string.');
		end
		
		if (~exist(in,'dir'))
			error('Proposed path does not seem to exist.');
		end
		
		%--
		% set path
		%--
		
		root = in;
		
		set_env('mingw_root',root);
		
	%--
	% error
	%--

	otherwise, error('Improper number of input arguments.');

end
