function sound = edit_sound_config(sound,lib)

% edit_sound_config - sound configuration editing
% -----------------------------------------------
%
% sound = edit_sound_config(sound,lib)
%
% Input:
% ------
%  sound - sound to edit
%  lib - parent library (def: active library)
%
% Output:
% -------
%  sound - edited sound

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set default active library
%--

if nargin < 2
	lib = get_active_library;
end

%--
% check whether sound is open
%--

if numel(sound) ~= 1
	return;
end

if sound_is_open(sound)

	str = sprintf([ ...
		'Open sounds cannot be configured.\n', ...
		'Please close ''', name, ''' before configuring.' ...
		]);

	warn_dialog(str, 'Configure'); return;

end

%--
% update sound attributes
%--

sound = sound_attribute_update(sound, lib);

%---------------------------------
% EDIT SOUND CONFIG
%---------------------------------

% TODO: check that sound is closed, otherwise prompt to close and reopen

%--
% present edit configuration dialog
%--

out = sound_config_dialog(sound);

values = out.values;

if isempty(values)
	return;
end

out = sound_load(lib,sound_name(sound)); sound = out.sound;

%--
% update properties
%--

sound.output.class = lower(values.class{1});

if (values.resample && (sound.samplerate ~= values.samplerate))
	sound.output.rate = values.samplerate;
else
	sound.output.rate = [];
end

% sound.time_stamp.enable = values.enable_time_stamps;
% 
% sound.time_stamp.collapse = values.collapse_sessions;

%--
% update tags and notes
%--

sound = set_tags(sound, values.tags);

sound.notes = values.notes;
	
%--
% save sound in library
%--

% NOTE: we may consider discarding parts of the state

sound_save(lib,sound,out.state);
