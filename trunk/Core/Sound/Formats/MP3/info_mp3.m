function out = info_mp3(f, table_length)

% info_mp3 - get sound file info
% ------------------------------
%
% info = info_mp3(f, table_length)
%
% Input:
% ------
%  f - file location
%
%  table_length - number of entries in seek table
%
% Output:
% -------
%  info - format-specific info structure

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

out = sound_info_mex(f);

if out.frames < 1
	error('File contains no vaild frames');
end

