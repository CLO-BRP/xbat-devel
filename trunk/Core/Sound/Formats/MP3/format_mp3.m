function format = format_mp3

% format_mp3 - create format structure
% ------------------------------------
%
% format = format_mp3
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% create format structure
%--

format = format_create;

%--
% fill format structure
%--

format.name = 'MPEG-1 Layer 3 (MP3)';

format.home = 'http://lame.sourceforge.net/';

format.ext = {'mp3'};

format.info = @info_mp3;

format.read = @read_mp3;

format.write = @write_mp3;

format.encode = @encode_mp3;

format.decode = @decode_mp3;

format.seek = 1;

format.compression = 1;

