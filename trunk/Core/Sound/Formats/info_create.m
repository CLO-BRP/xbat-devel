function info = info_create(varargin)

% info_create - create info structure
% -----------------------------------
%
%  info = info_create
%
% Output:
% -------
%  info - info structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 563 $
% $Date: 2005-02-21 05:59:20 -0500 (Mon, 21 Feb 2005) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE INFO STRUCTURE
%---------------------------------------------------------------------

persistent INFO_PERSISTENT;

if isempty(INFO_PERSISTENT)
	
	%--------------------------------
	% SYSTEM INFORMATION
	%--------------------------------
	
	info.name = [];			% file name
	
	info.format = [];		% human readable format name (match to available formats)
	
	info.file = [];			% file location (full path)
	
	info.date = [];			% last modification date
	
	info.bytes = [];		% size of file in bytes
	
	%--------------------------------
	% FILE CONTENT INFORMATION
	%--------------------------------
	
	info.channels = [];		% number of channel in file
	
	info.samplerate = [];	% samplerate in hertz
	
	info.samplesize = [];	% number of bits per sample
	
	info.samples = [];		% number of samples in file
	
	info.duration = [];		% duration of file (convenience field)
	
	%--------------------------------
	% FORMAT INFORMATION
	%--------------------------------
	
	info.info = [];	% format specific file information
	
	
else
	
	info = INFO_PERSISTENT;
		
end

%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if length(varargin)
	
	%--
	% try to get field value pairs from input
	%--
	
	info = parse_inputs(info, varargin{:});

end


