function [file,ix] = get_previous_file(sound,time)

% get_previous_file - get previous file from specific time
% --------------------------------------------------------
%
% [file,ix] = get_previous_file(sound,time)
%
% Input:
% ------
%  sound - sound
%  time - sound time
%
% Output:
% -------
%  file - previous file from sound time
%  ix - index of file in sound files

% NOTE: previous file is previous or the same

[file,ix] = get_current_file(sound,time);

if (ischar(sound.file) || (ix == 1))
	return;
end

file_time = get_file_times(sound, file);

if file_time >= map_time(sound, 'real', 'slider', time)
	ix = ix - 1;
end

file = sound.file{ix};
	