function [obj,adapter,info] = get_analog_input(name, type)

% get_input - get input object and associated adapter
% ---------------------------------------------------
% 
% [obj,adapter] = get_input(name)
%
% Input:
% ------
%  name - input object adapter name
%
%  type - desired adapter type
%
% Output:
% -------
%  obj - input object
%
%  adapter - adapter
%
%  info - hardware info about this specific adapter
%
%--
% get named input object, create if needed
%--

obj = daqfind('name',name);

if (~isempty(obj))
    	
	obj = obj{1};
	
	adapter = find_adapter(obj);
			  
else
	
	adapters = get_adapters;
	
	%--
	% create and name input object
	%--
	
	if nargin < 2
	
		adapter = adapters(1);
		
	else
		
		adapter = find_adapter(type);
		
	end
	
	obj = analoginput(adapter.type, adapter.ID);
	
end

%--
% Set up initial acquisition parameters
%--

set(obj, ...
	'name', name ...
);

if nargout > 2

	info = daqhwinfo(obj);
	
end