function player = buffered_player(sound, ix, n, ch, speed, filter, buflen)

% buffered_player - create audioplayer for long playback
% ------------------------------------------------------
%
% player = buffered_player(sound, ix, n, ch, speed, filter, buflen)
%
% Input:
% ------
%  sound - sound to play
%  ix - starting index 
%  n - number of samples 
%  ch - channels to read
%  speed - play speed
%  filter - output of 'get_active_filters'
%  buflen - buffer length in samples
% 
% Output:
% -------
%  player - buffered player struct

%------------------------
% HANDLE INPUT
%------------------------

%--
% set default buffer
%--

if (nargin < 7) || isempty(buflen)
	buflen = 10^6;	
end

%--
% set default filter
%--

if (nargin < 6) 
	filter = [];
end

%------------------------
% CREATE PLAYER
%------------------------

%--
% create player state
%--

% NOTE: this keeps the required state to get filtered data and buffer

player.sound = sound;

player.filter = [];

if isfield(filter, 'signal_filter') && ~isempty(filter.signal_filter)
	
	player.filter.ext = filter.signal_filter;

	player.filter.context = filter.signal_context;

end

player.speed = speed;

player.samplerate = [];

player.resample = [];

player.ix = ix;

player.n = n;

player.ch = ch; 

player.buffer = [];

player.buflen = buflen;

player.bix = ix; 

%--
% configure resample if needed
%--

[r, p, q] = get_player_resample_rate(player.speed * get_sound_rate(sound));

player.samplerate = r;

if isempty(p)
	return;
end

player.resample = {@resample, p, q}; 

