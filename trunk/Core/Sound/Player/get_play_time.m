function time = get_play_time(par)

% get_play_time - get current play time from play display
% -------------------------------------------------------
%
% time = get_play_time(par)
%
% Input:
% ------
%  par - player display parent
%
% Output:
% -------
%  time - current play time

%--
% find play time within parent
%--

play_line = findobj(par, 'type', 'line', 'tag', 'PLAY_DISPLAY');

% NOTE: return empty if there is no display

if isempty(play_line)
	time = []; return;
end

%--
% get time from display position
%--

% NOTE: there may be multiple lines, the should have the same time

time = get(play_line(1), 'xdata'); time = time(1);
