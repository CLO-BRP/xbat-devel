function [times,start,sessions] = get_session_boundaries(sound, time, duration)

% get_session_boundaries - get sessions boundaries within limits
% --------------------------------------------------------------
%
% [times] = get_session_boundaries(sound, time, duration)
%
% Inputs:
% -------
%  sound - sound structure
%  time - start time of time extend
%  duration - time extent
%
% Outputs:
% --------
%  times - vector of times


if ~has_sessions_enabled(sound)
	times = []; start = []; sessions = []; return;
end

%--
% get time limits and sessions.  
%--

% NOTE: sound-time/session-time transformations are taken care of in get_sound_sessions()
	
limits = [time, time + duration];

sessions = get_sound_sessions(sound);

%--
% get boundary times
%--

[start_times, start_ix] = in_limits([sessions.start], limits);

[end_times, end_ix] = in_limits([sessions.end], limits);

[times,ix] = sort([start_times, end_times]);

if (nargout)
	start = [ones(size(start_times)), zeros(size(end_times))]; start = start(ix);
end

sessions = sessions([start_ix end_ix]);


%-----------------------------------
% IN_LIMITS
%-----------------------------------

function [values, ix] = in_limits(values, limits)

ix = find((values < limits(2)) & (values > limits(1)));

values = values(ix);