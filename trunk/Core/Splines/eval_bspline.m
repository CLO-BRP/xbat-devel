function s = eval_bspline(t, weights, spline_fun)

% eval_bspline - evaluate b-spline approximation
% ----------------------------------------------
% s = eval_bspline(t, weights, spline_fun)
%
% Input:
% ------
%  t - parameter vector
%  weights - weights
%  spline_fun - function handle to individual spline function
%
% Output:
% -------
%  s - values for t

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

if nargin < 4 || isempty(spline_fun)
	spline_fun = @bspline3;
end

s = zeros(size(t));

for j = 1:length(weights)
	s = s + weights(j) * spline_fun(t, j - 2);
end
