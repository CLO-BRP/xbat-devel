function Y = rgb_permute(X,p)

% rgb_permute - permute color planes
% ----------------------------------
%
% Y = rgb_permute(X,p)
%
% Input:
% ------
%  X - input image
%  p - plane permutation
%
% Output:
% -------
%  Y - output image

for k = 1:3
	Y(:,:,k) = X(:,:,p(k));
end
