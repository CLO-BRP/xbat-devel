function Y = lab_to_rgb(X)

% lab_to_rgb - lab to rgb conversion
% ----------------------------------
%
% Y = lab_to_rgb(X)
%
% Input:
% ------
%  X - lab image
%
% Output:
% -------
%  Y - rgb image

%--
% check size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% compute conversion
%--

Y = xyz_to_rgb(lab_to_xyz(X));
