function Y = rgb_to_hsv(X)

% rgb_to_hsv - rgb to hsv conversion
% ----------------------------------
%
% Y = rgb_to_hsv(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - hsv image

%--
% get size of input
%--

[m,n,d] = size(X);

%--
% map input values into the unit interval if needed
%--

b = fast_min_max(X);

if ((b(1) < 0) | (b(2) > 1))
	
% 	disp(' ');
% 	warning('Converting RGB image to unit values.');
	
	X = lut_range(X,[0 1]);
	
end

%--
% compute according to input
%--

switch (d)

	case (1)
	
		%--
		% single color conversion
		%--
		
		if (m*n == 3)
			
			Y = rgb_to_hsv_(X(:)');
			
		%--
		% colormap conversion
		%--
		
		elseif (n == 3)	
			
			Y = rgb_to_hsv_(X);
			
		%--
		% improper input
		%--
		
		else
		
			disp(' ');
			error('Improper input size.');
			
		end
	
	case (3)
	
		%--
		% color image conversion
		%--
				
		X = rgb_vec(X);
				
		Y = rgb_to_hsv_(X);
			
		Y = rgb_reshape(Y,m,n);
		
		%--
		% map hue to radians
		%--
		
		% this may not be the right place to do this
		
% 		r = 360 / (2 * pi);
% 		
% 		Y(:,:,1) = Y(:,:,1) ./ r;
		
end
