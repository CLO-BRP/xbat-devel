function R = test_integral(n,it)

% test_integral - test box filtering code
% ----------------------------------
%
% R = test_integral(n,it)
%
% Input:
% ------
%  n - size of image
%  it - number of iterations
%
% Output:
% -------
%  R - timing and accuracy results

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 295 $
% $Date: 2004-12-16 13:55:03 -0500 (Thu, 16 Dec 2004) $
%--------------------------------

%------------------------
% HANDLE INPUT
%------------------------

if ((nargin < 2) || isempty(it))
	it = 20;
end

if ((nargin < 1) || isempty(n))
	n = [500,1500];
end

%-------------------------
% RUN TESTS
%-------------------------

%--
% compute and compare filtering performance and accuracy
%--

for k = 1:it
	
	%--
	% create random image
	%--
	
	X = 100000 * rand(n(1),n(2));
	
	%--
	% filter with direct and sparse code
	%--
	
	tic; Y1 = cumsum(cumsum(X,1),2); t1(k) = toc;
	
	tic; Y2 = integral_image_(X); t2(k) = toc;
			
	%--
	% compare output
	%--
	
	E = fast_min_max(Y1 - Y2);
	
	%--
	% pack results and display
	%--
	
	% NOTE: direct computation time, sparse time, speedup ratio, error bounds

	R(k,:) = [t1(k), t2(k), t1(k)/t2(k), E];
	
end

R(it + 1,:) = sum(R,1) ./ it;
	