function Y = mask_saturation(X,Z)

% mask_saturation - saturation representation of mask
% ---------------------------------------------------
% 
% Y = mask_saturation(X,Z)
%
% Input:
% ------
%  X - input image
%  Z - mask image
%
% Output:
% -------
%  Y - saturation mask image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 626 $
% $Date: 2005-02-28 20:22:02 -0500 (Mon, 28 Feb 2005) $
%--------------------------------

%--
% create hsv image
%--

Y = rgb_to_hsv(X);

%--
% mask saturation plane
%--

Y(:,:,2) = mask_apply(Y(:,:,2),Z);

% convert back to rgb

Y = hsv_to_rgb(Y);
