function Y = index_to_rgb(X,C)

% index_to_rgb - convert indexed image to rgb
%
% Y = index_to_rgb(X,C)
%
% Input:
% ------
%  X - indexed image
%  C - colormap
%
% Output:
% -------
%  Y - rgb image
%

%--
% allocate rgb image
%--

[m,n] = size(X);

Y = zeros(m,n,3);

%--
% create rgb image using lut
%--

for k = 1:3
	Y(:,:,k) = lut_apply(X,C(:,k),1,1);
end
