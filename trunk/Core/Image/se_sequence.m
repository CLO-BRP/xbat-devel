function [SE,n] = se_sequence(SE)

% se_sequence - ensure proper structuring element sequence
% --------------------------------------------------------
%
% [SE,n] = se_sequence(SE)
%
% Input:
% ------
%  SE - input structuring element sequence
%
% Output:
% -------
%  SE - actual structuring element sequence
%  n - lenghth of sequence

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--	
% cell array of scale parameters, sequence of scale parameters (scalar or structuring elements)
%--

if (iscell(SE))
	
	% get length of sequence
	
	n = length(SE);
	
	% convert scales to structuring elements if needed
	
	for k = 1:n
		if (isempty(se_rep(SE{k})))
			SE{k} = se_ball(SE{k});
		end
	end
	
% 	% output single strucuring element if needed
% 	
% 	if (n == 1)
% 		SE = SE{1};
% 	end

%--
% array of scale parameters, sequence of scalar scale parameters
%--

elseif (isempty(se_rep(SE)) && (min(size(SE)) == 1))
	
	% get length of sequence
	
	n = length(SE);
	
	% convert scales to structuring element
	
	for k = 1:n
		tmp{k} = se_ball(SE(k));
	end
	SE = tmp;
	
% 	% output single strucuring element if needed
% 	
% 	if (n == 1)
% 		SE = SE{1};
% 	end

%--
% structuring element
%--

else
	
	% wrap structuring element into cell array
	
	tmp = SE;
	clear SE;
	SE{1} = tmp;
	
	% length of sequence
	
	n = 1;
	
end