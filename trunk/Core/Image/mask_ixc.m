function ix = mask_ixc(Z);

% mask_ixc - index code for masks
% -------------------------------
%
% ix = mask_ixc(Z)
%
% Input:
% ------
%  Z - input mask
%
% Output:
% -------
%  ix - index code for input mask

%--
% get size of mask
%--

[m,n] = size(Z);

%--
% compute index code for mask
%--

[i,j] = find(Z);

ix = [m, n; i j];

