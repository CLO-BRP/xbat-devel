function Y = image_thresh(X,T)

% image_thresh - image thresholding
% ---------------------------------
%
% Y = image_thresh(X,T)
%
% Input:
% ------
%  X - input image
%  T - single or multiple increasing thresholds
%
% Output:
% -------
%  M - thresholded or multiply thresholed image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-01-26 15:11:52 -0500 (Wed, 26 Jan 2005) $
% $Revision: 468 $
%--------------------------------

% NOTE: the results of this computation is related to quantization

%--
% check threshold input
%--

if ((length(T) > 1) && any(diff(T) <= 0))
	disp(' ');
	error('Thresholds must be increasing.');
end

%--
% convert input to double if needed
%--

% TODO: mex should handle a variety of types

if (~strcmp(class(X),'double'))
	X = double(X);
end

%--
% use mex for thresholding
%--

Y = image_thresh_(X,T);
