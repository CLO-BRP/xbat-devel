function [file, root] = get_windows_svn

%--
% check for subversion element of path
%--

% NOTE: we assume a default install that adds an element to the path with 'subversion'

path = get_windows_path;

ix = [];

for k = 1:length(path)

	if ~isempty(strfind(lower(path{k}), 'subversion'))
		ix = k; break;
	end
	
end

if isempty(ix)
	file = ''; root = []; return;
end

%--
% get root and file
%--

root = path{ix}; file = fullfile(root, 'svn.exe');