function [status, result] = svn(varargin)

% svn - access subversion command-line client
% -------------------------------------------
%
% [status, result] = svn(varargin)
%
% NOTE: use essentially as from any other command-line
%
% NOTE: the functional syntax above allows you to capture the output

%-------------------
% SETUP
%-------------------

%--
% get tool
%--

if ispc

	% NOTE: we first check if subversion is availble in the system, then in XBAT
	
	file = get_windows_svn;
	
	if isempty(file)
		file = 'svn.exe';
	end
	
else
	
	file = 'svn';
	
end

tool = get_tool(file);

if ~nargin
    status = tool; return;
end

% NOTE: throw an error if we try to use an unavailable tool

if isempty(tool)
    error(['''', tool.name, ''' is not available.']);
end 

executable = tool.file;

editor = get_tool(ternary(ispc, 'scite.exe', 'gedit'));

if ~isempty(editor)
    editor = editor.file;
end
      
%--
% declare known commands
%--

known_commands = svn_commands;

%-------------------
% HANDLE INPUT
%-------------------

if nargin
	command = varargin{1};
else
	command = '';
end

args = [];

if nargin > 1
	args = varargin(2:end);
end
	
%--
% check command
%--

if ~ismember(command, known_commands)
	error(['Unrecognized command ''', command, '''.']);
end

%--
% build basic command string
%--

command_str = ['"', executable, '" ', command];

%--
% add editor command to args if necessary
%--

if ismember(command, {'ci', 'commit'}) && ~isempty(editor) && ~any(strmatch('--editor-cmd', args))
    args{end + 1} = ['--editor-cmd "', editor, '"'];
end

%--
% append arguments to command string
%--

for k = 1:length(args)
	
	% NOTE: try to quote filenames if needed, but not flag or number type arguments
	
	str = args{k};
	
	if ~ismember(str(1), '-1234567890"')
		str = ['"', str, '"'];
	end
	
	command_str = [command_str, ' ', str];

end

%--
% evaluate
%--

if ~nargout
    system(command_str);
else
    [status, result] = system(command_str);
end

%--------------------------------
% SVN_COMMANDS
%--------------------------------

function commands = svn_commands

commands = {...
	'help', ...
	'add', ...
	'blame', ...
	'praise', ...
	'annotate', ...
	'ann', ...
    'cat', ...
    'checkout', ...
	'co', ...
    'cleanup', ...
	'commit', ...
	'ci', ...
    'copy', ...
	'cp', ...
    'delete', ...
	'del', ...
	'remove', ...
	'rm', ...
    'diff', ...
	'di', ...
    'export', ...
    'help', ...
	'?', ...
	'h', ...
    'import', ...
    'info', ...
    'list', ...
	'ls', ...
    'lock', ...
    'log', ...
    'merge', ...
    'mkdir', ...
    'move', ...
	'mv', ...
	'rename', ...
	'ren', ...
    'propdel', ...
	'pdel', ...
	'pd', ...
    'propedit', ...
	'pedit', ...
	'pe', ...
    'propget', ...
	'pget', ...
	'pg', ...
    'proplist', ...
	'plist', ...
	'pl', ...
    'propset', ...
	'pset', ...
	'ps', ...
    'resolved', ...
    'revert', ...
    'status', ...
	'stat', ...
	'st', ...
    'switch', ... 
	'sw', ...
    'unlock', ...
    'update', ...
	'up' ...
};
