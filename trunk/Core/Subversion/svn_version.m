function [version, exported] = svn_version(root)

% svn_version - get SVN version string
% ------------------------------------
%
% [version, exported] = svn_version(root)
%
% Input:
% ------
%  root - root directory
%
% Output:
% -------
%  version - string
%  exported - exported test, direcory is not a working copy

%--
% set default root
%--

if nargin < 1
	root = pwd;
end 

%--
% get version tool
%--

tool = get_tool('svnversion.exe');

% NOTE: declare unknown version and not working copy if we are missing tool

if isempty(tool)
	version = ''; exported = 1; return;
end

%--
% get version string
%--

[status, version] = system(['"', tool.file, '" -n "', root, '"']);

exported = strcmpi(version, 'exported');

