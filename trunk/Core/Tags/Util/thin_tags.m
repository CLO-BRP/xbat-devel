function tags = thin_tags(tags, p)

% thin_tags - select tags with a certain probability
% --------------------------------------------------
%
% tags = thin_tags(tags, p)
%
% Input:
% ------
%  tags - tags to select from
%  p - probability of selection
%
% Output:
% -------
%  tags - thin tags

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default thinning probability
%--

if (nargin < 2)
	p = 0.25;
end

%--
% check tags
%--

if ~is_tags(tags)
	error('Input tags are not valid.');
end

if ischar(tags)
	tags = {tags};
end

%--------------------------
% THIN TAGS
%--------------------------

%--
% get tag indices using probability
%--

ix = find(rand(size(tags)) < p);

%--
% select tags
%--

if isempty(ix)
	tags = {}; return;
end

% NOTE: sorted indices preserve the tag order

tags = tags(ix);
