function [value, tag] = valid_tag(tag, rep)

% valid_tag - check tag validity and enforce it
% ---------------------------------------------
%
% [value, tag] = valid_tag(tag, opt)
%
% Input:
% ------
%  tag - candidate tag
%  rep - space replacement (def: '-')
%
% Output:
% -------
%  value - tag validity test result
%  tag - derived valid tag

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% check for string input
%--

if ~ischar(tag)
	error('Tags are strings.');
end

%--
% set replace any spaces to get valid tag
%--

if any(isspace(tag))
	
	% NOTE: we set 'hyphen' as default space replacement

	if (nargin < 2)
		rep = '-';
	end
	
	value = 0; tag = strrep(strtrim(tag), ' ', rep); return;

else

	value = 1; % NOTE: string with no spaces, we have a tag
	 
end

