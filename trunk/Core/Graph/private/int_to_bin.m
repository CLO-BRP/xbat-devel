function b = int_to_bin(x,k)

% int_to_bin - integer to binary conversion
% --------------------------------------
%
% b = int_to_bin(x,k)
%
% Input:
% ------
%  x - integers
%  k - length of binary representation
%
% Output:
% -------
%  b - binary representations

%--
% set length of representation
%--

if (nargin < 2)
	k = max(floor(log2(x))) + 1;
end

%--
% compute binary representations
%--

n = length(x);

b = zeros(k,n);

y = x(:)';

for j = (k - 1):-1:0
	f = (y - 2^j) >= 0;
	y = y - (2^j)*f;
	b(j + 1,:) = f;
end	

