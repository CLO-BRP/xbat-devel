function [id,str] = xbat_error(id,str)

% xbat_error - create error message and identifier
% ------------------------------------------------
%
% [id,str] = xbat_error(id,str)
%
% Input:
% ------
%  id - simple message identifier
%  str - error message
%
% Output:
% ------
%  id - message identifier
%  str - error message

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

% NOTE: add error logging controlled by an environment variable

% NOTE: this can be used to hide errors from the users while tracking

%--
% set generic identifier
%--

% NOTE: identifier creation needs some documentation

if (isempty(id))
	id = 'XBAT:GENERIC';
else
	id = ['XBAT:' upper(strrep(id,' ','_'))];
end

%--
% append and indent message
%--

sep = char(double('-') * ones(1,length(id)));

str = ['\n' id '\n' sep '\n' str];

str = strrep(str,'\n','\n\t');

str = ['\n', str_line(72), str, '\n', str_line(72)];

% NOTE: should this be done here or outside, should we also display?

str = sprintf(str);