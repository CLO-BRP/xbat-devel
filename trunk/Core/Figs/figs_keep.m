function figs_keep(varargin)

% figs_keep - keep a set of figures and close others
% --------------------------------------------------
% 
% figs_keep x1 ... xN
%
% Input:
% ------
%  x1 ... xN - handles of figures to keep (separated by spaces)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:50-04 $
%--------------------------------

%--
% get open figures
%--

h = findobj(0,'type','figure');

%--
% get figures to delete
%--

for k = 1:nargin
	g(k) = str2num(varargin{k});
end

d = setdiff(h(:)',g(:)');

%--
% close remaining figures
%--

for k = 1:length(d)
	close(d(k));
end
