function x = str_to_double(str)

% str_to_double - stripped down version of str2double
% ---------------------------------------------------
%
% x = str_to_double(str)
%
% Input:
% ------
%  str - string or string cell array
%
% Output:
% -------
%  x - double equivalent

% NOTE: this code gains roughly 20 percent over 'str2double'

% NOTE: consider writing this as mex as well

%--
% return on empty
%--

if (isempty(str))
	x = [];
	return;
end

%--
% handle strings
%--

% TODO: this should probably be rewritten as mex

if (ischar(str))

    lenS = length(str);
	
    % try to get 123, 123i, 123i + 45, or 123i - 45
	
	% NOTE: we only want to consider the first case
	
    [a,n,err,nix] = sscanf(str,'%f %1[ij] %1[+-] %f',4);
	
	%--
    % we only consider the simplest case of a double
	%--
	
    if ((n == 1) && (isempty(err)) && (nix > lenS))
        x = a;
        return;
	end

%--
% handle string cell arrays
%--

elseif (iscellstr(str))
	
    for k = numel(str):-1:1,
        x(k) = str_to_double(str{k});
	end
	
    x = reshape(x,size(str));
	
%--
% handle general cell arrays
%--

elseif (iscell(str))
	
	x = [];
	
	for k = numel(str):-1:1
		
		if iscell(str{k})
			x(k) = NaN;
		else
			x(k) = str_to_double(str{k});
		end
		
	end
	
    x = reshape(x,size(str));
	
%--
% input is not a string
%--

else
	
    x = NaN;
	
end
