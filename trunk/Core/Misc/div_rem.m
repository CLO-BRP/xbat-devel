function [div,rem] = div_rem(x,y)

% div_rem - integer division and remainder
% ----------------------------------------
%
% [div,rem] = div_red(x,y)
%
% Input:
% ------
%  x - value
%  y - divisor
%
% Output:
% -------
%  div - integer division result 
%  rem - remainder after integer division

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2261 $
% $Date: 2005-12-09 17:58:49 -0500 (Fri, 09 Dec 2005) $
%--------------------------------

%--
% check input sizes
%--

if (numel(y) > 1)
	if (numel(x) > 1)
		if (~isequal(size(x),size(y)))
			error('Divisor must be scalar or same size as values.');
		end
	else
		error('Divisor must be scalar for scalar input value.');
	end
end

%--
% compute division and possibly remainder
%--

div = floor(x ./ y);

if (nargout > 1) 
	rem = x - (div .* y);
end