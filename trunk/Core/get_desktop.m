function desktop = get_desktop

% get_desktop - get the desktop folder for the system user
% --------------------------------------------------------
%
% desktop = get_desktop
%
% Output:
% -------
%  desktop - path to the user's desktop

%--
% handle platform specific stuff
%--

switch computer

    case {'GLNX86', 'GLNXA64', 'HPUX', 'SOL2'}

        [status, desktop] = system('echo ~/Desktop'); desktop(end) = '';
        
    case {'MAC'}
        
        desktop = '';

    case {'PCWIN', 'PCWIN64'}

        % TODO: figure out how to get this from the registry

        %--
        % get windows info
        %--
        try

            info = get_windows_info;

            if isempty(info)
                desktop = ''; return;
            end

            %--
            % get user desktop folder
            %--

            desktop = ['C:\Documents and Settings\', info.user, '\Desktop'];

        catch

            desktop = xbat_root;

        end

end

% NOTE: check the directory exists

if ~exist_dir(desktop)
    desktop = '';
end