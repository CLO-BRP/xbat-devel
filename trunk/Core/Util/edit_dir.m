function edit_dir(in, fun)

% edit_dir - edit all files in directory
% --------------------------------------
%
% edit_dir(in, fun)
%
% Input:
% ------
%  in - directory to get files from (def: pwd)
%  fun - edit function

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default edit function
%--

if nargin < 2
	fun = @edit;
end

%--
% set default directory
%--

if nargin < 1
	in = pwd;
end

%--
% check directory exists
%--

if ~exist_dir(in)
	error('Unable to find input directory.');
end 

%--------------------------
% OPEN FILES FOR EDIT
%--------------------------

% TODO: make the edit function return the editable file extensions

% NOTE: consider making the editable file extension as an input

% NOTE: these are the files currently supported by the matlab editor

editable = {'m','c','html'};

%--
% get files in directory
%--

files = dir(in);

% NOTE: we remove dot files and directories

files(strmatch('.', {files.name})) = []; files([files.isdir]) = [];

files = strcat(in, filesep, {files.name});

%--
% call edit function on each of the files
%--

for k = 1:length(files) 
	fun(files{k});
end

