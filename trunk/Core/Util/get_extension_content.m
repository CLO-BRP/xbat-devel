function content = get_extension_content(source, ext, pat)

% get_extension_content - get directory contents by extension
% -----------------------------------------------------------
%
% content = get_extension_content(source, ext, pat)
%
% Input:
% ------
%  source - directory
%  ext - extensions to scan for
%  pat - further pattern to match for name
%
% Output:
% -------
%  content - directory contents

%--
% handle input
%--

if nargin < 3 
	pat = '';
end

%--
% initialize content and record initial directory
%--

init = pwd; content = [];

try
	
	%--
	% move to source directory
	%--
	
	cd(source);
	
	%--
	% loop over extensions
	%--
	
	% NOTE: pack single extension into cell if needed
	
	if ischar(ext)
		ext = {ext};
	end 

	for k = 1:length(ext)

		%--
		% get extension content
		%--
		
		ext_content = dir(strrep([pat, ext{k}], '**', '*'));

		if isempty(ext_content)
			continue;
		end
		
		%--
		% append extension content
		%--
		
		if ~isempty(content)
			content = [content; ext_content];
		else
			content = ext_content;
		end

	end
	
catch
	
	content = [];

end

%--
% return to initial directory 
%--

cd(init);
