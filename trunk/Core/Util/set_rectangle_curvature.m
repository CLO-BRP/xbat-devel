function value = set_rectangle_curvature(handle, radius)

% set_rectangle_curvature - set rectangle curvature in pixels
% -----------------------------------------------------------
%
% value = set_rectangle_curvature(handle, radius)
%
% Input:
% ------
%  handle - rectangle handle
%  radius - curvature radius in pixels
%
% Output:
% -------
%  value - set indicator

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set default radius
%--

if nargin < 2
	radius = 10;
end

%--
% handle multiple handles recursively
%--

% NOTE: no errors are displayed when setting multiple handles

if numel(handle) > 1
	
	value = zeros(size(handle));
	
	for k = 1:numel(handle)
		try
			value(k) = set_rectangle_curvature(handle(k), radius);
		end
	end 
	
	return;

end

%--
% check handle input
%--

if ~ishandle(handle) || ~strcmp(get(handle, 'type'), 'rectangle')
	error('Input must be handle of type rectangle.');
end

%-----------------------
% SET CURVATURE
%-----------------------

% NOTE: when axes have non-trivial aspect we also need 'xlim', 'ylim', and 'dataspectratio'

%--
% get needed parent axes properties
%--

ax.handle = get(handle, 'parent'); 

ax.pos = get_size_in(ax.handle, 'pixels', 'pack');

%--
% get rectangle width and height in pixels
%--

pos = get(handle, 'position');

width = ax.pos.width * pos(3); height = ax.pos.height * pos(4);

%--
% compute curvature values to get desired result
%--

curvature = radius ./ [width, height];

set(handle, 'curvature', curvature);




