function str = md5(in)

% md5 - compute MD5 digest for matlab variable
% --------------------------------------------
%
% str = md5(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  str - MD5 digest string

%--
% get string from input if needed
%--

if ~ischar(in)
	in = to_str(in);
end

%--
% compute MD5 digest using MEX
%--

str = md5_mex(in);
