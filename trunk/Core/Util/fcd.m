function out = fcd(fun)

% fcd - change directory to function parent directory
% ---------------------------------------------------
%
% fcd fun
%
% Input:
% ------
%  fun - function name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%--
% return current directory if no input
%--

if (nargin < 1)
	out = pwd; return;
end

%--
% get function file location 
%--

% NOTE: this step may be fragile, since MATLAB could change the prefix

out = which(fun);

prefix = 'built-in (';

if strmatch(prefix, out)
	out(end) = []; out(1:length(prefix)) = [];
end

%--
% get function parent directory and move to it
%--

out = fileparts(out);

if ~isempty(out)
	cd(out);
end

%--
% supress output
%--

% NOTE: this behavior does hides spelling mistakes, missing files, and final directory

% if ~nargout
% 	clear out;
% end