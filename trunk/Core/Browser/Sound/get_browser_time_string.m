function str = get_browser_time_string(par, time, context)

% get_browser_time_string - get browser time string
% -------------------------------------------------
%
% str = get_browser_time_string(par, time, context)
%
% Input:
% ------
%  par - browser
%  time - browser time, slider time
%  context - extension context
%
% Output:
% -------
%  str - browser time string

%--
% handle input
%--

if nargin < 3
	
	if (nargin < 1) || isempty(par)
		par = get_active_browser;
	end
	
	if isempty(par)
		str = ''; return;
	end 
	
	context = get_extension_context([], par); 
	
end

% NOTE: sometimes we use this because it is hard to get the slider time from context

if (nargin < 2) || isempty(time)
	slider = get_time_slider(par); time = slider.value;
end

%--
% get browser time string
%--

% NOTE: get real time from slider time, this handles sessions

time = map_time(context.sound, 'real', 'slider', time);

% NOTE: we use grid options and real-time to produce string

str = get_grid_time_string(context.display.grid, time, context.sound.realtime);