function export_library(lib,root,view)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default view
%--

% NOTE: the name default is a convention

if ((nargin < 3) || isempty(view))
	view = 'default';
end

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% create library root directory
%--

% NOTE: this will create the libraries root directory if needed

lib_root = create_dir([root, filesep, 'Library', filesep, lib.name]);

if (isempty(lib_root))
	return;
end

%--
% create library page
%--

% data.id = lib.id;

data.lib = lib;

out = 'index.html';

process_template(view,'library',data,lib_root,out);

%--
% export library sounds
%--

sounds = get_library_sounds(lib)

for sound = sounds
	export_sound(lib,sound,lib_root,view);
end