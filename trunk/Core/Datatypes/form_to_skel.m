function skel = form_to_skel(form)

% form_to_skel - get form skeleton
% --------------------------------
%
% skel = form_to_skel(form)
%
% Input:
% ------
%  form - form 
%
% Output:
% -------
%  skel - skeleton

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.7 $
% $Date: 2004-06-08 13:54:58-04 $
%--------------------------------

%------------------------------------------
% HANDLE COMPOSITE TYPES RECURSIVELY
%------------------------------------------

%--
% handle cell arrays
%--

if (iscell(form))
	
	skel = cell(size(form));
	
	for k = 1:numel(form)
		skel{k} = form_to_skel(form{k});
	end
	
	return;
	
end

%--
% handle structures
%--

if (isstruct(form))
	
	% NOTE: we only allow scalar structures
	
	if (length(form) > 1)
		error('Only scalar structures are supported.');
	end
	
	names = fieldnames(form);
	
	if (isempty(find(strcmp(names,'numel'))))
		for k = 1:length(names)
			skel.(names{k}) = form_to_skel(form.(names{k}));
		end
	else
		skel.numel = form.numel;
	end
	
	return;
	
end