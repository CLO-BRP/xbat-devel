function [A, M] = fast_amplitude(X, window, overlap, mode)

%--
% handle input
%--

if nargin < 4
	mode = 'abs';
end

if nargin < 3 || isempty(overlap)
	overlap = 0;
end

if nargin < 2 || isempty(window)
	window = ones(1, 100);
end

if isscalar(window)
	window = ones(1, window);
end

%--
% compute amplitude
%--

nch = size(X, 2); 

switch mode
	
	case 'rms'
	
		% NOTE: square signal to compute RMS, this should happen in-place
		
		X = X.^2;
		
		A = cell(1, nch); M = cell(1, nch);
		
		for k = 1:nch		
			[A{k}, M{k}] = fast_amplitude_mex(X(:, k), window, overlap);
		end
		
		% NOTE: in this case only one column from each cell is informative
		
		for k = 1:nch
			A{k} = A{k}(:, 2); M{k} = M{k}(:, 2);
		end
		
		A = sqrt([A{:}]); M = sqrt([M{:}]);
	
	case 'abs'

		for k = 1:nch	
			[A(:,:,k), M(:,:,k)] = fast_amplitude_mex(X(:, k), window, overlap);	
		end

	otherwise

		error('Unrecognized mode.')

end

