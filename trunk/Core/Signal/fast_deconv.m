function y = fast_deconv(x, h)

% fast_deconv - fast deconvolution
% --------------------------------
% 
% y = fast_deconv(x, h)
%
% Input:
% ------
%  x - input signal
%  h - filter
%
% Output:
% -------
%  y - inverse filtered signal

% NOTE: consider using better fft sizes

n = length(x) + length(h) - 1; N = pow2(nextpow2(n));

X = fft(x, N); H = fft(h, N); Y = X ./ H;

y = real(ifft(Y, N)); y = y(1:n);