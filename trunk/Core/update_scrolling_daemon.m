function flag = set_scrolling_time(obj,par,time)

% set_scrolling_time - update scrolling daemon time table
% -------------------------------------------------------
%
% flag = set_scrolling_time(obj,par,time)
%
% Input:
% ------
%  obj - daemon timer object
%  par - browser figure handle
%  time - time to set as current time
%
% Output:
% -------
%  flag - update success flag

%--
% check that figure still exists
%--

% NOTE: it is possible to do garbage collection here

if (isempty(find(par == get(0,'children'))))
	
	flag = 0;
	return;
	
	% TODO: perhaps display some warning, and choose special flag
	
end

%--
% get timer if needed
%--

if (isempty(obj))
	
	obj = timerfind('name','XBAT Scrolling Daemon');
	
	if (isempty(obj))
		
		flag = 0;
		return;
		
		% TODO: perhaps display some warning, and choose special flag
		
	end
	
end

%--
% update timer handle scroll time table
%--


	


