function site = create_site(name)

% CREATE_SITE create site directory structure
%
% site = create_site(name)

%--
% check site name and whether site exists
%--

[root, exists] = site_root(name);

if isempty(root)
	error('Site name must be a proper filename with no spaces.');
end 

if exists
	disp(['Site ''', name, ''' seems to exist.']); return;
end

%--
% create site
%--

root = create_dir(root);

if isempty(root)
	error(['Failed to create ''', name, ''' site root.']);
end

child = site_children;

for k = 1:length(child)
	
	if isempty(create_dir([root, filesep, child{k}]))
		error(['Failed to create ''', name, ''' site child directory ''', child{k}, '''.']);
	end
	
end

site = name;

if ~nargout
	clear site;
end 
