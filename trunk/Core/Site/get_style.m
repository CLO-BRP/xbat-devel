function style = get_style(site, name)

% GET_STYLE get style file from site by name
%
% style = get_style(site, name)

style = get_asset(site, 'styles', name);
