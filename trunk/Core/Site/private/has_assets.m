function [value, assets] = has_assets(type, site, update)

% HAS_ASSETS check for site assets
%
% [value, assets] = has_assets(type, site, update)
%
% NOTE: this function has reversed arguments to allow for argument passing

if nargin < 3
	update = 0;
end

assets = get_assets(site, type, update); 

value = length(assets);


