function asset = get_asset(site, type, name)

% GET_ASSET get named asset of specified type from site
%
% asset = get_asset(site, type, name)

%--
% get all site assets of type
%--

assets = get_assets(site, type); 

%--
% match and output asset
%--

ix = strmatch(name, assets);

switch length(ix)
	
	case 0, asset = '';
		
	case 1, asset = [assets_root(site, type), filesep, assets{ix}]; 
 
	otherwise, asset = strcat(assets_root(site, type), filesep, assets(ix));
		
end 
