function tool = nircmd

% nircmd - get nircmd tool
% ------------------------
%
% tool = nircmd
%
% Output:
% -------
%  tool - tool file and root

% TODO: reveal commands

tool = get_tool('nircmdc.exe');