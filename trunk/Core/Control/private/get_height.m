function height = get_height(control)

% get_height - compute control height in tile units
% -------------------------------------------------
%
% height = get_height(control)
%
% Input:
% ------
%  control - control
%
% Output:
% -------
%  height - height in tile units

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3397 $
% $Date: 2006-02-03 19:55:30 -0500 (Fri, 03 Feb 2006) $
%--------------------------------

switch (control.layout)
	
	case ('compact')
		height = control.lines + control.space;
		
	otherwise
		height = control.label + control.lines + control.space;

end
