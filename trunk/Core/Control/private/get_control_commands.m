function commands = get_control_commands

% get_control_commands - get list of available control commands
% -------------------------------------------------------------
%
% commands = get_control_commands 
%
% Output:
% -------
%  commands -  commands

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3297 $
% $Date: 2006-01-29 12:34:00 -0500 (Sun, 29 Jan 2006) $
%--------------------------------

%--
% create persistent list of commands if needed
%--

persistent UPDATE_COMMANDS;

if (isempty(UPDATE_COMMANDS))
	
	names = {'show','hide','enable','disable'};
	
	UPDATE_COMMANDS = strcat('__',upper(names),'__')';
	
end

%--
% return list
%--

commands = UPDATE_COMMANDS;