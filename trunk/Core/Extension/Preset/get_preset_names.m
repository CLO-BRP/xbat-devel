function names = get_preset_names(varargin)

% get_preset_names - get preset names
% -----------------------------------
%
% names = get_preset_names(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  names - preset names

names = file_ext(get_preset_files(varargin{:}));