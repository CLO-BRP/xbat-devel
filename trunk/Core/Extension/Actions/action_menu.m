function handles = action_menu(par, type)

% action_menu - attach actions to actions menu
% --------------------------------------------
%
% handles = action_menu(par, type)
%
% Input:
% ------
%  par - action menus parent 
%  type - action type
%
% Output:
% -------
%  handles - handles

%------------------
% HANDLE INPUT
%------------------

if ~is_action_type(type)
	error('Unrecognized action type.');
end

%------------------
% CREATE MENU
%------------------

%--
% prepare parent by clearing children and callback
%--

delete(allchild(par)); set(par, 'callback', []);

%--
% get actions
%--

ext = get_extensions([type, '_action']);

% NOTE: return early creating informative menu

handles = [];

if isempty(ext)
	handles(end + 1) = uimenu(par, 'label', '(No Actions Found)', 'enable', 'off'); return;
end

%--
% create action menus
%--

% TODO: create categorical action menus, perhaps further organization

for k = 1:length(ext)
	
	% NOTE: add menus to control framework, tag information helps for now
	
	handles(end + 1) = uimenu(par, ...
		'label', ext(k).name, ...
		'tag', ext(k).name, ...
		'callback', {@action_dispatch, type} ...
	);

end
