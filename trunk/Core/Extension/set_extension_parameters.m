function ext = set_extension_parameters(ext, varargin)

% set_extension_parameters - set extension parameters
% ---------------------------------------------------
%
% ext = set_extension_parameters(ext, 'field', 'value')
%
% Input:
% ------
%  ext - the extension
%  varargin - a list of field, value pairs
%
% Output:
% -------
%  ext - the extension with modified parameters

%--
% handle input
%--

if mod(numel(varargin), 2)
	error('field, value pairs only');
end

%--
% parse pairs
%--

[field, value] = get_field_value(varargin, fieldnames(ext.parameter));

%--
% convert to a struct and merge
%--

new_params = cell2struct(value, field, 2);

ext.parameter = struct_update(ext.parameter, new_params);

% NOTE: this is currently violating MVC since there is no 'M' (palette)

