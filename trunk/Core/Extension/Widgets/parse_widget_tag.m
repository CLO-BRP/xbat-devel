function info = parse_widget_tag(tag)

% parse_widget_tag - parse widget tag to get info
% -----------------------------------------------
%
% info = parse_widget_tag(tag)
%
% Input:
% ------
%  tag - widget figure tag
% 
% Output:
% -------
%  info - widget info

%--
% configure and parse tag
%--

sep = '::'; fields = {'header', 'type', 'name', 'user', 'library', 'sound', 'listen'};

info = parse_tag(tag, sep, fields);

%--
% parse listen string to make event information explicit
%--

info.listen = parse_listen(info.listen);


%-------------------------------
% PARSE_LISTEN
%-------------------------------

function listen = parse_listen(str)

% parse_listen - parse listener binary indicator string
% -----------------------------------------------------
%
% listen = parse_listen(str)
%
% Input:
% ------
%  str - indicator binary string
%
% Output:
% -------
%  listen - event listener indicator struct

%--
% setup
%--

event = get_widget_events;

% NOTE: does this break for other character sets?

value = double(str) - double('0');

%--
% parse listen into struct
%--

listen = struct; 

for k = 1:length(event)
	listen.(event{k}) = value(k);
end

% NOTE: this is currently the most expensive line

listen = collapse(listen);
