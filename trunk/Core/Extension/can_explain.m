function value = can_explain(ext)

% can_explain - check whether an extension has any explain handlers
% -----------------------------------------------------------------
%
% value = can_explain(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - result of test

value = 0; 

%--
% check for explain branch in functions structure
%--

if ~isfield(ext.fun, 'explain')
	return;
end

%--
% check for any available explain handlers
%--

explain = flatten(ext.fun.explain);

fields = fieldnames(explain);

for k = 1:length(fields)
	if ~isempty(explain.(fields{k}))
		value = 1; return;
	end
end 
