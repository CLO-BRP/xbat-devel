function measurement_display(par, m, ix, data)

% measurement_display - display measurement data in browser
% ---------------------------------------------------------
% 
% handles = measurement_display(par, m, ix, data)
%
% Input:
% ------
%  par - parent browser
%  m - log index
%  ix - event index
%  data - browser data (def: get_browser(par))
%
% Output:
% -------
%  error

% TODO: update display controls (and store) and allow for 'none', 'always
% on', and 'on selection' display

% NOTE: the goal of this function is to update the display for a single
% event

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% skip active detection log
%--

% NOTE: active detections currently have no measures

if ischar(m) || isempty(m)
	return;
end

%--
% get browser state if needed
%--

if nargin < 4 || isempty(data)
	data = get_browser(par);
end

%--
% get display measurements
%--

view = data.browser.measurement.view;

if isempty(view)
	return;
end

%---------------------------
% DISPLAY MEASUREMENTS
%---------------------------

event = data.browser.log(m).event(ix); 

%--
% loop over event measurements
%--

for j = 1:length(event.measurement)

	%--
	% check if we are being displayed
	%--
	
	if ~ismember(event.measurement(j).name, view)
		continue;
	end
	
	%--
	% display measurement
	%--
	
	fun = event.measurement(j).fun; tag = [int2str(m) '.' int2str(ix)];

	try
		handles = fun('display', par, m, ix, data); set(handles, 'tag', tag);
	catch
		nice_catch(lasterror, 'measurement display failed'); 
	end
				
end

