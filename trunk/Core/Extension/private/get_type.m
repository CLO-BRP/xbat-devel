function type = get_type(caller)

% get_type - get type of extension by examining caller location
% -------------------------------------------------------------
%
% type = get_type(caller)
%
% Output:
% -------
%  type - type of caller if computable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% compute type from caller location
%--

% NOTE: the caller location includes the immediate parent directory as well

loc = caller.loc;

switch (length(loc))
	
	case (2)	
		 type = lower(loc{1});
		 
	case (3) 
		type = lower([loc{2}, '_', loc{1}]);
		
	% TODO: implement some way of handling more organization
	
	otherwise
		type = '';
		
end

% NOTE: remove traling 's', this is not very smart code
		
if (type(end) == 's')
	type(end) = [];
end
