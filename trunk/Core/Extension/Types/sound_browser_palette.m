function fun = sound_browser_palette

% sound_browser_palette - function handle structure
% -------------------------------------------------
%
% fun = sound_browser_palette
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.palette = control_fun;