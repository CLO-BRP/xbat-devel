function fun = param_fun(name)

% param_fun - parameter functions structure
% -----------------------------------------
%
% fun = param_fun(name)
%
% Output:
% -------
%  fun - function structure

if ~nargin || isempty(name)
	name = 'parameter';
end

% NOTE: the first cell contains output names, the second cell input names

fun.create = {{name}, {'context'}}; 

fun.compile = {{name, 'context'}, {name, 'context'}};

fun.control = control_fun(name);

fun.load = {{name}, {'store', 'context'}};

fun.menu = menu_args(name);

fun.save = {{}, {name, 'store', 'context'}};