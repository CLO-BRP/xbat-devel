function args = menu_args(name)

% menu_args - argument description for menu functions
% ---------------------------------------------------
%
% args = menu_args(name)
%
% Input:
% ------
%  name - name of object to display in menu (def: 'obj')
%
% Output:
% -------
%  args - argument description

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% set default name
%--

if (nargin < 1)
	name = 'obj';
end

%--
% create argument description
%--

% NOTE: the first cell contains output names, the second cell input names

% NOTE: input contains menu parent handle, object to display, and display options

% NOTE: output contains handles to created menus

args = {{'handles'}, {'par', name, 'context'}};