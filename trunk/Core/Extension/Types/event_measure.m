function fun = event_measure

% event_measure - function handle structure
% -----------------------------------------
%
% fun = event_measure
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.compute = {{'value'}, {'event', 'param', 'context'}};

fun.parameter = param_fun;

fun.value = value_fun;