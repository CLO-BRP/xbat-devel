function y = fast_median(X,Z)

% fast_median - fast median value computation
% -------------------------------------------
% 
% y = fast_median(X,Z)
%
% Input:
% ------
%  X - input image
%  Z - computation mask (def: [])
%
% Output:
% -------
%  y - median value

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5032 $
% $Date: 2006-05-19 15:45:54 -0400 (Fri, 19 May 2006) $
%--------------------------------

%----------------------
% HANDLE INPUT
%----------------------

%--
% set mask and apply mask if needed
%--

if (nargin < 2)
	Z = [];
end

if (~isempty(Z))
	ix = find(Z); X = X(ix);
end

%--
% consider special cases
%--

% NOTE: this duplicates the functionality of 'median'

if isempty(X) || any(isnan(X))
	y = nan; return;
end

%----------------------
% COMPUTE MEDIAN (MEX)
%----------------------

y = fast_median_(X);