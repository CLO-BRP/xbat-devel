function [d,m] = fast_mad(X)

% fast_mad - fast median absolute deviation
% -----------------------------------------
%
% [d,m] = fast_mad(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  d - median absolute deviation
%  m - median

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-12-15 13:52:40 -0500 (Thu, 15 Dec 2005) $
% $Revision: 2304 $
%--------------------------------

%--
% use fast median computation to compute median absolute deviation
%--

m = fast_median(X);

d = fast_median(abs(X - m));
