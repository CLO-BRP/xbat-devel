function y = lognormal_pdf(x,p)

% lognormal_pdf - compute lognormal pdf
% -------------------------------------
% 
% y = lognormal_pdf(x,p)
%
% Input:
% ------
%  x - points to evaluate
%  p - lognormal parameters [mean, deviation]
%
% Output:
% -------
%  y - pdf values

%--
% evaluate pdf at points
%--

z2 = ((log(x) - p(1)) ./ p(2)).^2;

y = (1 ./ (sqrt(2*pi) * p(2) .* x)) .* exp(-z2 ./ 2);

% fig; plot(x,y); axes_scale_bdfun(gca);