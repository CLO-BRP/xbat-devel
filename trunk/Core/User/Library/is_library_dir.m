function value = is_library_dir(in)

% value - checks whether a specified in is a library
% --------------------------------------------------
%
% value = is_library_dir(in)
%
% Input:
% ------
%  in - directory to check (def: pwd)
%
% Output:
% -------
%  value - library directory indicator

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 4391 $
% $Date: 2006-03-27 13:52:56 -0500 (Mon, 27 Mar 2006) $
%--------------------------------

%-------------------------------
% HANDLE INPUT
%-------------------------------

%--
% set default and check for existence
%--

if (nargin < 1)
	in = pwd;
end

if (~exist_dir(in))
	error('Input directory does not exist.');
end

%-------------------------------
% TEST AND RETURN
%-------------------------------

%--
% check for library file
%--

[root,name] = path_parts(in);

file = get_library_file(root,name);

if (~exist(file,'file'))
	value = 0; return;
end

%--
% try to load library file
%--

if (isempty(load_library(file)))
	value = 0; return;
end

%--
% we loaded the library, it's a library
%--

value = 1;




