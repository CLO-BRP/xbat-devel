function [lib, usr] = parse_libname(str)

% parse_libname - parse a libray name into [user, library]
% --------------------------------------------------------
%
% [usr,lib] = parse_libname(str)
%
% Inputs:
% ------
% str - the name string
%
% Outputs:
% --------
% usr - the name of owner of the library
% lib - the name of the library

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 4335 $
% $Date: 2006-03-21 12:12:34 -0500 (Tue, 21 Mar 2006) $
%--------------------------------

if (~nargin || ~isstr(str))
	error('input must exist and be a string');
end

[usr, lib] = strtok(str, filesep);

if isempty(lib)	
	lib = usr;
	usr = [];
	return;	
end

lib = lib(2:end);

