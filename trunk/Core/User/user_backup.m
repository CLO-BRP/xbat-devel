function user_backup(user)

% sound_backup - backup user
% ----------------------------------------
% sound_backup(user)
%
% Input:
% ------
%  user - user

%--
% check for the existence of the backup directory. create if needed
%--

backup_dir = [users_root, filesep, '__BACKUP'];

if (~exist(backup_dir,'dir'))
	mkdir(users_root,'__BACKUP');
end

% NOTE: block on the directory creation, this may not be needed

while(~exist(backup_dir,'dir'))
	pause(0.1);
end

%--
% zip sound directory backup folder with time stamp
%--

in = user_root(user); % location of sound directory

out = [backup_dir, filesep, user.name, '_', datestr(now, 30), '.zip']; % location of backup zip file
	
zip(out,in);