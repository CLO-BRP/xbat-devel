function files = users_root_update

% users_root_update - update users to tolerate root update
% --------------------------------------------------------
%
% files = users_root_update
%
% Output:
% -------
%  files - user files updated

% TODO: consider allowing for 'move' and 'rename'

%--
% get user files
%--

users = get_folder_names(users_root); files = {};

for k = 1:length(users)
	files{end + 1} = get_user_file(users{k});
end

files = files(:);

%--
% update user library references
%--

root = xbat_root;

for k = 1:length(files)
	
	load(files{k});
	
	for j = 1:length(user.library)
		user.library{j} = replace_root(user.library{j}, root);
	end
	
	save(files{k}, 'user');
	
end


function str = replace_root(str, root)

[root, name] = fileparts(root);

ix = strfind(str, [filesep, name, filesep]);

if isempty(ix)
	return;
end

ix = ix(end);

% NOTE: this is to handle the library path filesep issues

str = fullfile(root, str(ix:end));

