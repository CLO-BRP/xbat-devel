function root = sessions_root(user)

% sessions_root - get user sessions root
% --------------------------------------
%
% root = sessions_root(user)
%
% Input:
% ------
%  user - user (def: active user)
%
% Output:
% -------
%  root - sessions root

% NOTE: this function returns the sessions directory, making sure it exists

if (nargin < 1)
	user = get_active_user;
end

root = create_dir([user_root(user), filesep, 'Sessions']);
