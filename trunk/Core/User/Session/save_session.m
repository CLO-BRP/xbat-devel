function [info, file] = save_session(name, user)

% save_session - save user session
% --------------------------------
% 
% [info, file] = save_session(name, user)
%
% Input:
% ------
%  name - session name
%
% Output:
% -------
%  info - session file info
%  file - session file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1869 $
% $Date: 2005-09-28 16:45:31 -0400 (Wed, 28 Sep 2005) $
%--------------------------------

%--
% set session extension
%--

ext = 'txt';

%----------------------
% HANDLE INPUT
%----------------------

%--
% set default user
%--

if (nargin < 2)
	user = get_active_user;
end

%--
% set default session name
%--

% NOTE: consider using dates for session names

if (nargin < 1)
	name = 'last_session';
end

if ~proper_filename(name)
	error('Session name must be a proper filename.');
end

%----------------------
% SAVE SESSION
%----------------------

%--
% capture session content
%--

content = capture_session;

%--
% write session file and get file info
%--

file = [sessions_root(user), filesep, name, '.', ext];

try
	info = file_writelines(file, content);
catch
	info = [];
end

%--
% display session info
%--

if ~nargout && ~isempty(info)
	
	disp(' '); disp(info.name); str_line(info.name);
	
	for k = 1:length(content)
		disp(content{k});
	end

	disp(' ');
	
end
