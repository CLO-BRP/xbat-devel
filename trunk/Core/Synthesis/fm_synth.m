function x = fm_synth(f, a, rate, oversamp)

%--
% handle input
%--

if nargin < 4 || isempty(oversamp)
	oversamp = 2;
end

if nargin < 3 || isempty(rate)
	rate = 3*max(f);
end

if nargin < 2 || isempty(a)
	a = 'constant';
end

%--
% fabricate amplitude envelope if necessary
%--

if ischar(a)
	
	switch a
		
		case 'constant', a = ones(size(f));
			
		case 'tapered', a = hann(length(f));
			
	end
	
end

if length(f) ~= length(a)
	error('f and a must be the same size.');
end
			
%--
% interpolate frequency track
%--

f_up = interp1(f, linspace(1, length(f), oversamp * length(f)), 'spline');

%--
% divide down to correct bandwidth
%--

f_up = 2 * pi * (f_up ./ (rate * oversamp));

%--
% separate into frequency/phase
%--

f0 = f(1); ph = seq_int(f_up - f0);

%--
% modulate
%--

x = sin((f0 * [0:(length(f_up) - 1)]) + ph);

x = resample(x, 1, oversamp);

x = a .* x;
