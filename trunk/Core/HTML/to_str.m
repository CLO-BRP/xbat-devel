function str = to_str(value, sep)

% to_str - convert a vector of values to string
% ---------------------------------------------
%
% str = to_str(value)
%
% Input:
% ------
%  value - value
%
% Output:
% -------
%  str - string

% TODO: allow for two configurable separators

%--
% set default implode separator
%--

if nargin < 2
	sep = '|';
end 

%--
% create row value vector
%--

value = value(:)';

%--
% consider class of value for conversion
%--

str = '';

switch class(value)
	
	case 'char', str = value;
		
	case 'cell'
		
		for k = 1:length(value)
			str{k} = to_str(value{k}, ';');
		end
		
	case 'struct'
		
		if length(value) > 1
			error('Only scalar structs are supported.');
		end
		
		value = flatten_struct(value); field = fieldnames(value);
		
		for k = 1:length(field)		
			str{k} = [field{k}, ':', to_str(value.(field{k}), ';')];
		end
		
	otherwise
		
		if all(value == floor(value))
			
			str = int_to_str(value);
		
		else
			
			str = cell(size(value));
		
			for k = 1:length(value)
				str{k} = num2str(value(k));
			end

		end

end

%--
% implode cell arrays to get string
%--

while iscell(str)
	str = str_implode(str, sep);
end