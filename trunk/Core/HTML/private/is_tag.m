function value = is_tag(in)

% is_tag - test whether input is tag
% -----------------------------------
%
% value = is_tag(in)
%
% Input:
% ------
%  in - input to test
%
% Output:
% -------
%  value - tag indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--
% set tag fields
%--

fields = {'open', 'close'};

%--
% test for struct with proper fields
%--

% NOTE: we disregard field order in this test

value = isstruct(in) && isempty(setdiff(fieldnames(in), fields));
