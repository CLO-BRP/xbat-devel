function root = html_temp_root

% html_temp_root - place to store HTML temp files
% -----------------------------------------------
%
% root = html_temp_root
%
% Output:
% -------
%  root - path to HTML temp root

root = create_dir([html_cache_root, filesep, 'Temp']);

if isempty(root)
	error('Unable to create HTML temp root.');
end
