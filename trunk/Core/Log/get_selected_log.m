function [log, lib] = get_selected_log(pal)

% get_selected_log - get log selected in XBAT palette
% ---------------------------------------------------
%
% [sound, lib] = get_selected_log(pal)
%
% Input:
% ------
%  pal - palette
%
% Output:
% -------
%  log - the log
%  lib - library containing log

if ~nargin || isempty(pal)	
	pal = get_palette(0, 'XBAT');
end

if isempty(pal)
	return;
end

lib = get_active_library;

log = {}; lib = [];

%--
% get selected logs from names
%--

string = get_control(pal, 'Logs', 'value');

for k = 1:length(string)

	name = parse_tag(string{k}, filesep, {'sound', 'log'});

	log{k} = get_library_logs('logs', lib, name.sound, name.log);

end

log = [log{:}];

