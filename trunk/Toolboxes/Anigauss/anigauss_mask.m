function F = anigauss_mask(sv,su,phi,dv,du,tol)

% anigauss_mask - generate equivalent filter mask
% -----------------------------------------------
%
% F = anigauss_mask(sv,su,phi,dv,du)
%
% Input:
% ------
%  sv - v deviation
%  su - u deviation
%  phi - orientation
%  dv - derivative order in v
%  du - derivative order in u
%
% Output:
% -------
%  F - equivalent filter mask

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

% NOTE: the dimensional ordering of the arguments suggests rows and columns

%----------------------------------------
% HANDLE INPUT
%----------------------------------------

%--
% set truncation tolerance
%--

% NOTE: the default is no truncation

if (nargin < 6)
	tol = [];
end

%--
% set derivative orders 
%--

if ((nargin < 5) || isempty(du))
	du = 0;
end

if ((nargin < 4) || isempty(dv))
	dv = 0;
end

%--
% set orientation
%--

if ((nargin < 3) || isempty(phi))
	phi = 0;
end

%--
% set deviations
%--

if ((nargin < 1) || isempty(sv))
	sv = 1;
end

if ((nargin < 2) || isempty(su))
	su = sv;
end

%----------------------------------------
% CREATE MASK
%----------------------------------------

%--
% estimate required image size
%--

% NOTE: the orientation angle is provided in degrees

co = abs(cosd(phi)); so = abs(sind(phi)); 
		
w = ceil(3 * max((su * co),(sv * so)));

h = ceil(3 * max((su * so),(sv * co)));

%--
% create impulse image to filter
%--

X = zeros((2 * h) + 1,(2 * w) + 1); X(h + 1,w + 1) = 1;

%--
% compute mask equivalent through impulse filtering
%--

F = anigauss(X,sv,su,phi,dv,du);

%--
% truncate mask using tolerance
%--

% NOTE: since we may actually use this finite mask, normalize

if (~isempty(tol))
	G = F ./ max(F(:)); G = filt_tight(G,tol); F = G ./ sum(G(:));
end

%----------------------------------------
% DISPLAY MASK
%----------------------------------------

if (~nargout)
	
	fig; image_view(F);
	
	title( ...
		['S = ', num2str(su), ', ', num2str(sv), ', PHI = ', int2str(phi)] ... 
	);

end