function [event, context] = compute(page, parameter, context)

% RANDOM - compute

event = empty(event_create);

start = page.start + rand * page.duration;

duration = parameter.min_length + rand * (parameter.max_length - parameter.min_length);

new_event = event_create;

new_event.time = [start, start + duration];

new_event.freq = [100, page.rate/4];

new_event.score = rand;

new_event.channel = floor(1 + rand * context.sound.channels);

if rand > parameter.probability
	event(end + 1) = new_event;
end