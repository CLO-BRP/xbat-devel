function parameter = parameter__create(context)

% RANDOM - parameter__create

parameter = struct;

parameter.min_length = 1;

parameter.max_length = 2;

parameter.probability = 0.001;

