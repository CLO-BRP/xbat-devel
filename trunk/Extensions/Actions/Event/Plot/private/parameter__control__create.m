function control = parameter__control__create(parameter,context)

% PLOT - parameter__control__create

control = empty(control_create);

%--
% method control
%--

methods = {'periodogram', 'music', 'cov', 'welch'};

control(end + 1) = control_create( ...
	'name', 'method', ...
	'style', 'popup', ...
	'string', methods, ...
	'value', find(strcmp(parameter.method, methods)) ...
);
