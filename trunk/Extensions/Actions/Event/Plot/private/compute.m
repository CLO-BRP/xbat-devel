function [result,context] = compute(event,parameter,context)

% PLOT - compute

result = [];

h = fig;

%--
% amplitude plot
%--

subplot(2, 1, 1);

t = linspace(event.time(1), event.time(2), length(event.samples));

plot(t, event.samples);

title(['Amplitude Of Event #' int2str(event.id)]);

xlabel('Time (s)'); ylabel('Amplitude (normalized)');

%--
% spectrum plot
%--

subplot(2, 1, 2);

%--
% initialize spectrum estimation object
%--

method = parameter.method{1};

spec_obj = spectrum.(method);

%--
% plot psd or pseudospectrum, depending on method
%--

switch method
	
	case {'periodogram', 'welch', 'cov'}
		
		psd(spec_obj, event.samples, 'Fs', event.rate);
	
	case 'music'
		
		pseudospectrum(spec_obj,hilbert(event.samples),'Fs',event.rate); 
		
	otherwise
		
end

title(['Spectrum Of Event #' int2str(event.id)]);

xlabel('Frequency (KHz)'); ylabel('Amplitude (dB)');

%--
% some prettification
%--

set(h, 'numbertitle', 'off', 'name', ['Event Plot (# ', int2str(event.id), ')']);

%--
% send back result
%--

result = h;
