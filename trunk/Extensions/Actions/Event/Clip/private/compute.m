function [result, context] = compute(event, parameter, context)

% CLIP - compute

%--
% compute file name
%--

time = map_time(context.sound, 'real', 'record', event.time); 

% NOTE: try to use full datetime if possible

if ~isempty(context.sound.realtime)
    timestr = datestr((time(1) / 86400) + context.sound.realtime, 30);
else
    timestr = datestr(time(1) / 86400, 'HHMMSS.FFF');
end

ext = lower(parameter.format{1});

%--
% get event id string, padded with zeros
%--

idstr = int_to_str(event.id, 9999);

% NOTE: the clip file is the result

name = token_replace( ...
    parameter.file_names, file_name_tokens, {context.log, sound_name(context.sound), timestr, idstr} ...
);

result.file = [parameter.output, filesep, name, '.', ext];

%--
% write clip to file
%--

create_event_clip( ...
    context.sound, event, result.file, parameter.padding, parameter.taper ...
);





	
	



