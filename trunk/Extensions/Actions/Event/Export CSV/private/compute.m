function [result, context] = compute(event, parameter, context)

% EXPORT CSV - compute

result = struct;

event = prepare_csv_event(event, context);

if ~isfield(context, 'state')
	context.state.event = event;
else
	context.state.event(end + 1) = event;
end
