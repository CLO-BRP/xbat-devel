function block = get_block_range(rate)

%--
% set block limits in samples
%--

samples = [2^4, 2^12];

%--
% convert to time
%--

block = samples ./ rate;