function result = control__callback(callback, context)

% DATE_TIME - control__callback

result = [];

switch callback.control.name

	case 'datetime'
	
		value = get_control(callback.pal.handle, 'datetime', 'value');

		%--
		% enforce validity of date string
		%--
		
		try
			datevec(value);
		catch
			set_control(callback.pal.handle, 'datetime', 'string', datestr(context.attribute.datetime, 0));
		end
		
	case 'get_from_files'
		
		file = context.sound.file;
		
		if iscell(file)
			file = file{1};
		end
		
		num = file_datenum(file);
		
		if ~isempty(num)
			set_control(callback.pal.handle, 'datetime', 'string', datestr(num));
		end
		
end