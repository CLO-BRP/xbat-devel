function [handles, context] = on__play__stop(widget, data, parameter, context)

% SCOPE - on__play__stop

%--
% clear scope lines
%--

ax = scope_axes(widget);

set(get(ax(1), 'title'), 'string', '');

delete(scope_line(ax));

%--
% trigger page event
%--

% NOTE: this will re-display a marker ir available

[handles, context] = on__page(widget, data, parameter, context);