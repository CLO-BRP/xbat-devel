function [handles, context] = on__page(widget, data, parameter, context)

% SCOPE - on__page

% NOTE: we consider this event because scope may adapt to page

%--
% check for existing marker
%--

marker = []; % get_browser_marker(context.par);

if isempty(marker) || isempty(marker.time)
	handles = []; return;
end

%--
% add marker to data and set marker
%--

% NOTE: the marker is not currently included in the basic widget data

data.marker = marker;

handles = on__marker__create(widget, data, parameter, context);
