function parameter = parameter__create(context)

% SCOPE - parameter__create

parameter = struct;

% TODO: the default value should depend on the sound rate

parameter.duration = 0.01;

parameter.adapt = 1;
