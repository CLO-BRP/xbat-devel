function [parameter, context] = parameter__compile(parameter, context)

% SCOPE - parameter__compile

% TODO: limit above and below

if parameter.adapt && isfield(context, 'page')
	parameter.duration = min(1, 0.025 * context.page.duration);
end
