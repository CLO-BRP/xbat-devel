function [handles, context] = on__play__update(widget, data, parameter, context)

% SCOPE - on__play__update

%--
% get current scope time
%--

time = get_play_time(context.par); 

%--
% get scope samples from buffer
%--

% TODO: factor this into a helper

% NOTE: return if we can't get data to display

if isempty(data.buffer)
	handles = []; return;
end

rate = get(data.player, 'samplerate'); n = floor(parameter.duration * rate / data.buffer.speed);

buffer = data.buffer;
	
overflow = (buffer.ix + n - 1) - length(buffer.samples);

if overflow <= 0
	samples = buffer.samples(buffer.ix:(buffer.ix + n - 1), :);
else
	samples = [buffer.samples(buffer.ix:end, :); zeros(overflow, size(buffer.samples, 2))];
end

%--
% update scope display
%--

handles = update_scope_display(widget, time, parameter.duration, samples, context);

