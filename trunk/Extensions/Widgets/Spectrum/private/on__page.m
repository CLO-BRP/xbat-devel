function [handles, context] = on__page(widget, data, parameter, context)

% SPECTRUM - on__page

handles = [];

%--
% get handles
%--

ax = spectrum_axes(widget); [center, low, high] = page_line(ax);

%--
% get image data
%--

% TODO: this should be part of the event data

im = data.browser.images;

if iscell(im)
	im = im{1};
end

im = im(1);

X = get(im, 'cdata');

%--
% compute spectrum estimates
%--

[rows, cols] = size(X);

S = sum(X, 2) ./ cols;

D = sqrt(sum((X - S * ones(1, cols)).^2, 2) ./ (cols - 1));

%--
% display lines
%--

grid = linspace(0, 0.5 * data.sound.rate, rows);

set(center, ...
	'xdata', grid, 'ydata', S ...
);

set(low, ...
	'xdata', grid, 'ydata', S - D ...
);

set(high, ...
	'xdata', grid, 'ydata', S + D ...
);


%--
% set spectrum y axis limits according to page
%--

clim = get(data.browser.axes(1), 'clim');

set(ax, 'ylim', clim);

set(ax, 'xlim', data.page.freq);
