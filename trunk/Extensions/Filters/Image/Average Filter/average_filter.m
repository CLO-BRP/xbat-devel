function ext = moving_average

% NOTE: this extension should perhaps inherit from a linear base

% NOTE: then all linear filters can share compute, the key component

ext = extension_inherit(morphological_base);

ext.category = {'Blur','Statistical'};
