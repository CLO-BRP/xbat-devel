function [X, context] = compute(X, parameter, context)

% SIMPLE RECURSIVE BACKGROUND - compute

% TODO: consider if we want a single slice offset in the computation

%--
% compute background estimate
%--

n = size(X, 2); B = zeros(size(X)); lambda = parameter.lambda;

% NOTE: use last background estimate as starting point if available

if isfield(context, 'state') && isfield(context.state, 'background')
	B(:, 1) = (1 - lambda) * context.state.background + lambda * X(:, 1);
end

% NOTE: update background estimate recursively

for k = 2:n
	B(:, k) = (1 - parameter.lambda) * B(:, k - 1) + lambda * X(:, k);
end

%--
% output resulting background or signal estimate
%--

switch lower(parameter.output{1})
	
	case 'background'
		X = B;
		
	case 'signal'
		X = X - B;
		
end

%--
% store last estimate in context, for use when we are paging!
%--

context.state.background = B(:, end);
