function [types,ix] = gradient_types

% gradient_types - gradient type strings
% --------------------------------------
%
% [types,ix] = gradient_types
%
% Output:
% -------
%  types - cell of type strings
%  ix - default value index

types = {'Dilation','Symmetric','Erosion'};

ix = 2;