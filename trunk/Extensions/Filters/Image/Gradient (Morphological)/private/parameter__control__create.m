function control = parameter__control__create(parameter,context)

% GRADIENT (MORPHOLOGICAL) - parameter__control__create

%--
% add leading gradient types control
%--

[types,ix] = gradient_types;

control(1) = control_create( ...
    'name','type', ...
    'alias','Gradient', ...
	'style','popup', ...
	'space',1.25, ...
	'string',types, ...
	'value',ix ...
);

control(end + 1) = control_create( ...
	'style','separator' ...
);

%--
% append parent controls
%--

fun = parent_fun;

par_control = fun(parameter,context);

control = [control, par_control];