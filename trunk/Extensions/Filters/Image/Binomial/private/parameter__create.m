function parameter = parameter__create(context)

% BINOMIAL - parameter__create

%--
% get parent parameters
%--

fun = parent_fun; parameter = fun(context);

%--
% extend parameters
%--

parameter.width = 1;

parameter.height = 1;

parameter.link = 0;