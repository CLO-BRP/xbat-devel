function result = parameter__control__callback(callback, context)

% DIFFERENCE - parameter__control__callback

% result = struct;

% NOTE: disable selection configuration button

handle = findobj(callback.pal.handle, 'tag', 'sel_config');

if ~isempty(handle)
	set(handle, 'enable', 'off');
end

fun = parent_fun; result = fun(callback, context);
