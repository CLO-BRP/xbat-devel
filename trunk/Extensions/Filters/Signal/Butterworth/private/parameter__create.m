function parameter = parameter__create(context)

% LOWPASS-BUTTERWORTH - parameter__create

% parameter = struct;

fun = parent_fun; parameter = fun(context);

rate = get_sound_rate(context.sound); nyq = 0.5 * rate;

parameter.max_freq = 0.5 * nyq;

parameter.order = 8;

parameter.type = 'low';