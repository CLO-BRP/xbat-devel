function [X, context] = compute(X, parameter, context)

% FFT_TRUNCATE - compute

%---------------------------------
% PARAMETER
%---------------------------------

n = 512;

p = 0.02; q = 1 - p;

%---------------------------------
% PROCESS
%---------------------------------

%--
% pad samples for framing
%--

[N, ch] = size(X);

if rem(N, n)
	X = [X; zeros(n - rem(N, n), ch)];
end

N2 = size(X, 1);

%--
% process channels
%--

Y = zeros(size(X));

for k = 1:ch
	
	%--
	% get frames and transform to frequency
	%--
	
	Z = fft(reshape(X(:, k), n, N2/n));
	
	%--
	% process frames
	%--
	
	for j = 1:(N2/n)
		Z(:,j) = truncate(Z(:, j), q);
	end
	
	%--
	% transform to samples and undo framing
	%--
	
	Y(:,k) = vec(ifft(Z));
	
end

%--
% remove pad samples
%--

X = Y(1:N, :);


%--------------------------------------
% TRUNCATE
%--------------------------------------

function X = truncate(X, q)

% NOTE: full complex sort is much slower, we don't need phase

[ignore, ix] = sort(abs(X));

% TODO: check for extreme index computations

n = ceil(q * length(X));

X(ix(1:n)) = 0; 


%--------------------------------------
% THRESHOLD
%--------------------------------------

function X = threshold(X, t)

X(abs(X) < t) = 0;


