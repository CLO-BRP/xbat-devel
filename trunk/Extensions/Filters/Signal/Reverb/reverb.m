function ext = reverb

% REVERB - reverberation filter

ext = extension_create;

ext.category = {'Reverb', 'Multipath'};

ext.version = '0.1';

ext.author = 'Matt Robbins';

