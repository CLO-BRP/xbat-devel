function [X, context] = compute(X, parameter, context)

% IMPULSE-NOISE - compute

%--
% get uniform noise vector and probability of impulse
%--

N = rand(size(X)); p = parameter.percent / 100;

%--
% select positions and place impulses
%--

switch lower(parameter.type{1})
	
	case 'mixed', X(N > (1 - 0.5 * p)) = 1; X(N < 0.5 * p) = -1;
		
	case 'negative', X(N < p) = -1;
		
	case 'positive', X(N < p) = 1;
		
end