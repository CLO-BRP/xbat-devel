function enhanced = wavelet_perceptual_decomp(y,Fs,level_start,level_end,fmin,fmax,flow,fhigh,type1,type2,type3,plot_tree,event, autoflag, sil_ratio)
%  __  __                            _   _       
% |  \/  |                          | | | |      
% | \  / | __ _ _ __ __ _ _   _  ___| |_| |_ ___ 
% | |\/| |/ _` | '__/ _` | | | |/ _ \ __| __/ _ \
% | |  | | (_| | | | (_| | |_| |  __/ |_| ||  __/
% |_|  |_|\__,_|_|  \__, |\__,_|\___|\__|\__\___|
%                      | |                       
%                      |_|                       
%  _    _       _                    _ _         
% | |  | |     (_)                  (_) |        
% | |  | |_ __  ___   _____ _ __ ___ _| |_ _   _ 
% | |  | | '_ \| \ \ / / _ \ '__/ __| | __| | | |
% | |__| | | | | |\ V /  __/ |  \__ \ | |_| |_| |
%  \____/|_| |_|_| \_/ \___|_|  |___/_|\__|\__, |
%                                           __/ |
%                                          |___/ 
% 
% Last Modified by Dylan Conley for 
% Marquette University's Speech and Signal Processing Lab.
%
% This file is part of the Marquette University filters add-on for XBAT.
%
% To run this program, open xbat.m and select 'Perceptual Wavelet 
% Decomposition' from filters/Marquette.
% 
%
% wavelet_perceptual_decomp filter
%   y = noisy signal
%   Fs = sample rate
%   event = structure to get times and duration from 
%	autoflag = boolean value indicating whether to use auto-detection of silence region or not
%	sil_ratio = used in atuo-detection

%
% Marquette University�s Speech and Signal Processing Laboratory has 
% developed a method for wavelet thresholding based on a perceptual auditory 
% model, which can be applied to human speech or any species of animals (as 
% needed for the Dolittle project). This method uses a special scaling on the
% frequency axis and an optimal thresholding based on modifications of the 
% Ephraim Malah filter formulas. This filter utilizes the following 
% functions: Automatic Down Decomposition, Calculate Frequency, Calculate 
% Greenwood Frequency, Greenwood, Inverse Greenwood.
% Parameters
% The Perceptual Wavelet Decomposition Filter has five main parameters:
% Start Level, End  Level, Minimum Frequency, Maximum Frequency, 
% Thresholding Type
% 
%
%   See also 


%
%   Copyright 2009 Marquette University, Speech and Signal Processing
%   Laboratory.

%   Edited for use in 
%   Marquette Filters by: Dylan Conley  June 29, 2009
%
if autoflag ==0
    start_time = event.time(1);
    end_time = event.time(2);
    
    scalefact=sqrt(var(y(ceil(Fs*start_time):floor(Fs*end_time))));
else
    sil = 256*findsil(y, 256, sil_ratio);
    scalefact=sqrt(var(y(sil)));
end
y=y/scalefact;      % Should make noisevar approx. = 1


T = auto_down_decomp (y, level_start,level_end,fmin,fmax,flow,fhigh,Fs);
TN=leaves(T,'dp');
num_sub=size(TN,1);
cfs = read(T,'data',TN);
for m=1:num_sub
            % ============ hard/soft ============
    if(type1==1) % hard
            the1=thselect(cfs{m},'rigrsure');
            cfs_en=wthresh(cfs{m},'h',the1);

            % =========== Ephraim ===============
   elseif(type2==1) % soft
            the1=thselect(cfs{m},'rigrsure');
            cfs_en=wthresh(cfs{m},'s',the1);
 
   elseif(type3==1) % MMSE
            alpha=0.92;
            lambdaD=ones(1,length(cfs{m}));
            cfs_en=wavelet_OMLSA_cohen(cfs{m},lambdaD,alpha);
            cfs_en=cfs_en';

    end
    % ===================================
    T = write(T,'cfs',TN(m,:),cfs_en);
end
 if(plot_tree==1) % hard
        plot(T)
end
enhanced = wprec(T);