function f=cal_green(Fmin,Fmax,Flow,Fhigh,numFilts)
% Fmin = 2500;
% Fmax = 7000;
% Flow = 2500;
% Fhigh = 7000;
% numFilts = 20;

% Fmin~Fmax: hearing range
% Flow~Fhigh: calling range


%percMel = [mel(Flow):(mel(Fhigh)-mel(Flow))/(numFilts+1):mel(Fhigh)];
%actMel = Imel(percMel);

precGreen = [greenwood(Flow,Fmin,Fmax):(greenwood(Fhigh,Fmin,Fmax)-greenwood(Flow,Fmin,Fmax))/(numFilts+1):greenwood(Fhigh,Fmin,Fmax)];
%actGreen = A * (10.^(a.*perc) - k);
actGreen = Igreenwood(precGreen, Fmin, Fmax);

f=actGreen(2:numFilts+1);