function f=p(x)

% p - Positive or not
% ===================
% Usage: f=p(x)
% (p(x)={x,if x>0; 0,if x<0}

f=(x>0).*x;
