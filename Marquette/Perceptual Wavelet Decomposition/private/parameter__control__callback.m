function result = parameter__control__callback(callback, context)

% PERCEPTUAL WAVELET DECOMPOSITION - parameter__control__callback

result = struct;
%----------------------------
% SETUP
%----------------------------

%--
% unpack callback
%--

control = callback.control; pal = callback.pal; par = callback.par; 

%--
% set update flags
%--

result.update = 1; 



%----------------------------
% CALLBACKS
%----------------------------
type1 = get_control(pal.handle, 'type1', 'value');
type2 = get_control(pal.handle, 'type2', 'value');
type3 = get_control(pal.handle, 'type3', 'value');


switch control.name
    
    case 'flag'
        flag = get_control(pal.handle, 'flag', 'value');
        
        if flag == 0
            set_control(pal.handle, 'ratio', 'enable', 'off');
            set_control(pal.handle, 'start_time', 'enable', 'on');
            set_control(pal.handle, 'end_time', 'enable', 'on');
            set_control(pal.handle, 'identify' ,'enable', 'on');
        else
            set_control(pal.handle, 'ratio', 'enable', 'on');
            set_control(pal.handle, 'start_time', 'enable', 'off');
            set_control(pal.handle, 'end_time', 'enable', 'off');
            set_control(pal.handle, 'identify' ,'enable', 'off');  
        end
        
    case 'type1'
        if type1==1 && (type2==1 || type3==1)
            set_control(pal.handle, 'type2', 'value', 0);
            set_control(pal.handle, 'type3', 'value', 0);
        end
        
    case 'type2'
        if type2==1 && (type1==1 || type3==1)
            set_control(pal.handle, 'type1', 'value', 0);
            set_control(pal.handle, 'type3', 'value', 0);
        end
        
    case 'type3'
        if type3==1 && (type2==1 || type1==1)
            set_control(pal.handle, 'type2', 'value', 0);
            set_control(pal.handle, 'type1', 'value', 0);
        end
            
    case 'identify'
        data = get(par.handle, 'userdata');		
		event = data.browser.selection.event;	
        if isempty(event) || isempty(event.time)
            errordlg('Please make a selection or choose "autodetect silence"'); return;
        else
            set_control(pal.handle, 'start_time', 'value', event.time(1));
            set_control(pal.handle, 'end_time', 'value', event.time(2));
        end
        
    case 'start_time'
        start_time = get_control(pal.handle,'start_time','string');
        if isnumeric(str2num(start_time)) && ~isempty(str2num(start_time))
            set_control(pal.handle, 'start_time', 'value', str2num(start_time));
        else
            errordlg(sprintf([start_time, '\nis not a valid time. Please enter a numeric value (ex: 5, 1.2 or 12e-3)']),'Whoops!');
        end
        
    case 'end_time'
        end_time = get_control(pal.handle,'end_time','string');
        if isnumeric(str2num(end_time)) && ~isempty(str2num(end_time))
            set_control(pal.handle, 'end_time', 'value', str2num(end_time));
        else
            errordlg(sprintf([end_time, '\nis not a valid time. Please enter a numeric value (ex: 5, 1.2 or 12e-3)']),'Whoops!');
        end
end