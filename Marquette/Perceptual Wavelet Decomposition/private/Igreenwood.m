function act = Igreenwood(perc, fmin, fmax)
k = .88;
A = fmin / (1 - k);
a = log10((fmax / A) + k);
%Currently scales 0 - 1 for perceptual frequency
%a = (1 / fmax) * log10((fmax / A) + k);

act = A * (10.^(a.*perc) - k);
    