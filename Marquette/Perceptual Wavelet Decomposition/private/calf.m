function f_ln=calf(l,n,fhigh)
LL=l;
for i=1:LL
    for j=1:2^LL
        f(i,j)=fhigh/(2^i)*(j-0.5);
    end
end

f_ln=f(l,n+1);