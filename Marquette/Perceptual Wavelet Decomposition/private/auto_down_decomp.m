function T = auto_down_decomp (y, level_start,level_end,fmin,fmax,flow,fhigh,Fs)
T = wpdec(y,level_start,'dmey');
MSE=10^10;
for level=level_start:level_end-1
    T_node=leaves(T,'dp');
    for node=1:size(T_node,1)
        T=wpsplt(T,T_node(node,:));
        TN=leaves(T,'dp');
        for p=1:size(TN,1)
            freq_wpt(p)=calf(TN(p,1),TN(p,2),Fs/2);
        end
        freq_green=cal_green(fmin,fmax,flow,fhigh,size(TN,1));
        SE=sum(abs((freq_green-freq_wpt)));
        %SE=sum((freq_green-freq_wpt).^2);
        if SE<MSE
            MSE=SE;
        else
            T=wpjoin(T,T_node(node,:));
        end
        clear freq_green
        clear freq_wpt
    end
end

