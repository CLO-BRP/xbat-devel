function perc = greenwood(act, fmin, fmax)
DEBUG=0;

k = .88;
A = fmin / (1 - k);
a = log10((fmax / A) + k);
%a = (1 / fmax) * log10((fmax / A) + k);

perc = (1 ./ a) .* log10((act ./ A) + k);

if(DEBUG==1)
    disp(['a = ' num2str(a) '; A = ' num2str(A) '; k = ' num2str(k)]);
end
