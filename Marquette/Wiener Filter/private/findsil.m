function frmind = findsil(x,frsize,p)
% function frmind = findsil(x,frsize,p)
% quick function to identify most likely silence regions in a waveform
% Note this is NOT an endpoint detector
% It just finds the lowest energy frames and concatenates them
% Note it could be improved a lot, to have minimum duration sections,
% to look at spectral content/flatness, etc.
%
% Inputs: x - signal
%         frsize - framesize in points
%         p - prior prob of noise (% of signal to use)
% Outputs: sil - concatenated silence frames
%          frmind - index of frames used 

numfr=floor(length(x)/frsize);
numsil=max(1,floor(numfr*p));

for i=1:(numfr-1)   % last one might be zero padded, don't includes
    wind=(1+(i-1)*frsize):(i*frsize);
    en(i)=sum(x(wind).^2);
end

[sen,enind]=sort(en);
frmind=sort(enind(1:numsil));  % put contiguous frames adjacent