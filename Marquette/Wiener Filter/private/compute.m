function [X, context] = compute(X, parameter, context)
%{
par = get_active_browser;		
        if isempty(par)
			return;
        end					
		%--
		% get relevant clip data from parent
		%--		
		% NOTE: when selection derives from selected event other information may be contained 		
		data = get(par, 'userdata');		
		event = data.browser.selection.event;		
        if isempty(event) || isempty(event.time)
			return;
        end
%         times = event.time;
%         t = times(2) - times(1);
%         t_amt = num2str(t);
%         msgbox(t_amt);
%}

parameter.start_time =str2num(parameter.start_time);
parameter.end_time = str2num(parameter.end_time);
if parameter.flag == 0
    if parameter.start_time > parameter.end_time
        hold = parameter.start_time; parameter.start_time = parameter.end_time; parameter.end_time = hold;
    elseif parameter.start_time == parameter.end_time
        errordlg('The silence region must have a nonzero duration.','Whoops!');
    end
    
    if isempty(parameter.start_time) || isempty(parameter.end_time)
        errordlg('Must select a silence region on the spectrogram.'); return;
    end
else
    parameter.start_time =0;
    parameter.end_time =0;
end

event = struct;
event.time(1) = (parameter.start_time);
event.time(2) = (parameter.end_time);
event.duration = event.time(2) - event.time(1);


X = WF2(X, get_sound_rate(context.sound), parameter.l, parameter.alpha, event, parameter.flag, parameter.ratio);


if parameter.save == 1
   [newfile,newpath] = uiputfile('*.wav'); 
   
   if newfile ~= 0 
       [name,type] = strtok(newfile ,'.');
       while ~isempty(type)
           [next,type] = strtok(type, '.');
           if ~isempty(type)
               name = [name, '.',next];
           end
       end
       if ~strcmpi(next,'wav')
           newfile = [name, '.wav'];
       end
       wavwrite(X,get_sound_rate(context.sound),[newpath,newfile]);
       msgbox(['Filtered sound successfully saved as ', newfile,'. Make sure to add this file to the XBAT library if you wish to do further processing with it.'],'Congratulations');      
   end
end