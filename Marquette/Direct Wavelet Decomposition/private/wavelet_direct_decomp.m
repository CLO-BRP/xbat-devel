function enhanced = wavelet_direct_decomp(y,Fs,level,type1,type2,type3,event,autoflag,sil_ratio)
%  __  __                            _   _       
% |  \/  |                          | | | |      
% | \  / | __ _ _ __ __ _ _   _  ___| |_| |_ ___ 
% | |\/| |/ _` | '__/ _` | | | |/ _ \ __| __/ _ \
% | |  | | (_| | | | (_| | |_| |  __/ |_| ||  __/
% |_|  |_|\__,_|_|  \__, |\__,_|\___|\__|\__\___|
%                      | |                       
%                      |_|                       
%  _    _       _                    _ _         
% | |  | |     (_)                  (_) |        
% | |  | |_ __  ___   _____ _ __ ___ _| |_ _   _ 
% | |  | | '_ \| \ \ / / _ \ '__/ __| | __| | | |
% | |__| | | | | |\ V /  __/ |  \__ \ | |_| |_| |
%  \____/|_| |_|_| \_/ \___|_|  |___/_|\__|\__, |
%                                           __/ |
%                                          |___/ 
% Last Modified by Dylan Conley for 
% Marquette University's Speech and Signal Processing Lab.
%
% This file is part of the Marquette University filters add-on for XBAT.
%
% To run this program, open xbat.m and select 'Direct Wavelet 
% Decomposition' from filters/Marquette.
% 
%
% wavelet_direct_decomp filter
%   y = noisy signal
%   Fs = sample rate
%   t = time   *will change this
%   event = structure to get times and duration from 
%	autoflag = boolean value indicating whether to use auto-detection of silence region or not
%	sil_ratio = used in auto-detection
%
% In wavelet-based methods, the signal is decomposed using a wavelet 
% transform instead of an FFT, which has certain beneficial properties in 
% terms of time-frequency resolution. The wavelet coefficients are then 
% thresholded (possibly using a noise model as with the above methods) and 
% the signal reconstructed. There are a number of variations on the 
% thresholding method. This filter utilizes the following functions: Direct 
% Wavelet Decomposition, Wavelet OMLSA Cohen.
% Parameters
% Level, Thresholding Type
% 
%
%   See also


%   Copyright 2009 Marquette University, Speech and Signal Processing
%   Laboratory.


if autoflag ==0
    start_time = event.time(1);
    end_time = event.time(2);
    scalefact=sqrt(var(y(ceil(Fs*start_time):floor(Fs*end_time))));
    
else  
    sil = 256*findsil(y, 256, sil_ratio);
    scalefact=sqrt(var(y(sil)));
end

y=y/scalefact;      % Should make noisevar approx. = 1

T = wpdec(y,level,'dmey');
TN=leaves(T,'dp');
num_sub=size(TN,1);
cfs = read(T,'data',TN);
for m=1:num_sub
    
   if(type1==1) % hard
            the1=thselect(cfs{m},'rigrsure');
            cfs_en=wthresh(cfs{m},'h',the1);
            
   elseif(type2==1) % soft
            the1=thselect(cfs{m},'rigrsure');
            cfs_en=wthresh(cfs{m},'s',the1);
        
   elseif(type3==1) % MMSE
            alpha=0.92;
            lambdaD=ones(1,length(cfs{m}));
            cfs_en=wavelet_OMLSA_cohen(cfs{m},lambdaD,alpha);
            cfs_en=cfs_en';
            
    end
    % ===================================
    T = write(T,'cfs',TN(m,:),cfs_en);
end
enhanced = wprec(T);