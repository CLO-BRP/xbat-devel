function [parameter, context] = parameter__compile(parameter, context)
%{
% DIRECT WAVELET DECOMPOSITION - parameter__compile

%-----------------------------
% HANDLE INPUT
%-----------------------------

if isempty(parameter.templates)
	return;
end


%{
%--
% remove ignored clips
%--

for k = length(parameter.templates.clip):-1:1
	
	% NOTE: the zero mode is ignore
	
	if (parameter.templates.clip(k).mode == 0)
		parameter.templates.clip(k) = []; 
	end
	
end
%}


%--
% check for any clips
%--

% NOTE: there are no clips to compile to templates, return

if isempty(parameter.templates.clip)
	return;
end

%-----------------------------
% COMPILE PARAMETER
%-----------------------------

%--
% pack current sound spectrogram parameters
%--

parameter.specgram = context.sound.specgram;

%--
% compute current clip spectrograms
%--

clip = parameter.templates.clip;

for k = 1:length(clip)
	
	[clip(k).spectrogram, clip(k).mask, clip(k).freq_ix] = template_spectrogram(clip(k), context.sound, parameter);

	% NOTE: this is the number of bins in the template

	clip(k).pixels = sum(clip(k).mask(:));

end

parameter.templates.clip = clip;

%}