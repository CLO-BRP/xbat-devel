function result = parameter__control__callback(callback, context)

% LINEAR_BASE - parameter__control__callback

result = [];

%--
% update filter display
%--

% NOTE: this happens for any control

update_filter_display(callback, context);


%-------------------------------------------------------------------------
% UPDATE_FILTER_DISPLAY
%-------------------------------------------------------------------------

function update_filter_display(callback, context)

%-------------------------
% SETUP
%-------------------------

pal = callback.pal.handle;

ext = get_callback_extension(callback);

%--
% get 'filter' control parent axes handle
%--

ax = findobj(pal,'type','axes','tag','filter');

% NOTE: this should perhaps show up as an error

if (isempty(ax))
	return;
end

rate = get_sound_rate(context.sound);

%------------------------------------------------
% UPDATE CONTROL VALUES
%------------------------------------------------

%--
% update filter control
%--

% NOTE: this uses new code to set the value of an axes control

control_update([], pal, 'filter', ext.parameter.filter);

%------------------------------------------------
% UPDATE FILTER DISPLAY
%------------------------------------------------

%-------------------------------
% SETUP
%-------------------------------

%--
% set display colors
%--

% NOTE: eventually consider axes display here

response_color = [0 0 0];

guide_color = 0.5 * ones(1,3);

%--
% copy filter for convenience
%--

filter = ext.parameter.filter;

%--
% prepare display axes
%--

axes(ax); hold on;

delete(get(ax, 'children'));

%-------------------------------
% DISPLAY
%-------------------------------

% NOTE: update plot based on display control state

[ignore, opt] = control_update([], pal, 'display'); 

opt = lower(strtok(opt{1}, ' '));

switch opt
	
	%-------------------------------
	% FREQUENCY RESPONSE
	%-------------------------------
	
	case 'frequency'
		
		%--
		% compute and plot frequency response
		%--
		
		[h, f] = freqz(filter.b, filter.a, 512, rate);
		
		plot(f, abs(h), 'k-');
		
		%--
		% compute and set axes limits
		%--
		
		% NOTE: the vertical limits are based on the distortion specified
		
		xlim = [0, 0.5 * rate]; ylim = [-0.2, max(1.2, 1.2 * max(abs(h)))];
		
		set(ax, ...
			'ylim', ylim, 'xlim', xlim ...
		);

		%--
		% display basic guides
		%--
		
		% NOTE: display one and zero lines

		tmp = plot(xlim, [1 1], ':'); set(tmp, 'color', guide_color);
		
		tmp = plot(xlim, [0 0], ':'); set(tmp, 'color', guide_color);
		
		%--
		% display special guides
		%--
		
		% NOTE: it may not be the control we check for

		if has_control(pal, 'min_freq')

			min_freq = get_control(pal, 'min_freq', 'value');
			
			line(min_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);

			if has_control(pal, 'transition')
				
				trans = get_control(pal, 'transition', 'value');
				
				line((min_freq - trans) * [1 1], ylim, ...
					'parent', ax, 'linestyle', ':', 'color', guide_color ...
				);

			end
			
		end

		if has_control(pal, 'max_freq')

			max_freq = get_control(pal, 'max_freq', 'value');
			
			line(max_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);

			if has_control(pal, 'transition')
				
				trans = get_control(pal, 'transition', 'value');
				
				line((max_freq + trans) * [1 1], ylim, ...
					'parent', ax, 'linestyle', ':', 'color', guide_color ...
				);
			
			end

		end
		
		if has_control(pal, 'center_freq')

			center_freq = get_control(pal, 'center_freq', 'value');

			line(center_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);

		end
		
	%-------------------------------
	% IMPULSE RESPONSE
	%-------------------------------
	
	case 'impulse'
	
		%--
		% plot fir impulse response
		%--
		
		[h, t] = impz(filter.b, filter.a);
		
		plot(t, h, 'k-');
		
		%--
		% compute and set axes limits
		%--
		
		% NOTE: we update axes limits to be centered and fairly tight

		if (length(t) > 1)
			xlim = fast_min_max(t); ylim = fast_min_max(h) + 0.1 * [-1, 1];
		else
			xlim = [-0.5, 0.5]; ylim = [0, h] + 0.1 * [-1, 1];
		end
		
		set(ax, 'ylim', ylim, 'xlim', xlim);

		%--
		% display guides
		%--
		
		% NOTE: display zero line

		tmp = plot(xlim, [0 0], ':'); set(tmp, 'color', guide_color);
		
end
