function [X, context] = compute(X, parameter, context)

% LINEAR_BASE - compute

% TODO: add frequency domain filtering for long FIR filters

X = filter(parameter.filter.b, parameter.filter.a, X);
