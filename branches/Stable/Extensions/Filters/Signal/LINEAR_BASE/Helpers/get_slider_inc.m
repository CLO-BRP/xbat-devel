function inc = get_slider_inc(nyq)

% get_slider_inc - get slider increment for frequency sliders using nyquist

if (nyq > 1000)
	inc = [100, 500];
else
	inc = [50, 100];
end

if (nyq == 1)
	inc = [0.05, 0.1];
end