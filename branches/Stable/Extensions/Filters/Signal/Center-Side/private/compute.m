function [X, context] = compute(X, parameter, context)

% CENTER-SIDE - compute

% NOTE: this breaks when playing a selection, because it is multi-channel

T = 0.5 * [1, 1; 1, -1]; X = X * T;
