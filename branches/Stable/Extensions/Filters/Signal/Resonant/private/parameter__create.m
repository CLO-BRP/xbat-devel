function parameter = parameter__create(context)

% RESONANT - parameter__create

%--
% get parent parameters
%--

fun = parent_fun; parameter = fun(context);

%--
% get nyquist from context
%--

rate = get_sound_rate(context.sound); nyq = 0.5 * rate;

%--
% filter parameters
%--

parameter.center_freq = 0.5 * nyq;

parameter.width = 0.2 * nyq;