function ext = amplitude_activity

% amplitude_activity - amplitude activity based detector
% ------------------------------------------------------
%
% detector = amplitude_activity
%
% Output:
% -------
%  detector - detector description used in XBAT integration


ext = extension_create;

ext.version = '0.2';

ext.author = 'Harold K. Figueroa';



