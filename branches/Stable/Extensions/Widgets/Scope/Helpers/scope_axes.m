function handle = scope_axes(widget, varargin)

handle = create_axes(widget, 'scope_axes', varargin{:});
