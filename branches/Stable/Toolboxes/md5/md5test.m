clc
clear all
echo off

% This file is for testing md5.m
% We compare our values against the values with the tool md5.exe that
% you can freely download at http://www.fourmilab.ch/md5/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% string tests. md5-values precalculated with md5.exe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tstr1 = 'We delights in being respectful of the environment';
tstr2 = '398534.093';
tstr1md5 = '02807B41CB1E3AB509772ACDA528EE73';
tstr2md5 = '8C862A32460F0F801673329A919676FC';
disp( ' ' );
disp( '-> string tests. md5-values precalculated with md5.exe' );

if strcmpi( md5( tstr1 ), tstr1md5 ) ~= 1, error( 'error with md5 of tstr1' ), else disp( 'tstr1 ok' ), end;
if strcmpi( md5( tstr1, 0 ), tstr1md5 ) ~= 1, error( 'error with md5 of tstr1' ), else disp( 'tstr1 with 0 ok' ), end;
if strcmpi( md5( tstr2 ), tstr2md5 ) ~= 1, error( 'error with md5 of tstr2' ), else disp( 'tstr2 ok' ), end;
if strcmpi( md5( tstr2, 0 ), tstr2md5 ) ~= 1, error( 'error with md5 of tstr2' ), else disp( 'tstr2 with 0 ok' ), end;
if md5( tstr1, tstr1md5 ) ~= 1, error( 'error with md5check of tstr1' ), else disp( 'tstr1 md5check ok' ), end;
if md5( tstr1, 0, tstr1md5 ) ~= 1, error( 'error with md5check of tstr1' ), else disp( 'tstr1 with 0 md5check ok' ), end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% double tests. Cannot be checked with md5.exe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tdoub1 = 398534.093;
tdoub2 = 398534.09300;
th1 = hilb( 10 );
disp( ' ' );
disp( '-> double tests. Cannot be checked with md5.exe' )

if strcmpi( md5( tdoub1 ), md5( tdoub2 ) ) ~= 1, error( 'double values different' ), else disp( 'tdoub1 and tdoub2 same' ), end;
if strcmpi( md5( tdoub1 ), md5( tdoub1, 0 ) ) ~= 1, error( 'tdoub1 values different' ), else disp( 'tdoub1 with 0 same' ), end;
if strcmpi( md5( tdoub1, 0 ), md5( tdoub2, 0 ) ) ~= 1, error( 'double values with 0 different' ), else disp( 'tdoub1 and tdoub2 with 0 same' ), end;
if strcmpi( md5( th1 ), md5( th1, 0 ) ) ~= 1, error( 'hilbert matrix values different' ), else disp( 'hilbert matrix same' ), end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% file tests. md5-values precalculated with md5.exe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% path to testfile

tfname = 'testfile.txt';
pa = which( 'md5test.m' );
idx = findstr( pa, 'md5test.m' );
pa = [pa( 1:idx-1 ) tfname];

% magic matrix and file

tmagname = 'testmagic.txt';
tm1 = magic( 4 );
save testmagic.txt tm1 -ascii;  % huh? isn't there a functional form to save an ascii file???
tm2 = tm1;
clear tm1;
tm1 = load( tmagname );

% md5's

tfnamemd5 = 'BCB73C13F18FCEB13F3E484CF1479662';
tfmagnamemd5 = '7971AF55FAABFA31D855FF7BCC8A1CD9';

% check now

disp( ' ' );
disp( '->file tests. md5-values precalculated with md5.exe' );

if strcmpi( md5( pa, 1 ), tfnamemd5 ) ~= 1, error( 'error with md5 of testfile' ), else disp( 'testfile ok' ), end;
if md5( pa, 1, tfnamemd5 ) ~= 1, error( 'error with md5check of testfile' ), else disp( 'md5check of testfile ok' ), end;
if strcmpi( md5( tmagname, 1 ), tfmagnamemd5 ) ~= 1, error( 'error with md5 of magtestfile' ), else disp( 'magtestfile ok' ), end;
if md5( tmagname, 1, tfmagnamemd5 ) ~= 1, error( 'error with md5check of magtestfile' ), else disp( 'md5check of magtestfile ok' ), end;
if strcmpi( md5( tm1 ), md5( tm2 ) ) ~= 1, error( 'error with md5 of orig./loaded magtestfile' ), else disp( 'loaded orig./magtestfile ok' ), end;

delete( tmagname );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% tests with larger files. If you want to run this test, you need
%%% to download/run my xlstest5 program that generates the necessary
%%% xls files. This md5 function is currently not intended to sign
%%% larger files (> 5 MB). Ok, I tested with 90 MB file, it works, but
%%% is 3 times slower than with the md5.exe tool and everything is
%%% loaded in the memory.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if 0
	x1 = 'xlsTest_d.xls';
	x1md5 = '5C23C04E543DA76AD3814ED4F7DEEB31';
	x2 = 'xlsTest_ammc.xls';
	x2md5 = '09D1AF27CCA5BF830D4B764BA7319B61';

	disp( ' ' );
	disp( '->larger file tests.' );
	tic
	if md5( x1, 1, x1md5 ) ~= 1, error( 'error with md5 of xlsTest_d' ), else disp( 'xlsTest_d ok' ), end;
	toc
	tic
	if md5( x2, 1, x2md5 ) ~= 1, error( 'error with md5 of xlsTest_ammc' ), else disp( 'xlsTest_ammc ok' ), end;
	toc
end;
