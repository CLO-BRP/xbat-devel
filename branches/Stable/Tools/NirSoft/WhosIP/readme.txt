

WhosIP v1.02
Copyright (c) 2004 - 2005 Nir Sofer
Web site: http://www.nirsoft.net



Description
===========

WhosIP is a simple command-line utility that allows you to easily find
all available information about an IP address: The owner of the IP
address, the country/state name, IP addresses range, contact information
(address, phone, fax, and email), and more.



Versions History
================


* Version 1.02: Added -B option in ripe.net queries, to avoid the
  default data filtering.
* Version 1.01: Added support for AfriNIC IP Addresses.
* Version 1.00: First release



Usage
=====

WhosIP [-r] host-name



[-r]
If you specify this option, the original WHOIS record is displayed.
Otherwise, only a summary of the WHOIS record is displayed.

host-name
Host name or IP address.

Examples:
WhosIP -r www.nirsoft.net
WhosIP www.yahoo.com
WhosIP 216.239.59.147

Example for WhosIP output:

WHOIS Source: ARIN
IP Address:   64.233.167.104
Country:      USA - California
Network Name: GOOGLE
Owner Name:   Google Inc.
From IP:      64.233.160.0
To IP:        64.233.191.255
Allocated:    Yes
Contact Name: Google Inc.
Address:      2400 E. Bayshore Parkway, Mountain View
Email:        arin-contact@google.com
Abuse Email:
Phone:        +1-650-318-0200
Fax:          

WHOIS Record:

OrgName:    Google Inc. 
OrgID:      GOGL
Address:    2400 E. Bayshore Parkway
City:       Mountain View
StateProv:  CA
PostalCode: 94043
Country:    US

NetRange:   64.233.160.0 - 64.233.191.255 
CIDR:       64.233.160.0/19 
NetName:    GOOGLE
NetHandle:  NET-64-233-160-0-1
Parent:     NET-64-0-0-0-0
NetType:    Direct Allocation
NameServer: NS1.GOOGLE.COM
NameServer: NS2.GOOGLE.COM
Comment:    
RegDate:    2003-08-18
Updated:    2004-03-05

TechHandle: ZG39-ARIN
TechName:   Google Inc. 
TechPhone:  +1-650-318-0200
TechEmail:  arin-contact@google.com 

OrgTechHandle: ZG39-ARIN
OrgTechName:   Google Inc. 
OrgTechPhone:  +1-650-318-0200
OrgTechEmail:  arin-contact@google.com

# ARIN WHOIS database, last updated 2004-09-22 19:10
# Enter ? for additional hints on searching ARIN's WHOIS database.




System Requirements
===================


* Any Windows operating system: Windows 95/98/ME/NT/2000/XP/2003.
* Internet connection.


License
=======

This utility is released as freeware. You are allowed to freely
distribute this utility via floppy disk, CD-ROM, Internet, or in any
other way, as long as you don't charge anything for this. If you
distribute this utility, you must include all files in the distribution
package, without any modification !



Disclaimer
==========

The software is provided "AS IS" without any warranty, either expressed
or implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not
be liable for any special, incidental, consequential or indirect damages
due to loss of data or any other reason.



Feedback
========

If you have any problem, suggestion, comment, or you found a bug in my
utility, you can send a message to nirsofer@yahoo.com
