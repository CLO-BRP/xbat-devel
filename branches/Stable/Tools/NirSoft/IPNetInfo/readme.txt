


IPNetInfo v1.09
Copyright (c) 2004 - 2005 Nir Sofer
Web site: http://www.nirsoft.net



Description
===========

IPNetInfo is a small utility that allows you to easily find all available
information about an IP address: The owner of the IP address, the
country/state name, IP addresses range, contact information (address,
phone, fax, and email), and more.

This utility can be very useful for finding the origin of unsolicited
mail. You can simply copy the message headers from your email software
and paste them into IPNetInfo utility. IPNetInfo automatically extracts
all IP addresses from the message headers, and displays the information
about these IP addresses.



How does it work ?
==================

The IP address information is retrieved by sending a request to the whois
server of ARIN. If ARIN doesn't maintain the information about the
requested IP address, a second request is sent to the whois server of
RIPE, APNIC, LACNIC or AfriNIC.
After the IP address information is retrieved, IPNetInfo analyzes the
Whois record and displays it in a table.

Notice: From time to time, the WHOIS server of ARIN is down and doesn't
respond to WHOIS requests from IPNetInfo, and thus IPNetinfo fails to
retrieve the IP address. When such a thing occurs, simply try again later.



Retrieving the message headers from your email client
=====================================================

If you don't know how to get the message headers from your email client,
this web site can help you - it provides detailed explanation about how
to get the message headers in each email client.



Versions History
================


* Version 1.09:
  o Added -B option in ripe.net queries, to avoid the default data
    filtering.

* Version 1.08:
  o New option: Resolve IP Addresses.

* Version 1.07:
  o New option: pause for X seocnds after retrieving Y IP addresses.

* Version 1.06:
  o Added command-line support.
  o Integration with CurrPorts utility.

* Version 1.05: Added support for AfriNIC IP Addresses.
* Version 1.04: Added support for Windows XP visual styles.
* Version 1.03: Fixed bug: garbage characters appeared in the 'Host
  Name' column.
* Version 1.02:
  o Ability to extract the host name from a Web address. For example:
    If you type 'http://www.nirsoft.net/utils', IPNetInfo will extract
    the information about the IP address of www.nirsoft.net (Only when
    'Convert host names to IP addresses' is checked)
  o When the host name that you typed is converted to IP address, the
    original host name will be displayed under the 'Host Name' column
    (Only when 'Convert host names to IP addresses' is checked)

* Version 1.01: Fixed small bug: IP addresses appears in email format
  (email@IPAddress) are now parsed properly.
* Version 1.00: First Release.



System Requirements
===================


* Windows operating system: Windows 98/ME/2000/XP/2003.
* Internet connection.



Using IPNetInfo
===============

IPNetInfo is standalone program, so it doesn't require any installation
process or additional DLLs. In order to start using it, simply copy the
executable file (ipnetinfo.exe) to any folder you like, and run it.

When you run IPNetInfo, the "Choose IP Addresses" window appears. You
have to type one or more IP addresses separated by comma, space, or CRLF
characters. If you want to find the origin of email message that you
received, copy the entire message header to the clipboard, and then click
the "Paste" button.
You can also use the following advanced options:
* Resolve IP addresses: If you select this option, all IP addresses are
  converted back to the host name. The resolved host name is displayed in
  'Resolved Name' column.
* Convert host names to IP addresses: If you select this option, all
  host names that you type will be converted to IP addresses. You can use
  this option if you want to know who owns the IP address of specific Web
  site (For example: If you type 'www.yahoo.com', you'll get the
  information about the IP address of Yahoo Web site)
  You should not select this option for message headers.
* Load only the last IP address: In most email messages, the last IP
  address in the message headers is the address of the computer that sent
  the message. So if you select this option for message headers, you'll
  get the desired IP address in most cases (but not in all of them !).
  However, for finding the origin of unsolicited mail, it's not
  recommended to use this option, because many spammers add fake headers
  and IP addresses in order to deceive the user who tries to trace them.
  When you try to trace the origin of unsolicited mail, you should
  examine all IP addresses that appears in the message headers.

After choosing the desired options and IP addresses, click the 'OK'
button in order to start retrieving the IP addresses information.
After the data is retrieved, the upper pane displays a nice summary of
all IP addresses that you requested, including the owner name, country,
network name, IP addresses range, contact information, and more. You can
view this summary in your browser as HTML report, copy it to the
clipboard, or save it as text/HTML/XML file.
When you click a particular item in the upper pane, the lower pane
displays the original WHOIS record. You can copy the original WHOIS
records to the clipboard, or save them to text file by using "Save Whois
Records" option.

Notice: The IP addresses summary in the upper pane displays only partial
information, If you want to contact the owner of IP address for reporting
about spam/abuse problems, you should also look at the full Whois record
in the lower pane.



Non-Public IP Addresses
=======================

IPNetInfo always ignores the following special IP address blocks, because
they are not used as public Internet addresses:
* 10.0.0.0 - 10.255.255.255
* 127.0.0.0 - 127.255.255.255
* 169.254.0.0 - 169.254.255.255
* 172.16.0.0 - 172.31.255.255
* 192.168.0.0 - 192.168.255.255
* 224.0.0.0 - 239.255.255.255



Command-Line Options
====================



/ipfile <Filename>
Load IP addresses from the specified file.
For example:
ipnetinfo.exe /ipfile "c:\temp\ip-list.txt"

/ip <IP Address>
Load IPNetInfo with the specified IP address.
For example:
ipnetinfo.exe /ip 216.239.59.103



Translating IPNetInfo to other languages
========================================

IPNetInfo allows you to easily translate all menus, dialog-boxes, and
other strings to other languages.
In order to do that, follow the instructions below:
1. Run IPNetInfo with /savelangfile parameter:
   ipnetinfo.exe /savelangfile
   A file named ipnetinfo_lng.ini will be created in the folder of
   IPNetInfo utility.
2. Open the created language file in Notepad or in any other text
   editor.
3. Translate all menus, dialog-boxes, and string entries to the
   desired language. Optionally, you can also add your name and/or a link
   to your Web site. (TranslatorName and TranslatorURL values) If you add
   this information, it'll be used in the 'About' window.
4. After you finish the translation, Run IPNetInfo, and all translated
   strings will be loaded from the language file.
   If you want to run IPNetInfo without the translation, simply rename
   the language file, or move it to another folder.


License
=======

This utility is released as freeware. You are allowed to freely
distribute this utility via floppy disk, CD-ROM, Internet, or in any
other way, as long as you don't charge anything for this. If you
distribute this utility, you must include all files in the distribution
package, without any modification !



Disclaimer
==========

The software is provided "AS IS" without any warranty, either expressed
or implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not
be liable for any special, incidental, consequential or indirect damages
due to loss of data or any other reason.



Feedback
========

If you have any problem, suggestion, comment, or you found a bug in my
utility, you can send a message to nirsofer@yahoo.com
