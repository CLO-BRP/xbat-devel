function [db_log = db_log_create(name, sound, lib)

% db_create_log - create sqlite based log
% ---------------------------------------
%
% db_log = db_log_create(name, sound, lib)
%
% Input:
% ------
%  name - log name
%  sound - parent sound
%  lib - container library
%
% Output:
% -------
%  db_log - db_log handles

%--
% handle input
%--

db_log = db_log_struct;

if nargin < 3
	lib = get_active_library;
end 

if isempty(lib)
	return;
end

if nargin < 2
	sound = get_active_sound;
end

if ischar(sound)
	sound = get_library_sound(lib, 'name', sound);
end

if isempty(sound)
	return;
end

if ~proper_filename(name)
	error('Log name must be proper filename.');
end

%--
% create root directory
%--

[root, exists] = create_dir(fullfile(lib.path, sound_name(sound), 'Logs', name));

% NOTE: return if the log exists or we can't create its root

if isempty(root)
	return;
end

%--
% create events database
%--

events = [root, filesep, 'Events.db'];

[status, result] = sqlite_cli(events, create_event_table);

%--
% fill db_log struct
%--

% NOTE: to work with the database logs we need to handle various files

db_log.root = root;

db_log.events = events;


%-------------------------
% DB_LOG_STRUCT
%-------------------------

% TODO: consider if we need a struct or if we just work with the root

function db_log = db_log_struct

db_log.root = '';

db_log.events = '';

% NOTE: this will be an array of measure database structs, consider name and version

db_log.measure = [];



% FIRST LAYER

% db_log = db_log_create(name, sound, lib)

% db_logs = get_db_logs(lib, sound, name, select)

% db_log = open_db_log(par, db_log)

% db_log = close_db_log(par, name)

% NOTE: now we will only build events in memory as needed

% count = db_log_add_events(db_log, events)

% events = db_log_select_events(db_log, select, action)

% events = get_events_in_page(db_log, page)

% SECOND LAYER

