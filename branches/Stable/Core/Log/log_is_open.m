function [g, h] = log_is_open(log, h)

% log_is_open - try to see whether log is open
% --------------------------------------------
%
% g = log_is_open(log, h)
%
% Input:
% ------
%  log - log structure
%  h - figures to check
%
% Output:
% -------
%  g - handle of figure with open log
%  h - figures checked

%--
% set figures to check
%--

% at the moment we only check the sound browser windows

if (nargin < 2)
	h = get_xbat_figs('type', 'sound');
end

%--
% return if there are no candidate windows open
%--

if isempty(h)
	g = []; return;
end

%--
% loop over figures checking for log
%--
	
g = [];

for k = 1:length(h)
	
	%--
	% get userdata
	%--
	
	data = get(h(k),'userdata');
	
	%--
	% there are some logs in this figure
	%--
	
	if ~isempty(data.browser.log)
		
		%--
		% compare log file names
		%--
				
		ix = find(strcmp(struct_field(data.browser.log, 'file'), log.file));
		
		%--
		% compare log paths
		%--
				
		if ~isempty(ix)
			
			if strcmp(data.browser.log(ix).path, log.path)
				
				% TODO: we only want to have a log open at the moment, however we
				% may allow various logs open if we implement the read-only
				% option
				
				g = h(k); return;
				
			end
		end
		
	end
	
end
	
