function db_logs = get_db_logs(lib, sound, name, select)

%--
% handle input
%--

if nargin < 4
	select = '';
end 

if nargin < 3
	name = '';
end

if nargin < 2
	sound = [];
end

if nargin < 1 || isempty(lib)
	lib = get_active_library;
end

%--
% get library logs
%--

if isempty(sound) 
	sound = get_library_sounds(lib);
end

log_roots = strcat(lib.path, sound_name(sound), filesep, 'Logs')'


