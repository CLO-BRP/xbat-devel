function pitch = spectrum_to_pitch(spec, nyq)

[pix, val] = fast_peak_valley(spec, 1);

f = linspace(0, nyq, size(spec, 1));

[ignore, ix] = sort(mean(val,1)); 

ix = flipud(ix(:));

disp(' ');

for k = 1:min(length(ix), 10)
	
	pitch(k) = f(pix(ix(k)));
	
% 	freq_to_note(pitch(k));
	
end

