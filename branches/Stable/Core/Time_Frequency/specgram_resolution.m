function [dt, df] = specgram_resolution(param, rate)

% specgram_resolution - spectrogram time frequency resolution
% ----------------------------------------------------
%
% [dt, df] = specgram_resolution(param, rate)
%
% Input:
% ------
%  param - spectrogram parameters
%  rate - sample rate
%
% Output:
% -------
%  dt - time resolution
%  df - frequency resolution

%--
% time frequency resolution
%--

overlap = round(param.fft * (1 - param.hop));

hop_samples = param.fft - overlap;

dt = (hop_samples * param.sum_length) / rate;

if nargout < 2
	return;
end

% NOTE: this may not be exact

nyq = rate / 2;

bins = floor(param.fft / 2) + 1;

df = nyq / (bins - 1);
	

% df = rate / param.fft;
