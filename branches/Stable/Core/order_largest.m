function [v,ix,Y] = order_largest(X,k)

% order_largest - compute largest elements in an array
% ----------------------------------------------------
%
% [v,ix,Y] = order_largest(X,k)
%
% Input:
% ------
%  X - array of values
%  k - largest position indices
%
% Output:
% -------
%  v - largest values
%  ix - largest value indices in input array
%  Y - partiallt ordered array

%--
% check and sort desired indices
%--

if (any(k ~= round(k)) | any(k < 1))
	disp(' ');
	error('Order index values must be positive integers.');
end

% sort from largest to smallest

[k,kix] = sort(k(:)');
k = fliplr(k);

%--
% copy input array and invert order of values by changing sign
%--

Y = -X;

%--
% compute values and indices of largest values
%--

% the mex file changes the variable Y

[v,ix] = kth_smallest_(Y,k);

%--
% reorder output and fix signs on values
%--

v = fliplr(v);
v = -v(kix);

ix = fliplr(ix);
ix = ix(kix);

%--
% output array, change signs again
%--

if (nargout > 2)
	Y = -Y;
end
