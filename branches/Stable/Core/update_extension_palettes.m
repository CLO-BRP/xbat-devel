function pals = update_extension_palettes(par, data)

% update_extension_palettes - update extension palettes when logs change
% ----------------------------------------------------------------------
%
% pals = update_extension_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6335 $
% $Date: 2006-08-28 18:03:45 -0400 (Mon, 28 Aug 2006) $
%--------------------------------

%------------------------------------------------------
% HANDLE INPUT
%------------------------------------------------------

%--
% check browser handle input
%--

if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%------------------------------------------------------
% GET DETECTOR PALETTES
%------------------------------------------------------

%--
% get detector names
%--

name = {data.browser.sound_detector.ext.name};

%--
% get detector palettes by name
%--

pals = [];

for k = length(name):-1:1

	pal = get_palette(par, name{k}, data);

	if ~isempty(pal)
		pals(end + 1) = pal;
	end
	
end

% NOTE: there are no palettes to update

if isempty(pals)
	return;
end

%------------------------------------------------------
% UPDATE PALETTES
%------------------------------------------------------

%--
% get open logs state
%--

state = ~isempty(data.browser.log);

%--
% update detector palettes
%--

for k = 1:length(pals)

	%--
	% set output log display
	%--
	
	if ~state
		L = {'(No Open Logs)'}; m = 1;
	else
		L = file_ext(struct_field(data.browser.log, 'file')); m = data.browser.log_active;
	end

	handles = get_control(pals(k), 'output_log', 'handles');
		
	set(handles.obj, ...
		'string', L, 'value', m ...
	);

	%--
	% set output log and scan enable state
	%--

	set_control(pals(k), 'scan', 'enable', state);

	set_control(pals(k), 'output_log', 'enable', state);

end

%------------------------------------------------------
% CHECK FOR MEASURE PALETTES
%------------------------------------------------------






