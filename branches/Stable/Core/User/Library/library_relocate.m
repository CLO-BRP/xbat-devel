function [lib, user] = library_relocate(user, old_path, new_path)

lib = [];

active = isequal(user, get_active_user);

%--
% if new path is not provided, we need to look
%--

if nargin < 3
	
	new_path = uigetdir(pwd, 'Select location of missing library.');
	
	if ~new_path
		return;
	end

end

%--
% return for empty new path
%--

if isempty(new_path)
	return;
end

%----------------------
% UPDATE LIBRARY
%----------------------

%--
% try to load library from new location
%--

[root, name] = path_parts(new_path); file = get_library_file(root, name);

lib = load_library(file); 

if isempty(lib)
	return;
end

%--
% update library path and save
%--

lib.path = [new_path, filesep]; 

library_save(lib);

%-----------------------------
% UPDATE USER
%-----------------------------

ix = find(strcmp(old_path, user.library));

user.library{ix} = lib.path;

user_save(user);

if active
	set_env('xbat_user', user);
end




