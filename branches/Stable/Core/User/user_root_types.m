function types = user_root_types

% user_root_types - type of user roots
% ------------------------------------
%
% types = user_root_types
%
% Output:
% -------
%  types - type of user directories

types = {'base', 'libraries', 'preferences', 'presets', 'sessions'};