function delete_user(user)

% delete_user - completely remove user from system
% ------------------------------------------------
% delete_user(user)
%
% Input:
% ------
%  user - user

%--
% return early for default user
%--

if strcmp(user.name, 'Default')
	warn_dialog({'You can''t delete the Default user!'}); return;
end

%--
% Find this user in the file system
%--

user_path = user_root(user);

%--
% confirm that we really want to do this
%--

str = {'Are you sure you want to completely', ...
	['remove user "', user.name, '" , with full path: '], ...
	['"' user_path '" ?'] ...
};

answer = quest_dialog(str, 'Confirm Delete');

if ~strcmp(answer, 'Yes')
	return;
end

%--
% unsubscribe user from all non-default libraries
%--

lib = get_libraries(user);

for j = 1:length(lib)
	
	if strcmp(get_library_name(lib(j), user), 'Default')
		continue;
	end

	user_unsubscribe(lib(j), user);
	
end

%--
% backup user
%--

user_backup(user);

%--
% delete directory
%--

rmdir(user_path, 's');

set_active_user(get_active_user);


