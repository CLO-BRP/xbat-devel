function migrate_xbat(type, source)

% migrate_xbat - migrate older version files to current version
% -------------------------------------------------------------
%
% migrate_xbat(type, source)
%
% Input:
% ------
%  type - migration type
%  source - origin of migration

% NOTE: migration works as concept, since we are working only with XBAT files

%------------------
% HANDLE INPUT
%------------------

if nargin < 1
	
	result = migrate_dialog; 
	
	if ~strcmp(result.action, 'ok')
		return;
	end
	
	type = result.values.type{1};
	
	source = result.values.source;
	
end

%------------------
% MIGRATE
%------------------

%--
% set up waitbar
%--

migrate_wait(type);

%--
% migrate
%--

migrate_fun = ['migrate_', lower(type)];

if ~isempty(which(migrate_fun))
	
	migrate_fun = str2func(migrate_fun); 

	result = migrate_fun(source);
	
end

%--
% clean up waitbar
%--

migrate_wait('finish');

%--
% handle result
%--

if isempty(result)
	warn_dialog(['No ', type, ' found in specified location.'], 'Invalid Migration Source'); return;
end

%--
% update xbat palette state
%--

xbat_palette('User');






