function [users, cancel] = migrate_users(source)

% migrate_users - migrate users from previous version
% ---------------------------------------------------
% 
% users = migrate_users(source)
% 
% Input:
% ------
%  source - migration source
%
% Output:
% -------
%  users - migrated users

%--
% get source users root
%--

root = users_root(source, 0);

if isempty(root)
	users = []; return;
end

%--
% migrate users
%--

users = {}; library = {};

names = get_folder_names(root);

if ~length(names)
	users = []; return;
end

migrate_wait('Users', length(names), names{1});

for k = 1:length(names)
	
	[users{end + 1}, library{end + 1}, cancel] = migrate_user(fullfile(root, names{k}), 0);
	
	if cancel
		break;
	end
	
end

%--
% update progress waitbar
%--

libs = {};

for k = 1:numel(library)
	libs = {libs, library{k}{:}};
end

[sounds, logs] = library_folder_contents(libs);

count = numel(users) + numel(libs) + numel(sounds) + numel(logs);

set_migrate_wait_ticks(count);

%--
% migrate libraries and re-subscribe
%--

for k = 1:length(users)
	[ignore, cancel] = migrate_libraries(library{k}, users{k});
	
	if cancel
		break;
	end
	
end



