function toolbox = get_toolbox(name, local)

% GET_TOOLBOX get toolbox information struct
%
% toolbox = get_toolbox(name)

%--
% handle input
%--

if nargin < 2
	local = 1;
end

%--
% get root using name and check it exists
%--

root = toolbox_root(name, local);

if ~exist_dir(root)
	toolbox = [];
end

%--
% pack toolbox information struct
%--

toolbox.name = name;

toolbox.root = root;

