function cache_set(cache)

% cache_set - put or update cache data 
% ------------------------------------
%
% cache_set(cache)
%
% Input:
% ------
%  cache - cache structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get environment userdata
%--

data = get(0,'userdata');

%--
% cache is not empty
%--

if (isfield(data,'cache') && length(data.cache))
	
	%--
	% check for cache update
	%--
	
	labels = struct_field(data.cache,'label');
	ix = find(cache.label == labels);
	
	%--
	% update cache or set new cache
	%--
	
	if (~isempty(ix))
		data.cache(ix) = cache; 
	else
		data.cache(length(data.cache) + 1) = cache;
	end

%--
% empty cache, create new cache
%--

else
	
	data.cache = cache;
	
end

%--
% update environment userdata
%--

set(0,'userdata',data);
