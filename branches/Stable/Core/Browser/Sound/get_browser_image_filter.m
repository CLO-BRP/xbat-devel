function ext = get_browser_image_filter(varargin)

% get_browser_image_filter - get browser imagefilter
% --------------------------------------------------
%
% ext = get_browser_image_filter(par,name,data)
%
% Input:
% ------
%  par - parent browser
%  name - image filter name
%  data - parent state
%
% Output:
% -------
%  ext - browser image filter

ext = get_browser_extension('image_filter',varargin{:});

