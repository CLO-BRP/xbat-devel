function handles = geometry_plot(par, pal, ax, data)

% geometry_plot - plot array geometry
% -----------------------------------
%
% handles = geometry_plot(par, ax)
%
% Input:
% ------
%  par - browser handle
%  ax - display axes
%
% Output:
% -------
%  handles - structured handles

%--
% handle input
%--

if nargin < 3 || isempty(ax)
	
	ax = findobj(pal, 'tag', 'Geometry_Plot', 'type', 'axes');
	
	if isempty(ax)
		return;
	end
	
end

if nargin < 1 || isempty(par)
	par = get_active_browser;
end

if nargin < 4 || isempty(data)
	data = get_browser(par);
end

G = get_geometry(data.browser.sound);

ch = get_channels(data.browser.channels);

%--
% check whether we are here through clicking
%--

if (ax == overobj('axes'))
	flag = 1;
else
	flag = 0;
end

%--
% update display
%--

array_line = findobj(ax, 'tag', 'ARRAY_LINE');

select_line = findobj(ax, 'tag', 'SELECT_LINE');

if isempty(array_line)
	
	delete(get(ax,'children'));
	
	array_line = line(G(:,1), G(:,2), ...
		'parent', ax, ...
		'tag', 'ARRAY_LINE', ...
		'linestyle', 'none', ...
		'marker', 'o', ...
		'markerfacecolor', [.5 .5 .5], ...
		'markeredgecolor', [0 0 0], ...
		'hittest', 'off' ...
	);

	select_line = line(G(1,1), G(1,2), ...
		'parent', ax, ...
		'tag', 'SELECT_LINE', ...
		'linestyle', 'none', ...
		'marker', 'o', ...
		'markersize', 10, ...
		'color', [1 0 0], ...
		'hittest', 'off' ...
	);

end

xlim = fast_min_max(G);

dx = 0.1 * diff(xlim);

xlim = [xlim(1) - dx, xlim(2) + dx];

set(ax, 'xlim', xlim, 'ylim', xlim);

%--
% select closest channel
%--

if (flag == 1)

	%--
	% get closest channel
	%--

	p = get(ax, 'currentpoint');

	p = p(1,:); p(3) = 0;
	
	d = sum((G - repmat(p, [size(G,1),1])).^2, 2);

	[ignore, ix] = sort(d);

	%--
	% display highlight
	%--

	set(select_line, ...
		'xdata', G(ix(1), 1), ...
		'ydata', G(ix(1), 2) ...
	);

	%--
	% update select channel
	%--
	
	g = findobj(pal, 'tag', 'Select Channel', 'style', 'popupmenu');
	
	set(g, 'value', ix(1));

	browser_controls(par, 'Select Channel', g);
	

else

	%--
	% get select channel value
	%--
	
	g = findobj(pal, 'tag', 'Select Channel', 'style', 'popupmenu');
	
	value = get(g, 'value');

	%--
	% display highlight
	%--
	
	set(select_line, ...
		'xdata', G(value, 1), ...
		'ydata', G(value, 2) ...
	);

end

set(get(ax, 'children'), 'hittest', 'off');
