function ext = get_active_detector(varargin)

% get_active_detector - get browser active detector
% -------------------------------------------------
%
% ext = get_active_detector(par,data)
%
% Input:
% ------
%  par - parent browser
%  data - parent state
%
% Output:
% -------
%  ext - active detector extension

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2173 $
% $Date: 2005-11-21 18:26:27 -0500 (Mon, 21 Nov 2005) $
%--------------------------------

ext = get_active_extension('detector',varargin{:});