function tag = browser_tag(sound, lib, user)

% browser_tag - build browser tag
% -------------------------------
%
% tag = browser_tag(sound, lib, user)
%
% Input:
% ------
%  sound - sound
%  lib - library
%  user - user
%
% Output:
% -------
%  tag - tag

%----------------
% HANDLE INPUT
%----------------

if (nargin < 3) || isempty(user)
	user = get_active_user;
end

if (nargin < 2) || isempty(lib)
	lib = get_active_library;
end

%----------------
% BUILD TAG
%----------------

% TODO: extend to other types of browser

type = 'XBAT_SOUND_BROWSER'; sep = '::';

tag = [type, sep, user.name, sep, get_library_name(lib), sep, sound_name(sound)];

