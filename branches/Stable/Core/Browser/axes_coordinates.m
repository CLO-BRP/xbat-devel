function [x,y,ctrl] = axes_coordinates(event, sound, khz, map) 

% axes_coordinates - get coordinates to draw event and control points
% -------------------------------------------------------------------
%
% [x,y,ctrl] = axes_coordinates(event, sound, khz, map) 
%
% Input:
% ------
%  event - event
%  sound - sound
%  khz - kilohertz display indicator
%  map - sound time mapping indicator
%
% Output:
% -------
%  x - x coordinates
%  y - y coordinates
%  ctrl - control point coordinate matrix
 
%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set sound time mapping
%--

if (nargin < 4) || isempty(map)
	map = 1;
end

%--
% set kilohertz display
%--

if (nargin < 3) || isempty(khz)
	khz = 1;
end

%---------------------------------
% COMPUTE EVENT VERTICES
%---------------------------------

%--
% prepare event considering axes mapping
%--

% TIME

start = event.time(1);

if map && ~isempty(sound)
	start = map_time(sound, 'slider', 'record', start);
end

stop = start + diff(event.time);

event.time = [start, stop];

% FREQUENCY

if khz
	event.freq = event.freq / 1000;
end

%--
% get event display points
%--

[vertex, control] = get_event_display_points(event); 

x = vertex.x; 

y = vertex.y; 

ctrl = control;