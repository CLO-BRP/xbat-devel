function [ix, obj] = find_tags(obj, match, logic)

% find_tags - find objects based on tags
% --------------------------------------
%
% ix = find_tags(obj, match, logic)
%
% Input:
% ------
%  obj - tagged objects
%  match - tags to match
%  logic - logic to user for combining search
%
% Output:
% -------
%  ix - objects found indices

%--
% set default logic ahd check logic
%--

if nargin < 3
	logic = 'and';
end

if ~ismember(logic, {'and', 'or'})
	error('Unrecognized logical operator.');
end

%--
% check tags to match input
%--

if ischar(match)
	match = {match};
end

if ~is_tags(match)
	error('Tags to match input must be tags.');
end

%--
% compute index array
%--

ix = true(size(obj)); tags = get_tags(obj); 

for k = length(tags):-1:1
	
	switch logic
		
		% NOTE: if any of the tags to match is missing remove object
		
		case 'and'
			if any(~ismember(match, tags{k}))
				ix(k) = 0; 
			end
		
		% NOTE: if none of the tags to match belongs to tags remove
		
		case 'or'
			if ~any(ismember(match, tags{k}))
				ix(k) = 0;
			end
			
	end
	
end

%--
% select objects if needed
%--

if nargout > 1
	obj = obj(ix);
end