function out = double_space(in)

% double_space - add alternating empty strings to string cell array
% -----------------------------------------------------------------
%
% out = double_space(in)
%
% Input:
% ------
%  in - cell array of strings
% 
% Output:
% -------
%  out - double spaced cell array of strings

for k = 1:length(in) - 1
	out{2*k - 1} = in{k};
	out{2*k} = ' ';
end

out{end + 1} = in{end};