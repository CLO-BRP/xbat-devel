function s = integer_unit_string(x, unit, plural)

if nargin < 3
	plural = 1;
end

s = int2str(round(x));

if nargin < 2 || isempty(unit)
	return;
end

if (x ~= 1) && plural
	unit = string_plural(unit);
end

s = [s, ' ', unit];

