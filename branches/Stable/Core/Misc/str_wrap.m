function t = str_wrap(s,n)

% str_wrap - wrap string into paragraph
% -------------------------------------
%
% t = str_wrap(s,n)
%
% Input:
% ------
%  s - input string
%  n - line length (def: 72)
%
% Output:
% -------
%  t - paragraph string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-07-06 13:36:09-04 $
%--------------------------------

%--
% set line length
%--

if (nargin < 2)
	n = 72;
end

%--
% get blanks
%--

ix = findstr(s,' ');

%--
% compute linebreaks
%--

t = mod(ix,n);
b = [1 ix(find(t(1:end - 1) > t(2:end))) length(s)];

%--
% add linebreaks
%--

t = [s(b(1):b(2)) '\n'];

for k = 2:length(b) - 1
	t = [t s(b(k) + 1:b(k + 1)) '\n'];
end

t = t(1:end - 2);




