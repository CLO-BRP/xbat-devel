function str = disp_link(file, text)

% link_file - create a matlab link to a file
% ------------------------------------------
%
% str = link_file(file, text)
%
% Input:
% ------
%  file - file
%  text - link text
%
% Output:
% -------
%  str - link string


if nargin < 2 || isempty(text)
	text = file;
end

[ignore, ext] = file_ext(file);

switch ext
	
	case 'html' 
		
		type = 'matlab:web';
		
	otherwise
		
		type = 'matlab:edit';
		
end

str = ['<a href="', type, '(''', file, ''')">', text, '</a>'];

%--
% just display the link if we didn't ask for output
%--

if ~nargout
	disp(str);
end
