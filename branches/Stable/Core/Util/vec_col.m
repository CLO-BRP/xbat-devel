function x = vec_col(X)

% vec_col - concatenate columns of matrix
% ---------------------------------------
%
% x = vec_col(X)
% 
% Input:
% ------
%  X - input matrix
% 
% Output:
% -------
%  x - column vector obtained from matrix

% TODO: check for matrices

x = X(:);


