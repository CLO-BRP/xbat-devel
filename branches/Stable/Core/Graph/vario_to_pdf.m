function p = vario_to_pdf(g,b)

% vario_to_pdf - variogram to pdf transformation
% ----------------------------------------------
% 
%  p = vario_to_pdf(g)
%
% Input:
% ------
%  g - variogram
%  b - linking interval
%
% Output:
% -------
%  p - probability

%--
% parse interval
%--

b = parse_interval(b);

%--
% compute intensity
%--

g = sqrt(g);
p = zeros(size(g));

for k = 1:length(g)
	p(k) = 2*(normcdf(b(2),0,g(k)) - normcdf(b(1),0,g(k)));
end

%--
% discrete normalization to get probability
%--

p = p/sum(p);

