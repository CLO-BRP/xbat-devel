%--
% get parameters
%--

P = {'Size of Random Image','Probability of Occupation','Dilation SE','Number of Neighbors'};
T = 'Connected Component Labelling';
NL = 1;
DEF = {'512','0.02','se_ball(4)','4'};
			
AN = input_dialog(P,T,NL,DEF);

n = eval(AN{1});
p = eval(AN{2});
SE = eval(AN{3});
nn = eval(AN{4});
