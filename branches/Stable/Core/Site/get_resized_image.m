function [resized, file] = get_resized_image(site, name, width)

% GET_RESIZED_IMAGE get smaller resized image from site image
%
% [resized, file] = get_resized_image(site, name, width)

%--
% set default width 
%--

if nargin < 3
	width = 256;
end

%--
% get parent image
%--

file = get_image(site, name);

if isempty(file)
	return;
end

%--
% get resize ratio to see if we need to resize
%--

info = imfinfo(file); M = width / info.Width;

% NOTE: the image is smaller than requested, there is no need to resize

if M >= 1
	resized = name; return;
end
		
%--
% build resized image name
%--

[root, name, ext] = fileparts(file);

resized = [name, '__', int2str(width), ext];

%--
% check for resized file in assets
%--

out = [root, filesep, resized];

if exist(out, 'file')
	file = out; return;
end

%--
% create resized image if needed
%--

imwrite(imresize(imread(file), M, 'bicubic'), out);

% NOTE: this will allow the system to find the new image

sites_cache_clear;

while ~exist(out, 'file')
	disp('Waiting on image resize ...'); pause(0.1);
end

file = out;

