function  [files, updated] = build_site(site, theme, opt)

% BUILD_SITE build site
%
% [files, updated] = build_site(site, theme, opt)

% TODO: define 'opt' and implement passing 

start = clock;

disp(' ');

disp(['Starting to build site ''', site, ''' (', datestr(start), ')']);

sites_cache_clear;

%--
% get site model if needed
%--

% NOTE: we consider that input is either site name or model

if ischar(site)
	model = model_site(site);
else
	model = site;
end

%--
% add theme to model
%--

if (nargin < 2)
	model.theme = [];
else
	model.theme = theme;
end

%--
% build pages
%--

N = length(model.pages); files = cell(N, 1); updated = zeros(N, 1);

disp(' ');

for k = 1:N
	
	[files{k}, updated(k)] = build_page(model, model.pages(k));
	
	disp(['Updating <a href="matlab:web(''', files{k}, ''', ''-browser'')">', files{k}, '</a>']);
	
end

disp(' ');

stop = clock;

disp(['Finished building site ''', site, ''' (', datestr(stop), ', ', sec_to_clock(etime(stop, start)), ')']);

disp(' ');

if ~nargout
	clear files;
end 
