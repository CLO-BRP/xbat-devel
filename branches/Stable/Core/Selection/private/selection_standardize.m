function sel = selection_standardize(sel)

% selection_standardize - make selection have positive offset
% -----------------------------------------------------------
%
% sel = selection_standardize(sel)
%
% Input:
% ------
%  sel - selection
%
% Output:
% -------
%  sel - standard selection

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%--
% update anchor if needed
%--

if sel.offset(1) < 0
	sel.anchor(1) = sel.anchor(1) + sel.offset(1);
end

if sel.offset(2) < 0
	sel.anchor(2) = sel.anchor(2) + sel.offset(2); 
end

%--
% make offsets positive
%--

sel.offset = abs(sel.offset);