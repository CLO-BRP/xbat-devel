function selection_log(par, log, data)

% selection_log - log selection to chosen or active log
% -----------------------------------------------------


if nargin < 3 || isempty(data)
	data = get_browser(par);
end

logs = get_browser(par, 'log', data);

if isempty(log)
	m = data.browser.log_active;
elseif ischar(log)	
	m = find(strcmp(logs,log));
else
	m = log;
end
	
%--
% return for empty selection
%--

if isempty(data.browser.selection.handle)
	return;
end

%--
% get annotation information
%--

ANNOT = data.browser.annotation.annotation;
ANNOT_NAME = data.browser.annotation.name;

%--
% add existing selection
%--

if isempty(data.browser.annotation.active)
	flag = log_update(par,m); % this function modifies the userdata
else

	% add option in annotation interface to set default log in
	% annotation dialog

	ixa = find(strcmp(ANNOT_NAME,data.browser.annotation.active));
	
	flag = feval(ANNOT(ixa).fun,'selection',par);
	
end

%--
% update display
%--

if flag

	browser_display(par, 'events'); % the available data is not current

	pal = get_palette(par, 'Event');
	
	if isempty(pal)
		return;
	end

	handles = get_control(pal, 'find_events', 'handles');

	browser_controls(par, 'find_events', handles.obj);	

end