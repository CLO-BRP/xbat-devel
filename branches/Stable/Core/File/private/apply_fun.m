function [line, state] = apply_fun(line, fun, args, state)

% apply_fun - apply line processing function
% ------------------------------------------
%
% [line, state] = apply_fun(line, fun, args, state)
%
% Input:
% ------
%  line - line to process
%  fun - process line function handle
%  args - arguments for process line function
%  state - processor state
%
% Output:
% -------
%  line - processed line
%  state - processor state

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% return quickly on no processing
%--

if isempty(fun)
	return;
end

%--
% consider availability of arguments in evaluation
%--

try
	
	if nargin < 4

		% NO STATE
		
		if isempty(args)
			line = fun(line);
		else
			line = fun(line, args{:});
		end
		
	else

		% STATE
		
		if isempty(args)
			[line, state] = fun(line, state);
		else
			[line, state] = fun(line, state, args{:});
		end

	end
	
catch
	
	nice_catch(lasterror, 'WARNING: Failed to process line.');

end