function [v,ix,Y] = order_smallest(X,k)

% order_smallest - compute smallest elements in an array
% ------------------------------------------------------
%
% [v,ix,Y] = order_smallest(X,k)
%
% Input:
% ------
%  X - array of values
%  k - smallest position indices
%
% Output:
% -------
%  v - smallest values
%  ix - smallest value indices in input array
%  Y - partially ordered array

%--
% check and sort desired indices
%--

if (any(k ~= round(k)) | any(k < 1))
	disp(' ');
	error('Order index values must be positive integers.');
end

% sort from largest to smallest

[k,kix] = sort(k(:)');
k = fliplr(k);

%--
% copy input array
%--

Y = X;

%--
% compute values and indices of smallest values
%--

% the mex file changes the variable Y

[v,ix] = kth_smallest_(Y,k);

%--
% reorder output
%--

v = fliplr(v);
v = v(kix);

ix = fliplr(ix);
ix = ix(kix);
