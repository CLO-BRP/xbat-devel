function value = event_in_page(event, sound, page)

% event_in_page - check whether events are in page
% ------------------------------------------------
%
% value = event_in_page(event, sound, page)
%
% Input:
% ------
%  event - event
%  sound - sound
%  page - page
%
% Output:
% -------
%  value - in page indicator

% TODO: consider adding boundary hits and selection information to output

%--------------------
% HANDLE INPUT
%--------------------

% TODO: 'is_proper_page' function, this could be useful for ducks everywhere

%--------------------
% PERFORM TEST
%--------------------

%--
% TIME SELECTION
%--

%--
% get event times
%--

% NOTE: this is the selection case, selection event times are in 'slider' time

if isempty([event.id])
	
	time = event.time;
	
else
	
	% NOTE: logged event times are in 'record' time, page in 'slider' time map!
	
	time = reshape([event.time]', 2, [])';
		
	time = map_time(sound, 'slider', 'record', time);

end

%--
% check page time
%--	

% NOTE: a page struct contains start and duration fields, possibly stop

page.stop = page.start + page.duration;

value = ...
	((time(:,1) > page.start) & (time(:,1) < page.stop)) | ...
	((time(:,2) > page.start) & (time(:,2) < page.stop)) ...
;

if ~any(value)
	return;
end

%--
% CHANNEL SELECTION
%--

% NOTE: return if there is no channel selection to be done

if ~isfield(page, 'channel')
	return;
end

if isempty(page.channel)
	return;
end

%--
% get event channels
%--

% NOTE: we convert to integers to use the faster table lookup, note the transpose

channel = uint8([event(value).channel])';

%-- 
% check channels and update test
%--

% NOTE: build a channel indicator table considering zero offset

table = zeros(1, length(sound.channels) + 1); table(page.channel + 1) = 1;

% NOTE: this is an interesting use of logical indexing

value(value) = value(value) & lut_apply(channel, table);

