function state = harray_toolbar(par, state)

% harray_toolbar - show and hide harray toolbar
% -------------------------------------------------
%
% state = harray_toolbar(par, state)
%
% Input:
% ------
%  par - parent figure
%  state - 'on' or 'off' (def: toggle state)
%
% Output:
% -------
%  state - current toolbar state

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

% TODO: make toolbar update available from this function

%-------------------------------------
% HANDLE INPUT
%-------------------------------------

%--
% set default state action
%--

if (nargin < 2) || isempty(state)
	state = 'toggle';
end

%--
% set parent figure
%--

if (nargin < 1)
	par = gcf;
end

if isempty(par)
	state = []; return;
end

%-------------------------------------
% UPDATE TOOLBAR STATE
%-------------------------------------

%--
% get harray data
%--

data = harray_data(par);

if isempty(data)
	state = []; return;
end

%--
% update state
%--

switch state
	
	case 'on', data.base.tool.on = 1;
		
	case 'off', data.base.tool.on = 0;
	
	otherwise
		
		data.base.tool.on = double(~data.base.tool.on);
		
		if data.base.tool.on
			state = 'on';
		else
			state = 'off';
		end

end

% NOTE: we update harray data

harray_data(par, data);
