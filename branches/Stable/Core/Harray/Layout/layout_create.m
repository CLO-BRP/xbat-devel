function layout = layout_create(m, n)

% layout_create - create hierarchical array level configuration
% -------------------------------------------------------------
%
% layout = layout_create(row, col)
%
% Input:
% ------
%  m - number of rows
%  n - number of columns
%
% Output:
% -------
%  layout - layout structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

% TODO: implement field value input to allow more configuration on create

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set default layout size
%--

if (nargin < 1)
	m = 2; n = 2;
end

if (nargin < 2)
	n = m;
end

%--------------------------------------------
% CREATE LAYOUT
%--------------------------------------------

%--
% set layout units and current scale
%--

layout.units = 'centimeters';

layout.scale = 1;

%--
% set tool bars
%--

% TODO: allow for multiple toolbars, use a name in some way

layout.tool.on = 0;

layout.tool.size = 0.65;

layout.tool.name = 'DEFAULT';

%--
% set status bar
%--

layout.status.on = 0;

layout.status.size = 0.65;

%--
% set colorbar
%--

layout.color.on = 0;

layout.color.pad = 0.5;

layout.color.size = 1;

%--
% set layout margins
%--

% NOTE: the mnemonic for this margin order is TRBL ('trouble')

layout.margin = 0.5 * ones(1, 4); 

% NOTE: the larger value for top margin and row padding allow titles

layout.margin(1:3) = 0.7;

%--
% set row and column descriptions
%--

% NOTE: the pad values are for the spacing between axes in each dimension

layout.row = dim_create(m, 0.75);

layout.col = dim_create(n, 0.5);


%--------------------------------------------
% DIM_CREATE
%--------------------------------------------

function dim = dim_create(n, pad)

% dim_create - create structure to handle layout dimension
% --------------------------------------------------------
%
% dim = layout_dim(n, pad, units)
%
% Input:
% ------
%  n - number of elements in dimension
%  pad - uniform padding
%
% Output:
% -------
%  dim - layout dimension description

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

dim.size = n;

if (n > 0)
	dim.frac = ones(1, n) / n; dim.pad = pad * ones(1, n - 1);
else
	dim.frac = []; dim.pad = [];
end
