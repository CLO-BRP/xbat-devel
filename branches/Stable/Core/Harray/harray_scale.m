function scale = harray_scale(par, scale)

% harray_scale - set scale for harray layout
% ------------------------------------------
%
% scale = harray_scale(par, scale)
%
% Input:
% ------
%  par - parent figure
%  scale - desired layout scale (def: 1)
%
% Output:
% -------
%  scale - current layout scale

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

%-------------------------------------
% HANDLE INPUT
%-------------------------------------

%--
% set default scale
%--

if (nargin < 2) || isempty(scale)
	scale = 1;
end

%--
% set parent figure
%--

if (nargin < 1)
	par = gcf;
end

if isempty(par)
	scale = []; return;
end

%-------------------------------------
% UPDATE LAYOUT SCALE
%-------------------------------------

%--
% get harray data
%--

data = harray_data(par);

if isempty(data)
	scale = []; return;
end

%--
% scale layouts
%--

data.base = layout_scale(data.base, scale);

data.layout = layout_scale(data.layout, scale);

%--
% update harray data
%--

harray_data(par, data);
