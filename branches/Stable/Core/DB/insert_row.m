function sql = insert_row(in,name)

% insert_row - create table for struct array
% --------------------------------------------
%
% out = insert_row(in,name)
%
% Input:
% ------
%  in - struct array
%  name - table name
%
% Output:
% -------
%  out - struct output 

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% try to get name from input
%--

if nargin < 2
	
	name = inputname(1);
	
	if isempty(name)
		error('Unable to get table name from input name.');
	end
	
end
	
%---------------------------------
% CREATE QUERIES
%---------------------------------

%--
% flatten struct and use field names as column names
%--

in = flatten_struct(in); 

column = fieldnames(in);

%--
% build create table statement
%--

cols = {}; vars = {}; vals = {};

for k = 1:length(column)
	
	cols{end + 1} = column{k};
	
	vars{end + 1} = ['@', column{k}];
	
	vals{end + 1} = in.(column{k});
	
end

sql{1} = ['INSERT OR REPLACE INTO ', name, ' ('];

sql = {sql{:}, cols

sql{end}(end:end+1) = ' )';

%--
% evaluate in DB
%--

sqlite(file, sql, vals);


