function [status, result] = sqlite(file, command, values)

if iscell(command)
	command = [command{:}];
end

if nargin < 3
	status = sqlite_mex(file, command);
else
	status = sqlite_mex(file, command, values);
end

result = [];