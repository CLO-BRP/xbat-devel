#include "sqlite3.h"

#include "mex.h"

#include "matrix.h"

/*
 * SQL EXECUTE CALLBACK
 ----------------------------------------*/

static int callback (void *NotUsed, int argc, char **argv, char **azColName) {
	
	int i;
  
	for (i = 0; i < argc; i++){
		mexPrintf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
  
	mexPrintf("\n");
  
	return 0;
	
}

/*
 * BIND TOKENS
 ----------------------------------------*/

void bind_tokens (sqlite3_stmt *statement, mxArray *params) {
	
	mxArray *cell;
	
	int k, cell_len, str_len, el_size, result;
	
	char *param_str;
	
	if (!mxIsCell(params)) {
		return;
	}
			
	cell_len = mxGetM(params) * mxGetN(params);
	
	for (k = 0; k < sqlite3_bind_parameter_count(statement); k++){
		
		cell = mxGetCell(params, k);
		
		switch (mxGetClassID(cell)) {
			
			case (mxCHAR_CLASS):
				
				str_len = mxGetM(cell) * mxGetN(cell) + 1;
				
				param_str = calloc(str_len, sizeof(char));
				
				mxGetString(cell, param_str, str_len);
				
				result = sqlite3_bind_text(statement, k + 1, (const char *) param_str, str_len, SQLITE_TRANSIENT);
				
				free(param_str);
				
				break;
				
			case (mxDOUBLE_CLASS):
				
				result = sqlite3_bind_double(statement, k + 1, mxGetScalar(cell));
				
				break;
				
			default:
					
				el_size = mxGetElementSize(cell);
					
				result = sqlite3_bind_blob(statement, k + 1, cell, el_size, SQLITE_TRANSIENT);
					
		}
		
	}
	
}

/*
 * MEX FUNCTION
 ----------------------------------------*/

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	
	sqlite3 *db;

	sqlite3_stmt *statement = 0;	
	
	mxArray *cell;
	
	char *zErrMsg = 0;
	
	int k, status, str_len, cell_len, el_size;
  
	char *file_name;

	char *command;
	
	char *param_str;
	
	/*
	 * HANDLE INPUT
	 ----------------------------------------*/
	
	if (nrhs < 2) {
		mexErrMsgTxt("Filename and statement to execute input is required.\n");
	}
	
	/*
	 * get file string
	 */

	str_len = mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1;

	file_name = mxCalloc(str_len, sizeof(char));

	mxGetString(prhs[0], file_name, str_len);  
	
	/*
	 * get statement string
	 */
		
	str_len = mxGetM(prhs[1]) * mxGetN(prhs[1]) + 1;

	command = mxCalloc(str_len, sizeof(char));

	mxGetString(prhs[1], command, str_len);
		
	/*
	 * ACCESS DATABASE
	 ----------------------------------------*/
	
	/*
	 * OPEN DATABASE
	 */
	
	status = sqlite3_open(file_name, &db);
	
	if (status) {
		mexPrintf("Can't open database: %s\n", sqlite3_errmsg(db)); goto done;
	}
	
	/*
	 * PREPARE STATEMENT
	 */
	
	status = sqlite3_prepare(db, command, str_len, &statement, 0);
	
	if (status || statement == NULL) {
		mexPrintf("Problem with SQL statement\n", sqlite3_errmsg(db)); goto done;
	}
	
	/*
	 * BIND TOKENS
	 */
	
	if (nrhs > 2) {
		bind_tokens(statement, (mxArray *) prhs[2]);
	}
	
	/*
	 * EXECUTE STATEMENT
	 */
	
	while ((status = sqlite3_step(statement)) == SQLITE_ROW) {
	
	}
	
	sqlite3_finalize(statement);
	
	/*
	 * CLOSE
	 */

	done:
		
	sqlite3_close(db);
	
	if (nlhs) {
		plhs[0] = mxCreateDoubleScalar((double) status);
	}
	
	mxFree(file_name); 
	
	mxFree(command);
	
}
