function out = get_type_and_constraints(column, value)

% get_type_and_constraints - get type and constraint information
% --------------------------------------------------------------
%
% out = get_type_and_constraints(column, value)
%
% Input:
% ------
%  column - column name
%  value - column instance values
%
% Output:
% -------
%  out - struct output

% TODO: use a sequence of values to infer types

out.constraints = get_constraints(column);

out.type = get_type(value);


%-----------------------------------------------------
% GET_CONSTRAINTS
%-----------------------------------------------------

function str = get_constraints(column)

% get_constraints - get constraints based on naming conventions
% -------------------------------------------------------------
%
% str = get_constraints(column)
%
% Input:
% ------
%  column - field name
%
% Output:
% -------
%  str - constraint description

%--
% initialize constraints and normalize column name
%--

str = cell(0); column = lower(column);

%--
% consider not null conventions
%--

not_null_words = {'created', 'modified', 'author'};
	
if ismember(column, not_null_words)
	str{end + 1} = 'NOT NULL';
end

%--
% consider key conventions
%--

if strcmp(column, 'id')
	
	str{end + 1} = 'NOT NULL PRIMARY KEY'; 
	
else
			
	key_words = {'_id'};

	for k = 1:length(key_words)
		
		if ~isempty(strfind(column, key_words{k}))
			str{end + 1} = 'NOT NULL FOREIGN KEY'; break;
		end
		
	end
	
end

%--
% concatenate constraints
%--

str = strcat(str, {' '}); str = [str{:}]; str(end) = [];


%-----------------------------------------------------
% GET_TYPE
%-----------------------------------------------------

function type = get_type(value)

% get_type - get type description for value
% -----------------------------------------
%
% type = get_type(value)
%
% Input:
% ------
%  value - content of struct field
%
% Output:
% -------
%  type - fragment describing value type

% TODO: add date types to this function

%--
% DEFAULT
%--

% NOTE: return empty for container and object types

type = '';

%--
% NUMERIC
%--

if isnumeric(value)
	
	% NOTE: array contents are stored as blobs, then restored with casting
	
	if numel(value) > 1
		type = 'BLOB'; return;
	end
	
	if isfloat(value)
		type = 'REAL'; return;
	end

	if isinteger(value)
		type = 'INTEGER'; return;
	end
	
	type = 'BLOB';
	
end

%--
% LOGICAL
%--

if islogical(value)
	
	if numel(value) > 1
		type = 'BLOB'; return;
	end
	
	type = 'BOOLEAN'; return;
	
end

%--
% STRINGS
%--

if ischar(value)
	
	type = 'VARCHAR';
	
	% NOTE: we try to convert the string to a date to check for datetime
	
	try
		datenum(value); type = 'DATETIME';
	catch
		% NOTE: there is nothing to catch
	end
	
	return;
	
end

if iscellstr(value)
	type = 'TEXT'; return;
end