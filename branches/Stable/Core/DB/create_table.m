function [str, lines] = create_table(prototype, name, force)

% create_table - create table for struct array
% --------------------------------------------
%
% [str, lines] = create_table(prototype, name, force)
%
% Input:
% ------
%  prototype - struct
%  name - table name
%  force - force create
%
% Output:
% -------
%  str - sql string
%  lines - sql lines

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set no force
%--

if nargin < 3 
	force = 0;
end

%--
% try to get name from input
%--

if (nargin < 2)
	
	name = inputname(1);
	
	if isempty(name)
		error('Unable to get table name from input name.');
	end
	
end
	
%---------------------------------
% CREATE QUERIES
%---------------------------------

%--
% flatten struct and use field names as column names
%--

prototype = flatten_struct(prototype); 

column = fieldnames(prototype);

%--
% initialize statement and consider force
%--

lines = {};

if force
	lines{end + 1} = ['DROP TABLE IF EXISTS ', name, '; '];
end

%--
% build create table statement
%--

lines{end + 1} = ['CREATE TABLE IF NOT EXISTS ', name, ' ('];

for k = 1:length(column)	
	
	%--
	% get type and constraints
	%--
	
	temp = get_type_and_constraints(column{k}, prototype.(column{k}));
	
	%--
	% build column description line
	%--
	
	lines{end + 1} = ['  ', column{k}, ' ', temp.type , ' ', temp.constraints, ','];
	
end

lines{end}(end) = []; 

lines{end + 1} = ');';

lines = strrep(lines(:), ' ,', ',');

%--
% output sql as string as well as lines
%--

str = sql_string(lines);

