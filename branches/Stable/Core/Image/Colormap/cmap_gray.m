function C = cmap_gray(n,b,c)

% cmap_gray - brightness and contrast controlled gray colormap
% ------------------------------------------------------------
%
% C = cmap_gray(n,b,c)
%
% Input:
% ------
%  n - number of colors
%  b - brightness
%  c - contrast
% 
% Output:
% -------
%  C - colormap matrix

%--
% set brightness and contrast
%--

if (nargin < 3)
	c = 0;
end

if (nargin < 2)
	b = 0.5;
end

%--
% set number of colors
%--

if (nargin < 1)
	n = 256;
end

%--
% create colormap
%--

if (c == 1)
	
	%--
	% black and white colormap
	%--
	
	n1 = round(b * n);
	n2 = n - n1;
	C = [zeros(n1,3); ones(n2,3)];
	
elseif (c == 0)
	
	%--
	% linear colormap over all range
	%--
	
	C = gray(n);
	
else
	
	%--
	% linear colormap over limited range
	%--
	
	m = round((1 - c) * n); % length of range of variation
	C = gray(m);
	
	n1 = round(b * (n - m));
	n2 = (n - m) - n1;
	
	C = [zeros(n1,3); C; ones(n2,3)];
	
end

