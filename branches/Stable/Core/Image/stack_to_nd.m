function Y = stack_to_nd(X)

% stack_to_nd - convert image stack to multiple plane image
% ---------------------------------------------------------
% 
% Y = stack_to_nd(X)
%
% Input:
% ------
%  X - image stack
% 
% Output:
% -------
%  Y - multiple plane image

%--
% check grayscale stack input
%--

t = is_stack(X);

if (~t)
	error('Input is not an image stack.');
end

if (t > 1)
	error('Only grayscale image stacks are supported.');
end

%--
% put frames into n-dimensional image
%--

[r,c] = size(X{1}); s = length(X);

Y = zeros(r,c,s);

for k = 1:s
	Y(:,:,k) = X{k};
end