function G = filt_tight(F,tol)

% filt_tight - tighten support of filter using value tolerance
% ------------------------------------------------------------
%
% G = filt_tight(F,tol)
%
% Input:
% ------
%  F - input filter
%  tol - filter value tolerance (def: 10^-4)
%
% Output:
% -------
%  G - tight support filter

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-02-16 19:58:53 -0500 (Wed, 16 Feb 2005) $
% $Revision: 539 $
%--------------------------------

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set tolerance
%--

if ((nargin < 2) || isempty(tol))
	tol = 10^-6;
end

%--------------------------------------------
% COMPUTE TIGHT SUPPORT FILTER
%--------------------------------------------

%--
% compute mask of values exceeding tolerance
%--

Z = (abs(F) >= tol);

%--
% no entries satisfy tolerance
%--

if (all(Z(:) == 0))
	
	G = [];

%--
% all entries satisfy tolerance
%--

elseif (all(Z(:) == 1))
	
	G = F;

%--
% tighten to symmetric support
%--

else

	%--
	% get size and indexing for entries that satisfy tolerance
	%--
	
	[m,n] = size(Z);
	
	[i,j,v] = find(Z);

	%--
	% get min and max rows
	%--
	
	bi = fast_min_max(i);
	pi = min(bi(1),m - bi(2) + 1);
	bi = [pi, m - pi + 1];
	
	%--
	% get min and max columns
	%--
	
	bj = fast_min_max(j);
	pj = min(bj(1),n - bj(2) + 1);
	bj = [pj, n - pj + 1];

	%--
	% tighten filter support
	%--
	
	G = F(bi(1):bi(2),bj(1):bj(2));
	
end