function [i,j,x] = get_image_pixels(h,n)

% get_image_pixels - get pixel data
% ---------------------------------
%
% [i,j,x] = get_image_pixels(X,n)
%         = get_image_pixels(h,n)
%
% Input:
% ------
%  X - image
%  h - handle to parent figure or axes (def: gcf)
%  n - number of pixels to select
%
% Output:
% ------
%  i,j - row and column indices
%  x - pixel values one pixel per row

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:53-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set number of pixels
%--

if (nargin < 2)
	n = Inf;
end

%--
% set parent handle
%--

if (nargin < 1)
	h = gcf;
end

%--
% handle input
%--

if (ishandle(h))

	% get indices
	
	[j,i] = ginput(n);
	
	i = round(i);
	j = round(j);
	
	% get values
	
	if (nargout > 2)
	
		X = get_image_data(h);
		
		switch(ndims(X))
			case (2)
				x = X(sub2ind(size(X),i,j));
			case(3)
				x = zeros(length(i),3);
				for k = 1:length(i)
					x(k,:) = vec_col(X(i(k),j(k),:))';
				end
		end
		
	end
	
%--
% image input
%--

else
	
	X = h;
	
	% display image
	
	g = fig;	image_view(X);
	
	% get indices
	
	[j,i] = ginput(n);
	
	i = round(i);
	j = round(j);
	
	% get values
	
	if (nargout > 2)
	
		switch(ndims(X))
			case (2)
				x = X(sub2ind(size(X),i,j));
			case(3)
				x = zeros(length(i),3);
				for k = 1:length(i)
					x(k,:) = vec_col(X(i(k),j(k),:))';
				end
		end
		
	end
	
	% close display
	
	close(g);
	
end
