function pal = find_waitbar(name)

% find_waitbar - find named waitbar 
% ---------------------------------
%
% pal = find_waitbar(name)
%
% Input:
% ------
% name - waitbar name
%
% Output:
% -------
%  pal - waitbar handle

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

%--
% get all waitbars
%--

pal = get_xbat_figs('type', 'waitbar');

%--
% return quickly if there are no waitbars
%--

if isempty(pal)
	return;
end

%--
% find named waitbar
%--

for ix = length(pal):-1:1
	
	if ~strfind(get(pal(ix), 'tag'), name)
		pal(ix) = [];
	end

end