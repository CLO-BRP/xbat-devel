function flag = goto_event(par, str, data)

% goto_event - move display to event and select
% ---------------------------------------------
%
% flag = goto_event(par, str, data)
%
% Input:
% ------
%  par - parent browser figure handle
%  str - event string
%  data - browser state

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 7019 $
% $Date: 2006-10-13 16:38:44 -0400 (Fri, 13 Oct 2006) $
%--------------------------------

% TODO: clean this up and make it pretty

%--
% set success flag
%--

flag = 1;

%--
% get state of browser if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%--------------------------------------------
% parse event string
%--------------------------------------------

[event, name, m, ix] = get_str_event(par, str, data);

%--
% return is event is not available
%--

if isempty(event)
	flag = 0; return;
end

%--------------------------------------------
% move to time position and select event
%--------------------------------------------

% NOTE: this has problems for page duration smaller than event duration

%--
% get parent time slider
%--

slider = get_time_slider(par);

%--
% get relevant variables for time computation
%--

event = data.browser.log(m).event(ix); 

sound = data.browser.sound; 

page = data.browser.page;

%--
% check that event is fully visible in page
%--

time = map_time(sound, 'slider', 'record', event.time);

% TODO: consider the option of always centering the event on goto

if any(time < slider.value) || any(time > (slider.value + page.duration))
	
	%--
	% time required to put event at center of page
	%--
	
	% NOTE: we need to consider the ends of the sound
	
	t = (sum(time) - page.duration) / 2;
	
	if (t > slider.max)
		t = slider.max;
	end
	
	if (t < 0)
		t = 0;
	end
	
	%--
	% move to required time
	%--
	
	set_time_slider(par, 'value', t); 
	
end

%--
% select event
%--

% NOTE: this pause is sensitive to the scrolling daemon period

pause(0.15);

% TODO: an error ocurrs when event channel is not currently displayed

double_click('off');

if data.browser.log(m).visible
	
	event_bdfun(par, m, ix);
	
else
	
	% NOTE: clearing id makes the system believe this is just a selection
	
	event.id = []; figure(par); browser_bdfun(event);
	
end

double_click('on');

