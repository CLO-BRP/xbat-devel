//----------------------------------------------
// INCLUDE FILES
//----------------------------------------------

#include "mex.h"

//----------------------------------------------
// FUNCTIONS
//----------------------------------------------

void fast_quartiles (double *q, double *x, int N);

void fast_quartiles (double *q, double *x, int N)

{

	register int k;
	
	register double q1, q2, q3;
	
	for (k = 0; k < N - 1; k = k + 2) {
		        
		x1 = *(x + k);
		x2 = *(x + k + 1);
		
		if (x1 > x2) {
			xx = x1;
			x1 = x2;
			x2 = xx;
		}
		
		if (x1 < L) {
			L = x1;
		}
		
		if (x2 > U) {
			U = x2;
		} 
      
	}
	
	if (N % 2) {
	
		xx = *(x + N - 1);
						
		if (xx < L) {
			L = xx;
		} else if (xx > U) {
			U = xx;
		}
		
	}
	
	*b = L;
	*(b + 1) = U;
	
}   

//----------------------------------------------
// MEX FUNCTION
//----------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
		
	double *X; int N;
	
	double *b;

		//--
		// INPUT
		//--
		
		// input cumulative sum vector

	  	X = mxGetPr(prhs[0]);
	  	N = mxGetM(prhs[0]) * mxGetN(prhs[0]);
				
  		//--
  		// OUTPUT
  		//--
    	
    	// extreme values
  		
  		q = mxGetPr(plhs[0] = mxCreateDoubleMatrix(1, 3, mxREAL));
  		
  		//--
  		// COMPUTATION
  		//--
  		
  		fast_quartiles(q,X,N);

}
