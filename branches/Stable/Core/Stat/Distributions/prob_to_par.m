function [par,type] = prob_to_par(str)

% prob_to_par - get parameter names for probability distribution
% --------------------------------------------------------------
%
% [par,type] = prob_to_par(str)
%
% Input:
% ------
%  str - distribution name
%
% Output:
% -------
%  par - parameter names
%  type - parameter types

%--
% create distribution parameter table
%--

T = { ...
	'Gaussian',{'Mu','Sigma'},{'Location','Scale'}; ...
	'Generalized Gaussian',{'Mu','Sigma','Alpha'},{'Location','Scale','Shape'}; ...
	'Log-Gaussian',{'Mu','Sigma'},{'Location','Scale'}; ...
	'Log-Generalized Gaussian',{'Mu','Sigma'},{'Location','Scale'}; ...
	'Exponential', {'Mu'},{'Scale'}; ...
	'Gamma', {'A','B'},{'Shape','Scale'}; ...
	'Power Law',{'Mu','Sigma','Alpha'},{'Location','Scale','Shape'}; ...
	'Rayleigh', {'B'},{'Scale'}; ...
	'Weibull', {'A','B'},{'Scale','Shape'}; ...
	'Beta',{'A','B'},{'Shape','Shape'}; ...		
};
		
%--
% look up parameter names and types if needed
%--

ix = find(strcmp(str,T(:,1)));

if (~isempty(ix))
	
	par = T{ix,2};	
	
	% pad the parameter names to the same length
	
% 	tmp = (strvcat(par));
% 	for k = 1:length(par)
% 		par{k} = tmp(k,:);
% 	end
	
	if (nargout > 1)
		type = T{ix,3};
	end
	
end