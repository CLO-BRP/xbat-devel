function p = power_fit(X)

% power_fit - one sided power law fit
% -----------------------------------
% 
% p = power_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  p - generalized gaussian parameters [mean, deviation, shape]

%--
% put data in column and zero center
%--

x = X(:);

m = sum(x)/length(x);
x = x - m;

%--
% keep positive elements and duplicate reflect them
%--

ix = find(x < 0);
x(ix) = [];
x = [x; -x];

%--
% fit using mex and update mean parameter
%--

p = zeros(1,3);
[p(3),p(2),p(1)] = gauss_fit_(x);

p(1) = p(1) + m;
