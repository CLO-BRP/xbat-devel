function p = gauss_fit(X)

% gauss_fit - generalized gaussian fit
% ------------------------------------
% 
% p = gauss_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  p - generalized gaussian parameters [mean, deviation, shape]

%--
% put data in column and zero center
%--

x = X(:);

m = sum(x)/length(x);
x = x - m;

%--
% fit using mex and update mean parameter
%--

p = zeros(1,3);
[p(3),p(2),p(1)] = gauss_fit_(x);

p(1) = p(1) + m;
