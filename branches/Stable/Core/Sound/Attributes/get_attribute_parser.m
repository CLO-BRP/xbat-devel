function parser = get_attribute_parser(attribute)

% get_attribute_parser - get parsing function for a type of attribute
% -------------------------------------------------------------------
%
% parser = get_attribute_parser(attribute)
%
% Input:
% ------
%  attribute - the attribute name
%
% Output:
% -------
%  parser - function handle to the parser

parser = [];

[root, ignore] = fileparts(mfilename('fullpath'));

file = [root, filesep, 'Parsers', filesep, attribute, '.m'];

if ~exist(file, 'file')
	return;
end

parser = str2func(attribute);