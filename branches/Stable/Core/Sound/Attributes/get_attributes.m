function attr = get_attributes(in, flag)

% get_attributes - get sound attributes from sound files
% ------------------------------------------------------
%
% attr = get_attributes(in, flag)
%
% Input:
% ------
%  in - parent directory
%  flag - attributes search flag
%
% Output:
% -------
%  attr - attributes struct

%------------------------------------------
% SETUP
%------------------------------------------

% NOTE: Microsoft Excel format (Comma-Separated Values)

file_ext = '.csv'; 

[ignore, name] = fileparts(in);

attr = attributes_create;

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% set default attribute search flag and consider search
%--

if nargin < 2 || isempty(flag)
	flag = 1;
end

%------------------------------------------
% LOAD AVAILABLE ATTRIBUTES
%------------------------------------------

% NOTE: return empty attributes if no search

if ~flag
	return; 
end

%--
% check for attributes directory for this sound
%--

root = get_attributes_root(in);

if ~exist_dir(root)
	return;
end

%--
% load attributes
%--

fields = get_attribute_fields;

for k = 1:length(fields)
	
	%--
	% try to find a file for this attribute
	%--
	
	file = [root, filesep, fields{k}, file_ext];
	
	if ~exist(file, 'file')
		continue;
	end
	
	%--
	% try to find a parser for this attribute
	%--
	
	fun = get_attribute_parser(fields{k});
	
	if isempty(fun)
		continue;
	end
	
	%--
	% try running parser.
	%--
	
	try
		attr.(fields{k}) = fun(file);
	catch
		disp(['Error loading ', disp_link(file, fields{k}), ' for ''', name, '''.']);
	end
		
end

	
%------------------------------------------
% ATTRIBUTES_CREATE
%------------------------------------------

function attr = attributes_create

fields = get_attribute_fields; 

values = cell(1,length(fields));

attr = cell2struct(values,fields,2);
