function time = date_time(file)

% date_time - get starting date and time for sound
% ------------------------------------------------
%
% table = date_time(file)
%
% Inputs:
% -------
% file - file with path
%
% Outputs:
% --------
% date - matlab-format date vector

lines = file_readlines(file);

if isempty(lines)
	return
end

%--
% get number of columns from first line
%--

header = lines{1};

t = {};

while (1)
	
	[t{end + 1} header] = strtok(header, ',');
	
	if isempty(header)
		break;
	end
	
end

% TODO: get units from second line

%--
% create date number by reading lines
%--

cols = length(t); 

if cols > 1 
	time = []; return;
end

N = numel(datevec(now));

time = strread(lines{3}, '', N, 'delimiter', ',');

try 
	time = datenum(time);
catch
	str = ['File ''date_time.csv'' does not seem to be valid.']; warn_dialog(str, 'Configure');
	time = []; return;
end



