function flag = file_speed(mode,h,in)

% file_speed - handle speed of sound data file
% --------------------------------------------
%
%     t = file_speed('read',f)
%
%  flag = file_speed('load',h,f)
%       = file_speed('write',h,s)
%
% Input:
% ------
%  h - handle of figure modified
%  f - name of file to read
%  s - speed of sound to write to file
%
% Output:
% -------
%  flag - execution success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set handle
%--

if ((nargin < 2) | isempty(h))
	h = gcf;
end

%--
% get userdata
%--

if (~strcmp(mode,'read'))
	data = get(h,'userdata');
end

%--
% read or write depending on mode
%--

switch (mode)
	
%--
% read speed of sound from file
%--

case ('read')
	
	%--
	% rename input
	%--
	
	in = h;
	
	%--
	% try to read values from file
	%--
	
	try
		
		s = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Speed of Sound Data','modal' ...
		);
		waitfor(tmp);
		
		flag = [];
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	if ((length(s) > 1) | (s < 0))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper speed of sound information.'], ...
			' XBAT Error  -  Speed of Sound Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% output speed of sound
	%--
	
	flag = s;
	
	return;
	
%--
% load speed of sound into browser from file
%--

case ('load')
	
	%--
	% get file if needed
	%--
	
	if ((nargin < 3) | isempty(in))

		%--
		% go to figure sound directory
		%--
		
		pi = pwd;
		
		cd(data.browser.sound.path);
		
		%--
		% get file to read using dialog
		%--
		
		[in,p] = uigetfile( ...
			{'*.txt','Text Files (*.txt)'; '*.*','All Files (*.*)'}, ...
			'Select Speed of Sound Data File: ' ...
		);  
		
		%--
		% return if file dialog was cancelled
		%--
		
		if (~in)
			
			flag = 0;
			cd(pi);	
			return;
			
		else
			
			in = [p,in];
			cd(pi);
			
		end

	end
	
	%--
	% try to read values from file
	%--
	
	try
		
		s = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Speed of Sound Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	if ((length(s) > 1) | (s < 0))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper speed of sound information.'], ...
			' XBAT Error  -  Speed of Sound Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% update speed of sound in browser
	%--
	
	data.browser.sound.speed = s;
	
	set(h,'userdata',data);
	
	%--
	% update speed of sound data file
	%--
	
	flag = file_speed('write',h,s);
	
	if (~flag)
		
		tmp = error_dialog('Error updating speed of sound data file.', ...
			'XBAT Error  -  Speed of Sound Data','modal');
		waitfor(tmp);
		
		return;
		
	end
	
%--
% write speed of sound to file
%--

case ('write')
		
	%--
	% convert date number to date vector
	%--
	
	if ((nargin < 3) | isempty(in))
		s = data.browser.sound.speed;
	else
		s = in;
	end
	
	%--
	% get template string to write to file and add sound data
	%--
	
	str = file_to_str('file_speed.txt');
	
	str = strrep(str,'$PATH',strrep(data.browser.sound.path,'\','\\'));
	
	if (~strcmp(data.browser.sound.type,'File'))
		str = strrep(str, ...
			'$FILE',[data.browser.sound.file{1} ' , ... , ' data.browser.sound.file{end}]);
	else
		str = strrep(str,'$FILE',data.browser.sound.file);
	end	
	
	%--
	% add speed data to string
	%--

	str = [str, num2str(s)];
	
	%--
	% write file
	%--

	if (~strcmp(data.browser.sound.type,'File'))
		pre = data.browser.sound.path;
		ix = findstr(pre,filesep);
		pre = pre(ix(end - 1):(end - 1));
	else
		pre = file_ext(data.browser.sound.file);
	end
	
	flag = str_to_file(str,[data.browser.sound.path pre '_Speed.txt']);
	
	%--
	% update speed of sound related menus
	%--
	
	mg = data.browser.sound_menu.data(9);
	
	set(mg,'label','Speed of Sound:  (Available)');
	
	delete(get(mg,'children'));
		
	L = { ...
		['Speed:  ' num2str(s) ' m/sec'], ...
		'Edit Speed of Sound ...', ...
		'Import Speed of Sound ...' ...
	};

	S = bin2str([0 1 0]);
	
	tmp = menu_group(mg,'browser_sound_menu',L,S);
	
	set(tmp(1:end - 2),'callback','');
	
end

%--
% set flag
%--

flag = 1;


	
	
	
	