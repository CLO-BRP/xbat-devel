function sound = sound_attribute_update(sound)

% sound_attribute_update - update sound attributes
% ------------------------------------------------
%
% sound = sound_attribute_update(sound, mode)
%
% Input:
% ------
%  sound - sound
%  mode - mode
%
% Output:
% -------
%  sound - updated sound

%--
% get attributes from files
%--

attr = get_attributes(sound.path);

%--
% copy attributes to sound
%--

% TODO: consider renaming to make this whole process dynamic

% REALTIME

sound.realtime = attr.date_time;

% SENSOR GEOMETRY

sound.geometry = attr.sensor_geometry;

% SOUND SPEED

sound.speed = attr.sound_speed;

% TIME STAMPS

if isempty(sound.time_stamp)
	
	%--
	% fill in entire time stamps field if it doesn't exist.
	%--
	
	sound.time_stamp = attr.time_stamps;
	
else
	
	%--
	% only update the table. Enable and collapse are config options.
	%--
	
	if isfield(attr.time_stamps, 'table')
		sound.time_stamp.table = attr.time_stamps.table;
	end
	
end

% SENSOR CALIBRATION

if ~isfield(attr, 'sensor_calibration')
	sound.calibration = [];
else
	sound.calibration = attr.sensor_calibration;
end



