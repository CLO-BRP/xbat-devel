function flag = file_calibration(mode,h,in)

% file_calibration - handle calibration data file
% -----------------------------------------------
% 
%     t = file_calibration('read',f)
%
%  flag = file_calibration('load',h,f)
%       = file_calibration('write',h,t)
%
% Input:
% ------
%  h - handle of figure modified
%  f - name of file to read
%  t - calibration data to write to file
%
% Output:
% -------
%  flag - execution success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set handle
%--

if ((nargin < 2) | isempty(h))
	h = gcf;
end

%--
% get userdata
%--

if (~strcmp(mode,'read'))
	data = get(h,'userdata');
end

%--
% read or write depending on mode
%--

switch (mode)
	
%--
% read calibration data from file
%--

case ('read')
	
	%--
	% rename input
	%--
	
	in = h;
	
	%--
	% try to read values from file
	%--
	
	try
		
		c = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Calibration Data','modal' ...
		);
		waitfor(tmp);
		
		flag = [];
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
% 	nch = data.browser.sound.channels;
% 	
% 	if (any(size(c) ~= [nch,2]) | any(c(:,1) < 0) | any(round(c(:,1)) ~= c(:,1)))
	
	if ((size(c,2) ~= 2) | any(c(:,1) < 0) | any(round(c(:,1)) ~= c(:,1)))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper calibration information.'], ...
			' XBAT Error  -  Calibration Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% output calibration data
	%--
	
	flag = c(:,2);
	
	return;
	
%--
% read calibration data into browser from file
%--

case ('load')
	
	%--
	% get file if needed
	%--
	
	if ((nargin < 3) | isempty(in))

		%--
		% go to figure sound directory
		%--
		
		pi = pwd;
		
		cd(data.browser.sound.path);
		
		%--
		% get file using dialog
		%--
		
		[in,p] = uigetfile( ...
			{'*.txt','Text Files (*.txt)'; '*.*','All Files (*.*)'}, ...
			'Select Calibration Data File: ' ...
		);  
		
		%--
		% return if file dialog was cancelled
		%--
		
		if (~in)
			
			flag = 0;
			cd(pi);	
			return;
			
		else
			
			in = [p,in];
			cd(pi);
			
		end

	end
	
	%--
	% try to read values from file
	%--
	
	try
		
		c = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Calibration Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	nch = data.browser.sound.channels;
	
	if (any(size(c) ~= [nch,2]) | any(c(:,1) < 0) | any(round(c(:,1)) ~= c(:,1)))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper calibration information.'], ...
			' XBAT Error  -  Calibration Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% update calibration in browser
	%--
	
	data.browser.sound.calibration = c(:,2);
	
	set(h,'userdata',data);
	
	%--
	% update calibration data file
	%--

	flag = file_calibration('write',h,c(:,2));
	
	if (~flag)
		
		tmp = error_dialog('Error updating calibration data file.', ...
			'XBAT Error  -  Calibration Data','modal');
		waitfor(tmp);
		
		return;
		
	end
		
%--
% write calibration to file
%--

case ('write')
		
	%--
	% set calibration values if needed
	%--
	
	if ((nargin < 3) | isempty(in))
		c = data.browser.sound.calibration;
	else
		c = in;
	end
	
	%--
	% get template string to write to file and add sound data
	%--
	
	str = file_to_str('file_calibration.txt');
	
	str = strrep(str,'$PATH',strrep(data.browser.sound.path,'\','\\'));
	
	if (~strcmp(data.browser.sound.type,'File'))
		str = strrep(str, ...
			'$FILE',[data.browser.sound.file{1} ' , ... , ' data.browser.sound.file{end}]);
	else
		str = strrep(str,'$FILE',data.browser.sound.file);
	end	
	
	%--
	% add calibration data to string
	%--
	
	nch = data.browser.sound.channels;
	
	for k = 1:nch
		str = [str, int2str(k) ', ' num2str(c(k)) '\n'];
	end
	
	%--
	% write file
	%--

	if (~strcmp(data.browser.sound.type,'File'))
		pre = data.browser.sound.path;
		ix = findstr(pre,filesep);
		pre = pre(ix(end - 1):(end - 1));
	else
		pre = file_ext(data.browser.sound.file);
	end
	
	flag = str_to_file(str,[data.browser.sound.path pre '_Calibration.txt']);
	
	%--
	% update calibration related menus
	%--
	
	mg = data.browser.sound_menu.data(7);
	
	set(mg,'label','Calibration:  (Available)');
	
	delete(get(mg,'children'));
	
	L = cell(0);
	for k = 1:nch
		L{k} = ['Channel ' int2str(k) ':  ' num2str(c(k)) ' dB'];
	end
	
	L{nch + 1} = 'Edit Calibration ...'; 
	L{nch + 2} = 'Import Calibration ...';
	
	n = length(L);

	S = bin2str(zeros(1,n));
	S{nch + 1} = 'on';
	
	tmp = menu_group(mg,'browser_sound_menu',L,S);
	
	set(tmp(1:end - 2),'callback','');
	
end

%--
% set flag
%--

flag = 1;
	
	