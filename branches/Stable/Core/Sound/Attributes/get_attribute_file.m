function file = get_attribute_file(sound, attribute_type)

% get_attribute_file - get file for a sound and attribute type
% ------------------------------------------------------------
%
% file = get_attribute_file(sound, attribute_type)
%
% Input:
% ------
%  sound - sound
%  attribute_type - attribute type
%
% Output:
% -------
%  file - file

ext = '.csv';

root = get_attributes_root(sound);

file = fullfile(root, [attribute_type, ext]);
