function player_pause

%--
% get current player
%--

player = get_current_player;

% NOTE: return if we can't find it or if it's not playing

if isempty(player) || ~isplaying(player)
	return;
end

%--
% pause player
%--

pause(player);
