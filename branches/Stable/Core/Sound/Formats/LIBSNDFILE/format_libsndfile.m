function format = format_libsndfile

% format_libsndfile - create format structure
% -------------------------------------------
%
% format = format_libsndfile
%
% Output:
% -------
%  format - generic format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% create format structure
%--

format = format_create;

%--
% fill function and info fields of format structure (set)
%--

% NOTE: this is a base format, no extensions are associated to it

format.home = 'http://www.mega-nerd.com/libsndfile/';

format.info = @info_libsndfile;

format.read = @read_libsndfile;

format.write = @write_libsndfile;

format.seek = 1;

format.compression = 0;

format.config = @sound_config;