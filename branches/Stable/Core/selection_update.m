function selection_update(h, data)

% selection_update - update selection related display
% ---------------------------------------------------
%
% selection_update(h, data)
%
% Input:
% ------
%  h - browser figure handle
%  data - browser figure userdata

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 819 $
% $Date: 2005-03-27 15:06:08 -0500 (Sun, 27 Mar 2005) $
%--------------------------------

%-------------------------------------------
% HANDLE INPUT
%-------------------------------------------

%--
% get userdata if needed
%--

if (nargin < 2)
	data = get(h, 'userdata');
end

%-------------------------------------------
% DISCARD SELECTION DISPLAY
%-------------------------------------------

if ~data.browser.selection.zoom
	
	%--
	% delete selection figure if it exists 
	%--
	
	sel = get_xbat_figs( ...
		'parent', h, 'type', 'selection' ...
	);

	if ~isempty(sel)
		delete(sel);
	end
	
% 	return;
	
end

%-------------------------------------------
% UPDATE SELECTION DISPLAY
%-------------------------------------------

% TODO: decide to update selection zoom display when there is no selection or delete?

% NOTE: deletion leads to recreation when navigating events, update!

% NOTE: we rely on the selection handles being an indicator of selection

if ~isempty(data.browser.selection.handle)

	double_click('off');
	
	%--
	% select new or logged selection
	%--

	% NOTE: the low level function 'selection_display' is called by 'event_bdfun' and 'browser_bdfun'
	
	m = data.browser.selection.log;

	if ~isempty(m)

		id = data.browser.selection.event.id;
		
		% NOTE: we need to find the index of the event in the log
		
		ix = find(struct_field(data.browser.log(m).event,'id') == id);

		event_bdfun(h, m, ix);

	else

		% NOTE: setting current figure should not be required

		set(0, 'currentfigure', h);

		browser_bdfun(data.browser.selection.event);

	end
	
	double_click('on'); 

else
	
	% NOTE: if we add a close request function to this figure change to 'close'
	
	sel = get_xbat_figs( ...
		'parent', h, 'type', 'selection' ...
	);

	if ~isempty(sel)
		set(sel, ...
			'color', get(h, 'color'), ...
			'colormap', get(h, 'colormap') ...
		);
	end
	
end

