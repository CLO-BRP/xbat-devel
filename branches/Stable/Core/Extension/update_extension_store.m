function out = update_extension_store(in, types)

% update_extension_store - update browser extension store for types
% -----------------------------------------------------------------
%
%  par = update_extension_store(par, types)
%
% data = update_extension_store(data, types)
%
% Input:
% ------
%  par - handle
%  data - state
%  types - update types
%
% Output:
% -------
%  par - updated handle
%  data - updated state

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set all types default
%--

if nargin < 2
	types = get_extension_types;
end 

if ischar(types)
	types = {types};
end

%--
% handle different input to get browser state
%--

% NOTE: we handle browser handle, or state input

if is_browser(in)
	data = get_browser(in);
else
	data = in;
end

%--
% get context
%--

context.sound = data.browser.sound;

context.user = get_active_user;

context.library = get_active_library;

%--
% update store for specific types
%--

for k = 1:numel(types)
	
	%--
	% normalize type
	%--
	
	type = type_norm(types{k});
	
	%--
	% get and initialize extensions of type
	%--

	exts = get_extensions(type);

	if ~isempty(exts)
		exts = extension_initialize(exts, context);
	end
	
	%--
	% update store
	%--
	
	% TODO: consider updating store, not recreating
	
	% NOTE: this destroys state for all extensions of type
	
	data.browser.(type).ext = exts;

	data.browser.(type).active = '';

end

%--
% update browser state
%--

% NOTE: update state for browser input, return updated state for state input

if is_browser(in)
	set(in, 'userdata', data); out = in;
else
	out = data;
end
