function out = action_menu(par, type)

% action_menu - attach actions to actions menu
% --------------------------------------------
%
% h = action_menu(par, type)
%
% Input:
% ------
%  par - parent menu for action menus
%  type - action type
%
% Output:
% -------
%  out - menu handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

if ~is_action_type(type)
	error('Unrecognized action type.');
end

%--------------------------------------
% CREATE MENU
%--------------------------------------

%--
% prepare parent by clearing children and callback
%--

delete(allchild(par)); set(par, 'callback', []);

%--
% get actions
%--

ext = get_extensions([type, '_actions']);

% NOTE: return early creating informative menu

if isempty(ext)
	
	uimenu(par, ...
		'label', '(No Actions Found)', ...
		'enable', 'off' ...
	);

	return;

end

%--
% create action menus
%--

% TODO: create categorical action menus, perhaps further organization

for k = 1:length(ext)
	
	% NOTE: add menus to control framework, tag information helps for now
	
	uimenu(par, ...
		'label', ext(k).name, ...
		'tag', ext(k).name, ...
		'callback', {@action_dispatch, type} ...
	);

end