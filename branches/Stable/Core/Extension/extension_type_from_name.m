function type = extension_type_from_name(name)

% extension_type_from_name - get extension type from name
% -------------------------------------------------------
%
% type = extension_type_from_name(name)
%
% Input:
% ------
%  name - extension name
%
% Output:
% -------
%  type - extension type

type = '';

%--
% get extension names and types
%--

ext = get_extensions;

names = {ext.name}; types = {ext.subtype};

%--
% find extensions by name
%--

ix = find(strcmp(name, names));

if isempty(ix)
	return;
end

type = types(ix);

if (length(ix) < 2)
	type = type{1};
end
	