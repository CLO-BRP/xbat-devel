function out = attribute_menu(par)

% attribute_menu - attach attribute menu to parent
% ------------------------------------------------
%
% h = attribute_menu(par)
%
% Input:
% ------
%  par - parent menu
%
% Output:
% -------
%  out - menu handles

%--------------------------------------
% CREATE MENU
%--------------------------------------

%--
% prepare parent by clearing children and callback
%--

delete(allchild(par)); set(par, 'callback', []);

%--
% get actions
%--

ext = get_extensions('sound_attributes');

% NOTE: return early creating informative menu

if isempty(ext)
	
	uimenu(par, ...
		'label', '(No Attributes Found)', ...
		'enable', 'off' ...
	);

	return;

end

%--
% create attribute menus
%--

for k = 1:length(ext)
	
	uimenu(par, ...
		'label', ext(k).name, ...
		'tag', ext(k).name, ...
		'callback', {@attribute_dispatch, ext(k)} ...
	);

end