function out = attribute_dialog(ext, context)

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'string', 'Edit', ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'min', 1 ...
);

ext_control = empty(control_create);

try
	ext_control = ext.fun.control.create;
catch
	extension_warning(ext, 'Control creation failed.', lasterror);
end

for k = 1:length(ext_control)
	control(end + 1) = ext_control(k);
end

%--
% configure dialog
%--

opt = dialog_group;

% NOTE: add color according to parent context

header_type = 'root';

opt.header_color = get_extension_color(header_type);

opt.ext = ext;

%--
% present dialog
%--

% TODO: route callback and pass context in the router

out = dialog_group(ext.name, ...
	control, opt, {@callback_router, ext.fun.control.callback, context} ...
);


%----------------------------------------
% CALLBACK_ROUTER
%----------------------------------------

function callback_router(obj, eventdata, fun, context)

%--
% get callback context
%--

callback = get_callback_context(obj, 'pack');

%--
% call extension specific callback
%--

if ~isempty(fun)
	fun(callback, context);
end
