function regenerate_main(ext)

% regenerate_main - generate main extension function
% --------------------------------------------------
%
% regenerate_main(ext)
%
% Input:
% ------
%  ext - extension to regenerate main for

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1161 $
% $Date: 2005-07-05 16:25:08 -0400 (Tue, 05 Jul 2005) $
%--------------------------------

%-------------------------
% SETUP
%-------------------------

%--
% get main name and file
%--

info = functions(ext.fun.main); file = info.file; main = info.function;

%--
% get parent and parent main if there is one
%--

parent = ext.parent;

if ~isempty(parent)
	parent = parent.main(); info = functions(parent.fun.main); parent_main = info.function;
end

%-------------------------
% WRITE MAIN
%-------------------------

%--
% open file
%--

fid = fopen(file, 'w');

%--
% write declaration
%--

if isempty(parent)
		
	fprintf(fid,'%s\n\n%s\n\n', ...
		['function ext = ', main], ...
		'ext = extension_create;' ...
	);

else
	
	fprintf(fid,'%s\n\n%s\n\n', ...
		['function ext = ', main], ...
		['ext = extension_inherit(', parent_main, ');'] ...
	);
	
end

%--
% write body
%--

fields = {'short_description', 'category', 'version', 'author', 'email', 'url'};

for k = 1:length(fields)

	%--
	% create field line
	%--
	
	field = fields{k};
	
	switch field

		case 'category'
			
			line = get_category_line(ext, parent);
	
		otherwise
			
			if ~isempty(parent) && isequal(ext.(field), parent.(field))
				
				% NOTE: most fields are inherited, you can edit this line to update
				
				line = ['% ext.', field, ' = '''';'];
				
				% NOTE: the short description is cleared for children
				
				if strcmp(field, 'short_description')
					line(1:2) = [];
				end
				
			else
				
				% NOTE: extensions without parent must explicitly set fields
				
				line = ['ext.', field, ' = ''', ext.(field), ''';'];
			
			end
				
	end
	
	%--
	% write line to file
	%--
	
	% NOTE: the file is double spaced
	
	fprintf(fid, '%s\n\n', line);
	
end 

%--
% close main
%--

fclose(fid);


%-------------------------
% GET_CATEGORY_LINE
%-------------------------

function line = get_category_line(ext, parent)

%--
% get explicit categories and inherit parent categories
%--

% NOTE: we refer to parent categories, only unique categories are explicit

if isempty(parent)
	line = ''; explicit = ext.category;
else
	line = 'ext.category{:}, '; explicit = setdiff(ext.category, parent.category);
end

%--
% build categories string
%--

if ischar(explicit)
	explicit = {explicit};
end

for k = 1:length(explicit)
	line = [line, '''', explicit{k}, ''', '];
end

if ~isempty(line)
	line(end - 1:end) = [];
end

%--
% finish declaration, comment if there are no explicit categories
%--

if isempty(explicit)
	pre = '% ';
else
	pre = '';
end

line = [pre, 'ext.category = {', line, '};'];


