function fun = sound_feature

% sound_feature - function handle structure
% -----------------------------------------
%
% fun = sound_feature
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.compute = {{'feature', 'context'}, {'page', 'parameter', 'context'}};

fun.parameter = param_fun;

fun.display = display_fun;