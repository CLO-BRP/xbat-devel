function fun = display_fun

% display_fun - display function structure
% ----------------------------------------
%
% fun = display_fun
%
% Output:
% -------
%  fun - display function structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: the first cell contains output names, the second cell input names

fun.layout = {{'handles'}, {'par', 'parameter', 'context'}};

fun.display = {{'handles'}, {'par', 'data', 'parameter', 'context'}};

fun.parameter = param_fun;
