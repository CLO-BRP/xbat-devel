function extension_warning(ext, str, info)

% extension_warning - produce warning for extension failure
% ---------------------------------------------------------
%
% extension_warning(ext, str, info)
%
% Input:
% ------
%  ext - extension producing message
%  str - message string
%  info - error info struct as provided by 'lasterror'

%--
% put warning together using extension
%--

type_str = upper(strrep(ext.subtype,'_',' ')); 

name_str = upper(ext.name);

str = ['WARNING: In ', type_str, ' extension ''', name_str, '''. ', str];

%--
% call nice catch
%--

nice_catch(info, str);
	