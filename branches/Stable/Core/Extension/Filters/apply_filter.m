function Y = apply_filter(X, ext, context)

% apply_filter - apply filter extension to data
% ---------------------------------------------
%
% Y = apply_filter(X, ext, context)
%
% Input:
% ------
%  X - input data
%  ext - filter extension
%  context - context
%
% Output:
% -------
%  Y - filtered data

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------