function handle = help_menu(par, ext, fun)

% help_menu - create extension help menu
% --------------------------------------
%
% help_menu(par, ext, fun)
%
% Input:
% ------
%  par - parent
%  ext - extension
%  fun - callback

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default callback
%--

if (nargin < 3) || isempty(fun)
	fun = {@help_menu_callback, ext};
end

%--------------------------
% CREATE HELP MENU
%--------------------------

%--
% create parent menu
%--

handle = uimenu(par, ...
	'enable', 'off', ...
	'label', 'Help' ...
);

%--
% get extension help contents
%--
