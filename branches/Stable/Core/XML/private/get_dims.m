function [d,nd,str] = get_dims(in)

% get_dims - get dimension information from an array
% --------------------------------------------------
%
% [d,nd,str] = get_dims(in)
%
% Input:
% ------
%  in - input variable
%
% Output:
% -------
%  d - size vector
%  nd - number of dimensions (different from 'ndims')
%  str - dimensions description string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%-----------------------------------------
% GET SIZE AND DIMENSIONS
%-----------------------------------------

%--
% get size array
%--

d = size(in);

%--
% compute dimensions based on size
%--

if (max(d) == 1)

	nd = 0; % scalar

elseif (min(d) == 1) 
	
	nd = 1; % vector

else 

	nd = length(d); % matrices and higher dimensional arrays

end

%--
% create dimensions description string
%--

% NOTE: this is used in the  array wrappers in the xml representation

str = '';

if (nd)	
	
	for k = 1:length(d)
		str = [str, int2str(d(k)), ' '];
	end

	str = str(1:(end - 1));

end