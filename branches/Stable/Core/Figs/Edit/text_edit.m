function h = text_edit(x,y,s,str,id)

% text_edit - create editable text
% --------------------------------
% 
% h = text_edit(x,y,s)
%
% Input:
% ------
%  x,y - text location
%  s - text string
%
% Output:
% -------
%  h - handle of text

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% set str
%--

if (nargin < 4)
	str = 'Initialize';
end

%--
% main switch
%--

switch (str)

	case ('Initialize')
	
		h = text(x,y,s);
		
		id = round((10^4)*rand(1));
		set(h,'UserData',id);
		
		set(h,'ButtonDownFcn',['text_edit([],[],[],''Edit'',' num2str(id) ')']);
		
	case ('Edit')
	
		set(findobj(get(gca,'Children'),'UserData',id),'Editing','on');

end


