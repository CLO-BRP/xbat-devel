function h = get_figs

% get_figs - get fig handles
% --------------------------
%
% h = get_figs
%
% Output:
% -------
%  h - handles to figs

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% NOTE: this is older code part of the 'fig' functions

%--
% find figures tagged as FIG
%--

h = findobj(0,'type','figure','tag','FIG');