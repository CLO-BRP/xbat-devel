��    J     l"  �  �D      �[  C   �[  %   �[     #\     =\  "   \\  A   \  ,   �\     �\     ]     ]     1]     O]     l]     �]     �]  )   �]      �]  ,   �]  v   '^  %   �^     �^     �^     �^     �^  
   _     _  #   +_  !   O_     q_     �_     �_  '   �_     �_  %   �_      `  .   ?`     n`     �`  3   �`     �`  "   �`  "   a     5a  &   Ra     ya      �a  &   �a  +   �a  '   �a  0   %b  (   Vb  .   b  0   �b  2   �b  %   c  '   8c  A   `c  @   �c  +   �c     d  *   'd     Rd     rd     �d  $   �d  !   �d  i   �d  8   \e     �e     �e     �e  %   �e     f     f  &   'f     Nf  4   ^f  $   �f  B   �f  ;   �f  +   7g  $   cg     �g  "   �g  8   �g     �g     h  	   h  /   h  7   Ih     �h     �h     �h  <   �h     �h  B   
i     Mi  >   ji  0   �i  9   �i  0   j  9   Ej     j  2   �j  6   �j  &   k  )   )k     Sk     fk     lk     k  &   �k     �k  '   �k     �k     l  2   6l  <   il  9   �l     �l  +   �l  &   m  $   Em  )   jm  A   �m  -   �m  '   n  4   ,n  4   an  .   �n  ,   �n  6   �n  '   )o  9   Qo  0   �o  .   �o  %   �o  5   p  ,   Gp  $   tp     �p  $   �p     �p     �p  F   q  ?   Oq  2   �q  :   �q  4   �q  )   2r     \r     nr     �r     �r     �r     �r  	   �r  
   �r     �r     �r      s     .s  ;   Ds  N   �s     �s     �s  %   t  &   .t     Ut     pt     �t     �t     �t     �t     �t  3   �t     *u     Bu  &   ^u     �u  $   �u     �u     �u  '   �u  (   v     Cv     Uv      lv     �v     �v     �v     �v  <   �v     5w     Kw     dw  .   ww  +   �w  ?   �w  %   x     8x     Lx  "   _x     �x      �x     �x  $   �x     �x     y     6y     Ny     hy     xy     �y     �y     �y     �y     �y     �y     z     z     4z  "   Nz     qz     �z     �z     �z  "   �z     �z     �z     {  R   ,{     {     �{     �{     �{     �{     |  '   4|  $   \|  $   �|  "   �|  $   �|     �|      }  2   .}  &   a}     �}     �}     �}     �}     �}     �}  "   ~  &   7~  !   ^~     �~     �~     �~     �~  7   �~       >   -  O   l  N   �  8   �  -   D�  3   r�  /   ��  <   ր  7   �      K�  !   l�  )   ��  2   ��  &   �  /   �  9   B�  W   |�  0   Ԃ  %   �      +�  <   L�  ;   ��  ;   Ń  ;   �  <   =�  A   z�  6   ��  #   �     �     6�  e   D�     ��     ��  &   ؅     ��     �  P   .�  7   �  8   ��  8   ��  c   )�  V   ��     �     �  !   �     2�     Q�     h�     ��     ��     ��  6   ш  2   �  *   ;�  -   f�  .   ��  (   É  (   �     �      5�  #   V�     z�     ��     ��     ͊     �  .   �  %   0�  $   V�     {�  ;   ��  A   ̋  2   �     A�  /   ]�  s   ��  s   �  i   u�  D   ߍ  <   $�  '   a�  #   ��  $   ��  $   Ҏ  u   ��     m�  .   ��  �   ��  $   M�  (   r�     ��     ��     ѐ     �  �  ��     ��     ƒ     ޒ     ��     �     '�     9�     N�  !   h�     ��      ��     ē     ߓ     ��  p   �  !   ��  $   ��     ϔ     �     ��  &   �  %   .�  >   T�     ��  S   ��  a   �     d�  #   ��  G   ��  U   �  -   F�  3   t�  $   ��     ͗     �  &   ��     &�     A�  $   ]�  !   ��     ��     ��  '   Ә  �  ��  ;   ��  *   Κ     ��  6   
�     A�  !   a�      ��     ��  2   ��  +   �     �  :   .�     i�  �   ��     6�     U�     f�     |�  5   ��     ȝ      �     �     %�     >�  %   V�  &   |�     ��  "   ��  &   �  %   �  %   1�     W�     t�  /   ��     ��     ޟ     ��     �  '   /�     W�  %   j�  -   ��  )   ��  !   �     
�     '�  W   E�     ��  .   ��  -   �  .   �     H�  #   g�     ��     ��     Ƣ     �  %   ��  "   �  7   @�     x�  *   ��     ��  !   ţ  9   �     !�  #   3�     W�     o�     ��     ��     ¤  B   ݤ  D    �  U   e�  W   ��  ?   �  R   S�     ��  &   ��  #   �  n   �     {�  #   ��  S   ��  /   �  '   @�     h�     �  '   ��  #   ��  !   �  7   �  =   >�  =   |�  '   ��  &   �     	�      �  "   <�  %   _�     ��     ��     ��     Ԫ  )   �  6   �  b   L�  e   ��  5   �  )   K�  %   u�  +   ��  /   Ǭ     ��  '   �     9�     X�     t�  /   ��     ��     ѭ  *   �  +   �     H�  7   Z�     ��     ��     Į  )   Ԯ     ��     �     :�     U�     [�  '   n�  h   ��  O   ��     O�  3   `�  '   ��  I   ��     �  <   &�  7   c�     ��      ��  5   б  2   �  &   9�  4   `�     ��     ��  A   ϲ  <   �  I   N�  .   ��  4   ǳ  6   ��  j   3�     ��  %   ��     ݴ  "   ��  #   �     C�  2   P�  !   ��  ,   ��      ҵ     �  ,   �     1�     L�  !   `�  !   ��     ��     ��     Ѷ     �  *   ��      %�  0   F�  ,   w�  0   ��  6   շ     �     &�     B�     _�     l�     y�     ��  #   ��  )   ��  $   �     �     �  ,   5�  B   b�     ��      ��  C   ۹     �  #   7�  3   [�  #   ��     ��     к  #   ��  #   �     5�  >   O�     ��     ��     ��     ɻ     �     �  (   �  *   @�     k�  $   ��  L   ��      ��      �  %   <�  !   b�  %   ��  9   ��     �     ��     �     3�  .   O�  ,   ~�      ��  !   ̾  '   �     �     '�  0   0�  A   a�  ?   ��  1   �  7   �  (   M�  &   v�  -   ��  (   ��  #   ��  	   �  
   "�  .   -�  !   \�  &   ~�     ��  $   ��  -   ��     �     '�  $   C�  $   h�  ,   ��  ?   ��  '   ��  "   "�     E�     W�  1   t�     ��     ��     ��  0   ��     �  ,   .�  ,   [�  8   ��     ��     ��     ��     ��     �  K   #�      o�  #   ��      ��  �   ��  (   X�  -   ��  &   ��     ��     ��      �     3�  V   E�     ��  0   ��  '   ��  %   �  3   5�  %   i�  �   ��  �   \�     ��     �     $�     =�  (   V�  ?   �     ��  $   ��  #   ��      �  $   7�     \�  )   |�  	   ��  *   ��  +   ��  ,   �     4�      H�  '   i�     ��     ��  "   ��     ��     ��  �   �  d   ��  �   �  ,   ��  .   ��  &   �  #   (�  $   L�     q�     ��  <   ��      ��  *   �     /�  #   G�     k�     ~�  3   ��  D   ��  u   �  6   z�  9   ��  C   ��    /�  �  G�  &   F�  (   m�  &   ��  /   ��     ��  &    �     '�  .   <�  (   k�     ��     ��     ��  y   ��  W   ]�  "   ��  �   ��     ��     ��     ��  8   ��     6�  D   M�     ��  $   ��  2   ��     �     "�  !   1�     S�     h�     w�     ��     ��     ��     ��  #   ��  -   ��  "   ,�  (   O�  0   x�     ��     ��     ��  2   ��     $�  G   C�  N   ��  ,   ��  ;   �  3   C�  (   w�     ��  |   ��  /   <�  !   l�  I   ��     ��  h   ��  (   _�  <   ��  &   ��  2   ��  $   �  )   D�     n�     ��  *   ��     ��  1   ��  (   %�  &   N�  "   u�  E   ��     ��     ��     ��  <   �  I   W�  :   ��  /   ��     �  E   ��  .   ��  1   �  =   3�  5   q�  "   ��  h   ��  -   3�     a�  O   �     ��  %   ��  8   �  9   L�  1   ��  *   ��  (   ��  &   �  [   3�  &   ��  $   ��     ��  4   ��     &�     A�     U�     m�     ��     ��  7   ��  +   ��  3   �  ]   P�  ^   ��  R   �  $   `�  ?   ��  $   ��  %   ��     �     -�      H�     i�  0   ��  $   ��  &   ��     ��  *   �     G�     P�  +   `�  ;   ��     ��  2   ��     �     5�  )   R�  $   |�  $   ��     ��     ��     �     �     7�     W�     w�  #   ��  ,   ��  1   ��     �     %�     :�     U�     r�     ��     ��     ��     ��  (   ��  &   �  '   ;�     c�     ��  ,   ��     ��  /   ��     �  )   /�  '   Y�     ��  %   ��  "   ��  -   ��     �  *   .�  +   Y�  !   ��      ��  #   ��     ��     �  (   +�     T�  "   f�     ��  (   ��  ?   ��  "   �  
   /�  E   :�  #   ��     ��     ��  ?   ��  �   #�  A   ��  @   �  ,   Q�     ~�  U   ��     ��     �  >   $�  3   c�  !   ��     ��     ��     ��     ��  R   �  1   i�  +   ��  +   ��  0   ��     $�     ?�  
   T�     _�     u�  !   ��  0   ��  0   ��     �     )�     E�  $   U�  #   z�     ��     ��  "   ��  !   ��      �  0   �     E�  1   b�     ��     ��  1   ��     ��  "   �  ,   2�  "   _�  *   ��     ��     ��     ��     ��  
   ��     �     �  $   /�  &   T�  '   {�  )   ��     ��  
   ��  *   ��  /    �  "   P�     s�  )   ��     ��     ��  #   ��  ,   �  ,   ?�      l�     ��  ,   ��  '   ��     ��  )        6  /   N  !   ~  3   �  (   �     �  -    0   F    w    � (   � .   � )       / $   I    n    �    � *   �    � N        O M   m *   � /   �         7    P ,   m !   �    � /   �    � Q     t  R �  � ]   � U   � �   G	 S   �	 $  P
 T   u   � f   � P   A �  � O   � 4   � n    F   � 5   � W    �   Z \   � �   I `   � 6   N �   � =   8 H   v    �    �    �        '    C    ]    r `   � Y  � E   I (   � !   � &   � %    H   ' +   p    �    � "   � "   � "    "   )    L    ` %   n    � %   � t   � (   I    r    z    �    �    �    � %   � '       C #   ]    � 4   �    � /   �     #   $    H    a (   s    � #   � #   �     /       M     g )   � 1   � /   � K    .   ` 2   � L   � .       > .   \ 2   � >   � )   �    '  +   <     h  %   �     �  #   �      �  s   ! D   �! #   �!    �! #   �! -   "    M"    e" +   x"    �" 5   �"     �" 2   # +   J# -   v# '   �#    �# !   �# 2   $    :$    C$    X$ (   g$ 3   �$    �$    �$    �$ 6   �$    '% B   F%    �% 0   �% 0   �% 3   & *   5& 3   `&    �&    �& 3   �&     �& ,   '    K' 	   a'    k'    �' !   �'     �' -   �'    	(    &( ?   C( 6   �( A   �(    �( 0   )    B) .   a) 6   �) I   �)    * $   0* 2   U* /   �* '   �* $   �* *   + '   0+ ,   X+ /   �+ )   �+    �+ 0   �+ /   &, #   V,    z, !   �,    �, '   �, U   �, H   :- 9   �- U   �- I   . 1   ].    �. #   �.    �.    �.    �.    /    /    6/    I/    _/ $   v/    �/ 5   �/ J   �/    80    X0 !   w0 #   �0    �0    �0    �0    1    #1    >1    N1 6   l1    �1    �1 /   �1 #   2 #   22    V2    j2 -   ~2    �2    �2 '   �2    3    3    ;3 ,   T3    �3 ;   �3    �3    �3    4 )   "4    L4 C   k4 &   �4    �4    �4 &   �4    &5 *   ?5    j5 !   �5    �5    �5    �5    �5    6    ,6    G6 !   b6    �6 !   �6 !   �6    �6    �6    7    %7 #   @7    d7    7    �7    �7 )   �7    �7    �7    8 M   08    ~8    �8 #   �8     �8 #   �8 #   9 )   ?9 !   i9 /   �9 #   �9 )   �9 )   	:     3: :   T: !   �:     �:    �: #   �: '   ;    9; %   X; *   ~; !   �;     �;    �;    <    <    2< 2   B<    u< K   �< \   �< \   7= ;   �= 4   �= 8   > 2   >> B   q> A   �> (   �> &   ?    F? $   b?    �? 5   �? A   �? ?   @ ,   X@ (   �@ (   �@ A   �@ A   A >   [A !   �A G   �A D   B ;   IB #   �B    �B    �B f   �B    FC '   ZC '   �C    �C    �C g   �C H   ;D K   �D K   �D g   E m   �E    �E    F '   F "   CF    fF    }F    �F    �F    �F ;   �F R   G 9   oG :   �G '   �G )   H )   6H !   `H    �H !   �H    �H !   �H    �H    I *   3I $   ^I '   �I -   �I    �I H   �I N   5J A   �J    �J ;   �J n   !K n   �K _   �K F   _L 8   �L "   �L (   M    +M    KM U   gM    �M &   �M �   �M !   �N #   �N    �N    �N    �N    O �  )O    �P    �P    Q $   7Q    \Q    uQ    �Q    �Q '   �Q    �Q &   R /   :R !   jR    �R `   �R    S !   &S    HS 	   fS    pS #   �S    �S 5   �S    �S H   T W   gT    �T    �T ?   �T E   2U /   xU %   �U $   �U    �U    V 0   &V $   WV    |V !   �V    �V    �V    �V    W   #W 2   'Y 5   ZY    �Y 6   �Y #   �Y %   �Y &   $Z    KZ /   kZ #   �Z    �Z 5   �Z "   [ �   ([    �[    �[    \    \ <   8\ !   u\ )   �\ #   �\    �\    ] -    ] *   N] (   y] ,   �] 1   �] 1   ^ 3   3^ ,   g^    �^ 3   �^ &   �^ &   _    +_ !   J_ 2   l_    �_ 5   �_ 5   �_ 4   )` 2   ^`    �` #   �` e   �` (   9a 7   ba 2   �a 3   �a &   b ,   (b )   Ub ,   b &   �b    �b -   �b '   c /   Gc    wc <   �c    �c    �c ;   �c    *d '   ;d    cd $   {d    �d    �d    �d 8   �d 2   0e 8   ce >   �e 2   �e >   f    Mf 0   gf #   �f ~   �f !   ;g    ]g G   yg 0   �g 2   �g    %h    8h /   Ph &   �h     �h I   �h G   i J   Zi 0   �i '   �i    �i !   j '   9j 2   aj !   �j    �j    �j    �j    �j ;   k _   Pk e   �k 4   l 0   Kl /   |l '   �l )   �l    �l -   m    Km    am    �m /   �m    �m -   �m -   n 3   ?n    sn /   �n    �n    �n    �n !   �n    o    9o    Ro    ko    ro :   �o Z   �o M   p    fp N   �p <   �p e   q %   rq @   �q ?   �q    r !   /r 9   Qr 6   �r $   �r 2   �r    s #   -s 8   Qs 2   �s A   �s )   �s 2   )t /   \t T   �t    �t '   �t !   &u !   Hu !   ju    �u 0   �u (   �u $   �u    v    :v ,   Pv    }v    �v (   �v $   �v    �v    w    *w    @w 3   `w $   �w :   �w 7   �w 6   ,x 9   cx    �x    �x    �x    �x    y    y    .y    Ay ,   `y )   �y 	   �y    �y )   �y N   z    Vz !   uz ]   �z    �z     { 4   1{ +   f{    �{    �{ "   �{ "   �{    | 9   /|    i|    |    �|    �|    �|    �|    �| &   }    ,} .   H} R   w}    �}    �} #   
~     .~ !   O~ 9   q~    �~    �~    �~    �~ -    -   ?    m $   � !   �    � 	   � '   � 0   � 7   F� @   ~� 6   �� 3   ��    *� $   C� $   h�    ��    ��    �� -   ʁ $   �� !   �    ?�    R� 0   m�    ��    ��    ڂ *   �� ,   !� ?   N� 8   �� )   ǃ    �    	� A   %�     g�    ��    �� 1   ��    � *    � >   +� /   j�    ��    ��    υ    �    �� S   �    e� $   ~� $   �� y   Ȇ $   B� $   g� '   ��    �� '   ͇ $   ��    � Q   *�    |� &   �� $   ��    � 4   �� "   2� �   U� �   �    ��    Њ    �    �� '   � 4   =�    r� ,   �� +   ��    �    ��    � *   =�    h� 0   u� $   �� <   ˌ    �    � &   ;� #   b�    ��    ��    �� $   Ǎ �   � n   t� �   � "   l� *   ��    ��    ُ    �    �    &� 0   9� %   j� /   ��    ��    ܐ    ��    � B   )� 3   l� g   �� >   � B   G� ;   �� �   ƒ �   *   �� $   ߕ (   � -   -�    [� ,   p�    �� '   �� !   ږ    ��    �    (� b   =� V   �� $   �� �   �    �    �    )� <   I�    �� I   �� !   � $   
� 5   /�    e�    �� %   ��    ��    ƚ    ך    �    ��    
�    #� '   :� '   b� $   �� -   �� ,   ݛ    
� $   � !   ?� '   a�    �� \   �� X   �� 1   W� 9   �� 4   Ý *   ��    #� _   >� -   ��    ̞ V   �    <� p   Y�    ʟ -   �     � /   8�     h� &   �� #   �� &   Ԡ /   �� )   +�    U� 2   t� )   �� ,   ѡ D   ��    C�    P�    c� /   �� C   �� D   �� $   ;� n   `� <   ϣ    � (   *� 2   S� /   �� &   �� j   ݤ '   H�    p� f   ��    � !    � '   "� K   J� 3   ��    ʦ    � &   � e   ,� &   ��    ��    է ;   �    '�    C�    S�    c�    s�    �� ?   �� 8   Ө A   � ^   N� f   �� X   � !   m� +   �� !   �� 0   ݪ 2   � 2   A� %   t�    �� F   �� 0   �� 1   /� %   a� 4   ��    ��    ά *   � 6   �    B� /   a�    ��    �� *   ŭ $   � $   �    :�    X� #   k� !   �� '   �� '   ٮ '   � $   )� '   N� A   v�    �� '   ̯    ��    �    *�    E�    a�    �     �� /   �� )   � &   �     ?�    `� -   x� n   �� 3   �    I� 7   h� 2   �� &   Ӳ #   �� $   � /   C�    s� ,   �� -   �� !   � #   	� )   -�    W�    s� 6   ��    ɴ &   ڴ    �    � H   ;� $   ��    �� M   �� 0   
�    ;�    R� B   n� �   �� C   E� <   �� )   Ʒ     � N   �    `� !   v� K   �� /   � *   �    ?� &   L�    s� !   �� ]   �� -   � 4   ;� 9   p� 1   ��    ܺ    ��    � $   � (   3�    \� +   x� *   ��    ϻ    �    � *   � !   ?�    a�    n� %   �� !   ��    ɼ ,   ּ    � )   �    C�    V� $   i�    ��    �� '   ��    � '   ��    &� 
   9�    D�    `�    v�    ��    ��    ��    Ǿ    ݾ #   ��    �    0� 3   =� ,   q� !   ��    �� '   ߿    �    � 1   /� !   a� .   ��    ��    �� &   �� +    �    ,� !   ?�    a� ,   t� #   �� 2   �� *   ��     #� .   D� &   s�    ��    �� $   �� >   �� $   *�    O� *   e�    ��    ��    �� *   ��    �� [   �    j� d   �� &   �� ,   �     @�    a�    |�     ��    ��    �� 6   ��    � h   � *  ��   �� S   �� W   � �   k� ^   �   t� M   y� i  �� g   1� S   �� �  �� J   �� 4   �� e   3� A   �� 2   �� g   � �   v� R   � �   V� h   �� 7   \� �   �� =   b� E   �� #   ��     
�    +�    I�    d� $   �    ��    �� B   ��    �  �  �   7  E  �  �   �  �  �  -          	   o      S    N  �   ,  Q     �       f       =   r  }    b   �  �      �   
          Z       �  �      �  �  n  Q      �                �  )  �  �       4  �     j   >  �       R   A  �  �  7               1  �        �  �           *  �   �      �           �   �  �  o      �      g  2  �  �  3   �  �  V  N  �      {  R  8  �  �  �      �     �          |     �  �   �  �      &   �  �  9       S      �  �       �  �     ]      �   �      �  E              g      x  �  �  h  �      �      1  G           |  �  �   C   �          �  G            h   �   �  5    �   �  �          �      ;      �  #  �  /    +  �      �  �  �  �  8          �  �       �  �    x   �         d      '                T      �  �  $  =  �  I   �  �  �      
  h  �          Z  �  �       ~    �         �  �      o   �  �    �  �  �   �      �      �  �      ,  X          �  �   �      |    %  �     �        -  �  �      $          =  �  (           a   �  t      )      �   g   �    �  3    �        �       �  �      �  @  �  �   �   �  >  9  e  Y  	  0  \      �   H      �                 (  �          i  A  J  �      �    "   �          N  U    �  .      Z  �  ,  �   �      i  U   �    �   S      �    3  �    ,  A  �      �      �          ^  �      �  I        1   �  (  �  �  �                q      U      �  r  �  B  z  �  k  4   �   s  �  D  �      �   w  R      m  8  �      /      *                   w   8   C  @       �       L                        �   �  �  �  �  �  �      >   �   2      P   }  �  �           ;  A   M  .  �  �  w  7  �   u      �  �      l  �           	      �  �      P     @  M   �  "  �      �  �     6  �  �  ^  G  �   �   �   V       u  _   ;   �      j  �  �       �  m  �  �  6   �      ?  3  �           �  '      �  �   �  �  G      �  Y  W   r  d  �  �          �           X                Q  �  �             �      8        �     �  �  c  /  \  �              �  �      �    _      �  �  	  <  �  q  �    e      }   ^       !      �  V  �  �  �  �          T         �  �     g  "  -   M  �                  �   F  .      �    �      c     S   �   a  5          f          �  #   L         �  K  -        �    �  *              U  )  �   �  P  �  �  �  T  {   )       [   �  
   B  �  H  �  I  �            <      �  F  �              �   �      �   �  �   k  �  �  �  @  �  �  �  ,   d   �   �       F          �  �     q   �      �           �  j      �   �  >  0   ?  �  �   �  D  �  �  �   �       .                 k   �  �          �  �       �          �     /   &          G  x     R      �    �   �  4        �   �      B         z          w      �  �  1  �   y  �  ~          2  �  �   �   �     &  �      3  �  �    [  �  �           i  �  �          �      �  �  9          �      �  Y         �    �              u   ]   X   �      p  W      }  C      �   )  �  %      n      �  >  O          �    c     9  J      <  �   *  W  l  �   �  p   �   !           �              %    %   h  �    �      e   �      �        �          A  (  �  �   �  �  �   �  B    �   
    �  �   �      �       �       Y  �    s   �  �  �  b  &      
          �        �  s  �  �        �  �  '   ;                  �  :  �   �        Z  6  O       �  �          {  �       O  C     �      u      �      �          �  �  �  v      �  �  0            I        f   E  �  �  �        �   L   5              �  !  �  F      D  �     D  �      n  �  �      a      �  �   �       �   /  "  �      `      H           �  �  $   �                �       �    �      �   ]      �         m   X  �          �  �   �    �  �      �  E  	               #            f      �  �   �   �      �   J      J  �      �                          �          t  �   J       �  �      �  5  p      �  �       �      ?  :  �  �   �   @  O  �   9  �   *   :  H    �    z  y  [  |   �    s  0  �                   �  �   �  �   ;      0  �       t     W      �          �  �  1  `  �     �      b          y  Q   b  +   �   ?   +  �  �  �              =  N   !  �  �  [  �      �  ~   H  &  v  :   �       �      �          �  6  �                    "  -      �  �  F   7  �   �  �  K  4  �  �  �               �       �      %      �   e        �   �  �   �        `  �   �  �  �   �   �  �  <  \  =      �       �      �       a         \   �  D   I  T      `       �  �  �        ~  <             ]      �  5  q  �   �    {    #  +  �  �  E   +  n     4      �  �  (      �      �       �       :  �  �  �      .   �  v      V  $  t    6  k      l   m  B    K   �       v   �  �  �       ?      L      _  �      j  �    �  �  y   �      �      �  �  �  '      �  '      �    �    �   ^          P      c  �  �     r   �   2      �  p      i   �  #  �                             �  _      �    �       K  !  �  �       �   7  �  C        o  l  x  �  z   M  �  �  �  �  2                    �      �      d      �      $          �   
------- Committed new rev %ld (loaded from original rev %ld) >>>

 
------- Committed revision %ld >>>

 
Committed revision %ld.
 
Error: no prefixes supplied.
 
Fetching external item into '%s'
 
Log message unchanged or not specified
a)bort, c)ontinue, e)dit
 
Performing status on external item at '%s'
 
Recovery completed.
 
Valid options:
      * adding path : %s ...      * deleting path : %s ...      * editing path : %s ...      * replacing path : %s ...    %ld => (dropped)
  (from %s:%ld)  - The certificate has an unknown error.
  - The certificate has expired.
  - The certificate hostname does not match.
  - The certificate is not issued by a trusted authority. Use the
   fingerprint to validate the certificate manually!
  - The certificate is not yet valid.
  arg "%s": unknown command.

 %s	(revision %ld) %s	(working copy) %s of '%s' %s request failed on '%s' %s, version %s
   compiled %s, %s

 %s:  (Not a versioned resource)

 %sProperty changes on: %s%s %s\ No newline at end of file%s %swarning: %s
 '%s'
is not the same repository as
'%s' '%s' already exists '%s' already exists and is in the way '%s' does not appear to be a URL '%s' does not appear to be a working copy path '%s' does not define '%s()' '%s' does not exist '%s' does not seem to have a URL associated with it '%s' exists and is non-empty '%s' exists and is not a directory '%s' has an unrecognized node kind '%s' has local modifications '%s' has lock token, but no lock owner '%s' has no URL '%s' has no ancestry information '%s' hook failed with error output:
%s '%s' in revision %ld is an unrelated object '%s' is a URL when it should be a path
 '%s' is a URL, but URLs cannot be commit targets '%s' is a URL, probably should be a path '%s' is a reserved name and cannot be imported '%s' is a wcprop, thus not accessible to clients '%s' is already a working copy for a different URL '%s' is already under version control '%s' is an URL when it should be a path '%s' is in the way of the resource actually under version control '%s' is marked as absent, so it cannot be scheduled for addition '%s' is neither a file nor a directory name '%s' is not a directory '%s' is not a directory in filesystem '%s' '%s' is not a file or directory '%s' is not a valid EOL value '%s' is not a working copy '%s' is not a working copy directory '%s' is not under version control '%s' is not under version control and is not part of the commit, yet its child '%s' is part of the commit '%s' is scheduled for addition within unversioned parent '%s' locked by user '%s'.
 '%s' not found '%s' not found in filesystem '%s' not versioned, and not exported
 '%s' path not found '%s' returned %d '%s' to '%s' is not a valid relocation '%s' unlocked.
 '%s' was not found in the repository at revision %ld '%s' was not present on the resource 'DAV:baseline-collection' was not present on the baseline resource 'DAV:version-name' was not present on the baseline resource 'incremental' option only valid in XML mode 'verbose' option invalid in XML mode (Binary files differ)
 (R)eject or accept (t)emporarily?  (R)eject, accept (t)emporarily or accept (p)ermanently?  (local) (no author) (no date) --This line, and those below, will be ignored-- --auto-props and --no-auto-props are mutually exclusive .merge-left.r%ld .merge-right.r%ld .working <<< Started new transaction, based on original revision %ld
 <delta-pkg> is missing ancestry A MERGE response for '%s' is not a child of the destination ('%s') A checksum mismatch occurred A conflict in the working copy obstructs the current operation A log message was given where none was necessary A path under version control is needed for this operation A problem occurred; see later errors for details A report cannot be generated because no data was supplied A repository hook failed A representation vanished or changed between reads A working copy merge source needs an explicit revision APR_APPEND not supported for adm files Aborting commit: '%s' remains in conflict Activity not found Added Adding         %s
 Adding  (bin)  %s
 All authentication providers exhausted Apache got a malformed URI Apache has no path to an SVN filesystem Apply-textdelta already active Apply-textdelta not active Are all the targets part of the same working copy? At least one property change failed; repository is unchanged At least one revision must be non-local for a pegged diff At revision %ld.
 Attempt to change immutable filesystem node Attempt to deltify '%s' against itself Attempt to pop empty WC unwind stack Attempt to remove or recreate fs root dir Attempt to set wc property '%s' on '%s' in a non-commit operation Attempt to unlock with non-empty unwind stack Attempted command in administrative dir Attempted to create a node with an illegal name '%s' Attempted to delete a node with an illegal name '%s' Attempted to get checksum of a *non*-file node Attempted to get length of a *non*-file node Attempted to get textual contents of a *non*-file node Attempted to lock an already-locked dir Attempted to make a child clone with an illegal name '%s' Attempted to open node with an illegal name '%s' Attempted to open non-existent child node '%s' Attempted to write to non-transaction Attempting restricted operation for modified resource Attempting to commit to a URL more than once Authentication error from server: %s Authentication realm: %s
 Author name cannot contain a newline Authorization failed Bad URL passed to RA layer Bad database version: compiled with %d.%d.%d, running against %d.%d.%d Bad database version: got %d.%d.%d, should be at least %d.%d.%d Bad encoding option: prop value not stored as UTF8 Bad merge; ancestor, source, and target not all in same fs Bad merge; target '%s' has id '%s', same as ancestor Bad parent pool passed to svn_make_pool() Bad property name Bad property name: '%s' Bad type indicator Baseline incorrect Berkeley DB deadlock error Berkeley DB error Bogus URL Bogus date Bogus filename Bogus mime-type Bogus revision information given Bogus revision report Can't add '%s' to a parent directory scheduled for deletion Can't add '%s' to deleted directory; try undeleting its parent directory first Can't append '%s' to '%s' Can't calculate requested date Can't calculate the request body size Can't change working directory to '%s' Can't check directory '%s' Can't check path '%s' Can't chmod '%s' Can't close directory '%s' Can't close file '%s' Can't close stream Can't connect to host '%s' Can't convert module path to UTF-8 from UCS-2: '%s' Can't copy '%s' to '%s' Can't create directory '%s' Can't create null stdout for hook '%s' Can't create pipe for hook '%s' Can't create process '%s' attributes Can't create socket Can't create tunnel Can't detect MIME type of non-file '%s' Can't determine the native path encoding Can't expand time Can't fetch FSFS mutex Can't find a temporary directory Can't find a working copy path Can't find an entry Can't find entry '%s' in '%s' Can't find entry for '%s' Can't find parent directory's entry while trying to add '%s' Can't flush file '%s' Can't flush file to disk Can't flush stream Can't get attribute information from file '%s' Can't get attribute information from stream Can't get default file perms for file at '%s' (file stat error) Can't get exclusive lock on file '%s' Can't get file name Can't get password Can't get shared lock on file '%s' Can't get working directory Can't grab FSFS repository mutex Can't hide directory '%s' Can't lock charset translation mutex Can't make directory '%s' Can't manipulate current date Can't move '%s' to '%s' Can't move source to dest Can't open '%s' Can't open directory '%s' Can't open file '%s' Can't open stderr Can't open stdin Can't open stdio file Can't open stdout Can't parse '%s' Can't read contents of link Can't read directory Can't read directory '%s' Can't read directory entry in '%s' Can't read file '%s' Can't read from connection Can't read stdin Can't read stream Can't recode error string from APR Can't remove '%s' Can't remove directory '%s' Can't remove file '%s' Can't replace '%s' in deleted directory; try undeleting its parent directory first Can't restore working directory Can't rewind directory '%s' Can't set access time of '%s' Can't set file '%s' read-only Can't set file '%s' read-write Can't set permissions on '%s' Can't set position pointer in file '%s' Can't set position pointer in stream Can't set process '%s' child errfile Can't set process '%s' child input Can't set process '%s' child outfile Can't set process '%s' cmdtype Can't set process '%s' directory Can't set proplist on *immutable* node-revision %s Can't set text contents of a directory Can't start process '%s' Can't stat '%s' Can't stat directory '%s' Can't store FSFS mutex Can't tokenize command '%s' Can't ungrab FS mutex Can't ungrab FSFS repository mutex Can't unlock charset translation mutex Can't write property hash to '%s' Can't write to '%s' Can't write to connection Can't write to file '%s' Can't write to stream Cannot calculate blame information for binary file '%s' Cannot change node kind Cannot commit both '%s' and '%s' as they refer to the same URL Cannot copy or move '%s': it is not in the repository yet; try committing first Cannot copy or move '%s': it's not in the repository yet; try committing first Cannot copy or move '%s': it's not under version control Cannot copy path '%s' into its own child '%s' Cannot copy to '%s' as it is scheduled for deletion Cannot display: file marked as a binary type.%s Cannot invoke editor to get log message when non-interactive Cannot modify transaction named '%s' in filesystem '%s' Cannot move URL '%s' into itself Cannot move path '%s' into itself Cannot negotiate authentication mechanism Cannot non-recursively commit a directory deletion Cannot replace a directory from within Cannot revert '%s': unsupported entry node kind Cannot revert '%s': unsupported node kind in working copy Cannot revert addition of current directory; please try again from the parent directory Cannot revert: '%s' is not under version control Cannot set '%s' on a directory ('%s') Cannot set '%s' on a file ('%s') Cannot specify revision for deleting versioned property '%s' Cannot specify revision for editing versioned property '%s' Cannot specify revision for setting versioned property '%s' Cannot specify revisions (except HEAD) with move operations Cannot unlock path '%s', no authenticated username available Cannot verify lock on path '%s'; no matching lock-token available Cannot verify lock on path '%s'; no username available Cannot write property hash for '%s' Capability entry is not a word Caught signal Certificate information:
 - Hostname: %s
 - Valid: from %s until %s
 - Issuer: %s
 - Fingerprint: %s
 Changed paths:
 Changed-path entry not a list Checked out external at revision %ld.
 Checked out revision %ld.
 Checkout complete.
 Checksum mismatch for '%s':
   expected checksum:  %s
   actual checksum:    %s
 Checksum mismatch for '%s'; expected '%s', actual: '%s' Checksum mismatch for '%s'; expected: '%s', actual: '%s' Checksum mismatch for '%s'; recorded: '%s', actual: '%s' Checksum mismatch for resulting fulltext
(%s):
   expected checksum:  %s
   actual checksum:    %s
 Checksum mismatch indicates corrupt text base: '%s'
   expected:  %s
     actual:  %s
 Checksum: %s
 Client certificate filename:  Client error in parsing arguments Client/server version mismatch Comment (%i line):
%s
 Comment (%i line):
%s

 Comment (%i lines):
%s
 Comment (%i lines):
%s

 Commit failed (details follow): Commit item '%s' has copy flag but an invalid revision Commit item '%s' has copy flag but no copyfrom URL Commit succeeded, but other errors follow: Commit succeeded, but post-commit hook failed Committing in directory scheduled for addition Config error: invalid boolean value '%s' Config error: invalid integer value '%s' Conflict Current Base File: %s
 Conflict Previous Base File: %s
 Conflict Previous Working File: %s
 Conflict Properties File: %s
 Connection closed unexpectedly Copied From Rev: %ld
 Copied From URL: %s
 Copied: %s (from rev %ld, %s)
 Copy from mutable tree not currently supported Copying from transactions not allowed Corrupt DB: faulty predecessor count Corrupt current file Corrupt entry in 'copies' table for '%s' in filesystem '%s' Corrupt entry in 'transactions' table for '%s' in filesystem '%s' Corrupt filesystem revision %ld in filesystem '%s' Corrupt node-id in node-rev Corrupt working copy: '%s' has no default entry Corrupt working copy: '%s' in directory '%s' (which is scheduled for addition) is not itself scheduled for addition Corrupt working copy: '%s' in directory '%s' (which is scheduled for deletion) is not itself scheduled for deletion Corrupt working copy: '%s' in directory '%s' (which is scheduled for replacement) has an invalid schedule Corrupt working copy: '%s' in directory '%s' has an invalid schedule Corrupt working copy: directory '%s' has an invalid schedule Could not create a GET request for '%s' Could not create a PUT request (%s) Could not create a request (%s '%s') Could not create top-level directory Could not fetch the Version Resource URL (needed during an import or when it is missing from the local, cached props) Could not save file Could not save the URL of the version resource Could not use external editor to fetch log message; consider setting the $SVN_EDITOR environment variable or using the --message (-m) or --file (-F) options Could not write svndiff to temp file Couldn't determine absolute path of '%s' Couldn't do property merge Couldn't find a repository Couldn't open a repository Couldn't open log Create a new directory under version control.
usage: 1. mkdir PATH...
       2. mkdir URL...

  Create version controlled directories.

  1. Each directory specified by a working copy PATH is created locally
    and scheduled for addition upon the next commit.

  2. Each directory specified by a URL is created in the repository via
    an immediate commit.

  In both cases, all the intermediate directories must already exist.
 Creating DAV sandbox dir Creating conf directory Creating db lock file Creating db logs lock file Creating hook directory Creating lock dir Creating passwd file Creating post-commit hook Creating post-revprop-change hook Creating pre-commit hook Creating pre-revprop-change hook Creating start-commit hook Creating svnserve.conf file Credential data unavailable DAV request failed; it's possible that the repository's pre-revprop-change hook either failed or is non-existent Data cannot be safely XML-escaped Default entry has no revision number Default entry is missing URL Deleted Deleting       %s
 Delta does not contain enough new data Delta does not fill the target window Delta property block detected - not supported by svndumpfilter Deltifying revision %ld... Describe the usage of this program or its subcommands.
usage: help [SUBCOMMAND...]
 Describe the usage of this program or its subcommands.
usage: svndumpfilter help [SUBCOMMAND...]
 Destination '%s' already exists Destination '%s' is not a directory Destination directory exists, and will not be overwritten unless forced Destination directory exists; please remove the directory or use --force to overwrite Did not expect '%s' to be a working copy root Diff version inconsistencies in representation '%s' Directory '%s' has no THIS_DIR entry Directory '%s' has no URL Directory entry corrupt Directory needs to be empty but is not Dirlist element not a list Disabled repository feature Do not display filtering statistics. Don't filter revision properties. Dropped %d node(s):
 Dropped %d revision(s).

 Dumpstream data appears to be malformed Duplicate something in working copy or repository, remembering history.
usage: copy SRC DST

  SRC and DST can each be either a working copy (WC) path or URL:
    WC  -> WC:   copy and schedule for addition (with history)
    WC  -> URL:  immediately commit a copy of WC to URL
    URL -> WC:   check out URL into WC, schedule for addition
    URL -> URL:  complete server-side copy;  used to branch & tag
 Editing property on non-local target '%s' not yet supported Either a URL or versioned item is required Empty error list End revision %ld is invalid (youngest revision is %ld) Entry '%s' has illegal schedule Entry '%s' has invalid '%s' value Entry '%s' has invalid node kind Entry '%s' has no URL Entry '%s' has unexpectedly changed special status Entry '%s' is already under version control Entry already exists Entry for '%s' exists (though the working file is missing) Entry for '%s' has no URL Entry for '%s' is marked as 'copied' but is not itself scheduled
for addition.  Perhaps you're committing a target that is
inside an unversioned (or not-yet-versioned) directory? Entry has an invalid attribute Entry has no URL Entry has no revision Error aborting report Error bumping revisions post-commit (details follow): Error calling external program Error checking existence of '%s' Error closing directory '%s' Error closing filesystem Error closing null file Error closing read end of stderr pipe Error closing write end of stderr pipe Error comparing '%s' and '%s' Error during recursive add of '%s' Error getting 'affected time' for '%s' Error getting 'affected time' of '%s' Error getting 'affected time' on '%s' Error getting UID of process Error in child process: %s Error in post-commit clean-up (details follow): Error modifying entry for '%s' Error modifying entry of '%s' Error opening db lockfile Error opening local file Error parsing %s property on '%s': '%s' Error parsing '%s' Error processing command '%s' in '%s' Error reading administrative log file in '%s' Error reading spooled %s request response Error replacing text-base of '%s' Error resolving case of '%s' Error restoring text for '%s' Error running '%s':  exitcode was %d, args were:
in directory '%s', basenames:
%s
%s
%s Error running post-commit hook Error spooling the %s request response to disk Error unlocking locked dirs (details follow): Error validating server certificate for '%s':
 Error waiting for process '%s' Error writing entries file for '%s' Error writing hash to '%s' Error writing log file for '%s' Error writing log for '%s' Error writing to '%s' Error writing to '%s': unexpected EOF Exactly one file argument required Excluding (and dropping empty revisions for) prefixes:
 Excluding prefixes:
 Expected FS format '%d'; found format '%d' Expires: %s
 Explicit target argument required Explicit target required ('%s' interpreted as prop value) Export complete.
 Exported external at revision %ld.
 Exported revision %ld.
 External at revision %ld.
 External checkout complete.
 External export complete.
 External update complete.
 Failed to add directory '%s': copyfrom arguments not yet supported Failed to add directory '%s': object of the same name already exists Failed to add directory '%s': object of the same name as the administrative directory Failed to add directory '%s': object of the same name is already scheduled for addition Failed to add file '%s': object of the same name already exists Failed to add file '%s': object of the same name is already scheduled for addition Failed to delete mmap '%s' Failed to find label '%s' for URL '%s' Failed to find time on revision %ld Failed to get exclusive repository access; perhaps another process
such as httpd, svnserve or svn has it open? Failed to get new string key Failed to load properties from disk Failed to mark '%s' absent: item of the same name is already scheduled for addition Failed to revert '%s' -- try updating instead.
 Failed to run '%s' hook; broken symlink Failure loading plugin File '%s' already exists File '%s' has binary mime type property File '%s' has inconsistent newlines File '%s' has local modifications File '%s' in directory '%s' is not a versioned resource File already exists: filesystem '%s', revision %ld, path '%s' File is not mutable: filesystem '%s', revision %ld, path '%s' File not found: revision %ld, path '%s' Filesystem directory has no such entry Filesystem has no item Filesystem has no such copy Filesystem has no such node-rev-id Filesystem has no such representation Filesystem has no such string Filesystem is already open Filesystem is corrupt Filesystem is not open Filesystem object has not been opened yet Filesystem path '%s' is neither a file nor a directory Filter out nodes with given prefixes from dumpstream.
usage: svndumpfilter exclude PATH_PREFIX...
 Filter out nodes without given prefixes from dumpstream.
usage: svndumpfilter include PATH_PREFIX...
 Final line in revision file longer than 64 characters Final line in revision file missing space First line of '%s' contains non-digit First revision cannot be higher than second Format of an svn:externals property was invalid Found a working copy path Found malformed header in revision file Framing error in pipe protocol GET request failed for '%s' General filesystem error Got source path but no source revision for '%s' HTTP Path Not Found Illegal svn repository URL '%s' Illegal target for the requested operation Impossibly long repository root from server In directory '%s' Including (and dropping empty revisions for) prefixes:
 Including prefixes:
 Incompatible library version Incomplete data Incomplete or unterminated property block Inconsistent line ending style Incorrect arguments supplied Incorrect parameters given Index Input/output error Inter-repository relocation not allowed Internal error: there is an unknown parent (%d) for the 'DAV:response' element within the MERGE response Invalid %s property on '%s': target involves '.' or '..' or is an absolute path Invalid URL '%s' Invalid URL: illegal character in proxy port number Invalid URL: negative proxy port number Invalid URL: proxy port number greater than maximum TCP port number 65535 Invalid change kind in rev file Invalid change ordering: new node revision ID without delete Invalid change ordering: non-add change on deleted path Invalid change type Invalid changes line in rev-file Invalid config: illegal character in debug mask value Invalid config: illegal character in timeout value Invalid config: negative timeout value Invalid config: unable to load certificate file '%s' Invalid configuration value Invalid copy source path '%s' Invalid diff stream: [new] insn %d overflows the new data section Invalid diff stream: [src] insn %d overflows the source view Invalid diff stream: [tgt] insn %d starts beyond the target view position Invalid diff stream: insn %d cannot be decoded Invalid diff stream: insn %d has non-positive length Invalid diff stream: insn %d overflows the target view Invalid editor anchoring; at least one of the input paths is not a directory and there was no source entry Invalid end revision %ld Invalid file or dir token during edit Invalid filesystem path syntax Invalid filesystem revision number Invalid filesystem transaction name Invalid lock Invalid operation on the current working directory Invalid prop-mod flag in rev-file Invalid report for top level of working copy Invalid revision number supplied Invalid schedule Invalid server response to dated-rev request Invalid start revision %ld Invalid target path Invalid text-mod flag in rev-file Item already exists in filesystem Item is out-of-date Last Changed Author: %s
 Last Changed Date Last Changed Rev: %ld
 Left locally modified or unversioned files Line endings other than expected Local URL '%s' contains only a hostname, no path Local URL '%s' contains unsupported hostname Local URL '%s' does not contain 'file://' prefix Local, non-commit operations do not take a log message Location entry not a list Lock Comment (%i line):
%s
 Lock Comment (%i lines):
%s
 Lock Created Lock Expires Lock Owner: %s
 Lock Token: %s
 Lock comment has illegal characters Lock failed: newer version of '%s' exists Lock file '%s' is not a regular file Lock has expired Lock token is incorrect Log command for directory '%s' is mislocated Log entry missing 'name' attribute (entry '%s' for directory '%s') Log entry not a list Log message contains a zero byte Log message file is a versioned file; use '--force-log' to override MD5 checksum is missing MIME type '%s' does not contain '/' MIME type '%s' ends with non-alphanumeric character MIME type '%s' has empty media type Malformed URL for repository Malformed XML: %s at line %d Malformed copyfrom line in node-rev Malformed copyroot line in node-rev Malformed dumpfile header Malformed dumpstream: Revision 0 must not contain node records Malformed error list Malformed file Malformed network data Malformed representation header Malformed skeleton data Malformed stream data Malformed svndiff data in representation Malformed text rep offset line in node-rev Merge conflict during commit Mismatch popping the WC unwind stack Mismatched FS module version for '%s': found %d.%d.%d%s, expected %d.%d.%d%s Missing 'dest' attribute in '%s' Missing 'left' attribute in '%s' Missing 'revision' attribute for '%s' Missing 'right' attribute in '%s' Missing 'timestamp' attribute in '%s' Missing changed-path information for '%s' in revision %ld Missing cpath in node-rev Missing default entry Missing kind field in node-rev Missing node-id in node-rev Missing propname and repository path arguments Missing propname or repository path argument Missing repository path argument Missing required node revision ID Missing required revision specification Missing revision Modified Module for accessing a repository on local disk. Module for accessing a repository using the svn network protocol. Module for accessing a repository via WebDAV (DeltaV) protocol. Module for working with a Berkeley DB repository. Module for working with a plain file (FSFS) repository. Move will not be attempted unless forced Mutually exclusive arguments specified Name does not refer to a filesystem directory Name does not refer to a filesystem file Name is not a single path component Name: %s
 Name: %s%s Negative expiration date passed to svn_fs_lock Neon was unable to parse URL '%s' Network connection closed unexpectedly Network read/write error Network socket initialization failed New entry name required when importing a file No '.' entry in: '%s' No auth file path available No authentication provider available No changes to property '%s' on '%s'
 No changes to property '%s' on revision %ld
 No common parent found, unable to operate on disjoint arguments No copy with id '%s' in filesystem '%s' No default entry in directory '%s' No entry for '%s' No external editor available No part of path '%s' was found in repository HEAD No such XML tag attribute No such entry: '%s' No such revision %ld No such thing as 'base' working copy properties! No such transaction No support for repos <--> working copy moves No transaction named '%s' in filesystem '%s' No username is currently associated with filesystem '%s' No write-lock in '%s' Node Kind: directory
 Node Kind: file
 Node Kind: none
 Node Kind: unknown
 Non-ASCII character (code %d) detected, and unable to convert to/from UTF-8 Non-numeric limit argument given Non-string as part of file contents Non-string as part of text delta None of the environment variables SVN_EDITOR, VISUAL or EDITOR is set, and no 'editor-cmd' run-time configuration option was found Not all required revisions are specified Not authorized to open root of edit operation Null rep, but offset past zero already Number is larger than maximum Object is not a revision root Object is not a transaction root Obstructed update Only diffs between a path's text-base and its working files are supported at this time Only one revision allowed Only relative paths can be specified after a URL Operation does not apply to binary file Operation does not apply to directory Option --tunnel-user is only valid in tunnel mode.
 Out of date: '%s' in transaction '%s' Output the content of specified files or
URLs with revision and author information in-line.
usage: blame TARGET[@REV]...

  If specified, REV determines in which revision the target is first
  looked up.
 Output the content of specified files or URLs.
usage: cat TARGET[@REV]...

  If specified, REV determines in which revision the target is first
  looked up.
 Passphrase for '%s':  Password for '%s':  Path '%s' already exists Path '%s' does not exist Path '%s' does not exist in revision %ld Path '%s' ends in '%s', which is unsupported for this operation Path '%s' is not a file Path '%s' not found in head revision Path '%s' not found in revision %ld Path has no lock token Path is not a working copy directory Path is not a working copy file Path syntax not supported in this context Path: %s
 Please upgrade the server to 0.19 or later Premature end of content data in dumpstream Problem on first log entry in a working copy Problem running log Process '%s' failed (exitwhy %d) Process '%s' returned error exitcode %d Prop diffs element not a list Properties Last Updated Property '%s' is an entry property Property not found Proplist element not a list Protocol error: the MERGE response for the '%s' resource did not return all of the properties that we asked for (and need to complete the commit) Protocol error: we told the server not to auto-merge any resources, but it said that '%s' was merged Put files and directories under version control, scheduling
them for addition to repository.  They will be added in next commit.
usage: add PATH...
 Python exception has been set with the error RA layer didn't receive requested OPTIONS info RA layer failed to create HTTP request RA layer failed to fetch properties RA layer failed to init socket layer RA layer file already exists RA layer request failed REPORT response handling failed to complete the editor drive REVISION   PATH
--------   ----
 REVISION   PATH <ID>
--------   ---------
 Ran out of unique names Read access denied for root of edit Read error in pipe Reading '%s' Reading from stdin is currently broken, so disabled Reading one svndiff window read beyond the end of the representation Recursively clean up the working copy, removing locks, resuming
unfinished operations, etc.
usage: cleanup [PATH...]
 Reference to non-existent node '%s' in filesystem '%s' Reference to non-existent revision %ld in filesystem '%s' Relative source revision %ld is not available in current repository Remove 'conflicted' state on working copy files or directories.
usage: resolved PATH...

  Note:  this subcommand does not semantically resolve conflicts or
  remove conflict markers; it merely removes the conflict-related
  artifact files and allows PATH to be committed again.
 Remove files and directories from version control.
usage: 1. delete PATH...
       2. delete URL...

  1. Each item specified by a PATH is scheduled for deletion upon
    the next commit.  Files, and directories that have not been
    committed, are immediately removed from the working copy.
    PATHs that are, or contain, unversioned or modified items will
    not be removed unless the --force option is given.

  2. Each item specified by a URL is deleted from the repository
    via an immediate commit.
 Remove revisions emptied by filtering. Renumber revisions left after filtering. Rep '%s' both mutable and non-fulltext Rep contents are too large: got %s, limit is %s Replacing      %s
 Repository URL required when importing Repository UUID: %s
 Repository access is needed for this operation Repository access method not implemented Repository argument required Repository creation failed Repository has no UUID Repository has not been enabled to accept revision propchanges;
ask the administrator to create a pre-revprop-change hook Repository lock acquired.
Please wait; recovering the repository may take some time...
 Resolved conflicted state of '%s'
 Restore pristine working copy file (undo most local edits).
usage: revert PATH...

  Note:  this subcommand does not require network access, and resolves
  any conflicted states.  However, it does not restore removed directories.
 Restored '%s'
 Reverted '%s'
 Revision %ld committed as %ld.
 Revision %ld doesn't match existing revision %ld in '%s' Revision %ld skipped.
 Revision action '%c' for revision %ld of '%s' lacks a prior revision Revision entry not a list Revision file lacks trailing newline Revision property '%s' not allowed in this context Revision range is not allowed Revision: %ld
 Revisions renumbered as follows:
 SSL is not supported Schedule: add
 Schedule: delete
 Schedule: normal
 Schedule: replace
 Second revision required Sending        %s
 Server did not send repository root Server does not support date-based operations Server requires minimum version %d Set new value for property '%s' on '%s'
 Set new value for property '%s' on revision %ld
 Skipped '%s'
 Skipped missing target: '%s'
 Skipping binary file: '%s'
 Something is wrong with the log message's contents Source '%s' is not a directory Source URL '%s' is from foreign repository; leaving it as a disjoint WC Source and dest appear not to be in the same repository (src: '%s'; dst: '%s') Source url '%s' is from different repository Special code for wrapping server errors to report to client Start revision %ld is greater than end revision %ld Start revision must precede end revision Status against revision: %6ld
 Storage of non-regular property '%s' is disallowed through the repository interface, and could indicate a bug in your client String does not represent a node or node-rev-id String length larger than maximum Subcommand '%s' doesn't accept option '%s'
Type 'svn help %s' for usage.
 Subcommand argument required
 Subversion is a tool for version control.
For additional information, see http://subversion.tigris.org/
 Successful edit status returned too soon Sum of subblock sizes larger than total block content length Svndiff contains corrupt window header Svndiff data contains backward-sliding source view Svndiff data contains corrupt window Svndiff data contains invalid instruction Svndiff data ends unexpectedly Svndiff data has invalid header Svndiff has backwards-sliding source views Svndiff has invalid header Symbolic links are not supported on this platform Syntax error in native-eol argument '%s' Syntax error in revision argument '%s' Syntax error parsing revision '%s' Target lists to diff may not contain both working copy paths and URLs Test failed Text Last Updated Text delta chunk not a string The %s request returned invalid XML in the response: %s (%s) The '--transaction' (-t) and '--revision' (-r) arguments can not co-exist The CHECKOUT response did not contain a 'Location:' header The MERGE property response had an error status The OPTIONS response did not include the requested activity-collection-set; this often means that the URL is not WebDAV-enabled The UUID property was not found on the resource or any of its parents The VCC property was not found on the resource The file-revs report didn't contain any revisions The following repository access (RA) modules are available:

 The get-file-revs command didn't return any revisions The latest repos revision is %ld.
 The location for '%s' for revision %ld does not exist in the repository or refers to an unrelated object The log message file is under version control The log message is a pathname The log message is a pathname (was -F intended?); use '--force-log' to override The operation was interrupted The path was not part of a repository The relative-path property was not found on the resource The repository at '%s' has uuid '%s', but the WC has '%s' The repository is locked, perhaps for db recovery The specified diff option is not supported The specified transaction is not mutable There is already a versioned item '%s' This client is too old to work with working copy '%s'; please get a newer Subversion client This is an empty revision for padding. Too many arguments to import command Too many nested items Transaction '%s' is not based on a revision; how odd Transaction '%s' removed.
 Transaction is dead Transaction is not dead Transaction is out of date Transaction out of date Transmitting file data  Tried a versioning operation on an unversioned resource Tried to change an immutable representation Try 'svn add' or 'svn add --non-recursive' instead? Trying to add new property '%s' with value '%s',
but property already exists with value '%s'. Trying to change property '%s' from '%s' to '%s',
but property already exists with value '%s'. Trying to change property '%s' from '%s' to '%s',
but the property does not exist. Trying to use an unsupported feature Tunnel scheme %s requires environment variable %s to be defined Two top-level reports with no target Two versioned resources are unrelated Type '%s --help' for usage.
 Type '%s help' for usage.
 URL '%s' contains a '..' element URL '%s' doesn't exist URL '%s' doesn't match existing URL '%s' in '%s' URL '%s' is not properly URI-encoded URL '%s' non-existent in that revision URL '%s' refers to a directory URL '%s' refers to a file, not a directory URL: %s
 UUID Token: %s
 Unable to extract data from response header Unable to find repository location for '%s' in revision %ld Unable to locate auth file Unable to lock/unlock across multiple repositories Unable to make any directories Unable to make name for '%s' Unable to open an ra_local session to URL Unable to open auth file for reading Unable to open auth file for writing Unable to open repository '%s' Unable to open root of edit Undefined tunnel scheme '%s' Unexpected EOF on stream Unexpected EOF writing contents Unexpected end of svndiff input Unexpected node kind found Unexpected or unknown property kind Unexpected server response to authentication Unexpectedly found '%s': path is marked 'missing' Unknown FS type Unknown FS type '%s' Unknown XML encoding: '%s' Unknown authorization method Unknown command '%s' Unknown command: '%s'
 Unknown entry kind for '%s' Unknown hostname '%s' Unknown node kind for '%s' Unknown or unexpected kind for path '%s' Unknown or unversionable type for '%s' Unknown status '%s' in command response Unknown svn protocol command Unknown svn_node_kind Unmergeable scheduling requested on an entry Unrecognized URL scheme '%s' Unrecognized binary data encoding; can't decode Unrecognized line ending style Unrecognized logfile element '%s' in '%s' Unrecognized node kind '%s' from server Unrecognized node kind: '%s' Unrecognized node-action on node '%s' Unrecognized record type in stream Unrecognized revision type requested for '%s' Unrecognized stream data Unsupported FS loader version (%d) for bdb Unsupported FS loader version (%d) for fsfs Unsupported RA plugin ABI version Unsupported dumpfile version: %d Unsupported node kind for path '%s' Unsupported repository version Unsupported working copy format Unversioned properties on revision %ld:
 Update complete.
 Updated external to revision %ld.
 Updated to revision %ld.
 Use --force to override this restriction User %s does not own lock on path '%s' (currently locked by %s) Username does not match lock owner Username:  Valid UTF-8 data
(hex:%s)
followed by invalid UTF-8 sequence
(hex:%s) Value will not be set unless forced Version %d is not non-negative Version file format not correct Version mismatch in '%s': found %d.%d.%d%s, expected %d.%d.%d%s WARNING: Referencing data in revision %ld, which is older than the oldest
WARNING: dumped revision (%ld).  Loading this dump into an empty repository
WARNING: will fail.
 Waiting on repository lock; perhaps another process has it open?
 When specifying working copy paths, only one target may be given Won't delete locally modified directory '%s' Working copy '%s' locked Working copy format of '%s' is too old (%d); please check out your working copy again Working copy is corrupt Working copy is not up-to-date Working copy not locked; this is probably a bug, please report Working copy path '%s' does not exist in repository Working copy text base is corrupt Write error Write-lock stolen in '%s' XML data was not well-formed XML parser failed in '%s' Your .svn/tmp directory may be missing or corrupt; run 'svn cleanup' and try again Your commit message was left in a temporary file: allocating new copy ID (getting 'next-key') allocating new node ID (getting 'next-key') allocating new representation (getting next-key) applying log message to %s authorization failed be verbose bumping next copy key bumping next node ID key bypass the repository hook system call post-commit hook after committing revisions call pre-commit hook before committing revisions closing changes cursor could not connect to server creating change creating cursor for reading a string creating cursor for reading changes daemon mode deleting changes deleting entry from 'copies' table deleting entry from 'nodes' table descend recursively disable automatic log file removal [Berkeley DB] disable automatic properties disable fsync at transaction commit [Berkeley DB] display this help display update information disregard default and svn:ignore property ignores do no interactive prompting do not cache authentication tokens do not cross copies while traversing history do not output the trailing newline do not print differences for deleted files don't unlock the targets done.
 dump incrementally enable automatic properties exported%s fetching changes force operation to run force validity of log message source give output suitable for concatenation ignore ancestry when calculating merges ignore any repos UUID found in the stream ignore externals definitions inetd mode last changed rather than current revisions listen hostname or IP address (for daemon mode) listen once (useful for debugging) listen port (for daemon mode) load at specified directory in repository maximum number of log entries next-id file corrupt no progress (only errors) to stderr notice ancestry when calculating differences operate on a revision property (use with -r) operate on single directory only output in XML pass contents of file ARG as additional args path '%s' doesn't exist in revision %ld print as little as possible print differences against the copy source print extra information property '%s' deleted (recursively) from '%s'.
 property '%s' deleted from '%s'.
 property '%s' deleted from repository revision %ld
 property '%s' set (recursively) on '%s'
 property '%s' set on '%s'
 property '%s' set on repository revision %ld
 read user configuration files from directory ARG relocate via URL-rewriting root of directory to serve run in foreground (useful for debugging) set repos UUID to that found in stream, if any show full paths instead of indenting them show help on a subcommand show node revision ids for each path specify a password ARG specify a username ARG specify revision number ARG specify revision number ARG (or X:Y range) specify transaction name ARG subcommand '%s' doesn't accept option '%s'
Type 'svnadmin help %s' for usage.
 subcommand argument required
 svn: run 'svn cleanup' to remove locks (type 'svn help cleanup' for details)
 svndiff data requested non-existent source svndiff requested position beyond end of stream svndiff window length is corrupt system('%s') returned %d timed out waiting for server treat value as being in charset encoding ARG try operation but make no changes tunnel mode tunnel username (default is current uid's name) unknown usage: svnadmin create REPOS_PATH

Create a new, empty repository at REPOS_PATH.
 usage: svnadmin deltify [-r LOWER[:UPPER]] REPOS_PATH

Run over the requested revision range, performing predecessor delti-
fication on the paths changed in those revisions.  Deltification in
essence compresses the repository by only storing the differences or
delta from the preceding revision.  If no revisions are specified,
this will simply deltify the HEAD revision.
 usage: svnadmin dump REPOS_PATH [-r LOWER[:UPPER]] [--incremental]

Dump the contents of filesystem to stdout in a 'dumpfile'
portable format, sending feedback to stderr.  Dump revisions
LOWER rev through UPPER rev.  If no revisions are given, dump all
revision trees.  If only LOWER is given, dump that one revision tree.
If --incremental is passed, then the first revision dumped will be
a diff against the previous revision, instead of the usual fulltext.
 usage: svnadmin help [SUBCOMMAND...]

Describe the usage of this program or its subcommands.
 usage: svnadmin hotcopy REPOS_PATH NEW_REPOS_PATH

Makes a hot copy of a repository.
 usage: svnadmin list-dblogs REPOS_PATH

List all Berkeley DB log files.

WARNING: Modifying or deleting logfiles which are still in use
will cause your repository to be corrupted.
 usage: svnadmin list-unused-dblogs REPOS_PATH

List unused Berkeley DB log files.

 usage: svnadmin load REPOS_PATH

Read a 'dumpfile'-formatted stream from stdin, committing
new revisions into the repository's filesystem.  If the repository
was previously empty, its UUID will, by default, be changed to the
one specified in the stream.  Progress feedback is sent to stdout.
 usage: svnadmin lstxns REPOS_PATH

Print the names of all uncommitted transactions.
 usage: svnadmin recover REPOS_PATH

Run the Berkeley DB recovery procedure on a repository.  Do
this if you've been getting errors indicating that recovery
ought to be run.  Recovery requires exclusive access and will
exit if the repository is in use by another process.
 usage: svnadmin rmlocks REPOS_PATH LOCKED_PATH...

Unconditionally remove lock from each LOCKED_PATH.
 usage: svnadmin rmtxns REPOS_PATH TXN_NAME...

Delete the named transaction(s).
 usage: svnadmin setlog REPOS_PATH -r REVISION FILE

Set the log-message on revision REVISION to the contents of FILE.  Use
--bypass-hooks to avoid triggering the revision-property-related hooks
(for example, if you do not want an email notification sent
from your post-revprop-change hook, or because the modification of
revision properties has not been enabled in the pre-revprop-change
hook).

NOTE: revision properties are not historied, so this command
will permanently overwrite the previous log message.
 usage: svnadmin verify REPOS_PATH

Verifies the data stored in the repository.
 usage: svnlook author REPOS_PATH

Print the author.
 usage: svnlook cat REPOS_PATH FILE_PATH

Print the contents of a file.  Leading '/' on FILE_PATH is optional.
 usage: svnlook changed REPOS_PATH

Print the paths that were changed.
 usage: svnlook date REPOS_PATH

Print the datestamp.
 usage: svnlook diff REPOS_PATH

Print GNU-style diffs of changed files and properties.
 usage: svnlook dirs-changed REPOS_PATH

Print the directories that were themselves changed (property edits)
or whose file children were changed.
 usage: svnlook help [SUBCOMMAND...]

Describe the usage of this program or its subcommands.
 usage: svnlook history REPOS_PATH [PATH_IN_REPOS]

Print information about the history of a path in the repository (or
the root directory if no path is supplied).
 usage: svnlook info REPOS_PATH

Print the author, datestamp, log message size, and log message.
 usage: svnlook log REPOS_PATH

Print the log message.
 usage: svnlook tree REPOS_PATH [PATH_IN_REPOS]

Print the tree, starting at PATH_IN_REPOS (if supplied, at the root
of the tree otherwise), optionally showing node revision ids.
 usage: svnlook uuid REPOS_PATH

Print the repository's UUID.
 usage: svnlook youngest REPOS_PATH

Print the youngest revision number.
 use ARG as diff command use ARG as external editor use ARG as merge command use ARG as the newer target use ARG as the older target use deltas in dump output use strict semantics use threads instead of fork wait instead of exit if the repository is in
                             use by another process Project-Id-Version: subversion 1.2.0
Report-Msgid-Bugs-To: dev@subversion.tigris.org
POT-Creation-Date: 2006-08-23 13:49-0500
PO-Revision-Date: 2004-09-12 22:05+0800
Last-Translator: Subversion Developers <dev@subversion.tigris.org>
Language-Team: Simplified Chinese <dev@subversion.tigris.org>
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit
 
------- 提交新修订版 %ld (从原始修订版 %ld 装载) >>>

 
------- 提交后的修订版 %ld >>>

 
提交后的修订版为 %ld。
 
错误：没有提供前置字首，
 
正在取得外部项目至“%s”
 
日志信息未改变，或是未指定
a)中断，c)继续，e)编辑
 
对外部项目“%s”进行状态检查
 
修复完成。
 
有效选项：
      * 正在添加路径：%s ...      * 正在删除路径：%s ...      * 正在修改路径：%s ...      * 正在替换路径：%s ...    %ld => (丢弃)
  (从 %s:%ld)  - 本凭证发生不明的错误。
  - 本凭证已过期。
  - 本凭证的主机名称不符。
  - 本凭证并不是由受信任的权威机权所核发。请手动利用指纹以验证
   凭证的有效性！
  - 本凭证尚未进入有效期间。
  参数 “%s”：未知的命令。

 %s	（修订版 %ld） %s	（工作拷贝） %s 的结果 “%s” %s 请求“%s”失败 %s，版本 %s
   编译于 %s，%s

 %s：(不是受版本控制的资源)

 %s 属性改变于：%s%s %s\ 在文件 %s 末尾没有新行 %s警告：%s
 “%s”
与
“%s”
并不在同一个档案库中 “%s”已存在 “%s”已存在，变成路中间的大石头 “%s”不像是地址(URL) “%s”不像是工作拷贝路径 “%s”未定义 '%s()' “%s”不存在 “%s”似乎没有关联的地址(URL) “%s”已存在并且非空 “%s”已存在，但不是目录 “%s”的节点种类无法辨识 “%s”已有本地修改 ‘%s’锁被盗取，但没有锁的所有者 “%s”没有地址(URL) “%s”没有演进历程信息 外挂“%s”失败，错误输出：
%s “%s”于修订版 %ld 是一个无关的物件 “%s”是地址(URL)，但是应该是路径
 “%s”是一个地址(URL)，但是地址(URL)不可为作提交的目标 “%s”是地址(URL)，也许该是路径。 “%s”是一个保留的名称，无法被导入 “%s”是一个工作拷贝属性(wcprop)，应此不能被客户端存取 “%s”已是不同地址(URL)的工作拷贝 “%s”已纳入版本控制 “%s”是地址(URL)，但是应该是路径 “%s”挡了实际纳入版本控制资源的路 “%s”被标记为不存在，所以无法加入新增调度 “%s”不是文件名也不是目录名 “%s”不是目录 “%s”不是文件系统“%s”的目录 “%s”不是文件或目录 “%s”不是一个有效的 EOL 值 “%s”不是工作拷贝 “%s”并不是工作拷贝目录 “%s”尚未纳入版本控制 “%s”尚未纳入版本控制，也不是提交的一部份，但是它的子路径“%s”是提交的一部份 “%s”于未纳入版本控制的父路径中被加入新增调度 “%s”被用户“%s”锁定。
 找不到“%s” 在文件系统中找不到“%s” “%s”未纳入版本控制，并未导出
 找不到路径“%s” “%s” 返回 %d “%s”到“%s”不是有效的重定位 “%s”被解除锁定。
 在档案库的修订版 %2$ld 中找不到“%1$s” “%s”不存在于本资源上 'DAV:baseline-collection' 不存在于基线资源 'DAV:version-name' 不存在于基线资源 只有在XML模式“增加”选项才有效 在XML模式中“详细”选项无效 (二进制文件差异)
 (R)拒绝 或 (t)暂时接受 ？ (R)拒绝，(t)暂时接受 或 (p)永远接受？ (本地) (没有作者信息) (没有时间) --此行及以下内容将会被忽略-- --auto-props 与 --no-auto-props 是互相排斥的 .merge-left.r%ld .merge-right.r%ld .working <<< 开始新的事务，基于最初的修订版 %ld
 <delta-pkg> 没有演进历程 对“%s”的 MERGE 回应并不是目标 (“%s”) 的子项目 总和检查值不符 工作拷贝中的冲突阻碍了目前的操作 在没有需要的地方给出一个日志信息 本操作仅能作用于纳入版本控制的路径 有问题发生；请参阅随附的细节 无法产生报告，因为没有提供任何数据 档案库外挂错误 表现消失或读时被改变 工作拷贝的合并来源需特别指定修订版 APR_APPEND 不支持管理文件 中止提交：“%s”仍处于冲突状态 没有找到活动项 已加入 新增           %s
 新增 (二进制)  %s
 所有的认证提供者已耗尽 Apache 得到一个畸形的 URI Apache 没有指向 SVN 文件系统的路径 Apply-textdelta 已经激活 Apply-textdelta 没有激活 是否所有的目标都是同一个工作拷贝的一部份？ 至少有一个属性变更失败；档案库未改变 对于 peg 差异，必须至少有一个非本地端的修订版 于修订版 %ld。
 尝试要修改不可改变的文件系统节点 企图 deltify “%s” 自身 尝试弹出空的工作拷贝(WC)展开堆栈 尝试要移除或再次建立文件系统的根目录 试图在非提交操作中，设定工作拷贝属性“%s”于“%s” 尝试解锁非空展开堆栈 尝试于管理目录中执行命令 企图用一个非法的名字“%s”创建节点 企图删除一个非法名字“%s”的节点 企图获取非文件节点的校验和 企图获取非文件节点的长度 企图获取非文件节点的文本内容 尝试要锁定一个已锁定的目录 企图以非法名字“%s”建立子克隆 企图以不合法的名字“%s”打开节点 企图打开不存在的子节点“%s” 企图写至非事务 尝试对已修改的资源进行受限的操作 尝试对地址(URL)进行一次以上的提交 来自服务器的认证错误：%s 认证领域：%s
 作者名称不可包含换行符 授权失败 传递至 RA 层的地址(URL)有问题 不正确的数据库版本：用 %d.%d.%d 版本编译，正在运行版本 %d.%d.%d 不正确的数据库版本：得到 %d.%d.%d，应该最低是 %d.%d.%d 有问题的编码选项：属性内容未以 UTF8 储存 错误的合并；最初的，原始，目标不是全部在同一个文件系统(fs) 错误的合并：目标“%s”拥有编号“%s”，与初始的相同 无效的内存池(pool)传递到 svn_make_pool() 有问题的属性名称 有问题的属性名称：“%s” 错误的类型 基线不正确 Berkeley DB 发生死锁错误 Berkeley DB 错误 有问题的地址(URL) 有问题的日期 有问题的文件名 有问题的 mime-type 使用了有问题的修订版信息 有问题的修订版报告 无法新增“%s”至已预定要删除的父目录 无法新增“%s”至已删除的目录；请先尝试恢复其父目录 不能附加“%s”到“%s” 无法计算被请求的日期 无法算出请求的主体大小 无法更改工作拷贝至“%s” 无法检查目录“%s” 无法检查路径“%s” 无法 chmod“%s” 无法关闭目录“%s” 无法关闭文件“%s” 无法关闭流 无法连线至主机“%s” 不能把模块路径从UCS-2转换为UTF-8：“%s” 不能复制“%s”到“%s” 无法创建目录“%s” 无法为外挂“%s”创建空的标准输出 无法为外挂“%s”创建管道 无法建立进程“%s”的属性 无法建立 socket 无法建立 tunnel 不能检测非文件的“%s”的MIME类型 不能确定本地路径编码 无法明确时间 不能获取文件系统(FSFS)互斥体 无法找到临时目录 找不到工作拷贝路径 无法找到一个项目 无法在“%2$s”中找到项目“%1$s” 无法找到“%s”的项目 在尝试新增“%s”时，无法找到父目录的项目 不能写入文件“%s” 不能把文件写入磁盘 不能写入流 无法从文件“%s”获取属性信息 无法从流获取属性信息 不能获取”%s“文件的默认文件权限(文件 stat 错误) 不能获取文件“%s”的排他锁 无法获取文件名 无法取得密码 不能获取文件“%s“的共享锁 无法取得工作目录 不能获取文件(FSFS)档案库互斥体 无法隐藏目录“%s” 不能锁定字符转换互斥体 无法创建目录“%s” 不能获得当前日期 不能移动“%s”到“%s” 无法移动来源至目的 不能打开“%s” 无法打开目录“%s” 不能打开文件“%s” 无法打开标准错误 (stderr) 无法打开标准输入 无法启动标准输出入文件 无法打开标准输出 (stdout) 无法剖析“%s” 不能读取连接的内容 无法读取目录 无法读取目录“%s” 无法在目录“%s”读取内容 不能读取文件“%s” 无法读取连线 无法读取标准输入 无法读取流 无法自 APR 将错误信息重新编码 无法删除“%s” 无法删除目录“%s” 不能移动文件“%s” 无法在已删除的目录中替换“%s”；请先尝试恢复其父目录 无法还原工作目录 无法返回目录“%s” 不能设置“%s”的存取时间 无法设置文件“%s”只读 无法设置文件“%s”可读写 不能在“%s”设置许可权限 无法在文件“%s”设置定位指针 无法在流中设置定位指针 无法设置子进程“%s”错误输出文件 无法设置子进程“%s”输入 无法设置子进程“%s”输出文件 无法设置进程“%s”的命令类型 无法设定进程“%s”目录 不能在不可更改节点修订版 %s 设置属性列表 无法设定目录的文字内容 不能取得进程“%s”状态 不能取得“%s”状态 无法获取目录“%s”的状态 不能存储文件系统(FSFS)互斥体 不能 tokenize 命令“%s” 不能释放文件系统(FS)互斥体 不能是否文件(FSFS)档案库互斥体 不能解锁字符转换互斥体 无法写入属性集到“%s” 无法写入至“%s” 无法写入连线 无法写入文件“%s” 无法写入流 无法为二进制文件“%s”计算谴责信息 无法更改节点种类 无法提交“%s”与“%s”，因为它们都指向同一个地址(URL) 无法复制或移动“%s”：它尚未存在于档案库之中；请尝试先进行提交 无法复制或移动“%s”：它尚未存在于档案库之中；请尝试先进行提交 无法复制或移动“%s”：它尚未纳入版本控制 无法复制路径“%s”至其子目录“%s”中 无法复制到“%s”至因为它已预定为要删除 无法显示：文件标记为二进制类型。%s 在非交互式的情况下不能调用编辑器获取日志信息 无法在文件系统“%2$s”中修改名为“%1$s”的事务 无法移动地址(URL)“%s”至本身 无法移动路径“%s”至其本身 无法协商出认识机制 无法以非递归提交目录删除 不能在目录中替换 无法恢复“%s”：不支持的项目节点种类 无法恢复“%s”：工作拷贝中有不支持的节点种类 无法恢复目前目录的新增；请从父目录再试一次 无法恢复：“%s”未纳入版本控制 无法设定“%s”于目录 (“%s”) 无法设定“%s”于文件 (“%s”) 删除纳入版本控制的属性“%s”时不可指定修订版 编辑纳入版本控制的属性“%s”时不可指定修订版 无法指定修订版以设定版本控制下的属性“%s” 移动操作不可指定修订版 不能解除路径”%s“的锁定，没有可用的已授权用户名 不能校验路径“%s”上的锁；没有匹配的可用锁标识 不能在路径“%s”校验锁；没有有效的用户名 无法写入“%s”的属性杂凑 容量条目不是一个 word 捕捉到信号(中断) 凭证信息：
 - 主机名称：%s
 - 有效期间：自 %s 至 %s
 - 发行者：%s
 - 指纹：%s
 改变的路径：
 已修改路径条目不是一个列表 取出修订版 %ld 的外部定义。
 取出修订版 %ld。
 取出完成。
 “%s”的总和检查值不符合：
  预期的总和检查值：%s
  实际的总和检查值：%s
 “%s”的总和检查值不一致：预期“%s”，实际：“%s” “%s”的总和检查值不符合；预期：“%s”，实际：“%s” “%s”的总和检查值不符合；记录：“%s”，实际：“%s” “%s”的总和检查值不符合：
  预期的总和检查值：%s
  实际的总和检查值：%s
 总和检查值不一致，表示文件参考基础损坏：“%s”
       预期： %s
       实际： %s
 校验和：%s
 用户凭证文件名： 客户端于剖析参数时发生错误 客户端/服务器版本不匹配 注释 (%i 行)：
%s
 注释(%i 行)：
%s

 注释 (%i 行)：
%s
 注释(%i 行)：
%s

 提交失败(细节见下)： 提交项目“%s”有复制标志，但是修订版无效 提交项目“%s”有复制标志，但是没有复制地址地址(copyfrom URL) 提交成功，但是发生了其它的错误，如后： 提交成功，但是发送提交外挂(post-commit)失败 于预计新增的目录中进行提交 配置错误：无效的布尔值“%s” 配置错误：无效的整数值“%s” 冲突之前的当前文件：%s
 冲突之前的文件：%s
 冲突之前的工作文件：%s
 冲突的属性文件：%s
 网络连线意想不到地关闭 复制自修订版：%ld
 复制自地址(URL)：%s
 已复制：%s (来自修订版 %ld，%s)
 当前不支持从可变的树复制 自事务进行复制是不被允许的 无效的数据库：先前的数目不完整 当前文件损坏 文件系统“%2$s”中“%1$s”的 'copies' 表格有无效的项目 文件系统“%2$s”中“%1$s”的 'transactions' 表格有无效的项目 在文件系统“%2$s”中的文件系统修订版 %1$ld 损坏 node-rev 中的 node-id 损坏 损坏的工作拷贝：目录“%s”没有默认的项目 损坏的工作拷贝：“%s”于目录“%s”中 (已加入新增调度) 与加入新增调度的不一致 损坏的工作拷贝：“%s”于目录“%s”中 (已加入删除调度) 与加入删除调度的不一致 损坏的工作拷贝：“%s”于目录“%s”中 (已加入替换调度) 有无效的调度 损坏的工作拷贝：“%s”于目录“%s”中有无效的调度 损坏的工作拷贝：目录“%s”有无效的调度 无法为“%s”创建 GET 请求 无法产生一个加入(PUT)请求 (%s) 无法建立请求 (%s“%s”) 无法创建最上层目录 不能获取版本资源地址(URL)(导入时需要或本地，缓存属性中缺少) 无法储存文件 无法储存版本资源的地址(URL) 无法使用外部编辑器取得日志信息；请尝试设定 $SVN_EDITOR 环境变数，或是使用 --message (-m) 或 --file (-F) 选项 无法写 svndiff 至临时文件 不能确定“%s”的绝对路径 无法进行属性合并 无法找到档案库 无法打开档案库 无法打开日志文件 建立纳入版本控制下的新目录。
用法：1、mkdir PATH...
      2、mkdir URL...

  建立版本控制的目录。

  1、每一个以工作拷贝 PATH 指定的目录，都会建立在本地端，并且加入新增
     调度，以待下一次的提交。

  2、每个以地址(URL)指定的目录，都会透过立即提交于档案库中建立。

  在这两个情况下，所有的中间目录都必须事先存在。
 正在创建 DAV sandbox 目录 正在创建 conf 目录 正在创建数据库锁文件 正在创建数据库日值锁文件 正在创建外挂目录 正在创建锁文件目录 正在创建 passwd 文件 正在创建 post-commit 外挂 正在创建 post-revprop-change 外挂 正在创建 pre-commit 外挂 正在创建 pre-revprop-change 外挂 正在创建 start-commit 外挂时发生错误 正在创建 svnserve.conf 文件 无法取得凭证数据 DAV 请求失败；可能是档案库的 pre-revprop-change 外挂执行失败；或是不存在 XML 数据不能正确地解码 默认项目没有修订版编号 默认项目没有地址(URL) 已删除 删除           %s
 Delta不能包含足够的新数据 Delta 不能填充目标窗口 Delta 属性块被检测到 - svndumpfilter 不支持 正在 deltify 修订版 %ld... 描述本程序或其子命令的用法。
用法：help [子命令...]
 描述本程序或其子命令的用法。
用法：svndumpfilter help [SUBCOMMAND...]
 目的“%s”已存在 目的“%s”不是目录 目的目录已存在，除非强迫为之，否则不会覆盖 目的目录已存在；请删除目录或用 --force 来覆盖目录 并未预期“%s”是工作拷贝的根目录 在“%s”请求Diff 的版本冲突 目录“%s”没有 THIS_DIR 项目 目录“%s”没有地址(URL) 目录条目损坏 目录必须为空的，但是结果并非如此 目录列表元素不是一个列表 关闭了的档案库功能 不显示过滤的统计数据。 不过滤修订版属性。 丢弃 %d 节点：
 丢弃 %d 修订版。

 导出流数据中出现畸形 在工作拷贝或档案库中复制数据，恢复历史。
用法：copy 起源 目的

  起源 和 目的 可以是工作拷贝路径或地址(URL)：
    工作拷贝  -> 工作拷贝  ：  复制和通过调度进行增加(包含历史)
    工作拷贝  -> 地址(URL) ：  马上提交一个工作拷贝到地址(URL)
    地址(URL) -> 工作拷贝  ：  签出地址(URL)到工作目录，通过调度进行增加
    地址(URL) -> 地址(URL) ：  完全服务器端复制；一般用于分支和标签
 尚不支持编辑非本地目标“%s”的属性 必须提供地址(URL)或纳入版本控制的项目 空的错误列表 结束修订版 %ld 无效 (最新的修订版为 %ld) 项目“%s”有不合法的调度 项目“%s”有无效的“%s”值 项目“%s”有无效的节点种类 项目“%s”没有地址(URL) 项目“%s”意想不到地变换特殊状态 项目“%s”已纳入版本控制 项目已存在 存在“%s”的项目 (不过工作文件已丢失) “%s”的项目没有地址(URL) “%s”项目被标记为“已复制”，但是本身尚未加入新增调度。也许您提交的
目标是位于未纳入版本控制 (或尚未纳入) 的目录中？ 项目有无效的属性 项目没有地址(URL) 项目没有修订版 取消报告时发生错误 post-commit 增加修订版时发生错误 (细节见下)： 呼叫外部程序时发生错误 检查“%s”是否存在时发生错误 关闭目录“%s”时发生错误 关闭文件系统发生错误 关闭空文件发生错误 关闭读标准错误管道末尾发生错误 关闭标准错误管道末尾发生错误 比较“%s”与“%s”时发生错误 进行“%s”的递归新增时发生错误 取得“%s”的 'affected time' 时发生错误 取得“%s”的 'affected time' 时发生错误 取得“%s”的“affected time”时发生错误 获取进程用户ID（UID）时发生错误 子程序错误：%s post-commit 清除时发生错误 (细节见下)： 修改“%s”的项目时发生错误 修改“%s”的项目时发生错误 打开数据库锁文件错误 打开本地文件时发生错误 剖析 %s 属性于“%s”发生错误：“%s” 分解“%s”时发生错误 在“%2$s”中处理命令“%1$s”时发生错误 于“%s”中读取管理日志文件时发生错误 读取请求 %s 回应的脱机数据时出现错误 替换“%s”的文件参考基础时发生错误 解析“%s”时发生错误 还原“%s”文字时发生错误 运行“%s”时发生错误：退出码是 %d，参数是：
在目录“%s”，名字：
%s
%s
%s 执行 post-commit 外挂时发生错误 请求 %s 的响应脱机保存到磁盘时出现错误 解除锁定目录时发生错误(细节见下)： 验证“%s”的服务器凭证时发生错误：
 为进程“%s”写入时发生错误 写入“%s”的项目文件时发生错误 写哈希数据至“%s”时发生错误 写入“%s”的日志文件时发生错误 为“%s”写入日志时发生错误 写至“%s”时发生错误 写入“%s”时发生错误：意外的 EOF 必须提供恰好一个的文件参数 排除 (以及丢弃空修订版) 的字首：
 排除字首：
 期待文件系统(FS)格式 “%d”；找到格式“%d” 过期：%s
 必须明确地提供目标 必须明确地提供目的 (“%s”解释为属性内容) 导出完成。
 导出修订版 %ld 的外部定义。
 导出修订版 %ld。
 于修订版 %ld 的外部定义。
 外部定义取出完成。
 外部定义导出完成。
 外部定义更新完成。
 无法新增文件“%s”：copyfrom 参数尚不支持 无法新增目录“%s”：同名物件已存在 无法新增目录“%s”：物件与管理目录同名 无法新增目录“%s”：同名物件已加入新增调度 无法新增文件“%s”：同名物件已存在 无法新增文件“%s”：同名物件已加入新增调度 删除mmap ”%s“失败 无法找到标记“%s”于地址(URL)“%s” 无法找到修订版 %ld 的时间 无法取得独占存取权限；可能是其他如 httpd，svnserve 或 svn 的进程正在打开它
并没有释放权限？ 取新的字符串关键字失败 无法从磁碟装载属性 无法将“%s”标记为不存在：同名项目已加入新增调度 无法恢复“%s”-- 请改用更新试试。
 运行外挂“%s”失败；无效的符号连接 载入外挂失败 文件“%s”已存在 文件“%s”有二进制的 mime 型别属性 文件“%s”内的换行符不一致 文件“%s”有本地的修改 文件“%s”于目录“%s”中的并不是纳入版本控制的资源 文件已存在：文件系统“%s”，修订版 %ld，路径“%s” 文件无法变更：文件系统“%s”，修订版 %ld，路径“%s” 文件找不到：修订版 %ld，路径“%s” 文件系统目录没有这样的项目 文件系统没有项目 文件系统没有这样的拷贝 文件系统没有这样的 node-rev-id 文件系统没有这样的表现 (representation) 文件系统没有这样的字串 文件系统已打开 文件系统损坏 文件系档未打开 文件系统对象尚未打开 文件系统中路径“%s”既不是文件也不是目录 自转存流移除符合指定字首的节点。
用法：svndumpfilter exclude PATH_PREFIX...
 自转存串流移除未包含指定字首的节点。
用法：svndumpfilter include PATH_PREFIX...
 修订版文件的最后一列大于 64 个字符长 修订版文件的最后一列少掉空白字符 “%s”的第一行包含了非阿拉伯数字 第一个修订版不可高于第二个 svn:externals 属性的格式是无效的 找到一个工作拷贝路径 在修订版文件中找到畸形的文件头 管道协议帧错误 对“%s”的 GET 请求失败 一般性文件系统错误 获得源路径，但”%s“没有源修订版 找不到 HTTP 路径 不合法的 svn 档案库地址(URL)“%s” 对请求的操作来说是不合法的目标 服务器回传的档案库根路径长得太夸张 在目录“%s”中 涵盖 (以及丢弃空修订版) 的字首：
 包含的字首：
 不相容的程序库版本 不完全的数据 不完整或未结束的属性块 不一致的行结束样式 使用不正确的参数 使用不正确的参数 索引 输出入错误 不支持不同档案库之间的重新安置 (relocation) 内部错误：在 MERGE 回应中，“DAV:response”元素有个未知的父项目 (%d) 无效属性 %s 于“%s”：目录牵涉到 '.' 或 '..' 或是绝对路径 无效地址(URL)“%s” 无效的地址(URL)：代理(proxy)通信端口编号中有不合法的字符 无效的地址(URL)：负的代理(proxy)通信端口数值 无效的地址(URL)：代理(proxy)通信端口数值大于最大值的 TCP 通信端口数值 65535 rev 文件中有无效的改变种类 无效的改变顺序：有新的节点修订版 ID 而没删除 无效的改变顺序：对删除路径进行非新增的改变 无效的变更类型 rev-file 中有无效的变更行 无效的配置：除错遮罩值中有不合法的字符 无效的配置：超时数值中有不合法的字符 无效的配置：负的超时数值 无效的配置：无法载入凭证文件“%s” 无效的配置值 无效的复制来源路径“%s” 无效的差异流：[new] insn %d 溢出新数据区域 无效的诧异流：[src] insn %d 溢出源视图 无效的差异流：[tgt] insn %d 起点超过目标视图位置 无效的差异流：insn %d 不能解码 无效的差异流：insn %d 出现非正的长度 无效的差异流：insn %d 溢出目标视图 无效的编辑器定位；至少一个输入路径不是目录并且没有源入口 无效的结束修订版 %ld 编辑时遇到无效的文件或目录 无效的文件系统路径语法 无效的文件系统修订版号 无效的文件系统事务名称 无效的锁定 这个操作对目前的工作拷贝目录无效 rev-file 中有无效的 prop-mod 标志 无效的最上层工作拷贝报告 提供了无效的版本号码 无效的计划任务 对 dated-rev 请求的无效服务器回应 无效的起始修订版 %ld 无效的目标路径 rev-file 中有无效的 text-mod 标志 该项目已存在于文件系统中 项目已过时 最后修改的作者：%s
 最后修改的时间 最后修改的修订版：%ld
 保留本地修改或未纳入版本管理的文件 一行以意料之外的方式结束 本地地址(URL)“%s”仅含主机名称，没有路径 本地地址(URL)“%s”包含未支持的主机名称 本地地址(URL)“%s”并未包含 'file://' 开头 本地操作，非提交的操作不能获取日志信息 区域条目不是一个列表 锁定注释(%i line)：
%s
 锁定注释(%i 行)：
%s
 锁定已创建的 锁定过期的 锁定所有者：%s
 锁定记号：%s
 锁定注释存在非法字符 锁失败：新的版本“%s”已经存在 锁定文件“%s”不是一般的文件 锁过期 锁定标记是不正确的 对目录“%s”的日志命令被错置 日志文件项目丢失 'name' 属性 (项目“%s”对应至目录“%s”) 日志条目不是一个列表 日志信息中有一个零字节 日志信息档是一个版本控制中的文件；请使用 '--force-log' 以跳过此限制 没有 MD5 指纹检查值 MIME 类别“%s”没包含 '/' MIME 类别“%s”以非字母与数字字符结束 MIME 类别“%s”含有空的媒体类别 档案库的地址(URL)畸形 畸形的XML：%s 在第 %d 行 node-rev 中的 copyfrom 行畸形 node-rev 中的 copyroot 行畸形 畸形的导出文件头 畸形的导出流：修订版 0 不能包含节点记录 畸形的错误列表 畸形的文件 畸形的网络数据 畸形的描述文件头 畸形的框架数据 畸形的流数据 请求的svndiff 数据畸形 node-rev 中没有 text rep offset 列 提交时发生合并冲突 不匹配的弹出工作拷贝(WC)展开堆栈 对“%s”有不符合的 FS 模块版本：找到 %d.%d.%d%s，预期 %d.%d.%d%s “%s”丢失了 'dest' 属性 “%s”丢失了 'left' 属性 “%s”丢失了 'revision' 属性 “%s”丢失了 'right' 属性 “%s”丢失 'timestamp' 属性 未提供“%s”于修订版 %ld 的 changed-path 信息 node-rev 中没有 cpath 丢失默认项目 node-rev 中没有 kind 栏位 node-rev 中没有 node-id 未提供属性名称与档案库路径参数 未提供属性名称或档案库路径参数 未提供档案库路径参数 未提供必须的节点修订版 ID 未提供请求的修订版规格 丢失修订版 已修改 访问本地磁盘的档案库模块。 使用svn网络协议访问档案库的模块。 通过WebDAV (DeltaV) 协议访问档案库的模块。 模块与伯克利数据库(Berkeley DB)档案库一起工作。 模块与纯文本文件(FSFS)档案库一起工作。 除非强行操作，否则不会执行移动操作 指定了互斥的参数 名称没有指向文件系统目录 名称没有指向文件系统文件 名称不是一个单一路径 文件名：%s
 名字：%s%s 不能接受的有效期传递到 svn_fs_lock Neon 不能分析地址(URL)”%s“ 网络连线意想不到地关闭 网络读写错误 网络 socket 启始失败 导入文件时，需要一个新的项目名称 不存在 '.' 项目：“%s” 未提供 auth 文件路径 无可用的认证提供者 “%2$s”的“%1$s”属性没有改变
 属性“%s”于修订版 %ld 没有改变
 没有找到公共父节点，不能在分离的参数上操作 文件系统“%2$s”中没有编号“%1$s”的拷贝 在目录“%s”中没有默认的项目 没有“%s”的项目 无法使用外部编辑器 路径“%s”的任何一部份在档案库 HEAD 中都找不到 没有这样的 XML 标签属性 没有这样的项目:“%s” 没有这个修订版 %ld 没有 '基础' 工作拷贝属性这样的东西 无此事务 不支持档案库 <--> 工作拷贝移动 在文件系统“%2$s”中不存在名为“%1$s”的事务 文件系统“%s”当前没有用户名关联 “%s”中没有写入锁定 节点种类：目录
 节点种类：文件
 节点种类：无
 节点种类：未知
 检测到非ASCII字符 (代码 %d)，并且不能转换到UTF-8(或从UTF-8转换) 给了非数字的参数 文件内容的一部份不是字串 文件差异的一部份不是字串 没有一个 SVN_EDITOR，VISUAL 或 EDITOR 环境变数被设定，执行时期的设定中也没有 'editor-cmd' 选项 并未提供全部所需的修订版 未授权打开根进行编辑操作 空的报告，偏移已经超过零值 数字大小超过上限 物件不是一个修订版的根物件 物件不是一个事务的根物件 塞住的更新 目前只支持路径的文件参考基础与其工作文件之间的文件差异 仅允许一个修订版 地址(URL)后只能指定相对路径 本操作不适用于二进制文件 本操作不适用于目录 选项 --tunnel-user 只有隧道模式时有效。
 过期：”%s“在事务”%s“ 输出指定文件或地址(URL)的内容，每行包含修订版和作者信息。

用法：blame 目标[@修订版]...

      如果指定了修订版，将从指定的修订版开始查找。
  
 输出指定文件或地址(URL)的内容。
用法：cat 目标[@修订版]...

      如果指定了修订版，将从指定的修订版开始查找。
  
 “%s”的密码： “%s”的密码： 路径“%s”已存在 路径“%s”不存在 路径“%s”不存在于修订版 %ld 路径“%s”以“%s”结束，本操作不支持 路径“%s”不是文件 路径“%s”于 HEAD 修订版中找不到 路径“%s”于修订版 %ld 中找不到 路径没有锁定标记 路径不是工作拷贝目录 路径不是工作拷贝文件 路径语法在此上下文中并不支持 路径：%s
 请将服务器升级至 0.19 或更新的版本 导出流中内容数据过早结束 操作于工作拷贝的第一个日志项目时发生问题 无法执行日志 进程“%s”失败(返回 %d) 进程“%s”返回错误退出码 %d Prop diffs 元素不是一个列表 属性最后更新 属性“%s”是项目属性 找不到属性 属性列表元素不是一个列表 通讯协定错误：对“%s”的 MERGE 回应并未返回所有我们请求的属性 (这些是完成提交操作所不可缺少的) 通讯协定错误：我们告知服务器不要自动合并任何资源，但是它报告“%s”已被合并 把文件和目录放入版本控制中，
通过调度加到档案库。它们会在下一次提交时加入。
用法：add 路径...
 Python 异常已被设定为错误 RA 层无法取得请求的 OPTIONS 信息 RA 层建立 HTTP 请求失败 RA 层无法取得属性 RA 层无法起始 socket 层 RA 层文件已存在 RA 层请求失败 报告响应处理失败不能完成编辑驱动 修 订 版   路径
--------   ----
 修 订 版   路径 <ID>
--------   ---------
 已用完不重复的名称 编辑的根目录读取拒绝 管道发生读取错误 正在读取“%s” 从标准输入设备读取数据时被中断，所以把它禁止 正在读取请求的一个 svndiff 窗口最末端 递归清理工作拷贝，去除锁，记录未完成操作，等等。

用法：cleanup [路径...]
 在文件系统“%2$s”中引用不存在的节点“%1$s” 在文件系统“%2$s”中引用不存在的修订版“%1$ld” 相对来源修订版 %ld 不存在于目前档案库之中 移除工作拷贝的目录或文件的 '冲突' 状态。
用法：resolved PATH...

  注意：本子命令不会依语法来解决冲突或是移除冲突标记；它只是移除冲突的
        相关文件，然后让 PATH 可以再次提交。
 从版本控制中删除文件和目录。
用法：1、delete 路径...
      2、delete 地址...

  1、每一个通过路径指定的项目会被进行调度删除，下次提交时被真正删除。
     文件和目录若未改变将马上从工作拷贝中删除。
     路径本身或包含不受版本控制或已修改项目，
     不会被删除，除非指定了 --force 选项。
    

  2、每一个地址(URL)指定的项目会被马上从档案库中删除，并提交。
    
 移除因过滤而产生的空修订版。 过滤后重编余下的修订版。 报告 “%s” 可变并且非全文本 报告内容太大：得到 %s，限制是 %s 替换           %s
 导入时必须提供档案库的地址(URL) 档案库 UUID：%s
 本操作需要对档案库进行存取 该档案库存取方法未实作 需有档案库参数 档案库创建失败 档案库没有 UUID 档案库未激活接受修订版属性改变；请管理员创建一个 pre-revprop-change 外挂 已取得档案库锁定。
请稍候；修复档案库也许得花费一些时间...
 “%s”的已解决的冲突状态
 恢复原始未改变的工作拷贝文件 (恢复大部份的本地修改)。
用法：revert PATH...

  注意：本子命令不会存取网络，并且会解除冲突的状况。但是它不会恢复
        被删除的目录
 已还原“%s”
 已恢复“%s”
 修订版 %ld 提交为 %ld。
 修订版 %ld 不符合现有的修订版 %ld 于“%s”中 跳过修订版 %ld。
 修订版动作 '%c' 对修订版 %ld 于“%s”缺少前一个修订版 修订版条目不是一个列表 修订版文件缺少结尾换行符 修订版属性“%s”在此上下文中并不允许 不允许的修订版范围 修订版：%ld
 修订版被重新编号，如下：
 不支持 SSL 调度：新增
 调度：删除
 调度：正常
 调度：替换
 需要第二个修订版 正在发送       %s
 服务器没有返回档案库根路径 服务器不支持基于日期的操作 服务器最低要求的版号为 %d “%2$s”的“%1$s”属性被设为新值
 属性“%s”于修订版 %ld 设为新值
 跳过“%s”
 跳过找不到的目标：“%s”
 跳过二进制文件：“%s”
 日志信息的内容有不妥的地方 源“%s”不是目录 来源地址(URL)“%s”来自其它的档案库；把它当作是脱离的工作拷贝(WC) 来源与目的似乎不在同一个档案库中 (来源：“%s”；目的：“%s”) 来源地址(URL)“%s”来自其它的档案库 指定的代码封装服务器错误以便报告客户端 起始修订版 %ld 比结束修订版号 %ld 为大 起始修订版必须大于结束修订版 状态于修订版：%6ld
 不允许通过档案库接口存储非正规的属性“%s”，并且您的客户端有漏洞 字串并未表示一个节点或 node-rev-id 字串长度超出上限 子命令“%s”不接受选项“%s”
请输入“svn help %s”以取得用法。
 必须提供子命令参数
 Subversion 是个版本控制系统的工具。
欲取得详细资料，请参考 http://subversion.tigris.org/
 成功修改状态返回太快 子块大小总数大于块内容长度总数 Svndiff 数据包含无效窗口 Svndiff 数据包含向后变化的资源视图 Svndiff 数据包含无效窗口 svndiff 数据包含了无效的指令 svndiff 数据意想不到地结束 svndiff 数据包含了无效的档头 Svndiff 数据包含向后变化的资源视图 Svndiff 数据包含了无效的数据头 此平台不支持符号链接 原生换列符号参数“%s”中有语法错误 修订版参数“%s”中有语法错误 剖析修订版“%s”时发生语法错误 差异比较目标不可同时包含工作拷贝路径与地址(URL) 测试失败 文本最后更新 文字差异区段并非字串 %s 请求的回应返回无效的 XML：%s (%s) '--transaction' (-t) 与 '--revision' (-r) 参数不能同时存在 签出(CHECKOUT)的回应没有包含一个 'Location:' 的头信息 MERGE 属性回应包含错误状态 OPTIONS 回应并不包含请求的 activity-collection-set；通常这表示该地址(URL)并不支持 WebDAV 无法在本资源或任何其父项目中找到 UUID 属性 本资源找不到 VCC 属性 file-revs 报告中没有任何修订版 可使用以下的档案库存取 (RA) 模块：

 get-file-revs 命令没有返回任何修订版 最新的档案库修订版为 %ld。
 “%s”的位置对应至修订版 %ld 并不存在于档案库之中，或是指向一个无关的物件 日志信息文件于版本控制之下 日志信息是路径名 日志信息是一个路径名称 (-F 是故意的吗？)；请使用 '--force-log' 以跳过此限制 操作被中断 路径不是档案库的一部份 本资源上找不到相对路径属性 于“%s”的档案库有 uuid “%s”，但是工作拷贝的是“%s” 档案库被锁定，也许正在作数据库修复 指定的 diff 选项不支持 指定的事务不可改变 已有纳入版本控制项目“%s” 本客户端已过时，无法存取工作拷贝“%s”；请取得更新版的 Subversion 客户端 This is an empty revision for padding. 导入命令有太多参数 太多嵌套的项目 事务“%s”并非根基于某个修订版；多神秘啊 事务“%s”被移除。
 事务已结束 事务未结束 事务已过时 事务过时 传输文件数据 尝试对未纳入版本控制的资源进行版本控制操作 尝试变更一个不可改变的表现 (representation) 尝试用 “svn add”或 “svn add --non-recursive”代替？ 尝试添加新属性“%s”的值为“%s”，
但是属性已经存在，值为“%s”。 尝试把属性“%s”的值从“%s”改变为“%s”，
但是属性的值已经是“%s”了。 尝试把属性“%s”的值从“%s”改变为“%s”，
但是属性并不存在。 尝试要使用不支持的功能 隧道规则 %s 需要设定环境变数 %s 两个最上层报告没有目标 两个纳入版本控制的资源没有相关性 请使用“%s help”以得到用法和帮助。
 请使用“%s help”以得到用法和帮助。
 地址(URL)“%s”包含 '..' 元素 地址(URL)“%s”不存在 地址(URL)“%s”不符合现有的地址(URL)“%s”于“%s”中 地址(URL)“%s”不是正确地以 URI 编码 地址(URL)“%s”并不存在于该修订版中 地址(URL)“%s”指向一个目录 地址(URL)“%s”指向一个文件，而非目录 地址(URL)：%s
 UUID 标识：%s
 不能从回应的头信息中获取数据 无法找到“%s”的档案库位置于修订版 %ld 无法定位认证文件文件 不能跨越多个档案库进行 锁定/解锁 无法建立任何目录 不能使用名字“%s” 无法打开地址(URL)的 ra_local 会话 不能打开认证文件读取数据 无法打开认证文件写入数据 无法打开档案库“%s” 无法打开编辑 无法辨识的隧道规则“%s” 流中有意料之外的结束符 写内容时有意料之外的结束符 找到一个意料之外的节点种类 找到一个意料之外的节点种类 意想不到或未知的属性种类 意想不到的认证时服务器回应 意想不到地找到了“%s”：路径已被标记为 '丢失' 未知的 FS 类别 未知的文件系统(FS)类别“%s” 未知的 XML 编码：“%s” 未知的授权方法 未知的命令：“%s” 未知的命令：“%s”
 未知的项目类型“%s” 未知的主机名称“%s” “%s”为未知的节点类型 路径“%s”有未知或意想不到的种类 类型“%s”未知或不可版本控制 命令回应中的未知状态“%s” 未知的 svn 通讯协定命令 未知的 svn_node_kind 该项目有一个无法合并的调度请求 无法辨识的地址(URL)规则(一般需要svn://，http://，file://等开头指明使用的规则)“%s” 无法辨识的二进制数据编码：无法解译 无法辨识的行结束样式 无法确认的日志文件元素“%s”于“%s”中 无法辨识来自服务器的节点种类“%s” 无法确认的节点种类：“%s” “%s”的节点种动作法辨识 流中有无法辨识的记录类型 无法辨识请求的“%s”的修订版类别 无法辨识的流数据 不支持的 bdb 的 FS 载入器版本 (%d) 不支持的 fsfs 的 FS 载入器版本 (%d) 不支持的 RA 外挂 ABI 版本 不支持的导出文件版本：%d 路径“%s”为不支持的节点种类 不支持的档案库版本 不支持的工作拷贝格式 修订版 %ld 上的未纳入版本控制的属性：
 更新完成。
 更新外部定义至修订版 %ld。
 更新至修订版 %ld。
 请用 --force 来撤销限制 用户 %s 不是路径“%s”锁的所有者(当前由用户 %s 锁定) 用户名和锁定所有者不匹配 用户登录名： 有效 UTF-8 数据
(16进制：%s)
后接无效 UTF-8 序列
(16进制：%s) 除非强迫为之，否则其值不会被设定 版本 %d 未被确认 版本文件格式不正确 “%s”的版本不一致：找到 %d.%d.%d%s，预期 %d.%d.%d%s 警告：修订版 %ld 的参考数据比最旧的转存数据修订版 (%ld)还旧。
警告： 装载这个转存到空的档案库会失败。

 等待档案库锁定中；也许其它的进程把它打开了？
 当指定工作拷贝路径，只有一个目标会被指定 无法删除本地修改的目录“%s” 工作拷贝“%s”被锁定了 “%s”的工作拷贝格式已过时(%d)；请重新取出您的工作拷贝 工作拷贝已损坏 工作拷贝未更新至最新版 工作目录没有被锁定；这可能是一个漏洞，请报告开发者 工作拷贝路径“%s”不存在档案库中 工作拷贝的文件参考基础已损坏 写入错误 “%s”中的写入锁定被偷走了 XML 的数据不合语法 XML 剖析器无法处理“%s” 您的 .svn/tmp 目录可能丢失或损坏；请执行“svn cleanup”，然后再试一次 您的提交信息保留在临时文件中： 正在分配新拷贝ID （正在获取'next-key'） 正在分配新的节点 ID （正在获取 'next-key'） 正在分配新请求 （正在获取 next-key） 应用日志信息至“%s” 授权失败 详细 正在提取下一个拷贝关键字 正在提取下一个节点 ID 关键字 跳过档案库外挂系统 提交修订版后调用 post-commit 外挂 提交修订版签调用 pre-commit 外挂 正在关闭更改指针 无法连接到服务器 正在创建更改 正在为读取一个字符串创建指针 正在为读取更改创建指针 后台模式 正在删除更改 正在从 'copies' 表中删除条目 正在从 'nodes' 表删除条目 向下递归 关闭日志文件自动移除 [Berkeley DB] 使自动属性无效 关闭事务提交的 fsync [Berkeley DB] 显示这个帮助 显示更新信息 忽略默认值和 svn:ignore 属性 不要交互提示 不要缓冲用户验证 查看历史不要跨越不同的拷贝 不输出其后的换行符 不要打印已删除文件的不同处 不要锁定目标 完成。
 以差异增量进行转存 使自动属性有效 已导出%s 正在获取更改 强制操作运行 强制校验日志信息资源 给予适合输出的 合并时忽略原始信息 忽略所有流中的档案库 UUID 忽略外部定义 inetd 模式 最近一次改变的，而不是目前的修订版 监听主机名或IP地址（后台模式） 监听一次（调试时有用） 监听端口（后台模式） 于档案库指定的目录进行载入 日值项最大值 next-id 文件损坏 不显示进度 (仅错误) 至标准错误输出 比较差异时提示原始信息 在修订版属性上作操作(使用-r参数) 只在单个目录操作 输出为 XML 传递文件 ARG 内容为附件参数 路径“%s”不存在于修订版 %ld 中 打印尽可能少 显示差异并对照原始内容 打印附加信息 属性“%s”自“%s”删除 (递归)。
 属性“%s”自“%s”删除。
 属性“%s”自档案库修订版 %ld 中删除
 属性“%s”设定 (递归) 于“%s”
 属性“%s”设定于“%s”
 设定属性“%s”于档案库修订版 %ld
 从目录 ARG 读取用户配置文件 通过URL改写重新部署 服务根目录 在前台运行（调试时有用） 如果有的话，以流中的数值来设定档案库的 UUID 显示完整路径代替缩略形式 显示子命令帮助 为每个路径显示节点修订版编号 指定密码 ARG 指定用户 ARG 指定修订版编号 ARG 指定修订版编号 ARG (或 X:Y 范围) 指定事务名称 ARG 子命令“%s”不接受选项“%s”
请使用“svnadmin help %s”以了解用法。
 必须提供子命令参数
 svn：请执行“svn cleanup”以移去锁定 (输入“svn help cleanup”以取得详细说明)
 svndiff 数据请求不存在的来源 svndiff 请求的位置超出了流的结尾 svndiff 的窗格长度已毁损 系统(“%s”) 返回 %d 等待服务器超时 指定的值 ARG 为字符编码 尝试操作但不修改 隧道模式 隧道用户名（模式是当前用户UID的名字） 未知 用法：svnadmin create REPOS_PATH

于 REPOS_PATH 建立一个新的，空的 Subversion 档案库。
 用法：svnadmin deltify [-r LOWER[:UPPER]] REPOS_PATH

在指定的修订版范围中，对其中变动的路径作 deltification。藉由仅储存
与前一修订版的差异，deltification 本质上可压缩档案库。如果没有指定
修订版的话，则直接对 HEAD 修订版进行。
 用法：svnadmin dump REPOS_PATH [-r LOWER[:UPPER]] [--incremental]

将文件系统的内容，以一种可携式“转存”格式输出到标准输出，并将信息
报告输出到标准错误。将 LOWER 与 UPPER 之间修订版内容转存出来。如果没
有指定修订版的话，转存所有的修订版树。如果只有指定 LOWER 的话，只转存
一个修订版树。如果使用了 --incremental 选项，那么第一个转存的修订版会
是与前一个修订版的差异，而非平常的全文输出。
 用法：svnadmin help [SUBCOMMAND...]

显示本程序或其子命令的用法。
 用法：svnadmin hotcopy REPOS_PATH NEW_REPOS_PATH

产生档案库的即时拷贝。
 用法：svnadmin list-dblogs REPOS_PATH

列出所有的 Berkeley DB 日志文件。

警告：修改或删除仍在使用中的记录文件将导致档案库损坏。
 用法：svnadmin list-unused-dblogs REPOS_PATH

列出无用的 Berkeley DB 日志文件。

 用法：svnadmin load REPOS_PATH

从标准输入读取“转存”格式的流，将新的修订版提交至档案库的文件
系统中。如果档案库原先是空的，默认会将其 UUID 以流中的数值代之。
进度报告会送至标准输出。
 用法：svnadmin lstxns REPOS_PATH

显示所有未处理事务的名称。
 用法：svnadmin recover REPOS_PATH

对档案库进行 Berkeley DB 修复程序。如果您遇到要求进行修复的
错误信息，请执行本命令。

警告：只有在您绝对确定您是唯一存取档案库的人时，才执行本命令。
本命令必须有独占的存取权限，如果档案库被其它进程所占用，它会马
上结束。

 用法：svnadmin rmlocks REPOS_PATH LOCKED_PATH...

无条件地从每一个 LOCKED_PATH 删除锁。
 用法：svnadmin rmtxns REPOS_PATH TXN_NAME...

从档案库删除命名事务。
 用法：svnadmin setlog REPOS_PATH -r REVISION FILE

将修订版 REVISION 的日志信息设定为 FILE 的内容。使用 --bypass-hooks
以回避修订版属性相关的挂勾 (举个例子，像是您不想要 post-revprop-change
挂勾寄出通知邮件，或是修订版属性修改并未在 pre-revprop-change 挂勾中
启动)。

注意：修订版属性并未纳入版本控管，所以这个命令会永远盖写掉先前的
日志信息。
 用法：svnadmin verify REPOS_PATH

校验储存于档案库的数据。
 用法：svnlook author REPOS_PATH

显示作者。
 用法：svnlook cat REPOS_PATH FILE_PATH

显示文件的内容。FILE_PATH 前的 '/' 可省略。
 用法：svnlook changed REPOS_PATH

显示已改变的路径。
 用法：svnlook date REPOS_PATH

显示日期。
 用法：svnlook diff REPOS_PATH

以 GNU 样式，显示档案库中改变的文件与属性差异。
 用法：svnlook dirs-changed REPOS_PATH

显示本身曾改变 (或属性编辑) 过的目录，或是其下的文件更曾动过目录。
 用法：svnlook help [SUBCOMMAND...]

显示本程序或其子命令的用法。
 用法：svnlook history REPOS_PATH [PATH_IN_REPOS]

显示档案库中，某个路径的历史记录信息 (如果没有指定路径，则为根目录)。
 用法：svnlook info REPOS_PATH

显示作者，日期戳，日志信息大小，以及日志信息。
 用法：svnlook log REPOS_PATH

显示日志信息。
 用法：svnlook tree REPOS_PATH [PATH_IN_REPOS]

显示目录树，自 PATH_IN_REPOS 开始 (如果有提供的话，不然就从目录树的
根目录开始)，可选择性地显示节点修订版 id。
 用法：svnlook uuid REPOS_PATH

显示档案库的 UUID。
 用法：svnlook youngest REPOS_PATH

显示最新的修订版号。
 使用 ARG 作为差异比较命令 使用 ARG 作为外部编辑器 使用 ARG 作为合并命令 使用 ARG 作为新目标 使用 ARG 作为旧目标 于转存输出中使用文件差异 使用严格的语法 使用线程代替进程 如果档案库被其他进程占用，等待而不直接退出
   