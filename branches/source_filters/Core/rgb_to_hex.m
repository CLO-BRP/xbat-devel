function hex = rgb_to_hex(rgb)

% rgb_to_hex - convert RGB matrix to hex strings
% ----------------------------------------------
%
% hex = rgb_to_hex(rgb)
%
% Input:
% ------
%  rgb - RGB matrix
%
% Output:
% -------
%  hex - cell array of hexadecimal color strings
%
% Example:
% --------
%  hex = rgb_to_hex(jet)

%------------------
% HANDLE INPUT
%------------------

% TODO: factor to and extend in 'is_rgb' function

%--
% check input size
%--

[r, c] = size(rgb);

if c ~= 3
	error('RGB matrix must have three columns');
end

%--
% check input range
%--

[a, b] = fast_min_max(rgb);

if (a < 0) || (b > 1)
	error('RGB matrix must have values between zero and one.'); 
end

%------------------
% GET HEX STRINGS
%------------------

%--
% convert interval values to 8-bit integers
%--

rgb = round(255 * rgb);

%--
% perform string conversion
%--

hex = cellstr([dec2hex(rgb(:, 1)), dec2hex(rgb(:, 2)), dec2hex(rgb(:, 3))]);

