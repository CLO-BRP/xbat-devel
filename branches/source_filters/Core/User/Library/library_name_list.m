function names = library_name_list(user, mode)

% library_name_list - get list of library names for a given user
% --------------------------------------------------------------
%
% names = library_name_list(user, mode)
%
% Input:
% ------
%  user - user
%  mode - {'display', 'full'}
%
% Output:
% -------
%  names - library names

if nargin < 2 || isempty(mode)
	mode = 'display';
end

if nargin < 1 || isempty(user)
	user = get_active_user;
end

%--
% handle multiple inputs recursively
%--

if length(user) > 1
	
	names = {};
	
	for j = 1:length(user)
		list = library_name_list(user(j), 'own');
		names = {names{:}, list{:}};
	end
	
	return;
	
end
		
%--
% get user libraries
%--

library = get_libraries(user);

if isempty(library)
	names = {'(No Libraries)'};
end

%--
% build library name using author and library names
%--

names = cell(0);

for j = 1:length(library)
	
	switch (mode)
		
		case ('display'), names{end + 1} = get_library_name(library(j), user);
			
		case ('full'), names{end + 1} = get_library_name(library(j));
			
		case ('own')	
			
			if ~strcmpi(library(j).author, user.name)
				continue;
			end
			
			names{end + 1} = get_library_name(library(j));
			
	end
	
end

%--
% put default on top
%--

ix = find(strcmp('Default', names));

if ~isempty(ix)
	names(ix) = []; names = {'Default', names{:}};
end

