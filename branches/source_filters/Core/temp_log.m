function log = temp_log(sound)

% detector_log - create temporary log
% -----------------------------------
%
% log = temp_log(sound)
%
% Input:
% ------
%  sound - log sound
%
% Output:
% -------
%  log - temporary log structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1174 $
% $Date: 2005-07-13 16:54:00 -0400 (Wed, 13 Jul 2005) $
%--------------------------------

% NOTE: the file is deleted after creation, log can be saved if needed

%--
% create log using temporary file name
%--

log = log_create([tempname '.mat']);

%--
% set log sound
%--

if (nargin)
	log.sound = sound; 
end

%--
% set display options for temporary log
%--

log.linestyle = ':';

log.linewidth = 0.5;

log.color = [1 0 0];

log.autosave = 0;

log.autobackup = 0;

%--
% delete log file
%--

delete([log.path, log.file]);