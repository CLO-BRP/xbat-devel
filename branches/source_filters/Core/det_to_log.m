function log = det_to_log(det,f)

% det_to_log - convert detection structure to log structure
% ---------------------------------------------------------
%
% log = det_to_log(det,f)
%
% Input:
% ------
%  det - detection structure
%  f - sound structure or sound type, 'file' or 'group'
%
% Output:
% -------
%  log - log structure

%--
% create log
%--

log = log_create;

%--
% add sound to log
%--

if (isstruct(f))
	log.sound = f;
else
	log.sound = sound_create(f);
end

%--
% fill log with detection data
%--

rate = log.sound.samplerate;
dt = 1134 / rate;

for k = 1:length(det)
	
	event = event_create;
	event.channel = det(k).channel;
	event.time(1) = det(k).sample / rate;
	event.time(2) = event.time(1) + dt;
	event.duration = dt;
	event.freq = [0,4500];
	event.bandwidth = 4500;
	
	event.notes = num2str(det(k).value);
	
	log.event(k) = event;
	
end

log.length = length(det);

%--
% save log
%--

log_save(log);