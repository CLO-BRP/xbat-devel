function [lib, cancel] = migrate_libraries(source, user)

% migrate_libraries - migrate user libraries
% ------------------------------------------
% 
% lib = migrate_libraries(source, user)
%
% Input:
% ------
%  source - source libraries root
%  user - user requesting library migration
%
% Output:
% -------
%  lib - migrated libraries

%--
% get active user if needed
%--

if nargin < 2 || isempty(user)
	user = get_active_user;
end

%--
% pack into cell
%--

if ischar(source)
	source = {source};
end

names = dir_name(source);

%--
% migrate libraries
%--

lib = {};

migrate_wait('Libraries', length(source), names{1});

for k = 1:length(source)
	
	[lib{end + 1}, cancel] = migrate_library(source{k}, user); 
	
	if cancel
		break;
	end
	
end

lib = [lib{:}];