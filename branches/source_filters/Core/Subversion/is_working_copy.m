function value = is_working_copy(root)

% is_working_copy - check whether a directory is a working copy
% -------------------------------------------------------------
%
% value = is_working_copy(root)
%
% Input:
% ------
%  root - root directory
%
% Output:
% -------
%  value - result of is working copy test

[ignore, exported] = svn_version(root);

value = ~exported;