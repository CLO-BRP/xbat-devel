function out = accordion(n)

%--
% set default number of iterations
%--

if ~nargin
	n = 100;
end

%--
% create large accordion struct
%--

str = 'uncopyrightable';

for k = 1:length(str)
	
	% NOTE: this adds dots between the letters
	
	part = str(k:end); part(2, :) = '.'; part(end) = []; part = part(:)';
	
	eval(['out.', strrep(part, '.', '__'), ' = ', int2str(k), ';']);

end

out = collapse(out);

%--
% flatten and collapse repeatedly
%--

for k = 1:n
	
	flat = flatten(out);
	
	out = collapse(flat);
	
end