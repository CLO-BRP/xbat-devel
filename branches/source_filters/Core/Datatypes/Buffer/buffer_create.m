function buf = buffer_create(n,content)

% buffer_create - create a circular buffer of entries
% ---------------------------------------------------
%
% buf = buffer_create(n,content)
%
% Input:
% ------
%  n - length of buffer
%  content - initial content
%
% Output:
% -------
%  buf - buffer

% a buffer has a length a current index and content

buf.length = n; 

buf.index = 1;

buf.content = cell(n,1);

buf.updated = zeros(n,1);

% TODO: extend to handle a variable number of input content

% add content if needed

if (nargin > 1) 
	buf = buffer_add(buf,content);
end