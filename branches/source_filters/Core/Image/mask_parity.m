function Z = mask_parity(m,p)

% mask_parity - create parity masks
% ---------------------------------
%
% Z = mask_parity(m,p)
%
% Input:
% ------
%  m - size of mask
%  p - parity of rows and columns
%
%   -1 - all
%    0 - even
%    1 - odd
%
% Output:
% -------
%  Z - parity mask

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 335 $
% $Date: 2004-12-21 19:10:44 -0500 (Tue, 21 Dec 2004) $
%--------------------------------

%--
% create mask according to type
%--

if (all(p > -1))

	% set type parameter

	t = p(1) + 2*p(2);

	Z = mask_parity_(m,t);	
	
elseif ((p(1) == -1) && (p(2) > -1))

	% set type parameter

	t = 4;
	if (~p(2))
		t = 5;
	end
		
	Z = mask_parity_(m,t);	
	
elseif ((p(1) > -1) && (p(2) == -1))

	% set type parameter

	t = 6;
	if (~p(1))
		t = 7;
	end
		
	Z = mask_parity_(m,t);	
	
else

	disp(' ');
	error('No mask is needed to select all rows and columns.');

end

		
