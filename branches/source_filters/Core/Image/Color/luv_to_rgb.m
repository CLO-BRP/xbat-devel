function Y = luv_to_rgb(X)

% luv_to_rgb - luv to rgb conversion
% ----------------------------------
%
% Y = luv_to_rgb(X)
%
% Input:
% ------
%  X - luv image
%
% Output:
% -------
%  Y - rgb image

%--
% check size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% compute conversion
%--

Y = xyz_to_rgb(luv_to_xyz(X));
