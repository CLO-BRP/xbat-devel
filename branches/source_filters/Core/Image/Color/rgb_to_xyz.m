function Y = rgb_to_xyz(X,A)

% rgb_to_xyz - rgb to xyz conversion
% ----------------------------------
%
% Y = rgb_to_xyz(X,A)
%
% Input:
% ------
%  X - rgb image
%  A - colorspace conversion matrix
%
% Output:
% -------
%  Y - xyz image

%--
% check size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% set colorspace transformation matrix
%--

if (nargin < 2)
	A = [ ...
		0.393  0.365  0.192; ...
		0.212  0.701  0.087; ...
		0.019  0.112  0.958 ...
	];
end

% scale the colorspace transformation matrix

A = A ./ 255;

%--
% compute the conversion
%--

Y = double(X);
Y = rgb_reshape(rgb_vec(Y)*A,m,n);