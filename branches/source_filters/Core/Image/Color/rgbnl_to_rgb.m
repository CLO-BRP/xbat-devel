function Y = rgbnl_to_rgb(X)

% rgbnl_to_rgb - nonlinear rgb to rgb image
% -----------------------------------------
%
% Y = rgbnl_to_rgb(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - nonlinear rgb image

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	error('Input image is not a multiple channel image.');
end

%--
% compute conversion
%--

Y = X;

Y(:,:,1) = (Y(:,:,1) * (1/255)).^(2.5) * 255;
Y(:,:,2) = (Y(:,:,2) * (1/255)).^(2.5) * 255;
Y(:,:,3) = (Y(:,:,3) * (1/255)).^(2.5) * 255;
