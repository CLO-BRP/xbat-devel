function Y = xyz_to_rgb(X,A)

% xyz_to_rgb - xyz to rgb conversion
% ----------------------------------
%
% Y = xyz_to_rgb(X,A)
%
% Input:
% ------
%  X - rgb image
%  A - colorspace conversion matrix
%
% Output:
% -------
%  Y - xyz image

%--
% check size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% set the colorspace tranformation matrix
%--

if (nargin < 2)
	A = [ ...
		0.393  0.365  0.192; ...
		0.212  0.701  0.087; ...
		0.019  0.112  0.958 ...
	];
end

% scale colorspace transformation matrix

A = inv(A) .* 255;

%--
% compute conversion
%--

Y = rgb_reshape(rgb_vec(X)*A,m,n);