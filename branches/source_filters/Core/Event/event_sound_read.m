function event = event_sound_read(event, sound)

% event_sound_read - read event samples from sound
% ------------------------------------------------
%
% event = event_sound_read(event, sound)
%
% Input:
% ------
%  event - event to read
%  sound - sound to read from
%
% Output:
% -------
%  event - event with samples and rate

% NOTE: this function is a start, there are various possible refinements

%-------------------
% HANDLE INPUT
%-------------------

if length(event) > 1
	
	% NOTE: the single event function catches exceptions
	
	for k = 1:length(event) 
		event(k) = event_sound_read(event(k), sound);
	end
	
	return; 
	
end

%-------------------
% SETUP
%-------------------

MAX_SAMPLES = 10^7;

%-------------------
% READ EVENT
%-------------------

%--
% append fields to event
%--

% NOTE: consider adding these fields to the basic event

event.samples = []; event.rate = get_sound_rate(sound);

%--
% check that event is of reasonable size
%--

if (event.rate * event.duration) > MAX_SAMPLES
	return;
end

%--
% read page samples and add to page, also add sample rate
%--

try
	event.samples = sound_read(sound, 'time', event.time(1), event.duration, event.channel);
catch
	nice_catch(lasterror, 'Failed to read event samples.');
end
