function C = channel_matrix(n)

% channel_matrix - create channel display matrix
% ----------------------------------------------
%
% C = channel_matrix(n)
%
% Input:
% ------
%  n - number of channels
%
% Output:
% -------
%  C - channel display matrix

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

C = [(1:n)', ones(n,1)];