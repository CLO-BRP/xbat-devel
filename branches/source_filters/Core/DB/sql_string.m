function str = sql_string(lines) 

% sql_string - get nice string from lines
% ---------------------------------------
%
% str = sql_string(lines)
%
% Input:
% ------
%  lines - sql lines
%
% Output:
% -------
%  str - sql string

if ischar(lines)
	str = lines;
else
	str = [lines{:}];
end

% NOTE: we just mess with some spacing to make the string nice

str = strrep(strrep(str, '  ', ' '), '( ', '(');