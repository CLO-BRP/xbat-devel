function build

% build - build mex files in a 'MEX' directory
% --------------------------------------------
% 
% Move to a 'MEX' directory and call build to build contents

% NOTE: this is very paranoid, consider something smarter and faster

clear functions;

%--
% check we are in MEX directory
%--

here = pwd;

if ~strncmp(fliplr(here), 'XEM', 3)
	disp(' ');
	disp('This is not a ''MEX'' directory, there is nothing to build!'); 
	disp(' ');
	return;
end

%--
% build using build scipt
%--

private = fullfile(here, 'private', 'build.m');

if exist_dir(private)
	
	cd(private); build; cd(here); return;

end

%--
% build generically
%--

content = dir; names = {content.name};

if isempty(names)
	return;
end

disp(' ');

for ix = 1:length(names)
	
	if any(strfind(names{ix}, '_mex.c')) || any(strfind(names{ix}, '_.c'))
		disp(['Building ''' names{ix} ''' ...']); build_mex(names{ix});
	end
	
end	 

disp(' ');
