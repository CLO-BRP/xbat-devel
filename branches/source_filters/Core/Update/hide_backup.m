function out = hide_backup(state)

% hide_backup - set hiding of backup directories in libraries
% -----------------------------------------------------------
%
% out = hide_backup(state)
%
% Input:
% ------
%  state - hiding state 'on' or 'off'
%
% Output:
% -------
%  out - directories affected by state change

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%------------------------------------
% HANDLE INPUT
%------------------------------------

if (nargin < 1)
	state = 'on';
end

%------------------------------------
% SCAN LIBRARY DIRECTORIES
%------------------------------------

%--
% get libraries
%--

libs = get_unique_libraries; 

%--
% scan libraries updating backup directories
%--

for k = 1:length(libs)
	p{k} = scan_dir(libs(k).path,{@hide_update,state},1);
end

%--
% remove empty cells and concatenate directories
%--

out = cell(0);

for k = 1:length(libs)
	ix = cellfun('isempty',p{k}); p{k}(ix) = []; out = {out{:}, p{k}{:}};
end

% for k = 1:length(out)
% 	disp(out{k});
% end


%------------------------------------
% HIDE_UPDATE (SCAN CALLBACK)
%------------------------------------

function p = hide_update(p,state)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%--
% get name of leaf directory
%--

[par,leaf] = path_parts(p);

if (~strcmpi(leaf,'__BACKUP'))
	p = ''; return;
end

%--
% update hidden state of backup directories
%--

switch (state)
	
	case ('on')
		fileattrib(p,'+h');
		
	case ('off')
		fileattrib(p,'-h');
		
end