function H = jitter_graph(G,d)

% jitter_graph - jitter graph positions
% -------------------------------------
% 
% H = jitter_graph(G)
%
% Input:
% ------
%  G - input graph
%  d - distortion parameter (def: 0.1)
%
% Output:
% -------
%  H - graph with jittered positions
%

%--
% set distortion paramerter
%--

if (nargin < 2)
	d = 0.1;
end

%--
% get positions
%--

X = G.X;

%--
% distort positions
%--

X = X + d*(rand(size(X)) - 0.5);

%--
% set positions
%--

G.X = X;

H = G;
