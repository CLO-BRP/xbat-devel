function z = circle(n,r)

% circle - compute positions on circle
% ------------------------------------
% 
% z = circle(n,r)
%
% Input:
% ------
%  n - number of positions
%  r - radius
%
% Output:
% -------
%  z - complex points on circle

%--
% compute angle sequence
%--

t = linspace(0,2*pi,n + 1); 
t = t(1:n)';

%--
% compute points
%--

z = r*exp(i*t);
