function g = set_waitbar_image(ax,color,value)

% set_waitbar_image - set image in waitbar axes
% ---------------------------------------------
%
% g = set_waitbar_image(ax,color)
%
% Input:
% ------
%
% Output:
% -------
%  g - handle to image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1000 $
% $Date: 2005-05-03 19:36:26 -0400 (Tue, 03 May 2005) $
%--------------------------------

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set empty value and color
%--

if (nargin < 3)
	value = [];
end

if (nargin < 2)
	color = []; 
end

%--------------------------------------------
% SET WAITBAR IMAGE
%--------------------------------------------

%--
% create image if needed
%--

% NOTE: this also sets the color

g = findall(ax,'type','image');

if (isempty(g))
	
	if (isempty(color))
		error(['Color must be provided when creating a waitbar.']);
	end
	
	%--
	% create image and set initial properties
	%--
		
	g = image('parent',ax);
		
	set(g, ...
		'cdata',create_gradient(color), ...
		'xdata',[0,1], ...
		'ydata',[0,1], ...
		'handlevisibility','off', ...
		'hittest','off' ...
	);

	set(ax,'layer','top');
			
%--
% update the color if needed
%--

else
	
	create = 0; 
	
	if (~isempty(color))
		set(g,'cdata',create_gradient(color));
	end
	
end

%--
% update value if needed
%--

if (~isempty(value))
		
	% NOTE: it is not clear why the value must be halved ???
	
	if (value > 0)
		set(g,'xdata',[0, value/2],'visible','on'); 
	else
		set(g,'visible','off');
	end
	
end


%-----------------------------------------------------
% CREATE_GRADIENT
%-----------------------------------------------------

% TODO: develop a variety of gradients

% NOTE: this will eventually move to a separate function

function X = create_gradient(color,n)

%---------------------------------
% HANDLE INPUT
%---------------------------------

if (nargin < 2)
	n = 64;
end

%---------------------------------
% CREATE IMAGE
%---------------------------------

for k = 1:3
	X(:,1,k) = (5 * color(k) + linspace(0.1,1,n)') / 6;
end
