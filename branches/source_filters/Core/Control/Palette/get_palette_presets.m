function [presets,f] = get_palette_presets(varargin)

% get_palette_presets
% -------------------
%
% presets = get_palette_presets
%
% Output:
% -------
%  presets - palette presets array
%  N - number of valid presets (mostly used when equal to zero)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%--
% set (possibly create) palette preset directory
%--

par = [xbat_root, filesep, 'Presets'];

p = [par, filesep, 'Palettes'];

if (~exist(p,'dir'))
	mkdir(par, 'Palettes');
end

%--
% load presets from matfiles in directory
%--

f = what_ext(p,'mat'); 

f = f.mat(:);

% NOTE: return if there are no preset files

if (~length(f))
	presets = []; return;
end
	
j = 0;

for k = length(f):-1:1
	
	% NOTE: failure does not increment counter and removes file from list
	
	try
		tmp = load([p, filesep, f{k}],'state');
	catch
		f(k) = [];
		continue;
	end
		
	j = j + 1;
	presets(j) = tmp.state;
	
end

presets = flipud(presets(:));