function flag = palette_enable(pal,value)

% palette_enable - enable or disable all controls in palette
% ----------------------------------------------------------
%
% flag = palette_enable(pal,value)
%
% Input:
% ------
%  pal - palette handle
%  value - enable value
%
% Output:
% -------
%  flag - success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3168 $
% $Date: 2006-01-11 16:54:20 -0500 (Wed, 11 Jan 2006) $
%--------------------------------

flag = [];

%--
% convert value
%--

switch (lower(value))
	
	case ('on')
		value = '__ENABLE__';
		
	case ('off')
		value = '__DISABLE__';
		
	otherwise
		return;
		
end

%--
% get control names
%--

% NOTE: only controls that provide values need to be considered 

controls = fieldnames(get_control_values(pal));

%--
% enable or disable controls
%--

% use foreach construction

for k = 1:length(controls)
	control_update([],pal,controls{k},value);
end

flag = 1;