function value = show_desktop(value)

% show_desktop - set visibility of windows desktop
% ------------------------------------------------
% 
% state = show_desktop
%
% show_desktop(1) - show the desktop
%
% show_desktop(0) - hide the desktop

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

% NOTE: we share desktop display state through environment variable

%------------------------------------------------------------
% HANDLE INPUT
%------------------------------------------------------------

%--
% return value if no input
%--

if nargin < 1
	
	value = get_env('show_desktop'); 
	
	% NOTE: handle empty value here in case 'startup' fails to set it
	
	if ~isempty(value)
		return;
	else
		value = 1;
	end
	
end 

%------------------------------------------------------------
% BUILD AND EXECUTE COMMAND
%------------------------------------------------------------

%--
% get tool file
%--

tool = nircmd;

% NOTE: this allows the command syntax

if ischar(value)
	value = eval(value);
end

if value
	
	[status, result] = system(['"', tool.file, '" win show class progman']);
	
	set_env('show_desktop', 1);
	
else
	
	[status, result] = system(['"', tool.file, '" win hide class progman']);
	
	set_env('show_desktop', 0);
	
end