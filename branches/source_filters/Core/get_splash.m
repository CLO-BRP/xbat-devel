function pal = get_splash

% get_splash - get splash figure handle
% -------------------------------------
%
% pal = get_splash
%
% Output:
% -------
%  pal - handle to splash

%--
% get open waitbars
%--

pals = get_xbat_figs('type', 'waitbar'); pal = [];

if isempty(pals)
	return;
end

%--
% find one named as splash
%--

% NOTE: the tag should be more structured

info = parse_tag(get(pals, 'tag'), '::', {'type', 'name'});

for k = 1:length(info)
	
	if strcmpi(info(k).name, 'splash')
		pal = pals(k); return;
	end
	
end
