function p = frechet_pdf(x,a,b,c)

% frechet_pdf - frechet extreme value distribution
% -----------------------------------------------
% 
% p = frechet_pdf(x,a,b,c)
%
% Input:
% ------
%  x - points to evaluate
%  a - location parameter
%  b - scale parameter
%  c - shape parameter
%
% Output:
% -------
%  p - frechet distribution

%--
% compute shape of probability density function of frechet
%--

z = (x - a) ./ b;

ix = find(x <= a);
z(ix) = 0;

z1 = z.^(-c);
z2 = z1 ./ z;

p = (c / b) .* z2 .* exp(-z1);

% set division by zero points to zero

ix = find(isnan(p));
p(ix) = 0;


