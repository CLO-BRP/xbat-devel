function p = gumbel_fit(X)

% gumbel_fit - fit gumbel extreme value distribution
% --------------------------------------------------
%
% p = gumbel_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  p - gumbel parameters

%--
% put data into vector
%--

x = X(:);

%--
% compute initial parameter estimates
%--

p(1) = mean(x);
p(2) = std(x);

%--
% minimize negative likelihood
%--

opt = optimset;
opt.Display = 'iter';

p = fminsearch('gumbel_likelihood',p,opt,x);
