function L = gumbel_likelihood(p,X)

% gumbel_likelihood - gumbel log likelihood
% -----------------------------------------
%
% L = gumbel_likelihood(p,X)
%
% Input:
% ------
%  p - gumbel parameters
%  X - input data
%
% Output:
% -------
%  L - negative log likelihood of data

%--
% put data in vector and separate parameters
%--

x = X(:);

a = p(1);
b = p(2);

%--
% compute negative log likelihood
%--

d = -x + a;
z = d / b;

L = sum((1 / b) * exp((d - (b * exp(z))) / b));
% L = -L;