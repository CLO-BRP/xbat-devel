function flag = update_detect_menu

% update_detect_menu - update detect menu for open browsers
% ---------------------------------------------------------
%
% update_detect_menu

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1814 $
% $Date: 2005-09-21 15:24:01 -0400 (Wed, 21 Sep 2005) $
%--------------------------------

%--
% get handles to open browser figures
%--

if (nargin < 1)
	par = get_xbat_figs('type', 'sound');
end

% NOTE: return if there is nothing to update

if isempty(par)
	return;
end

%--
% destroy and create filter menus
%--

% NOTE: this destroys any detector state stored in browsers, change this

for k = 1:length(par)
	
	%--
	% find parent menu
	%--
	
	menu = findobj(par(k), 'type', 'uimenu', 'label', 'Detect');
	
	if isempty(menu)
		continue;
	end
	
	%--
	% kill all children then parent
	%--
	
	try
		delete(allchild(menu)); delete(menu);
	end
		
	%--
	% recreate filter menu
	%--
	
	browser_detect_menu(par(k));
	
end
