function h = fig(m,n,k)

% fig - create figure with fig menu
% ---------------------------------
%
% h = fig(m,n,k)
%
% Input:
% ------
%  m,n - figure rows and columns
%  k - position
%
% Output:
% -------
%  h - handle to figure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:52-04 $
%--------------------------------

% TODO: allow configuration of tools available, say only tile and cascade

%--
% handle variable input
%--

switch (nargin)

	%--
	% default size fig
	%--
	
	case (0)
		
		h = figure;
		
		set(h, ...
			'tag','FIG', ...
			'doublebuffer','on', ...
			'backingstore','on' ...	
		);
	
		fig_menu(h);
		
	%--
	% grid positioned figure
	%--
	
	case (3)
		
		h = figure;
		
		set(h, ...
			'tag','FIG', ...
			'doublebuffer','on', ...
			'backingstore','on' ...	
		);
	
		fig_sub(m,n,k,h);	
		
		fig_menu(h);
	
end