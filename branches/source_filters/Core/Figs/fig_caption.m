function str = fig_caption(str,h)

% fig_caption - get and set fig caption
% -------------------------------------
%
% str = fig_caption(h)
%     = fig_caption(str,h)
%
% Input:
% ------
%  str - caption string
%  h - figure handle (def: gcf)
%
% Output:
% -------
%  str - caption string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:42-04 $
%--------------------------------

%--
% set figure handle
%--

if (nargin < 1)
	str = gcf;
end

%--
% set caption string
%--

if isstr(str)

	% set figure handle
	
	if (nargin < 2)
		h = gcf;
	end
	
	% set caption string
	
	data = get(h,'userdata');
	data.fig.caption = str;
	set(h,'userdata',data);

%--
% get caption string
%--

elseif ishandle(str)

	h = str;
	data = get(h,'userdata');
	str = data.fig.caption;
	
end
