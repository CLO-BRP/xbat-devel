function log = new_log(logname, user, lib, sound)

% new_log - make a new log belonging to a particular user and sound

log = [];

%--
% check for empty string and proper file name
%--

if isempty(logname) || ~proper_filename(logname)
	return;
end

%--
% check whether a log with this name already exists
%--

logs = get_library_logs('info', lib, sound);

if any(strcmp(logs,[sound_name(sound) filesep logname]))
	return;
end

%--
% create new log and open
%--

lib_path = lib.path;

logname = [lib_path, sound_name(sound), filesep, 'Logs', filesep, logname, '.mat'];

log = log_create(logname,'sound',sound,'author',user.name);

%---------------------------------
% UPDATE VISIBLE STATE
%---------------------------------

%--
% update XBAT palette if available
%--

xbat_palette('find_sounds');

%--
% try to open log in browser if possible
%--

par = get_active_browser;

if ~isempty(par)
	log_open(par, logname);
end