function [value, unit] = is_proper_size_unit(obj, unit)

% is_proper_size_unit - check and normalize size unit for object
% --------------------------------------------------------------
%
% [value, unit] = is_proper_size_unit(obj, unit)
%
% Input:
% ------
%  obj - sizeable object
%  unit - proposed unit
% 
% Output:
% -------
%  value - propriety
%  unit - normalized unit string

%--
% check units type
%--

if ~ischar(unit)
	error('Units must be specified as a string.');
end

%--
% expand some unit abbreviations
%--

switch lower(unit)
	
	case 'cm', unit = 'centimeters';
		
	case 'in', unit = 'inches';
		
	case 'px', unit = 'pixels';

end

%--
% get allowed object units and check we are one of them
%--

try
	units = set(obj, 'units');
catch
	value = 0; unit = '__NO_UNITS__'; return;
end

value = ismember(unit, units);

