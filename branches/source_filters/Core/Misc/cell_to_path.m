function out = cell_to_path(in)

% cell_to_path - concatenate cell strings into path string
% --------------------------------------------------------
%
% out = cell_to_path(in)
%
% Input:
% ------
%  in - input cell string
%
% Output:
% -------
%  out - path string

%--
% handle input
%--

if ~iscellstr(in)
	error('Input must be cell array of strings.');
end

%--
% concatenate
%--

in = strcat(in, pathsep); out = [in{:}]; 

% NOTE: we remove the trailing filesep

out(end - (length(pathsep) - 1):end) = [];