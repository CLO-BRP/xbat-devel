function test = has_menu(h)

% has_menu - test whether figure has a menu bar
% ---------------------------------------------
%
% test = has_menu(h)
%
% Input:
% ------
%  h - figure handle
%
% Output:
% -------
%  test - menu indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 915 $
% $Date: 2005-04-05 18:59:46 -0400 (Tue, 05 Apr 2005) $
%--------------------------------

% TODO: generalize this to handles, and possibly output other results

%--
% check for handle and figure
%--

if (~ishandle(h))	
	test = 0; return;
end

if (~strcmp(get(h,'type'),'figure'))
	test = 0; return;
end

%--
% check for top level menus of figure
%--

% NOTE: we check for menus which are figure children

tmp = get(h,'children');

tmp = findobj(tmp,'type','uimenu','parent',h); 

test = ~isempty(tmp);
