function figure_resize(par)

% figure_resize - apply figure resize function
% --------------------------------------------
%
% figure_resize(par)
%
% Input:
% ------
%  par - figure handle

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% handle input
%--

if any(~ishandle(par))
	error('Input must be handles.');
end

%--
% handle multiple handles recursively
%--

if numel(par) > 1
	
	for k = 1:numel(par)
		figure_resize(par(k));
	end
	
	return;
	
end

%--
% check handle type
%--

if ~strcmp(get(par, 'type'), 'figure')
	return;
end

%---------------------------
% GET AND APPLY RESIZE
%---------------------------

%--
% get figure resize function
%--

fun = get(par, 'resizefcn');

if isempty(fun)
	return;
end

%--
% apply resize function
%--

switch class(fun)
	
	case ('cell'), args = fun(2:end); fun = fun{1}; fun(par, [], args{:});
		
	case ('char'), eval(fun);
		
	case ('function_handle'), fun(par, []);
		
end