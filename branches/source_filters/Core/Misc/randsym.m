function A = randsym(n,d)

% randsym - full random symmetric matrix
% --------------------------------------
%
% A = randsym(n,d)
%
% Input:
% ------
%  n - size of matrix
%  d - desired eigenvalues
%
% Output:
% -------
%  A - full random symmetric matrix with possibly specified eigenvalues

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%--
% set default eigenvalues
%--

% NOTE: default values are uniform in the unit interval

if ((nargin < 2) || isempty(d))
	d = rand(1,n);
end

%--
% create random orthogonal basis
%--

V = orth(rand(n));

%--
% put together random symmetric matrix
%--

A = V * diag(d) * V';