function process_page_file(file, data, debug)

% PROCESS_PAGE_FILE process page file markup
%
% process_page_file(file, data)

if nargin < 3
	debug = 0;
end

%--
% create temporary file
%--

temp = [tempname, '.html'];

if ~copyfile(file, temp)
	error('Unable to create temporary file.');
end

%--
% use file process with callback
%--

file_process(file, temp, {@process_callback, data, debug});

%--
% delete temporary file
%--

delete(temp);


