function label = get_label(obj) 

% get_label - get tagged objects label
% ------------------------------------
%
% label = get_label(obj)
%
% Input:
% ------
%  obj - taggable object array
%
% Output:
% -------
%  label - object label array

%--
% handle input
%--

if ~is_taggable(obj)
	error('Input must be taggable.'); return;
end

if length(obj) > 1
	
	label = cell(size(obj));
	
	for k = 1:length(obj)
		label{k} = get_label(obj(k));
	end
	
	return;
	
end

%--
% get tags and check for labels
%--

% NOTE: allow a single tag to be the label

tags = get_tags(obj); label = '';
	
switch length(tags)

	case 0

	case 1, label = tags{1};

	otherwise

		for j = 1:length(tags)
			if (tags{j}(1) == '*')
				label = tags{j}(2:end); return;
			end
		end

end