function pal = refresh_extension(callback, context)

% refresh_extension - update browser extension state
% --------------------------------------------------
%
% pal = refresh_extension(callback, context)
%
% Input:
% ------
%  callback - callback context
%  context - extension context
%
% Output:
% -------
%  pal - new extension palette

% NOTE: this function may use 'set_browser_extension'

%-----------------------------
% SETUP
%-----------------------------

%--
% unpack input
%--

pal = callback.pal; par = callback.par; 

ext = context.ext;

%-----------------------------
% REFRESH EXTENSION
%-----------------------------

%--
% close palette
%--

close(pal.handle);

%--
% refresh extension
%--

% TODO: figure out where to put a refresh of the system extension cache

data = get_browser(par.handle);

[ext, ix] = get_browser_extension(ext.subtype, par.handle, ext.name, data);

data.browser.(ext.subtype).ext(ix) = extension_initialize(ext, context);

set(par.handle, 'userdata', data);

%--
% open palette
%--

pal = extension_palettes(par.handle, ext.name, ext.subtype);
