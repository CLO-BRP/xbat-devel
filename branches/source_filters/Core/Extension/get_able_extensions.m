function ext = get_able_extensions(in)

% get_able_extensions - get enabled extensions that compute
% ---------------------------------------------------------
%
% ext = get_able_extensions(type)
%
% Input:
% ------
%  type - extension type (def: '', get all extension types)
%  ext - extension array
%
% Output:
% -------
%  ext - able extensions

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set default to check all extensions
%--

if (nargin < 1)
	in = '';
end

%--
% get or rename extensions
%--

% NOTE: 'get_extensions' will check the extension type string

if isstr(in)
	ext = get_extensions(in);
else
	ext = in;
end

%--
% get developer level
%--

level = xbat_developer;

%--------------------------------------------
% SELECT ABLE EXTENSIONS
%--------------------------------------------

for k = length(ext):-1:1
	
	%--
	% remove disabled and non-computing extensions
	%--
	
	% NOTE: the non-computing test is deprecated
	
	if ~ext(k).enable || isempty(ext(k).fun.compute)
		ext(k) = [];
	end

	%--
	% remove base extensions if we are not developers
	%--
	
	% NOTE: base extensions have uppercase names by convention
	
	if (level < 1) && isequal(ext(k).name, upper(ext(k).name))
		ext(k) = [];
	end
	
end