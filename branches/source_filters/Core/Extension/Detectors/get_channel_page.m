function page = get_channel_page(page, k);

%--
% create channel specific page from multiple channel page
%--

page.channels = page.channels(k);

page.samples = page.samples(:, k);

% TODO: in the general scan we will also need to pull out channel features

if ~isfield(page, 'filtered')
	page.filtered = [];
end

if ~isempty(page.filtered)
	page.filtered = page.filtered(:, k);
end