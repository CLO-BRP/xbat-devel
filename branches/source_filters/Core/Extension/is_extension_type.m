function value = is_extension_type(type)

% is_extension_type - check whether proposed string is extension type
% -------------------------------------------------------------------
%
% value = is_extension_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - result of test

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

if ~ischar(type)
	value = 0; return;
end

value = ismember(type, get_extension_types);

% value = ~isempty(find(strcmp(get_extension_types, type)));
