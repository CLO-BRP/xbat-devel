function root = extensions_root(type)

% extensions_root - extensions root directory
% ------------------------------------------
%
% root = extensions_root(type)
%
% Input:
% ------
%  type - extension type (def: '', get root of all extensions)
% 
% Output:
% -------
%  root - root directory for extensions

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% TODO: develop 'set' and 'get', and allow multiple roots

%--
% all extensions root
%--

root = [xbat_root, filesep, 'Extensions'];

%--
% specific extension type root
%--

if nargin
	
	if ~ischar(type) || ~is_extension_type(type)
		error('Unrecognized extension type.');
	end
	
	root = [root, filesep, type_to_dir(type)];
	
end

