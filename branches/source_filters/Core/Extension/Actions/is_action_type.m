function value = is_action_type(type)

% is_action_type - test if string is action type
% ----------------------------------------------
%
% value = is_action_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - type indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% handle input
%--

if ~ischar(type)
	error('Action type must be string.');
end

%--
% check if proposed string is action type
%--

value = ismember(type, get_action_types);