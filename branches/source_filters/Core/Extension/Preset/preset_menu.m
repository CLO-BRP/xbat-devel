function preset_menu(par, ext)

% preset_menu - create preset menu for extension
% ------------------------------------------
%
% preset_menu(par, ext)
%
% Input:
% ------
%  par - menu parent
%  ext - extension (def: get from parent figure)

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% get extension from parent figure
%--

% NOTE: typically 'par' and 'pal' are the same

pal = ancestor(par, 'figure');

if nargin < 2
	
	% NOTE: this seems to happen in two places, here and 'preset_compile'
	
	[value, ext] = is_extension_palette(pal);
	
	if ~value
		error('Unable to get extension from palette.');
	end
	
end

%--
% check if extension has presets
%--

if ~has_presets(ext)
	return;
end 

%--------------------------
% SETUP
%--------------------------

%--
% clear former menu if needed
%--

top = findobj(par, 'type', 'uimenu', 'tag', 'TOP_PRESET_MENU');

if ~isempty(top)
	delete(allchild(top));
end

%--------------------------
% CREATE PRESET MENU
%--------------------------

%--
% create top menu if needed
%--

if isempty(top)
	top = uimenu(par, 'label', 'Presets', 'tag', 'TOP_PRESET_MENU');
end 

%--
% create command menus
%--

% TODO: develop a browse command, to search preset collection

uimenu(top, ...
	'label', 'Save ...', ...
	'callback', {@save_preset_callback, par, ext} ...
);

uimenu(top, ...
	'enable', 'on', ...
	'label', 'Load ...', ...
	'callback', {@load_preset_callback, pal, ext} ...
);

%--
% create preset menus
%--

names = file_ext(get_preset_files(ext));

if isempty(names)
	
	uimenu(top, ...
		'label', '(No Presets Found)', ...
		'enable', 'off', ...
		'separator', 'on' ...
	);

else
	
	uimenu(top, ...
		'label', '(Presets)', ...
		'separator', 'on', ...
		'enable', 'off' ...
	);

	named = [];

	for k = 1:length(names)
		named(end + 1) = uimenu(top, ...
			'label', names{k}, ...
			'callback', {@open_preset_callback, pal, ext} ...
		);
	end

% 	set(named(1), 'separator', 'on');
	
end

uimenu(top, ...
	'label', 'Refresh', ...
	'separator', 'on', ...
	'callback', {@refresh_menu_callback, par, ext} ...
);

uimenu(top, ...
	'label', 'Show Files ...', ...
	'separator', 'off', ...
	'callback', {@show_files_callback, ext} ...
);


%-------------------------------------
% SAVE_PRESET_CALLBACK
%-------------------------------------

function save_preset_callback(obj, eventdata, par, ext) 

%--
% compile preset
%--

pal = ancestor(par, 'figure');

preset = preset_compile(pal);

%--
% present dialog to save session
%--

info = save_preset_dialog(preset);

% NOTE: return if no session was saved

if isempty(info)
	return;
end 

%--
% rebuild preset menu
%--

preset_menu(par, ext);


%-------------------------------------
% LOAD_PRESET_CALLBACK
%-------------------------------------

function load_preset_callback(obj, eventdata, pal, ext) 

%--
% load preset through dialog
%--

preset = load_preset_dialog(ext);

if isempty(preset)
	return;
end 
	
%--
% load preset into browser
%--

par = get_palette_parent(pal);

if isempty(par)
	return;
end 

% TODO: get system extension and update with some preset fields

set_browser_extension(par, preset.ext);


%-------------------------------------
% OPEN_PRESET_CALLBACK
%-------------------------------------

function open_preset_callback(obj, eventdata, pal, ext) 

%--
% load preset from file
%--

preset = preset_load(ext, get(obj, 'label'));

%--
% load preset into browser
%--

par = get_palette_parent(pal);

if isempty(par)
	return;
end 

% TODO: get system extension and update with some preset fields

set_browser_extension(par, preset.ext);


%--------------------------------------
% REFRESH_MENU_CALLBACK
%--------------------------------------

function refresh_menu_callback(obj, eventdata, par, ext)

preset_menu(par, ext);


%-------------------------------------
% SHOW_FILES_CALLBACK
%-------------------------------------

function show_files_callback(obj, eventdata, ext)

show_file(preset_dir(ext));
