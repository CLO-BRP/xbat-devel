function [info, file] = preset_save(preset, name, type, tags, notes)

% preset_save - save preset to file
% ---------------------------------
%
% [info, file] = preset_save(preset, name, type, tags, notes)
%
% Input:
% ------
%  preset - preset
%  name - name
%  type - type
%  tags - tags
%  notes - notes
%
% Output:
% -------
%  info - preset file info
%  file - preset file

% NOTE: this is the current generic parameter preset store

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default empty tags and notes
%--

if nargin < 5
	notes = {};
end

if nargin < 4
	tags = {};
end

if nargin < 3
	type = '';
end 

%--
% handle tags
%--

if ischar(tags)
	tags = str_to_tags(tags);
end

if ~is_tags(tags)
	error('Input tags are not proper tags.');
end

%--
% check preset name
%--

if ~proper_filename(name)
	error('Preset name must be proper filename.');
end

%-------------------
% SAVE PRESET
%-------------------

%--
% get preset file location
%--

file = preset_file(preset.ext, name, type);

%--
% update preset
%--

if ~isempty(tags)
	preset.tags = tags;
end

if ~isempty(notes)
	preset.notes = notes;
end

%--
% save preset file and get info
%--

% TODO: the extension is part of the preset check for save method

try
	save(file, 'preset'); info = dir(file);
catch 
	info = [];
end
