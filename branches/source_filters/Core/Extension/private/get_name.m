function name = get_name(caller)

% get_name - get name of extension by examining caller location
% -------------------------------------------------------------
%
% name = get_name(caller)
%
% Output:
% -------
%  name - name of extension if computable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% compute name from caller location
%--

% NOTE: this is the name of the parent directory of the caller

name = caller.loc{end};

