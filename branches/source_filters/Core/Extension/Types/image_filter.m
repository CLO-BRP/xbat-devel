function fun = image_filter

% image_filter - function handle structure
% -----------------------------------------
%
% fun = image_filter
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: for now context output is to indicate any mixing and type casting

fun.compute = {{'X', 'context'}, {'X', 'parameter', 'context'}};

fun.parameter = param_fun;

fun.explain = display_fun;
