function fun = log_annotation

% log_annotation - function handle structure
% -----------------------------------------
%
% fun = log_annotation
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.parameter_create = []; 

fun.parameter_control = [];

fun.compute = [];