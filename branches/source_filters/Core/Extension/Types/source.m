function fun = source

% source - function handle structure
% ----------------------------------
%
% fun = source
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: a source extension generates synthetic data

fun.compute = {{'data'}, {'parameter', 'context'}};

fun.parameter = param_fun;