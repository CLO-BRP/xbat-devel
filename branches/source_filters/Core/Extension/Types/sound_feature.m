function fun = sound_feature

% sound_feature - function handle structure
% -----------------------------------------
%
% fun = sound_feature
%
% Output:
% -------
%  fun - structure for extension type API

fun.compute = {{'feature', 'context'}, {'page', 'parameter', 'context'}};

fun.parameter = param_fun;

fun.view = display_fun('feature');