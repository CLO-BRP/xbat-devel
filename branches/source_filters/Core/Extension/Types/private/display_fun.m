function fun = display_fun(name)

% display_fun - display function structure
% ----------------------------------------
%
% fun = display_fun(name)
%
% Output:
% -------
%  fun - display function structure

if ~nargin || isempty(name)
	name = 'data';
end

% NOTE: the first cell contains output names, the second cell input names

fun.layout = {{'handles'}, {'par', 'parameter', 'context'}};

fun.display = {{'handles'}, {'par', name, 'parameter', 'context'}};

fun.parameter = param_fun;
