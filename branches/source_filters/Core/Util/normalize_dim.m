function A = normalize_dim(A, k, opt)

% normalize_dim - center and normalize dimension
% ----------------------------------------------
%
% A = normalize_dim(A, k)
%
% Input:
% ------
%  A - array to normalize
%  k - dimension to normalize
%
% Output:
% -------
%  A - normalized array

%--
% set default normalization options
%--

if nargin < 3
	
	opt.center = 1; opt.normalize = 1;
	
	if ~nargin
		A = opt; return;
	end
	
end

%--
% get dimensions and check input
%--

d = ndims(A); 

if k > d
	error('Dimension to normalize is not available.');
end

%--
% get repeat vector to reconstitute collapsed array
%--

m = size(A, k); rep = ones(1, d); rep(k) = m;

%--
% normalize array dimension
%--

if opt.center
	A = A - repmat(mean(A, k), rep);
end

if opt.normalize
	A = A ./ sqrt(repmat(sum(A.^2, k), rep));
end
