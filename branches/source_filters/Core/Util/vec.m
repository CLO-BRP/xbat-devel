function x = vec(X)

% vec - convert array to vector
% -----------------------------
%
% x = vec(X)
%
% Input:
% ------
%  X - input array
%
% Output:
% -------
%  x - vector obtained from array

% NOTE: this allows better access to a simple built-in operation

x = X(:);
