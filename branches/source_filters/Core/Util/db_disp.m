function out = db_disp(str, var, opt)

% db_disp - display caller message to help debug process
% ------------------------------------------------------
%
% db_disp(str, var, opt)
%
% Input:
% ------
%  str - message string
%  var - variable to display
%  opt - display options

% TODO: clear up state setting code

% TODO: produce a linked display using 'stack_line'

%--------------------------------------
% SETUP
%--------------------------------------

%--
% create persistent display state store
%--

persistent DB_DISP_STATE; 

if isempty(DB_DISP_STATE)
	DB_DISP_STATE = 1;
end

%--
% set display state
%--

flag = 0;

if (nargin == 1) && ~ischar(str)
	
	switch str
		
	case {0,1}
		DB_DISP_STATE = str; flag = 1;

	otherwise
		error('Display state must be zero or one.');
		
	end
	
end

out = DB_DISP_STATE; 

if ~nargin
	return;
end

if ~nargout
	clear('out');
end
	
%--
% get and remove ourselves from stack
%--

% NOTE: there are some problems with asking 'dbstack' to omit us from the start

stack = dbstack; stack(1) = [];

%--
% display state when called from the command line
%--

if ~length(stack)
	
	switch DB_DISP_STATE

	case 0
		disp(' '); disp('DB_DISP is off.'); disp(' ');

	case 1
		disp(' '); disp('DB_DISP is on.'); disp(' ');
			
	end
	
	return; 

end

if flag
	return;
end

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% set options
%--

% NOTE: for the moment we ask for a compact display

if (nargin < 3) || isempty(opt)
	opt = 0;
end

%--
% set empty variable default
%--

if (nargin < 2)

	var = []; name = [];
	
else
	
	name = inputname(2);
	
	if isempty(name)
		name = 'COMPUTED_VALUE';
	end
	
end

%--
% set empty message default
%--

if (nargin < 1)
	str = '';
end

%--
% return if state is off
%--

if ~DB_DISP_STATE
	return;
end

%--------------------------------------
% DISPLAY MESSAGE
%--------------------------------------

% TODO: produce a different display string

out.message = upper(str);

out.stack = stack;

% NOTE: we display the variable last

if ~isempty(name)
	out.(name) = var;
end

%--
% display according to options
%--

switch opt
	
	% TODO: make this a link to the editor
	
	case 0
		disp(['''', stack(1).file, ''' at line ', int2str(stack(1).line), ': ', out.message]);
		
	% TODO: transform the XML into something that perhaps opens a browser
	
	otherwise
		xml_disp(out);
		
end

%--
% suppress output display
%--

if ~nargout
	clear out; 
end
