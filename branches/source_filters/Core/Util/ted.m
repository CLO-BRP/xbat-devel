function [status, result] = ted(file)

% ted - text edit file in an appropiate editor
% -------------------------------------------
%
% ted file
%
% [status, result] = ted(file)
%
% Input:
% ------
%  file - file
%
% Output:
% -------
%  status - status
%  result - result

%-----------------
% HANDLE INPUT
%-----------------

%--
% check input is simple file name, not full name
%--

name = isempty(strfind(file, filesep));

if name
	file0 = file;
end

%--
% check for file
%--

% TODO: try to find the file in the current directory first

file = which(file);

% NOTE: create file if needed, consider asking for confirmation

if isempty(file) && name
	file = [pwd, filesep, file0]; fclose(fopen(file, 'w'));
end

if isempty(file) || ~exist(file, 'file')
	error('Unable to find file for editing.');
end

%-----------------
% HANDLE INPUT
%-----------------

%--
% select editor using file extension
%--

[ignore, ignore, ext] = fileparts(file); ext = lower(ext);

switch ext
	
	%--
	% matlab editor
	%--
	
	case {'.c', '.m'}, edit(file); status = 1; result = '';
		
	%--
	% binary files and sound files
	%--
	
	% TODO: use hex editor for these, this is a kind of text
	
	% NOTE: the exported symbols tool is called directly on 'dll' files
	
	case {'dll', 'exe'}
		
	case get_formats_ext
	
	%--
	% scite
	%--
	
	otherwise, [status, result] = scite(file);
		
end
