function opt = get_axes_selection_options(ax)

% get_axes_selection_options - get default selection options for axes
% -------------------------------------------------------------------
%
% opt = get_axes_selection_options(ax)
%
% Input:
% ------
%  ax - axes handles
%
% Output:
% -------
%  opt - axes selection options

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

[ignore, opt] = is_selection_axes(ax);
