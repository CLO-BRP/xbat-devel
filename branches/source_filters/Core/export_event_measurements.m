function out = export_event_measurements(h,name,field)

% export_event_measurements - export event measurements
% -----------------------------------------------------
%
% flag = export_event_measurements(h,name,field)
%
% Input:
% ------
%  name - measurement name
%  field - measurement fields
%
% Output:
% -------
%  flag - export success flag