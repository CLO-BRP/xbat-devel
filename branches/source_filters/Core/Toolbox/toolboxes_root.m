function root = toolboxes_root(local) 

% TOOLBOXES_ROOT get root directory for all toolboxes
% 
% root = toolboxes_root(local) 

%--
% handle input
%--

if ~nargin
	local = 1;
end

%--
% build toolboxes root
%--

if local
	root = create_dir([xbat_root, filesep, 'Toolboxes']);
else
	root = [matlabroot, filesep, 'toolbox'];
end

if isempty(root) 
	error('Unable to access toolboxes root directory.');
end
