function [value, info] = has_toolbox(name)

% has_toolbox - check for toolbox and get version info
% ----------------------------------------------------
%
% [value, info] = has_toolbox(name)
%
% Input:
% ------
%  name - toolbox
%
% Output:
% -------
%  value - result
%  info - version info

%--
% consider built-in toolboxes
%--

info = ver(name);

value = ~isempty(info);

%--
% consider external toolboxes
%--

% NOTE: explain why we do this, and consider a better way
