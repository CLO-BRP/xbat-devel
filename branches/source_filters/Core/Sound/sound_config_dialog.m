function out = sound_config_dialog(sound)

% sound_config_dialog - configurable sound options dialog
% -----------------------------------------------------
%
% out = sound_config_dialog(sound)
%
% Input:
% ------
%  sound - sound to edit
%
% Output:
% -------
%  out - dialog output

%--------------------------------
% Author: Matt Robbins, Harold Figueroa
%--------------------------------
% $Revision: 3131 $
% $Date: 2006-01-06 19:00:21 -0500 (Fri, 06 Jan 2006) $
%--------------------------------

% TODO: consider name of dialog and parent menu command label

% TODO: integrate format specific options into this dialog and framework

% NOTE: consider namespacing control names

%-----------------------------------------------------
% CREATE CONTROLS
%-----------------------------------------------------

%---------------------
% HEAD
%---------------------

%--
% create sound name header
%--

control(1) = control_create( ...
	'style','separator', ...
	'type','header', ...
	'space',0.1, ...
	'min',1, ...
	'string',sound_name(sound) ...
);

%--
% create tabs
%--

tabs = {'Time', 'Source'};

% NOTE: the format tab is for format specific configuration options

format_test = 0;

if format_test
	tabs{end + 1} = 'Format';
end

control(end + 1) = control_create( ...
	'name','config_tabs', ...
	'style','tabs', ...
	'lines',1.25, ...
	'tab',tabs ...
);

%---------------------
% DATE TIME
%---------------------

control(end + 1) = control_create( ...
	'name', 'datetime', ...
	'style', 'listbox', ...
	'tab', tabs{1}, ...
	'lines', 0.5, ...
	'space', 0.75, ...
	'string', {datestr(sound.realtime, 0)}, ...
	'value', 1 ...
);


% control(end + 1) = control_create( ...
% 	'name', 'datetime_toggle', ...
% 	'alias', 'use datetime', ... 
% 	'style', 'checkbox', ...
% 	'tab', tabs{1} ...
% );

%---------------------
% SESSION
%---------------------

% TODO: consider hide if there are no time stamps available or create button

% TODO: load current sound state onto controls

% control(end + 1) = control_create( ...
% 	'style','separator', ...
% 	'space', 1, ...
% 	'tab',tabs{1} ...
% );

% TODO: check for availability of time stamped sessions

sessions = get_sound_sessions(sound, 0);

if isempty(sessions)
	
	state = '__DISABLE__'; str = '(NOT AVAILABLE)'; lines = 1; value = [];
	
	enable = 0; collapse = 0;
	
else
	
	state = '__ENABLE__'; str = schedule_info_str(sessions); lines = numel(str);  value = 1;
	
	enable = sound.time_stamp.enable; collapse = sound.time_stamp.collapse;
	
end

control(end + 1) = control_create( ...
	'name', 'time_stamps', ...
	'alias', 'schedule', ...
	'style', 'listbox', ...
	'lines', lines, ...
	'tab', tabs{1}, ...
	'space',1, ...
	'max', 2, ...
	'initialstate',state, ...
	'string', str, ...
	'value', value ...
);

control(end + 1) = control_create( ...
	'name', 'enable_time_stamps', ...
	'alias', 'use schedule', ...
	'style', 'checkbox', ...
	'tab', tabs{1}, ...
	'space',0.75, ...
	'initialstate',state, ...
	'value', enable ...
);

control(end + 1) = control_create( ...
	'name', 'collapse_sessions', ...
	'alias', 'hide silence', ...
	'style', 'checkbox', ...
	'tab', tabs{1}, ...
	'space',1.5, ...
	'initialstate',state, ...
	'value', collapse ...
);

%---------------------
% CLASS
%---------------------

types = upper(get_sound_output_types);

value = find(strcmpi(types, sound.output.class));

if isempty(value)
	value = 1;
end

%--
% create controls
%--

control(end + 1) = control_create( ...
	'name', 'class', ...
	'alias','datatype', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'space',1.5, ...
	'string', types, ...
	'value', value ...
);	

%---------------------
% SAMPLERATE
%---------------------

%--
% get current rate
%--

if (~isempty(sound.output.rate))
	value = sound.output.rate; state = 1; status = '__ENABLE__';
else
	value = sound.samplerate; state = 0; status = '__DISABLE__';
end

%--
% create controls
%--

control(end + 1) = control_create( ...
    'name','samplerate', ...
    'style','slider', ...
	'tab', tabs{2}, ...
	'space',1, ...
    'min', 100, ...
    'max', 96000, ...
    'value', value, ...
	'initialstate', status ...
);

control(end + 1) = control_create( ...
	'name', 'resample', ...
	'style', 'checkbox', ...
	'tab', tabs{2}, ...
	'value', state ...
);

ext = get_extensions('signal_filter');

string = {ext.name}; string = {'(No Filter)', string{:}}; value = 1;

if isfield(sound.output, 'filter') && ~isempty(sound.output.filter)
	
	name = sound.output.filter.name;
	
	value = find(strcmp(name, string));
	
end

control(end + 1) = control_create( ...
	'name', 'filter', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'string', string, ...
	'value', value ...
);

ext = get_extensions('source');

string = {ext.name}; string = {'(No Source)', string{:}}; value = 1;

if isfield(sound.output, 'source') && ~isempty(sound.output.source)
	
	name = sound.output.source.name;
	
	value = find(strcmp(name, string));
	
end

control(end + 1) = control_create( ...
	'name', 'source', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'string', string, ...
	'value', value ...
);
	

%-----------------------------------------------------
% CREATE DIALOG
%-----------------------------------------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 12;

opt.header_color = get_extension_color('root');

%--
% create dialog
%--

name = 'Configure';

out = dialog_group(name, control, opt, {@dialog_callback, sound});


%---------------------------------------------------------
% DIALOG_CALLBACK
%---------------------------------------------------------

function result = dialog_callback(obj,eventdata,sound)

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1938 $
% $Date: 2005-10-17 08:49:40 -0400 (Mon, 17 Oct 2005) $
%--------------------------------

% NOTE: there are some naming tensions in this dialog

result = [];

[control,par] = get_callback_context(obj); 

switch (control.name)
	
	%--
	% source datatype control
	%--
	
	case ('class')

		values = get_control_values(par.handle);
		
	%--
	% resample state control
	%--
	
	case ('resample')
		
		values = get_control_values(par.handle);
		
		if (values.resample)
			
			control_update([],par.handle,'samplerate', '__ENABLE__');
			
		else
			
			% NOTE: we set the samplerate display to the native samplerate
			
			control_update([],par.handle,'samplerate',sound.samplerate);
			
			control_update([],par.handle,'samplerate', '__DISABLE__');
			
		end
	
	%--
	% resample rate control
	%--
	
	case ('samplerate')
		
		slider_sync(obj,control.handles);	
		
		values = get_control_values(par.handle);
		
		control_update([],par.handle,'samplerate',round(values.samplerate));
	
end
