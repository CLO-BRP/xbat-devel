function time = get_slider_time(sound, time, backwards)

% get_slider_time - get slider time from sound time
% -------------------------------------------------
%
% time = get_slider_time(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - sound time
%
% Output:
% -------
%  time - slider time

if nargin < 3 || isempty(backwards)
	backwards = 0;
end

if has_sessions_enabled(sound) && xor(sound.time_stamp.collapse, backwards)
	time = get_recording_time(sound, time);
end