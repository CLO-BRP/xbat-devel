function root = get_attributes_root(context)

% get_attributes_root - get attributes directory relative to input path
% ---------------------------------------------------------------------
%
% root = get_attributes_root(context)
% 
% Input:
% ------
%  context - context (includes sound and container library)
%
% Output:
% -------
%  root - attributes path


% NOTE: also check that we can write with the sound? perhaps create_dir,
% except that it may exist in non-writeable media, which if it contains the
% attributes means we cannot edit them???


%--
% handle sound input
%--

% NOTE: placing attributes with the data provides universal access

if strcmpi(context.sound.type, 'file stream')	
	root = [context.sound.path, filesep, '__XBAT', filesep, 'Attributes'];
else
	root = fullfile(context.library.path, sound_name(context.sound), 'Attributes');
end
