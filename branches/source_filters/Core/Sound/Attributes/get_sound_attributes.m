function attr = get_sound_attributes(context)

%--
% create empty attribute
%--

attr.name = ''; attr.value = struct; attr = empty(attr);

%--
% get known attribute filenames
%--

ext = get_extensions('sound_attribute');

if isempty(ext);
	return;
end

files = strcat({ext.name}, attribute_file_ext);

%--
% scan attributes root for attribute files
%--

root = get_attributes_root(context);

contents = dir(root);

if isempty(contents)
	return;
end

for k = 1:length(contents)
	
	%--
	% skip unrecognized files
	%--
	
	ix = find(strcmp(contents(k).name, files));
	
	if isempty(ix)
		continue;
	end
	
	%--
	% add attribute to array
	%--
	
	attr(end + 1).name = ext(ix).name;
	
	file = get_attribute_file(context, ext(ix).name);
	
	attr(end).value = []; 

	try
		attr(end).value = ext(ix).fun.load(file, context);
	catch
		extension_warning(ext(ix), 'Load failed.', lasterror);
	end
	
end
