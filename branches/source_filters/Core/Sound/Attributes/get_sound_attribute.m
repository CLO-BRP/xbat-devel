function value = get_sound_attribute(sound, name)

% get_sound_attribute - get sound attribute value from name
% ---------------------------------------------------------
%
% value = get_sound_attribute(sound, name)
%
% Input:
% ------
%  sound - sound
%  name - attribute name
%
% Output:
% -------
%  value - attribute value

value = [];

ix = find(strcmp(name, {sound.attributes.name}));

if isempty(ix)
	return;
end

value = sound.attributes(ix).value;
