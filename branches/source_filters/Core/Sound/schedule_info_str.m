function [str] = schedule_info_str(schedule)

% schedule_info_str - get string list for recording schedule
% ----------------------------------------------------------
%
% str = schedule_info_str(schedule)
%
% Inputs:
% -------
%  schedule - array of scheduled sessions
%
% Outputs:
% --------
%  str - each session is in its own cell

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1982 $
% $Date: 2005-10-24 11:59:36 -0400 (Mon, 24 Oct 2005) $
%--------------------------------

for k = 1:length(schedule)
	
	str{k} = ['[', sec_to_clock(schedule(k).start), ' - ', sec_to_clock(schedule(k).end), ']'];
	
end