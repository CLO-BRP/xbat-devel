function [value,type] = is_sound_type(type)

% is_sound_type - check and normalize sound type string
% -----------------------------------------------------
%
% [value,type] = is_sound_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - valid type indicator
%  type - normalized type string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2201 $
% $Date: 2005-12-06 08:15:05 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% check type
%--

if (~ischar(type))
	value = 0; type = ''; return;
end

%--
% add some type string flexibility
%--

type = lower(strtrim(type));

%----------------------------
% CHECK AND NORMALIZE
%----------------------------

types = sound_types;

ix = find(strcmp(types,type));

if (isempty(ix))
	value = 0; type = ''; return;
end

value = 1;