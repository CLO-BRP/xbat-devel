function [file, ix] = get_current_file(sound, time)

% get_current_file - get file at specific time
% --------------------------------------------
%
% [file, ix] = get_current_file(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - sound time
%
% Output:
% -------
%  file - file at sound time
%  ix - index of file in sound files

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% check input time to be non-negative
%--

if (time < 0)
	error('Sound times are always positive.');
end

%--
% single file sounds are special
%--

if ischar(sound.file)
	ix = 1; file = sound.file; return;
end

%---------------------------
% GET CURRENT FILE
%---------------------------

%--
% get file start sound times
%--

start = get_file_times(sound);

%--
% locate time within file start times
%--

ix = find(time >= start,1,'last'); file = sound.file{ix};