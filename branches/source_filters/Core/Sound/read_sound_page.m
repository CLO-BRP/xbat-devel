function page = read_sound_page(sound, page, channels)

% read_sound_page - read scan page from sound
% -------------------------------------------
%
% page = read_sound_page(sound, page, channels)
%
% Input:
% ------
%  sound - sound
%  page - scan page
%  channels - channels
%
% Output:
% -------
%  page - sound page (contains samples and channels information)

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default to read all channels
%--

if (nargin < 3)
	channels = [];
end

%-------------------
% READ SOUND PAGE
%-------------------

%--
% add channels to page
%--

page.channels = channels;

%--
% read page samples and add to page, also add sample rate
%--

page.samples = sound_read(sound, 'time', page.start, page.duration, page.channels);

page.rate = get_sound_rate(sound);
