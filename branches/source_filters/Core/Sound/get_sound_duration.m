function duration = get_sound_duration(sound)

% get_sound_duration - get duration using sessions
% ------------------------------------------------
%
% duration = get_sound_duration(sound)
%
% Inputs:
% -------
% sound - an XBAT sound structure
%
% Outputs:
% --------
% duration - the duration of the sound (in seconds)

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

if isempty(sound)
	duration = 0; return;
end

%--
% handle multiple inputs recursively
%--

if numel(sound) > 1
	
	duration = [];
	
	for k = 1:numel(sound)
		duration(k) = get_sound_duration(sound(k));
	end
	
	return;
	
end
	
%--
% get recording duration if there are no sessions
%--

if (isempty(sound.time_stamp) || ~sound.time_stamp.enable)
	duration = sound.duration; return;
end

%--
% get duration based on sessions state
%--

% NOTE: the result of this function is used as the slider duration

sessions = get_sound_sessions(sound);

duration = sessions(end).end;