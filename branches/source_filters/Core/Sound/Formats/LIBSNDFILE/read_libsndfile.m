function [X,opt] = read_libsndfile(f,ix,n,ch,opt)

% read_libsndfile - read samples from sound file
% ----------------------------------------------
%
% [X,opt] = read_libsndfile(f,ix,n,ch,opt)
%
% Input:
% ------
%  f - file info
%  ix - initial sample
%  n - number of samples
%  ch - channels to select
%  opt - conversion options
%
% Output:
% -------
%  X - samples from sound file
%  opt - updated conversion options

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 587 $
% $Date: 2005-02-22 23:28:55 -0500 (Tue, 22 Feb 2005) $
%--------------------------------

%------------------------------------
% READ SAMPLES FROM FILE
%------------------------------------

%--
% get file from file info
%--

% NOTE: in this case we only need the file name

f = f.file;

%--
% convert file string to C string
%--

% NOTE: this string replacement handles network files

str = strrep(f,'\\','??');

str = strrep(str,'\','\\');

str = strrep(str,'??','\\');

%--
% create sample class prototype for configuring MEX if needed
%--

if ((nargin < 5) || isempty(opt) || isempty(opt.class))
	
	c = [];
	
else
	
	switch (opt.class)
		
		case ('double'), c = double([]);
			
		case ('single'), c = single([]);
			
		case ('int16'), c = int16([]);
			
		case ('int32'), c = int32([]);
			
		otherwise, error(['Unsupported sample type ''', opt.class, '''.']);
			
	end
	
end

%--
% get samples using mex
%--

X = sound_read_(str,ix,n,int16(ch),c)'; % note the transpose

% X = X(:,ch);
