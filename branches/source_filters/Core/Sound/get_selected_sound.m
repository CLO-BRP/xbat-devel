function [sound, lib] = get_selected_sound(pal)

% get_selected_sound - get sound selected in XBAT palette
% -------------------------------------------------------
%
% [sound, lib] = get_selected_sound(pal)
%
% Input:
% ------
%  pal - palette
%
% Output:
% -------
%  sound - sound
%  lib - library containing sound

if ~nargin || isempty(pal)	
	pal = get_palette(0, 'XBAT');
end

if isempty(pal)
	return;
end

lib = get_active_library;

sound = {};

%--
% get selected sounds in XBAT palette
%--

name = get_control(pal, 'Sounds', 'value');

for k = 1:length(name)
	sound{k} = get_library_sounds(lib, 'name', name{k});
end

sound = [sound{:}];

