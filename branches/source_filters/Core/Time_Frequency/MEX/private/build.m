

clear functions

%--
% build double precision DLL
%--

build_mex('../fast_specgram_mex.c', '-output "../../private/fast_specgram_mex_double.dll"', '-lfftw3');

%--
% build single precision DLL
%--

build_mex('../fast_specgram_mex.c', '-DFLOAT', '-output "../../private/fast_specgram_mex_single.dll"', '-lfftw3f');

