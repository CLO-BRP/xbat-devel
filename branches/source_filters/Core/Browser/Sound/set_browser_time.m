function time = set_browser_time(par, time, mode, data)

% set_browser_time - set browser page-start time
% ----------------------------------------------
%
% set_browser_time(par, time)
%
% Input:
% ------
%  par - parent browser handle
%  time - time
%
% Output:
% -------
%  time - browser slider time

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

if isempty(par)
	
	par = get_active_browser;
	
	if isempty(par)
		return;
	end
	
end

if nargin < 3
	mode = [];
end

%-------------------------------
% SET TIME
%-------------------------------

set_browser_page(par, mode, 'time', time);
