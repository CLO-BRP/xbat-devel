function ext = get_callback_extension(callback) 

% get_callback_extension - get extension from callback context
% ------------------------------------------------------------
%
% ext = get_callback_extension(callback)
%
% Input:
% ------
%  callback - callback context
%
% Output:
% -------
%  ext - extension in callback context

%--
% get extension type from palette tag
%--

tag = parse_tag(callback.pal.tag, '::', {'ignore', 'type', 'ignore'});

type = lower(tag.type);

%--
% get extension from parent browser if possible
%--

if ~isempty(callback.par.handle)
	ext = get_browser_extension(type, callback.par.handle, callback.pal.name);
else
	ext = get_extension(type, callback.pal.name, callback.pal.handle);
end

