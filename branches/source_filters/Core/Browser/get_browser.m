function value = get_browser(par, field, data)

% get_browser - get browser state
% -------------------------------
%
%  data = get_browser(par)
%
% value = get_browser(par, field)
%
% Input:
% ------
%  par - browser handle
%  field - field name
%  data - stored state
%
% Output:
% -------
%  data - stored state
%  value - field value

% NOTE: this function provides uniform access to browser state and fields

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% set default parent
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

% NOTE: return if there is no parent available

if isempty(par) 
	value = []; return;
end

%--
% get browser data and return
%--

if (nargin < 2)
	value = get(par, 'userdata'); return;
end

%--
% be flexible about field names
%--

% NOTE: allow spaces instead of periods and be case insensitive

if any(field == ' ')
	field = lower(strrep(field, ' ', '.'));
end

%------------------------------------------
% GET BROWSER FIELD
%------------------------------------------

switch field

	%--
	% special fields
	%--

	% NOTE: special fields are computed or stored in odd way

	%--
	% stored fields
	%--

	otherwise

		% NOTE: get state store if needed

		if (nargin < 3)
			data = get_browser(par);
		end

		% NOTE: we could use default value safety, but we prefer failure
		
		value = get_field(data.browser, field);

end


