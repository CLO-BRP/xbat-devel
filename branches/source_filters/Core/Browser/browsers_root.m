function root = browsers_root

% browsers_root - browsers root directory
% ---------------------------------------
%
% root = browsers_root
%
% Output:
% -------
%  root - root directory for browsers

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% TODO: develop'set' and 'get', possibly allow multiple roots

root = [xbat_root, filesep, 'Core', filesep, 'Browser'];

