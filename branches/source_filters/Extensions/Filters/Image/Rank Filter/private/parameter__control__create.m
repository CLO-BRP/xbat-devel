function control = parameter__control__create(parameter,context)

% RANK FILTER - parameter__control__create

%--
% get size of structuring element
%--

% NOTE: create structuring element using parameters and compute size

n = se_size(create_se(parameter));

%--
% add leading rank control
%--

control(1) = control_create( ...
    'name','rank', ...
    'alias','Rank', ...
	'style','slider', ...
	'min',1, ...
	'max',n, ...
	'slider_inc',[1,2], ...
	'value',parameter.rank ...
);

control(end + 1) = control_create( ...
	'style','separator' ...
);

%--
% append parent controls
%--

fun = parent_fun;

par_control = fun(parameter,context);

control = [control, par_control];