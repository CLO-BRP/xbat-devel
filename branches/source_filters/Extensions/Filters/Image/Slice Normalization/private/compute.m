function [X, context] = compute(X, parameter, context)

% SLICE NORMALIZATION - compute

X = normalize_dim(X, 1, parameter);

% rows = size(X, 1);
% 
% if parameter.center
% 	X = X - (ones(rows, 1) * mean(X, 1));
% end
% 
% if parameter.normalize
% 	X = X ./ (ones(rows, 1) * sqrt(sum(X.^2, 1)));
% end