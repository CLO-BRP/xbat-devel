function parameter = parameter__create(context)

% SLICE NORMALIZATION - parameter__create

parameter = struct;

parameter.center = 1;

parameter.normalize = 1;