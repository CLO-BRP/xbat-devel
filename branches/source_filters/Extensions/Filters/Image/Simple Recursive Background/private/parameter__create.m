function parameter = parameter__create(context)

% SIMPLE RECURSIVE BACKGROUND - parameter__create

parameter = struct;

parameter.output = {'signal'}; 

parameter.half_life = 0.5;
