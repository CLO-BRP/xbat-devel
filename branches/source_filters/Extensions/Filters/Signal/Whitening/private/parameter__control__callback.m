function result = parameter__control__callback(callback, context)

% WHITENING - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'use_log'
		
		value = get_control(callback.pal.handle, callback.control.name, 'value');
			
		set_control(callback.pal.handle, 'noise_log', 'enable', value);
		
end

