function log = get_noise_log(parameter, context)

% TODO: check if the context contains library, etc.

%--
% get log name
%--

name = parameter.noise_log;

if isempty(name)
	log = []; return;
end

if iscell(name)
	name = name{1};
end

%--
% get log from browser
%--

log = get_browser_logs(context.par, 'name', name);