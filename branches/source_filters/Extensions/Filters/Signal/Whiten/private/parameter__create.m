function parameter = parameter__create(context)

% WHITEN - parameter__create

fun = parent_fun; parameter = fun(context);

parameter.order = 16;

% NOTE: the next two parameters regularize the inverse filter in different ways

parameter.r = 0;

nyq = 0.5 * get_sound_rate(context.sound);

parameter.max_freq = 0.75 * nyq;

parameter.lowpass = 0;

parameter.noise_log = [];

parameter.use_log = 0;


