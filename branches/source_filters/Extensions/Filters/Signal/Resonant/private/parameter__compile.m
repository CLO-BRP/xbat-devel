function [parameter, context] = parameter__compile(parameter, context)

% RESONANT - parameter__compile

nyq = get_sound_rate(context.sound) / 2;

%--
% compute numerator and denominator polynomials
%--

theta = pi * parameter.center_freq / nyq;

r = (nyq - parameter.width) / nyq;

parameter.filter.a = [1, -2 * sqrt(r) * cos(theta), r]; 

parameter.filter.b = (1 - r) * sin(theta);



