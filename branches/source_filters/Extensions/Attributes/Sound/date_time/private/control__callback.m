function result = control__callback(callback, context)

% DATE_TIME - control__callback

result = [];

%--
% enforce validity of date string
%--

value = get_control(callback.pal.handle, 'datetime', 'value');

try
	datevec(value);
catch
	set_control(callback.pal.handle, 'datetime', 'string', datestr(context.attribute.datetime, 0));
end