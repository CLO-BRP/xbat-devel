function control = control__create(attribute, context)

% DATE_TIME - control__create

control = empty(control_create);

string = datestr(attribute.datetime, 0);

control(end + 1) = control_create( ...
	'style', 'edit', ...
	'name', 'datetime', ...
	'string', string ...
);
	
		