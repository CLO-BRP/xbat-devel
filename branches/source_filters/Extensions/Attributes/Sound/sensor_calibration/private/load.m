function attribute = load(store, context)

% SENSOR_CALIBRATION - load

attribute = struct;

attribute.calibration = sensor_calibration(store);
