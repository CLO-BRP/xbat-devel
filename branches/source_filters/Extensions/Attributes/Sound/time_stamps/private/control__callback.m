function result = control__callback(callback, context, ix)

% TIME_STAMPS - control__callback

if nargin < 3
	ix = [];
end

result = [];

%--
% retrieve time stamps and axes handle
%--

time_stamps = get_control(callback.pal.handle, 'time_stamps', 'value');

ax_han = get_control(callback.pal.handle, 'time_stamps', 'handles');

%--
% load time stamps into 'fishbowl' if necessary
%--

if isempty(time_stamps) || ~isstruct(time_stamps)
	
	time_stamps = context.attribute;
	
	set_control(callback.pal.handle, 'time_stamps', 'value', time_stamps);
	
end

%--
% get index of currently-selected stamp
%--

list_han = get_control(callback.pal.handle, 'scan', 'handles');

if isempty(ix)

	str = get(list_han.obj, 'string');

	value = get_control(callback.pal.handle, 'scan', 'value');

	if numel(value) == 0
		value = '';
	else
		value = value{1};
	end

	ix = find(strcmp(str, value));

	if isempty(ix)
		ix = 1;
	end

end

%--
% possibly edit time stamps
%--

switch callback.control.name
	
	case 'record_time'
		
		value = get_control(callback.pal.handle, callback.control.name, 'value');
		
		time_stamps.table(ix, 1) = value;
		
	case 'maps_to'
		
		value = get_control(callback.pal.handle, callback.control.name, 'value');
		
		time_stamps.table(ix, 2) = clock_to_sec(value);	
		
	otherwise
		
		set_control(callback.pal.handle, 'record_time', ...
			'value', time_stamps.table(ix, 1) ...
		);
	
		set_control(callback.pal.handle, 'maps_to', ...
			'string', sec_to_clock(time_stamps.table(ix, 2)) ...
		);
		
end

%--
% store edited time_stamps
%--

set(ax_han.obj, 'userdata', time_stamps);

%--
% get sessions
%--

context.sound.time_stamp = time_stamps;

sessions = get_sound_sessions(context.sound);

%--
% display sessions
%--

display_sessions(ax_han.obj, sessions, ix, callback, context);

%--
% update selection box
%--

str = time_stamps_info_str(time_stamps.table);

set(list_han.obj, ...
	'string', time_stamps_info_str(time_stamps.table), ...
	'value', ix ...
);


%--------------------------------
% PARSE_TIME_STAMP_STR
%--------------------------------

function [record, real] = parse_time_stamp_str(str)

info = parse_tag(str, ' -> ', {'record', 'real'});

record = clock_to_sec(info.record); 

real = clock_to_sec(info.real);


%--------------------------------
% DISPLAY_SESSIONS
%--------------------------------

function display_sessions(ax, sessions, selected, callback, context)

if nargin < 3
	selected = [];
end

set(ax, ...
	'layer', 'top', ...
	'box', 'on', ...
	'xlim', [0, max([sessions.end])], ...
	'ylim', [0, 1], ...
	'xtick', [], ...
	'ytick', [] ... 
);

delete(get(ax, 'children'));

callback.control.name = '';

for k = 1:length(sessions)
	
	start = sessions(k).start; stop = sessions(k).end;
	
	if k == selected
		color = [1, 0, 0];
	else
		color = [0, 0, 0.9];
	end
		
	patch('parent', ax, ...
		'xdata', [start, stop, stop, start, start], ...
		'ydata', [0, 0, 1, 1, 0], ...
		'facecolor', (4 * get(ax, 'color') + color) / 5, ...
		'buttondownfcn', {@session_patch_callback, callback, context, k}, ... 
		'facealpha', 1 ...
	);


end

set(ax, 'box', 'on');


function session_patch_callback(obj, eventdata, callback, context, k)

control__callback(callback, context, k);
