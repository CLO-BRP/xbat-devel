function save(attribute, store, context)

% TIME_STAMPS - save

lines{1} = 'sound time, stamp time';

lines{2} = 'stamps';

table = attribute.table;

for k = 1:length(table)	
	lines{end + 1} = [num2str(table(k, 1)), ', ' num2str(table(k, 2))];
end

file_writelines(store, lines);
