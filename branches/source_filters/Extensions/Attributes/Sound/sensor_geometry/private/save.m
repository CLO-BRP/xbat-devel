function save(attribute, store, context)

% SENSOR_GEOMETRY - save

result = context.dialog_result;

%--
% check for toolbox
%--

if isempty(which('m_ll2xy'))
	attribute.global = [];
end

%--
% check for save format
%--

switch result.units{1}
	
	case 'lat-lon'
	
		lines{1} = 'lat, lon, elev';

		lines{2} = ['ll, ', attribute.ellipsoid];
	
		type = 'global';
	
	otherwise
	
		lines{1} = 'x, y, z';
	
		lines{2} = 'xyz';
	
		type = 'local';
	
end

%--
% write geometry to lines
%--

[ignore, ref] = min(sum(attribute.local.^2, 2));

for k = 1:size(attribute.local, 1)
	
	line = '';
	
	if k == ref
		line = '*';
	end
		
	for j = 1:3
	
		if numel(attribute.(type)(k, :)) >= j
			value = attribute.(type)(k, j);
		else
			value = 0;
		end
					
		line = [line, num2str(value), ', '];
		
	end
	
	line(end-1:end) = [];
	
	lines{end + 1} = line;
	
end
	
file_writelines(store, lines);


