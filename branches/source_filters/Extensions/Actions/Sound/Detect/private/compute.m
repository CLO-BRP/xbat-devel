function [result, context] = compute(sound, parameter, context)

% DETECT - compute

result = [];

ext = get_extension('sound_detector', parameter.detector{1});

preset = preset_load(ext, parameter.preset{1});

%--
% create new log
%--

log_name = parameter.output;

log_name = token_replace(log_name, '%', ...
	'SOUND_NAME', sound_name(sound), 'PRESET_NAME', parameter.preset{1} ...
);

log = new_log(log_name, context.user, context.library, sound);

%--
% scan into log
%--

% NOTE: detector_scan saves the log so we don't need to

log = detector_scan(preset.ext, sound, [], [], log);







%-----------------------------------------------
% TOKEN_REPLACE
%-----------------------------------------------

function str = token_replace(str, sep, varargin)

[field, value] = get_field_value(varargin);

for k = 1:length(field)
	
	tok = [sep, field{k}, sep];
	
	str = strrep(str, tok, value{k});
	
end