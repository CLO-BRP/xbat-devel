function [result, context] = compute(event, parameter, context)

% CLIP - compute

%--
% write clip to file
%--

% NOTE: the clip file is the result

result.file = [parameter.output, filesep, event.userdata, '_', int2str(event.id), '.', parameter.format{1}];

sound_file_write(result.file, event.samples, event.rate);
