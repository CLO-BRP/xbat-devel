function control = parameter__control__create(parameter, context)

% CLIP - parameter__control__create

control = empty(control_create);

%--
% tabs
%--

% TODO: the space of the preceding header must be updated based on the type
% of control that follows

tabs = {'File', 'Padding'};

% control(end + 1) = control_create( ...
% 	'style', 'tabs', ...
% 	'tab', tabs ...
% );

%--
% format
%--

% NOTE: the next two lines get writeable format file extensions

formats = get_writeable_formats;

for k = 1:length(formats)
	ext{k} = formats(k).ext{1};
end

ext = upper(ext); value = find(strcmpi(ext, parameter.format));

if isempty(value)
	value = 1;
end

control(end + 1) = control_create( ...
	'name', 'format', ...
	'style', 'popup', ...
	'tab', tabs{1}, ...
	'string', ext, ...
	'value', value ...
);

%--
% output
%--

control(end + 1) = control_create( ...
	'name', 'output', ...
	'alias', 'output directory', ...
	'style', 'edit', ...
	'tab', tabs{1}, ...
	'lines', 3, ...
	'string', parameter.output ...
);

%--
% show files
%--

control(end + 1) = control_create( ... 
	'name', 'show_files', ...
	'style', 'checkbox', ...
	'tab', tabs{1}, ...
	'value', parameter.show_files ...
);

control(end).space = 1.5;

% control(end + 1) = control_create( ...
% 	'name', 'browse ...', ...
% 	'style', 'buttongroup', ...
% 	'lines', 1.5, ...
% 	'width', 0.5, ...
% 	'align', 'right' ...
% );
