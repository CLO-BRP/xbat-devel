function [handles, context] = on__play__stop(widget, data, parameter, context)

% SCOPE - on__play__stop

handles = [];

ax = scope_axes(widget);

set(get(ax(1), 'title'), 'string', '');

delete(scope_line(ax));