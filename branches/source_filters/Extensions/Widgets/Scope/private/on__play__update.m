function [handles, context] = on__play__update(widget, data, parameter, context)

% SCOPE - on__play__update

handles = [];

%--
% update axes time display
%--

ax = scope_axes(widget);

current = get_play_time(context.par); 

set(get(ax(1), 'title'), 'string', sec_to_clock(current));

%--
% get scope data
%--

% NOTE: return if we can't get data to display

if isempty(data.buffer)
	return;
end

% NOTE: the step seems like a parameter

step = 0.01; rate = get_sound_rate(context.sound); n = floor(step * rate);

time = linspace(current, current + step, n);

buffer = data.buffer;

try
	samples = buffer.samples(buffer.ix:(buffer.ix + n - 1), :);
catch
	samples = zeros(n, 2);
end

%--
% update display
%--

handles = scope_line(ax);

% NOTE: consider only updating 'ydata' here, and no update on axes

set(handles(1), ...
	'xdata', time, ...
	'ydata', samples(:, 1) ...
);

if length(handles) > 1
	
	if size(samples, 2) > 1
		samples = samples(:, 2);
	end
	
	set(handles(2), ...
		'xdata', time, ...
		'ydata', samples ...
	);

end

scope_axes(widget, ...
	'xlim', [current, current + step] ...
);
