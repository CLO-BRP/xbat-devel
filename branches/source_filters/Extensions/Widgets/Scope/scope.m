function ext = scope

ext = extension_create;

ext.short_description = 'Waveform scope display';

ext.category = {''};

ext.version = '0.2';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

