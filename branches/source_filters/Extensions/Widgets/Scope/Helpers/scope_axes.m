function handle = scope_axes(widget, varargin)

handle(1) = create_axes(widget, 'scope_axes::1', varargin{:});

handle(2) = create_axes(widget, 'scope_axes::2', varargin{:});