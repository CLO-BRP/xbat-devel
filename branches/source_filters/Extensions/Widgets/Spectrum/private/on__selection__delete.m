function [handles, context] = on__selection__delete(widget, data, parameter, context)

% SPECTRUM - on__selection__delete

handles = [];

%--
% delete selection lines and frequency guides
%--

ax = spectrum_axes(widget);

delete(selection_line(ax));

delete(frequency_guides(ax));

