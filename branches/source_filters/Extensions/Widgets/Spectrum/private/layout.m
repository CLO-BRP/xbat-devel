function handles = layout(widget, parameter, context)

% SPECTRUM - layout

%--
% layout axes and get handles
%--

layout = layout_create(1); layout.margin(1) = 0.5;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

set(handles(1), 'tag', 'spectrum_axes');

%-------------
% TEST CODE
%-------------

%--
% set display options
%--

% TODO: this should happen at a higher level

color = context.display.grid.color;

set(handles, 'xcolor', color, 'ycolor', color);