function [handles, context] = on__play__update(widget, data, parameter, context)

% SPECTRUM - on__play__update

handles = [];

ax = spectrum_axes(widget);

%--
% update time display
%--

time = get_play_time(context.par); 

set(get(ax, 'title'), 'string', sec_to_clock(time));

%--
% update current spectrum
%--

im = data.browser.images;

if iscell(im)
	im = im{1};
end

im = im(1);

slice = get_spectrogram_slices(im, time);

if isempty(slice)
	return;
end

%--
% update display
%--

nyq = get_sound_rate(context.sound) / 2;

play_line(ax, ...
	'xdata', linspace(0, nyq, numel(slice)), ...
	'ydata', slice ...
);

