function [handles, context] = on__play__update(widget, data, parameter, context)

% SIMILARITY MATRIX - on__play__update

handles = [];

%--
% get relevant times
%--

page = data.page;

time = [page.start, page.start + page.duration];

current = get_play_time(context.par); 

%--
% update cursors
%--

ax = similarity_matrix_axes(widget);

if isempty(ax)
	return;
end

get_cursor(ax, 1, ...
	'xdata', current * ones(1,2), ...
	'ydata', time ...
);

get_cursor(ax, 2, ...
	'ydata', current * ones(1,2), ...
	'xdata', time ...
);