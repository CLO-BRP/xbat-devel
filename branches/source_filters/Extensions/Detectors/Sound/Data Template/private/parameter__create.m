function parameter = parameter__create(context)

% DATA TEMPLATE - parameter__create

% TODO: context must include sound and user (consider user preferences)

%--
% templates
%--

% TODO: develop clips as part of the system

parameter.templates = [];

%--
% correlation threshold and extreme deviation test
%--

parameter.thresh = 0.4;

parameter.thresh_test = 1;

parameter.deviation = 3;

parameter.deviation_test = 0;

%--
% template masking
%--

% NOTE: there are other parameters used for masking

% TODO: add further masking parameters, they will be hidden

parameter.mask = 0;

parameter.mask_percentile = 0.6;
