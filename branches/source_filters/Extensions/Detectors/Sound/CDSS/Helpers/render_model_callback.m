function [out, info] = render_model_callback(obj, eventdata, store, context)

% render_model_callback - callback to render model to file
% --------------------------------------------------------
%
% [out, info] = render_model_callback(obj, eventdata, store, context)
%
% Input:
% ------
%  store - handle to object storing model
%  context - extension context
%
% Output:
% -------
%  out - full output filename
%  info - output file info

%--
% get model data, this contains events and components
%--

data = get(store, 'userdata');

%--
% create and play model signal
%--

[signal, rate] = get_model_signal(data, context);

%--
% create prompt user for filenames
%--

[out1, out2] = uiputfile({'*.wav',  'WAV-files (*.wav)'}, 'Render Model Signal To ...');

if isequal(out1, 0)
	return;
end

out = [out2, out1];

%--
% create file
%--

wavwrite(signal, rate, 16, out);

if (nargout > 1)
	info = dir(out);
end