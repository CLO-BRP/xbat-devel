function parameter = parameter__create(context)

% NOISE - parameter__create

parameter = struct;

parameter.min_length = 0.1;

parameter.threshold = 0.03; 