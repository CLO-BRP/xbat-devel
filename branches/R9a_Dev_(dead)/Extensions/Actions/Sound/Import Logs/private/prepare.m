function [result, context] = prepare(parameter, context)

% Import Logs - prepare

%initializations
result = struct;
context.state.kill = 0;

%-------------------------------------------------------------------------
%set source path
%-------------------------------------------------------------------------
preset_path = fullfile( ...
  xbat_root, 'Extensions', 'Actions', 'Sound', context.ext.name, 'private', 'preset.mat');

if exist(preset_path, 'file')  
  load(preset_path);  
else  
  RootPath = '';  
end

[LogList, RootPath] = uigetfile({'*.mat','XBAT Logs'}, ...
	                             'Select logs', ...
															 RootPath,  ...
															 'MultiSelect', 'on');

%stop execution if no log select
if isequal(LogList, 0)  
  context.state.kill = 1;  
  return;  
end

save(preset_path, 'RootPath');


%pass data to compute.m
if ~iscell(LogList)
  LogList = {LogList};
end
context.state.RootPath = RootPath;
context.state.LogList = LogList;
context.state.NumLogs = length(LogList);
context.state.flag = 0;

