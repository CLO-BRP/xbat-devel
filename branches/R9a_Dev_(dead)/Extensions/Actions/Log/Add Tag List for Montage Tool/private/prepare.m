function [result, context] = prepare(parameter, context)
result = struct;

%% REFRESH - prepare

% Log action refreshes structure variable name and file field for XBAT 
% logs, using the file name of the log.
%
% Allows XBAT actions to be run on XBAT logs whose names have been changed
% without having to first open them in XBAT. 
%

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. 

% Change Log
%
% 1. THIS VERSION IS NOT CONFUSED BY HAVING MULTIPLE LOGS WITH THE SAME FILE
%    NAME IN MULTIPLE XBAT LIBRARY FOLDERS

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

%initializations
context.state.kill = 0;

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

%% condition tag list

%delete leading and trailing blanks in each tag
parameter.tag_list = strtrim(parameter.tag_list);

%convert renaming blanks into underlines
for i = 1:length(parameter.tag_list)
  parameter.tag_list{i}(isspace(parameter.tag_list{i})) = '_';
end

%get rid of duplicate tags, maintaining the same tag order
[~, IDX] = unique(parameter.tag_list);

IDX = sort(IDX);

parameter.tag_list = parameter.tag_list(IDX);

disp(' ')
disp('Tag list used (after conditioning):')

for i = 1:length(parameter.tag_list)
  
  fprintf('  %s\n', parameter.tag_list{i});
  
end


%% process logs

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;
    
    %add tag list for Montage Tool
    NewLog.userdata.tag_list = parameter.tag_list;
     
%     NewLog.userdata.tag_list = ...
%       {'noise'; ...
%        'BWHD'; ...
%        'amb'};

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end


%%

% disp('**********************************************************')
% disp('')
% disp('ADD TAG LIST FOR MONTAGE TOOL log action has finished adding tag lists.')
% disp('')
% disp('Stop process before memory leak engages.')
% disp('')
% disp('**********************************************************')

