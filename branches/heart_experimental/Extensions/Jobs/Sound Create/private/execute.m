function [job, context] = execute(job, parameter, context)

% SOUND CREATE - execute

result = struct;

db_disp; flatten(job), parameter

% TODO: to complete this job we have to -
%
% - make sure we have the user and make the user current, 
% - create a BARN library for user and make it current to it,
% - create sound in BARN library



function job = job_create_sound( store, current )

% job_create_sound - for BARN
% ----------------------------
%
%  job = job_create_sound( store, current )
%
% Input:
% ------
%  store - with jobs
%  current - struct for job
%
% Output:
% -------
% job

user = get_database_objects(store, 'user', current.user_id);

file = get_database_objects(store, 'data_file', current.job.id);

db_disp; disp(file)

for j = 1:numel(file)
	sound = create_sound_from_file(file(j), user, store, server)
	
% 	add_sounds
end

update_job(store, 'id', current.id, ...
	'progress', 1, ...
	'report', ['Sound created.'] ...
);


%-----------------------------------------
% HELPERS
%-----------------------------------------

%-----------------------------
% CREATE_SOUND_FROM_FILE
%-----------------------------

function sound = create_sound_from_file(file, user, store, server)

%--
% make sure we have access to the file
%--

db_disp; file

if ~data_file_available(file)
	
	file.location = map_data_file_location(file, user, store, server);
	
	disp(file)
	
	if ~data_file_available(file)
		return;
	end
end

db_disp; file

%--
% create sound
%--

if file.directory
	sound = sound_create('file stream', file.location);
else
	sound = sound_create('file', file.location);
end

%--
% add to default user library
%--

% NOTE: this is the sound we need to compute upon

lib = get_libraries(get_barn_user(user), 'name', 'Default');

add_sounds(sound, lib);

%--
% add to barn
%--

sound = set_barn_sound(store, sound, user);


%-----------------------------
% DATA_FILE_AVAILABLE
%-----------------------------

function available = data_file_available(file)

if file.directory
	available = exist(file.location, 'dir');
else
	available = exist(file.location, 'file');
end


%-----------------------------
% MAP_DATA_FILE_LOCATION
%-----------------------------

function location = map_data_file_location(file, user, store, server)

% NOTE: the file locations are web server centric, how the web application acceses the files

% NOTE: these locations must be transformed to make sense in the context of the compute server

% NOTE: this is the user ftp file transformation

start = strfind(file.location, user.email);

location = fullfile(user_files_root(user, store, server), strrep(file.location(start + numel(user.email):end), '/', filesep));

