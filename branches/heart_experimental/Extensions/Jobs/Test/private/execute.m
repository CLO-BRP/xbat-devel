function [job, context] = execute(job, parameter, context)

% TEST - execute

fprintf('EXECUTE\n%s\n%s\n%s\n', ...
	['name: ', job.name], ...
	['daemon: ', job.daemon_guid], ... 
	['transaction: ', job.daemon_transaction] ...
);

for k = 1:20
	if parameter.delay
		start = clock;
		
		while etime(clock, start) < parameter.delay
			% NOTE: we are just waiting without 'pause'
		end
	end
	
	fprintf('.'); % NOTE: this is where the computations would go!
	
	job = update_job(context.store, job, 'progress', k * 5);
end

fprintf('\nCompleted job.\n\n');

job.daemon = ''; job.completed_at = datestr(now, 31);