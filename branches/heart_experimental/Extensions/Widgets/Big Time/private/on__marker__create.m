function [handles, context] = on__marker__create(widget, data, parameter, context)

% BIG TIME - on__marker__create

% data.marker.time

handles = display_big_time(widget, data.marker.time, context);

set(widget, 'name', [context.ext.name, ' (Marker)']);
