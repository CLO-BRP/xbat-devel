function [handles, context] = on__play__start(widget, data, parameter, context)

% BIG TIME - on__play__start

handles = []; 

%--
% indicate we are displaying play time
%--

set(widget, 'name', [context.ext.name, ' (Play)']);
