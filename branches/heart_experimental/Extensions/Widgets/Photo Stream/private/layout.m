function handles = layout(widget, parameter, context)

% PHOTO STREAM - layout

% TODO: add menu creation to this function, create an image navigation menu

% NOTE: include jump to start of image commands and perhaps an options to
% set the page duration to match the image duration

% NOTE: the application of the above to the general case should work from a
% file describing the image time-stamps and duration perhaps, off course
% off course this makes things less DRY

handles = create_axes(widget, 'container', 'position', [0, 0, 1, 1]);