function handle = spectrum_axes(widget, channel, varargin)

handle = create_axes(widget, ['spectrum_axes::', int2str(channel)], varargin{:});
