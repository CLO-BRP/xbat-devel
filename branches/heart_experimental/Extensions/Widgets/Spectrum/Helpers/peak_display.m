function handles = peak_display(par, x, y, name, opt)

% TODO: improve handle tracking and create clearing function

% NOTE: we could also choose to display peaks in a defined range, the event range

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set base options and update if needed
%--

base.guides = 1; 

base.marker = 'o';

base.color = [1 0 0]; 

base.peaks = 4; % NOTE: a value of zero for peaks displays all peaks

base.ylim = []; 

base.fast = 1; % NOTE: if we ask for a fast display, we skip setting of various properties

% NOTE: return base options if no input is provided

if ~nargin
	handles = base; return;
end

if nargin > 4
	opt = struct_update(base, opt);
else
	opt = base;
end

%--
% set default name
%--

if nargin < 4 || isempty(name)
	name = 'peak_display';
end

%-----------------------
% COMPUTE AND DISPLAY
%-----------------------

%--
% compute and possibly select peaks
%--

if isempty(x)
	
	% NOTE: this branch can be used to initialize a peak display with no data
	
	x = [nan, nan]; y = [nan, nan];
	
else
	
	ix = find(y(2:end - 1) > max(y(1:end - 2), y(3:end))) + 1;

	if isempty(ix)
		return;
	end

	x = x(ix); y = y(ix);

	% NOTE: a non-zeros peaks values tells us to select

	if opt.peaks && (opt.peaks < numel(x))

		% NOTE: for a value of 1 we can simply choose the max

		[y, p] = sort(y, 'descend'); x = x(p);

		x = x(1:opt.peaks); y = y(1:opt.peaks);

	end
		
end

%--
% display peaks
%--

handles = [];

[handles(end + 1), created] = create_line(par, [name, '::peaks'], ...
	'xdata', x(1:end), ...
	'ydata', y(1:end) ...
);

if created || ~opt.fast
	
	set(handles(end), ...
		'linestyle', 'none', ...
		'marker', opt.marker, ...
		'color', opt.color ...
	);

end

% NOTE: display top peak distinctly and display time and value

[handles(end + 1), created] = create_line(par, [name, '::peak'], ...
	'xdata', x(1), ...
	'ydata', y(1) ...
);

if created || ~opt.fast
	
	set(handles(end), ...
		'linestyle', 'none', ...
		'marker', opt.marker, ...
		'markersize', 12, ...
		'color', opt.color ...
	);

end

%--
% display guides
%--

if opt.guides
	
	if isempty(opt.ylim)
		opt.ylim = get(par, 'ylim');
	end

	range = opt.ylim + [-1, 1]; 
	
	for k = 1:1 % numel(x)
		
		[handles(end + 1), created] = create_line(par, [name, '::guides', int2str(k)], ...
			'xdata', x(k) * ones(1, 2), ...
			'ydata', range ...
		);
	
		if created || ~opt.fast
			
			set(handles(end), ...
				'linestyle', ':', ...
				'marker', 'none', ...
				'color', opt.color ...
			);
		
		end
	
	end
	
end

%--
% display peak label
%--

% TODO: display time and value for peak, this may clutter display when displaying marker and selection info

% NOTE: we only display a single peak label per parent, we disregard the 'name'

handle = findobj(par, 'tag', 'PEAK_LABEL_TEXT', 'type', 'text');

if isempty(handle)
	handle = par;
end

if isempty(opt.ylim)
	opt.ylim = get(par, 'ylim');
end
	
handles(end + 1) = display_label_text(handle, 'REPLACE ME', x(1), opt.ylim);

set(handles(end), ...
	'tag', 'PEAK_LABEL_TEXT', ...
	'userdata', [x(1), y(1)] ...
);


