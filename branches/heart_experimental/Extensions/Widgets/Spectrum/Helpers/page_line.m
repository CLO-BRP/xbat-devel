function [center, low, high] = page_line(ax, channel, varargin)

channel = int2str(channel);

center = create_line(ax, ['page_line::', channel], varargin{:});

low = create_line(ax, ['low_page_line::', channel], varargin{:}, 'linestyle', ':');

high = create_line(ax, ['high_page_line::', channel], varargin{:}, 'linestyle', ':');

if nargout < 3
	low = [low, high];
end
