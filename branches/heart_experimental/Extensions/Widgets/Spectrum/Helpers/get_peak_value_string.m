function str = get_peak_value_string(time, value, context)

% get_peak_value_string - get string to display as peak label
% -----------------------------------------------------------
%
% str = get_peak_value_string(time, value, context)
%
% Input:
% ------
%  time - browser time (slider time, event time)
%  value - frequency and intensity values in Hz and dB
%  context - browser context
%
% Output:
% -------
%  str - display string  

%--
% time information
%--

time = get_browser_time_string([], time, context);

%--
% frequency and intensity information
%--

% TODO: create other shortcut and hiding functions to talk to grid

if browser_using_khz([], context)
	unit = 'kHz'; value(1) = value(1) / 1000; 
else
	unit = 'Hz';
end

value = [num2str(value(1)), ' ', unit, ', ', num2str(value(2)), ' dB'];

%--
% assemble peak value string
%--

% NOTE: this string could be useful for non-peak locations as well in the future

str = [time, ', PEAK: ' value];


