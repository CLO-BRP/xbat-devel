function [handles, context] = on__selection__edit__update(widget, data, parameter, context)

% SPECTRUM - on__selection__edit__update

% TODO: factor code from 'on__selection__create' to allow for a more efficient use here

[handles, context] = on__selection__create(widget, data, parameter, context);