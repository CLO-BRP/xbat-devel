function handles = layout(widget, parameter, context)

% MAP_BASE - layout

%--
% layout axes
%--

harray(widget, layout_create(1));

handles = harray_select(widget, 'level', 1);

%--
% get map
%--

% TODO: should the center and level of the widget be parameters?

% NOTE: that would make these modifiable through the keypress function

% TODO: this widget will require the implementation of the 'resize' and 'keypress' events

parameter

get_size_in(handles, 'pixels')