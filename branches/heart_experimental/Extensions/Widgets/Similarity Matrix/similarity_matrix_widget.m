function ext = similarity_matrix_widget

ext = extension_create(mfilename);

ext.short_description = 'Matrix of slice similarities';

% ext.category = {};

ext.version = '0.1';

ext.guid = '2a66dcaa-7871-4453-8beb-6d66f854a1f0';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

