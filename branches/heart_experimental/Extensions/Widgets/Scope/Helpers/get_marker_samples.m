function samples = get_marker_samples(marker, parameter, context)

% NOTE: this can probably more general, but this is a little helper

%--
% get marker page
%--

page.start = marker.time - 0.5 * parameter.duration; 

% NOTE: should this derive from a marker property or scope parameter?

page.duration = parameter.duration;

%--
% get marker samples from context sound
%--

page = read_sound_page(context.sound, page, marker.channel);

page = filter_sound_page(page, context);

if isempty(page.filtered)
	samples = page.samples;
else
	samples = page.filtered;
end

%--
% window to taper at edges
%--

% TODO: consider tapering so that envelope computation will be more stable at edges

% taper = floor(0.05 * numel(samples));
% 
% window = repmat((linspace(0, 1, taper)').^2, 1, size(samples, 2));
% 
% samples(1:taper, :) = window .* samples(1:taper, :);
% 
% samples(end - taper + 1:end, :) = flipud(window) .* samples(end - taper + 1:end, :);