function ylim = get_scope_ylim(samples, parameter)

%--
% set width allowance in number of sample deviations
%--

% TODO: turn this into a parameter, and consider others

width = 6;

%--
% compute center and scale for positive and negative samples
%--

% TODO: factor this center-scale extraction

pos.samples = samples(samples > 0); 

if ~isempty(pos.samples)
	pos.center = mean(pos.samples); pos.scale = std(pos.samples);
else
	pos.center = 0; pos.scale = 0;
end

neg.samples = samples(samples < 0); 

if ~isempty(neg.samples)
	neg.center = mean(neg.samples); neg.scale = std(neg.samples);
else
	neg.center = 0; neg.scale = 0;
end

%--
% compute reasonable limits
%--

% NOTE: these limits should contain most samples, consider making symmetric

ylim(1) = min(neg.center - width * neg.scale, pos.center - width * pos.scale);

ylim(2) = max(pos.center + width * pos.scale, neg.center + width * neg.scale);
