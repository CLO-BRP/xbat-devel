function control = parameter__control__create(parameter, context)

% SCOPE - parameter__control__create

control = empty(control_create);

control(end + 1) = control_create( ...
	'name', 'adapt', ...
	'style', 'checkbox', ...
	'value', parameter.adapt ...
);