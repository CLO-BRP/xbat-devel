function [parameter, context] = parameter__compile(parameter, context)

% SCOPE - parameter__compile

% TODO: this seems to be called twice, when it should be called once

if parameter.adapt && isfield(context, 'page')

	parameter.duration = max( ...
		min( ...
			parameter.MAX_SAMPLES / context.sound.rate, parameter.PAGE_FRACTION * context.page.duration ...
		), ...
		parameter.MIN_SAMPLES / context.sound.rate ...
	);
	
end