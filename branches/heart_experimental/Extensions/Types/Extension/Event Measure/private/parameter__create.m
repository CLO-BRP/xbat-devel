function parameter = parameter__create(context)

% EVENT MEASURE - parameter__create

parameter = struct;

ext = get_active_extension(context.ext.subtype, context.par);

parameter.active = ~isempty(ext) && ismember(context.ext.name, {ext.name});

parameter.overlay = 1;