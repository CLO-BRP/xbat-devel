function [event, handles] = compute_active_event_measures(par, event, data, ax, top)

% compute_active_event_measures - used to compute active selection measures
% -------------------------------------------------------------------------
%
% [event, handles] = compute_active_event_measures(par, event, data, top)
%
% Input:
% ------
%  par - browser
%  event - event
%  data - browser state
%  top - parent menu for measure menu
%
% Output:
% -------
%  event - measured event
%  handles - handles

%----------------------
% HANDLE INPUT
%----------------------

% NOTE: we initialize trivial handles output

handles = struct;

%--
% get active browser as default
%--

if ~nargin || isempty(par)
	par = get_active_browser; 
end

if isempty(par)
	return;
end

if nargin < 3 || trivial(data)
	data = get_browser(par);
end

%--
% get selection if needed
%--

if nargin < 2 || isempty(event)

	sel = get_browser_selection(par, data); 
	
	if isempty(sel.event) 
		return; 
	end
	
end

%--
% get parent axes and menu parent if needed
%--

% NOTE: using the active axes is always a bit scary, perhaps we should use 'gcbo' or its ancestor

if nargin < 4
	ax = gca;
end

% TODO: figure out a way to get the top menu if needed

if nargin < 5
	top = [];
end

%----------------------
% SETUP
%----------------------

%--
% check for active measures
%--

[active, ext, context] = has_active_event_measures(par, data);

if ~active
	return;
end

%----------------------
% COMPUTE
%----------------------

%--
% compute active measures
%--

% TODO: we should compute, but perhaps we should also display

[event, context] = compute_event_measures(event, ext, context);

%----------------------
% DISPLAY
%----------------------

% NOTE: it is not clear that this belongs here

%--
% create menus
%--

if ~isempty(top)
	handles.menu = create_event_measure_menus(top, event, ext, context);
end

%--
% produce browser displays
%--

% NOTE: we know which axes the event is displayed in, pass it along

event.axes = ax;

handles.display = create_browser_measure_displays(par, event, ext, context);

% NOTE: these are decoupled because 'compute' and 'display' are triggered by different event

% NOTE: for display we probably trigger 'on event display' and 'on selection create'

%--
% produce views if needed
%--

% NOTE: this is currently handled elsewhere, however context constraints suggest we do it here

for k = 1:numel(ext)
	
	if ~isfield(ext(k).control, 'view') || ~ext(k).control.view
		continue;
	end
	
	% TODO: is this the only event?
	
	listener = get_event_listener(ext(k), 'selection__create', true);
	
	if isempty(listener)
		continue;
	end
	
	widget = create_widget(par, ext(k), context(k));
	
% 	result = widget_update(par, widget, event, data, layout, context)
	
end

