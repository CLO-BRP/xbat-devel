function measurement = measurement_create(varargin)

% measurement_create - create measurement structure
% -------------------------------------------------
%
% measurement = measurement_create
%
% Output:
% -------
%  measurement - XBAT measurement (result) structure

persistent MEASUREMENT_PERSISTENT;

if isempty(MEASUREMENT_PERSISTENT)
	
	measurement.name = ''; % name of measurement
	
	measurement.fun = ''; % function implementing measurement
	
	measurement.author = ''; % author of measurement
	
	measurement.created = now; % creation date 
	
	measurement.modified = []; % modification date
	
	measurement.value = []; % measurement values computed
	
	measurement.parameter = []; % paremeters used in measurement computation
	
	measurement.userdata = []; % store for user?
	
	MEASUREMENT_PERSISTENT = measurement;
	
else 

	measurement = MEASUREMENT_PERSISTENT;
	
	measurement.created = now;
	
end


%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if length(varargin)
	measurement = parse_inputs(measurement, varargin{:});
end
