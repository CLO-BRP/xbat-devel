function [fun, info] = log_annotation

% log_annotation - function handle structure
% -----------------------------------------
%
% [fun, info] = log_annotation
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun.parameter_create = []; 

fun.parameter_control = [];

fun.compute = [];