function str = page_to_str(page, fun)

% page_to_str - scan page string
% ------------------------------
%
% str = page_to_str(page, fun)
%
% Input:
% ------
%  page - scan page
%  fun - time string function (def: @sec_to_clock)
%
% Output:
% -------
%  str - page string

%--
% set time conversion function
%--

if nargin < 2
	fun = @sec_to_clock;
end

%--
% create string
%--

% TODO: integrate full and interval display

str = ['[', fun(page.start), ', ', fun(page.start + page.duration)];

if page.full
	str = [str, ')'];
else
	str = [str, ']'];
end

switch page.interval
	
	case 0
		
	case 1, str = ['{', str];
		
	case 2, str = [str, '}'];
		
	case 3, str = ['{', str, '}'];
		
end