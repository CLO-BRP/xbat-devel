function [fun, info] = signatures

% signatures - function handle structure
% ---------------------------------------
%
% [fun, info] = sound_file_format
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun.info = {{'info'}, {'file'}};

% NOTE: this function is meant to extract non-essential metadata from file, for example 'id3 tags' in the case of MP3

fun.meta = {{'meta'}, {'file'}};

fun.read = {{'samples', 'options'}, {'file', 'start', 'duration', 'channels', 'options'}};

fun.write = {{'result'}, {'file', 'samples', 'rate', 'parameter'}};

% NOTE: decode and encode use 'WAV' as a reference format

fun.decode = {{'result'}, {'input', 'output'}};

fun.encode = {{'result'}, {'input', 'output', 'parameter'}};

% NOTE: these are the CODEC parameters it is not clear what role the context plays in this part of the API

fun.parameter = param_fun;