function fun = signatures

% NOTE: in this interface 'log' will be very much like the XBAT 'sound'

%--
% create log
%--

fun.store.create = {{'store'}, {'name', 'parameter', 'context'}};

fun.store.parameter = param_fun;

fun.store.delete = {{'result'}, {'store', 'context'}};

%--
% access log events
%--

event.save = {{'id'}, {'store', 'event', 'context'}};

% NOTE: this will also be provided by the 'cache' database

% NOTE: order may be superfluous

event.info = {{'info'}, {'store', 'id', 'order', 'context'}};

event.read = {{'event'}, {'store', 'id', 'order', 'context'}};

event.delete = {{'result'}, {'store', 'id', 'context'}};

%--
% access log extension data
%--

extension.save = {{'result', 'id'}, {'store', 'id', 'ext', 'data', 'context'}};

extension.read = {{'data', 'id'}, {'store', 'id', 'ext', 'context'}};

extension.delete = {{'result'}, {'store', 'id', 'ext', 'context'}};

event.extension = extension;

%--
% basic find helpers
%--

% NOTE: these find helpers are dynamically tied to the event model, they all output an id array

% NOTE: we may not need these, as these services will be provided by the 'cache' database

% [helpers, args] = event_find_helpers;
% 
% for k = 1:length(helpers)
% 	event.find.(helpers{k}) = args{k};
% end
% 
% event.find = collapse(event.find);

%--
% measure find helpers
%--

% TODO: develop some way of supporting measure based find, partial help

%--
% pack
%--

fun.event = event;

