function result = widget_update(par, widget, event, data, layout, context)

% widget_update - update widget on event
% --------------------------------------
%
% result = widget_update(par, widget, event, data, layout, context)
%
% Input:
% ------
%  par - parent handle
%  widget - widget handle
%  event - event name
%  data - event data
%  layout - force layout
%  context - context
%
% Output:
% -------
%  result - update result struct

% NOTE: this line helps debug situations where redundant events are produced

% caller = get_caller; [ignore, caller, ignore] = fileparts(caller.file); disp([caller, ' > ', event, ' > ', get(widget, 'tag')]);

% TODO: implement handling of large pages, this should affect the API

%--
% initialize output
%--

% NOTE: we pack the widget handle and event under consideration, at this point we have not updated

result.widget = widget; result.event = event; result.updated = 0; 

% NOTE: we note the start time and declare that no time has elapsed yet, this will be correct when we don't know how to listen

result.elapsed = 0; start = clock;

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set no layout default
%--

if (nargin < 5) || isempty(layout)
	layout = 0;
end

%--
% get widget state and context
%--

if nargin > 5
	ext = get_widget_extension(widget);
else
	[ext, context] = get_widget_extension(widget);
end

context.ext = ext;

% context.debug = get_widget_debug_state(widget);

context.pal = get_extension_palette(ext, par);

%--
% get function handles and parameters from extension
%--

% NOTE: the below 'extension_warning' may not a proper use of this function

switch ext.type
	
	case {'widget', 'sound_browser_palette'}
		
		fun = ext.fun; parameter = ext.parameter;
		
	otherwise
		
		try
			fun = ext.fun.view; parameter = ext.parameters.view;
		catch
			extension_warning(ext, 'Failed to get widget function handles or parameters.');
		end

end

%--
% handle multiple events
%--

if iscellstr(event)	
	
	for k = 1:numel(event)
		current = event{k}; result = widget_update(par, widget, current, get_widget_data(par, current), layout, context);
	end
	
	return;
	
end
	
%-----------------------
% UPDATE WIDGET
%-----------------------

% TODO: fix parameter passing for non-widget extensions 

%--
% layout widget 
%--

if layout && ~isempty(fun.layout)
	
	try
		fun.layout(widget, parameter, context);
	catch
		extension_warning(ext, 'Widget layout failed.', lasterror);
	end
	
end

%--
% get event listener
%--

listener = get_field(fun.on, event);

% NOTE: return if we don't know how to listen

if isempty(listener)
	return;
end

%--
% get widget data if needed
%--

% NOTE: currently the widget data is uniform for events

if (nargin < 4) || isempty(data)
	data = get_widget_data(par, event);
end

% NOTE: return if there is no widget data

if isempty(data)
	return;
end

%--
% get event samples if needed
%--

% TODO: set proper declaration of sample use for all extensions

if ext.uses.samples && ~isempty(strmatch('selection', event))

	% NOTE: when there is no selection the selection event is length zero
	
	if ~isempty(data.selection.event) && isempty(data.selection.event)
	
		% NOTE: we update the data used here, and output the updated event for the benefit of other widgets

		data.selection.event = get_event_samples(data.selection.event, context.sound, context);

		result.UPDATED_EVENT = data.selection.event;
		
	end
	
end

if ext.uses.samples && ~isempty(strmatch('marker', event))
	
	% TODO: get marker samples packed into marker
	
	% NOTE: 'AR Spectrum' is currently using a trick that will not play well with this
	
end

%--
% compile parameters for some events if needed
%--

% TODO: store resulting extension in widget

switch ext.type
	
	case 'widget'
		
		if isempty(strfind(event, 'update')) && ~isempty(fun.parameter.compile)
			
			[parameter, context] = fun.parameter.compile(parameter, context);
			
			% NOTE: this shares compiled parameters with future non-compiling events
			
% 			db_disp updating-extension; stack_disp; updated = keep_fields(ext, {'name', 'type'}) %#ok<NOPRT,NASGU>
			
			ext.parameter = parameter; set_widget_extension(widget, ext, context);
			
		end
			
	otherwise
		
end

%--
% perform widget event callback
%--

% NOTE: the widget callback does not need the parent directly

try
	result.handles = listener(widget, data, parameter, context); result.updated = 1;
catch
	result.handles = []; extension_warning(ext, ['Widget ''', event, ''' listener update failed.'], lasterror);
end

%--
% tag handles for display management
%--

% NOTE: tagging here could make sense, as this could be a part of the event model

% TODO: figure out the right way to do this. what if handles are already tagged?

% TODO: consider appending the tags and generalising code that works with tags to handle multiple tags!

% TODO: ask kathy what she would like to do with these handles, perhaps ones we wish to manage are tagged, ones we want managed are output

% TODO: a persistent store function to support developers, better than using context?, it can use 'get_caller'?

% if ~isempty(result.handles)
% 	
% 	disp(upper(event));
% 	
% 	disp(get(widget, 'tag')); 
% 	
% 	if numel(result.handles) > 1
% 		iterate(@disp, get(result.handles, 'tag')); 
% 	else
% 		disp(get(result.handles, 'tag'));
% 	end
% 
% 	disp(' '); disp(' ');
% 
% end 

%--
% get elapsed time
%--

% NOTE: this can help with coarse level profiling or time budgeting for widget listeners

result.elapsed = etime(clock, start);


%--------------------------------
% WIDGET_RESIZE
%--------------------------------

% TODO: when used, the signature of this function should change to get the parameter struct as input

function widget_resize(widget, eventdata, resize) %#ok<INUSL>

%--
% setup
%--

par = get_widget_parent(widget);

if par < 1
	return;
end

data = get_widget_data(par, 'resize');

[ext, context] = get_widget_extension(widget);

%--
% call widget resize function
%--

% TODO: it is not clear that these are always the desired parameters

try
	resize(widget, data, ext.parameter, context);
catch
	extension_warning(ext, 'Widget resize function failed.');
end


