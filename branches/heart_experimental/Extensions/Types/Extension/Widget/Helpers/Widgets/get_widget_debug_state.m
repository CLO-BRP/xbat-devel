function value = get_widget_debug_state(widget)

%--
% check for menu state
%--

handle = findobj(widget, 'tag', 'debug-state');

if isempty(handle)
	value = 0;
else
	value = strcmp(get(handle, 'check'), 'on');
end
