function display_events_widget_update(widget, context)

% NOTE: this function should allow us to display events on widgets without refreshing the browser

% NOTE: we need some easy fast access for displayed events, perhaps this is can be done using displayed event tags

%-----------------------
% SETUP
%-----------------------

%--
% get basic context elements
%--

par = context.par; sound = context.sound;

%--
% check for events to display
%--

log = get_active_detection_log(par);

if isempty(log.event)
	return;
end

% FIGURE OUT HOW TO GET THESE

color = [1 1 0];

%-----------------------
% DISPLAY
%-----------------------

for k = 1:numel(log.event)
	
	event = log.event(k);
	
	%--
	% collect widget data
	%--

	% TODO: figure out what the data should really be

	data.event = event;

	data.event.display.time = map_time(sound, 'slider', 'record', event.time);

	% NOTE: we could perhaps simply pass this as the log color

	data.event.display.color = log.color;

	data.log.color = log.color; data.log.linestyle = log.linestyle; data.log.linewidth = log.linewidth;

	data.log.name = 'ACTIVE_DETECTION_LOG'; data.log.index = 0; data.event.id = k;

	%--
	% update widget
	%--
	
	for j = 1:length(widget)

		% NOTE: the following is the signature for the next call, result = widget_update(par, widget, event, data, layout, context)

		result = widget_update(par, widget(j), 'event__display', data, 0, context);

		if isempty(result.handles)
			continue;
		end
		
		set(result.handles, 'tag', ['previous-display ', event_tag(data.event, data.log)]);
		
	end
	
end
