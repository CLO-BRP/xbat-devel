function daemon = widget_daemon

% widget_daemon - widget on timer event daemon
% --------------------------------------------
%
% daemon = widget_daemon
%
% Output:
% -------
%  daemon - configured timer

% TODO: update to use 'get_time_slider' and 'set_time_slider'

%-------------------------------
% SETUP
%-------------------------------

name = 'XBAT Widget Daemon';

mode = 'fixedRate'; 

% NOTE: 10 times a second

rate = 0.1; 

%-------------------------------
% GET SINGLETON TIMER
%-------------------------------

%--
% try to find timer
%--

daemon = timerfind('name', name);

if ~isempty(daemon)
	return;
end

%--
% create and configure timer if needed
%--
	
daemon = timer;

set(daemon, ...
	'name',name, ...
	'executionmode', mode, ...
	'busymode', 'drop', ...
	'period', rate, ...
	'timerfcn',@widget_timer_callback ...
);


%---------------------------------------------------
% WIDGET_TIMER_CALLBACK
%---------------------------------------------------

function widget_timer_callback(obj, eventdata)

% widget_timer_callback - call widget timer callbacks
% ---------------------------------------------------
%
% widget_timer_callback(obj, eventdata)
%
% Input:
% ------
%  obj - callback object
%  eventdata - reserved by matlab

%----------------------
% SETUP
%----------------------

%--
% get active browser
%--

par = get_active_browser;

if ~is_browser(par)
	return;
end

%--
% get listening widgets
%--

widget = get_widgets(par, 'timer');

if isempty(widget)
	return;
end

%--
% update widgets
%--

update_widgets(par, 'timer');
