function set_widget_state(widget, state)

if strcmp(get(widget, 'name'), state.name)	
	
	set(widget, ...
		'position', state.position ...
	);

end