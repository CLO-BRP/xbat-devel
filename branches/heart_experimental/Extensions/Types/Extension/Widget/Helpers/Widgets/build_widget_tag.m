function tag = build_widget_tag(par, ext)

% build_widget_tag - build tag for widget figure
% ----------------------------------------------
%
% tag = build_widget_tag(par, ext)
%
% Input:
% ------
%  par - parent browser
%  ext - widget extension
%
% Output:
% -------
%  tag - widget figure tag

%---------------------
% HANDLE INPUT
%---------------------

%--
% check for browser and get info
%--

[proper, info] = is_browser(par);

if ~proper
	error('Input handle is not browser handle.');
end

%---------------------
% BUILD TAG
%---------------------

%--
% build tag with common information
%--

% NOTE: the header indicates the figure type, the type the extension type

header = 'XBAT_WIDGET'; type = upper(ext.subtype); sep = '::';

% NOTE: we match user, library, and sound, change type and add widget name

tag = [ ...
	header, sep, type, sep, ext.name, sep, info.user, sep, info.library, sep, info.sound, sep, get_listener_string(ext) ...
];

tag = [tag, sep, 'MD5_', md5(tag)];

