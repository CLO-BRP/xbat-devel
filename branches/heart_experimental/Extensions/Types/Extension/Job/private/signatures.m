function [fun, info] = signatures

% JOB - signatures

fun = struct; info = [];

fun.atomize = {{'job', 'context'}, {'job', 'parameter', 'context'}};

fun.execute = {{'job', 'context'}, {'job', 'parameter', 'context'}};

% NOTE: the controls and menus may be used to configure job handler?

fun.parameter = param_fun;
