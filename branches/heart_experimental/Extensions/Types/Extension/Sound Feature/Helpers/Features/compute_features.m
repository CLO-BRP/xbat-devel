function out = compute_features(feature, sound, page, context)

% compute_features - compute features for page
% --------------------------------------------
%
% compute_features(feature, sound, page, context)
%
% Input:
% ------
%  feature - feature extension array
%  sound - sound to compute on
%  page - page to compute on
%  context - feature context array

% NOTE: currently all features are multi-channel consider single-channel feature extensions?

%---------------------------------
% SETUP
%---------------------------------

% NOTE: this is a hack to resolve an inconsistent page structure 

if ~isfield(page, 'channels')
	page.channels = page.channel;
end

%--
% build scan for page
%--

scan = scan_intersect(get_sound_scan(sound), get_page_scan(page));

if isempty(scan.start)
	return;
end

%--
% remove features that have no implementation
%--

for k = length(feature):-1:1	
	
	% NOTE: feature and context arrays should match
	
	if ~isstruct(feature(k).fun)
		feature(k) = []; context(k) = [];
	end	
	
end

if isempty(feature)
	return;
end
	
%--
% add page, grid options, and other parent fields to context for convenience
%--

% NOTE: this function is called from a single parent, these fields are certainly shared

if isempty(context(1).par)
	context_grid = [] ; context_axes = [];
else
	data = get_browser(context(k).par); context_grid = data.browser.grid; context_axes = data.browser.axes;
end

page.freq = context(1).page.freq;

for k = 1:length(context)
	
	% NOTE: this is the full page, not one of the scan pages

	context(k).page = page;

	context(k).grid = context_grid; context(k).axes = context_axes;
	
end

%---------------------------------
% INITIALIZE
%---------------------------------

% NOTE: the context contents are largely shared

view = zeros(size(feature)); wait = view;

for k = 1:length(feature)
	
	%--
	% select extension and create feature context
	%--
	
	ext = feature(k); 
	
	%--
	% set view and waitbar flags
	%--

	% NOTE: these should not be exclusive
	
	viewing = ~isempty(ext.fun.view.on.compute) && ext.control.active;

	waiting = ~viewing;

	%--
	% waitbar setup
	%--

	% NOTE: the feature waitbar is not currently implemented

% 	if waiting	
% 		pal = feature_waitbar(context(k)); waitbar_update(pal, 'PROGRESS', 'message', 'Starting ...');	
% 	end

	%--
	% view setup
	%--

	% NOTE: create the view figure, parameters, and layout figure if needed
	
	if viewing

		%--
		% create and possibly layout view figure
		%--
		
		[context(k).view.figure, created] = create_widget(context(k).par, context(k).ext);

		% NOTE: the created clause ensures we only layout on creation
		
		if ~isempty(ext.fun.view.layout) && created	
			
% 			try
% 				ext.fun.view.layout(context(k).view.figure, ext.parameters.view, context(k));
% 			catch
% 				extension_warning(ext, 'View layout failed.', lasterror);
% 			end
			
			extension_call(ext, 'view.layout', context(k).view.figure, ext.parameters.view, context(k));
		else
			
			% NOTE: only handles provided as output are managed and cleared
			
			clear_previous_widget_display(context(k).view.figure);
			
		end

	end

	%--
	% pack contexts and state variables
	%--

	view(k) = viewing; wait(k) = waiting;
end

%---------------------------------
% COMPUTE
%---------------------------------

%--
% initialize output
%--
	
% NOTE: we do this even when no output is requested, to help store feature values in view

for k = 1:length(feature)

	out(k).name = feature(k).name; 

	out(k).channels = page.channels;
	
	out(k).page = {}; out(k).value = {};
end

% NOTE: get channels from input page before we change what page means

channels = page.channels;

%--
% page and compute
%--

% NOTE: at this point page becomes the scan page, in the previous line page is the big page

[page, scan] = get_scan_page(scan); 

if ~isempty(page)
	page.cache = struct;
end

%--
% continue while we have pages
%--

first_page = 1;

while ~isempty(page)
	
	%--
	% store page in output to match value
	%--
	
	for k = 1:length(feature)
		out(k).page{end + 1} = page;
	end
	
	%--
	% read and filter page
	%--
	
	% TODO: a reasonable way to set the default buffer, allow extensions to configure this
	
	
	page = read_sound_page(sound, page, channels, min(60, max(1, 0.5 * page.duration)));
	
	% TODO: the packing of context with respect to active extensions is currently problematic
	
	if isfield(context(1), 'active')
		
		if isfield(context(1).active, 'signal_filter')

			temp = context(1); temp.ext = temp.active.signal_filter;

			page = filter_sound_page(page, temp);
		end
	end
	
	%--
	% compute features
	%--
	
	% TODO: modify this section to only do the display if needed
	
	for k = 1:length(feature)
		
		% NOTE: this means that we don't accept scan changes from features
		
		ext = feature(k); context(k).scan = scan;
			
		%--
		% compile parameters for page adaptation
		%--

		% NOTE: this is not as for detectors, in that case we are configuring the pager
		
		if ~isempty(ext.fun.parameter.compile)
			
% 			try
% 				ext.parameter = ext.fun.parameter.compile(ext.parameter, context(k));
% 			catch
% 				extension_warning(ext, 'Parameter compilation failed.', lasterror);
% 			end
			
			ext.parameter = extension_call(ext, 'parameter.compile', ext.parameter, context(k));
			
			context(k).ext = ext;	
		end
		
		%--
		% compute 
		%--
		
		% TODO: figure out how this interacts with exception handling
		
% 		[value, context(k), page] = get_page_feature(page, ext, context(k));
		
		value = struct;
		
% 		try
% 			[value, context(k)] = ext.fun.compute(page, ext.parameter, context(k));
% 		catch
% 			extension_warning(feature(k), 'Compute failed.', lasterror);
% 		end
		
		[value, context(k)] = extension_call(ext, 'compute', page, ext.parameter, context(k));
		
		%--
		% reduce
		%--
		
		% NOTE: currently reduction uses the feature compute parameters, although it is typically used for display
		
		% NOTE: computed values are being reduced, although this is typically used for display
		
		% NOTE: reduction is related to spectrogram summary computations
		
		if isfield(ext.fun, 'reduce') && ~isempty(ext.fun.reduce)
			
% 			if first_page && ~isempty(ext.fun.view.parameter.compile)
% 				
% % 				context(k).pages = pages; context(k).page_value = value;
% 
% 				try
% 					ext.parameter = ext.fun.view.parameter.compile(ext.parameter, context(k));
% 				catch
% 					extension_warning(ext, 'First page parameter compilation failed.', lasterror);
% 				end
% 				
% 				context(k).ext = ext; feature(k) = ext;
% 				
% % 				if isfield(context, 'repeat_page');
% % 					k = k - 1; continue;
% % 				end
% 				
% 			end
			
			% TODO: 'reduce' is currently using 'compute' parameters, not 'view' parameters

% 			try
% 				[value, context(k)] = ext.fun.reduce(value, ext.parameter, context(k));
% 			catch
% 				extension_warning(feature(k), 'Reduce failed.', lasterror);
% 			end
			
			[value, context(k)] = extension_call(ext, 'reduce', value, ext.parameter, context(k));
		end
		
		%--
		% display view
		%--
		
		if ~view(k)
			continue;
		end

		% HACK: this is not an actual solution to the described problem
		
		% NOTE: this will update the stored widget extension on the first page the feature computation
		
		% NOTE: this may go a long way to making sure that the view parameters are updated for non-widget widgets
		
		% TODO: we can also include this call in the relevant control callbacks
		
		if first_page
			set_widget_extension(context(k).view.figure, ext, context(k));
		end
		
		context(k).view.handles = [];

% 		try
% 			
% 			[handles, context(k)] = ext.fun.view.on.compute( ...
% 				context(k).view.figure, value, ext.parameters.view, context(k) ...
% 			);
% 			
% 			context(k).view.handles = handles;
% 			
% 			% TODO: setting means the extension cannot use tags, make tags more like 'tags'!
% 			
% 			% NOTE: come up with a way of appending and testing for presence of one
% 			
% 			% NOTE: having both the extension and this code use tags could be dangerous
% 			
% 			% NOTE: another possiblility is to check for a tag before doing this
% 			
% 			add_handle_tag(context(k).view.handles, 'current-display');
% 			
% 		catch
% 			
% 			extension_warning(ext, 'Multi-channel view failed.', lasterror);
% 			
% 		end
		
		[handles, context(k)] = extension_call(ext, 'view.on.compute', ...
			context(k).view.figure, value, ext.parameters.view, context(k) ...
		);

		context(k).view.handles = handles;
		
		add_handle_tag(context(k).view.handles, 'current-display');
		
		%--
		% pack feature values
		%--
		
		% NOTE: each page adds and element to value, later possibly collapsed
		
		out(k).value{end + 1} = value;	
	end
	
	%--
	% turn a page
	%--
	
	[page, scan] = get_scan_page(scan); 
	
	if ~isempty(page)
		page.cache = struct;
	end
	
	first_page = 0;
end

%--
% store feature values in view figure
%--

% NOTE: typically a feature is also viewed, this provides some large page persistence to the values

for k = 1:length(feature)
	
	widget = context(k).view.figure;
	
	data = get(widget, 'userdata'); data.data = out(k); set(widget, 'userdata', data);
end

%--
% manage view objects
%--

for k = 1:length(view)

	if ~view(k)
		continue;
	end 

	%--
	% update managed display handle tags
	%--

	replace_handle_tag(find_handles_with_tag(context(k).view.figure, 'current-display'), 'current-display', 'previous-display');
	
	%--
	% get explain handles
	%--

% 	view_all = findobj(context(k).view.figure);

	view_axes = findobj(context(k).view.figure, 'type', 'axes');

	%--
	% make sure objects are displayed and axes grids visible
	%--

	% TODO: this is one of the most costly lines, figure out what to do
	
% 	set(setdiff(view_all, view_axes), 'visible', 'on');

	% NOTE: this resolves the display problem with the tool and status

	% NOTE: another way might be making these axes callback visible

	tags = get(view_axes, 'tag');

	if ischar(tags)
		tags = {tags};
	end

	for k = length(tags):-1:1 %#ok<FXSET>

		if ~isempty(findstr(tags{k}, 'HARRAY'))
			view_axes(k) = [];
		end
	end

	set(view_axes, 'layer', 'top');
end
