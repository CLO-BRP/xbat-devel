function activate_sound_feature(ext, par, action)

% activate_sound_feature - browser updates
% ----------------------------------------
%
% activate_sound_feature(ext, par, action)
%
% Input:
% ------
%  ext - to consider
%  par - to update
%  action - 'add' or 'delete'

%--
% set default activate action
%--

if nargin < 3
	action = 'add';
end

%--
% get active browser if needed
%--

if nargin < 2
	par = get_active_browser;
end 

if isempty(par)
	return;
end

%--
% set extension active state
%--

set_active_extension(action, ext, par);
		
%--
% reflect state in browser
%--

% NOTE: udpate active menu, and close feature view or refresh browser display

% TODO: we should not need a full browser refresh, only the feature

active = strcmp(action, 'add');

set_active_menu(par, ext, active);


widget = find_widget(par, ext); 

[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name);


if ~active

	% TODO: perhaps the closing is a default deactivate action, or should happen after the widget event
	
	close(widget); widget = [];

else

	% NOTE: this is incomplete but should be faster when done, full refresh happens on page
	
	feature_refresh(context);

	widget = find_widget(par, ext); 

end

%--
% trigger activate or deactiveate event
%--

event = ternary(strcmp(action, 'add'), 'activate', 'deactivate');

fun = ext.fun.view.on.(event);

if ~isempty(fun)
	
	widget = find_widget(par, ext); 
	
	data = get_widget_data(par, event);
	
	try
		[result, context] = fun(widget, data, context.ext.parameters.view, context);
	catch
		extension_warning(ext, [title_caps(event), ' action failed.'], lasterror);
	end
	
end


%----------------------------------
% SET_ACTIVE_MENU
%----------------------------------

% TODO: add proper callback to active menu, this pattern will be useful elsewhere

function set_active_menu(par, ext, active)

handle = [];

%--
% get feature parent menu
%--

par = findobj(par, 'type', 'uimenu', 'label', 'Feature', 'parent', par);

if isempty(par)
	return;
end

handle = findobj(par, 'label', ext.name);

if ~isempty(handle)
	set(handle, 'check', ternary(active, 'on', 'off'));
end