function parameter = parameter__create(context)

% SOUND FEATURE - parameter__create

parameter = struct;

% NOTE: the context extension is the child extension, not the type itself

parameter.active = extension_is_active(context.ext, context.par);
