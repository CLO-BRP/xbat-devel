function parameter = amplitude_detection_parameters(parameter, context)

% PEAK DETECTION

parameter.duration = 0.025;		% min duration 'scale' for signals of interest

parameter.snr = 4;			% min SNR at peak amplitude point

parameter.noise_floor = 0;

parameter.threshold = 0;		% min max amplitude threshold

% PEAK GROUPING

% a relative decline in amplitude of this magnitude is considered quiet and triggers the split of otherwise contiguous events

parameter.break = 0.4;

% NOTE: a decline in amplitude of this magnitude will stop the recording of an event, not currently revealed

% TODO: develop a convergence rather than a threshold strategy to stop an event

% NOTE: this is tricky, when working with groups it can hide the group structure

parameter.stop = 1/16;

% NOTE: these control whether we simply join contiguous events and whether we fill gaps

parameter.join_contiguous = 0;

parameter.fill_gaps = 1;