function [handles, context] = explain__on__selection__create(widget, data, parameter, context)

% DATA TEMPLATE - explain__on__selection__create

handles = [];

%--
% clear previous selection display
%--

delete(findobj(widget, 'tag', 'selection_handles'));

%--
% get selection
%--

sel = data.selection;

%--
% get axes
%--

% TODO: this could be a get channel axes function

ax = findobj(widget, 'type', 'axes'); tags = get(ax, 'tag');

ix = find(strcmp(int2str(sel.event.channel), tags));

if isempty(ix)
	return; 
end

ax = ax(ix);

%--
% display selection
%--

handles = display_selection(ax, sel);

set(handles, 'tag', 'selection_handles');


%-----------------------------
% DISPLAY_SELECTION
%-----------------------------

function handles = display_selection(ax, sel)

%--
% get various coordinates
%--

ylim = get(ax, 'ylim'); xlim = sel.event.time;

start = xlim(1); stop = xlim(2); 

low = ylim(1); high = ylim(2);

pad = 0.05 * diff(ylim);

%--
% display rectangle
%--

handles = [];

x = [start, start, stop, stop, start]; y = [high - pad, low + pad, low + pad, high - pad, high - pad];

handles(end + 1) = patch( ...
	'xdata', x, 'ydata', y, 'edgecolor', sel.color, 'facecolor', 'none' ...
);

%--
% display control points
%--

% NOTE: getting these to work will require some work

x = start; y = mean(ylim);

handles(end + 1) = line( ...
	'xdata', x, 'ydata', y, 'marker', 's' ...
);

x = mean(xlim);

handles(end + 1) = line( ...
	'xdata', x, 'ydata', y, 'marker', '+' ...
);

x = stop;

handles(end + 1) = line( ...
	'xdata', x, 'ydata', y, 'marker', 's' ...
);

%--
% set various collective properties
%--

set(handles, 'parent', ax);

set(handles(2:end), 'color', sel.color);


