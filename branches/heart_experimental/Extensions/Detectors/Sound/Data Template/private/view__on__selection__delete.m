function [handles, context] = explain__on__selection__delete(widget, data, parameter, context)

% DATA TEMPLATE - explain__on__selection__delete

delete(findobj(widget, 'tag', 'selection_handles'));

handles = [];

