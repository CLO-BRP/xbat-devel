function info = parameter__save(parameter, store, context)

% DATA TEMPLATE - parameter__save

db_disp custom-preset-load

store = [store, '.mat'];

try
	save(store, 'parameter'); info = dir(store);
catch
	info = dir(store);
end