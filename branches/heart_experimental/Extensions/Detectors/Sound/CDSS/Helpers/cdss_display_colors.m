function color = cdss_display_colors

%--
% color name colors
%--

% TODO: create a function to produce the standard color table as struct

color.white = [1 1 1]; color.black = [0 0 0];

color.dark_gray = 0.25 * color.white;

color.gray = 0.5 * color.white;

color.light_gray = 0.75 * color.white;

base = 0.8 * eye(3); 

color.red = base(1,:); color.green = base(2,:); color.blue = base(3,:);

color.orange = [220, 110, 20] / 255;

%--
% specific display colors
%--

color.component = color.red;

color.model = color.blue;

color.knots = color.orange;

color.signal = color.light_gray;
