function [event, context] = compute(page, parameter, context)

% I CAN COUNT - compute

% event = empty(event_create);

%--
% call parent to get linked events
%--

fun = parent_fun(mfilename('fullpath')); [event, context] = fun(page, parameter, context);

%--
% get chains from events
%--

if isempty(event)
	return;
end

chain = get_event_chains(event); event = empty(event_create);

%--
% check for proper chain lengths and weld such chains
%--

for k = 1:numel(chain)
	
	% NOTE: there is much more that can be done than simply count elements of the segmentation 
	
	%--
	% enforce bounds on the number of links in the chain and weld chain
	%--
	
	count = numel(chain(k).element);
	
	if count < parameter.min_links || count > parameter.max_links
		continue;
	end
	
	event(end + 1) = weld_event_chain(chain(k)); 
	
	%--
	% enforce minimum duration constraint
	%--
	
	if event(end).duration < parameter.min_duration, event(end) = []; continue; end
	
	%--
	% add tags and score event
	%--
	
	% NOTE: we can add some tags and a score to help us screen the output
	
	event(end) = add_tags(event(end), ['COUNT::', int2str(count)]);
	
	% NOTE: the root does not change the order, it is for purely esthetic purpose
	
	event(end).score = sqrt(count/100); 
	
end