function parameter = parameter__create(context)

% I CAN COUNT - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.min_links = 3; 

parameter.max_links = 42;

parameter.min_duration = 1;
