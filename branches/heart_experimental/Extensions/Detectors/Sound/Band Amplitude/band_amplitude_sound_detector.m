function ext = band_amplitude_sound_detector

ext = extension_inherit(mfilename, amplitude_sound_detector);

ext.short_description = 'Amplitude detection on specified band';

% ext.category = {ext.category{:}};

ext.version = '0.1';

ext.guid = '9ed5933a-34ad-42af-a6aa-11922de09fc9';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

