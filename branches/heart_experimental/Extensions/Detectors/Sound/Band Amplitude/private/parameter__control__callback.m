function result = parameter__control__callback(callback, context)

% BAND AMPLITUDE - parameter__control__callback

result = struct;

if is_tabs_callback(callback)
	return; 
end

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);

switch callback.control.name
	
	case 'sel_config'
		
		sel = get_browser_selection(context.par); 
		
		set_control(context.pal, 'min_freq', 'value', sel.event.freq(1));
		
		set_control(context.pal, 'max_freq', 'value', sel.event.freq(2));

		% NOTE: this gets the extensions from the browser again, updates and recompiles parameters if needed
		
		context.ext = get_callback_extension(callback);
	
	% TODO: enforce 'min' and 'max' constraints for filter controls
		
end

update_filter_display(callback, context);