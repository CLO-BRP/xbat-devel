function [event, value, context] = compute(page, parameter, context)

% BAND AMPLITUDE - compute

% TODO: to develop a detector that considers exclusion bands we are better off starting from the feature

%--
% filter page to focus on band of interest
%--

if isempty(page.filtered)
	page.filtered = page.samples; 
end

if parameter.whiten
	page.filtered = filter([1, -1], 1, page.filtered);
end

if isfield(parameter, 'filter')
	page.filtered = filter(parameter.filter.b, parameter.filter.a, page.filtered);
end

%--
% perform amplitude detection on the filtered signal
%--

% NOTE: the parent detector will take care of setting the proper frequency bounds

fun = parent_fun(mfilename('fullpath')); [event, value, context] = fun(page, parameter, context);


