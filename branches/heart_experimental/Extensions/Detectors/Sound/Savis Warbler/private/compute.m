function [event, value, context] = compute(page, parameter, context)

% SAVIS WARBLER - compute

event = empty(event_create); value = struct;

% ---
% Just compute the novelty power spectrum
% ---
fun = parent_fun(mfilename('fullpath'),2); [feature, context] = fun(page, parameter, context); context.view.data = feature;

% deal with multiple channels
feature = select_channel(feature, page);

% ---
% We subtract the side channel information from the center channels
% ---

% TODO: delete intermediate band beforehand
feature.novelty_power_spec.value = subtract_flanking_band(feature.novelty_power_spec.value);

feature.novelty_power_spec.ref_energy = feature.novelty_power_spec.ref_energy(3:end);

% ---
% Now, detect the savi's warbler periodicity within each of
% the remaining subbands
% ---

% modify input for detector
context2 = context;
context2.feature = feature;
parameter2 = parameter;
parameter2.band_details = parameter2.band_details(:,3:end);

fun = parent_fun(mfilename('fullpath'),1); [event, value, context] = fun(page, parameter2, context2); context.view.data = feature;

% ---
% we now combine the results (events) of the 4 remaining subbands.
% ---
;




function feature = select_channel(feature, page)
% deal with multiple channels: select active

if length(page.channels) > 1
    feature.novelty_power_spec.value = feature.novelty_power_spec.value{page.channel};
    feature.novelty_power_spec.ref_energy = feature.novelty_power_spec.ref_energy{page.channel};
end



function nps = subtract_flanking_band(nps)
% ---
% We subtract the side channel information from the center channels
% ---

% isolate flanking band
flank = nps{1};

% skip intermediate band and substract from remaining band
for i = 3:numel(nps)
    nps{i} = max(0, nps{i} - flank);
end

nps = nps(3:end);
    
    
