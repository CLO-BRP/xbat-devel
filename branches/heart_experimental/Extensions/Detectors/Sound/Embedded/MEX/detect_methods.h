
/*--------------------------------------------------*/
/* USER METHODS THAT MUST BE PROVIDED IN A SO LIB   */
/*--------------------------------------------------*/

/*
 *  parameter_initialize - prepare the parameter structure
 */
 
void * (* parameter_initialize)();

/*
 * state_initialize - prepare the state
 */

void * (* state_initialize)(void * parameters);

/*
 * detect - perform detection on buffer using parameters and state
 */

int (* detect)(event_t * events, sample_t * buf, int nbuf, void * parameters, void * state, sound_setup * info); 

/*
 * setup - set up the audio engine
 */

sound_setup * (* setup)(void * parameters);

/*
 * data_to_str - get string representation of special detector data
 */
 
 char * (* data_to_str)(void * data);
 
 /*
  * cleanup - free storage and so forth
  */
 
 void (* cleanup)(void * parameters, void * state);
