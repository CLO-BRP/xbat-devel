function [event, context] = compute(page, parameter, context)

% EMBEDDED - compute

if ~isempty(page.filtered)
    page.samples = page.filtered;
end

event = empty(event_create);

%-
% call detector on this page
%-

gain = 10 ^ (parameter.gain / 20);

x = int32(page.samples * gain * (2^31 - 1));

if isempty(parameter.detector)
    return;
end

e_events = embedded_detector_mex(x, fullfile(parameter.path, parameter.detector{1}), parameter.config_file);

%-
% convert events into full XBAT events
%-

if isempty(e_events)
    return;
end

data = {e_events(:).data};

event = update_event(e_events, 1);

for k = 1:length(event) 
    event(k).detection.embedded = data{k}; event(k).channel = page.channels;
end



