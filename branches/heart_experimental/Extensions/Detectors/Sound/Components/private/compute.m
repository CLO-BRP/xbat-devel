function [event, context] = compute(page, parameter, context)

% COMPONENTS - compute

event = empty(event_create);

%--
% compute parent feature
%--

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); 

%--
% get components from feature
%--

% NOTE: these lines show that the child must have some knowledge of the parent

X = feature.spectrogram.value;

parameter.time = feature.time; delta = diff(feature.time(1:2));

parameter.freq = feature.freq;

if ~iscell(X)
	X = {X};
end 

for k = 1:numel(X)

	X{k} = comp_label(X{k}, se_ball(2), 8);

	X{k} = comp_bdry(X{k}, [1 0 1 0]);

	label = unique(X{k}(:)); label(1) = [];

	event = empty(event_create);

	for j = 1:numel(label)

		[row, col] = find(X{k} == label(j)); count = numel(row);

		if count < 16
			continue;
		end

		col = fast_min_max(col);
		
		if delta * diff(col) < parameter.min_duration
			continue;
		end
		
		row = fast_min_max(row);

		time = parameter.time(col) - context.page.start; time = time(:)';

		freq = parameter.freq(row); freq = freq(:)';

		event(end + 1) = event_create( ...
			'channel', context.page.channels(k), ...
			'time', time, ...
			'freq', freq ...
		);

	end
	
end

if numel(X) == 1
	X = cellfree(X);
end

feature.spectrogram.value = X;

context.view.data = feature;
