function [out lpscore] = xpeaks(in, hw)

% this shall be the great peak picking algo.
% 1. lowpass
% 2. hillvalley
% 3. plateaus
% 4. filter

Wp = 1/hw;
Ws = Wp*1.2;

% ---
% We low-pass filter the function in order to get a smooth base for simple
% peak picking. 
% ---
% TODO: move the filter definition out of this function
% TODO: the filtering causes bad peak positions for shapes  where two
%       maxima are very near (<< hw) to each other. Have a second look at
%       original signal and peak estimates.
% ---

% ---
% define the lowpass filter globally, but take care on recalculating if any
% changes are made in the filter parameters
% NOTE: for the current purpose, butterworth is rather fast and  nice, but
% may be difficult and produce some anomalities for other purposes. 
% planning to switch to binom. linear filter
% ---
global globalvars;

if ~isfield(globalvars,'bopeak_lp') || globalvars.bopeak_lp.hw ~= hw;
    
    [N, Wn] = buttord(Wp, Ws, 1, 12);
    [B,A] = butter(N,Wn);
    globalvars.bopeak_lp.b = B;
    globalvars.bopeak_lp.a = A;
    globalvars.bopeak_lp.hw = hw; 
else
    
    B = globalvars.bopeak_lp.b;
    A = globalvars.bopeak_lp.a;
end

% ---
% we use filtfilt for phase annullation
% NOTE: using filtfilt, the filters frequency is squared
% ---
inflt = filtfilt(B,A,in);

% ---
% peakvalley
% ---
[ix, h, w] = fast_peak_valley(inflt);

% ---
% account for different ouput cases, esp. empty output
% ---
if isempty(ix)
    out = [];
    lpscore = [];
    return;
    
elseif ix(1) < 0
    out = ix(2:2:end);
    
else
    out = ix(1:2:end);  
end

% ---
% filter
% ---
% TODO: use a sliding mean for better adaptation to (e.g. temporal) energy changes
% --
mean_h = mean(in(out));
out = out(in(out) > 0.1 * mean_h);

lpscore = inflt(out);

% ---- debug stuff
% fig;
% plot(in)
% 
% hold
% plot(in,'r')
% plot(out,repmat(mean_h,1,numel(out)),'bo');




