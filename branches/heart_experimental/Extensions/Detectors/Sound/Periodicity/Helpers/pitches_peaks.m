function pitches = pitches_peaks(in)
% generates pitch candidates using the peaks search 

nump = 4;
decay = 0.8;

%TODO: the hillwidth measure below should depend on the frequency range
%searched for, as fr high frequencies the maxima may get quite close
hillwidth = 5;

[pitches, lpscore] = xpeaks(in,5);
scores = in(pitches);

[scores, idx] = sort(scores,'descend');
pitches = pitches(idx);

meanval = mean(scores);
pitches = pitches(pitches(1:min(end,nump)) > decay*meanval);