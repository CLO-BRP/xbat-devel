function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% PEAK AMPLITUDE - view__on__selection__create

% handles = [];

% NOTE: this is an experiment on how to possibly reuse features as event measure parents

% TODO: factor much of this function into a helper that can be reused by feature based measures

%-----------------
% SETUP
%-----------------

%--
% get parent function and extension, and some short names
%--

[fun, par] = parent_fun(mfilename('fullpath')); 

sound = context.sound; selection = data.selection;

%--
% get sufficient representation for parent extension
%--

% TODO: is there a way to get rid of this? or perhaps factor it?

[par, ignore, par.context] = get_browser_extension(par.subtype, context.par, par.name); %#ok<ASGLU>

% NOTE: we use the compiled child parameters

par.parameter = context.ext.parameter;

par.parameters = context.ext.parameters;

% db_disp; flatten(par.parameter)

%--
% get measure value
%--

% TODO: this should be part of the event data or some easy way to access should be available

value = selection.event.measure.(context.ext.fieldname).value;

%-----------------
% PRODUCE VIEW
%-----------------

%--
% get event page and call parent feature to produce on compute view
%--

% NOTE: selection event times are in 'slider' time we map time to get a normal event

selection.event.time = map_time(sound, 'record', 'slider', selection.event.time);

page = get_event_page(selection.event, sound, context);

% TODO: figure out what fishy stuff is happening here in the mixing of extensions and contexts

par.control.active = 1;

compute_features(par, sound, page, context);

% NOTE: we have to reset the widget extension after 'compute_features'

set_widget_extension(widget, context.ext, context);

%--
% perform direct parent callback
%--

[handles, context] = fun(widget, data, parameter, context);

%--
% update page duration
%--

ax = get(handles(1), 'parent');

set(ax, 'xlim', [page.start, page.start + page.duration]);

%--
% display peak guide and label
%--

% TODO: use a singleton line for the peak as well

color = 0.75 * [1, 0, 1];

ylim = get(ax, 'ylim'); ydata = [ylim(1) - 1, value.peak.value * [-1, 1], ylim(2) + 1];

handles(end + 1) = line('parent', ax, ...
	'color', color, ...
	'xdata', value.peak.time * ones(1, 4), ...
	'marker', 's', ...
	'linewidth', 2, ...
	'ydata', ydata ...
);

[handles(end + 1), created] = create_obj('text', ax, 'peak-label');

if created
	set(handles(end), 'verticalalignment', 'bottom');
end

str = [sec_to_clock(value.time), ', PEAK: ', num2str(value.value)];
	
display_label_text(handles(end), str, value.time);

%--
% display quartiles
%--

% NOTE: we can easily compare how the energy distribution and peak relate

handles(end + 1) = create_obj('line', ax, 'quartile-box');

xdata = [value.q1, value.q2, value.q3]; ydata = zeros(1, 3);

set(handles(end), ...
	'xdata', xdata, ...
	'ydata', ydata, ...
	'marker', 's', ...
	'linestyle', '-', ...
	'linewidth', 2, ...
	'color', color ...
);

%--
% display level
%--

handles(end + 1) = create_obj('line', ax, 'median-level');

xdata = [value.q1, value.q3]; ydata = value.median * ones(1, 2);

set(handles(end), ...
	'xdata', xdata, ...
	'ydata', ydata, ...
	'marker', 'none', ...
	'linestyle', '-', ...
	'linewidth', 1, ...
	'color', color ...
);

%--
% clean up display
%--

uistack(handles(end - 1:end), 'top');

delete(findobj(widget, 'tag', 'selection_handles'));

