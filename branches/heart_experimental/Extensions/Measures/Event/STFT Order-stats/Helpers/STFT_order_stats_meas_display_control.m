function varargout = STFT_order_stats_meas_display_control(mode, varargin);

%%
%% 'STFT_order_stats_meas_display_control'
%%
%% measurement display control
%% allows selectable display of measures through a gui control
%%
%% syntax:
%% -------
%%  controller_handle = STFT_order_stats_meas_display_control('create_control', control_fig_tag, control_fig_name);
%%
%%  STFT_order_stats_meas_display_control('update_control', controller_handle);
%%
%%  chkstate = STFT_order_stats_meas_display_control('get_uistate', controller_handle);
%%
%%  chkstate = STFT_order_stats_meas_display_control('reset_uistate', controller_handle);
%%
%%  g = STFT_order_stats_meas_display_control('display', chkstate, event, meas_ix, kHz_flag, ev_ax_h);
%%
%%  STFT_order_stats_meas_display_control('toggle_display');
%%
%%
%% K.A. Cortopassi, July 2005
%%
%% K.A. Cortopassi, January 2006
%% some mods so each browser has its own display control
%%  



switch (mode)


  case ('create_control')

    control_fig_tag = varargin{1};
    control_fig_name = varargin{2};

    control_label = {...
      'Analysis Box';...
      'IPR T/F Box';...
      'Spread T/F Box';...
      'Concentration T/F Box';...
      'Median T/F';...
      'Peak T/F';...
      'Spec Peak T/F';...
      'ST IPR (Freq) Contour';...
      'ST Spread (Freq) Contour';...
      'ST Conc (Freq) Contour';...
      'ST Med Freq Contour';...
      'ST Peak Freq Contour';...
      'NB IPR (Time) Contour';...
      'NB Spread (Time) Contour';...
      'NB Conc (Time) Contour';...
      'NB Med Time Contour';...
      'NB Peak Time Contour';...
      };

    callback_fun = [mfilename, '(''toggle_display'');'];

    g_tag = STFT_order_stats_graphics_objects('define', control_fig_tag);

    controller_handle = figure('tag', control_fig_tag,...
      'name', control_fig_name,...
      'numbertitle', 'off',...
      'units', 'normalized',...
      'position', [0.5, 0.5, 0.21, 0.25]); %% note:  'position', [left bottom width height]

    %% do this little trick to prevent other programs (namely XBAT) using gca
    %% from screwing up my gui figure
    set(gca,'visible','off');

    nchk = length(control_label);
    inc = 1/nchk;

    for inx = 1:nchk

      chk_handle(inx) = uicontrol(controller_handle,...
        'style', 'checkbox',...
        'units', 'normalized',...
        'position', [.1 1-inx*inc 1 inc],...
        'string', control_label{inx},...
        'callback', callback_fun);

      % g = findobj('tag', g_tag{inx});
      %% tags cleared by browser so must use 'userdata' field
      g = findobj('userdata', g_tag{inx});
      %% set the userdata of checkbox(inx) to be all handles of
      %% g_tag{inx} type objects on the screen
      set(chk_handle(inx), 'userdata', g);

    end

    %% mark all check boxes 'off'
    set(chk_handle, 'value', 0);

    %% uicheckbox handles stored in figure userdata
    set(controller_handle, 'userdata', chk_handle);
    %     set(controller_handle, 'userdata', chk_handle,...
    %       'windowbuttondownfcn', ['energy_dist_display_control(''update_control'');'],...
    %       'windowbuttonmotionfcn', ['energy_dist_display_control(''update_control'');'],...
    %       'hittest', 'on'...
    %       );

    varargout{1} = controller_handle;




  case('update_control')

    controller_handle = varargin{1};

    %% uicheckbox handles stored in figure userdata
    chk_handle = get(controller_handle, 'userdata');
    control_fig_tag = get(controller_handle, 'tag');

    g_tag = STFT_order_stats_graphics_objects('define', control_fig_tag);
    nchk = length(g_tag);

    for inx = 1:nchk
      % g = findobj('tag', g_tag{inx});
      %% tags cleared by browser so must use 'userdata' field
      g = findobj('userdata', g_tag{inx});
      %% set the userdata of checkbox(inx) to be all handles of
      %% g_tag{inx} type objects on the screen
      set(chk_handle(inx), 'userdata', g);
    end




  case('get_uistate')

    controller_handle = varargin{1};

    %% uicheckbox handles stored in figure userdata
    chk_handle = get(controller_handle, 'userdata');
    control_fig_tag = get(controller_handle, 'tag');

    g_tag = STFT_order_stats_graphics_objects('define', control_fig_tag);

    nchk = length(g_tag);

    %     chkstate = [];
    %     for inx = 1:nchk
    %       %% get state of check boxes
    %       chkstate = [chkstate, get(chk_handle(inx), 'value')];
    %     end

    chkstate = get(chk_handle, 'value');
    chkstate = [chkstate{:}];

    varargout{1} = chkstate;




  case('reset_uistate')

    g_tag = STFT_order_stats_graphics_objects('define', '');
    nchk = length(g_tag);

    chkstate = zeros(1, nchk);

    varargout{1} = chkstate;




  case ('display')
    %% draw all measurement displays for the first time

    chkstate = varargin{1};
    event = varargin{2};
    meas_ix = varargin{3};
    kHz_flag = varargin{4};
    ev_ax_h = varargin{5};
    control_fig_tag = varargin{6};

    value = event.measurement(meas_ix).value;
    param = event.measurement(meas_ix).parameter;

    g = STFT_order_stats_graphics_objects('display', chkstate, value, kHz_flag, param, event, ev_ax_h, control_fig_tag);

    %% return the graphics handles
    varargout{1} = g;




  case ('toggle_display')

    cbo_h = gcbo;

    if get(cbo_h, 'value') == 1
      % box is checked
      % fprintf(1,'box is checked\n');
      g = get(cbo_h, 'userdata');
      try
        set(g, 'visible', 'on');
      end
    else
      % box is unchecked
      % fprintf(1,'box is un-checked\n');
      g = get(cbo_h, 'userdata');
      try
        set(g, 'visible', 'off');
      end
    end


end %% end switch/case

return;





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 'STFT_order_stats_graphics_objects'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = STFT_order_stats_graphics_objects(mode, varargin);

%%
%% function to display the STFT_AR_contour measurement graphics objects
%% and define the object tags
%%
%% syntax:
%% -------
%% g_tag = STFT_order_stats_graphics_objects('define');
%% g = STFT_order_stats_graphics_objects('display', chkstate, value, kHz_flag, param, event);
%%

switch mode

  case 'define'

    control_fig_tag = varargin{1};

    g_tag = {...
      ['STFT_order_stats_Analysis_Box_', control_fig_tag];...
      ['STFT_order_stats_IPR_', control_fig_tag];...
      ['STFT_order_stats_Spread_', control_fig_tag];...
      ['STFT_order_stats_Concentration_', control_fig_tag];...
      ['STFT_order_stats_Median_', control_fig_tag];...
      ['STFT_order_stats_Peak_', control_fig_tag];...
      ['STFT_order_stats_Spec_Peak_', control_fig_tag];...
      ['STFT_order_stats_ST_IPR_(Freq)_Contour_', control_fig_tag];...
      ['STFT_order_stats_ST_Spread_(Freq)_Contour_', control_fig_tag];...
      ['STFT_order_stats_ST_Conc_(Freq)_Contour_', control_fig_tag];...
      ['STFT_order_stats_MF_contour_', control_fig_tag];...
      ['STFT_order_stats_PF_contour_', control_fig_tag];...
      ['STFT_order_stats_NB_IPR_(Time)_Contour_', control_fig_tag];...
      ['STFT_order_stats_NB_Spread_(Time)_Contour_', control_fig_tag];...
      ['STFT_order_stats_NB_Conc_(Time)_Contour_', control_fig_tag];...
      ['STFT_order_stats_MT_Contour_', control_fig_tag];...
      ['STFT_order_stats_PT_Contour_', control_fig_tag];...
      };



    varargout{1} = g_tag;


  case 'display'

    chkstate = varargin{1};
    value = varargin{2};
    kHz_flag = varargin{3};
    param = varargin{4};
    event = varargin{5};
    ev_ax_h = varargin{6};
    control_fig_tag = varargin{7};

    ev_chan = event.channel;


    c = getcolors;

    g_tag = STFT_order_stats_graphics_objects('define', control_fig_tag);
    numtag = length(g_tag);

    g = [];
    for inx = 1:numtag

      if chkstate(inx)
        visible_state = 'on';
      else
        visible_state = 'off';
      end

      tag_ID = g_tag{inx};

      switch (tag_ID)


        case (['STFT_order_stats_Analysis_Box_', control_fig_tag])
          % display analysis box, time extent and param.flo - fhi

          x = [event.time(1), event.time(1), event.time(2), event.time(2), event.time(1)];
          if strcmpi(param.frange_flag, 'yes')
            y = [param.flo, param.fhi, param.fhi, param.flo, param.flo];
          else
            y = [event.freq(1), event.freq(2), event.freq(2), event.freq(1), event.freq(1)];
          end
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', ':', ...
            'linewidth', 3, ...
            'color', c.y, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case (['STFT_order_stats_IPR_', control_fig_tag])
          %% display time-frequency percentiles box
          x = [value.p1_time, value.p1_time, value.p1_time+value.ipr_time, value.p1_time+value.ipr_time, value.p1_time];
          y = [value.p1_freq, value.p1_freq+value.ipr_freq value.p1_freq+value.ipr_freq, value.p1_freq, value.p1_freq];
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'linewidth', 2, ...
            'color', c.lg, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_Spread_', control_fig_tag])
          %% display time-frequency spread box
          x = [value.lower_time, value.lower_time, value.lower_time+value.spread_time, value.lower_time+value.spread_time, value.lower_time];
          y = [value.lower_freq, value.lower_freq+value.spread_freq, value.lower_freq+value.spread_freq, value.lower_freq, value.lower_freq];
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-',...
            'color', c.lb,...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_Concentration_', control_fig_tag])
          %% display time-frequency concentration box
          %           conc_ext_time = value.conc_time / 2;
          %           conc_ext_freq = value.conc_freq / 2;
          %           med_time = value.p1_time + value.mpr_time;
          %           x = [med_time - conc_ext_time, med_time - conc_ext_time, ...
          %             med_time + conc_ext_time, ...
          %             med_time + conc_ext_time, ...
          %             med_time - conc_ext_time];
          %           y = [value.med_freq - conc_ext_freq, value.med_freq + conc_ext_freq, ...
          %             value.med_freq + conc_ext_freq, ...
          %             value.med_freq - conc_ext_freq, ...
          %             value.med_freq - conc_ext_freq];
          %% or, start at lower_time/freq so conc is always contained in spread
          x = [value.lower_time, value.lower_time, value.lower_time+value.conc_time, value.lower_time+value.conc_time, value.lower_time];
          y = [value.lower_freq, value.lower_freq+value.conc_freq, value.lower_freq+value.conc_freq, value.lower_freq, value.lower_freq];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-',...
            'color', c.b,...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_Median_', control_fig_tag])
          %% display time and frequency center (= 2nd quartile = median)
          x = [value.p1_time + value.mpr_time];
          y = [value.med_freq];
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'marker', '+',...
            'color', c.lg,...
            'markersize', 11,...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_Peak_', control_fig_tag])
          %% display time and frequency peak
          x = value.p1_time + value.ppr_time;
          y = value.peak_freq;
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'marker', 'x',...
            'color', c.lr,...
            'markersize', 11,...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_Spec_Peak_', control_fig_tag])
          %% display time and frequency peak
          x = value.p1_time + value.ppr_time_spec;
          y = value.peak_freq_spec;
          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'marker', '*',...
            'color', c.r,...
            'markersize', 11,...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_MF_contour_', control_fig_tag])
          %% display ST center freq contour

          x = value.tvec;
          y = value.STmed_freq_contour;

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.lv, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_PF_contour_', control_fig_tag])
          %% display ST center freq contour

          x = value.tvec;
          y = value.STpeak_freq_contour;

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.v, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_MT_Contour_', control_fig_tag])
          %% display NB center time contour

          x = value.NBp1_time_contour + value.NBmpr_time_contour;
          y = value.fvec;

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.y, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_PT_Contour_', control_fig_tag])
          %% display NB center time contour

          x = value.NBp1_time_contour + value.NBppr_time_contour;
          y = value.fvec;

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.lo, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case (['STFT_order_stats_ST_IPR_(Freq)_Contour_', control_fig_tag])
          %% display ST bandwith contour

          x = [value.tvec, fliplr(value.tvec), value.tvec(1)];
          y = [value.STp1_freq_contour, fliplr(value.STp1_freq_contour + value.STipr_freq_contour), value.STp1_freq_contour(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.lp, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_ST_Spread_(Freq)_Contour_', control_fig_tag])
          %% display ST spread contour

          x = [value.tvec, fliplr(value.tvec), value.tvec(1)];
          y = [value.STlower_freq_contour, fliplr(value.STlower_freq_contour+value.STspread_freq_contour), value.STlower_freq_contour(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.p, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_ST_Conc_(Freq)_Contour_', control_fig_tag])
          %% display ST spread contour

          x = [value.tvec, fliplr(value.tvec), value.tvec(1)];
          y = [value.STlower_freq_contour, fliplr(value.STlower_freq_contour+value.STconc_freq_contour), value.STlower_freq_contour(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.b, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_NB_IPR_(Time)_Contour_', control_fig_tag])
          %% display NB duration contour

          x = [value.NBp1_time_contour, fliplr(value.NBp1_time_contour + value.NBipr_time_contour), value.NBp1_time_contour(1)];
          y = [value.fvec, fliplr(value.fvec), value.fvec(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.o, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_NB_Spread_(Time)_Contour_', control_fig_tag])
          %% display NB duration contour

          x = [value.NBlower_time_contour, fliplr(value.NBlower_time_contour+value.NBspread_time_contour), value.NBlower_time_contour(1)];
          y = [value.fvec, fliplr(value.fvec), value.fvec(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.lr, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];


        case(['STFT_order_stats_NB_Conc_(Time)_Contour_', control_fig_tag])
          %% display NB duration contour

          x = [value.NBlower_time_contour, fliplr(value.NBlower_time_contour+value.NBconc_time_contour), value.NBlower_time_contour(1)];
          y = [value.fvec, fliplr(value.fvec), value.fvec(1)];

          if (kHz_flag)
            y = y / 1000;
          end
          h = line('tag', tag_ID, 'userdata', tag_ID,...
            'visible', visible_state, ...
            'xdata', x, ...
            'ydata', y, ...
            'linestyle', '-', ...
            'color', c.gy, ...
            'linewidth', 2, ...
            'parent', ev_ax_h ...
            );

          %% add graphics objects to handle list
          g = [g, h];




      end %% end switch/case

    end %% for loop

    %% return the graphics objects handles
    varargout{1} = g;

end %% end switch/case

return;