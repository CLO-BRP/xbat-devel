function varargout = STFT_spec_display(calling_mode, varargin);

%%
%% 'STFT_spec_display'
%%
%% K.A. Cortopassi, January 2006
%%
%% Display the effects of denoising in a little GUI
%% with some minimal controls
%%
%%
%% syntax:
%%
%% STFT_spec_display(calling_mode, varargin);
%%
%%  STFT_spec_display('create_ui', rawsigspc, sigspc, tvecspc, fvecspc);
%%  STFT_spec_display('cb_next');
%%  STFT_spec_display('cb_brit_slider');
%%  STFT_spec_display('cb_cont_slider');
%%
%% input:
%%
%% rawsigspc == spec matrix (before denoising)
%% sigspc == spec matrix (after denoising)
%% tvecspc == vector of spec time values
%% fvecspc == vector of spec frequency values
%% ev_ID == ID of event spec
%% ev_start = event spec start time
%% ev_end == event spec end time
%%
%% output:
%%
%% plot_next_flag = flag indicating whether to continue plotting
%%


gui_tag = 'STFT_Raw_vs_Denoise_fig';

%% set up a persistent variable to remember...
persistent plot_next_flag



switch (calling_mode)

  case ('create_ui')
    create_gui_fig(gui_tag, varargin);
    varargout{1} = plot_next_flag;
    return;

  case ('cb_next')
    cb_next_cancel(gui_tag);
    plot_next_flag = 1;
    return;

  case ('cb_cancel')
    cb_next_cancel(gui_tag);
    plot_next_flag = 0;
    return;

  case ('cb_brit_slider')
    cb_set_brit_cont(gui_tag);
    return;

  case ('cb_cont_slider')
    cb_set_brit_cont(gui_tag);
    return;

end





%%~~~~
%% FUNCTIONS
%%~~~~


%%~~~~
function create_gui_fig(gui_tag, spc_data);
%%~~~~

%% check for open GUI
set(0, 'showhiddenhandles', 'on');
fig_h = findobj(0, 'tag', gui_tag);
set(0, 'showhiddenhandles', 'off');

if isempty(fig_h)
  %% generate GUI

  %% set up screen position
  scrsz = get(0, 'screensize');
  wid = 800;
  hgt = 700;
  figpos = [scrsz(3)-wid-1 scrsz(4)-hgt-32, wid, hgt];

  %% set up colormap with brightness/contrast
  cont = .5;
  brit = 0.75;
  N_full = 256;
  my_colormap = gen_colormap(brit, cont, N_full);

  %% set up figure colors
  fig_bgc = [.9 .9 .9];
  slid_bgc = [.875 .875 .875];

  %% create a keypress function
  keypress_fun = [mfilename, '(''key_press_intercept'');'];

  %% generate the GUI figure
  fig_h = figure('tag', gui_tag,...
    'name', 'STFT Order Stats : Raw vs Denoised',...
    'numbertitle', 'off',...
    'units', 'pixels',...
    'position', figpos,...
    'color', fig_bgc,...
    'resize', 'off',...
    'colormap', my_colormap,...
    'handlevisibility', 'callback',...
    'keypressfcn', keypress_fun);
  %       'closerequestfcn', [mfilename, '(''cb_quit'');'],...
  %       'keypressfcn', keypress_fun);

  %% set up some ui controls
  callback_fun = [mfilename, '(''cb_next'');'];
  ui_h1 = uicontrol(fig_h,...
    'style', 'pushbutton',...
    'string', 'Next Event >>',...
    'tooltipstring', 'hit to move on to next event...',...
    'tag', 'next_button',...
    'units', 'normalized',...
    'position', [.7,.16,.25,.05],...
    'handlevisibility', 'callback',...
    'callback', callback_fun,...
    'enable', 'on');

  callback_fun = [mfilename, '(''cb_cancel'');'];
  ui_h11 = uicontrol(fig_h,...
    'style', 'pushbutton',...
    'string', 'Cancel Plotting',...
    'tooltipstring', 'hit to cancel spectrogram display...',...
    'tag', 'cancel_button',...
    'units', 'normalized',...
    'position', [.1,.16,.25,.05],...
    'handlevisibility', 'callback',...
    'callback', callback_fun,...
    'enable', 'on');

  ui_h2 = uicontrol(fig_h,...
    'style', 'text',...
    'string', sprintf('Brightness\n(%3.2f)', brit),...
    'tooltipstring', 'set brightess...',...
    'tag', 'brit_text',...
    'units', 'normalized',...
    'position', [.06,.095,.15,.05],...
    'backgroundcolor', fig_bgc,...
    'handlevisibility', 'callback',...
    'enable', 'on');

  callback_fun = [mfilename, '(''cb_brit_slider'');'];
  ui_h3 = uicontrol(fig_h,...
    'style', 'slider',...
    'tooltipstring', 'set brightess...',...
    'tag', 'brit_slider',...
    'min', 0,...
    'max', 1,...
    'value', brit,...
    'units', 'normalized',...
    'position', [.25,.095,.7,.035],...
    'backgroundcolor', slid_bgc,...
    'handlevisibility', 'callback',...
    'callback', callback_fun,...
    'enable', 'on');

  ui_h4 = uicontrol(fig_h,...
    'style', 'text',...
    'string', sprintf('Contrast\n(%3.2f)', cont),...,...
    'tooltipstring', 'set contrast...',...
    'tag', 'cont_text',...
    'units', 'normalized',...
    'position', [.06,.03,.15,.05],...
    'backgroundcolor', fig_bgc,...
    'handlevisibility', 'callback',...
    'enable', 'on');

  callback_fun = [mfilename, '(''cb_cont_slider'');'];
  ui_h5 = uicontrol(fig_h,...
    'style', 'slider',...
    'tooltipstring', 'set contrast...',...
    'tag', 'cont_slider',...
    'min', 0,...
    'max', 1,...
    'value', cont,...
    'units', 'normalized',...
    'position', [.25,.03,.7,.035],...
    'backgroundcolor', slid_bgc,...
    'handlevisibility', 'callback',...
    'callback', callback_fun,...
    'enable', 'on');

  %% generate the subplots for the raw and denoised specs
  ax_h1 = subplot(25,1,[1:9], 'parent', fig_h, 'tag', 'ax1_raw');
  ax_h2 = subplot(25,1,[12:20], 'parent', fig_h, 'tag', 'ax2_denoised');

else
  %% get the axes handles for the subplots
  ax_h1 = findobj(fig_h, 'tag', 'ax1_raw');
  ax_h2 = findobj(fig_h, 'tag', 'ax2_denoised');
  %% get the next button handle
  ui_h1 = findobj(fig_h, 'tag', 'next_button');
  %% get the cancel button handle
  ui_h11 = findobj(fig_h, 'tag', 'cancel_button');
end

%% get the spec data for plotting
rawsigspc = spc_data{1};
sigspc = spc_data{2};
tvecspc = spc_data{3};
fvecspc = spc_data{4};
ev_ID = spc_data{5};
ev_start = spc_data{6};
ev_end = spc_data{7};

%% turn off log of zero warning
warning off
%% plot the raw and denoised spec data
imagesc(tvecspc, fvecspc, 10*log10(rawsigspc), 'parent', ax_h1);
imagesc(tvecspc, fvecspc, 10*log10(sigspc), 'parent', ax_h2);
%% turn on log of zero warning
warning on

%% for some reason, imagesc screws up my title display as well
%% so add titles after
set(get(ax_h1, 'title'), 'string', sprintf('Raw Data, Event #%d', ev_ID));
set(get(ax_h2, 'title'), 'string', sprintf('Denoised Data, Event #%d', ev_ID));
%% in fact it screws up every setting, so do them all now
set(ax_h1, 'xticklabel', '');
set(get(ax_h1, 'ylabel'), 'string', 'Freq (Hz)');
set(get(ax_h2, 'ylabel'), 'string', 'Freq (Hz)');
set(get(ax_h2, 'xlabel'), 'string', 'Time (s)');


%% I don't understand why, but using image or imagesc seems to reset the
%% axis tag to empty
%% force it back to correct tag
set(ax_h1, 'tag', 'ax1_raw');
set(ax_h2, 'tag', 'ax2_denoised');

%% flip the y-axis display
axis(ax_h1, 'xy');
axis(ax_h2, 'xy');

%% draw colorbars
colorbar('peer', ax_h1);
colorbar('peer', ax_h2);

%% draw event bounds
ln_h(1) = line([ev_start, ev_start], [fvecspc(1), fvecspc(end)], 'parent', ax_h1);
ln_h(2) = line([ev_end, ev_end], [fvecspc(1), fvecspc(end)], 'parent', ax_h1);
ln_h(3) = line([ev_start, ev_start], [fvecspc(1), fvecspc(end)], 'parent', ax_h2);
ln_h(4) = line([ev_end, ev_end], [fvecspc(1), fvecspc(end)], 'parent', ax_h2);
set(ln_h, 'color', 'r', 'linewidth', 2, 'linestyle', '--');

%% display the next button
set(ui_h1, 'visible', 'on');
%% display the cancel button
set(ui_h11, 'visible', 'on');
%% pause execution
uiwait(fig_h);

return;




%%~~~~
function cb_next_cancel(gui_tag)
%%~~~~

%% get gui figure handle
fig_h = findobj(0, 'tag', gui_tag);
uiresume(fig_h);
%% get the next button handle
ui_h1 = findobj(fig_h, 'tag', 'next_button');
%% hide the next button
set(ui_h1, 'visible', 'off');
%% get the cancel button handle
ui_h1 = findobj(fig_h, 'tag', 'cancel_button');
%% hide the cancel button
set(ui_h1, 'visible', 'off');

return;




%%~~~~
function cb_set_brit_cont(gui_tag)
%%~~~~

%% get gui figure handle
fig_h = findobj(0, 'tag', gui_tag);
ui_h_brit = findobj(fig_h, 'tag', 'brit_slider');
ui_h_cont = findobj(fig_h, 'tag', 'cont_slider');
tex_h_brit = findobj(fig_h, 'tag', 'brit_text');
tex_h_cont = findobj(fig_h, 'tag', 'cont_text');

%% set up colormap with brightness/contrast
brit = get(ui_h_brit, 'value');
cont = get(ui_h_cont, 'value');
N_full = size(get(fig_h, 'colormap'), 1);

my_colormap = gen_colormap(brit, cont, N_full);
set(fig_h, 'colormap', my_colormap);
set(tex_h_brit, 'string', sprintf('Brightness\n(%3.2f)', brit));
set(tex_h_cont, 'string', sprintf('Contrast\n(%3.2f)', cont));

return;




%%~~~~
function my_colormap = gen_colormap(brit, cont, N_full);
%%~~~~

N_lim = round((1 - cont) * N_full);
my_colormap = flipud(gray(N_lim));
all_off = round(brit * (N_full - N_lim));
all_on = N_full - N_lim - all_off;
my_colormap = [ones(all_off, 3); my_colormap; zeros(all_on, 3)];

return;