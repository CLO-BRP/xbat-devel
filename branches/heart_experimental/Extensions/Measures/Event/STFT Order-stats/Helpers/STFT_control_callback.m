function STFT_control_callback(varargin)

%% set up parameter input palette callbacks
%% to control the behaviour of the uicontrols
%% K.A. Cortopassi, March 2008

cb_obj = varargin{1}; %% supplied by MATLAB

%% get name of the uicontrol object
cb_obj_name = get(cb_obj, 'tag');


switch (cb_obj_name)
  %% switch through specific uiobject tags

  case {'fftsz'}
    %% synch up slider and edit boxes
    %% make even integer
    slider_synch(cb_obj, 'even');

  case ('std_frange')
    %% enable the associated controls ('flo', 'fhi') if 'frange_flag' is checked
    checkbox_group_synch(cb_obj, 'flo', 'fhi');

  case {'flo', 'fhi'}
    %% synch up slider and edit boxes
    %% make sure that lower does not exceed upper
    slider_pair_synch(cb_obj, 'flo', 'fhi');

  case ('denoise_spc')
    %% enable the associated controls ('denoise_type', 'denoise_perc',
    %% 'noise_pad') if 'denoise_flag' is checked
    checkbox_group_synch(cb_obj, 'denoise_type', 'denoise_perc');

  case {'datawin', 'overlap', 'denoise_perc', 'ev_pad', 'meas_perc'}
    %% synch up slider and edit boxes
    slider_synch(cb_obj);

end



return;