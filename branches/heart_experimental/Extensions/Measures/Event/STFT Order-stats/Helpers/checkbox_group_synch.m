function checkbox_group_synch(cb_checkbox, varargin);

%%
%% 'checkbox_group_synch'
%%
%% - function to positively associate a group of ui objects with
%% a check box; if the box is checked, the objects indicated are
%% enabled for user interaction
%%
%%
%%
%% syntax:
%% -------
%%    checkbox_group_synch(cb_checkbox, obj_tag_1, obj_tag_2, ...);
%%
%% input:
%% ------
%%
%%    cb_checkbox == callback checkbox object
%%    obj_tag_1, obj_tag_2, ... == string tags for objects to associate
%%
%% output:
%% -------
%%
%%
%%
%% K.A. Cortopassi, March 2004
%%



%% get handle to parent object
cb_checkbox_parent = get(cb_checkbox, 'parent');


cb_checkbox_assoc_objs = [];

%% get the group of ui objects to associate
for inx = 1:length(varargin)

  cb_checkbox_assoc_objs = [cb_checkbox_assoc_objs;...
    findobj(cb_checkbox_parent, 'tag', varargin{inx})];

end


checkbox_value = get(cb_checkbox, 'value');

%% enable or disable ui group depending on value of checkbox
if ~checkbox_value

  set(cb_checkbox_assoc_objs, 'enable', 'off');

else

  set(cb_checkbox_assoc_objs, 'enable', 'on');

end


return;