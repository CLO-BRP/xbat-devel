function checkbox_group_unsynch(cb_checkbox, varargin);

%%
%% 'checkbox_group_unsynch'
%%
%% - function to negatively associate a group of ui objects with
%% a check box; if the box is checked, the objects indicated are
%% disabled (no user interaction available)
%%
%%
%%
%% syntax:
%% -------
%%    checkbox_group_unsynch(cb_checkbox, obj_tag_1, obj_tag_2, ...);
%%
%% input:
%% ------
%%
%%    cb_checkbox == callback checkbox object
%%    obj_tag_1, obj_tag_2, ... == string tags for objects to dissociate
%%
%% output:
%% -------
%%
%%
%%
%% K.A. Cortopassi, March 2004
%%



%% get handle to parent object
cb_checkbox_parent = get(cb_checkbox, 'parent');


cb_checkbox_assoc_objs = [];

%% get the group of ui objects to dissociate
for inx = 1:length(varargin)

  cb_checkbox_assoc_objs = [cb_checkbox_assoc_objs;...
    findobj(cb_checkbox_parent, 'tag', varargin{inx})];

end


checkbox_value = get(cb_checkbox, 'value');

%% disable or enable ui group depending on value of checkbox
if ~checkbox_value

  set(cb_checkbox_assoc_objs, 'enable', 'on');

else

  set(cb_checkbox_assoc_objs, 'enable', 'off');

end


return;