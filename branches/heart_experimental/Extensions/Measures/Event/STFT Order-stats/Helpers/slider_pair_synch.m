function slider_pair_synch(cb_slider, lower_slider_name, upper_slider_name, slider_type);

%%
%% 'slider_pair_synch'
%%
%% - function to associate and properly control two sliders
%% that together set lower and upper bounds on a value
%%
%%
%%
%% syntax:
%% -------
%%   slider_pair_synch(cb_slider, lower_slider_name, upper_slider_name, slider_type);
%%
%% input:
%% ------
%%   cb_slider == handle to callback slider
%%   lower_slider_name == string tag of lower bounding slider
%%   upper_slider_name == string tag of upper bounding slider
%%   slider_type == numeric class of slider (for now, integer or float)
%%
%% output:
%% -------
%%
%%
%%
%% Kathryn A. Cortopassi, March 2004
%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% synchronize all parts of the callback slider (slider control and
%% text edit box)
if exist('slider_type')
  slider_synch(cb_slider, slider_type);
else
  slider_synch(cb_slider);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% synchronize and properly control the bounding slider pair
%%

%% get info about the callback slider object
cb_slider_name = get(cb_slider, 'tag');
cb_slider_parent = get(cb_slider, 'parent');

%% get lower slider value
lower_slider_objs = findobj(cb_slider_parent, 'tag', lower_slider_name);
lower_slider = findobj(lower_slider_objs, 'style', 'slider');
lower_slider_value = get(lower_slider, 'value');

%% get upper slider value
upper_slider_objs = findobj(cb_slider_parent, 'tag', upper_slider_name);
upper_slider = findobj(upper_slider_objs, 'style', 'slider');
upper_slider_value = get(upper_slider, 'value');


if lower_slider_value > upper_slider_value
  %% values are improperly set for the slider pair

  switch (cb_slider_name)
    %% ammend value of the callback slider

    case (lower_slider_name)
      %% set lower slider value to that of upper

      set(lower_slider, 'value', upper_slider_value);
      slider_synch(lower_slider);

    case (upper_slider_name)

      %% set upper slider value to that of lower
      set(upper_slider, 'value', lower_slider_value);
      slider_synch(upper_slider);

  end

end


return;