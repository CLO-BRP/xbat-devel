function Spec = subMATper(Spec, P, data_form, adjustment);

%**
%** "subMATper.m" 
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Syntax: Spec = subMATper(Spec, P, data_form, adjustment);
%**
%** A function to zero the data that has a value below that of the given
%** 'P' percentile value on a matrix-wide basis;
%** various adjustments can be made to the masked (unthresholded) values 
%**
%** 'Spec' == an MxN spectrogram array (or any array of values)
%** 'P' == is a percent value
%** 'data_form' == a flag telling how the values are stored,
%**                Amplitude, Power, or dB
%** 'adjustment' == a flag telling how to deal with masked values after thresholding,
%**                 None, Bias, Demean, or Binarize
%**
%** NOTE: when you want to accomplish true additive noise correction,
%**       data_form should be power, and adjustment should be bias
%**

%**
%** by Kathryn A. Cortopassi
%** created 21-Sept-2001
%** modified 23-May-2002
%**
%** modifications March 2005
%** 


%% check for the correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));


%% unwrap the spectrogram matrix column-wise and sort the values in ascending order
SpecDist = sort(Spec(:));

%% find our absolute threshold as the Pth percentile value of 'SpecDist' by going to the 
%% index which equals P% of N 
%% N is the total length 'SpecDist', P% of N equals (NP/100) rounded to the nearest integer
num_bin = length(SpecDist);
perc_inx = round((P/100) * num_bin);
perc_inx = min(num_bin, max(1, perc_inx));
abs_thresh = SpecDist(perc_inx);

%% find indices of values equal to or below absolute threshold value
noise_inx = (Spec - abs_thresh) <= 0;
mask_inx = ~noise_inx;

%% zero out the spectrogram values below abs_threshold (considered noise)
Spec(noise_inx) = 0;

%% adjust the masked (unthresholded) spectrogram values
switch (adjustment)
  
  case {'None'}
    %% do nothing 
    
  case {'Bias'}
    Spec(mask_inx) = Spec(mask_inx) - abs_thresh; 
    
  case {'Demean'}
    Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
    
  case {'Binarize'}
    Spec(mask_inx) = 1; 
    
  otherwise
    error(sprintf('Mask adjustment flag ''%s'' not recognized!', data_form));
    
end


%% end function
return;