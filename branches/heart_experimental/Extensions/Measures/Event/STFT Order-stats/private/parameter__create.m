function param = parameter__create(context)

%% set up parameter structure for the STFT_order_stats measurement
%% K.A. Cortopassi, March 2008


%% get some data info from context
nyq = context.sound.samplerate / 2;

fftsz = context.sound.specgram.fft;
datawin = context.sound.specgram.win_length;
overlap = (1 - context.sound.specgram.hop) / datawin;
winfunc = context.sound.specgram.win_type;


%% create empty parameter structure
param = struct;

%% set up defaults

%% FFT size in points for spectrogram generation
param.fftsz = fftsz;
%% data window size for spectrogram generation as a fraction of FFT size
param.datawin = datawin;
%% window overlap for spectrogram generation as a fraction of window length
param.overlap = overlap;
%% data window taper function for spectrogram generation
param.winfunc = winfunc;
%% toggle to plot spectrograms
param.plot_spc = 0;

%% toggle to use standardized frequency range in BW/duration estimate
param.std_frange = 0;
%% low frequency corner of standard range
param.flo = nyq * .25;
%% high frequency corner of standard range
param.fhi = nyq * .75;
%% event pad (s) for measurement (and noise estimation)
param.ev_pad = 5;

%% toggle to use waveform filtering from browser
param.filter_wav = 0;
%% store filter used from browser
param.filter_ext = '';
%% toggle to use spectrogram matrix-based denoising
param.denoise_spc = 0;
%% spectrogram denoising type
param.denoise_type = 'Broadband';
%% denoising parameter (percent or dB depending on type)
param.denoise_perc = 50;

%% percent of power to accumulate for BW/duration estimate
param.meas_perc = 50;


return;