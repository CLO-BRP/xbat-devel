function handles = value__menu__create(par, value, context)

% STFT ORDER-STATS - value__menu__create

handles = [];

field = fieldnames(value);

if isempty(value.(field{1}))
	handles(end + 1) = uimenu(par, 'label', '(Not enough data to process event)', 'enable', 'off'); return;
end

% TODO: we should produce a hierarchical menu, this could be aided by a reorganization of the value struct

for k = 1:numel(field)
	current = value.(field{k});

	% NOTE: we skip non-scalar values for now
	
	if isnumeric(current) && numel(current) > 1
		continue;
	end
	
	handles(end + 1) = uimenu(par, 'label', [field_to_label(field{k}), ':  ', to_str(current)]);  %#ok<AGROW>
end

% db_disp; value


%----------------------
% FIELD_TO_LABEL
%----------------------

function label = field_to_label(field)

label = field;