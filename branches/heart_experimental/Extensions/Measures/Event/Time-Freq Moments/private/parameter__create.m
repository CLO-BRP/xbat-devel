function parameter = parameter__create(context)

% TIME-FREQUENCY MOMENTS - parameter__create

parameter = struct;

parameter.specgram = struct_update(fast_specgram, context.sound.specgram);