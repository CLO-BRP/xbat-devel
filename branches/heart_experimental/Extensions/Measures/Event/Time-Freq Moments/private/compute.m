function [value, context] = compute(event, parameter, context)

% TIME-FREQUENCY MOMENTS - compute

%-------------------
% SETUP
%-------------------

value = struct;

context.sound.specgram = parameter.specgram; 

% TODO: consider whether we want 'filtered' or 'unfiltered' samples

event = event_specgram(event, context.sound, context);

%-------------------
% TIME MOMENTS
%-------------------

%--
% compute local time grid and energy-based weights
%--

% NOTE: scaling by the event rate gives us local time in seconds

local = (1:numel(event.samples)) / event.rate; 

% NOTE: this could be smoother

weight = event.samples.^2; total = sum(weight);

%--
% compute location as weighted mean and deviation from this location
%--

% NOTE: these are computed in time units

location = (local * weight) / total; deviation = (local - location); 

%--
% compute other statistical estimates using the above ingredients
%--

% TODO: update considering the wikipedia articles

scale = (deviation.^2 * weight) / total; scale = sqrt(scale);

skew = (deviation.^3 * weight) / total; skew = skew / scale^3;

kurt = (deviation.^4 * weight) / total; kurt = kurt / scale^4;

%--
% pack time distribution parameters estimates with proper names
%--

% TODO: consider if there is a local measure of time available

value.time.mean = event.time(1) + location;

value.time.deviation = scale;

value.time.skewness = skew;

value.time.kurtosis = kurt;

%-------------------
% FREQ MOMENTS
%-------------------

local = event.specgram.freq';

weight = mean(event.specgram.value, 2); total = sum(weight);

%--
% compute location as weighted mean and deviation from this location
%--

% NOTE: these are computed in time units

location = (local * weight) / total; deviation = (local - location); 

%--
% compute other statistical estimates using the above ingredients
%--

% TODO: update considering the wikipedia articles

scale = (deviation.^2 * weight) / total; scale = sqrt(scale);

skew = (deviation.^3 * weight) / total; skew = skew / scale^3;

kurt = (deviation.^4 * weight) / total; kurt = kurt / scale^4;

%--
% pack freq distribution parameters estimates with proper names
%--

value.frequency.mean = location;

value.frequency.deviation = scale;

value.frequency.skewness = skew;

value.frequency.kurtosis = kurt;

%--
% debug display
%--

if context.debug
	disp(int2str(event.id)); flatten(value)
end
