function [b1, a1, b2, a2] = design_equal_loudness(rate)

% design_equal_loudness - filters 
% -------------------------------
%
% [b1, a1, b2, a2] = design_equal_loudness(rate)
%
% Input:
% ------
%  rate - of signal to filter (def: 44100)
%
% Output:
% -------
%  b1, a1, b2, a2 - filter coefficients
%
% NOTE: based on http://replaygain.hydrogenaudio.org/mfiles/equalloudfilt.m

%--
% handle input
%--

% NOTE:  the user hasn't specified a sampling frequency, use the CD default

if nargin < 2
	rate = 44100;
end

%--
% get filters from cache if possible
%--

persistent FILTER

if isempty(FILTER)
	FILTER = struct;
end

field = ['r', int2str(rate)];

if isfield(FILTER, field)	
	[b1, a1, b2, a2] = deal(FILTER.(field){:}); return;
end

%--
% describe filters
%--

% NOTE: we specify the 80 dB Equal Loudness curve

if rate == 44100 || rate == 48000
	EL80 = [0,120;20,113;30,103;40,97;50,93;60,91;70,89;80,87;90,86;100,85;200,78;300,76;400,76;500,76;600,76;700,77;800,78;900,79.5;1000,80;1500,79;2000,77;2500,74;3000,71.5;3700,70;4000,70.5;5000,74;6000,79;7000,84;8000,86;9000,86;10000,85;12000,95;15000,110;20000,125;rate/2,140];
	
elseif rate == 32000
	
	EL80 = [0,120;20,113;30,103;40,97;50,93;60,91;70,89;80,87;90,86;100,85;200,78;300,76;400,76;500,76;600,76;700,77;800,78;900,79.5;1000,80;1500,79;2000,77;2500,74;3000,71.5;3700,70;4000,70.5;5000,74;6000,79;7000,84;8000,86;9000,86;10000,85;12000,95;15000,110;rate/2,115];
else
	error('Filter not defined for current sample rate');
end

%--
% design filters
%--

% NOTE: convert frequency and amplitude of the equal loudness curve into format suitable for 'yulewalk'

f = EL80(:,1)./(rate/2); m = 10.^((70 - EL80(:, 2)) / 20);

% a MATLAB utility to design a best bit IIR filter

[b1, a1] = yulewalk(10, f, m);

% add a 2nd order high pass filter at 150Hz to finish the job

[b2, a2] = butter(2, (150/(rate/2)), 'high');

%--
% update cache
%--

FILTER.(field) = {b1, a1, b2, a2};