function parameter = parameter__create(context)

% REPLAY-GAIN - parameter__create

parameter = struct;

% NOTE: this is the frame size in seconds, 50 milliseconds is the default for Replay-Gain

parameter.frame = 50 / 1000;

% NOTE: we may be working with fast changing signals, in this case ensure 20 amplitude bins

% NOTE: making clips equal or larger than 1 second will mean that we always use the standard

parameter.adapt = true;

parameter.bins = 20;