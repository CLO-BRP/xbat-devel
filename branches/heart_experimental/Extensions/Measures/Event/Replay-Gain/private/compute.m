function [value, context] = compute(event, parameter, context)

% REPLAY-GAIN - compute

value = struct;

%--
% adapt the frame if needed 
%--

if parameter.adapt
	% NOTE: in this case we want to ensure a sufficient set of 'bins'
	
	fixed = ceil(parameter.frame * context.sound.rate);
	
	if (numel(event.samples) / fixed) < parameter.bins
		
		parameter.window = ones(1, floor(numel(event.samples) / parameter.bins));
	else
		parameter.window = ones(1, fixed);
	end
end

%--
% compute amplitude measurements
%--

a = fast_amplitude(event.samples, parameter.window, 0, 'rms');

%--
% summarize amplitude pack value struct
%--

if isempty(a)
	s = nan(1, 5);
else
	s = five_point_summary(a);
end

value.frame.duration = numel(parameter.window) / context.sound.rate; 

value.frame.count = numel(a); 

value.amplitude.low = s(1); 

value.amplitude.median = s(3); 

value.amplitude.iqr = s(4) - s(2); 

value.amplitude.high = s(end);


%-----------------------------
% FIVE_POINT_SUMMARY
%-----------------------------

function point = five_point_summary(value)

value = sort(value); ix = round(1 + (numel(value) - 1) * [0.05, 0.25, 0.5, 0.75, 0.95]);

point = value(ix);
