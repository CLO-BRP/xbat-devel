//------------------
// INCLUDE FILES
//------------------

#include "mex.h"

//------------------
// FUNCTIONS
//------------------

void check_input (double *x, int N) {

	int k = 0;
	
	for (; k < N - 1; k++) {
		if (*(x + k) > *(x + k + 1)) mexErrMsgTxt("Input is not monotonically increasing.");	
	}
	
	if (*(x + N - 1) > 1) mexErrMsgTxt("Input is not normalized.");
	
}

void quartile_indices (double *q, double *x, int N) {

    int k = 0; double q1 = 0, q2 = 0, q3 = 0;
	
	for (; k < N; k++) {
		if (*(x + k) >= 0.25) { *(q + 0) = k + 1; break; }
	}
	
	for (; k < N; k++) {
		if (*(x + k) >= 0.50) { *(q + 1) = k + 1; break; }
	}
	
	for (; k < N; k++) {
		if (*(x + k) >= 0.75) { *(q + 2) = k + 1; break; }
	}
	
}   

//------------------
// MEX FUNCTION
//------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    double *X; int N; double *q;

    //--
    // INPUT
    //--
    
    // NOTE: this should be a cumulative sum vector for this function to make sense

    X = mxGetPr(prhs[0]); N = mxGetM(prhs[0]) * mxGetN(prhs[0]);
    
    //--
    // OUTPUT
    //--
    	
    // NOTE: the output consists of the quantile indices 
    
    q = mxGetPr(plhs[0] = mxCreateDoubleMatrix(1, 3, mxREAL));
    
    //--
    // COMPUTATION
    //--
    
	// NOTE: the way the check is written we scan the array twice, just be fast not paranoid
	
	// check_input(X, N);
	
    quartile_indices(q, X, N);

}
