function control = view__parameter__control__create(parameter, context)

% TIME-FREQUENCY QUARTILES - view__parameter__control__create

control = empty(control_create);

% TODO: compute index from parameter

ix = 1;

control(end + 1) = popup_control('plot', {'Box', 'Cross'}, 1, 'space', 1.5);