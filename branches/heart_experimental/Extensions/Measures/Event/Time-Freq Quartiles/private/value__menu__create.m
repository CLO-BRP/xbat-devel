function handles = value__menu__create(par, value, context)

% TIME-FREQUENCY QUARTILES - value__menu__create

value.time.median = get_browser_time_string(context.par, value.time.median);

value.info = keep_fields(value, {'id', 'created_at', 'modified_at'});

handles = flat_struct_menu(par, value);
