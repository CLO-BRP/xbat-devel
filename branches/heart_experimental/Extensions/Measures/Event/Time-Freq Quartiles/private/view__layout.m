function handles = view__layout(widget, parameter, context)

% TIME-FREQUENCY QUARTILES - view__layout

handles = [];

%--
% layout and create axes array
%--

layout = layout_create(2);

layout.row.frac = [0.8, 0.2];  layout.col.frac = [0.8, 0.2];

harray(widget, layout);

%--
% set basic axes properties including tags
%--

handle = harray_select(widget, 'level', 1); 

set(handle(end), 'visible', 'off');

