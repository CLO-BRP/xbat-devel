function [handles, context] = view__on__marker__create(widget, data, parameter, context)

% FORMANTS - view__on__marker__create

handles = [];

%--
% display something
%--

ax = findobj(widget, 'type', 'axes', 'tag', 'bottom');

if isempty(ax)
	return;
end

%--
% get marker samples
%--

duck.duration = parameter.frame;

samples = get_marker_samples(data.marker, duck, context);

%--
% create and display AR model
%--

a = lpc(samples, parameter.model_order);

[h, f] = freqz(1, a, 512, context.sound.rate);

line('parent', ax, 'xdata', f, 'ydata', abs(h));