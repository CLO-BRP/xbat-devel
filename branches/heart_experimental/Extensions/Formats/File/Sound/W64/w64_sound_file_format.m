function ext = w64_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Sound Forge W64 (Sony)';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'w64'};

% ext.version = '';

ext.guid = 'b490b66a-6b0d-4ece-aea0-495d05d4da33';

% ext.author = '';

% ext.email = '';

% ext.url = '';

