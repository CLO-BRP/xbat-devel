function parameter = parameter__create(context)

% MP3 - parameter__create

if ~nargin || ~isfield(context, 'file') 
	parameter = struct;
else
	parameter = write_mp3(context.file);
end 
