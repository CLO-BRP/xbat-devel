function ext = wav_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Waveform Audio File Format (Microsoft)';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'wav'};

% ext.version = '';

ext.guid = '13359bc6-acbe-4ebe-9aaa-1db64471ba21';

% ext.author = '';

% ext.email = '';

% ext.url = '';

