function event = event_cache(store, write)

% event_cache - cache MAT-file log events for easy access
% -------------------------------------------------------
%
% event = event_cache(log, write)
%
% Input:
% ------
%  store - a log event structure
%  write - whether to update cache
%
% Output:
% -------
%  event - the event array

%--
% setup
%--

persistent EVENT_CACHE;

if isempty(EVENT_CACHE)
    EVENT_CACHE = struct; 
end

%--
% handle input
%--

if nargin < 2
    write = 0;
end

event = [];

%--
% get or set
%--

hash = ['log_' md5(store.file)];

if ~isfield(EVENT_CACHE, hash)
    write = 1;
end

if isfield(EVENT_CACHE, hash)
    event = EVENT_CACHE.(hash);
end

if isempty(event) || write
    
    log = load(store.file);
    
    event = log.event;
    
    EVENT_CACHE.(hash) = event;
    
end






