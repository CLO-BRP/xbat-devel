function [sql, lines] = create_user_table(force)

% create_user_table - create table for users
% ------------------------------------------
%
% [query, lines] = create_user_table(force)
%
% Input:
% ------
%  force - drop existing table if needed
%
% Output:
% -------
%  sql - query string
%  lines - query representation

%--
% set no force default
%--

opt = create_table; 

if ~nargin || isempty(force)
	force = 0; 
end 

opt.force = force;

%--
% create user prototype
%--

hint = column_type_hints;

user.id = hint.integer; 

user.name = hint.string; 

user.alias = hint.string; 

user.email = hint.string;

user.created_at = hint.timestamp; user.modified_at = hint.timestamp;

%--
% generate create query
%--

opt.constraints = {'UNIQUE (name, email)'};

% NOTE: the table name is inferred from the 'user' variable name

if nargout
	[sql, lines] = create_table(user, [], opt);
else
	create_table(user, [], opt);
end
