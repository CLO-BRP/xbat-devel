function test_sqlite_log

% TODO: generalize so that we can use to test the various log formats

%--
% create new log
%--

% TODO: create a test environment to place this log in

user = get_active_user; 

lib = get_active_library; 

sound = get_library_sounds(lib); 

if isempty(sound)
	error('There are no sounds in current library.');
end

sound = sound(1);

format = 'SQLite'; name = 'Test-Log';

log = new_log(sound, name, format, values, lib, user)

