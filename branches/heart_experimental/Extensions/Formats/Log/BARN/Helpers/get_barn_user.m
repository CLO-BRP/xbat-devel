function out = get_barn_user(in)

% get_barn_user - ensure local representation of barn user
% --------------------------------------------------------
%
% user = get_barn_user(user)
%
% Input:
% ------
%  user - in barn
%
% Output:
% -------
%  user - corresponding in xbat
%
% NOTE: this match is achieved on the basis of the user email

%--
% handle multiple input
%--

if numel(in) > 1
	out = iterate(mfilename, in); return;
end

%--
% check for email
%--

% NOTE: when email is not available we are typically working with a robot user

% NOTE: by using the empty array output the iterate case returns a cell array with possibly empty cells

if isempty(in.email)
	out = []; return;
end

%--
% ensure we have a user with common email
%--

out = get_users('email', in.email);
	
if isempty(out)
	out = user_create; out = struct_update(out, in); out.name = out.email;
	
	out = add_user(out);
	
	user_save(out);
end