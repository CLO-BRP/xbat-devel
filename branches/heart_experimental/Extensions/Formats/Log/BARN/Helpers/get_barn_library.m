function library = get_barn_library(store, library)

%--
% get active library default
%--

if nargin < 2
	library = get_active_library;
end

%--
% make sure we have library author
%--

user = get_users('name', library.author); 

% NOTE: if the author of the library is no longer an user make the current user the author

if isempty(user)
	user = get_active_user;
end

user = set_barn_user(store, user);

%--
% check for and possibly set library
%--

current = get_database_objects_by_column(store, 'library', 'name', library.name);

if ~isempty(current)
	library = current; return;
end

library = rmfield(library, 'id');

library.user_id = user.id;

library = set_barn_objects(store, library);