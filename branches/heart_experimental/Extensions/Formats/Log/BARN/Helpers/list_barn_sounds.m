function sounds = list_barn_sounds(store)

sounds = get_database_objects_by_column(store, 'sound');