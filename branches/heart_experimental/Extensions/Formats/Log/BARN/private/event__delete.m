function result = event__delete(store, id, context)

% BARN - event__delete

result = struct;

if nargin < 2 || isempty(id)
    return;
end

%--
% delete event and basic annotation
%--

if numel(id) == 1
	condition = [' = ', int2str(id), ';'];
else
	condition = [' IN (', str_implode(id, ', ', @int2str), ');'];
end

table = {'event', 'rating', 'tagging', 'noting'}; key = {'id', 'rateable_id', 'taggable_id', 'annotated_id'};

sql = {'BEGIN;'};

for k = 1:numel(table)
	sql{end + 1} = ['  DELETE FROM ', table{k}, ' WHERE ', key{k}, condition];  %#ok<AGROW>
end

sql{end + 1} = 'COMMIT;';

% db_disp delete-query; iterate(@disp, sql); 

query(store, sql);

% TODO: remove extension store entries, these may be substantial, from a storage perspective

% NOTE: these are the previous two iterations of the event and basic annotation code above

% % NOTE: these are not private operations, others may have rated or tagged these events, however all relations must be discarded
% 
% query(store, ['DELETE FROM rating WHERE rateable_id', condition]);
% 
% % [ignore, rating] = get_ratings(store, id, [], 'event');
% % 
% % delete_database_objects(store, rating);
% 
% query(store, ['DELETE FROM tagging WHERE taggable_id', condition]);
% 
% % [ignore, tagging] = get_taggings(store, id, [], 'event');
% % 
% % delete_database_objects(store, tagging);
% 
% query(store, ['DELETE FROM noting WHERE notable_id', condition]);
% 
% % [ignore, annotation] = get_notes(store, id, [], 'event');
% % 
% % delete_database_objects(store, annotation);



