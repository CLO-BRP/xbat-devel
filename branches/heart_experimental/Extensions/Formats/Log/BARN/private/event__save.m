function id = event__save(store, event, context)

% BARN - event__save

%---------------------------------
% SETUP
%---------------------------------

% NOTE: the request user may be a robot annotator, a detector of classifier

user = set_barn_user(store, context.request.user);

% NOTE: the active user is the human operator

active_user = set_barn_user(store, get_active_user);

if user.id  == active_user.id
    robot = [];
else
    robot = user; user = active_user;
end

if ~strcmp(context.request.save, 'all')
	
	id = [event.id];
	
	% NOTE: we should not need these in the future
	
	if numel(id) ~= numel(event)
		% NOTE: the set of identifiers can only be smaller than the set of events
		
		if isempty(id)
			error('Events are missing identifiers in partial save request.');
		end

		error('Some events are missing identifiers in partial save request.');
	end	
end

%---------------------------------
% HANDLE SAVE REQUEST
%---------------------------------

switch context.request.save
	
	% NOTE: we assume partial save requests always occur on saved events with identifiers
	
	case 'tags'
		set_taggings(store, id, {event.tags}, user, 'event');
		
	case 'rating'
		set_ratings(store, id, {event.rating}, user, 'event');
		
	case 'notes'
		% TODO: make sure that save notes handles identifier input
		
% 		set_notes(store, id, {event.notes}, user);
		
	case 'all'
		%--
		% keep basic metadata fields
		%--

		% NOTE: we intentionally lose these in the process of storing the basic event

		tags = {event.tags}; ratings = {event.rating}; % notes = {event.notes};

		%--
		% save basic event
		%--
		
		event = prepare_events_for_store(store, event);
		
		% TODO: cleanup this code, the comma-separated list input limits this operation to only new or only known events
		
		% NOTE: there is no need to have code that tries to consider the mixed case
		
		adding = isempty([event.id]);
		
		event = set_barn_objects(store, event, [event.id]);

		id = keep_fields(event, {'id', 'guid'});
		
		%--
		% get user and save author relation, tags, rating, and notes
		%--

		insert_relation(store, 'event', [id.id], 'user', user.id);

        if ~isempty(robot)
            insert_relation(store, 'event', [id.id], 'user', robot.id);
        end
        
		set_taggings(store, event, tags, user);

		set_ratings(store, event, ratings, user);

% 		set_notes(store, event, notes, user);
		
		if ~adding
			return;
		end
		
		%------------------------------------
		
		% TODO: this should allow for a finer, possibly faster, update
		
% 		update_relation_count(store, 'sound', 'event');
% 		
% 		update_relation_count(store, 'log', 'event');
				
		% NOTE: the 'ix' allows us to reconstruct the [event.log_id] array using logs, it contains the repetitions
		
		[logs, ignore, ix] = unique([event.log_id]); %#ok<ASGLU>
		
		sql{1} = 'BEGIN;';
 		
		for k = 1:numel(logs)	
			added = sum(ix == k); 
			
			log = get_database_objects(store, 'log', logs(k)); updated = log.events_count + added;
			
			sql{end + 1} = ...
				['UPDATE log SET events_count = ', int2str(updated),' WHERE log.id = ', int2str(log.id), ';']; %#ok<*AGROW>
						
			if ~isempty(log.sound_id)
				
				sound = get_database_objects(store, 'sound', log.sound_id); updated = sound.events_count + added;
				
				sql{end + 1} = ...
					['UPDATE sound SET events_count = ', int2str(updated),' WHERE sound.id = ', int2str(sound.id), ';'];
			end
		end
			
		sql{end + 1} = 'COMMIT;';

% 		db_disp fast-update-of-relation-counts-here; stack_disp; iterate(@disp, sql); 
		
		[status, result] = query(store, sql); %#ok<NASGU,ASGLU>

		%------------------------------------
			
	otherwise
		
		error(['Unrecognized request to save ''', context.request.save, '''.']);	
end


%-------------------------------------
% PREPARE_EVENTS_FOR_STORE
%-------------------------------------

function event = prepare_events_for_store(store, event)

%--
% get event-relevant recording info from store
%--

% NOTE: we ask only for recordings that contain event starts

file = [event.file]; position = unique([file.position]);

sql = [ ...
	'SELECT * FROM recording_sound', ...
	' WHERE sound_id = ', int2str(store.log.sound_id), ...
	' AND position IN (', str_implode(position, ', ', @int2str), ');' ...
];

[ignore, relation] = query(store, sql); %#ok<ASGLU>

% NOTE: we translate the sound file position in the input event into a recording identifier

position = [relation.position]; recording_id = zeros(size(event));

for k = 1:numel(event)
	
	try
		recording_id(k) = relation(position == event(k).file.position).recording_id;
	catch
		nice_catch(lasterror, 'Problem setting recording relation, event.recording_id set to -1.');

		recording_id(k) = -1;
	end
end

recording_start = [file.start];

%--
% get table event and append BARN-specific foreign-keys
%--

% NOTE: this function is also used to store events in cache.db

event = get_table_event(event(:));

% NOTE: an event is contained in a log and starts in a recording

for k = 1:numel(event)

	event(k).recording_id = recording_id(k); event(k).recording_start = recording_start(k);

	event(k).log_id = store.log.id;

	event(k).sound_id = store.log.sound_id;
end
