function ext = mobysound_log_format

ext = extension_create(mfilename);

ext.short_description = 'MobySound text format';

% ext.category = {};

% ext.ext = {};

ext.version = '0.1';

ext.guid = 'cef6a0e0-d023-4eeb-90c4-45309112203c';

ext.author = 'Matt';

ext.email = 'matthew.robbins@gmail.com';

ext.url = '';

ext.enable = false;

