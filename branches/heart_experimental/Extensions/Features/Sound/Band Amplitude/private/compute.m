function [feature, context] = compute(page, parameter, context)

% BAND AMPLITUDE - compute

%--
% filter page to focus on band of interest
%--

if isempty(page.filtered)
	page.filtered = page.samples; 
end

if parameter.whiten
	page.filtered = filter([1, -1], 1, page.filtered);
end

if isfield(parameter, 'filter')
	page.filtered = filter(parameter.filter.b, parameter.filter.a, page.filtered);
end

%--
% compute amplitude feature on the band filtered signals
%--

% NOTE: the parent detector will take care of setting the proper frequency bounds

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);
