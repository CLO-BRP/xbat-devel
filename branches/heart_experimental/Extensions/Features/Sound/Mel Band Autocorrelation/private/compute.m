function [feature, context] = compute(page, parameter, context)

% MEL BAND AUTOCORRELATION - compute

page = extend_page_with_buffer(page);

% feature = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--------------------------

% TODO: much of this belongs in a parameter compile function

% get energy "sampling rate"
fps = 1/feature.resolution.time;

acorr_params = fast_autocorr;

% set FFT length to reasonable value (two times max lag + zero-lag)
acorr_params.fft = ceil((fps*parameter.max_lag))*2+1;

% set hop size
acorr_params.hop = (fps/parameter.temp_res)/acorr_params.fft;


%--------------------------


% consider multiple channels
if ~iscell(feature.spectrogram.value)
    
    % get windowed autocorrelation
    [R, lags, time] = fast_autocorr(feature.band_energy.value', fps,acorr_params );
    feature.band_autocorrelation.value=R;
else
    for k=1:numel(feature.spectrogram.value)
        [R, lags, time] = fast_autocorr(feature.band_energy.value{k}', fps,acorr_params);
        feature.band_autocorrelation.value{k}=R;
    end
end

feature.band_autocorrelation.lags=lags;

feature.band_autocorrelation.time = time + page.start;

feature.band_autocorrelation.time_step=time(2)-time(1);

feature.band_autocorrelation.rate=1/(time(2)-time(1));


% TODO: this function is likely to be factored, if it does not already exist


function page = extend_page_with_buffer(page)

if ~page.buffer
	return;
end

%--
% extend page
%--

page.start = page.start - page.buffer;

page.duration = page.duration + 2 * page.buffer;

page.samples = [page.before.samples; page.samples; page.after.samples];

%--
% remain consistent
%--

page.before.samples = []; page.after.samples = []; page.buffer = 0;



