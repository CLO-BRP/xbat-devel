function [center, label] = ansi_center_frequencies(low, high, voices)

% ansi_center_frequencies - center of ANSI standard bandpass filters
% ------------------------------------------------------------------
%
% [center, label] = ansi_center_frequencies(low, high, voices)
%
% Input:
% ------
%  low - lowest center to consider
%  high - highest center to consider
%  voices - number of voices per octave
%
% Output:
% -------
%  center - actual centers
%  label - typical labels for actual centers

%---------------------------------
% HANDLE INPUT
%---------------------------------

% NOTE: together the default values output ANSI standard frequencies 

%--
% set default three voices
%--

if nargin < 3 || isempty(voices)
	voices = 3; 
else
	if round(voices) ~= voices
		error('The number of voices per octave must be an integer.');
	end
end 

%--
% set frequency bounds 
%--

if nargin < 2 || isempty(high)
	high = 5500;
end 

if nargin < 1 || isempty(low)
	low = 90;
end

%---------------------------------
% GET FREQUENCIES AND LABELS
%---------------------------------

%--
% compute and select relevant fractional-octave spaced center-frequencies 
%--

center = (1000).*((2^(1/voices)).^(-10:24)); 	

center = center((center >= low) & (center <= high));

%--
% compute human readable labels for the above frequencies
%--

if nargout > 1
	label = round_to_order(center);
end

% NOTE: what follows is older code, with some human tuned numbers

% NOTE: the grouping below indicates octave partitioning and is related to a multirate implementation for the lower frequencies

% label = [ ...
% 	100, 125, 160, ...
% 	200, 250, 315, ...
% 	400, 500, 630, ...
% 	800, 1000, 1250, ...
% 	1600, 2000, 2500, 3150, 4000, 5000 ...
% ];