function [feature, context] = compute(page, parameter, context)

% FROST - compute

%--
% compute parent feature
%--

% feature = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% compute frequency order statistics
%--

if ~iscell(feature.spectrogram.value)
	
	frost.value = feature.freq(percent_index(feature.spectrogram.value, parameter.percent));
	
else
	
	for k = 1:numel(feature.spectrogram.value)
		frost.value{k} = feature.freq(percent_index(feature.spectrogram.value{k}, parameter.percent));
	end
	
end

frost.time = feature.time';

feature.frost = frost;


%-----------------------
% PERCENT_INDEX
%-----------------------

function IX = percent_index(A, percent) 

%--
% set default percentiles
%--

if nargin < 2 || isempty(percent)
	percent = [5, 25, 50, 75, 95];
end

%--
% normalize and compute percent ranks
%--

% NOTE: we normalize along the columns and use this to compute the non-trivial percent indices

N = A ./ repmat(sum(A, 1), size(A, 1), 1); C = cumsum(N, 1);

IX = zeros(numel(percent), size(N, 2));

for j = 1:numel(percent)
	
	switch percent(j)
		
		case 0, [ignore, IX(j, :)] = min(A, [], 1);
			
		case 100, [ignore, IX(j, :)] = max(A, [], 1);
			
		otherwise
			
			% TODO: consider how to vectorize this operation
			
			for k = 1:size(N, 2)
				
				ix = find(C(:, k) > (percent(j) / 100), 1);
				
				if isempty(ix)
					IX(j, k) = size(N, 1);
				else
					IX(j, k) = ix;
				end
				
			end
	
	end

end

