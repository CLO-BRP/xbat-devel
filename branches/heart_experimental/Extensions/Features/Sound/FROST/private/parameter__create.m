function parameter = parameter__create(context)

% FROST - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.percent = [5, 25, 50, 75, 95];