function parameter = parameter__create(context)

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.bank_type = 'rectangular';

parameter.bands = linspace(0,1,9);