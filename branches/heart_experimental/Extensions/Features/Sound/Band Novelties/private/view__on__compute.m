function [handles, context] = view__on__compute(widget, data, parameter, context)

% BAND NOVELTIES - view__on__compute

% NOTE: we use the energy display for novelty curves, thus, the feature 
% data is copied to the corresponding energy field 

data.band_energy=data.band_novelty;

fun = parent_fun(mfilename('fullpath'),1); [handles, context] = fun(widget, data, parameter, context);



