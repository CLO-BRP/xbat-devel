function [feature, context] = compute(page, parameter, context)

% Band Novelties - compute

% ----
% NOTE: this is essentailly the code from band_energy. 
% there should be a generalized band operations extension to inherit the
% band definition and creation from
% ----


%--
% compute parent feature (spectrogram)
%--
fun = parent_fun(mfilename('fullpath'),2); [feature, context] = fun(page, parameter, context);


%--
% compute novelties
%--

nyquist=context.sound.samplerate/2;

if ~iscell(feature.spectrogram.value)
	[band_novelty.max band_novelty.value] = novelty_genwindow(feature.spectrogram.value,feature.freq./nyquist,parameter);	
else
	
	for k = 1:numel(feature.spectrogram.value)
		[band_novelty.max{k} band_novelty.value{k}] = novelty_genwindow(feature.spectrogram.value{k},feature.freq./nyquist,parameter);
	end
end

% NOTE: we do not know the novelty of the first spectrogram slide
band_novelty.time = feature.time(2:end)'; 

feature.band_novelty = band_novelty;


%-----------------------
% GENERIC BAND ENERGY
%-----------------------

function [valmax, out]= novelty_genwindow(specf, freqs, parameter) %#ok<INUSL>

% NOTE: get novelties per coefficient, weight via the band window and add.
numbands=size(parameter.band_details,2);

% get the energy increase for each spectral coefficient
specdiff = max(diff(specf,1,2),0);

out = parameter.bank' * specdiff;

valmax = max(out,[],2);

