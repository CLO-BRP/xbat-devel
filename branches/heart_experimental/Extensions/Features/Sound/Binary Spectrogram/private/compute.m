function [feature, context] = compute(page, parameter, context)

% BINARY SPECTROGRAM - compute

% feature = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% subtract simple background estimate
%--

X = feature.spectrogram.value;

if ~iscell(X)
	X = process_channel(X, parameter, context);
else
	for k = 1:numel(X)
		X{k} = process_channel(X{k}, parameter, context);
	end
end
	
feature.spectrogram.value = X;


%--------------------------
% PROCESS_CHANNEL
%--------------------------

function X = process_channel(X, parameter, context, k, N)

%--
% perform background subtraction
%--

% [U, S, V] = svds(X, 1);
% 
% X = X - U * S * V';

% NOTE: this uses a recent minimum background estimate

X = mbe(X);

%--
% determine anomaly level, and apply threshold
%--

% TODO: consider a strong contrast as fuzzy threshold

[n, x] = hist(X, 101); p = n / sum(n);

[p0, ix0] = max(p); t = 1/20; x0 = x(ix0); %#ok<NASGU>

ix1 = ix0 + find(p(ix0 + 1:end) < t, 1, 'first'); x1 = x(ix1);

if context.debug
	
	figure(context_figure('create', context, 'debug'));
	
	semilogy(x, p); hold on; plot(x0, p0, 'ro'); plot(x1, p(ix1), 'bo'); hold off;
	
	figure(context.par);
	
end

X = double(X > parameter.alpha * x1);

%--
% a small amount of morphological processing to improve connection
%--

% TODO: determine what amount of processing belongs here and which elsewhere

X = morph_close(X, ones(5, 21));

% X = morph_close(X, se_ball(2));

% X = morph_open(X, se_ball(2));

% X = morph_dilate(X, se_ball(1));
