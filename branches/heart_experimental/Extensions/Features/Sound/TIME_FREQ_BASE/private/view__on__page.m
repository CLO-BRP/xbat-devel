function [handles, context] = view__on__page(widget, data, parameter, context)

% SPECTROGRAM - view__on__page

%--
% perform parent update
%--

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% update colormap
%--

set_spectrogram_colormap(widget, parameter);

%--
% set cursors
%--

display_harmonic_cursor(widget, parameter, context);

%--
% allow clicks to go through spectrogram images
%--

% TODO: this is a very blunt, consider making it smarter

set(findobj(widget, 'type', 'image'), 'hittest', 'off');













