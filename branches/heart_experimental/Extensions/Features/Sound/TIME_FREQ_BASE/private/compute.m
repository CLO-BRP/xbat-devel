function [feature, context] = compute(page, parameter, context)

% TIME_FREQ_BASE - compute

% TODO: we should easily be able to dynamically pass remaining 'args' to function call

[feature.distribution.value, feature.time, feature.freq] = parameter.fun(page.samples, context.sound.rate);
