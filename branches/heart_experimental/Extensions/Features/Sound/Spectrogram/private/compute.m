function [feature, context] = compute(page, parameter, context)

% SPECTROGRAM - compute

% TODO: generalize next section to 'required_page' and turn this into method

% NOTE: the pager can then query the various extensions and ensure an adequate buffer

% TODO: create a helper to assist in getting the required samples

%-------------------------
% GET REQUIRED SAMPLES
%-------------------------

% NOTE: the 'old' parameter field holds an adaptation of the feature parameter to the old 'fast_specgram' parameters

%--
% compute required duration
%--

% NOTE: the required duration will equal or exceed the page duration

% NOTE: the new duration only includes the end modification not the start

duration = required_page_duration(page, parameter.old, context);

%--
% get any additional required samples
%--

% NOTE: we work with filtered samples if available

source = ternary(isempty(page.filtered), 'samples', 'filtered');

samples = page.(source);

change = duration - page.duration;

if change
	
	deficit = ceil(change * page.rate);

	% NOTE: the buffer is symmetric so the single test is enough, we add samples at the start and end of the page
	
	if deficit < size(page.after.samples, 1)
		
		samples = [page.before.(source)(end - deficit + 1:end, :); samples; page.after.(source)(1:deficit, :)];
	
	end
	
end

%-------------------------
% COMPUTE SPECTROGRAM
%-------------------------

[X, freq, time] = fast_specgram(samples, context.sound.rate, 'norm', parameter.old);

if parameter.smooth
	
	X = smooth_spectrogram(X, parameter);
	
end

%-------------------------
% PACK FEATURE
%-------------------------

% NOTE: non-struct feature fields are skipped by 'reduce'

% NOTE: we modify the start of the page considering 'change'

feature.time = page.start - change + time;

feature.freq = freq;

feature.resolution = specgram_resolution(parameter.old, context.sound.rate, 1);

feature.time_step = feature.resolution.time;

feature.rate = 1 / feature.resolution.time;

% NOTE: this is the only field 'reduced'

feature.spectrogram.value = X; 

% db_disp 'spectrogram size'; size(X)

% TODO: factor this function, extend to handle dimensions separately and 'nan'

% TODO: update 'hist_view', this could be useful


%-------------------------
% SMOOTH_SPECTROGRAM
%-------------------------

function X = smooth_spectrogram(X, parameter)

%--
% get method and width from parameters
%--

	method = lower(parameter.method); width = parameter.width;

	if iscell(method)
		method = method{1};
	end

%--
% filter considering possibly multiple images in cell
%--

if ~iscell(X)

	switch method

		case 'binomial'
			X = linear_filter(X, get_binomial(width));

		case 'median'
			X = median_filter(X, ones(width));

	end

else

	switch method

		case 'binomial'
			for k = 1:numel(X)
				X{k} = linear_filter(X{k}, get_binomial(width));
			end

		case 'median'
			for k = 1:numel(X)
				X{k} = median_filter(X{k}, ones(width));
			end

	end

end


%-------------------------
% GET_BINOMIAL
%-------------------------

function F = get_binomial(width)

%--
% setup cache and check cache
%--

persistent CACHE;

if isempty(CACHE)
	CACHE = cell(1, 128);
end

% NOTE: this only works for width 3 to 255

ix = 0.5 * (width - 1);

if ~isempty(CACHE{ix})
	F = CACHE{ix}; return;
end

%--
% get filter, possibly decomposed, and update cache
%--

if width < 7
	F = filt_binomial(width, width);
else
	F = filt_decomp(filt_binomial(width, width));
end

CACHE{ix} = F;


