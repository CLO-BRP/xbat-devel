function X = resample_spectrogram(X, freq, parameter)

%--
% consider frequency scale
%--

switch lower(cellfree(parameter.scale))

	case 'linear'

		g = [];

	case 'log'

		if parameter.min_freq == 0
			start = 0;
		else
			start = log2(parameter.min_freq);
		end

		stop = log2(parameter.max_freq);

		g = 2.^linspace(start, stop, size(X, 1) + 1)'; g(1) = [];

	case 'linear-log'

		split = cellfree(parameter.split); split = eval(split(1:2)) / 100;

		n1 = floor(split * size(X, 1)); n2 = size(X, 1) - n1;

		g1 = linspace(parameter.min_freq, parameter.split_freq, n1);

		g2 = 2.^linspace(log2(parameter.split_freq), log2(parameter.max_freq), n2 + 1); g2(1) = [];

		g = [g1, g2]';

end

if ~isempty(g)

	% TODO: do this more efficiently and better
	
	for j = 1:size(X, 2)
		X(:, j) = interp1q(freq, X(:, j), g);
	end

	% TODO: smooth lower frequencies more aggresively
	
	X = linear_filter(X, [0.25, 0.5, 0.25]);

end

%--
% resample for display size
%--

% TODO: make this smarter if at all

% [r, c] = size(X);
% 
% if r > 1050
% 	X = imresize(X, [1024, c]);
% end



