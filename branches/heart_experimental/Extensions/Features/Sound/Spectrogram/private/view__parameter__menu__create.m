function handles = view__parameter__menu__create(par, parameter, context)

% SPECTROGRAM - view__parameter__menu__create

handles = scaffold_menu(par, 'view', context, {'orientation'});

set(findobj(handles, 'tag', 'invert'), 'separator', 'on');