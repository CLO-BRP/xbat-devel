function handles = display_harmonic_cursor(widget, parameter, context)

%--
% check for widget before we continue
%--

if isempty(widget)
	return; 
end

%----------------------
% DELETE CURSOR
%----------------------

handles = findobj(widget, 'type', 'line'); tags = get(handles, 'tag');

delete(handles(strmatch('HARMONIC::', tags)));

% NOTE: nothing to do if the cursor is not displayed

if ~parameter.cursor
	handles = []; return; 
end

%----------------------
% DISPLAY CURSOR
%----------------------

%--
% setup for cursor creation
%--

page = context.page; 

time = [page.start, page.start + page.duration];

freq = 0:parameter.fundamental:((parameter.harmonics + 1)* parameter.fundamental); freq([1, end]) = [];
	
freq = map_frequency(freq, parameter, context, 0);

%--
% set cursors in each channel
%--

handles = nan(parameter.harmonics, numel(page.channels));

for j = 1:numel(page.channels)

	%--
	% try to get display axes
	%--
	
	channel = page.channels(j); 

	ax = get_channel_axes(widget, channel);

	if isempty(ax)
		continue; 
	end

	%--
	% display cursor lines
	%--
	
	for k = 1:parameter.harmonics

		handles(k, j) = create_line(ax, ['HARMONIC::', int2str(k)], ...
			'xdata', time, ... 
			'ydata', freq(k) * [1 1] ...
		);

	end

end

%--
% set cursor line callbacks
%--

context.handles = handles;

set(handles, ...
	'color', 0.75 * [1 0 1], ...
	'buttondownfcn', {@cursor_callback, 'start', parameter, context} ...
);


%-----------------------------------
% CURSOR_CALLBACK
%-----------------------------------

function cursor_callback(obj, eventdata, mode, parameter, context) %#ok<INUSL>

persistent POS AX PAR POINTER PAL; 

switch mode
	
	case 'start'

		%--
		% setup persistent values
		%--
		
		% NOTE: get cursor position, harmonic index
		
		part = str_split(get(obj, 'tag'), '::'); 
		
		POS = eval(part{2});
		
		% NOTE: get relevant handles using callback object once
		
		AX = get(obj, 'parent'); PAR = get(AX, 'parent'); POINTER = get(PAR, 'pointer');
		
		% NOTE: get extension palette so we may update the fundamental control
		
		% NOTE: we get first the handle, then we check handle and control, if either is a problem we clear the handle
		
		PAL = context.pal;
		
		if ~isempty(PAL) && ~ishandle(PAL)
			PAL = [];
		end
		
		if ~isempty(PAL) && isempty(get_control(PAL, 'fundamental'))
			PAL = []; 
		end
		
		%--
		% set pointer and callbacks
		%--
		
		set(PAR, ...
			'pointer', 'top', ...
			'windowbuttonmotionfcn', {@cursor_callback, 'update', parameter, context}, ...
			'windowbuttonupfcn', {@cursor_callback, 'stop'} ...
		);
		
	case 'update'
		
		% TODO: limit the position of the first cursor to lie within the page
		
		%--
		% update cursors
		%--
		
		point = get(AX, 'currentpoint'); base = point(1, 2);
		
		base = map_frequency(base, parameter, context) / POS;
		
		for k = 1:parameter.harmonics	
		
			freq = map_frequency(k * base * [1 1], parameter, context, 0);
			
			set(context.handles(k, :), 'ydata', freq);
		
		end
		
		%--
		% update control
		%--
		
		% TODO: consider doing this only on stop, or using a lighter weight update
		
		if ~isempty(PAL)
			set_control(PAL, 'fundamental', 'value', base);	
		end
		
	case 'stop'
	
		set(PAR, ...
			'pointer', POINTER, ...
			'windowbuttonmotionfcn', [], ...
			'windowbuttonupfcn', [] ...
		);
		
end