function handles = view__layout(widget, parameter, context)

% SIGNAL_FEATURE - view__layout

if ~isfield(parameter, 'orientation')
	handles = signal_layout(widget, context);
else
	handles = signal_layout(widget, context, [], parameter.orientation);
end




