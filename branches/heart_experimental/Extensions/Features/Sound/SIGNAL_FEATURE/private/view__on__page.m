function [handles, context] = view__on__page(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__page

page = context.page; 

slider = get_time_slider(context.par); page.start = slider.value; 

handles = display_time_guides(widget, page, context);