function [handles, context] = view__on__event__display(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__event__display

%--
% get axes
%--

% TODO: this could be a get channel axes function

ax = get_channel_axes(widget, data.event.channel);

if isempty(ax)
	return;
end 

%--
% display event
%--

opt = time_event_display; 

% NOTE: the event display color is what 'event_view' uses for ghost events

opt.color = data.event.display.color; % opt.color = data.log.color; 

opt.linestyle = data.log.linestyle; opt.linewidth = data.log.linewidth;

handles = time_event_display(ax, data.event, opt);

% NOTE: this relies on handle packing within 'time_event_display'

% TODO: we need to feed this function the handles to consider in the reorder

% NOTE: perhaps restacking happens elsewhere

faster_uistack(handles(2), 'bottom');

%--
% add selection behavior to event display
%--

par = context.par;

if isempty(par)
	return;
end

handle = findobj(handles, 'type', 'patch');

if isempty(handle)
	return;
end

% NOTE: we don't need a buttondown or menus attached to the ghost events

if ~isequal(data.event.display.color, data.log.color)
	return; 
end

set(handle, ...
	'buttondownfcn', {@select_event_callback, par, data.log, data.event.id} ...
);

%--
% display measures
%--

% TODO: this may not be the way to do this

% %  NOTE: we can pass the display axes to the measure display helper, and some request
% 
% data.event.axes = ax; data.request = {'time'};
% 
% % TODO: get measures available with their extension
% 
% available = fieldnames(data.event.measure);
% 
% for k = 1:numel(available)
% 	
% 	%--
% 	% get measure extension ask if it cares
% 	%--
% 	
% 	[ext, ignore, ext.context] = get_cached_browser_extension('page', 'event_measure', par, available{k});
% 	
% 	fun = ext.fun.view.on.event.display;
% 	
% 	if isempty(fun)
% 		continue;
% 	end 
% 	
% 	% TODO: implement 'request' handling description and skip displays not supporting request
% 	
% 	%--
% 	% call measure display helper to produce display
% 	%--
% 	
% 	data.value = data.event.measure.(available{k}).value;
% 	
% 	% TODO: this caused a nasty recursive computation in the case of 'Peak Amplitude' before adding this method to it
% 	
% 	[part, context] = fun(widget, data, ext.parameter, ext.context);
% 
% end


%-------------------------------
% SELECT_EVENT_CALLBACK
%-------------------------------

function select_event_callback(obj, eventdata, par, log, id) %#ok<INUSL>

% NOTE: the selection event must come from the index m log for this to work

set_browser_selection(par, [], log, id);

