function [handles, context] = view__on__selection__edit__stop(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__selection__edit__stop

handles = [];

%--
% clear selection rubber band display
%--

tag = 'selection_rubber_band';

delete(findobj(widget, 'tag', tag));
