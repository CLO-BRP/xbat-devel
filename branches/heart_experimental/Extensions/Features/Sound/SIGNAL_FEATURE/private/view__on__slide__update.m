function [handles, context] = view__on__slide__update(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__slide

%--
% update time guides
%--

page = context.page; page.start = data.slide.time;

handles = display_time_guides(widget, page, context);

%--
% update selection display
%--

% NOTE: we check that we have a logged selection in page

% selection = data.selection;
% 
% if ~isempty(selection.event) && ~isempty(selection.log) && event_in_page(selection.event, page, context.sound)
% 	part = view__on__selection__create(widget, data, parameter, context); handles = [handles, part];
% end
