function value = get_feature_value(feature, time, mode)

% get_feature_value - get feature values at selected times and channel
% --------------------------------------------------------------------
%
% value = get_feature_value(feature, time, mode)
%
% Input:
% ------
%  feature - feature
%  time - time
%  mode - 'interp' or 'select'
%
% Output:
% -------
%  value - feature at selected times and channel

% NOTE: considered adding 'channel' selection, however 'channel' packing can be very idiosyncratic!

% TODO: factor 'reduce' into something like this

% TODO: add parameters that control various pass-through behaviors and interpolation

% TODO: we also need another function that simply gets already computed values in a time interval

% NOTE: perhaps this function should be called 'feature_interp'

%--
% set default mode to interpolate
%--

if nargin < 3
	mode = 'interp';
end

%--
% get common time 
%--

if isfield(feature, 'time')
	common.time = feature.time;
else
	common.time = [];
end
	
%--
% reduce feature fields
%--

field = fieldnames(feature);

% TODO: the following field filtering and the first part of the next loop are not compatible

% NOTE: we remove fields that are not structured or 'time' from list of fields to process

for j = length(field):-1:1
	
	if ~isstruct(feature.(field{j})) || strcmp(field{j}, 'time')
		field(j) = [];
	end
	
end

for j = 1:length(field)
	
	% NOTE: we pass fields that are not value-time structured
	
	% TODO: reconsider this next step
	
	if ~isstruct(feature.(field{j}))
		value.(field{j}) = feature.(field{j});
	end
	
	%--
	% get field time and reduced time
	%--
	
	if ~isfield(feature.(field{j}), 'time')
		time0 = common.time;
	else
		time0 = feature.(field{j}).time;
	end 
	
	% NOTE: continue if we cannot deduce time relation
	
	% TODO: pass these through as well? probably not
	
	if isempty(time0)
		value.(field{j}).interp = 0; continue;
	end

	%--
	% get feature value at desired times
	%--
	
	% TODO: consider other strategies and handle other packings
	
	% NOTE: this is meant for a 'time', 'value' packing in a feature field
	
	element = fieldnames(feature.(field{j}));
	
	for k = 1:length(element)
		
		switch mode
			
			% NOTE: in this mode we obtain feature values at a specific set of times
			
			case 'interp'
				
				% NOTE: set the feature time to the desired output time
				
				if strcmp(element{k}, 'time')
					value.(field{j}).time = time; continue;
				end

				% NOTE: we skip interpolation when the time and value are of different lengths

				% NOTE: we record that the interpolation did not make sense

				if length(feature.(field{j}).(element{k})) ~= length(time0)
					value.(field{j}).interp = 0; continue;
				end

				% TODO: extend parametrization to include resampling function

				% NOTE: for display we want a fast method, tried 'interp1q' but it failed for multi-channel

				value.(field{j}).(element{k}) = interp1(time0, feature.(field{j}).(element{k}), time , 'linear');

				value.(field{j}).interp = 1;
		
			% NOTE: in this mode we select existing computed values within a specified time interval
			
			case 'select'
				
				% TODO: we want to get indices of interest by checking for times within interval of interest

				
		end
				
				
	end
	
end

