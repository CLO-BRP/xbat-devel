function ax = get_channel_axes(par, channel, display, sep)

% TODO: consider moving this function to a more global place

%------------------
% HANDLE INPUT
%------------------

%--
% set default tag separator and display name
%--

if nargin < 4
	sep = '::';
end 

if nargin < 3
	display = '';
end 

%------------------
% FIND AXES
%------------------

%--
% get axes handles and tags
%--

ax = findobj(par, 'type', 'axes'); tags = get(ax, 'tag');

%--
% find axes by matching tag
%--

% NOTE: we may simply match the channel number or string, or that preceded by a display name

match = int2str(channel);

if ~isempty(display)
	match = [display, sep, match];
end

ix = find(strcmp(match, tags));

if isempty(ix)
	ax = []; return;
end

ax = ax(ix);
