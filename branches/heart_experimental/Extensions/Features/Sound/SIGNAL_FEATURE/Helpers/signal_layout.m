function handles = signal_layout(widget, context, display, orientation, order)

% signal_layout - layout a signal type display
% --------------------------------------------
%
% handles = signal_layout(widget, context, display, orientation, order)
%
% Input:
% ------
%  widget - parent
%  context - context
%  display - display names
%  orientation - 'horizontal' or 'vertical'
%
% Output:
% -------
%  handles - handles

%----------------
% HANDLE INPUT
%----------------

%--
% default order and orientation
%--

if nargin < 5
	order = 'interleave';
end

if nargin < 4 || isempty(orientation)
	orientation = 'horizontal';
end

if iscell(orientation)
	orientation = orientation{1};
end

horizontal = strcmpi(orientation, 'horizontal');

%--
% default display
%--

% NOTE: this is a single un-named display

if nargin < 3 || isempty(display)
	display = {''};
end

%----------------
% LAYOUT
%----------------

%--
% clear axes from figure
%--

delete(findobj(widget, 'type', 'axes'));

%--
% layout axes and get handles
%--

% NOTE: this is a generic stacked channels feature display, factor!

total = numel(display) * numel(context.page.channels);

if horizontal

	layout = layout_create(total, 1);

	layout.margin(1) = 0.5; layout.margin(4) = 1.25;

	layout.row.pad = 0.75 * ones(total - 1, 1);
	
	prefix.label = 'y'; prefix.grid = 'x';

else
	
	layout = layout_create(1, total);
	
	layout.margin(1) = 0.5; layout.margin(4) = 1.25;

	layout.col.pad = 0.75 * ones(total - 1, 1);
	
	prefix.label = 'x'; prefix.grid = 'y';
	
end

% TODO: make use of the 'harray' layout engine

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

% TODO: add title display when we improve the layout

switch order
	
	case 'stack'
		
		for j = 1:numel(display)
			
			for k = 1:numel(context.page.channels)
				
				str = int2str(context.page.channels(k));

				if isempty(display{j})
					set(handles(k), 'tag', str);
				else
					set(handles(k), 'tag', [str, '::', display{j}]);		
				end

				set(get(handles(k), [prefix.label, 'label']), 'string', ['Ch ', str]);

			end
			
		end
		
	case 'interleave'
		
		for k = 1:numel(context.page.channels)

			str = int2str(context.page.channels(k));
			
			for j = 1:numel(display)	

				if isempty(display{j})
					set(handles(k), 'tag', str);
				else
					set(handles(k), 'tag', [str, '::', display{j}]);		
				end

				set(get(handles(k), [prefix.label, 'label']), 'string', ['Ch ', str]);

			end

		end
		
end

% NOTE: this tagging convention did not scale and has been superseded by widgets

% % NOTE: this is the tagging convention currently used by 'get_play_axes'
% 
% for k = 1:length(handles)
% 	
% 	str = int2str(context.page.channels(k));
% 	
% 	set(handles(k), 'tag', str);
% 	
% 	set(get(handles(k), 'ylabel'), 'string', ['Ch ', str]);
% 	
% end

%--
% set grid state
%--

set(handles, ...
	[prefix.grid, 'ticklabelmode'], 'manual', [prefix.grid, 'ticklabel'], [] ...
);

%--
% set double click play
%--

set(handles, 'buttondownfcn', {@double_click_play, context.par, 'page'});

