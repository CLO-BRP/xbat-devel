function [handles, context] = view__on__compute(widget, data, parameter, context)

% SPECTRAL_REDUCTION - view__on__compute

handles = [];

% fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

page = context.page;

for k = 1:numel(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(k)));
	
	if isempty(ax)
		continue;
	end
		
	if numel(page.channels) == 1
		value = data.reduction.value;
	else
		value = data.reduction.value{k};
	end
	
	handles(end + 1) = line('parent', ax, ...
		'xdata', data.reduction.time, ...
		'ydata', value ...
	);
	
end

% NOTE: this helped debug a discrepancy between a feature computation length and a child detector feature computation

% db_disp; flatten(data), page, stack_disp; disp(' ');