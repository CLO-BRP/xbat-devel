function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% SPECTRAL_REDUCTION - view__on__selection__create

fun = parent_fun(mfilename('fullpath'), 2); [handles, context] = fun(widget, data, parameter, context);
