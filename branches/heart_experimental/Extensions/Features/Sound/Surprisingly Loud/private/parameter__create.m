function parameter = parameter__create(context)

% SURPRISINGLY LOUD - parameter__create

parameter = struct;

%--
% binarization parameters
%--

parameter.alpha = 0.25;

parameter.score = 2;

%--
% component processing parameters
%--

parameter.sieve = 1;

parameter.coalesce = 1;

parameter.threshold = 3;

parameter.threshold_on = 0;
