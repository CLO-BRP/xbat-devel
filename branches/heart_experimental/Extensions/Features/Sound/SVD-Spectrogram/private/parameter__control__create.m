function control = parameter__control__create(parameter, context)

% SVD-SPECTROGRAM - parameter__control__create

%--
% create own controls
%--

control = empty(control_create);

control(end + 1) = slider_control('rank', parameter.rank, [1, 16], 'integer', ...
	'space', 1 ...
);

control(end + 1) = checkbox_control({'normalize', 'center and normalize'}, parameter.normalize);

control(end + 1) = checkbox_control('residual', parameter.residual);

control(end + 1) = section_collapse_control(1);

%--
% append required parent controls
%--

fun = parent_fun(mfilename('fullpath')); control = [control, fun(parameter, context)];
