function [handles, context] = view__on__activate(widget, data, parameter, context)

% SVD-SPECTROGRAM - view__on__activate

handles = [];

% TODO: we should have a way of hiding these as we have ways of hiding other children

% NOTE: we need a 'register_child' function, and it should be used throughout

% handles(end + 1) = debug_figure('create', context);