function [handles, context] = view__on__deactivate(widget, data, parameter, context)

% SVD-SPECTROGRAM - view__on__deactivate

handles = [];

handle = findobj(0, 'type', 'figure', 'tag', get_context_tag('test', context));

if ~isempty(handle)
	close(handle); 
end
