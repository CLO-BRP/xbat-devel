function handle = context_figure(action, context, name)

% context_figure - manage figures through context and name
% ---------------------------------------------------------
%
% handle = context_figure('create', context, name)
%
%        = context_figure('find', context, name)
%
%          context_figure('close', context, name)
%
%          context_figure('delete', context, name)
%
% Input:
% ------
%  context - at hand
%  name - for figure
%
% Output:
% -------
%  handle - to figure

%--
% set default name and handle multiple names
%--

% NOTE: the default reveals the initial use of this utility

if nargin < 3
	name = 'debug';
end

if iscell(name) && numel(name) > 1
	handle = iteraten(mfilename, 3, action, context, name); return;
end

%--
% build tag from name and context
%--

% NOTE: the tag contains the name, extension, and parent information

fields = {'CONTEXT', name, context.ext.fieldname, int2str(context.par), md5(get(context.par, 'tag'))};

tag = str_implode(fields, '::');

%--
% perform action
%--

switch action
	
	case 'create'
		
		% NOTE: the use of 'create_figure' in this branch ensures a singleton figure
		
		handle = create_figure(tag);
		
	case {'find', 'close', 'delete'}
		
		handle = findobj(0, 'type', 'figure', 'tag', tag);
		
		if isempty(handle)
			return;
		end
		
		switch action
			
			case 'close', close(handle);
				
			case 'delete', delete(handle);
				
		end
		
end