function parameter = parameter__create(context)

% LOG-FREQUENCY SPECTROGRAM - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% NOTE: these default parameters correspond to the typical ideal piano

parameter.low_freq = 27.5;

parameter.octaves = 22/3;

parameter.steps = 12;
