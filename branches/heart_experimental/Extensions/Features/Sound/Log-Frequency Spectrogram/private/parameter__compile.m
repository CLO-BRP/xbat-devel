function [parameter, context] = parameter__compile(parameter, context)

% LOG-FREQUENCY SPECTROGRAM - parameter__compile

fun = parent_fun(mfilename('fullpath')); [parameter, context] = fun(parameter, context);
