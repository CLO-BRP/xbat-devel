function result = parameter__control__callback(callback, context)

% ZC-RATE - parameter__control__callback

% result = struct;

%--
% call parent extension callback
%--

% NOTE: this relates to amplitude computation controls

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);

%--
% setup for our own callback
%--

pal = callback.pal;

parameter = context.ext.parameter;

low = get_control(pal.handle, 'min_freq', 'value');

high = get_control(pal.handle, 'max_freq', 'value');

%--
% core of the callback
%--

switch callback.control.name
	
	case 'min_freq'
		
		if (high - low) < parameter.min_bandwidth
			
			control = set_control(pal.handle, 'max_freq', 'value', low + parameter.min_bandwidth);
			
			if control.value == control.range(2)
				set_control(pal.handle, 'min_freq', 'value', control.value - parameter.min_bandwidth);
			end
			
		end
		
	case 'max_freq'
		
		if (high - low) < parameter.min_bandwidth
			
			control = set_control(pal.handle, 'min_freq', 'value', high - parameter.min_bandwidth);
			
			if control.value == control.range(1)
				set_control(pal.handle, 'max_freq', 'value', control.value + parameter.min_bandwidth);
			end
			
		end
		
	case 'max_freq_active'
		
		state = get_control(pal.handle, 'max_freq_active', 'value');
		
		set_control(pal.handle, 'max_freq', 'enable', state);
		
		set_control(pal.handle, 'noise_gate', 'enable', state);
		
	case 'sel_config'
		
		sel = get_browser_selection(context.par); 
		
		set_control(pal.handle, 'min_freq', 'value', sel.event.freq(1));
		
		set_control(pal.handle, 'max_freq', 'value', sel.event.freq(2));
		
	% TODO: the behavior of this control is not fully defined, does it really consider 'min_bandwidth'?
	
	case 'noise_gate'
		
		noise = get_control(pal.handle, 'noise_gate', 'value');
		
		% NOTE: this bounces back the 'noise_gate' value when we make it to close to the 'max_freq', this is not so simple 
		
		if (noise - high) < parameter.min_bandwidth
			
			set_control(pal.handle, 'noise_gate', 'value', high + parameter.min_bandwidth);
			
		end
		
end
