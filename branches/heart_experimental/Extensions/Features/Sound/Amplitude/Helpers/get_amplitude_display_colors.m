function color = get_amplitude_display_colors(parameter)

%--
% get RGB values for named color
%--

rgb = color_to_rgb(parameter.color);

%--
% compute various derived colors used in amplitude display
%--

% MAX

color.max.facecolor = 0.85 * ones(1, 3);

color.max.edgecolor = 0.5 * ones(1, 3);
	
% RMS

color.rms.facecolor = 0.70 * 0.33 * (2 + rgb);

color.rms.edgecolor = 0.70 * rgb;

% MEAN

color.mean.facecolor = 0.85 * 0.33 * (2 + rgb);

color.mean.edgecolor = 0.85 * rgb;

%--
% consider color transparency
%--

color.background = color_to_rgb(parameter.background);

% TODO: factor this, consider efficiency

color = flatten(color); fields = fieldnames(color);

alpha = parameter.alpha; beta = 1 - alpha;

for k = 1:numel(fields)
	field = fields{k};
	
	color.(field) = alpha * color.(field) + beta * color.background;
end

color = unflatten_struct(color);
	
	



