function [handles, context] = view__on__marker__create(widget, data, parameter, context)

% AMPLITUDE - view__on__marker__create

% NOTE: we have to get the function handle here to deal with name ambiguity

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

if isempty(handles)
	return;
end

%--
% get feature value at marker time
%--

marker = data.marker;

% NOTE: we have a two-step process, first select the page, then get the time values

% TODO: this currently fails in the case of feature derived detectors, we should fix this

try
	[value, page, data] =  get_feature_value_page(widget, marker.time);
catch
	return;
end

% TODO: currently, for multiple value amplitudes we fail to do this, now we just return

try
	value = get_feature_value(value, marker.time);
catch
	return;
end

%--
% display feature value
%--

% TODO: develop a more interesting and useful display

ax = get_channel_axes(widget, marker.channel);

if isempty(ax)
	return;
end 

% NOTE: we must select only the marker channel values

ix = find(marker.channel == data.channels, 1); 

if isempty(ix)
	return; 
end

% NOTE: make sure we clean-up markers on other axes

if ~isfield(parameter, 'clean_up') || parameter.clean_up
	
	delete(findobj(widget, 'tag', 'rms-marker')); 

end

% NOTE: this function ensures a tag-based singleton condition, per axes

handle = create_line(ax, 'rms-marker', ...
	'xdata', marker.time, ...
	'ydata', value.rms.value(ix), ...
	'hittest', 'off', ...
	'marker', 'o', ...
	'markersize', 8, ...
	'markerfacecolor', 0.5 * (marker.color + 0.75), ...
	'markeredgecolor', marker.color / 2 ...
);

if ~isfield(parameter, 'clean_up')
	
	uistack(handle, 'top');

end

append_label(handles, [', RMS: ', num2str(value.rms.value(ix))]);


%---------------------------
% APPEND_LABEL
%---------------------------

function append_label(handles, str)

handle = findobj(handles, 'type', 'text'); 

if isempty(handle)
	return
end

handle = handle(1);

set(handle, 'string', [get(handle, 'string'), str]);


%---------------------------
% GET_FEATURE_VALUE_PAGE
%---------------------------

function [value, page, data] = get_feature_value_page(widget, time)

%--
% get feature data from view widget
%--

data = get_feature_data(widget);

value = data.value; page = data.page;

% NOTE: return all value pages if there is no time selection

if nargin < 2
	return;
end

%--
% select page containing time of interest
%--

for k = 1:length(data.page)

	% NOTE: if time is in page we have found the page
	
	if (time >= page{k}.start) && (time <= page{k}.start + page{k}.duration)
		break;
	end

end

value = value{k}; page = page{k};


%---------------------------
% GET_FEATURE_DATA
%---------------------------

function data = get_feature_data(widget)

% NOTE: the 'data' field of the widget event data is the provided by the feature
	
data = get(widget, 'userdata'); data = get_field(data, 'data', struct);


