function parameter = view__parameter__create(context)

% AMPLITUDE - view__parameter__create

%--
% color and alpha parameters
%--

parameter.color = 'bright blue';

parameter.background = 'white';

parameter.alpha = 0.85;

%--
% display parameters
%--

parameter.max = 1;

parameter.symmetric = 1;
