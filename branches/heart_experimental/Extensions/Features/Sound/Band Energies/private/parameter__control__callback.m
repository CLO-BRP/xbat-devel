function result = parameter__control__callback(callback, context)

% BAND ENERGY - parameter__control__callback

% result = struct;

%----------------------
% CALLBACK
%----------------------

%--
% call parent and skip this callback in case of tabs
%--

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);

if strcmp(callback.control.kind, 'tabs')
	return;
end

%--
% setup
%--

nyq = 0.5 * context.sound.rate;

guide_color = 0.5 * ones(1, 3);

%--
% dispatch callback based on control
%--

pal = callback.pal.handle; selected = [];

switch callback.control.name
	
	case 'bank'
		
		%--
		% determine selected band
		%--
		
		current = get(callback.obj, 'currentpoint'); current = current(1);
		
		% NOTE: each column contains a start, center, stop description of a band
		
		band_details = context.ext.parameter.band_details;
		
		[ignore, selected] = min(abs(current - band_details(2, :)));
		
		band = nyq * band_details(:, selected);
		
		%--
		% update controls
		%--
		
		set_control(pal, 'band_start', 'value', band(1));
		
		set_control(pal, 'band_center', 'value', band(2));
		
		set_control(pal, 'band_stop', 'value', band(3));
		
	case 'contiguous'
		
		% NOTE: when filters are contiguous we cannot change start and stop
		
		set_control(pal, 'band_start', 'enable', ~callback.value);

		set_control(pal, 'band_stop', 'enable', ~callback.value);
		
	% NOTE: the following enforce the start, center, stop order
	
	case 'band_start'
		
		if callback.value > get_control(pal, 'band_stop', 'value')

		end
		
	case 'band_center'
		
		if callback.value > get_control(pal, 'band_stop', 'value')

		end

		if callback.value < get_control(pal, 'band_start', 'value')
			
		end

	case 'band_stop'
		
		if callback.value < get_control(pal, 'band_start', 'value')
			
		end

end

%----------------------
% UPDATE DISPLAY
%----------------------

%--
% setup for display
%--

handles = get_control_handles(pal, 'bank'); ax = handles.obj;

values = get_control_values(pal); 

%--
% display guides
%--

% NOTE: create display guides

delete(get(ax, 'children'));

offset = 0.02;

set(ax, ...
	'box', 'on', ...
	'xlim', [0, 1] + offset * [-1, 1], ...
	'ylim', [-0.2, 1.2] ...
);

guide = [];

guide(end + 1) = line( ...
	'parent', ax, ...
	'xdata',[-2, 2], ...
	'ydata', [0, 0] - offset ...
);

guide(end + 1) = line( ...
	'parent', ax, ...
	'xdata',[-2, 2], ...
	'ydata', [1, 1] + offset ...
);

% NOTE: this is a service to a child

parameter = context.ext.parameter;

if isfield(parameter, 'trans_freq')
	
	% NOTE: eventually we may turn this guide into a control
	
	guide(end + 1) = line( ...
		'parent', ax, ...
		'ydata',[-2, 2], ...
		'xdata', parameter.trans_freq * [1, 1] ...
	);

end

set(guide, ...
	'linestyle', ':', ...
	'color', guide_color, ...
	'hittest', 'off' ...
);

%--
% display filter bank
%--

freqs = parameter.bank_freqs;

filters = parameter.bank ./ max(max(parameter.bank));

handles = [];

for k = 1:size(filters, 2)
	
	response = filters(:, k); 
	
	% NOTE: this suppresses the display of the zero part of the response, except for the zeros adjacent to non-zeros
	
	mask = morph_erode(uint8(response == 0), ones(3, 1)) > 0;

	response(mask) = nan;

	% NOTE: this improves the display of rectangles
	
	if strcmpi(cellfree(parameter.bank_type), 'rectangular')
		edges = [find(~mask, 1, 'first'), find(~mask, 1, 'last')]; freqs(edges) = freqs(edges + [1, -1]);
	end
	
	handles(end + 1) = line( ...
		'tag', int2str(k), ...
		'parent', ax, ...
		'xdata', freqs, ...
		'ydata', response ...
	);

end

if ~isempty(selected)
	
	set(handles(selected), 'linewidth', 2);
	
	guide(end + 1) = create_line(ax, 'center_control', ...
		'linestyle', ':', ...
		'ydata', [-2, 2], ...
		'xdata', band_details(2, selected) * [1, 1], ...
		'buttondownfcn', {@center_control_callback, 'start', band_details, selected} ...
	);

	label = band_details(:, selected);
	
	if label(1) == label(2)
		label(1) = nan;
	end 
	
	if label(2) == label(3)
		label(3) = nan;
	end
	
	opt = display_freq_labels; opt.alternate = 1; opt.fun = @(label) num2str(round(nyq * label * 10) / 10);
	
	handles = display_freq_labels(ax, label, opt);
	
end
   
% set(guide, 'color', guide_color);

set(handles, 'hittest', 'off');

%--
% make sure display does not bleed
%--

if ~is_visible(ax)
	set(guide, 'visible', 'off'); set(handles, 'visible', 'off');
end



%-------------------------------
% CENTER_CONTROL_CALLBACK
%-------------------------------

function center_control_callback(obj, eventdata, mode, band_details, selected) %#ok<INUSL>

persistent CURSOR BAND SELECTED;
	
switch mode
	
	case 'start'
		CURSOR = obj; BAND = band_details; SELECTED = selected;
		
	case 'move'
		current = get(get(obj, 'parent'), 'currentpoint'); current = current(1);
		
	case 'stop'
		CURSOR = []; BAND = [];
		
end

count = size(band_details, 2);

if selected == 1
	range = [0, BAND(2, 2)];
	
elseif SELECTED == count
	range = [BAND(2, count - 1), 1];
	
else
	range = BAND(2, SELECTED + [-1, 1]);
	
end

current = clip_to_range(current, range);

set(obj, 'xdata', current * [1, 1]);





