function [handles, context] = view__on__compute(widget, data, parameter, context)

% BAND ENERGY - view__on__compute

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% display band energy overlay
%--

nyquist = context.sound.samplerate/2;

band_details = context.ext.parameter.band_details;

numbands = size(band_details,2);

page = context.page;

for j = 1:length(page.channels)

	%--
	% get and update axes
	%--

	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(j)));

	if isempty(ax)
		continue;
	end

	%--
	% setup text label display
	%--

	xlim = get(ax, 'xlim');

	% TODO: use 'get_size_in' to offset position

	textpos = xlim(2);

	%--
	% get maximum values for each curve
	%--
	if iscell(data.band_energy.value)
		value = data.band_energy.value{j};
		valmaxes = data.band_energy.max{j};
	else
		value = data.band_energy.value;
		valmaxes = data.band_energy.max;
	end
	valmax = max(valmaxes);


	for k = 1:numbands
		%--
		% find display min/max positions for each display row
		%--
		
		uplim = map_frequency(band_details(3,k).*nyquist, parameter, context, 0);
		lowlim = map_frequency(band_details(1,k).*nyquist, parameter, context, 0);

		%--
		% get scalings for each energy curve
		%--
		
		dBval = log10(1+value(k, :));
		scale(k) = (uplim-lowlim)*0.5/log10(1+valmaxes(k));
		relscale(k) = 10*log10(valmax/valmaxes(k));

		%--
		% draw curve and text
		%--

		handles(end + 1) = line( ...
			'parent', ax, ...
			'xdata', data.band_energy.time, ...
			'ydata', dBval.*scale(k) + lowlim, ...
			'color', [1 0.5 0], ...
			'linewidth', 1 ...
		);

		handles(end + 1) = text(textpos, lowlim, sprintf(' +%.2f dB', relscale(k)), ...
			'parent', ax, ...
			'verticalalignment', 'bottom' ...
		);

		if relscale(k) == 0
			set(handles(end), ...
				'fontsize', 1.2 * get(handles(end), 'fontsize'), ...
				'fontweight', 'bold' ...
			);
		end

	end
	
	%--
	% display event network for child
	%--

	if isfield(data, 'event') && isfield(data.event, 'component')

		detected = data.event; component = unique(detected.component);
		
		color = hsv(numel(component));
		
		for k1 = 1:numel(component)
			
			ix = find(detected.component == component(k1));
			
			for k2 = 1:numel(ix)
				
				event = detected.event(ix(k2));
				
				time = mean(event.time) + page.start;
				
				freq = map_frequency(mean(event.freq), parameter, context, 0);
				
				line( ...
					'parent', ax, ...
					'xdata', time, ...
					'ydata', freq, ... 
					'marker', 'o', ...
					'markersize', 10, ...
					'markerfacecolor', color(k1, :), ...
					'linestyle', 'none', ...
					'color', 0.5 * ones(1, 3) ...
				);
				
			end
			
		end

	end

end






