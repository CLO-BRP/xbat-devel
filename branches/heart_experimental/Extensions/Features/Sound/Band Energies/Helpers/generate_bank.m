function  [A, freqs] = generate_bank(parameter, context)

% get filter shapes for all bands using definition via band_details
% currently, rectangular and triangular shapes, as well as shapes defined
% by curves are supported

n = ceil(0.5 * parameter.fft);
freqs = (0:n)/n;

band_details=parameter.band_details;
numbands=size(band_details,2);

N = numel(freqs);
A = zeros(N,numbands);

% get window matrix
for k = 1:numbands
	
    % initialize band positions
    minpos = find(freqs >= band_details(1,k),1,'first');
    centerpos = find(freqs <= band_details(2,k),1,'last');
    maxpos = find(freqs < band_details(3,k),1,'last');
    
    switch lower(cellfree(parameter.bank_type))
        
        %--
        % triangular case ---------------------------------------------
        %--
        case 'triangular'
			
            % account for normalization
            if parameter.normalize
                p = 2;
            else p = -1;
            end

            % get triangle
            % NOTE: triangle counts form zero
            A(:, k) = band_triangle(N,...
                max(1,minpos)-1,...
                min(numel(freqs),centerpos)-1,...
                min(numel(freqs),maxpos)-1, p);

        %--
        % rectangular case ---------------------------------------------
        %--
        case 'rectangular'
			
            % account for normalization
            if parameter.normalize
                p = maxpos-minpos+1;
			else
				p = 1;
            end

            % get unity function
            A(:, k) = [zeros(minpos-1,1);...
                ones(maxpos-minpos+2,1);...
                zeros(N-maxpos-1,1)]./p;
		%--
        % hann window case ---------------------------------------------
        %--	
		case 'hann'

			parameter.curve = hann(31);
			parameter.bank_type = 'generic';
            parameter.band_details=band_details(:,k);
            
			[A(:, k)] = generate_bank(parameter, context);

        %--
        % generic case ---------------------------------------------
        %--
        case 'generic'
			
            % account for normalization
            if parameter.normalize
                p = 2;
            else p = -1;
            end

            % get curve
            % NOTE: stretchcurve counts from zero
            A(:, k) = stretch_curve(parameter.curve,...
                N,...
                max(1,minpos)-1,...
                min(numel(freqs),centerpos)-1,...
                min(numel(freqs)-1,maxpos), p);            
            
        otherwise
            error(['Unrecognized filter type ''', parameter.bank_type, '''.']);
    end
end
