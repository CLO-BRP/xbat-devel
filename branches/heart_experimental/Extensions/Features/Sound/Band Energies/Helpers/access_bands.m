function value = access_bands(control, value, mode)

% NOTE: generally speaking we may want access to the control structure, here it is not used

% NOTE: this function takes an explicit mode, however we could infer the mode from the value type
	
switch mode
	case 'get'
		if ischar(value)
			value = sscanf(value, '%f,');
		end
		
	case 'set'
		if ~ischar(value)
			value = str_implode(value, ', ', @num2str);
		end
		
end