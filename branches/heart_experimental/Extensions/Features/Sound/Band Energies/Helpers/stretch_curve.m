function C = stretch_curve(curve, N, low, center, high, p)

% triangle - create centered window curve from arbitrary vector
% ------------------------------------------
%
% C = stretch_curve(curve, N, low, center, high, p)
%
% Input:
% ------
%  curve - vector to be interpolated
%  N - length of sequence
%  low, center, high - low, center, and high elements, starting from 0 !!!
%  p - normalization type
%
% Output:
% -------
%  C - stretched curve

%--
% set default normalization
%--

if nargin < 5
	p = 2;
end

%--
% compute
%--
% 
% x = 0:10;
% y = sin(x);
% xx = 0:.25:10;
% yy = spline(x,y,xx);
% plot(x,y,'o',xx,yy)


C = zeros(N, 1);

% split input curve in two "halves and interpolate each of these according
% to given parameters
splitpoint=floor(numel(curve)/2);

if ~(low == center)
    xl=0:splitpoint-1; % input range
    yl=curve(1:splitpoint); % curve 
    xxl=[0:center-low]/(center-low)*(splitpoint-1); %interpolated output range
    C((low:center) + 1) =spline(xl,yl,xxl); % interpolated curve
end

if ~(center == high)
    xr=0:numel(curve)-splitpoint-1; % input range
    yr=curve(splitpoint+1:end); % curve 
    xxr=[0:high-center]/(high-center)*(splitpoint-1); %interpolated output range
    C((center:high) + 1) =spline(xr,yr,xxr);
end
%--
% normalize sequence
%--

if p < 0
	return;
end

C = C ./ norm(C, p);