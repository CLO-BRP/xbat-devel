function attribute = load(store, context)

% SOUND_SPEED - load

attribute.speed = sound_speed(store);
