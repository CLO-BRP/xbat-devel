function G = default_geometry(n)

%--
% create grid on unit circle
%--

t = linspace(0,2*pi,n + 1)';

t = t(1:(end - 1));

x = cos(t);

y = sin(t);

%--
% pack into geometry matrix
%--

G = 100 * [x, y, zeros(n,1)];

G = round(G);
