function [X, context] = compute(X, parameter, context)

% AMPLIFY - compute

% NOTE: this is a trivial whitening

if parameter.emphasis
	X = filter([1, -parameter.zero], 1, X);
end

fun = parent_fun(mfilename('fullpath')); [X, context] = fun(X, parameter, context);
