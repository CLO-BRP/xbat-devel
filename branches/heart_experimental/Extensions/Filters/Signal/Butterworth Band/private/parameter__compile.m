function [parameter, context] = parameter__compile(parameter, context)

% BUTTERWORTH BAND - parameter__compile

nyq = 0.5 * context.sound.rate;

W = [parameter.min_freq, parameter.max_freq] / nyq;

[parameter.filter.b, parameter.filter.a] = butter(parameter.order, W);