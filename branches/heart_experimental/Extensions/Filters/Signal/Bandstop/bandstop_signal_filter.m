function ext = bandstop_signal_filter

ext = extension_inherit(mfilename, bandpass_signal_filter);

ext.short_description = 'Bandstop signal using equiripple filter';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '5db3e88b-4763-4888-a2e7-215d6293f872';

% ext.author = '';

% ext.email = '';

% ext.url = '';

