function ext = waveshrink_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Denoising thrgough wavelet shrinkage';

ext.category = {'Enhance', 'Noise-Reduction', 'Wavelet'};

ext.version = '0.1';

ext.guid = '959176dd-7f19-4f7e-9a6b-8ae91a51ff53';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

