function parameter = parameter__create(context)

% WAVESHRINK - parameter__create

parameter = struct;

parameter.wavelet = 'Symmlet';

parameter.index = 8;

parameter.method = 'Visu';