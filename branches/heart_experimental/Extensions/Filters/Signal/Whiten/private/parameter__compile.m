function [parameter, context] = parameter__compile(parameter, context)

% WHITEN - parameter__compile

% NOTE: this is a context heavy compilation, we bail often!

%--
% check page context
%--

% NOTE: we cannot compile without page context

if ~isfield(context, 'page')
	return;
end

page = context.page;

% NOTE: we need the single channel pages when we compile for this filter

if length(page.channels) > 1
	return;
end

%--
% collect relevant data
%--

data = [];

if ~parameter.use_log
	
	%--
	% use page samples as data
	%--
	
% 	events = get_quiet_sections(page, context);
	
	% NOTE: in case of no log use page data as noise source
	
	% TODO: consider if we have and want to use a selection
	
	% TODO: consider a simple thresholding and closing selection of samples
		
	%--
	% check for page samples, read if necessary
	%--
	
	if isfield(page, 'samples')
		data = page.samples;
	else
		data = sound_read(context.sound, 'time', page.start, min(page.duration, 10), page.channels);
	end
	
else
	
	%--
	% get page-relevant events
	%--
	
	event = get_noise_events(parameter, context);

	if isempty(event)
		return;
	end
	
	%--
	% get noise data from noise log events
	%--
	
	% NOTE: this reads and stores event samples
	
	event = event_sound_read(event, context.sound);
	
	% NOTE: columns are harder to concatenate than rows?
	
	data = event(1).samples;
	
	for k = 2:length(event)
		data = [data; event(k).samples];
	end
	
end

% NOTE: return if we were unable to collect data

if isempty(data)
	return;
end

%--
% get model from data and update parameter filter
%--

% NOTE: this is the background model filter

model.a = aryule(data, parameter.order);

if parameter.regularize
	model.a = model.a .* ((1 - parameter.regularize) .^ (0:parameter.order));
end

model.b = 1;

% NOTE: our filter is actually the inverse filter

parameter.filter.b = model.a;

parameter.filter.a = model.b;

%--
% add lowpass filter if needed
%--

if parameter.lowpass
	
	%--
	% get lowpass filter from extension
	%--
	
	% NOTE: providing a better way to do this would be great
	
	ext = get_browser_extension('signal_filter', context.par, 'Lowpass');
	
	ext.parameter.max_freq = parameter.max_freq;
	
	nyq = 0.5 * get_sound_rate(context.sound);
	
	ext.parameter.transition = 0.25 * (nyq - parameter.max_freq);
	
	ext.parameter = ext.fun.parameter.compile(ext.parameter, context);
	
	%--
	% compose model and lowpass filters
	%--
	
	parameter.filter.b = conv(parameter.filter.b, ext.parameter.filter.b);
	
end



function model = get_event_model(event, order, tol)

% NOTE: consider the order as a max order when a tolerance is provided

%--
% initialize model
%--

model.a = 1; model.b = 1;

%--
% fixed order model
%--

if nargin < 3
	
	
	
%--
% adaptive order model
%--

else
	
	order = 2:order; current = order(1);
	
	iteration = 1; converged = 0;
	
	while ~converged && (iteration < length(order))
		
		current = order(k);
		
		model.a = aryule(event.samples, current);
		
		filtered = filter(model.a, 1, event.samples);
		
		error = compute_error(filtered);
		
		% TODO: we want to bisect the available orders to get meet the tolerance not way below
		
		if error <= tol
			converged = 1;
		end
		
		iteration = iteration + 1;
		
	end	
	
	% TODO: display error as part of message
	
	if ~converged
		disp('Event modelling iteration failed to converge.');
	end
	
end



function error = compute_error(samples)

error = 1;

X = fast_specgram(samples);

S = mean(X, 2);

% TODO: we want a flat spectrum, decide how to compute error

