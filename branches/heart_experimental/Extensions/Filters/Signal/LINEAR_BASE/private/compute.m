function [X, context] = compute(X, parameter, context)

% LINEAR_BASE - compute

% TODO: consider factoring most of this into its own function

%--
% pad signal
%--

N = size(X, 1); n = impzlength(parameter.filter.b, parameter.filter.a) - 1;

X = [flipud(X(1:n, :)); X; flipud(X(end - n + 1:n, :))];

%--
% filter
%--

% FIR FILTER

if length(parameter.filter.a) == 1
	
	%--
	% perform some tests if we are in test
	%--
	
	% NOTE: this is not typically what we want when we turn on 'debug' for children
	
	if 0 % context.debug
		
		info.pad = n;
		
		info.length = length(parameter.filter.b);
		
		%--
		% compute and time fast convolution
		%--
		
		start = clock; 
		
		X1 = fast_conv(X, parameter.filter.b); X1 = X1(n + 1: n + N, :);
		
		elapsed.fast_conv = etime(clock, start);
		
		%--
		% compute and time filter
		%--
		
		start = clock;
		
		X2 = filter(parameter.filter.b, parameter.filter.a, X); X2 = X2(n + 1: n + N, :);
		
		elapsed.filter = etime(clock, start);
		
		info.elapsed = elapsed;
		
		info.error = max(abs(X1 - X2));
		
		flatten(info)
		
		size(X), size(X1), size(X2)
		
	end
	
	% NOTE: algorithm selection could be adaptive, consider 'wisdom'
	
	if length(parameter.filter.b) > 128
		
		X = fast_conv(X, parameter.filter.b);
		
	else

		X = filter(parameter.filter.b, parameter.filter.a, X);
	
	end
	
% IIR FILTER

else
 
	X = filter(parameter.filter.b, parameter.filter.a, X);

end

%--
% select valid part of signal
%--

X = X(n + 1: n + N, :);