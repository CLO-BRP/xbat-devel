function ext = butterworth_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Lowpass and highpass using Butterworth filter';

ext.category = {ext.category{:}, 'Frequency', 'IIR'};

ext.version = '0.1';

ext.guid = '5ea7db21-07c1-48fa-b963-9d007a0f4b48';

ext.author = 'Matt';

ext.email = '';

% ext.url = '';

