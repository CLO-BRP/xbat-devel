function ext = point_operation_base_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Base for point operation filters';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'd99808e5-d472-4561-b65e-609a0cd6d1ee';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

