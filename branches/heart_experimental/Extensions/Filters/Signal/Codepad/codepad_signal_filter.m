function ext = codepad_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Interactively experiment with signal filter code';

% ext.category = {};

ext.version = '0.15';

ext.guid = '84c11ff5-c6c6-4ba4-b3fa-a803082349b1';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

