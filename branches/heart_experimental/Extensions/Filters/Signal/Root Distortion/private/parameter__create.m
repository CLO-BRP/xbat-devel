function parameter = parameter__create(context)

% ROOT DISTORTION - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @root_distortion;

parameter.exponent = 0.5;


function X = root_distortion(X, parameter)

X = sign(X) .* abs(X).^parameter.exponent; 
