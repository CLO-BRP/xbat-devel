function parameter = parameter__create(context)

% PRE-EMPHASIS - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.zero = 0.95;