function ext = morphological_base_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Base for morphological and flat neighborhood filters';

ext.category = {'Morphological'};

ext.version = '0.2';

ext.guid = 'd0c14c86-e6f5-432a-ad24-47089504b2ff';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

