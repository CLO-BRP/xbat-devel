function result = parameter__control__callback(callback, context)

% MORPHOLOGICAL_BASE - parameter__control__callback

%--
% set default result
%--

result = [];

pal = callback.pal;

%--
% peform control update
%--

switch callback.control.name
	
	case {'width', 'height'}
		
		%--
		% consider linked controls
		%--
		
		link = get_control(pal.handle, 'link', 'value');
		
		if link
		
			value = get_control(pal.handle, callback.control.name, 'value');
			
			if strcmp(callback.control.name, 'width')
				other = 'height';
			else
				other = 'width';
			end
						
			set_control(pal.handle, other, 'value', value);
			
		end
	
	case 'link'
		
		% NOTE: there is nothing to do when we un-link
		
		if ~get(callback.obj, 'value')
			result.update = 0; return;
		end
		
		%--
		% get width and height
		%--

		width = get_control(pal.handle, 'width', 'value');

		height = get_control(pal.handle, 'height', 'value');

		% NOTE: there is nothing to do if these are already the same

		if width == height
			result.update = 0; return;
		end

		%--
		% set both controls to minimum value
		%--

		if (width < height)
			
			set_control(pal.handle,'height', 'value', width);
			
		elseif (width > height)
			
			set_control(pal.handle, 'width', 'value', height);
			
		end
	
end

%--
% update icon display of parameters
%--

plot_se(pal.handle, get_control_values(pal.handle));

