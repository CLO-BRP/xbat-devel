function ext = average_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Local average';

ext.category = {ext.category{:}, 'Blur', 'Statistical'};

% ext.version = '';

ext.guid = '7ba6ab19-7ba0-4aa1-964a-548fabc38d11';

% ext.author = '';

% ext.email = '';

% ext.url = '';

