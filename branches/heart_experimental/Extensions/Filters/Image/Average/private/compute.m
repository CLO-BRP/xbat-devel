function [X, context] = compute(X, parameter, context)

% MOVING AVERAGE - compute

SE = create_se(parameter); N = se_size(SE);

rectangle = strcmpi(parameter.style, 'rectangle');

if rectangle
	X = box_filter(X, SE) ./ N; 
else	
	X = linear_filter(X, SE ./ N);
end