function [X, context] = compute(X, parameter, context)

% RECURSIVE AVERAGE - compute

% TODO: consider if we want a single slice offset in the computation

%--
% setup and initialize
%--

n = size(X, 2); lambda = parameter.lambda;

%--
% compute simple background estimate
%--

if ~has_background_state(context)
	state = 0.5 * (min(X, [], 2) + median(X, 2));
else
	state = context.state.background;
end 

B = raber(X, lambda, state);
	
%--
% compute adaptive background estimate
%--

if parameter.adaptive
	
	%--
	% compute signal mask using simple background estimate
	%--
	
	% TODO: this can be improved, and some parameter can be revealed
	
	Z = (X - B) <= fast_rank(B(:), 0.90); 
	
	Z = logical(morph_erode(uint8(Z), ones(3)));
	
	%--
	% produce or cleanup debug display
	%--

	% TODO: create helper to produce titles and tags for these figs
	
	if ~context.debug
		close(named_fig('RAIFDB'));
	else
		par = named_fig('RAIFDB', context.par); figure(par); image_view(flipud(Z)); title('Background Indicator'); image_menu(par, 'Light Gray');
	end
	
	%--
	% compute adaptive background estimate
	%--
	
	if has_background_state(context)
		B(:, 1) = (1 - lambda) * context.state.background + lambda * X(:, 1);
	end

	for k = 2:n
		
		B(~Z(:, k), k) = B(~Z(:, k), k - 1);
		
		B(Z(:, k), k) = (1 - parameter.lambda) * B(Z(:, k), k - 1) + lambda * X(Z(:, k), k);
		
	end

	
end

%--
% output resulting background or signal estimate
%--

switch lower(parameter.output{1})
	
	case 'background', X = B;
		
	case 'signal'
		
		X = X - B;
		
		% NOTE: this parameter is available, but currently not revealed as control
		
		if parameter.positive
			X = max(X, 0);
		end
		
end

%--
% store background state
%--

context.state.background = B(:, end);


%--------------------------------
% HAS_BACKGROUND_STATE
%--------------------------------

function value = has_background_state(context)

value = isfield(context, 'state') && isfield(context.state, 'background');

