function [X, context] = compute(X, parameter, context)

% EROSION - compute

SE = create_se(parameter);

X = morph_erode(X, SE);
