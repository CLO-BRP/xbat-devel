function ext = opening_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Morphological opening via erosion-dilation';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '87364d43-0cc5-4778-aefe-c2650fcdd5e9';

% ext.author = '';

% ext.email = '';

% ext.url = '';

