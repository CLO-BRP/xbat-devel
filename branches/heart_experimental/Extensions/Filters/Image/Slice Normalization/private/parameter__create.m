function parameter = parameter__create(context)

% SLICE NORMALIZATION - parameter__create

% NOTE: these are the default options of the 'normalize_dim' function

parameter = normalize_dim;

