function ext = slice_normalization_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Normalize slices to zero mean and unit norm';

ext.category = {'Statistical'};

ext.version = '0.1';

ext.guid = '27d59752-e03d-490b-b42f-9f4b34815f42';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

