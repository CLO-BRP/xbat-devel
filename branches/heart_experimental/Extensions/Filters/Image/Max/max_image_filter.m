function ext = max_image_filter

ext = extension_inherit(mfilename, dilation_image_filter);

ext.short_description = 'Local max (dilation)';

ext.category = {ext.category{:}, 'Statistical'};

% ext.version = '';

ext.guid = '64589766-5ed9-43c9-aee3-4aa3c1416a75';

% ext.author = '';

% ext.email = '';

% ext.url = '';

