function [result, context] = compute(log, parameter, context)

% ITERATE EVENT ACTION - compute

result = struct;

%--
% get log events
%--

events = log_get_events(log); 

% NOTE: return if there are no events to iterate over

if isempty(events)
	return;
end

%--
% unpack hosted extension
%--

% NOTE: we rename the parent context to preserve it and to get clearer code below

ext = parameter.ext; context0 = context; context = parameter.context;

%--
% prepare
%--

if ~isempty(ext.fun.prepare)
	
	try
		[output, context] = ext.fun.prepare(ext.parameter, context);
	catch
		extension_warning(ext, 'Prepare failed.', lasterror);
	end
	
end

%--
% iterate compute event action over log events
%--

% NOTE: we update the event action context with the log sound, this is the context sound

context.sound = log.sound; context.log = log;

for k = 1:length(events)

	% TODO: consider making reading optional, here and in 'action_dispatch'
	
	event = event_sound_read(events(k), context.sound);
	
	[result.result(k), context] = perform_action(event, ext, ext.parameter, context);

end

%--
% conclude
%--

if ~isempty(ext.fun.conclude)

	try
		ext.fun.conclude(ext.parameter, context);
	catch	
		extension_warning(ext, 'Conclude failed.', lasterror);	
	end
	
end

%--
% restore context
%--

context = context0;

