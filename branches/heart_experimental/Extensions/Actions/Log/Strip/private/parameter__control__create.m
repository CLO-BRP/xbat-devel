function control = parameter__control__create(parameter, context)

% STRIP - parameter__control__create

control = empty(control_create);

control(end + 1) = control_create( ...
	'name', 'suffix', ...
	'style', 'edit', ...
	'tooltip', 'Name suffix for stripped logs', ...
	'initialstate', '__DISABLE__', ...
	'string', parameter.suffix ...
);

control(end + 1) = control_create( ...
	'name', 'inplace', ...
	'alias', 'modify in-place', ...
	'tooltip', 'Modify input logs in-place', ...
	'style', 'checkbox', ...
	'value', parameter.inplace ...
);

control(end + 1) = control_create( ...
	'name', 'measures', ...
	'style', 'listbox', ...
	'tooltip', 'Select measures to strip from logs', ...
	'lines', 2, ...
	'max', 2, ...
	'string', parameter.measures, ...
	'value', [] ...
);

control(end + 1) = control_create( ...
	'name', 'all', ...
	'tooltip', 'Strip all measures from log', ...
	'style', 'checkbox', ...
	'value', parameter.all ...
);
