function ext = create_digest_log_action

ext = extension_create(mfilename);

ext.short_description = 'Create a portable digest sound from a log';

ext.category = {'export'};

ext.version = '0.1';

ext.guid = '972089aa-6af8-4fb1-8226-4123bf18a1a1';

ext.author = 'Matt';

ext.email = 'mer34@cornell.edu';

ext.url = '';

