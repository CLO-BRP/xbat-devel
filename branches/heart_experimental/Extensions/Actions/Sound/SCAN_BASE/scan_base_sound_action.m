function ext = scan_base_sound_action

ext = extension_create(mfilename);

ext.short_description = 'Base for actions that scan through the sound';

% ext.category = {};

ext.version = '0.1';

ext.guid = '735246b5-210e-4756-a3f6-a062757b7143';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

