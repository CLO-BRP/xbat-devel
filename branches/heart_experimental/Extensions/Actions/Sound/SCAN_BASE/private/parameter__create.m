function parameter = parameter__create(context)

% SCAN_BASE - parameter__create

parameter = struct;

%--
% scan extent parameters
%--

% NOTE: because we iterate over sounds these values are percents

% TODO: consider the future possibility that these may be vectors, as in the case of a sampling scan 

parameter.start = 0;

parameter.stop = 100;

parameter.channels = []; % NOTE: no channel selection indicates all channels should be scanned

%--
% scan page parameters
%--

% NOTE: this value is typically set by the child extension, it makes no sense to set it here

parameter.duration = [];

parameter.overlap = 0; % NOTE: express this as fraction of page in percent

%--
% callback parameters
%--

% NOTE: the callbacks must pass context through, this is a basic store for the callback computation

parameter.callback.prepare.fun = @prepare_callback;

parameter.callback.page.fun = @page_callback;

parameter.callback.conclude.fun = @conclude_callback;


% NOTE: these document the signatures and basic technique needed to use this base extension

%---------------------------
% PREPARE_CALLBACK
%---------------------------

function [result, context] = prepare_callback(parameter, context)

result = struct;


%---------------------------
% PAGE_CALLBACK
%---------------------------

function [result, context] = page_callback(page, parameter, context) %#ok<INUSL>

% TODO: explore the contents of the context, specifically the context.sound

result = struct;

disp(str_implode({sound_name(context.sound), sec_to_clock(page.start), num2str(page.duration)}, ', '));


%---------------------------
% CONCLUDE_CALLBACK
%---------------------------

function [result, context] = conclude_callback(parameter, context) %#ok<INUSL>

result = struct;

