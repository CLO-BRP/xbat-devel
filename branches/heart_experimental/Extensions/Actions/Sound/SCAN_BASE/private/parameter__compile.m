function [parameter, context] = parameter__compile(parameter, context)

% SCAN_BASE - parameter__compile

%--
% check callback parameters
%--

% NOTE: the main purpose of this code is to check for the type

callbacks = {'prepare', 'page', 'conclude'};

for k = 1:numel(callbacks)
	
	[valid, type] = is_callback(parameter.callback.(callbacks{k}).fun);

	if ~valid
		error('Callback parameter is not proper callback.');
	end

	parameter.callback.(callbacks{k}).parametrized = strcmp(type, 'parametrized');
	
end

%--
% get page duration from clock string
%--

parameter.duration = clock_to_sec(parameter.duration);
