function [result, context] = prepare(parameter, context)

% GET DATE TIME - prepare

result = struct;

% NOTE: we access the 'date_time' extension save helper so that we can save

[context.helper.ext, ignore, context.helper.context] = get_browser_extension('sound_attribute', 0, 'date_time');