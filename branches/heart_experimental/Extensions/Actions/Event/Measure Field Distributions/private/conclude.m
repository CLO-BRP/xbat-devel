function [result, context] = conclude(parameter, context)

% MEASURE FIELD DISTRIBUTIONS - conclude

result = struct;

%--
% get state contents
%--

state = context.state;

if isempty(context.state.id)
	return;
end

%--
% examine measure fields to see which to display
%--

field = fieldnames(state.value); bins = floor(sqrt(numel(state.id)));

for k = 1:numel(field)
	
	if numel(state.value(1).(field{k})) > 1
		continue;
	end
	
	value = [state.value.(field{k})];
	
	fig; hist([state.value.(field{k})], bins); title(field{k});
	
end