function [result, context] = compute(event, parameter, context)

% MEASURE FIELD DISTRIBUTIONS - compute

result = struct;

if isempty(parameter.measure)
	return;
end

% NOTE: we add the events that have the measure

name = parameter.measure{1};

if isfield(event.measure, name)
	
	% get relevant information from event
	
	id = event.id; value = flatten(event.measure.(name).value);
	
	% pack information into context
	
	if isempty(context.state.id)	
		context.state.id = id; context.state.value = value;
	else
		context.state.id(end + 1) = id; context.state.value(end + 1) = value;
	end
	
end