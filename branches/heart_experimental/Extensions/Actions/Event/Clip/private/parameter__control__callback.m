function result = parameter__control__callback(callback, context)

% CLIP - parameter__control__callback

result = struct;

pal = callback.pal.handle;

enable = get_control(pal, 'tapering', 'value');

set_control(pal, 'taper_length', 'enable', enable);


