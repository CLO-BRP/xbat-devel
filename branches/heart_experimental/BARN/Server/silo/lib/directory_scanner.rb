# NOTE: the directory scanner can scan a forest of directories processing each file found

# NOTE: the directory scanner can skip unmodified directories on successive scans

class DirectoryScanner

	attr_accessor :roots, :process
	
	def initialize(roots, process, modified = {})
		@roots = roots
		
		@process = process
		
		@modified = modified
	end
	
	def scan
				
		@modified = process_dirs(roots, @modified) do |file|
			process.call file
		end
		
	end
	
end

# TODO: we could pack results from the processor for output

# NOTE: a nil modified means we don't care about modification, we always process

def process_dirs(roots, modified = nil)
	
	return unless block_given?
	
	roots.each do |root|
		
		Dir.foreach(root) do |leaf|
			
			next if leaf == '.' || leaf == '..'
			
			file = File.join(root, leaf)

			if File.file? file
				
				yield file

			elsif File.directory? file
									
				if modified.nil? || modified[file].nil? || modified[file] < File.mtime(file)
					
					process_dirs(file, modified) do |child|
						yield child
					end
					
					modified[file] = File.mtime(file) unless modified.nil?	
				end
				
			end
			
		end
		
	end
	
	return modified

end