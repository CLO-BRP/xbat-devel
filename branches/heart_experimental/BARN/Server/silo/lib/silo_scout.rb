#--
# setup
#--

require 'uri'

require 'net/http'

require 'json'

require 'yaml'

#--
# classes and helpers
#--

class SiloScout
	def initialize(drop, source, sink = nil, resolution = 1)
		@drop = URI.parse(drop)
		
		@source = source
		
		if sink.nil?
			@sink = drop
		else
			@sink = sink
		end
		
		@resolution = resolution	
	end
	
	# TODO: consider the reason for failure, and also manage resolution based on responses
	
	def start
		every_n_seconds(@resolution) do
			puts ''; puts "Scouting #{@drop.to_s} at #{Time.now.strftime("%X")}"
			
			begin
				response = Net::HTTP.get_response(@drop)
			
				puts response.header.inspect
			rescue
				puts "Failed connection to #{@drop.to_s} at #{Time.now.strftime("%X")}"
			end
		end
	end
end

def every_n_seconds(n)
	loop do	
		while true
			before = Time.now;
			
			yield;
			
			interval = n - (Time.now - before); sleep(interval) if interval > 0
		end
	end
end