require 'platform'

class Worm
	
	attr_accessor :database, :verbose
	
	#--
	# setup on load
	#--
		
	if Platform.is_windows?
		helper = File.join(File.expand_path(File.dirname(__FILE__)), 'bin', 'win32', 'sqlite3.exe')
	else
		helper = 'sqlite3'
	end
	  
	@@sqlite = helper
		
	#--
	# basic operations
	#--
	
	def initialize(database, verbose = false)
		@database = database
				
		@verbose = verbose
		
		@table_info_cache = {}
	end
	
	def execute(query)
		if @verbose
			puts ''
			puts 'WORM ----------------------------------------------------------------';
			puts ''
			puts 'DATABASE:'
			puts database;
			puts ''
			puts 'QUERY:'
			puts query;
		end
				
		output = `#{@@sqlite} "#{database}" "#{query}"`;
		
		if @verbose
			puts ''
			puts 'OUTPUT:'
			puts output;
			puts ''
			puts '---------------------------------------------------------------------';
			puts ''
		end
		
		return output
	end
	
	# CREATE
	
	def insert(table, hash)
		hash = filter_hash(hash, table)
					
		execute(
			"INSERT INTO #{table} (#{hash.keys.join(', ')}) VALUES ('#{hash.values.join('\', \'')}')"
		)
		
		return execute("SELECT MAX(id) FROM #{table}").chomp
	end
		
	# NOTE: this method will typically be called from 'insert'
	
	def insert_multiple(table, hash)
	
	end
		
	# READ
	
	def find_all(table)
		pack(table, execute("SELECT * FROM #{table}"))
	end
	
	def find(table, id)
		find_by(table, 'id', id)		
	end
	
	def find_by(table, column, value)
		find_where(table, by_value_condition(column, value))
	end
	
	def find_like(table, column, value)
		find_where(table, like_condition(column, value))
	end
	
	def find_where(table, condition)
		pack(table, execute(
			"SELECT * FROM #{table} WHERE #{condition}"
		))
	end
	
	# NOTE: this is how we pack found results into an array of hashes, 'find' only does '*' selects
	
	# TODO: stored new-lines can wreak havoc on this naive parser
	
	def pack(table, output)
				
		columns = table_info(table, 'name');
		
		rows = []; # initialize row array
		
		output.each_line do |line|
			parts = line.split('|');
						
			row = {}; # initialize row hash
			
			columns.each_with_index do |column, k|
				row[column] = parts[k]
			end
			
			rows << row # add row to rows
		end
		
		return rows
	end
	
	# UPDATE

	# DELETE
	
	def delete(table, id)
		delete_by(table, 'id', id)
	end
	
	def delete_by(table, column, value)
		delete_where(table, by_value_condition(column, value))
	end
	
	def delete_where(table, condition)
		execute(
			"DELETE FROM #{table} WHERE #{condition}"
		)
	end
	
	#--
	# helpers
	#--
	
	# NOTE: single quotes are escaped as double quotes in SQLite
	
	def filter_hash(hash, table)
		
		columns = table_info(table, 'name'); filtered = {}
		
		hash.each_pair do |key, value|
			if columns.include?(key)
				value.gsub!(/'/, "''") if value.is_a? String
				
				filtered[key] = value
			end
		end
				
		return filtered
	end

	def by_value_condition(column, value)
		
		# NOTE: conventional 'id' columns are not quoted, everything else is
		
		if column == 'id' || (column.length > 3 && column[-3..-1] == '_id')
			if value.is_a? Array
				condition = "#{column} IN (#{value.join(', ')})"
			else
				condition = "#{column} = #{value}"
			end
		else
			if value.is_a? Array
				value.map! {value.gsub(/'/, "''") if value.is_a? String}	
			else
				value.gsub!(/'/, "''") if value.is_a? String
			end
			
			if value.is_a? Array				
				condition = "#{column} IN ('#{value.join('\', \'')}')"
			else
				condition = "#{column} = '#{value}'"
			end
		end
		
		return condition
	end
	
	def like_condition(column, value)
	  condition = "#{column} LIKE %#{value.shift}%"
	  
	  value.each do |value|
		condition << " OR #{column} LIKE %#{value.shift}%"
	  end
	end
	
	def table_info(table, field = nil)
						
		if @table_info_cache[table].nil?
			output = execute("PRAGMA table_info(#{table})"); info = []
			
			output.each_line do |line|
				parts = line.split('|')
								
				info << {
					'id' => parts[0],
					'name' => parts[1],
					'type' => parts[2],
					'notnull' => parts[3],
					'dflt_value' => parts[4]
				}
			end
			
			@table_info_cache[table] = info
		else
			info = @table_info_cache[table]
		end
		
		if field.nil?
			return info
		else
			return info.map {|row| row[field]}
		end
		
	end
	
end
