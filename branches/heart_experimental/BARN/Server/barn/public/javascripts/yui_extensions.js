
// Since YUI only deals in numbers, we must translate to something useable
// we are only interested in XML and JSON currently, when necessary, extend this if statement

YAHOO.util.DataSourceBase.prototype.responseFormat = function(){
    var response_format = 'xml';
    if(this.responseType == YAHOO.util.DataSource.TYPE_JSON){
        response_format = 'js'
    }
    return response_format;    
}();