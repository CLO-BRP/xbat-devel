function render_sort_menu () {

    $('.render-sort-menu').livequery( function () {
		
		$(".dropdown dt").click(function() {
			$(".dropdown dd ul").toggle();
		});

		$(".dropdown dd ul li").click(function() {
   			var text = $(this).html();
    		$(".dropdown dt").html(text);
    		$(".dropdown dd ul").hide();
			/* submit with appropriate url */
		});

		$(document).bind('click', function(e) {
			var $clicked = $(e.target);
			if (! $clicked.parents().hasClass("dropdown"))
				$(".dropdown dd ul").hide();
		});

        $(this).removeClass('render-sort-menu');

	});
}