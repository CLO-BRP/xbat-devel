=begin
	
	This module is to assist with resource controllers that service primarily polymorphic models 
	and are typically accessed through a parent.
	
	This is included in the Controllers derived from ActionController and handles multiple instances
	from a single parent, and hopefully deep nesting with multiple parents

	/sounds/{1,5}/notes
	/sounds/5/tags
	/sounds/1/shares
	
	NB: This module requires the use of NamedScopeHelper in the polymorphic parents
	
=end


#NOTE this module appears to be unused across the application


module ParentResourceHelper # :nodoc:
	
	def self.included(base) # :nodoc:
		base.extend ClassMethods
	end

	#-------------------
	# Class Methods
	#-------------------
	
	module ClassMethods
		def expects_parents(*parents)
			@parent_resources ||= parents
 		end
	  	
		# legacy: deprecation warning
	  # def parent_resource_class
	  # 			logger.info "Deprecation warning: parent_resource_class will be removed"
	  # 	    (params[:type]) ? params[:type].classify.constantize : nil
	  # 	  end  
	  # 		alias :parent_class :parent_resource_class
	  # 
	  # 	  def parent_id
	  # 			logger.info "Deprecation warning: parent_id will be removed"
	  # 	    (parent_resource_class) ? params["#{ parent_resource_class.to_s.downcase }_id"] : nil
	  # 	  end
	end
	# ClassMethods
	
	#-------------------
	# Instance Methods
	#-------------------
	
	#-------------------
	# set_parent_resources
	#-------------------
	
	# This method is typically called in a before_filter, someplace common like the application controller
	
def set_parent_resources
	# Here we set the current user to determine which access privileges the user has/
	# Note that is is possible to set the public params to have a nil user, which looks for public resources...
	@current_user = (logged_in? && params[:public].blank?) ? current_user : nil
	
	@parent_keys = []
	
	if parent_instances.blank?
	
		@parents = nil
		# raise ArgumentError, "No parent resources found in the request path \"#{request.path}\". #{self.class} has to be accessed through a PolyParent route!"
	else
		@parents = parent_instances
		
		# setting @parent_class in this way will not work for deep nesting... but why deep nest?
		@parent_class = @parents.first.class
		
		# replace the parent ids with those filtered by access
		# @parent_params = {@parent_keys.join => "{#{@parents.map{ |p| p.id }.join(',')}}"}
		# @parent_params.merge( params )
	end
end

	#-------------------
	# parent_instances
	#-------------------
	
	def parent_instances
		# NOTE: we find the parent's id's from the params hash (coming from a controller presumably)
		
		parent_params = params.reject{ |key, value| !key.to_s.include?("_id") }

		# This little block of love loops over the found parent hashes from the params, 
		# populating a parents array (initialized to [] in inject()) and returns it to @parent_instances
		@parent_instances ||=
		instances = parent_params.inject([]) do |parents, params|
			# We only expect a single parent, but this should handles them all.			
			# inject method passes the enumerated one at a time - in this case a single key/value pair
			params = params.to_a
			key    = params[0].to_s.split("_")[0]
			value  = params[1]
			
			if self.class.expects_parents.include?(key.to_sym)
				klass = key.classify.constantize.base_class
				# We have special needs here to handle sets!
				instance = (klass.respond_to? :in_set) ? klass.in_set(value) : klass.find(value)
				parents << instance
			else
 				barn_logger :error, trace, "Parent instances for #{key.classify} were asked for, but not found in parent expectations for #{self.class}"
			end
			
			# NOTE: we return the parents, not the params
			parents
		end

		# for the sake of clarity
		return @parent_instances.flatten
	end
	
	#-------------------
	# requested_through_parents?
	#-------------------
	
	def requested_through_parents?
		
		(@parents.blank?) ? false : true
	end
	
	#-------------------
	# parent_readable_by?
	#-------------------
	
	def parent_readable_by?(user)
		return false if @parents.blank?
		
		@parents.find_all{ |parent| parent.readable_by?(user) }.size > 0
	end
	
	#-------------------
	# parent_writeable_by?
	#-------------------
		
	def parent_writeable_by?(user)
		return false if @parents.blank?
		
		@parents.find_all{ |parent| parent.writeable_by?(user) }.size > 0
	end
		
end