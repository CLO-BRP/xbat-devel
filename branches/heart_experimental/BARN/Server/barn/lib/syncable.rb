module Syncable
	def self.included(base) # :nodoc:
		base.extend ClassMethods
		base.cattr_accessor :sync_associations
  end

	module SingletonMethods
	end
	
	module InstanceMethods
	end
	
	module ClassMethods
		def syncs(*a)
			self.sync_associations = a
			
			extend Syncable::SingletonMethods
			include Syncable::InstanceMethods
		end

		
		def syncable
			self.sync_associations
		end
		
		def syncable?(array = [])
			array = [array].flatten unless !array.is_a?(Array)

			if array.blank?
				return !self.sync_associations.blank?
			end
			
			return self.sync_associations.include?(array)
		end
		
	
		def sync_includes_hash(options = {})
			includes = {}
			
			self.sync_associations.each do |a|	
				klass = a.to_s.singularize.capitalize.constantize
				
				if klass.respond_to?(:sync_includes_hash)
					includes[a] = {:include => klass.sync_includes_hash}
				else
					includes[a] = {} 
				end
			end
			
			return includes
		end

		
		def find_for_sync(user, date)
			find(:all, :conditions => ["user_id = ? AND ( (modified_at IS NULL AND created_at > ?) OR modified_at > ? OR created_at IS NULL)", user.id, date, date])
		end
		
	end
	

end