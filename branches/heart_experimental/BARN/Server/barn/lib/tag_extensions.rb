# Tag extensions allow us to further shape the behavior of the taggable entities
# it is intended to be used wherever acts_as_taggable is used in model-land

module ActiveRecord #:nodoc:
  module Acts #:nodoc:
    module Taggable #:nodoc:
		module InstanceMethods			

			def tag_accept=(value)

        		added_tags << value.strip
				self.tag_list.add( value.strip )

				#self.tag_add = value;
        		@vote = 1
			end
			
			def tag_reject=(value)

        		added_tags << value.strip
				self.tag_list.add( value.strip )

				#self.tag_add = value;
       			@vote = -1
			end

			def tag_add=(value)				
				# NOTE: we delegate to tag_list= the job of updating @added_tags
				
				self.tag_list = self.tag_list.concat(value.split(",")).uniq
			end

			def tag_list=(value)
				# NOTE: 'TagList.from' handles both strings and arrays
				
				@tag_list = TagList.from(value)
				
				@tag_names = tags.map(&:name)	
				
				@difference = @tag_list.reject { |proposed| @tag_names.include?(proposed) }
				
				unless @difference.empty?
					@difference.each { |tag| added_tags << tag.strip }
				end
			end

			def tag_remove=(value)
				self.tag_list = self.tag_list.remove(value.split(","))
			end

			def added_tags
				# NOTE: this methods simply helps us manage the 'added_tags' array
				
				if @added_tags.nil?
					@added_tags = []		
				end

				@added_tags
			end
			
			# TODO: do we really need the @rating, is this like the @mode?
			
			def tag_rated=(value)
				@rating = true
				args = value.split(",")
				val = args[0]
				rating = args[1]

				added_tags << val.strip
				self.tag_list.add( val.strip )

				@vote = rating
			end

			def list_for_tagset( value )
				@tagset = TagSet.find( value )
				
				found = taggings.find(:all, :conditions => ["tag_id IN (?) AND user_id = ?", @tagset.tags.map(&:id), User.current])
				
				if found.empty?
					return ''
				else
					return found.map {
						|tagging| tagging.tag.to_s
					}.join(',')
				end
			end

			def tagged_as?(value)
				
				self.tag_list.include?(value)
			end

			def tagged_as_count(value)
				
				self.tags.select{ |tag| tag.to_s == value }.length
			end

			def tagged_as_for_user?(value, user)
				
				taggings.find(:all, :conditions => [ "user_id = ?", user.id ]).map { |tagging| tagging.tag.to_s }.include?( value )
			end

			def my_tags(user_id = nil)
			  
				if user_id.nil?
				  user_id = User.current.id; #user.id;
				end
				
				taggings.find(:all, :conditions => [ "user_id = ?", user_id ] ).map(&:tag)
			end

			def other_tags()
				# NOTE: this is an exclusive or set of tags created by other users versus my own
				
				taggings.find(:all, :conditions => [ "user_id <> ? && tag_id not in (?)", user.id, my_tags.map(&:id) ] ).map(&:tag)
			end

			def namespace_tags( namespace )
				
				taggings.find(:all).map(&:tag).uniq.select { |tag| get_namespace( tag ) == namespace }
			end

			def find_namespace( tags, namespace )
				
				output = []
				
				tags.each do |tag|
					if (get_namespace( tag ) == namespace)
						return true
					end
				end
				
				false
			end

			def get_namespace( tag )
				
				set = tag.to_s.split(':')
				
				namespace = set[0..set.length-2].join(":")
			end


			# NOTE: overrides default in acts_as_taggable
			
			def save_tags
				
				return unless @tag_list

				existing_tags = my_tags().map { |tag| tag.name }
				
				tags_to_remove = existing_tags - @tag_list
				
				drop_tags = my_tags().select { |tag| tags_to_remove.include? tag.to_s }
				
				#TODO - is this _really_ necessary?
				
				if ( !@vote.nil? )
					#a rated tag - there will be no deletion in this case, bc it came from the tag_rated accessor above
					#however, we will have to drop the selected tag so it can be reentered with the new score
					drop_tags = my_tags().select { |tag| added_tags.include? tag.to_s }
				end
				
				if @vote.nil?
					@vote = 1
				end
				
				Tagging.vote = @vote				
				
				#remove preexisting associations if tags are not in the list
				self.class.transaction do
				
					if drop_tags.any?
						taggings.find(:all, :conditions => ["tag_id IN (?) AND user_id = ?", drop_tags.map(&:id), User.current]).each(&:destroy)
						
						taggings.reset
					end
					
					# NOTE: if new tags are introduced, add them here

					added_tags.each do |new_tag_name|
						
						tags << Tag.find_or_create_with_like_by_name(new_tag_name)
					end
				end
				
				true
			end
			
		end
	
	end
  end
end