class M3uParser < PlaylistParser
  attr_accessor :lines
  
  def parse(file)
    file.each_line {|line| @lines << line}
    
    if !extended?
      @lines.each{|line| @entries << Entry.new(line.strip, {}) }
    else
      @lines.delete_at(0)
      @lines.each_slice(2) { |lines|
        @entries << Entry.new(lines[1].strip, parse_metadata(lines[0]))
      }
    end
    
    return @entries
  end
  
private

  # Check the first line to see if we are dealing with the 'extended' format
  def extended?
    @lines.first.strip.eql? '#EXTM3U'
  end

  # Get any metadata from the file
  def parse_metadata(string)
    string.delete!('#EXTINF:').strip!
    content = string.split(',')
    
    return {:duration => content[0], :title => content[1]}
  end
end