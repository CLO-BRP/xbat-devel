class PlaylistParser
  Entry = Struct.new(:path, :metadata)

  def initialize
    @lines   = []
    @entries = []
  end
    
  def parse(file)
    raise 'Abstract method called'
  end
end