$LOAD_PATH << File.dirname(File.expand_path(__FILE__))
$LOAD_PATH << File.join(File.dirname(File.expand_path(__FILE__)), '..', 'lib')
$LOAD_PATH << File.join(File.dirname(File.expand_path(__FILE__)), '..', 'lib', 'parsers')

require 'test/unit'
require 'playlist'

class PlaylistTest < Test::Unit::TestCase
  def setup
    directory = File.dirname(File.expand_path(__FILE__))
    
    file = File.join(directory, 'files', 'playlist.m3u')
    @playlist = Playlist.new(file)
        
    file = File.join(directory, 'files', 'playlist_extended.m3u')
    @playlist_extended = Playlist.new(file)
  end
  
  def test_for_regular_parse
    path = 'Alternative\everclear - SMFTA.mp3'
    
    assert_equal(@playlist.first, path)
    assert_equal(@playlist.entries.first.path, path)
    
    assert_equal(@playlist.entries.size, 5)
    
    assert_equal(@playlist.entries.first.metadata, {})
  end
  
  def test_for_extended_parse
     path = 'Alternative\everclear - SMFTA.mp3'

     assert_equal(@playlist_extended.first, path)
     assert_equal(@playlist_extended.entries.first.path, path)

     assert_equal(@playlist_extended.entries.size, 5)

     assert_equal(@playlist_extended.entries.last.metadata, {:duration=>"-1", :title=>"My Cool Stream"}
     )
   end
end