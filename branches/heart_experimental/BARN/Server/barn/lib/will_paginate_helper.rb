module WillPaginateHelper
  
  def self.included(base) # :nodoc:
    super
    base.extend ClassMethods
    base.send(:attr_accessor, :per_page)
    base.per_page = 10
    # Call this constant to induce an error if will_paginate in not installed
    WillPaginate
  end
  
  module ClassMethods
    
    # WillPaginate needs this
    def per_page=(num)
      @@per_page = num
    end
    
    # WillPaginate needs this
    def per_page
      return @@per_page
    end
    
  end
end