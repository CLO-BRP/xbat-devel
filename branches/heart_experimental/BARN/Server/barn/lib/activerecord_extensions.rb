module ActiveRecord
  class Base
	
		#-----------
		# associations
		#-----------
		
   	def self.associations
			reflect_on_all_associations.map{ |a| a.name }
    end

		#-----------
		# has_association?
		#-----------
		
    def self.has_association?(association)
			association  = association.to_sym if association.is_a?(String)
			
			return Set.new(associations).member?(association)
    end

		#-----------
		# convert_includes_to_joins
		#-----------
		
		# It may be possible to get the includes from the scope,
		# then do the other cleanup work here. 
		
		# have a look at find_with_associations in ActiveRecord::Associations::ClassMethods
		# this may be the same or similar to the following

		def self.convert_includes_to_joins(includes)
			includes = (includes.is_a? Array) ? includes : [includes]
			joins = []

			includes.each do |i|
				sql = ""
				# this protected method of ActiveRecord::Base modifies the join_sql in place
				self.send(:add_joins!, sql, {:joins => i}, nil)
				joins << sql
			end

			return joins
		end
		
		#-----------
		# merge_joins
		#-----------
		
		# This is lifted from Rails 2.3 ....
		
		def self.merge_joins(*joins)
			if joins.any?{|j| j.is_a?(String) || array_of_strings?(j) }
				joins = joins.collect do |join|
					join = [join] if join.is_a?(String)
					unless array_of_strings?(join)
						join_dependency = ActiveRecord::Associations::ClassMethods::InnerJoinDependency.new(self, join, nil)
						join = join_dependency.join_associations.collect { |assoc| assoc.association_join }
					end
					join
				end
				joins.flatten.map{|j| j.strip}.uniq
			else
				joins.collect{|j| safe_to_array(j)}.flatten.uniq
			end
		end
		# merge_joins
		
		#-----------
		# array_of_strings?
		#-----------
		
		# This is lifted from Rails 2.3 ....
		
		def self.array_of_strings?(o)
			o.is_a?(Array) && o.all?{|obj| obj.is_a?(String)}
		end
		
  end
end
