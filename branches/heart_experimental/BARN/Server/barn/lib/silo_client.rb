require 'rubygems'
require 'restclient'
# require 'activerecord'
# require 'silo'
# require 'deferred_request'

class SiloClient
	attr_accessor :silo, :login, :password
	
	HTTP_METHODS = [
	  :get, 
	  :post, 
	  :put, 
	  :delete
	]
	
	# This initialization needs some work
	def initialize(options)
		@login	 	= options[:login] 
		@password = options[:password]
		@user 		= options[:user]
		@current_user = options[:current_user]
    @base_uri = "http://localhost" 
    
		# get an instance of the silo we are looking working with.
		@silo = if options[:guid]
			Silo.find_by_guid(options[:guid])
		elsif options[:id]
			Silo.find(options[:id])
		end
		
 		if @silo
			@base_uri = "http://#{@silo.host}" 
			@base_uri << ":#{@silo.port}" if @silo.port
		end
	end
	
	#--------
	# sugar
	#--------

  # Example helper method, but you could also say:
  # SiloClient.new(params).request :get, 'index.html'
	def audio
		request :get, 'audio'
	end
	
	#--------
	# execute
	#--------
	
	# Generic wrapper and catch for requests to the silo.
	# This may well suck and need to be replaced with Net::HTTP, but seems clean enough for now	
	
	def request(method, request)
	  # Check for approved methods
	  raise Exception unless SiloClient::HTTP_METHODS.include?(method)
	  
	  # Attempt to make a request - RestClient and Net::HTTP like to raise exceptions
		begin
			remote_response = RestClient.send(method, File.join(@base_uri, request))
			
			response = {
				:status   => remote_response.headers[:http_status], 
				:body 		=> remote_response,
				:headers  => remote_response.headers
			}
			
		# Net::HTTP connection error	
		rescue Errno::ECONNREFUSED => error
			defer_request(request)			

			response = {
				:body 		=> "As far as Net::HTTP can tell, this server does not exist at all or is completely unreachable. Request deferred.",
				:error 		=> error,
				:deferred => true
			}
		
		# RestClient exceptions	
		rescue RestClient::Exception => error_response
			response = {
				:status 	=> error_response.http_code, 
				:body 		=> error_response.response.read_body,
				:error 		=> true
			}
			
			# 408 (request timeout)
			if error_response.class == RestClient::RequestTimeout
				defer_request(request)
				
				# Make sure we note what happened in the response
				response.merge({:deferred => true})
			end
		end
		
		
		return response			
	end

protected

	def defer_request(request)
		DeferredRequest.create(
			:request => request, 
			:silo_id => @silo.id, 
			:user_id => @current_user.id, 
			:submitted_at => Time.now
		)
	end
	
end