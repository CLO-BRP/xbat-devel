require 'rubygems'
require 'spork'

Spork.prefork do
  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.

end

Spork.each_run do
  # This code will be run each time you run your specs.

end

# --- Instructions ---
# - Sort through your spec_helper file. Place as much environment loading 
#   code that you don't normally modify during development in the 
#   Spork.prefork block.
# - Place the rest under Spork.each_run block
# - Any code that is left outside of the blocks will be ran during preforking
#   and during each_run!
# - These instructions should self-destruct in 10 seconds.  If they don't,
#   feel free to delete them.
#




# This file is copied to ~/spec when you run 'ruby script/generate rspec'
# from the project root directory.
ENV["RAILS_ENV"] = "test"
require File.expand_path(File.dirname(__FILE__) + "/../config/environment")
require 'spec'
require 'spec/rails'
# 
# module SpecHelper
# 	def get_all_actions(cont)
# 		c = Module.const_get(cont.to_s.pluralize.capitalize + "Controller")
# 		c.public_instance_methods(false).reject{ |action| ['rescue_action'].include?(action) }
#  	end
# 
#   def controller_actions_should_fail_with_get(cont, except=[])
#     actions_to_test= get_all_actions(cont).reject{ |a| except.include?(a) }
#     actions_to_test.each do |a|
#       # puts "... #{a}"
#       get a
#       response.should redirect_to("http://test.host/#{cont.to_s.pluralize}")
#       flash[:error].should == 'Operation Failed'
#     end
#   end
# end

Spec::Runner.configure do |config|
  
  include AuthenticatedTestHelper
  
  include AuthenticatedSystem
  
  # If you're not using ActiveRecord you should remove these
  # lines, delete config/database.yml and disable :active_record
  # in your config/boot.rb
  
  config.use_transactional_fixtures = true
  
  config.use_instantiated_fixtures  = false
  
  config.fixture_path = RAILS_ROOT + '/spec/fixtures/'

  # == Fixtures
  #
  # You can declare fixtures for each example_group like this:
  #   describe "...." do
  #     fixtures :table_a, :table_b
  #
  # Alternatively, if you prefer to declare them only once, you can
  # do so right here. Just uncomment the next line and replace the fixture
  # names with your fixtures.
  #
  # config.global_fixtures = :table_a, :table_b
  #
  # If you declare global fixtures, be aware that they will be declared
  # for all of your examples, even those that don't use them.
  #
  # You can also declare which fixtures to use (for example fixtures for test/fixtures):
  #
  # config.fixture_path = RAILS_ROOT + '/spec/fixtures/'
  #
  # == Mock Framework
  #
  # RSpec uses it's own mocking framework by default. If you prefer to
  # use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr
  #
  # == Notes
  # 
  # For more information take a look at Spec::Example::Configuration and Spec::Runner
  
  # The following are some default database records for use in the Specs to test
  # valid objects and views
  
  #-------
  # Note
  #-------
  
  def default_note(options = {})
	user = options[:user] || default_user
	
	options.delete(:user)
	
	note = Note.find_or_create_by_title_and_body({ 
	  :title       => 'My New Note', 
	  :body => 'Its fun to annotate.'
	}.merge(options))
	
	sound = default_sound(:user => user)
	
	sound.add_notes_by_user(note, user)
	
	return note
  end  

  #-------
  # Sound
  #-------
  
  def default_sound(options = {})
	user = options[:user] || default_user
	
	options.delete(:user) if options[:user]
	
	sound = Sound.find_or_create_by_name_and_user_id({ 
	  :name    => 'My Awesome Sound', 
	  :public  => 1, 
	  :user_id => user.id 
	}.merge(options))
	
	return sound
  end

  #-------
  # Recording
  #-------
  
  def default_recording(options = {})
	user = options[:user] || default_user
	
	options.delete(:user) if options[:user]
	
	recording = Recording.create({ 
	  :name => 'recording1',
	  :file => '/nowhere',
	  :date => Time.now,
	  :bytes => 666,
	  :format => 'wav',
	  :channels => 2,
	  :samplerate => 44100,
	  :samplesize => 0,
	  :samples => 123456,
	  :duration => 1.0,
	  :content_type => "audio/wav",
	}.merge(options))
	
	sound = default_sound; sound.recordings << recording
	
	return recording
  end

  #-------
  # Project
  #-------
  
  def default_project(options = {})
	user = default_user
	
	project = Project.create({ 
	  :name        => 'My Awesome Project', 
	  :description => 'Say something nice and short.', 
	  :public      => 1, 
	  :user_id     => user.id 
	}.merge(options))
	
	return project
  end
  
  #-------
  # User
  #-------
  
  def default_user(options = {})
	
	if options && options[:login] || options[:email]
	  user = User.find_by_login(options[:login] || options[:email])
	  
	  return user if user
	end
	
	# only do this if we are not creating another user
	if options.blank?
	  user = User.find_by_login('joey.buttafuoco@gmail.com')
	  
	  return user if user
	end
	
	user = User.create({ 
	  :name => 'joey', 
	  :password => 'xxxxxx', 
	  :password_confirmation => 'xxxxxx', 
	  :login => 'joey.buttafuoco@gmail.com',
	  :email => 'joey.buttafuoco@gmail.com'
	}.merge(options))
	
	user.save!
	user.register!
	user.activate!
	user.reload
	
	return user
  end
  
  def default_admin_user(options = {})
    user = default_user(options)
    user.roles << default_role
    user.save!
    return user
  end
  
  def default_role
    Role.create(:name => 'administrator')
  end

  #-------
  # Log
  #-------
  
  def default_log(options = {})
	user = options[:user] || default_user
	
	options.delete(:user) if options[:user]
	
	log = Log.find_or_create_by_name_and_user_id({ 
	  :name    => 'My Awesome Log', 
	  :public  => 1, 
	  :user_id => user.id,
	  :sound_id => default_sound.id	
	}.merge(options))
	
	return log
  end
  
  #-------
  # Event
  #-------

  def default_event(options = {})
	user = options[:user] || default_user
	
	# NOTE: without this we get 'event_user' problems, however we seem to be not setting thie relation
	
	options.delete(:user) if options[:user]
	
	recording =  default_recording;
	
	sound = default_sound(:user => user); # sound.recordings << recording
	
	log = default_log(:user => user, :sound => sound)
	
	# TODO: is this the method we should use here? it seems wrong
	
	event = Event.find_or_create_by_log_id_and_recording_id({
	  :recording_id => recording.id,
	  :sound_id => sound.id, 
	  :log_id => log.id
	}.merge(options))
	
	return event
  end

  # NOTE: The following are some default Mock Objects for use in the Specs
    
  def mock_event(options = {}) 
    hash = {
      :id => 1,
      :start  => 0.0,
      :duration   => 1,
      :score   => 1,
      :low   => 0.0,
      :high   => 44100.00,
      :channel   => 1,
      :created_at   => Time.now,
      :rating => 0,
      :tags => '',
      :tag_list => '',
      :to_xml => "Event-in-XML", 
      :to_json => "Event-in-JSON", 
      :errors => []
    }
    
    options = hash.merge(options)
    
    return event = mock_model(Event, options)
  end
  
end
