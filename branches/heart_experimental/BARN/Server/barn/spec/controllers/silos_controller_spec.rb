#require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
#$:.unshift File.expand_path(File.dirname(__FILE__) + '/../../vendor/gems/fakeweb/lib')
#require 'fakeweb'
#
#
#
#describe SilosController do
#	integrate_views
#
#	before :all do
#		@guid = "ae820030-7360-41d0-aa19-5bc067b7686b"
#	end
#
#	describe "route generation" do
#		it "should map #deferred_request" do
#			route_for(:controller => "silos", :action => "deferred_request", :guid => @guid).should == "/silos/#{@guid}/request"
#		end
#
#		it "should generate params for #deferred_request" do
#      params_from(:get, "/silos/#{@guid}/request").should == {:controller => "silos", :action => "deferred_request", :guid => @guid}
#    end
#
#		it "should map #deferred_response" do
#			route_for(:controller => "silos", :action => "deferred_response", :guid => @guid).should == "/silos/#{@guid}/response"
#		end
#
#		it "should generate params for #deferred_response" do
#      params_from(:post, "/silos/#{@guid}/response").should == {:controller => "silos", :action => "deferred_response", :guid => @guid}
#    end
#
#		it "should map #remote_request" do
#			route_for(:controller => "silos", :action => "remote_request", :guid => @guid, :request => 'audio').should == "/silos/#{@guid}/audio"
#		end
#	end
#
#	describe "logged in user" do
#		before :each do
#			# this is a nasty mix of mocks and fixture data :(
#
#			@bill = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
#			@silo = mock_model(Silo, :guid => @guid, :host => "localhost", :port => "4567", :user => @bill)
#			login_as @bill
#			Silo.stub!(:find_by_guid).and_return(@silo)
#		end
#
#		it "should NOT respond to GET requests" do
#			get :deferred_response
#			response.headers["Status"].should =~ /400/
#
#			get :update
#			response.headers["Status"].should =~ /400/
#		end
#
#		it "should respond with text if the remote server is available and has text response" do
#			remote_resource = "{audio:1}"
#			@silo = Silo.create(:user_id => @bill.id)
#			Silo.stub!(:find_by_guid).and_return(@silo)
#			FakeWeb.register_uri(:get, "http://#{@silo.host}:#{@silo.port}/audio", :string => remote_resource, :content_type => "text/html")
#			#@silo.stub!(:get).and_return(remote_resource)
#
#			get :remote_request, :guid => @silo.guid, :request => 'audio'
#			response.body.should == remote_resource
#		end
#
#		it "should handle a connection error with a deferred request!" do
#			FakeWeb.register_uri(:get, "http://#{@silo.host}:#{@silo.port}/audio", :string => "404", :content_type => "text/html", :status => 404)
#			# RestClient.stub!(:get).and_raise RestClient::ResourceNotFound
#
#			#get :remote_request, :guid => @silo.guid, :request => 'audio'
#			#response.body.should == "404"
#
#			#FakeWeb.register_uri(:get, "http://#{@silo.host}:4567/audio", :string => "TIMEOUT!", :content_type => "text/html", :status => 408)
#			# RestClient.stub!(:get).and_raise RestClient::ResourceNotFound
#
#			#get :remote_request, :guid => @guid, :request => 'audio'
#			#response.body.should == "TIMEOUT!"
#		end
#
#		it "should handle request to an unknown silo" do
#			#FakeWeb.register_uri(:get, "http://#{@silo.host}/audio", :string => "Silo here", :content_type => "text/html", :status => 404)
#			# RestClient.stub!(:get).and_raise RestClient::ResourceNotFound
#
#			Silo.stub!(:find_by_guid).and_return(nil)
#			get :remote_request, :guid => @guid, :request => 'audio'
#			response.response_code.should == 500
#		end
#	end
#
#
#end