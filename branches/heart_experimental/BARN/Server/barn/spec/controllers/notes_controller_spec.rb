
require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe NotesController do
  #fixtures :user # THIS CLEARS OUT ALL USERS!!!!!! (please dont use fixtures)
	@@formats = %w{json js xml html}
  
	describe "GET" do
		before :all do
			@sound = default_sound
			@note = default_note
		end
		
		before :each do
			login_as(default_user)
		end
		
		it "should be a success" do
			get :index, :type => "sound", :sound_id => @sound.id
      response.headers["Status"].should =~ /200/ # OK
		end
	end

	before :all do
		# cleanup from any previous tests
		# Sound.destroy_all
		Noting.destroy_all
		Note.destroy_all
		#Sound.destroy_all
			
		user = User.find_by_login('ted')
		@user = (user.blank?) ? default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com') : user
  end

  before :each do
		login_as @user
  end

	describe "set notation routing" do		
		@@formats.each do |format| 
			it "/notes/{:id}.format should hit the index method, not the show method" do
				params_from(:get, "/notes/{1}.#{format}").should == {:controller => "notes", :action => "index", :id => "{1}", :format => format}
			end
		end
		
		it "/notes/{:id} should hit the index method, not the show method" do
			params_from(:get, "/notes/{1}").should == {:controller => "notes", :action => "index", :id => "{1}"}
		end
	end
	
	describe "nested routing" do		
		@@formats.each do |format| 
			it "should have the proper params for sound" do
				expected_params = {
					:controller => "notes", 
					:action => "create",
					:type => "sound",
					:sound_id => "1",
					:format => format
				}
				
				params_from(:post, "/sounds/1/notes.#{format}").should == expected_params
			end
			# TODO: add tests for projects, and other parent resources of notes
		end
	end
	
  # POST
  describe "POST /notes" do	
    before(:all) do
			# this sucks
			#User.destroy_all
			Sound.destroy_all
			
    	@note = default_note
			@user = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
			@sound = default_sound(:user => @user)
		end

    @@formats.each do |format| 

      it "should NOT create a note without a parent resource" do
        put :create, :note => {:title => "new note", :body => "yes, its a new note"}, :format => format
        # check_roles in the before filter should catch the above update attempt
				response.headers["Status"].should =~ /302/ if format == 'html' # Redirect
				response.headers["Status"].should =~ /403/ if format != 'html' # Unauthorized
        
				# response.headers["Status"].should =~ /412/ if format != 'html' # Precondition Failed
	
				Note.count.should == 1
      end

      it "should create a note with a parent resource" do
        put :create, :note => {:title => "new note", :body => "yes, its a new note"}, :type => "sound", :sound_id => @sound.id, :format => format
        # check_roles in the before filter should catch the above update attempt
        # response.headers["Status"].should =~ /302/ if format == 'html' # Redirect
        response.headers["Status"].should =~ /201/ if format != 'html' # Created
	
				Note.count.should == 2
      end

      it "should NOT allow creation of duplicate notes" do
				# This relys on the default_note !
        put :create, :note => {:title => "My New Note", :body => "Its fun to annotate."}, :type => "sound", :sound_id => @sound.id, :format => format
				Note.count.should == 1 # we already have a note from the before all
      end

			it "should NOT allow creation of notes on resources not owned (thus not annotatable)" do
				# TODO
			end
    end
  end # "POST /notes"

end