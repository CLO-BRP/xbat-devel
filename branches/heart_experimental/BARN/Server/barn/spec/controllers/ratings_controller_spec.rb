require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RatingsController do

	fixtures :user, :sound, :rating
	
	@@formats = %w{json js xml}


	def mock_rating(stubs={})
		@mock_rating ||= mock_model(Rating, stubs)
	end

	def mock_sound(stubs={})
		@mock_sound ||= mock_model(Sound, stubs)
	end


	def setup_test
	end

	before(:each) do
		setup_test
	end

	describe "GET" do 
		integrate_views
		
		describe "with user NOT logged in" do	
			@@formats.each do |format|
				it "should deny the request for format #{format}" do
					
					get :index, :format => format  		
					response.headers["Status"].should =~ /401/ # Unauthorized
				end
			end
		end
	
		describe "with user logged in" do
			before :each do
				login_as(:default)
			end
		
			it "should not have an HTML representation" do
				
				get :index, :format => 'html'
				response.headers["Status"].should =~ /406/ # Unacceptable
				
				get :index, :sound_id => 3, :format => 'html'
				response.headers["Status"].should =~ /406/ # Unacceptable
			end
	
			@@formats.each do |format| # loop through all the expected response formats  
			
				it "should be success as a resource" do					
					get :index, :format => format
					
					response.headers["Status"].should =~ /200/ # OK
				end
			
				it "should be success as a nested (Sound) resource" do
					testSound = Sound.find(:first)

					get :index, :sound_id => testSound.id, :format => format

					response.headers["Status"].should =~ /200/ # OK
				end
				
				it "should return all ratings if not requested as a nested resource" do
					get :index, :format => format # js and xml should also do the 
					
					response.headers["Status"].should =~ /200/ # OK
					
					# NOTE: there are 10 default user ratings in the fixture, none by others 
										
					assigns[:ratings].size.should equal(10) 
				end
	
				it "should return only ratings from the parent resource" do

					# the type and sound_id params are added automatically from the routes. 
					# in the spec, we must specify it manually


					#TODO regrettably, the mock blows up when the JSON is seralized in the view
					# I verified that this is not an issue with standard code - must change tests in some fashion

					Sound.stub!(:find).with( any_args() ).and_return( @sound = mock_sound(:save => true) )

					@sound.should_receive(:ratings_by_user).with( any_args() ).and_return( @rating = mock_rating(:save => true) )

					#puts @rating.to_yaml

					get :index, :sound_id => @sound.id, :type => 'Sound', :format => format

					assigns[:ratings].should equal( mock_rating )

				end
			end
		end
	end
	
	describe "POST" do 
		integrate_views
		
		describe "with user logged in" do
			before :each do
				login_as(:aaron)
			end
		
			@@formats.each do |format| # loop through all the expected response formats
				it "should create a new rating for #{format}" do

					Rating.stub!(:new).and_return( @rating = mock_rating(:save => true) )

					@rating.should_receive(:user_id=).with( any_args() ).and_return( 3 )

					post :create, :rating => {:rateable_id => 4, :rateable_type => 'sound', :rating => '5'}, :format => format

					assigns[:rating].should equal(mock_rating)
					
					response.headers["Status"].should =~ /201/ # Created
				end
				
				it "should update a new rating if it already exists" do 
					post :create, :rating => {:rateable_id => 4, :rateable_type => 'sound', :rating => '2'}, :format => format
					post :create, :rating => {:rateable_id => 4, :rateable_type => 'sound', :rating => '3'}, :format => format
					post :create, :rating => {:rateable_id => 3, :rateable_type => 'sound', :rating => '2'}, :format => format
					
					sound(:sound_3).ratings_by_user(user(:aaron)).size.should equal(1)
					
					sound(:sound_4).ratings_by_user(user(:aaron)).size.should equal(1)
					
					response.headers["Status"].should =~ /201/ # Created
				end
			end
		end
	end


end # describe RatingsController
