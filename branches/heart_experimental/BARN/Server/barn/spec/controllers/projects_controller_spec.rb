=begin

  Tests for the ProjectsController class.
 
  Fixtures are not used here! - just an experimentation in style
=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ProjectsController do
  #fixtures :user # THIS CLEARS OUT ALL USERS!!!!!! (please dont use fixtures)
  
  # We need to set the test up with some default users to simulate the different permissioning scenerios
  def setup_test
    # lets clean up a little bit
    Project.destroy_all # this is probably the wrong thing to do here....
    
    bill = User.find_by_login('bill')
    bill.destroy if bill
    
    ted = User.find_by_login('ted')
    ted.destroy if ted

	admin = User.find_by_login('admin')
	admin.destroy if admin
            
    Role.default_roles.each do |role|
      Role.find_or_create_by_name(role)
    end
    
    @bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
    @ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
    @admin = default_admin_user(:login => 'admin', :email => 'admin@gmail.com')
    @guest = default_user
    
    # BILLS PROJECTS - the focal user
    @bills_public_project  = default_project(:name => "Bill's Public Project", :public => 1, :user_id => @bill.id)
    @bills_private_project = default_project(:name => "Bill's Private Project", :public => 0, :user_id => @bill.id)
    
    # TEDS PROJECTS
    @teds_public_project  = default_project(:name => "Teds's Public Project", :public => 1, :user_id => @ted.id)
    
    @teds_private_project_with_membership = default_project(:name => "Ted's Private Project WITH membership", :public => 0, :user_id => @ted.id)
    @teds_private_project_with_membership.add_users(@bill, 'member')
    @teds_private_project_with_membership.save!
    @teds_private_project_with_membership.reload
    
    @teds_private_project_without_membership = default_project(:name => "Ted's Private Project W/O membership", :public => 0, :user_id => @ted.id)
  end
  
  # Tests for GET requests to the SoundsController 
  describe "GET" do 
    integrate_views

    before(:all) do
      setup_test
    end
    
		# This belongs in a routing spec similar to sounds

	describe "set notation routing" do

		%w{js xml html}.each do |format|
			it "/projects/{:id}.format should hit the index method, not the show method" do
				params_from(:get, "/projects/{1}.#{format}").should == {:controller => "projects", :action => "index", :id => "{1}", :format => format}
			end
		end

		it "/projects/{:id} should hit the index method, not the show method" do
			params_from(:get, "/projects/{1}").should == {:controller => "projects", :action => "index", :id => "{1}"}
		end

	end
		
    # ---------------------------------
    # GUEST ACCESS
    # ---------------------------------
    
    describe "access to projects (not logged in)" do
      # TESTS
      # loop through all the expected response formats
      %w{js xml html}.each do |format| 
        
        it "/projects for '#{format}' should be success" do
          get :index, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end
        
        it "/projects for '#{format}' should only show public projects" do
          get :index, :format => format
          assigns[:projects].all?{|p| p.public? == true }.should be_true
          response.headers["Status"].should =~ /200/ # OK
        end

        it "/projects/{id} (public project) for '#{format}' should be success" do
          get :show, :id => @bills_public_project.id, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end

        it "/projects/new for '#{format}' should not be allowed" do
          get :new, :format => format
          # HTML format should redirect to the home page, other formats return unauthorized
          if format == "html"
            response.headers["Status"].should =~ /302/ # Redirect (Moved Temporarily) 
            response.should redirect_to(new_session_url)
          else
            response.headers["Status"].should =~ /401/ # Unauthorized
          end
        end

        it "/projects/{id}/edit for '#{format}' should redirect" do
          get :edit, :id => @bills_public_project.id, :format => format
          # HTML format should redirect to the home page, other formats return unauthorized
          if format == "html"
            response.headers["Status"].should =~ /302/ # Redirect (Moved Temporarily) 
            response.should redirect_to(new_session_url)
          else
            response.headers["Status"].should =~ /401/ # Unauthorized
          end
        end
        
      end # formats.each
    end # "guest access to projects

    
    # ---------------------------------
    # USER ACCESS  - Bill is the focus
    # ---------------------------------
    
    describe "logged in user access to private projects" do
      integrate_views
      
      before :each do
        login_as(@bill)
      end

      # TESTS
      %w{js xml html}.each do |format| 
        
        # /projects
        it " should show owned and member projects for '#{format}'" do
          get :index, :format => format
          response.headers["Status"].should =~ /200/ # OK

		  puts "id " + User.current.id.to_s

          projects = assigns[:projects]

		  projects.each { |project|
		  	puts project.id.to_s + " " + project.name
		  }

          projects.length.should equal(3)
          
          # Test the names of the projects - this is showing everything except Ted's public project
          assert projects.any?{|p| p.name == "Bill's Private Project"}
          assert projects.any?{|p| p.name == "Bill's Public Project"}
          assert projects.any?{|p| p.name == "Ted's Private Project WITH membership"}  
        end
        
        # /projects?public=1
        it "should show public projects only, regardless of membership for '#{format}'" do
          get :index, :public => 1, :format => format
          response.headers["Status"].should =~ /200/ # OK
  
          projects = assigns[:projects]

          projects.size.should equal(2)
          
          # Test the names of the projects - this is showing all public projects
          assert projects.any?{|p| p.id == @teds_public_project.id}
          assert projects.any?{|p| p.id == @bills_public_project.id}
        end

        # /projects/{id}
        it "should be visible for '#{format}' if you own the project" do
          get :show, :id => @bills_private_project.id, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end
        
        # /projects/{id}      
        it "/projects/{id} should be visible for '#{format}' if you DO NOT own the project, and ARE a user" do
          get :show, :id => @teds_private_project_with_membership.id, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end
        
        # /projects/{id}
        it " /projects/{id} should NOT be visible for '#{format}' if you DO NOT own the project, and are ARE NOT a user" do
          get :show, :id => @teds_private_project_without_membership.id, :format => format
          if format == "html"
            response.headers["Status"].should =~ /302/ # Redirect (Moved Temporarily) 
            response.should redirect_to('/session/new')
          else
            response.headers["Status"].should =~ /403/ # Forbidden is the default now
          end
        end
        
      end  # formats  
    end # registered user access to projects
    
    # it "/projects should show only those you own OR are a user of" do
    #       get :index
    #       assigns[:projects].count.should == 2
    #       response.headers["Status"].should =~ /200/ # OK
    #       
    # 
    #       @anothers_project.add_users(default_user)
    #       @anothers_project.save!
    #       @anothers_project.reload
    # 
    #       # The following should NOT work because we are logged in as default_user
    #       # put :update, :id => @anothers_project.id, :add_users => default_user.login
    #       get :index
    #       assigns[:projects].count.should == 3
    #       response.headers["Status"].should =~ /200/ # OK
    #     end
    #     
    # 
    #     # TESTS FOR PRIVATE PROJECTS
    #     describe "private projects" do
    #        before(:all) do
    #          setup_test
    #          # gives us @public_project, @private_project, @project_with_membership, @project_without_membership
    #        end
    # 
    #        before :each do
    #          login_as(@project.owner) # which is the default_user...
    #        end
    #        
    #        it "should be visible if you own the project" do
    #          get :show, :id => @private_project.id
    #          puts response.body
    #          response.headers["Status"].should =~ /200/ # OK, not redirect like it would be if pri
    #        end
    #      end
    #        

    
  end # GET
  
  # PUT 
  describe "PUT" do
    
    before(:all) do
      setup_test
	  @sound = default_sound(:user_id => @bill)
    end
    
    before :each do
      login_as(@bill) # which is the default_user...
    end
    
    %w{js xml html}.each { |format| 
      it "/projects should NOT allow you to update a project you do not own" do
        put :update, :id => @teds_private_project_without_membership.id, :add_users => @bill.login, :format => format
        # check_roles in the before filter should catch the above update attempt
        response.headers["Status"].should =~ /302/ if format == 'html' # Redirect
        #response.headers["Status"].should =~ /403/ if format != 'html' # Forbidden
  
				#TODO: the main problem right now is that the response is always ok
				
        # Make sure nothing was changed
        get :index
        assigns[:projects].size.should == 3
        response.headers["Status"].should =~ /200/ # OK
      end
			
	  it "/projects/:id should allow members add assets" do
		# Just to make sure that the setup is correct
		@teds_private_project_with_membership.has_member?( @bill ).should be_true

		put :update, :id => @teds_private_project_with_membership.id, :add_sounds => @sound.id, :format => format
		response.headers["Status"].should =~ /302/ if format == 'html' # Redirect
		response.headers["Status"].should =~ /200/ if format != 'html' # Success
      end

	  it "/projects/:id should allow members tag assets" do

		put :update, :id => @teds_private_project_with_membership.id, :project =>{:tag_list => "love-testing"}, :format => format
		response.headers["Status"].should =~ /302/ if format == 'html' # Redirect
		response.headers["Status"].should =~ /200/ if format != 'html' # Success

		@teds_private_project_with_membership.tag_list.size.should == 1
		@teds_private_project_with_membership.tag_list.first.should == "love-testing"
      end
	}
		
  end

  # DELETE 
  describe "DELETE /projects" do
	  before(:all) do
      setup_test
    end
    
    before :each do
    	login_as(@bill) # which is the default_user...
    end

		%w{js xml html}.each do |format| 
			it "should allow for deletion of a single project" do
				delete :destroy, :id => @bills_public_project.id, :format => format
				Project.count.should == 4
				response.headers["Status"].should =~ /200/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
			end
			
      it "should allow deletion in sets" do
        delete :destroy, :id => "{#{@bills_public_project.id},#{@bills_private_project.id}}", :format => format
				Project.count.should == 3 # in this test bills public project has already been deleted
				response.headers["Status"].should =~ /200/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
      end

      it "should NOT allow deletion of projects not owned - invalid sets" do
        delete :destroy, :id => "{#{@teds_private_project_without_membership.id},#{@bills_private_project.id}}", :format => format
				Project.count.should == 5
				response.headers["Status"].should =~ /409/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
      end

      it "should  allow deletion of projects by site admin" do
				login_as(@admin)
        delete :destroy, :id => "{#{@teds_private_project_without_membership.id},#{@bills_private_project.id}}", :format => format
				Project.count.should == 3
				response.headers["Status"].should =~ /200/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
      end

			it "should not be crazy" do
				Project.count.should == 5
			end
    end
	end

end
