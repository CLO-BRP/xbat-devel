=begin

State table for access to a Sound resource.
In cells marked, additional conditions apply based on the state of another resource.

	
								|	public		|	private		|
	---------------------------------------
	not logged in	|						|						|
	---------------------------------------
	logged in			|	+					|	++				|
	---------------------------------------
	
	+  User logged in and Sound public
		
		* Owner of Sound?
		
	++ User logged in and Sound private

		* Owner of Sound?
		* Sound shared with User?
		* Accessed through a Project?
			- User a Project member?

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe "Sound access:" do
	
	before do
		@owner = mock_model(User)
	end
	
	#----------
	# User NOT logged in
	#----------
	
	describe "User NOT logged in" do
		before do
			@user  = nil
		end
		
		it "should have the correct access" do
			# it "should NOT be creatable by the User" do
			Sound.creatable_by?(@user).should be_false
			# it "should be indexable by the User" do
			Sound.indexable_by?(@user).should be_true
		end
		
		describe "Sound is PUBLIC (x1,y1)" do
			before do
				@sound = Sound.new(:user => @owner, :public => 1)
			end
		
			it "should have the correct access" do
				# it "should be readable by the User" do
				@sound.readable_by?(@user).should be_true
				# it "should NOT be updatable by the User" do
				@sound.updatable_by?(@user).should be_false
				# it "should NOT be deletable by the User" do
				@sound.deletable_by?(@user).should be_false
			end
		end
		
		describe "Sound is PRIVATE (x2,y1)" do
			before do
				@sound = Sound.new(:user => @owner, :public => 0)
			end

			it "should have the correct access" do					
				# it "should NOT be readable by the User" do
				@sound.readable_by?(@user).should be_false
				#it "should NOT be updatable by the User" do
				@sound.updatable_by?(@user).should be_false
				# it "should NOT be deletable by the User" do
				@sound.deletable_by?(@user).should be_false
			end
		end
	end
	
	#----------
	# User logged in
	#----------
	
	describe "User logged in (possibly as the sound's owner)" do
		before do
			@user = mock_model(User)
		end
		
		it "should have the correct access" do
			#it "should be creatable by the User" do
			Sound.creatable_by?(@user).should be_true
			#it "should be indexable by the User" do
			Sound.indexable_by?(@user).should be_true
		end
				
		#----------
		# Sound is PUBLIC (x1,y2)
		#----------
		
		describe "Sound is PUBLIC (x1,y2)" do
			before do
				@sound = Sound.new(:user => @owner, :public => 1)
			end
			
			it "should have the correct access" do
				# it "should be readable by the User" do
				@sound.readable_by?(@user).should be_true
			
				# it "should NOT be updatable by the User, but updateable by the owner"
				@sound.updatable_by?(@user).should be_false
				@sound.updatable_by?(@owner).should be_true
			
				#it "should NOT be deletable by the User, but deletable by the owner"
				@sound.deletable_by?(@user).should be_false
				@sound.deletable_by?(@owner).should be_true
			end
		end
		
		#----------
		# Sound is PRIVATE (x2,y2)
		#----------
		
		describe "Sound is PRIVATE (x2,y2)" do
			before do
				@sound = Sound.new(:user => @owner, :public => 0)
			end

			it "should have the correct access" do
				# it "should NOT be readable by the User, but readable by the owner" do
				@sound.readable_by?(@user).should be_false
				@sound.readable_by?(@owner).should be_true
			
				# it "should NOT be updatable by the User, but updateable by the owner" do
				@sound.updatable_by?(@user).should be_false
				@sound.updatable_by?(@owner).should be_true
		
				# it "should NOT be deletable by the User, but deletable by the owner" do
				@sound.deletable_by?(@user).should be_false
				@sound.deletable_by?(@owner).should be_true
			end
			
			#----------
			# accessed through a Project
			#----------
			
			describe "accessed through a Project" do
				before do
					@project_owner = mock_model(User)
					@project = mock_model(Project, :public => 1, :user => @project_owner)
				end
				
				it "should have the correct access" do
					#it "should be indexable by the User" do
					# HOW IS THIS TESTED?
					@project.stub!(:readable_by? => false)
					Sound.indexable_by?(@user, [@project]).should be_false
					
					@project.stub!(:readable_by? => true)
					Sound.indexable_by?(@user, [@project]).should be_true
				end
				
				describe "with membership or is public" do
					it "should be readable (even though its a private Sound)" do
						# Here we are faking out the Project who will be tested elsewhere
						# so we are assuming that these are the responses from the Project!
						@project.stub!(:readable_by? => true)
						@project.stub!(:project_sounds => [mock_model(ProjectAsset, :asset_id => @sound.id, :asset_type => "Sound")])
						# these methods always expect an array of parents
						@sound.readable_by?(@user, [@project]).should be_true
						@sound.updatable_by?(@user, [@project]).should be_false
						@sound.deletable_by?(@user, [@project]).should be_false
	
						# just to make sure
						@sound.readable_by?(@user).should be_false
					end
				end
				
				describe "WITHOUT membership or is private" do
					it "should NOT be readable" do
						# Here we are faking out the Project who will be tested elsewhere
						# so we are assuming that these are the responses from the Project!
						@project.stub!(:readable_by? => false)
						@project.stub!(:project_sounds => [])

						# these methods always expect an array of parents
						@sound.readable_by?(@user, [@project]).should be_false
						@sound.updatable_by?(@user, [@project]).should be_false
						@sound.deletable_by?(@user, [@project]).should be_false
						
						@sound.readable_by?(@user).should be_false
					end
				end
			end
			
		end
	end

end