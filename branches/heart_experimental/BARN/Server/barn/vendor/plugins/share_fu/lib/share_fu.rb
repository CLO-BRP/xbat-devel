module ActiveRecord #:nodoc:
  module Acts #:nodoc:
    module ShareFu #:nodoc:
      def self.included(base)
        base.extend(ClassMethods)
      end
      
      module ClassMethods
        def shareable
          has_one :shared, :as => :shareable
					has_many :sharings, :through => :shared
					
					# THESE ARE TO BE PUT IN THE SHARE_FU PLUGIN
					named_scope :shared_with, lambda { |*args| 
				    user = (args.first.is_a?(User))? args.first : User.find(args.first)
				    {
				      :include => [:user, :shared, :sharings], 
				      :conditions => ["#{Sharing.table_name}.recipient_id = ?", user.id ]
				    } 
				  }

					named_scope :shared_by, lambda { |*args| 
				    user = (args.first.is_a?(User))? args.first : User.find(args.first)
				    {
				      :include => [:user, :shared, :sharings], 
				      :conditions => ["#{Sharing.table_name}.user_id = ?", user.id ]
				    } 
				  }
					# /THESE ARE TO BE PUT IN THE SHARE_FU PLUGIN
          
          include ActiveRecord::Acts::ShareFu::InstanceMethods
          extend ActiveRecord::Acts::ShareFu::SingletonMethods
        end
      end
      
      module SingletonMethods

      end
      
      module InstanceMethods
        # has many sharings
        def sharings
          (!shared.nil?)? shared.sharings : nil
        end

        def share(options={})
          recipient = options[:to] || options[:with]
          sharer    = options[:from] 

          @shared = Shared.find(:first, :conditions => "shareable_type = '#{self.class.to_s}' and shareable_id = #{self.id}")
          if @shared.blank?
            @shared = Shared.create(
              :shareable_type => self.class.to_s, 
              :shareable_id => self.id, 
              :forwardable => true
            )
          end

          @sharing = Sharing.find(:first, :conditions => "shared_id = #{@shared.id} and user_id = #{sharer.id} and recipient_id = #{recipient.id}")
          if @sharing.blank?
            @sharing = Sharing.new(:user_id => sharer.id, :recipient_id => recipient.id)
            @shared.sharings << @sharing
            reload
          end
          return @sharing
        end

        def remove_share(options = {})
          recipient = options[:to] 
          sharer    = options[:from] 

          if shared.blank?
            return false
          end

          @sharing = Sharing.find(:first, :conditions=>["shared_id = ? and recipient_id = ? and user_id=?", shared.id, recipient.id, sharer.id])
          if @sharing
            @sharing.destroy
            # clean up after yourself please ... :)
            shared.destroy if shared and shared.sharings and shared.sharings.count < 1
            reload
            return true
            #object.reload
          end
        end
        
        def shared_with?(user)
          if shared and !shared.sharings.blank?
            return true if shared.sharings.any?{|sharing| sharing.recipient_id == user.id}
          end
          return false
        end
        
        def shared_to
          if shared and !shared.sharings.blank?
            User.find(:all, :conditions=> "id in (#{shared.sharings.collect{|sharing| sharing.recipient_id}.join(',')})")
          end
        end

        def shared_by
          if shared and !shared.sharings.blank?
            User.find(:all, :conditions=> "id in (#{shared.sharings.collect{|sharing| sharing.user_id}.join(',')})")
          end
        end
        
        def is_shareable_by_user?(current_user)
          @shared = Shared.find(:first, :conditions => "shareable_type = '#{self.class.to_s}' and shareable_id = #{self.id}")
          user == current_user or (shared_with?(current_user) and @shared.forwardable)
        end
 
      end
    end # module Annotated 
  end # module Acts
end # module ActiveRecord
