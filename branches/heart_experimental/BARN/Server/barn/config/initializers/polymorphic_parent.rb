=begin
	
	This module is to assist with resource controllers that service primarily polymorphic models 
	and are typically accessed through a parent.
	
	This is included in the Controllers derived from ActionController and handles multiple instances
	from a single parent, and hopefully deep nesting with multiple parents

	/sounds/{1,5}/notes
	/sounds/5/tags
	/sounds/1/shares
	
	NB: This module requires the use of NamedScopeHelper in the polymorphic parents
	
=end

module PolymorphicParent # :nodoc:
	
	def self.included(base) # :nodoc:
		base.extend ClassMethods
	end
	
	module ClassMethods
		
		def parent_resources(*parents)
			@parent_resources ||= parents
 		end
	  
		# legacy: deprecation warning
	  def parent_resource_class
			logger.info "Deprecation warning: parent_resource_class will be removed"
	    (params[:type]) ? params[:type].classify.constantize : nil
	  end  
		alias :parent_class :parent_resource_class

	  def parent_id
			logger.info "Deprecation warning: parent_id will be removed"
	    (parent_resource_class) ? params["#{ parent_resource_class.to_s.downcase }_id"] : nil
	  end
	
	end
	# ClassMethods
	
	def parent_instances
		
		@parent_instances ||=
		request.path.split('/').reject(&:blank?).inject([]) do |parents, path_component|
			path_component = path_component.singularize
			
			if self.class.parent_resources.include?(path_component.to_sym)
				instance = instance_from_path_component(path_component)
				parents << add_klassy_name(instance)
			end
			
			parents.flatten
		end
	end
	


  def set_polymorphic_parents
		@this_user = (logged_in? && params[:public].blank?) ? User.current : nil
    
		@parent_keys = []
    if parent_instances.blank?
      @parents = nil
			#raise ArgumentError, "No parent resources found in the request path \"#{request.path}\". #{self.class} has to be accessed through a PolyParent route!"
    else
      @parents = parent_instances
			# this will not work for deep nesting... but why deep nest?
			# raise @parents.first.class.to_s
			@parent_class = @parents.first.class
			@parent_params = {@parent_keys.join => "{#{@parents.map{ |p| p.id }.join(',')}}"}
			@parent_params.merge( params )
    end
  end

private

  def parent_resource_id(parent)
		parent_key = "#{ parent }_id"
		@parent_keys << parent_key
    request.path_parameters[parent_key]
  end

  def instance_from_path_component(path_component)
    klass = path_component.classify.constantize.base_class
		# ID sets are handled by the named_scope
		# This also depends no the User.current method!
		if @this_user
    	klass.in_set(parent_resource_id(path_component)).accessible_by( @this_user )#.becomes(klass)
		else
			klass.in_set(parent_resource_id(path_component)).public
		end
  end

	def add_klassy_name(instance)
		returning instance do
			instance.instance_eval do
				def klassy_name
					self.class.class_name.downcase
				end
			end
		end
	end
		
end