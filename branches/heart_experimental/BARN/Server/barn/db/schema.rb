# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 0) do

  create_table "attachment", :id => false, :force => true do |t|
    t.integer   "attachable_id",   :null => false
    t.string    "attachable_type"
    t.integer   "asset_id",        :null => false
    t.string    "asset_type"
    t.timestamp "created_at",      :null => false
    t.timestamp "modified_at",     :null => false
  end

  create_table "barn_master", :force => true do |t|
    t.string    "type"
    t.string    "name"
    t.text      "create_sql"
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "data_file", :force => true do |t|
    t.string    "guid"
    t.string    "location"
    t.string    "name"
    t.string    "ext"
    t.datetime  "ctime"
    t.datetime  "mtime"
    t.integer   "size"
    t.integer   "directory",    :limit => 1
    t.integer   "in_ftp",       :limit => 1
    t.integer   "parent_id"
    t.integer   "project_id"
    t.integer   "user_id",                   :null => false
    t.string    "content_hash"
    t.timestamp "created_at",                :null => false
    t.timestamp "modified_at",               :null => false
  end

  create_table "event", :force => true do |t|
    t.string    "guid"
    t.float     "start"
    t.float     "duration"
    t.float     "low"
    t.float     "high"
    t.integer   "channel"
    t.float     "score"
    t.timestamp "created_at",      :null => false
    t.timestamp "modified_at",     :null => false
    t.integer   "log_id",          :null => false
    t.integer   "recording_id",    :null => false
    t.float     "recording_start"
  end

  create_table "event_measure__musical_range__value", :primary_key => "event_id", :force => true do |t|
    t.string "UpperBound"
    t.string "LowerBound"
    t.string "Span"
    t.string "error__UpperBound"
    t.string "error__LowerBound"
    t.string "error__Span"
  end

  add_index "event_measure__musical_range__value", ["event_id"], :name => "event_id", :unique => true

  create_table "event_measure__peak_amplitude__parameter", :force => true do |t|
    t.string  "content_hash"
    t.string  "reduce__method"
    t.integer "reduce__target"
    t.float   "block"
    t.integer "auto"
    t.float   "overlap"
    t.integer "user_id",        :null => false
  end

  create_table "event_measure__peak_amplitude__value", :primary_key => "event_id", :force => true do |t|
    t.float   "value"
    t.float   "time"
    t.float   "q1"
    t.float   "q2"
    t.float   "q3"
    t.float   "mean"
    t.float   "median"
    t.integer "parameter_id", :null => false
  end

  add_index "event_measure__peak_amplitude__value", ["event_id"], :name => "event_id", :unique => true

  create_table "event_measure__time_freq_quartiles__parameter", :force => true do |t|
    t.string  "content_hash"
    t.integer "specgram__fft"
    t.float   "specgram__hop"
    t.integer "specgram__hop_auto"
    t.string  "specgram__win_type"
    t.text    "specgram__win_param"
    t.integer "specgram__win_length"
    t.string  "specgram__sum_type"
    t.string  "specgram__sum_quality"
    t.integer "specgram__sum_length"
    t.integer "specgram__sum_auto"
    t.integer "restrict_to_event_band"
    t.integer "min_frequency"
    t.integer "user_id",                :null => false
  end

  create_table "event_measure__time_freq_quartiles__value", :primary_key => "event_id", :force => true do |t|
    t.float   "time__low"
    t.float   "time__median"
    t.float   "time__high"
    t.float   "time__range"
    t.float   "time__asymmetry"
    t.float   "frequency__low"
    t.float   "frequency__median"
    t.float   "frequency__high"
    t.float   "frequency__range"
    t.float   "frequency__asymmetry"
    t.integer "parameter_id",         :null => false
  end

  add_index "event_measure__time_freq_quartiles__value", ["event_id"], :name => "event_id", :unique => true

  create_table "event_user", :id => false, :force => true do |t|
    t.integer   "event_id",   :null => false
    t.integer   "user_id",    :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "event_user", ["event_id", "user_id"], :name => "event_id", :unique => true

  create_table "file_server", :force => true do |t|
    t.string    "server"
    t.string    "username"
    t.string    "password"
    t.string    "initial_path"
    t.string    "protocol"
    t.integer   "port"
    t.integer   "user_id",      :null => false
    t.timestamp "created_at",   :null => false
    t.timestamp "modified_at",  :null => false
  end

  create_table "image", :force => true do |t|
    t.string    "guid"
    t.string    "filename"
    t.integer   "size"
    t.string    "content_type"
    t.string    "thumbnail"
    t.integer   "parent_id"
    t.integer   "height"
    t.integer   "width"
    t.integer   "user_id",      :null => false
    t.timestamp "created_at",   :null => false
    t.timestamp "modified_at",  :null => false
  end

  create_table "library", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.integer   "user_id",     :null => false
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "library_sound", :id => false, :force => true do |t|
    t.integer   "library_id", :null => false
    t.integer   "sound_id",   :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "library_sound", ["library_id", "sound_id"], :name => "library_id", :unique => true

  create_table "library_user", :force => true do |t|
    t.integer   "library_id", :null => false
    t.integer   "user_id",    :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "library_user", ["library_id", "user_id"], :name => "library_id", :unique => true

  create_table "log", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.integer   "user_id",                       :null => false
    t.integer   "sound_id",                      :null => false
    t.boolean   "public",      :default => true
    t.timestamp "created_at",                    :null => false
    t.timestamp "modified_at",                   :null => false
  end

  create_table "note", :force => true do |t|
    t.string    "guid"
    t.string    "title"
    t.text      "body"
    t.string    "content_hash"
    t.timestamp "created_at",   :null => false
  end

  create_table "noting", :force => true do |t|
    t.integer   "note_id",        :null => false
    t.string    "annotated_type"
    t.integer   "annotated_id",   :null => false
    t.integer   "user_id",        :null => false
    t.timestamp "created_at",     :null => false
    t.timestamp "modified_at",    :null => false
  end

  create_table "peer", :force => true do |t|
    t.string    "host"
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "peer_user", :force => true do |t|
    t.integer   "peer_id",    :null => false
    t.integer   "user_id",    :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "peer_user", ["peer_id", "user_id"], :name => "peer_id", :unique => true

  create_table "project", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.text      "description"
    t.integer   "visible"
    t.integer   "public"
    t.integer   "user_id",     :null => false
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "project_asset", :force => true do |t|
    t.integer   "project_id",  :null => false
    t.integer   "asset_id",    :null => false
    t.string    "asset_type"
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "project_user", :force => true do |t|
    t.integer   "project_id",  :null => false
    t.integer   "user_id",     :null => false
    t.string    "state"
    t.integer   "role_id",     :null => false
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  add_index "project_user", ["project_id", "user_id"], :name => "project_id", :unique => true

  create_table "rating", :force => true do |t|
    t.string    "rateable_type"
    t.integer   "rateable_id",   :null => false
    t.integer   "rating"
    t.integer   "user_id",       :null => false
    t.timestamp "created_at",    :null => false
    t.timestamp "modified_at",   :null => false
  end

  create_table "recording", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.string    "file"
    t.string    "date"
    t.integer   "bytes"
    t.string    "format"
    t.integer   "channels"
    t.integer   "samplerate"
    t.integer   "samplesize"
    t.integer   "samples"
    t.integer   "duration"
    t.string    "content_hash"
    t.text      "location"
    t.timestamp "created_at",   :null => false
    t.timestamp "modified_at",  :null => false
  end

  create_table "recording_sound", :force => true do |t|
    t.integer   "recording_id",       :null => false
    t.integer   "sound_id",           :null => false
    t.integer   "position"
    t.integer   "cumulative_samples"
    t.timestamp "created_at",         :null => false
    t.timestamp "modified_at",        :null => false
  end

  add_index "recording_sound", ["recording_id", "sound_id"], :name => "recording_id", :unique => true

  create_table "role", :force => true do |t|
    t.string    "name"
    t.timestamp "created_at",  :null => false
    t.timestamp "modified_at", :null => false
  end

  create_table "role_user", :id => false, :force => true do |t|
    t.integer   "role_id",    :null => false
    t.integer   "user_id",    :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "role_user", ["role_id", "user_id"], :name => "role_id", :unique => true

  create_table "shared", :force => true do |t|
    t.string    "shareable_type"
    t.integer   "shareable_id",   :null => false
    t.integer   "forwardable"
    t.timestamp "created_at",     :null => false
    t.timestamp "modified_at",    :null => false
  end

  add_index "shared", ["shareable_id"], :name => "shareable_id", :unique => true

  create_table "sharing", :force => true do |t|
    t.integer   "shared_id",    :null => false
    t.integer   "user_id",      :null => false
    t.integer   "recipient_id", :null => false
    t.timestamp "created_at",   :null => false
  end

  create_table "sound", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.integer   "user_id",                        :null => false
    t.boolean   "public",       :default => true
    t.integer   "channels"
    t.float     "samplerate"
    t.float     "duration"
    t.float     "samples"
    t.string    "content_hash"
    t.timestamp "created_at",                     :null => false
    t.timestamp "modified_at",                    :null => false
  end

  create_table "sound_detector__band_amplitude__parameter", :force => true do |t|
    t.string  "content_hash"
    t.string  "reduce__method"
    t.integer "reduce__target"
    t.float   "block"
    t.integer "auto"
    t.float   "overlap"
    t.float   "duration"
    t.integer "snr"
    t.integer "noise_floor"
    t.integer "threshold"
    t.float   "break"
    t.float   "stop"
    t.integer "join_contiguous"
    t.integer "fill_gaps"
    t.integer "whiten"
    t.integer "min_freq"
    t.integer "max_freq"
    t.integer "butter_order"
    t.text    "filter__a"
    t.text    "filter__b"
    t.integer "user_id",         :null => false
  end

  create_table "sound_detector__band_amplitude__value", :primary_key => "event_id", :force => true do |t|
    t.float   "start"
    t.float   "stop"
    t.integer "parameter_id", :null => false
  end

  add_index "sound_detector__band_amplitude__value", ["event_id"], :name => "event_id", :unique => true

  create_table "sound_user", :id => false, :force => true do |t|
    t.integer   "sound_id",   :null => false
    t.integer   "user_id",    :null => false
    t.timestamp "created_at", :null => false
  end

  add_index "sound_user", ["sound_id", "user_id"], :name => "sound_id", :unique => true

  create_table "sync", :force => true do |t|
    t.string    "http_method"
    t.string    "http_user_agent"
    t.string    "http_request_headers"
    t.string    "added"
    t.string    "updated"
    t.string    "deleted"
    t.integer   "added_count"
    t.integer   "updated_count"
    t.integer   "deleted_count"
    t.integer   "peer_user_id",         :null => false
    t.timestamp "created_at",           :null => false
    t.timestamp "modified_at",          :null => false
  end

  create_table "tag", :force => true do |t|
    t.string    "name"
    t.timestamp "created_at", :null => false
  end

  create_table "tagging", :force => true do |t|
    t.integer   "tag_id",        :null => false
    t.string    "taggable_type"
    t.integer   "taggable_id",   :null => false
    t.integer   "user_id",       :null => false
    t.timestamp "created_at",    :null => false
  end

  create_table "task", :force => true do |t|
    t.string    "guid"
    t.string    "name"
    t.string    "description"
    t.text      "task"
    t.integer   "priority"
    t.integer   "user_id",      :null => false
    t.integer   "project_id"
    t.datetime  "started_at"
    t.string    "worker"
    t.integer   "attempt"
    t.float     "progress"
    t.text      "report"
    t.timestamp "last_update",  :null => false
    t.datetime  "completed_at"
    t.timestamp "created_at",   :null => false
    t.timestamp "modified_at",  :null => false
  end

  create_table "user", :force => true do |t|
    t.string    "guid"
    t.string    "login"
    t.string    "name"
    t.string    "email"
    t.timestamp "created_at",                                       :null => false
    t.timestamp "modified_at",                                      :null => false
    t.string    "crypted_password"
    t.string    "salt"
    t.string    "remember_token"
    t.timestamp "remember_token_expires_at",                        :null => false
    t.string    "activation_code"
    t.timestamp "activated_at",                                     :null => false
    t.string    "state",                     :default => "passive"
    t.timestamp "deleted_at",                                       :null => false
  end

end
