class CreateLogs < ActiveRecord::Migration
  def self.up
    begin drop_table "log" rescue true end
    
    create_table :log do |t|
      t.column :name, :string
      t.column :user_id, :integer
      t.timestamps
    end
    
    create_table :log_sound do |t|
      t.column :log_id, :integer
      t.column :sound_id, :integer
      t.column :created_at, :timestamp
    end
  end

  def self.down
    drop_table :log_sound
    drop_table :log
  end
end
