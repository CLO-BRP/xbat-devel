class AddGuidToPeers < ActiveRecord::Migration
  def self.up
		add_column :peer, :guid, :string
  end

  def self.down
		remove_column :peer, :guid
  end
end
