class CreateSoundUsers < ActiveRecord::Migration
  def self.up
    create_table :sound_user do |t|
      t.column :sound_id, :integer
      t.column :user_id, :integer
      t.column :created_at, :datetime
    end
  end

  def self.down
    drop_table :sound_user
  end
end
