class AddLocationToRecording < ActiveRecord::Migration
  def self.up
		add_column :recording, :location, :string
  end

  def self.down
		remove_column :recording, :location
  end
end
