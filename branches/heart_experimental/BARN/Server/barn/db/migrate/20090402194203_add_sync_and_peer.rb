class AddSyncAndPeer < ActiveRecord::Migration
  def self.up
	  create_table :sync do |t|
      t.string 		:http_method
			t.string 		:http_user_agent
      t.text 			:http_request_headers
			t.string 		:added
			t.string 		:updated
			t.string 		:deleted
			t.string 		:added_count, :default => 0
			t.string 		:updated_count, :default => 0
			t.string 		:deleted_count, :default => 0
			t.integer 	:peer_user_id
      t.datetime 	:created_at
      t.datetime 	:modified_at
    end

	  create_table :peer do |t|
			t.string 		:host
      t.datetime 	:created_at
      t.datetime 	:modified_at
    end

	  create_table :peer_user do |t|
			t.integer		:peer_id
			t.integer		:user_id
      t.datetime 	:created_at
      t.datetime 	:modified_at
    end
  end

  def self.down
  end
end
