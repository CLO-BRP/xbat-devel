class CreateImages < ActiveRecord::Migration

  def self.up
    create_table :image do |t|
      t.column :filename, :string
      t.column :size, :integer
      t.column :content_type, :string
      t.column :thumbnail, :string
      t.column :parent_id, :integer
      t.column :height, :integer
      t.column :width, :integer
      t.column :user_id, :integer            
      t.column :created_at, :datetime

      t.column :asset_id, :integer
      t.column :asset_type, :string
    end
  end

  def self.down
    drop_table :image
  end
end