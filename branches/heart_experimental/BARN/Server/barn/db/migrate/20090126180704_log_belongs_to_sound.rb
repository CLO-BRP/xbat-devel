class LogBelongsToSound < ActiveRecord::Migration
  def self.up
    drop_table :log_sound
    add_column :log, :sound_id, :integer
  end

  def self.down
    create_table :log_sound do |t|
      t.column :log_id, :integer
      t.column :user_id, :integer
      t.column :sound_id, :integer
      t.column :created_at, :timestamp
    end
  end
end
