class AddPortToPeer < ActiveRecord::Migration
  def self.up
		add_column :peer, :port, :string
  end

  def self.down
		remove_column :peer, :port
  end
end
