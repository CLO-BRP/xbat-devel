class AddContentTypeToRecording < ActiveRecord::Migration
  def self.up
    add_column :recording, :content_type, :string
  end

  def self.down
    remove_column :recording, :content_type
  end
end
