class ActsAsAnnotatedMigration < ActiveRecord::Migration
  def self.up
    begin drop_table "note" rescue true end

    create_table :note do |t|
      t.column :title, :string
      t.column :body, :text
      t.column :content_hash, :text
      t.column :created_at, :timestamp
    end

    begin drop_table "annotation" rescue true end

    create_table :annotation do |t|
      t.column :note_id, :integer
      t.column :annotated_id, :integer

      # You should make sure that the column created is
      # long enough to store the required class names.
      t.column :annotated_type, :string

      t.column :user_id, :integer, :null => true
      t.column :created_at, :timestamp
    end

    add_index :annotation, :note_id
    add_index :annotation, [:annotated_id, :annotated_type]
  end

  def self.down
    drop_table :annotation
    drop_table :note
  end
end
