class RemovePagesTable < ActiveRecord::Migration
  def self.up
		drop_table :page
  end

  def self.down
		create_table :page do |t|
			t.string :title
			t.string :permalink
			t.text :body
			t.datetime :created_at
			t.datetime :modified_at
			t.timestamps
		end
	end
end
