class CreateVolumesTable < ActiveRecord::Migration
  def self.up
		create_table :volume do |t|
			t.string :location
			t.string :name
			t.string :username
			t.string :password
			t.timestamps
		end
  end

  def self.down
		drop_table :volume
  end
end
