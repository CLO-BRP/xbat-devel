class AddUserToTaggings < ActiveRecord::Migration
  def self.up
    add_column :tagging, :user_id, :string, :null => false 
  end

  def self.down
    remove_column :tagging, :user_id
  end
end
