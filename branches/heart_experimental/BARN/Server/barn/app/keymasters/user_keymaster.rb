module UserKeymaster
  module ClassMethods
  	def indexable_by?(user, parents = nil)
  		# If requested through a parent(s), check the parents readability
  		(parents && parents_readable_by?(user, parents)) ? true : false
    end

		# This method determines whether a logged in user can create a user account
		# It is used by the Gatekeeper to assess authorization for BOTH the new and create methods
		# By default, admins can do anything, so this would be ignored if the user is an admin.
		# By setting this to TRUE, it allows anyone to create an account (pending activation of course)
		
  	def creatable_by?(user, parent = nil)
  		true
    end
  end

  def readable_by?(user, parents = nil)
		# we may eventually want some provision in here for private profiles...
		true
  end

  def updatable_by?(user, parent = nil)
		(user && user.id == self.id) ? true : false
  end

  def deletable_by?(user, parent = nil)
		# Only an admin should delete an account for now
		false
		#(user && user.id == self.user.id) ? true : false
  end
end