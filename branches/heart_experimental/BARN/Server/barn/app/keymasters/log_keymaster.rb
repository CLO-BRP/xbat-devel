module LogKeymaster
  
  module ClassMethods
    def indexable_by?(user, parents = nil)
  		parents.blank? || parents_readable_by?(user, parents)
    end

  	def creatable_by?(user, parent = nil)
  		user ? true : false
    end
  end

	# NOTE: these talk to the instance
	
  def readable_by?(user, parents = nil)
		public? || owned_by?(user) || parents_readable_by?(user, parents) && in_project?(parents.first)
  end

  def updatable_by?(user, parent = nil)
		owned_by?(user)
  end

	# TODO: consider the 'log_user' table
	
  def deletable_by?(user, parent = nil)
		false
  end
	
	# NOTE: these are helpers to the access helpes
	
	def owned_by?(user)
    return false if user.blank?
    
    (user_id == user.id)
  end
  
  def in_project?(project)
    return false if project.blank?
    
    project.project_logs.any? { |relation| relation.asset_id == id }
  end
end