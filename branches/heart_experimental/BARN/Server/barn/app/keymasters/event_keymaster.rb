module EventKeymaster

  module ClassMethods	
  	def indexable_by?(user, parents = nil)
  		# a logged in user can see a listing of his/her events
  		if parents.blank?
  			return (user)? true : false
  		end
    
		# if there are parents, check the parents
		
		project = parents.find{|parent| parent.class == Project}
		sound   = parents.find{|parent| parent.class == Sound}
		log	    = parents.find{|parent| parent.class == Log}
		
		recording = parents.find{|parent| parent.class == Recording}
   
		if (sound || log) && project
			
			return parents_readable_by?(user, [project])
		elsif recording
			return true;
		else
			parents_readable_by?(user, parents)
		end
    end

  	def creatable_by?(user, parent = nil)
      # any logged in user can create an Event
      user ? true : false
    end
  end
  
  def readable_by?(user, parents = nil)
    owned_by?(user) || parents_readable_by?(user, parents) && belongs_to_parents?(user, parents)
  end

  def updatable_by?(user, parent = nil)
		return true;
  
		owned_by?(user)
  end

  def deletable_by?(user, parent = nil)
		owned_by?(user)
  end
  
  def owned_by?(user)
    return false if user.blank?
    # there is some danger here that the self.user is blank
    (self.user.id == user.id) #|| sound_users.any?{|relation| relation.user_id == user.id}
  end
  
  def belongs_to_parents?(user, parents)
    return false if parents.blank?
    parents = [parents] if !parents.is_a?(Array)
    
    # The expectation here is that the parents are ordered as they are defined
    # in logical_parents
    
    parents.each do |parent|
			case parent.class.to_s
				when 'Project'
					assets = ProjectAsset.count(:conditions =>
						"project_id = #{parent.id} AND (asset_id = #{self.log.id} AND asset_type = 'Log') OR (asset_id = #{self.sound.id} AND asset_type = 'Sound')"
					)
					return (assets > 0 && parent.has_member?(user) || parent.public?) ? true : false
					
				when 'Sound'
					return self.sound.id == parent.id
				when 'Log'
					return self.log.id == parent.id
			end
    end
    
    return false # this should not be necessary
  end

  
end