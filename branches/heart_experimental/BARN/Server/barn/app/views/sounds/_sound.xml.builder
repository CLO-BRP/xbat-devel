xml.sound {
  xml.id(sound.id, :type => 'integer')
  
  xml.name(sound.name)
  
  # xml.location(sound.location, :type => 'string')
  
  xml.channels(sound.channels, :type => 'integer')
  
  xml.samplerate(sound.samplerate, :type => 'float')
  
  xml.samples(sound.samples, :type => 'integer')
  
  xml.duration(sound.duration, :type => 'float')
  
  rating = sound.rating_by_user(User.current)
  
  if !rating.blank?
    xml.user_rating(rating, :type => 'float')
    
    xml.user_rating_id(rating.id, :type => 'integer')
  else
    xml.user_rating(nil, :type => 'float')
    
    xml.user_rating_id(nil, :type => 'integer')
  end
  
  xml.average_rating(sound.rating, :type => 'float')
  
  xml.number_of_ratings(sound.ratings.size, :type => 'float')
  
  xml.tag_list(sound.tag_list, :type => 'string')  
  
  xml.user_id(sound.user_id, :type => 'integer')
  
  xml.created_at(sound.created_at, :type => 'timestamp')
  
  xml.modified_at(sound.modified_at, :type => 'timestamp')
}