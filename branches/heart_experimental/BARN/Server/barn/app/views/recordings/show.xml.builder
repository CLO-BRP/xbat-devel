# The following XML Builder template was distilled from this original code:

# recordings = @recordings.to_xml(:dasherize => false) do |xml|
#   @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :builder => xml, :dasherize => false)
# end
# render :text =>  recordings

# This template hands either a single recording from the 'show' action or multiple recordings
# via the index action and show action called with a set notation

xml.instruct! :xml, :version=>"1.0"

# NOTE: a single recording is represented in a simple way, a set of recordings is a result set packed with some metadata

unless @recording.is_a?(Enumerable)
  xml << render(:partial => 'recording', :locals => {:recording => @recording})
else
  xml.tag!(:result) {
    # metadata
    @metadata = {} if @metadata.blank?
    xml << @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :dasherize => false)
    
    #data    
    xml.tag!(:recordings, :type => 'array') {
      xml << render(:partial => 'recording', :collection => @recording)
    }
  }
end