module ProjectsHelper
	
	def columns_default
      columns = super
      
      #columns.concat ListingColumn.assemble(%w[project access description rating tags])
      
      #NOTE: controlling width here may be outdated, since we now have columns classes?
      
	#  columns.concat ListingColumn.assemble([
	#	  {:name => 'project', :width => '50%'},
	#	  {:name => 'access', :width => '4%'},
	#	  {:name => 'rating', :width => '12%'},
	#	  {:name => 'tags', :width => ''}
	#  ]);
	  
	  if logged_in?
		columns.concat ListingColumn.assemble([
			{:name => 'project', :width => '60%'},
			{:name => 'access', :width => '4%'},
			{:name => 'rating', :width => ''},
		]);
	  else
		columns.concat ListingColumn.assemble([
			{:name => 'project', :width => '60%'},
			{:name => 'rating', :width => ''},
		]);
	  end
	end
	

	# TODO: add number of members column to project

	def column_markup (row, column, project)
			
		case column.name
			
		when 'access'
			html = ""
			
			if logged_in?
				status = (project.public?)? 'unlocked' : 'locked'
				
				html << jquery_icon(status)
			end
			
		when 'project'
			
			html = "<h2>#{link_to(h(project.name), project_path(project))}</h2>"
			
			if !project.description.nil? && project.description.length > 0
				
				description = project.description
			else
				description = "<span class='description_hint'>No description available</span>"
			end
			
			html << "<div class='description'>#{description}</div>"
			
			if !project.long_description.nil? && project.long_description.length > 0
				
				description = project.long_description
				
				html << "<div class='long_description'>#{description}</div>"
			else
				description = "<span class='description_hint'>No long-description available</span>"
			end
		
			#html << "<div class='long_description'>#{description}</div>"	
			
			#html << "<h3>Tags</h3>"
			
			html << render(:partial => 'tags/tags', :locals => {:taggable => project})
			
			return html
		
			## description = '('+ role + ', ' + publicity + ')'
			#description = " (#{role})"
			#
			#return sprintf(str, icon, name, description)
		
		else
			super
			
		end
	end
	
	 
	def column_sorting (column)
			
		case column.name

		when 'access'
			{:order => 'project.public', :direction => toggle_sort_direction('project.public')}
			
		when 'project'
			{:order => 'name', :direction => toggle_sort_direction('name')}
				
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating')}
			
		else
			return super
			
		end
	end
	
	
	def sub_menu

		element = [];

		case params[:action]

		when 'show'
			# TODO: implement adding project members and sounds

			# NOTE: consider a new interface metaphor where we can look through a list of sounds or users and add them to another list

			if user_view?
				element = [
					#{:title => 'Add Member', :path => "#{params[:id]}/users"},
					#{:title => 'Add Sound', :path => "/some"},
					{:title => "New Scan", :path => "#{params[:id]}/jobs/new?type=scan"}
				];
			end
					
		else # index
			if user_view? && params[:type].blank?
				element = [
					{:title => "New Project", :path => "projects/new"}
				];
			end
		end

		return element;
	end

	#def sub_menu
	#	return [ { :title => "add project", :path => "/projects/new" }, { :title => "configure scan", :address => "/projects/configure_scan" } ]
	#end

end
