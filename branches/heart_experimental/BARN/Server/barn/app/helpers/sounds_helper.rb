module SoundsHelper

	def header_markup ( column, params )
		
		case column.name
			
			#when 'sound'
			#	#this sort of problem is where a viewmodel would help
			#	order = params.include?(:order) ? params[:order] : 'name'
			#
			#	control = "<dl class='render-sort-menu dropdown'><dt><a>#{order}</a></dt>"
			#	control << "<dd><ul>"
			#
			#	sound_sort.each { | sortcol |
			#		sorting = column_sorting(sortcol)
			#		control << "<li>" + link_to(title_caps(sortcol.name), params.merge(sorting).merge({:page => 1}), :class => "sort-listing") + "</li>"
			#	}
			#
			#	control << "</ul></dd>"
			#	control << "</dl>"
			#
			#	html = "<th width='#{column.width}' class='multisort'><span>#{title_caps('sort by')}</span>#{control}<span>#{params[:direction] == 'desc' ? 'descending' : 'ascending'}</span></th>"
		  	
#			when 'name'
#				html = "<th width='#{column.width}'>Sound</th>"
				
		  	when 'tags'
				html = "<th width='#{column.width}'>#{title_caps('tags')}</th>"
		    
		    else
		  		super
		end
	end

	def sound_sort
	
		sortcols = ListingColumn.assemble(%w[name duration channels events samplerate ratings tags])
	end

	def columns_default
	    # NOTE: from the parent we get selection and expansion columns
	    
	    columns = super

		if logged_in?
			columns.concat ListingColumn.assemble([
				{:name => 'sound', :width => '50%'},
				{:name => 'access', :width => '4%'},
				{:name => 'events', :width => '4%'},
				{:name => 'duration', :width => '8%'},
				{:name => 'date', :width => ''},
			]);
		else
			columns.concat ListingColumn.assemble([
				{:name => 'sound', :width => '50%'},
				{:name => 'events', :width => '4%'},
				{:name => 'duration', :width => '8%'},
				{:name => 'date', :width => ''},
			]);	
		end
		
		#if logged_in?
		#	columns.concat ListingColumn.assemble([
		#		{:name => 'name', :width => '25%'},
		#		{:name => 'public', :width => '4%'},
		#		{:name => 'events', :width => '4%'},
		#		{:name => 'rating', :width => '12%'},
		#		{:name => 'tags', :width => ''}
		#	]);
		#else
		#	columns.concat ListingColumn.assemble([
		#		{:name => 'name', :width => '25%'},
		#		{:name => 'events', :width => '4%'},
		#		{:name => 'rating', :width => '12%'},
		#		{:name => 'tags', :width => ''}
		#	]);	
		#end
		
 	end
  
	def column_markup( row, column, sound )
		  
		case column.name

		when 'name'
			html = ''
			
			if params[:project_id]
				html << link_to(sound.name, project_sound_path(params[:project_id],sound))
			else
				html << link_to(sound.name, sound_path(sound))
			end
			
		when 'duration'
			sec_to_clock(sound.duration)
		
		when 'date'
			if sound.date_time.nil?
				return "<span class='hint'>Date not available</span>"
			else
				# TODO: we need to set the timezone somewhere, look at the sound attribute and the wrapper class
				
				return sound.date_time.datetime.to_s[0..-5] # NOTE: this removed the ' UTC' suffix
			end
			
		when 'contributor'
			sound.user.login if sound.user
  
		when 'sound'
			  
			html = "<h2>#{link_to(h(sound.name), sound_path(sound))}</h2>"
			
			if !sound.description.nil? && sound.description.length > 0
				
				description = sound.description
			else
				description = "<span class='description_hint'>No description available</span>"
			end
			
			html << "<div class='description'>#{description}</div>"
			
			#if !sound.long_description.nil? && sound.long_description.length > 0
			#	
			#	description = sound.long_description
			#else
			#	description = "<span class='description_hint'>No long-description available</span>"
			#end
			#
			#html << "<div class='long_description'>#{description}</div>"	
			
			if false # !sound.location.nil?
				html << "<h3>Location</h3>"
				
				html << "
					<div class='location'>
						Lat.&nbsp;<span class='lat'>#{sound.location.lat}</span>,&nbsp;&nbsp;Long.&nbsp;<span class='lng'>#{sound.location.lng}</span>
					</div>
				"
			end
			
			#html << "<h3>Tags</h3>"
			
			html << render(:partial => 'tags/tags', :locals => {:taggable => sound})
			
			return html
		
			#html = "<span class='sound'>"
			#
			#if params[:project_id]
			#	html << link_to(sound.name, project_sound_path(params[:project_id],sound))
			#else
			#  html << link_to(sound.name, sound_path(sound))
			#end
			#html << "</span>"
			#
			#html << "<ul class='sounddata'>"
			#html << "<li><em>T</em> #{sec_to_clock(sound.duration)}</li>"
			#html << "<li><em>CH</em> #{sound.channels}</li>"
			#html << "<li><em>R</em> #{rate_to_hertz(sound.samplerate)}</li>"
			#html << "</ul>"
			#
			#html << "<span class='render simple-rating' target='#{send(sound.class.to_s.downcase + '_path', sound)}'>
			#	#{sound.rating}
			#</span>"			
  
		  when 'events'
			sound.events_count
			
		else
			super
			
		end
		  
	end
	
	
	def column_sorting (column)		
		
		params = {}
		
		case column.name
			
		when 'sound'
			params = {:order => 'sound.name', :direction => toggle_sort_direction('sound.name')}
			
		when 'public'
			params = {:order => 'sound.public', :direction => toggle_sort_direction('sound.public')}
			
		when 'name'
			params = {:order => 'sound.name', :direction => toggle_sort_direction('sound.name')}
		
		when 'log'	
			params = {:order => 'log.name', :direction => toggle_sort_direction('log.name')}
		
		when 'events'
			params = {:order => 'sound.events_count', :direction => toggle_sort_direction('sound.events_count', 'desc')}
		
		else
			return super
		
		end
		
		return params
	end

	def post_row( options, columns, row, object )
	
		super
	end
	
	def sub_menu
		element = [];
			
		#case params[:action]
		#	
		#when 'show'
		#	if user_view?
		#		element.push({:title => "New Scan", :path => "#{params[:id]}/jobs/new?type=scan"})
		#	end
		#	
		#else 
		#	if user_view?
		#		element.push({:title => "New Sound", :path => "sounds/new"})
		#	end
		#end
		
		return element; 
	end	

end
