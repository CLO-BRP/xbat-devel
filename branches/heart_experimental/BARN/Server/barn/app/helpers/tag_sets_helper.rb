module TagSetsHelper

	def header_markup ( column, params )
		
		case column.name

			when 'name'
				html = "<th width='#{column.width}'>Sound</th>"
				
		  when 'tags'
				html = "<th width='#{column.width}'>#{title_caps('tags')}</th>"
		    
		  else
		  	super
		end
	end

	def sound_sort
		sortcols = ListingColumn.assemble(%w[name tags])
	end

	def columns_default
	  # NOTE: from the parent we get selection and expansion columns
	    
	  columns = super
		
		columns.concat ListingColumn.assemble([
			{:name => 'name', :width => '25%'},
			{:name => 'public', :width => '4%'},
			{:name => 'tags', :width => ''}
		]);
 	end
  
	def column_markup( row, column, tag_set )
		  
		case column.name
  
		when 'name'
			html = ''
			
			if params[:project_id]
				html << link_to(tag_set.name, project_tag_set_path(params[:project_id], tag_set))
			else
				html << link_to(tag_set.name, tag_set_path(tag_set))
			end
			
		else
			super
		end
		  
	end
	
	
	def column_sorting (column)		
		
		params = {}
		
		case column.name

		when 'name'
			params = {:order => 'sound.name', :direction => toggle_sort_direction('sound.name')}

		else
			return super
		end
		
		return params
	end

	def post_row( options, columns, row, object )
		super
	end

end
