# NOTE: methods added to this helper will be available to all templates in the application

module ApplicationHelper
	
	include ServerStatistics
	
	# NOTE: this include is not working as a way of including the 'tag_cloud' helper
	
	#include TagsHelper

    include ListingHelper

	include MenuHelper

	include FilePathing

	include TabHelper

	#------------------------
	
	def user_view?
		# NOTE: a similar expression appears in a number of controllers and perhaps should be factored
		
		logged_in? && params[:public].blank?
	end
	
	def public_view?
		
		!user_view?
	end
	
	#------------------------
	
	def tag_cloud(tags, classes)
		return if tags.blank?

		# NOTE: remove the plus 1 when we are done
		
		max_count = tags.sort_by(&:count).last.count.to_f + 1
		
		tags = tags.sort_by(&:name)
		
		tags.each do |tag|
		  index = ((tag.count.to_i / max_count) * (classes.size - 1)).round
		  
		  yield tag, classes[index]
		end
	end
	
	
	def jquery_icon (type, opt = {})
		
		# NOTE: the 'icon' is a div.jquery-icon
		
		html = ""
		
		# html = "<ul style='list-style: none; margin: 0px; padding: 0px; float: left'><li style='float: left; margin: 0px; margin-right: 1em;' id='#{opt[:id]}' class='#{opt[:class]} jquery-icon ui-state-default ui-corner-all'>"
		
		# NOTE: the actual icon is an icon div child with class 'ui-icon'

		html = "<div class='#{opt[:class]} jquery-icon ui-state-default ui-corner-all'>"

		html << "<span uri='#{opt[:uri]}' class='ui-icon ui-icon-#{type}' style='margin: 3px'></span>"
	
		# NOTE: so the icon can have associated targets (we use class selection) or target (id selection)
		
		# TODO: consider moving this to use attributes of the container div
		
		if !opt[:targets].blank?
			html << "<span style='display: none;' class='targets'>" + opt[:targets] + "</span>"
		end
		
		if !opt[:target].blank?
			html << "<span style='display: none;' class='target'>" + opt[:target] + "</span>"
		end

		html << "</div>"		
	end
	
    # TODO: what we are trying to do is create a simple way to create a background load toggle

    def toggle_control (target, uri)
        jquery_icon 'plus', {:class => 'toggle', :targets => target, :uri => uri}
    end


	def render_flat(options = {})
		render(options).gsub(/\n/,'')
	end


	def toggle_sort_direction(column = nil, default = "asc")
		
		if column and column == params[:order]
			
			if params[:direction].blank?
				default
			elsif params[:direction].downcase == "desc"
				"asc"
			else
				"desc"
			end
		else
			default
		end
	end


	# TODO: check whether this is in use anywhere, deprecated in favor of random identifiers
	
	def id_from_uri(uri)
		
		return nil if uri.nil? # otherwise tests will fails because of the way we are determining path information
		
		#--
		# Make the container ID to which AJAX responses from this partial are targeted
		#--
		
		path_array = uri.split('/')
		path_array.delete_at(0) # this returns the deleted element, thus no chaining...
		path = path_array.join('-') 
		
		#--
		# account for possible format extension
		#--
		
		if path.split('.').size > 1
			path_array = path.split('.')
			path_array.pop
			path = path_array.join("")
		end
		
		return path
	end


	def rand_id
		'id_' + rand(999999999).to_s
	end


	def filesep
		Platform.is_windows? ? '\\' : '/'
	end


	#TODO this is defined in the application controller as well -> refactor to eliminate duplication

	def getParentFromParamType

		parent_type = nil
		parent_id = nil

		params.keys.each { |key|
			if key.to_s.include? '_id'
				parent_type = key.to_s.split('_id')[0]
				parent_id = params[key]
			end
		}

		[parent_type, parent_id]
	end


	#---------------------
	# DISPLAY HELPERS
	#---------------------
		
	# NOTE: this is a little patch so that we don't have a trivial title
	
	# NOTE: direct setting of title is needed
	
	def get_page_title(params)
		
		title = "BARN - #{params[:controller].capitalize}"
		
		if params.include?("type") && !params[:type].blank?
			# NOTE: this is based on the projects navigation, it may not generalize
			
			title << " - #{params[:type].capitalize}"
		end
		
		return title
	end
	
	
	def title_caps (title)
		part = title.gsub(/_/, ' ').split(' '); part[0].capitalize!; part.join(' ')
	end
	
	
	def sec_to_clock (sec)
		return "0:00" if sec.nil?
		
		clock = [sec/3600, sec/60 % 60].map{|t| t.floor.to_s.rjust(2, '0')}.join(':') + ':'
		
		sec = sec % 60
		
		if sec >= 10
			clock += sprintf('%0.3f', sec)
		else
			clock += '0' + sprintf('%0.3f', sec)
		end
	end
	
	
	def rate_to_hertz (rate)
		return if rate.blank? 
		
		if rate > 1000
			sprintf('%.2f kHz', rate/1000)
		else
			sprintf('%.2f Hz', rate)
		end
	end
	
	
	def score_to_percent (score)
		if score.blank?
			return '' 
		else
			return sprintf('%.2f', 100 * score) + '%'
		end
	end
	
	
	def size_to_bytes (size)
		return if size.nil? 
		
		frac = 1
		
		if size > frac.terabytes
			sprintf('%.2f', size / 1.terabyte.to_f) + ' TB'
		
		elsif size > frac.gigabytes
			sprintf('%.2f', size / 1.gigabyte.to_f) + ' GB'
			
		elsif size > frac.megabytes
			sprintf('%.2f', size / 1.megabyte.to_f) + ' MB'
			
		elsif size > frac.kilobytes
			(size / 1.kilobyte).to_s + ' kB'
			
		else
			size.to_s + ' B'
			
		end
	end

	# TODO: this should probably be called 'get_query_tags'
	
	def get_filter_tags(filter)
				
		if filter.blank?
			return [];
		end
		
		# TODO: this is copied from named_scope_filter.rb, we should refactor
		
		tokens = filter.split(',').each {|part| part.strip!}; tags = [];
		
		tokens.each do |token|
			
			key, modifier, value = token.scan(/([A-Za-z0-9-_.]+:)?\s*([<=>]{1,2})?\s*(\*?[A-Za-z0-9-_.:]+\*?)/)[0]
			
			if key.nil? || key == 'tag:'
				tags << value
			end
		end
		
		return tags;
	end
	
	
	#---------------------
	# PAGINATION STUFF
	#---------------------

	def render_per_page_controls( params, total_entries, class_default )

		values = ""

		[5, 10, 20, 50, 100].each do |per_page|

			if per_page < 2 * total_entries

				if params[:per_page].to_i == per_page || (params[:per_page].blank? && class_default == per_page)
					values += per_page.to_s + "\n"
				else
					values += link_to(per_page.to_s, params.merge({'per_page' => per_page, 'page' => 1}))  + "\n"
				end
			end
		end
		
		out = "<div class='per-page-controls'>" + values + "</div>"
	end


	#---------------------
	# LISTING STUFF
	#---------------------
	
	def get_listing_display_options(locals, params)
		
		# TODO: this needs work
		
		options = {:heading => 0, :caption => 1, :selection => 1, :expansion => 1}
		
		options.merge(locals).merge(params)
	end


	def header_markup ( column, params )

		case column.name
		
		# TODO: reconsider the various cases, in particular custom markup is interfering with sorting
		
		when ''
				
		when 'indicator'
			"<th class='indicator' width='#{column.width}'><div style='width=#{column.width} !important'></div></th>"
			
		when 'selection'
			"<th class='selection' width='#{column.width}'></th>"
			
		when 'expansion'
			"<th class='expansion' width='#{column.width}'></th>"
			
		else
			sorting = column_sorting(column)

			if sorting.empty?
				"<th class='#{column.name}' width='#{column.width}'>#{title_caps(column.name)}</th>"
			else
				# TODO: this kind of markup construction is error-prone, break it down
				
				#<th <%= "class='sorted #{params[:direction]}'" if params[:order] == sorting[:order] %> ><%= link_to(title_caps(column.name), params.merge(sorting).merge({:page => 1}), :class => "sort-listing") %></th>
				html = "<th width='#{column.width}' class='#{column.name} #{'sorted ' + params[:direction] if params[:order] == sorting[:order]}' >"
				html << link_to(title_caps(column.name), params.merge(sorting).merge({:page => 1}), :class => "sort-listing")
				html << "</th>"
			end
		end
	end


    def columns_default
		#{ :name => 'indicator', :width => '16px', :header => true },
		
		columns = ListingColumn.assemble([
            { :name => 'selection', :width => '20px', :header => true },
			{ :name => 'expansion', :width => '40px', :header => true }
        ])		
    end


	def column_markup (row, column, object)
	
		case column.name

		when 'indicator'
			'&nbsp;'
			
		when 'selection'
			"
				</div><input type='checkbox' targets='#{row[:handle]}' name='select-row' class='render-select-row selection-checkbox' id='select-#{row[:label]}' value='1'>
			"

		when 'expansion'
			uri = row[:uri]

			if !uri.nil?
				jquery_icon('plus', {:class => 'grid_toggle', :targets => row[:handle], :uri => uri})
			end

		when 'rating', 'rating.rating'
			"
			<div class='render simple-rating' target='#{send(object.class.to_s.downcase + '_path', object)}'>
				#{object.rating}
			</div>
			"
		
		when 'public', 'access'
			html = ""
			
			# TODO: the distinction is not show vs not-show, but rather whether the icon is live
			
			#if logged_in?
				status = (object.public?)? 'unlocked' : 'locked'
				
				html << jquery_icon(status)
				
				# html << "<span class='icon #{status}'>&nbsp;&nbsp;</span>"
			#end
		
		when 'score'
			score_to_percent(object.score)
	
		when 'tags'
			render(:partial => 'tags/tags', :locals => {:taggable => object})
			
		when 'bytes'
			size_to_bytes object.bytes
				
		when 'samplerate'
			rate_to_hertz(object.samplerate)
		
		when 'created', 'modified'
			column_name = column.name + '_at';

			"
			<span title='#{object.send(column_name) if object.respond_to?(column_name)}'class='at-time-ago'>
				#{time_ago_in_words(object.send(column_name)) + ' ago' unless object.send(column_name).nil?}
			</span>
			<span class='at-time'>
				#{object.send(column_name) if object.respond_to?(column_name)}
			</span>
			"
																 
		else
			begin
				h(object.send(column.name))
			rescue
				"Failed to markup #{column.name}."
			end

		end
	end


	def column_sorting (column)
				
		case column.name
			
		when 'rating'
			{:order => 'rating.rating', :direction => toggle_sort_direction('rating.rating', 'desc'), :include => 'ratings'}
		  
		when 'tags'
			{}
		
		when 'created', 'modified'
			column_name = column.name + '_at'; {:order => column_name, :direction => toggle_sort_direction(column_name, 'desc')}
			
		else
			{:order => column.name, :direction => toggle_sort_direction(column.name)}
			
		end
	end


    def post_row( options, columns, row, object )

		html = ""

		if options[:expansion] == 1
			html << "<tr style='display: none; border-top: none;' class='#{row[:handle]}  #{row[:state]}'>"
			html << "<td colspan=2 class='hide-cell'></td>"
			html << "<td class='content-pane' colspan='#{columns.length + 3}'>"
			html << "<p>Loading ...</p>"
			html << "</td>"
			html << "</tr>"
		end
	end


	#---------------------
	# FEED STUFF
	#---------------------
	
	# TODO: consider using an SVG icon
	
	def feed_description(type)
		type = type.gsub('_', ' ')
		
		case params[:action]
		
		when 'show'
			# TODO: it would be nice to include the name of the object, but this is not trivial
			
			description = type.singularize
			
		when 'index'	
			# NOTE: these are descriptions for the various 'index' actions
			
			if params.include?("query") && !params[:query].blank?
				
				description = "#{type.downcase} matching query \"#{params['query']}\""
				
			elsif params.include?("type") && !params[:type].blank?
				# NOTE: the assumption here is that 'type' serves as adjective
				
				description = "#{params[:type]} #{type.downcase}"
			else
				description = "#{type.downcase}"
			end
			
		end
		
		return description
	end
	
	def feed_link(type)		
		remove = %w{heading_markup heading_selector partial per_page page}
		
		rss_params = params.reject{|key,val| remove.include?(key)}
			
		rss_params['format'] = :rss
			
		link = link_to(
			image_tag('feed-icon.png', :border => 0, :style =>'padding-top:3px;'),
			rss_params,
			:title => feed_description(type) + ' RSS Link'
		)
		
		return "<div style='float:left;'>#{link}</div>"
	end


	# TODO: expand this to include various other formats
	
	def export_links(params, formats = nil)
		
		# NOTE: by default we include all the export formats, reconsider this
		
		formats ||= [:csv, :json, :rss, :xml]
		
		# NOTE: the first rejected keys are not relevant for feed and export links, the 'type' key is not relevant when empty
			
		export_params = params.reject{|key, val| ['heading_selector', 'per_page', 'page', 'layout', 'methods'].include?(key)}
			
		export_params.delete('type') if params.include?('type') && params[:type].blank?
		
		# TODO: consider the option of image links? there's probably a more concise way to do this
		
		links = '';
		
		if formats.include?(:csv)
			export_params['format'] = :csv
			
			links << link_to(
				'CSV', export_params, :class => 'csv', :title => "CSV&nbsp;export&nbsp;for&nbsp;#{feed_description(params[:controller])}"
			);
		end
		
		if formats.include?(:rss)
			export_params['format'] = :rss
			
			links << link_to(
				'RSS', export_params, :class => 'rss', :title => "RSS&nbsp;feed&nbsp;for&nbsp;#{feed_description(params[:controller])}"
			);
		end
		
		# NOTE: this sets the right header, we use 'JS' for display brevity ... it is also correct, in analogy to the 'XML' label
		
		if formats.include?(:json)
			export_params['format'] = :json
			
			links << link_to(
				'JS', export_params, :class => 'js', :title => "JSON&nbsp;export&nbsp;for&nbsp;#{feed_description(params[:controller])}"
			);
		end
		
		if formats.include?(:xml)
			export_params['format'] = :xml
			
			links << link_to(
				'XML', export_params, :class => 'xml', :title => "XML&nbsp;export&nbsp;for&nbsp;#{feed_description(params[:controller])}"
			);
		end
		
		return links
	end
	

	def javascript_for_controller()

		if ( !params[:controller].nil? )
			javascript_include_tag('controllers/' + params[:controller])
		end

	end

end
