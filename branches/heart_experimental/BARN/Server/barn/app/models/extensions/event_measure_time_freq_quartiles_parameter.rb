class EventMeasureTimeFreqQuartilesParameter < ActiveRecord::Base
  set_table_name "event_measure__time_freq_quartiles__parameter"
  
  has_many :event_measure_time_freq_quartiles, :foreign_key => :parameter_id, :class_name => "EventMeasureTimeFreqQuartiles"
  
  alias_method :values, :event_measure_time_freq_quartiles
end
