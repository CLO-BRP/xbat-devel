class EventMeasureTimeFreqQuartiles < ActiveRecord::Base
  set_table_name "event_measure__time_freq_quartiles__value"
  
  belongs_to :event
  belongs_to :event_measure_time_freq_quartiles_parameter, :foreign_key => :parameter_id
  
  alias_method :parameter, :event_measure_time_freq_quartiles_parameter
end
