class EventMeasureFormants < ActiveRecord::Base
  set_table_name "event_measure__formants__value"
  
  belongs_to :event
  
  belongs_to :event_measure_formants_parameter, :foreign_key => :parameter_id
  
  alias_method :parameter, :event_measure_formants_parameter
end
