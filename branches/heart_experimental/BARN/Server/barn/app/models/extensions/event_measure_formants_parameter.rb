class EventMeasureFormantsParameter < ActiveRecord::Base
  set_table_name "event_measure__formants__parameter"
  
  has_many :event_measure_formants, :foreign_key => :parameter_id, :class_name => "EventMeasureFormants"
  
  alias_method :values, :event_measure_formants
end
