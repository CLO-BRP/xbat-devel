class Attachment < ActiveRecord::Base
	include AccessControlHelper
	
  belongs_to :asset, :polymorphic => true
  belongs_to :attachable, :polymorphic => true
end