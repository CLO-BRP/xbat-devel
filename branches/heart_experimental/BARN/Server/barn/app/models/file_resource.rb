class FileResource < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	include SetNotationHelper

	# NOTE: these are 'primary' objects. could files be related to a 'log'?
	
	logical_parents :project, :sound, :user

	belongs_to :attachable, :polymorphic => true	

	acts_as_annotated	

	# NOTE: we have added a :guid interpolator to Paperclip::Interpolations! This is a trivial but critical method.
	# if we upgrade Paperclip we need to consider this. We should open the Interpolations class somewhere on our own tree.

	has_one :user

	has_attached_file :resource,
					  :url => "/files/file_resources/" + ("%08d" % :id).scan(/..../).join('/') + "/:guid/:basename.:extension",
					  :path => RAILS_ROOT + "/public/files/file_resources/" + ("%08d" % :id).scan(/..../).join('/') + "/:guid/:basename.:extension"

	before_save GUIDGenerator.new

  def location
    return RAILS_ROOT + "/public/files/file_resources/" + ("%08d" % :id).scan(/..../).join('/') + "/" + guid + "/"
  end

end


