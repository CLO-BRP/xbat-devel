class Recording < ActiveRecord::Base
  
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	include Syncable
	
	logical_parents :sound
	
	before_save GUIDGenerator.new
	
	#--
	# associations
	#--
	
	# sound association
	
	has_many :recording_sounds
	
	has_many :sounds, :through => :recording_sounds
	
	has_many :events
	
	# attachment
	
	# NOTE: attachment is not currently supported in a windows environment
	
	has_attachment :path_prefix => 'public/files/uploaded/recording', :storage => :file_system
                  
	#--
	# active record extensions
	#--
	
	acts_as_rateable
	
	acts_as_taggable
	
	acts_as_annotated
	
	shareable     
  
	syncs :events, :notes, :tags, :ratings

	#--
	# validations
	#--
	
	# NOTE: here we need to validate manually since we are not using the default attachemnt fields
	
	# validates_as_attachment
	
	attr_accessor :size, :filename
  
	#------------------
	# named scopes
	#------------------
	
	# readable_by - overwrite
	
	# TODO: account for sharing
	
	named_scope :readable_by, lambda { |*args| 
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		
		{
			:include => :sounds, 
			:conditions => ["#{Sound.table_name}.user_id = ?", user.id ]
		} 
	}
	
	# NOTE: since there is currently no public flag on Recording, we defer to its parent sounds
	
	named_scope :public, lambda { |*args| 
		{
			:include => :sounds, 
			:conditions => ["#{Sound.table_name}.public = 1"]
		} 
	}
  
	# NOTE: this helper maps files extensions to MIME types
	
	# NOTE: because a bug in Flash 8 sets the content-type always to application/octet-stream.
	
	# http://blog.airbladesoftware.com/2007/8/8/uploading-files-with-swfupload
	
	# http://jimneath.org/2008/05/15/swfupload-paperclip-and-ruby-on-rails/

	def swf_uploaded_data=(data)
		data.content_type = MIME::Types.type_for(data.original_filename)
		
		data.content_type = data.original_filename.split('.').last if data.content_type.blank?
		
		self.uploaded_data = data
	end
  
	# attachment_fu hacks for renamed columns file/filename, and filename sanitation
	
	def file=(new_name)
		write_attribute :file, sanitize_filename(new_name)
	end
  
	def sanitize_filename(filename)
		filename.strip!
		
		# NOTE: File.basename doesn't work with windows paths on unix get only the filename, not the whole path
		
		filename.gsub!(/^.*(\\|\/)/, '')
		
		# NOTE: replace non-alphanumeric, underscore or periods with underscore
		
		filename.gsub!(/[^\w\.\-]/, '_')
		
		filename
	end

	# NOTE: herer we get the thumbnail name for a filename, 'foo.jpg' becomes 'foo_thumbnail.jpg'
	
	def thumbnail_name_for(thumbnail = nil)
		return file if thumbnail.blank?
		
		ext = nil
		
		basename = file.gsub /\.\w+$/ do |s|
			ext = s; ''
		end
		
		# NOTE: ImageScience doesn't create gif thumbnails, only png
		
		ext.sub!(/gif$/, 'png') if attachment_options[:processor] == "ImageScience"
		
		"#{basename}_#{thumbnail}#{ext}"
	end
 
end
