class Job < ActiveRecord::Base

	include WillPaginateHelper
	
	include NamedScopeHelper

	include SetNotationHelper

	belongs_to :user
	
	belongs_to :project
	
	acts_as_tree :order => 'name'

	named_scope :readable_by, lambda { |*args|
		
		return {} if args.nil? || args.first.nil?
		
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		
		{
			:include => [:user],
			:conditions => ["#{table_name}.user_id = ?", user.id ]
		}
	}

	def before_create
		self.created_at = Time.now
	end
	
end
