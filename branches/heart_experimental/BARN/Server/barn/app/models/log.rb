class Log < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	include Syncable
	

	acts_as_rateable
	
	acts_as_taggable
	
	acts_as_annotated
	
	shareable
	
	
	logical_parents :project, :sound
	
	syncs :events, :notes, :tags, :ratings
	
	before_save GUIDGenerator.new
	
	#---------------
	# Associations
	#---------------
	
	belongs_to :user

	belongs_to :sound

	has_many :events
	
	# NOTE: project association, but a Sound could theoretically be attached to anything
	
	has_many :project_assets, :as => :asset
	
	has_many :projects, :through => :project_assets
		
	#---------------
	# Validations
	#---------------
	
	validates_presence_of :name
	
	validates_presence_of :user_id
	
	# TODO: we may want to relax the sound_id requirement so that we may store events from various sounds in a single log collection
	
	validates_presence_of :sound_id	

	def self.filterable_keys
		%w[]
	end

	#------------------
	# Named Scopes
	#------------------

	named_scope :in_project, lambda { |*args|
		project_id = args.first || {}

		return {} if project_id.blank?

		# Please note, this does not handle 'deep nesting'

		if project_id
		  conditions = "project_asset.project_id = #{project_id}"
		  joins = "INNER JOIN sound ON log.sound_id = sound.id INNER JOIN project_asset ON project_asset.asset_type = 'Sound' AND project_asset.asset_id = sound.id"
		end

		{
			:joins => joins,
			:conditions => conditions
		}
	}

	named_scope :readable_by, lambda { |*args| 
		return {} if args.nil? || args.first.nil?
		
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		{
			:include => [:user], 
			:conditions => ["#{table_name}.user_id = ?", user.id ]
		} 
	}
	
	def date_time
		
		sound.date_time
	end
end
