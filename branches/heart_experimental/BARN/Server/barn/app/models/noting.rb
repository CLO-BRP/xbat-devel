class Noting < ActiveRecord::Base #:nodoc:
	include WillPaginateHelper # this may be unnecessary now
	include NamedScopeHelper

	# PolymorphicNamedScopeHelper overrides with_parent and order_by
	# include PolymorphicNamedScopeHelper
	
	#has_many :annotations, :dependent => :destroy, :select => "annotation.*"
	# parent_resources :as => :annotated #:join_table => Annotation.table_name, 
	
	belongs_to :note
	belongs_to :annotated, :polymorphic => true
	belongs_to :user
	
	def after_destroy
		if Note.destroy_unused
			if note.notings.count.zero?
				note.destroy
			end
		end
	end

	named_scope :include_note, 
		{
			:select => "noting.*, note.title, note.body",
			:joins => :note
		}

	
end
