class UserMailer < ActionMailer::Base
	
	def signup_notification(user)
		setup_email(user)
		
		@subject += 'Please activate your new account'
		
		# NOTE: @body[:url] becomes '@url' in the view 'signup_notification.html.erb'
	
		# NOTE: @body[:user] becomes '@user' in the view 'signup_notification.html.erb'
		
		@body[:url] = "http://barn.xbat.org/activate/#{user.activation_code}"
	
		@body[:user] = user
	end
	
	def activation(user)
		setup_email(user)
		
		@subject += 'Your account has been activated!'
		
		@body[:url] = "http://barn.xbat.org/"
	end
	
	def password_change(user)
		setup_email(user)
		
		@subject << 'Your password has been changed.'
	end
	
	# def forgot_password(user)
	# end
	
	def ftp_account_notification(user, password, email)
		setup_email(user, email)
		
		@username = email
		
		@subject += 'Your FTP account has been created.'
		
		@body[:password]  = password
	end
	
	def forgot_password(to, login, pass, sent_at = Time.now)
		@subject = "Your password is ..."
		
		@body['login'] = login; @body['pass']  = pass
		
		@recipients = to
		
		@from = 'support@barn.xbat.org'
		
		@sent_on = sent_at
		
		@headers = {}
	end  
	
	protected
	
	def setup_email(user, email_address = nil)
		@recipients  = user.email
		
		@from = "BARN admin"
		
		@subject = "[BARN] "
		
		@sent_on = Time.now
		
		# TODO: this is one place where we confuse 'username' and 'email'
		
		@body[:username] = email_address || user.email
	end
end
