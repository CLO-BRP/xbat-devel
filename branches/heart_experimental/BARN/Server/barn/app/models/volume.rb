# There really is not a compelling reason for this to be database backed.
# Perhaps in the future, features can be added that take advantage of this.

class Volume < ActiveRecord::Base
	
	has_many :projects
	
	validates_uniqueness_of :name
	
	def mount_point
		if FILESYSTEM == UNIX
			File.join(UNIX_VOLUMES_ROOT, name.downcase)
		else
			"#{WINDOWS_VOLUMES_ROOT}\\#{name}"
		end		
	end
	
	alias_method :unix_mount_point, :mount_point
	
	def windows_mount_point
		"#{WINDOWS_VOLUMES_ROOT}\\#{name}"
	end
	
	def service
		"//#{location}/#{name}"
	end
	
	#------
	# mount
	#------
		
	def mount
		return false if mounted?
		
		# NOTE: make the mount point directory, this needs to be sudo ... then mount
		
		`sudo mkdir -p #{mount_point}` # FileUtils.mkdir_p mount_point
				
		result = `sudo mount.cifs #{service} #{mount_point} -o user=#{username},password=#{password},ro`
	
		if (result.blank?)
			return true
		else
			logger.error result
			
			return false
		end
	end
	
	#--------
	# unmount
	#--------
	
	def unmount
		return false if !mounted?
		
		result = `sudo umount #{mount_point}` 
		
		return (result.blank?) ? true : false
	end
	
	alias_method :umount, :unmount

	#---------
	# mounted?
	#---------
	
	def mounted?
		result = `mount | grep #{mount_point}`
		
		return (result.blank?) ? false : true
	end
	
end