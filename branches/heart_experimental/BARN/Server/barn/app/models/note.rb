require 'digest/md5'

class Note < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	include WillPaginateHelper
	include NamedScopeHelper

	attr_accessor		:hash
	cattr_accessor	:destroy_unused
	self.destroy_unused = true

	logical_parents :sound, :log, :event, :recording

	#---------------
	# Associations
	#---------------
	
	has_many :notings, :dependent => :destroy#, :select => "annotation.*"
	
	# It is necessary to specify the associated object here that have notes.
	# The reason is because the named_scope called with_parent uses explicit associations to join tables
	[:projects, :sounds, :recordings, :logs, :events, :images].each do |resource|
	  has_many resource, :through => :notings, :source => :annotated, :source_type => resource.to_s.classify,	:class_name => resource.to_s.classify
  end
  # has_many :projects,   :through => :notings, :source => :annotated, :source_type => "Project",   :class_name => "Project"
  # has_many :sounds,     :through => :notings, :source => :annotated, :source_type => "Sound",     :class_name => "Sound"
  # has_many :recordings, :through => :notings, :source => :annotated, :source_type => "Recording", :class_name => "Recording"
  # has_many :logs,       :through => :notings, :source => :annotated, :source_type => "Log",       :class_name => "Log"
  # has_many :events,     :through => :notings, :source => :annotated, :source_type => "Event",     :class_name => "Event"
  # has_many :images,     :through => :notings, :source => :annotated, :source_type => "Image",     :class_name => "Image"
	 
	acts_as_taggable
	
	#---------------
	# Validations
	#---------------
	
	validates_presence_of		:title
	validates_presence_of		:body
	validates_uniqueness_of	:content_hash
  
	#---------------
	# Filters
	#---------------
	
	before_save GUIDGenerator.new

	before_validation :create_content_hash
	
	#---------------
	# Named Scopes 
	#---------------
	
	# this is not correct. readable_by implies the ACL of either the note or its parent, not its ownership
	
	named_scope :readable_by, lambda { |*args| 
		return {}
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		{
			:select => "note.*, noting.annotated_type, noting.annotated_id, noting.user_id",
			:include => :notings,
			:conditions => ["#{Noting.table_name}.user_id = ?", user.id ]
		} 
	}

	named_scope :with_annotation, {
		:select => "note.*, noting.annotated_type, noting.annotated_id, noting.user_id",
		:joins => :notings
	}

	
	#---------------
	# Instance Methods
	#---------------
	
	def users
	  annotations.map{|a| a.user}
	end
	
	# This hash is what guarantees uniqueness of the note
	def create_content_hash
		return if title.blank? || body.blank?
		
		str = '' << title.strip  << body.strip
		self.content_hash = Digest::MD5.hexdigest(str);
	end

	def exists?
		create_content_hash
		if Note.find_by_content_hash(content_hash)
			return true
		else
			return false
		end
	end

end