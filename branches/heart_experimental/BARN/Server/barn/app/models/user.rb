require 'digest/sha1'

require 'fileutils'

require 'password_generator'

require 'find'

require 'file_hash'

require 'job' # to prevent pollution from Rake::Task, there is a better solution through Rake configuration

class User < ActiveRecord::Base

	include Authentication
	
	include Authentication::ByPassword
	
	include Authentication::ByCookieToken
	
	include Authorization::AasmRoles
	
	include NamedScopeHelper
	
	include SetNotationHelper	
	
	include AuthorizationApocalypse::Keymaster
	
	has_one :location, :as => :locatable
	
	acts_as_mappable :through => :location
	
	# NOTE: 'logical_parents' define expected parents for resource it is used by AuthorizationApocalypse::Keymaster
	
	logical_parents :project

	before_save GUIDGenerator.new
	
	#-------------
	# PLUGINS
	#-------------
		
	acts_as_rateable
	
	acts_as_taggable
	
	acts_as_annotated

	#-------------
	# ASSOCIATIONS
	#-------------	
	
	# has_many :role_users
	
	# has_many :roles, :through => :role_users
	
	has_and_belongs_to_many :roles
	
	has_many :sounds
	
	has_many :logs

	has_many :tag_sets, :as => :context
	
	has_many :jobs

	has_many :event_user	
	
	has_many :events, :through => :event_user
	
	# NOTE: there are projects we own and projects we participate in, these are qualitatively different
	
	has_many :owned_projects, :source =>:project, :class_name => "Project"
	
	# project memberships
	
	has_many :project_users
	
	has_many :project_memberships, :source =>:project,	:through => :project_users, :class_name => "Project"

	has_many :data_files
	
	# NOTE: the single quotes, the single quotes, uh ... the single quotes
	
	# NOTE: 'user.projects' returns all the projects that the user either owns or is a member of ...
	
	has_many :projects, :class_name => "Project", :finder_sql =>
		'SELECT project.* ' +
		'FROM project ' +
		'LEFT OUTER JOIN project_user ON project.id = project_user.project_id ' +
		'WHERE project_user.user_id = #{id} OR project.user_id = #{id} ' +
		'GROUP BY project.id'

	has_many :file_resources, :as => :attachable
	
	has_many :tag_sets, :as => :context
	
	# NOTE: these help the user carry out actions outside of the web application session
	
	has_many :tokens # this allows us to access all tokens simultaneously
	
	has_one :rss_token, :dependent => :destroy, :class_name => 'Token', :conditions => "action='rss'"
  
	has_one :api_token, :dependent => :destroy, :class_name => 'Token', :conditions => "action='api'"

	#-------------
	# VALIDATIONS
	#-------------

	validates_presence_of	:login
	
	validates_length_of		:login,	:within => 3..40
	
	validates_uniqueness_of	:login
	
	validates_format_of		:login, :with => Authentication.login_regex, :message => Authentication.bad_login_message
	
	
	validates_format_of		:name, :with => Authentication.name_regex, :message => Authentication.bad_name_message, :allow_nil => true
	
	validates_length_of		:name, :maximum => 100
	
	
	validates_presence_of	:email
	
	validates_length_of		:email, :within => 6..100 #r@a.wk
	
	validates_uniqueness_of	:email
	
	validates_format_of		:email, :with => Authentication.email_regex, :message => Authentication.bad_email_message
	
	#-------------
	# NAMED_SCOPES
	#-------------
		
	named_scope :in_project, lambda { |*args|
		project_id = args.first
		{
			#:joins => "LEFT OUTER JOIN project ON project.user_id = user.id LEFT OUTER JOIN project_user ON project_user.user_id = user.id",
			#:conditions => ["project_user.project_id = ? OR project.id = ?", project_id, project_id],
			:joins => "LEFT OUTER JOIN project_user ON project_user.user_id = user.id",
			:conditions => ["project_user.project_id = ?", project_id],
			:group => "user.id"
		}
	}
	
	named_scope :restrict_fields, {
		:select => "user.id, user.name, user.created_at, user.modified_at, user.login, user.email"
	}
		
	named_scope :filtered_by, lambda { |*args|
		params = args.first || {}
		filter = params[:query]
		conditions = []
		
		return {} if filter.blank?

		filter = filter.split(' ').each {|part| part.strip!};
		
		filter.each do |part|			
			conditions << "#{self.table_name}.name LIKE '%#{part}%'"
			conditions << "#{self.table_name}.login LIKE '%#{part}%'"
		end

		{
			:conditions => conditions.join(' OR ')
		}
	}
	
	# HACK HACK HACK -- how to do attr_accessible from here?
	# prevents a user from submitting a crafted form that bypasses activation
	# anything else you want your user to change should be added here.
	
	attr_accessible :login, :email, :name, :password, :password_confirmation, :roles
	
	attr_accessor :role



	def self.current=(user)
		@current_user = user
	end

	def self.current
		@current_user ||= nil #User.anonymous
	end


	def self.find_by_login_or_email(string)
		
		find :first, :conditions => "login = '#{string}' or email = '#{string}'"
	end
	
	# Authenticates a user by their login name and unencrypted password.	Returns the user or nil.
	#
	# uff.	this is really an authorization, not authentication routine.	
	# We really need a Dispatch Chain here or something.
	# This will also let us return a human error message.
	#
	def self.authenticate(login, password)
		
		return nil if login.blank? || password.blank?
		
		u = find_in_state :first, :active, :conditions => {:login => login} # need to get the salt
		
		u && u.authenticated?(password) ? u : nil
	end

	def login=(value)
		
		write_attribute :login, (value ? value.downcase : nil)
	end

	def email=(value)
		
		write_attribute :email, (value ? value.downcase : nil)
	end
	
	def has_role?(rolename)
		
		roles.find_by_name(rolename) ? true : false
	end
	
	def is_admin?
		
		has_role?('administrator')
	end
	
	def project_role(project)
		
		membership = ProjectUser.find(:first, :conditions => "project_id = #{project.id} and user_id = #{id}")
		
		if !membership.blank? # there should only be one
			return membership.role.name
		end
		
		return nil
	end
	
	def has_project_role?(project, rolename)
		
		role = Role.find_by_name(rolename)
		
		if role.blank? # there is no such role
			raise "No such role #{rolename}"
		end
		
		membership = ProjectUser.find(:all, :conditions => "project_id = #{project.id} and user_id = #{id} and role_id = #{role.id}")
		
		return (membership.blank?)? false : true
	end
	
	def send_new_password
		new_pass = PasswordGenerator.new(3).password
		
		self.password = self.password_confirmation = new_pass
		
		self.save
		
		update_unix_password(password)
		
		UserMailer.deliver_forgot_password(self.email, self.login, new_pass)
		
		return true
	end
  
	#-------------------------
	# token related
	#-------------------------
		
	def rss_key
		token = self.rss_token || Token.create(:user => self, :action => 'rss')
		
		token.value
	end
	
	# TODO: where is the 'create_api_token' method? why different from the rss code?
	
	def api_key
		# token = self.api_token || self.create_api_token(:action => 'api')
		
		token = self.api_token || Token.create(:user => self, :action => 'api')
		
		token.value
	end
	
	# NOTE: these allow us to retrieve a used starting from a key
	
	def self.find_by_rss_key(key)
		
		token = Token.find_by_action_and_value('rss', key)
		
		token && token.user.active? ? token.user : nil
	end
	
	def self.find_by_api_key(key)
		
		token = Token.find_by_action_and_value('api', key)
		
		token && token.user.active? ? token.user : nil
	end
	
  	#-------------------------
	# data file related
	#-------------------------
	
	# This method is a wrapper around find_by_email
	
	def self.find_by_directory(path)
		login = File.basename(path)
		
		login = login.split("-").first if RAILS_ENV == "development"
		
		# TODO: should this maybe be find_by_login instead?
		
		user = find_by_email(login)
	end
	
	def update_data_files
		
		each_file_path do |path|
			process_file path
		end
	end
	
	def each_file_path(&block)
		Find.find(ftp_root) { |path|
			puts path
			next if path == ftp_root 
			Find.prune if File.basename(path) =~ /^\./

			yield path
		}
	end
	
	def data_file_count
		Dir.entries(ftp_root).reject{|f| f =~ /^\./}.size
	end
	
	def cleanup_data_files
		# If there are no files in the files system, then cleanup the database
		# This is essentially a soft delete at the moment.
		
		if data_file_count == 0 && DataFile.count(:conditions => ["user_id = ? AND in_ftp = 1", self.id]) > 0
			data_files = DataFile.find(:all, :conditions => ["user_id = ?", self.id])
			data_files.each { |f| f.soft_delete }
			return
		end
		
		# Get all the files that are in the database, but not in the file system
		data_files = DataFile.find(:all, :conditions => {:user_id => self.id})

		each_file_path do |path|
			data_files.reject! {|f| f.location == path}
		end

		data_files.each {|f| f.soft_delete } unless data_files.blank?
	end
	
	def remove_missing_data_files
		DataFile.delete_all({:user_id => self.id, :in_ftp => false})
	end
	
	def process_file(path)
		
		data_file = DataFile.find(:first, :conditions => ["user_id = ? AND location = ?", self.id, path])

		# If this file exists, we check to see if there are changes
		# and save them as necessary

		if data_file 
			if data_file.mtime != File::Stat.new(path).mtime
				puts "File #{path} has changed!"
				# The DataFile exist, but has changed
				data_file.update_attributes(
					:size  => File.size(path),
					:ctime => File::Stat.new(path).ctime,
					:mtime => File::Stat.new(path).mtime,
					:content_hash => FileHash.md5(path)
				)
			end

			return data_file 
		end

		# From here, we know that we are dealing with an unknown file

		parent = DataFile.find(:first, :conditions => ["user_id = ? AND location = ?", self.id, File.dirname(path)])

		parent_id = (!parent.blank?) ? parent.id : nil

		if !File.directory?(path)	
			content_hash = FileHash.md5(path)
		else
			content_hash = nil
		end

		data_file = DataFile.create(
			:guid 		 => SimpleUUID.generate,
			:location  => path,
			:name 		 => File.basename(path),
			:directory => File.directory?(path),
			:ext 			 => File.extname(path), # this is the extension
			:parent_id => parent_id,
			:size 		 => File.size(path),
			:user_id 	 => self.id,
			:ctime     => File::Stat.new(path).ctime,
			:mtime     => File::Stat.new(path).mtime,
			:in_ftp 	 => true,
			:content_hash => content_hash
		)

		return data_file
	end

	#--------------
	# UNIX ACCOUNTS
	#--------------
	
	def ftp_username
		username = email
		
		if RAILS_ENV == 'development'
			username = username + "-dev" 
		end
		
		return username
	end
	
	def ftp_root
		
		File.join(USER_FTP_ROOT, ftp_username)
	end
	
	def establish_unix_account(password)
		if !Platform.is_linux? || RAILS_ENV == "test"
			return false
		end
		
		password = PasswordGenerator.new(3).password
		
		FileUtils.mkdir_p ftp_root	# make sure this exits ...
				
		result = `sudo ruby /#{RAILS_ROOT}/script/establish_unix_account.rb #{ftp_username} #{password} #{ftp_root}`
						
		UserMailer.deliver_ftp_account_notification(self, password, ftp_username)
		
		logger.info result
	end
	
	def remove_unix_account
		if !Platform.is_linux? || RAILS_ENV == "test"
			return false
		end

		result = `sudo ruby /#{RAILS_ROOT}/script/remove_unix_account.rb #{ftp_username}`
		
		logger.info result
	end
	
	def update_unix_password(password)
		if !Platform.is_linux? || RAILS_ENV == "test"
			return false
		end

		result = `sudo ruby /#{RAILS_ROOT}/script/update_unix_password.rb #{ftp_username} #{password} #{ftp_root}`
		
		logger.info result
	end
 
protected
		
	def make_activation_code
		self.deleted_at = nil
		
		self.activation_code = self.class.make_token
	end
end
