class FileResourcesController < ApplicationController
	
	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required, :except => [:index, :show]
	
	before_filter :has_permission?

	# GET /parent/{1,3,5-10}/file_resources


	def index
		# NOTE: something feels odd, like there is something we are overlooking, or something missing elsewhere
		
		parent_type, parent_id = getParentFromParamType

		# NOTE: there are two listings we may be interested in: what we have attached to something, what we've attached to anything
		
		if parent_type.blank?
			conditions = ['file_resource.user_id = ?', User.current.id]
		else	
			conditions = ['attachable_type = ? AND attachable_id = ?', parent_type, parent_id]
		end
		
		results = FileResource.with_conditions(conditions).paginate_with_metadata(params)

		@file_resources   = results[:collection]
				
		@metadata = results[:metadata]
		
		#@tags     = results[:tags]

		response_for( @file_resources, {:metadata => @metadata}.merge(params) )
	end


	def create
		@file_resource = FileResource.new(params[:file_resource])

		parent_type, parent_id = getParentFromParamType

		@file_resource.attachable_type = parent_type
		
		@file_resource.attachable_id = parent_id
		
		@file_resource.user_id = User.current.id

		@file_resource.save

		flash[:notice] = "File Resource Saved"

		respond_to do |format|
			format.html{
				redirect_back_or_default("/file_resources/#{@file_resource.id}")
			}
			
			format.xml{ render :xml => @file_resource }
			
			format.js{ render :json => @file_resource }
		end
		
		return
	end

	def destroy
		@file_resources = FileResource.in_set(params[:id])

		@file_resources.each do |file_resource|

			begin
				file_resource.destroy
			rescue Exception => e
				logger.error e
			end
		end

		respond_to do |format|
			format.html { redirect_to projects_url }

			format.xml { head :ok }

			format.js { head :ok }

			format.json { head :ok }
		end
	end

end