class ProjectsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required, :except => [:index, :show]
	
	before_filter :has_permission?
	
	# Example standard URIs
	# GET /projects
	# GET /projects.(html|xml|js|json)
	# GET /projects/{1}
	# GET /projects/{1-10, 15}
	# GET /projects.xml?public=1&page=1&order=name

	# Example Nested URIs
	# GET /users/1/projects
	# GET /users/{1,3,5-10}/projects
	 
	def index		
		# NOTE: see named_scope_helper.rb for paginate_by_user_with_metadata

		if !params[:public].blank? && params[:public]
			
			params[:type] = 'public'
		end
		
		case params[:type] || nil

		when 'public'

			results = Project.public().paginate_with_metadata(params)
			
		when 'watched'
			role = Role.find( :first, :conditions => ["name = 'watcher'"])


			#TODO - watched_by_user and readable_by used together cause colliding scopes in the SQL
			#but, users may have 'watched' projects that have subsequently become private, so we must enforce both
			#
			#Also, I am concerned that readable_by will not segregate users properly.

			results = Project.watched_by_user(User.current, role).paginate_with_metadata( params );
		
		else
			results = Project.paginate_by_user_with_metadata(User.current, params);				
			#results = Project.paginate_by_user_with_metadata(@user, params);
		end
 			
		@projects = results[:collection]
		
		@metadata = results[:metadata]
		
		@tags = results[:tags]
		
		# see application.rb for response_for_resource
		
		response_for( @projects, {:metadata => @metadata}.merge(params) )
	end
	

	# GET /projects/1
	# GET /projects/1.(html|xml|js|json)
	
	def show
		@project = Project.find(params[:id]) unless @project # loaded by check_permissions

		response_for( @project, params )
	end
	

	# GET /project/new
	# GET /project/new.(html|xml|js|json)
	
	def new
		@project = Project.new

		response_for( @project, params )
	end

	
	# GET /project/1/edit
	
	def edit
		@project = Project.find(params[:id]) unless @project # loaded by check_permissions
		
		respond_to do |format|
			format.html
			# is this method really needed in other formats??
		end
	end

	
	# POST /project
	# POST /project.(html|xml|js|json)
	
	def create
		@project = Project.new(params[:project])

		#We will add this to preserve a notion of uniqueness for ownership for deletion.  it is otherwise unused
		@project.user_id = User.current.id

		@project.attach_images(params[:images])
		
		respond_to do |format|
			
			if @project.save
				
				@project.add_users(User.current, 'administrator')

				#add other members
				params[:role_id] = 2
				@project.update_memberships( params )
				
				flash[:notice] = "Project \"#{@project.name}\" was successfully created."
				
				format.html { redirect_to(projects_path) }
				
				format.xml { render :xml => @project, :status => :created, :location => @project }
				
				format.js { render :json => @project, :status => :created, :location => @project }
				
				format.json { render :json => @project, :status => :created, :location => @project }
			else
				format.html { render :action => "new" }
				
				format.xml { render :xml => @project.errors, :status => :unprocessable_entity }
				
				format.js { render :json => @project.errors, :status => :unprocessable_entity }
				
				format.json { render :json => @project.errors, :status => :unprocessable_entity }
			end
		end
	end


	# PUT /project/1
	# PUT /project/1.xml
	# PUT /projects/{1,2,3}

	# Projects can also be updated in batch, that is, update params are applied to many projects simultaneously.
	
	def update
		
		# NOTE: get all projects referenced, we filter while updating considering permissions?
		
		@projects = Project.in_set(params[:id])
		
		# NOTE: here we update the project ASSETS if they are sent: sounds, logs, images, documents (soon), etc
		
		projects = @projects.find_all{ |project| project.assets_updatable_by?(User.current) }

		projects.each do |project|
			project.attach_images(params[:images])
			
			project.update_assets(params)

			project.update_attribute(:tag_list, params[:project][:tag_list] ) if params[:project] && params[:project][:tag_list]

			project.update_attribute(:tag_add, params[:tag_add]) if params[:tag_add]

			if params[:verb]
				if params[:verb] == 'watch'
					project.add_users(User.current, "watcher")
				elsif params[:verb] == 'remove'
					project.remove_user_role(User.current, "watcher")
				end
			end

		end
		
		# NOTE: update PROJECTS and MEMBERS
		
		projects = @projects.find_all{ |project| project.updatable_by?(User.current) }

		statusText = ""

		projects.each do |project|
			
			project.update_memberships(params)
			
			statusText = "Updated Project Associations."

			# TODO: the following will raise an exception if there are errors, beware and maybe catch this in the future
			
			# NOTE: this is the Rails built-in workhorse
			
			project.update_attributes!( params[:project] )
		end

		#TODO: refactor so a status can be returned

		# if :noview is not specified
		
		if (params[:noview] && params[:noview] == 'true')
			
			render :text => statusText
		else
			respond_to do |format|
				format.html {
					redirect_to(@projects.size > 1) ? "/projects/{#{@projects.map{|p| p.id}.join(',')}}" : project_url(@projects)
				}
				format.xml { render :xml => @projects, :status => :ok }

				format.js { render :json => @projects, :status => :ok }

				format.json { render :json => @projects, :status => :ok }
			end
		end
		
	end


	# DELETE /project/1
	# DELETE /project/1.xml
	# DELETE /projects/{1,2,3}

	# NOTE: projects may be deleted one at a time or in sets (though I'm not sure why you would want to delete a set of projects)

	def destroy
		@projects = Project.in_set(params[:id])
		
		@projects.each do |project|
			
			begin
				project.destroy
			rescue Exception => e
				logger.error e
			end
		end

		respond_to do |format|
			format.html { redirect_to projects_url }
			
			format.xml { head :ok }
			
			format.js { head :ok }
			
			format.json { head :ok }
		end
	end	


	def find_users
		#TODO this should be access controlled so it is not abused

		errors = []; found = []

		users = Project.parse_users(params[:add_users])

		users.each do |user|
			userObj = User.find_by_login_or_email(user)
			
			if (userObj.blank?)
				errors << "\t" + user + " \n"
			else
				found << "\t" + user + " \n"
			end
		end

#		textOut = "We could only find users for: \n" + found.to_s + "\nDo you want to invite: \n" + errors.to_s

		textOut = "Ok"

		if ( errors.length > 0 && found.length > 0 )
			textOut = "We could only find users for: \n" + found.to_s
		elsif ( found.length == 0 )
			textOut = "We could not find existing users for any of those email addresses."
		end

		render :text => textOut, :status => :ok
	end

end
