class JobsController < ApplicationController
	
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	
	def index
		user_id = (logged_in? && params[:public].blank?) ? User.current.id : 0

		parent_id = params[:parent] || nil
		
		if parent_id.nil?	
			condition = ["#{Job.table_name}.parent_id IS NULL"]
			
		elsif parent_id == 'any'
			# NOTE: this is typically used for the update section, we don't want parent selection or pagination.
			
			condition = ''; params[:per_page] = 1000;
 			
		else
			# TODO: we should check that this is an integer?
			
			condition = ["#{Job.table_name}.parent_id = ?", parent_id]
		end

		result = Job.with_conditions(condition).paginate_by_user_with_metadata(User.current, params)

		@jobs = result[:collection]

		@metadata = result[:metadata]
		
		response_for(@jobs, {:metadata => @metadata}.merge(params))
	end

	def create
		
		#--
		# get objects to operate on from request
		#--

		ids = []

		if params[:id]
			ids = params[:id].split(",")

		elsif params[:job_target]
			ids = params[:job_target].split(",")

		end

		#--
		# set default null task description
		#--

		# TODO: why do we have this at all?

        @name = "Null job"

        @description = "Null job, nothing to do."

        @notice = "Null job Created"

		#--
		# build job description based on job action
		#--

		# TODO: consider renaming job_action to something else 'command', 'cmd', ...

		@xml = Builder::XmlMarkup.new

        @xml.comment! "M2XML"

        case params[:job_action]

		when "import_file"

			@xml.job(:class=>'struct', :size=> '1 1') do
				@xml.type(params[:type], :class=>'char', :size=> '1 1')

				@xml.action( params[:job_action], :class=>'char', :size=> '1 1')

				@xml.id(ids.join(" "), :class=>'double', :size=>"1 #{ids.size}")
			end

			@notice = "A job has been created to import files: #{ids.join(', ')}"

			@atomic = true
	  
			handler_type = 'xbat'
	  
			handler_guid = ''


		when "create_sound"

			#--
			# setup
			#--

			@file_resources = FileResource.in_set( params[:job_target] )

			#--
			# build job description
			#--

			@name = "Create Sound"

			@description = "Adding selected sounds metadata to database"

			@xml.job(:class=>'struct', :size=> '1 1') do
				@xml.action( params[:job_action], :class=>'char', :size=> '1 1')

				@xml.files do
					@file_resources.each { |resource|
						@xml.location ( resource.location + resource.resource_file_name )
					}
				end
			end

			@notice = "A job has been created to create sounds."

			@atomic = true
	  
			handler_type = 'xbat'
	  
			handler_guid = 'afe5eb63-78ad-406e-a3dc-c99fd9fe896d'

		when "scan_sound"

			#--
			# setup
			#--

			# NOTE: get extension and corresponding preset object

			extension = Extension.find(params[:extension][:id])

			preset = ExtensionPreset.find(params[:preset][:id])

			# NOTE: translate local ids into portable ids

			port_extension_id = extension.guid

			port_preset_id = preset.guid

			port_sound_ids = Sound.get_portable_id_hash(ids).values

			#--
			# build job description
			#--

			@name = "Sound Scan"

			@description = "Scanning selected sounds with detector %s, preset %s" % [extension.name, preset.name]

			@xml.job(:class=>'struct', :size=> '1 1') do
				@xml.action( params[:job_action], :class=>'char', :size=> '1 1')

				@xml.detector( port_extension_id, :class=>'char', :size=> '1 1')

				@xml.preset( port_preset_id, :class=>'char', :size=>'1 1')

				@xml.sound( port_sound_ids.join(" "), :class=>'char', :size=>"1 #{port_sound_ids.size}")
			end

			@notice = "A task has been created to scan sounds: #{port_sound_ids.join(', ')}"

			@atomic = 0
	  
			handler_type = 'xbat'
	  
			handler_guid = ''

		end

		# TODO: something is fishy, 'created_at' is not automatically set

		@job = Job.create(
			:name => @name,
			:description => @description,
			:task => @xml.target!,
			:atomic => @atomic,
			:parent_id => 0,
			:priority => 1,
			:user_id => User.current.id,
			:handler_type => handler_type,
			:handler_guid => handler_guid,
			:created_at => Time.now
		)

		flash[:notice] = @notice
		
		redirect_to "/#{params[:redirect]}" || '/'
	end

	def update
		
	end
	
	def destroy
	
	end
	
end