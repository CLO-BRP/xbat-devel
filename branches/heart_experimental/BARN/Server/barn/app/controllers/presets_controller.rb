class PresetsController < ApplicationController
  def index
    @presets = ExtensionPreset.find(:all, :conditions => ["extension_id = ?", params[:extension_id]], :readonly => true )
    render :layout => false
  end
end