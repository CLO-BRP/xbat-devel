class SoundsController < ApplicationController	
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:index, :show]
	
	before_filter :has_permission?
		
	# GET /sounds
	# GET /sounds.(html|xml|js|json)
	# GET /sounds/{1}
	# GET /sounds/{1-10, 15}
	# GET /sounds.xml?public=1&page=1&order=name

	# GET /users/1/sounds
	# GET /projects/{1,3,5-10}/sounds
	
	def index
		# NOTE: we do not limit the sounds to user-owned, we may have project access

		user_id = (logged_in? && params[:public].blank?) ? User.current.id : 0

		if params[:include].blank?
			params[:include] = 'location'
		else
			params[:include] += ',location' unless params[:include].include?('location')
		end

		case params['partial']

			when 'widgets/map'

				@sounds = Sound.with_parent_in_set( params ).filtered_by( params ).order_by( params ).find(:all)

				response_for( @sounds, params )

			else
				# TODO: figure out how includes are handles in the named_scopes

				#includes = sanitize_associations(params[:include], Sound)

				#barn_logger("#{params.to_yaml}")

				results = if requested_through_parents?
					Sound.paginate_with_metadata( params )
				else
					Sound.paginate_by_user_with_metadata( User.current, params )
				end

				@sounds   = results[:collection]

				@metadata = results[:metadata]

				@tags     = results[:tags]


				#---------------------------

				# NOTE: this is used to support the 'Detect' dialog, it may not belong here

				@extensions = Extension.find(:all, :conditions => ["type = ?", ExtensionType::SOUND_DETECTOR], :order => "name" )

				@logs = Log.find(:all, :conditions => ["user_id = ?", user_id], :order => "name" )

				#@extensions = Extension.find(:all, :select => 'extension.*, count(extension_preset.id) as preset_count', :joins => 'left outer join extension_preset on extension_preset.parameter_id = extension.id', :group => 'extension.id', :conditions => "type = '" + ExtensionType::SOUND_DETECTOR + "'", :order => "name" )

				#---------------------------


				# see application.rb for response_for

				response_for( @sounds, {:metadata => @metadata}.merge(params) )
			end
	end
	

	# GET /sounds/:id
	
	def show
		
		if params[:include].blank?
			params[:include] = 'location'
		else
			params[:include] += ',location' unless params[:include].include?('location')
		end
		
		includes = sanitize_associations(params[:include] || [], Sound)
				
		@sound = Sound.find(params[:id], :include => includes)
				
		# TODO: why do we get this here?
		
		#@logs = @sound.logs.paginate(:page => 1) # revisit this

		# NOTE: this may not be where this belongs, further we don't want the full location object!
		
		# NOTE: it seems that we are not using the builder.xml approach!
 		
		response_for( @sound, params )
	end


	# GET /sound/new
	
	def new
		@sound = Sound.new

		response_for( @sound, params )
	end
	

	# POST /sound
	
	#	 curl 'http://localhost:3000/sounds' -X POST -H 'Content-Type: application/xml'	-d '
	# 
	#	 <sound>
	#		 <name>A test sound</name>
	#		 <channels>2</channels>
	#		 <samplerate>22050.00</samplerate>
	#		 <duration>12.555</duration>
	#		 <samples>4512679</samples>
	#		 <!-- The user should not be sent with the POST, it should be gathered from the session -->
	#		 <user_id>1</user_id>
	#	 </sound>
	#	 '

	def create
		@sound = Sound.new(params[:sound])
		
		@sound.user = User.current
		
		# TODO:
		# 1. create a content hash
		# 2. check to see if the sound already exists - if so, only create a UserSound relation.
		
		respond_to do |format|
			if @sound.save
				format.html { redirect_to sound_path(@sound) }
				
				format.xml	{ render :template => 'sounds/show.xml.builder', :status => :created }
				
				format.any(:js, :json) { render :json => @sound, :status => :created, :location => @sound }
			else
				format.html {
					flash[:error]	= "Something went wrong."
					
					render :action => 'new'
				}
				
				format.xml	{ render :xml => @sound.errors, :status => :unprocessable_entity }
				
				format.any(:js, :json)	 { render :json => @sound.errors, :status => :unprocessable_entity }
			end
		end
	end


	# GET /sound/:id/edit
	
	def edit
		@sound = Sound.find(params[:id])

		respond_to do |format|
			format.html
			# is this method really needed in other formats??
		end
	end


	# PUT /sound/:id
	# PUT /sound/{:id}
		
	def update
		#--
		# setup
		#--
		
		success = false;
		
		location = params[:sound][:location]
		
		has_location = !location.nil? && location[:lat].length > 0 && location[:lng].length > 0

		if !has_location
			params[:sound].delete(:location)
		end
		
		#barn_logger(has_location.to_yaml)
		#
		#barn_logger(params[:sound].to_yaml)
		
		#--
		# get and update sounds
		#--
		
		@sounds = Sound.in_set(params[:id])
						
		@sounds.each do |sound|

			# NOTE: here we create the associated location object to replace the form hash
			
			if has_location
				
				params[:sound][:location] = Location.new(
					:locatable_id => sound.id,
					:locatable_type => 'Sound',
					:lat => location[:lat],
					:lng => location[:lng]
				)
			end

			# NOTE: the params[:sound] hash contains information we may want to use in flash
			
			#barn_logger(params.to_yaml)
			
			success = sound.update_attributes(params[:sound])
		end
		
		#--
		# respond to request
		#--
		
		if success
			flash[:notice] = "Your sound was successfully updated."

			if ( @sounds.length == 1 )
				redirect_to :action => 'show', :id => params[:id]
			else
				render :json => @sounds.map{|s| s.id}, :status => :ok
			end
			
			# TODO: can we use 'response_for'? probably not. what would the function have to do?
			
			#response_for(@sounds, params)
		else
			respond_to do |format|
		
				format.html { 
					flash[:error] = "The was a problem saving your record"
					
					render :action => 'edit' # presumably we came from the edit action
				} 
				
				format.xml { render :xml => @sounds.map{|s| s.errors}, :status => :unprocessable_entity }
				
				format.js { render :json => @sounds.map{|s| s.errors}, :status => :unprocessable_entity }
				
				format.json { render :json => @sounds.map{|s| s.errors}, :status => :unprocessable_entity }
			end
		end
		
	end # update

	# DELETE /sound/:id
	# DELETE /sound/{:id}
	
	def destroy
		# NOTE: we only destroy the user sound association not the sound resource
		
		@sounds = Sound.in_set(params[:id])
				
		@sounds.each do |sound|
			sound.sound_users.each do |relation|
				relation.destroy if relation.user_id == User.current.id
			end
		end

		respond_to do |format|
			format.html { redirect_to sounds_url }
			
			format.xml { head :ok }
			
			format.any(:js, :json)	{ head :ok }
		end
	end
		
end