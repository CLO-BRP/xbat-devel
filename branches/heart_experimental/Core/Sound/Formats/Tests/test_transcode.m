function test_transcode(in, formats)

% test_transcode - for file to various formats
% --------------------------------------------
%
% test_transcode(in, formats)
%
% Input:
% ------
%  in - file to transcode
%  formats - to convert to
%
% Output:
% -------
%  result - of operation

% TODO: extend this to consdider many channel recordings

%--
% handle input
%--

if ~nargin
	in = which('BlackCappedVireo.flac'); 
end

if nargin < 2
	formats = get_writeable_formats;
end

%--
% read input file and transcode to various output formats
%--

[X, rate] = sound_file_read(in);

base = fullfile(fileparts(mfilename('fullpath')), 'test');

for k = 1:numel(formats)
	
	out = [base, '.', formats(k).ext{1}]; 
	
	if exist(out, 'file')
		delete(out);
	end
	
	sound_file_write(out, X, rate);
	
	Y = sound_file_read(out);
	
	try
		range = fast_min_max(X - Y);
		
		disp(' ');
		disp(['''', formats(k).name, ''' reconstruction error ', mat2str(range)]);
	catch
		% NOTE: this test handles the exception type
		
		if ~isequal(size(X), size(Y))
			say = ['Restored ''', formats(k).name, ''' sample array is not same size as input.'];
			
			disp(' ');
			disp(upper(say));
		else
			say = ['Failed to campare to ''', formats(k).name, ''' restored samples.'];
			
			nice_catch(lasterror, upper(say));
		end

	end	
end

disp(' ');

