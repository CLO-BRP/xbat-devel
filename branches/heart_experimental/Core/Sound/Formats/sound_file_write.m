function out = sound_file_write(file, X, r, opt)

% sound_file_write - write samples to sound file
% ----------------------------------------------
%
%  opt = sound_file_write(file)
%
% info = sound_file_write(file, X, r, opt)
%
% Input:
% ------
%  file - file location
%  X - samples to write to file
%  r - sample rate
%  opt - format specific write options
%
% Output:
% -------
%  opt - format specific write options
%  info - for file
% 
% See also: sound_file_read, sound_file_encode

%------------------------------
% HANDLE INPUT
%------------------------------

% TODO: check for real samples matrix, and positive integer samplerate

%--
% try to get file format
%--

% TODO: implement all available sound file formats as extensions, notably MP3

[format, ext] = get_file_format(file);

%--
% check for read only format
%--

if isempty(format.write)
	disp(['WARNING: ''', format.name, ''' is a read-only format.']); out = 0; return;
end

%--
% set default options if needed
%--

% TODO: add append option field

if nargin < 4	
	opt = struct;
	
	if isfield(format, 'parameter') && ~isempty(format.parameter.create)
		
		% NOTE: currently all we need for context is the file
		
		context.file = file;
		
		try
			opt = format.parameter.create(context);
		catch
			extension_warning(ext, 'Failed to create format parameters.', lasterror);
		end
	end
	
end

% NOTE: return format specific write options if needed

if nargin == 1
	out = opt; return;
end

%------------------------------
% WRITE SAMPLES TO FILE
%------------------------------

% TODO: collect information on the write performance of the various formats

%--
% write file using handler
%--

% NOTE: the out flag here simply indicates success in writing

out = format.write(file, X, r, opt);

if isstruct(out)
    out = out.success;
end

%--
% get number of bytes written to file from system
%--

% NOTE: convert out from success flag to number of bytes written

if nargout && out

	% NOTE: block on file existence to get bytes
	
	while ~exist(file, 'file')
		pause(0.025);
	end
	
	out = sound_file_info(file); % out = out.bytes;
end