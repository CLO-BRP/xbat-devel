function [bytes, info] = ogg_cache_size

% ogg_cache_size - get bytes used by ogg cache files
% --------------------------------------------------
%
% [bytes, info] = ogg_cache_size
%
% Output:
% -------
%  bytes - used by ogg cache files
%  info - cache files info

info = dir(ogg_cache_root); info(1:2) = []; bytes = sum([info.bytes]);