function ogg_cache_reduce(target, trigger)

% ogg_cache_reduce - reduce the size of the ogg cache
% ---------------------------------------------------
%
% ogg_cache_reduce(target, trigger)
%
% Input:
% ------
%  target - desired size
%  trigger - size that triggers action (def: target)

%--
% handle input
%--

if ~nargin || isempty(target)
	target = 10^12;
end

% NOTE: trigger for double the cache size by default

if nargin < 2
	trigger = 2 * target; 
end

if trigger < target
	error('Trigger size must be larger or equal to target size.');
end

%--
% consider cache reduction
%--

[bytes, info] = ogg_cache_size;

if bytes > trigger
	
	%--
	% get bytes and modification date, sort in descending modification date
	%--
	
	byte = [info.bytes]; modified = [info.datenum];
	
	[modified, ix] = sort(modified, 'descend'); byte = byte(ix);
	
	%--
	% select older files for deletion and delete
	%--
	
	info = info(cumsum(byte) > target); 
	
	files = strcat(ogg_cache_root, filesep, {info.name});
	
	iterate(@delete, files);
	
end
