function out = write_flac(f, X, r, opt)

% write_flac - write samples to sound file
% ----------------------------------------
%
%  opt = write_flac(f)
%
% flag = write_flac(f, X, r, opt)
%
% Input:
% ------
%  f - file location
%  X - samples to write to file
%  r - sample rate
%  opt - format specific write options
%
% Output:
% -------
%  opt - format specific write options
%  flag - success flag

%--
% set default encoding options
%--

% NOTE: we get the options as if coming from WAV

if (nargin < 4) || isempty(opt)	
	opt = encode_flac('temp.wav');
end 

%--
% return default options
%--
	
if nargin == 1
	out = opt; return;
end

%--
% write flac using libsndfile
%--

% TODO: use encoding options? it is not clear that this is supported

out = write_libsndfile(f, X, r);
