function [installed, tool] = install_flac

% install_flac - command-line tools
% ---------------------------------
%
% [installed, tool] = install_flac
%
% Output:
% -------
%  installed - indicator
%  tool - info
% 
% See also: install_lame

%--
% handle simpler non-PC check first
%--

if ~ispc
	tool = get_tool('flac');
	
	installed = ~isempty(tool);
	
	if ~installed
		disp('To install FLAC on a non-PC system you should use your package manager.');
	end
	
	return;
end

%--
% PC check and possible install
%--

tool = get_tool('flac.exe');

installed = ~isempty(tool);

if installed
    return;
end

%--
% download and install
%--

url = 'http://xbat.org/downloads/installers/flac-1.2.1-all-win.zip';

installed = install_tool('FLAC', url, 'urlwrite');

if nargout > 1
    tool = get_tool('flac.exe');
end
