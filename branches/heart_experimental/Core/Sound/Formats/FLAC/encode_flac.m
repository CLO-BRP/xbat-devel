function out = encode_flac(f1, f2, opt)

% encode_flac - encode a file to flac
% -----------------------------------
%
% opt = encode_flac(f1)
%
% com = encode_flac(f1, f2, opt)
%
% Input:
% ------
%  f1 - input file
%  f2 - output file
%  opt - encoding options
%
% Output:
% -------
%  opt - encoding options, -1 if no direct encoding
%  com - command to execute to perform encoding

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

%--
% set default encoding options
%--

if nargin < 3 || isempty(opt)

	opt.compression_level = 5; % NOTE: this is a summary flag for a complex configuration
end

%--
% possibly translate string compression level
%--

% NOTE: this code is typically not used, it serves to document level values

if ischar(opt.compression_level)
	
	switch opt.compression_level
	
		case 'fastest'
			opt.compression_level = 0;
		
		case 'default'
			opt.compression_level = 5;
		
		case 'best'
			opt.compression_level = 8;
			
		otherwise
			disp(' ');
			error(['Unrecognized character compression level ''', opt.compression_level, '''.']);	
	end
	
end

%--
% check for directly handled input format, otherwise indicate in options
%--

ext1 = get_formats_ext(get_file_format('temp.wav'));

ext2 = get_formats_ext(get_file_format('temp.aif'));

exts = {ext1{:}, ext2{:}};

[ignore,ext] = file_ext(f1);

if isempty(find(strcmpi(ext,exts), 1))
	opt = -1;
end
	
%--
% return options if needed
%--

if nargin < 3
	out = opt; return;
end

%--
% indicate that encoding is not supported
%--

if isequal(opt, -1)
	disp(' ');
	error(['Direct FLAC encoding from ', upper(ext), ' is not supported']);
end

%----------------------------------------------------
% BUILD COMMAND STRING
%----------------------------------------------------

%--
% persist location of command-line helper
%--

persistent FLAC;

if isempty(FLAC)
	FLAC = get_tool('flac.exe');
end

%--
% build full command string
%--

% NOTE: read on the implications of the 'lax' option

out = [ ...
	'"', FLAC.file, '" --force --lax --totally-silent --compression-level-', int2str(opt.compression_level), ...
	' --output-name="', f2, '" "', f1, '"' ...
];











