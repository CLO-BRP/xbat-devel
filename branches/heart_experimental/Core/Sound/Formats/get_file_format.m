function [format, base] = get_file_format(file) 

% get_file_format - get format from file
% --------------------------------------
%
% [format, ext] = get_file_format(file)
%
% Input:
% ------
%  file - filename
%
% Output:
% -------
%  format - old file format handler
%  ext - file format extension

%--
% try to get format handler using file extension
%--

[ignore, ext] = file_ext(file);

% TODO: make sure format extensions are complete! when this is the case we
% will only use these

% NOTE: generally speaking the old format representation is not sufficient
% for some purposes such as encoding 

base = get_extensions('sound_file_format', 'ext', ext);

if isempty(base)
	format = get_formats(0, 'ext', ext);
else
    format = base.fun; format.ext = base.ext;    
end

%--
% display warning when format was not found
%--

% NOTE: this is the goal of this function, to handle failure, develop other ways

if isempty(format)
	error(['Unable to get format handler for sound file with extension ''', ext, '''.']);
end
