function [file, root] = uiget_sound_files(in)

% uiget_sound_files - get location of sound files interactively
% -------------------------------------------------------------
%
% [file, root] = uiget_sound_files(type)
%
%              = uiget_sound_files(sound)
%
% Input:
% ------
%  type - of sound
%  sound - structure
%
% Output:
% -------
%  file - name
%  root - path for file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%--
% handle input
%--

if nargin < 1 || isempty(in)
	in = 'file';
end

%--
% sound type input
%--

if ischar(in)
	
	% NOTE: check and normalize input type and set initial directory
	
	[ignore,type] = is_sound_type(in);
	
	if isempty(type)
		error(['Unrecognized sound type ''', in, '''.']);
	end

	sound_dir = get_env('xbat_path_sound');
	
%--
% sound input
%--

else
	% NOTE: get type from sound and set initial directory
	
	type = in.type; sound_dir = in.path;
	
end

%-----------------------------------------------
% INITIALIZE
%-----------------------------------------------

%--
% set initial directory
%--

current_dir = pwd;

if isempty(sound_dir)
	
	set_env('xbat_path_sound',current_dir);
	
else	
	try
		cd(sound_dir);
	catch
		set_env('xbat_path_sound',current_dir); % NOTE: we know this exists
	end
end

%-----------------------------------------------
% GET FILE LOCATION
%-----------------------------------------------

%--
% get dialog file filter specification
%--

% filter = get_formats_filter;

%--
% get file locations depending on type of sound
%--

switch lower(type)

	case 'file'
		
		% NOTE: 'multiselect' allows creation of many file sounds
		
		% NOTE: there is a known matlab bug when selecting many files
		
        [file, root] = uigetfile('*.*', 'Select Sound File(s):', 'MultiSelect', 'on');
		
		if isnumeric(file) && root == 0; % NOTE: this condition means 'return on cancel'
			file = []; root = []; 
			
			return;
		end

	case {'file stream', 'stream'}
		
		% NOTE: this will allow to select a member of the file stream
		
		root = uigetdir(pwd, 'Select folder containing sound file stream');
		
		if isnumeric(root) && root == 0
			file = []; root = []; 
			
			return;
		end
		
		file = [];
end
		
%--
% return to original directory
%--

cd(current_dir);

%--
% update sound path environment variable
%--

if ischar(root)
	set_env('xbat_path_sound',root);
end

%--
% format path for single trailing separator
%--

while root(end) == filesep
	root(end) = [];
end

root(end + 1) = filesep;

%--
% output as single output
%--

if iscell(file)
	file = file(:);
end

if nargout < 2
	
	% NOTE: single files output a string multiple files a cell 
	
	if iscell(file)
		full = strcat(root, file);
	else
		full = [root, file];
	end
	
	file = full;
end
