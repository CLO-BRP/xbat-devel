function [result, serial] = stack_files(prefix, format, duration, serial, varargin)

% stack_files - into multi-channel file
% -------------------------------------
%
% [result, serial] = stack_files(prefix, format, duration, serial, file_1, ... , file_n)
%
% Input:
% ------
%  prefix - for output file name
%  format - of output
%  duration - of output files
%  serial - number to begin current file sequence
%  file_k - input file
%
% Output:
% -------
%  result - info for output files
%  serial - current serial 

% TODO: allow for dialog to get files to stack

%--
% handle input
%--

if isempty(duration)
	duration = 300;
end

if isempty(format)
	format = 'wav';
end 

if isempty(serial)
	serial = 1;
end 

file = varargin; 

for k = 1:numel(file)
	info(k) = sound_file_info(file{k});
end

if numel(unique([info.samplerate])) > 1
	error('Files have different samplerates.');
end

%--
% create output file
%--

rate = info(1).samplerate;

samples = min([info.samples]);

channels = sum([info.channels]);

start = 0; block = rate * min(samples / rate, duration);

while start < samples
	
	%--
	% read input samples
	%--
	
	X = []; count = min(block, samples - start);
	
	for k = 1:numel(file)
		X = [X, sound_file_read(file{k}, start, count)];
	end 
	
	%--
	% write output file and get info
	%--
	
	current = [prefix, '-', cellfree(int_to_str(serial, 1000)), '.', format];
	
	disp(['writing ''', current, ''' ...']);
	
	sound_file_write(current, X, rate);

	if ~exist('result', 'var')
		result = sound_file_info(current);
	else
		result(end + 1) = sound_file_info(current);
	end

	start = start + block; serial = serial + 1;
	
end

