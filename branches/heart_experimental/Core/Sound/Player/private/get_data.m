function [X, player] = get_data(player)

%--
% get sound, starting index, duration, and channels from player
%--

sound = player.sound;

ix = player.bix; n = min(player.buflen, (player.ix + player.n) - player.bix);

ch = player.ch;

%--
% get samples from source
%--
		
% TODO: figure out what this 'sound.data' clause is, it may be for 'variable' type sounds

if isfield(sound, 'data')
	X = sound.data;
else
	X = sound_read(sound, 'samples', ix, n, ch);
end

%--
% filter samples
%--

if ~isempty(player.filter)
	
% 	in = max(abs(X(:)));
	
	for k = 1:numel(player.filter)
		X = apply_signal_filter(X, player.filter(k).ext, player.filter(k).context);
	end
	
% 	out = max(abs(X(:)));
	
end

%--
% resample for player
%--

X = player_resample(X, player);
