function playing = is_playing

%--
% access global players
%--

global BUFFERED_PLAYER_1 BUFFERED_PLAYER_2;

%--
% check that a player is playing
%--

playing = 0;

if ...
	(~isempty(BUFFERED_PLAYER_1) && isplaying(BUFFERED_PLAYER_1)) || ...
	(~isempty(BUFFERED_PLAYER_2) && isplaying(BUFFERED_PLAYER_2)) ...
	
	playing = 1;
	
end
