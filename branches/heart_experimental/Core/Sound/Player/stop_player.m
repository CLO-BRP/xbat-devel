function value = stop_player

% stop_player - stop current player
% ---------------------------------
% 
% value = stop_player
% 
% Output:
% -------
%  value - stop indicator

% NOTE: eventually there may be multiple players, one per parent

% NOTE: we find the player timer and stop it, this will take care of deleting it

player = timerfind('name', 'PLAY_TIMER');

value = ~isempty(player);

if value
	stop(player);
end