function [X, rate] = resample_for_play(X, rate, lowest)

% resample_for_play - considering initial rate and reliable playback rates
% ------------------------------------------------------------------------
%
% [X, rate] = resample_for_play(X, rate, lowest)
%
% Input:
% ------
%  X - input samples
%  rate - of input
%  lowest - resample rate (def: [], no lowest rate requirement)
%
% Output:
% -------
%  X - resampled input
%  rate - of output
%
% See also: buffered_player, get_player_resample_rate

% NOTE: this fragment is taken from 'buffered_player'

%--
% handle input
%--

if nargin < 3
	lowest = [];
end

%--
% setup and possibly resample
%--

[rate, p, q] = get_player_resample_rate(rate, lowest);

if isempty(p)
	return;
end

X = resample(X, p, q); 