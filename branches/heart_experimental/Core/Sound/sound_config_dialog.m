function out = sound_config_dialog(sound)

% sound_config_dialog - configurable sound options dialog
% -----------------------------------------------------
%
% out = sound_config_dialog(sound)
%
% Input:
% ------
%  sound - sound to edit
%
% Output:
% -------
%  out - dialog output

%---------------------
% HANDLE INPUT
%---------------------

%--
% try to use a single selected sound
%--

if ~nargin
	sound = get_selected_sound;
end

if numel(sound) ~= 1
	return;
end

%---------------------
% CREATE DIALOG
%---------------------

%--
% create header and tabs
%--

control(1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.1, ...
	'min', 1, ...
	'string', ['Sound (', sound_name(sound), ')'] ...
);

tabs = {'Source', 'Tags'};

% NOTE: the format tab is for format specific configuration options

if 1 % has_controls(ext)
	tabs{end + 1} = 'Format';
end

control(end + 1) = control_create( ...
	'style', 'tabs', ...
	'lines', 1.25, ...
	'tab', tabs ...
);

%--
% source tab controls
%--

% NOTE: these are options stored in the sound 'output' field

types = upper(get_sound_output_types);

value = find(strcmpi(types, sound.output.class));

if isempty(value)
	value = 1;
end

control(end + 1) = control_create( ...
	'name', 'class', ...
	'alias','datatype', ...
	'style', 'popup', ...
	'tab', tabs{1}, ...
	'space', 1.5, ...
	'string', types, ...
	'value', value ...
);	

if ~isempty(sound.output.rate)
	value = sound.output.rate; state = 1; status = '__ENABLE__';
else
	value = sound.samplerate; state = 0; status = '__DISABLE__';
end

control(end + 1) = control_create( ...
    'name', 'samplerate', ...
    'style', 'slider', ...
	'tab', tabs{1}, ...
	'space', 1, ...
    'min', 100, ...
    'max', 96000, ...
    'value', value, ...
	'initialstate', status ...
);

control(end + 1) = control_create( ...
	'name', 'resample', ...
	'style', 'checkbox', ...
	'tab', tabs{1}, ...
	'value', state ...
);

%--
% tags tab controls
%--

str = tags_to_str(get_tags(sound));

control(end + 1) = control_create( ...
	'name', 'tags', ...
	'style', 'edit', ...
	'string', str, ...
	'tab', tabs{2}, ...
	'space', 0.75, ...
	'color', ones(1,3) ...
);

control(end + 1) = control_create( ...
	'name', 'notes', ...
	'style', 'edit', ...
	'string', sound.notes, ...
	'tab', tabs{2}, ...
	'color', ones(1,3), ...
	'lines', 4, ...
	'space', 1.5 ...
);

%--
% configure and present dialog
%--

opt = dialog_group;

opt.width = 13;

opt.header_color = get_extension_color('root');

out = dialog_group('Edit Sound ...', control, opt, {@dialog_callback, sound});


%---------------------
% DIALOG_CALLBACK
%---------------------

function result = dialog_callback(obj, eventdata, sound)

result = [];

callback = get_callback_context(obj, eventdata); par = callback.pal;; 

%--
% perform control-specific action
%--

switch control.name
	
	case 'resample'
		
		values = get_control_values(par.handle);
		
		set_control(par.handle, 'samplerate', 'enable', values.resample);
		
		if ~values.resample
			set_control(par.handle, 'samplerate', 'value', sound.samplerate);
		end
	
end
