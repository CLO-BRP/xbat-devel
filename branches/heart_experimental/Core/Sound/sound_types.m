function types = sound_types

% sound_types - list of sound types
% ---------------------------------
%
% types = sound_types
%
% Output:
% -------
%  types - sound type strings

types = { ...
	'file', ...
	'file stream', ...
	'playlist', ...
	'variable' ...
};