function table = trim_schedule(table)

% trim_schedule - remove time stamps that do not result in gaps
% -------------------------------------------------------------
%
% input:
% ------
%  table - time stamp table
%
% Output:
% -------
%  table - edited time stamp table

% NOTE: if the time differences are equal in both domains, then there are
% no gaps

if isempty(table)
	return;
end

ix = find(diff(table(:, 1)) >= diff(table(:, 2)));

table(ix + 1,:) = [];
