function count = get_file_count(sound)

% get_file_count - get number of files in sound
% ---------------------------------------------
%
% count = get_file_count(sound)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  count - number of files in sound

%--
% handle multiple sounds
%--

if numel(sound) > 1
	count = iterate(mfilename, sound); return;
end

%--
% get file count
%--

if ischar(sound.file)
	count = 1;
else
	count = numel(sound.file); % NOTE: this field may contain names or indices, the size is correct
end
