function [root, exists] = sound_root(varargin)

if nargout < 2
    root = library_sound_root(varargin{:});
else
    [root, exists] = library_sound_root(varargin{:});
end