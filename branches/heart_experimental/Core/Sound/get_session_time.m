function time = get_session_time(sound, time)

% get_session_time - get expanded time from collapsed time
% --------------------------------------------------------
%
% [time, date] = get_session_time(time, sound)
%
% Inputs:
% -------
% time - vector of times
% sound - sound structure
%
% Outputs:
% --------
% time - vector of times

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 819 $
% $Date: 2005-03-27 15:06:08 -0500 (Sun, 27 Mar 2005) $
%--------------------------------

collapsed = get_sound_sessions(sound, 1);

expanded = get_sound_sessions(sound, 0);

for k = 1:numel(time)
	
	session = find([collapsed.start] <= time(k) & [collapsed.end] > time(k));
	
	if isempty(session)
		session = length(expanded);
	end

	time(k) = time(k) + expanded(session).start - collapsed(session).start;
	
end
