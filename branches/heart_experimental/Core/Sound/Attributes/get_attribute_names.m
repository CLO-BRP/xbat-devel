function names = get_attribute_names

% get_attribute_names - get all attribute names available
% -------------------------------------------------------
%
% names = get_attribute_names
%
% Output:
% -------
%  names - attribute names

ext = get_extensions('sound_attribute');

if isempty(ext)
	names = {};
else
	names = {ext.name}';
end