function [file, exists] = get_attribute_file(context, name)

% get_attribute_file - get file for a sound and attribute type
% ------------------------------------------------------------
%
% [file, exists] = get_attribute_file(context, name)
%
% Input:
% ------
%  context - context
%  name - attribute name
%
% Output:
% -------
%  file - file
%  exists - indicator
%
% See also: get_attributes_root

%--
% build canonical filename and ensure that attributes directory exists
%--
		
root = get_attributes_root(context);

%--
% ensure that root exists
%--

if ~exist_dir(root)
	
	result = mkdir(root);
	
	if ~result
		error('Can''t create attributes root directory.');
	end
end

file = fullfile(root, [name, attribute_file_ext]);

%--
% check file exists
%--

% NOTE: this should work properly for full filenames, 'exist' is a bit quirky

if nargout > 1
	exists = exist(file, 'file');
end
