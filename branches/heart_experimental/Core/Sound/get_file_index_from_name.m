function ix = get_file_index_from_name(sound, file)

% get_file_index_from_name - get sound file index in sound from filename
% ----------------------------------------------------------------------
%
% ix = get_file_index_from_name(sound, file)
%
% Input:
% ------
%  sound - sound
%  file - file
% 
% Output:
% -------
%  ix - file index in sound

%--
% handle multiple files
%--

% NOTE: the input '2' means that the items to iterate over are the second argument 'file'

if iscell(file) && numel(file) > 1
	
	ix = iteraten(mfilename, 2, sound, file); return;

end

%--
% get file index from name
%--

% NOTE: first we consider matching the input to whatever the sound stores as files

ix = find(strcmp(file, sound.file));

if ~isempty(ix)
	return;
end

% NOTE: then we try getting this through the file name only
		
% NOTE: we separate the directory part of name from name

[p1, p2, p3] = fileparts(file); name = [p2, p3];

if sound.path(end) == filesep
	sound.path(end) = [];
end

% NOTE: returning a nan plays better with the iterate function

if ~strcmp(sound.path, p1)
	ix = nan; return
end

% NOTE: in these cases the sound file field typically contains the name of the file not the path

ix = find(strcmp(name, sound.file));
		