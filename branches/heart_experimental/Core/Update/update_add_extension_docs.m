ext = get_extensions;

for k = 1:numel(ext)
	% NOTE: this is DOCS source directory, not where the output is found
	
	docs = init_docs(extension_root(ext(k), 'Docs'));
	
	cd(fileparts(docs)); svn('add', 'DOCS');
end