function root = server_users_root(server)

% server_users_root - where users put data files
% ----------------------------------------------
%
% root = server_users_root(server)
%
% Input:
% ------
%  server - description
% 
% Output:
% -------
%  root - of users files

%--
% handle input
%--

if ~nargin
	[ignore, server] = local_barn;
end

%--
% get files root by inspecting server description
%--

if ~isempty(server.users)
	
	root = server.users;
	
elseif ~isempty(server.root)
	
	root = fullfile(server.root, 'users');
	
else
	root = '';
end