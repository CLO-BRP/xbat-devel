function server = rails_server(varargin)

% rails_server - description structure
% ------------------------------------
%
% server = rails_server(field, value, ... )
%
% Input:
% ------
%  field - name
%  value - of field
%
% Output:
% -------
%  server - description

%--
% initialize server description
%--

server.host = 'localhost';

server.port = 3000;

server.root = rails_root;

% NOTE: these are the locations for output files and user files

server.files = '';

server.users = '';

%--
% update description from input
%--

server = parse_inputs(server, varargin{:});
