function [root, exists] = get_barn_extension_root(ext, server)

% get_barn_extension_root - directory in server
% ---------------------------------------------
%
% [root, exists] = get_barn_extension_root(ext, server)
%
% Input:
% ------
%  ext - extension
%  server - defaults to local_barn server
%
% Output:
% -------
%  root - directory
%  exists - indicator

%--
% handle input 
%--

if nargin < 2
	[ignore, server] = local_barn;
end

if isempty(server)
	error('Unable to determine server.');
end

if (isstruct(ext) || iscell(ext)) && numel(ext) > 1
	[root, exists] = iterate(mfilename, ext, server); return;
end

%--
% compute barn extension type root
%--

if isstruct(ext)
	ext = ext.type;
end

% NOTE: this root depends only on the type, it is a mix of the Rails and XBAT directory conventions

% root = strcat(server.root, strrep(['/app/models/extensions/', lower(type_to_dir(ext))], '/', filesep));

root = strcat(server.root, strrep('/app/models/extensions/', '/', filesep));

if nargout > 1
	exists = exist(root, 'dir') > 0;
end