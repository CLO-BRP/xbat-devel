function root = rails_root

% rails_root - root of BARN rails server 
% --------------------------------------
%
% root = rails_root
%
% Output:
% -------
%  root - directory

[ignore, server] = local_barn;

if isempty(server)
	root = fullfile(barn_root, 'Server', 'barn');
else
	root = server.root;
end