function [load_paths, created] = generate_load_paths(server)

%--
% handle input
%--

if ~nargin
	[ignore, server] = local_barn;
end

if isempty(server) 
	error('Local barn server is not set.');
end

%--
% generate extension root path descriptions
%--

types = get_barn_extension_types; 

% NOTE: this is the part appended to 'environment.rb' so that Rails can find the extension models

load_paths = strcat('#{RAILS_ROOT}/app/models/extensions/', iterate(@strrep, lower(type_to_dir(types)), filesep, '/'));

%--
% make sure that these exist in the filesystem
%--

for k = 1:numel(types)
	[ignore, created(k)] = create_dir(get_barn_extension_root(types{k})); 
end

%--
% display results
%--

if ~nargout
	disp(' '); disp(server.root); disp(' ');
	
	for k = 1:numel(load_paths)
		if ~created(k)
			disp(load_paths{k});
		else
			disp([load_paths{k}, ' (created)']);
		end
	end
	
	disp(' '); clear load_paths;
end