function delete_barn_log(store, log, trace)

% delete_barn_log - along with various associations
% -------------------------------------------------
%
% delete_barn_log(store, log, trace) 
%
% Input:
% ------
%  store - database
%  log - to remove, or identifier
%  trace - flag

%--
% handle input
%--

if nargin < 3
	trace = true; % NOTE: this is while we develop, or perhaps we keep this for this relatively rare operation
end

initial = query_trace; query_trace(trace);

if isstruct(log) && isfield(log, 'store')
	id = log.store.id; 
else
	id = log;
end

%--
% delete log related objects
%--

% EVENT TAGGINGS

% NOTE: this is where we delete any event tags, we also need a general tagging cleanup function

% TODO: we need code like this in the 'delete_database_objects' function, possibly factored into its own function

sub = ['SELECT id FROM event WHERE log_id = ', int2str(id)];

sql = ['DELETE FROM tagging WHERE taggable.type = ''Event'' AND taggable.id IN (', sub, ')'];

query(store, sql);

% EVENTS

sql = ['DELETE FROM event WHERE log_id = ', int2str(id)];

query(store, sql);

% LOG 

sql = ['DELETE FROM log WHERE id = ', int2str(id)];

query(store, sql); 

query_trace(initial);