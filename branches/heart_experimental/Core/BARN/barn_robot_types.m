function robots = barn_robot_types

% barn_robot_types - extensions types that may become users
% ---------------------------------------------------------
%
% types = barn_robot_types
%
% Output:
% -------
%  types - of robots

types = get_barn_extension_types; 

robots = types(~string_contains(types, {'annotation', 'attribute', 'feature'}));