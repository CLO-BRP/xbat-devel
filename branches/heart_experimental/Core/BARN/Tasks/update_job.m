function job = update_job(store, job, varargin)

% update_job - in store through field value pairs
% ------------------------------------------------
%
% job = update_job(store, job, field, value, ... )
%
% Input:
% ------
%  store - for jobs
%  job - to update
%  field, value - pairs for update
%
% Output:
% -------
%  job - updated
%
% See also: create_job, create_child_job

% TODO: we should remove the daemon once we've completed the job? probably not, review the 'pick_job' query

%--
% handle input
%--

[field, value] = get_field_value(varargin, {'progress', 'report', 'completed_at'});

field = [field, 'last_update']; value = [value, datestr(now, 31)];

%--
% update input job
%--

for k = 1:numel(field)
	% NOTE: here we build the 'set' part of the update
	
	switch class(value{k})
		
		case 'char'
			value{k} = ['''', value{k}, ''''];
			
		otherwise
			value{k} = num2str(value{k});
	end

	% NOTE: we over-write the field, it's economical and reads well below
	
	field{k} = [field{k}, ' = ', value{k}];
end

% NOTE: this is where we actually update and possibly get the updated job

sql = ['UPDATE job SET ', str_implode(field, ', '), ' WHERE daemon_transaction = ''', job.daemon_transaction, ''''];

query(store, sql);

if nargout
	[status, job] = query(store, ['SELECT * FROM job WHERE daemon_transaction = ''', job.daemon_transaction, '''']); %#ok<*ASGLU>
end

%--
% update parent job if needed
%--

if ~isempty(job.parent_id)
	
	id = int2str(job.parent_id);
	
	[status, parent] = query(store, ['SELECT id, parent_id, daemon_transaction FROM job WHERE id = ', id]);
	
	% NOTE: here we get all children, including ourself
	
	[status, child] = query(store, ['SELECT id, progress, parent_fraction, completed_at FROM job WHERE parent_id = ', id]);

	% NOTE: here we compute the parent progress based on the children
	
	progress = 0; 
	
	for k = 1:numel(child)
		
		if ~isempty(child(k).progress)	
			progress = progress + child(k).progress * child(k).parent_fraction; 
		end
	end
	
	% NOTE: here we compute the parent completion based on the children completion
	
	completion = {child.completed_at};
	
	if all(~iterate(@isempty, completion))
		completion = sort(completion); completed_at = completion{end};
	else
		completed_at = [];
	end
	
	% NOTE: this recursion handles multiple level hierarchies
	
	if isempty(completed_at)
		update_job(store, parent, 'progress', progress);
	else 
		update_job(store, parent, 'progress', progress, 'completed_at', completed_at);
	end
end


% TODO: we are having problems getting the same time as the server, fix this

% [status, result] = query(store, 'SELECT CURRENT_TIMESTAMP;'); 

