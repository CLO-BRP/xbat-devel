function job = create_random_jobs(count)

% create_random_jobs - to test job queue
% --------------------------------------
%
% job = create_random_jobs(count)
%
% Input:
% ------
%  count - of random jobs
%
% Output:
% -------
%  job - array

if ~nargin
	count = 10;
end

%--
% create a persistent counter to name jobs
%--

persistent TOTAL_JOBS;

if isempty(TOTAL_JOBS)
	TOTAL_JOBS = 1;
end

%--
% create numbered jobs with random descriptions
%--

job = empty(create_job); 

user = set_barn_user;

handler = get_extensions('job', 'name', 'Test');
 
for k = 1:count
	
	description = because;
	
	if numel(description) > 255
		description = description(1:255);
	end
	
	job(end + 1) = create_job( ...
		'name', ['job-', int2str(TOTAL_JOBS)], ...
		'handler_type', 'XBAT', ...
		'handler_guid', handler.guid, ...
		'description', description, ...
		'atomic', round(rand), ...
		'user_id', user.id ...
	); %#ok<AGROW>

	TOTAL_JOBS = TOTAL_JOBS + 1;
end

if ~nargout
	disp(job); clear job; 
end

