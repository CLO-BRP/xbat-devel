function establish_project_user(store)

% establish_project_user - relation table
% ---------------------------------------
%
% establish_project_user(store)
%
% Input:
% ------
%  store - database
%
% See also: establish_barn_schema
%
% NOTE: this relation requires some configuration for creation, it's a little function

if ~nargin
	store = [];
end

value = struct; value.state = 'string'; value.role_id = 1;

opt = establish_relation; opt.id = 1; opt.unique = {'project_id', 'user_id', 'role_id'};

% TODO: for this relation we want the 'user', 'project', and 'role' keys to be jointly unique

establish_barn_relation(store, 'user', 'project', value, opt);