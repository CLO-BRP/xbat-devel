function logs = scan_for_old_logs(root, pack)

% scan_for_old_logs - to collect candidates for import
% ----------------------------------------------------
%
% [source, log] = scan_for_old_logs(root, pack)
%
% Input:
% ------
%  root - for search
%  pack - output as single struct (def: false)
%
% Output:
% -------
%  logs - from corresponding source

if nargin < 2
	pack = false;
end

%--
% get log source directories
%--

% NOTE: the indicator could do more work for us, get a list of logs per source for example

source = find_dirs(root, @indicator, true);

if ~pack && nargout < 2
	return;
end

%--
% get logs per directory
%--

% TODO: make sure we output sound validation and location when we are not packed

[logs, sound, location] = get_old_log_files(source);

if pack
	path = source; clear source;
	
	for k = 1:numel(path)
		source(k).path = path{k};
		
		source(k).logs = logs{k};
		
		source(k).sound = cellfree(sound(k));
		
		source(k).location = location{k};
	end
end

if ~nargout
	disp(' '); disp(source); clear source;
end

% TODO: this does not belong here, we need a function that works with the proposed import sources 

%--
% try to match logs with sounds based on source hint
%--

% for k = 1:numel(source)
% 	
% 	for j = 1:numel(source(k).logs)
% 		if source(k).sound(j)
% 			continue;
% 		end
% 		
% 		% TODO: this is where we try to match
% 	end
% 	
% end

%-------------------------
% INDICATOR
%-------------------------

function value = indicator(p)

% NOTE: a directory is not a source by default

value = 0;

%--
% check path string 
%--

% NOTE: library organization means the path will contain 'logs', we also exclude the backup directory

candidate = string_contains(p, 'logs', false) && ~string_contains(p, '__backup', false);

% NOTE: sometimes logs are moved to a non-library organization

% candidate = ~string_contains(p, '__backup', false);

if ~candidate
	return;
end

%--
% check directory file content for log files
%--

% NOTE: the second argument tells function to stop after first find

value = ~isempty(get_old_log_files(p, true));




