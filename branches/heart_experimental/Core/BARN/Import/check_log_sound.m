function [available, location] = check_log_sound(log, fast, verbose)

% check_log_sound - availability
% ------------------------------
%
% [available, location] = check_log_sound(log, fast, verbose)
%
% Input:
% ------
%  log - files
%  fast - check (def: true)
%  verbose - indicator (def: true)
%
% Output:
% -------
%  available - indicator
%  location - of sound data
%
% NOTE: the 'fast' check only checks for the existence of the file stream directory, not every file

%--
% handle input
%--

if nargin < 3
	verbose = true;
end

if nargin < 2
	fast = true;
end

if iscellstr(log) && numel(log) > 1
	[available, location] = iterate(mfilename, log, fast, verbose); return;
end 

if verbose
	disp(['Loading: ', cellfree(log)]); start = clock;
end

log = log_load(log);

if verbose
	elapsed = etime(clock, start); disp(sec_to_clock(elapsed)); disp '';
end

% NOTE: 'exist' does not care about the MATLAB path when full filenames are used

location = log.sound.path; available = exist(location, 'dir') ~= 0;

if ~available
	return;
end

switch log.sound.type
	
	case 'file'
		location = fullfile(location, log.sound.file);
		
		available = exist(location, 'file') ~= 0;
		
	case 'file stream'
		if ~fast
			location = iteraten(@fullfile, 2, location, log.sound.file);

			available = iterate(@exist, location, 'file') ~= 0;
		end
		
end
