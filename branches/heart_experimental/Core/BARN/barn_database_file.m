function [file, root] = barn_database_file(name)

if ~nargin
	name = 'development';
end

root = fullfile(barn_root, 'Database');

file = fullfile(root, [name, '.sqlite3']);