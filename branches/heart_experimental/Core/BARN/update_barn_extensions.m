function [added, stored] = update_barn_extensions(store, clean)

% update_barn_extensions - database representation
% ------------------------------------------------
%
% [added, stored] = update_barn_extensions(store)
%
% Input:
% ------
%  store - database
%  clean - table
%
% Output:
% -------
%  added - extensions
%  stored - all stored extensions

%--
% handle input
%--

if nargin < 2
	clean = false;
end

%--
% get all applcation extensions and BARN prototype
%--

% NOTE: we make sure we have all current extension by reloading

ext = get_extensions('!');

obj = empty(get_barn_prototype('extension'));

%--
% construct extension representation based on prototype
%--

% NOTE: we could be more dynamic here, not useful at the moment

for i = 1:numel(ext)
	
	current = ext(i);
	
	obj(end + 1).type = current.type; %#ok<AGROW>
	
	obj(end).name = current.name;
	
	obj(end).guid = current.guid;
	
	% TODO: determine these in some way, reconsider table columns?
	
% 	obj(end).input_type = 'input-object-type';
% 	
% 	obj(end).output_type = 'output-object-type';
	
	table = extension_table_names(current);
	
	obj(end).tablename__parameter = table.parameter;
	
	obj(end).tablename__value = table.value;
end

%--
% get current BARN stored extensions and determine which need to be added
%--

stored = get_database_objects(store, 'extension');

% NOTE: clear the extension table for the clean option, another possiblity would be to drop and establish

if clean
	delete_database_objects(store, stored, 'extension');
	
	stored = empty(stored);
end

% NOTE: we determine difference between extension lists using the GUID.

[ignore, ix] = setdiff({obj.guid}, {stored.guid}); missing = obj(ix); %#ok<ASGLU>

if ~isempty(missing)
	set_database_objects(store, missing, [], fieldnames(missing), 'extension');
end

%--
% prepare output
%--

added = missing;

if nargout > 1
	stored = get_database_objects(store, 'extension');
end
