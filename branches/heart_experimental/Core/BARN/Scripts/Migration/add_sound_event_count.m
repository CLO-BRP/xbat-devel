function add_sound_event_count(store)

% add_sound_event_count - migration
% ------------------------------
%
% add_sound_event_count(store)
%
% Input:
% ------
%  store - database

if ~is_table_column(store, 'sound', 'events_count')
	add_column(store, 'sound', 'events_count', 'sql', 'INTEGER NOT NULL');
end

update_relation_count(store, 'sound', 'event');

