function render_barn_resources(type, varargin)

% render_barn_resources - assets, images and sound clips
% ------------------------------------------------------
%
% render_barn_resources(type, varargin)
%
% Input:
% ------
%  type - of resource to render

if isempty(local_barn)
	return;
end

%--
% handle input
%--

if ~nargin
	type = 'all';
end

type = lower(string_singular(type));

switch type
	case 'all'
		disp 'Rendering all resources not currently implemented.'
		
	case 'log'
		render_barn_events(varargin{:});
	
	case 'sound'
		render_barn_sound(varargin{:});
end


