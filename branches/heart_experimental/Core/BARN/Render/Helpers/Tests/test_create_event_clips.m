function sound = test_create_event_clips(speeds, log)

% test_create_event_clips - at various speeds for selected log
% ------------------------------------------------------------
%
% sound = test_create_event_clips(speeds, log)
%
% Input:
% ------
%  speeds - to create clips for (def: [0.25, 0.5, 1, 2, 4])
%  log - to get events from
%
% Output:
% -------
%  sound - array, one for each speed
%
% NOTE: all sounds are sampled at 44100, remember each clip is gain normalized
%
% NOTE: to hear each sound '[X, r] = sound_read(sound(k)); wavplay(X, r);'
%
% See also: create_event_clips

%--
% handle input
%--

if nargin < 2
	log = get_selected_log;
	
	if isempty(log)
		return;
	end 
end

if ~nargin
	speeds = [0.25, 0.5, 1, 2, 4];
end

%--
% create test clip output directory
%--

here = fileparts(mfilename('fullpath'));

root = create_dir(fullfile(here, 'test_clips'));

[status, event] = query(log.store, ['SELECT id FROM event WHERE log_id = ', int2str(log.store.log.id), ' LIMIT 20']); %#ok<ASGLU>

event = log_get_events(log, [event.id]);

opt = create_event_clip; opt.rate = 44100;

for k = 1:numel(speeds);
	
	for j = 1:numel(event)
		
		opt.speed = speeds(k);
		
		rootk = create_dir(fullfile(root, strrep(num2str(speeds(k)), '.', '-')));
		
		file = fullfile(rootk, ['clip_' int_to_str(j, 10, '0'), '.mp3']);
		
		if exist(file, 'file')
			delete(file);
		end
		
		create_event_clip(log.sound, event(j), file, opt);
	end
	
	sound(k) = sound_create('file stream', rootk); %#ok<AGROW>
end 


	
	
	