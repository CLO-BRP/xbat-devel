function result = render_barn_events(log, event, render)

% render_barn_events - clips and images according to request
% ----------------------------------------------------------
%
% render = render_barn_events
%
% result = render_barn_events(log, event, render)
%
% Input:
% ------
%  log - parent
%  event - array
%  render - options
%
% Output:
% -------
%  render - options default
%  result - of render
%
% See also: render_barn_resources, create_event_clip, create_event_thumb

%----------------------
% HANDLE INPUT
%----------------------

%--
% set default render options
%--

% TODO: should these be informed by the sound under consideration? the log sound rate for example?

if nargin < 3
	% TODO: here we should consider the required context
	
	% NOTE: for simplicity we may want to maintain a clip and context-clip for now
	
	% CLIP
	%--------------------
	
	% NOTE: here we consider the multiple speeds
	
	clip = create_event_clip; clip.rate = 44100; clip.speed = [0.5, 1, 2];
	
	render.clip = clip;
	
	% THUMB
	%--------------------
	
	thumb = create_event_thumb;

	% TODO: are these spectrogram options reasonable defaults?
	
	specgram = fast_specgram; specgram.fft = 256; specgram.win_length = 1; specgram.hop = 0.125;
	
	thumb.specgram = specgram;
	
	render.thumb = thumb;
	
	% NOTE: output default options if needed
	
	if ~nargin
		result = render; return;
	end
end

%--
% by default get all events and all logs in current library
%--

% NOTE: in this case we will render all events in log

if nargin < 2
	event = [];
end

if ~nargin
	log = get_logs(get_active_library);
end

if isempty(log)
	return;
end

%--
% render all events for input logs
%--

if numel(log) > 1
	% NOTE: in this case we disregard the 'event' input
	
	result = iterate(@render_barn_events, log, [], render); return;
end

%--
% render all log events
%--

if isempty(event)
	
	log_id = int2str(log.store.log.id); limit = 200;
	
	[ignore, count] = query(log.store, ...
		['SELECT COUNT(id) AS total FROM event WHERE log_id = ', log_id] ...
	); %#ok<*ASGLU>
	
	pages = ceil(count.total / limit);
	
	disp(' ');
	
	for k = 1:pages
		
		disp(['Rendering page ', int2str(k), ' of ', int2str(pages)]); disp(' ');
		
		[ignore, page] = query(log.store, ...
			['SELECT id FROM event WHERE log_id = ', log_id, ' ORDER BY score DESC LIMIT ', int2str(limit), ' OFFSET ', int2str((k - 1) * limit)] ...
		);
				
		event = log_get_events(log, [page.id], 0);
		
		start = clock;
		
		result{k} = render_barn_events(log, event, render);
		
		disp(sec_to_clock(etime(clock, start)));
	end
	
	result = [result{:}]; return;
end

%----------------------
% RENDER
%----------------------

for k = 1:numel(event)

	% HACK: this should not be needed eventually
	
	if isempty(event(k).guid)
		db_disp 'THIS SHOULD NOT HAPPEN, when we get here the event should have a GUID!'; stack_disp; continue; 
	end 

	disp(['Rendering event ', event(k).guid]); start = clock;
	
	%--
	% compute and ensure clear destination
	%--
	
	[destination, created] = create_dir(local_barn_files(event(k), 'event')); 
	
	if ~created
		delete(fullfile(destination, '*'));
	end

	%--
	% render clips
	%--
	
	% NOTE: we render to the native samplerate as flac and wav, then we render various speeds in MP3
	
	clip = render.clip; clip.rate = log.sound.samplerate; clip.speed = 1;
		
	files = {};
	
% 	db_disp; fullfile(destination, 'clip.fla')
	
	files{end + 1} = create_event_clip(log.sound, event(k), fullfile(destination, 'clip.fla'), clip); %#ok<*AGROW>
	
	files{end + 1} = create_event_clip(log.sound, event(k), fullfile(destination, 'clip.wav'), clip);
	
	for j = 1:numel(render.clip.speed)
		
		clip = render.clip; clip.speed = render.clip.speed(j);
		
		% NOTE: this convention leads to backwards BARN view compatible rendering, it also makes some sense
		
		if clip.speed == 1
			name = 'clip.mp3';
		else
			name = ['clip_', strrep(num2str(clip.speed), '.', '-'), '.mp3'];
		end
		
		file = fullfile(destination, name);
		
		files{end + 1} = create_event_clip(log.sound, event(k), file, clip);
	end

	%--
	% render thumbs
	%--
	
	thumb = render.thumb; 
	
	for j1 = 1:numel(render.thumb.specgram)
		
		thumb.specgram = render.thumb.specgram(j1);
		
		for j2 = 1:numel(render.thumb.map)

			thumb.map = render.thumb.map(j2);
			
			% TODO: reconsider thumb name and convey to BARN code
			
			file = fullfile(destination, str_implode({'thumb', int2str(thumb.specgram.fft), thumb.map.name, 'png'}, '.'));
						
			files{end + 1} = create_event_thumb(log.sound, event(k), file, thumb);
		end
	end
	
	result(k).files = files; result(k).elapsed = etime(clock, start);
	
	info = dir(destination); result(k).bytes = sum([info.bytes]);
	
	disp(['elapsed = ', num2str(result(k).elapsed), ', bytes = ', int2str(result(k).bytes)]); disp('  ');
end

% OLDER CODE, still has some ideas about the various views we might be interested in

% %----------------------
% % SETUP
% %----------------------
% 
% %--
% % configure thumbs
% %--
% 
% opt = create_event_thumb;
% 
% if isempty(render.specgram)
% 	
% 	rate = get_sound_rate(log.sound); time = specgram_resolution(log.sound.specgram, rate);
% 	
% 	% TODO: we should adapt the display parameters maintaining 'aspect ratio'
% 	
% 	specgram = fast_specgram;
% 
% 	% NOTE: these are the images currently on display in the BARN interface, the '0.75' increases the time resolution
% 	
% % 	specgram.fft = 256; specgram.win_length = 1; specgram.hop = 0.75 * (time * rate) / 256;
% 
% 	specgram.fft = 256; specgram.win_length = 1; specgram.hop = 0.5 * 0.75 * (time * rate) / 256;
% 	
% 	specgram(end + 1) = specgram;
% 	
% % 	specgram(end).fft = 512; specgram(end).win_length = 0.5; specgram(end).hop = (time * rate) / 512;
% 	
% % 	db_disp; disp(specgram)
% end
% 
% map.name = 'gray-light'; map.scale = 1; map.matrix = flipud(gray(512));
% 
% % map(end + 1) = map;
% % 
% % map(end).name = 'gray-dark'; map(end).matrix = gray(512);
% 
% % map(end + 1).name = 'jet'; map(end).matrix = jet(512); map(end).scale = 0;
% % 
% % map(end + 1).name = 'sunset'; map(end).matrix = cmap_pseudo(512); map(end).scale = 0;