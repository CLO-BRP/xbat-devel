function [file, event] = create_event_clip(sound, event, file, clip)

% create_event_clip - file with some options
% ------------------------------------------
%
% [file, event] = create_event_clip(sound, event, file, clip)
%
% clip = create_event_clip
% 
% Input:
% ------
%  sound - to read from
%  event - to read
%  file - to write
%  clip - options
%
% Output:
% -------
%  file - name
%  event - with corresponding samples
%  clip - options default
% 
% See also: create_event_thumb, event_sound_read

%--
% set and possibly output default options
%--

if nargin < 4
	% NOTE: these control padding and tapering of pad
	
	clip.pad = 0; clip.taper = 0; 
	
	% NOTE: the rate can be 'player', 'input', or specified rate
	
	clip.rate = 'player'; clip.speed = 1;
	
	if ~nargin
		file = clip; return;
	end
end

%--
% setup
%--

% TODO: make sure we hit a format supported rate?

switch clip.rate
	
	case 'player'
		sound.output.rate = get_player_resample_rate(sound.samplerate);

	case 'input'
		sound.output.rate = [];
		
	otherwise
		sound.output.rate = clip.rate;
end

% NOTE: the 'sound.output.rate' will induce the samplerate, we then keep the file rate as 'rate' to produce the change in speed

% NOTE: 'sound_file_read' will actually carry out the rate conversion

rate = sound.output.rate;

sound.output.rate = sound.output.rate / clip.speed;

%--
% read samples if necessary
%--

event = event_sound_read(event, sound, clip.pad, clip.taper);

% db_disp; sound, clip, event, file

factor = replay_gain(event.samples, event.rate);

event.samples = factor * event.samples;

%--
% write file
%--

if iscell(file)
	file = [file{:}];
end

% TODO: we should test this and also extend to handle callbacks

if isa(file, 'function_handle')
	file = file(event);
end

sound_file_write(file, event.samples, rate);





