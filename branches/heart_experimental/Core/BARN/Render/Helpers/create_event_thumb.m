function file = create_event_thumb(sound, event, file, thumb)

% create_event_thumb - create event thumb image
% ---------------------------------------------
%
% file = create_event_thumb(sound, event, file, thumb)
%
% thumb = create_event_thumb
%
% Input:
% ------
%  sound - containing event
%  event - to thumb
%  file - to output
%  thumb - options
%
% Output:
% -------
%  file - produced
%  thumb - options default
%
% See also: create_event_clip

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default options and possibly output
%--

if nargin < 4
	
	thumb.pad = 0; thumb.taper = 0; % event reading options
	
	thumb.band = [];
	
	thumb.specgram = []; % spectrogram computation options
	
	thumb.map.name = 'gray-light';
	
	thumb.map.scale = 1;
	
	thumb.map.matrix = flipud(gray(512));
		
	thumb.overlay = [];
	
	if ~nargin
		file = thumb; return;
	end
end

if trivial(thumb.specgram)
	
	thumb.specgram = sound.specgram;
end

%--------------------------
% CREATE IMAGE FILES
%--------------------------
				
%--
% read samples if necessary
%--

% if thumb.pad || isempty(event.samples)

event.rate = get_sound_rate(sound);

event.duration = required_duration(event, thumb.specgram);

event = event_sound_read(event, sound, thumb.pad, thumb.taper);

% end

% db_disp; thumb.specgram, event.rate, resolution = specgram_resolution(thumb.specgram, event.rate, true)

%--
% compute spectrogram and create image
%--

[X, freq] = fast_specgram(event.samples, event.rate, 'norm', thumb.specgram);

% NOTE: we flip and scale image values, these are conventional

X = lut_dB(X);

%--
% create requested colormap and convert image to RGB
%--

if thumb.map.scale
% 	stop = find(freq > 4000, 1, 'first');
% 	
% 	if ~isempty(stop)
% 		Y = X(1:stop, :);
% 	end

	% TODO: we need protection from zeros (in MP3) and DC and trend offset (generally bad recordings)
	
	Y = X;
	
% 	Y = Y(:, 1:4:end);	
	
	if isempty(thumb.band)
		map = cmap_scale([], Y, thumb.map.matrix);
	else
		map = cmap_scale([], Y, thumb.map.matrix);
	end
else
	map = thumb.map.matrix;
end

% NOTE: each spectrogram is scaled independently

X = flipud(X);

X = gray_to_rgb(X, map);

% TODO: possibly display event as mask

% X = add_event_overlay(X, event, thumb);

%--
% write image
%--

% NOTE: convert image to uint8 ourselves, 'imwrite' takes too long doing the same

imwrite(uint8(lut_range(X, [0, 255])), file);


%--------------------------
% ADD_EVENT_OVERLAY
%--------------------------

function X = add_event_overlay(X, event, thumb)
	
% TODO: we need to integrate resolution and mask options

%--
% compute event mask rectangle in pixel coordinates
%--

% NOTE: the spectrogram resolution allows conversion of event bounds to pixel ranges

[dt, df] = specgram_resolution(thumb.specgram, event.rate);

% NOTE: clip start time is zero

pad = thumb.pad;

x1 = max(floor(pad / dt), 1); 

x2 = ceil((diff(event.time) + pad) / dt);

y1 = max(floor(event.freq(1) / df), 1); 

y2 = ceil(event.freq(2) / df);

% NOTE: here we assemble the mask

Z = zeros(size(X, 1), size(X, 2));

Z(y1:y2, x1:x2) = 1;

Z = flipud(Z);

%--
% configure and apply event mask to image
%--

mask_opt = mask_gray_color;

mask_opt.bound = 1;

mask_opt.color = [0.5, 1, 0.5];

X = mask_gray_color(X, Z, mask_opt);


%--------------------------
% REQUIRED_DURATION
%--------------------------

function duration = required_duration(event, parameter, context) %#ok<INUSL>

%--
% compute required page duration
%--

dt = specgram_resolution(parameter, event.rate);

hop = (parameter.fft - round((1 - parameter.hop) * parameter.fft));

duration = ((event.duration / dt) * hop * parameter.sum_length + parameter.fft) / event.rate;



