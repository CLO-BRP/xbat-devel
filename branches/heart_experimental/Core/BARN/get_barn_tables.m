function table = get_barn_tables(store, type, confirm)

% get_barn_tables - using master table
% ------------------------------------
%
% table = get_barn_tables(store, type, confirm)
%
% Input:
% ------
%  store - description
%  type - of table to request
%  confirm - table existence
%
% Output:
% -------
%  table - objects

%--
% get all tables from master by default
%--

if nargin < 2 || isempty(type)
	type = 'all';
end

%--
% select tables from master
%--

switch type

	case 'all'
		[status, table] = query(store, 'SELECT * FROM barn_master;');

	otherwise
		[status, table] = query(store, ['SELECT * FROM barn_master WHERE type = ''', type, ''';']);

end

if isempty(table)
	return; 
end

%--
% confirm that returned tables exist
%--

% TODO: perform clean of barn master, should select unique and delete all and reset

if nargin < 3 || ~confirm
	return; 
end

reject = empty(table);

for k = numel(table):-1:1
	
	if ~has_table(store, table(k).name)
		reject(end + 1) = table(k); table(k) = []; 
	end
	
end

if ~isempty(reject)
	query(store, ['DELETE FROM barn_master WHERE id IN (', str_implode([reject.id], ', ', @int2str), ');']);
end
