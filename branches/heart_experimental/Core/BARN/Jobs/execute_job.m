function result = execute_job(job, store, server)

% execute_job - manager for jobs
% --------------------------------
%
% result = execute_job(job, store, server)
%
% Input:
% ------
%  job - to execute
%  store, server - connection information, store holds queue
%
% See also: job_daemon, pick_job

[handler, ignore, context] = get_browser_extension('job', 0, job.handler_guid); %#ok<ASGLU>

context.store = store; context.server = server;

%--
% delegate job execution to handler
%--

helper = handler.fun.execute;

result = struct; result.started = clock;
 
if ~isempty(helper)
	
	% NOTE: job 'attempt' 'starts' as we delegate to helper
			
	job = update_job(store, job, ...
		'attempt', job.attempt + 1, 'started_at', get_current_timestamp(store) ...
	);

	try
		[job, context] = helper(job, handler.parameter, context); %#ok<NASGU>
		
		% NOTE: job is 'complete' when the execution helper returns

		job = update_job(store, job, 'completed_at', get_current_timestamp(store));
	catch
		% NOTE: job has 'failed' when helper throws exception
		
		% TODO: we want to set other job fields to allow this job to be retried
		
		% NOTE: 'delayed_job' uses the 'run_at' and attempt increasing delays to handle failed job retries
		
		job = update_job(store, job, ...
			'failed_at', get_current_timestamp(store) ...
		);
		
		extension_warning(handler, 'Failed to execute job.', lasterror);
		
		result.lasterror = lasterror;
	end
else
	
	% NOTE: this is a sort of job limbo, the job has been handled but not completed
end

%--
% pack results
%--

result.job = job;

result.failed = isfield(result, 'lasterror');

result.elapsed = etime(clock, result.started);

