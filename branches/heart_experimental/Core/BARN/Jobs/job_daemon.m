function daemon = job_daemon(store, server)

% job_daemon - used to poll BARN store queued jobs
% ------------------------------------------------
%
% daemon = job_daemon(store, server)
%
% Input:
% ------
%  store - to poll
%  server - to interact with
%
% Output:
% -------
%  daemon - timer
%
% See also: pick_job, execute_job

%--
% set local barn store default
%--

if ~nargin
	[store, server] = local_barn;
end

%--
% create barn daemon to check on store
%--

guid = generate_uuid;

daemon = create_timer('BARN jobs daemon', {@on__timer, guid, store, server}, 2, 'fixedRate', true);

% NOTE: this should allow the timer skip polling while working on a job

set(daemon, 'busymode', 'drop');


%------------------------
% ON__TIMER
%------------------------

function on__timer(obj, eventdata, guid, store, server) %#ok<INUSL>

% NOTE: we could output the 'handler' here and pass it to execute

job = pick_job(store, guid);

if isempty(job)
	return;
end

result = execute_job(job, store, server);

% db_disp; disp(flatten(result));


