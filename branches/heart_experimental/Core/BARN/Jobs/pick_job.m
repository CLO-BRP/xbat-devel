function job = pick_job(store, daemon)

% pick_job - from store 
% ---------------------
%
% job = pick_job(store, daemon)
%
% Input:
% ------
%  store - to check
%  daemon - guid
%
% Output:
% -------
%  job - picked
%
% See also: job_daemon, execute_job

% TODO: consider return of jobs atomized

%--
% handle input
%--

if ~nargin || isempty(store)
	error('Store input is required to pick job.');
end

%--
% pick job
%--

% NOTE: we make sure to decompose non-atomic jobs and ultimately pick an atomic job

while true
	job = pick_non_atomic(store, daemon);
	
	if isempty(job)
		break;
	end
	
	atomize(store, job);
end

job = pick_atomic(store, daemon);


%----------------------
% ATOMIZE
%----------------------

function child = atomize(store, job)

% atomize - manager for a non-atomic job
% ---------------------------------------
%
% child = atomize(store, job)
%
% Input:
% ------
%  store - for the queue
%  job - non-atomic
%
% Output:
% -------
%  child - jobs

%--
% get job handle extension and context
%--

% handler = get_extensions('job', 'guid', job.handler_guid);

handler = get_browser_extension('job', 0, job.handler_guid);

% TODO: develop context concept for jobs

context.store = store; context.debug = true;

% TODO: this likely indicates a malformed job, consider response, consider logging mechanism

if isempty(handler)
	
	db_disp handler-not-found;	return;
end

%--
% try to atomize job and pack results into the queue
%--

% NOTE: it is not certain that jobs returned will be atomic? they may need another round, like the asteroids

helper = handler.fun.atomize;

try
	if isempty(helper)
		error('Non-atomic job handler is missing atomizer.');
	end
	
	% NOTE: non-atomic job starts on atomization 
	
	job = update_job(store, job, 'started_at', get_current_timestamp(store));
	
	child = helper(job, handler.parameter, context);
catch	
	extension_warning(handler, 'Job atomization failed.', lasterror);
end

% NOTE: the parent identifier is what we can reasonable set, other fields we cannot here

for k = 1:numel(child)
	% NOTE: this makes sure the jobs are new and do not seem handled yet
		
	child(k).id = []; child(k).daemon_guid = []; child(k).daemon_transaction = [];
	
	if isempty(child(k).parent_id)
		child(k).parent_id = job.id;
	end
end

if ~isempty(child)
	
	child = set_barn_objects(store, child, [], [], 'job');
end


%----------------------
% PICK_NON_ATOMIC
%----------------------

function job = pick_non_atomic(store, daemon)

job = pick_job_helper(store, daemon, false);


%----------------------
% PICK_ATOMIC
%----------------------

function job = pick_atomic(store, daemon)

job = pick_job_helper(store, daemon, true);


%----------------------
% PICK_JOB_HELPER
%----------------------

function job = pick_job_helper(store, daemon, atomic)

% pick_job_helper - helps daemon pick job
% ---------------------------------------
%
% job = pick_job_helper(store, daemon, atomic)
% 
% Input:
% ------
%  store - with jobs
%  daemon - identifier
%  atomic - flag
%
% Output:
% -------
%  job - picked
 
% db_disp(['daemon-', daemon, ' picking ...']);

%--
% we use a transaction and corresponding flag to select and mark a job
%--

transaction = generate_uuid;

switch lower(store.adapter)
	
	case 'mysql'
		% NOTE: this uses a special trick to avoid using a table in the UPDATE and FROM clauses

		% REF: http://www.xaprb.com/blog/2006/06/23/how-to-select-from-an-update-target-in-mysql/
		
		sql = { ...
			'BEGIN;', ...
			['  UPDATE job SET daemon_guid = ''', daemon, ''', daemon_transaction = ''', transaction, ''''], ... 
			['  WHERE id = (SELECT id FROM (SELECT * FROM job) AS X WHERE atomic ', ternary(atomic, '=', '<>'), ' 1 AND started_at IS NULL AND daemon_guid IS NULL LIMIT 1);'], ...
			'COMMIT;' ...
		};
		
	otherwise
		% TODO: this is currently failing for SQLite with the JDBC driver
		
		sql = { ...
			'BEGIN;', ...
			['  UPDATE job SET daemon_guid = ''', daemon, ''', daemon_transaction = ''', transaction, ''''], ... 
			['  WHERE id = (SELECT id FROM (SELECT * FROM job) AS X WHERE atomic ', ternary(atomic, '=', '<>'), ' 1 AND started_at IS NULL AND daemon_guid IS NULL LIMIT 1);'], ...
			'COMMIT;' ...
		};
end

query(store, sql_string(sql));

%--
% get transaction job
%--

[ignore, job] = query(store, ['SELECT * FROM job WHERE daemon_transaction = ''', transaction, '''']); %#ok<ASGLU>


