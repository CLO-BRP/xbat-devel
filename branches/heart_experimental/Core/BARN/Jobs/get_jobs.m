function job = get_jobs(store, state, limit)

% get_jobs - request jobs from store in various ways
% ----------------------------------------------------
%
% job = get_jobs(store, state, limit)
%
% Input:
% ------
%  store - with jobs
%  state - of jobs 'waiting', 'started', 'running', 'completed'
%  limit - for request
%
% Output:
% -------
%  job - list

% TODO: reconsider selectors, we want monitors here, we are not picking jobs here

%--
% handle input
%--

if nargin < 3
	limit = [];
end

if nargin < 2
	state = 'waiting';
end

if ~nargin
	store = local_barn;
end 

if isempty(store)
	job = []; return;
end

%--
% get jobs by type
%--
				
if ~is_known_state(state)
	error(['Unrecognized job state ''', state, '''.']);
end

[status, job] = query(store, get_state_query(state, limit)); %#ok<ASGLU>

% db_disp; disp(get_state_query(type, limit)); disp(job);

if ~nargout
	disp(job); clear job;
end


%--------------------------------
% KNOWN_STATES
%--------------------------------

function states = known_states

states = {'waiting', 'started', 'running', 'completed'};


%--------------------------------
% IS_KNOWN_COMMAND
%--------------------------------

function result = is_known_state(state)

result = string_is_member(state, known_states);


%--------------------------------
% GET_REQUEST_QUERY
%--------------------------------

function sql = get_state_query(state, limit)

%--
% handle input
%--

if nargin < 2
	limit = 1;
end

%--
% build common prefix and suffix parts of query
%--

if strcmp(state, 'completed')
	prefix = 'SELECT * FROM job WHERE completed_at IS NOT NULL '; 
else
	prefix = 'SELECT * FROM job WHERE completed_at IS NULL '; 
end

if isempty(limit)
	suffix = ';';
else
	suffix = [' LIMIT ', int2str(limit), ';'];
end

%--
% build query based on command
%--

switch state
	
	case 'waiting'
		sql = [prefix, 'AND started_at IS NULL ORDER BY created_at', suffix];
		
	case 'started'
		sql = [prefix, 'AND started_at IS NOT NULL ORDER BY started_at', suffix];
	
	case 'running'
		sql = [prefix, 'AND running = 1 ORDER BY started_at', suffix];
		
	case 'completed'
		sql = [prefix, 'ORDER by completed_at DESC', suffix];
		
end
