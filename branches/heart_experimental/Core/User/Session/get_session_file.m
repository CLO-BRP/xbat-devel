function [file, exists] = get_session_file(name, user)

% get_session_file - get session file name
% ----------------------------------------
%
% [file, exists] = get_session_file(name, user)
%
% Input:
% ------
%  name - session name
%  user - user (def: active user)
%
% Output:
% -------
%  file - full session filename
%  exists - indicator

%--
% handle input
%--

% NOTE: we need a name to get a session file

if ~nargin || isempty(name)
	file = '';
end

if ~proper_filename(name)
	error('Proposed session filename is not a proper filename.');
end

% NOTE: we set the active user as default

if nargin < 2
	user = get_active_user;
end

%--
% get session filename
%--

file = fullfile(sessions_root(user), [name '.txt']);

%--
% check file exists
%--

if nargout > 1
	exists = exist(file, 'file');
end 
