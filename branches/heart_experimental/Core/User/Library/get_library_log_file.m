function file = get_library_log_file(lib, sound, name)

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

if nargin < 3
	error('Sound name input is required.');
end

if isstruct(sound)
	sound = sound_name(sound);
end

%--
% set library to active library
%--

if (nargin < 1) || isempty(lib)
	lib = get_active_library;
end
	
%--
% handle multiple sounds recursively
%--

if iscellstr(name)
		
	% NOTE: the signature is unfortunate, as it does not allow 'iterate'
	
	file = cell(length(name), 1);
	
	for k = 1:length(name)
		file{k} = get_library_log_file(lib, sound, name{k});
	end 
	
	return;
	
end

%--
% check sound name
%--

if ~ischar(name)
	error('Sound name input must be string of string cell array.');
end

%-----------------------------------
% BUILD LOG FILE LOCATION
%-----------------------------------

%--
% build log file name
%--

file = [lib.path, sound, filesep, 'Logs', filesep, name, '.mat'];

