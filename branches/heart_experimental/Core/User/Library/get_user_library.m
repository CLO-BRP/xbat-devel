function [lib, user] = get_user_library(name)

% get_user_library - get library from full name
% ---------------------------------------------
%
% [lib, user] = get_user_library(name)
%
% Input:
% ------
%  name - full library name
%
% Output:
% -------
%  lib - library
%  user - user

%----------------------
% HANDLE INPUT
%----------------------

% TODO: check for proper full library name

lib = []; user = [];

%----------------------
% GET LIBRARY
%----------------------

%--
% get user and library name from full library name
%--

info = parse_tag(name, filesep, {'user', 'name'}); 

user = info.user;

name = info.name;

%--
% get actual user and library
%--

user = get_users('name', user);

if isempty(user)
	return;
end

lib = get_libraries(user, 'name', name);
