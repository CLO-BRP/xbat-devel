function file = library_save(lib)

% library_save - save library data to file
% -----------------------------------------
%
% file = library_save(lib)
%
% Input:
% ------
%  lib - library to save
%
% Output:
% -------
%  file - library file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1982 $
% $Date: 2005-10-24 11:59:36 -0400 (Mon, 24 Oct 2005) $
%--------------------------------

%--
% handle multiple libraries recursively
%--

if (numel(lib) > 1)

	for k = 1:numel(lib)
		file{k} = library_save(lib(k));
	end
	
	return;
	
end

%--
% create library file path and update modification date
%--

lib.modified = now;

file = get_library_file(lib);

%--
% save library data to file
%--

% TODO: consider output of library file info

try
	save(file, 'lib');
catch
	file = ''; disp(['WARNING: Failed to save library ''', lib.name, ''' to file.']);
end

