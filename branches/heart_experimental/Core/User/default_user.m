function user = default_user

% default_user - defines and creates default user
% -----------------------------------------------
%
% user = default_user
%
% Output:
% -------
%  user - default user

%--
% create default user structure
%--

% NOTE: we give the default user a guid email conventionally, as a convenience

user = user_create( ...
	'id',0, ...
	'name','Default', ...
	'alias','Default', ...
    'email', generate_uuid ...
);

%--
% add default user to file system
%--

user = add_user(user);