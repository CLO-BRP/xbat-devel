function user = user_create(varargin)

% user_create - create user structure
% -----------------------------------
%
% user = user_create
%
% Output:
% -------
%  user - user structure

% NOTE: user data is typically stored in a mat file in user folder within users

%--------------------------------
% PRIMITIVE FIELDS
%--------------------------------

user.type = 'user';

% TODO: compute this in some meaningful way

user.id = round(rand * 10^12);

%--------------------------------
% USER FIELDS
%--------------------------------

% NOTE: consider adding further contact information

user.name = '';

% NOTE: the two next fields currently have no use

user.alias = '';

user.organization = '';

user.email = '';

user.password = '';

% user.crypted_password = '';

user.url = '';

user.prefs = struct;

user.prefs.new_log_format = 'BARN';

%--------------------------------
% LIBRARY FIELDS
%--------------------------------

% NOTE: library paths are stored here, using it further information can be obtained.

% TODO: check availability of libraries on user load

user.library = {}; % path to libraries used

% TODO: check to see if this field is used in the user code

user.default = []; % index to default library

user.active = []; % index to active library

%--------------------------------
% ADMINISTRATIVE FIELDS
%--------------------------------

% NOTE: what changes induce modification date change

user.created = now; % creation date

user.modified = []; % modification date

%--------------------------------
% METADATA FIELDS
%--------------------------------

% user.annotation = annotation_create; % array of annotation structures
% 
% user.measurement = measurement_create; % array of measurement structures

%--------------------------------
% USERDATA FIELD
%--------------------------------

user.userdata = []; % userdata field is not used by system

	
%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if ~isempty(varargin)
	
	user = parse_inputs(user, varargin{:});
	
	% NOTE: check for the type integrity of given data, movin closer to objects
	
	%--------------------------------
	% CHECK FIELDS
	%--------------------------------
	
	%--------------------------------
	% COMPUTE FIELDS
	%--------------------------------
	
	%--
	% alias is name
	%--
	
	% TODO: this way of computing alias leads to obscure choices
	
	if isempty(user.alias) && ~isempty(user.name)
		user.alias = user.name;
	end
	
end