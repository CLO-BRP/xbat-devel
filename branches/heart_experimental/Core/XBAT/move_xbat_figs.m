function move_xbat_figs(handle, type, opt)

% move_xbat_figs - helper for moving xbat figs
% --------------------------------------------
%
% move_xbat_figs(handle, type, opt)
%
% Input:
% ------
%  handle - figures to move
%  type - move type 'tile', 'cascade', 'arrange'
%  opt - move options

% TODO: allow for input order

% TODO: allow for tiling options

%---------------
% HANDLE INPUT
%---------------

%--
% get sound browser handles
%--

if isempty(handle)
	handle = get_xbat_figs('type', 'sound');
end

%--
% remove invisible figures and return for trivial cases
%--

% NOTE: for single figure display we do not move figure

for k = length(handle):-1:1
	
	if strcmp(get(handle(k), 'visible'), 'off')
		handle(k) = [];
	end
	
end 

if numel(handle) < 2
	return;
end

%--
% set default options
%--

if nargin < 3
	opt = struct;
end


%---------------
% TILE
%---------------

%--
% stop figure display daemons
%--

pd = timerfind('name','XBAT Palette Daemon'); stop(pd);

bd = timerfind('name','XBAT Browser Daemon'); stop(bd);

%--
% sort windows using names and move 
%--

[ignore, ix] = sort(get(handle, 'name')); handle = handle(ix);

switch type
	
	case 'tile'
		
		figs_tile(handle);

	case 'cascade'
		
		figs_cascade(handle);
		
		% NOTE: this 'orders' the figures to match the cascade order
		
		for k = 1:length(handle)
			pos(k, :) = get(handle(k), 'position');
		end

		[ignore, ix] = sort(pos(:, 1));

		for k = 1:length(handle)
			figure(handle(ix(k)));
		end
		
	case 'arrange'
		
		figs_arrange(handle);
		
	% TODO: consider some kind of error message or warning
	
	otherwise, return;
		
end

%--
% start daemons
%--

start(pd); start(bd);