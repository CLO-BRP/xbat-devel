function cascade_xbat_figs(handle)

if ~nargin
	handle = [];
end

move_xbat_figs(handle, 'cascade');
