function [handle, tag] = get_xbat_figs(varargin)

% get_xbat_figs - get handles to xbat figures
% -------------------------------------------
%
% [handle, tag] = get_xbat_figs
%
%               = get_xbat_figs('field', value, ...)
%
% Input:
% ------
%  field - field to select on ('type', 'parent', or 'child')
%  value - value of field
%
% Output:
% -------
%  handle - handles
%  tag - tags

% TODO: make this code output the figure type and tag table

% TODO: make this function output parentage and children information

%------------------------------------
% SETUP
%------------------------------------

% NOTE: other get fields may be developed as needed

persistent ALLOWED_FIELDS;

if isempty(ALLOWED_FIELDS)
	
	ALLOWED_FIELDS = {'type', 'parent', 'child'};
end

app = 'XBAT';

%------------------------------------
% HANDLE INPUT
%------------------------------------

%--
% get field  value pairs or select all
%--

% NOTE: 'get_field_value' will produce an error on improper field value pair input

if ~isempty(varargin)	
	[field, value] = get_field_value(varargin, ALLOWED_FIELDS);
else
	field{1} = 'type'; value{1} = 'all';
end

%--
% check input values
%--

% NOTE: we ensure single handle input for 'parent' and 'child' based 'get'

ix = find(strcmp(field, 'parent'));

if ~isempty(ix)

	if ~ishandle(value{ix}) || (length(value{ix}) > 1)
		error('Only single handle ''parent'' get is supported.');
	end
end

ix = find(strcmp(field, 'child'));

if ~isempty(ix)

	if ~ishandle(value{ix}) || (length(value{ix}) > 1)
		error('Only single handle ''child'' get is supported.');
	end
end

%------------------------------------
% SET FIGURES TO SELECT FROM
%------------------------------------

%--
% get open figures if type is the first 'get' criterion
%--
	
handle = get(0, 'children');

% NOTE: return empty on no figures

if isempty(handle)
	tag = ''; return;
end

%------------------------------------
% SELECT FIGURES
%------------------------------------

%--
% select on all fields
%--

for k = 1:length(field)

	switch field{k}

		% NOTE: this selection rule relies on proper figure tags 
		
		case 'type', handle = type_select(app, handle, value{k});
		
		% NOTE: consider whether to implement as parent or child based 
		
		case 'parent', handle = parent_select(app, handle, value{k});
		
		% NOTE: this results in at most one figure handle
		
		case 'child', handle = child_select(app, handle, value{k});
			
	end
end

%--
% output tags if needed
%--

if nargout > 1
	tag = get(handle, 'tag');
end


%---------------------------------------------------------
% TYPE_SELECT
%---------------------------------------------------------

function out = type_select(app, in, type)

% type_select - select figures based on type of figure
% ----------------------------------------------------
%
% out = type_select(app, in, type)
%
% Input:
% ------
%  in - figures to select from
%  type - type to select
%
% Output:
% -------
%  out - selected figures

%--
% return empty on empty
%--

if isempty(in)
	out = []; return;
end

%--
% select figures based on type
%--

switch lower(type)
	
	%------------------------------------
	% ALL XBAT FIGURES
	%------------------------------------
	
	case 'all'
		
		%--
		% set type tag prefix and length
		%--
		
		% TODO: these prefixes are hard coded throughout the system,
		% consider creating an access function
		
		% NOTE: the hard coded tags provide some readability to the code
		
		type = { ...
			[app '_SOUND_BROWSER'], ...
			[app '_LOG_BROWSER'], ...
			[app '_DATA_BROWSER'], ...
			[app '_PALETTE'], ...
			[app '_WAITBAR'], ...
			[app '_WIDGET'], ...
			[app '_WIDGET::SOUND_FEATURE'], ...
			[app '_WIDGET::SOUND_DETECTOR'], ...
			[app '_EXPLAIN'], ...
			[app '_SELECTION'], ...
			'FIG' ...
		};
	
		% NOTE: this can be done more efficiently
		
		out = [];
		
		for k = 1:length(type)
			
			tmp = match_tag_prefix(in, type{k}, 1);
			
			if ~isempty(tmp)
				out = [out; tmp]; %#ok<AGROW>
			end
		end
		
	%------------------------------------
	% SOUND BROWSER FIGURES
	%------------------------------------
	
	case 'sound'
		
		out = match_tag_prefix(in, [app '_SOUND_BROWSER'], 1);
		
	%------------------------------------
	% LOG BROWSER FIGURES
	%------------------------------------
	
	case 'log'
		
		out = match_tag_prefix(in, [app '_LOG_BROWSER'], 1);
		
	%------------------------------------
	% DATA BROWSER FIGURES
	%------------------------------------
	
	case 'data'
		
		out = match_tag_prefix(in, [app '_DATA_BROWSER'], 1);
	
	%------------------------------------
	% PALETTES
	%------------------------------------
	
	case 'palette'
	
		out = match_tag_prefix(in, [app '_PALETTE'], 1);
		
	%--
	% BROWSER PALETTES
	%--
	
	case 'browser_palettes'
		
		out = match_tag_prefix(in, [app '_PALETTE'], 1);
		
		if isempty(out)
			return;
		end
		
		name = get(out, 'name');
		
		% NOTE: 'browser_palettes' outputs the list of browser palette names
		
		[ignore, ix] = intersect(name, browser_palettes); %#ok<ASGLU>
		
		if isempty(ix)
			out = []; return;
		end
		
		out = out(ix);
		
	%--
	% EXTENSION PALETTES
	%--
	
	case 'extension_palettes'
		
		out = match_tag_prefix(in, [app '_PALETTE'], 1);
		
		if isempty(out)
			return;
		end
		
		name = get(out, 'name');
		
		% NOTE: 'browser_palettes' outputs the list of browser palette names
		
		[ignore, ix] = setdiff(name, browser_palettes); %#ok<ASGLU>
		
		if isempty(ix)
			out = []; return;
		end
		
		out = out(ix);
		
	%------------------------------------
	% WAITBARS
	%------------------------------------
	
	case 'waitbar'
	
		out = match_tag_prefix(in, [app '_WAITBAR'], 1);
		
	%------------------------------------
	% WIDGET
	%------------------------------------
	
	case 'widget'
	
		out = match_tag_prefix(in, [app '_WIDGET'], 1);
		
	%------------------------------------
	% EXPLAIN
	%------------------------------------
	
	case {'detector-view', 'explain'}
	
		out = match_tag_prefix(in, [app '_WIDGET::SOUND_DETECTOR'], 1);
		
	case {'feature-view', 'view'}
	
		out = match_tag_prefix(in, [app '_WIDGET::SOUND_FEATURE'], 1);
		
	%------------------------------------
	% SELECTION
	%------------------------------------
	
	case 'selection'
	
		out = match_tag_prefix(in, [app '_SELECTION'], 1);

	%--
	% unrecognized figure type
	%--
	
	otherwise
		
		error(['Unrecognized ', app, ' figure type ''', type, '''.']);
		
end


%---------------------------------------------------------
% PARENT_SELECT
%---------------------------------------------------------

function out = parent_select(app, in, par)

% parent_select - select figures based on parent
% ----------------------------------------------
%
% out = parent_select(in, par)
%
% Input:
% ------
%  in - potential children
%  par - parent
%
% Output:
% -------
%  out - children

% NOTE: we could also check the parent fields of the registered figures

%--
% get parent recognized children
%--

data = get(par, 'userdata');

try
	out = [data.browser.palettes(:)', data.browser.children(:)'];
catch
	out = [];
end

%--
% consider inputs that are recognized
%--

out = intersect(in, out);

%--
% add neglected children to output
%--

% NOTE: these figures declare a parent even though they are not registered

in = in(~string_begins(get(in, 'tag'), [app '_SOUND_BROWSER']));

for k = 1:length(in)
	
	%--
	% try to get parent from potential child
	%--
	
	data = get(in(k), 'userdata');
	
	try
		par2 = data.parent;
	catch
		try
			par2 = data.browser.parent;
		catch
			continue;
		end
	end
	
	%--
	% add neglected child to list of children
	%--
	
	if (par == par2)
		out(end + 1) = in(k);
	end
	
end

%--
% we probably have added duplicates return unique values
%--

out = unique(out);


%---------------------------------------------------------
% CHILD_SELECT
%---------------------------------------------------------

function out = child_select(app, in, child) %#ok<INUSL>

% child_select - select figure based on child
% -------------------------------------------
%
% out = child_select(in, child)
%
% Input:
% ------
%  in - figures to select from
%  child - child figure
%
% Output:
% -------
%  out - selected figures

%--
% get child registered parent
%--

data = get(child, 'userdata');

% TODO: there are two places where this information is stored, change this

try
	out = data.parent;
catch
	try 
		out = data.browser.parent;
	catch
		out = [];
	end
end

