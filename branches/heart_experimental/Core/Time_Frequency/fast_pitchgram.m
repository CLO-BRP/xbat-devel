function [B, freq, time] = fast_pitchgram(X, rate, out, parameter)

% fast_pitchgram - short time fourier of fourier transform
% --------------------------------------------------------
% 
% [B, freq, time] = fast_pitchgram(X, rate, out, parameter)
%
% NOTE: use 'help fast_specgram'

%--
% handle input
%--

if nargin < 4
	parameter = fast_specgram;
end

if nargin < 3
	out = 'norm';
end

%--
% compute spectrogram
%--

[B, freq, time] = fast_specgram(X, rate, out, parameter);

B = normalize_dim(B);

%--
% compute 'pitchgram'
%--


N = size(B, 1); nyq = 0.5 * rate;

x = nyq ./ (10 + (1:N)'); freq = linspace(0, 0.25 * nyq, N)';

fig; plot(x); hold on; plot(freq);

if ~iscell(B)
	B = dct(B); B = interp1(x, B, freq);
else
	for k = 1:length(B)
		B{k} = dct(B{k}); B{k} = interp1(x, flipud(B{k}), freq);
	end
end
