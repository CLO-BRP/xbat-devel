function file = create_spectrogram_movie(sound, page, parameter)

%--
% handle input
%--

if nargin < 3
	parameter = fast_specgram; parameter.fft = 1024; parameter.hop = 0.06125;
end

if nargin < 2
	page.duration = 4; page.frame_step = 0.025;
end

if ~nargin
	sound = get_selected_sound;
end

if isempty(sound)
	return; 
end

%--
% initialize display and movie
%--

fig = figure; set(fig, 'DoubleBuffer', 'on'); 

ax = axes('parent', fig, 'position', [0 0 1 1]);

mov = avifile('example.avi');

%--
% compute movie frame spectrograms
%--

duration = get_sound_duration(sound); rate = get_sound_rate(sound);

frames = (duration - page.duration) / page.frame_step;

for k = 1:frames
	
	disp(['creating frame ', int2str(k)]);
	
	% TODO: read sound compute spectrogram and add frame to movie
	
	X = sound_read(sound, 'time', k * page.frame_step, page.duration, 1);
	
	X = flipud(fast_specgram(X, rate, 'norm', parameter));
	
	if k == 1
		im = imagesc(X); truesize;
	else
		set(im, 'cdata', X);
	end
	
	colormap(flipud(gray)); cmap_scale; drawnow;
	
	fra = getframe(ax); mov = addframe(mov, fra);
	
end

%--
% finalize movie
%--

mov = close(mov);
