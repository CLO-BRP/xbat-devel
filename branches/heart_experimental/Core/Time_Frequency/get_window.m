function [h, n] = get_window(parameter)

% get_window - compute window from spectrogram parameters
% -------------------------------------------------------
%
% [h, n] = get_window(parameter)
%
% Input:
% ------
%  parameter - spectrogram parameters used by fast_specgram
%
% Output:
% -------
%  h - window vector
%  n - actual window length, no padding
%
% See also: fast_specgram

%--
% rename fft and window length
%--

N = parameter.fft;

n = round(N * parameter.win_length); 

%--
% get window function and test for parameter
%--

fun = window_to_fun(parameter.win_type);

if isempty(fun)
	error(['Unrecognized window type ''', parameter.win_type, '''.']);
	
% 	parameter.win_type = 'hann'; fun = window_to_fun(parameter.win_type);
end

param = window_to_fun(parameter.win_type, 'param');

%--
% compute base window
%--

if isempty(param)
	h = fun(n);
else
	h = fun(n, parameter.win_param);
end

%--
% zero pad window to fft length if needed
%--

if n < N
	h((n + 1):N) = 0;
end



