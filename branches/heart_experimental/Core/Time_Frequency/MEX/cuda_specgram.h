//---------------------------------------------------
// TYPE ALIASING
//---------------------------------------------------

#ifdef FLOAT

#define MX_CLASS mxSINGLE_CLASS
#define C_TYPE float
#define CUFFT_TYPE cufftComplex
#define CUFFT_PLAN_TYPE CUFFT_C2C
#define CUFFT_EXECUTE cufftExecC2C

#else /* FLOAT */

#define MX_CLASS mxDOUBLE_CLASS
#define C_TYPE double
#define CUFFT_TYPE cufftDoubleComplex
#define CUFFT_PLAN_TYPE CUFFT_Z2Z
#define CUFFT_EXECUTE cufftExecZ2Z

#endif /* FLOAT */

//----------------------------------------------------
// HOST FUNCTION PROTOTYPE
//----------------------------------------------------

extern "C" cudaError_t cuda_specgram(C_TYPE *d_real, C_TYPE *d_imag, C_TYPE *d_data, unsigned int N, C_TYPE *d_func,
		                      unsigned int w, unsigned int o, unsigned int ts, int comp_type, int maxGridSizeX);
