//------------------------------------
// Spectragram on overlapping windows.
//------------------------------------

#include <cuda.h> 
#include <cufft.h>
#include "cuda_specgram_stream.h"

//--------------------------------------------------------------
// Copy data into windows for transform in place (single thread)
//--------------------------------------------------------------

__global__ void windowKernel_serial(cufftComplex *d_fft, float *d_data, unsigned int numTransforms, unsigned int transformSize, unsigned int w, unsigned int o)
{
	unsigned int i, j;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < transformSize; j++)
		{
			d_fft[i*transformSize + j].x = d_data[i*(w-o) + j];
			d_fft[i*transformSize + j].y = 0.0;
		}
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//---------------------------------------------------------------

__global__ void windowKernel_single(cufftComplex *d_fft, float *d_data, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//   and apply window function
//---------------------------------------------------------------

__global__ void windowKernelFunc_single(cufftComplex *d_fft, float *d_data, float *d_func, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx] * d_func[inc];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
		inc += MAX_THREADS;
	}
}

//----------------------------------------------------
// Compute magnitude of complex values (single thread)
//----------------------------------------------------

__global__ void magnitudeKernel_serial(float *d_mag, cufftComplex *d_fft, unsigned int numTransforms, unsigned int transformSize)
{
	unsigned int i, j;
	unsigned int hop = (transformSize >> 1) + 1;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < hop; j++)
		{
			d_mag[i * hop + j] = sqrtf(d_fft[i * transformSize + j].x * d_fft[i * transformSize + j].x +
					                   d_fft[i * transformSize + j].y * d_fft[i * transformSize + j].y) / transformSize;
		}
	}
}

//-----------------------------------------------------
// Compute magnitude of complex values (multi threaded)
//-----------------------------------------------------

__global__ void magnitudeKernel_single(float *d_Mag, cufftComplex *d_FFT, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int midx = blockIdx.x * ((transformSize>>1) + 1) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = ((transformSize>>1) + 1 - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Mag[midx] = sqrtf(d_FFT[fidx].x * d_FFT[fidx].x + d_FFT[fidx].y * d_FFT[fidx].y) / (float) transformSize;
		midx += MAX_THREADS;
		fidx += MAX_THREADS;
	}
}

//---------------------------------------------
// Compute FFT of windowed data
//---------------------------------------------

extern "C" void cuda_specgram_stream_single(float *h_transforms, float *h_data, unsigned int numSamples, unsigned int w, unsigned int o, unsigned int transformSize);

void cuda_specgram_stream_single(float *h_transforms, float *h_data, unsigned int numSamples, unsigned int w, unsigned int o, unsigned int transformSize)
{
	// Inputs
	float			*l_data;
	float			*d_data;
	
	// Outputs
	float			*l_transforms;
	float			*d_transforms;
	
	// Intermediate values
	cufftComplex	*d_fft;
	
	// CUDA streams
	cudaStream_t	cStreams[NUM_STREAMS];
	int				s;
	
	// Indices and offsets
	unsigned int	numTransforms;
	unsigned int	numTransformsPerStream;
	unsigned int	offset;
	unsigned int	numBytes;
	unsigned int	moffset, foffset, doffset;
	unsigned int	blocks;

	// CUFFT
    cufftHandle		fp;
	
	
	// Allocate and fill page-locked input buffer
	
	cudaMallocHost((void **) &l_data, numSamples * sizeof(float));
	
	memcpy((void *) l_data, (void *) h_data, numSamples * sizeof(float));
	
	// Allocate device memory for input

	cudaMalloc((void **) &d_data, numSamples * sizeof(float));

	// Compute number of transforms
	
	numTransforms = (numSamples - w) / (w - o) + 1;
	
	// Allocate page-locked output buffer
	
	cudaMallocHost((void **) &l_transforms, numTransforms * (transformSize/2+1) * sizeof(float));
		
	// Allocate device memory for output
	
	cudaMalloc((void **) &d_transforms, numTransforms * (transformSize/2+1) * sizeof(float));
	
	// Allocate device memory to buffer FFT input and output
	
	cudaMalloc((void **) &d_fft, numTransforms * transformSize * sizeof(cufftComplex));
	
	// Initialize FFT library
	
    cufftPlan1d(&fp, transformSize, CUFFT_C2C, numTransforms);

	// Create stream descriptors
	
	for (s = 0; s < NUM_STREAMS; s++)
	{
		cudaStreamCreate(&cStreams[s]);
	}
	
	// Copy signal data to device

	offset = (numSamples + NUM_STREAMS - 1) / NUM_STREAMS;
	numBytes = offset * sizeof(float);
	for (s = 0; s < NUM_STREAMS; s++)
	{
		if (s == (NUM_STREAMS - 1) && (numSamples % NUM_STREAMS) != 0)
		{
			numBytes = (numSamples - offset * s) * sizeof(float);
		}
		cudaMemcpyAsync((void *) (d_data + offset * s), (void *) (l_data + offset * s),
				numBytes, cudaMemcpyHostToDevice, cStreams[s]);
	}

	// Invoke window kernel

	numTransformsPerStream = (numTransforms + NUM_STREAMS - 1) / NUM_STREAMS;
	doffset = numTransformsPerStream * (w - o);
	foffset = numTransformsPerStream * transformSize;
	blocks = numTransformsPerStream;
	for (s = 0; s < NUM_STREAMS; s++)
	{
		if (s == (NUM_STREAMS - 1) && (numTransforms % NUM_STREAMS) != 0)
		{
			blocks = numTransforms - numTransformsPerStream * s;
		}
		windowKernel_single<<<blocks, MAX_THREADS, 0, cStreams[s]>>>
				(d_fft + s * foffset, d_data + s * doffset, w, o, transformSize);
	}

	// Sync, all windowing kernels must complete before doing FFT

	cudaThreadSynchronize();

	// Compute FFT
	
	cufftExecC2C(fp, d_fft, d_fft, CUFFT_FORWARD);

	// Invoke magnitude kernel

	numTransformsPerStream = (numTransforms + NUM_STREAMS - 1) / NUM_STREAMS;
	moffset = numTransformsPerStream * (transformSize/2 + 1);
	foffset = numTransformsPerStream * transformSize;
	blocks = numTransformsPerStream;
	for (s = 0; s < NUM_STREAMS; s++)
	{
		if (s == (NUM_STREAMS - 1) && (numTransforms % NUM_STREAMS) != 0)
		{
			blocks = numTransforms - numTransformsPerStream * s;
		}
		magnitudeKernel_single<<<blocks, MAX_THREADS, 0, cStreams[s]>>>
				(d_transforms + s * moffset, d_fft + s * foffset, w, o, transformSize);
	}

	// Copy result from device

	numTransformsPerStream = (numTransforms + NUM_STREAMS - 1) / NUM_STREAMS;
	offset = numTransformsPerStream * (transformSize/2+1);
	numBytes = offset * sizeof(float);
	for (s = 0; s < NUM_STREAMS; s++)
	{
		if (s == (NUM_STREAMS - 1) && (numTransforms % NUM_STREAMS) != 0)
		{
			numBytes = (numTransforms - numTransformsPerStream * s) * (transformSize/2+1) * sizeof(float); 
		}
		cudaMemcpyAsync((void *) (l_transforms + offset * s), (void *) (d_transforms + offset * s),
				numBytes, cudaMemcpyDeviceToHost, cStreams[s]);
	}

	// Sync, data must all transfer from device to host before subsequent use

	cudaThreadSynchronize();

	// Copy result from page-locked buffer to Matlab variable

	memcpy((void *) h_transforms, (void *) l_transforms, numTransforms * (transformSize/2 + 1) * sizeof(float));

	// Free device memory
			
	cudaFree(d_data);
			
	cudaFree(d_transforms);
	
	cudaFree(d_fft);
	
	// Free host memory
	
	cudaFreeHost(l_data);
	
	cudaFreeHost(l_transforms);

	// Destroy streams
	for (s = 0; s < NUM_STREAMS; s++)
	{
		cudaStreamDestroy(cStreams[s]);
	}
	
	// CUFFT cleanup
	
    cufftDestroy(fp);
	
}