function value = show_other_sounds(value)

% show_other_sounds - set visibility mode of other sounds
% -------------------------------------------------------
% 
% state = show_other_sounds
%
% show_other_sounds(1) - show other sounds
%
% show_other_sounds(0) - hide other sounds

%--
% return value
%--

if nargin < 1
	
	value = get_env('show_other_sounds');
	
	% NOTE: handle empty value here in case 'startup' fails to set it
	
	if ~isempty(value)
		return;
	else
		value = 1;
	end
	
end

%--
% update value
%--

% NOTE: this allows command syntax

if ischar(value)
	value = eval(value);
end

if value
	set_env('show_other_sounds', 1);
else
	set_env('show_other_sounds', 0);
end