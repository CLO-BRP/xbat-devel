function par = update_filter_menu

% update_filter_menu - update filter menu for open browsers
% ---------------------------------------------------------
%
% par = update_filter_menu
%
% Output:
% -------
%  par - updated browser handles

%--
% re-create filter menus for open browsers
%--

par = get_xbat_figs('type', 'sound');

for k = 1:length(par)
	
	%--
	% find and delete existing menu
	%--
	
	menu = findobj(par(k), 'type', 'uimenu', 'label', 'Filter');
	
	% NOTE: we don't re-create the menu if is was not available
	
	if isempty(menu)
		continue;
	end
	
	% NOTE: we delete the children then the parent
	
	try
		delete(allchild(menu)); delete(menu); 
	catch 
		nice_catch(lasterror, 'Failed to delete all previous menus.');
	end
		
	%--
	% re-create menu updating filter stores
	%--

	browser_filter_menu(par(k));
	
end
