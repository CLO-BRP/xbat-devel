function fun = get_template(view,name)

% get_template - get view template handle
% ---------------------------------------
%
% fun = get_template(view,name)
%
% Input:
% ------
%  view - view name
%  name - template name
%
% Output:
% -------
%  fun - template function handle, empty if it does not exist

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--------------------------------
% SETUP
%--------------------------------

% NOTE: set convenient no fun default

fun = []; 

%--------------------------------
% GET TEMPLATE 
%--------------------------------

%--
% get view from view name
%--

view = get_view(view);

% NOTE: no fun if view is not available

if (isempty(view))
	return;
end 

%--
% get fun from view
%--

if (isfield(view.fun,name))
	fun = view.fun.(name);
end