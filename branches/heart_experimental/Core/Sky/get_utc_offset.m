function offset = get_utc_offset

% get_utc_offset - from system
% -----------------------------
%
% offset = get_utc_offset
%
% Output:
% -------
%  offset - in hours
%
% NOTE: this uses the java.util.Date object
%
% NOTE: the output is the number of hours to add to the local time to get UTC time


% TODO: the offsets computed by Java and the MEX have different sign

% NOTE: the offset is initially in minutes

current = java.util.Date; offset = double(current.getTimezoneOffset) / 60;

% offset = get_utc_offset_mex;	