function [flag, g] = log_open(par, p, strict)

% log_open - open log in browser figure
% -------------------------------------
%
% [flag, g] = log_open(par, p)
%
% Input:
% ------
%  par - handle to browser figure (def: gcf)
%  p - log path, or full location, or log
%  strict - warning option
%
% Output:
% -------
%  flag - open confirmation flag
%  g - handle of figure that has log open

%--
% handle input
%--

if nargin < 1 || isempty(par)
	par = get_active_browser;
end

if nargin < 2 || isempty(p)   
	info = parse_tag(get(par, 'tag')); p = logs_root(info.sound, info.library); 
end

if nargin < 3 || isempty(strict)
	strict = 1;
end

%--
% handle log or location
%--

if isstruct(p)
    log = p;
else
    log = open_log_file(p);
end

%--
% handle missing log
%--

if isempty(log)
    
    if strict
        error('could not find specified log file.');
    end

    g = []; flag = 0; return;
   
end
    
%--
% get final log file and path
%--

[p, fn] = fileparts(log.file);

%--
% check that log is not already open
%--

g = log_is_open(log);

if ishandle(g)
    flag = 0; return;
end

%--
% update path and file if log file has been moved or renamed
%--

if ~strcmp(log.file, fullfile(p, fn))
	
	log.file = fullfile(p, fn);
	
end

% NOTE: make visible double not logical

% TODO: consider setting visibility to 'on' on open

log.visible = double(log.visible);

% NOTE: make 'no patch' signature equal to zero not -1

if log.patch < 0
	log.patch = 0;
end

%--
% save changes to log
%--

log_save(log);

%--
% check that log corresponds to sound
%--

if ~log_is_compatible(log, get_browser(par, 'sound'))

	tmp = quest_dialog( ...
		['Selected log is not compatable with this sound. Attempt to open log ''', log.file, ''' in new window ?'], ...
		' XBAT Question > Open Log', ...
		'modal' ...
	);
	
	%--
	% open sound browser for log sound and open log
	%--
	
    if ~strcmp(tmp, 'Yes')
		return;
    end

    sound = get_library_sounds(get_active_library, 'name', sound_name(log.sound)); 
    
    if isempty(sound)
        return;
    end
    
	par = browser(sound);

	if ~isempty(par)
		log_open(par, log);
	end

	return;
	
end

%--
% append log to browser data structure
%--

data = get_browser(par);

if isempty(data.browser.log)
    
    data.browser.log = log; data.browser.log_active = 1;
    
else
    
    m = length(data.browser.log) + 1;
    
    data.browser.log(m) = log; data.browser.log_active = m;
    
end

set(par, 'userdata', data);

%--
% update browser menu
%--

browser_log_menu(par, 'Initialize');


% NOTE: updates between the lines will mostly be implemented through widget event in the future

%-----------------------

db_disp 'implement log open event'

widget = get_widgets(par, 'log__open')

%--
% perform log related palette updates
%--

% NOTE: this does not affect the state of the browser on open, only on close

update_log_palette(par, data);

update_extension_palettes(par, data);

%--
% update find events if needed
%--

update_find_events(par, [], data);

%-----------------------


%--
% update browser display
%--

browser_display(par, 'update');


%--------------------------------
% OPEN_LOG_FILE
%--------------------------------

function log = open_log_file(p)

% open_log_file - open a log file from (possibly incomplete) path
% ---------------------------------------------------------------

log = [];
    		
%--
% check for file existence.  open immediately if possible
%--

if exist(p, 'file') == 2
    log = log_load(p); return; 
end
    
%--
% otherwise look for directory

if exist(p, 'dir') == 7
    
    %--
	% try to get log file starting from specified directory
	%--
		
	pi = pwd; cd(p);
	
	[f, p] = uigetfile( ...
		{ ...
			'*.mat','MAT-Files (*.mat)'; ...
			'*.zip','ZIP-Files (*.zip)'; ...
			'*.*','All Files (*.*)' ...
		}, ...
		'Select XBAT Log File: ' ...
	);
	
    cd(pi);
 
    %--
	% return on cancel
	%--
    
	if ~f
		return;
	end

	%--
	% build log location string
	%--
    
    log = log_load([p, f]);
    
end



