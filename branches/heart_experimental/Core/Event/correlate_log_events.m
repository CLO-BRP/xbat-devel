function result = correlate_log_events(log, parameter, opt)

%----------------------
% HANDLE INPUT
%----------------------

%--
% set and possibly output default options
%--

if nargin < 3
	opt = image_corr; opt.pad_row = 0; opt.pad_col = 1;	
end

if ~nargin
	result = opt; return;
end

%--
% set spectrogram parameters
%--

if nargin < 2 || isempty(parameter)
	
	parameter = fast_specgram; 
	
	parameter.fft = 1024; parameter.hop = 0.125; parameter.win_length = 0.5;

end


%----------------------
% SETUP
%----------------------

%--
% copy parameters to sound
%--

% NOTE: the sound spectrogram parameters are used in 'event_specgram'

sound = log.sound; sound.specgram = parameter;

%----------------------
% COMPUTE
%----------------------

start = clock;

%--
% get relevant events
%--

% TODO: make event selection part of the input

% NOTE: this means that the result should indicate the identifier index map

id = get_log_event_field(log, 'id');

% score = get_log_event_field(log, 'score');

% id = id(score > 0.7);

% fig; hist(score, 101); title('score'); 

id = id(1:min(300, numel(id)));

event = log_get_events(log, id);

result.id = [event.id];

%--
% read samples and compute spectrograms
%--

% NOTE: we compute the first spectrogram so we can get the row selection indices

output = event_specgram(event(1), sound);

rows = output.specgram.rows; 

% NOTE: append a row to ensure odd sized spectrograms, this will double the top row if needed

select = rows(1):rows(2); if ~mod(numel(select), 2), select(end + 1) = select(end); end

for k = 1:numel(event)
	
	%--
	% compute spectrograms, then select relevant rows, center, and normalize
	%--
	
	output = event_specgram(event(k), sound); 
	
	current = output.specgram.value(select, :);
	
	current = current - mean(current(:));
	
	current = current ./ sqrt(sum(current(:).^2));
	
	% NOTE: append a column to ensure odd sized spectrograms
	
	[rows, cols] = size(current); if ~mod(cols, 2), current(:, end + 1) = 0; end
	
	spec{k} = current; 
	
	disp(['spec ', int2str(k), ' of ', int2str(numel(event))]);
	
end
	
db_disp 'spectrograms computed'; elapsed = etime(clock, start)

%--
% correlate spectrograms
%--

band = 1000; result.C = sparse(numel(event), numel(event));

% handle = fig;

for row = 1:numel(event)

% 	set(handle, 'name', ['EVENT #', int2str(event(row).id)]);
	
	local = clock;
	
	start = row + 1; stop = min(row + band, numel(event));
	
% 	disp(['starting row ', int2str(row), ' comparing from ', int2str(start), ' to ', int2str(stop)]);
	
	k = 1; dur = event(row).duration;
	
	for col = start:stop
		
		R = image_corr(spec{row}, spec{col}, opt); 
		
		% TODO: display and determine problems here with large values
		
% 		figure(handle); 
% 		
% 		subplot(5,2,k); 
% 		
% 		plot(R); [mva, mix] = max(R); hold on; plot(mix, mva, 'ro'); 
% 		
% 		title(['VS EVENT #', int2str(event(col).id), ' ', num2str(mva), '/', num2str(dur / event(col).duration)]);
% 		
% 		k = k + 1;
		
		result.C(row, col) = real(max(R));
		
	end 
	
	disp(['finished row ', int2str(row), ' of ', int2str(numel(event)), ', elapsed = ', num2str(etime(clock, local))]);
% 	disp(' ');

% 	pause; clf(handle);
	
end

% db_disp 'correlations computed'; elapsed = etime(clock, start)
