function tag = event_tag(event, log)

% event_tag - get the display tag for an event
% --------------------------------------------
%
% tag = event_tag(event, log)
%
% Input:
% ------
%  event - event or event id
%  log - log or log name
%
% Output:
% -------
%  tag - the tag

%--
% handle log or log name input
%--

if ~ischar(log)
    log = log_name(log);
end

%--
% iterate for multiple events
%--

if numel(event) > 1
	tag = iterate(mfilename, event, log); return;
end

%--
% handle event or event id
%--

switch class(event)
	
	case 'cell'
		id = event{1};
		
	case 'struct'
		id = event.id;
		
	otherwise
		id = event;
		
end

if isnumeric(id)
    id = num2str(id);
end

%--
% assemble the tag
%--

tag = ['EVENT::', log, '::', id];
