function level = event_level(event, full)

% event_level - compute event level
% ---------------------------------
%
% level = event_level(event)
%
% Input:
% ------
%  event - event
%  full - force full evaluation of level, relevant for hierarchical events
%
% Output:
% -------
%  level - level

if nargin < 2
	full = 0;
end

%--
% handle multiple events
%--

if numel(event) > 1
	level = iterate(mfilename, event, full); return;
end 

%--
% handle hierarchical events, events with children
%--

count = event_has_children(event);

if count
	
	for k = 1:count
		level(k) = event_level(event.children(k));
	end 
	
	level = max(level) + 1; return;

end

%--
% events with no children are first level
%--

level = 1; 

