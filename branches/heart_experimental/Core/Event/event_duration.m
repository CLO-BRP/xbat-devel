function duration = event_duration(event, full)

% event_duration - get event duration
% -----------------------------------
%
% duration = event_duration(event, full)
%
% Input:
% ------
%  event - event 
%  full - force full evaluation of duration, relevant for hierarchical events
%
% Output:
% -------
%  duration - event duration

if nargin < 2
	full = 0;
end

duration = diff(event_time(event, full), 1, 2);
