function browser_event_delete(par, log, id, data)

% event_delete - delete events given log names and ids
% -----------------------------------------------------
%
% event_delete(par, log, id, data)
%
% Input:
% ------
%  par - the browser
%  log - list of log names or logs
%  id - list of event ids
%  data - browser state

%--
% handle input
%--

if isempty(par)
    par = get_active_browser;
end

if nargin < 4 || isempty(data)
    data = get_browser(par);
end

if isstruct(log)
    names = log_name(log);
else
    names = log;
end

if ischar(names)
    names = {names};
end

%--
% confirm deletion
%--

if numel(id) > 1
	str = ['Delete the ', int2str(numel(id)), ' selected events ?'];
else
	str = 'Delete the selected event ?';
end

% NOTE: we permute the button positions to protect against habit
	
tmp = {'Yes', 'No', 'Cancel'};

tmp = tmp(randperm(3));
	
action = quest_dialog(str, '  Delete Event(s) ...', tmp{:}, 'No');

if ~strcmp(action, 'Yes')
	return;
end

%--
% delete events
%--

for name = list(unique(names))
    
    log = get_browser_log(par, name, data); ix = strcmp(name, names);
    
    log_delete_events(log, id(ix));
    
end

%--
% update browser display and event palette
%--

% TODO: we could check to see if there are any visible events in the current page

browser_display(par, 'events', data);

update_find_events(par, [], data);


