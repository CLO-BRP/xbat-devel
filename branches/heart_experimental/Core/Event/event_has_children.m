function count = event_has_children(event)

% event_has_children - get children count for event
% -------------------------------------------------
%
% count = event_has_children(event)
%
% Input:
% ------
%  event - event
%
% Output:
% -------
%  count - direct children count, serves as indicator as well

% TODO: develop recursive count to get all children, perhaps 'event_descendants'

%--
% handle multiple events
%--

if numel(event) > 1
	count = iterate(mfilename, event); return;
end

%--
% count children
%--

count = numel(event.children);