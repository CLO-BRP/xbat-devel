function event = map_events(event, sound, from, to)

% map_events - map time of a collection of events
% -----------------------------------------------
%
% event = map_events(event, sound, from, to)
%
% Input:
% ------
%  event - events
%  sound - sound
%  from - origin timebase
%  to - destination timebase
%
% Output:
% -------
%  event - mapped events


if nargin < 4 || isempty(to)
    to = 'record';
end

if nargin < 3 || isempty(from)
    from = 'slider';
end

for k = 1:length(event)

    time = event(k).time;

    %--
    % map start time to absolute recording time
    %--

    start_time = map_time(sound, to, from, time(1));

    time = [start_time, start_time + diff(time)];

    event(k).time = time;

end