function d = distance(x, A)

% NOTE: we typically avoid computing distances between two sets of points

%--
% get and check sizes
%--

[mx, nx] = size(x); [mA, nA] = size(A);

if mx ~= 1
	error('The first input must be a single point.');
end

if nx ~= nA
	error('Points in distance computation must live in same space.');
end

%--
% compute distance or distances
%--

if mA == 1
	d = sqrt(sum((x - A).^2));
else
	d = sqrt(sum((repmat(x, mA, 1) - A).^2, 2));
end
