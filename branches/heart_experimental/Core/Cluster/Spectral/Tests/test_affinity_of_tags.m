function R = test_affinity_of_tags(log)

%--
% handle input
%--

if ~nargin
	
	log = get_active_log; 
	
	if isempty(log)
		log = get_selected_log;
	end
	
	if isempty(log) 
		R = []; return; 
	end 
	
end

%--
% setup
%--

% par = fig; ax = axes('parent', par);

%--
% get events by tag, create a matrix as in the theorem
%--

sets = get_training_sets(log);

events = [];

for k = 1:length(sets)
	events = [events; sets(k).event];
end

%--
% correlate events
%--

disp(' ');

% NOTE: we set reasonable spectrogram parameters

sound = log.sound;

sound.specgram = fast_specgram; sound.specgram.win_length = 0.25;

R = zeros(numel(events));

for i = 1:numel(events)
	
	start = clock;
	
	row = get_event_specgram(events(i), sound);
	
	for j = i + 1:numel(events)
		
		column = get_event_specgram(events(j), sound);
		
		values = correlate(row, column);
		
		ix = fast_peak_valley(values, 1);
		
		if isempty(ix)
			r = max(values);
		else
			r = max(values(ix));
		end
		
		R(i,j) = 1 - r;
		
% 		plot(ax, values); hold on; plot(ix, values(ix), 'ro'); drawnow; hold off;
		
	end
	
	disp(['finished row ', int_to_str(i), ' ', sec_to_clock(etime(clock, start))]);
	
end

size(R)

R = R + R';


%---------------------------
% GET_EVENT_SPECGRAM
%---------------------------

function specgram = get_event_specgram(event, sound)

% NOTE: the general function is works with events

event = event_specgram(event, sound); 

specgram = event.specgram.value;


%---------------------------
% CORRELATE
%---------------------------

function values = correlate(row, column)

% NOTE: use the smaller image as filter

if size(row, 2) < size(column, 2)
	[row, column] = deal(column, row);
end

% NOTE: make sure the filter is odd-width

if ~mod(size(column, 2), 2)
	column(:, end + 1) = 0;
end

% TODO: add required normalization and background subtraction

opt = image_corr; 
	
opt.pad_row = 0; opt.pad_col = 1; % opt.mask = 0;
	
values = image_corr(column, row, opt);


