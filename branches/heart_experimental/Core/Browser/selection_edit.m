function selection_edit(mode, ctrl, log, id)

% selection_edit - edit selections and events
% -------------------------------------------
%
%  selection_edit('start', ctrl)
%
%  selection_edit('move', ctrl)
%
%  selection_edit('stop', ctrl)
%
%  selection_edit('start', ctrl, m, ix)
%  
%  selection_edit('move', ctrl, m, ix)
%  
%  selection_edit('stop', ctrl, m, ix)
%
% Input:
% ------
%  ctrl - control point used for editing
%  log - name
%  id - of event

% TODO: this could use some refactoring, something of an understatement

%---------------------------------------------------------------
% PERSISTENT VARIABLES
%---------------------------------------------------------------

%--
% edit display
%--

persistent PTR;

persistent POINTER_NAMES;

persistent M_PAR M_AX PAR AX FIGURE_DATA SELECTION_HANDLES;

persistent AXES_XLIM AXES_YLIM;

persistent SEL_XLIM SEL_YLIM;

persistent EDIT_HANDLE EDIT_XDATA EDIT_YDATA;

persistent FLIP TEMP_EVENT;

persistent MOVE_FLAG;

%--
% selection zoom display
%--

persistent SELECTION_FIGURE;

persistent OPT_START OPT_MOVE OPT_RESIZE;

%--
% widget variables
%--

persistent WIDGETS LISTENER_CONTEXT;

%--
% switch on mode
%--

switch mode
	
%---------------------------------------------------------------
% START EDIT
%---------------------------------------------------------------

case 'start'
	
	%--
	% create persistent table of pointer names
	%--
		
	if isempty(POINTER_NAMES)
		
		%------------------------------------------------------
		% pointer list for the various control points
		%------------------------------------------------------
		
		POINTER_NAMES = { ...
			'right', 'topr', 'top', 'topl', 'left', 'botl', 'bottom', 'botr', 'fleur', 'fleur' ...
		};
	
		%------------------------------------------------------
		% selection display update options
		%------------------------------------------------------
		
		% NOTE: the default options only resize
		
		opt = selection_display;
		
		%--
		% edit start options
		%--
		
		OPT_START = opt; 
		
		OPT_START.show = 1;
		
		%--
		% edit move options
		%--
		
		OPT_MOVE = opt;
		
		OPT_MOVE.resize = 0;
		
		%--
		% edit resize options
		%--
		
		OPT_RESIZE = opt;
		
	end
	
	%--
	% create persistent copy of relevant information from userdata
	%--
	
	M_PAR = gcbf;
	
	% TODO: the second clause should be more robust
	
	if is_browser(M_PAR)
		PAR = M_PAR;
	else
		data = get(M_PAR, 'userdata'); PAR = data.parent;
	end
	
	FIGURE_DATA = get(PAR, 'userdata');
	
	SELECTION_HANDLES = FIGURE_DATA.browser.selection.handle;
	
	set(SELECTION_HANDLES(7:10), 'clipping', 'on');
	
	%--
	% get needed axes limits
	%--
	
	M_AX = gca;
	
	%--
	% set editing button motion and button up functions
	%--
		
	% TODO: separate cases into functions and use function handles
	
	if nargin < 3
		
		%--
		% set selection editing callbacks
		%--

		set(M_PAR, ...
			'WindowButtonMotionFcn', {@selection_edit_callback, 'move', ctrl}, ...
			'WindowButtonUpFcn', {@selection_edit_callback, 'stop', ctrl} ...
		);
	
	else 
		
		%--
		% set event selection editing callbacks
		%--

		set(M_PAR, ...
			'WindowButtonMotionFcn', {@selection_edit_callback, 'move', ctrl}, ...
			'WindowButtonUpFcn', {@selection_edit_callback, 'stop', ctrl, log, id} ...
		);
	
	end
	
	%--
	% get starting pointer
	%--
	
	PTR = get(M_PAR, 'pointer');
	
	%--
	% set pointer according to control point
	%--
	
	if ctrl < 9
		
		%--
		% boundary control point selection editing
		%--
		
		set(M_PAR, 'pointer', POINTER_NAMES{ctrl});
	
	else
		
		%--
		% selection movement editing
		%--
		
		% NOTE: check for horizontal or vertical motion constraint
		
		tmp = double(get(M_PAR, 'currentcharacter'));

		if isempty(tmp)
			tmp = double(' ');
		end

		switch tmp

			case double('h'), set(M_PAR, 'pointer', 'left');

			case double('v'), set(M_PAR, 'pointer', 'top');

			otherwise, set(M_PAR, 'pointer', 'fleur');

		end

	end

	%--
	% create persistent drag line representation of selection
	%--
	
	% NOTE: this same dragline could be used to replace the rubber band box
	
	AX = get(SELECTION_HANDLES(1), 'parent');
	
	EDIT_XDATA = get(SELECTION_HANDLES(1), 'xdata');
	
	EDIT_YDATA = get(SELECTION_HANDLES(1), 'ydata');
			
	EDIT_HANDLE = line( ...
		'parent', AX, ...
		'xdata', EDIT_XDATA, ...
		'ydata', EDIT_YDATA, ... 
		'color', FIGURE_DATA.browser.selection.color, ...
		'linestyle', ':', ...
		'linewidth', 0.5 ...
	);

	%--
	% find and store session boundaries that we can't cross
	%--
	
	sessions = get_sound_sessions(FIGURE_DATA.browser.sound);
	
	borders = get_session_borders(sessions, EDIT_XDATA);
	
	AXES_XLIM = get(AX, 'xlim'); 
	
	AXES_YLIM = get(AX, 'ylim');
	
	SEL_XLIM = AXES_XLIM;
	
	if ~isempty(borders)
		SEL_XLIM = [max(borders(1), SEL_XLIM(1)), min(borders(2), SEL_XLIM(2))];
	end
	
	SEL_YLIM = AXES_YLIM;
	
	if strcmpi(POINTER_NAMES{ctrl}, 'fleur')

		half_width = 0.5 * abs(diff(fast_min_max(EDIT_XDATA)));

		SEL_XLIM = [ ...
			SEL_XLIM(1) + half_width, SEL_XLIM(2) - half_width ...
		];

		half_height = 0.5 * abs(diff(fast_min_max(EDIT_YDATA)));
		
		SEL_YLIM = [ ...
			SEL_YLIM(1) + half_height, SEL_YLIM(2) - half_height ...
		];
		
	end

	%--
	% create persistent variables for dynamic updating of labels
	%--

	 % NOTE: watch out for the frequency scaling
	
	FLIP = 0;
	
	TEMP_EVENT = event_create( ...
		'id', FIGURE_DATA.browser.selection.event.id, ...
		'channel', FIGURE_DATA.browser.selection.event.channel, ...
		'time', [EDIT_XDATA(1), EDIT_XDATA(2)], ...
		'freq', [EDIT_YDATA(2), EDIT_YDATA(3)] ...
	);
	
	%--
	% handle axes frequency scaling
	%--
	
	if strcmp(FIGURE_DATA.browser.grid.freq.labels, 'kHz')
		TEMP_EVENT.freq = 1000 * TEMP_EVENT.freq;
	end
	
	%--
	% set move flag
	%--
	
	MOVE_FLAG = 0;
	
	%--
	% update selection controls
	%--
	
	% TODO: implement this using the new palette listeners
	
	set_selection_controls(PAR, TEMP_EVENT, 'start', FIGURE_DATA);
	
	%--------------------
	
	% TODO: implement this as a widget
	
	%--
	% see if the selection display figure exists
	%--
	
	SELECTION_FIGURE = get_xbat_figs('parent', PAR, 'type', 'selection');
	
	%--
	% create persistent copy of selection zoom handles and state
	%--
		
	% NOTE: the SELECTION_FIGURE input may be empty, however the output should not
	
	SELECTION_FIGURE = selection_display(PAR, TEMP_EVENT, OPT_START, FIGURE_DATA, SELECTION_FIGURE);
	
	%--------------------
	
	%--
	% update widgets
	%--
	
	LISTENER_CONTEXT = get_extension_context([], PAR, FIGURE_DATA);
	
	update_widgets(PAR, 'selection__edit__start', FIGURE_DATA, LISTENER_CONTEXT);
	
	WIDGETS = get_widgets(PAR, 'selection__edit__update');
	
	
	% TODO: implement PALETTE LISTENERS
	
	
	%--
	% refresh figure
	%--
	
	% NOTE: this code is taken from the built-in refresh function
	
	tmp = get(M_PAR, 'color');
	
	set(M_PAR, 'color', [0 0 0], 'color', tmp);
	
%---------------------------------------------------------------
% MOVE EDIT
%---------------------------------------------------------------

case 'move'
	
	%--
	% set move flag
	%--
	
	MOVE_FLAG = 1;
	
	%--
	% get current point in axes
	%--
	
	p = get(M_AX, 'CurrentPoint'); p = p(1, 1:2);
	
	%--
	% handle boundary conditions during motion
	%--
			
	tol = 0.01;
	
	if p(1) <= SEL_XLIM(1) + tol
		p(1) = SEL_XLIM(1) + tol;
	end
	
	if p(1) >= SEL_XLIM(2) - tol
		p(1) = SEL_XLIM(2) - tol;
	end
	
	tol = 0;
	
	if p(2) <= SEL_YLIM(1) + tol
		p(2) = SEL_YLIM(1) + tol;
	end
	
	if p(2) >= SEL_YLIM(2) - tol
		p(2) = SEL_YLIM(2) - tol;
	end
	
	%---------------------------------------------------------------
	% UDPATE DISPLAY ACCORDING TO CONTROL
	%---------------------------------------------------------------
	
	switch ctrl
		
		%--
		% EAST control point
		%--
		
		case 1
			EDIT_XDATA(2:3) = p(1);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA);
			
		%--
		% NORTH-EAST control point
		%--
		
		case 2
			EDIT_XDATA(2:3) = p(1);
			EDIT_YDATA(3:4) = p(2);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
			
		%--
		% NORTH control point
		%--
		
		case 3
			EDIT_YDATA(3:4) = p(2);
			set(EDIT_HANDLE,'ydata',EDIT_YDATA);
			
		%--
		% NORTH-WEST control point 
		%--
		
		case 4
			EDIT_XDATA([1,4,5]) = p(1);
			EDIT_YDATA(3:4) = p(2);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
			
		%--
		% WEST control point
		%--
		
		case 5
			EDIT_XDATA([1,4,5]) = p(1);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA);
			
		%--
		% SOUTH-WEST control point
		%--
		
		case 6
			EDIT_XDATA([1,4,5]) = p(1);
			EDIT_YDATA([1,2,5]) = p(2);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
		
		%--
		% SOUTH control point
		%--
		
		case 7
			EDIT_YDATA([1,2,5]) = p(2);
			set(EDIT_HANDLE,'ydata',EDIT_YDATA);
		
		%--
		% SOUTH-EAST control point
		%--
		
		case 8
			EDIT_XDATA(2:3) = p(1);
			EDIT_YDATA([1,2,5]) = p(2);
			set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
	
		%--
		% CENTER control point
		%--
		
		case {9, 10}

			% TODO: evaluate how these modes are accesed and whether they are needed
			
			switch get(gcf, 'pointer')
				
				% horizontal motion	
				
				case 'left'
					EDIT_XDATA = EDIT_XDATA + (p(1) - (EDIT_XDATA(1) + EDIT_XDATA(2)) / 2);
					set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
				
				% vertical motion
				
				case 'top'
					EDIT_YDATA = EDIT_YDATA + (p(2) - (EDIT_YDATA(2) + EDIT_YDATA(3)) / 2);
					set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
					
				% unconstrained motion
				
				case 'fleur'
					EDIT_XDATA = EDIT_XDATA + (p(1) - (EDIT_XDATA(1) + EDIT_XDATA(2)) / 2);
					EDIT_YDATA = EDIT_YDATA + (p(2) - (EDIT_YDATA(2) + EDIT_YDATA(3)) / 2);
					set(EDIT_HANDLE,'xdata',EDIT_XDATA,'ydata',EDIT_YDATA);
					
			end	
			
	end
	
	%---------------------------------------------------------------
	% COMPUTE SELECTION FLIP STATE
	%---------------------------------------------------------------

	FLIP = 0;

	%--
	% add horizontal flip bit
	%--
	
	if EDIT_XDATA(1) > EDIT_XDATA(2)
		FLIP = FLIP + 1;
	end

	%--
	% add vertical flip bit
	%--
	
	if EDIT_YDATA(2) > EDIT_YDATA(3)
		FLIP = FLIP + 2;
	end

	%---------------------------------------------------------------
	% UPDATE TEMPORARY EVENT
	%---------------------------------------------------------------

	%--
	% update based on horizontal flip
	%--
	
	if (FLIP == 0 || FLIP == 2)
		TEMP_EVENT.time = [EDIT_XDATA(1), EDIT_XDATA(2)];
	else
		TEMP_EVENT.time = [EDIT_XDATA(2), EDIT_XDATA(1)];
	end

	%--
	% update based on vertical flip
	%--
	
	if (FLIP == 0 || FLIP == 1)
		TEMP_EVENT.freq = [EDIT_YDATA(2), EDIT_YDATA(3)];
	else
		TEMP_EVENT.freq = [EDIT_YDATA(3), EDIT_YDATA(2)];
	end

	%--
	% set frequency bounds considering out of axes motion
	%--
	
	if TEMP_EVENT.freq(1) < AXES_YLIM(1)
		TEMP_EVENT.freq(1) = AXES_YLIM(1);
		flag1 = 1;
	else
		flag1 = 0;
	end

	if TEMP_EVENT.freq(2) > AXES_YLIM(2)
		TEMP_EVENT.freq(2) = AXES_YLIM(2);
		flag2 = 1;
	else
		flag2 = 0;
	end

	OPT_MOVE.resize = max(flag1, flag2);
	
	%--
	% handle axes frequency scaling
	%--
	
	if strcmp(FIGURE_DATA.browser.grid.freq.labels, 'kHz')
		TEMP_EVENT.freq = 1000 * TEMP_EVENT.freq;
	end
		
	%---------------------------------------------------------------
	% UPDATE GRID LINES IF NEEDED
	%---------------------------------------------------------------

	if FIGURE_DATA.browser.selection.grid == 1

		%-----------------------------
		% update grid lines
		%-----------------------------
		
		if (FLIP == 0 || FLIP == 2)
			xl = [AXES_XLIM(1) EDIT_XDATA(1) nan EDIT_XDATA(2) AXES_XLIM(2)];
		else
			xl = [AXES_XLIM(1) EDIT_XDATA(2) nan EDIT_XDATA(1) AXES_XLIM(2)];
		end 
		
		if (FLIP == 0 || FLIP == 1)
			yl = [AXES_YLIM(1) EDIT_YDATA(2) nan EDIT_YDATA(3) AXES_YLIM(2)];
		else
			yl = [AXES_YLIM(1) EDIT_YDATA(3) nan EDIT_YDATA(2) AXES_YLIM(2)];
		end
		
		tmp = ones(1,5);
		
		set(SELECTION_HANDLES(3),'xdata',EDIT_XDATA(1) * tmp,'ydata',yl);
		
		set(SELECTION_HANDLES(4),'xdata',EDIT_XDATA(2) * tmp,'ydata',yl);
		
		set(SELECTION_HANDLES(5),'xdata',xl,'ydata',EDIT_YDATA(2) * tmp);
		
		set(SELECTION_HANDLES(6),'xdata',xl,'ydata',EDIT_YDATA(3) * tmp);

		%-----------------------------
		% update labels if displayed
		%-----------------------------
		
		if (FIGURE_DATA.browser.selection.label == 1)
			
			%--
			% create time and frequency label strings using temporary event
			%--
			
			[time, freq] = selection_labels(PAR, TEMP_EVENT, FIGURE_DATA);
			
			%-----------------------------
			% update grid labels 
			%-----------------------------
			
			%--
			% time labels
			%--
			
			if (FLIP == 0 || FLIP == 2)
				
				tmp = get(SELECTION_HANDLES(7),'position');		
				tmp(1) = EDIT_XDATA(1) - 0.0375;
				set(SELECTION_HANDLES(7),'position',tmp,'string',time{1});
					
				tmp = get(SELECTION_HANDLES(8),'position');
				tmp(1) = EDIT_XDATA(2) + 0.0375;
				set(SELECTION_HANDLES(8),'position',tmp,'string',time{4});
				
			else
				
				tmp = get(SELECTION_HANDLES(7),'position');		
				tmp(1) = EDIT_XDATA(2) - 0.0375;
				set(SELECTION_HANDLES(7),'position',tmp,'string',time{1});
					
				tmp = get(SELECTION_HANDLES(8),'position');
				tmp(1) = EDIT_XDATA(1) + 0.0375;
				set(SELECTION_HANDLES(8),'position',tmp,'string',time{4});

			end
		
			%--
			% frequency labels
			%--
			
			tt1 = TEMP_EVENT.time(1) - FIGURE_DATA.browser.time;
			tt2 = (FIGURE_DATA.browser.time + FIGURE_DATA.browser.page.duration) - TEMP_EVENT.time(2);
			
			mar = (0.0375 * (yl(end) - yl(1))) / FIGURE_DATA.browser.page.duration;
			
			if (tt1 <= tt2)
				
				tmp = get(SELECTION_HANDLES(9),'position');
				tmp(1) = xl(end) - 0.0375;
				
				if (FLIP == 0 || FLIP == 1)
					tmp(2) = EDIT_YDATA(2) - mar;
				else
					tmp(2) = EDIT_YDATA(3) - mar;
				end
				
				set(SELECTION_HANDLES(9), ...
					'position',tmp,'string',freq{1}, ...
					'HorizontalAlignment','right', ...
					'VerticalAlignment','top' ...
				);
				
				tmp = get(SELECTION_HANDLES(10),'position');
				tmp(1) = xl(end) - 0.0375;
				
				if (FLIP == 0 || FLIP == 1)
					tmp(2) = EDIT_YDATA(3) + mar;
				else
					tmp(2) = EDIT_YDATA(2) + mar;
				end
				
				set(SELECTION_HANDLES(10), ...
					'position',tmp,'string',freq{4}, ...
					'HorizontalAlignment','right', ...
					'VerticalAlignment','bottom' ...
				);
	
			else
				
				tmp = get(SELECTION_HANDLES(9),'position');
				tmp(1) = xl(1) + 0.0375;
				
				if (FLIP == 0 || FLIP == 1)
					tmp(2) = EDIT_YDATA(2) - mar;
				else
					tmp(2) = EDIT_YDATA(3) - mar;
				end
				
				set(SELECTION_HANDLES(9), ...
					'position',tmp,'string',freq{1}, ...
					'HorizontalAlignment','left', ...
					'VerticalAlignment','top' ...
				);
				
				tmp = get(SELECTION_HANDLES(10),'position');
				tmp(1) = xl(1) + 0.0375;
				
				if (FLIP == 0 || FLIP == 1)
					tmp(2) = EDIT_YDATA(3) + mar;
				else
					tmp(2) = EDIT_YDATA(2) + mar;
				end

				set(SELECTION_HANDLES(10), ...
					'position',tmp,'string',freq{4}, ...
					'HorizontalAlignment','left', ...
					'VerticalAlignment','bottom' ...
				);
				
			end
			
		end
	
	end 
	
	%--
	% update selection controls
	%--
	
	set_selection_controls(PAR, TEMP_EVENT, 'move', FIGURE_DATA);
	
	%--
	% refresh figure
	%--
	
	% NOTE: taken from refresh without various checks
	
	tmp = get(M_PAR,'color');
	
	set(M_PAR,'color',[0 0 0],'color',tmp);
	
	%--
	% update selection display
	%--
	
	% NOTE: the selection display window is not resized when moving

	if (ctrl == 9)
		selection_display(PAR, TEMP_EVENT, OPT_MOVE, FIGURE_DATA, SELECTION_FIGURE);
	else
		selection_display(PAR, TEMP_EVENT, OPT_RESIZE, FIGURE_DATA, SELECTION_FIGURE);
	end
	
	%--
	% update widgets
	%--
	
	% NOTE: this is too slow, we implement our own version with cached data
	
% 	update_widgets(PAR, 'selection__edit__update');
	
	FIGURE_DATA.browser.selection.event = TEMP_EVENT;
	
	data = get_widget_data([], 'selection__edit__update', FIGURE_DATA);
	
	for k = 1:length(WIDGETS)
		
		result = widget_update(PAR, WIDGETS(k), 'selection__edit__update', data, 0, LISTENER_CONTEXT);
		
		set_widget_status(WIDGETS(k), result);
		
	end
	
%---------------------------------------------------------------
% STOP EDIT
%---------------------------------------------------------------

case 'stop'
	
	%--
	% set empty button motion and button up functions
	%--
	
	set(M_PAR,'WindowButtonMotionFcn', '');
	
	set(M_PAR,'WindowButtonUpFcn', '');
	
	%--
	% set pointer back to arrow pointer
	%--
	
	set(M_PAR, 'Pointer', PTR);
	
	%--
	% check move flag in order to update
	%--
	
	if MOVE_FLAG
		
		%--
		% update EDIT_XDATA and EDIT_YDATA if selection was flipped
		%--
	
		if (EDIT_XDATA(1) > EDIT_XDATA(2))
			t1 = EDIT_XDATA(2);
			t2 = EDIT_XDATA(1);
			EDIT_XDATA = [t1,t2,t2,t1,t1];
		end
			
		if (EDIT_YDATA(2) > EDIT_YDATA(3))
			f1 = EDIT_YDATA(3);
			f2 = EDIT_YDATA(2);
			EDIT_YDATA = [f1,f1,f2,f2,f1];
		end
		
		%--
		% set selection frequency bounds to boundary if needed
		%--
		
		if (EDIT_YDATA(1) < AXES_YLIM(1))
			EDIT_YDATA([1,2,5]) = AXES_YLIM(1);
		end
		
		if (EDIT_YDATA(3) > AXES_YLIM(2))
			EDIT_YDATA([3,4]) = AXES_YLIM(2);
		end
		
		%--
		% logged event editing
		%--
		
		if nargin > 2
			
            log = get_browser_log([], log, FIGURE_DATA);
            
            event = log_get_events(log, id);
			
			%--
			% update time and frequency related fields
			%--
			
			event.time = [EDIT_XDATA(1), EDIT_XDATA(2)];
			
			if strcmp(FIGURE_DATA.browser.grid.freq.labels, 'kHz')
				event.freq = 1000 * [EDIT_YDATA(2), EDIT_YDATA(3)];
			else
				event.freq = [EDIT_YDATA(2), EDIT_YDATA(3)];
			end
			
			event.duration = diff(event.time);
			
			event.bandwidth = diff(event.freq);
			
			event.time(1) = map_time(FIGURE_DATA.browser.sound, 'record', 'slider', event.time(1));
			
			event.time(2) = event.time(1) + event.duration;
			
			%--
			% update modification date
			%--
			
			event.modified = get_current_timestamp;
			
			%--
			% recompute measurements if needed
			%--
			
			% TODO: also consider active measures for non-logged selections
			
			recompute = FIGURE_DATA.browser.measurement.recompute;
			
			if ~isempty(recompute)
				
				for k = 1:length(event.measurement)

					%--
					% check if we need to recompute this measure
					%--
					
					obj = event.measurement(k);

					if ~ismember(obj.name, recompute)
						continue;
					end
					
					%--
					% try to recompute measure
					%--
					
					try
						event = feval(obj.fun, 'batch', FIGURE_DATA.browser.sound, event, obj);
					catch
						% TODO: eventually call extension warning
					end

				end
				
			end
			
			%--
			% update event, log, and userdata
			%--
			
			event.file = get_event_file(event, FIGURE_DATA.browser.sound);
			
            log_save_events(log, event);
			
			%--
			% clear previous display
			%--
			
			% NOTE: the clearing of these displays does not seem to belong here
	
			% TODO: factor this little cleanup function
			
			widgets = get_widgets(PAR);
			
			tag = event_tag(event, log);
			
			for k = 1:numel(widgets)
				delete_obj(widgets(k), tag);
			end
			
			delete(findall(PAR, 'tag', tag));
			
			%--
			% reset display
			%--
			
			event_view_2(PAR, event, log);
			
			event_bdfun(PAR, log, id);
			
		%--
		% selection event editing
		%--
		
		else
			
			%--
			% copy selection event
			%--
			
			event = FIGURE_DATA.browser.selection.event;
	
			%--
			% update selection event
			%--
	
			event.time = [EDIT_XDATA(1), EDIT_XDATA(2)];
			
			if strcmp(FIGURE_DATA.browser.grid.freq.labels, 'kHz')
				event.freq = 1000 * [EDIT_YDATA(2), EDIT_YDATA(3)];
			else
				event.freq = [EDIT_YDATA(2), EDIT_YDATA(3)];
			end
			
			event.duration = diff(event.time);
			
			event.bandwidth = diff(event.freq);
			
			%--
			% create label strings for event
			%--
			
			[time,freq] = selection_labels(PAR, event, FIGURE_DATA); 
			
			%--
			% update patch and line positions
			%--
			
			set(SELECTION_HANDLES(1:2), 'xdata', EDIT_XDATA, 'ydata', EDIT_YDATA);
		
			%--
			% update grid lines
			%--
	
			xl = [AXES_XLIM(1) EDIT_XDATA(1) nan EDIT_XDATA(2) AXES_XLIM(2)];
			yl = [AXES_YLIM(1) EDIT_YDATA(2) nan EDIT_YDATA(3) AXES_YLIM(2)];
			
			tmp = ones(1,5);
			
			set(SELECTION_HANDLES(3),'xdata',EDIT_XDATA(1) * tmp,'ydata',yl);
			set(SELECTION_HANDLES(4),'xdata',EDIT_XDATA(2) * tmp,'ydata',yl);
			
			set(SELECTION_HANDLES(5),'xdata',xl,'ydata',EDIT_YDATA(2) * tmp);
			set(SELECTION_HANDLES(6),'xdata',xl,'ydata',EDIT_YDATA(3) * tmp);
			
			%--
			% update grid labels
			%--
			
			% time labels
			
			tmp = get(SELECTION_HANDLES(7),'position');		
			tmp(1) = EDIT_XDATA(1) - 0.0375;
			set(SELECTION_HANDLES(7),'position',tmp,'string',time{1});
				
			tmp = get(SELECTION_HANDLES(8),'position');
			tmp(1) = EDIT_XDATA(2) + 0.0375;
			set(SELECTION_HANDLES(8),'position',tmp,'string',time{4});
	
			% frequency labels
			
			tt1 = event.time(1) - FIGURE_DATA.browser.time;
			tt2 = (FIGURE_DATA.browser.time + FIGURE_DATA.browser.page.duration) - event.time(2);
			
			mar = (0.0375 * (yl(end) - yl(1))) / FIGURE_DATA.browser.page.duration;
			
			if (tt1 <= tt2)
				
				tmp = get(SELECTION_HANDLES(9),'position');
				tmp(1) = xl(end) - 0.0375;
				tmp(2) = EDIT_YDATA(2) - mar;
				
				set(SELECTION_HANDLES(9), ...
					'position',tmp,'string',freq{1}, ...
					'HorizontalAlignment','right', ...
					'VerticalAlignment','top' ...
				);
				
				tmp = get(SELECTION_HANDLES(10),'position');
				tmp(1) = xl(end) - 0.0375;
				tmp(2) = EDIT_YDATA(3) + mar;
				
				set(SELECTION_HANDLES(10), ...
					'position',tmp,'string',freq{4}, ...
					'HorizontalAlignment','right', ...
					'VerticalAlignment','bottom' ...
				);
	
			else
				
				tmp = get(SELECTION_HANDLES(9),'position');
				tmp(1) = xl(1) + 0.0375;
				tmp(2) = EDIT_YDATA(2) - mar;
				
				set(SELECTION_HANDLES(9), ...
					'position',tmp,'string',freq{1}, ...
					'HorizontalAlignment','left', ...
					'VerticalAlignment','top' ...
				);
				
				tmp = get(SELECTION_HANDLES(10),'position');
				tmp(1) = xl(1) + 0.0375;
				tmp(2) = EDIT_YDATA(3) + mar;
				
				set(SELECTION_HANDLES(10), ...
					'position',tmp,'string',freq{4}, ...
					'HorizontalAlignment','left', ...
					'VerticalAlignment','bottom' ...
				);
				
			end
		
			%--
			% update control point positions
			%--
			
			xc = (EDIT_XDATA(1) + EDIT_XDATA(2)) / 2;
			yc = (EDIT_YDATA(2) + EDIT_YDATA(3)) / 2;
			
			ctrl = [ ...
				EDIT_XDATA(2), yc; ...
				EDIT_XDATA(2), EDIT_YDATA(3); ...
				xc, EDIT_YDATA(3); ...
				EDIT_XDATA(1), EDIT_YDATA(3); ...
				EDIT_XDATA(1), yc; ...
				EDIT_XDATA(1), EDIT_YDATA(1); ...
				xc, EDIT_YDATA(1); ...
				EDIT_XDATA(2), EDIT_YDATA(1); ...
				xc, yc ...
			];
		
			for k = 1:9
				set(SELECTION_HANDLES(10 + k),'xdata',ctrl(k,1),'ydata',ctrl(k,2));
			end
			
			%--
			% update userdata
			%--
			
			FIGURE_DATA.browser.selection.event = event;		
			
			set(PAR, 'userdata', FIGURE_DATA);
			
		end

	end
	
	%--
	% update widgets, perhaps also compute active measures
	%--
	
	update_widgets(PAR, 'selection__edit__stop');
	
	% NOTE: we now delete the drag line
	
	try
		delete(EDIT_HANDLE);
	catch
		% NOTE: there is nothing to catch here really
	end
	
	% NOTE: this 'set' generates a 'selection__create' event
	
	% NOTE: the 'selection__create' event provides non-live selection update!
	
	if exist('event', 'var')
		
		user = get_active_user; event.author = user.name;
		
		set_browser_selection(PAR, event, FIGURE_DATA.browser.selection.log);
		
	end
	
	%--
	% reset current character
	%--
	
	% NOTE: this should be useful in other places
	
	set(M_PAR, 'currentcharacter', ' ');
	
end


%---------------------------------
% SELECTION_EDIT_CALLBACK
%---------------------------------

% NOTE: this function could use 'varargin' after 'eventdata'

function selection_edit_callback(obj, eventdata, mode, ctrl, log, id)

if nargin < 5
	selection_edit(mode, ctrl);
else
	selection_edit(mode, ctrl, log, id)	
end


