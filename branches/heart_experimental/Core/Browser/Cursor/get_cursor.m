function cursor = get_cursor(par)

if nargin < 1
	par = get_active_browser;
end

if isempty(par)
	cursor = []; return;
end