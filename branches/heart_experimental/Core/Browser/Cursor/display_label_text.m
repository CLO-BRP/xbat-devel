function handle = display_label_text(handle, str, loc, ylim)
	
% display_label_text - display label type text in axes
% ----------------------------------------------------
%
% handle = display_label_text(handle, str, loc, ylim)
%
% Input:
% ------
%  handle - text or axes handle
%  str - string
%  loc - location of label in x
%  ylim - limits for parent axes
%
% Output:
% -------
%  handle - text handle

% NOTE: for the moment this only works for labels at the top of the axes placed using x

%--
% handle input
%--

% NOTE: if we get a text handle we update this text otherwise we expect a parent axes handle

update = strcmp('text', get(handle, 'type'));

if update
	ax = get(handle, 'parent');
else
	ax = handle;
end

% NOTE: this is a performance option, this seems to take quite some time, relatively

if nargin < 4
	ylim = get(ax, 'ylim');
end

%--
% compute desired pixel padding in axis units
%--

% NOTE: we need to get axes height in normalized units and figure size in pixels

pos = get_size_in(ax, 'normalized'); height1 = pos(4);

pos = get_size_in(get(ax, 'parent'), 'pixels'); height2 = pos(4);

pad = (4 / (height1 * height2)) * (diff(ylim));

%--
% create or update label text object
%--

if update	
	set(handle, 'position', [loc, ylim(2) + pad, 0], 'string', str);	
else	
	handle = text(loc, ylim(2) + pad, str,  'parent', ax, 'verticalalignment', 'bottom');
end 

%--
% update visibility of text
%--

xlim = get(ax, 'xlim');

if loc < xlim(1) || loc > xlim(2)
	set(handle, 'clipping', 'on');
else
	set(handle, 'clipping', 'off');
end


