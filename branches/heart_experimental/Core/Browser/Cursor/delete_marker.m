function data = delete_marker(par, data)

% delete_marker - delete browser marker
% -------------------------------------
% 
% data = delete_marker(par, data)
%
% Input:
% ------
%  par - parent
%  data - state
% 
% Output:
% -------
%  data - updated state

%--
% get state from browser
%--

if nargin < 2
	data = get_browser(par); update = 1;
else
	update = 0;
end

%--
% update browser state and display
%--

data.browser.marker = marker_create;

% NOTE: we only update the state if it is something we procured

if update
	set(par, 'userdata', data);
end

clear_marker(par);

%--
% update widgets
%--

update_widgets(par, 'marker__delete', data);

