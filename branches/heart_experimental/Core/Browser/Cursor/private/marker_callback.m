function marker_callback(obj, eventdata, mode, data) %#ok<INUSL>

persistent M_AX M_PAR AX PAR PAR_POINTER PAR_DATA UPDATE_LISTENERS;

% TODO: clean-up and reconsider various names

switch mode
	
	case 'start'
		
		%--
		% get and cache parent state
		%--
		
		PAR_DATA = data;
		
		%--
		% check whether the motion figure is the parent figure
		%--

		M_AX = get(obj, 'parent'); M_PAR = get(M_AX, 'parent');
		
		if is_browser(M_PAR)
			
			PAR = M_PAR; AX = M_AX;
			
		else
			
			data = get(M_PAR,'userdata');
			
			if ~isfield(data, 'parent')
				return;
			end
			
			PAR = data.parent; obj = get_marker_handles(PAR); 
			
			if isempty(obj)
				return;
			end
			
			AX = get(obj(1), 'parent');
			
		end
		
		%--
		% set move pointer and callbacks
		%--
		
		PAR_POINTER = get(M_PAR, 'pointer'); set(M_PAR, 'pointer', 'left');
		
		set(M_PAR, ...
			'windowbuttonmotionfcn', {@marker_callback, 'update'}, ...
			'windowbuttonupfcn', {@marker_callback, 'stop'} ...
		);
	
		%--
		% get and cache udpate listenings widgets
		%--
		
		UPDATE_LISTENERS = get_widgets(PAR, 'marker__move__update');
		
	case 'update'
		
		%--
		% set marker without saving marker, marker is persistently stored
		%--
		
		time = get(M_AX, 'currentpoint'); time = time(1);
		
		% TODO: note the conditions that lead to an empty marker output
		
		marker = set_marker(PAR, AX, time, PAR_DATA, UPDATE_LISTENERS);
		
		if ~isempty(marker)
			PAR_DATA.browser.marker = marker; 
		end
		
	case 'stop'
		
		% NOTE: this seems like the place to implement the 'stop' event
		
		%--
		% set marker and store
		%--
		
		time = PAR_DATA.browser.marker.time;
		
		% NOTE: this will update the most recent browser state
		
		set_marker(PAR, AX, time);
		
		%--
		% restore pointer and callbacks
		%--
		
		set(M_PAR, 'pointer', PAR_POINTER);
		
		% NOTE: we should store the initial callbacks so that we can restore
	
		set(M_PAR, ...
			'windowbuttonmotionfcn', [], ...
			'windowbuttonupfcn', [] ...
		);
		
end
