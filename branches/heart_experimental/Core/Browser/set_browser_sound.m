function set_browser_sound(par, sound, data)

%--
% handle input
%--

if nargin < 3
	data = get_browser(par);
end

info = get_browser_info(par);

%--
% set slider time and duration for new sound
%--

time = map_time(data.browser.sound, 'record', 'slider', data.browser.time);

%--
% set browser fields
%--

set_browser(par, data, 'time', time, 'sound', sound);

%--
% close browser
%--

close(par);

%--
% reopen and set time
%--

par = open_library_sound(info.sound, get_library_from_name(info.library));

set_browser_time(par, time, 'record');



