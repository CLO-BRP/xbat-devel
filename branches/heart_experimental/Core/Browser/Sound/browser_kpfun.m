function result = browser_kpfun(par, key)

% browser_kpfun - key press function for browser figure
% -----------------------------------------------------
%
% result = browser_kpfun(par, key)
%
% Input:
% ------
%  par - browser
%  key - keypress information

% NOTE: we could use the 'shift', 'return', and 'tab' keys for modal or focal changes

%--
% initialize output
%--

result.keep_focus = 1;

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% check handle input
%--

if ~is_browser(par)
	return;
end

%--
% get keypress struct
%--

if nargin < 2
	key = get_keypress(par);
end

% NOTE: build keypress struct from character input

if ischar(key)
	key.char = key; key.code = double(key.char); key.key = [];
end

%--------------------------------------------
% KEYPRESS CALLBACK
%--------------------------------------------

%--
% handle function keys
%--

% NOTE: at the moment we use these for various visibility commands

% TODO: declare a set of supported keys and make the second clause an 'ismember' test

if ~isempty(key.key) && ((key.key(1) == 'f') || strcmp(key.key, 'space'))
	
	switch key.key

		%--
		% toggle palette display
		%--
		
		case 'f1'
			browser_window_menu(par, 'Hide Palettes'); result.keep_focus = 0;
		
		%--
		% toggle other sounds display
		%--
		
		case 'f2'
			browser_window_menu(par, 'TOGGLE_OTHERS');
		
		%--
		% toggle desktop display
		%--
		
		case 'f3'
			browser_window_menu(par, 'TOGGLE_DESKTOP');
		
		%--
		% cycle through open sounds, previous and next sound
		%--
		
		% NOTE: this is particularly useful when hiding other sounds
		
		% NOTE: 'f6' is back because forward is more intuitive and 'f5' more accesible
		
		case 'f5'
			show_browser(par, 'next'); result.keep_focus = 0;
		
		case 'f6'
			show_browser(par, 'previous'); result.keep_focus = 0;
			
		%--
		% toggle play state
		%--
		
		% NOTE: this is not working reliably, the keypress is not triggering
		
		case 'space'
			toggle_play_state;

	end
	
	return;
	
end 
	
%--
% return on empty characters
%--

if isempty(key.code)
	return;
end

%--
% handle keystroke commands
%--

switch key.code
	
	case double('T')
		
		tag_page_events(par);
		
	%--------------------------------------------
	% MODE COMMANDS
	%--------------------------------------------
		
	%--
	% Hand Mode
	%--
	
	case double('h')
		
		% NOTE: this command should also be put into some other place
		
		if ~strcmp(get_browser_mode(par), 'hand')
			set_browser_mode(par, 'hand');
		else
			set_browser_mode(par, 'select');
		end
		
	%--
	% Select Mode
	%--
	
	case double('s')
		
		set_browser_mode(par, 'select');
				
	%--------------------------------------------
	% SOUND COMMANDS
	%--------------------------------------------
	
	%--
	% Play Page
	%--

	case double('P')
		browser_sound_menu(par, 'Page');
		
	%--
	% Play Selection
	%--
	
	case double('p')
		browser_play(par);
		
	%--
	% Other Rate
	%--
	
	case double('r')
		browser_sound_menu(par, 'Other Rate ...'); result.keep_focus = 0;
		
	%--
	% Volume Control
	%--
	
	case double('v')
		browser_sound_menu(par, 'Volume Control ...'); result.keep_focus = 0;
		
	%--------------------------------------------
	% NAVIGATE COMMANDS
	%--------------------------------------------
		
	% NOTE: the view navigation is currently unstable under the new
	% navigation framework
	
	case double('w')
		
		goto_next_event(par);
		
	case double('W')
		
		goto_previous_event(par);
		
	%--
	% Previous Event
	%--
	
	case double('E')
		
		%--
		% try to get event palette, if not found  return
		%--
		
		g = get_palette(par, 'Event');
		
		if isempty(g)
			return;
		end
		
		%--
		% execute previous event callback
		%--
		
		control_callback([],g, 'previous_event');
		
	%--
	% Next Event
	%--
	
	case double('e')
		
		%--
		% try to get event palette, if not found return
		%--
		
		g = get_palette(par, 'Event');
		
		if isempty(g)
			return;
		end
		
		%--
		% execute previous event callback
		%--
		
		control_callback([],g, 'next_event');
		
	%--
	% Previous View
	%--
	
	case double('<')
		browser_view_menu(par, 'Previous View');
		
	%--
	% Next View
	%--
	
	case double('>')
		browser_view_menu(par, 'Next View');
		
	%--
	% Previous Page (back arrow)
	%--
	
	case {28, double('b')}
		browser_view_menu(par, 'Previous Page');
		
	%--
	% Next Page (forward arrow)
	%--
	
	case {29, double('n')}
		browser_view_menu(par, 'Next Page');
		
	%--
	% First Page (up arrow)
	%--
	
	case 30
		browser_zoom('out', par);
		
	%--
	% Last Page (down arrow)
	%--
	
	case 31
		browser_zoom('in', par);
		
	%--
	% Go To Page ...
	%--
	
	case double('g')
		browser_view_menu(par, 'Go To Page ...'); result.keep_focus = 0;
		
	%--
	% Previous File
	%--
	
	% NOTE: perhaps this should be an idempotent commmand, look through the code
	
	case double('[')
		browser_view_menu(par, 'Previous File');
		
	%--
	% Next File
	%--
	
	case double(']')
		browser_view_menu(par, 'Next File');
		
	%--
	% Go To File ...
	%--
	
	case double('f')
		browser_view_menu(par, 'Go To File ...');

	%--------------------------------------------
	% COLORMAP COMMANDS
	%--------------------------------------------
	
	%--
	% Gray 
	%--
	
	% NOTE: the trailing space in the name of the menu command
	
	case double('G')
		browser_view_menu(par, 'Gray ');
	
	%--
	% Hot 
	%--
	
	case double('H')
		browser_view_menu(par, 'Hot');
		
	%--
	% Jet
	%--
	
	case double('J')
		browser_view_menu(par, 'Jet');
		
	%--
	% Bone
	%--
	
	case double('B')
		browser_view_menu(par, 'Bone');
		
	%--
	% HSV
	%--
	
	case double('V')
		browser_view_menu(par, 'Jet');
		
	%--
	% Center (Selection or Marker)
	%--
	
	% NOTE: this used to toggle the colorbar
	
	case double('c')
		
% 		browser_view_menu(par, 'Colorbar');
		
		% TODO: we want to map this to center, either selection or marker
		
		%--
		% get page center from selection or marker
		%--
		
		sel = get_browser_selection(par);
		
		if ~isempty(sel.event)
			center = mean(sel.event.time);
		else
			marker = get_browser_marker(par); center = marker.time;
		end
		
		page = get_browser_page(par); 
		
		%--
		% set page start time
		%--
		
		set_browser_time(par, center - 0.5 * page.duration);
		
	%--
	% Auto Scale
	%--
	
	case double('a')
		browser_view_menu(par, 'Auto Scale');
			
	%--
	% Invert
	%--
	
	case double('i')
		browser_view_menu(par, 'Invert');
		
	%--------------------------------------------
	% SELECTION COMMANDS
	%--------------------------------------------
	
	%--
	% Delete Selection (delete), and Stop Play
	%--
	
	case 127
		
		%--
		% check for sound player
		%--
		
		% NOTE: eventually there may be multiple players, one per parent
		
		if stop_player
			return;
		end

		%--
		% delete selection from current figure
		%--
		
		delete_selection(par);
	
	%--
	% Log Selection To
	%--
	
	% NOTE: there are problems with the name of this command
	
	case double('l')
		event = selection_log(par);
	
	%--
	% Selection Grid
	%--
	
	case double('''')
		browser_view_menu(par, 'Grid');
				
	%--
	% Selection Labels
	%--
	
	case double('"')
		browser_view_menu(par, 'Labels');
				
	%--
	% Control Points
	%--
	
	case double('.')
		browser_view_menu(par, 'Control Points');
		
	%--
	% Selection Zoom
	%--
	
	% NOTE: this is a mode, however it sometimes behaves as an action
	
	case double('z')
		browser_view_menu(par, 'Selection Zoom');
			
	%--------------------------------------------
	% GRID COMMANDS
	%--------------------------------------------
	
	%--
	% Time Grid (used to be 'Grid')
	%--
	
	case double(';')
		browser_view_menu(par, 'Time Grid');
				
	%--
	% File Grid
	%--
	
	case double(':')
		browser_view_menu(par, 'File Grid');
			
	%--------------------------------------------
	% MISCELLANEOUS COMMANDS
	%--------------------------------------------
	
	%--
	% Actual Size
	%--
	
	case double('='), browser_window_menu(par, 'Actual Size');
		
	%--
	% Half Size
	%--
	
	case double('-'), browser_window_menu(par, 'Half Size');
		
	%--------------------------------------------
	% PALETTE COMMANDS
	%--------------------------------------------
	
	case double('x')
		
		pal = xbat_palette; figure(pal); result.keep_focus = 0;
	
	otherwise
		
end

% NOTE: consider if we want the browser to keep focus


