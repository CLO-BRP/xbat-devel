function [ext, context] = get_compiled_extension(ext, par, pal)

% get_compiled_extension - get extension with up to date browser state
% -------------------------------------------------------------------
%
% [ext, context] = get_compiled_extension(ext, par, pal)
%
% Input:
% ------
%  ext - extension name
%  par - browser
%  pal - to look for values
%
% Output:
% -------
%  ext - compiled extension
%  context - context generated

%----------------------
% HANDLE INPUT
%----------------------

%--
% set active browser and no palette default
%--

if nargin < 2
	par = get_active_browser;
end

if isempty(par)
	return;
end

if nargin < 3
	pal = [];
end

data = get_browser(par);

%--
% get basic extension if needed
%--

if iscell(ext)
	ext = get_extensions(ext{1}, 'name', ext{2});
end

%----------------------
% SETUP 
%----------------------

%--
% get initial extension context, and possibly control values from palette
%--

% NOTE: the context is used by various extension methods to do their jobs

context = get_extension_context(ext, par, data);

context.pal = pal;

if ~isempty(pal)

	ext.control = get_control_values(pal);

	% NOTE: if we have a debug control, it will be used to set the context

	if isfield(ext.control, 'debug')
		context.debug = ext.control.debug;
	end

end

%--
% get current compiled parameters for all required types
%--

[value, types] = has_any_parameters(ext);

for k = 1:length(types)
	
	%--
	% get type, parameter function branch, and currently stored parameters
	%--
	
	type = types{k};

	% TODO: that we use the old parameter store here, perhaps that should change
	
	if strcmp(type, 'compute')
		fun = ext.fun.parameter; parameter = ext.parameter;
	else
		fun = ext.fun.(type).parameter; parameter = ext.parameters.(type);
	end 
	
	% NOTE: in what follows we do what we need to get current parameters

	%--
	% we have non-trivial parameters and no palette to update them with, compile with fresh context
	%--
	
	if ~trivial(parameter) && isempty(pal)

		% NOTE: if we compile, we compile to ensure fresh context

		if ~isempty(fun.compile)

			try
				[parameter, context] = fun.compile(parameter, context);
			catch
				extension_warning(ext, warning_message(type, 'compilation'), lasterror);
			end

		end

		% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
		if strcmp(type, 'compute')
			ext.parameter = parameter; ext.parameters.(type) = parameter;
		else
			ext.parameters.(type) = parameter;
		end

		continue;

	end

	%--
	% we have trivial parameters, create default parameters given context
	%--

	if trivial(parameter)

		try
			parameter = fun.create(context);
		catch
			extension_warning(ext, warning_message(type, 'creation'), lasterror); continue;
		end
		
		% NOTE: trivial parameters need no update, we assume they need no compilation

		if trivial(parameter)
			continue;
		end

	end

	%--
	% update parameters from matching controls if we have a palette
	%--
	
	% NOTE: this may fail if we have name conflicts

	if ~isempty(pal)

		% NOTE: update must be configured to be shallow

		opt = struct_update; opt.flatten = 0;
		
		parameter = struct_update(parameter, ext.control, opt);
		
% 		db_disp; stack_disp; parameter, ext.control

	end

	%--
	% compile current parameters if needed
	%--
	
	if ~isempty(fun.compile)

		try
			[parameter, context] = fun.compile(parameter, context);
		catch
			extension_warning(ext, warning_message(type, 'compilation'), lasterror);
		end

	end
	
	%--
	% store current compiled parameters of type in extension
	%--
	
	% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
	if strcmp(type, 'compute')
		ext.parameter = parameter; ext.parameters.(type) = parameter;
	else
		ext.parameters.(type) = parameter;
	end

end

%--
% put current extension in context
%--

context.ext = ext;


%------------------------
% WARNING_MESSAGE
%------------------------

function str = warning_message(type, failed)

str = ['''', title_caps(type), ''' parameter $API failed.'];

str = strrep(str, '$API', failed);
