function handle = selection_menu(par, event, data)

% selection_menu - create menu for selection display in browser
% -------------------------------------------------------------
%
% handle = selection_menu(par, event, data)
%
% Input:
% ------
%  par - menu parent handle
%  event - selection event
%  data - parent browser state
%
% Output:
% -------
%  handle - created menu handles

% TODO: develop and use this menu in SIGNAL_FEATURE views

% NOTE: consider whether there are any logs open when creating this menu

%------------------------
% SETUP
%------------------------

%--
% create context menu
%--

context = get(par, 'uicontextmenu');

if ~isempty(context)
	children = findobj(context, 'type', 'uimenu'); delete(children);
end

top = uicontextmenu('parent', ancestor(par, 'figure'));

set(par, 'uicontextmenu', top);

%------------------------
% CREATE MENU
%------------------------

%--
% create basic menu
%--

% NOTE: we are now creating the full menu and selectively enabling, this affords hinting of functionality

% NOTE: this will be dynamic, it also considers the possiblte singleton nature of the type

L = { ...
	'Selection', ...
	'(Basic)', ...
	'Event', ...
	'Hierarchy', ...
	'(Annotation)', ...
	'Tags', ...
	'Rating', ...
	'Annotation', ...
	'Measurement', ...
	'Classification', ...
	'(Action)', ...
	'Play Selection', ...
	'Center Selection', ...
	'Log Selection To    ', ...
	'Actions', ...
	'(Edit)', ...
	'Cut Selection', ...
	'Copy Selection', ...
	'Delete Selection' ...
};

n = length(L); 

S = bin2str(zeros(1, n)); S{2} = 'on';

handle{1} = menu_group(top, 'browser_edit_menu', L, S);

set(handle{1}(1), 'enable', 'off');

parenthetical_menus(handle{1});

% NOTE: we set play as a separate callback

set(get_menu(handle{1}, 'Play Selection'), 'callback', 'browser_sound_menu(gcf, ''Play Selection'');');

%--
% create core children menus
%--

handle{end + 1} = log_to_menu(get_menu(handle{1}, 'Log Selection To    '), data);

handle{end + 1} = selection_event_menu(handle{end - 1}(3), event, data);

handle{end + 1} = action_menu(get_menu(handle{1} , 'Actions'), 'event');

%--
% tag selection menu handles
%--

handle = [handle{:}];

set(handle, 'tag', 'selection-menu');

set(top, 'tag', 'selection');


%----------------------------
% SELECTION_EVENT_MENU
%----------------------------

function handle = selection_event_menu(par, event, data)

%--
% compute selection info labels
%--

% NOTE: not all of these may be used

opt = event_labels;

opt.time = data.browser.grid.time.labels;

opt.freq = data.browser.grid.freq.labels;

[time, freq] = event_labels(event, data.browser.sound, opt);

%--
% create menu
%--

if ~ischar(event.created) || isempty(event.created)
	event.created = datestr(now, 31);
end

% db_disp; event.created, time, freq

author = event_author_string(event); 

L = { ...
	'(Channel)', ...
	['Channel:  ', int2str(event.channel)], ...
	'(Time)', ...
	['Start Time:  ', time{1}], ...
	['End Time:  ', time{2}], ...
	['Duration:  ', time{3}], ...
	'(Freq)', ...
	['Min Freq:  ', freq{1}], ...
	['Max Freq:  ', freq{2}], ...
	['Bandwidth:  ', freq{3}], ...
	'(Info)', ...
	['Author:  ', author], ...
	['Created:  ', event.created] ...
};

handle = menu_group(par, '', L);

parenthetical_menus(handle);


%----------------------------
% LOG_TO_MENU
%----------------------------

function handle = log_to_menu(par, data)

% TODO: this function only needs 'log' and 'log_active' input

if data.browser.log_active
	
    L = log_name(data.browser.log); 

    if ischar(L)
        L = {L};
    end

    handle = menu_group(par, 'browser_edit_menu', L);

    % NOTE: display check on active log indicating default log behavior

    set(handle(data.browser.log_active), 'check', 'on');	

else

	L = {'(No Open Logs)'};
	
	handle = menu_group(par, 'browser_edit_menu', L);
	
	set(handle, 'enable', 'off');
	
end

