function [selection, count] = get_browser_selection(par, data)

% get_browser_selection - get browser selection
% ---------------------------------------------
%
% selection = get_browser_selection(par, data)
%
% Input:
% ------
%  par - browser
%  data - state
%
% Output:
% -------
%  selection - selection

%--
% handle input
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end 

% TODO: what is a reasonable output here?

if isempty(par)
	selection = []; count = 0; return;
end

if nargin < 2
	data = get_browser(par);
end

if isempty(data)
	selection = []; count = 0; return;
end

%--
% get selection
%--

selection = data.browser.selection; 

count = numel(selection.event);

%--
% make selection event proper event
%--

% TODO: figure out who may have used this

% if count
%     selection.event = map_events(selection.event, data.browser.sound, 'slider', 'record');
% end
