function daemon = browser_daemon

% browser_daemon - create a timer object to handle browser state saves
% --------------------------------------------------------------------
%
% daemon = browser_daemon
%
% Output:
% -------
%  daemon - timer object

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2202 $
% $Date: 2005-12-06 08:24:51 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

%--
% look for timer
%--

daemon = timerfind('name','XBAT Browser Daemon');

%--
% create and configure timer if needed
%--

if isempty(daemon)

	daemon = timer;

	set(daemon, ...
		'name', 'XBAT Browser Daemon', ...
		'timerfcn',  @save_browsers_state, ...
		'executionmode', 'fixedRate', ...
		'period', 60 ...
	);

end


%---------------------------------------------------
% SAVE_BROWSER_STATE
%---------------------------------------------------

function save_browsers_state(obj, evendata)

% save_browsers_state - timer callback to save browsers state
% -----------------------------------------------------------

%--
% get sound browser figures
%--

h = get_xbat_figs('type', 'sound');

%--
% save sound browser states
%--

for k = 1:length(h)
	
	% NOTE: this exception is encountered when a figure is deleted before save

	try
		browser_sound_save(h(k));
	catch
		nice_catch(lasterror, 'Browser state save failed.');
	end
	
end

%--
% check tool and toolbox directories, update if needed
%--

% TODO: this is a partial pattern for watched folders, implement callback watched folders

persistent LAST_CONTENT; 

content = no_dot_dir(tools_root, 1); tools = {content.name};

content = no_dot_dir(toolbox_root, 1); toolbox = {content.name};

if isempty(LAST_CONTENT)
	LAST_CONTENT.tools = tools; LAST_CONTENT.toolbox = toolbox; return;
end

%--
% check for added or removed elements
%--

added = setdiff(tools, LAST_CONTENT.tools);

if ~isempty(added)
	added = iteraten(@fullfile, 2, tools_root, added); append_path(added{:}); disp(' '); iterate(@disp, added); disp(' ');
end

added = setdiff(toolbox, LAST_CONTENT.toolbox);

if ~isempty(added)
	added = iteraten(@fullfile, 2, toolbox_root, added); append_path(added{:}); disp(' '); iterate(@disp, added); disp(' ');
end

%--
% update persistent last content
%--

content = no_dot_dir(tools_root); tools = {content.name};

content = no_dot_dir(toolbox_root); toolbox = {content.name};

LAST_CONTENT.tools = tools; LAST_CONTENT.toolbox = toolbox;


% TODO: implement removed in a similar way, and refactor

