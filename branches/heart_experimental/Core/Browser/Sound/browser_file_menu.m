function out = browser_file_menu(par, str)

% browser_file_menu - browser file function menu
% ----------------------------------------------
%
% out = browser_file_menu(par, str)
%
% Input:
% ------
%  par - figure handle (def: gcf)
%  str - menu command string (def: 'Initialize')
%
% Output:
% -------
%  out - command dependent output

%--------------------------------
% SETUP
%--------------------------------

%--
% set command string
%--

if nargin < 2
	str = 'Initialize';
end

%--
% perform command sequence
%--

% a cell array of strings as input performs a sequence of commands

if iscell(str)
	
	for k = 1:length(str)
		try
			browser_file_menu(par, str{k}); 
		catch
			nice_catch(lasterror, ['Unable to execute command ''', str{k}, '''.']);
		end
	end
	
	return;
	
end

%--
% set handle
%--

if nargin < 1
	par = gcf;
end

%--
% set default output
%--

out = [];

%--
% main switch
%--

switch str

%--------------------------------
% CREATE MENU
%--------------------------------

case 'Initialize'
	
	% add code to make sure menu is created. create while menu does not
	% exist

	%--
	% get userdata
	%--
	
	if ~isempty(get(par, 'userdata'))
		data = get(par, 'userdata');
	end
	
	%--
	% File
	%--
	
	L = { ...
		'File', ...
		'(Log)', ...
		'New Log ...', ...		
		'Open Log ...', ...
		'(Figure)', ...
		'Save Figure', ...
		'Save Figure As ...', ...
		'Export Figure ...', ...
		'(Print)', ...
		'Page Setup ...', ...
		'Print Preview ...', ...
		'Print ...', ...
		'Close' ...			
	};
	
	n = length(L);
	
	S = bin2str(zeros(1,n)); S{end} = 'on';
	
	A = cell(1,n);
	
	A{3} = 'N'; A{4} = 'O'; A{end - 1} = 'P'; A{end} = 'W';
	
	tmp = menu_group(par, 'browser_file_menu', L, S, A);
	
	parenthetical_menus(tmp);
	
	data.browser.file_menu.file = tmp;
	
	set(tmp(1),'position',1);
	
	set(get_menu(tmp,'View Settings'), 'enable', 'off');
	
	set(get_menu(tmp,'Save View Settings ...'), 'enable', 'off');
	
	%--
	% save userdata
	%--
	
	set(gcf,'userdata',data);
	
%--------------------------------
% MENU COMMANDS
%--------------------------------

case 'New Log ...'
	
	new_log_dialog(par);

case 'Open Log ...'
		
	log_open(par);
	
case 'Page Setup ...'
		
	pagesetupdlg(par);

case 'Print Preview ...'
		
	printpreview(par); 
	
case 'Print ...'
		
	printdlg(par);
	
case 'Save Figure'
		
	%--
	% get userdata
	%--
	
	data = get(par,'userdata');
	
	%--
	% check for save location
	%--
	
	if isempty(data.browser.save)
		browser_file_menu(par, 'Save Figure As ...'); return;
	else
		fig_save(par, data.browser.save);
	end

case 'Save Figure As ...'

	%--
	% get location to save
	%--
	
	[fn,p] = uiputfile( ...
   		{ ...
			'*.fig', 'MATLAB Figure Files (*.fig)'; ...
			'*.*', 'All Files (*.*)' ...
		}, ...
		'Save Figure As ...' ...
	);

	%--
	% save file if put file completed
	%--
	
	if (fn)
		
		%--
		% update browser save
		%--
		
		tmp = [p filesep fn];
		
		data.browser.save = tmp;
		set(par,'userdata',data);
		
		%--
		% save figure
		%--
		
		fig_save(par,tmp);
		
	end

case 'Export Figure ...'
		
	% TODO: add preference setting for printing with and without the uicontrols
	
	export_figure(par);
		
case 'Close'

	%--
	% get userdata
	%--
	
	data = get(par,'userdata');
	
	%--
	% confirm and save updates to sound file
	%--
		
	% NOTE: this innocuous looking functions is doing quite a bit
	
	browser_sound_save(par,data);
	
	%--
	% delete children and palettes
	%--
	
	% NOTE: this function is capable of finding neglected children and destroying them
	
	tmp = get_xbat_figs('parent',par);
	
	try
		delete(tmp);
	end
	
% 	try
% 		delete(data.browser.children);
% 	end
% 	
% 	try
% 		delete(data.browser.palettes);
% 	end
				
	%--
	% check for open logs and save before closing
	%--
		
	if (data.browser.log_active)
		
		%--
		% get logs
		%--
		
		log = data.browser.log;
		
		%--
		% update open state and save logs
		%--
		
		for k = 1:length(log)
			
			log(k).open = 0;
			
			log(k).sound = sound_update(data.browser.sound, data);
			
			log_save(log(k));

		end
		
	end
		
	%--
	% stop daemons if needed
	%--
	
	% NOTE: the only remaining sound browser is being closed right now
	
	if length(get_xbat_figs('type', 'sound')) == 1
		
		% TODO: centralized management for daemons
		
		timer_stop('XBAT Browser Daemon');
		
		timer_stop('XBAT Scrolling Daemon');
	
		timer_stop('XBAT Jogging Daemon');
	
	end
	
	%--
	% show another browser and delete this one
	%--
	
	% NOTE: we may add fault tolerance to log saving, catch failure and abort close 

	show_browser(par,'previous'); 
	
	delete(par);
		
	%--
	% update window menu
	%--
	
	browser_window_menu;
	
end


%--------------------------------
% EXPORT_FIGURE
%--------------------------------

function export_figure(par,formats)

% export_file - export figure to file
% -----------------------------------
%
% export_figure(par,formats)
%
% Input:
% ------
%  par - figure handle
%  formats - available export formats

%--
% set default available formats
%--

% NOTE: only the color EPS export is really useful
	
% NOTE: BMP is low quality and the fonts are not right

% NOTE: illustrator format file does not open in current versions

if (nargin < 2) || isempty(formats)
	formats = { ...
		'*.emf', 'Enhanced Metafiles (*.emf)', 'meta'; ...
		'*.eps', 'Encapsulated PostScript (*.eps)', 'epsc2'; ...
		'*.tif', 'TIFF Images (*.tif)', 'tiff'; ...
		'*.jpg', 'JPEG Images (*.jpg)', 'jpeg'; ...
		'*.png', 'PNG Images (*.png)', 'png' ...
	};
end

%--
% get raw extensions from formats
%--

for k = 1:size(formats,1)
	exts{k} = formats{k,1}(3:end); 
end

%--
% get file type and location interactively
%--

[fn,p,fix] = uiputfile(formats(:,1:2), ['Export Figure ...']);

%--
% print figure to file according to export options
%--

if (fn)
	
	%--
	% handle filename
	%--
	
	ix = findstr(fn,'.'); % check for extension separator
	
	if (isempty(ix))
		
		%--
		% add extension using information from menu
		%--
				
		fn = [fn '.' exts{fix}];
		
	else
		
		%--
		% check that extension is available from available extensions
		%--
		
		tmp = fn((ix(end) + 1):end);
				
		ix = find(strcmp(exts,tmp));
		
		if (isempty(ix))
			
			fn = [fn '.' ext]; % append extension
			
		else
						
			fix = ix; % update file export option
			
		end

	end
	
	%--
	% print file based on extension
	%--
	
	mode = get(par,'paperpositionmode');
	
	set(par,'paperpositionmode', 'auto');
		
	print(['-f' int2str(par)], ['-d' formats{fix,3}], [p fn]);
	
	set(par,'paperpositionmode',mode);
	
end
