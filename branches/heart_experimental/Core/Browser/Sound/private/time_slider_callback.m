function time_slider_callback(obj, eventdata, par, force)

% NOTE: we are thinking that 'set_time_slider' will call this fucntion
% programatically and that the time slider will of course have it as its
% callback

%--
% initialize request
%--

persistent REQUEST;

if isempty(REQUEST)
	REQUEST = 0;
end

%--
% force request value
%--

if nargin > 3
	REQUEST = force;
end

%--
% check if we can ignore request
%--

% NOTE: this relies on a refresh ha
if nargin < 4
	
	current = get(obj, 'value'); previous = get_browser_history(par, 'time');

	if current == previous
		REQUEST = 0; return;
	end

end

% NOTE: it seems like this should talk to the daemon and let it do the
% actual refreshing?

if REQUEST
	browser_refresh(par); REQUEST = 0; return;
end

REQUEST = ~REQUEST;



