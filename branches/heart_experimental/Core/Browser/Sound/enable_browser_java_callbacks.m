function par = enable_browser_java_callbacks(par) 

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end

if isempty(par)
	return;
end

%--
% get all callback names and java handle to assist in callback setting
%--

names = get_callback_names(par);

par = getjframe(par); 

%--
% set browser callbacks
%--

% TODO: figure out which callbacks we can use to some effect

% NOTE: scroll-wheel callbacks are currently not listened to, this is disappointing

% NOTE: in this case we set all callbacks to the tracer, these are MATLAB callbacks! this is test code

for k = 1:numel(names)
	set(par, names{k}, {@callback_tracer, names{k}});
end


%-----------------------
% GET_CALLBACK_NAMES
%-----------------------

function names = get_callback_names(par)

%--
% get java frame properties
%--

prop = getjframe(par);

names = prop.ListOfCallbacks;

% %--
% % get property names check they end in callback
% %--
% 
% all = fieldnames(prop); tag = 'Callback'; len = numel(tag); names = {};
% 
% for k = numel(all):-1:1
% 	
% 	if numel(all{k}) < len || ~strncmp(all{k}(end - len + 1:end), tag, len)
% 		continue;
% 	end
% 	
% 	names{end + 1, 1} = all{k}; 
% 	
% end 


%-----------------------
% CALLBACK_TRACER
%-----------------------

function callback_tracer(obj, eventdata, str)

%--
% display callback name and the callback handler offerings
%--

disp(' '); disp(str); obj, eventdata %#ok<NOPRT>

methods(obj)

