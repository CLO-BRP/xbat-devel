function ext = get_browser_detector(varargin)

% get_browser_detector - get browser detector
% -------------------------------------------
%
% ext = get_browser_detector(par,name,data)
%
% Input:
% ------
%  par - parent browser
%  name - detector name
%  data - parent state
%
% Output:
% -------
%  ext - browser detector

ext = get_browser_extension('detector',varargin{:});

