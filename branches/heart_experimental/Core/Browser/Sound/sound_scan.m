function sound_scan(sound, scan, active)

% sound_scan - scan sound with active extensions
% ----------------------------------------------
% 
% sound_scan(par)
%
% sound_scan(sound, scan, active)
%
% Input:
% ------
%  par - browser
%  sound - sound
%  page - page
%  context - context
%  active - extensions

%---------------------
% HANDLE INPUT
%---------------------

%--
% handle single parent input
%--

if nargin < 2

	data = get_browser(par);
	
	%--
	% get parent sound and page
	%--
	
	sound = data.browser.sound;
	
	page.start = data.browser.time;
	
	page.duration = data.browser.page.duration;
	
	page.channels = get_channels(data.browser.channels);
	
	scan = scan_intersect(get_sound_scan(sound), get_page_scan(page));
	
	%--
	% get active extensions
	%--
	
	active = get_active_extensions(par, data);
	
end

%---------------------
% SETUP
%---------------------

context.scan = scan;

%---------------------
% SCAN PAGE
%---------------------
	
while 1
	
	%--
	% get next page
	%--
	
	[page, context.scan] = get_scan_page(context.scan);
	
	if isempty(page)
		break;
	end

	%--
	% read sound page
	%--
	
	page = read_sound_page(sound, page, channels); 
	
	if isempty(page.samples)
		continue;
	end
	
	% TODO: add reading of page buffer
	
	%--
	% filter sound page
	%--
	
	% TODO: consider ordering the filters
	
	for k = 1:length(active.signal_filter)
		
		context.ext = active.signal_filter(k);
		
		[page, context] = filter_sound_page(page, context);
		
		active.sound_filter(k) = context.ext;
		
	end

	%--
	% compute sound features on page
	%--
	
	for k = 1:length(active.sound_feature)
		
		context.ext = active.sound_feature(k);
		
		[page, context] = feature_sound_page(page, context);
		
		active.sound_feature(k) = context.ext;
		
	end
	
	%--
	% detect events in sound page
	%--
	
	% NOTE: this probably means the end of separate active detection code
	
	%---------------------
	% DISPLAY
	%---------------------
	
	page_display(page)
	
end



