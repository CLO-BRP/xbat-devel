function [type, par] = browser_play(par, type, data)

% browser_play - play browser page or selection
% ---------------------------------------------
%
% browser_play(par, type, data)
%
% Input:
% ------
%  par - browser
%  type - play type 'page' or 'selection'
%  data - browser state

%--------------------
% HANDLE INPUT
%--------------------

%--
% get active browser as default
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

% NOTE: return if we can't get a browser

if isempty(par) || ~is_browser(par)
	return;
end

%--
% get browser state if needed
%--

if nargin < 3
	data = get_browser(par);
end

%--
% set default play type
%--

% NOTE:check for selection to get default play mode

if (nargin < 2) || isempty(type)
	
	[ignore, count] = get_browser_selection(par, data);

	type = ternary(count, 'selection', 'page');
	
end

%--------------------
% PLAY
%--------------------

switch type

	case 'selection'
		browser_sound_menu(par, 'Selection');

	case 'page'
		browser_sound_menu(par, 'Page');

end
