function [names, active, initial] = get_open_log_names(par, data)

% get_open_log_names - get names of open logs
% -------------------------------------------
%
% [names, active, initial] = get_open_log_names(par, data)
%
% Input:
% ------
%  par - browser
%  data - state
%
% Output:
% -------
%  names - open log names
%  active - active log index
%  initial - state for log type control

%--
% get browser state if needed
%--

if nargin < 2
	data = get_browser(par);
end

%--
% get open log names
%--

% NOTE: this function is too intimate

initial = '';

try
	names = file_ext({data.browser.log.file}); active = data.browser.log_active; initial = '__ENABLE__';
catch
	names = {};
end

% NOTE: this functions provides output for control creation only when asked

if isempty(names) && (nargout > 1)
	names = {'(No Open Logs)'}; active = 1; initial = '__DISABLE__';
end
