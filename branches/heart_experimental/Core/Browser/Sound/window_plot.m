function window_plot(pal, parameter)

% window_plot - create and update window display
% ----------------------------------------------
% 
% window_plot(pal, parameter)

%--
% get display axes handle
%--

handles = get_control(pal, 'Window_Plot', 'handles');

% NOTE: this is a temporary situation, eventually there will be one name

if isempty(handles)
	handles = get_control(pal, 'window_plot', 'handles');
end

% NOTE: return quickly if display is not there

if isempty(handles)
	return;
end

ax = handles.axes;

%--
% prepare display
%--

axes(ax); hold on;

delete(get(ax, 'children'));

%--
% compute window and shifted window
%--

padwin = get_window(parameter);

nfft = parameter.fft;

next = floor(parameter.hop * nfft);

nextwin = [zeros(next,1); padwin(1:(nfft - next))];

%--
% display window and shifted window
%--
	
handle = [];

handle(end + 1) = plot(padwin, 'k');

set(handle(end), ...
	'linewidth', 1 ...
);

hold on;

handle(end + 1) = plot(ax, nextwin, ':k');

handle(end + 1) = plot(ax, [1, nfft], [0, 0], ':');

handle(end + 1) = plot(ax, [1, nfft], [1, 1], ':');

set(handle(end - 1:end), ...
	'color', 0.5 * ones(1,3) ...
);

set(ax, ...
	'xlim', [1, nfft], ...
	'ylim', [-0.2, 1.2] ...
);

% NOTE: make sure that hidden axes plots remain hidden

set(handle, 'visible', get(ax, 'visible'));

