function set_browser_extension(par, ext, state)

% set_browser_extension - set browser extension store
% ---------------------------------------------------
%
% set_browser_extension(par, ext, state)
%
% Input:
% ------
%  par - browser
%  ext - extension
%  state - state

% TODO: this function should be callable from 'update_extension_store'

%-------------------------
% STORE IN ROOT BROWSER
%-------------------------

% NOTE: this can be a dangerous operation

if par == 0
	extensions_cache(ext); return;
end

%-------------------------
% UPDATE STATE
%-------------------------
	
data = get_browser(par);

%--
% get current extension and index, then update extension
%--

if ~isempty(ext)

	[current, ix, context] = get_browser_extension(ext.subtype, par, ext.name, data);

	context.ext = ext;
	
	% NOTE: in the case of presets 'ext' will be 'preset.ext'

	data.browser.(current.subtype).ext(ix) = ext;

end

%--
% update widget states if needed
%--

if nargin > 2
	
	if isempty(data.browser.widget_states)
		
		data.browser.widget_states = state;
	
	else
		
		ix = find(strcmp(state.name, {data.browser.widget_states.name}));
		
		if isempty(ix)
			data.browser.widget_states(end + 1) = state;
		else
			data.browser.widget_states(ix) = state;
		end
		
	end
	
end

%--
% store updated state
%--

set(par, 'userdata', data);

%-------------------------
% REFLECT STATE
%-------------------------

% NOTE: under what conditions does this happen?

if isempty(ext)
	return;
end

% NOTE: this only works for extensions that have control palettes

%--
% check for palette to update
%--

pal = get_extension_palette(ext, par);

if isempty(pal)
	return;
end

%--
% check for controls
%--

% NOTE: when a type has presets, it implements a control create function

[value, types] = has_any_presets(ext);

if ~value
	return;
end

%--
% get various types of controls, update palette, and ensure consistent display
%--

for type = list(types)
	
	%--
	% get control creation helper for type
	%--
	
	% NOTE: the warning produced below relates to the strange 'cell' iterator behavior
	
	type = type{1}; %#ok<FXSET>
	
	if strcmp(type, 'compute')
		helper = ext.fun.parameter.control.create; parameter = ext.parameter;
	else
		helper = ext.fun.(type).parameter.control.create; parameter = ext.parameters.(type);
	end
	
	% NOTE: we create the control to get the callbacks
	
	try
		control = helper(parameter, context);
	catch
		extension_warning(ext, ['''', title_caps(type), ''' parameters control creation failed.'], lasterror); continue;
	end

	%--
	% update controls and make display consistent
	%--

	% NOTE: this fails if parameter compilation clobbers controlled parameter values
	
	% NOTE: this also fails when various types of parameters have the same fieldnames
	
	before = get_control_values(pal);
	
	set_control_values(pal, parameter);

	after = get_control_values(pal);
	
	% NOTE: the onload callback ensures consistent display

	% TODO: consider moving onload into 'set_control_values'
	
	palette_onload(pal, control);
	
end 


%-------------------------
% PALETTE_ON_LOAD
%-------------------------

function palette_onload(pal, control)

% TODO: order 'onload' callbacks by making value a position and indicator

%--
% check for controls
%--

if (nargin < 2) || isempty(control)
	return;
end

%--
% check for onload
%--

onload = [control.onload];

if ~any(onload)
	return;
end

%--
% perform onload callbacks
%--

for k = 1:length(onload)
	
	if onload(k)
		control_callback([], pal, control(k).name);
	end
	
end


