function spectrogram_controls_callback(callback)

% spectrogram_controls_callback - callbacks for spectrogram palette controls
% --------------------------------------------------------------------------
%
% spectrogram_controls_callback(callback)
%
% Input:
% ------
%  callback - callback context

%--
% unpack callback
%--

control = callback.control; pal = callback.pal;

%---------------------------
% PERFORM CALLBACKS
%---------------------------

%--
% get control state
%--

% NOTE: the callback control is not as rich as the get control? this may be outdated

value = callback.control.value;

handles = get_control(pal.handle, control.name, 'handles');

%--
% update interface elements
%--

switch control.name

	case 'fft'
		% NOTE: we display fft size factorization
		
		if handles.uicontrol.edit, set(handles.uicontrol.edit, 'tooltipstring', factor_str(value)); end

	case 'hop_auto'
		
		set_control(pal.handle, 'hop', 'enable', ~value);


	case 'sum_auto'

		set_control(pal.handle, 'sum_length', 'enable', ~value);

	case 'win_type'
		
		% NOTE: check for parametrized window and update parameter value and control
		
		parameter = window_to_fun(value, 'parameter');
		
		handles = get_control(pal.handle, 'win_param', 'handles'); obj = handles.uicontrol.slider;
		
		if isempty(parameter)	
			set_control(pal.handle, 'win_param', 'enable', 0);
			
			set(handles.uicontrol.slider, ...
				'min', 0, ...
				'max', 1, ...
				'value', 0 ...
			);

			set(handles.uicontrol.edit, 'string', []);
		else			
			set_control(pal.handle, 'win_param', 'enable', 1);

			set(handles.uicontrol.slider, 'min', parameter.min, 'max', parameter.max);
			
			set_control(pal.handle, 'win_param', 'value', parameter.value);
		end
end

%--
% update window plot
%--

parameter = get_control_values(pal.handle);

parameter = struct_iterate(parameter, @(value)(cellfree(value)));

window_plot(pal.handle, parameter);