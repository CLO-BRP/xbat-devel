function name = browser_name(sound)

% browser_name - browser figure name
% ----------------------------------
%
% name = browser_name(sound)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  name - name

name = [' XBAT Sound  -  ' sound_name(sound)];