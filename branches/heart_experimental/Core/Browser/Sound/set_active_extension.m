function active = set_active_extension(mode, ext, par)

% set_active_extension - set extension as active extension
% --------------------------------------------------------
%
% active = set_active_extension('set', ext, par)
%
%        = set_active_extension('add', ext, par)
% 
%        = set_active_extension('del', ext, par)
%
% Input:
% ------
%  par - browser
%  ext - extension
%
% Output:
% -------
%  active - names of active extensions of type

%-----------------
% HANDLE INPUT
%-----------------

%--
% set active browser default
%--

if nargin < 3
	par = get_active_browser;
end

if isempty(par)
	active = {}; return;
end

%--
% check mode input
%--

if ~ismember(mode, {'set', 'add', 'del'})
	
	error('Unrecognized set mode.');
	
end

%-----------------
% SET ACTIVE
%-----------------

%--
% get browser state
%--

data = get_browser(par);

%--
% check extension is available for consideration
%--

type = ext.subtype; name = ext.name;

ix = find(strcmp(name, {data.browser.(type).ext.name}), 1);

if isempty(ix)
	return;
end
	
%--
% update active state store according to mode
%--

active = data.browser.(type).active;

% TODO: this set code should be simpler, always use cells instead of strings

if ischar(active)
	
	if isempty(active)
		active = {};
	else
		active = {active};
	end
	
end

switch mode
	
	case 'set'
		active = name;

	case 'add'
		active = union(active, name);
		
	case 'del'
		active = setdiff(active, name);
		
end

if isempty(active)
	active = '';
end

if iscell(active) && (length(active) == 1)
	active = active{1};
end

data.browser.(type).active = active;

%--
% update browser state
%--

set(par, 'userdata', data);

%--
% reflect state
%--

% NOTE: the reflection of state should be handled by each dispatcher, extension type extension

% TODO: update palettes, update menus, refresh browser
