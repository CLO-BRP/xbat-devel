function info = get_browser_info(par)

% get_browser_info - get basic browser info
% -----------------------------------------
%
% info = get_browser_info(par)
%
% Input:
% ------
%  par - browser handle
%
% Output:
% -------
%  info - browser info

% TODO: create 'set_browser_info' to store browser info?

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% check handle input
%--

if any(~ishandle(par) & par)
	error('Handle input is required.');
end

%--
% check figure handles
%--

if any(~strcmp(get(par, 'type'), 'figure') & par)
	error('Input handles must be figure handles.');
end

%--
% handle arrays recursively
%--

if numel(par) > 1
	
	for k = 1:numel(par)
		info(k) = get_browser_info(par(k));
	end
	
	info = reshape(info, size(par)); return;
	
end

%------------------------------
% GET BROWSER INFO
%------------------------------

if par
	
	info = parse_browser_tag(get(par, 'tag'));
	
else
	
	% NOTE: the root browser does not have an associated sound
	
	% NOTE: the presence of the string 'BROWSER' declares this as a browser
	
	info.type = 'app_root_BROWSER';
	
	info.user = get_field(get_active_user, 'name');
	
	info.library = get_library_name(get_active_library);
	
	info.sound = '';
		
end

% info.handle = par;
