function run_tests(root)

% run_tests - find and run all tests
% ----------------------------------
%
% test = run_tests(root)
%
% Input:
% ------
%  root - of tests scan
%
% Output:
% -------
%  test - results

% NOTE: we have variable input consider adding say 'logger' options and function

if ~nargin
	root = pwd;
end

%--
% find test files
%--

files = find_tests(root);

if isempty(files) 
	disp(' '); disp(root); disp(' '); disp('  No tests found.'); disp(' '); return;
end

%--
% run tests
%--

test = empty(result_create);

disp(' ');

for k = 1:numel(files)
	
	% NOTE: 'tests' contains files, we get 'test' the function name
	
	[parent, name, ignore] = fileparts(files{k});
	
	%--
	% perform test
	%--

	test(k) = result_create('name', name, 'file', files{k}, 'started', clock);
	
	disp(['Running test ''', name, ''' from ''', parent '''.']);
	
	try
		feval(name); test(k).passed = 1;
	catch	
		test(k).passed = 0; test(k).error = lasterror;
	end
	
	test(k).elapsed = etime(clock, test(k).started);
	
	%--
	% report on test result
	%--
	
	if test(k).passed
		disp(['Passed.  (', sec_to_clock(test(k).elapsed), ')']);
	else
		disp(['Failed.  (', sec_to_clock(test(k).elapsed), ')']);
	end
	
	if ~test(k).passed
		nice_catch(test(k).error);
	end
	
	disp(' ');
	
end

%--
% simple summary report
%--

disp('________________________________________________________________');
disp('________________________________________________________________');
disp(sprintf('\n'));
disp(sprintf('Finished running %d tests.', numel(test)));
disp(sprintf('\n'));
    
if any(~[test.passed])
    
	disp('The following tests FAILED:');
    for i=1:numel(test)
        if ~test(i).passed
            disp(['  ', test(i).file]);
        end
    end
    
    % disp(test)
else
	disp('All tests passed.');
end

disp(sprintf('\n'));

%-----------------------------------
% RESULT_CREATE
%-----------------------------------

function test = result_create(varargin)

%--
% create prototype
%--

test.name = '';

test.file = '';

test.passed = [];

test.started = [];

test.elapsed = [];

test.error = struct;

%--
% set fields from input provided
%--

if ~numel(varargin)
	return; 
end

test = parse_inputs(test, varargin{:});

