function config = get_test_database(adapter, database)

% get_test_database - configuration for specific adapter
% ------------------------------------------------------
%
% config = get_test_database(adapter, database)
%
% Input:
% ------
%  adapter - name
% 
% Output:
% -------
%  config - for test database
% 
% NOTE: this function clears the database requested

%--
% set default adapter
%--

if ~nargin
    adapter = 'sqlite';
end

%--
% load test configuration from file
%--

config = load_database_config(which('test.yml'), adapter);

if nargin > 1
	config.database = database; config = create_database_config(config);
end

% NOTE: 'database' input means the YAML description is not responsible for the database name

% NOTE: it should not be a problem as this function should be able to 'drop' and 'create' databases

% NOTE: 'database' input is a full file in the case of SQLite

%--
% make sure we start with a clean database
%--

switch config.adapter

	case 'sqlite'

		if exist(config.file, 'file')
			delete(config.file)
		end

	case lower(known_jdbc_adapter)

		% TODO: drop and create database config.database

	otherwise

		error('What are you doing? Step away from the keyboard.');

end

