function image = get_image(site, name)

% GET_IMAGE get image file from site by name
%
% image = get_image(site, name)

image = get_asset(site, 'images', name);
