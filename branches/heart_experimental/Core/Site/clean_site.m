function clean_site(site, type)

% clean_site - to endure fresh build
% ----------------------------------
%
% clean_site(site, type)
%
% Input:
% ------
%  site - name
%  type - of cleanup 'all', 'images', 'scripts', 'styles'

%--
% handle input
%--

if nargin < 2
	type = 'all';
end

%--
% setup
%--

fclose all;

root = build_root(site);

%--
% clean according to type
%--

switch type

	case 'all'
		
	% NOTE: this will not be typically sufficient to ensure that the resources are re-created
	
	case {'images', 'scripts', 'styles'}, root = fullfile(root, type);
		
	otherwise, error(['Unrecognized cleanup type ''', type, '''.']);

end

content = no_dot_dir(root);

for k = 1:length(content)

	current = fullfile(root, content(k).name);

	if content(k).isdir
		remove_path(current); rmdir(current, 's');
	else
		delete(current);
	end

end