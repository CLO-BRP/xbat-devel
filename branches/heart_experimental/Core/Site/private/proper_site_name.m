function value = proper_site_name(name)

% PROPER_SITE_NAME check for proper site name
%
% value = proper_site_name(name)

value = proper_filename(name) && ~any(isspace(name));
