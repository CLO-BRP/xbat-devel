function str = prepare_label(str)

% PREPARE_LABEL prepare string for display 
%
% str = prepare_label(str)

% TODO: consider known acronyms and special characters that need escape

if strmatch('api', str)
	str(1:3) = upper(str(1:3));
end 

if strmatch('svn', str)
	str(1:3) = upper(str(1:3));
end 

str = title_caps(str);