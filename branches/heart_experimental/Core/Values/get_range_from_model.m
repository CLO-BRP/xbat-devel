function value = get_range_from_model(model)

value = flatten(model);

field = fieldnames(value); 

% TODO: consider the possibility of struct field description as well

for k = 1:length(field)
	value.(field{k}) = value.(field{k}){2};
end

value = unflatten(value);
