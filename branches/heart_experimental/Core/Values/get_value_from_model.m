function value = get_value_from_model(model)

value = flatten(model);

field = fieldnames(value); 

% TODO: consider the possibility of field description as struct also

% NOTE: the value is stored in the first cell of the field description

for k = 1:length(field)
	value.(field{k}) = value.(field{k}){1};
end

value = unflatten(value);