function sounds = migrate_sounds(source, lib)

% migrate_sounds - migrate library sounds
% ------------------------------------------
% 
% sounds = migrate_sounds(source, lib)
%
% Input:
% ------
%  source - source sounds root
%  lib - library to which to migrate sounds
%
% Output:
% -------
%  sounds - migrated sounds

%--
% get active library if needed
%--

if nargin < 2 || isempty(lib)
	lib = get_active_library;
end

%--
% setup
%--

names = get_folder_names(source);

if numel(names) == 0
	sounds = []; return;
end

%--
% waitbar update
%--

migrate_wait('Sounds', length(names), names{1});

%--
% migrate sounds
%--

sounds = {};

for k = 1:length(names)
	sounds{end + 1} = migrate_sound([source, filesep, names{k}], lib);
end

sounds = [sounds{:}];

%--
% refresh library cache
%--

get_library_sounds(lib, 'refresh');


