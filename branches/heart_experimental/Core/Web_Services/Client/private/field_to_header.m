function header = field_header(field)

header = strrep(title_caps(field), ' ', '-');