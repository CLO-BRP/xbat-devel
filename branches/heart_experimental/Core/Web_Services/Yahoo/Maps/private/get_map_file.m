function [file, key, format] = get_map_file(opt)

% get_map_file - get specified map file name
% ------------------------------------------
%
% [file, key, format] = get_map_file(format, opt)
%
% Input:
% ------
%  format - map image format ('.png' or '.gif')
%  opt - map request options struct
%
% Output:
% -------
%  file - map image file name
%  key - key
%  format - map image format

%--
% handle input
%--

% NOTE: this default image type could change if the service changes

if isempty(opt.image_type)
	format = '.png';
end

%--
% get key and build filename
%--

key = get_map_key(opt);

file = fullfile(map_cache_root, [key, format]);





