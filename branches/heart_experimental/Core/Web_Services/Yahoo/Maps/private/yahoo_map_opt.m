function opt = yahoo_map_opt

% yahoo_map_opt - build yahoo map options struct
% ----------------------------------------------
%
% opt = yahoo_map_opt
%
% Output:
% -------
%  opt - options struct

%--
% set yahoo API fields
%--

opt.appid = get_appid;

opt.street = '';

opt.city = ''; 

opt.state = ''; 

opt.zip = [];

opt.location = '';

opt.latitude = [];

opt.longitude = [];

opt.image_type = '';

opt.image_height = [];

opt.image_width = [];

opt.zoom = [];

% NOTE: the default is a one mile radius display

opt.radius = 1;

opt.output = '';

%--
% set other fields
%--

% NOTE: by setting this field we can skip the cache and get the map again

opt.skip_cache = 0; 


% TODO: add 'varargin' named parameter list input along with some validation code


%-------------------
% GET_APPID
%-------------------

function appid = get_appid

persistent APP_ID;

if ~isempty(APP_ID)
	appid = APP_ID; return;
end

appid = urlread('http://xbat.org/map');

APP_ID = appid;

