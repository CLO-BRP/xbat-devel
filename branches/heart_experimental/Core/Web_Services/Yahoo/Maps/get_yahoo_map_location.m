function [file, result, request] = get_yahoo_map_location(location, radius, opt)

% get_yahoo_map_location - get map from yahoo map service by location
% -------------------------------------------------------------------
%
% [file, result] = get_yahoo_map_location(location, radius, opt)
%
% Input:
% ------
%  location - location 
%  radius - display radius in miles
%  opt - map request options
%
% Output:
% -------
%  file - map image file
%  result - url from result of map request
%  request - request url

%--
% handle input
%--

if nargin < 3
	opt = yahoo_map_opt;
end

% NOTE: we store all the inputs into an options struct

if exist('location', 'var')
	opt.location = location; 
end

if exist('radius', 'var');
	opt.radius = radius;
end

%--
% get file name and check cache
%--

file = get_map_file(opt);

if exist(file, 'file') && ~opt.skip_cache
	result = ''; request = ''; return;
end 

%--
% get map image file from yahoo
%--

request = build_yahoo_map_url(opt);

% TODO: add exception handling to network requests

result = parse_yahoo_result(urlread(request));

urlwrite(result, file);


