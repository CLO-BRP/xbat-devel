function mode = get_browser_mode(par)

%--
% get active browser if needed
%--

if ~nargin
	par = get_active_browser;
end

if isempty(par)
	return; 
end

%--
% determine current mode
%--

% NOTE: this function should evolve as the modes themselves evolve

down = get(par, 'windowbuttondownfcn'); up = get(par,'windowbuttonupfcn');

if isempty(down) && isempty(up)
	mode = 'select';
else
	mode = 'hand';
end 