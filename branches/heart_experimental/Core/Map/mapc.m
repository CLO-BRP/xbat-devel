function result = mapc(value, range, mode, limits)

% mapc - scalars to color range
% -----------------------------
%
% result = mapc(value, range, mode, limits)
% 
% Input:
% ------
%  value - to map
%  range - of colors
%  mode - for color interpolation 'hsv' (default) or 'rgb'
%  limits - for input values
%
% Output:
% -------
%  result - colors

%--
% handle input
%--

if nargin < 4
	limits = fast_min_max(value);
end

if nargin < 3
	mode = 'rgb';
end

if nargin < 2
	range = 0.75 * [1 0 0; 0 1 0]; 
end

%--
% map values
%--

if strcmp(mode, 'hsv')
	range = rgb_to_hsv(range);
end

value = value(:);

for k = 1:3
	scale = (range(2, k) - range(1, k)) / diff(limits);

	result(:, k) = scale * (value - limits(1)) + range(1, k);
end

size(result)

if strcmp(mode, 'hsv')
	result = hsv_to_rgb(result);
end
