function scroll_callback(obj, eventdata, par)

% scroll_callback - timer callback to implement scrolling display
% --------------------------------------------------------------
%
% scroll_callback(obj, eventdata, par)
%
% Input:
% ------
%  obj - callback object
%  eventdata - reserved by matlab
%  par - browser handle

%----------------------
% SETUP
%----------------------

%--
% get and check browser handle
%--

if (nargin < 3) || isempty(par)
	par = get_active_browser;
end

if ~is_browser(par)
	return;
end

%--
% get slider value
%--

slider = get_time_slider(par);

if isempty(slider)
	widget_timer_update(par); return;
end

%----------------------
% SCROLL IF NEEDED
%----------------------

% TODO: figure out where the scroll request originates

%--
% check for slider change
%--

change = get_browser_history(par, 'time') - slider.value;

if ~change
	widget_timer_update(par); return;
end

%--
% detect slider motion
%--

motion = slider.previous_value - slider.value; 

%--
% detect a slow-update condition
%--

slow_update = get_browser_history(par, 'elapsed') > 0.5 * get(obj, 'period');

%--
% select update mode
%--

if isempty(motion) || isempty(slow_update)
	fast = 0;
else
	fast = motion && slow_update;
end

%--
% perform update
%--

try	
	browser_refresh(par, [], fast); 
catch
	nice_catch(lasterror);
end

set_time_slider(par, 'previous_value', slider.value);

%--
% update widgets
%--

widget_timer_update(par);


%----------------------
% WIDGET_TIMER_UPDATE
%----------------------

function widget_timer_update(par)

%--
% get listening widgets
%--

widget = get_widgets(par, 'timer');

if isempty(widget)
	return;
end

%--
% update widgets
%--

update_widgets(par, 'timer');