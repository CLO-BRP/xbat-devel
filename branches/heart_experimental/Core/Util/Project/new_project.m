function [project, root, created] = new_project(name, description, author, repository)

%--
% use dialog
%--

if ~nargin
	[project, root, created] = new_project_dialog; return;
end

%--
% handle input
%--

if nargin && isstruct(name)

	% NOTE: we get the 'name' field last
	
	description = name.description; author = name.author; repository = name.repository; name = name.name;
else
	if nargin < 4
		repository = ''; 
	end
	
	if nargin < 3
		author = '';
	end
	
	if nargin < 2
		description = '';
	end
	
	if nargin < 1
		name = '';
	end 
end 

%--
% check for project, otherwise start creating
%--

% NOTE: we should return -1 if we fail (or cancel, in the case of the dialog) to create project

if project_exists(name, author);
	project = get_project(name, author); root = project_root(project); created = 0; return;
end

project = project_create(name, description, author, repository);

%--
% create directories
%--

% NOTE: the project root directory is the start of the project

[root, created] = create_dir(project_root(project));

% NOTE: when we create a project we also create children directories

if created
	append_path(root); append_path(project_extensions_root(project));
end

%--
% create project function and get project
%--

generate_project_function(project); 

project = get_project(project);



