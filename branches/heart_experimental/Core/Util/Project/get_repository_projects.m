function [project, source] = get_repository_projects(repository)

% get_repository_projects - starting from repository address
% ----------------------------------------------------------
%
% [project, source] = get_repository_projects(repository)
%
% Input:
% ------
%  repository - root address
% 
% Output:
% -------
%  project - name
%  source - location for project
%
% See also: get_project_repositories

% NOTE: we should not worry about namespace problems, MATLAB does not allow this really, not yet

%--
% handle input
%--

if ~nargin
	repository = get_project_repositories;
end 

if ischar(repository)
	repository = {repository};
end

%--
% get project names from repositories
%--

project = {}; source = {}; 

for k = 1:numel(repository)
	%--
	% check repository
	%--
	
	[repo, leaf] = projects_repository_root(repository{k});
	
	[status, result] = svn('list', repo);
	
	%--
	% report failure if needed
	%--
	
	% NOTE: the second part of the test consider whether a projects root directory is available
	
	if status && isempty(strfind(result, [leaf, ''' non-existent']))
		
		str = [' WARNING: Retrieval from ''', repository{k}, ''' failed.']; sep = str_line(72, '_');
		
		disp(sep); disp(' '); disp(str); disp(sep); disp(' '); 
		
		disp(' MESSAGE:'); disp(' '); disp(result); disp(sep); disp(' ');
	end
	
	%--
	% pack output
	%--
	
	part = file_readlines(result);
	
	% NOTE: remove non-directory elements of list
	
	for j = numel(part):-1:1
		% NOTE: we remove the trailing directory indicator to get the name
		
		if part{j}(end) == '/'
			part{j}(end) = [];
		else
			part(j) = []; 
		end
	end
	
	for j = 1:numel(part)
		project{end + 1} = part{j}; source{end + 1} = repository{k};
	end
end

