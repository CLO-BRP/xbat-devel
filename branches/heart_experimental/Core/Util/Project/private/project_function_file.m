function file = project_function_file(name, author)

file = fullfile(project_root(name, author), [genvarname(name), '__project.m']);