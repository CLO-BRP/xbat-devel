function [root, leaf] = projects_repository_root(repository)

leaf = 'XBAT_PROJECTS'; root = [repository, '/', leaf];