function repo = new_repository_dialog

% TODO: turn this into a manage repositories dialog as well, a different name will make sense

% TODO: add controls to create new repository

% TODO: support BZR as well as SVN

% TODO: add code to validate repository exists when we add it, may help with typos

%--
% create controls
%--

control = header_control('Repository', 0);

control(end + 1) = control_create( ...
	'name', 'url', ...
	'alias', 'URL', ...
	'style', 'edit', ...
	'space', 1.5 ...
);

% control(end + 1) = control_create( ...
% 	'name', 'repositories', ...
% 	'style', 'popup', ...
% 	'tab', tabs{2}, ...
% 	'space', 1, ...
% 	'string', get_project_repositories, ...
% 	'value', 1 ...
% );

%--
% render controls
%--

opt = dialog_group; opt.width = 15;

out = dialog_group('Add Repository ...', control, opt);

if isempty(out.values)
	repo = []; return;
end

repo = out.values.url;


