function ext = get_project_extensions(project, type)

if nargin < 2
	type = ''; 
end

if isstruct(project)
	project = project.name;
end 

ext = get_extensions(type, 'project', project);
