function handle = complex_scatter(par, z, value, map, varargin)

% complex_scatter - points in plane with value
% --------------------------------------------
% 
% handle = complex_scatter(par, z, value, 'field', value, ... )
%
% map = complex_scatter
%
% Input:
% ------
%  par - axes 
%  z - point positions
%  value - to map, as many scalars as points
%  map - description
%  field, value - pairs
%
% Output:
% -------
%  handle - array to lines created
%  map - default
%
% NOTE: the 'map' input describes how to map values to display
%
% See also: complex_field, complex_plot, complex_text

%--
% handle input
%--

if nargin < 4 || trivial(map)
	% NOTE: by default we map the values to the markersize in given range
	
	map.prop = 'markersize';
	
	map.range = [4, 32];
	
	map.colormap = @jet;
	
	if ~nargin
		handle = map; return; 
	end 
end

if numel(z) ~= numel(value)
	error('Number of position points and values should be equal.');
end
	
%--
% plot points with complex positions
%--

handle = zeros(size(z)); 

value = map_values(map, value);

for k = 1:numel(z)
	
	handle(k) = line( ...
		'parent', par, ...
		'xdata', real(z(k)), ...
		'ydata', imag(z(k)), ...
		'linestyle', 'none', ...
		'marker', 'o' ...
	);
	
	% TODO: we could generalize this, that is one reason to keep this as a separate line
	
	set(handle(k), map.prop, value(k, :));
end

%--
% set any requested properties
%--

if ~isempty(varargin)
	
	set(handle, varargin {:});
end


%--------------------------
% MAP_VALUES
%--------------------------

function output = map_values(map, input)

range = fast_min_max(input);

if string_contains(map.prop, 'color')
	
	color = map.colormap(256); 
	
	index = (255 / diff(range)) * (input - range(1)) + 1; index = round(index(:));
	
	output = color(index, :);
else
	output = (diff(map.range) / diff(range)) * (input - range(1)) + map.range(1);
	
	output = output(:);
end
