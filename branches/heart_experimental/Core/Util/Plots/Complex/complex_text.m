function handle = complex_text(par, z, value, varargin)

% complex_text - text with complex positioning
% --------------------------------------------
% 
% handle = complex_text(par, z, str, 'field', value)
%
% Input:
% ------
%  par - axes 
%  z - positions for text
%  value - strings for text
%  field, value - pairs
%
% Output:
% -------
%  handle - array to lines created
%
% NOTE: this function plots point sets as lines, for point plots use 'scatter_complex'
%
% See also: complex_field, complex_plot, complex_scatter

%--
% handle input
%--

if numel(z) ~= numel(value)
	error('Number of position points and string values should be equal.');
end

if ~iscellstr(value)
	error('String value input should be a cell array of strings.');
end

%--
% plot text with complex positions
%--

handle = zeros(size(z));

for k = 1:numel(z)
	handle(k) = text(real(z(k)), imag(z(k)), value{k});
end

set(handle, 'parent', par);

%--
% set any requested properties
%--

if ~isempty(varargin)
	
	set(handle, varargin {:});
end