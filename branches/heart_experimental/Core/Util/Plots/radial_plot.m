function radial_plot

% radial_plot - for data with a circular variable
% -----------------------------------------------
%
% radial_plot
%
% Input:
% ------
%  t - circular variable
%  r - radial variable
%  field, value - pairs
%
% Output:
% -------
%  handle - struct to objects created
%
% See also: set_radial_grid, polar

% NOTE: consider annular and wedge plots, try to avoid polar distortion

% NOTE: consider migration pattern displays, extent, intensity, availability for observation
