function A = add_matrix_row(A, k, r)

% add_matrix_row - add row to matrix after given row position
% -----------------------------------------------------------
%
% A = add_matrix_row(A, k, r)
%
% Input:
% ------
%  A - matrix to update
%  k - add position
%  r - row to add
%
% Output:
% -------
%  A - updated matrix


%--
% handle input
%--

[m, n] = size(A);

% NOTE: by default we add zeros

if nargin < 3
	r = zeros(1, n);
else
	if numel(r) ~= n
		error('Row to add must have proper number of elements.');
	end
end

% NOTE: by default we append a row to the matrix

if nargin < 2
	k = m;
else
	if (k ~= round(k)) || (k < 0) || (k > m)
		error('Row add position must be an integer between 0 and the starting number of rows.');
	end
end

%--
% update matrix
%--

A = [A(1:k, :); r; A(k + 1:end, :)];