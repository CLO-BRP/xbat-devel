function [lines, numbers] = todos(fun)

% todos - display todos
% ---------------------
%
% [lines, numbers] = todos(fun)
%
% Input:
% ------
%  fun - function or handle
%
% Output:
% -------
%  lines - declaring todos
%  numbers - of todo lines

prefix = '% TODO:';

% TODO: consider input for this function, filenames, function names, iteration

% TODO: use on a directory and apply to every file in the directory

% TODO: factor rest of function

% NOTE: we read, test, and select lines matching prefix

file = which(fun);

lines = file_readlines(file); 

numbers = strmatch(prefix, lines); 

lines = lines(numbers);

if ~nargout
	
	% TODO: align line numbers in display
	
	% TODO: link display to opening of file to line
	
	disp(' ');
	
	disp(['   ', file]);
	disp(' ');
	iterate(@disp, strcat({'   '}, iterate(@int2str, numbers), {'. '}, lines)); 
	
	disp(' '); 
	
	clear lines numbers;
	
end