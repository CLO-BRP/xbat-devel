function [flag, result] = tab_select(par, pal, tab, data, callback)

% tab_select - select tab in control group
% ----------------------------------------
%
% flag = tab_select(par, pal, tab, data)
%
% Input:
% ------
%  par - parent
%  pal - name or handle
%  tab - name
%  data - parent state
%
% Output:
% -------
%  flag - selection

%----------------------------------------------------
% HANDLE INPUT
%----------------------------------------------------

if nargin < 5
	callback = 1;
end

% TODO: modify so that we can use this function from within 'control_group'

%--
% see if we need to get palette handle from the parent
%--

if isempty(par)
	
	% NOTE: when no parent is provided palette needs to be a figure handle
		
	if ~ishandle(pal) || ~strcmp(get(pal, 'type'), 'figure')
		error('Palette input must be figure handle.'); 
	end
	
else
	
	%--
	% when a parent is provided see if palette is a string
	%--
	
	if ischar(pal)
		
		if (nargin < 4) || isempty(data)
			data = get(par, 'userdata');
		end
		
		pal = get_palette(par, pal, data);
		
	else
		
		if ~ishandle(pal) || ~strcmp(get(pal, 'type'), 'figure')
			error('Palette input must be figure handle.');
		end
		
	end
	
end

%--
% return if we did not get the palette handle
%--

flag = 0; result = struct;

if isempty(pal)
	return;
end

%--
% check for tab input
%--

if (nargin < 3) || isempty(tab)
	return;
end

%----------------------------------------------------
% SELECT TAB
%----------------------------------------------------

%--
% get compiled tabs from palette
%--

data = get(pal, 'userdata');

tabs = data.tabs;

% NOTE: return quickly if there are no tabs

if isempty(tabs)
	return;
end

%--------------------------------
% UPDATE CONTROLS
%--------------------------------

%--
% find tab to select in tabs, the latter are the various tab groups or controls
%--

tab_ix = [];

for k = 1:length(tabs)
	
	%--
	% get tab index within tabs and parent tabs
	%--
	
	tab_ix = find(strcmp(tabs(k).tab.name, tab), 1);
	
	% NOTE: we can discard other tabs at this point
	
	if ~isempty(tab_ix)
		tabs = tabs(k); break;
	end
	
end

% NOTE: return quickly if we did not find tab in tabs

if isempty(tab_ix)
	return;
end

%--
% hide other tab controls and show selected tab controls
%--

for k = 1:length(tabs.child.name)
	
	switch tabs.child.parent{k}

		case tab
			set_control(pal, tabs.child.name{k}, 'command', '__SHOW__');
			
		otherwise
			set_control(pal, tabs.child.name{k}, 'command', '__HIDE__');
			
	end
	
end

%--------------------------------
% UPDATE TABS
%--------------------------------

%--
% get actual tab object
%--

% NOTE: rectangle and text tagged with tab name, not tabs name

obj = findobj(findall(pal, 'type', 'rectangle'), 'flat', 'tag', tab);

if isempty(obj)
	return;
end

%--
% get tab container
%--

container = findall(get(obj, 'parent'));

%----------------------------------------------------
% HIDE ALL TABS
%----------------------------------------------------

% NOTE: background face color is darker than the selected tab face color

% TODO: the tab colors are hard coded here and in 'control_group', fix this

%--
% send all tabs to back
%--

set(findobj(container, 'type', 'rectangle'), ...
	'edgecolor', 0.6 * ones(1, 3), ...
	'facecolor', 0.95 * get(pal, 'color') ...	
);

set(findobj(container, 'type', 'text'), ...
	'color', 0.6 * ones(1, 3) ...
);

%--
% the lines help to produce the physical effect of the tab setting
%--

set(findobj(container, 'type', 'line'), ...
	'visible', 'on' ...
);

%-------------------------------
% SHOW CURRENT TAB
%-------------------------------

%--
% bring tab to front
%--

set(findobj(container, 'tag', tab, 'type', 'line'), ...
	'visible', 'off' ...
);

set(findobj(container, 'tag', tab, 'type', 'text'), ...
	'color', zeros(1, 3) ...
);

set(findobj(container, 'tag', tab, 'type', 'rectangle'), ...
	'edgecolor', zeros(1, 3), ...
	'facecolor', get(pal, 'color') ...
);

flag = 1;

%-------------------------------
% TRIGGER TAB CALLBACK
%-------------------------------

% NOTE: tab callbacks can cause problems when extension 'context' is truly needed

if ~callback || ~data.opt.tab_callbacks
	return;
end

%--
% get tabs control and evaluate callback if found
%--

control = data.control(tabs.ix);

if isempty(control.callback)
	return;
end

% TODO: consider a direct way of connecting this callback by chaining callbacks

result = eval_callback(control.callback, obj, []);




