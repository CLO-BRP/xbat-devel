function value = get_set_value(varargin)

% get_set_value - get value of control possibly considering 'set' behavior
% ------------------------------------------------------------------------
% 
% value = get_set_value(control, value)
%
% Input:
% ------
%  control - to get value for
%  value - to 'set'
%
% Output:
% -------
%  value - after 'set'

value = get_value_through_callback('set', varargin{:});
