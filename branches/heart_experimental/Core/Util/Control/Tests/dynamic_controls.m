function out = dynamic_controls(mode)

%--
% set default mode for test
%--

if ~nargin
	mode = 'palette';
end 

modes = {'palette', 'dialog'};

if ~ismember(mode, modes)
	error('Test mode must be ''palette'' or ''dialog''.');
end

%--
% create palette starting from figure
%--

pal = figure;

name = 'Dynamic';

fun = @dynamic_callback;

% NOTE: typically we will have a control that indicates the controls being used

% NOTE: for example, in the case of extensions we may have a 'simple' and 'expert' interface

control = control_set('A'); 

switch mode
	
	case 'palette'
		
		opt = control_group; opt.header_color = (2 * [1 0.5 0] + 1) / 3;
		
		out = control_group([], fun, name, control, opt, pal);

	% TODO: update 'dialog_group' to support initial figure input
	
	case 'dialog'
		
		opt = dialog_group; opt.header_color = (2 * [0 0.5 1] + 1) / 3;
		
		out = dialog_group(name, control, opt, fun, [], pal);

end


%-------------------
% DYNAMIC_CALLBACK
%-------------------

function dynamic_callback(obj, eventdata)

%--
% get callback context
%--

callback = get_callback_context(obj, eventdata); pal = callback.pal;

%--
% dispatch callback
%--

switch callback.control.name
	
	case 'select'

		%--
		% get figure name and value of control set select control
		%--
		
		name = get(pal.handle, 'name');

		value = get_control(pal.handle, 'select', 'value'); control = control_set(value{1});

		% NOTE: this call can preserve control values for similarly named controls

		control = update_control_values(control, get_control_values(pal.handle));
			
		%--
		% reload palette or dialog
		%--
		
		if ~is_dialog(pal.handle)
			
			control_group([], @dynamic_callback, name, control, [], pal.handle);
			
		else
			
			dialog_group(name, control, [], @dynamic_callback, [], pal.handle);
			
		end

	otherwise
		
end


%-------------------
% CONTROL_SET
%-------------------

function control = control_set(name)

%--
% create header, tabs, and control selection control
%--

control = empty(control_create); 

control(end + 1) = control_create( ... 
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'string','Dynamic' ...
);

tabs = {'Tab 1', 'Tab 2'};

names = {'A', 'B', 'C'}; value = find(strcmp(name, names), 1);

if isempty(value)
	value = 1; 
end

control(end + 1) = control_create( ... 
	'name', 'select', ...
	'style', 'popup', ...
	'space', 1.5, ...
	'string',names, ...
	'value', value ...
);

control(end + 1) = control_create( ... 
	'style', 'separator', ...
	'space', 0.11 ...
);

control(end + 1) = control_create( ... 
	'style', 'tabs', ...
	'tab', tabs ...
);

% NOTE: the selection control's value is set based on the name input!

%--
% create unique name control set controls
%--

switch name
	
	case 'A'
		
		for k = 1:4
			control(end + 1) = control_create( ... 
				'name', ['slider_', int2str(k)], ...
				'style', 'slider', ...
				'tab', tabs{1}, ...
				'string', {'A', 'B', 'C'}, ...
				'value', 1 ...
			);
		end
		
		for k = 1:3
			control(end + 1) = control_create( ...
				'name', ['check_', int2str(k)], ...
				'style', 'checkbox', ...
				'tab', tabs{2} ...
			);
		end
		
	case 'B'
		
		for k = 1:3
			control(end + 1) = control_create( ...
				'name', ['check_', int2str(k)], ...
				'style', 'checkbox', ...
				'tab', tabs{1} ...
			);
		end
		
		for k = 1:4
			control(end + 1) = control_create( ... 
				'name', ['slider_', int2str(k)], ...
				'style', 'slider', ...
				'tab', tabs{2}, ...
				'string', {'A', 'B', 'C'}, ...
				'value', 1 ...
			);
		end
		
	case 'C'
		
		
end