function [call,value] = control_callback_tracer(obj)

% control_callback_tracer - utility function for callback development
% -------------------------------------------------------------------
%
% [call,value] = control_callback_tracer(obj)
%
% Input:
% ------
%  obj - callback object handle
%
% Output:
% -------
%  call - packed callback context
%  value - control value

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%---------------------------------
% GATHER INFORMATION
%---------------------------------

%--
% get callback context
%--

% TODO: change this function to handle 'eventdata' input

call = get_callback_context(obj);

%--
% get callback control value
%--

[ignore,value] = control_update([],call.pal.handle,call.control.name);

%---------------------------------
% DISPLAY INFORMATION
%---------------------------------

% TODO: improve display

str = '';

if (~isempty(call.par.handle))
	str = ['PAR: ', call.par.name, ', '];
end

str = [str, 'PAL: ', call.pal.name, ', CONTROL: ', call.control.name];

disp(' ');
disp(str);
disp(str_line(str))

xml_disp(value);