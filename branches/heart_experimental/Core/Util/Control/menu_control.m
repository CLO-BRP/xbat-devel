function handle = menu_control(par, name, value, varargin)

% menu_control - convenient menu creation in analogy to controls
% --------------------------------------------------------------
%
% handle = menu_control(par, name, value, varargin)
%
% Input:
% ------
%  par - parent handle
%  name - name and alias
%  value - binary
%  varargin - uimenu field value pairs
%
% Output:
% -------
%  handle - to menu

%--
% set default value
%--

% NOTE: the value is binary and controls the 'check' property of the menu

if nargin < 3
	value = 0;
end 

%--
% consider name input to get alias
%--

if ~ischar(name) && ~(iscellstr(name) && (length(name) == 2))
	error('Name is a string or a cell array of two strings.'); 
end

% NOTE: the 'title_caps' conversion of the explicit 'alias' is aggresive but typically convenient

if iscell(name)
	alias = title_caps(name{2}); name = name{1};
else
	alias = title_caps(name);
end

%--
% create menu using input
%--

handle = uimenu(par, ...
	'tag', name, 'label', alias, 'check', ternary(value, 'on', 'off'), varargin{:} ...
);

