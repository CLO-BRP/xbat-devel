function result = color_dialog_callback(obj, eventdata) %#ok<*INUSD>

% color_dialog_callback - intended to work with color_dialog_controls
% -------------------------------------------------------------------
%
% result = color_dialog_callback(obj, eventdata)
%
% Input:
% ------
%  obj, eventdata - conventional callback inputs
%
% Output:
% -------
%  result - of callback
%
% NOTE: the result is not well defined yet
%
% See also: color_dialog, color_dialog_controls

result = [];

callback = get_callback_context(obj, eventdata); pal = callback.pal;

switch callback.control.name
	
	case 'filter'
		
		handles = get_control_handles(pal.handle, 'name');
		
		names = get_color_names(callback.value); 
		
		if numel(names) < 2
		
			value = 1;
		
		else

			name = get_control_value(pal.handle, 'name');
			
			value = find(strcmp(names, name));
			
			if isempty(value)
				value = 2;
			end
			
		end
		
		set(handles.obj, 'string', names, 'value', value);
		
		set_control(pal.handle, 'name', 'label', ['Names (', int2str(numel(names) - 1), ')']);
		
		color_dialog_callback(handles.obj, eventdata); return;
		
	case 'name'
		
		color = get_color_by_name(cellfree(callback.value));
		
		if isempty(color)
			return;
		end 
		
		set_control(pal.handle, 'red', 'value', color.rgb(1));
		
		set_control(pal.handle, 'green', 'value', color.rgb(2));
		
		set_control(pal.handle, 'blue', 'value', color.rgb(3));
		
	case {'red', 'green', 'blue'}
		
		set_control(pal.handle, 'name', 'value', 1);
		

end

%--
% update color display
%--

values = get_control_values(pal.handle);

color = values_to_color(values);

colors = display_similar(pal.handle, color);

color_display(pal.handle, colors(1));

%--
% store color in color display axes control
%--

control_update([], pal.handle, 'color', color);
		

%----------------------------------
% VALUES_TO_COLOR
%----------------------------------

function color = values_to_color(values)

color(1) = values.red;

color(2) = values.green;

color(3) = values.blue;


%----------------------------------
% COLOR_DISPLAY
%----------------------------------

function color_display(pal, color)

% color_display - update color display in color dialog
% ----------------------------------------------------
%
% color_display(pal, color)
%
% Input:
% ------
%  pal - palette figure
%  color - color to display

%--
% get color display axes handle
%--

% NOTE: select axes object from axes control handles

handles = get_control(pal, 'color', 'handles'); ax = handles.obj;

%--
% get handle to or create image in color display axes
%--

obj = findobj(pal, 'tag', 'color_image');

if isempty(obj)
		
	obj = image( ...
		'tag', 'color_image', ...
		'parent', ax, ...
		'xdata', [0, 1], ...
		'ydata', [0, 1] ...
	);

	% NOTE: the layer top setting is critical for proper axes display
	
	set(ax, ...
		'layer', 'top', ...
		'xtick', [], ...
		'ytick', [], ...
		'xlim', [0, 1], ...
		'ylim', [0, 1] ...
	);

end

%--
% set image data according to color
%--

% NOTE: we create a one pixel color image, we need to use the third dimension

for k = 1:3
	value(:, :, k) = color.rgb(k);
end

set(obj, 'cdata', value);

set_control(pal, 'color', 'value', color);



%----------------------------------
% DISPLAY_SIMILAR
%----------------------------------

function colors = display_similar(pal, color)

%--
% get axes size and palette tilesize
%--

handles = get_control_handles(pal, 'similar'); ax = handles.obj;

pos = get_size_in(ax, 'pixels');

opt = get_palette_settings; tilesize = opt.tilesize;

%--
% determine precise size and number of color patches
%--

tiles = floor(pos(3:4) / (1.5 * tilesize)); 

space = (pos(3) - tiles(1) * tilesize) / (tilesize * (tiles(1) - 1));

%--
% get similar colors and set control value
%--

colors = similar_css_colors(color, prod(tiles));

set_control(pal, 'similar', 'value', colors); 

%--
% set parent axes properties
%--

base = get(ancestor(handles.obj, 'figure'), 'color');

set(ax, ...
	'xlim', [0, pos(3)], ...
	'ylim', [0, pos(4)], ...
	'ydir', 'reverse', ...
	'hittest', 'off', ...
	'xcolor', base, ...
	'ycolor', base, ...
	'color', 'none' ...
);

%--
% build color patches
%--

count = 1; handles = [];

for k = 1:tiles(1)
	
	for l = 1:tiles(2)
		
		pos = get_patch_position(k, l, space);
		
		xdata = tilesize * pos.xdata; ydata = tilesize * pos.ydata;
		
		handles(end + 1) = patch( ...
			'parent', ax, ...
			'clipping', 'off', ...
			'xdata', xdata, ... 
			'ydata', ydata, ... 
			'buttondownfcn', {@similar_callback, colors(count)}, ...
			'facecolor', colors(count).rgb ...
		); %#ok<AGROW>
	
		if count == 1
			
% 			set(handles(end), 'linewidth', 1, 'edgecolor', 0.25 * ([3 0 0] + base));
			
			% NOTE: the display below does not work well with the label
			
% 			offset = 3;
% 			
% 			xdata = xdata - offset; xdata(2:3) = xdata(2:3) + 2 * offset;
% 			
% 			ydata = ydata - offset; ydata(3:4) = ydata(3:4) + 2 * offset;
% 			
% 			handles(end + 1) = patch( ...
% 				'parent', ax, ...
% 				'clipping', 'off', ...
% 				'xdata', xdata, ... 
% 				'ydata', ydata, ... 
% 				'hittest', 'off', ...
% 				'edgecolor', 'red', ...
% 				'facecolor', 'none' ...
% 			);
			
		end 
		
		count = count + 1;
		
	end
	
end

% NOTE: this makes sure that the patches are visible along with parent axes

set(handles, 'visible', get(ax, 'visible'));


%----------------------------------
% SIMILAR_CALLBACK
%----------------------------------

function similar_callback(obj, eventdata, color) %#ok<INUSL>

% NOTE: this function releases the color name filter and sets the name control

pal = ancestor(obj, 'figure');

filter = get_control_value(pal, 'filter');

if ~isempty(filter)
	control = set_control(pal, 'filter', 'value', ''); color_dialog_callback(control.handles.obj, eventdata);
end

control = set_control(pal, 'name', 'value', color.name); color_dialog_callback(control.handles.obj, eventdata);


%----------------------------------
% GET_PATCH_POSITION
%----------------------------------

function pos = get_patch_position(k, l, space)

% NOTE: this function outputs patch positions in tile units

if nargin < 3
	space = 0.5;
end

offset = 1 + space;

pos.xdata = [0 1 1 0 0] + offset * (k - 1);

pos.ydata = [0 0 1 1 0] + offset * (l - 1);