function control = edit_control(name, str, varargin)

% edit_control - create shortcut
% ------------------------------
%
% control = edit_control(name, str, field, value, ... )
%
% Input:
% ------
%  name - and possibly alias
%  str - value
%  field - name
%  value - for conrtrol field
%
% Output:
% -------
%  control - struct

%--
% get alias from name
%--

if iscell(name)
	alias = name{2}; name = name{1};
else
	alias = name;
end

%--
% set default string
%--

if nargin < 2 || isempty(str)
	str = '';
end

%--
% create control
%--

control = control_create( ...
	'name', name, ...
	'alias', alias, ...
	'style', 'edit', ...
	'string', str, ...
	varargin{:} ...
);