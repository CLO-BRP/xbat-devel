function daemon = palette_flock_daemon

% palette_flock_daemon - create a timer to handle palette flocking
% ----------------------------------------------------------------
%
% daemon = palette_flock_daemon
%
% Output:
% -------
%  daemon - timer object
%
% THIS IS NOT READY FOR USERS !!!

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3168 $
% $Date: 2006-01-11 16:54:20 -0500 (Wed, 11 Jan 2006) $
%--------------------------------

%--
% create timer object
%--

daemon = timer;

%--
% configure timer object
%--

set(daemon, ...
	'name','XBAT Palette Flock Daemon', ...
	'timerfcn',@update_flock_state, ...
	'executionmode','fixedRate', ...
	'period',0.1 ...
);

%---------------------------------------------------
% UPDATE_PALETTE_DISPLAY
%---------------------------------------------------

function update_flock_state(obj,evendata)

% update_flock_state - timer callback to update palette display
% -------------------------------------------------------------

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3168 $
% $Date: 2006-01-11 16:54:20 -0500 (Wed, 11 Jan 2006) $
%--------------------------------

%--
% get flock daemon and update the userdata
%--

tmp = timerfind('name','XBAT Palette Flock Daemon');

if (~isempty(tmp))
	set(tmp,'userdata',0);
end