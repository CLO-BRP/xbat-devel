function fun = choose_fun(name, pat, match)

% choose_fun - choose a function when names collide
% -------------------------------------------------
%
% fun = choose_fun(name, pat, match)
%
% Input:
% ------
%  name - of function
%  pat - pattern to match in full filename
%  match - function (def: string_contains)
%
% Output:
% -------
%  fun - chosen function handle
%
% NOTE: this function can access private functions, do this at your own risk
%
% See also: which

%--
% handle input
%--

% NOTE: whatever the match function is it takes the input string, the pattern and returns an indicator

if nargin < 3
	match = @string_contains;
end

if nargin < 2
	pat = '';
end

%--
% get all available named functions, filter with pattern if needed
%--

fun = which(name, '-all');

% NOTE: this function does not work with methods

for k = numel(fun):-1:1
	if ~isempty(strfind(fun{k}, '@')), fun(k) = []; end
end

if ~isempty(pat)
	
	for k = numel(fun):-1:1
		if ~match(fun{k}, pat), fun(k) = []; end
	end 
	
end 

if isempty(fun)
	return;
end 

for k = 1:numel(fun)
	
	if string_begins(fun{k}, 'built-in (')
		fun{k} = fun{k}(11:end - 1);
	end
	
end

%--
% get function handles
%--

current = pwd;

try
	for k = 1:numel(fun)
		cd(fileparts(fun{k})); fun{k} = str2func(name);
	end
catch
	cd(current); rethrow(lasterror);
end

cd(current);

%--
% display if result not captured
%--

% NOTE: we display the result of functions on the function handle

if ~nargout
	info = iterate(@functions, fun); disp(info); clear fun;
else
	if numel(fun) == 1
		fun = fun{1};
	end
end

