function xml_disp(in, fun)

% xml_disp - display variable as xml
% ----------------------------------
%
% xml_disp(in, fun)
%
% Input:
% ------
%  in - variable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1880 $
% $Date: 2005-09-29 17:29:36 -0400 (Thu, 29 Sep 2005) $
%--------------------------------

% TODO: develop XSLT and web display

% TODO: allow passing of arguments to callback, use cell array convention

%----------------------------------------
% HANDLE INPUT
%----------------------------------------

%--
% set default display handler
%--

if ((nargin < 2) || isempty(fun))
	fun = @screen_dump;
end

%----------------------------------------
% SERIALIZE AND DISPLAY INPUT
%----------------------------------------

%--
% serialize variable
%--

% NOTE: we use 'inputname' to pass the variable name

str = to_xml(in, [], inputname(1));

%--
% display
%--

fun(str);


%-----------------------------------------------
% SCREEN_DUMP
%-----------------------------------------------

function screen_dump(str)

%--
% replace element and matrix tags with concise tags
%--

pat = {'cell', 'element', 'matrix', 'struct'}; rep = {'C', 'E', 'M', 'S'};

for k = 1:length(pat)
	str = strrep(str, pat{k}, rep{k});
end

%--
% replace root directories
%--

str = strrep(str, strrep(app_root, '\', '\\'), '$app_root');

str = strrep(str, strrep(matlabroot, '\', '\\'), '$MATLAB_ROOT');

%--
% display to screen
%--

disp(sprintf(str));



