function [result, tools] = texify(source, format, varargin)

% texify - a source file, output in same directory
% ------------------------------------------------
%
% [result, tools] = texify(source, format, varargin)
%
% tool = texify
%
% Input:
% ------
%  source - file
%  format - of output 'dvi', 'ps', or 'pdf' (def: 'pdf')
%  varargin - arguments to texify.exe
% 
% Output:
% -------
%  result - file
%  tools - used to build file
%  tool - texify
%
% See also: miktex_tools, miktex_root

% TODO: this pattern may be reusable to produce 'png', look at 'latex_image'

% TODO: consider using the 'pdflatex' approach described here: http://mintaka.sdsu.edu/GF/bibliog/latex/LaTeXtoPDF.html

%--
% handle input
%--

if nargin < 2 || isempty(format)
	format = 'pdf'; 
end

%--
% get and possibly output tools
%--

tools = miktex_tools('texify.exe'); 

if ~nargin
	result = tools; return;
end

%--
% inspect file for graphic types contained
%--

types = graphic_types(source);

%--
% create PDF from source file
%--

% NOTE: 'texify.exe' does not seem to accept output file input, so we go to the source directory

start = pwd; target = fileparts(source);

if ~isempty(target)
	cd(target); 
end

try
	% NOTE: the first step always uses 'texify' which ensures proper compilation of bibliography, indexes, etc.
	
	if strcmp(format, 'pdf') && ~string_is_member('.eps', types)
		
		% TODO: consider removing this build process, the alternative multi-step process can build everything
	
		% NOTE: this is how we build PDF when no EPS files are used
		
		[status, result] = use_tool(tools, ['"', source, '"'], '-V', '--pdf', '--batch', '--clean', varargin{:});
		
		if status, error(result); end
		
		if ~nargout
			result = file_ext(source, 'pdf'); winopen(result);
		end
	else
		% NOTE: this is how we build DVI, it may be used later to get PS or PDF
		
		[status, result] = use_tool(tools, ['"', source, '"'], '-V', '--batch', '--clean', varargin{:}); %#ok<ASGLU>
		
		if status, error(result); end
		
		if strcmp(format, 'dvi')
			if ~nargout
				result = file_ext(source, 'dvi'); winopen(result);
			end
			
			return;
		end
		
		% NOTE: this is how we build PS from DVI, it may be used later to get PDF
		
		tools(end + 1) = miktex_tools('dvips');
		
		[status, result] = use_tool(tools(end), ['"', file_ext(source, 'dvi'), '"']); %#ok<ASGLU>
				
		if status, error(result); end
		
		if strcmp(format, 'ps')
			if ~nargout
				result = file_ext(source, 'ps'); winopen(result);
			end
			
			return;
		end
		
		% NOTE: this is how build PDF from PS 
		
		tools(end + 1) = miktex_tools('ps2pdf.exe');
		
		[status, result] = use_tool(tools(end), ...
			['"', file_ext(source, 'ps'), '"'], ['"', file_ext(source, 'pdf'), '"'] ...
		);
		
		if status, error(result); end
		
		if ~nargout
			result = file_ext(source, 'pdf'); winopen(result);
		end
	end
	
	cd(start);
catch
	cd(start);
end


%--------------------------
% GRAPHIC_TYPES
%--------------------------

function types = graphic_types(file)

% graphic_types - contained in file
% ---------------------------------
%
% types = graphic_types(file)
%
% Input:
% ------
%  file - source
% 
% Output:
% -------
%  types - contained in file

% TODO: consider a function that simply asks whether graphics are included at all

opt = file_readlines; opt.pre = '%';

lines = file_readlines(file, [], opt); 

types = {}; known = strcat('.', {'eps', 'png', 'jpg'});

for k = 1:numel(lines) 
	
	for j = 1:numel(known)
		% NOTE: we check each line against any type not found yet, assume one type per line
		
		if string_contains(lines{k}, known{j})
			types{end + 1} = known{j}; known(j) = []; break; %#ok<AGROW>
		end
	end
	
	if isempty(known)
		return;
	end
end

