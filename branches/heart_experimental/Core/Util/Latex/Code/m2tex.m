function [result, status] = m2tex(in, out, varargin)

% m2tex - creates Latex code representation
% -----------------------------------------
%
% [result, status] = m2tex(in, out, vargargin)
%
% Input:
% ------
%  in, out - files
%
% Output:
% -------
%  result, status - of helper script

% TODO: create example file and build

% TODO: build a module documentation tool, use the SVN packages

%--
% setup and handle input
%--

persistent helper;

if isempty(helper)
	helper = which('texify.pl');

	if isempty(helper)
		error('Unable to find ''texify.pl'' helper script.');
	end
end

if nargin < 2 || isempty(out)
	out = [in, '.tex'];
end

%--
% call helper perl script
%--

% NOTE: when we use which we assume the file is a full path or in MATLAB path

[result, status] = perl(helper, 'matlab', '-i', which(in), '-o', out, varargin{:});