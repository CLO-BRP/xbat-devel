function fail = safer_eval(str)

% safer_eval - evaluate string with some concern
% ----------------------------------------------
%
% fail = safer_eval(str)
%
% Input:
% ------
%  str - string to evaluate
%
% Output:
% -------
%  fail - failure indicator
%
% NOTE: this is a crude experiment to see what may be possible

%--
% check string for 'unsafe' commands
%--

censored = {'quit', 'close', 'delete', 'stop', 'set', 'figure', 'uicontrol'};

% NOTE: nothing happens when the string contains censored commands

for k = 1:numel(censored)
	if strfind(str, censored{k}), fail = true; return; end
end

%--
% evaluate string
%--

try
	eval(str); fail = false;
catch %#ok<CTCH>
	fail = true; nice_catch;
end
