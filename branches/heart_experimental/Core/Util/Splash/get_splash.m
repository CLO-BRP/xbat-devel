function pal = get_splash

% get_splash - get splash figure handle
% -------------------------------------
%
% pal = get_splash
%
% Output:
% -------
%  pal - handle to splash

%--
% get open waitbars
%--

% NOTE: this line is taken from 'get_xbat_figs' on which we should not depend

% TODO: implement a more complete APP environment variable and use here

pals = match_tag_prefix(get(0, 'children'), ['XBAT_WAITBAR'], true);

% pals = get_xbat_figs('type', 'waitbar'); 

pal = [];

if isempty(pals)
	return;
end

%--
% find one named as splash
%--

% NOTE: the tag should be more structured

info = parse_tag(get(pals, 'tag'), '::', {'type', 'name'});

for k = 1:length(info)
	
	if strcmpi(info(k).name, 'splash')
		pal = pals(k); return;
	end
	
end
