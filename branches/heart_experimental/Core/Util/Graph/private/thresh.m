function Y = thresh(X, str)

% thresh - threshold interval of values
% -------------------------------------
% 
% Y = thresh(X, str)
%
% Input:
% ------
%  X - input matrix
%  str - interval string
%
% Output:
% -------
%  Y - indicator matrix
%

%--
% parse interval string
%--

[edge, type] = parse_interval(str); start = edge(1); stop = edge(2);

%--
% compute threshold matrix
%--

switch type

	case 0
		Y = (X > start) & (X < stop);
		
	case 1
		Y = (X > start) & (X <= stop);
		
	case 2
		Y = (X >= start) & (X < stop);
		
	case 3
		Y = (X >= start) & (X <= stop);
		
end

