function [b,t] = parse_interval(s)

% parse_interval - parse string with interval information
% -------------------------------------------------------
%
% [b,t] = parse_interval(s)
%
% Input:
% ------
%  s - interval string
%
% Output:
% -------
%  b - interval endpoints
%  t - type of interval
%    0 - open (,)
%    1 - closed above (,]
%    2 - closed below [,)
%    3 - closed [,]
%

%--
% parse string
%--

[a,b] = strtok(s,', ');

l = (a(1) == '[');
a = str2num(a(2:end));

b = b(2:end);
u = (b(end) == ']');
b = str2num(b(1:end - 1));

%--
% get endpoints and type
%--

b = [a, b];
t = 2*l + 1*u;
