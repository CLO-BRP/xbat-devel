function [Dx, Dy] = fast_nearest(X, Y, type)

% fast_nearest - distance computation
% -----------------------------------
%
% [Dx, Dy] = fast_nearest(X, Y, type)
%
% Input:
% ------
%  X, Y - column vector collections
%  type - of distance (def: 'euclidean')
%
% Output:
% -------
%  Dx, Dy - distance to nearest neighbor for X and Y, along with indices into Y and X respectively
%
% See also: fast_distance

%--
% handle input
%--

if nargin < 3
	type = 'euclidean';
end

if nargin < 2
	Y = X;
end

type = get_code_from_name(type);

%--
% compute using MEX
%--

% TODO: implement the 'k' parameter for k-smallest or k-largest or both

D = fast_distance_mex(X, Y, type, 1);

% TODO: reconsider MEX packing, this might not generalize well

Dx = D(1:size(X, 2), :); Dy = D(size(X, 2) + 1:end, :);