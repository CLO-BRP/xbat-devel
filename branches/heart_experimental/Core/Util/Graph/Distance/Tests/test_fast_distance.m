function test_fast_distance(type)

%--
% handle input
%--

if ~nargin
	type = 'euclidean';
	
	% TODO: implement remaining cases
	
	% NOTE: the current 'dist_hamming' functions is designed for integer matrices
	
% 	type = {'hamming', 'absolution', 'euclidean'};
end

if ischar(type)
	type = {type};
end

%--
% run tests
%--

for j = 1:numel(type)
	
	disp(' ');
	disp(upper(type{j}));
	disp(' ');
	
	for k = 2:5:220
		
		X = randn(k, 600); Y = randn(k, 400); failure = false;
		
		% MATLAB based computation
		
		try
			start = clock;
			
			D1 = dist_euclidean(X, Y);
			
			elapsed1 = etime(clock, start);
		catch
			failure = true; elapsed1 = inf; % nice_catch;
		end
		
		% MEX based computation
		
		try
			start = clock;
			
			D2 = fast_distance(X, Y);
			
			elapsed2 = etime(clock, start);
		catch
			failure = true; elapsed2 = inf; % nice_catch;
		end
		
		% report
		
		passed = ~failure && (max(vec(abs(D1 - D2))) < eps);
		
		disp(['d = ', int2str(k), ', passed = ', int2str(passed), ', elapsed = [', sec_to_clock(elapsed1), ', ', sec_to_clock(elapsed2) '], speed = ', num2str(elapsed1/elapsed2)]);
	end
end

disp(' ');

