function D = dist_diff(X, Y, fun)

% dist_diff - differences used in distance computations, possibly mapped
% ----------------------------------------------------------------------
%
% D = dist_diff(X, Y, fun)
%
% Input:
% ------
%  X - column vectors, row index
%  Y - column vectors, column index
%  fun - distance defining difference map (def: none)
%
% Output:
% -------
%  D - 3-dimensional array containing differences used in distances
%
% NOTE: using this as a basis for distance computation only works for small < 2000 collections
%
% See also: dist_euclidean, dist_euclidean2

%--
% set default map to empty
%--

if nargin < 3
	fun = [];
end

%--
% set default Y and check dimensionality
%--

if nargin < 2 || isempty(Y)
	Y = X;
end

if size(X, 1) ~= size(Y, 1), error('Vector collections have different dimensionality.'); end

%--
% pack vectors into multidimensional arrays to compute differences
%--

% NOTE: the output difference matrix is m x n x d, where d is the number of rows in X and Y

m = size(X, 2); n = size(Y, 2);

X = permute(repmat(X, [1, 1, n]), [2, 3, 1]);

Y = permute(repmat(Y, [1, 1, m]), [3, 2, 1]);

%--
% compute differences
%--

D = X - Y;

%--
% apply distance defining difference map
%--

% NOTE: we will not be collapsing here

if isempty(fun)
	return;
end 

% NOTE: we allow callback type parametrized maps

if iscell(fun)
	D = fun{1}(D, fun{2:end});
else
	D = fun(D);
end


