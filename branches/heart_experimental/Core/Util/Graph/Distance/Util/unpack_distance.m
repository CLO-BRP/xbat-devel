function D = unpack_distance(d, tol)

% unpack_distance - index, value points representation into matrix
% ----------------------------------------------------------------
%
% D = unpack_distance(d, tol)
% 
% Input:
% ------
%  d - distance index, value points
%
% Output:
% -------
%  D - distance matrix
%
% See also: pack_distance
%
% NOTE: this is mostly a call to 'sparse', full if density merits

if nargin < 2
	tol = 0.1;
end

D = sparse(d(:, 1), d(:, 2), d(:, 3));

if size(d, 1) / numel(D) > tol
	D = full(D);
end