function D = dist_plane(x, y)

% dist_plane - distance matrix for plane points 
% ---------------------------------------------
%
% D = dist_plane(x, y)
%
% Input:
% ------
%  x - plane positions, row index
%  y - plane positions, column index
%
% Output:
% ------
%  D - distance matrix

%--
% handle input
%--

if nargin < 2
	y = x;
end

%--
% represent point row vectors as complex numbers
%--

if isreal(x)
	x = x(:, 1) + 1i*x(:, 2);
end

if isreal(y)
	y = y(:, 1) + 1i*y(:, 2);
end

%--
% compute distance matrix
%--

% TODO: figure out why the 'colon' vectorization does not work in this case

% NOTE: we use non-hermitian transpose to produce proper alignment

D = abs(repmat(x.', [numel(y), 1]) - repmat(y, [1, numel(x)]));
