function [G, X] = file_ext(F, x)

% file_ext - set or strip file extensions
% ---------------------------------------
%
% [G, X] = file_ext(F, x)
%
% Input:
% ------
%  F - input strings
%  x - extension string (def: '' (remove extensions))
%
% Output:
% -------
%  G - strings with extensions
%  X - input extensions when extensions are stripped

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 698 $
% $Date: 2005-03-11 16:20:26 -0500 (Fri, 11 Mar 2005) $
%--------------------------------

%--
% set default extension (strip extensions)
%--

if nargin < 2
	x = '';
end

%--
% put string into cell
%--

if ischar(F)
	F = {F}; flag = 1;
else
	flag = 0;
end

%--
% strip extension
%--

if isempty(x)
	
	G = cell(size(F)); X = cell(size(F));
	
	for k = 1:length(F)
			
		ix = find(F{k} == '.', 1, 'last');

		if isempty(ix)
			G{k} = F{k}; X{k} = '';
		else
			G{k} = F{k}(1:(ix(end) - 1)); X{k} = F{k}((ix(end) + 1):end);
		end
		
	end
	
%--
% set extensions
%--

else

	%--
	% add extension
	%--

	G = cell(size(F));
		
	for k = 1:length(F)
		
		ix = find(F{k} == '.', 1, 'last');
		
		if isempty(ix)
			s = F{k}; X{k} = '';
		else
            s = F{k}(1:(ix(end) - 1)); X{k} = F{k}((ix(end) + 1):end);
		end
		
		G{k} = [s, '.', x];	
		
	end
	
end

%--
% output string
%--

% NOTE: modification of output type is not a good idea although convenient

if flag
	G = G{1}; X = X{1};
end

