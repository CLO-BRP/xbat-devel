function [lines, nos, comment] = file_readlines(in, fun, opt)

% file_readlines - load and possibly process lines of file
% --------------------------------------------------------
%
% [lines, nos, comment] = file_readlines(in, fun, opt)
%
% opt = file_readlines
%
% Input:
% ------
%  in - input file or file identifier
%  fun - fun to process each line
%  opt - options
%
% Output:
% -------
%  lines - in file
%  nos - line numbers
%  comment - lines and nos in struct
%  opt - options
%
% NOTE: process 'fun' is off when counting lines, unless we ask with 'force_fun'
%
% NOTE: 'no_end' only applies when counting lines

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% set default options and output options if needed
%--

if (nargin < 3) || isempty(opt)
	
	opt = file_process;
	
	opt.force_fun = false;
	
	% NOTE: only comes into play when 
	opt.no_end = false;
	
	if ~nargin
		lines = opt; return;
	end	
end

%--
% set no fun default
%--

if nargin < 2
	fun = [];
end

%--------------------------------------
% READ FILE LINES
%--------------------------------------

% NOTE: setting empty output indicates read to buffer behavior

if nargout < 2
	
	lines = file_process([], in, fun, opt);
else
	% NOTE: we make sure that we read full file first, then remove comments and space as needed
	
	opt2 = opt; opt2.pre = ''; opt2.skip = false;
	
	% NOTE: callbacks are not turned off when counting lines unless we ask
	
	% NOTE: we use this when adding files to 'ftl' database
	
	if ~opt.force_fun
		fun = []; 
	end
	
	lines = file_process([], in, fun, opt2); nos = 1:numel(lines);
	
	% NOTE: we remote empty lines and comments after read
			
	if ~isempty(opt.pre)
		remove = string_begins(strtrim(lines), opt.pre);
		
		% NOTE: if we are asked to report comments pack these before removing them from the lines output
		
		if nargout > 2
			comment.lines = lines(remove); comment.nos = nos(remove);
		end
		
		lines(remove) = []; nos(remove) = [];
	end
	
	if opt.skip
		remove = cellfun('isempty', strtrim(lines));
		
		lines(remove) = []; nos(remove) = [];
	end
	
	% NOTE: this can and should be implemented as a callback that erases end, then 'skip' can get rid of them
	
	if opt.no_end
		
		for k = numel(lines):-1:1
			
			if strcmp(strtrim(lines{k}), 'end')
				lines(k) = []; nos(k) = [];
			end
		end
	end
	
end
