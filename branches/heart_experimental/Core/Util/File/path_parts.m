 function [par, leaf] = path_parts(str)

% path_parts - separate directory path into parent and leaf
% ---------------------------------------------------------
%
% [par, leaf] = path_parts(str)
%
% Input:
% ------
%  str - directory path string
%
% Output:
% -------
%  par - parent part 
%  leaf - leaf part 

% NOTE: we trim a final file separator if found, 'pwd' output does not have final filesep

if str(end) == filesep
	str(end) = [];
end

[par, leaf] = fileparts(str);
