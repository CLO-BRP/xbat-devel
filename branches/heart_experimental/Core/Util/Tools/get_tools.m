function tools = get_tools(source, pat, types, scan)

% get_tools - get tools from directory
% ------------------------------------
%
% tools = get_tools(source, pat, types, scan)
%
% Input:
% ------
%  source - directory
%  pat - name pattern
%  types - tool types to consider
%  scan - scan for help indicator or hint
%
% Output:
% -------
%  tools - tools found

%--
% handle input
%--

if ~exist_dir(source)
	tools = []; return;
end

if nargin < 4
	scan = 1;
end

if nargin < 3
	types = get_tool_types;
end

if nargin < 2
	pat = '';
end

%--
% get tools
%--

tools = [];

content = get_extension_content(source, types, pat);

for k = 1:length(content)
	
	%--
	% check for pattern match
	%--
	
	tool = get_tool(fullfile(source, content(k).name), scan);
	
	if ~isempty(tool)
		
		if isempty(tools)
			tools = tool;
		else 
			tools(end + 1) = tool;
		end
		
	end
	
end
