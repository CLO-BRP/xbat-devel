function [status, result] = generic_tool(name, file, url, helper, varargin)

% generic_tool - get and use a tool generically, install if possible
% ------------------------------------------------------------------
%
% [status, result] = generic_tool(name, file, url, helper, arg1, ... ,argn)
%
% Input:
% ------
%  file - tool file
%  name - tool name
%  url - tool install url
%  helper - tool intall helper
%  arg - argument
%   
% Output:
% -------
%  status - status
%  result - result

% TODO: pack tool description in a struct for future expansion of framework

% NOTE: the current arguements are fields, also add a 'background' indicator field

%--
% initialize trivial output
%--

% NOTE: this is a failure status, in this case would indicate failure to try

status = 1; result = '';

%--
% get tool, install if needed
%--

% TODO: consider whether this pattern can be encapsulated by 'install_tool'

tool = get_tool(file); 

if isempty(tool)
	
	% NOTE: we return empty if we don't know how or fail to install
	
	if (nargin < 3) || isempty(url)
		return;
	end
	
	% NOTE: in windows the name is used for the parent directory in 'Tools'
	
	% TODO: in *nix systems this should be an adequate package name
	
	if ~install_tool(name, url, helper);
		return;
	end
	
	tool = get_tool(file);
	
end

% NOTE: possibly return tool

if isempty(varargin)
	status = tool; return;
end

%--
% use tool passing arguments in a generic way
%--
	
str = ['"', tool.file, '" ', str_implode(varargin)];

disp(str);

switch nargout
	
	case 0, system(str);

	case 1, status = system(str);
		
	case 2, [status, result] = system(str);
		
end