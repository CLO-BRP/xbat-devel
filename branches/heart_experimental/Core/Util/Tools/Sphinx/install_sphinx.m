function success = install_sphinx

% install_sphinx - text search engine
% -----------------------------------
%
% success = install_sphinx
% 
% Output:
% -------
%  success - indicator
% 
% See also: install_sphinx_service

success = install_tool('Sphinx', 'http://sphinxsearch.com/downloads/sphinx-0.9.8.1-win32-pgsql.zip');

if ~success
	uninstall_tool('Sphinx');
end