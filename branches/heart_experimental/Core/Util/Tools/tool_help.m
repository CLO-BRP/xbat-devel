function tool = tool_help(tool)

% TODO: a 'tool' should really be an object

% TODO: display linked tool file and location information

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% get tool from file name if needed
%--

if ischar(tool)

	tool = get_tool(tool);
	
	% NOTE: return if tool is unavailable, consider display
	
	if isempty(tool)
		return;
	end
	
end

%-------------------------
% DISPLAY HELP
%-------------------------

clc;

%--
% display header
%--

disp(' ');
str_line(64,'_');
disp(' ');
disp([' ', upper(tool.name)]);
str_line(64,'_');
disp('  ');

%--
% display help
%--

if isempty(tool.help)
	
	disp([' No help files available for ''', tool.name, '''.']);
	
else
	
	for k = 1:length(tool.help)
	
		[ignore, ignore, ext] = fileparts(tool.help{k});
		
		if ~isempty(ext)
			disp([' <a href="matlab:winopen(''', tool.help{k}, ''');">', tool.help{k}, '</a>']);
		else
			disp([' <a href="matlab:scite(''', tool.help{k}, ''');">', tool.help{k}, '</a>']);
		end
	end
	
end

disp('  ');
str_line(64,'_');
disp('  ');