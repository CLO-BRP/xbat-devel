function tool = nircmd

% nircmd - get nircmd tool
% ------------------------
%
% tool = nircmd
%
% Output:
% -------
%  tool - tool file and root

% TODO: reveal commands

if ~ispc
    tool = []; return;
end

%--
% get tool
%---

tool = get_tool('nircmdc.exe');

% NOTE: we install tool to get tool if needed

if isempty(tool) && install_tool('NirCmd', 'http://www.nirsoft.net/utils/nircmd.zip');
	
	tool = get_tool('nircmdc.exe');
	
end