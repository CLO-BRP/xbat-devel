function result = sha1(in)

% sha1 - compute SHA-1 hash for matlab variable
% ---------------------------------------------
%
% result = sha1(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  result - SHA-1 hash string
%
% See also: hash, md5, sha512, to_str

if ~ischar(in)
	in = to_str(in);
end

result = hash(in, 'sha-1');