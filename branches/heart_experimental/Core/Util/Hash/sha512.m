function result = sha512(in)

% sha512 - compute SHA-512 hash for matlab variable
% -------------------------------------------------
%
% result = sha512(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  result - SHA-512 hash string
%
% See also: hash, md5, sha1, to_str

if ~ischar(in)
	in = to_str(in);
end

result = hash(in, 'sha-512');