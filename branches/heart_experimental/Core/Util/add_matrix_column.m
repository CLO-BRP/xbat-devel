function A = add_matrix_column(A, k, c)

% add_matrix_column - add column to matrix after given column position
% --------------------------------------------------------------------
%
% A = add_matrix_column(A, k, c)
%
% Input:
% ------
%  A - matrix to update
%  k - add position
%  c - column to add
%
% Output:
% -------
%  A - updated matrix

%--
% handle input
%--

[m, n] = size(A);

% NOTE: by default we add zeros

if nargin < 3
	c = zeros(m, 1);
else
	if numel(c) ~= m
		error('Column to add must have proper number of elements.');
	end
end

% NOTE: by default we append a column to the matrix

if nargin < 2
	k = m;
else
	if (k ~= round(k)) || (k < 0) || (k > n)
		error('Column add position must be an integer between 0 and the starting number of columns.');
	end
end

%--
% update matrix
%--

A = [A(:, 1:k), c, A(:, k + 1:end)];