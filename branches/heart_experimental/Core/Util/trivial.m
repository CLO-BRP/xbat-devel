function value = trivial(in)

% trivial - check for trivial variable content
% --------------------------------------------
%
% value = trivial(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  value - indicator

% NOTE: a variable has trivial contents if it is empty or is a struct with no fields

% TODO: consider other outputs, and perhaps the case of an empty struct with fields

value = isempty(in) || (isstruct(in) && isempty(fieldnames(in)));