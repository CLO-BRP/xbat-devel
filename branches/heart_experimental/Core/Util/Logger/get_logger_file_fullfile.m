function [ log_fullfile ] = get_logger_file_fullfile( logname )

log_fullfile = fullfile(fileparts(mfilename('fullpath')), 'Logs', [logname, '.log']);
