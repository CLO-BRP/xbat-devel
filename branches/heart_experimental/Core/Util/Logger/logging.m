function value = logging(value)

% logging - control logging behavior
% -------------------------------------
%
% logging
%
% logging(value)
%
% logging = logging
%
% Input:
% ------
%  value - logging level to set {'fatal', 'error', 'warn', 'info', 'debug', 'all'}
%
% Output:
% -------
%  value - current trace level
%
% NOTE: call without arguments to get a graphical interface

% TODO: add option to consider SQL when determining whether to trace

% NOTE: consider 'listen' and 'ignore' options, these will be best set using the GUI

%--
% get value
%--

if ~nargin

	value = get_env('logging');

	if isempty(value)
		value = 0; set_env('logging', value);
	end

	if nargout
		return;
	end
	
%--
% set value
%--

else

    if iscell(value)
        set_env('logging', value);
        return
    end
    
    switch value

        case {0, 1}
            set_env('logging', value);

        case {'0', '1'}
            set_env('logging', eval(value));

        otherwise
            set_env('logging', value);
    end
   
end

