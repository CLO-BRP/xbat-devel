function root = real_toolbox_root(name)

% real_toolbox_root - closest path to toolbox root
% ------------------------------------------------
%
% root = real_toolbox_root(name)
%
% Input:
% ------
%  name - local toolbox name
%
% Output:
% -------
%  root - toolbox intended root

root = toolbox_root(name, 1, 0);