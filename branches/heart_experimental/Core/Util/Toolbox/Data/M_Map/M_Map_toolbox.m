function toolbox = M_Map_toolbox

%------------------------
% DATA
%------------------------

toolbox.name = 'M_Map';

toolbox.home = 'http://www.eos.ubc.ca/~rich/map.html';

toolbox.url = 'http://www.eos.ubc.ca/%7Erich/m_map1.4.zip';

toolbox.install = @install;

%------------------------
% INSTALL
%------------------------

function install

