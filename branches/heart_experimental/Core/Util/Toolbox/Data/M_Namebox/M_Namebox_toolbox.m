function toolbox = M_Namebox_toolbox

%------------------------
% DATA
%------------------------

toolbox.name = 'M_Namebox';

toolbox.home = 'http://www.nersc.no/~even/matlab/m_namebox/m_namebox.html';

toolbox.url = 'http://www.nersc.no/~even/matlab/m_namebox.zip';

toolbox.install = @install;

%------------------------
% INSTALL
%------------------------

function install

