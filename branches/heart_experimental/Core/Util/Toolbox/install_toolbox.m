function value = install_toolbox(name, url)

% install_toolbox - download toolbox and add to path and patch
% ------------------------------------------------------------
%
% value = install_toolbox(name, url)
%
% Input:
% ------
%  name - tool name
%  url - download url
%
% Output:
% -------
%  value - success indicator

%--------------------
% HANDLE INPUT
%--------------------

%--
% get data if needed
%--

if nargin < 2
	toolbox = get_toolbox_data(name); url = toolbox.url;
end

%--------------------
% INSTALL TOOLBOX
%--------------------

%--
% download, unzip if needed, and add to path
%--

try
	
	%--
	% get destination directory
	%--

	root = create_dir(toolbox_root(name));

	if isempty(root)
		error(['Failed to create toolbox root directory for ''', name, '''.']);
	end

	%--
	% download tool
	%--
	
	disp(' '); disp(['Downloading and installing ''', name, ''', this may take some time ...']); disp(' ');
	
	start = clock;
		
	if ischar(url)
		url = {url};
	end 
	
	for k = 1:numel(url)
		
		%--
		% get download file type
		%--
		
		type = '';
		
		if strcmpi(url{k}(end - 3:end), '.zip')
			type = 'zip';
		end
		
		if isempty(type) && strcmpi(url{k}(end - 6:end), '.tar.gz')
			type = 'tar';
		end
		
		if isempty(type)
			type = 'file';
		end
		
		%--
		% set temp or download file
		%--
		
		switch type
			
			case 'zip'
				file = fullfile(root, 'temp.zip');

			case 'tar'
				file = fullfile(root, 'temp.tar.gz');
				
			otherwise
				ix = find(url{k} == '/', 1, 'last'); file = fullfile(root, url{k}(ix + 1:end));
				
		end
		
		% NOTE: this provides download progress indicators, otherwise we should just pass the url to 'unzip' or 'untar'		
		
		% TODO: fix curl tools!

% 		curl_get(url{k}, file, ['Downloading ''', name, '''']);
% 
% 		while curl_wait(file)
% 			pause(0.25);
% 		end

		urlwrite(url{k}, file);
		
		switch type
			
			case 'zip'
				unzip(file, root); delete(file);
	
			case 'tar'
				untar(file, root); delete(file);
				
		end
		
	end
	
	%--
	% add to path
	%--
	
	% TODO: consider private directories and factor, also used in 'install_tool'
	
	append_path(root);
	 
	elapsed = etime(clock, start);
	
	% TODO: this must be moved after the install script runs
	
	disp(['Done. (', num2str(elapsed), ' sec)']);
	disp(' ');
	
	value = 1;
	
catch
	
	value = 0; nice_catch(lasterror, ['Failed to get ''', name, '''.']);
	
end

%--
% execute install script if available
%--

if isfield(toolbox, 'install') && ~isempty(toolbox.install)
	toolbox.install();
end


