function form = get_dateform(code)

% get_dateform - determine date format string from date format code
% -----------------------------------------------------------------
%
% form = get_dateform(code)
%
% Input:
% ------
%  code - date format code
% 
% Output:
% -------
%  form - date format string
%
% NOTE: the format index relation is taken from the MATLAB 'datestr' function

switch code
	
	case -1, form = 'dd-mmm-yyyy HH:MM:SS';
	case 0,  form = 'dd-mmm-yyyy HH:MM:SS';
	case 1,  form = 'dd-mmm-yyyy';
	case 2,  form = 'mm/dd/yy';
	case 3,  form = 'mmm';
	case 4,  form = 'm';
	case 5,  form = 'mm';
	case 6,  form = 'mm/dd';
	case 7,  form = 'dd';
	case 8,  form = 'ddd';
	case 9,  form = 'd';
	case 10, form = 'yyyy';
	case 11, form = 'yy';
	case 12, form = 'mmmyy';
	case 13, form = 'HH:MM:SS';
	case 14, form = 'HH:MM:SS PM';
	case 15, form = 'HH:MM';
	case 16, form = 'HH:MM PM';
	case 17, form = 'QQ-YY';
	case 18, form = 'QQ';
	case 19, form = 'dd/mm';
	case 20, form = 'dd/mm/yy';
	case 21, form = 'mmm.dd,yyyy HH:MM:SS';
	case 22, form = 'mmm.dd,yyyy';
	case 23, form = 'mm/dd/yyyy';
	case 24, form = 'dd/mm/yyyy';
	case 25, form = 'yy/mm/dd';
	case 26, form = 'yyyy/mm/dd';
	case 27, form = 'QQ-YYYY';
	case 28, form = 'mmmyyyy';
	case 29, form = 'yyyy-mm-dd';
	case 30, form = 'yyyymmddTHHMMSS';
	case 31, form = 'yyyy-mm-dd HH:MM:SS';
	otherwise
		error('MATLAB:datestr:DateNumber', 'Unknown date format number: %s', code);
		
end