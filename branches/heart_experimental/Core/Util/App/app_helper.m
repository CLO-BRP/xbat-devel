function helper = app_helper(varargin)

% app_helper - registry set and get
% ---------------------------------
%
% helper = app_helper(name, helper, ... )
%
%        = app_helper(name)
%
%        = app_helper
%
% Input:
% ------
%  name - of helper
%  helper - function
%
% Output:
% -------
%  helper - function
%
% See also: app_root

% TODO: this does not need to rely on the 'appdata' functions, we could simply persist here

%--
% handle input considering number of arguments
%--

switch numel(varargin)
	
	%--
	% get all helpers operation
	%--
	
	case 0
		data = flatten(getappdata(0));
		
		if isfield(data, 'app_helper')
			helper = data.app_helper;
		else 
			helper = struct;
		end
	
	%--
	% get single helper
	%--
	
	case 1
		key = genvarname(['app_helper__', name]);
		
		helper = getappdata(0, key); 
	
	%--
	% set multiple helpers
	%--
	
	otherwise
		[name, helper] = get_field_value(varargin);

		for k = 1:numel(name) 
			key = genvarname(['app_helper__', name{k}]);
			
			setappdata(key, helper{k});
		end

end

