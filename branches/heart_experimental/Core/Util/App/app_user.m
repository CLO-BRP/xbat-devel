function user = app_user(varargin)

% app_user - helper registration and 'set' and 'get'
% --------------------------------------------------
%
% user = app_user(user)
%
% Input:
% ------
%  user - to set
%
% Output:
% -------
%  user - current

% NOTE: this is an experiment to abstract the App user concept

% NOTE: the registration is probably further abstractable

persistent HELPER;

if isempty(HELPER)
	HELPER = struct;
end

switch varargin
	case 0
		if ~isfield(HELPER, 'get')
			error('No App user ''get'' function is registered.');
		end
		
		user = HELPER.get();
		
	case 1
		if ~isfield(HELPER, 'set')
			error('No App user ''set'' function is registered.');
		end
		
		% NOTE: 'set' implements a 'set and get' type contract where it returns the set user
		
		user = HELPER.set(varargin{1});
		
	case 2
		switch varargin{1}
			case {'set', 'get', 'check'}
				HELPER.(varargin{2}) = varargin{2};
			
			otherwise
				error(['Unrecognized App user method ''', varargin{1}, '''.']);
		end	
end