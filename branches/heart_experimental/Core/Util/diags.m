function [D, ix] = diags(A, ix)

% diags - get multiple diagonals from a matrix as matrix
% ------------------------------------------------------
%
% [D, ix] = diags(A, ix)
%
% Input:
% ------
%  A - matrix
%  ix - diagonal indices
%
% Output:
% -------
%  D - diagonals as columns matrix
%  ix - diagonal indices

%--
% set default indices
%--

% NOTE: an all indices default is convenient, since it is hard to express when we want it

if (nargin < 2)
	ix = -(size(A, 1) - 1):(size(A, 2) - 1);
end

%--
% get diagonals
%--

n = length(diag(A)); D = zeros(n, length(ix)); 

for k = 1:length(ix)
	
	d = diag(A, ix(k));
	
	if ix(k) < 0
		D(n - length(d) + 1:end, k) = d;
	else
		D(1:length(d), k) = d;
	end
	
end
