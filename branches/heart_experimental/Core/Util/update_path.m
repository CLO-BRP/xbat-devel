function dirs = update_path(type, root, rec)

% update_path - update path with tree or path
% -------------------------------------------
%
% dirs = update_path('append', root, rec)
%
%      = update_path('remove', root, rec)
%
% Input:
% ------
%  root - tree root (def: pwd)
%  rec - follow tree (def: 1)
%
% Output:
% -------
%  dirs - directories appended or removed from path
%
% See also: append_path, remove_path

%--
% handle input
%--

% NOTE: default recursive append

if nargin < 3
	rec = 1;
end

% NOTE: default current directory starting point

if (nargin < 2) || isempty(root) 
	root = pwd;
end

% NOTE: check for proper type

if ~ismember(type, {'append', 'remove'})
	
	error('Path update type must be ''append'' or ''remove''.');
end

%--
% collect directories to append or remove
%--

if rec
	dirs = scan_dir(root);
else
	dirs = {root};
end

%--
% update path based on type
%--

switch type
	
	case 'append'
		% NOTE: we filter paths that are 'private'
		
		for k = length(dirs):-1:1
			
			[ignore, leaf] = fileparts(dirs{k}); %#ok<ASGLU>
			
			if strcmp(leaf, 'private')
				dirs(k) = []; continue;
			end
			
			if strfind(dirs{k}, [filesep, 'private', filesep])
				dirs(k) = [];
			end
		end
		
		if ~isempty(dirs)
			addpath(dirs{:}, '-end');
		end
		
	case 'remove'
		% NOTE: we filter directories not in path
		
		for k = length(dirs):-1:1
			
			if ~in_path(dirs{k})
				dirs(k) = [];
			end
		end
		
		if ~isempty(dirs)
			rmpath(dirs{:});
		end
end
