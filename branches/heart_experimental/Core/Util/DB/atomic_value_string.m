function part = atomic_value_string(value, field) %#ok<INUSL>

% atomic_value_string - get string to represent value
% ---------------------------------------------------
%
% part = atomic_value_string(value, field)
%
% Input:
% ------
%  value - value
%  field - source of value
%
% Output:
% -------
%  part - string representation

% TODO: use the field value input to determine conversion behavior, for example dates

%--
% handle special empty numeric case
%--

% NOTE: all other empty values are stored as XML and we can recover their class

if isempty(value) && isnumeric(value)
	part = 'NULL'; return;
end

%--
% handle special case of simple strings and numeric scalars
%--

% NOTE: we don't need all those XML tags for simple strings or scalars that we may store natively

% TODO: consider making this a function

if ischar(value)
	
	% NOTE: with this new approach we must escape quotes
	
	part = ['''', strrep(value, '''', ''''''), ''''];
	
	% NOTE: no escape fields are simple, and should contain no offending symbols
	
% 	if (nargin < 2) || no_escape_field(field)
% 		part = ['''', value, ''''];
% 	else
% 		part = ['''', spcharin(value), '''']; 
% 	end
	
	return;
	
end

if islogical(value)
	value = double(value);
end

if isnumeric(value) && isscalar(value) && isreal(value)
	
	part = num_to_str(value);
	
	% NOTE: using NULL to represent NaN, the SQLite convention, means we cannot distinguish it from the empty numeric array
	
	if strcmp(part, 'NaN')
		part = 'NULL';
	end
	
	return;
	
end

%--
% handle complex value cases
%--

part = ['''', m2xml(value), ''''];

