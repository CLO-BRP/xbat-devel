function [result, status] = update_relation_count(store, parent, child, helpers)

% update_relation_count - in database
% -----------------------------------
%
% update_relation_count(store, parent, child, helpers)
%
% Input:
% ------
%  store - database
%  parent - table name, these have many children
%  child - table name, these belong to parent
%  helpers - and options used to determine relevant column names
%
% Output:
% -------
%  helpers - default, uses Rails conventions

% TODO: consider allowing a helper for the basic 'id' convention

%--
% handle input
%--

% TODO: consider extending to general options, so we can add say 'allowed' types for 'parent' and 'child'

if nargin < 4
	helpers.parent_key = @(child)([child, '_id']);
	
	helpers.count_column = @(child)([pluralize(child), '_count']);
	
	helpers.fast = false;
	
	if ~nargin
		result = helpers; return;
	end
end

% NOTE: below we try to get child and parent objects from input, and therefore their identifiers

% NOTE: this 'type from input name' pattern is dangerous, if we had objects we would get type as the class

% TODO: it is not clear how the child identifiers can be used in the general query

if isstruct(child)
	
	child_obj = child; child = inputname(3);
	
	if isempty(child) && isfield(child_obj, 'type')
		child = child_obj.type;
	end
	
	if isempty(child)
		error('Unable to determine child type from input.');
	end
end

% NOTE: it is clear how to use the parent identifiers to only update some

if isstruct(parent)
	
	parent_obj = parent; parent = inputname(2);

	if isempty(parent) && isfield(parent_obj, 'type')
		parent = parent_obj.type;
	end
	
	if isempty(parent)
		error('Unable to determine parent type from input.');
	end
end

% TODO: check for the availability of various relevant columns, unless 'fast'

%--
% build and execute update query
%--

% NOTE: this is the child's foreign key to the parent

if ischar(helpers.count_column)
	
	parent_key = helpers.parent_key;
else
	parent_key = helpers.parent_key(parent);
end

% NOTE: this is where the parent keeps a count of the children

if ischar(helpers.count_column)
	
	count_column = helpers.count_column;
else
	count_column = helpers.count_column(child);
end

% TODO: consider limiting the update to a set of parents, not necessarily all

% NOTE: the sub-select creates the 'relation_count' table, which contains the 'child_count' for each parent represented through its 'parent_id'

try
	sql = [ ...
		'UPDATE ', parent, ' AS parent INNER JOIN', ...
		' (SELECT ', parent, '.id AS parent_id, COUNT(', child, '.id) AS child_count FROM ', parent, ' LEFT JOIN ', child, ' ON ', child, '.', parent_key, ' = ', parent, '.id GROUP BY ', parent, '.id)' ...
		' AS relation_count ON parent.id = relation_count.parent_id SET parent.', count_column,' = relation_count.child_count' ...
	];

	[status, result] = query(store, sql); 
catch
	% NOTE: this is a two-part update that works in SQLite
	
	% TODO: the problem seems to be with the 'UPDATE' command in general, the access mode?
	
	sql = [ ...
		'SELECT ', parent, '.id AS parent_id, COUNT(', child, '.id) AS child_count ', ...
		'FROM ', parent, ' LEFT JOIN ', child, ' ON ', child, '.', parent_key, ' = ', parent, '.id GROUP BY ', parent, '.id' ...
	];
	
	[status, relation] = query(store, sql);
	
	obj = get_database_objects(store, parent, [relation.parent_id]);
	
% 	db_disp; disp(relation); disp(obj)
	
	for k = 1:numel(obj)
		obj(k).(count_column) = relation([relation.parent_id] == obj(k).id).child_count;
	end
		
	set_database_objects(store, obj, [obj.id], {}, parent);
end
		
		