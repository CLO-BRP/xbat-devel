function type = build_prototype(field, source)

type = struct;

%--
% produce a default prototype
%--

if nargin < 2

	for k = 1:numel(field.column)
		type.(field.column{k}) = field.hint{k};
	end
	
%--
% build a prototype using provided a value source
%--

else
	% NOTE: the value source uses not the column names, but the field names!

	for k = 1:numel(field.column)

		if isfield(source, field.name{k})
			type.(field.column{k}) = source.(field.name{k});
		else
			% TODO: should we use the hint here? probably not
			
			type.(field.column{k}) = [];
		end
	end	
end

