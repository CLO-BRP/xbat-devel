function test_find_notes

% SETUP
% ----------------

[store, object, note, user] = setup_test_notes;

stores = {struct([]), store};

% TEST
% ----------------

notes = find_notes(store, [], 'conditions', ['user_id = ', num2str(user.id)]);


% TEST
% ----------------

% create a note
note.id    = []; 
note.title = 'Note';
note.body  = 'This is a note';

notes = find_notes(store, [], note, 'conditions', ['user_id = ', num2str(user.id)]);




% TEST
% ----------------

note(2).title = 'hello';
note(2).body  = 'a new unsaved note';

notes = find_notes(store, [], note, 'conditions', ['user_id = ', num2str(user.id)]);


% TEST
% ----------------

% This is one way to find the notes for an object

notes = find_notes(store, [], note, 'conditions', ['annotated_id = ', num2str(object.id)]);

% This should be synonymous

notes = find_notes(store, object, note);


% print
notes = find_notes([], object);

% This causes an error becuase there is no id
% another_object.id = '';
% notes = find_notes([], another_object, note);


