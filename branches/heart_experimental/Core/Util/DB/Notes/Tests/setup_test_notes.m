function [store, object, note, user] = setup_test_notes(num_notes)

if nargin
    num_notes = 1;
end

store = fullfile(fileparts(mfilename('fullpath')), 'test.sqlite');

if exist(store, 'file')
	delete(store);
end

% setup the notes

establish_notes(store);

%--
% object
%--

object.id   = 1; 
object.name = 'name';

query(store, create_table(object));

object.id   = []; 
object.name = 'My first object';

object = set_database_objects(store, object, []);

%--
% user
%--

user.id    = 1;


% create a note

note.id    = []; 
note.title = 'Note';
note.body  = 'This is a note';

% annotatable.notes = ['note 1', 'note 2']

% TEST
% ---------------------------
% One object, one note (same number of notes as objects)

set_notes(store, object, note, user(1));

note = find_notes(store, object);