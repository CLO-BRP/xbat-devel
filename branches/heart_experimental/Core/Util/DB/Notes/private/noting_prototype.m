function noting = noting_prototype

hint = column_type_hints;

noting.id = hint.i; 

% NOTE: we want to consider indicating what to do on noting revision

% noting.version = hint.i;

noting.note_id = hint.i;

noting.annotated_type = hint.s; 

noting.annotated_id = hint.i;

noting.user_id = hint.i;

noting.created_at = hint.ts; 

noting.modified_at = hint.ts;

