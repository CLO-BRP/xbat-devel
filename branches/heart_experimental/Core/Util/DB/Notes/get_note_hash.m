function hash = get_note_hash(note)

% get_note_hash - content based signature for note
% ------------------------------------------------
%
% hash = get_note_hash(note)
%
% Input:
% ------
%  note - struct
%
% Output:
% -------
%  hash - signature

if numel(note) > 1
	hash = iterate(mfilename, note); return;
end

hash = md5(keep_fields(note, {'title', 'body'}));