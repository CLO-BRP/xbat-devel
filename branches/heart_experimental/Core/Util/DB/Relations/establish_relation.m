function [status, result] = establish_relation(store, table1, table2, value, opt)

% establish_relation - create table to support relation
% -----------------------------------------------------
%
% [status, result] = establish_relation(store, table1, table2, value, opt)
%
%              sql = establish_relation([], table1, table2, value, opt)
%
% Input:
% ------
%  store - database
%  table1, table2 - involved in relation
%  value - of relation
%  opt - options
%
% Output:
% -------
%  status - of query
%  result - of query
%  sql - query string

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set and possibly output default options
%--

% TODO: finish documenting the meaning of these options, reconsider order here

if nargin < 5
	opt.force = false;		% force the creation of relation, drops previous relation
	
	% NOTE: these default values are over-ridden by the existence of a non-trivial value for the relation
	
	opt.id = false;			% add primary key 'id'
	
	opt.modified = false;	% add modification timestamp at 'modified_at'
	
	opt.unique = {};
	
	% NOTE: these fields relate to the name of the table and column names in the case of a self relation
	
	opt.name = '';		% set relation table name
	
	opt.sort = true;		% sort base table names when building relation table name
		
	opt.self = 'target';
	
	opt.matrix = false;		% create matrix relation, columns are 'row_id' and 'col_id' 
	
	opt.relax = false;		% skip testing for existence of related tables
end

if ~nargin
	status = opt; return;
end

%--
% set no default value
%--

% NOTE: the value stores a prototype for the value of the relation

if nargin < 4 || isempty(value) 
	value = struct;
end

%--
% set self relation default
%--

if nargin < 3 || isempty(table2)
	table2 = table1;
end 

%-----------------------------
% SETUP
%-----------------------------

% NOTE: we may force the relation table to have a specified name

relation = relation_table_name(table1, table2, opt);

% TODO: consider using 'create_table' to do this

if opt.force
	sql = ['DROP IF EXISTS ', relation, ';']; query(store, sql);
end

%--
% check for tables to relate
%--

% NOTE: if we opt to 'relax' then we skip the related table test

if ~opt.relax && ~isempty(store) && (~has_table(store, table1) || ~has_table(store, table2))
	error('A table to relate is missing.'); 
end

%-----------------------------
% CREATE RELATION TABLE
%-----------------------------

%--
% check for self relation
%--

self = strcmp(table1, table2);

%--
% create prototype
%--

% NOTE: we create a prototype object, declare table constraints in the 'create_table' options, and create

if self
	% TODO: implement consequences of 'matrix' convention
	
	% NOTE: in the non-matrix case we have a default 'target_id' (uses opt.self) column which indicates the objects pointed to
	
	% NOTE: in the non-matrix case of self relations there is intrinsic asymmetry

	if opt.matrix
		field = {'row_id','col_id'};
	else
		field = {[opt.self, '_id'], [table1, '_id']};
	end
else
	% NOTE: we allow the sort options to affect table name and column order

	if opt.sort
		field = strcat(sort({table1, table2}), '_id');
	else
		field = strcat({table1, table2}, '_id');
	end
end 

% NOTE: when we request it or a relation has a 'value' we give it its own primary key

if opt.id || ~trivial(value)
	prototype.id = 1; 
else
	prototype = struct;
end

prototype = struct_merge(prototype, pack_field_value(field, [1, 1]));

% NOTE: a relation may have values, we merge a this to the relation prototype

% NOTE: the trivial value structure corresponds to a binary relation

% NOTE: for complex values we should use a foreign key value instead of actual values

if ~trivial(value)
	
	prototype = struct_merge(prototype, value);
end

prototype.created_at = 1;

% NOTE: when a relation has a 'value' we give it a modification date

if opt.modified || ~trivial(value)

	prototype.modified_at = 1; 
end 

%--
% create table query, and if possible table
%--

topt = create_table; 

% NOTE: typically we are unique on the relation identifiers, and user if available

% NOTE: we may be unique across values as well, given the timestamp fields this means keep everything

if isempty(opt.unique)
	
	topt.constraints = ['UNIQUE(', str_implode(field, ', '), ')'];
else
	if ~isempty(setdiff(opt.unique, fieldnames(prototype)))
		
		error('A requested unique column field is not available.');
	end
	
	topt.constraints = ['UNIQUE(', str_implode(opt.unique, ', '), ')'];
end

if ~isempty(store)
	topt.store = store;
end

[sql, lines] = create_table(prototype, relation, topt);

% NOTE: we output the sql in both line and string representation

if isempty(store)
	
	status = sql; result = lines; 
	
	if ~nargout
		disp(' '); sql_display(lines); disp(' '); clear status;
	end
	
	return;
end

%--
% establish relation if database was provided
%--

[status, result] = query(store, sql);

