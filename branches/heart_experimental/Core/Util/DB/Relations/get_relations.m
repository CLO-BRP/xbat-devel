function relation = get_relations(store, table1, id1, table2, id2, opt)

% get_relations - from database
% -----------------------------
%
% relation = get_relations(store, table1, id1, table2, id2, opt)
%
% Input:
% ------
%  store - database
%  table1 - in relation
%  id1 - table1 identifiers
%  table2 - in relation
%  id2 - table2 identifiers
%  opt - relation options
%
% Output:
% -------
%  relation - struct array

%--
% handle input
%--

if nargin < 6
	opt = establish_relation; 
end 

if ~nargin
	relation = opt; return; 
end 

% NOTE: in this case we only select on one identifier

if nargin < 5
	id2 = []; 
end 

%--
% build query
%--

table = relation_table_name(table1, table2, opt);

sql = ['SELECT * FROM ', table];

if ~isempty(id1)
	
	if numel(id1) > 1
		sql = [sql, ' WHERE ', table1, '_id IN (', str_implode(id1, ', ', @int2str), ')'];
	else
		sql = [sql, ' WHERE ', table1, '_id = ', int2str(id1)];
	end
	
end

if ~isempty(id2)

	if numel(id2) > 1
		sql = [sql, ' AND ', table2, '_id IN (', str_implode(id2, ', ', @int2str), ')'];
	else
		sql = [sql, ' AND ', table2, '_id = ', int2str(id2)];
	end
	
end

sql = [sql, ';'];

%--
% execute query if possible
%--

[ignore, relation] = query(store, sql);
