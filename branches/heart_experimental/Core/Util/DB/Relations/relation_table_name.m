function name = relation_table_name(table1, table2, opt)

%--
% get and possibly output default options
%--

if nargin < 3
	opt = establish_relation;
end

if ~nargin
	name = opt; return;
end 

%--
% check if relation table is explicitly named
%--

if ~isempty(opt.name)
	
	name = opt.name; 

%--
% build relation table name from related table names
%--

else 
	
	% NOTE: the default uses the convention of ordering the table names in the relation table

	if opt.sort
		name = str_implode(sort({table1, table2}), '_');
	else
		name = [table1, '_', table2];
	end
	
end