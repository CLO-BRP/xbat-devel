function view = select_related(store, focus, related, where)

% select_related - generate queries for typical joins
% ---------------------------------------------------
% 
% view = select_related(store, focus, related, where)
%
% Input:
% ------
%  store - database
%  focus - table
%  related - tables
%  where - condition
%
% Output:
% -------
%  view - struct containing queries
%
% NOTE: aliases are not used, the 'WHERE' syntax should refer to table names

% TODO: implement the discovery of latent relations, not explicit in table names but in foreign key column naming conventions

%--------------------------
% HANDLE INPUT
%--------------------------

view = struct;

%--
% set trivial where and make sure it is celled
%--

if nargin < 4
	where = {}; 
end

if ischar(where) 
	where = {where}; 
end

% NOTE: this should not be required, 'sql_string' should be smarter

if ~isempty(where) && where{1}(1) ~= ' ';
	where{1} = [' ', where{1}];
end

%--
% consider all possibly related atomic tables
%--

if nargin < 3
	related = get_atomic_tables(store);
end

if ischar(related)
	related = {related};
end

%--
% consider all tables as focus and merge results
%--

if nargin < 2 || isempty(focus)
	
	focus = get_atomic_tables(store);
	
	for k = 1:numel(focus)
		view = struct_merge(view, select_related(store, focus{k}, related, where));
	end 
	
	if ~nargout
		display_result(view); clear view;
	end

	return;	
end

%--------------------------
% SETUP
%--------------------------

%--
% get relation table names along with related table names and info
%--

% NOTE: the potentially related tables are reduced here to an actual table subset

[relation, related, info] = get_relation_tables(store, focus, related);

%--
% get single related table join accessors
%--

% NOTE: the output of these should be related table column values along with focus identifier

for k = 1:numel(related)
	
	select = get_select(focus, relation{k}, related{k}, info{k});
	
	join = get_join(focus, relation{k}, related{k});
	
	sql = {select{:}, join{:}, where{:}};
	
	sql{end}(end + 1) = ';';
	
	view.(['get_', focus, '_', related{k}]).sql = sql;
	
	view.(['get_', focus, '_', related{k}]).focus = focus;
end

%--
% display result during development
%--

if ~nargout
	display_result(view); clear view; 
end


%-------------------------
% DISPLAY_RESULT
%-------------------------

function display_result(view)
	
field = fieldnames(view);

disp(' ');

for k = 1:numel(field)
	
	% NOTE: the full string display helps debug any missing space problems
	
	disp(field{k}); 
% 	disp(' '); 
% 	sql_display(sql_string(view.(field{k}).sql));
	disp(' '); 
	sql_display(view.(field{k}).sql); 
	disp(' ');
	
end


%-------------------------
% GET_RELATION_TABLES
%-------------------------

function [relation, related, columns] = get_relation_tables(store, focus, related)

% TODO: filter considered tables using related from the start

%--
% get relation tables, we also check for base table
%--

% NOTE: relation tables contain base table name as part of their name

% [status, result] = query(store, ['SELECT name FROM sqlite_master WHERE type = ''table'' AND name LIKE ''%', focus ,'%'';']); %#ok<ASGLU>
% 
% relation = {result.name};

table = get_database_tables(store);

relation = table(string_contains(table, focus));

% NOTE: we should be careful with the table name input, here we check for the relation focus table

if ~string_is_member(focus, relation)
	error(['Focus table ''', focus, ''' not found.']);
end

relation = setdiff(relation, focus);

% NOTE: relation tables have a single underscore

for k = numel(relation):-1:1

	if sum(relation{k} == '_') ~= 1
		relation(k) = [];
	end
end

if isempty(relation)
	relation = {}; related = {}; columns = {}; return;
end 

if ischar(relation)
	relation = {relation};
end

%--
% parse relation table names to get related tables if needed
%--

if nargout < 2
	return;
end 

% NOTE: the other part of the relation table name is the related table

for k = 1:numel(relation)	
	
	% NOTE: we need this logic to allow for self referencing relations such as 'tag_tag'
	
	part = str_split(relation{k}, '_');

% 	db_disp; disp('RELATION:'); disp(relation{k}); disp('PART:'); iterate(@disp, part); disp('FOCUS:'); disp(focus)
	
	if isequal(part{1}, part{2})
		actual(k) = part(1);
	else
		actual(k) = setdiff(part, focus);
	end
end

[related, ixa] = intersect(actual, related); relation = relation(ixa);

% NOTE: we may have removed all relations from consideration here

if isempty(relation)
	relation = {}; related = {}; columns = []; return;
end 

%--
% get related table info if requested
%--

if nargout < 3
	return;
end

% NOTE: this serves to check the availability of related tables, if this fails they are missing

columns = cell(size(related));

for k = 1:numel(related)
	columns{k} = get_table_columns(store, related{k});
	
% 	[status, info{k}] = query(store, ['PRAGMA table_info(''', related{k}, ''');']); %#ok<ASGLU>
end


%-------------------------
% GET_SELECT
%-------------------------

% NOTE: this is a working example from the tag tag relation

% SELECT tag_tag.target_id AS id, tag_tag.tag_id AS tag_id, tag.name AS name, tag_tag.created_at AS created_at 

function sql = get_select(focus, relation, related, columns)

%--
% get identifier elements of select as well as relevant related columns
%--

if strcmp(focus, related)
	focus_id = [relation, '.target_id AS id'];
else
	focus_id = [focus, '.id AS id'];
end

% TODO: make this optional

related_id = '';

if string_is_member('id', columns)
	
	if strcmp(focus, related)
		related_id = [relation, '.', related, '_id AS ', related, '_id, '];
	else
		related_id = [related, '.id AS ', related, '_id, '];
	end
end
	
remove = {'id', 'created_at', 'modified_at'}; columns = setdiff(columns, remove);

% NOTE: we select the focus table identifier, the related table identifier and content, and the relation creation timestamp

% NOTE: the 'created_at' column in the relation table is part of the conventions

sql{1} = [ ...
	'SELECT ', focus_id, ', ', ...
	related_id, str_implode(strcat(related, {'.'}, columns, {' AS '}, columns), ', '), ', ', ...
	relation, '.created_at AS created_at' ...
];


%-------------------------
% GET_JOIN
%-------------------------

function sql = get_join(focus, relation, related)

%--
% pack input into cells if needed for uniformity
%--

if ischar(relation)
	relation = {relation};
end

if ischar(related)
	related = {related};
end

%--
% build join part of query and express as string
%--

sql{1} = [' FROM ', focus, ' ']; 

% NOTE: this implements the join encoded in the foreign key naming conventions

for k = 1:numel(related)

	if strcmp(focus, related{k})
		
		% NOTE: in the case of the self-join we only join, well once
		
		sql{end + 1} = [' JOIN ', relation{k}, ' ON ', focus, '.id = ', relation{k}, '.', focus, '_id'];
	else
		sql{end + 1} = [' JOIN ', relation{k}, ' ON ', focus, '.id = ', relation{k}, '.', focus, '_id'];

		sql{end + 1} = [' JOIN ', related{k}, ' ON ', relation{k}, '.', related{k}, '_id = ', related{k}, '.id'];
	end	
end

% NOTE: this is a working example for the tag tag relation

% FROM tag JOIN tag_tag ON tag.id = tag_tag.tag_id 


