function counts = tag_counts(store, order, type)

% tag_counts - computation from store
% -----------------------------------
%
% counts = tag_counts(store, order, type)
%
% Input:
% ------
%  store - database
%  order - for list 'id', 'name', or 'count' (def: 'name')
%  type - of object to count
%
% Output:
% -------
%  counts - of tag frequencies
%
% See also: known_tags

%--
% handle input
%--

if nargin < 3
	type = [];
end

if nargin < 2
	order = 'name';
end

%--
% compute counts, possibly order
%--

% TODO: the 'camelize' transformation implies the use of the taggable 'class' convention in the tagging schema

if isempty(type)
	condition = '';
else
	condition = [' WHERE tagging.taggable_type = ''', camelize(type), ''''];
end

sql = [ ...
	'SELECT tag.id AS id, tag.name AS name, COUNT(tagging.id) AS count', ...
	' FROM tag JOIN tagging ON tag.id = tagging.tag_id', ...
	  condition, ...
	' GROUP BY tag.id' ...
];

[status, counts] = query(store, sql); %#ok<ASGLU>

% TODO: we should be able to order in the query, consider code simplicity

if isfield(counts, order)

	column = {counts.(order)};

	if ~iscellstr(column)
		column = cell2mat(column);
	end

	[ignore, ix] = sort(column); %#ok<ASGLU>

	counts = counts(ix);
end

%--
% display if output not captured
%--

if ~nargout
	disp(' ');
	str_wrap(str_implode(strcat({counts.name}, {' ('}, iterate(@int2str, [counts.count]), ')'), ', '));
	disp(' ');
	
	clear counts;
end


