function [tagging, tagged] = find_by_tag(store, tag, varargin)

% find_by_tag - find tagged objects
% ---------------------------------
%
% [tagging, tagged] = find_by_tag(store, tag, field, value, ... , opt)
%
% opt = find_by_tag
%
% Input:
% ------
%  store - database
%  tag - or tag list filter ('or', default for 'opt.logic' is used to combine multiple tags)
%  field, value - pairs to modify find, allowed fields are 'type' (of object), 'limit', 'offset', and 'order'
%  opt - options
%
% Output:
% -------
%  tagging - relations
%  tagged - objects
%  opt - options
%
% NOTE: we only get 'tagged' object when the 'type' input is defined
%
% See also: known_tags, tag_counts


% TODO: we probably want to move the 'logic' option to the field, value part of the signature

% TODO: develop the 'and' query

% TODO: consider adding the (field, value) option to other finders

% NOTE: get all types of tagged objects by default

type = ''; tagged = [];

%--
% handle input
%--

% NOTE: the options struct may be the last of the variable arguments list

if ~isempty(varargin) && isstruct(varargin{end})
	
	opt = varargin{end}; varargin(end) = [];
else
	% NOTE: we will likely not need all of these, but it makes sense to have this
	
	% NOTE: option names may not be the best at the moment
	
	opt.type_convention = @camelize;
	
	opt.table_convention = @lower;
	
	opt.primary_key = 'id';
	
	opt.logic = 'or';
	
	if ~nargin
		tagging = opt; return;
	end 
end

% NOTE: make sure that we have a cell array representation for the tags

if ischar(tag)
	if string_contains(tag, ',')
		sep = ',';
	else
		sep = ' ';
	end
	
	tag = str_split(tag, sep);
end

%--
% build and execute find tagging query
%--

% NOTE: we first get the identifiers of 'query' tags found in store

known = get_taggings(store);

tag_id = get_tag_id(intersect({known.name}, unique_tags(tag)), known);

sql = {'SELECT * FROM tagging '};

if numel(tag_id) == 1
	
	sql{end + 1} = [' WHERE tagging.tag_id = ', int2str(tag_id)];
else
	% TODO: implement 'AND' combination of tags
	
	switch opt.logic
		case 'or'
			sql{end + 1} = [' WHERE tagging.tag_id IN (', str_implode(tag_id, ', ', @int2str), ')'];
			
		case 'and'
			error('AND not implemented yet.');
			
		otherwise
			error(['Unrecognized logic ''', opt.logic, ''' for multiple tag combination.']);
	end
end

% NOTE: here we add the 'limit', 'offset', and 'order' elements to the query if available

if ~isempty(varargin)
	% TODO: move this up, so we can consider select early on
	
	[field, value] = get_field_value(varargin, {'select', 'type', 'condition', 'limit', 'offset', 'order'});

	append = struct;
	
	for k = 1:numel(field)
		append.(field{k}) = value{k}; 
	end
	
	if isfield(append, 'type')
		type = append.type;
		
		sql{end + 1} = [' AND tagging.taggable_type = ''', opt.type_convention(type), ''''];
	end

	if isfield(append, 'condition')
		% NOTE: spaces are added for input convenience
		
		% TODO: consider a more general approach here
		
		sql{end + 1} = [' ', append.condition, ' '];
	end
	
	if isfield(append, 'limit')
		
		if isfield(append, 'offset')
			sql{end + 1} = [' LIMIT ', int2str(append.offset), ', ', int2str(append.limit)];
		else
			sql{end + 1} = [' LIMIT ', int2str(append.limit)];
		end
	end
	
	if isfield(append, 'order')
		sql{end + 1} = [' ORDER BY ', append.order];
	end
end

% db_disp; iterate(@disp, sql); sql_string(sql)

[status, tagging] = query(store, sql_string(sql)); %#ok<ASGLU>

%--
% get objects if requested in the case of a single type
%--

if isempty(type) || nargout < 2
	if ~nargout
		disp(tagging); clear tagging;
	end
	
	return;
end

% TODO: consider only retrieving the object identifiers

% opt = get_database_objects_by_column; opt.fields = {'id'};

tagged = get_database_objects_by_column(store, opt.table_convention(type), opt.primary_key, [tagging.taggable_id]);



