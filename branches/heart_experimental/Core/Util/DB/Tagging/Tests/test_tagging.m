function test_tagging(verb)

if ~nargin
	verb = 0;
end

%--
% setup test database 
%--

% TODO: extend to other adapters, through 'get_test_database'

store = tagging_test_file;

if exist(store, 'file')
	delete(store);
end

%--
% store some objects and create a couple of users
%--

object.id = 1; object.name = 'name';

query(store, create_table(object));

for k = 1:9
	object(k).id = []; object(k).name = ['object', int2str(k)];
end 

object = set_database_objects(store, object);

% NOTE: this is the only field we need for tagging

user.id = 1; user(2).id = 2;

%--
% make taggable and perform various tag operations
%--

establish_tagging(store);

% add tags using some syntax variations

set_taggings(store, [], 'red green blue');

set_taggings(store, [], {'cat' 'dog' 'mouse'});

set_taggings(store, [], {'boat car', {'cat' 'dog' 'mouse'}});

set_taggings(store, [], {{'ship'}, {'truck'}, {'cat' 'dog' 'mouse'}});

set_taggings(store, [], 'red green blue purple');

% tag some objects

set_taggings(store, object, 'test', user(1));

set_taggings(store, object(1:6), 'red green blue', user(1), 'object');

% NOTE: here we test 'set_taggings' with identifier input

set_taggings(store, [object(4:6).id], 'cat mouse bear', user(2), 'object');

% NOTE: this is useful for visual inspection

for j = 1:numel(user)
	
	% NOTE: here we test 'get_taggings' with identifier input
	
	for k = 1:numel(object)
		tags = get_taggings(store, object(k).id, user(j), 'object'); object(k).tags = tags{1};
	end

	if verb
		disp(user(j)); disp(object);
	end

end

% NOTE: this is the test

% NOTE: the call to 'cellfree' is required because the output of 'get_taggings' is a cell array of tag sets

% NOTE: tags are alphabetically ordered on retrieval, if not we could check for empty 'setdiff' 

assert_equal(cellfree(get_taggings(store, object(1), user(1), 'object')), {'blue', 'green', 'red'});

assert_equal(cellfree(get_taggings(store, object(6), user(1), 'object')), {'blue', 'green', 'red'});

assert_equal(cellfree(get_taggings(store, object(6), user(2), 'object')), {'bear', 'cat', 'mouse'});

assert_equal( ...
	cellfree(get_taggings(store, object(6), [], 'object')), union({'bear', 'cat', 'mouse'}, {'blue', 'green', 'red'}) ...
);

