function [status, result] = establish_tagging(store)

% establish_tagging - add tables for tagging
% ------------------------------------------
%
%     [sql, lines] = establish_tagging
%
% [status, result] = establish_tagging(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  sql - string
%  lines - of query
%  status - of query request
%  result - of query

%--
% create tag and tagging prototypes 
%--

hint = column_type_hints;

% TAG

tag.id = hint.i; 

tag.name = hint.s; 

tag.created_at = hint.ts;

% TAGGING

tagging.id = hint.i; 

tagging.tag_id = hint.i; 

tagging.taggable_type = hint.s; 

tagging.taggable_id = hint.i;

tagging.score = hint.real;

tagging.vote = hint.i;

tagging.user_id = hint.i;

tagging.created_at = hint.ts;

tagging_opt = create_table; 

tagging_opt.unique = {'tag_id', 'taggable_type', 'taggable_id', 'user_id'};

description = empty(column_description);

description(end + 1) = struct('name', 'score', 'type', 'REAL', 'constraints', 'DEFAULT NULL');

description(end + 1) = struct('name', 'vote', 'type', 'INTEGER', 'constraints', 'DEFAULT 1');

tagging_opt.column = description;

% tag_set

tag_set.id = hint.integer;

tag_set.guid = hint.string;

tag_set.name = hint.string; 

tag_set.context_id = hint.integer;

tag_set.context_type = hint.string;

% NOTE: if a tagset uses a prefix, then the interface will show the
% post-prefix value but store the prefix as well? What about other
% interfaces? They perhaps do something special as well with namespaced
% tags

tag_set.prefix = hint.string;

tag_set.closed = hint.logical;

tag_set.public = hint.logical;

tag_set.created_at = hint.ts;

%--
% build query to create tag and tagging tables
%--

opt = create_table; opt.store = store; tagging_opt.store = store;

sql = {'BEGIN;', create_table(tag, '', opt), create_table(tagging, '', tagging_opt), create_table(tag_set, '', opt), 'COMMIT;'};

%--
% output query
%--

if ~nargin
	store = []; 
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql); 
end 


