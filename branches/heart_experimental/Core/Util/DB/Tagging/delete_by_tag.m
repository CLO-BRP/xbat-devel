function deleted = delete_by_tag(store, tag, type, clean)

% delete_by_tag - and type from store
% -----------------------------------
% 
% deleted = delete_by_tag(store, tag, type, cleanup)
%
% Input:
% ------
%  store - database
%  type - of object
%  tag - or tag list filter ('or' is used to combine multiple tags)
%  clean - tags after delete
%  
% Output:
% -------
%  deleted - objects
%
% See also: find_by_tag, delete_database_objects, delete_taggings, cleanup_database_tags

%--
% handle input
%--

if nargin < 4
	clean = false;
end

%--
% select and delete objects using tag, then remove all related taggings
%--

% TODO: this should also be user aware, to prevent accidental deletion, no protection from malicious activities

[ignore, deleted] = find_by_tag(store, tag, 'type', type); %#ok<ASGLU>

% NOTE: no doing this proved catastrophic in an earlier version

if isempty(deleted)
	return;
end

% TODO: delete all should not be allowed easily!

delete_database_objects(store, deleted, type);

delete_taggings(store, deleted, type);

%--
% cleanup if requested
%--

if ~clean
	return; 
end

cleanup_database_tags(store);
