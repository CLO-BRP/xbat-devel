function [tags, tagging] = get_taggings(store, obj, user, table)

% get_taggings - get object tags from database
% --------------------------------------------
%
% [tags, tagging] = get_taggings(store, obj, user, table)
%
% Input:
% ------
%  store - database
%  obj - tagged
%  user - tagger
%  table - name
%
% Output:
% -------
%  tags - for objects
%  tagging - relation objects
%
% See also: set_taggings

% TODO: when called with only 'store' input this should be called 'get_tags', however this may have clashed ... reconsider this problem

%--
% return all tags, for all users
%--

if nargin < 2
	tags = get_database_objects_by_column(store, 'tag'); return;
end

%--
% typically reflect table from input
%--

if nargin < 4
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%--
% get object identifiers
%--

% NOTE: we accept identified object array or identifier array input

if isstruct(obj)
	id = get_id_field(obj);
else
	id = obj;
end

%--
% return tags for objects
%--

% NOTE: we get enough of a tagging relation 'object' that we can use the relation output to delete them

% NOTE: in the case of rating we get even more because we intend to update them

sql = { ...
	'SELECT tagging.id AS id, tagging.taggable_id AS taggable_id, tag.name AS name ', ...
	' FROM tagging', ...
	' JOIN tag ON tagging.tag_id = tag.id', ...
	[' WHERE tagging.taggable_type = ''', table, ''' AND'] ...
};

% NOTE: user input means we only retrieve tags for that user 

if nargin > 2 && ~trivial(user)
	sql{end + 1} = [' tagging.user_id = ', int2str(user.id), ' AND'];
end

if numel(obj) > 1
	sql{end + 1} = [' tagging.taggable_id IN (', str_implode(id, ', ', @int2str), ');'];
else
	sql{end + 1} = [' tagging.taggable_id = ', int2str(id) ';'];
end

% NOTE: return the query if we have no database to query

if trivial(store)
	
	tags = sql;

	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear tags;
	end
	
	return;
end

[status, tagging] = query(store, sql_string(sql)); %#ok<ASGLU>

%--
% extract tag names and pack results
%--

% NOTE: we filter result using the object identifier, and use comma-separated list get tags cell array

taggable_id = [tagging.taggable_id];

for k = 1:numel(id)
	
	tags{k} = sort({tagging(taggable_id == id(k)).name}); %#ok<AGROW>
end






