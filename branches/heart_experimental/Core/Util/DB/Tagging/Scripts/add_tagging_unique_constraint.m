function [status, result] = add_tagging_unique_constraint(store)

% add_tagging_unique_constraint - to ensure a single tag, object, user combination
% --------------------------------------------------------------------------------
%
% [status, result] = add_tagging_unique_constraint(store)
% 
% Input:
% ------
%  store - to update
%
% Output:
% -------
%  status, result - of query
%
% See also: establish_tagging

[status, result] = query(store, 'ALTER TABLE tagging ADD CONSTRAINT UNIQUE(tag_id, taggable_type, taggable_id, user_id)');