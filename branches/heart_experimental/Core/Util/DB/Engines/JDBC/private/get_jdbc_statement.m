function statement = get_jdbc_statement(connection)

% TODO: the 'TYPE_FORWARD_ONLY' type is required by the SQLite driver, what does this mean?

% NOTE: we probably don't want an exception handler here, recognize driver

try
	statement = connection.createStatement(java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);
catch
	statement = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
end