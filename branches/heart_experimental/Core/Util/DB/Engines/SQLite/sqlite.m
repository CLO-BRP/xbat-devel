function [status, result] = sqlite(file, mode, sql, parameters, full_names)

% sqlite - sqlite access
% ----------------------
%
% [status, result] = sqlite(file, mode, sql, parameters)
%
% Input:
% ------
%  file - database file
%  mode - access mode
%  sql - sql
%  parameters - parameters
%
% Output:
% -------
%  status - status
%  result - result

% TODO: possibly factor this to use in various places, note the existence of the 'query' function

if isstruct(file)
	
	candidate = {'file', 'database', 'sqlite', 'sqlite3'};
	
	for k = 1:numel(candidate)
		
		if isfield(file, candidate{k})
			file = file.(candidate{k}); break;
		end	
	end
end

% TODO: we should be able to simplify this code considerably

%---------------------
% HANDLE INPUT
%---------------------

persistent HELPER

if isempty(HELPER)
	% NOTE: this call to exist is handled by the overloaded 'function_handle' version
	
	if exist(@sqlite_mex) %#ok<EXIST>
		HELPER = @sqlite_mex;
	else
		HELPER = @sqlite_jdbc;
	end
end

%--
% set no full names default
%--

if nargin < 5 || isempty(full_names)
    full_names = 0;
end

%--
% handle file and query input case, we infer the mode from the SQL
%--

% TODO: this should be completed and eventually completely removed from interface

if nargin < 3
	sql = mode; mode = 'prepared'; 
	
	if ~iscell(sql)
		if strncmp(sql, 'DELETE', 6)
			mode = 'exec';
		end
	else
		if strcmp(sql{1}, 'BEGIN;')
			mode = 'exec';
		end
	end	
end

%--
% check mode and get MEX code
%--

modes = {'exec', 'get_table', 'prepared'};

if ~string_is_member(mode, modes), error('Unrecognized database access mode.'); end

switch mode	
	case 'exec', mode = 1;
		
	case 'get_table', mode = 2;
		
	case 'prepared', mode = 3;	
end

% NOTE: add full name option to mode code 

if full_names
    mode = mode + 256;
end

%--
% determine if we are tracing and begin trace if needed
%--

% TODO: factor much of the trace code

if ~sqlite_trace
	
	tracing = 0; 	
else	
	%--
	% get caller and current configuration to determine if we are tracing
	%--
	
	caller = get_caller; 
	
	% NOTE: can't really trace from the command-line
	
	tracing = ~isempty(caller); 
	
	% NOTE: we may only want to listen to specific callers
	
	if tracing 		
		listen = get_env('sqlite_trace_listen');

		if ~isempty(listen)
			tracing = any(string_is_member(listen, {caller.stack.name}));
		end	
	end
	
	% NOTE: we may want to ignore callers
	
	if tracing	
		% NOTE: we don't trace various low level queries, they are numerous and typically not interesting
		
		always_ignore = {'sqlite_get_elements', 'get_relation_tables', 'has_table'};

		ignore = get_env('sqlite_trace_ignore');
		
		if isempty(ignore)
			ignore = always_ignore;
		else
			ignore = union(ignore, always_ignore);
		end

		if ~isempty(ignore)
			tracing = ~string_is_member(caller.name, ignore);
		end
	end
end

%--
% start tracing if requested
%--

if tracing
	% TODO: factor and make the link the caller location
		
	sep = str_line(72,'_'); disp(sep);
	disp(' ');
	disp(' SQLITE-TRACE');
	disp(sep);
	disp(' ');
	disp('CALLER:')
	
	stack_disp(caller.stack);
	
	% NOTE: the line below is nearly as good as the function call
	
% 	iterate(@disp, strcat({'  '}, iterate(@linkify, {caller.stack.file})));

	% TODO: the file can be linkified to, and explicitly opened with the 'SQLite Database Browser' tool
	
	disp(' ');
	disp('FILE:');
	disp(['  ', linkify(file, '', 'show_file')]);
	disp(' ');
	disp('SQL:');
	sql_display(sql);
	disp(' ');
end

%--
% ensure SQL string
%--

if ~ischar(sql) && ~iscellstr(sql)
	error('SQL input must be string or string cell array.');
end

sql = sql_string(sql);

%--
% check parameters
%--

% NOTE: it is easier to check here rather than in the MEX

if nargin > 3 && ~isempty(parameters)
	
	if (ndims(parameters) ~= 2) || ~iscell(parameters)
		error('Parameters must be a cell matrix.');
	end
	
	[count, sets] = size(parameters);
	
	if count ~= count_parameters(sql)
		error('SQL parameter count does not match available parameters.');
	end
	
	% NOTE: does this ever happen given the dimension test?
	
	if ~sets
		error('There are no available parameter sets.');
	end 
end

%---------------------
% ACCESS DATABASE
%---------------------

%--
% check whether we need to return data
%--

output_requested = query_returns_result(sql);

%--
% execute statement
%--

if tracing
	start = clock;
end 

try
	if ~output_requested

		if nargin < 4
			status = HELPER(file, mode, sql);
		else
			status = HELPER(file, mode, sql, parameters);
		end

		result = [];
	else
		if nargin < 4
			[status, result] = HELPER(file, mode, sql);
		else
			[status, result] = HELPER(file, mode, sql, parameters);
		end
	end
	
catch	
	disp(' ');
	
	disp(['Should ''', file, ''' exist, for the following query?']);
	
	if iscell(sql)
		iterate(@disp, sql);
	else
		disp(sql);
	end
	
	disp(' ');
	
	% TODO: consider something more graceful
	
	rethrow(lasterror);
end

% NOTE: we are not measuring the time required to recover values by tracing here

if tracing
	
	disp('ELAPSED:')
	disp(['  ', num2str(etime(clock, start), 5), ' sec']);
	disp(' ');
	disp('STATUS:')
	disp(['  ', int2str(status), ' (''', decode_status(status), ''')']);
	disp(sep);
	disp(' ');
	
end

%---------------------
% RECOVER FROM M2XML
%---------------------

if isstruct(result) && ~isempty(result)
	result = recover_values(result);
end

%--
% return result as single output
%--

if nargout < 2
    status = result;
end


%---------------------
% COUNT_PARAMETERS
%---------------------

function count = count_parameters(sql)

% NOTE: we are only handling positional parameters

count = length(find(sql == '?'));


%---------------------
% DECODE_STATUS
%---------------------

% NOTE: these are the C status codes for SQLite

function status = decode_status(code)

persistent STATUS_CODES;

if isempty(STATUS_CODES)
	
	STATUS_CODES = { ...
		0, 'SQLITE_OK'; ...
		1, 'SQLITE_ERROR'; ...
		2, 'SQLITE_INTERNAL'; ...
		3, 'SQLITE_PERM'; ...
		4, 'SQLITE_ABORT'; ...
		5, 'SQLITE_BUSY'; ...
		6, 'SQLITE_LOCKED'; ...
		7, 'SQLITE_NOMEM'; ...
		8, 'SQLITE_READONLY'; ...
		9, 'SQLITE_INTERRUPT'; ...
		10, 'SQLITE_IOERR'; ...
		11, 'SQLITE_CORRUPT'; ...
		12, 'SQLITE_NOTFOUND'; ...
		13, 'SQLITE_FULL'; ...
		14, 'SQLITE_CANTOPEN'; ...
		15, 'SQLITE_PROTOCOL'; ...
		16, 'SQLITE_EMPTY'; ...
		17, 'SQLITE_SCHEMA'; ...
		18, 'SQLITE_TOOBIG'; ...
		19, 'SQLITE_CONSTRAINT'; ...
		20, 'SQLITE_MISMATCH'; ...
		21, 'SQLITE_MISUSE'; ...
		22, 'SQLITE_NOLFS'; ...
		23, 'SQLITE_AUTH'; ...
		24, 'SQLITE_FORMAT'; ...
		25, 'SQLITE_RANGE'; ...
		26, 'SQLITE_NOTADB'; ...
		100, 'SQLITE_ROW'; ...
		101, 'SQLITE_DONE' ...
	};
end

status = STATUS_CODES{[STATUS_CODES{:, 1}] == code, 2};

