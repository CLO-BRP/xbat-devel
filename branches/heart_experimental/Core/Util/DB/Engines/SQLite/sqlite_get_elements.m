function [element, info] = sqlite_get_elements(store, type)

% sqlite_get_elements - SQLite adapter for 'get_database_elements'
% ----------------------------------------------------------------
%
% [element, info] = sqlite_get_elements(store, type)
%
% Input:
% ------
%  store - database
%  type - of element: 'TABLE', 'VIEW', or 'INDEX'
%
% Output:
% -------
%  element - names
%  info - for elements

%--
% get element names
%--

[status, result] = sqlite(store, ['SELECT name FROM sqlite_master WHERE type = ''', type, ''';']); %#ok<ASGLU>

element = {result.name};

%--
% get element info
%--

if nargout > 1
	info = cell(1, numel(element));
	
	switch type
		
		case {'table', 'view'}
			
			for k = 1:numel(element)
				info{k} = sqlite(store, ['PRAGMA table_info(', element{k}, ');']);
			end
			
		case 'index'
			
			for k = 1:numel(element)
				info{k} = sqlite(store, ['PRAGMA index_info(', element{k}, ');']);
			end
			
		% NOTE: perhaps there is some way of defining 'info' for other elements
			
		otherwise, info{k} = [];
			
	end
end
