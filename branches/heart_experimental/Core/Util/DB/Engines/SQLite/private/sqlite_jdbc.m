function [status, result] = sqlite_jdbc(file, mode, sql) %#ok<INUSL>

% sqlite_jdbc - helper, adapter 
% -----------------------------
%
% [status, result] = sqlite_jdbc(file, mode, sql)
%
% Input:
% ------
%  file - database
%  mode - ignored, part of adapter
%  sql - query
%
% Output:
% -------
%  status - of query
%  result - of query

store = create_database_config('adapter', 'sqlite-jdbc', 'database', file);

[status, result] = jdbc(store, sql);

% NOTE: the code below is from sqlite.m, these are the ways 'sqlite_mex' is called

% NOTE: the 'mode' used in 'sqlite_mex' is something that JDBC determines as well

% if ~output_requested
% 	
% 	if nargin < 4
% 		status = HELPER(file, mode, sql);
% 	else
% 		status = HELPER(file, mode, sql, parameters);
% 	end
% 	
% 	result = [];
% else
% 	if nargin < 4
% 		[status, result] = HELPER(file, mode, sql);
% 	else
% 		[status, result] = HELPER(file, mode, sql, parameters);
% 	end
% end