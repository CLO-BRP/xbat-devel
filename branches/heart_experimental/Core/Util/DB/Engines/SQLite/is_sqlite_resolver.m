function value = is_sqlite_resolver(proposed)

% is_sqlite_resolver - test proposed value
% ----------------------------------------
%
%     value = is_sqlite_resolver(proposed)
%
% resolvers = is_sqlite_resolver
%
% Input:
% ------
%  proposed - resolver name
%
% Output:
% -------
%  value - result of proposed test
%  resolvers - resolver names

resolvers = {'ROLLBACK', 'ABORT', 'FAIL', 'IGNORE', 'REPLACE'};

if ~nargin
	value = resolvers; return;
end

value = string_is_member(proposed, resolvers, 1);