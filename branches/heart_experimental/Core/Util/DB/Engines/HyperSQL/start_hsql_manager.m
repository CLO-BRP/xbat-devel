function start_hsql_manager

% start_hsql_manager - gui
% ------------------------
%
% start_hsql_manager
% 
% See also: start_hsql

tool = get_tool('runManagerSwing.bat');

if isempty(tool)
	error('You must first install HyperSQL by calling ''install_hsql''.');
end

% NOTE: we change directory for the sake of the 'runServer.bat' script

start = pwd; cd(tool.root);

eval(['!"', tool.file, '" &']);

cd(start);