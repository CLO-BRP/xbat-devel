function config = database_config_create(varargin)

%--
% initialize struct
%--

config = struct;

config.adapter = '';

config.database = '';

config.hostname = ''; 

config.username = '';

config.password = '';

%--
% set fields from input
%--

if ~nargin
	return;
end

config = parse_inputs(config, varargin{:});

%--
% check fields
%--

% NOTE: we can add any default values and or sanity checks here

if isempty(config.adapter)
	error('Adapter name input is required.');
end

if isempty(config.hostname)
	config.hostname = 'localhost';
end

if isempty(config.username)
	config.username = 'root';
end
