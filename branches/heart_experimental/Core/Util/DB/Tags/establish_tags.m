function [done, status, result] = establish_tags(file, table, opt) 

% establish_tags - add tagging tables to database
% -----------------------------------------------
%
% [done, status, result] = establish_tags(file, table, opt)
%
% Input:
% ------
%  file - file
%  table - name
%  opt - struct
%
% Output:
% -------
%  done - indicator
%  status - status
%  result - result

%---------------------
% HANDLE INPUT
%---------------------

%--
% set no force default
%--

if nargin < 3
	opt.force = 0; opt.user_id = 1;
end

if ~nargin
	done = opt; return;
end

%--
% create tag table if needed
%--

if ~has_table(file, 'tag')
	
	% NOTE: we don't force the creation of 'tag' table when we are making tables taggable
	
	if nargin > 2 && ~isempty(table)
		opt.force = 0;
	end 
	
	done = create_tag_table(file, opt.force);
	
	if ~done
		error('Failed to create ''tag'' table.');
	end
	
end

% NOTE: if there are no tables to make taggable we are done.

if nargin < 2 || isempty(table)
	return;
end

%--
% handle many table input
%--

many = iscell(table) && numel(table) > 1;
	
if many
	[done, status, result] = iteraten(mfilename, 2, file, table, opt); return;
end

%--
% check for table to make taggable and compute relation table name
%--

target = has_table(file, table);
	
if ~target
	error(['Table ''', table, ''' to make taggable does not exist.']); 
end

relation = tag_relation_table(table);

%--
% drop relation table if we are forcing creation
%--

if opt.force
	
	sql = ['DROP TABLE IF EXISTS ', relation, ';']; status = query(file, sql);
	
	if status == 1
		error(['Failed to drop ''', relation, ''' table.']);
	end

end

%--
% create tag relation table
%--

ropt = establish_relation; 

if strcmp(table, 'tag')
	table_id = [ropt.self, '_id'];
else
	table_id = [table, '_id'];
end

ropt.unique = {table_id, 'tag_id'};

if opt.user_id
	value.user_id = 1; ropt.unique{end + 1} = 'user_id';
else
	value = struct;
end

sql = establish_relation('', table, 'tag', value, ropt);

[status, result] = query(file, sql); done = status == 101;


%-----------------------
% CREATE_TAG_TABLE
%-----------------------

function [done, status, result] = create_tag_table(file, force)

%--
% set no force default, and drop table if we are forcing creation
%--

if nargin < 2
	force = 0;
end

%--
% create tag table
%--

hint = column_type_hints;

tag.id = hint.integer; tag.name = hint.string; tag.created_at = hint.timestamp; 

opt = create_table; opt.force = force; opt.unique = {'name'};

% TODO: add further tag string constraints to table and other improvements

[status, result] = query(file, create_table(tag, [], opt)); done = status == 101;




