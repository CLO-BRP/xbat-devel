function [tag, tagging] = delete_database_tags(store, tag, type, pretend)

% delete_database_tag - along with ssociated taggings
% ---------------------------------------------------
%
% [tag, tagging] = delete_database_tag(store, tag, type, pretend)
%
% Input:
% ------
%  store - database
%  tag - to delete
%  type - of taggable object (def: 'all', any tagged objects)
%  pretend - flag (def: true)
%
% Output:
% -------
%  tag - removed
%  tagging - relations removed
% 
% See also: get_database_tags, rename_database_tags, cleanup_database_tags

%--
% handle input
%--

% NOTE: consider changing to true when finished

if nargin < 4
	pretend = true;
end

if nargin < 3
	type = 'all';
end

if iscellstr(tag)
	% NOTE: tags and taggings will be globbed in the output
	
	[tag, tagging] = iteraten(mfilename, 2, store, tag, type, pretend); return;
end

%--
% get objects, then delete if needed
%--

[status, tag] = query(store, ['SELECT * FROM tag WHERE tag.name = ''', tag, '''']); %#ok<ASGLU>

if isempty(tag)
	tagging = []; 
	
	disp(['Tag ''', tag, ''' not found in database.']); return;
end

sql = ['SELECT * FROM tagging WHERE tagging.tag_id = ', int2str(tag.id)];

if ~strcmp(type, 'all')
	% NOTE: for convenience we set title-caps the type, this follows the conventional 
	
	sql = [sql, ' AND tagging.taggable_type = ''', strrep(title_caps(type), ' ', ''), ''''];
end

[status, tagging] = query(store, sql); %#ok<ASGLU>

if ~pretend
	% NOTE: in both calls below the table name is inferred from the variable name
	
	if isempty(tagging)
		disp(['No taggings corresponding to tag ''', tag, ''' found in database.']);
	else
		delete_database_objects(store, tagging);
	end
	
	delete_database_objects(store, tag);
end

if ~nargout
	disp(tag); disp(tagging); clear tag tagging;
end
	



