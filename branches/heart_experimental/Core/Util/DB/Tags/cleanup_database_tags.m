function duplicate = cleanup_database_tags(store, pretend)

% cleanup_database_tags - remove duplicates and update taggings
% -------------------------------------------------------------
%
% cleanup_database_tags(store, pretend)
%
% Input:
% ------
%  store - database
%  pretend - flag
%
% Output:
% -------
%  duplicate - tag groups, as identifier sets
%
% See also: rename_database_tags, get_database_tags

% TODO: eventually we also want to remove unused tags

%--
% handle input
%--

if nargin < 2
	pretend = true;
end

%--
% get current tags and collect to known tags
%--

[current, id] = get_database_tags(store);

% NOTE: the 'unique' output satisfies 'current = known(ixc)', the 'ixc' indices are indices into the 'known' array

[known, ignore, ixc] = unique(current); %#ok<ASGLU>

%--
% update taggings and delete current duplicates
%--

remove = []; duplicate = {};

for k = 1:numel(known)
	% NOTE: duplicate tags mean multiple hits in the current set indices
	
	hits = find(ixc == k);
	
	if numel(hits) < 2
		continue;
	end
	
	% NOTE: we have to dereference the indices to get the database identifiers!
	
	hits = id(hits); 
			
	duplicate{end + 1} = get_database_objects(store, 'tag', hits); %#ok<AGROW>
	
	% NOTE: with multiple hits, we point the taggings to the first tag and mark the rest for removal
	
	sql = [ ...
		'UPDATE tagging', ...
		' SET tag_id = ', int2str(hits(1)), ...
		' WHERE tag_id IN (', str_implode(hits(2:end), ',', @int2str), ')' ...
	];

	remove = [remove, hits(2:end)]; %#ok<AGROW>
	
	if pretend
		disp(' '); disp(sql);
	else
		query(store, sql);
	end
end

if ~isempty(remove)
	
	sql = ['DELETE FROM tag WHERE id IN (', str_implode(remove, ',', @int2str),')'];
	
	if pretend
		disp(' '); disp(sql);
	else
		query(store, sql);
	end
end

if ~nargout && pretend
	disp(' ');
	
	for k = 1:numel(duplicate)
		disp(['GROUP ', int2str(k)]);
	
		disp(duplicate{k});
	end
	
	clear duplicate;
end

