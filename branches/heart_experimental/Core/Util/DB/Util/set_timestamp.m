function obj = set_timestamp(name, obj, stamp)

% set_timestamp - field of struct array
% -------------------------------------
%
% obj = set_timestamp(name, obj, stamp)
%
% Input:
% ------
%  name - of timestamp field
%  obj - array
%  stamp - time
%
% Output:
% -------
%  obj - updated array

if nargin < 3
	stamp = get_current_timestamp; 
end

obj = set_existing_field(obj, name, stamp);