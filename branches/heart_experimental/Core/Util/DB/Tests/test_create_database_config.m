function test_create_database_config

% SETUP
%------------------

yaml = fullfile(fileparts(mfilename('fullpath')), 'test_database.yml');
db   = fullfile(fileparts(mfilename('fullpath')), 'test_database.sqlite3' );


% TEST
%------------------

% The following is essentially what load_database_config does

yaml = load_yaml(yaml);

config = create_database_config(yaml.test);

assert( isfield(config, 'file') );
assert( isfield(config, 'root') );



% TEST
%------------------

config = create_database_config( ... 
	'adapter', 'sqlite', ...
	'database', db ...
);

assert( isfield(config, 'file') );
assert( isfield(config, 'root') );


