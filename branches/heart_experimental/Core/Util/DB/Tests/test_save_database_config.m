function test_save_database_config

% SETUP
%------------------

root = fileparts(mfilename('fullpath'));

file = fullfile(root, 'test_database_saved.yml');

db   = fullfile(root, 'test_database.sqlite3' );

% cleanup the file to save into

if exist(file, 'file')
    delete(file)
end


% TEST
%------------------
% save a config created from create_database_config WITHOUT a 'name'

config = create_database_config( ... 
	'adapter', 'sqlite', ...
	'database', db ...
);

save_database_config(file, config, 'default');

config = load_database_config(file, 'default');

assert_equal( config.database, db );


% TEST
%------------------
% Save a config created from create_database_config WITH a 'name' and file
% already exists (from the test above). This tests the 'append'

config = create_database_config( ... 
	'adapter', 'sqlite', ...
	'database', db ...
);

save_database_config(file, config, 'test');

config = load_database_config(file);

assert( isfield(config, 'default') );

assert( isfield(config, 'test') );

assert_equal( config.test.adapter, 'sqlite' );


