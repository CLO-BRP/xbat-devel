function [config, yaml] = load_database_config(file, name)

% load_database_config - load database config from YAML file
% ----------------------------------------------------------
%
% [config, yaml] = load_database_config(file, name)
%
% Input:
% ------
%  file - describing configurations
%  name - of configuration
%
% Output:
% -------
%  config - description
%  yaml - file contents

%--
% load all configurations from file
%--

yaml = load_yaml(file);

% NOTE: we are not looking for a specific environment, return all

if nargin < 2

	config = struct; name = fieldnames(yaml);

	for i = 1:numel(name)
		config.(name{i}) = create_database_config(yaml.(name{i}));
	end

	return;

end

%--
% select particular configuration by name
%--

if ~isfield(yaml, name) 
	error(['Configuration ''', name, ''' not available in ''', file, '''.']);
end

config = create_database_config(yaml.(name));

