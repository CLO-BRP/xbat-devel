function [status, result] = establish_rating(store)

% establish_rating - add tables for rating
% ------------------------------------------
%
%     [sql, lines] = establish_rating
%
% [status, result] = establish_rating(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  status - of query request
%  result - of query

%--
% build prototype to create rating table
%--

hint = column_type_hints;

% RATING TABLE

rating.id = hint.i; 

rating.rateable_type = hint.s; 

rating.rateable_id = hint.i;

rating.rating = hint.i;

rating.user_id = hint.i;

rating.created_at = hint.ts; 

rating.modified_at = hint.ts;

%--
% OUTPUT OR EXECUTE QUERY
%--

if ~nargin
	store = [];
end

opt = create_table; opt.store = store; 

sql = create_table(rating, '', opt);

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql);
end
