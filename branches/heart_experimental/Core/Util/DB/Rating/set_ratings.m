function set_ratings(store, obj, rating, user, table)

% set_ratings - rate objects in database
% --------------------------------------
%
% set_ratings(store, objects, tags, table)
%
% Input:
% ------
%  store - database
%  objects - to rate
%  rate - value
%  user - rating
%  table - to rate
%
% See also: get_ratings

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% this calling form is used for adding rating values to database
%--

if isempty(obj)
    
% 	for k = 1:numel(rating)
% 		rate(k).score = rating{k};
% 	end
% 
% 	set_database_objects(store, rate);
	
	return;
end

%--
% typically reflect table from input name
%--

if nargin < 5
	table = inputname(2);
end

if isempty(table)
	error('Table to rate is not available explicitly or implicitly from input.');
end

%--
% check rating input
%--

% NOTE: here we replicate scalar input to match the number of objects

if numel(rating) == 1
	value = cellfree(rating); rating = cell(size(obj)); [rating{:}] = deal(value);
end

if numel(rating) ~= numel(obj)
	error('Ratings must be scalar or match the number of objects.');
end

% NOTE: this should be a basic operation in MATLAB

if isnumeric(rating)

	value = rating; rating = cell(size(rating));

	for k = 1:numel(rating)
		rating{k} = value(k);
	end
end

% NOTE: we have reduced the problem to the matching cell of ratings and objects problem

%---------------------------
% SETUP
%---------------------------

%--
% make sure all proposed ratings are in store
%--

% known = get_ratings(store);
% 
% append = setdiff(unique(iterate(@cellfree, rating)), [known.score]);
% 
% if ~isempty(append)
% 
% 	if ~iscell(append)
% 		append = {append};
% 	end
% 
% 	set_ratings(store, [], append); known = get_ratings(store);
% 
% end

%--
% get object identifiers
%--

% NOTE: we accept identified object array or identifier array input

if isstruct(obj)
	id = get_id_field(obj);
else
	id = obj;
end

%---------------------------
% SET RATINGS
%---------------------------

%--
% get current object ratings and compare with proposed to determine action
%--

% NOTE: the outputs here come wrapped in a cell to handle empty ratings

% TODO: consider changing this approach to use a 'nan' representation of empty?

[current, relation] = get_ratings(store, id, user, table);

% NOTE: get rating value identifiers 

% rate_id = get_rate_id(rating, known);

% NOTE: initialize empty 'rating' objects

[ignore, r] = query(store, 'SELECT * FROM rating LIMIT 0'); %#ok<ASGLU>

% r.rating = 1; r.rateable_type = 'string'; r.rateable_id = 1; r.user_id = 1; r = empty(r);

i = 1;

% NOTE: initialize relations to remove and relations to update arrays

remove = r; update = r;

for k = 1:numel(id)
	
	%--
	% nothing changed, nothing to do
	%--
	
	if isempty(current{k}) && isempty(rating{k})
		continue;
	end
	
	%--
	% rating was added 
	%--
	
	if isempty(current{k}) && ~isempty(rating{k})
		
		r(i).rateable_id = id(k); 
		
		% NOTE: this is the Rails polymorphic relation naming convention, the class not table name
		
		r(i).rateable_type = camelize(table);
		
		r(i).rating = rating{k}; 
		
		i = i + 1; continue;
	end
	
	%--
	% rating was deleted
	%--
	
	if ~isempty(current{k}) && isempty(rating{k})
		
		remove(end + 1) = relation{k}; %#ok<AGROW>
	end
	
	%--
	% check if rating was updated
	%--
	
	if current{k} ~= rating{k}
		% NOTE: we have to remove the cell wrapper here before assigment
				
		update(end + 1) = relation{k}; update(end).rating = rating{k}; %#ok<AGROW>
	end
end

%--
% add and update ratings
%--

if ~isempty(r)
	
	r = set_user_id(r, user.id); 
	
	set_database_objects(store, r, [], fieldnames(r), 'rating');
end

if ~isempty(update)
	
	update(1).modified_at = []; update = set_modified_at(update); 
	
	set_database_objects(store, update, [update.id], fieldnames(update), 'rating');
end

%--
% delete ratings
%--

if ~isempty(remove)
	
	delete_database_objects(store, remove, 'rating');
end


% TODO: various of these functions should be factored as little utility functions

%--------------------------------
% GET_RATE_ID
%--------------------------------

function id = get_rate_id(rating, known) %#ok<DEFNU>

id = cell(size(rating)); score = [known.score]; 

for k = 1:numel(rating)
	
	if isempty(rating{k})
		id{k} = []; continue;
	end
	
	selected = known(score == rating{k});
	
	if ~isempty(selected)
		id{k} = selected.id;
	end
end

