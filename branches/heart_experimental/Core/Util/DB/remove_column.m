function removed = remove_column(store, table, column)

% remove_column - from database table
% -----------------------------------
%
% removed = remove_column(store, table, column)
%
% Input:
% ------
%  store - database
%  table - name
%  column - name
%
% Output:
% -------
%  removed - column indicator
%
% See also: add_column, is_table_column 

if ~is_table_column(store, table, column)
	
	removed = false;
else
	query(store, ['ALTER TABLE ', table, ' DROP COLUMN ', column]);
	
	removed = true;
end