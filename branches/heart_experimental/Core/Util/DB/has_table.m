function [value, tables] = has_table(store, table)

% has_table - check for table existence
% -------------------------------------
% 
% [value, tables] = has_table(store, table)
%
% Input:
% ------
%  store - database
%  table - table 
%
% Output:
% -------
%  value - indicator
%  tables - all tables
%
% NOTE: this is an alias to 'database_has_table'

[value, tables] = database_has_table(store, table);