function move_pdf(source, destination)

% move_pdf - files
% ----------------
% 
% move_pdf(source, destination)
%
% Input:
% ------
%  source - directory
%  destination - directory
% 
% See also: file_md5, scan_dir_for
%
% NOTE: the 'source' directory will be scanned for '*.pdf' files
%
% NOTE: a 'PDF' directory will be created in the 'destination' directory 

%--
% handle input
%--

if nargin < 2 || isempty(destination)
	destination = uigetdir(pwd, 'Choose destination:'); if ~destination, return; end
end

if ~nargin || isempty(source)
	source = uigetdir(pwd, 'Choose source:'); if ~source, return; end
end

if ischar(source)
	files = scan_dir_for('pdf', source);
end

%--
% move files
%--

disp(' ');
disp('Moving files to archive destination ....'); start = clock;
disp(' ');

for k = 1:numel(files)
	try
		hash = file_md5(files{k});
	catch
		disp('HASH FAILED!'); display(files{k}); continue;
	end
	
	result = fullfile(destination, 'PDF', hash_location(hash), [hash, '.pdf']);
	
	if exist(result, 'file')
		disp(['DUPLICATE ''', files{k}]); continue;
	end
	
	[ignore, name] = fileparts(files{k});  %#ok<ASGLU>
	
	disp(['Moving ''', name, ' (', hash, ') ...']);
		
	try
		create_dir(fileparts(result));

		copyfile(files{k}, result);
	catch
		disp('MOVE FAILED!'); display(files{k});
	end
end

disp(['Done. (', sec_to_clock(etime(clock, start)), ')']);


%-------------------------
% GET_HASH_LOCATION
%-------------------------

function location = hash_location(value)

location = [value(1:2), filesep, value(3:4), filesep];


%----------------
% DISPLAY
%----------------

function display(file)

disp(['<a href="matlab:show_file(''', file, ''');">', file, '</a>']);



