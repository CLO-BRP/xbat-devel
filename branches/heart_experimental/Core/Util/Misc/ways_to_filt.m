% waystofilt.m "conv" vs "filter" vs "freq domain" vs "time domain"

% DATA
%--
h = [1 -1 2 -2 3 -3]; % impulse response h[k]
x = randn(1, 20); % input data x[k]

% CONV
%--
yconv = conv(h, x) %#ok<*NOPTS> % convolve x[k]*h[k]

% FILT
%--
yfilt = filter(h, 1, x) % filter x[k] with h[k]

% FREQ
%--
n = length(h) + length(x) - 1; % pad length for FFT
ffth = fft([h, zeros(1, n-length(h))]); % FFT of impulse response  =  H[n]
fftx = fft([x,  zeros(1, n-length(x))]); % FFT of input = X[n]
ffty = ffth.*fftx; % product of H[n] and X[n]
yfreq = real(ifft(ffty)) % IFFT of product gives y[k] z = [zeros(1, length(h)-1), x]; % initial state in filter = 0

% TIME
%--
% NOTE: we pad and slide the impulse response from end to end, this yields
% the same as the frequency domain computation

p = zeros(1, length(h) - 1); xp = [p, x, p];

for k = 1:length(x) + length(h) - 1 % time domain method
	ytime(k) = fliplr(h) * xp(k:k + length(h) - 1)'; %#ok<SAGROW> % iterates once for each x[k]
end 

ytime