function [rate, X] = video_test(n, callback)

%----------------------
% HANDLE INPUT
%----------------------

%--
% set callback
%--

if nargin < 2
	callback = [];
end

%--
% set number of frames
%--

if ~nargin
	n = 60;
end

%----------------------
% TEST VIDEO
%----------------------

%--
% create and configure video object
%--

obj = videoinput('winvideo', 1);

triggerconfig(obj, 'manual');

start(obj); t0 = clock;

%--
% capture and process frames
%--

% NOTE: in a real application we call this from a timer callback

par = figure; ax = axes('parent', par); 
	
X = get_frame(obj, callback); im = imagesc(X, 'parent', ax);

class(X), size(X)

for i = 2:n
	X = get_frame(obj, callback); set(im, 'cdata', X);
end

%--
% stop and delete video object
%--

rate = n / etime(clock, t0);

stop(obj); delete(obj);


%----------------------
% GET_FRAME
%----------------------

function X = get_frame(obj, callback)

%--
% get snapshot
%--

X = getsnapshot(obj); 

if isempty(callback)
	return;
end

%--
% process snapshot
%--

X = eval_callback(callback, X);
