function X = get_button_image(file, transparent)

% GET_BUTTON_IMAGE get image to place on button
%
% X = get_button_image(file, transparent)

%--
% set white to be transparent
%--

if nargin < 2
	transparent = ones(1, 3);
end

%--
% load image
%--

X = double(imread(file)) / 255;

%--
% make transparent color transparent
%--

X = rgb_replace(X, transparent, nan * ones(1, 3));


%--------------------
% RGB_REPLACE
%--------------------

function X = rgb_replace(X, c1, c2)

row = size(X, 1); col = size(X, 2);

X = rgb_vec(X);

% NOTE: this does not scale well for large images

for k = 1:(row * col)
	
	if all(X(k, :) == c1)
		X(k, :) = c2;
	end
	
end

X = rgb_reshape(X, row, col);