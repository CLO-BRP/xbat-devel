function str = duration_string(seconds)

% duration_string - get string description of time duration
% ---------------------------------------------------------
%
% str = duration_string(seconds)
%
% Input:
% ------
%  seconds - seconds
%
% Output:
% -------
%  str - duration string

str = 'really, really long';

times = get_times; mult = 1;

for k = 2:numel(times)
	
	mult = mult * times{k - 1}{2};
	
	next = mult * times{k}{2};
	
	if seconds < next
		str = sprintf(['%2.1f ' times{k - 1}{1}], seconds / mult); return;
	end
	
end


%-------------------
% GET_TIMES
%-------------------

function times = get_times()

times{1} = {'seconds', 1};

times{end + 1} = {'minutes', 60};

times{end + 1} = {'hours', 60};

times{end + 1} = {'days', 24};

times{end + 1} = {'weeks', 7};

times{end + 1} = {'months', 4};

times{end + 1} = {'years', 12};

times{end + 1} = {'decades', 10};

times{end + 1} = {'centuries', 10};

times{end + 1} = {'millenia', 10};


