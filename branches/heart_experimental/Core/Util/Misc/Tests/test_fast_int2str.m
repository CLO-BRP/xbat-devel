function test_fast_int2str(value)

N = 10^3

start = clock;
for k = 1:N
	str1 = int2str(value);
end
elapsed_builtin = etime(clock, start)

start = clock;
for k = 1:N
	str2 = fast_int2str(value);
end
elapsed_fast = etime(clock, start)

speed_up = elapsed_builtin / elapsed_fast

test = isequal(str1, str2)

if ~test
	str1, str2
end