function value = iscellcell(in)

% TODO: clealy we can generalize this

if isa(in, 'cell')
	res = cellfun('isclass', in, 'cell'); value = all(res(:));
else
	value = false;
end