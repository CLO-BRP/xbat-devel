function [root, lines] = dir_disp(root)

%--
% handle input
%--

lines = {};

if ~nargin || isempty(root)
	root = uigetdir(); 
end

if ~root
	return;
end 

%--
% traverse directory structure and create display
%--

content = scan_dir(root);

lines = {};

for k = 1:numel(content)
	part = get_dir_lines(content{k}); lines = {lines{:}, part{:}}; lines{end + 1} = ' ';
end

%--
% output to temp file and display
%--

if ~nargout
	
	out =  fullfile(tempdir, 'content.txt'); info = file_writelines(out, lines);

	if info.bytes
		winopen(out);
	end
	
end


%----------------------
% GET_DIR_LINES
%----------------------

function lines = get_dir_lines(root)

content = no_dot_dir(root, -1);

lines = {root}; lines{end + 1} = str_line(root); lines = {lines{:}, content.name}; 


