function h = match_tag_prefix(g, str, fast)

% match_tag_prefix - select handles by matching tag prefix
% --------------------------------------------------------
%
% h = match_tag_prefix(g, str)
%
% Input:
% ------
%  g - input handles
%  str - prefix string
%
% Output:
% -------
%  h - selected handles

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% set default check for handle input and check if needed
%--

if nargin < 3
	fast = 0;
end

if ~fast
	if any(~ishandle(g))
		error('First input to this function must consist only of handles.');
	end
end

%-------------------------
% GET AND MATCH TAGS
%-------------------------

%--
% get object tags
%--

N = numel(g);

if N == 1
	tag = {get(g,'tag')}; % NOTE: pack single figure tag into cell array
else
	tag = get(g,'tag');
end

%--
% match tag prefix
%--

% TODO: this is an unusual pattern and we may simply use 'strncmp'

% NOTE: we get length of prefix and check up to there

n = length(str);

j = 0; h = zeros(size(g));

for k = 1:N
	
	% NOTE: check length of tag before comparing for speed
	
	if (length(tag{k}) >= n) && strncmp(tag{k}, str, n)
		j = j + 1; h(j) = g(k);
	end

end	

% NOTE: trim match array, output only selected handles

h = h(1:j);

