function ar = get_aspect_ratio(par)

% get_aspect_ratio - get aspect ratio of figure or screen
% -------------------------------------------------------
%
% ar = get_aspect_ratio(par)
%
% Input:
% ------
%  par - figure handle or '0' for screen
%
% Output:
% -------
%  ar - aspect ratio

%--
% handle multiple inputs
%--

if numel(par) > 1
	ar = iterate(mfilename, par); return;
end

%--
% get size or position
%--

if par == 0
	pos = get(0, 'screensize');
else
	pos = get(par, 'position');
end

%--
% compute aspect ratio
%--

ar = pos(3) / pos(4);