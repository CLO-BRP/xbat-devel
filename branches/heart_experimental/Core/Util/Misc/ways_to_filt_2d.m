% Harold Figueroa, hkf1@cornell.edu

%---------------------
% SETUP
%---------------------

% NOTE: a non-symmetric impulse response reveals orientation problems

h = magic(3); x = randn(5); 

%---------------------
% COMPUTE
%---------------------

% CONVOLUTION
%---------------------
yconv = conv2(h, x); 

% NOTE: the following lines select the 'valid' section of the result

p = 0.5 * (size(h) - 1);

yconv = yconv(p(1) + 1:end - p(1), p(2) + 1:end - p(2)) %#ok<*NOPTS>


% CORRELATION
%---------------------
ycorr = xcorr2(x, fliplr(flipud(h))); 

ycorr = ycorr(p(1) + 1:end - p(1), p(2) + 1:end - p(2))


% FILTER
%---------------------
yfilt = filter2(fliplr(flipud(h)), x) % filter x[k] with h[k]


% IMFILTER
%---------------------
yimf = imfilter(x, h, 0, 'conv') % NOTE: 'imfilter' can flip the impulse response for us


% FREQUENCY DOMAIN
%---------------------
n = size(x) + size(h) - 1;

ffth = fft2(h, n(1), n(2)); fftx = fft2(x, n(1), n(2));

ffty = ffth .* fftx; 

yfreq = real(ifft2(ffty));

yfreq = yfreq(p(1) + 1:end - p(1), p(2) + 1:end - p(2))


% TIME DOMAIN
%---------------------

% NOTE: 'n' is the padded size computed above, 'p' is the padding

xp = zeros(n); xp(p(1) + 1:end - p(1), p(2) + 1:end - p(2)) = x;

for k = p(1) + 1:n(1) - p(1)
	for l = p(2) + 1:n(2) - p(2)
		block = xp((k - p(1)):(k + p(1)), (l - p(2)):(l + p(2))) .* fliplr(flipud(h));
		
		ytime(k - p(1), l - p(2)) = sum(block(:));
	end
end

ytime