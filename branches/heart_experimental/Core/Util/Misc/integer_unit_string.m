function str = integer_unit_string(value, unit, large, pad)

%--
% handle input
%--

if nargin < 4
	pad = ' ';
end

if nargin < 3
	large = [];
end

%--
% convert value to string, possibly with padding
%--

str = int_to_str(value, large, pad);

%--
% add unit information if needed and possible
%--

if nargin < 2 || isempty(unit)
	return;
end

if (abs(value) > 1) || (value == 0)
	unit = string_plural(unit);
end

str = [str, ' ', unit];

