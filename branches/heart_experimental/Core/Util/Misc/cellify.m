function c = cellify(c)

% cellify - ensure that input is a cell array
% -------------------------------------------
% 
% c = cellify(c)
%
% Input:
% ------
%  c - any matlab variable
%
% Output:
% -------
%  c - a cell array (or the input if it was already)

if iscell(c)
    return;
end

c = {c};