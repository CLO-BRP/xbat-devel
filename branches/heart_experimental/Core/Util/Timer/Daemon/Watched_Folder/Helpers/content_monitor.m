function [change, current] = content_monitor(source, callback, opt)

% content_monitor - useful when watching folders
% ----------------------------------------------
%
% [change, current] = content_monitor(source)
%
% Input:
% ------
%  root - directory
%
% Output:
% -------
%  change - in content struct
%  current - content
%
% See also: watched_folder

%--
% handle input
%--

if nargin < 3
	opt.trace = false;
	
	if ~nargin
		change = opt; return;
	end
end

if ~nargin
	source = pwd;
end

%--
% get previous and current content
%--

store = content_monitor_store;

source = get_source(store, source);

% NOTE: first retrieve previously observed content, 'set_content' updates this information

% TODO: we only have to update database when we observe change, 'set' would follow change analysis

[status, previous] = query(store, ['SELECT * FROM content WHERE source_id = ', int2str(source.id), ';']); %#ok<ASGLU>

current = set_content(store, source.location);

% db_disp monitoring; disp source; disp(source); disp previous; disp(previous); disp current; disp(current);

%--
% compare content to compile change
%--

% NOTE: added files are in current content but not previous

[ignore, ix] = setdiff({current.name}, {previous.name}); %#ok<ASGLU>

change.added = current(ix);

% NOTE: deleted files are in previous but not in current content

[ignore, ix] = setdiff({previous.name}, {current.name}); %#ok<ASGLU>

change.deleted = previous(ix);

% NOTE: modified files are in both previous and current content with different modification dates

[ignore, ixa, ixb] = intersect({current.name}, {previous.name}); %#ok<ASGLU>

modified = [current(ixa).datenum] ~= [previous(ixb).datenum];

change.modified = current(ixa(modified));

%--
% trace change
%--

disp(' '); disp(['content-monitor: ', source.location]);

if isempty(change.added) && isempty(change.deleted) && isempty(change.modified)
	
	disp('No change in content.'); return;
end

action = {'added', 'deleted', 'modified'};

for j = 1:numel(action)
	
	for k = 1:numel(change.(action{j}))
		
		disp([title_caps(action{j}), ' ', change.(action{j})(k).name]);
	end
end