function store = content_monitor_store

% content_monitor_store - connection
% ----------------------------------
%
% store = content_monitor_store
%
% Output:
% -------
%  store - for content monitor
% 
% See also: content_monitor, watched_folder

% TODO: allow for this to be a set and get helper for a general database connection

%--
% compute store file 
%--

here = fileparts(mfilename('fullpath')); 

store = fullfile(here, 'content_monitor.sqlite3');

%--
% establish store if it does not exist
%--

if exist(store, 'file')
	return;
end

% TODO: add 'content_hash' to files, then 'get_content' must be updated to perform the computation

% NOTE: that adding the 'content_hash' computation may require developing a delegation mechanism

% TODO: consider any role for GUID columns, and consider the possibility of maintaining content history

content = no_dot_dir(here); content(1).id = 1; content(1).source_id = 1;

query(store, create_table(content));

source = struct('id', 1, 'location', 'location');

query(store, create_table(source));
	
