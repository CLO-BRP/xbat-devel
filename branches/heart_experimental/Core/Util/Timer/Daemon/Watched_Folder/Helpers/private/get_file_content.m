function content = get_file_content(source)

% get_file_content - from system directory once source is in store
% ----------------------------------------------------------------
%
% content = get_file_content(source)
%
% Input:
% ------
%  source - in database
%
% Output:
% -------
%  content - for database

% NOTE: we first get content information from filesystem, then we add source foreign key

content = no_dot_dir(source.location);

for k = 1:numel(content)
	% NOTE: we convert the logical 'isdir' to a double so that 'm2xml' will leave it alone
	
	content(k).isdir = ternary(content(k).isdir, 1, 0);
	
	content(k).source_id = source.id;
end