function content = set_content(store, source)

% set_content - for source in content monitor database
% ----------------------------------------------------
%
% content = set_content(store, source)
%
% Input:
% ------
%  store - for content monitor
%  source - location

%--
% make sure that source is in database
%--

[source, new] = get_source(store, source);

%--
% set content for source
%--

% NOTE: clear content for existing sources

if ~new
	query(store, ['DELETE FROM content WHERE source_id = ', int2str(source.id)]);
end

% NOTE: get and store current content

content = get_file_content(source);

content = set_database_objects(store, content);