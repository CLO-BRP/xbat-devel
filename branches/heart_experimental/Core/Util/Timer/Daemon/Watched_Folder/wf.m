function wf(op, daemon)

% wf - watched folder command 
% ---------------------------
%
% wf add daemon, wf append daemon
%
% wf rm, wf del, wf delete
%
% Input:
% ------
%  daemon - name (def: 'watched_folder')
%
% NOTE: add or delete current directory from a watched folder daemon

%--
% set default daemon
%--

if nargin < 2
	daemon = 'watched_folder';
end

%--
% perform operation, a set on the daemon
%--

switch op
	
	case {'add', 'append', 'rm', 'del', 'delete'}
		
		set_watched_folder(daemon, op, pwd);
		
end