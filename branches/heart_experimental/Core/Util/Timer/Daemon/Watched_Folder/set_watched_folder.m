function daemon = set_watched_folder(daemon, varargin)

% set_watched_folder - daemon properties
% --------------------------------------
%
% daemon = set_watched_folder(daemon, field, value, ... )
%
% Input:
% ------
%  daemon - name or timer
%  field - to set
%  value - for field
%
% Output:
%  daemon - updated

%--
% get daemon from name
%--

if ischar(daemon)
	daemon = timerfind('name', daemon);
end

%--
% parse field value
%--

if isempty(varargin)
	return;
end 

[fields, values] = get_field_value(varargin);

%--
% set daemon property, which may be timer property
%--

for k = 1:numel(fields)
	
	field = fields{k}; value = values{k};
	
	switch field
		
		case 'name'
			set(daemon, 'name', value);
			
		case 'list'
			callback = get(daemon, 'timerfcn'); callback{2} = value; set(daemon, 'timerfcn', callback);
			
		% NOTE: here we can add or remove directories from thir daemon's watched list
		
		case {'add', 'append', 'rm', 'del', 'delete'}
			
			callback = get(daemon, 'timerfcn'); list = callback{2};
			
			% NOTE: this relies on the list of allowed 'add' and 'delete' commands
			
			if field(1) == 'a'
				list = union(list, value);
			else
				list = setdiff(list, value);
			end
			
			callback{2} = list; set(daemon, 'timerfcn', callback);
			
		case 'callback'
			callback = get(daemon, 'timerfcn'); callback{3} = value; set(daemon, 'timerfcn', callback);
			
		% NOTE: this is misnamed, however it should be changed starting with 'create_timer'
			
		case 'rate'
			set(daemon, 'period', value);
			
		% NOTE: when we get some other request pass it to the timer object 'get'
			
		otherwise
			set(daemon, field, value);
			
	end
end