function result = test_hist_2d(n)

if ~nargin
	n = 512;
end

X = lut_range(randn(n)); Y = lut_range(randn(n));  

types = setdiff(get_cast_types, {'char', 'logical'});

for k = 1:numel(types)
	cast = str2func(types{k});
	
	if strcmp(types{k}, 'int8')
		% NOTE: in this case we have 8-bits for positive and negative
		
		Z = cast(X - 128); W = cast(Y - 128);
	else
		Z = cast(X); W = cast(Y);
	end
	
	par = fig; set(par, 'name', types{k});
	
	hist_2d(Z, W);
end

result = struct;