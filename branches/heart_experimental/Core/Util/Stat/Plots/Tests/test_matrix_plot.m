function [par, ax] = test_matrix_plot

fields = {'cat', 'dog', 'mouse'};

[par, ax] = matrix_plot(fields)

disp(get(ax, 'tag'))