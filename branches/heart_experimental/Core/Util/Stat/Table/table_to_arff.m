function out = table_to_arff(table, file)

% table_to_arff - export table to arff
% ------------------------------------
%
% lines = table_to_arff(table)
%
%  info = table_to_arff(table, file)
%
% Input:
% ------
%  table - to export
%  file - to write
% 
% Output:
% -------
%  lines - to write
%  info - for file

%--
% handle input
%--

% NOTE: we produce file lines as default

if nargin < 2
	file = '';
end

%--
% generate arff text representation of table
%--

% TODO: allocate lines cell array, this can be done

% TODO: make sure that relation and attribute names are valid, as well as any class values

lines = {}; lines{end + 1} = ['@relation ', table.name];

lines{end + 1} = ' '; 

for k = 1:numel(table.column.names)
	lines{end + 1} = ['@attribute ', table.column.names{k}, ' ', table.column.types{k}];
end

lines{end + 1} = ' ';

lines{end + 1} = '@data';

for k = 1:size(table.data, 1)
	lines{end + 1} = str_implode(table.data(k, :), ', ', @value_str);
end

%--
% write lines to file if needed
%--

if isempty(file)
	out = lines; 
	
	if ~nargout
		disp(' '); iterate(@disp, lines); disp(' '); clear out;
	end
	
	return;
end

out = file_writelines(file, lines);


%--------------------
% VALUE_STR
%--------------------

function value = value_str(value)

if ischar(value)
	return;
end

value = num2str(value, 16);
