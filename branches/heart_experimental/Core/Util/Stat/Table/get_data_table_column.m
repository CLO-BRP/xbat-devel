function value = get_data_table_column(table, name)

% get_data_table_column - by name
% -------------------------------
%
% value = get_data_table_column(table, name)
%
% Input:
% ------
%  table - source
%  name - of column
% 
% Output:
% -------
%  value - of column

[match, index] = string_is_member(name, table.column.names);

value = table.data(:, index);