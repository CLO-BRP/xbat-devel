function y = fast_median(X, Z)

% fast_median - fast median value computation
% -------------------------------------------
% 
% y = fast_median(X, Z)
%
% Input:
% ------
%  X - input data
%  Z - select indicator (def: [], consider all data)
%
% Output:
% -------
%  y - median

%----------------------
% HANDLE INPUT
%----------------------

%--
% set mask and apply mask if needed
%--

if nargin < 2
	Z = [];
end

% NOTE: the output of the logical selection is a column vector

if ~isempty(Z)
	X = X(logical(Z));
end

%----------------------
% COMPUTE MEDIAN
%----------------------

%--
% consider special cases
%--

% NOTE: this duplicates the functionality of the built-in 'median'

if isempty(X) || any(isnan(X(:)))
	y = nan; return;
end

%--
% compute using mex
%--

y = fast_median_(X);