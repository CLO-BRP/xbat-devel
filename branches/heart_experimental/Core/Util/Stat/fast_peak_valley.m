function [ix, h, w] = fast_peak_valley(x, type, stable)

% fast_peak_valley - fast extrema computation
% -------------------------------------------
%
% [ix, h, w] = fast_peak_valley(x, type)
%
% Input:
% ------
%  x - input sequence
%  type - type of extrema (def: 0)
%
% Output:
% -------
%  ix - extrema indices
%  h - extrema heights or depths
%  w - extrema widths

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 943 $
% $Date: 2005-04-15 18:03:43 -0400 (Fri, 15 Apr 2005) $
%--------------------------------

% TODO: align input and output for vectors

% TODO: generalize code to handle matrices and take dimension input

% TODO: include opt as part of the low level computation

% NOTE: the current approach used to separate peaks is not intuitive

%----------------------
% HANDLE INPUT
%----------------------

%--
% handle special input sequence cases
%--

% NOTE: these trivial cases are not properly handled by the MEX

if isempty(x) || (numel(x) < 3)
	ix = []; h = []; w = []; return;
end

if nargin < 3
	stable = false;
end

%--
% set output option
%--

if (nargin < 2) || isempty(type)
	type = 0;
end

%--
% set function to handle computation
%--

% NOTE: this test passed

% [ix, h ,w] = fast_peak_valley_(x), [ix2, h2, w2] = stable_peak_valley(x)
% 
% result = [isequal(ix, ix2), isequal(h, h2), isequal(w, w2)]

if stable
	fun = @stable_peak_valley;
else
	fun = @fast_peak_valley_;
end

%----------------------
% COMPUTE
%----------------------

%--
% compute peaks and valleys and select one class for output if needed
%--

switch nargout
	
	case 1	
		ix = fun(x);
		
		if type
			if type > 0
				ix = ix(ix > 0);
			else
				ix = -ix(ix < 0);
			end
		end
		
	case 2	
		[ix, h] = fun(x);
		
		if type		
			if type > 0
				tmp = find(ix > 0); ix = ix(tmp); h = h(:, tmp);
			else
				tmp = find(ix < 0); ix = -ix(tmp); h = h(:, tmp);
			end
		end
		
	case 3	
		[ix, h, w] = fun(x);
		
		if type
			if type > 0
				tmp = find(ix > 0); ix = ix(tmp); h = h(:, tmp); w = w(:, tmp);
			else
				tmp = find(ix < 0); ix = -ix(tmp); h = h(:, tmp); w = w(:, tmp);
			end
		end
end


%--------------------------------
% STABLE_PEAK_VALLEY
%--------------------------------

function [ix, h, w] = stable_peak_valley(x)

% TODO: this is a compatibility layer, it is meant to replace the MEX, a MATLAB implementation of the parent function could be more efficient

%--
% compute peak and valley locations
%--

% NOTE: the offset of one compensates for the shorter vector we are testing on

peak = find(x(2:end - 1) > max(x(1:end - 2), x(3:end))) + 1; peak(:, 2) = 1; peak = peak';

valley = find(x(2:end - 1) < min(x(1:end - 2), x(3:end))) + 1; valley(:, 2) = -1; valley = valley';

%--
% merge peak and valley indices while keeping type information
%--

% NOTE: in the end we store the type information in the sign of the index

try
	all = [peak, valley];
catch
	sp = size(peak), sv = size(valley)
	all = [peak; valley];
end

[ix, p] = sort(all(1, :)); type = all(2, p); ix = ix .* type;

%--
% compute height and width if needed
%--

% NOTE: we only take advantage of the simplest exit condition, otherwise we compute everything

if nargout < 2
	return;
end

aix = abs(ix);

for k = 1:numel(ix)
		
	if k == 1
		h(:, k) = x(aix(k)) - [x(1), x(aix(k + 1))]'; w(:, k) = [aix(k) - 1, aix(k + 1) - aix(k)]';
	elseif k == numel(ix)
		h(:, k) = x(aix(k)) - [x(aix(k - 1)), x(end)]'; w(:, k) = [aix(k) - aix(k - 1), numel(x) - aix(k)]';
	else 
		h(:, k) = x(aix(k)) - [x(aix(k - 1)), x(aix(k + 1))]'; w(:, k) = [aix(k) - aix(k - 1), aix(k + 1) - aix(k)]';
	end

	if ix(k) < 0
		h(:, k) = -1 * h(:, k);
	end

end

