/*
 * This sample implements 256-bin histogram calculation
 * of arbitrary-sized 8-bit data array
 */

#include "mex.h"

#include <stdlib.h>

#include "cuda_runtime.h"

#include "cuda_histogram256.h"

//----------------------------------------------
// MEX FUNCTION
//----------------------------------------------

// histogram256 - 1d histogram computation
// ---------------------------------------------
//
// H = histogram256(X,b)
//
// Input:
// ------
//  X - input data
//  b - value bounds
//
// Output:
// -------
//  H - bin counts

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    unsigned char *h_Data; unsigned int *h_Histogram;
	
	unsigned char *d_Data; unsigned int *d_Histogram;
	
	unsigned int N;
 
	double *bound, a, b;

	// static const unsigned int PARTIAL_COUNT = 240;
	
	// TODO: this replaced a former 'define' constant, on our way to making this part of the input
	
	const int BIN_COUNT = 256;

    //--
	// handle input
	//--

	// get data pointer and length
	
    N = mxGetM(prhs[0]) * mxGetN(prhs[0]);
	
    h_Data = (unsigned char *) mxGetPr(prhs[0]);
	
	// get value bounds

	bound = mxGetPr(prhs[1]); a = *bound; b = *(bound + 1);

    //--
	// allocate output
	//--
	
    h_Histogram = (unsigned int  *) mxGetPr(plhs[0] = mxCreateNumericMatrix(1, BIN_COUNT, mxUINT32_CLASS, mxREAL));

	//--
	// dispatch computation based on type
	//--
	
	switch (mxGetClassID(prhs[0])) {
		
		// BEGIN-EXPAND-TYPES
		
		case MEX_TYPE_CLASS:
			//--
			// allocate device memory and copy data from host
			//--
			
			cudaMalloc((void **) &d_Data, N * sizeof(MEX_TYPE));
			
			cudaMalloc((void **) &d_Histogram, BIN_COUNT * sizeof(unsigned int));

			initHistogram256();
			// cudaMalloc((void **) &d_PartialHistograms, PARTIAL_COUNT * BIN_COUNT * sizeof(unsigned int));
			
			cudaMemcpy(d_Data, h_Data, N * sizeof(MEX_TYPE), cudaMemcpyHostToDevice);
			
			//--
			// compute on device
			//--
			
			histogram256_MEX_TYPE_NAME(d_Histogram, (MEX_TYPE *) d_Data, N, a, b);
			
			//--
			// copy results from device to host and free device memory
			//--
			
			cudaMemcpy(h_Histogram, d_Histogram, BIN_COUNT * sizeof(unsigned int), cudaMemcpyDeviceToHost);
			
			closeHistogram256();
			// cudaFree(d_PartialHistograms);
			
			cudaFree(d_Data); cudaFree(d_Histogram);
			
			break;
		// END-EXPAND-TYPES	
	}
}
