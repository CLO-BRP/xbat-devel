function hist_2d_view(H, c, v, opt)

%--
% handle input
%--

if nargin < 4
	% TODO: add 'flag' and 'inputname' input to the options, use it from 'hist_2d'
	
	opt = 1;
end

%--
% normalize and scale histogram
%--

N = sum(H(:)); w = (c.x(2) - c.x(1)) * (c.y(2) - c.y(1));

G = H / (w * N);

% logarithmic scaling

G = log10(G); Z = isfinite(G);

m = double(fast_min_max(G, Z));

%--
% display according to options
%--

switch opt
	
	%--
	% log scaled normalized histogram
	%--
	
	case 1	

		% display image
		
		image(G, ...
			'cdatamapping', 'scaled', ...
			'xdata', c.x([1, end]), ...
			'ydata', c.y([1, end]) ...
		);
		
		set(gca, ...
			'clim', m ...
		);
		
		axis('xy');
		
		% set aspect ratio
		
		r = (c.x(2) - c.x(1)) / (c.y(2) - c.y(1)); r = min(r, 1/r);
		
		if r > 0.33
			axis('image');
		end
		
		% set colormap
		
		colormap(hot);
		
		% NOTE: we add quartile contours here, first we compute the values
		
		value = hist_2d_level(H);
		
		hold on;
		
		[ignore, handle] = contour(c.x, c.y, median_filter(H/N, se_ball(3)), value); %#ok<ASGLU>
		
		set(handle, ...
			'edgecolor', ones(1, 3), ...
			'linewidth', 2, ...
			'linestyle', '--' ...
		);
		
		%--
		% label axes
		%--
		
		return;
		
		% planes of multiple-plane image
		
		if flag	
			if ~isempty(inputname(1))
				N1 = [inputname(1) '(:,:,' num2str(p(1)) ')'];
				
				N2 = [inputname(1) '(:,:,' num2str(p(2)) ')'];
			else
				N1 = ['[inputname(1)](:,:,' num2str(p(1)) ')'];
				
				N2 = ['[inputname(1)](:,:,' num2str(p(2)) ')'];
			end
			
			xlabel_edit(N1); ylabel_edit(N2);
			
			title_edit(['Joint Histogram of ' N1 ' and ' N2]);
		else
			
			if ~isempty(inputname(1))
				N1 = inputname(1);
			else
				N1 = '[inputname(1)]';
			end
			
			xlabel_edit(N1);
			
			if ~isempty(inputname(2))
				N2 = inputname(2);
			else
				N2 = '[inputname(2)]';
			end
			
			ylabel_edit(N2);
						
			title_edit(['Joint Histogram of ' N1 ' and ' N2]);
		end
		
	%--
	% histogram and marginals
	%--
		
	case 2
		
		% compute marginals and product distribution
		
		wx = (c.x(2) - c.x(1));
		Gx = sum(H,1) / (wx * N);
		
		wy = (c.y(2) - c.y(1));
		Gy = sum(H,2) / (wy * N);
		
		Gxy = Gy*Gx;
		
		Gxy = log10(Gxy);
		Zxy = isfinite(Gxy);
		mxy = double(fast_min_max(Gxy,Zxy));
		
		%--
		% JOINT HISTOGRAM
		%--
		
		%--
		% display according to data type
		%--
		
		subplot(2,2,2);
		
		if (isa(X,'double'))
			
			% display image
			
			image(G,'CDataMapping','scaled','XData',b(1,:),'YData',b(2,:));
			set(gca,'Clim',m);
			axis('xy');
			
			% set aspect ratio
			
			% 				r = (b.x(2) - b.x(1)) / (b.y(2) - b.y(1));
			% 				r = min(r,1/r);
			%
			% 				if (r > 0.33)
			% 					axis('image');
			% 				end
			
			% add identity
			
		elseif (isa(X,'uint8'))
			
			% display image
			
			image(G,'CDataMapping','scaled','XData',[0 255],'YData',[0 255]);
			set(gca,'Clim',m);
			axis('xy');
			
			% set aspect ratio
			
			axis('image');
			
			% add identity
			
			hold on;
			plot([0,255],[0,255],'w:');
			
		end
		
		% set colormap
		
		colormap(hot);
		
		% add quartile contours
		
		hold on;
		
		[V,q] = hist_2d_level(H);
		
		[tmp,h] = contour(c.x,c.y,median_filter(H/N,se_ball(3)),V);
		set(h,'EdgeColor',ones(1,3),'LineStyle','--');
		
		%--
		% label axes
		%--
		
		% planes of multiple-plane image
		
		if (flag)
			
			if (~isempty(inputname(1)))
				N1 = [inputname(1) '(:,:,' num2str(p(1)) ')'];
				N2 = [inputname(1) '(:,:,' num2str(p(2)) ')'];
				T = ['Joint Histogram of ' N1 ' and ' N2];
				title_edit(T);
				xlabel_edit(N1);
				ylabel_edit(N2);
			else
				N1 = ['[inputname(1)](:,:,' num2str(p(1)) ')'];
				N2 = ['[inputname(1)](:,:,' num2str(p(2)) ')'];
				T = ['Joint Histogram of ' N1 ' and ' N2];
				title_edit(T);
				xlabel_edit(N1);
				ylabel_edit(N2);
			end
			
			% separate images
			
		else
			
			if (~isempty(inputname(1)))
				xlabel_edit(inputname(1));
				N1 = inputname(1);
			else
				xlabel_edit('[inputname(1)]');
				N1 = '[inputname(1)]';
			end
			
			if (~isempty(inputname(2)))
				ylabel_edit(inputname(2));
				N2 = inputname(2);
			else
				ylabel_edit('[inputname(2)]');
				N2 = '[inputname(2)]';
			end
			
			T = ['Joint Histogram of ' N1 ' and ' N2];
			title_edit(T);
			
		end
		
		% titled colorbar
		
		% 			colorbar;
		
		%--
		% MARGINALS
		%--
		
		% normalized histogram
		
		subplot(2,2,4);
		
		plot(c.x,Gx,'k-');
		if (length(c.x) < 100)
			hold on;
			plot(c.x,Gx,'ko');
		end
		
		ax = axis;
		axis([v.x(1) v.x(end) ax(3:4)]);
		
		grid on;
		
		axes_scale_bdfun(gca,'y');
		
		%--
		% label axes
		%--
		
		% planes of multiple-plane image
		
		if (flag)
			
			if (~isempty(inputname(1)))
				N1 = [inputname(1) '(:,:,' num2str(p(1)) ')'];
				xlabel_edit(['Histogram of ' N1]);
			else
				N1 = ['[inputname(1)](:,:,' num2str(p(1)) ')'];
				xlabel_edit(['Histogram of ' N1]);
			end
			
			% separate images
			
		else
			
			if (~isempty(inputname(1)))
				xlabel_edit(['Histogram of ' inputname(1)]);
			else
				xlabel_edit('Histogram of [inputname(1)]');
			end
			
		end
		
		% normalized histogram
		
		subplot(2,2,1);
		
		plot(Gy,c.y,'k-');
		if (length(c.y) < 100)
			hold on;
			plot(Gy,c.y,'ko');
		end
		
		ax = axis;
		axis([ax(1:2) v.y(1) v.y(end)]);
		set(gca,'XDir','reverse');
		% 		set(gca,'YAxisLocation','right');
		
		grid on;
		
		axes_scale_bdfun(gca,'x');
		
		%--
		% label axes
		%--
		
		% planes of multiple-plane image
		
		if (flag)
			
			if (~isempty(inputname(1)))
				N2 = [inputname(1) '(:,:,' num2str(p(2)) ')'];
				ylabel_edit(['Histogram of ' N2]);
			else
				N2 = ['[inputname(1)](:,:,' num2str(p(2)) ')'];
				ylabel_edit(['Histogram of ' N2]);
			end
			
			% separate images
			
		else
			
			if (~isempty(inputname(2)))
				ylabel_edit(['Histogram of ' inputname(2)]);
			else
				ylabel_edit('Histogram of [inputname(2)]');
			end
			
		end
		
		% PRODUCT HISTOGRAM
		
		%--
		% display according to data type
		%--
		
		subplot(2,2,3);
		
		if (isa(X,'double'))
			
			% display image
			
			image(Gxy,'CDataMapping','scaled','XData',b(1,:),'YData',b(2,:));
			set(gca,'Clim',mxy);
			axis('xy');
			
			% set aspect ratio
			
			% 				r = (b.x(2) - b.x(1)) / (b.y(2) - b.y(1));
			% 				r = min(r,1/r);
			%
			% 				if (r > 0.33)
			% 					axis('image');
			% 				end
			
			% add identity
			
		elseif (isa(X,'uint8'))
			
			% display image
			
			image(Gxy,'CDataMapping','scaled','XData',[0 255],'YData',[0 255]);
			set(gca,'Clim',m);
			axis('xy');
			
			% set aspect ratio
			
			axis('image');
			
			% add identity
			
			hold on;
			
			plot([0,255],[0,255],'w:');
			
		end
		
		% set colormap
		
		colormap(hot);
		
		% add quartile contours
		
		% 		  	hold on;
		%
		% 			[V,q] = hist_2d_level(Gxy);
		%
		% 		 	[tmp,h] = contour(c.x,c.y,median_filter(Gxy/N,se_ball(3)),V);
		% 			set(h,'EdgeColor',ones(1,3),'LineStyle','--');
		
		%--
		% label axes
		%--
		
		if (flag)
			
			if (~isempty(inputname(1)))
				title_edit('Product Distribution');
				xlabel_edit([inputname(1) '(:,:,' num2str(p(1)) ')']);
				ylabel_edit([inputname(1) '(:,:,' num2str(p(2)) ')']);
			else
				title_edit('Product Distribution');
				xlabel_edit(['[inputname(1)](:,:,' num2str(p(1)) ')']);
				ylabel_edit(['[inputname(1)](:,:,' num2str(p(2)) ')']);
			end
			
			if (~isempty(inputname(1)))
				N1 = [inputname(1) '(:,:,' num2str(p(1)) ')'];
				N2 = [inputname(1) '(:,:,' num2str(p(2)) ')'];
				T = ['Product Histogram of ' N1 ' and ' N2];
				title_edit(T);
				xlabel_edit(N1);
				ylabel_edit(N2);
			else
				N1 = ['[inputname(1)](:,:,' num2str(p(1)) ')'];
				N2 = ['[inputname(1)](:,:,' num2str(p(2)) ')'];
				T = ['Product Histogram of ' N1 ' and ' N2];
				title_edit(T);
				xlabel_edit(N1);
				ylabel_edit(N2);
			end
			
			% separate images
			
		else
			
			if (~isempty(inputname(1)))
				xlabel_edit(inputname(1));
				N1 = inputname(1);
			else
				xlabel_edit('[inputname(1)]');
				N1 = '[inputname(1)]';
			end
			
			if (~isempty(inputname(2)))
				ylabel_edit(inputname(2));
				N2 = inputname(2);
			else
				ylabel_edit('[inputname(2)]');
				N2 = '[inputname(2)]';
			end
			
			T = ['Product Histogram of ' N1 ' and ' N2];
			title_edit(T);
			
		end
		
		I = hist_mutual(H);
end
	