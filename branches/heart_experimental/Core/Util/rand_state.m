function [value, state] = rand_state(n, init, method)

% rand_state - call rand with set and get state
% ---------------------------------------------
%
% [value, state] = rand_state(n, init, method)
%
% Input:
% ------
%  n - values to generate
%  init - initial state
%  method - generation method
%
% Output:
% -------
%  value - values
%  state - states before generating each value

% TODO: create 'randn_state' and 'randn_state_blocks', reconsider last name

%--
% handle input
%--

if (nargin < 3) || isempty(method)
	method = 'twister';
end

if nargin < 2
	init = 0;
end

if nargin < 1
	n = 10;
end

%--
% configure generator
%--

% TODO: consider getting initial state of generator and setting at end

rand(method, init);

%--
% get values and states if needed
%--

% NOTE: the states are the states before generating the value

if nargout < 2
	
	value = rand(n, 1);
	
else
	
	value = zeros(n, 1);
	
	for k = 1:n
		state(:, k) = rand(method); value(k) = rand(1);
	end

end