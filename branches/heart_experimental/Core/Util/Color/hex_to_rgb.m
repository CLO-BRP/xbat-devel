function rgb = hex_to_rgb(hex)

% hex_to_rgb - convert hexadecimal color strings to RGB matrix
% ------------------------------------------------------------
%
% rgb = hex_to_rgb(hex) 
%
% Input:
% ------
%  hex - cell array of hexadecimal color strings
%
% Output:
% -------
%  rgb - RGB matrix
%
% Example:
% --------
%  hex = rgb_to_hex(jet(16)); jet2 = hex_to_rgb(hex); jet(16) - jet2

%--
% handle multiple inputs
%--

if iscell(hex)
	rgb = iterate(mfilename, hex); return;
end

%--
% convert hex pairs to fractional color
%--

rgb = [hex2dec(hex(1:2)), hex2dec(hex(3:4)), hex2dec(hex(5:6))] / 255;

