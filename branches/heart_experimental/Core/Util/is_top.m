function value = is_top(in)

% TODO: this function was extracted from 'get_play_axes' it is not yet general

value = 1;

%--
% get other visible objects 
%--

% TODO: add visible flag as input

others = get_others(in, 1);

if isempty(others)
	value = 1; return;
end

%--
% compare positions
%--

pos = get(in, 'position'); pos = pos(2) + pos(4);

for k = 1:length(others)
	
	pos2 = get(others(k), 'position');
	
	if pos < (pos2(2) + pos2(4))
		value = 0; return;
	end
	
end
