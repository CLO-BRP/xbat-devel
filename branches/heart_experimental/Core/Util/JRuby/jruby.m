function [status, result] = jruby(varargin)

% jruby - through system call
% ---------------------------
%
% [status, result] = jruby(varargin)
%
% Input:
% ------
%  varargin - arguments
%
% Output:
% -------
%  status, result - of system call
%
% See also: jruby_path

root = jruby_path; 

if isempty(root)
	error('Install JRuby first.'); 
end

if ispc
	helper = fullfile(root, 'bin', 'jruby');
	
	if ~nargin
		status = helper; return;
	end
	
	[status, result] = system(['"', fullfile(root, 'bin', 'jruby'), '" ', str_implode(varargin, ' ')]); 
else
	[status, result] = system(['jruby ', str_implode(varargin, ' ')]);
end

if ~nargout
	disp(result); clear status
end 