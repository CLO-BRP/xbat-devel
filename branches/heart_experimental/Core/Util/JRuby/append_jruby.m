function jar = append_jruby

% append_jruby - jar to classpath
% -------------------------------
%
% jar = append_jruby
%
% Output:
% -------
%  jar - appended to classpath
% 
% See also: jruby_path

root = fullfile(jruby_path, 'lib'); 

content = dir(root); content(1:2) = [];

jar = {};

for k = 1:numel(content)
	name = content(k).name;
	
	if length(name) > 4 && strcmp(name(end - 3:end), '.jar')
		jar{end + 1} = name; %#ok<AGROW>
	end
end 

if isempty(jar)
	return;
end

jar = append_classpath(strcat(root, filesep, jar));