function mov = avi_test(in)

%--
% handle input
%--

if ~nargin
	in = 'sample.avi';
end

%--
% get file info and load file
%--

% NOTE: the first branch works on all platforms

if islinux || issolaris
	mov.info = aviinfo(in); mov.frame = aviread(in); mov.reader = [];
else
	mov.info = mmfileinfo(in); mov.frame = []; mov.reader = mmreader(in);
end
