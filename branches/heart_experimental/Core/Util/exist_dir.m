function value = exist_dir(in, goto)

% exist_dir - check existence of directory
% ----------------------------------------
%
% value = exist_dir(in, goto)
%
% Input:
% ------
%  in - proposed directory
%  goto - go to directory to see if it exists
%
% Output:
% -------
%  value - existence indicator
% 
% See also: exist

%--
% set quick check default
%--

% NOTE: we could check for 'filesep' characters in the input and to set this value

if nargin < 2
	goto = false;
end

%--
% check for directory
%--

if ~goto
	% NOTE: this serves as a shortcut and returns an indicator rather than a code
	
	value = exist(in, 'dir') ~= 0;
else
	% NOTE: this is more expensive, and only works with full or adequate partial path names
	
	start = pwd;

	try
		cd(in); value = 1;
	catch %#ok<CTCH>
		value = false;
	end

	cd(start);
end