function y = fast_spline_resample(yi, xi, p)

if nargin < 3
    p = min(10, length(yi));
end

y = zeros(numel(xi), size(yi, 2));

for k = 1:size(yi, 2)
    y(:,k) = fast_spline_resample_mex(yi(:,k), xi - 1, p);
end