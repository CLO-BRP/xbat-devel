function Y = lut_dB(X, b, c, Z)

% lut_dB - fast dB conversion 
% ---------------------------
%
% Y = lut_dB(X, b, c, Z)
%
% Input:
% ------
%  X - input image
%  b - input limits
%  c - calibration offset (lut offset)
%  Z - computation mask
%
% Output:
% -------
%  Y - image in dB units

% TODO: create an inverse dB table

%--------------------
% HANDLE INPUT
%--------------------

% NOTE: we set no mask, zero offset and data limits default values

if (nargin < 4) || isempty(Z)
	Z = [];
end

if (nargin < 3) || isempty(c)
	c = 0;
end

if (nargin < 2) || isempty(b)
	b = fast_min_max(X);
end

%---------------------------------
% COMPUTE LOOK UP
%---------------------------------

% TODO: compare evaluation more thoroughly and possibly improve LUT code

if 1
	
	%--
	% create lut applying offset if needed
	%--

	% NOTE: an unevenly spaced table would be smaller, the code not as simple

	L = lut_fun(@decibel, b, 2048);

	if c
		L = L + c;
	end

	%--
	% apply look up table
	%--

	% TODO: review the behavior of 'lut_apply' for values out of range

	Y = lut_apply(X, L, b, Z);
	
else
	
	%--
	% compute decibels directly
	%--

	Y = decibel(X);

end

