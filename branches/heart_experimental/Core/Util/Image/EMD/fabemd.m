function Y = fabemd(X, opt)

% fabemd - fast adaptive bidimensional EMD
% ----------------------------------------
%
% Y = fabemd(X, opt)
%
% Input:
% ------
%  X - image
%  opt - options
%
% Output:
% -------
%  Y - BIMF array

% TODO: save intermediate computations, specifically extrema

% NOTE: consider a single coded image for extrema across iterations

%--
% handle input
%--

if nargin < 2
	opt.type = 4;
	
	opt.radial = false;
	
	opt.verb = 1;
	
	if ~nargin && nargout
		Y = opt; return;
	end  
end

if ~nargout || opt.verb
	fig; imagesc(X), colorbar; colormap(gray(256)); axis image;
end

for k = 1:3
	[L, U] = get_envelopes(X, opt);
	
	Y(:, :, k) = X - 0.5 * (L + U); 
	
	X = X - Y(:, :, k);
end

Y(:, :, end + 1) = X;

if ~nargout || opt.verb
	
	for k = 1:size(Y, 3), fig; imagesc(Y(:,:,k)), colorbar; cmap_real(129); axis image; end
end


%-----------------------
% GET_ENVELOPES
%-----------------------

% NOTE: this is essentially a method of the EMD computation

function [L, U, E] = get_envelopes(X, opt)

% get_envelopes - for image
% -------------------------
%
% [L, U, E] = get_envelopes(X, opt)
%
% Input:
% ------
%  X - image
%  opt - options, coming from parent
%
% Output:
% -------
%  L, U - lower and upper envelopes
%  E - extrema indicator

%--
% compute image extrema
%--

[E, count] = image_extrema(X); %#ok<NASGU>

% TODO: consider count before we move on

%--
% compute extrema distances and radius of operations given type
%--

[p(:, 1), p(:, 2)] = find(E == 1, 2000);

pd = fast_nearest(p'); pd = pd(:, 1);

[v(:, 1), v(:, 2)] = find(E == -1, 2000);

vd = fast_nearest(v'); vd = vd(:, 1);

switch opt.type
	
	case 1 % min-min
		r = min(min(pd), min(vd));
		
	case 2 % min-min
		r = max(min(pd), min(vd));
		
	case 3 % min-min
		r = min(max(pd), max(vd));
		
	case 4 % max-max
		r = max(max(pd), max(vd));
end

r = next_odd(r)

%--
% compute envelopes
%--

% NOTE: we should be able to make use of CUDA acceleration for all steps below

% TODO: improve filtering, improve filter

if opt.radial
	SE = se_ball(r); F = SE / se_size(SE);
else
	SE = ones(r); F = SE / r^2;
end

% NOTE: 'morph' functions decompose the structuring element, below we decompose the linear filter 

L = morph_erode(X, SE); U = morph_dilate(X, SE);

L = box_filter(L, F) / r^2;

U = box_filter(U, F) / r^2;

