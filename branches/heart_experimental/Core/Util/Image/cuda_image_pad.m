function [Y, e] = cuda_image_pad(X, pq, b)

% cuda_image_pad - pad image on GPU for sliding window processing
% ---------------------------------------------------------------
% 
% Y = cuda_image_pad(X, pq, b)
% 
% Input:
% ------
%  X - input image
%  pq - support parameters
%  b - boundary behavior
%
%   -2 - cyclic boundary
%   -1 - reflecting boundary
%    n - n padding for n >= 0
%
% Output:
% -------
%  Y - padded image
%  e - CUDA error code

% NOTE: Only reflective boundary is implemented

%--
% grayscale or rgb image
%--

p = pq(1); q = pq(2);

if q >= size(X, 2) || p >= size(X, 1)
	b = 0;
end

d = ndims(X);

switch (d)

	case (2)
	
		[Y, e] = cuda_image_pad_mex(X, [p q], b);
		
	case (3)
	
		%--
		% initialize output depending on datatype
		%--
			
		Y = zeros(size(X) + [2 * pq, 0]);
		
		if isa(X, 'uint8')
			Y = uint8(Y);
		end
		
		%--
		% pad plane by plane
		%--
		
		for k = 1:3
			[Y(:,:,k), e] = cuda_image_pad(X(:,:,k), pq, b);
		end

end
