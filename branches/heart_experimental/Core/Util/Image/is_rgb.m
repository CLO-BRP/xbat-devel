function [value, m, n] = is_rgb(X)

% is_rgb - check for rgb image
% ----------------------------
%
% value = is_rgb(X)
%
% Input:
% ------
%  X - image
%
% Output:
% -------
%  value - test indicator
%
% See also: is_binary, is_gray, is_tricolor

value = is_tricolor(X) && all(X(:) >= 0);

if nargout > 1
	[m, n, ignore] = size(X); %#ok<NASGU>
end
	