function g_dst = get_gabor_max(g_src, slice_type)

% get_gabor_max - across given dimension
% ------------------------------------------------------------
%
% g_dst = get_gabor_max(g_src, slice_type)
%
% Input:
% ------
%  g_src - struct (output of fast_gabor function)
%  slice_type - one of 'scale', 'orientation', 'wavelength' or 'frequency'
%
% Output:
% -------
%  g_dst - struct
%
% See Also: fast_gabor


if nargin < 2
    error('Missing parameters');
end


switch slice_type
    case 'orientation'
        values = unique(g_src.gabor.parameters(:,2:3), 'rows');
        
        g_dst.gabor.data = g_src.gabor.data(:, :, size(values, 1));
        
        for v = 1:size(values, 1)
            size(g_src.gabor.parameters(:,2:3));
            size(values(v,:));
        
            indices = ismember(g_src.gabor.parameters(:,2:3), values(v,:), 'rows');
            
            g_dst.gabor.data(:, :, v) = max(g_src.gabor.data(:, :, indices), [], 3);
            
            g_dst.gabor.parameters(v, 2:3) = values(v,:);
            g_dst.gabor.parameters(v, 1) = NaN;
        end
           
        g_dst.gaussian.data = g_src.gaussian.data;
        g_dst.gaussian.parameters = g_src.gaussian.parameters;
        g_dst.laplacian.data = g_src.laplacian.data;
        g_dst.laplacian.parameters = g_src.laplacian.parameters;
        
    case {'wavelength', 'frequency'}
        values = unique(g_src.gabor.parameters(:,[1 3]), 'rows');
        
        g_dst.gabor.data = g_src.gabor.data(:, :, size(values, 1));
        
        for v = 1:size(values, 1)
            size(g_src.gabor.parameters(:,[1 3]));
            size(values(v,:));
        
            indices = ismember(g_src.gabor.parameters(:,[1 3]), values(v,:), 'rows');
            
            g_dst.gabor.data(:, :, v) = max(g_src.gabor.data(:, :, indices), [], 3);
            
            g_dst.gabor.parameters(v, 1) = values(v,1);
            g_dst.gabor.parameters(v, 3) = values(v,2);
            g_dst.gabor.parameters(v, 2) = NaN;
        end
        
        g_dst.gaussian.data = g_src.gaussian.data;
        g_dst.gaussian.parameters = g_src.gaussian.parameters;
        g_dst.laplacian.data = g_src.laplacian.data;
        g_dst.laplacian.parameters = g_src.laplacian.parameters;
        
    case 'scale'
        values = unique(g_src.gabor.parameters(:,1:2), 'rows');
        
        g_dst.gabor.data = g_src.gabor.data(:, :, size(values, 1));
        
        for v = 1:size(values, 1)
            size(g_src.gabor.parameters(:,1:2));
            size(values(v,:));
        
            indices = ismember(g_src.gabor.parameters(:,1:2), values(v,:), 'rows');
            
            g_dst.gabor.data(:, :, v) = max(g_src.gabor.data(:, :, indices), [], 3);
            
            g_dst.gabor.parameters(v, 1:2) = values(v,:);
            g_dst.gabor.parameters(v, 3) = NaN;
        end
        
        g_dst.gaussian.data = g_src.gaussian.data;
        g_dst.gaussian.parameters = g_src.gaussian.parameters;
        
        g_dst.laplacian.data = g_src.laplacian.data;
        g_dst.laplacian.parameters = g_src.laplacian.parameters;
        
    otherwise
        error(['Unknown gabor slice type ''', slice_type, '''.']);
        
end