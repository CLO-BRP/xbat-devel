function [Y, Z] = galaxies_gabor(scales, orientations, intensity, N)

% galaxies_gabor - create synthetic images using the gabor filters
% ----------------------------------------------------------------
%
% Y = galaxies_gabor(scales, orientations, intensity, N)
%
% Input:
% ------
%  scales, orientations - to render
%  intensity - of points
%  N - size of image
%
% Output:
% -------
%  Y - synthetic images
%
% See also: fast_gabor

%--
% handle input
%--

if nargin < 4
    N = 512;
end

if nargin < 3
    intensity = 0.0001;
end

if nargin < 2
    orientations =  0:30:150;
end

if ~nargin || isempty(scales)
    scales = 4 * [1, 2, 4];
end

%--
% compute point pattern
%--

%--
% filter to get synthetic image
%--

Y = zeros(N);

for k = 1:numel(scales)

    for l = 1:numel(orientations)
        
        X = rand(N) > (1 - intensity);
        
        P = fast_gabor(X, scales(k), orientations(l), 0.25);
        
        Y = Y + P.gabor.data;
        
    end
end

fig; imagesc(abs(Y));

% NOTE: can we get the filtered image, or can we only get energy?

Z = fast_gabor(Y, scales, orientations, 0.25);


