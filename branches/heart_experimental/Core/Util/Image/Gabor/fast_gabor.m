function gabor = fast_gabor(img, scales, orients, frequencies, sf_table)

% fast_gabor - IIR Gabor filter implementation
% ------------------------------------------
% 
% gabor = fast_gabor(img, scales, orients, wavelengths, sf_table)
% 
% Input:
% ------
%  img - input image
%  scales - scale values 
%  orients - orientations in degrees
%  frequencies - normalized
%  sf_table - table of scale/frequency combinations to include in returned
%             values, default is to return all permutations
%
% Output:
% -------
%  gabor.gabor.data - gabor outputs
%  gabor.gabor.parameters - parameters of gabor outputs
%  gabor.gauss.data - gaussian filtered images
%  gabor.gauss.parameters - parameters of gaussian images
%  gabor.laplace.data - laplacian filtered images at each scale
%  gabor.laplace.parameters - parameters of laplacian images
%
% NOTE: MEX implementation is coded to support single precision floating
% point data only.

%--
% Handle input
%--

if ~nargin
    error('Missing input image');
end

if nargin < 4
    error('Missing filter parameters');
end

if nargin < 5
    sf_table = true(numel(scales), numel(frequencies));
end

% Match parameter values of existing XBAT filter code

orients = -1 * orients - 90;

wavelengths = pi ./ frequencies;

% Call MEX

[gabor.gabor.data, gabor.gabor.parameters, ...
 gabor.gaussian.data, gabor.gaussian.parameters, ...
 gabor.laplacian.data, gabor.laplacian.parameters] = iirgabor_mex(single(img), scales, orients, wavelengths, logical(sf_table));
