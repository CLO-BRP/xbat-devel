#pragma once

//functions for generic step, 3 tap, iir filtering
void iir_filt_forward(float *in, int stepin, float *out, int stepout, int length,  float *coeffs, float *i0);
void iir_filt_backward(float *in, int stepin, float *out, int stepout, int length,  float *coeffs, float *i0);

//overloads for speed up

void iir_filt_forward(float *in, float *out, int length,  float *coeffs, float *i0);
void iir_filt_backward(float *in, float *out, int length,  float *coeffs, float *i0);

void iir_filt_forward(float *in, float *out, int stepout, int length,  float *coeffs, float *i0);
void iir_filt_backward(float *in, float *out, int stepout, int length,  float *coeffs, float *i0);

void iir_filt_forward(float *in, int stepin, float *out, int length,  float *coeffs, float *i0);
void iir_filt_backward(float *in, int stepin, float *out, int length,  float *coeffs, float *i0);




