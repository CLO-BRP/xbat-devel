// AUTHOR:  Alexandre Bernardino - ISR/IST
// FILE:    IIRGabor.h
// VERSION: 1.0
// DATE:    30/06/05
// CONTACT: alex@isr.ist.utl.pt

// DESCRIPTION: C++ class for fast image filtering with a gabor filter bank.
// Image must be of type float, single channel

#pragma once

#include <complex>
using namespace std;


//Comment the following line if you are not using IPP
//#define USE_IPP

#ifdef USE_IPP
	#include <ipp.h>
	typedef Ipp32f* IMAGE_PTR;
#else
	typedef float* IMAGE_PTR;
#endif

typedef IMAGE_PTR* IMAGE_PTR_VEC;

class CIIRGabor
{
private:
	long m_i_lines;			//image height (pixels)
	long m_i_cols;			//image width (pixels)
	long m_i_stridepix;		//number of pixels in a line (for data aligment)
	long m_i_scales;        //number of scales considered
	long m_i_wavelengths;   //number of gabor wavelenghts considered
	long m_i_orientations;  //number of gabor orientations considered
	long m_i_kernels;       //number of kernels (scale*wavelength combinations - not all are used) 
	
	double pi;				//the irracional number 
	int stride;				//number of bytes in a line (for data alignment) : stride = m_i_stridepix*sizeof(float)
	double *m_scales;       //array of size m_i_scales with the scale values
	double *m_wavelengths;  //array of size m_i_wavelengths with the gabor wavelength values (in pixels)
	double *m_orientations; //array of size m_i_orientations with the gabor orientation values (in radians)

	double *m_dc_gain;				//array of size scales*orientations*wavelengths with the offsets for zero mean gabor filters.
	double *m_magnitudex_kernel;	//array of size scales*orientations*wavelengths with the horizontal magnitude of the freq. resp.
	double *m_phasex_kernel;		//array of size scales*orientations*wavelengths with the magnitude of the vertical filter freq. resp.
	double *m_magnitudey_kernel;    //array of size scales*orientations*wavelengths with the magnitude of the vertical filter freq. resp.
	double *m_phasey_kernel;		//array of size scales*orientations*wavelengths with the phase of the vertical filter freq. resp.
	double **m_wx;					//matrix [orientation][wavelength] with the horizontal frequency of each filter.
	double **m_wy;					//matrix [orientation][wavelength] with the vertical frequency of each filter.


	float **filt_coeffs;			//matrix [scale][6] with the filter coefficients for each scale (max 6 coeffs : 1 gain + 5 autoregressive coefs)
	complex<double> **filt_poles;   //complex matrix [scale][5] with the filter poles for each scale (max 5 poles)
	float **m_resid_step_level;     //matrix [scale][3] with the step forced response residuals for a 3 tap gauss filter.
	float **m_resid_ic_level;       //matrix [scale][9] with the step natural response residuals for a 3 tap gauss filter.
	float **m_resid_cosx_kernel;	//matrix [gabors][3] with the horizontal cosine forced response residuals for a 3 tap gauss filter.
	float **m_resid_sinx_kernel;	//matrix [gabors][3] with the horizontal sine forced response residuals for a 3 tap gauss filter.
	float **m_resid_cosy_kernel;	//matrix [gabors][3] with the vertical cosine forced response residuals for a 3 tap gauss filter.
	float **m_resid_siny_kernel;	//matrix [gabors][3] with the vertical sine forced response residuals for a 3 tap gauss filter.
	
	
	bool **m_scale_wav_table;		//boolean (0/1) matrix [scale][wavelenght] indication the combinations of scales and wavelengths to use
	IMAGE_PTR input;				//input image to process - must be floating point single channel
	IMAGE_PTR even;					//temporary image to store intermediate computation steps
	IMAGE_PTR odd;					//temporary image to store intermediate computation steps
	IMAGE_PTR temp;					//temporary image to store intermediate computation steps
	IMAGE_PTR_VEC gausslevel;		//array [scales] with the gaussian filtered input images, for each scale
	IMAGE_PTR_VEC laplevel;         //array [scales] with the difference between consecutive gaussian levels
	IMAGE_PTR_VEC reallevel;        //array [gabors] with the real part of a gabor filtered image
	IMAGE_PTR_VEC imaglevel;		//array [gabors] with the imaginary part of a gabor filtered image
	IMAGE_PTR_VEC gaborlevel;		//array [gabors] with the modulus of a gabor filtered image
	IMAGE_PTR_VEC sine;				//array [orientation*wavelength] with sine image
	IMAGE_PTR_VEC cosine;			//array [orientation*wavelength] with cosine image
	float *upper_boundary;			//temporary image line for boundary computations
	float *lower_boundary;			//temporary image line for boundary computations
	bool m_bAllocated;				//boolean variable indicating if the object has been allocated
	//BOOL m_bFastDyadic;
public:
	long GetLines();
	long GetCols();
	long GetStridePix();
	long GetNScales(){return m_i_scales;};
	long GetNWavs(){return m_i_wavelengths;};
	long GetNOrients(){return m_i_orientations;};
	long GetNKernels(){return m_i_kernels;};
	void GetScales(double *scales);
	void GetWavelengths(double *wavelengths);
	void GetOrientations(double *orientations);
	void GetScaleWavTable(bool *scalewavtable);

	CIIRGabor(void);
	virtual ~CIIRGabor(void);
	IMAGE_PTR AccessGaussianLevel(int level);
	IMAGE_PTR AccessLaplacianLevel(int level);
	IMAGE_PTR AccessGaborRealLevel(int level);
	IMAGE_PTR AccessGaborImagLevel(int level);
	IMAGE_PTR AccessGaborEnergyLevel(int level);
	bool ProcessImage(IMAGE_PTR in);
	void GaussFiltOnce(IMAGE_PTR in, IMAGE_PTR out, int width, int height, int stridepix, double scale);
	void GaussFiltHorz(IMAGE_PTR in, IMAGE_PTR out, int width, int height, int stridepix, double scale);
	bool AllocateResources(long lines, long cols, long nlevels, double *scales,
		long norients, double *orients, long nwavs, double *wavs, bool *sw_table );
	bool FreeResources();
	bool IsAllocated();
	bool InitResidues();
private:
	void _image_prod(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out);
	void _const_prod(IMAGE_PTR in, IMAGE_PTR out, float gain);
	void _image_add(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out);
	void _image_sub(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out);
	void _image_sqrt(IMAGE_PTR in, IMAGE_PTR out);
};
