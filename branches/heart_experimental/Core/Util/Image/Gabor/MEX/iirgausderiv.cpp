#include <complex>
using namespace std;

#include "iirfilt.h"

//
//Values of the pole locations for the iir recursive gaussian derivative filters of scale s = 2.
//From "Recursive Gaussian Derivative Filters", L.van.Vliet,Ian.T.Young and Piet.W.Verbeek
//
static const complex<double> d0_N3_L2[] = {
	complex<double>(1.41650,1.00829),
	complex<double>(1.41650,-1.00829),
	complex<double>(1.86543,0),
};

static const complex<double> d0_N4_L2[] = {
	complex<double>(1.13228,1.28114),
	complex<double>(1.13228,-1.28114),
	complex<double>(1.78534,0.46763),
	complex<double>(1.78534,-0.46763),
};

static const complex<double> d0_N5_L2[] = {
	complex<double>(0.86430,1.45389),
	complex<double>(0.86430,-1.45389),
	complex<double>(1.61433,0.83134),
	complex<double>(1.61433,-0.83134),
	complex<double>(1.87504,0),
};

extern const complex<double> d0_N3_Linf[] = {
	complex<double>(1.40098,1.00236),
	complex<double>(1.40098,-1.00236),
	complex<double>(1.85132,0),
};

static const complex<double> d0_N4_Linf[] = {
	complex<double>(1.12075,1.27788),
	complex<double>(1.12075,-1.27788),
	complex<double>(1.76952,0.46611),
	complex<double>(1.76952,-0.46611),
};

static const complex<double> d0_N5_Linf[] = {
	complex<double>(0.85480,1.43749),
	complex<double>(0.85480,-1.43749),
	complex<double>(1.61231,0.82053),
	complex<double>(1.61231,-0.82053),
	complex<double>(1.87415,0)
};

static const complex<double> d1_N3_Linf[] = {
	complex<double>(1.31553,0.97057),
	complex<double>(1.31553,-0.97057),
	complex<double>(1.77635,0),
};

static const complex<double> d1_N4_Linf[] = {
	complex<double>(1.04185,1.24034),
	complex<double>(1.04185,-1.24034),
	complex<double>(1.69747,0.44790),
	complex<double>(1.69747,-0.44790),

};

static const complex<double> d1_N5_Linf[] = {
	complex<double>(0.77934,1.41423),
	complex<double>(0.77934,-1.41423),
	complex<double>(1.50941,0.80828),
	complex<double>(1.50941,-0.80828),
	complex<double>(1.77181,0)
};


static const complex<double> d3_N3_Linf[] = {
	complex<double>(1.22886,0.93058),
	complex<double>(1.22886,-0.93058),
	complex<double>(1.70493,0),
};

static const complex<double> d2_N4_Linf[] = {
	complex<double>(0.94570,1.21064),
	complex<double>(0.94570,-1.21064),
	complex<double>(1.60161,0.42647),
	complex<double>(1.60161,-0.42647),
};

static const complex<double> d2_N5_Linf[] = {
	complex<double>(0.69843,1.37655),
	complex<double>(0.69843,-1.37655),
	complex<double>(1.42631,0.77399),
	complex<double>(1.42631,-0.77399),
	complex<double>(1.69668,0)
};



//compute the poles of the filter for scale s given the poles at scale 2
void calc_poles(int taps, const double scale, const complex<double> oldpoles[], complex<double> newpoles[])
{
	if((taps < 3)||(taps>5))
		throw "Invalid number of taps in calc_poles";
	
	if(newpoles == NULL)
		throw "NULL Pointer argument in calc_poles";

	complex <double> d1_2, d2_2, d3_2, d4_2, d5_2;
	d1_2 = oldpoles[0];
	d2_2 = oldpoles[1];
	d3_2 = oldpoles[2];
	if(taps > 3)
		d4_2 = oldpoles[3];
	else
		d4_2 = 0;
	if(taps > 4)
		d5_2 = oldpoles[4];
	else
		d5_2 = 0;
	
	double q, std, lambda;
	double tol = 0.01;
	complex <double> j(0,1), var;
	complex <double> d1_s, d2_s, d3_s, d4_s, d5_s;
	// computing new values for the poles
	q = scale/2;
	d1_s = exp(log(abs(d1_2))/q)*exp(j*arg(d1_2)/q);
	d2_s = exp(log(abs(d2_2))/q)*exp(j*arg(d2_2)/q);
	d3_s = exp(log(abs(d3_2))/q)*exp(j*arg(d3_2)/q);
	if( abs(d4_2) != 0 )
		d4_s = exp(log(abs(d4_2))/q)*exp(j*arg(d4_2)/q);
	else
		d4_s = 0;
	if( abs(d5_2) != 0 )
		d5_s = exp(log(abs(d5_2))/q)*exp(j*arg(d5_2)/q);
	else
		d5_s = 0;
	// computing the variance of the new filter
	var =  d1_s*2.0/(d1_s-1.0)/(d1_s-1.0) + d2_s*2.0/(d2_s-1.0)/(d2_s-1.0) + d3_s*2.0/(d3_s-1.0)/(d3_s-1.0)+d4_s*2.0/(d4_s-1.0)/(d4_s-1.0)+d5_s*2.0/(d5_s-1.0)/(d5_s-1.0);
	std = sqrt(var.real());
	while( fabs(scale-std) > tol )
	{
		lambda = scale/std;
		q = q*lambda;
		// computing new values for the poles
		d1_s = exp(log(abs(d1_2))/q)*exp(j*arg(d1_2)/q);
		d2_s = exp(log(abs(d2_2))/q)*exp(j*arg(d2_2)/q);
		d3_s = exp(log(abs(d3_2))/q)*exp(j*arg(d3_2)/q);
		if( abs(d4_2) != 0)
			d4_s = exp(log(abs(d4_2))/q)*exp(j*arg(d4_2)/q);
		else
			d4_s = 0;
		if( abs(d5_2) != 0)
			d5_s = exp(log(abs(d5_2))/q)*exp(j*arg(d5_2)/q);
		else
			d5_s = 0;
		// computing the variance of the new filter
		var =  d1_s*2.0/(d1_s-1.0)/(d1_s-1.0) + d2_s*2.0/(d2_s-1.0)/(d2_s-1.0) + d3_s*2.0/(d3_s-1.0)/(d3_s-1.0)+d4_s*2.0/(d4_s-1.0)/(d4_s-1.0)+d5_s*2.0/(d5_s-1.0)/(d5_s-1.0);
		std = sqrt(var.real());
	}
	newpoles[0] = d1_s;
	newpoles[1] = d2_s;
	newpoles[2] = d3_s;
	newpoles[3] = d4_s;
	newpoles[4] = d5_s;
}

//compute the coefficients of the filter, given its poles
//the output is written in array coeffs - the first element is the gain and 
//the remaining elements correspond to the autoregressive coefficients 
void calc_coeffs(int taps, const complex<double> poles[], float *coeffs)
{
	if((taps < 3)||(taps>5))
		throw "Invalid number of taps in calc_coeffs";
	
	if(coeffs == NULL)
		throw "NULL Pointer argument in calc_coeffs";

	complex <double> d1_s, d2_s, d3_s, d4_s, d5_s;
	d1_s = poles[0];
	d2_s = poles[1];
	d3_s = poles[2];
	if(taps > 3)
		d4_s = poles[3];
	else
		d4_s = 0;
	if(taps > 4)
		d5_s = poles[4];
	else
		d5_s = 0;
	
	//computing the filter coeffs
	if( taps == 3 )
	{
		complex<double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s;
		coeffs[1] = (float)real(-b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s));
		coeffs[2] = (float)real(b*(d1_s + d2_s + d3_s));
		coeffs[3] = (float)real(-b);
		coeffs[4] = 0.0f;
		coeffs[5] = 0.0f;
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3];
	}
	else if(taps == 4)
	{
		complex<double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s/d4_s;
		coeffs[1] = (float)real(-b*(d3_s*d2_s*d1_s + d4_s*d2_s*d1_s + d4_s*d3_s*d1_s + d4_s*d3_s*d2_s));
		coeffs[2] = (float)real(b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s + d4_s*d1_s + d4_s*d2_s + d4_s*d3_s));
		coeffs[3] = (float)real(-b*(d1_s + d2_s + d3_s + d4_s));
		coeffs[4] = (float)real(b);
		coeffs[5] = 0.0f;
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3] + coeffs[4];
	}
	else if(taps == 5)
	{
		complex <double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s/d4_s/d5_s;
		coeffs[1] = (float)real(-b*(d4_s*d3_s*d2_s*d1_s + d5_s*d3_s*d2_s*d1_s + d5_s*d4_s*d2_s*d1_s + d5_s*d4_s*d3_s*d1_s + d5_s*d4_s*d3_s*d2_s));
		coeffs[2] = (float)real(b*(d3_s*d2_s*d1_s + d4_s*d2_s*d1_s + d4_s*d3_s*d1_s + d4_s*d3_s*d2_s + d5_s*d2_s*d1_s + d5_s*d3_s*d1_s + d5_s*d3_s*d2_s + d5_s*d4_s*d1_s + d5_s*d4_s*d2_s + d5_s*d4_s*d3_s));
		coeffs[3] = (float)real(-b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s + d4_s*d1_s + d4_s*d2_s + d4_s*d3_s + d5_s*d1_s + d5_s*d2_s + d5_s*d3_s + d5_s*d4_s));
		coeffs[4] = (float)real(b*(d1_s + d2_s + d3_s + d4_s + d5_s));
		coeffs[5] = (float)real(-b);
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3] + coeffs[4] + coeffs[5];    
	}
}



//compute the coefficients of the filter for scale s given the poles at scale 2
//the output is written in array coeffs - the first element is the gain and 
//the remaining elements correspond to the autoregressive coefficients 
void calc_coeffs(int taps, const complex<double> poles[], const double s, float *coeffs)
{

	if((taps < 3)||(taps>5))
		throw "Invalid number of taps in calc_coeffs";
	
	if(coeffs == NULL)
		throw "NULL Pointer argument in calc_coeffs";

	complex <double> d1_2, d2_2, d3_2, d4_2, d5_2;
	d1_2 = poles[0];
	d2_2 = poles[1];
	d3_2 = poles[2];
	if(taps > 3)
		d4_2 = poles[3];
	else
		d4_2 = 0;
	if(taps > 4)
		d5_2 = poles[4];
	else
		d5_2 = 0;
	
	double q, std, lambda;
	double tol = 0.01;
	complex <double> j(0,1), var;
	complex <double> d1_s, d2_s, d3_s, d4_s, d5_s;
	// computing new values for the poles
	q = s/2;
	d1_s = exp(log(abs(d1_2))/q)*exp(j*arg(d1_2)/q);
	d2_s = exp(log(abs(d2_2))/q)*exp(j*arg(d2_2)/q);
	d3_s = exp(log(abs(d3_2))/q)*exp(j*arg(d3_2)/q);
	if( abs(d4_2) != 0 )
		d4_s = exp(log(abs(d4_2))/q)*exp(j*arg(d4_2)/q);
	else
		d4_s = 0;
	if( abs(d5_2) != 0 )
		d5_s = exp(log(abs(d5_2))/q)*exp(j*arg(d5_2)/q);
	else
		d5_s = 0;
	// computing the variance of the new filter
	var =  d1_s*2.0/(d1_s-1.0)/(d1_s-1.0) + d2_s*2.0/(d2_s-1.0)/(d2_s-1.0) + d3_s*2.0/(d3_s-1.0)/(d3_s-1.0)+d4_s*2.0/(d4_s-1.0)/(d4_s-1.0)+d5_s*2.0/(d5_s-1.0)/(d5_s-1.0);
	std = sqrt(var.real());
	while( fabs(s-std) > tol )
	{
		lambda = s/std;
		q = q*lambda;
		// computing new values for the poles
		d1_s = exp(log(abs(d1_2))/q)*exp(j*arg(d1_2)/q);
		d2_s = exp(log(abs(d2_2))/q)*exp(j*arg(d2_2)/q);
		d3_s = exp(log(abs(d3_2))/q)*exp(j*arg(d3_2)/q);
		if( abs(d4_2) != 0)
			d4_s = exp(log(abs(d4_2))/q)*exp(j*arg(d4_2)/q);
		else
			d4_s = 0;
		if( abs(d5_2) != 0)
			d5_s = exp(log(abs(d5_2))/q)*exp(j*arg(d5_2)/q);
		else
			d5_s = 0;
		// computing the variance of the new filter
		var =  d1_s*2.0/(d1_s-1.0)/(d1_s-1.0) + d2_s*2.0/(d2_s-1.0)/(d2_s-1.0) + d3_s*2.0/(d3_s-1.0)/(d3_s-1.0)+d4_s*2.0/(d4_s-1.0)/(d4_s-1.0)+d5_s*2.0/(d5_s-1.0)/(d5_s-1.0);
		std = sqrt(var.real());
	}

	//computing the filter coeffs
	if( taps == 3 )
	{
		complex<double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s;
		coeffs[1] = (float)real(-b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s));
		coeffs[2] = (float)real(b*(d1_s + d2_s + d3_s));
		coeffs[3] = (float)real(-b);
		coeffs[4] = 0.0f;
		coeffs[5] = 0.0f;
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3];
	}
	else if(taps == 4)
	{
		complex<double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s/d4_s;
		coeffs[1] = (float)real(-b*(d3_s*d2_s*d1_s + d4_s*d2_s*d1_s + d4_s*d3_s*d1_s + d4_s*d3_s*d2_s));
		coeffs[2] = (float)real(b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s + d4_s*d1_s + d4_s*d2_s + d4_s*d3_s));
		coeffs[3] = (float)real(-b*(d1_s + d2_s + d3_s + d4_s));
		coeffs[4] = (float)real(b);
		coeffs[5] = 0.0f;
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3] + coeffs[4];
	}
	else if(taps == 5)
	{
		complex <double> b = complex<double>(1.0,0.0)/d1_s/d2_s/d3_s/d4_s/d5_s;
		coeffs[1] = (float)real(-b*(d4_s*d3_s*d2_s*d1_s + d5_s*d3_s*d2_s*d1_s + d5_s*d4_s*d2_s*d1_s + d5_s*d4_s*d3_s*d1_s + d5_s*d4_s*d3_s*d2_s));
		coeffs[2] = (float)real(b*(d3_s*d2_s*d1_s + d4_s*d2_s*d1_s + d4_s*d3_s*d1_s + d4_s*d3_s*d2_s + d5_s*d2_s*d1_s + d5_s*d3_s*d1_s + d5_s*d3_s*d2_s + d5_s*d4_s*d1_s + d5_s*d4_s*d2_s + d5_s*d4_s*d3_s));
		coeffs[3] = (float)real(-b*(d2_s*d1_s + d3_s*d1_s + d3_s*d2_s + d4_s*d1_s + d4_s*d2_s + d4_s*d3_s + d5_s*d1_s + d5_s*d2_s + d5_s*d3_s + d5_s*d4_s));
		coeffs[4] = (float)real(b*(d1_s + d2_s + d3_s + d4_s + d5_s));
		coeffs[5] = (float)real(-b);
		coeffs[0] = 1.0f + coeffs[1] + coeffs[2] + coeffs[3] + coeffs[4] + coeffs[5];    
	}
}


//
//compute the dc value of the 2D filter modulated by a complex exponential
//with horizontal and vertical frequencies wx,wy.
//
double calc_dcvalue(float *coeffs, double wx, double wy)
{
	double t1,t2;
	complex<double> temp1, temp2, temp3, temp4;
	complex<double> pureimag(0.0,1.0);
	complex<double> z1x, z2x, z3x, z4x, z5x, z_1x, z_2x, z_3x, z_4x, z_5x, z1y, z2y, z3y, z4y, z5y, z_1y, z_2y, z_3y, z_4y, z_5y;
	z1x = exp(pureimag*wx); 
	z2x = exp(pureimag*wx*2.0);
	z3x = exp(pureimag*wx*3.0);
	z4x = exp(pureimag*wx*4.0);
	z5x = exp(pureimag*wx*5.0);
	z_1x = exp(-pureimag*wx); 
	z_2x = exp(-pureimag*wx*2.0);
	z_3x = exp(-pureimag*wx*3.0);
	z_4x = exp(-pureimag*wx*4.0);
	z_5x = exp(-pureimag*wx*5.0);
	z1y = exp(pureimag*wy); 
	z2y = exp(pureimag*wy*2.0);
	z3y = exp(pureimag*wy*3.0);
	z4y = exp(pureimag*wy*4.0);
	z5y = exp(pureimag*wy*5.0);
	z_1y = exp(-pureimag*wy); 
	z_2y = exp(-pureimag*wy*2.0);
	z_3y = exp(-pureimag*wy*3.0);
	z_4y = exp(-pureimag*wy*4.0);
	z_5y = exp(-pureimag*wy*5.0);

	double b0, a1, a2, a3, a4, a5;
	b0 = coeffs[0];
	a1 = coeffs[1];
	a2 = coeffs[2];
	a3 = coeffs[3];
	a4 = coeffs[4];
	a5 = coeffs[5];

	temp1 = 1.0 + z1x*a1 + z2x*a2 + z3x*a3 + z4x*a4 + z5x*a5;
	temp2 = 1.0 + z_1x*a1 + z_2x*a2 + z_3x*a3 + z_4x*a4 + z_5x*a5;
	temp3 = 1.0 + z1y*a1 + z2y*a2 + z3y*a3 + z4y*a4 + z5y*a5;
	temp4 = 1.0 + z_1y*a1 + z_2y*a2 + z_3y*a3 + z_4y*a4 + z_5y*a5;
	t1 = real(temp1*temp2*temp3*temp4);
	t2 = pow(b0,4.0);
	if(t1 == 0)
		throw "Divide by zero exception in calc_dcvalue";
	return t2/t1;
}

//
//Compute the magnitude and phase of the 1D gaussian filter 
//modulated by a complex exponential of frequency f
//
void calc_frequency_response(float *filter_coeffs, double freq, double *magnitude, double *phase)
{
	complex<double> freqresp;
	complex<double> pureimag(0.0,1.0);
	complex<double> z1, z2, z3, z4, z5;
	complex<double> w(freq, 0.0);
	z1 = exp(-pureimag*w);
	z2 = exp(-2.0*pureimag*w);
	z3 = exp(-3.0*pureimag*w);
	z4 = exp(-4.0*pureimag*w);
	z5 = exp(-5.0*pureimag*w);

	double b0, a1, a2, a3, a4, a5;
	b0 = filter_coeffs[0];
	a1 = filter_coeffs[1];
	a2 = filter_coeffs[2];
	a3 = filter_coeffs[3];
	a4 = filter_coeffs[4];
	a5 = filter_coeffs[5];

	freqresp = b0/(1.0+a1*z1+a2*z2+a3*z3+a4*z4+a5*z5);
	*magnitude = abs(freqresp);
	*phase = arg(freqresp);
}

void compute_step_forward_ic(float bord_val, float *coeffs, float *i0)
{
	//filter coefficients
	float b0 = coeffs[0];
	float a1 = coeffs[1];
	float a2 = coeffs[2];
	float a3 = coeffs[3];

	i0[0] = i0[1] = i0[2] = bord_val*b0/(1+a1+a2+a3);
}

void compute_natural_backward_ic( float *resid_ic, float *init_cond )
{
	float i0 = init_cond[0];
	float i1 = init_cond[1];
	float i2 = init_cond[2];

	init_cond[0]=i0*resid_ic[0]+i1*resid_ic[1]+i2*resid_ic[2];
	init_cond[1]=i0*resid_ic[3]+i1*resid_ic[4]+i2*resid_ic[5];
	init_cond[2]=i0*resid_ic[6]+i1*resid_ic[7]+i2*resid_ic[8];
}

void add_step_backward_ic( float *resid_step, float val, float *init_cond )
{
	init_cond[0] += val*resid_step[0];
	init_cond[1] += val*resid_step[1];
	init_cond[2] += val*resid_step[2];
}

void add_horz_real_backward_ic(float *resid_cosx, float *resid_sinx, int bord_loc, float bord_val, double phase0, double freq, float *init_cond )
{
	double d = freq*(bord_loc+1)-phase0;
	init_cond[0] += (float)(bord_val*(cos(d)*resid_cosx[0] - sin(d)*resid_sinx[0]));
	init_cond[1] += (float)(bord_val*(cos(d)*resid_cosx[1] - sin(d)*resid_sinx[1]));
	init_cond[2] += (float)(bord_val*(cos(d)*resid_cosx[2] - sin(d)*resid_sinx[2]));
}

void add_horz_imag_backward_ic(float *resid_cosx, float *resid_sinx, int bord_loc, float bord_val, double phase0, double freq, float *init_cond )
{
	double d = freq*(bord_loc+1)-phase0;
	init_cond[0] += (float)(bord_val*(sin(d)*resid_cosx[0] + cos(d)*resid_sinx[0]));
	init_cond[1] += (float)(bord_val*(sin(d)*resid_cosx[1] + cos(d)*resid_sinx[1]));
	init_cond[2] += (float)(bord_val*(sin(d)*resid_cosx[2] + cos(d)*resid_sinx[2]));
}

void add_vert_real_backward_ic(float *resid_cosy, float *resid_siny, float real_bord, float imag_bord,	double freq, float *init_cond )
{
	init_cond[0] += (float)((real_bord*cos(freq)+imag_bord*sin(freq))*resid_cosy[0] + (imag_bord*cos(freq)-real_bord*sin(freq))*resid_siny[0]);
	init_cond[1] += (float)((real_bord*cos(freq)+imag_bord*sin(freq))*resid_cosy[1] + (imag_bord*cos(freq)-real_bord*sin(freq))*resid_siny[1]);
	init_cond[2] += (float)((real_bord*cos(freq)+imag_bord*sin(freq))*resid_cosy[2] + (imag_bord*cos(freq)-real_bord*sin(freq))*resid_siny[2]);
}

void add_vert_imag_backward_ic(float *resid_cosy, float *resid_siny, float real_bord, float imag_bord,	double freq, float *init_cond )
{
	init_cond[0] += (float)((imag_bord*cos(freq)-real_bord*sin(freq))*resid_cosy[0] - (real_bord*cos(freq)+imag_bord*sin(freq))*resid_siny[0]);
	init_cond[1] += (float)((imag_bord*cos(freq)-real_bord*sin(freq))*resid_cosy[1] - (real_bord*cos(freq)+imag_bord*sin(freq))*resid_siny[1]);
	init_cond[2] += (float)((imag_bord*cos(freq)-real_bord*sin(freq))*resid_cosy[2] - (real_bord*cos(freq)+imag_bord*sin(freq))*resid_siny[2]);
}

void _compute_gauss3_resids( complex<double> filt_poles[], float *filt_coeffs, float *resid_ic, float *resid_step )
{

	complex <double> p1, p2, p3, _p1, _p2, _p3;
	complex <double> p1_1, p1_2, p1_3, p2_1, p2_2, p2_3, p3_1, p3_2, p3_3;
	double b0, a1, a2, a3;

	_p1 = filt_poles[0];
	_p2 = filt_poles[1];
	_p3 = filt_poles[2];
	b0 = filt_coeffs[0];
	a1 = filt_coeffs[1];
	a2 = filt_coeffs[2];
	a3 = filt_coeffs[3];


	p1 = 1.0/_p1;
	p2 = 1.0/_p2;
	p3 = 1.0/_p3;
	p1_1  = _p1;
	p1_2 = p1_1*_p1;
	p1_3 = p1_2*_p1;
	p2_1 = _p2;
	p2_2 = p2_1*_p2;
	p2_3 = p2_2*_p2;
	p3_1 = _p3;
	p3_2 = p3_1*_p3;
	p3_3 = p3_2*_p3;

	complex <double> res1a, res2a, res3a, res1b, res2b, res3b, res1c, res2c, res3c;
	//a3 = -p1*p2*p3
	res1a = b0/a3*p1_3/(1.0-p1_2)/(1.0-p1_1*(p2+p2_1)+p1_2)/(1.0-p1_1*(p3+p3_1)+p1_2);
	res2a = b0/a3*p2_3/(1.0-p2_2)/(1.0-p2_1*(p1+p1_1)+p2_2)/(1.0-p2_1*(p3+p3_1)+p2_2);
	res3a = b0/a3*p3_3/(1.0-p3_2)/(1.0-p3_1*(p2+p2_1)+p3_2)/(1.0-p3_1*(p1+p1_1)+p3_2);
	res1b = p1_1*res1a;
	res2b = p2_1*res2a;
	res3b = p3_1*res3a;
	res1c = p1_2*res1a;
	res2c = p2_2*res2a;
	res3c = p3_2*res3a;

	//initial condition residues
	complex<double> r0, r1, r2, r3, r4, r5, r6, r7, r8;

	r0 = (-a1*res1a-a2*res1b-a3*res1c);
	r1 = (-a2*res1a-a3*res1b);
	r2 = (-a3*res1a);
	r3 = (-a1*res2a-a2*res2b-a3*res2c);
	r4 = (-a2*res2a-a3*res2b);
	r5 = (-a3*res2a);
	r6 = (-a1*res3a-a2*res3b-a3*res3c);
	r7 = (-a2*res3a-a3*res3b);
	r8 = (-a3*res3a);

	resid_ic[0] = (float)real(r0+r3+r6);
	resid_ic[1] = (float)real(r1+r4+r7);
	resid_ic[2] = (float)real(r2+r5+r8);
	resid_ic[3] = (float)real(r0*p1+r3*p2+r6*p3);
	resid_ic[4] = (float)real(r1*p1+r4*p2+r7*p3);
	resid_ic[5] = (float)real(r2*p1+r5*p2+r8*p3);
	resid_ic[6] = (float)real(r0*p1*p1+r3*p2*p2+r6*p3*p3);
	resid_ic[7] = (float)real(r1*p1*p1+r4*p2*p2+r7*p3*p3);
	resid_ic[8] = (float)real(r2*p1*p1+r5*p2*p2+r8*p3*p3);

	//step residues
	complex <double> res1, res2, res3, res4;

	res1 = b0*res1a/(1.0-_p1);
	res2 = b0*res2a/(1.0-_p2);
	res3 = b0*res3a/(1.0-_p3);
	res4 = b0*b0/(1+a1+a2+a3)/(1+a1+a2+a3);
	resid_step[0] = (float)real(res1+res2+res3+res4);
	resid_step[1] = (float)real(res1*p1+res2*p2+res3*p3+res4);
	resid_step[2] = (float)real(res1*p1*p1+res2*p2*p2+res3*p3*p3+res4);
}

void _compute_gabor3_horz_resids(complex<double> filt_poles[], float *filt_coeffs, double f, float *resid_cosx, float *resid_sinx)
{
	complex <double> p1, p2, p3, _p1, _p2, _p3;
	complex <double> _p4, _p5, p4, p5;
	complex <double> p1_1, p1_2, p1_3, p2_1, p2_2, p2_3, p3_1, p3_2, p3_3, p4_1, p4_2, p4_3, p5_1, p5_2, p5_3;
	double b0, a1, a2, a3;

	_p1 = filt_poles[0];
	_p2 = filt_poles[1];
	_p3 = filt_poles[2];
	b0 = filt_coeffs[0];
	a1 = filt_coeffs[1];
	a2 = filt_coeffs[2];
	a3 = filt_coeffs[3];

	complex <double> res1, res2, res3, res4, res5;
	complex <double> res1a, res2a, res3a, res1b, res2b, res3b, res1c, res2c, res3c; 

	p1 = 1.0/_p1;
	p2 = 1.0/_p2;
	p3 = 1.0/_p3;
	p1_1  = _p1;
	p1_2 = p1_1*_p1;
	p1_3 = p1_2*_p1;
	p2_1 = _p2;
	p2_2 = p2_1*_p2;
	p2_3 = p2_2*_p2;
	p3_1 = _p3;
	p3_2 = p3_1*_p3;
	p3_3 = p3_2*_p3;
	
	res1a = b0/a3*p1_3/(1.0-p1_2)/(1.0-p1_1*(p2+p2_1)+p1_2)/(1.0-p1_1*(p3+p3_1)+p1_2);
	res2a = b0/a3*p2_3/(1.0-p2_2)/(1.0-p2_1*(p1+p1_1)+p2_2)/(1.0-p2_1*(p3+p3_1)+p2_2);
	res3a = b0/a3*p3_3/(1.0-p3_2)/(1.0-p3_1*(p2+p2_1)+p3_2)/(1.0-p3_1*(p1+p1_1)+p3_2);

	p4 = complex<double>(cos(f), sin(f));
	p5 = complex<double>(cos(f), -sin(f));
	_p4 = 1.0/p4;
	_p5 = 1.0/p5;
	p4_1 = _p4;
	p4_2 = p4_1*_p4;
	p4_3 = p4_2*_p4;
	p5_1 = _p5;
	p5_2 = p5_1*_p5;
	p5_3 = p5_2*_p5;
	
	res1 = (1.0-cos(f)*p1_1)*b0*res1a/(1.0-p4*p1_1)/(1.0-p5*p1_1);
	res2 = (1.0-cos(f)*p2_1)*b0*res2a/(1.0-p4*p2_1)/(1.0-p5*p2_1);
	res3 = (1.0-cos(f)*p3_1)*b0*res3a/(1.0-p4*p3_1)/(1.0-p5*p3_1);
	res4 = (1.0-cos(f)*p4_1)*b0*b0/a3*p4_3/(1.0-p4_2)/(1.0-p4_1*(p1+p1_1)+p4_2)/(1.0-p4_1*(p2+p2_1)+p4_2)/(1.0-p4_1*(p3+p3_1)+p4_2);
	res5 = (1.0-cos(f)*p5_1)*b0*b0/a3*p5_3/(1.0-p5_2)/(1.0-p5_1*(p1+p1_1)+p5_2)/(1.0-p5_1*(p2+p2_1)+p5_2)/(1.0-p5_1*(p3+p3_1)+p5_2);

	resid_cosx[0] = (float)real(res1+res2+res3+res4+res5);
	resid_cosx[1] = (float)real(res1*p1+res2*p2+res3*p3+res4*p4+res5*p5);
	resid_cosx[2] = (float)real(res1*p1*p1+res2*p2*p2+res3*p3*p3+res4*p4*p4+res5*p5*p5);

	res1 = (sin(f)*p1_1)*b0*res1a/(1.0-p4*p1_1)/(1.0-p5*p1_1);
	res2 = (sin(f)*p2_1)*b0*res2a/(1.0-p4*p2_1)/(1.0-p5*p2_1);
	res3 = (sin(f)*p3_1)*b0*res3a/(1.0-p4*p3_1)/(1.0-p5*p3_1);
	res4 = (sin(f)*p4_1)*b0*b0/a3*p4_3/(1.0-p4_2)/(1.0-p4_1*(p1+p1_1)+p4_2)/(1.0-p4_1*(p2+p2_1)+p4_2)/(1.0-p4_1*(p3+p3_1)+p4_2);
	res5 = (sin(f)*p5_1)*b0*b0/a3*p5_3/(1.0-p5_2)/(1.0-p5_1*(p1+p1_1)+p5_2)/(1.0-p5_1*(p2+p2_1)+p5_2)/(1.0-p5_1*(p3+p3_1)+p5_2);

	resid_sinx[0] = (float)real(res1+res2+res3+res4+res5);
	resid_sinx[1] = (float)real(res1*p1+res2*p2+res3*p3+res4*p4+res5*p5);
	resid_sinx[2] = (float)real(res1*p1*p1+res2*p2*p2+res3*p3*p3+res4*p4*p4+res5*p5*p5);
}

void _compute_gabor3_vert_resids(complex<double> filt_poles[], float *filt_coeffs, double f, float *resid_cosy, float *resid_siny)
{
	complex <double> p1, p2, p3, _p1, _p2, _p3;
	complex <double> _p4, _p5, p4, p5;
	complex <double> p1_1, p1_2, p1_3, p2_1, p2_2, p2_3, p3_1, p3_2, p3_3, p4_1, p4_2, p4_3, p5_1, p5_2, p5_3;
	double b0, a1, a2, a3;

	_p1 = filt_poles[0];
	_p2 = filt_poles[1];
	_p3 = filt_poles[2];
	b0 = filt_coeffs[0];
	a1 = filt_coeffs[1];
	a2 = filt_coeffs[2];
	a3 = filt_coeffs[3];

	complex <double> res1, res2, res3, res4, res5;
	complex <double> res1a, res2a, res3a, res1b, res2b, res3b, res1c, res2c, res3c; 

	p1 = 1.0/_p1;
	p2 = 1.0/_p2;
	p3 = 1.0/_p3;
	p1_1  = _p1;
	p1_2 = p1_1*_p1;
	p1_3 = p1_2*_p1;
	p2_1 = _p2;
	p2_2 = p2_1*_p2;
	p2_3 = p2_2*_p2;
	p3_1 = _p3;
	p3_2 = p3_1*_p3;
	p3_3 = p3_2*_p3;
	
	res1a = b0/a3*p1_3/(1.0-p1_2)/(1.0-p1_1*(p2+p2_1)+p1_2)/(1.0-p1_1*(p3+p3_1)+p1_2);
	res2a = b0/a3*p2_3/(1.0-p2_2)/(1.0-p2_1*(p1+p1_1)+p2_2)/(1.0-p2_1*(p3+p3_1)+p2_2);
	res3a = b0/a3*p3_3/(1.0-p3_2)/(1.0-p3_1*(p2+p2_1)+p3_2)/(1.0-p3_1*(p1+p1_1)+p3_2);

	p4 = complex<double>(cos(f), sin(f));
	p5 = complex<double>(cos(f), -sin(f));
	_p4 = 1.0/p4;
	_p5 = 1.0/p5;
	p4_1 = _p4;
	p4_2 = p4_1*_p4;
	p4_3 = p4_2*_p4;
	p5_1 = _p5;
	p5_2 = p5_1*_p5;
	p5_3 = p5_2*_p5;
	
	res1 = (1.0-cos(f)*p1_1)*b0*res1a/(1.0-p4*p1_1)/(1.0-p5*p1_1);
	res2 = (1.0-cos(f)*p2_1)*b0*res2a/(1.0-p4*p2_1)/(1.0-p5*p2_1);
	res3 = (1.0-cos(f)*p3_1)*b0*res3a/(1.0-p4*p3_1)/(1.0-p5*p3_1);
	res4 = (1.0-cos(f)*p4_1)*b0*b0/a3*p4_3/(1.0-p4_2)/(1.0-p4_1*(p1+p1_1)+p4_2)/(1.0-p4_1*(p2+p2_1)+p4_2)/(1.0-p4_1*(p3+p3_1)+p4_2);
	res5 = (1.0-cos(f)*p5_1)*b0*b0/a3*p5_3/(1.0-p5_2)/(1.0-p5_1*(p1+p1_1)+p5_2)/(1.0-p5_1*(p2+p2_1)+p5_2)/(1.0-p5_1*(p3+p3_1)+p5_2);

	resid_cosy[0] = (float)real(res1+res2+res3+res4+res5);
	resid_cosy[1] = (float)real(res1*p1+res2*p2+res3*p3+res4*p4+res5*p5);
	resid_cosy[2] = (float)real(res1*p1*p1+res2*p2*p2+res3*p3*p3+res4*p4*p4+res5*p5*p5);

	res1 = (sin(f)*p1_1)*b0*res1a/(1.0-p4*p1_1)/(1.0-p5*p1_1);
	res2 = (sin(f)*p2_1)*b0*res2a/(1.0-p4*p2_1)/(1.0-p5*p2_1);
	res3 = (sin(f)*p3_1)*b0*res3a/(1.0-p4*p3_1)/(1.0-p5*p3_1);
	res4 = (sin(f)*p4_1)*b0*b0/a3*p4_3/(1.0-p4_2)/(1.0-p4_1*(p1+p1_1)+p4_2)/(1.0-p4_1*(p2+p2_1)+p4_2)/(1.0-p4_1*(p3+p3_1)+p4_2);
	res5 = (sin(f)*p5_1)*b0*b0/a3*p5_3/(1.0-p5_2)/(1.0-p5_1*(p1+p1_1)+p5_2)/(1.0-p5_1*(p2+p2_1)+p5_2)/(1.0-p5_1*(p3+p3_1)+p5_2);

	resid_siny[0] = static_cast<float>(real(res1+res2+res3+res4+res5));
	resid_siny[1] = static_cast<float>(real(res1*p1+res2*p2+res3*p3+res4*p4+res5*p5));
	resid_siny[2] = static_cast<float>(real(res1*p1*p1+res2*p2*p2+res3*p3*p3+res4*p4*p4+res5*p5*p5));
}



void _iir_gaussfilt3_horz(float * in, float * tempbuf, float * out, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step)
{
	
	float i0[3]; //initial condition vector - to compute
	int i,s,bi,bf;
	float bi_val, bf_val;
	s = stridepix;
	//filtering rows
	for(i = 0; i < height; i++)
	{
		bi = i*s;
		bf = i*s+width-1;
		bi_val = in[bi];
		bf_val = in[bf];
		//causal phase
		//compute forward initial conditions (response to a step of bi_val amplitude)
		compute_step_forward_ic(bi_val, coeffs, i0);
		
		/*mag = b0/(1+a1+a2+a3);
		i0[0] = i0[1] = i0[2] = (float)(mag*bi_val);*/

		iir_filt_forward(in+bi,tempbuf,width,coeffs,i0);
		//anticausal phase
		i0[0] = tempbuf[width-1];
		i0[1] = tempbuf[width-2];
		i0[2] = tempbuf[width-3];
		
		compute_natural_backward_ic(resid_ic,i0);
		add_step_backward_ic(resid_step,bf_val,i0);

		iir_filt_backward(tempbuf,out+bi,width,coeffs,i0);
	}
}

void _iir_gaussfilt3_vert(float * inout, float *tempbuf, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step)
{

	//filter coefficients
	float b0 = coeffs[0];
	float a1 = coeffs[1];
	float a2 = coeffs[2];
	float a3 = coeffs[3];

	float i0[3]; //initial condition vector - to compute
	int j,s,bi,bf;
	float bi_val, bf_val;
	s = stridepix;

	//filtering columns	
	for(j = 0; j < width; j++)
	{
		bi = j;
		bf = (height-1)*s+j;
		bi_val = inout[bi];
		bf_val = inout[bf];
		//causal phase
		//compute forward initial conditions (response to a step of bi_val amplitude)
		compute_step_forward_ic(bi_val, coeffs, i0);

		/*mag = b0/(1+a1+a2+a3);
		i0[0] = i0[1] = i0[2] = (float)(mag*bi_val);*/

		iir_filt_forward(inout+bi,s,tempbuf,height,coeffs,i0);

		i0[0] = tempbuf[height-1];
		i0[1] = tempbuf[height-2];
		i0[2] = tempbuf[height-3];

		compute_natural_backward_ic(resid_ic,i0);
		add_step_backward_ic(resid_step,bf_val,i0);

		iir_filt_backward(tempbuf,inout+bi,s,height,coeffs,i0);
	}
}

void _iir_gaussfilt3(float * in, float * out, float *tempbuf, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step )
{
	_iir_gaussfilt3_horz(in, tempbuf, out, width, height, stridepix, coeffs, resid_ic, resid_step);
	_iir_gaussfilt3_vert(out, tempbuf, width, height, stridepix, coeffs, resid_ic, resid_step);
}


void compute_real_horz_forward_ic(double filtermag, double filterphase, double carrierfreq, double carrierphase, float bord_val, float *i0) 
{
		i0[0] = (float)(filtermag*bord_val*cos(carrierfreq*(-1)+filterphase+carrierphase));
		i0[1] = (float)(filtermag*bord_val*cos(carrierfreq*(-2)+filterphase+carrierphase));
		i0[2] = (float)(filtermag*bord_val*cos(carrierfreq*(-3)+filterphase+carrierphase));
}

void compute_imag_horz_forward_ic(double filtermag, double filterphase, double carrierfreq, double carrierphase, float bord_val, float *i0) 
{
		i0[0] = (float)(filtermag*bord_val*sin(carrierfreq*(-1)+filterphase+carrierphase));
		i0[1] = (float)(filtermag*bord_val*sin(carrierfreq*(-2)+filterphase+carrierphase));
		i0[2] = (float)(filtermag*bord_val*sin(carrierfreq*(-3)+filterphase+carrierphase));
}

void compute_real_vert_forward_ic(double filtermag, double filterphase, double carrierfreq, float bord_real_val, float bord_imag_val, float *i0) 
{
		i0[0] = (float)(filtermag*bord_real_val*cos(carrierfreq*(-1)+filterphase)+filtermag*bord_imag_val*sin(carrierfreq*(-1)+filterphase));
		i0[1] = (float)(filtermag*bord_real_val*cos(carrierfreq*(-2)+filterphase)+filtermag*bord_imag_val*sin(carrierfreq*(-2)+filterphase));
		i0[2] = (float)(filtermag*bord_real_val*cos(carrierfreq*(-3)+filterphase)+filtermag*bord_imag_val*sin(carrierfreq*(-3)+filterphase));
}

void compute_imag_vert_forward_ic(double filtermag, double filterphase, double carrierfreq, float bord_real_val, float bord_imag_val, float *i0) 
{
		i0[0] = (float)(filtermag*bord_imag_val*cos(carrierfreq*(-1)+filterphase)-filtermag*bord_real_val*sin(carrierfreq*(-1)+filterphase));
		i0[1] = (float)(filtermag*bord_imag_val*cos(carrierfreq*(-2)+filterphase)-filtermag*bord_real_val*sin(carrierfreq*(-2)+filterphase));
		i0[2] = (float)(filtermag*bord_imag_val*cos(carrierfreq*(-3)+filterphase)-filtermag*bord_real_val*sin(carrierfreq*(-3)+filterphase));
}

void _iir_gaborfilt3_real_horz(float * in, float * even, float *tempbuf, float * real, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx)
{
	
	//initial condition vector - to compute
	float i0[3]; 
	//local vars
	int i,s,bi,bf;
	float bi_val, bf_val;
	double initial_phase;

	s = stridepix;

	for(i = 0; i < height; i++)
	{
		bi = i*s;
		bf = i*s+width-1;
		bi_val = in[bi];
		bf_val = in[bf];

		//causal phase
		//initial contitions at the boundary for forward pass
		initial_phase = -wy*i;
		compute_real_horz_forward_ic(mag, phase, wx, initial_phase, bi_val, i0);

		iir_filt_forward(even+bi, tempbuf, width,  coeffs, i0);

		//anticausal phase
		i0[0] = tempbuf[width-1];
		i0[1] = tempbuf[width-2];
		i0[2] = tempbuf[width-3];
		compute_natural_backward_ic(resid_ic,i0);
		if(wx == 0.0)
			add_step_backward_ic(resid_step,bf_val,i0);
		else
			add_horz_real_backward_ic(resid_cosx, resid_sinx, width-1, bf_val, wy*i, wx, i0);

		iir_filt_backward(tempbuf, real+bi, width,  coeffs, i0);
	}
}

void _iir_gaborfilt3_imag_horz(float * in, float * odd, float *tempbuf, float * imag, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx)
{
	//initial condition vector - to compute
	float i0[3]; 
	//local vars
	int i,s,bi,bf;
	float bi_val, bf_val;
	double initial_phase;

	s = stridepix;

	for(i = 0; i < height; i++)
	{
		bi = i*s;
		bf = i*s+width-1;
		bi_val = in[bi];
		bf_val = in[bf];

		//causal phase
		//initial contitions at the boundary for forward pass
		initial_phase = -wy*i;
		compute_imag_horz_forward_ic(mag, phase, wx, initial_phase, bi_val, i0);

		iir_filt_forward(odd+bi, 1, tempbuf, 1, width,  coeffs, i0);

		//anticausal phase
		i0[0] = tempbuf[width-1];
		i0[1] = tempbuf[width-2];
		i0[2] = tempbuf[width-3];

		compute_natural_backward_ic(resid_ic,i0);
		if(wx == 0.0)
			add_step_backward_ic(resid_step,bf_val,i0);
		else
			add_horz_imag_backward_ic(resid_cosx, resid_sinx, width-1, bf_val, wy*i, wx, i0);

		iir_filt_backward(tempbuf, imag+bi, width,  coeffs, i0);
	}
}


void _iir_gaborfilt3_real_vert(float * real, float * imag, float *tempbuf, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosy, float *resid_siny, float *up_bound, float *low_bound)
{
	//initial condition vector - to compute
	float i0[3]; 
	//local vars
	int j,s, bi, bf;
	float  real_bi_val, imag_bi_val, real_bf_val, imag_bf_val;

	s = stridepix;

	for(j =0 ; j < width; j++)
	{
		bi = j;
		bf = (height-1)*s+j;
		real_bi_val = real[bi];
		imag_bi_val = imag[bi];
		real_bf_val = real[bf];
		imag_bf_val = imag[bf];

		//must save real boundary values to use on the imag part
		up_bound[j] = real[bi];
		low_bound[j] = real[bf];

		//causal phase
		compute_real_vert_forward_ic(mag, phase, wy, real_bi_val, imag_bi_val, i0); 

		iir_filt_forward(real+bi, s, tempbuf, height,  coeffs, i0);

		i0[0] = tempbuf[height-1];
		i0[1] = tempbuf[height-2];
		i0[2] = tempbuf[height-3];
		
		compute_natural_backward_ic(resid_ic,i0);
		if(wy == 0.0)
			add_step_backward_ic(resid_step,real_bf_val,i0);
		else
			add_vert_real_backward_ic(resid_cosy, resid_siny, real_bf_val, imag_bf_val, wy, i0);

		iir_filt_backward(tempbuf, real+bi, s, height,  coeffs, i0);

	}
}

void _iir_gaborfilt3_imag_vert(float * real, float * imag, float *tempbuf, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosy, float *resid_siny, float *up_bound, float *low_bound)
{
	//initial condition vector - to compute
	float i0[3]; 
	//local vars
	int j,s,bi,bf;
	float real_bi_val, imag_bi_val, real_bf_val, imag_bf_val;

	s = stridepix;

	for(j = 0; j < width; j++)
	{
		bi = j;
		bf = (height-1)*s+j;
		real_bi_val = up_bound[j];
		real_bf_val = low_bound[j];
		imag_bi_val = imag[bi];
		imag_bf_val = imag[bf];
		
		//causal phase
		compute_imag_vert_forward_ic(mag, phase, wy, real_bi_val, imag_bi_val, i0); 

		iir_filt_forward(imag+bi, s, tempbuf, height,  coeffs, i0);

		i0[0] = tempbuf[height-1]; 
		i0[1] = tempbuf[height-2];
		i0[2] = tempbuf[height-3];

		compute_natural_backward_ic(resid_ic,i0);
		if(wy == 0.0)
			add_step_backward_ic(resid_step,imag_bf_val,i0);
		else
			add_vert_imag_backward_ic(resid_cosy, resid_siny, real_bf_val, imag_bf_val, wy, i0);

		iir_filt_backward(tempbuf, imag+bi, s, height,  coeffs, i0);

	}
}



void _iir_gaborfilt3(float * in, float * even, float * odd, float * real, float * imag, float *tempbuf, 
		int width, int height, int stridepix, float *coeffs, double wx, double wy, double magx, double magy, double phasex, double phasey, 
		float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx, float *resid_cosy, float *resid_siny,
		float *up_bound, float *low_bound)
{
	//
	// image 'even+j*odd' is the modulation of image 'in' by the complex exponential :
	// exp{j(w*cos(theta)*x) - w*sin(theta)*y)} = exp{j(w*cos(theta)*x)}*exp{-j(w*sin(theta)*y)}
	//

	//The order of the operations is important
	_iir_gaborfilt3_real_horz(in, even, tempbuf, real, width, height, stridepix, coeffs, wx, wy, magx, phasex, resid_ic, resid_step, resid_cosx, resid_sinx);
	_iir_gaborfilt3_imag_horz(in, odd, tempbuf, imag, width, height, stridepix, coeffs, wx, wy, magx, phasex, resid_ic, resid_step, resid_cosx, resid_sinx);

	//
	// na imagem out, a margem superior 'e dada por 
	// ms(x,y) = out(x,Y)*exp(-j*w*sin(theta)*(y-Y))
	// onde Y a indice da localização da borda da imagem
	// Isto vai dar duas componentes - real e imaginaria:
	// ms_real(x,y) = out_real(x,Y)*cos(w*sin(theta)*(y-Y)) + out_imag(x,Y)*sin(w*sin(theta)*(y-Y))
	// ms_imag(x,y) = out_imag(x,Y)*cos(w*sin(theta)*(y-Y)) - out_real(x,Y)*sin(w*sin(theta)*(y-Y))
	//

	_iir_gaborfilt3_real_vert(real, imag, tempbuf, width, height, stridepix, coeffs, wx, wy, magy, phasey, resid_ic, resid_step, resid_cosy, resid_siny, up_bound, low_bound);
	_iir_gaborfilt3_imag_vert(real, imag, tempbuf, width, height, stridepix, coeffs, wx, wy, magy, phasey, resid_ic, resid_step, resid_cosy, resid_siny, up_bound, low_bound);
	
}

