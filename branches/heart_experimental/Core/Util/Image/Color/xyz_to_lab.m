function Y = xyz_to_lab(X, w)

% xyz_to_lab - lab to xyz conversion
% ----------------------------------
%
% Y = xyz_to_lab(X, w)
%
% Input:
% ------
%  X - xyz image
%  w - white point
%
% Output:
% -------
%  Y - lab image

%--
% handle input
%--

if ~is_tricolor(X)
	error('Input image does not have three channels.');
end

%--
% NOTE: set defwhite point
%--

if nargin < 2
   w = [0.9642, 1, 0.8249];
end

%--
% separate planes and rename
%--

Xn = w(1); Yn = w(2); Zn = w(3);

Z = double(X(:, :, 3)) ./ Zn;

Y = double(X(:, :, 2)) ./ Yn;

X = double(X(:, :, 1)) ./ Xn;

%--
% compute conversion
%--

offset = 0.008856;

% NOTE: each pair of lines below (the first with multiple statements) computes one plane

fY = Y.^(1/3); I = find(Y <= offset); fY(I) = 7.787 * Y(I) + (16/116);

L = 116 * (fY) - 16;

a = X.^(1/3); I = find(X <= offset); a(I) = 7.787 * X(I) + (16/116);

a = (a - fY) * 500;

b = Z.^(1/3); I = find(Z <= offset); b(I) = 7.787 * Z(I) + (16/116);

b = (fY - b) * 200;

% NOTE: here we pack the Lab image

Y(:, :, 1) = L; Y(:, :, 2) = a; Y(:, :, 3) = b;
