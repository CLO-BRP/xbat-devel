function Y = rgb_vec(X)

% rgb_vec - reshape RGB image into pixel row format
% -------------------------------------------------
%
% Y = rgb_vec(X)
%
% Input:
% ------
%  X - RGB image
%
% Output:
% -------
%  Y - image in pixel row form

%--
% check input
%--

% NOTE: the RGB test also check positivity, this tests only the shape

if ~is_tricolor(X)
	error('Input image is not tri-color.');
end

%--
% reshape to pixel row form
%--

Y = [vec(X(:, :, 1)), vec(X(:, :, 2)), vec(X(:, :, 3))];