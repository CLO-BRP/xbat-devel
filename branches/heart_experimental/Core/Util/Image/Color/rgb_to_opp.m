function Y = rgb_to_opp(X)

% rgb_to_opp - rgb to opponent colors conversion
% ----------------------------------------------
%
% Y = rgb_to_opp(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - opponent colors image
%
% See also: opp_to_rgb

%--
% check and get size of input
%--

[value, m, n] = is_rgb(X);

if ~value
	error('Input is not RGB image.');
end

%--
% define and apply linear transform
%--

V = [ ...
	1/2, 1/4, 1/3; ...
	-1/2, 1/4, 1/3; ...
	0, -1/2, 1/3 ...
];

Y = rgb_reshape(rgb_vec(double(X)) * V, m, n);
