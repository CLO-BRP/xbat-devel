function Y = opp_to_rgb(X)

% opp_to_rgb - opponent colors to rgb conversion
% ----------------------------------------------
%
% Y = opp_to_rgb(X)
%
% Input:
% ------
%  X - opponent colors image
%
% Output:
% -------
%  Y - rgb image
%
% See also: rgb_to_opp

%--
% get size of input
%--

[value, m, n] = is_tricolor(X);

if ~value
	error('Input is not tri-color image.');
end

%--
% define and apply linear transformation, we also enforce positivity
%--

V = [ ...
	1, -1, 0; ...
	2/3, 2/3, -4/3; ...
	1, 1, 1 ...
];

Y = rgb_reshape(rgb_vec(double(X)) * V, m, n);

Y(Y < 0) = 0;
