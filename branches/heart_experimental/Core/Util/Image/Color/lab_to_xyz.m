function Y = lab_to_xyz(X, w)

% lab_to_xyz - lab to xyz conversion
% ----------------------------------
%
% Y = lab_to_xyz(X, w)
%
% Input:
% ------
%  X - lab image
%  w - white point
%
% Output:
% -------
%  Y - xyz image

%--
% check size of input
%--

if ~is_tricolor(X)
	error('Input is not tri-color image.');
end

%--
% set white point and rename components
%--

if nargin < 2
   w = [0.9642, 1, 0.8249];
end

Xn = w(1); Yn = w(2); Zn = w(3);

%--
% separate planes
%--

X = double(X); L = X(:, :, 1); a = X(:, :, 2); b = X(:, :, 3);

%--
% allocate output planes
%--

X = zeros(size(L)); Y = X; Z = X;

%--
% compute conversion
%--

offset = 0.008856;

fY = (L + 16) .* (1/116); Y = fY.^3;

I = find(fY <= (7.787 * offset + (16/116))); Y(I) = (1/7.787) * (fY(I) - (16/116));

a = a./500 + fY; X = a.^3;

I = find(a <= (7.787 * offset + (16/116))); X(I) = (1/7.787) * (a(I) - (16/116));

b = fY - b./200; Z = b.^3;

I = find(b <= (7.787 * offset + (16/116))); Z(I) = (1/7.787) * (b(I) - (16/116));


tmp(:, :, 1) = Xn * X;
tmp(:, :, 2) = Yn * Y;
tmp(:, :, 3) = Zn * Z;

Y = tmp;