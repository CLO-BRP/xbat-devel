function Y = luv_to_rgb(X)

% luv_to_rgb - luv to rgb conversion
% ----------------------------------
%
% Y = luv_to_rgb(X)
%
% Input:
% ------
%  X - luv image
%
% Output:
% -------
%  Y - rgb image

Y = xyz_to_rgb(luv_to_xyz(X));
