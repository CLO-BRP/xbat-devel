function Y = cuda_linear_filter(X, F, n, b, same)

% cuda_linear_filter - implementation
% -----------------------------------
% 
% Y = cuda_linear_filter(X, F, n, b)
% 
% Input:
% ------
%  X - input image
%  F - filter
%  n - number of iterations (def: 1)
%  b - boundary behavior (def: -1, reflecting)
%
%   -2 - cyclic boundary
%   -1 - reflecting boundary 
%    n - n padding for n >= 0
%  same - return same type, default return doubles
%
% Output:
% -------
%  Y - minimum image
%
% NOTE: when 'filter' input is a struct, it should be as the output of 'filt_decomp'
%
% See also: linear_filter, cuda_max_filter, cuda_min_filter, filt_decomp

% NOTE: only reflective boundary is implemented

%--
% handle input
%--

if nargin < 5
    same = 0;
end

if nargin < 4
    b = -1;
end

if nargin < 3
    n = 1;
end

% NOTE: in 'filt_decomp' we have 'F.X = V; F.Y = U;' where the values come from the 'svd'

if ~isstruct(F)
	[row, col] = size(F); f = F(:); separable = 0;
else
	[row, col] = size(F.H); f = [F.Y * diag(F.S); F.X]; separable = numel(F.S);
end

% NOTE: filter must be double

f = double(f);

%--
% process single and multiple plane images
%--

switch ndims(X)

	case 2
		[Y, status] = cuda_linear_filter_mex(X, f, row, col, separable, n, b, same);

		handle_cuda_failure(status)
        
	case 3
		Y = X;
		
		for k = 1:size(X, 3)
			[Y(:, :, k), status] = cuda_linear_filter_mex(X(:, :, k), f, row, col, separable, n, b, same);
			
			handle_cuda_failure(status)
		end
end
