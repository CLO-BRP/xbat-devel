function t = is_binary(X)

% is_binary - check for binary image
% ----------------------------------
%
% t = is_binary(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  t - test result
%
% See also: is_gray, is_rgb, is_tricolor

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-27 15:35:44 -0400 (Tue, 27 Jun 2006) $
% $Revision: 5430 $
%--------------------------------

%--
% assume it all went to plan
%--

t = 1;

%--
% double image
%--

if isa(X, 'double')

	b = fast_min_max(X);
	
	if (b(1) < 0) || (b(2) > 1)
		t = 0;
		return;
	end
	
	if any(round(X) ~= X)
		t = 0;
		return;
	end
	
%--
% uint8 image
%--

elseif isa(X, 'uint8')

	b = fast_min_max(X);
	
	if (b(1) < 0) || (b(2) > 1)
		t = 0;
		return;
	end
end
