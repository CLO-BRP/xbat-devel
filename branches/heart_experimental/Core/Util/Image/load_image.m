function X = load_image

% load_image - load image from file
% ---------------------------------
%
% X = load_image
%
% Output:
% -------
%  X - image matrix

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 229 $
% $Date: 2004-12-09 14:54:20 -0500 (Thu, 09 Dec 2004) $
%--------------------------------

%--
% get image file interactively
%--

[f,p] = uigetfile( ...
	{ ...
		'*.gif;*.jpg;*.png;*.tif', 'All Image Files (*.gif, *.jpg, *.png, *.tif)';
        '*.gif',  'GIF Images (*.gif)'; ...
        '*.jpg','JPEG Images (*.jpg)'; ...
        '*.png','PNG Images (*.png)'; ...
        '*.tif','TIFF Images (*.tif)'; ...
        '*.*',  'All Files (*.*)' ...
	}, ...
	'Select an image file' ...
);

%--
% load image file
%--

% NOTE: return empty on cancel

if (f)
	X = imread([p,f]);
else
	X = [];
end