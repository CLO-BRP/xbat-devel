function image_bdfun(h,str)

% image_bdfun - axes selection display for image viewing
% ------------------------------------------------------
%
% image_bdfun(h)
%
% Input:
% ------
%  h - handle to figure (def: gcf)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:53-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set figure and command string if needed
%--

switch (nargin)
	
case (0)
	h = gcf;
	str = 'Initialize';
case (1)
	str = 'Initialize';
	
end
	
%--
% set bdfun or highlight axes
%--

switch (str)
	
%--
% set bdfun
%--

case ('Initialize')
	
	set(h,'windowbuttondownfcn',['image_bdfun(gcf,''highlight'');']);
	
%--
% highlight current axes
%--

case ('highlight')
	
% 	% get all axes handles
% 	
% 	axs = findobj(h,'type','axes');
% 	
% 	% make all axes normal
% 	
% 	for k = 1:length(axs)
% 		ax = axs(k);
% 		set(ax,'linewidth',0.5);
% 		set(get(ax,'title'),'fontweight','normal');
% 		set(get(ax,'xlabel'),'fontweight','normal');
% 		set(get(ax,'ylabel'),'fontweight','normal');
% 	end
% 	
% 	% get image axes handles
% 	
% 	im = get_image_handles(h);
% 	clear axs;
% 	for k = 1:length(im);
% 		axs(k) = get(im(k),'parent');
% 	end 
% 	
% 	% highlight current image axes
% 	
% 	ax = gca;
% 	if (any(axs == ax))
% 		set(ax,'linewidth',2);
% 		set(get(ax,'title'),'fontweight','bold');
% 		set(get(ax,'xlabel'),'fontweight','bold');
% 		set(get(ax,'ylabel'),'fontweight','bold');
% 	end
	
end