function Y = linear_filter(X, F, n, b, st)

% linear_filter - linear filtering
% --------------------------------
% 
% Y = linear_filter(X, F, n, b, st)
%
%   = linear_filter(X, F, Z, b, st)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  F - linear filter
%  n - number of iterations (def: 1)
%  Z - computation mask image (def: [])
%  b - boundary behavior (def: -1)
%  st - return same type as input, default is to return doubles
%
% Output:
% -------
%  Y - linear filtered image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-08-29 14:04:35 -0400 (Tue, 29 Aug 2006) $
% $Revision: 6339 $
%--------------------------------

% TODO: extend mex to compute with multiple filters

% TODO: extend to handle filter decomposition at the mex level

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% consider image or handle input
%--

% NOTE: this is very old code and has been put into a separate function

flag = 0;

if length(X) == 1 && ishandle(X)
	
	h = X;

	hi = get_image_handles(h);

	if length(hi) == 1
		X = get(hi, 'cdata'); flag = 1;
	else
		disp('Figure has more than one image. No computation performed.'); return;
	end
end

%--
% set boundary behavior
%--

% NOTE: the boundary behavior code is documented in 'image_pad'

if nargin < 5
    st = 0;
end

if nargin < 4
	b = -1;
end

%--
% iteration or mask
%--

if nargin < 3 || isempty(n)
	
	%--
	% set default behavior of single unmasked iteration
	%--
	
	n = 1; Z = [];
else
	%--
	% check whether input may be computation mask rather than iterations
	%--
	
	Z = []; [r, c, s] = size(X);
	
	if all(size(n) == [r, c])
		Z = n; n = 1;
	end
end

% Get size of raw filter data

Fraw = F;

if ~isreal(Fraw)
    Fraw = Fraw.H;
end

pq = (size(Fraw) - 1)./2;

% invoke Overlap Add Method 2D convolution if it will be faster

if isempty(Z) && numel(X) >= 32768 && numel(Fraw) >= 4300
	
	X = image_pad(X, pq, b);
	
	Y = olam_conv2(X, rot90(Fraw,2), 'valid'); return;
end

% invoke MATLAB conv2 if it will be faster

if isempty(Z)
	
	X = image_pad(X, pq, b);
	
	Y = conv2(double(X), double(rot90(Fraw,2)), 'valid'); return;
end

% invoke CUDA implementation if possible

% NOTE: the code below will remember where CUDA linear filter failed and not delegate

state.data = {class(X), size(F), st};

if isempty(Z) && cuda_enabled && ~cuda_has_failed(mfilename, state)
	
	try		
		Y = cuda_linear_filter(X, F, n, b, st); return;
	catch		
		cuda_has_failed(mfilename, state, lasterror);
		
		clear cuda_linear_filter_mex; % TODO: could this be integrated into 'cuda_has_failed'? probably not simple
	end
end

%---------------------------------------
% MULTIPLE PLANE IMAGES
%---------------------------------------

if ndims(X) > 2
	
	Y = zeros(size(X));
	
	for k = 1:size(X, 3)
		
		if isempty(Z)
			Y(:, :, k) = linear_filter(X(:, :, k), F, n, b);
		else
			Y(:, :, k) = linear_filter(X(:, :, k), F, Z, b);
		end
	end	
	
%---------------------------------------
% SCALAR IMAGES
%---------------------------------------

else 
	
	%--
	% iterate operator
	%--
	
	if isempty(Z)
		
		%--
		% simple filter computation
		%--
		
		if isreal(F)
			
			F = double(F);
			
			pq = (size(F) - 1)./2;
			
			for j = 1:n
				X = image_pad(X, pq, b);
				
				Y = linear_filter_mex(X, F, st);
				
				if n > 1
					X = Y;
				end
			end
			
		%--
		% decomposed filter computation
		%--
			
		else
			for j = 1:n
				Y = linear_filter_decomp(X, F, b, st);
				
				if n > 1
					X = Y;
				end
			end
		end
		
	%--
	% mask operator
	%--
		
	else
		% NOTE: the decomposed filter is not used on masked computation
		
		if ~issreal(F)
			F = F.H;
		end
		
		F = double(F);
		
		pq = (size(F) - 1)./2;
		
		X = image_pad(X, pq, b); Z = image_pad(Z, pq, 0);
		
		Y = linear_filter_mex(X, F, st, uint8(Z));
	end
end

%---------------------------------------
% DISPLAY OUTPUT
%---------------------------------------

% NOTE: this was part of the old image processing framework

% NOTE: this framework permits the processing of generic figure displayed images

if flag && view_output
	
	switch view_output
		
		%--
		% update display of image from figure
		%--
		
		case 1
			figure(h);
			set(hi, 'cdata', Y);
			set(gca, 'clim', fast_min_max(Y));
			
		%--
		% produce a new image display
		%--
			
		otherwise
			fig; image_view(Y);
	end
end

%---------------------------------------
% GET_FILTER_KEY
%---------------------------------------

function key = get_filter_key(F)

if isstruct(F)
	key = ['separable', int2str(size(F.H, 1)), 'x', int2str(size(F.H, 2))];
else
	key = ['direct', int2str(size(F, 1)), 'x', int2str(size(F, 2))];
end


%-----------------------------------------------
% LINEAR_FILTER_DECOMP
%-----------------------------------------------

function Y = linear_filter_decomp(X, F, b, st)

% linear_filter_decomp - apply decomposed linear filter
% -----------------------------------------------------
%
% Y = linear_filter_decomp(X, F, b, st)
%
% Input:
% ------
%  X - input image
%  F - decomposed filter
%  b - boundary behavior
%  st - return same type as input, default return doubles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-08-29 14:04:35 -0400 (Tue, 29 Aug 2006) $
% $Revision: 6339 $
%--------------------------------

% TODO: consider whether the approximation needs to be renormalized

%--
% pad input image once
%--

pq = (size(F.H) - 1)./2;

X = image_pad(X, pq, b);

%--
% apply first component
%--

fx = F.X(:, 1)'; fy = F.S(1) * F.Y(:, 1);

fx = double(fx); fy = double(fy);

Y = linear_filter_mex(linear_filter_mex(X, fx, st), fy, st);

%--
% apply remaining components if needed
%--

% NOTE: the rank is computed using the tolerance parameter

for k = 2:numel(F.S)

	fx = F.X(:, k)'; fy = F.S(k) * F.Y(:, k);	

    fx = double(fx); fy = double(fy);

    Y = Y + linear_filter_mex(linear_filter_mex(X, fx, st), fy, st);	
end
