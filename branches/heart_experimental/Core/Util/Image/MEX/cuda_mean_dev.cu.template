#include <cuda.h>

#include "cuda_image_pad.h"

//-----------------------------------------
// Filter blocks of pixels in shared memory
//   Input and output image include padding
//-----------------------------------------

// Tile local to this thread-block of pixels, including apron
extern __shared__ CUDA_TYPE tile_CUDA_TYPE_NAME[];

__global__ void mean_dev_CUDA_TYPE_NAME (
	CUDA_TYPE *Y, CUDA_TYPE *Z, CUDA_TYPE *X, unsigned int M, unsigned int N, CUDA_TYPE *F, unsigned int rM, unsigned int rN
)

{
    // Twice apron width for computing offsets
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);
    
    // ------------------------------
    // Fill shared memory pixel tiles
    // ------------------------------
                
    // Fill one row per thread for first blockDim.x + rM2 threads

    unsigned int bM = blockDim.x * blockIdx.x;          // Block index M
	unsigned int bN = blockDim.y * blockIdx.y;          // Block index N
    unsigned int ti = threadIdx.y * blockDim.x + threadIdx.x;
    
    // Boundaries for east and south edge tiles
    int mLimit = min(blockDim.x, M - blockDim.x * blockIdx.x) + rM2;
    int nLimit = min(blockDim.y, N - blockDim.y * blockIdx.y) + rN2;
    
    if (ti < mLimit)
    {
        CUDA_TYPE *t1 = tile_CUDA_TYPE_NAME + ti;
        CUDA_TYPE *d = X + bN * (M + rM2) + bM + ti;
        
        #pragma unroll 8
        for (int l = 0; l < nLimit; l++)
        {
            *t1 = *d;
            t1 += blockDim.x + rM2;
            d += M + rM2;
        }
    }

    // Synchronize tile load for thread block
    
    syncthreads();
	
    // --------------
    // Compute result
    // --------------
    
    // Get pixel location for this thread
    unsigned int iM = blockDim.x * blockIdx.x + threadIdx.x;    // Image index M
    unsigned int iN = blockDim.y * blockIdx.y + threadIdx.y;    // Image index N
    	
    // NOTE: If we are off the edge of the array then do no work
	
    if (iM >= M || iN >= N) { return; }

    CUDA_TYPE *p;
    CUDA_TYPE *f = F;
    CUDA_TYPE y;
	CUDA_TYPE z;
	CUDA_TYPE w;
    
	// Find mean
	w = 0;
    p = tile_CUDA_TYPE_NAME + threadIdx.y * (blockDim.x + rM2) + threadIdx.x;
    y = (F == NULL) ? (*p) : (*f++) * (*p);
    for (int n = 0; n <= rN2; n++)
    {
        p = tile_CUDA_TYPE_NAME + (threadIdx.y + n) * (blockDim.x + rM2) + threadIdx.x;
        if (n == 0) { p++; }
        
        #pragma unroll 8
        for (int m = (n == 0) ? 1 : 0; m <= rM2; m++)
        {
            y += (F == NULL) ? (*p) : (*f) * (*p);
			w += (F == NULL) ? (CUDA_TYPE) 1 : *f;
            p++; f++;
        }
    }
	
	y /= w;
    
    Y[iN * M + iM] = y;
	
	// Find deviation
	w = 0;
    p = tile_CUDA_TYPE_NAME + threadIdx.y * (blockDim.x + rM2) + threadIdx.x;
    z = (*p - y) * (*p - y);
	f++;
    for (int n = 0; n <= rN2; n++)
    {
        p = tile_CUDA_TYPE_NAME + (threadIdx.y + n) * (blockDim.x + rM2) + threadIdx.x;
        if (n == 0) { p++; }
        
        #pragma unroll 8
        for (int m = (n == 0) ? 1 : 0; m <= rM2; m++)
        {
            z += (*p - y) * (*p - y);
			p++; f++; w += 1.0;
        }
    }
    
    Z[iN * M + iM] = z / w;
}

// ---------------------------------------------------------
// cuda_image_filter
// ---------------------------------------------------------

extern "C" cudaError_t cuda_mean_dev_filter_CUDA_TYPE_NAME (CUDA_TYPE *Y, CUDA_TYPE *Z, CUDA_TYPE *X, unsigned int M, unsigned int N, CUDA_TYPE *F, unsigned int rM, unsigned int rN, int bound);

cudaError_t cuda_mean_dev_filter_CUDA_TYPE_NAME (
		CUDA_TYPE *Y, CUDA_TYPE *Z, CUDA_TYPE *X, unsigned int M, unsigned int N, CUDA_TYPE *F, unsigned int rM, unsigned int rN, int bound
)

{
	// Keep track of CUDA status
	
	cudaError_t cudaErr = cudaSuccess;
	
	// Double structuring element radius for dimension calculations
    
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);
    	
    // Compute how many pixels + apron can we fit in availabe shared memory
    //    Shared memory is 16K, we reserve 512 bytes for local variables
    
    double b = rM2 + rN2;
    double c = rM2 * rN2 - (16384.0 - 512.0) / (2.0 * sizeof(CUDA_TYPE));
    int tpb = (-b + sqrt(b*b - 4*c)) / 2;
    
    // Threads per block
    // There must be at least 1, CUDA docs suggest at least 32, max is 512
    if (tpb > 16) { tpb = 16; }
    if (tpb < 1)  { cudaErr = cudaErrorLaunchOutOfResources; }
    
    dim3 threadsPerBlock(tpb, tpb);

    // Block dimensions for processing the image
    dim3 numBlocks;
    
    // Shared memory tile size
	unsigned int shMemSize;
    
	// Grid size for median filtering
	numBlocks.x = (M + threadsPerBlock.x - 1) / threadsPerBlock.x;
	numBlocks.y = (N + threadsPerBlock.y - 1) / threadsPerBlock.y;

    // Size of shared memory tile buffer
	shMemSize = (threadsPerBlock.x + rM2) * (threadsPerBlock.y + rN2) * sizeof(CUDA_TYPE);
    
    // Global memory buffer for image with apron
	CUDA_TYPE *in = NULL;
    
    // Allocate padded image
    if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **)&in, (M + rM2) * (N + rN2) * sizeof(CUDA_TYPE)); }

	if (cudaErr == cudaSuccess)
	{
		// Copy image into center of padded image
		image_pad_center_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(in, X, M, N, rM, rN);
	
		// Pad image (in place)
		cuda_image_pad_only_CUDA_TYPE_NAME(in, M, N, rM, rN);
    
		// Compute the filter
		mean_dev_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock, shMemSize>>>(Y, Z, in, M, N, F, rM, rN);
	}
	
    // Deallocate padded image buffers
    if (in != NULL) { cudaFree(in); }
    
    return cudaErr;
}
