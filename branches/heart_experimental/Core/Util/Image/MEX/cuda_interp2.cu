#include "cutil.h"
#include "cuda.h"
#include "cubicTex2D.cu"

#define IMUL(a, b) __mul24(a, b)

//Round a / b to nearest higher integer value
int iDivUp(int a, int b){
	return (a % b != 0) ? (a / b + 1) : (a / b);
}
//////////////////////////////////////////////////////////////////////////////////////
///         Interpolation Kernel
//////////////////////////////////////////////////////////////////////////////////////

texture<float, 2, cudaReadModeElementType> texInput;

__global__ void interpTex( float2 *points, float *output, int output_numel, int output_width ){
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
	
	const int loc = IMUL(y,output_width) + x;
	
	if (loc < output_numel) { output[loc] = tex2D(texInput, points[loc].x, points[loc].y); }
}

__global__ void cubicInterpTex( float2 *points, float *output, int output_numel, int output_width ){
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
	
	const int loc = IMUL(y,output_width) + x;
	
	if (loc < output_numel) { output[loc] = cubicTex2D(texInput, points[loc].x, points[loc].y); }
}

extern "C"
cudaError_t interp2_function(float *f_input, int INPUT_W, int INPUT_H, int INPUT_SIZE, float *f_output, int OUTPUT_W, int OUTPUT_H, int OUTPUT_SIZE, float2 *f_points, int POINTS_SIZE, int interp_mode)
{
    // Clear last CUDA error
    cudaGetLastError();
    
    // Keep track of CUDA status
    cudaError_t cudaErr = cudaSuccess;
	
	// Allocate, copy input data into a 2D texture
	cudaArray *d_input;
	cudaChannelFormatDesc input_tex = cudaCreateChannelDesc<float>();
	
	cudaMallocArray(&d_input, &input_tex, INPUT_W, INPUT_H);
	cudaMemcpyToArray(d_input, 0, 0, f_input, INPUT_SIZE, cudaMemcpyHostToDevice);

    if (interp_mode == 0)
    {
        texInput.filterMode = cudaFilterModePoint;
    }
    else
    {
        texInput.filterMode = cudaFilterModeLinear;
    }
	texInput.normalized = 0;
	
	cudaBindTextureToArray(texInput, d_input);
	
	// Allocate, copy points data into a float2*
	float2 *d_points;
	cudaMalloc((void **)&d_points, POINTS_SIZE);
	cudaMemcpy(d_points, f_points, POINTS_SIZE, cudaMemcpyHostToDevice);
	
	// Allocate output space
	float *d_output;
	cudaMalloc((void **)&d_output, OUTPUT_SIZE);
	
	// Set up blocks, grid for parallel processing
	dim3 dimBlock(16, 12);
	dim3 dimGrid(iDivUp(OUTPUT_W,dimBlock.x),iDivUp(OUTPUT_H,dimBlock.y));
	
	// Run it
    if (interp_mode == 2)
    {
        cubicInterpTex<<<dimGrid, dimBlock>>> (d_points, d_output, OUTPUT_W*OUTPUT_H, OUTPUT_W);
    }
    else
    {
        interpTex<<<dimGrid, dimBlock>>> (d_points, d_output, OUTPUT_W*OUTPUT_H, OUTPUT_W);
    }
    
	// Copy the data back
	cudaMemcpy(f_output, d_output, OUTPUT_SIZE, cudaMemcpyDeviceToHost);
	
	cudaUnbindTexture(texInput);
	cudaFreeArray(d_input);
	
	cudaFree(d_points);
	cudaFree(d_output);
    
    return cudaErr;
}