#include "mex.h"

#include "cuda_image_thresh.h"

// multi_thresh_ - multiple image thresholds computation
// -----------------------------------------------------
//
// Y = multi_thresh_(X,T);
//
// Input:
// ------
//  X - input image
//  T - threshold values
//
// Output:
// -------
//  Y - multiply thresholded image

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
	register int m, n, N, P;
	
	register void *X, *T, *Y;
	
	int threads;
	
	mwSize dims[3];
	
	// Get input image
	
	X = (void *) mxGetPr(prhs[0]);
	m = mxGetM(prhs[0]); n = mxGetN(prhs[0]);
	N = m * n;
	
	// Output image dimensions
	
	dims[0] = m;
	dims[1] = n;

	// Threshold array
	
	T = (void *) mxGetPr(prhs[1]);
	P = mxGetM(prhs[1]) * mxGetN(prhs[1]);
	
	// TODO: Get default thread count from cuda_get_attributes
	
	if (nlhs < 3)
	{ threads = 256; }
	else
	{ threads = (int) mxGetScalar(prhs[2]); }
	
	// Call approriate MEX
	
	switch (mxGetClassID(prhs[0]))
	{
		case mxDOUBLE_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL));
			cuda_image_thresh_double((double *) Y, (double *) X, N, (double *) T, P, threads);
			break;
		case mxSINGLE_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL));
			cuda_image_thresh_single((float *) Y, (float *) X, N, (float *) T, P, threads);
			break;
		case mxINT8_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxINT8_CLASS, mxREAL));
			cuda_image_thresh_int8((char *) Y, (char *) X, N, (char *) T, P, threads);
			break;
		case mxINT16_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxINT16_CLASS, mxREAL));
			cuda_image_thresh_int16((short int *) Y, (short int *) X, N, (short int *) T, P, threads);
			break;
		case mxINT32_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxINT32_CLASS, mxREAL));
			cuda_image_thresh_int32((int *) Y, (int *) X, N, (int *) T, P, threads);
			break;
		case mxINT64_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxINT64_CLASS, mxREAL));
			cuda_image_thresh_int64((long long *) Y, (long long *) X, N, (long long *) T, P, threads);
			break;
		case mxUINT8_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxUINT8_CLASS, mxREAL));
			cuda_image_thresh_uint8((unsigned char *) Y, (unsigned char *) X, N, (unsigned char *) T, P, threads);
			break;
		case mxUINT16_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxUINT16_CLASS, mxREAL));
			cuda_image_thresh_uint16((unsigned short int *) Y, (unsigned short int *) X, N, (unsigned short int *) T, P, threads);
			break;
		case mxUINT32_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxUINT32_CLASS, mxREAL));
			cuda_image_thresh_uint32((unsigned int *) Y, (unsigned int *) X, N, (unsigned int *) T, P, threads);
			break;
		case mxUINT64_CLASS:
			Y = (void *) mxGetPr(plhs[0] = mxCreateNumericArray(2, dims, mxUINT64_CLASS, mxREAL));
			cuda_image_thresh_uint64((unsigned long long *) Y, (unsigned long long *) X, N, (unsigned long long *) T, P, threads);
			break;
	}
					
}
