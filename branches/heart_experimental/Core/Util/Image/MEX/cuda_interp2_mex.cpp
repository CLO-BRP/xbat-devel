#include "cutil.h"
#include "mex.h"
#include "cuda.h"
#include "cuda_runtime_api.h"
#include "texture_fetch_functions.h"
#include "cuda_interp2.h"
#include <stdio.h>
#include <stdlib.h>

static bool debug = false;

#define IMUL(a, b) __mul24(a, b)

//////////////////////////////////////////////////////////////////////////////////////
///         Main
//////////////////////////////////////////////////////////////////////////////////////

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) {
	// Check number of inputs
	if (nrhs !=4) mexErrMsgTxt("Must have four input arguments: data, rowpoints, colpoints, interp_mode");
	if (nlhs !=1) mexErrMsgTxt("Must have one output argument");
	
	// Check the class of input data
	if ( mxIsComplex(prhs[0]) || !mxIsClass(prhs[0],"double") ) {
		// try casting the input to double?
		mexErrMsgTxt("Input must be real, double type");
	}
	
	///// Allocate, set up data structures
	int OUTPUT_W, OUTPUT_H, OUTPUT_SIZE, INPUT_W, INPUT_H, INPUT_SIZE, POINTS_SIZE;
	
	double *input = mxGetPr(prhs[0]);
	INPUT_W = mxGetN(prhs[0]);
	INPUT_H = mxGetM(prhs[0]);
	INPUT_SIZE = INPUT_H * INPUT_W * sizeof(float);
	float *f_input;
	
	double *row_points = mxGetPr(prhs[1]);
	double *col_points = mxGetPr(prhs[2]);
	float2 *f_points;
    
    int interp_mode = (int) mxGetScalar(prhs[3]);
	
	///// Check if we're in "input points" or "number of points" mode
	if ( mxGetNumberOfElements(prhs[1]) == 1 ) {
		
		// number of points mode
		OUTPUT_W = (int) col_points[0];
		OUTPUT_H = (int) row_points[0];
		OUTPUT_SIZE = OUTPUT_W * OUTPUT_H * sizeof(float);
		POINTS_SIZE = 2*OUTPUT_SIZE;
		
		// we want N evenly spaced points from 0 to 1
		f_points = (float2 *)mxMalloc(POINTS_SIZE);
		
		for ( int r=0; r<OUTPUT_H; r++ ) {
			for ( int c=0; c<OUTPUT_W; c++ ) {
				f_points[c + OUTPUT_W*r].x = (float) c * (INPUT_W-1) / (OUTPUT_W-1) + 0.5f;
				f_points[c + OUTPUT_W*r].y = (float) r * (INPUT_H-1) / (OUTPUT_H-1) + 0.5f;
			}
		}
	}
	else {
		mexErrMsgTxt("Please input scalar number of rows, columns desired in output.");
		/*
		// input points directly mode
		OUTPUT_WIDTH = mxGetNumberOfElements(prhs[1]);
		// we load the points directly from the input
		f_points = (float *)mxMalloc(OUTPUT_WIDTH * sizeof(float));
		for ( int i=0; i<OUTPUT_WIDTH; i++ ) {
			f_points[i] = (float) input_points[i] * cmul + cadd;
		}
		*/
	}
	
    // Allocate output
	plhs[0] = mxCreateDoubleMatrix(OUTPUT_H, OUTPUT_W, mxREAL);
	double *output = mxGetPr(plhs[0]);
	float *f_output = (float *)mxMalloc(OUTPUT_SIZE);
	
	
	// Convert inputs from doubles to floats
	f_input = (float *)mxMalloc(INPUT_SIZE);
	for ( int r=0; r<INPUT_H; r++ ) {
		for ( int c=0; c<INPUT_W; c++ ) {
			f_input[c + INPUT_W*r] = (float) input[r + INPUT_H*c];
		}
	}
	
	// Process the data
    interp2_function(f_input, INPUT_W, INPUT_H, INPUT_SIZE, f_output, OUTPUT_W, OUTPUT_H, OUTPUT_SIZE, f_points, POINTS_SIZE, interp_mode);
	
    // Convert floats to doubles
	for ( int r=0; r<OUTPUT_W; r++ ) {
		for ( int c=0; c<OUTPUT_H; c++ ) {
			output[r + OUTPUT_H*c] = (double)f_output[c + OUTPUT_W*r];
		}
	}
	
    // Deallocate and return
	mxFree(f_input);
	mxFree(f_output);
	mxFree(f_points);
}
