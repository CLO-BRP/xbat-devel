function build_cuda_image_custom(global_opt)

% build_cuda_image_custom - non-standard filters
% ----------------------------------------------
%
% build_cuda_image_custom(global_opt)
%
% Input:
% ------
%  global_opt - build options struct, obtained from build_cuda_mex

%--
% handle input
%--

if ~nargin
	global_opt = build_cuda_mex;
end

%--
% build image padding MEX
%--

opt = global_opt;

% clear('cuda_image_pad_mex');

build_cuda_mex('image_pad', opt);

%--
% recompile padding code to create the linkable object file
%--

obj = 'cuda_image_pad.obj';

opt = global_opt; opt.link = false;
	
build_cuda_mex('image_pad', opt);

if exist('cuda_image_pad.cu.obj', 'file')
	% HACK: this ugly block resolves a discrepancy in the naming of output files between compilers in 32 and 64 bit environments 

	movefile('cuda_image_pad.cu.obj', 'cuda_image_pad.obj');
end

%--
% build filter MEXs
%--

%--
% median
%--
% NOTE: CUDA median filter does not work yet
%
% opt = build_cuda_mex;
% 
% opt.mex = {obj};
% 
% clear ('cuda_median_filter');
% 
% build_cuda_mex('median_filter', opt);

%--
% summed area table
%--

clear('cuda_sat');

build_cuda_mex('sat');

%--
% anisotropic diffusion
%--

opt = global_opt;

opt.mex = {obj};

clear('cuda_anisodiff');

build_cuda_mex('anisodiff', opt);

%--
% mean and deviation
%--

opt = global_opt;

opt.mex = {obj};

clear('cuda_mean_dev_mex');

build_cuda_mex('mean_dev', opt);

%--
% linear filter
%--

opt = global_opt;

opt.mex = {obj};

clear('cuda_linear_filter');

build_cuda_mex('linear_filter', opt);

%--
% morphological filter
%--

opt = global_opt;

opt.mex = {obj};

clear('cuda_linear_filter');

build_cuda_mex('morph_filter', opt);




