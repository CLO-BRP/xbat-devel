/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.  Users and possessors of this source code
 * are hereby granted a nonexclusive, royalty-free license to use this code
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.   This source code is a "commercial item" as
 * that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of
 * "commercial computer  software"  and "commercial computer software
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 *
 * Any use of this source code in individual and commercial software must
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */


#include "cufft.h"
#include "cutil.h"
#include "mex.h"
#include "cuda.h"
#include "cuda_runtime.h"

#include "cuda_conv2.h"

////////////////////////////////////////////////////////////////////////////////
// Main program
////////////////////////////////////////////////////////////////////////////////
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) {
    // Check inputs
    if (nrhs !=2) mexErrMsgTxt("Must have two input arguments: data, kernel");
    if (nlhs !=1) mexErrMsgTxt("Must have one output argument");
    if ( mxIsComplex(prhs[0]) || !mxIsClass(prhs[0], "double")) {
        mexErrMsgTxt("Input must be real, double type");
    }
    if ( mxIsComplex(prhs[1]) || !mxIsClass(prhs[1], "double")) {
        mexErrMsgTxt("Input must be real, double type");
    }
    int KERNEL_W, KERNEL_H, DATA_W, DATA_H, FFT_W, FFT_H, PADDING_H, PADDING_W;
    
    // Data dimensions
    DATA_H = (int) mxGetM(prhs[0]);
    DATA_W = (int) mxGetN(prhs[0]);
    
    // Kernel dimensions
    KERNEL_H = (int) mxGetM(prhs[1]);
    KERNEL_W = (int) mxGetN(prhs[1]);
    
    // Width and height of padding for "clamp to border" addressing mode
    PADDING_W = KERNEL_W - 1;
    PADDING_H = KERNEL_H - 1;
    
    // Derive FFT size from data and kernel dimensions
    FFT_W = calculateFFTsize(DATA_W + PADDING_W);
    FFT_H = calculateFFTsize(DATA_H + PADDING_H);
    
    plhs[0] = mxCreateDoubleMatrix(DATA_H, DATA_W, mxREAL);
    double *output = mxGetPr(plhs[0]);
    
    double *input_kernel = mxGetPr(prhs[1]);
    double *input_data   = mxGetPr(prhs[0]);
    
    // If there's just too much data to do in a single run, we need to break it up, eh?
    // how much "too big" is it?
    int MAX_FFT_W = 1048576/FFT_H;
    if ( FFT_W > MAX_FFT_W ) { // we need to break up the data
        
        int STRIP_W = MAX_FFT_W-KERNEL_W+1;
        int STRIP_SIZE = STRIP_W * DATA_H * sizeof(double);
        int OVERLAP_SIZE = DATA_H * KERNEL_W/2 * sizeof(double);
        
        double *strip_output = (double *)mxMalloc(STRIP_SIZE);
        double *strip_input  = (double *)mxMalloc(STRIP_SIZE);
        
        int REMAIN_W = DATA_W; // counter showing how much of the data remains to be processed
        
        // Do the first strip
        cudaMemcpy(strip_input, input_data, STRIP_SIZE, cudaMemcpyHostToHost);
        fftFunction(strip_output, strip_input, input_kernel, DATA_H, STRIP_W, KERNEL_H, KERNEL_W);
        cudaMemcpy(output, strip_output, STRIP_SIZE, cudaMemcpyHostToHost);
        
        REMAIN_W -= STRIP_W - KERNEL_W/2; // need some overlap on the right..
        
        while ( REMAIN_W > STRIP_W ) {
            // read the strip
            cudaMemcpy(strip_input, input_data + DATA_H*(DATA_W-REMAIN_W-KERNEL_W/2), STRIP_SIZE, cudaMemcpyHostToHost);
            // convolve the strip
            fftFunction(strip_output, strip_input, input_kernel, DATA_H, STRIP_W, KERNEL_H, KERNEL_W);
            // copy the result into the output
            cudaMemcpy(output + DATA_H*(DATA_W-REMAIN_W), strip_output+DATA_H*KERNEL_W/2, STRIP_SIZE-OVERLAP_SIZE, cudaMemcpyHostToHost);
            
            // set the remaining number of columns
            REMAIN_W -= STRIP_W - KERNEL_W;
        }
        
        // Now we have to do the remaining edge strip
        int LAST_STRIP_SIZE = REMAIN_W * DATA_H * sizeof(double);
        double *last_strip_output = (double *)mxMalloc(LAST_STRIP_SIZE+OVERLAP_SIZE);
        double *last_strip_input  = (double *)mxMalloc(LAST_STRIP_SIZE+OVERLAP_SIZE);
        
        cudaMemcpy(last_strip_input, input_data+DATA_H*(DATA_W-REMAIN_W-KERNEL_W/2), LAST_STRIP_SIZE+OVERLAP_SIZE, cudaMemcpyHostToHost);
        fftFunction(last_strip_output, last_strip_input, input_kernel, DATA_H, REMAIN_W+KERNEL_W/2, KERNEL_H, KERNEL_W);
        cudaMemcpy(output + DATA_H*(DATA_W-REMAIN_W), last_strip_output+DATA_H*KERNEL_W/2, LAST_STRIP_SIZE, cudaMemcpyHostToHost);
        
        mxFree(strip_output);
        mxFree(strip_input);
        mxFree(last_strip_output);
        mxFree(last_strip_input);
    }
    else {
        fftFunction(output, input_data, input_kernel, DATA_H, DATA_W, KERNEL_H, KERNEL_W);
    }
}