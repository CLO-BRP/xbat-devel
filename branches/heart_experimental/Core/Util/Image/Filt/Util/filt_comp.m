function H = filt_comp(F, k)

% filt_comp - compose filter from decomposition
% ---------------------------------------------
%
% H = filt_comp(F, k)
%
% Input:
% ------
%  F - filter decomposition structure
%  k - order of approximation (def: 0, full composition)
%
% Output:
% -------
%  H - filter mask

%--
% set rank of approximation
%--

if nargin < 2
	k = 0;
end

%--
% check rank of approximation
%--

if (k > 0) && (k > length(F.S))
	k = 0;
end

%--
% compute full or partial synthesis
%--

% NOTE: there is no need to compose full filter since the mask is stored

if k == 0
	H = F.H;
else
	H = F.Y(:, 1:k) * diag(F.S(1:k)) * F.X(:, 1:k)';	
end