function R = test_image_thresh(X, T, n)

% test_image_thresh - accelerated thresholding code
% -------------------------------------------------
%
% R = test_image_thresh(X, T, n)
%
% Input:
% ------
%  X - input image
%  T - threshold array
%  n - number of iterations
%
% Output:
% -------
%  R - timing and accuracy results

%--
% handle input
%--

if (nargin < 3) || isempty(n)
    n = 10;
end

if nargin < 2
	T = 1:100;
end

if ~nargin
	X = 100 * rand(500);
end

%--
% run tests
%--

t1 = zeros(n, 1); t2 = t1; E = zeros(n, 2);

for k = 1:n
    tic; Y1 = image_thresh(X, T, 'mex_old'); t1(k) = toc;
	
    tic; Y2 = image_thresh(X, T, 'cuda'); t2(k) = toc;
	
    E(k,:) = fast_min_max(Y1 - Y2);
end

R = [t1, t2, (t1./t2), E];

if ~nargout
	disp(' ');
	
	% NOTE: this is a cumbersome way of displaying a table
	
	fprintf('TIME\t\tCUDA-TIME\tSPEEDUP\t\tERROR-MIN\tERROR-MAX\n');
	
	for k = 1:n
		fprintf('%f\t', R(k, :)); fprintf('\n');
	end

	disp(' '); clear R;
end
