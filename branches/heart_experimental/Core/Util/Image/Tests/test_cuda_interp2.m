function [speedup nsize] = test_cuda_interp2()
%TESTCUDAINTERP2 Benchmarks GPU vs CPU bilinear interpolation.
%   [SPEEDUP DSIZE KSIZE] = TESTCUDACONV() runs 2-D bilinear
%   interpolations on random data using both CUDAINTERP2 and INTERP2.
%
%   SPEEDUP contains the relative speed of CONV2 and CUDACONV.
%   Thus if SPEEDUP(i,j) = 5, CUDAINTERP2 completed interpolation
%   of a random 300x300 matrices to NSIZE(i) x NSIZE(i) in 1/10 
%   the time of INTERP2.
%
%   NSIZE is the sqrt of the interpolated data size.

orig_size		= 100;
new_sizes		= 200:100:900;
nrepeats		= 3;

gpu_results		= zeros(3,numel(new_sizes));
cpu_results		= zeros(3,numel(new_sizes));

% fig;

data = rand(orig_size);

load clown;
data = X(1:100,1:100);

for mode = 0:2
    if mode == 0
        mode_name = '*nearest';
    end
    if mode == 1
        mode_name = '*linear';
    end
    if mode == 2
        mode_name = '*spline';
    end
    for ns = 1:numel(new_sizes)
        disp(sprintf('New data size: %d elements\n',new_sizes(ns)^2));
        
        gpu_temp = zeros(1,nrepeats);
        cpu_temp = zeros(1,nrepeats);
        
        for t = [1 1:nrepeats]
            tic;
            gpu_result = cuda_interp2(data,new_sizes(ns),new_sizes(ns),mode_name);
            gpu_temp(t) = toc;
                        
            tic;
            [x y] = meshgrid(linspace(1,orig_size,new_sizes(ns)),linspace(1,orig_size,new_sizes(ns)));
            cpu_result = interp2(data,x,y,mode_name);
            cpu_temp(t) = toc;
            
            % image_view(abs(gpu_result - cpu_result));
        end
                
        gpu_results(mode+1, ns) = mean(gpu_temp);
        cpu_results(mode+1, ns) = mean(cpu_temp);
    end
end

speedup = cpu_results./gpu_results;
nsize = new_sizes.^2;

figure;
plot(nsize,speedup,'-*');
xlabel('New data size (# of elements)')
ylabel('Relative speed (GPU/CPU)')

legend('Nearest','Bilinear','Bicubic','Location','NorthWest');

title('Interpolation (GPU vs CPU)')

%dlmwrite('performance2.dat',speedup,',');