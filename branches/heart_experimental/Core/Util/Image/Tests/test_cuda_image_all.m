function passed = test_cuda_image_all()

% A convenience function to test all CUDA image processing functions

passed = true;

% Test CUDA image pading, this is used by all neighborhood functions
if passed
    passed = test_cuda_image_pad;
end

if ~passed
    disp('CUDA image pad test FAILED!');
end

% Test CUDA linear filtering
if passed
    passed = test_linear_filter;
end

if ~passed
    disp('CUDA image linear filter test FAILED!');
end

% Test CUDA morphological filtering
if passed
    passed = test_cuda_morph;
end

if ~passed
    disp('CUDA morphological filter test FAILED!');
end

% Test CUDA anisotropic diffusion

% NOTE: Not working yet

% if passed
%     passed = test_anisodiff;
% end
% 
% if ~passed
%     disp('CUDA anisotropic diffusion  test FAILED!');
% end



