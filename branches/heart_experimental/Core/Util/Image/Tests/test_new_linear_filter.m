function all_passed = test_new_linear_filter(X)

% bench_all_filt - over a range of kernel sizes
% ---------------------------------------------------------------------
%
% bench_all_filt(run_cuda)
%
% Input:
% ------
%  X - image to filter

%--
% handle input and setup
%--


if ~nargin || isempty(X);
    
    % X = 255 * rand(1024);
    load clown;
end

f = [3 7 11 15 21 25 33 47 65 79 101 251 377];

%--
% run tests
%--

test_count = 1;

disp(' ');

for j = 1:numel(f)
    
    kern = 100 * randn(f(j), f(j));
    
    try
        start = tic; Y1 = old_linear_filter(X, kern); elapsed1(test_count) = toc(start);
    catch
        elapsed1(test_count) = inf; nice_catch;
    end
        
    cuda_enabled true;
    try
        start = tic; Y2 = linear_filter(X, kern); elapsed2(test_count) = toc(start);
    catch
        elapsed2(test_count) = inf; nice_catch;
    end
        
    p = ceil(0.5 * (size(kern) - 1));
    
    max_error = max(vec(abs(Y1(:) - Y2(:))));
    
    disp([
        'size = [', int2str(f(j)), ', ', int2str(f(j)), '],', ...
        ' err = ' num2str(max_error, '%0.4g') ' speedup = ' num2str(elapsed1(test_count)/elapsed2(test_count)) ...
        ]);
    
    
    test_count = test_count + 1;
end

fig;

semilogy(f, elapsed1, 'c', f, elapsed2, 'm');

xlabel('Square Kernel Size');
ylabel('Elapsed Time');

legend('OLD Linear Filter', 'NEW Linear Filter');

title(['Data size ', num2str(size(X,1)), 'x', num2str(size(X,2))]);

