function [result, info] = bench_cuda_image_pad(varargin)

% bench_cuda_image_pad - benchmark cuda image pad against Matlab implementation
% -------------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'exhaustive' - run extensive benchmark instead of fast benchmark
%  'display' - display CUDA padded image for each test
%  'verbose' - print status output
%  'silent' - print nothing
%  anything else - assumed to be type conversion function handle ie. 'double' 
%
% Output
% ------
%  result = 1 if passed, 0 if failed

% NOTE: default is fast bench on double precision data with no messages
% and no display, equivalent to
% 'test_cuda_image_pad('quick', 'silent', 'double')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0; display = 0;
type_func = @double;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case {'exhaustive', 'full'}, fast = 0;
            
        case 'silent', verbose = 0;
            
        case {'verbose', 'verb'}, verbose = 1;
            
        case 'display', display = 1;
            
        otherwise, type_func = str2func(varargin{i});
    end
end

%----------------------
% SETUP
%----------------------

%--
% load test data
%--

% NOTE: we load data and cast for type we are testing

load clown; % NOTE: this is an Matlab sample image

X = type_func(X);

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if isa(X, 'double') && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end
%--
% set parameter ranges
%--

% Padding sizes

if fast
    p = [0 1 5 10];  % Arbitrary values for 'quick' test
    q = [0 1 5 10];
else
    p = round(rand(1,20) * 100);
    q = round(rand(1,20) * 100);
end


%----------------------
% RUN TEST
%----------------------

%--
% Compare CUDA image pad to Matlab implementation over range of parameters
%--

if display
	par = fig;
end

for i = 1:numel(p)
    
    for j = 1:numel(q)
        
        if verbose
            fprintf('p %d/q %d', p(i), q(j));
        end
        
        tic; [i0, e0] = cuda_image_pad(X, [p(i) q(j)], -1); cuda_time(i, j) = toc;
        
        if e0
            fprintf('CUDA Error: %d:%s\n', num2str(e0), cuda_decode_error(e0));
            result = 0;
        end
        
        if display
			figure(par);
			
            image_view(double(i0));
        end
        
        % Matlab image pad
        
        tic; i1 = image_pad(X, [p(i) q(j)], -1); ml_time(i, j) = toc;
        
        % Report speedup
		if verbose
			fprintf('  %1.2f\n', ml_time(i,j) / cuda_time(i,j));
		end
        
    end % for q
    
end % for p

result.cuda_time = cuda_time;

result.ml_time = ml_time;
