function all_passed = test_cuda_conv2(X, k)

% test_cuda_conv2 - for variable kernel sizes
% ---------------------------------------------------------------------
%
% test_cuda_conv2(X, k)
%
% Input:
% ------
%  X - input image (def: random image)
%  k - kernel sizes (def: 1:2:13)

%--
% handle input and setup
%--

if nargin < 2
    f = [33 64 129 258 513];
end

if ~nargin || isempty(X);
    
    X = 255 * rand(1024);
end

all_passed = true;

% Make sure cuda is enabled

cuda_enabled true;

%--
% run tests
%--

test_count = 1;

disp(' ');

for j = 1:numel(f)
    
    for k = 1:numel(f)
        
        disp([num2str(f(j)), 'x', num2str(f(k))]);
        
        for l = 1:3
                        
            if l == 1
                type = 'full';
            end
            if l == 2
                type = 'same';
            end
            if l == 3
                type = 'valid';
            end
            
            kern = 100 * randn(f(j), f(k)); failure = false;
            
            try
                start = tic; Y1 = conv2(X, kern, type); elapsed1 = toc(start);
            catch
                failure = true; elapsed1 = inf; nice_catch;
            end
            
            try
                start = tic; Y2 = olam_conv2(X, kern, type); elapsed2 = toc(start);
            catch
                failure = true; elapsed2 = inf; nice_catch;
            end
            
            max_error = max(vec(abs(Y1(:) - Y2(:))));
            
            passed = ~failure && (max_error < numel(kern) * 10^-8);
            
            if ~passed
                all_passed = false;
            end
            
            % NOTE: this would probably be better using 'fprintf'
            
            disp([
                'size = [', int2str(f(j)), ', ', int2str(f(k)), '], passed = ', int2str(passed), ', max_error = ' num2str(max_error), ...
                ', elapsed = [', sec_to_clock(elapsed1), ', ', sec_to_clock(elapsed2), '], speed = ', num2str(elapsed1/elapsed2) ...
                ]);
            
            speedup(test_count) = elapsed1/elapsed2;
            test_count = test_count + 1;
            
        end
        
    end
end


disp(['Median speedup: ', num2str(median(speedup(:)))]);
disp(['Max speedup: ', num2str(max(speedup(:)))]);

