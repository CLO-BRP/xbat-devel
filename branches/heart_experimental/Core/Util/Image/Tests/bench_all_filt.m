function all_passed = bench_all_filt(X)

% bench_all_filt - over a range of kernel sizes
% ---------------------------------------------------------------------
%
% bench_all_filt(run_cuda)
%
% Input:
% ------
%  X - image to filter

%--
% handle input and setup
%--

if nargin < 2
    f = [3 7 15 21 25 33 65 101 251];
end

if ~nargin || isempty(X);
    
    % X = 255 * rand(1024);
    load clown;
end

all_passed = true;

%--
% run tests
%--

test_count = 1;

disp(' ');

for j = 1:numel(f)
    
    kern = 100 * randn(f(j), f(j));
    
    % Matlab conv2
    
    try
        start = tic; Y1 = conv2(X, kern,'same'); elapsed1(test_count) = toc(start);
    catch
        elapsed1(test_count) = inf; nice_catch;
    end
    
    % CUDA accelerated conv2
    
    cuda_enabled true;
    try
        start = tic; Y2 = cuda_conv2(X, kern); elapsed2(test_count) = toc(start);
    catch
        elapsed2(test_count) = inf; nice_catch;
    end
    
    % Overlap Add Method conv2
    
    try
        start = tic; Y3 = olam_conv2(X, kern); elapsed3(test_count) = toc(start);
    catch
        elapsed3(test_count) = inf; nice_catch;
    end
    
    % C MEX linear filter
    
    cuda_enabled false;
    try
        start = tic; Y4 = linear_filter(X, fliplr(flipud(kern)), 1, 0); elapsed4(test_count) = toc(start);
    catch
        elapsed4(test_count) = inf; nice_catch;
    end
    
    % CUDA accelerated linear filter
    
    cuda_enabled true;
    try
        start = tic; Y5 = linear_filter(X, fliplr(flipud(kern)), 1, 0); elapsed5(test_count) = toc(start);
    catch
        elapsed5(test_count) = inf; nice_catch;
    end
    
    p = ceil(0.5 * (size(kern) - 1));
    
    Y3 = Y3(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    
    V1 = Y1(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    V2 = Y2(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    V3 = Y3(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    V4 = Y4(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    V5 = Y5(p(1) + 1:end - p(1), p(2) + 1:end - p(2));
    
    max_error2 = max(vec(abs(V1(:) - V2(:))));
    max_error3 = max(vec(abs(V1(:) - V3(:))));
    max_error4 = max(vec(abs(V1(:) - V4(:))));
    max_error5 = max(vec(abs(V1(:) - V5(:))));
    
    disp([
        'size = [', int2str(f(j)), ', ', int2str(f(j)), '],', ...
        ' CUDA conv2 err = ' num2str(max_error2, '%0.4g'), ', speed = ', num2str(elapsed1(test_count)/elapsed2(test_count), '%0.4f'), ...
        ' OLAM err = ' num2str(max_error3, '%0.4g'), ', speed = ', num2str(elapsed1(test_count)/elapsed3(test_count), '%0.4f'), ...
        ' LF err = ' num2str(max_error4, '%0.4g'), ', speed = ', num2str(elapsed1(test_count)/elapsed4(test_count), '%0.4f') ...
        ' CUDA LF err = ' num2str(max_error5, '%0.4g'), ', speed = ', num2str(elapsed1(test_count)/elapsed5(test_count), '%0.4f') ...
        ]);
    
    
    test_count = test_count + 1;
end

fig;

semilogy(f, elapsed1, 'y', f, elapsed2, 'b', f, elapsed3, 'm', f, elapsed4, 'c', f, elapsed5, 'r');

xlabel('Square Kernel Size');
ylabel('Elapsed Time');

legend('Conv2', 'CUDA Conv2', 'OLAM Conv2', 'Linear Filter', 'CUDA LF');

title(['Data size ', num2str(size(X,1)), 'x', num2str(size(X,2))]);

