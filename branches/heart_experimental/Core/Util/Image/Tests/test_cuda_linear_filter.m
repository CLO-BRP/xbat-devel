function [result, info] = test_cuda_linear_filter(varargin)

% test_cuda_linear_filter
% -------------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'full' - run extensive benchmark instead of fast benchmark
%  'display' - display CUDA padded image for each test
%  'verbose' - print status output
%  'silent' - print nothing
%  anything else - assumed to be type conversion function handle ie. 'double'
%
% Output
% ------
%  result = 1 if passed, 0 if failed

% NOTE: default is fast bench on double precision data with no messages
% and no display, equivalent to
% 'test_cuda_linear_filter('quick', 'silent', 'double')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0; display = 0;
type_func = @double;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case {'exhaustive' 'full'}, fast = 0;
            
        case 'silent', verbose = 0;
            
        case 'verbose', verbose = 1;
            
        case 'display', display = 1;
            
        otherwise, type_func = str2func(varargin{i});
    end
end

%----------------------
% SETUP
%----------------------

%--
% initialize output
%--

result = 1;

%--
% load test data
%--

% NOTE: we load data and cast to type we are testing

load clown; % NOTE: this creates the 'X' variable below

X = [X, X; X, X]; X = [X; X]; %#ok<NODEF>

Xt = type_func(X);

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if isa(Xt, 'double') && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end

%--
% set parameter ranges
%--

% Kernel sizes

if fast
    sizes = [3];
	iter = [1];
else
    sizes = 3:2:31;
	iter = [1 3 5];
end

%----------------------
% RUN TEST
%----------------------

%--
% Compare CUDA filter function to MEX implementation over range of parameters
%--

if display
    par = fig;
end

separable = false;

for s = 1:numel(sizes)
	
	for k = 1:numel(iter)
    
	F = fspecial('gaussian', [sizes(s) sizes(s)]);
    
    if verbose
        fprintf('Kernel %02dx%02d %02d times:', sizes(s), sizes(s), iter(k));
	end

	try
		tic; i0 = cuda_linear_filter(Xt, F, iter(k)); cuda_time(s) = toc;
	catch
		result = 0; nice_catch(lasterror, 'CUDA linear filter failed.');
	end

    if display
        figure(par);
        
        image_view(double(i0));
    end
    
    tic; i1 = linear_filter(X, F, iter(k)); ml_time(s) = toc;
        
    % Set return code if we have an error
    
    % NOTE: Arbitrary test for max error > mean input value / 300
    
    err = max(max(i0 - i1));
    
    if err > 10^-6
        result = 0;
        if verbose
            fprintf(' FAILED! Error = %8.5f', err);
        end
    else
        if verbose
            fprintf(' Passed, Error = %8.5f', err);
        end
    end
    
    % Report speedup
    if verbose
        if result
            fprintf(' Speedup = %3.2f\n', ml_time(s) / cuda_time(s));
        else
            fprintf('\n');
        end
	end
    
	end % for k
	
end % for s

info.cuda_time = cuda_time;
info.ml_time = ml_time;
