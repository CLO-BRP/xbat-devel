function all_passed = test_cuda_conv2(X)

% test_cuda_conv2 - over a range of kernel sizes
% ---------------------------------------------------------------------
%
% all_passed = test_cuda_conv2(X)
%
% Input:
% ------
%  X - image to filter

%--
% handle input and setup
%--


if ~nargin || isempty(X);
    
    % X = 255 * rand(1024);
    load clown;
end

f = [3 7 11 15 21 25 33 47 65 79 101];

%--
% run tests
%--

test_count = 1;

disp(' ');

for j = 1:numel(f)
    
    kern = 100 * randn(f(j), f(j));
    
    try
        start = tic; Y1 = conv2(X, kern, 'same'); elapsed1(test_count) = toc(start);
    catch
        elapsed1(test_count) = inf; nice_catch;
    end
        
    cuda_enabled true;
    try
        start = tic; Y2 = cuda_conv2(X, kern); elapsed2(test_count) = toc(start);
    catch
        elapsed2(test_count) = inf; nice_catch;
    end
        
    p = ceil(0.5 * (size(kern) - 1));
    
    % NOTE: Just compare valid regions becuase CUDA implementation does not
    % support all shape types
    
    V1 = Y1(p(1) + 1:end - p(1),p(2) + 1:end - p(2));
    V2 = Y2(p(1) + 1:end - p(1),p(2) + 1:end - p(2));
        
    max_error = max(vec(abs(V1(:) - V2(:))));
    
    disp([
        'size = [', int2str(f(j)), ', ', int2str(f(j)), '],', ...
        ' err = ' num2str(max_error, '%0.4g') ' speedup = ' num2str(elapsed1(test_count)/elapsed2(test_count)) ...
        ]);
    
    
    test_count = test_count + 1;
end

fig;

semilogy(f, elapsed1, 'c', f, elapsed2, 'm');

xlabel('Square Kernel Size');
ylabel('Elapsed Time');

legend('Conv2', 'CUDA Conv2');

title(['Data size ', num2str(size(X,1)), 'x', num2str(size(X,2))]);

