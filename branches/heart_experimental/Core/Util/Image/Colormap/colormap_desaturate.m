function C = colormap_desaturate(C)

% NOTE: we convert to HSV space and manipulate both saturation and value

C = rgb_to_hsv(C); 

mask = linspace(0, 1, size(C, 1))';

C(:, 2) = mask .* C(:, 2); 

if all(C(1, :) < 0.4)
	C(:, 3) = mask .* C(:, 3); disp darken;
elseif all(C(1, :) > 0.6)
	C(:, 3) = flipud(mask) .* C(:, 3); disp lighten;
end

C = hsv_to_rgb(C);