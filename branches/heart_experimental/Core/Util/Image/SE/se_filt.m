function F = se_filt(SE)

% se_filt - supported difference filters decomposition
% ----------------------------------------------------
%
% F = se_filt(SE)
%

% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  F - difference filters supported by SE

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% get displacement vectors
%--

V = se_vec(SE);

%--
% create finite difference filters
%--

for k = 1:size(V,1)
	
	v = abs(V(k,:)) + 1;
	
	f = zeros(2*abs(V(k,:)) + 1);
	f(v(1),v(2)) = -1;
	f(v(1) + V(k,1),v(2) + V(k,2)) = 1;
	
	F{k} = f;
	
end

for k = size(V,1):-1:1
	if (sum(abs(F{k})) == 1)
		F{k} = [];
	end
end
	

