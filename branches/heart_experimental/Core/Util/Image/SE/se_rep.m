function r = se_rep(SE)

% se_rep - representation type of structuring element
% ---------------------------------------------------
% 
% r = se_rep(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  r - representation type 'mat', 'vec', or ''

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
% $Revision: 5169 $
%--------------------------------

[m, n] = size(SE);

if (n == 2) && all(round(SE(:)) == SE(:))
 
	r = 'vec';

elseif rem(m, 2) && rem(n, 2) && all((SE(:) ~= 0) == SE(:))

	r = 'mat';
else
	r = ''; % not a structuring element
	
	if ~nargout
		warning('Input is not a structuring element.');
	end
end
