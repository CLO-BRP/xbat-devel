function types = known_image_types

types = {'jpg', 'jpeg', 'png', 'tif', 'tiff'};