function Y = cuda_morph_filter(type, X, SE, b, n, varargin)

% cuda_morph_filter - dispatcher for CUDA morphological operations
% ----------------------------------------------------------------
%
% Y = cuda_morph_filter(type, X, SE, b, n, field, value, ... )
%
% Input:
% ------
%  type - of operation
%  X - input image 
%  SE - structuring element
%  b - boundary behavior (def: -1)
%  n - iterations (def: 1)
%  field, value - pairs for some operations
%
% Output:
% -------
%  Y - filtered image

%--
% handle input
%--

if nargin < 5
	n = 1;
end

if nargin < 4
	b = -1;
end

if ~isempty(varargin)
	[field, value] = get_field_value(varargin); %#ok<NASGU,ASGLU>
end

known = {'erode', 'dilate', 'gradient', 'open', 'close', ...
	'tophat-self', 'tophat-black', 'tophat-white'...
	'gradient-inner', 'gradient-outer'};

if ~string_is_member(type, known)
	error(['Unknown operation type ''', type, '''.']);
end

%--
% dispatch morphological operation
%--

SE = uint8(SE); [row, col] = size(SE);

separable = all(SE(:) == 1); code = cuda_morph_code(type);

switch ndims(X)
	case 2
		[Y, status] = cuda_morph_filter_mex(X, SE, row, col, separable, code, n, b);
		
		handle_cuda_failure(status)
		
	case 3
		Y = zeros(size(X), class(X));
		
		for k = 1:size(X, 3)
			[Y(:,:,k), status] = cuda_morph_filter_mex(X(:,:,k), SE, row, col, separable, code, n, b);
			
			handle_cuda_failure(status)
		end
end
		

%-------------------------------
% CUDA_MORPH_CODE
%-------------------------------

function code = cuda_morph_code(name)

% cuda_morph_code - translation
% -----------------------------
%
% code = cuda_morph_code(name)
%
% Input:
% ------
%  name - of operation
%
% Output:
% -------
%  code - integer, used by 'cuda_morph_filter_mex'

switch name
	case 'null', code = 0;
		
	case 'erode', code = 1;
		
	case 'dilate', code = 2;
		
	case 'open', code = 3;
		
	case 'close', code = 4;
		
	case 'tophat-white', code = 5;
        
    case 'tophat-self', code = 6;
		
	case 'tophat-black', code = 7;
		
	case 'gradient', code = 8;
   
    case 'gradient-inner', code = 9;
        
    case 'gradient-outer', code = 10;
end

