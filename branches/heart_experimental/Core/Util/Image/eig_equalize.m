
% scr_eig 
%--
% load image
%--

X = load_rgb;

%--
% compute eigencolor expansion
%--

[Y,V,c] =  rgb_to_eig(X);

% 	%--
% 	% equalize chromatic channels
% 	%--
% 	
% 	for k = 1:2
% 		Y(:,:,k) = hist_equalize(Y(:,:,k));
% 	end
	
	%--
	% shape chromatic channels
	%--
	
	for k = 1:2
		
		b = fast_min_max(Y(:,:,k));
		hc = linspace(b(1),b(2),256);
		x = linspace(0,1,256);
		h = (x.*(1 - x)).^3;
		
		Y(:,:,k) = hist_specify(Y(:,:,k),h,hc);
		
	end

%--
% return to rgb representation
%--

X_new = eig_to_rgb(Y,V,c);


%--
% display input and output
%--

fig; image_view(X); 

fig; image_view(X_new);




