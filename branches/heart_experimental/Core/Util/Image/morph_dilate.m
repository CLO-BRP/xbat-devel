function Y = morph_dilate(X, SE, n, b)

% morph_dilate - morphological dilation
% -------------------------------------
% 
% Y = morph_dilate(X, SE, n, b)
%   = morph_dilate(X, SE, Z, b)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  n - iterations of operation (def: 1)
%  Z - computation mask image (def: [])
%  b - boundary behavior (def: -1)
%
% Output:
% -------
%  Y - dilated image
%
% See also: morph_erode, morph_gradient, image_extrema

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%-------------------
% HANDLE INPUT
%-------------------

% NOTE: this checks for (MATLAB) handle input!

[X, N, tag, flag, g, h] = handle_input(X, inputname(1));

%--
% set boundary behavior
%--

if nargin < 4
	b = -1;
end

%--
% iteration or mask
%--

if (nargin < 3) || isempty(n)
	
	n = 1; Z = [];
else
	Z = [];
	
	switch tag
		case {'IMAGE_GRAY', 'IMAGE_GRAY_U8'}
			[r, c] = size(X);
			
		case {'IMAGE_RGB', 'IMAGE_RGB_U8', 'IMAGE_NDIM', 'IMAGE_NDIM_U8'}
			[r, c, s] = size(X);
			
		case 'IMAGE_STACK'
			[r, c] = size(X{1});
            
        otherwise
            [r, c] = size(X);
	end
		
	if all(size(n) == [r, c])
		Z = n; n = 1;
	end
end

%--
% check for trivial processing
%--

% convert structuring element to matrix and check for scalar

if length(se_mat(SE)) == 1
	
	Y = X; return;
end 

% invoke CUDA implementation if possible

% NOTE: the code below will remember where CUDA morph erode failed and not delegate

B = se_mat(SE); state.data = {class(X), size(B)};

if isempty(Z) && cuda_enabled && ~cuda_has_failed(mfilename, state)
	try
		Y = cuda_morph_filter('dilate', X, B, b, n); return;
	catch		
		cuda_has_failed(mfilename, state, lasterror);
		
		clear cuda_morph_filter_mex; % TODO: could this be integrated into 'cuda_has_failed'? probably not simple
	end
end

%-------------------
% COMPUTE
%-------------------

if string_contains(tag, 'STACK')
	
	Y = cell(size(X));
	
	for k = 1:numel(X)
		
		if isempty(Z)
			Y{k} = morph_dilate(X{k}, SE, n, b);
		else
			Y{k} = morph_dilate(X{k}, SE, Z, b);
		end
	end
	
elseif string_contains(tag, 'RGB') || string_contains(tag, 'NDIM')
	
	Y = zeros(size(X), class(X));
	
	for k = 1:size(X, 3)
		
		if isempty(Z)
			Y(:, :, k) = morph_dilate(X(:, :, k), SE, n, b);
		else
			Y(:, :, k) = morph_dilate(X(:, :, k), SE, Z, b);
		end
	end
	
else
	% NOTE: we decompose the structuring element, this contains expected speed-up (in 'ratio' field)
	
	SEQ = se_decomp(SE);
		
	if SEQ.ratio > 1.5
		
		if isempty(Z)
			for j = 1:n
				Y = dilate_decomp(X, SEQ, b); X = Y;
			end
		else
			Y = dilate_decomp(X, uint8(B), b, uint8(Z));
		end
	else	
		pq = se_supp(SE); SE = se_mat(SE);
		
		if isempty(Z)
			for j = 1:n
				X = image_pad(X, pq, b);
				
				Y = morph_dilate_mex(X, uint8(SE)); X = Y;
			end
		else
			X = image_pad(X, pq, b); Z = image_pad(Z, pq, 0);
			
			Y = morph_dilate_mex(X, uint8(SE), uint8(Z));
		end
	end
	
end

%--
% display output
%--

if flag && view_output
	
	switch view_output
		
		case 1
			figure(h);
			set(g, 'cdata', Y);
			set(gca, 'clim', fast_min_max(Y));
			title_edit(['\Er \pb{' N '}']);
			
		otherwise
			fig;
			image_view(Y);
			title_edit(['\Er \pb{' N '}']);
			
	end
end

%---------------------------------------
% GET_FILTER_KEY
%---------------------------------------

function key = get_filter_key(F)

if isstruct(F)
	key = ['separable', int2str(size(F.H, 1)), 'x', int2str(size(F.H, 2))];
else
	key = ['direct', int2str(size(F, 1)), 'x', int2str(size(F, 2))];
end

%----------------------------------------------
% DILATE_DECOMP
%----------------------------------------------

function Y = dilate_decomp(X, SEQ, b, Z)

% dilate_decomp - dilation using decomposed structuring element
% -------------------------------------------------------------
%
% Y = dilate_decomp(X, SEQ, b, Z)
%
% Input:
% ------
%  X - input image
%  SEQ - decomposed structuring element
%  b - boundary condition flag
%  Z - computation mask
%
% Output:
% -------
%  Y - dilated image

%--
% set mask
%--

if (nargin < 4)
	Z = [];
end

%--
% compute according to mask
%--

if isempty(Z)
	
	%--
	% dilate with first line if needed
	%--
	
	if length(SEQ.line{1}) > 1
		pq = se_supp(SEQ.line{1});
		
		Y = morph_dilate_mex( ...
			image_pad(X, pq, b), ...
			uint8(SEQ.line{1}) ...
		);
	else
		Y = X;
	end
	
	%--
	% dilate with second line if needed
	%--
	
	if length(SEQ.line{2}) > 1	
		pq = se_supp(SEQ.line{2});
		
		Y = morph_dilate_mex( ...
			image_pad(Y, pq, b), ...
			uint8(SEQ.line{2}) ...
		);
	end
	
	%--
	% apply residual and compute maximum if needed
	%--
	
	if ~isempty(SEQ.rest)	
		% NOTE: the maximum function is deined for relevant input classes

		pq = se_supp(SEQ.rest);
		
		Y = max( ...
			Y, ...
			morph_dilate_mex( ...
				image_pad(X, pq, b), ...
				uint8(SEQ.rest) ...
			) ...
		);
	end
else
	%--
	% dilate with first line if needed
	%--
	
	if length(SEQ.line{1}) > 1
	
		pq = se_supp(SEQ.line{1});
		
		Y = morph_dilate_mex( ...
			image_pad(X, pq, b), ...
			uint8(SEQ.line{1}), ...
			uint8(image_pad(Z, pq, 0)) ...
		);
	else
		Y = X;
	end
	
	%--
	% dilate with second line if needed
	%--
	
	if length(SEQ.line{2}) > 1
		pq = se_supp(SEQ.line{2});
		
		Y = morph_dilate_mex( ...
			image_pad(Y, pq, b), ...
			uint8(SEQ.line{2}), ...
			uint8(image_pad(Z, pq, 0)) ...
		);
	end
	
	%--
	% apply residual and compute maximum if needed
	%-- 
	
	if ~isempty(SEQ.rest)	
		% NOTE: the maximum function is deined for relevant input classes
		
		pq = se_supp(SEQ.rest);
		
		Y = max( ...
			Y, ...
			morph_dilate_mex( ...
				image_pad(X, pq, b), ...
				uint8(SEQ.rest), ...
				uint8(image_pad(Z, pq, 0)) ...
			) ...
		);
	end

end
	