function r = mask_rlc(Z)

% mask_rlc - run length code for masks
% ------------------------------------
%
% r = mask_rlc(Z)
%
% Input:
% ------
%  Z - input mask
%
% Output:
% -------
%  r - run length code for input mask

%--
% get size of mask and put in column vector form
%--

[m,n] = size(Z);

c = Z(:);

%--
% compute run-length code for mask
%--

r = [m; n; c(1); diff([0; find([diff(c); 1])])];
