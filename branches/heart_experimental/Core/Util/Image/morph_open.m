function Y = morph_open(X, SE, Z, b)

% morph_open - morphological opening
% ----------------------------------
% 
% Y = morph_open(X, SE, Z, b)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  Z - computation mask image (def: [])
%  b - boundary behavior (def: -1)
%
% Output:
% -------
%  Y - opened image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% this function does not currently support image stacks

%--
% handle input
%--

[X, N, tag, flag, g, h] = handle_input(X,inputname(1));

if nargin < 4
	b = -1;
end

if nargin < 3
	Z = [];
end

%--
% invoke CUDA implementation if possible
%--

% NOTE: the code below will remember where CUDA morph erode failed and not delegate

B = se_mat(SE); state.data = {class(X), size(B)};

if isempty(Z) && cuda_enabled && ~cuda_has_failed(mfilename, state)
	try
		Y = cuda_morph_filter('open', X, B, b); return;
	catch		
		cuda_has_failed(mfilename, state, lasterror);
		
		clear cuda_morph_filter_mex; % TODO: could this be integrated into 'cuda_has_failed'? probably not simple
	end
end

%--
% color image, plane by plane processing
%--

if ndims(X) > 2
	
	[r, c, s] = size(X);
	
	for k = 1:s
		Y(:, :, k) = morph_open(X(:, :, k), SE, Z, b);
	end	
	
%--
% scalar image
%--
	
else
	%--
	% compute opening
	%--
	
	Y = morph_dilate(morph_erode(X, SE, [], b), se_tr(SE), Z, b);
	
	%--
	% mask operator
	%--
	
	if ~isempty(Z)
		Y = mask_apply(Y, X, Z);
	end
end

%--
% display output
%--

if flag & view_output
	
	switch view_output	
		case 1
			figure(h);
			set(hi,'cdata',Y);
			set(gca,'clim',fast_min_max(Y));
			
		otherwise	
			fig; image_view(Y);		
	end
end