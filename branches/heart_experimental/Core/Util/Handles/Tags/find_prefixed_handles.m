function handles = find_prefixed_handles(parent, prefix, callback)

% find_prefixed_handles - and possibly execute callback
% -----------------------------------------------------
%
% handles = find_prefixed_handles(parent, prefix, callback)
%
% Input:
% ------
%  parent - handle
%  prefix - on tag
%  callback - action
%
% Output:
% -------
%  handles - found
%
% See also: prefix_handle_tags

%--
% get handles tagged with prefix
%--

% TODO: change signature to allow field-value filtering of handles before test

handles = findobj(parent);

handles = handles(string_begins(get(handles, 'tag'), prefix));

%--
% execute callback on handles if available
%--

% TODO: consider fuller callback model here, handling output and possibly exceptions

% NOTE: this is not terribly useful, without this we get the handles and do what we will

if nargin > 2
	
	if iscell(callback)
		callback{1}(handles, callback{:});
	else
		callback(handles);
	end
end