function handles = find_suffixed_handles(parent, suffix, callback)

% find_suffixed_handles - and possibly execute callback
% -----------------------------------------------------
%
% handles = find_suffixed_handles(parent, suffix, callback)
%
% Input:
% ------
%  parent - handle
%  suffix - on tag
%  callback - action
%
% Output:
% -------
%  handles - found

%--
% get handles tagged with suffix
%--

handles = findobj(parent);

handles = handles(string_ends(get(handles, 'tag'), suffix));

%--
% execute callback on handles if available
%--

% TODO: consider fuller callback model here, handling output and possibly exceptions

% NOTE: this is not terribly useful, without this we get the handles and do what we will

if nargin > 2
	
	if iscell(callback)
		callback{1}(handles, callback{:});
	else
		callback(handles);
	end
end