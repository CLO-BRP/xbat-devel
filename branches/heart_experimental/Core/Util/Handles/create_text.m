function [handle, created] = create_text(par, tag, varargin)

% create_text - singleton determined by tag
% -----------------------------------------
%
% [handle, created] = create_text(par, tag, varargin)
%
% Input:
% ------
%  par - handle
%  tag - identifier
%  varargin - field and value pairs
%
% Output:
% -------
%  handle - to text
%  created - indicator

[handle, created] = create_obj('text', par, tag, varargin{:});	