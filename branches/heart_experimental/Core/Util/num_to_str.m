function str = num_to_str(x, sep)

% num_to_str - adaptive precision number to string conversion
% -----------------------------------------------------------
%
% str = num_to_str(x);
%
% Input:
% ------
%  x - number input
%
% Output:
% -------
%  str - string representation

% NOTE: this function makes use of the vectorization behavior of 'sprintf'

%--
% handle complex case recursively
%--

if ~isreal(x)
	
	% NOTE: we convert each component, concatenate, and fix sign
	
	parts = strcat(strread(num_to_str(real(x)), '%s'), '+', strread(num_to_str(imag(x)), '%s'), 'i');
	
	str = sprintf('%s ', parts{:});
	
	str = strrep(str, '+-', '-');
	
	if nargin > 1
		str = strrep(str, ' ', sep);
	end
	
	return;
end

%--
% convert real numbers
%--

str = sprintf('%0.16f ', x); 

str = regexprep(str, '(0+)\s', '0 ');

str = strrep(strrep(str, '0 ', ' '), '. ', ' ');

if str(end) == ' '
	str(end) = '';
end

if nargin > 1
	str = strrep(str, ' ', sep);
end
	