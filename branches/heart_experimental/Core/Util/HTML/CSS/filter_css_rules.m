function [parsed, full] = filter_css_rules(in, type, pattern)

% filter_css_rules - by field or value match
% ------------------------------------------
%
% [parsed, full] = filter_css_rules(in, type, pattern)
%
% Input:
% ------
%  in - file or parsed rules
%  type - of match
%  pattern - to match or helper
%
% Output:
% -------
%  parsed - rules filtered
%  full - set of input rules
%
% See also: parse_css

%--
% handle input
%--

if ischar(in)
	full = parse_css(in);
else 
	full = in;
end

%--
% find rules containing color
%--

parsed = empty(full);

for k = 1:numel(full)
	
	current = full(k); props = current.rules.props; 
	
	switch type
		
		case 'field'
			fields = fieldnames(props);
			
			match = fields(string_contains(fields, pattern));
			
			if ~isempty(match)
				current.rules.props = keep_fields(props, match);

				parsed(end + 1) = current; %#ok<AGROW>
			end
			
		case 'value'
			error('Value filtering not implemented yet.');
	end
	
end


