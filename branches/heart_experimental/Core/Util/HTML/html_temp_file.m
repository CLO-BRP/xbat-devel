function name = html_temp_file

% html_temp_file - temporary HTML file name
% -----------------------------------------
%
% name = html_temp_file
%
% Output:
% -------
%  name - HTML temp file name

name = [html_temp_root, filesep, 'temp_', int2str(rand_ab(1, 1, 10^6)), '.html'];
