function dirs = append_path(varargin)

% append_path - append directory or tree to path
% ----------------------------------------------
%
% dirs = append_path(root, rec)
%
% Input:
% ------
%  root - tree root (def: pwd)
%  rec - follow tree (def: 1)
%
% Output:
% -------
%  dirs - directories appended to path
%
% NOTE: this functions uses a multiple input, and command calling pattern
%
% See also: remove_path, update_path

%--
% check input
%--

if (numel(varargin) == 1) && iscellstr(varargin{1});
	elements = varargin{1};
else
	elements = varargin;
end

%--
% update path
%--

dirs = update_path('append', elements{:});
