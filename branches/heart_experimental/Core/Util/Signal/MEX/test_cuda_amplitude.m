function [passed, R] = test_cuda_amplitude(n, i, w, o)

% test_cuda_amplitude
% ----------------
% 
% [passed, results] = test_cuda_amplitude(n, i, w, o)
%
% Input:
% ------
%  n - size of 'rand' matrix, default = 1000
%  i - number of iterations, default = 10
%  w - window width, default = 10
%  o - overlap, default = 0
%
% Output:
% -------
%  passed - the tests
%  results - of timing comparison

%--
% handle input
%--

if nargin < 1
	n = 1000;
end

if nargin < 2
	i = 10;
end 

if nargin < 3
	w = 10;
end

if nargin < 4
	o = 0;
end

% TODO: the function 'die' is missing

if w < 0
	die;
end

if o >= w
	die;
end

if w > n
	error('Signal length should exceed window length.');
end

passed = true;
%--
% Source data
%--

X = rand(n, 1) - 0.5;

l = (n - w) / (w - o) + 1;

%--
% Compute in C
%--

h = ones(w, 1);

% TODO: we have to make sure we can carry out this test, that the MEX file is accesible

try
	source = fullfile(fileparts(which('fast_amplitude')), 'private', ['fast_amplitude_mex.', mexext]);
	
	destination = fullfile(fileparts(mfilename('fullpath')));
	
	copyfile(source, destination);
end

cuda_enabled false;

tic; [m0, e0] = fast_amplitude(X, h, o); mexEnd = toc;

%--
% Compute in CUDA MEX
%--

cuda_enabled true;

for k = 1:i
	tic; [m1, e1] = fast_amplitude(X, w, o); cudaEnd = toc;
end

%--
% Compare results
%-- 

% TODO: these results should differ by something related to eps('single')

merr = max(abs(m0 - m1));

if merr > 0.01
	fprintf('FAILED means test with error of %d\n', merr);
	disp(m0(1:5,:));
	disp(m1(1:5,:));
else
	disp(['Passed means test']);
end

% disp(e0);
% disp(e1);

eerr = max(abs(e0 - e1));

if eerr > 0.01
	fprintf('FAILED extremes test with error of %d\n', eerr);
	disp(e0(1:5,:));
	disp(e1(1:5,:));
    passed = false;
else
	disp(['Passed extremes test']);
end

disp('   Mex Time  CUDA Time Speedup');
disp([mexEnd', cudaEnd', mexEnd./cudaEnd']);

R = [mexEnd', cudaEnd', mexEnd./cudaEnd'];