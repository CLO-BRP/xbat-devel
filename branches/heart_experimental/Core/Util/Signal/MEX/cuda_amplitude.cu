//-----------------------------------------------
// Amplitude calculations on overlapping windows.
//-----------------------------------------------

// Nvidia
#include <cuda.h>
#include "cuda_runtime.h"

// System
#include <float.h>

// XBAT
#include "cuda_amplitude.h"
		 
// Threads per block, one thread per window of data
	
#define THREADS_PER_BLOCK 128
		 

//-------------------------------------------------
// Compute means and extremes on one window of data
//-------------------------------------------------

__global__ void amplitudeKernel_single(float *d_Means, float *d_Extremes, float *d_Data, unsigned int N, unsigned int w, unsigned int o, unsigned int L)
{
	unsigned int tid = threadIdx.x;
	unsigned int gidx = (blockIdx.x * blockDim.x + tid) * (w-o);
	float	     n_sum, p_sum;
	int          n_count, p_count;
	float        min, max;
	float        val, abs_val;
	float        diff;
	unsigned int i;
	int          sign;
	
	// As long as we still have data, do work
	if (gidx <= N-w)
	{
		// Compute values for one window
	
		n_sum = 0.0;
		p_sum = 0.0;
		n_count = 0;
		p_count = 0;
		min = FLT_MAX;
		max = -FLT_MAX;
	
		for (i = 0; i < w; i++)
		{
			val = (float) d_Data[gidx + i];
			abs_val = fabsf(val);
			sign = (int)((unsigned int)__float_as_int(val) >> 31);
			
			diff = (val - abs_val);
			n_sum += diff * 0.5;
			n_count += sign;
			
			diff = (val + abs_val);
			p_sum += diff * 0.5;
			p_count += 1 - sign;
			
			if (val > max) { max = val; }
			if (val < min) { min = val; }
		}

		// Result
	
		d_Means[blockIdx.x * blockDim.x + tid] = (n_count > 0) ? n_sum / (float) n_count : 0.0;
		d_Means[blockIdx.x * blockDim.x + tid + L] = (p_count > 0) ? p_sum / (float) p_count : 0;
		d_Extremes[blockIdx.x * blockDim.x + tid] = min;
		d_Extremes[blockIdx.x * blockDim.x + tid + L] = max;
	}
}
 
void cuda_amplitude_single(float *d_Means, float *d_Extremes, float *d_Data, unsigned int N, unsigned int w, unsigned int o)
{
	// Compute size of result data
	
	unsigned int totalSums = (N - w) / (w - o) + 1;
	
	// Number of kernel invocations to process entire dataset given window width
	
	unsigned int gridSize = (totalSums + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
	
	//--
	// Compute amplitude
	//--
	
	amplitudeKernel_single<<<gridSize, THREADS_PER_BLOCK>>>(
        d_Means, d_Extremes,
        d_Data,
        N, w, o, totalSums
    );

}
