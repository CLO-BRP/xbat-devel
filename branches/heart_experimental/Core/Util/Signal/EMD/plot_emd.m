function plot_emd(IMF, x, par)

% plot_emd - visualize empirical mode decomposition
% -------------------------------------------------
%
% plot_emd(IMF, par)
%
% Input:
% ------
%  IMF - intrinsic mode functions, output of 'emd'
%  par - parent figure

% TODO: develop options, output handles

%--
% handle input and setup
%--

if nargin < 3
	par = fig;
end

N = size(IMF, 2); n = size(IMF, 1);

if nargin < 2
	x = linspace(0, 1, n);
end

%--
% produce display
%--

layout = layout_create(N, 1);

ax = harray(par, layout); ax(1) = [];

handle = zeros(size(ax)); created = false(size(ax));

for k = 1:N
	handle(k) = plot(ax(k), x, IMF(:, k));
	
	% TODO: do this based on the 95% percentile, then we overflow to indicate extremes, consider the residual trend display
	
	range = fast_min_max(IMF(:, k));
	
	set(ax(k), 'ylim', 1.1 * range);
end

set(handle, 'hittest', 'off');

for k = 1:N
	[handle(k), created(k)] = create_line(ax(k), 'zero', 'xdata', x([1, end]), 'ydata', zeros(1, 2));
end

if any(created)
	set(handle, 'color', 'black', 'linestyle', ':');
end

range = x([1, end]); 

set(ax, 'xlim', range);

set(ax, 'buttondownfcn', {@buttondown, ax});

set(ax(1:end - 1), 'xticklabel', []);

set(par, 'keypressfcn', {@keypress, ax, range});


%----------------------
% KEYPRESS
%----------------------

function keypress(obj, eventdata, ax, range)

xlim = get(ax(1), 'xlim'); center = mean(xlim); width = diff(xlim); 

% ylim = get(ax(1), 'ylim'); height = diff(ylim);

fraction = 0.2;

switch eventdata.Key
	
	case 'leftarrow'
		xlim = xlim - fraction * width;
		
		if xlim(1) < range(1)
			xlim = xlim + (range(1) - xlim(1));
		end
		
	case 'rightarrow'
		xlim = xlim + fraction * width;
		
		if xlim(2) > range(2)
			xlim = xlim + (range(2) - xlim(2));
		end
			
	case 'c'
		xdata = get(findobj(ax(1), 'tag', 'cursor-line'), 'xdata');
		
		xlim = xdata(1) + 0.5 * width * [-1, 1];
		
	case 'uparrow'
		xlim = center + width * [-1, 1];
				
	case 'downarrow'						
		xlim = center + 0.25 * width * [-1, 1]; 
end

set(ax, 'xlim', xlim);


%----------------------
% BUTTONDOWN
%----------------------

function buttondown(obj, eventdata, ax) %#ok<INUSL>

point = get(obj, 'currentpoint'); point = point(1, 1:2);

handle = zeros(size(ax)); created = zeros(size(ax));

for k = 1:numel(ax)
	ylim = get(ax(k), 'ylim');
	
	[handle(k), created(k)] = create_line(ax(k), 'cursor-line', ...
		'xdata', point([1, 1]), ...
		'ydata', ylim ...
	);
end

if any(created)
	set(handle, ...
		'color', 'red', 'marker', 's' ...
	);
end



