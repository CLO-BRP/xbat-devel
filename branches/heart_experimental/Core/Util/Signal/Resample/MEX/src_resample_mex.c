#include <samplerate.h>
#include "mex.h"
#include "matrix.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{

    SRC_DATA data;
    
    double ratio;
    
    int method, error;
    
    int channels, output_frames;
    
	/*
	 * get data
	 */

	data.data_in = (float *) mxGetData(prhs[0]);
    
    data.input_frames = mxGetN(prhs[0]); 
    
    channels = mxGetM(prhs[0]);  

    /*
     * get ratio
     */
    
    data.src_ratio = mxGetScalar(prhs[1]);
    
    /*
     * get method
     */
    
    method = (int) mxGetScalar(prhs[2]);
    
    /*
     * fill in the rest of the converter data
     */
    
    data.output_frames = (int) (data.src_ratio * (double) data.input_frames) + 1;
    
    plhs[0] = mxCreateNumericMatrix(0, 0, mxSINGLE_CLASS, mxREAL);
    
    data.data_out = (float *) mxCalloc(channels * data.output_frames, sizeof(float));
    
    /*
     * run the converter
     */
    
    error = src_simple(&data, method, channels);
    
    if (error) {
        mexPrintf("%s\n", src_strerror(error));
    }
    
    mxSetM(plhs[0], channels); mxSetN(plhs[0], data.output_frames_gen);
    
    mxSetData(plhs[0], data.data_out);
    
}
