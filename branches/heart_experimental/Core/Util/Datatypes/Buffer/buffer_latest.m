function [ix,content,updated] = buffer_latest(buf)

% buffer_latest - get latest buffer
% ---------------------------------
%
% [ix,updated,content] = buffer_latest(buf)
%
% Input:
% ------
%  buf - buffer
%
% Output:
% -------
%  ix - index of latest entry
%  updated - time of latest update
%  content - content of latest update

[updated,ix] = max(buf.updated);

content = buf.content{ix};