function ext = get_extensions(type, varargin)
 
% get_extensions - get available extensions
% -----------------------------------------
%
% ext = get_extensions(type, 'field', value, ... )
%
% Input:
% ------
%  type - extension type 
%  field - extension field name
%  value - extension field value
%
% Output:
% -------
%  ext - extensions
%
% See also: get_browser_extension

% NOTE: the default behavior of this function is to get all extensions from cache

% TODO: conditionally load considering toolbox availability


% TODO: allow selection by 'classname'


%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% iterate over multiple types
%--

if exist('type', 'var') && iscellstr(type)
	ext = iterate(mfilename, type, varargin{:}); return;
end

%--
% set default type, check for refresh
%--

if nargin < 1
	type = '';
end

[refresh, type] = get_refresh_and_type(type);

%----------------------------------
% USE PERSISTENT STORE 
%----------------------------------

% NOTE: 'extensions_cache' provides and manages a system level persistent extension store

if isempty(extensions_cache)
	% NOTE: this is one way to bootstrap
	
	try %#ok<TRYNC>
		extensions_cache(extension_type_extension_type);
	end
	
	extensions_cache(discover_extensions('extension_type'));
	
	% NOTE: discover all extensions, update cache, and get extensions from cache
	
	ext = extensions_cache(discover_extensions);
	
elseif refresh
	
	% NOTE: discover required extensions and update cache
	
	if isempty(type)
		ext = discover_extensions; extensions_cache(ext);
	else
		ext = discover_extensions(type); extensions_cache(ext);
	end
		
else

	% NOTE: get extensions from cache
	
	ext = extensions_cache;
	
end

%----------------------------------
% SELECT EXTENSIONS
%----------------------------------

if isempty(ext)
	return;
end 

%--
% get extensions of given type
%--

if ~isempty(type)
	
	ix = find(strcmp({ext.subtype}', type));
	
	if isempty(ix)
		ext = [];
	else
		ext = ext(ix);
	end
	
end

%--
% select using field value pairs
%--

if ~isempty(varargin)
	ext = select_extensions(ext, varargin{:});
end


%----------------------------------
% SELECT_EXTENSIONS
%----------------------------------

% TODO: this kind of code appears in various places update 'struct_select'

function ext = select_extensions(ext, varargin)

%--
% return quickly for trivial selection
%--

if isempty(varargin) || isempty(ext)
	return;
end

%--
% select using input field-value pairs 
%--

[field, value] = get_field_value(varargin);

for j = 1:length(field)

	%--
	% select on available fields
	%--

	if ~isfield(ext, field{j})
		continue;
	end
	
	
	for k = length(ext):-1:1
		
		% NOTE: this allows us to select extensions by matching from an element of an array field
		
		% NOTE: the first test is not strictly correct, it should be 'iscellstr'
		
		switch field{j}
			
			case 'project'
				if ~isstruct(ext(k).project) || ~isequal(ext(k).project.name, value{j})
					ext(k) = [];
				end
				
			otherwise
				if iscell(ext(k).(field{j})) && ischar(value{j}) && string_is_member(value{j}, ext(k).(field{j}))
					continue;
				end
				
				if ~isequal(ext(k).(field{j}), value{j})
					ext(k) = [];
				end
				
		end
		
	end

end

%--
% return simple empty on empty
%--

if isempty(ext)
	ext = []; return;
end
	

%----------------------------------
% GET_REFRESH_AND_TYPE
%----------------------------------

function [refresh, type] = get_refresh_and_type(str)

%--
% no refresh and all types default
%--

refresh = 0;

if isempty(str)
	type = ''; return;
end

%--
% check for refresh prefix, suffix, or command
%--

if str(1) == '!', refresh = 1; type = str(2:end); 
	
elseif str(end) == '!', refresh = 1; type = str(1:end - 1);

elseif strcmpi(str, 'refresh'), refresh = 1; type = '';
	
else type = str;
	
end

%--
% check non-empty type
%--

if ~isempty(type)
	
	type = type_norm(type);
	
	if ~is_extension_type(type);
		error(['Unrecognized extension type ''', type, '''.']);
	end
	
end

