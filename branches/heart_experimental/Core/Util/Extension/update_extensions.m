function update_extensions(verb)

% update_extensions - shortcut to updating system extensions
% ----------------------------------------------------------
%
% update_extensions(verb)
%
% Input:
% ------
%  verb - verbosity flag (def: 0)

% TODO: add a type input that will allow updating of a single type

% TODO: this functions is outdated, update to consider the various implemented types

if ~nargin
	verb = 0;
end

%--
% update extensions cache
%--

get_extensions('!');

if verb
	disp('Extensions cache updated.');
end

%--
% update extension menus
%--

update_filter_menu; 

update_detect_menu;

if verb
	disp('Extension menus updated.');
	disp(' ');
end
