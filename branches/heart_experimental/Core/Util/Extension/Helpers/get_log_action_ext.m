function ext = get_log_action_ext(varargin)

ext = get_extensions('log_action', 'name', varargin{:});
