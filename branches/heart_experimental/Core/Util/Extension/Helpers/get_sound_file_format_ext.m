function ext = get_sound_file_format_ext(varargin)

ext = get_extensions('sound_file_format', 'name', varargin{:});
