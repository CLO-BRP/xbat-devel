function ext = get_event_annotation_ext(varargin)

ext = get_extensions('event_annotation', 'name', varargin{:});
