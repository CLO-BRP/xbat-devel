function type = type_norm(type, skip)

% type_norm - flexible handling of type through normalization
% -----------------------------------------------------------
%
% type = type_norm(type, skip)
%
% Input:
% ------
%  type - type string
%  skip - skip available types check
%
% Output:
% -------
%  type - normalized type string

% TODO: this function can be generalized by providing types list input

%--
% set default no skip
%--

if (nargin < 2) || isempty(skip)
	skip = 0; 
end

%--
% normalize input string
%--

% NOTE: allow spaces, capitalization, irrelevant whitespace, and simple plural

type = lower(strrep(strtrim(type), ' ', '_'));

if type(end) == 's'
	type(end) = [];
end

%--
% possibly check normalized type against available types
%--

if skip
	return;
end

if ~is_extension_type(type)
	
	% NOTE: throw error if no output was requested, otherwise empty output is failure
	
	if ~nargout
		error(['Unrecognized extension type ''', type, '''.']);
	else
		type = '';
	end
	
end

