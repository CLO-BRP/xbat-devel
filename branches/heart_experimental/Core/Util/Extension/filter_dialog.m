function out = filter_dialog(ext, context)

%--
% get controls
%--

controls = get_filter_controls(ext, context, [], 'dialog');

%--
% set base options
%--

opt = dialog_group;

opt.left = 1; opt.right = 1;

opt.width = 9;

%--
% update configuration with filter specific configuration options
%--

if ~isempty(ext.fun.parameter.control.options)

	try
		opt = struct_update(opt, ext.fun.parameter.control.options(context));
	catch
		extension_warning(ext, 'Parameter compilation failed.', lasterror);
	end

end

%--
% set fixed configuration fields after possible update
%--

% NOTE: header at top requires this 

opt.top = 0;

% NOTE: the first field is a useful convention, the second essential

opt.header_color = get_extension_color(ext); opt.ext = ext;

%--
% create dialog group
%--

out = dialog_group(ext.name, controls, opt, {@filter_dispatch, ext});


