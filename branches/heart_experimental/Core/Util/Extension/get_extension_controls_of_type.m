function [control, fun] = get_extension_controls_of_type(type, ext, context)

%--
% initialize empty output
%--

control = empty(control_create); fun = [];

%--
% check we own controls for type
%--

if ~has_presets(ext, type)
	return;
end

%--
% get own controls for type
%--

if strcmp(type, 'compute')
	fun = ext.fun.parameter.control;
else
	fun = ext.fun.(type).parameter.control;
end

% TODO: eventually move compute parameters to 'parameters'

try
	if strcmp(type, 'compute')
		control = fun.create(ext.parameter, context);
	else
		control = fun.create(ext.parameters.(type), context);
	end
catch

	extension_warning(ext, [title_caps(type), ' control creation failed.'], lasterror); 
end