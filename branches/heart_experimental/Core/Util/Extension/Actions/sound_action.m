function result = sound_action(varargin)

% sound_action - from command line
% --------------------------------
%
% result = sound_action(name, target)
%
% Input:
% ------
%  name - of action
%  target - array
%
% Output:
% -------
%  result - of action
%
% NOTE: sugar for 'run_action'

%--
% handle input
%--

if nargin < 2
	target = get_selected_sound;
end

result = struct;

if isempty(target)
	return;
end

%--
% run action
%--

result = run_action('sound', varargin{:});