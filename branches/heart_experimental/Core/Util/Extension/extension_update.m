function out = extension_update(pal,in)

% extension_update - update state of extension using palette
% ----------------------------------------------------------
%
% out = extension_update(pal)
%     = extension_update(pal,in)
%
% Input:
% ------
%  pal - extension palette
%  in - extension to update (def: try to get extension from palette)
%
% Output:
% -------
%  out - updated extension

%-----------------------------------------------------
% HANDLE INPUT
%-----------------------------------------------------

%--
% get extension from palette name
%--

% NOTE: we can do this because we are about to update the extension state


%-----------------------------------------------------
% UPDATE EXTENSION
%-----------------------------------------------------

%--
% get extension palette control values and extension parameter values
%--

[param,values] = get_parameter_values(pal,in);

%--
% copy extension and update
%--

out = in; 

out.values = values;

out.parameter = param;