function [value, types] = has_any_parameters(ext)

% has_any_parameters - check extension has any parameters
% -------------------------------------------------------
%
% [value, types] = has_any_parameters(ext) 
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - has parameters indicator
%  types - available parameter types

if numel(ext) > 1
	[value, types] = iterate(mfilename, ext); return; 
end

%--
% get parameter types
%--

types = get_parameter_types(ext); value = 0;

%--
% check at least one types has presets
%--

for k = length(types):-1:1

	if ~has_parameters(ext, types{k})
		types(k) = []; continue;
	end

	value = 1;
	
	if nargout < 2
		return;
	end
	
end
