function fun = value_fun

% value_fun - value callback structure
% ------------------------------------
%
% fun = value_fun
%
% Output:
% -------
%  fun - callback function structure

fun.create = {{'value'}, {'context', 'varargin'}};

fun.control = control_fun;

fun.menu.create = menu_args('value');

fun.menu.callback = {{'result'}, {'callback', 'context'}};

% NOTE: a rich model description should allow us to plot values generically

fun.model = {{'model'}, {'context'}};
