function args = menu_args(name)

% menu_args - argument description for menu functions
% ---------------------------------------------------
%
% args = menu_args(name)
%
% Input:
% ------
%  name - name of object to display in menu (def: 'obj')
%
% Output:
% -------
%  args - argument description

if nargin < 1
	name = 'obj';
end

args = {{'handles'}, {'par', name, 'context'}};