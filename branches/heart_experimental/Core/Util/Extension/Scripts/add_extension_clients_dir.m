function add_extension_clients_dir

disp(' '); disp('Reloading extensions ...');

ext = get_extensions('!');

disp(' ');

for k = 1:numel(ext)
	
	[clients, created] = create_dir(fullfile(extension_root(ext(k)), 'Clients'));
	
	if created
		disp([clients, ' created.']);
	else
		disp([clients, ' exists, skipped.']);
	end
end

disp(' ');