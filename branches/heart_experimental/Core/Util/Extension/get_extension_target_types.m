function target_types = get_extension_target_types(types)

% get_extension_target_types - get target objects of extensions
% -------------------------------------------------------------
%
% target_types = get_extension_target_types(types)
%
% Input:
% ------
%  types - list of extension types (def: all extension types)
%
% Output:
% -------
%  target_types - list of target types

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% set all available extension types default
%--

if nargin < 1
	types = get_extension_types;
else
	if ~iscellstr(types)
		error('Extension type input must be cell array of strings.');
	end
end

%------------------------------
% GET TARGET TYPES
%------------------------------

target_types = unique(strtok(types, '_'));

% NOTE: consider the one part type 'detector', note the transpose

target_types = setdiff(target_types, 'detector')';