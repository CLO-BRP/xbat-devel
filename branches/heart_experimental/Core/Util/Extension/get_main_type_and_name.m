function [type, name] = get_main_type_and_name(main)

% get_main_type - get type from main
% ----------------------------------
%
% [type, name] = get_main_type_and_name(main)
%
% Input:
% ------
%  main - function name or handle
%
% Output:
% -------
%  type - type
%  name - name

% NOTE: this function relies on a single 'extensions_root'

% TODO: this function inspects the location of main to infer type and name, consider making more robust

%--
% get main file from input
%--

if isa(main, 'function_handle')
	file = functions(main); file = file.file;
else
	file = which(main);
end

%--
% get type root beyond extensions root parent
%--

[root, name] = fileparts(fileparts(file)); par = extensions_root;

if isempty(strfind(root, par))
	type = ''; return;
end 

root = strrep(root, par, ''); root(1) = [];

%--
% get type from type root
%--

% NOTE: we parse the string into parts, reverse them, and concatenate them

if filesep == '\'
	sep = '\\';
else
	sep = filesep;
end

parts = strread(root, '%s', 'delimiter', sep);

type = str_implode(flipud(lower(parts)), '_'); type(end) = [];
