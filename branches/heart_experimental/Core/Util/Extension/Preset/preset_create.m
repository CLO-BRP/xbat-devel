function preset = preset_create(ext, varargin)

% preset_create - create preset structure
% ---------------------------------------
% 
% preset = preset_create(ext, 'field', value, ...)
%
% Input:
% ------
%  ext - extension to create preset for
%  field - preset field name
%  value - preset field value
%
% Output:
% -------
%  preset - preset structure

% TODO: this does not seem to be the right signature

%------------------
% CREATE PRESET
%------------------

%--
% PRIMITIVE
%--

% NOTE: this is a volatile field, we set it on load

preset.name = '';

% NOTE: this is the content of the preset

preset.ext = [];

if nargin
	preset.ext = ext;
end

% NOTE: tags and notes support simple annotation

preset.tags = {};

preset.notes = {};

%--
% CONTEXT
%--

preset.context = [];

% TODO: consider adding this to context! it causes recursion problems

preset.active = []; % active extensions

%--
% ADMINISTRATIVE
%--

preset.store = struct;

preset.author = '';

preset.created = now;

preset.modified = [];

preset.userdata = [];

%--------------------------------
% SET PROVIDED FIELDS
%--------------------------------

if ~isempty(varargin)
	
	preset = parse_inputs(preset, varargin{:});
end


