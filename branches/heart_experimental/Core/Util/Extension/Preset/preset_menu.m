function handles = preset_menu(par, ext)

% preset_menu - create preset menu for extension
% ----------------------------------------------
%
% handles = preset_menu(par, ext)
%
% Input:
% ------
%  par - menu parent
%  ext - extension (def: get from parent figure)
%
% Output:
% -------
%  handles - created menus

handles = [];

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% get menu parent-figure
%--

% NOTE: typically 'par' and 'pal' are the same

pal = ancestor(par, 'figure');

%--
% get extension from parent figure
%--

if nargin < 2
	
	% NOTE: this seems to happen in two places, here and 'preset_compile'
	
	[value, ext] = is_extension_palette(pal);
	
	if ~value
		error('Unable to get extension from palette.');
	end
	
end

%--
% check if extension has presets
%--

if ~has_any_presets(ext)
	return;
end 

%--------------------------
% SETUP
%--------------------------

%--
% clear former menu if needed
%--

top = findobj(par, 'type', 'uimenu', 'tag', 'TOP_PRESET_MENU');

if ~isempty(top)
	delete(allchild(top));
end

%--------------------------
% CREATE PRESET MENU
%--------------------------

%--
% create top menu if needed
%--

if isempty(top)
	top = uimenu(par, 'label', 'Preset', 'tag', 'TOP_PRESET_MENU'); 
end 

%--
% create command menus
%--

% TODO: consider a browse command, to search preset collection

handles(end + 1) = top;

% NOTE: both save and load as the '...' indicates produce a dialog

% TODO: add preset type to dialog, not to menu

handles(end + 1) = uimenu(top, ...
	'label', 'Save ...', ...
	'accelerator', 'S', ...
	'callback', {@save_preset_callback, par, ext} ...
);

handles(end + 1) = uimenu(top, ...
	'enable', 'on', ...
	'label', 'Load ...', ...
	'accelerator', 'O', ...
	'callback', {@load_preset_callback, par, ext} ...
);

%--
% create preset type menus
%--

types = get_preset_types(ext);

pal_ext = ~isempty(strfind(ext.type, 'palette'));

for j = 1:length(types)
	
	%--
	% check for presets of a given type
	%--
	
	type = types{j}; [value, names, store] = has_presets(ext, type); 
	
	if ~value
		continue;
	end 
	
	%--
	% create preset type menus
	%--

	% NOTE: palette extensions will have a single type and it is not really a 'compute' type
	
	if ~pal_ext
		handles(end + 1) = uimenu(top, ...
			'label', ['(', title_caps(type), ')'], ...
			'enable', 'off', ...
			'separator', 'on' ...
		);
	end
	
	if isempty(names)

		handles(end + 1) = uimenu(top, ...
			'label', 'No Presets Found', ...
			'enable', 'off' ...
		);
	
		if pal_ext
			set(handles(end), 'separator', 'on');
		end

	else

		% TODO: 'mat' should not be hardcoded throughout the system

		if strcmp(store, 'mat')
			names = file_ext(names);
		end

		for k = 1:length(names)
				
			handles(end + 1) = uimenu(top, ...
				'label', names{k}, 'callback', {@open_preset_callback, par, ext, type} ...
			); %#ok<AGROW>

			if pal_ext && k == 1
				set(handles(end), 'separator', 'on');
			end
			
		end

	end
	
end

handles(end + 1) = uimenu(top, ...
	'label', 'Refresh', ...
	'separator', 'on', ...
	'callback', {@refresh_menu_callback, par, ext} ...
);

handles(end + 1) = uimenu(top, ...
	'label', 'Show Files ...', ...
	'separator', 'off', ...
	'callback', {@show_files_callback, ext} ...
);


%-------------------------------------
% SAVE_PRESET_CALLBACK
%-------------------------------------

function save_preset_callback(obj, eventdata, par, ext) 

%--
% compile preset
%--

pal = ancestor(par, 'figure');

preset = preset_compile(pal);

%--
% present dialog to save session
%--

% TODO: use parameter field 'preset', if available, to pre-load preset name field 

info = save_preset_dialog(preset);

% NOTE: return if no preset was saved

if isempty(info)
	return;
end 

%--
% rebuild preset menu
%--

preset_menu(par, ext);


%-------------------------------------
% LOAD_PRESET_CALLBACK
%-------------------------------------

function load_preset_callback(obj, eventdata, par, ext) 

%--
% load preset through dialog
%--

preset = load_preset_dialog(ext);

if isempty(preset)
	return;
end 
	
%--
% get parent browser
%--

pal = ancestor(par, 'figure');

bro = get_palette_parent(pal);

if isempty(bro)
	return;
end 

%--
% update browser extension
%--

% NOTE: get browser extension, update 'compute' parameters, and store preset name

current = get_browser_extension(ext.subtype, bro, ext.name);

current.parameter = preset.ext.parameter;

current.parameter.preset = preset.name; % NOTE: this field is useful to save the preset again

set_browser_extension(bro, current);

% TODO: consider loading of active extension states

%--
% update preset menu
%--

% NOTE: required if we mark the most recently loaded preset, should use 'current'

% preset_menu(par, ext);


%-------------------------------------
% OPEN_PRESET_CALLBACK
%-------------------------------------

function open_preset_callback(obj, eventdata, par, ext, type)  %#ok<INUSL>

%--
% handle input
%--

% NOTE: we get the palette and browser handles from the menu parent

pal = ancestor(par, 'figure');

bro = get_palette_parent(pal);

if isempty(bro)
	return;
end 

%--
% load preset from file
%--

name = get(obj, 'label');

preset = preset_load(ext, name, type);

% db_disp preset; preset

if isempty(preset)
	return;
end

%--
% update browser extension
%--

% NOTE: get browser extension, update 'compute' parameters, and store preset name

current = get_browser_extension(ext.subtype, bro, ext.name);

% TODO: this dichotomy, is a transition problem

if isfield(preset, 'ext')
	current.parameter = preset.ext.parameter;
else
	current.parameter = preset;
end

current.parameter.preset = preset.name; % HACK: this field is useful to save the preset again

set_browser_extension(bro, current);

%--
% update preset menu
%--

% NOTE: required if we mark the most recently loaded preset, should use 'current'

% preset_menu(par, ext);


%--------------------------------------
% REFRESH_MENU_CALLBACK
%--------------------------------------

function refresh_menu_callback(obj, eventdata, par, ext)

preset_menu(par, ext);


%-------------------------------------
% SHOW_FILES_CALLBACK
%-------------------------------------

function show_files_callback(obj, eventdata, ext)

show_file(preset_dir(ext));
