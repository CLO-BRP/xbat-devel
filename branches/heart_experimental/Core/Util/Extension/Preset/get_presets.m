function presets = get_presets(ext, type, varargin)

% get_presets - get extension presets
% -----------------------------------
%
% presets = get_presets(ext, type, 'field', value, ...)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%  field - field name
%  value - field value
%
% Output:
% -------
%  presets - presets

%--
% handle input
%--

if (nargin < 2) || isempty(type)
	type = 'compute';
end

%--
% initialize presets
%--

% NOTE: this is not currently used, in the non-trivial case

presets = empty(preset_create);

%--
% get preset files and root 
%--

[files, root] = get_preset_files(ext, type);

if isempty(root) || isempty(files)
	return;
end

names = file_ext(files);

%--
% load presets
%--

for k = 1:length(names)
	try
		preset = preset_load(ext, names{k});
	catch
		nice_catch(lasterror, 'Failed to load preset.'); continue;
	end
	
	% NOTE: the 'struct_update' approach provides a weak normalization
	
	opt = struct_update; opt.flatten = 0;
	
	presets(end + 1) = struct_update(preset_create, preset, opt); %#ok<AGROW>
end

%--
% filter presets using available criteria
%--

% TODO: add name filtering helper

if ~isempty(varargin) && ~isempty(presets)
	
	presets = filter_struct(presets, varargin{:});
end
