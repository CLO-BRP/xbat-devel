function [value, ext] = is_extension_palette(pal, opt)

% is_extension_palette - test whether palette belongs to extension
% ----------------------------------------------------------------
%
% [value, ext] = is_extension_palette(pal, opt)
%
% Input:
% ------
%  pal - palette handle
%  opt - palette options
%
% Output:
% -------
%  value - result of test
%  ext - palette extension

%-------------------
% HANDLE INPUT
%-------------------

%--
% check palette handle
%--

[test, ignore, kind] = is_palette(pal);

if ~test
	error('Input handle is not palette handle.');
end

%--
% get options from palete
%--

if nargin < 2
	data = get(pal, 'userdata'); opt = data.opt;
end

%-------------------
% SETUP
%-------------------

%--
% get extension type and name from palette
%--
	
% NOTE: extension is part of palette options

if isfield(opt, 'ext')
	
	type = opt.ext.type; name = opt.ext.name;

else % NOTE: this is deprecated and will be removed

	type = ''; name = get(pal, 'name');

end
	
%-------------------
% GET EXTENSION
%-------------------

%--
% get extension and create extension developer menus
%--

ext = get_extensions(type, 'name', name);

% NOTE: this has problems with multiple selected extensions

value = (length(ext) == 1);

if ~value
	ext = [];
end

%--
% ensure proper palette tag if possible
%--

% TODO: why is this done here?

if strcmpi(kind, 'widget');
	return;
end

if ~isempty(ext)
	set(pal, 'tag', get_extension_tag(ext, data.parent));
end
