function info = get_info(caller)

% get_info - get main file info
% -----------------------------
%
% info = get_info(caller)
%
% Output:
% -------
%  info - main file info if computable

%--
% compute name from caller location
%--

% NOTE: this is the name of the parent directory of the caller

info = dir(which(caller.file));
