function main = get_main(caller)

% get_main - get main handle for extension by examining caller location
% ---------------------------------------------------------------------
%
% main = get_main(caller)
%
% Output:
% -------
%  main - handle to main extension function if computable

% NOTE: we build a function handle from the caller name

main = str2func(caller.name);