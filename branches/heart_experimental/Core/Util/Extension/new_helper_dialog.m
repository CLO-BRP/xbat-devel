function helper = new_helper_dialog(ext, private)

% new_helper_dialog - dialog to assist in creation of new helpers
% ---------------------------------------------------------------
%
% helper = new_helper_dialog(ext, private)
%
% Input:
% ------
%  ext - extension
%  private - indicator
%
% Output:
% -------
%  helper - file

%--
% set public default
%--

if nargin < 2
	private = 0; 
end 

%--
% handle string input
%--

if ischar(ext)
	
	ext = parse_tag(ext, ...
		'::', {'subtype', 'name', 'parent'} ...
	);
	
	ext = get_extensions(ext.subtype, 'name', ext.name);
	
end 

%--
% create dialog controls
%--

% NOTE: the 'min' value makes this a non-collapsible header

control(1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', ['Helper  (', ext.name, ')'], ...
	'space', 0.75 ...
);

% TODO: consider prefix for names

control(end + 1) = control_create( ...
	'name', 'name', ...
	'alias', 'Name', ...
	'style', 'edit' ...
);

% NOTE: we use private instead of public, the first is a concept in MATLAB

control(end + 1) = control_create( ...
	'name', 'private', ...
	'style', 'checkbox', ...
	'value', private, ...
	'space', 0 ...
);

%--
% create dialog and get values
%--

% TODO: implement validation as control callbacks for dialog

% NOTE: validation callbacks can be executed prior to the formal button callback

% NOTE: the ok button may only be available when the contents are right

% NOTE: if any validation tests fail highlight controls in dialog

opt = dialog_group; opt.header_color = get_extension_color(ext);

out = dialog_group('New ...', control, opt);

if isempty(out.values)
	return;
end

values = out.values;

%--
% create helper file and open for editing
%--

try

	root = extension_root(ext);
	
	if values.private
		helper = [root, filesep, 'private', filesep, values.name, '.m'];
	else
		helper = [root, filesep, 'Helpers', filesep, values.name, '.m'];
	end
	
	fid = fopen(helper, 'w'); fprintf(fid, '%s', ['function ', values.name]); fclose(fid);

	% NOTE: the add method behavior does not currently invoque edit
	
	edit(helper);

catch

	helper = ''; nice_catch(lasterror, 'Helper function create failed.');

end
