function docs = init_extension_docs(ext, vcs)

% init_extension_docs - initialize extension docs
% -----------------------------------------------
%
% docs = init_docs(title, author, root, vcs)
%
% Input:
% ------
%  ext - to document
%  vcs - system to use 'svn', 'hg', or 'none' (def: 'svn')
%
% Output:
% -------
%  docs - directory
% 
% NOTE: the scope of this function is different from 'init_docs'
% 
% See also: generate_extension_dochead

% TODO: display files generated

%--
% handle input
%--

if nargin < 2
	vcs = 'svn';
end

if ~nargin
	ext = extension_here;
end 

if isempty(ext)
	disp('You are not currently in an extension directory.'); disp(' '); return;
end

%--
% make sure ancestor docs exist
%--

% TODO: we need to make sure that this operation is not destructive

if ~trivial(ext.parent)
	parent = get_extensions(ext.parent.type, 'name', ext.parent.name);
	
	init_extension_docs(parent, vcs);
end

%--
% initialize DOCS directory and main file
%--

root = extension_root(ext);

docs = fullfile(root, 'Docs', 'DOCS');

if ~exist(docs, 'dir')
	docs = create_dir(docs); 
	
	if isempty(docs)
		error('Unable to create DOCS directory.');
	end
	
	disp(['Created ', docs]);
else
	disp(['Skip ', docs]);
end 

file = fullfile(docs, 'docs.tex');

% NOTE: for now we clobber any existing file

if true % ~exist(file, 'file')
	%--
	% generate file contents
	%--
	
	% DOCHEAD
	
	lines = generate_extension_dochead(ext);
	
	% OVERVIEW 
	
	lines{end + 1} = '\section{Overview}';
	
	sub = {'Basic Concepts', 'Features', 'Typical Uses', 'Algorithm'};
	
	for k = 1:numel(sub)
		lines{end + 1} = ['%\subsection{', sub{k}, '}']; %#ok<*AGROW>
	end
	
	% PARAMETERS
	
	type = get_preset_types(ext);
	
	if ~isempty(type)
		
		lines{end + 1} = '\section{Parameters}';

		lines = [lines; file_readlines(which('about-parameters.tex'))];
		
		for k = 1:numel(type)
			lines{end + 1} = ['\subsection{', title_caps(type{k}), '}'];

			lines = [lines; file_readlines(which([type{k}, '-parameters.tex']))];
			
			% TODO: this function should load existing documentation if available

			[parameter.lines, parameter.file] = build_docs_parameter_list(ext, type{k});

			lines = [lines; parameter.lines];

			file_writelines(parameter.file, parameter.lines);
		end
	end 
	
	lines{end + 1} = '\end{document}';
	
	db_disp; iterate(@disp, lines);
	
	%--
	% write file
	%--
	
	info = file_writelines(file, lines);
	
	if isempty(info)
		error('Failed to create docs.tex file.');
	end 
		
	disp(['Created ', file]);
else
	disp(['Skip ', file]);
end

%--
% setup version control for docs
%--

switch vcs
	case 'svn'
		if ~is_working_copy(fileparts(docs))
			return;
		end
		
		svn('add', docs);
		
		svn('propset', 'svn:keywords', '"Id Author Date Rev URL"', file);
		
	case 'hg'
		error 'Mercurial support not yet implemented.'
		
end


