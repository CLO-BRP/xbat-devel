function [file, old] = regenerate_main(ext, norm)

% regenerate_main - generate main extension function
% --------------------------------------------------
%
% [file, old] = regenerate_main(ext, normalize)
%
% Input:
% ------
%  ext - extension to regenerate main for
%  norm - normalize main name
%
% Output:
% -------
%  file - generated main
%  old - former main

%------------------------
% HANDLE INPUT
%------------------------

% NOTE: we don't try to normalize by default

if nargin < 2
	norm = 0;
end

if numel(ext) > 1
	file = iterate(mfilename, ext, norm); old = []; return;
end

%------------------------
% SETUP
%------------------------

%--
% get main name and file
%--

info = functions(ext.fun.main); main = info.function;

if norm
	file = [extension_root(ext), filesep, get_main_name(ext), '.m']; old = info.file;
else
	file = info.file;
end

%--
% get parent and parent main if there is one
%--

parent = ext.parent;

if ~isempty(parent)
	parent = parent.main(); info = functions(parent.fun.main); parent_main = info.function;
end

%------------------------
% WRITE MAIN
%------------------------

%--
% open file
%--

fid = fopen(file, 'w');

%--
% write declaration
%--

if isempty(parent)
		
	if strcmp(ext.type, 'extension_type')
		fprintf(fid,'%s\n\n%s\n\n', ...
			['function ext = ', main], ...
			'ext = extension_create;' ...
		);
	else
		fprintf(fid,'%s\n\n%s\n\n', ...
			['function ext = ', main], ...
			'ext = extension_create(mfilename);' ...
		);
	end
else
	
	fprintf(fid,'%s\n\n%s\n\n', ...
		['function ext = ', main], ...
		['ext = extension_inherit(mfilename, ', parent_main, ');'] ...
	);
	
end

%--
% write body
%--

fields = {'short_description', 'category', 'ext', 'version', 'guid', 'author', 'email', 'url'};

for k = 1:length(fields)

	%--
	% create field line
	%--
	
	field = fields{k};
	
	switch field
		
		% NOTE: these particular helpers are not needed, it could all more dynamic
		
		case 'category', line = get_category_line(ext, parent);
			
		case 'ext'
			
			% NOTE: 'formats' and 'file_formats' may be associated with file extensions
			
			if strfind(ext.subtype, 'format')
				line = get_ext_line(ext, parent);
			else
				line = '';
			end
			
		case 'links', line = get_links_line(ext, parent);
		
		case 'guid'
			
			% NOTE: here we make sure that we generate a guid for a child extension
			
			if ~isempty(parent) && isequal(ext.guid, parent.guid)
				line = ['ext.guid = ''', generate_uuid, ''';'];
			else
				line = ['ext.guid = ''', ext.guid, ''';'];
			end
			
		otherwise
			
			if ~isempty(parent) && isequal(ext.(field), parent.(field))
				
				% NOTE: most fields are inherited, you can edit this line to update
				
				line = ['% ext.', field, ' = '''';'];
				
				% NOTE: the short description is cleared for children
				
				if strcmp(field, 'short_description')
					line(1:2) = [];
				end
				
			else
				
				% NOTE: extensions without parent must explicitly set fields
				
				line = ['ext.', field, ' = ''', ext.(field), ''';'];
			
			end
				
	end
	
	%--
	% write line to file
	%--
	
	% NOTE: the file is double spaced
	
	if ~isempty(line)
		fprintf(fid, '%s\n\n', line);
	end
	
end 

%--
% close main
%--

fclose(fid);

%--
% delete old file if we non-trivially normalized
%--

% NOTE: we normalized and an old file with a different name exists

% NOTE: we do this so the extension can be re-discovered immediately

if norm && ~strcmp(file, old) && exist(old, 'file')
	delete(old);
end


%------------------------
% GET_CATEGORY_LINE
%------------------------

function line = get_category_line(ext, parent) 

line = get_cell_line(ext, parent, 'category');


%------------------------
% GET_EXT_LINE
%------------------------

function line = get_ext_line(ext, parent) 

line = get_cell_line(ext, parent, 'ext');


%------------------------
% GET_LINKS_LINE
%------------------------

function line = get_links_line(ext, parent) 

% NOTE: in this case we want the most current links first

line = get_cell_line(ext, parent, 'links', 0);


%------------------------
% GET_CELL_LINE
%------------------------

function line = get_cell_line(ext, parent, field, prefix)

%--
% handle input
%--

if nargin < 4
	prefix = 1;
end 

%--
% get explicit field content
%--

% NOTE: we refer to parent content, only unique content is explicit

if isempty(parent)
	explicit = ext.(field);
else
	explicit = setdiff(ext.(field), parent.(field));	
end

%--
% build content line
%--

if ~isempty(parent) && prefix
	line = ['ext.', field, '{:}, '];
else
	line = '';
end

if ischar(explicit)
	
	if isempty(explicit)
		explicit = {};
	else
		explicit = {explicit};
	end
	
end

for k = 1:length(explicit)
	
	if isempty(explicit{k})
		continue;
	end
	
	line = [line, '''', explicit{k}, ''', '];
	
end

if ~isempty(line)
	line(end - 1:end) = [];
end

if ~isempty(parent) && ~prefix
	line = [line, ', ext.', field, '{:}'];
end

%--
% finish declaration, comment if there is no explicit content
%--

% NOTE: we don't need a line to inherit the property, only to modify it

if isempty(explicit)
	pre = '% ';
else
	pre = '';
end

line = [pre, 'ext.', field, ' = {', line, '};'];

