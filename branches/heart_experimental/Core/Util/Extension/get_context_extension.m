function [ext, in] = get_context_extension(context, type)

% get_context_extension - get extension from context
% --------------------------------------------------
% 
% [ext, in] = get_context_extension(context, type)
%
% Input:
% ------
%  context - context
%  type - extension type
%
% Output:
% -------
%  ext - context extension of given type (if provided), otherwise main
%  in - where extension was found 'main' or 'active'

%--
% get main context extension first
%--

ext = context.ext; in = 'main';

if nargin < 2
	return;
end

%--
% check extension type
%--

if ~is_extension_type(type)
	error('Unrecognized extension type.');
end

%--
% check main extension type for match, otherwise consider active extensions
%--

if isempty(ext) || ~strcmpi(ext.subtype, type)
	
	try
		ext = context.active.(type); in = 'active';
	catch 
		ext = []; in = '';
	end

end