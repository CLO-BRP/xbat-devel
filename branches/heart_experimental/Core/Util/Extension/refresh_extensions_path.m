function content = refresh_extensions_path(type, pretend)

% refresh_extension_path - scan extensions root and add contents to path
% ----------------------------------------------------------------------
%
% content = refresh_extensions_path(type, pretend)
%
% Input:
% ------
%  type - extension type
%  pretend - flag
%
% Output:
% -------
%  content - extension directories added to path

%--
% set no pretend default
%--

if nargin < 2
	pretend = 0;
end

%--
% set and check type
%--

% NOTE: empty means all types

if nargin < 1
	type = '';
end

%--
% get extension directories
%--

content = scan_dir(extensions_root(type));

% NOTE: remove private directories

for k = length(content):-1:1	
	
	if strfind(content{k}, 'private')
		content(k) = [];
	end
	
end

% NOTE: return if we are pretending

if pretend
	return;
end

%--
% add directories to path
%--

% TODO: implement path adding that can leave things unchanged if possible

% NOTE: the '-end' flag adds directories to the end of the path

addpath(content{:}, '-end');
