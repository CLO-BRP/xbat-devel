function extension_main_rename(pretend)

% extension_main_rename - rename extension main functions
% -------------------------------------------------------
%
% extension_main_rename(pretend)
%
% Input:
% ------
%  pretend - indicator

if nargin < 1
	pretend = 1;
end

%--
% get extensions
%--

exts = get_extensions;

%--
% get current main names and normalized main names
%--

for k = 1:length(exts)
	
	ext = exts(k); main = functions(ext.fun.main); 
	
	current{k} = main.file; new{k} = get_main_name(ext.name, ext.subtype, 1);
	
end

%--
% rename main functions
%--

% TODO: this can break inheritance relations, this needs to be considered

% NOTE: we can check for extensions with children and handle them specially

pi = pwd; disp(' ');

for k = 1:length(current)
	
	%--
	% compare current and normalized name
	%--
	
	[root, name, suffix] = fileparts(current{k});
	
	% NOTE: there is nothing when the name is already normalized
	
	if strcmp(name, new{k})
		disp([exts(k).name, ' main name is normalized. (', name, ')']); disp(' '); continue;
	else
		disp([exts(k).name, ' main name is not normalized, renaming:']);
		disp(name);
		disp(new{k});
		disp(' ');
	end
	
	if pretend
		continue;
	end
	
	%--
	% rename main if we need to normalize
	%--
	
	source = [name, suffix]; destination = [new{k}, suffix];
	
	cd(root); [status, result] = svn('rename', source, destination); 
	
	disp(result);
	
end

cd(pi);

%--
% rediscover extensions and regenerate main functions
%--

exts = get_extensions('!');

for k = 1:length(exts) 
	ext = exts(k); regenerate_main(ext);
end
