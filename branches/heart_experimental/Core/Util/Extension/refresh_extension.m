function pal = refresh_extension(callback, context)

% refresh_extension - update browser extension state
% --------------------------------------------------
%
% pal = refresh_extension(callback, context)
%
% Input:
% ------
%  callback - callback context
%  context - extension context
%
% Output:
% -------
%  pal - new extension palette

% NOTE: this function has a strange signature, it will probably need refactoring 

% TODO: the signature we want is (par, ext, pal), if the pal is missing some code is not needed

%-----------------------------
% SETUP
%-----------------------------

%--
% unpack input
%--

pal = callback.pal; par = callback.par; 

ext = context.ext;

%-----------------------------
% REFRESH EXTENSION
%-----------------------------

%--
% close palette
%--

close(pal.handle);

%--
% refresh extension
%--

data = get_browser(par.handle);

% NOTE: from the next line we get the index and the context

[ext, ix, context] = get_browser_extension(ext.subtype, par.handle, ext.name, data);

% NOTE: in the next two lines we get a fresh extension

extensions_cache(discover_extensions(ext.subtype, ext.name));

ext = get_extensions(ext.subtype, 'name', ext.name);

% NOTE: then we pack the extension in the right place and store

data.browser.(ext.subtype).ext(ix) = extension_initialize(ext, context);

set(par.handle, 'userdata', data);

%--
% open palette
%--

pal = extension_palettes(par.handle, ext.name, ext.subtype);
