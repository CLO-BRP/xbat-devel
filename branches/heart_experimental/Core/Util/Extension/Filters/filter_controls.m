function control = filter_controls(ext,presets)

% filter_controls - get filter controls
% -------------------------------------
%
% control = filter_controls(ext,presets)
%
% Input:
% ------
%  ext - filter extension
%  presets - preset names
%
% Output:
% -------
%  control - control array

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

% TODO: produce common type controls as well as specific type controls

%-----------------------------------
% PRESET
%-----------------------------------

%--
% header
%--

control(1) = control_create( ...
	'string','Presets', ...
	'style','separator', ...
	'type','header', ...
	'space',0.5 ...
);

%--
% preset
%--

L = {'(Manual)'};

if (~isempty(presets))
	L = {L{:}, presets{:}};
end

value = 1;

control(end + 1) = control_create( ...
	'name','preset', ...
	'style','popup', ...
	'space',0.75, ... 
	'lines',1, ... 
	'string',L, ...
	'value',value ...
);

%--
% new preset
%--

control(end + 1) = control_create( ...
	'name','new_preset', ...
	'style','buttongroup', ...
	'space',0.75, ...
	'align','left', ...
	'width',0.5, ...
	'lines',1.75 ...
);

%-----------------------------------
% FILTER
%-----------------------------------

%--
% header
%--

control(end + 1) = control_create( ...
	'string','Filter', ...
	'style','separator', ...
	'type','header' ...
);

%--
% opacity
%--

% TODO: generalize this to other operations

switch (ext.subtype)
	
	case ('signal_filter')
		
	case ('image_filter')
		
		control(end + 1) = control_create( ...
			'name','opacity', ...
			'style','slider', ...
			'min',0, ...
			'max',1, ...
			'value',1 ...
		);

end

%--
% active
%--

value = 0;

control(end + 1) = control_create( ...
	'name','active', ...
	'style','checkbox', ...
	'value',value ...
);