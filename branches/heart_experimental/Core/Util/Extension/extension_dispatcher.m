function [result, info] = extension_dispatcher(type, name, request, par, opt)

% extension_dispatcher - dispatch extension requests
% --------------------------------------------------
%
% [result, info] = extension_dispatcher(type, name, request, par)
%
% Input:
% ------
%  type - type
%  name - name
%  request - request
%  par - browser
%
% Output:
% -------
%  result - result
%  info - info

%--------------------
% HANDLE INPUT
%--------------------

if nargin < 5
	% NOTE: the default helpers are still tied to XBAT, this strategy may allow reuse
	
	% NOTE: there was a problem in reuse related to the odd input signature satisfied by this helper
	
% 	opt.helpers.get_extension = @get_browser_extension;
	
	% NOTE: using the anonymous function to rearrange the signature solves this problem
	
	opt.helpers.get_extension = @(parent, type, name)(get_browser_extension(type, parent, name));
	
	% NOTE: the helpers below take no arguments, therefore reuse is simple
	
	opt.helpers.get_parent = @get_active_browser;
	
	opt.helpers.get_debug = @()(get_user_preference(get_active_user, 'DEBUG'));
	
	if ~nargin
		result = opt; return;
	end
end

%--
% initialize output for convenience
%--

result = struct; info = struct;

%--
% default active browser
%--

if nargin < 4 || isempty(par)
	try
		par = opt.helpers.get_parent();
	catch
		nice_catch(lasterror, 'Failed while calling ''get_parent'' helper.');
		
		par = [];
	end
end 

if isempty(par)
	return;
end 

%--
% get debug state
%--

% TODO: implement this using a helper as well

try
	debug = opt.helpers.get_debug();
catch
	nice_catch(lasterror, 'Failed while calling ''get_debug'' helper.');

	debug = false;
end

%--------------------
% SETUP
%--------------------

%--
% get browser extension and context
%--

try
	[ext, ignore, context] = opt.helpers.get_extension(par, type, name); %#ok<ASGLU>
catch
	nice_catch(lasterror, 'Failed while calling ''get_extension'' helper.');
	
	rethrow(lasterror);
end

%--
% get parent type extension
%--

type = get_extensions('extension_type', 'name', title_caps(ext.subtype));

parameter = struct;

if has_any_parameters(type)
	
	% NOTE: we should be storing the computed parameters in the type extension
	
	try
		parameter = type.fun.parameter.create(context);
	catch
		parameter = struct; extension_warning(type, 'Failed to create parameters', lasterror);
	end
	
end

%--------------------
% HANDLE REQUEST
%--------------------

% NOTE: we handle the missing handle condition with an exception to reuse extension warning

info.started = clock;

try
	
	if ~isempty(type.fun.dispatch)
		result = type.fun.dispatch(request, parameter, context); info.status = 'done';
	else
		error('XBAT:ExtensionType:MissingDispatcher', 'Type dispatcher is not defined.');
	end
	
catch
	
	extension_warning(type, 'Failed to dispatch request.', lasterror); info.status = 'failed';

end

info.elapsed = etime(clock, info.started);

%--------------------
% DEBUG DISPLAY
%--------------------

% TODO: refine debug display and integrate error with debug when needed

if false % debug

	str = [ ...
		' DISPATCHING: ''', upper(request), ''' request for ''', ...
		upper(ext.name), ''' ''', upper(type.name) ''' extension. ' ...
	];
	
	disp(' ');
	str_line(str, '_');
	disp(' ');
	disp(str);
	str_line(str, '_');
	disp(' ');
	
	disp(' RESULT:')
	if ~trivial(result)
		disp(' ');
		disp(['   ', to_str(result)]);
	end
	disp(' ');
	
	disp(' STACK:');
	disp(' ');
	stack = dbstack('-completenames'); stack(1:2) = [];
	if ~isempty(stack)
		stack_disp(stack);
		disp(' ');
	end
	
	disp(' TIME:');
	disp(' ');
	time = [datestr(info.started), ' (', sec_to_clock(info.elapsed), ')'];
	disp(['   ', time]);
	disp(' ');
	
	str_line(str, '_');	
	disp(' ');
	
end



