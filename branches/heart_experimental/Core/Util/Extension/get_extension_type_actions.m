function [actions, arg] = get_extension_type_actions(main)

% get_extension_type_actions - get extension type actions
% -------------------------------------------------------
%
% actions = get_extension_type_actions(main)
%
% Input:
% ------
%  main - of 'extension_type' extension
%
% Output:
% -------
%  actions - extension type actions

%------------------
% SETUP
%------------------

%--
% set action argument description
%--

arg = {{'result', 'context'}, {'parameter', 'context'}};

%--
% get type and name from main function name or handle
%--

[type, name] = get_main_type_and_name(main); 

if ~strcmp(type, 'extension_type')
	actions = {}; return;
end

%------------------
% GET ACTIONS
%------------------

%--
% check for availability of actions method in extension_type extension
%--

root = [extensions_root('extension_type'), filesep, title_caps(name), filesep, 'private'];

file = [root, filesep, 'actions.m'];

% NOTE: return empty if extension_type extension does not provide actions

if ~exist(file, 'file') 
	actions = {}; return;
end

%--
% get actions method handle
%--

current = pwd; cd(root); 

fun = str2func('actions');

cd(current);

%--
% call actions method to get signatures
%--

% NOTE: we may only get names not signatures, this constraints the dispatch

actions = fun();

args = cell(size(actions));

for k = 1:numel(args)
	args{k} = arg;
end

arg = args;
