function attr = get_cuda_attributes(id)

% get_cuda_attributes - for devide identified
% -------------------------------------------
%
% attr = get_cuda_attributes(id)
%
% Input:
% ------
%  id - for device
%
% Output:
% -------
%  attr - struct

% NOTE: we keep a persistent store for all installed devices, we do this once per session

persistent ATTR;

if isempty(ATTR)
	ATTR = cuda_get_attributes_mex;
end

attr = ATTR(id + 1);