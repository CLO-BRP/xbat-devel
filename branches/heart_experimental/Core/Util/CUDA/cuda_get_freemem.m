function [f, r] = cuda_get_freemem(id)

% cuda_get_freemem - cuda free memory available
% ---------------------------------------------
%
% [f, r] = cuda_get_freemem(id)
%
% Input:
% ------
%  id - for device (def: [], gets info for all available devices)
%
% Output:
% -------
%  f - free memory available
%  r - cuda return code

% NOTE: we first check for availability of devices

if ~has_cuda
	f = 0;
    r = 0;
end

%--
% get info for all devices
%--

if ~nargin
    id = 0;
end

[f, r] = cuda_get_freemem_mex(id);

