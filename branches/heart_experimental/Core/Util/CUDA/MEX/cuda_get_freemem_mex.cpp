//---------------------------------------------------
// INCLUDE
//---------------------------------------------------

// C

#include "math.h"
#include "string.h"
#include "stdio.h"

// MATLAB

#include "mex.h"
#include "matrix.h"

// CUDA

#include "cuda.h"

//---------------------------------------------------
// MEX FUNCTION
//---------------------------------------------------

// cuda_get_freemem - get the free memory for the specified device
//                     and, optionally, the total memory
// ---------------------------------
//
// [f, r] = cuda_get_freemem(n)
//
// Input:
// ------
//  n - device index
//
// Output:
// -------
//  f - free memory 
//  r - CUDA status

//--------------------------------
// Author: William Hoagland
//--------------------------------
//--------------------------------

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
	//--------------------------------------------
	// VARIABLES
	//--------------------------------------------
	
	CUresult cuStatus = CUDA_SUCCESS;
	int dordinal;
	CUdevice device;
    CUcontext context;
	size_t freeMem = 0, totalMem;
	
	//--------------------------------------------
	// Ordinal value of device to query, default is 0
	//--------------------------------------------

	if (nrhs > 0) {
		dordinal = (int) mxGetScalar(prhs[0]);
	} else {
		dordinal = 0;
	}

	//--------------------------------------------
	// COMPUTATION
	//--------------------------------------------
	
    // Must always init

	cuInit(0);
	
    // Explicity get a specific device
    
	cuStatus = cuDeviceGet(&device, dordinal);
    
    // Create a CUDA context
    
    if (cuStatus == CUDA_SUCCESS) {
        cuStatus = cuCtxCreate(&context, 0, device);
    }
    // Make the query
    
	if (cuStatus == CUDA_SUCCESS) {
		cuStatus = cuMemGetInfo(&freeMem, &totalMem);
	}
	
    // Clean up
    
    cuCtxDetach(context);
  
	//--
	// Output
	//--
	
    plhs[0] = mxCreateScalarDouble((double) freeMem);
    if (nlhs > 1) {
        plhs[1] = mxCreateScalarDouble((double) cuStatus);
    }
    
	if (cuStatus != CUDA_SUCCESS)
	{			
		mexErrMsgTxt("Unable to query CUDA device.");
	}
}

