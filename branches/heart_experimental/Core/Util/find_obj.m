function [obj, tags] = find_obj(par, tag, fun, varargin)

% find_obj - find objects by focusing on matching tags
% ----------------------------------------------------
%
% [obj, tags] = find_obj(par, tag, fun, field, value, ... )
%
% Input:
% ------
%  par - parent
%  tag - tag or tag pattern
%  fun - matching function (def: @string_contains)
%  field - field name
%  value - field value
%
% Output:
% -------
%  obj - objects found
%  tags - object tags

%--
% handle input
%--

% NOTE: the default is case sensitive string comparison

if (nargin < 3) || isempty(fun)
	fun = @string_contains;
end

%--
% initialize trivial (no action) output
%--

obj = [];

%--
% find objects based on provided attributes
%--

% NOTE: we at least check that fields and values are paired

if ~isempty(varargin) && ~mod(length(varargin), 2)
	
	try
		obj = findobj(par, varargin{:});
	catch
		 nice_catch(lasterror, 'Failed to find objects using properties.'); return;
	end

else
	
	obj = findobj(par);
	
end

% NOTE: return if we have found nothing to delete

if isempty(obj)
	return;
end

%--
% get and mark tags to find select objects 
%--

% NOTE: the compare function is expected to take the tags followed by the pattern

% NOTE: that the compare function is expected to produce indices or an indicator

tags = get(obj, 'tag'); 

if ischar(tags)
	tags = {tags};
end

mark = eval_callback(fun, tags, tag); 

obj = obj(mark); tags = tags(mark); 

%--
% sort handles based on tags
%--

[tags, pix] = sort(tags); obj = obj(pix); 

