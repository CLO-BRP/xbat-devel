function result = in_available_drive(root, force)

% in_available_drive - test for directories
% -----------------------------------------
%
% result = in_available_drive(root, force)
%
% Input:
% ------
%  root - directory
%  force - refresh of available drives (def: false)
% 
% Output:
% -------
%  result - of test
%
% NOTE: existence not checked here, we test a location to be in an available drive
% 
% See also: get_drive_letters, create_dir

if ~ispc
	% NOTE: in this case we cannot determine a priori that drive is missing, let things fail later
	
	result = true; return;
end

%--
% handle input
%--

if nargin < 2
	force = false; 
end 

% NOTE: here we handle multiple directories recursively

if iscellstr(root)
	if force
		get_drive_letters(force); force = false;
	end
	
	result = iterate(mfilename, root, force); return;
end

%--
% test whether directory can be determined to be in available drive
%--

% NOTE: that we use 'upper(root(1))' in our equality test, 'get_drive_letters' looks for uppercase drives and we should be case-insensitive 

result = numel(root) >= 3 && strcmp(root(2:3), ':\') && any(upper(root(1)) == get_drive_letters(force));


