function drive = get_drive_letters(force)

% get_drive_letters - available
% -----------------------------
%
% drive = get_drive_letters(force)
% 
% Input:
% ------
%  force - refresh (def: false)
%
% Output:
% -------
%  drive - letters available
%
% See also: create_dir

% NOTE: this is only useful for PC

if ~ispc
	drive = ''; return;
end

if ~nargin
	force = false; 
end

persistent DRIVE;

if ~isempty(DRIVE) && ~force
	drive = DRIVE; return;
end

% NOTE: the next line turns off a scary but inconsequential warning

% REF: http://www.mathworks.com/support/solutions/en/data/1-81QJLT/index.html?solution=1-81QJLT

system_dependent('DirChangeHandleWarn', 'never');

drive = char(double('A'):double('Z'));

start = pwd;

% NOTE: we try to 'cd' to each drive, if we can it's available

for k = numel(drive):-1:1
	try
		cd([drive(k), ':\']);
	catch
		drive(k) = [];
	end
end

cd(start);

DRIVE = drive;
