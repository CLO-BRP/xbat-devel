function callback = set_callbacks(handle, callback)

% set_callbacks - set various callbacks for handle
% ------------------------------------------------
%
% [callback, field, value] = set_callbacks(handle, callback)
%
% Input:
% ------
%  handle - handle
%  callback - struct
%
% Output:
% -------
%  callback - struct

%--
% set proposed callbacks
%--

available = get_callback_fields(handle); proposed = fieldnames(callback);

for k = 1:numel(proposed)
	
	if ~string_is_member(proposed{k}, available)
		continue;
	end
	
	set(handle, proposed{k}, callback.(proposed{k}));
	
end

%--
% get callbacks to verify if needed
%--

if ~nargout
	return;
end

callback = get_callbacks(handle);