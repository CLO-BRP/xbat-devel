function [valid, local, domain] = proper_email(str)

% proper_email - test string
% --------------------------
%
% [valid, local, domain] = proper_email(str)
%
% Input:
% ------
%  str - candidate
%
% Output:
% -------
%  valid - indicator
%  local - part
%  domain - part

% TODO: this function is not complete and what's there can be improved

% NOTE: use http://en.wikipedia.org/wiki/E-mail_address as starting point

% TODO: write a test

%--
% handle input
%--

if iscellstr(str)
	[valid, local, domain] = iterate(mfilename, str); return;
end

valid = 0; local = ''; domain = '';

%--
% check for 'at' separator
%--

parts = str_split(str, '@'); 

if numel(parts) ~= 2 || any(iterate(@isempty, parts))
	return;
end

local = parts{1}; domain = parts{2};

%--
% check local part is proper
%--

% NOTE: if local part is quoted anything goes, no need for further testing

if ~strcmp(local([1, end]), '""')
	
	if local(end) == '.'
		return;
	end
	
	not_allowed = '()[]\;:,<>@'; contains = unique(local);
	
	if ~isempty(intersect(contains, not_allowed))
		return;
	end
	
end

%--
% check domain
%--

if numel(domain) > 255
	return;
end

parts = str_split(domain, '.'); 

% NOTE: we want at least one dot, a bit more stringent than the standard

% NOTE: the other two tests check the length of each part

if numel(parts) < 2 || any(iterate(@isempty, parts)) || any(iterate(@numel, parts) > 63)
	return;
end

if any(~isstrprop(setdiff(domain, '.'), 'alphanum'))
	return;
end 

valid = 1;


