function root = fig_path(root)

% fig_path - get and set, where figures are saved
% -----------------------------------------------
%
% root = fig_path(root)
%
% Input:
% ------
%  root - to set 
% 
% Output:
% -------
%  root - current

if ~nargin
	root = get_env('fig_path');
else
	root = set_env('fig_path', root);
end
