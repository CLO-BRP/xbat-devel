function h = figs_refresh(h)

% figs_refresh - redraw figures 
% -----------------------------
% 
% h = figs_refresh(h)
%
% Input:
% ------
%  h - handles of figures to refresh 
%
% Output:
% -------
%  h - figure handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% get figure handles
%--

if ((nargin < 1) | isempty(h))
	h = get(0,'children');
end

%--
% refresh figures
%--

for k = 1:length(h)
	refresh(h(k));
end
