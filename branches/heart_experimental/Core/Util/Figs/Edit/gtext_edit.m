function h = gtext_edit(s,str,id)

% gtext_edit - create editable gtext
% ----------------------------------
% 
% h = gtext_edit(s)
%
% Input:
% ------
%  s - gtext string
%
% Output:
% -------
%  h - handle of text

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% set str
%--

if (nargin < 2)
	str = 'Initialize';
end

%--
% main switch
%--

switch (str)

	case ('Initialize')
	
		h = gtext(s);
		
		id = round((10^6)*rand(1));
		set(h,'UserData',id);
		
		set(h,'ButtonDownFcn',['gtext_edit([],''Edit'',' num2str(id) ')']);
		
	case ('Edit')
		
		h = findobj(get(gca,'Children'),'UserData',id);
		
		set(h,'Editing','on');
		
end


