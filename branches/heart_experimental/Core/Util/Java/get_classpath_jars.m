function [jars, path] = get_classpath_jars(type)

% get_classpath_jars - get jar files in classpath
% -----------------------------------------------
%
% [jars, path] = get_classpath_jars(type)
%
% Input:
% ------
%  type - of classpath ('-static', '-dynamic', or '-all' (default))
%
% Output:
% -------
%  jars - files
%  path - to files

if ~nargin
	type = '-all';
end

path = javaclasspath(type); jars = cell(size(path));

for k = numel(path):-1:1
	
	if ~string_ends(path{k}, '.jar')
		path(k) = []; jars(k) = []; continue;
	end
	
	jars{k} = path{k}(find(path{k} == filesep, 1, 'last') + 1:end);
	
end

if ~nargout
	disp(' '); iterate(@disp, strcat({'    '}, jars)); disp(' '); clear jars;
end