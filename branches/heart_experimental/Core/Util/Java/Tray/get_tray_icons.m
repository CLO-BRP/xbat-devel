function [icons, tray] = get_tray_icons

tray = get_tray; icons = tray.getTrayIcons;