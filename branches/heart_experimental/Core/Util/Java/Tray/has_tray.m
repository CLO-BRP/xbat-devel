function [has, tray] = has_tray

% has_tray - check for and get system tray
% ----------------------------------------
%
% [has, tray] = has_tray
%
% Output:
% -------
%  has - indicator
%  tray - object

tray = get_tray; has = tray.isSupported;