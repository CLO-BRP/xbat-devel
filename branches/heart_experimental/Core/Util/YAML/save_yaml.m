function status = save_yaml(file, in, append)

% save_yaml - 
% ----------------------------------------------------------
%
% status = save_yaml(file, in, append)
%
% Input:
% ------
%  file   - path to file
%  in     - struct to convert to YAML
%  append - boolen : append to existing file or clear out the old one
%
% Output:
% -------
%  status - status of the file save


if nargin < 2
    in = file;
    file = [];
end

yaml_str = format_yaml(in);

if isempty(file)
    status = yaml_str;
    return
end

if exist('append', 'var') && append == 1
    write_mode = 'a';
else
    write_mode = 'w+';
end

% Prep the file

fid = fopen(file, write_mode);

% Write to the file

fprintf(fid, yaml_str);

% close the file

status = fclose(fid);




function out = format_yaml(in, level)

% Recursive function to format YAML strings

if iscell(in)
    error('My apologies, but save_yaml() does not yet support cell arrays :(');
end

if nargin < 2
    level = 0;
end

% Format structs

if isstruct(in)
    out    = sprintf('\n');
  	fields = fieldnames(in);
  
    for k=1:numel(fields)
        key   = fields{k};
        value = in.(key);
        if ischar(value)
            value = regexprep(value, '\', '\\\'); % escape backslashes for sprintf
        end
        out   = [out, sprintf('%s%s: %s\n', get_spaces(level), key, format_yaml(value, level + 1))];
    end

    return;
end

% This is the default value for simple data types [char, double, etc...]

out = in;


function spaces = get_spaces(level)

spaces = '';
for j=1:level
    spaces = [spaces, '  '];
end

