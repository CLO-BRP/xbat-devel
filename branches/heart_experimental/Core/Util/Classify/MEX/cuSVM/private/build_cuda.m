%
% Custom build script for cuSVM MEX files within XBAT infrastructure
%
% NOTE: Needed because of custom link with cuda BLAS library and unconventional directory structure because of code distribution

opt = build_cuda_mex;

% Set custom link

opt.mex = {'-lcublas'};

% Disable install

opt.install = false;

% Build
build_cuda_mex('SVMPredict', opt);
build_cuda_mex('SVMTrain', opt);

% Custom install

movefile(['../cuda_SVMPredict_mex.' mexext], ['../../../private/cuda_SVMPredict_mex.' mexext]);
movefile(['../cuda_SVMTrain_mex.' mexext], ['../../../private/cuda_SVMTrain_mex.' mexext]);