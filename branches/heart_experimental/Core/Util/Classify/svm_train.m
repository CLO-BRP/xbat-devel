function [model, info] = svm_train(data, value, opt)

% svm_train - train SVM model given data and their values
% -------------------------------------------------------
%
% [model, info] = svm_train(data, value, opt)
%
%           opt = svm_train
% Input:
% ------
%  data - for training
%  value - to regress or label to predict
%  opt - for training
%
% Output:
% -------
%  model - result of training
%  info - suplemental data

%--
% set and possibly output default train options
%--

if nargin < 3
	opt.cuda = true;
	
	opt.kernel = 'rbf';
    
	opt.regression = false;
    
	if ~nargin
		model = opt; return;
	end
end

% NOTE: check options to see if we can use CUDA, cuSVM currently only supports RBF kernel

if opt.cuda
	opt.cuda = cuda_enabled && string_is_member(opt.kernel, cuda_kernels);
end

%--
% train based on requested and available engine
%--

if opt.cuda
    [model, info] = cuSVMTrain(data, value, opt);
else
	if ~string_is_member(opt.kernel, libsvm_kernels)
		error(['Unknown kernel ''', opt.kernel, '''.']);
    end
    
    [model, info] = libSVMTrain(data, value, opt);
end


%------------------------
% CUDA_KERNELS
%------------------------

function kernels = cuda_kernels

kernels = {'rbf'};


%------------------------
% LIBSVM_KERNELS
%------------------------

function kernels = libsvm_kernels

% NOTE: the correspond internally (to 'libsvm') to codes 0, 1, 2, 3

kernels = {'linear', 'polynomial', 'rbf', 'sigmoid'};
