function file = add_doc(name, title, author, root)

% add_doc - section file
% ----------------------
%
% file = add_doc(name, title, author, root)
%
% Input:
% ------
%  name - for file
%  title - of section
%  author - of section
%  root - of DOCS directory
%
% Output:
% -------
%  file - created
%
% NOTE: documents added this way are meant to be included in 'docs.tex'

%--
% handle input
%--

if nargin < 4
	root = pwd;
end 

if strcmpi(name, 'docs')
	error('File name ''docs'' is reserved.')
end

% NOTE: make sure we have a DOCS directory, before we add file

docs = init_docs(root);

content = what_ext(docs, 'tex');

if isfield(content, 'tex') && string_is_member([name, '.tex'], content.tex)
	error(['File ''', name, '.tex'' already exists at this location.']);
end

%--
% add document
%--

file = fullfile(docs, [name, '.tex']); lines = generate_dochead(title, author);

file_writelines(file, lines);



