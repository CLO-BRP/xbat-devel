function lines = update_repo_url(lines)

% NOTE: the secure URL is not accessible without a password

for k = 1:numel(lines)
	lines{k} = strrep(lines{k}, 'URL: https://', 'URL: http://');
end

% NOTE: the '/source/browse' is the google-code browser, superior to the '/svn' location

for k = 1:numel(lines)
	lines{k} = strrep(lines{k}, ...
		'http://xbat-devel.googlecode.com/svn', 'http://code.google.com/p/xbat-devel/source/browse' ...
	);
end
