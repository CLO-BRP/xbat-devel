function docs = init_docs(title, author, root, vcs)

% init_docs - initialize docs directory
% -------------------------------------
%
% docs = init_docs(title, author, root, vcs)
%
% Input:
% ------
%  title - of document
%  author - of document
%  root - directory to add DOCS to
%  vcs - system to use 'svn', 'hg', or 'none' (def: 'svn')
%
% Output:
% -------
%  docs - directory

% TODO: display files generated

%--
% handle input
%--

if nargin < 4
	vcs = 'svn';
end

if nargin < 3
	root = pwd;
end 

%--
% initialize DOCS directory and main file
%--

docs = fullfile(root, 'DOCS');

if ~exist(docs, 'dir')
	docs = create_dir(docs); 
	
	if isempty(docs)
		error('Unable to create DOCS directory.');
	end
	
	disp(['Created ', docs]);
else
	disp(['Skip ', docs]);
end 

file = fullfile(docs, 'docs.tex');

if ~exist(file, 'file')
	info = file_writelines(file, generate_dochead(title, author));
	
	if isempty(info)
		error('Failed to create docs.tex file.');
	end 
	
	disp(['Created ', file]);
else
	disp(['Skip ', file]);
end

%--
% setup version control for docs
%--

switch vcs
	case 'svn'
		if ~is_working_copy(fileparts(docs))
			return;
		end
		
		svn('add', docs);
		
		svn('propset', 'svn:keywords', '"Id Author Date Rev URL"', file);
		
	case 'hg'
		error 'Mercurial support not yet implemented.'
		
end


