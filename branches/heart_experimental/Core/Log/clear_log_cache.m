function cache = clear_log_cache(log)

% clear_log_cache - deletes log cache file
% ----------------------------------------
%
% cache = clear_log_cache(log)
%
% Input:
% ------
%  log - array
%  par - browser (def: active browser)
%
% Output:
% -------
%  cache - files deleted
%
% See also: log_cache_file

if ~nargin
	par = get_active_browser;
	
	if isempty(par)
		cache = {}; return;
	else
		log = get_browser_logs(par);
	end
end

%--
% clear log caches
%--

for k = 1:numel(log)
	cache{k} = log_cache_file(log(k)); delete(cache{k});
end

if ~nargout
	disp(' '); iterate(@disp, strcat({'Cleared '}, cache)); disp(' '); clear cache;
end
