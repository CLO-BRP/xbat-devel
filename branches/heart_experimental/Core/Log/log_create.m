function log = log_create(sound, varargin)

% log_create - create log access structure
% ----------------------------------------
%
% log = log_create(sound, 'field', value, ... )
%
% Input:
% ------
%  sound - parent sound
%  field - field
%  value - value
%
% Output:
% -------
%  log - log

%--
% handle input
%--

if ~nargin
	sound = [];
end

%--
% create log structure
%--

log = log_struct;

%--
% fill structure using input
%--

log.sound = sound;

if size(varargin)
    log = parse_inputs(log, varargin{:});
end

%--
% get format extension
%--

[format, context] = get_log_format(log);

if isempty(format)
    error(['Unrecognized log format ''', log.format, '''.']);
end

% NOTE: version is currently format version

log.version = format.version;

%--
% get format-specific connection parameters
%--

if has_parameters(format, 'store')
	
	try
		log.store.parameter = format.fun.store.parameter.create(context);
	catch
		extension_warning(format, 'Failed to create connection parameters.', lasterror);
	end
	
end

    
%----------------------------------
% LOG_STRUCT
%----------------------------------

function log = log_struct

%--
% PRIMITIVE FIELDS
%--

log.name = '';

log.file = '';

% TODO: should this value be hard-coded here

log.format = 'MAT';

log.version = [];

% TODO: consider generating a better id

rand('state', sum(100 * clock));

log.id = round(rand * 10^16);

%--
% STORE FIELD
%--

% NOTE: the store stores the data

log.store = struct;

%--
% DATA FIELDS
%--

log.sound = [];

user = get_active_user;

log.author = user.name;

log.created = now;

log.modified = [];

% log.measurement = measurement_create;	
% 
% log.annotation = annotation_create;	
% 
% % NOTE: this was meant to store a log generation summary, for example detector scan descriptions
% 
% log.generation = []; 

%--
% DISPLAY FIELDS
%--

log.visible = 1;

log.color = color_to_rgb('Green');

log.linestyle = linestyle_to_str('Solid');

log.linewidth = 2;

log.patch = -1;

%--
% INTEGRITY FIELDS
%--

log.readonly = 0;

% TODO: we should 'reference count' the users of a log in the database

% log.open = 0;

%--
% CACHE FIELDS
%--

% TODO: create environment variable for this and editor

log.cache.clips = 0;

log.cache.images = 0;

%--
% EVENT
%--

% NOTE: for non 'MAT' logs these fields are used for temporary log stores

log.event = empty(event_create);

log.length = 0;

