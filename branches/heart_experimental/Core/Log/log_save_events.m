function event = log_save_events(log, event, request)

% log_save_events - save events to a log
% --------------------------------------
%
% event = log_save_events(log, event, request)
%
% request = log_save_events
%
% Input:
% ------
%  log - to save to
%  event - array to save
%  request - description
%
% Output:
% -------
%  event - saved
%  request - default description
%
% See also: render_barn_events

%--
% handle input
%--

% NOTE: if no request description comes in, the active user is requesting we save all parts of event

if nargin < 3
	request.save = 'all'; request.user = get_active_user; request.render = true;
	
	if ~nargin
		event = request; return;
	end
end

% NOTE: if the request comes in as a string we pack it and consider it coming from the active user

% NOTE: rendering requires more than the request indicator, we don't render when saving 'tags' or 'ratings'

if ischar(request)
	request_save = request; clear request;
	
	request.save = request_save; request.user = get_active_user; request.render = true;
end
 
% NOTE: there are no events to save, we are done

if isempty(event)
	return;
end

% NOTE: not clear we should need this

log.store = log_store(log);

%--
% separate parent and non-parent events
%--

% NOTE: in case of hierarchical events we return, the work is done by function call in the conditional

if save_hierarchical_events(log, event, request)
	return; 
end

%--
% make sure events have file info
%--

if ~isfield(event, 'file')
	
	for k = 1:numel(event)
		event(k).file = get_event_file(event(k), log.sound);
	end
	
end

%--
% add events to log
%--

[format, context] = get_log_format(log);

% NOTE: the context may have a request for the handler, we could expand this

context.request = request;

writer = format.fun.event.save;

% NOTE: we simply return as this format cannot write events, we also do not update the cache

if isempty(writer)
	return;
end

% NOTE: a 'measure' request assumes the events are unchanged and have been previously saved

if strcmp(request.save, 'measure') && (numel([event.id]) == numel(event))
	
	id = [event.id];
else
	%--
	% update events store
	%--

	% NOTE: the writer is responsible for the event and basic annotation stores, tags and ratings
	
	try
		id = writer(log.store, event, context);
		
		% TODO: this is not the last of this, all writers should provide local and global identifiers
		
		if isstruct(id)
			guid = {id.guid}; id = [id.id];
		else
			guid = cell(size(id));
		end
	catch
		extension_warning(format, 'Failed to save event.');
	end

	%--
	% update cache
	%--
	
	for k = 1:numel(event)
		event(k).id = id(k); event(k).guid = guid{k};
	end

	log_cache_update(log, 'update', event);
	
	%--
	% render barn resources if needed
	%--

	% TODO: clean this logic up, at least we can now supress rendering
	
	% NOTE: it is not clear that the final part of the test would pass right now

	if ~isequal(request.render, false) && ...
		~trivial(request.render) && ...
		~isempty(local_barn) && ...
		~string_is_member(request.save, {'tags', 'rating'}) 
		% && isequal(local_barn, log.store)

		% TODO: this should be deferred, we should submit a request to a daemon

		% TODO: we should configure the rendering here, partially done
		
		if isstruct(request.render)
			
			render_barn_events(log, event, request.render);
		else
			render_barn_events(log, event);
		end
		
	end
end

%--
% save event extension data if possible and needed
%--

if isempty(format.fun.event.extension)
    return;
end

%--
% make sure all events have legitimate id
%--

% NOTE: this happens when saving new events

if ~isequal(id, [event.id])
	
	for k = 1:length(event)
		event(k).id = id(k);
	end
end

%--
% extract extensions from all events and prepare them for storage
%--

[id, ext, data] = get_event_extension_data(event);

%--
% save the various types of extension data available
%--
    
% TODO: we need to properly check for this handle, the test above is suspect

for k = 1:length(ext)
	% TODO: figure out what this result should contain and what we do with it
	
	try
		result = format.fun.event.extension.save(log.store, id{k}, ext(k), data{k}, context);
	catch
		extension_warning(format, ['Failed to save data for ', upper(title_caps(ext(k).subtype)), ' ''', ext(k).name, '''.']);
	end
end


%---------------------------------
% SAVE_HIERARCHICAL_EVENTS
%---------------------------------

function count = save_hierarchical_events(log, event, request) %#ok<DEFNU>

%--
% check for hierarchical events
%--

free = iterate(@isempty, {event.children}); count = sum(~free);

if ~count
	return;
end

%--
% store free events
%--

event(free) = log_save_events(log, event(free), request);

%--
% store hierarchical events, parents and their children
%--

parent = event(~free); childless = rmfield(parent, 'children'); childless(1).children = [];

childless = log_save_events(log, childless, request);

for k = 1:numel(parent)
	%--
	% set children link to parent
	%--

	child = parent(k).children;

	for j = 1:numel(child)
		child(j).parent = childless(k).id;
	end

	%--
	% update parent id and children
	%--

	parent(k).id = childless(k).id;

	parent(k).children = log_save_events(log, child, request);
end

%--
% pack stored parents
%--

event(~free) = parent;

