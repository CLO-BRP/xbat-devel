function [log, events] = log_append(log, events, map)

% log_append - append events to log
% ---------------------------------
%
% [log, events] = log_append(log, events, map)
%
% Input:
% ------
%  log - log
%  events - event to append
%  map - whether to map
%
% Output:
% -------
%  log - updated log

%--
% handle input
%--

if nargin < 3
    map = 0;
end

%--
% validate events
%--

[events, reject, why] = validate_events(events, log.sound);

if ~isempty(reject)
	
	message = { ...
		'Some or all of the events from ', ...
		['"', log_name(log), '"'], ...
		'are incompatible with the log sound ', ...
		['"', sound_name(log.sound), '" '], ...
		'because they have incompatible:' ...
	};
	
	warn_dialog({message{:}, why{:}}, 'Problem Appending Log Events', 'modal');
	
	% NOTE: all events may have been rejected, in this case we are done

	if isempty(events)
		return;
	end

end

%--
% map event time if necessary
%--

% NOTE: mapping from 'slider' to 'record' time is what we need to do for selection logging.  That's why this is here

if map
    events = map_events(events, log.sound, 'slider', 'record');
end

%--
% insert events according to log format type
%--

events = log_save_events(log, events);

