function db_log_insert_extension(store, extension, event_id, type)

%--
% handle input
%--

if ~isfield(extension, 'event_id')
    
    if nargin < 3 || isempty(event_id)
        return;
    end
    
    extension.event_id = event_id;

end

%--
% get extension name from extension
%--

if isfield(extension, 'name')
    ext_name = extension.name;
else
    ext_name = 'noname';
end

%--
% create table for this measurement if necessary
%--

name = table_name([type, '_', ext_name]);

sql = create_table(extension, name);

sqlite(store.file, 'exec', sql);

%--
% insert measurement into table using parameter binding
%--

values = value_str(length(fieldnames(flatten_struct(extension))));

sql = [ ...
    'INSERT OR REPLACE INTO ' name ' VALUES ', values, ';' ...
];

sqlite(store.file, 'prepared', sql, struct2cell(flatten_struct(extension)));
