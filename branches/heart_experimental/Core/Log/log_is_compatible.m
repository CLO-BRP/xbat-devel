function out = log_is_compatible(log, sound)

% log_is_compatible - test if a given log can be opened in a given sound
% ----------------------------------------------------------------------

out = (log.sound.samplerate >= sound.samplerate) && (log.sound.channels <= sound.channels);