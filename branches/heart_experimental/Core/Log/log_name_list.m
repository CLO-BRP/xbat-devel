function [names, active, visible] = log_name_list(par, data)

% log_name_list - get list of browser logs and the index of the active log

if nargin < 2 || isempty(data)
    data = get_browser(par);
end

% TODO: this should be a function

if isempty(data.browser.log)
    names = {'(No Open Logs)'}; active = 1; visible = []; return;
end

names = log_name(data.browser.log);

if ~iscell(names)
    names = {names};
end

[names, ix] = sort(names); % logs are sorted as in other controls

if nargout < 2
    return;
end

active = find(ix == data.browser.log_active);

%--
% get indices of available logs
%--

if nargout < 3
    return;
end

visible = struct_field(data.browser.log,'visible');

visible = find(visible(ix));