function device = get_playrec_input_devices(varargin)

% get_playrec_input_devices - what it says
% -----------------------------------------
%
% device = get_playrec_input_devices(varargin)
%
% Input:
% ------
%  same as 'get_playrec_devices'
%
% Output:
% -------
%  device - input devices

device = get_playrec_devices_by_type('input', varargin{:}); 

if ~nargout
	disp(device); clear device;
end