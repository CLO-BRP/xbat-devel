function device = get_playrec_devices(varargin)

% get_playrec_devices - what it says
% ----------------------------------
%
% device = get_playrec_devices('field', value, ... )
%
% Input:
% ------
%  field - device field
%  value - device field value
%
% Output:
% -------
%  device - devices

%--
% get all devices
%--

device = playrec('getDevices');

%--
% possibly filter devices using field and value pairs
%-- 

if isempty(varargin)
	return;
end

% NOTE: this function will throw an error if an improper field is passed

% TODO: consider factoring valid device selection fields

[field, value] = get_field_value(varargin, union(fieldnames(device), 'driver'));

for k = 1:numel(field)
	
	if strcmp(field{k}, 'driver')
		field{k} = 'hostAPI';
	end
	
	for j = numel(device):-1:1
	
		% TODO: make string comparisons case insensitive
		
		if ~isequal(device(j).(field{k}), value{k})
			device(j) = [];
		end
		
	end
	
end

%--
% display output if needed
%--

if ~nargout
	disp(device); clear device;
end
