function root = cuda_root(root)

% cuda_root - where the various tools live
% ----------------------------------------
%
% root = cuda_root
%
% Output:
% -------
%  root - directory

persistent ROOT

% NOTE: we set the root when it is empty or when we are asked to

if isempty(ROOT) || nargin
	
	if nargin
		ROOT = root;
	else
		if ispc
			info = get_windows_info;
			
			if isfield(info, 'cuda_bin_path')
				% NOTE: these variables are typically registered during SDK install
				
				ROOT = fileparts(info.cuda_bin_path);
			else
				% NOTE: below are default installation directories
				
				ROOT = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v3.2';
			end
		else
			ROOT = '/usr/local/cuda';
		end
	end
	
	if ~exist(ROOT, 'dir')
		ROOT = []; error(['Proposed root ''', ROOT, ''' does not exist.']);
	end
end

root = ROOT;

