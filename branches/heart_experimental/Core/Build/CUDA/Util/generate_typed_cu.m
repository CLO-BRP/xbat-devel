function result = generate_typed_cu(source, type)

% generate_typed_cu - code from source template
% --------------------------------------------
%
%   type = generate_typed_cu
%
% result = generate_typed_cu(source, type)
%
% Input:
% ------
%  source - cuda file
%  type - names and types
%
% Output:
% -------
%  type - default types to generate
%  result - file info

%--
% handle input
%--

if nargin < 2
	type.name = { ...
		'int8', 'int16', 'int32', 'int64', ...
		'uint8', 'uint16', 'uint32', 'uint64', ...
		'single', 'double' ...
	};
	
	type.type = { ...
		'char', 'short int', 'int', 'long long int', ...
		'unsigned char', 'unsigned short int', 'unsigned int', 'unsigned long long int', ...
		'float', 'double' ...
	};
	
	if ~nargin
		result = type; return;
	end
end

% NOTE: this function checks the source file name and returns various path and file names

[source, root, file, head] = check_source(source);

%--
% generate file with properly typed code starting from template
%--

% NOTE: this reads the template file removing comments and blank lines

opt = file_readlines; opt.pre = '//'; opt.skip = true;

lines = file_readlines(source, [], opt);

% NOTE: get output file name and get writing handle

file = fullfile(root, file); 

out = get_fid(file, 'wt');

% NOTE: for each type we process the input lines and append the output file

for k = 1:numel(type.type)
	file_process(out, lines, {@type_replace, type.name{k}, type.type{k}});
end

fclose(out.fid);

result = dir(file);

result.root = root;

%--
% generate corresponding header
%--

% NOTE: we read through the generated file and keep 'extern' lines

head = fullfile(root, head);



out = get_fid(head, 'wt');

file_process(out, file, @select_header);

fclose(out.fid);

% TODO: use this approach altogether

lines = unique(file_readlines(head)); file_writelines(head, lines);

another = dir(head);

another.root = root;

result(end + 1) = another;


%--------------------
% TYPE_REPLACE
%--------------------

function line = type_replace(line, name, type)

line = strrep(line, 'CUDA_TYPE_NAME', name);

line = strrep(line, 'CUDA_TYPE', type);


%--------------------
% SELECT_HEADER
%--------------------

function line = select_header(line)

% NOTE: we clear all lines that do not match our desired pattern

trim = strtrim(line);

if numel(trim) < 6 || ~strcmpi('extern', trim(1:6)) || strcmpi('__shared__', trim(8:17))
	line = []; return;
end

%--------------------
% CHECK_SOURCE
%--------------------

function [source, root, file, head] = check_source(source)

[root, file, ext] = fileparts(source);

% NOTE: we check for a .template file

if ~strcmp(ext, '.template')
	error('Input source file should be a ''.template'' file.');
end

% NOTE: we make sure we have a full filename

if isempty(root)
	source = which(source);

	if isempty(source)
		error('Unable to find source file to compile.');
	end
	
	[root, file] = fileparts(source);
end

[ignore, name, ext] = fileparts(file);

% NOTE: we check for a .cu (and previously a .template) file

if ~strcmp(ext, '.cu')
	error('Template file should be ''.cu'' file.');
end

head = [name, '.h'];

