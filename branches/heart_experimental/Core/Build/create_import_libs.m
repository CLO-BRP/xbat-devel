function [def, import] = create_import_libs(destination, clean, ignore)

% create_import_libs - create import libs used for MEX compilation
% ----------------------------------------------------------------
%
% [def, import] = create_import_libs(ignore)
%
% Input:
% ------
%  ignore - symbol prefix characters to ignore
%
% Output:
% -------
%  def - def files
%  import - import libraries

%------------------
% HANDLE INPUT
%------------------

%--
% ignore certain prefixed symbols
%--

if nargin < 3 || isempty(ignore)
	ignore = '?';
end

if nargin < 2 || isempty(clean)
    clean = false;
end

if ~nargin || isempty(destination)
    destination = create_dir(fullfile(libs_root, 'lib'));
end

if isempty(destination)
	error(['Failed to create destination directory ''', destination, '''.']);
end

%------------------
% CREATE LIBS
%------------------

%--
% get tools
%--

% NOTE: this tool extracts symbols from 'dll' files, it is part of 'mingw-utils'

pexports = get_tool('pexports.exe');

if isempty(pexports)
	error('Unable to create import libs, missing ''pexports'' utility. Install ''mingw-utils''.');
end

% NOTE: this tool creates the actual import libraries using the extracted symbols

if ispc
	dlltool = get_mingw_tool('dlltool.exe');
else
	dlltool = get_tool('dlltool.exe');
end

if isempty(dlltool)
	error('Unable to create import libs, missing ''dlltool'' utility. MinGW install needs repair.');
end

%--
% declare libs to process
%--

libs = {'libmat', 'libmex', 'libmx'};

%--
% process libraries to get 'def' files
%--

switch computer
	case {'PCWIN', 'PCWIN32'}
		root = fullfile(matlabroot, 'bin', 'win32');

	case 'PCWIN64'
		root = fullfile(matlabroot, 'bin', 'win64');
end 

disp(' ');

for k = 1:length(libs)	
	
    def{k} = fullfile(destination, [libs{k}, '.def']);
    
    import{k} = fullfile(destination, [libs{k}, '.a']);
    
	%--
	% extract symbols from DLL if needed or requested
	%--
	
	if ~exist(def{k}, 'file') || clean

		str = ['"', pexports.file, '" "', root, filesep, libs{k}, '.dll"'];
		
		[status, result] = system(str); disp(str);
		
		if status
			error(result);
		end
	
		%--
		% create 'def' files
		%--

		symbols = file_readlines(result);

		for j = length(symbols):-1:1

			if ismember(symbols{j}(1), ignore)
				symbols(j) = [];
			end

		end

		file_writelines(def{k}, symbols);

		disp([def{k}, ' (', int2str(numel(symbols) - 2), ')']); disp(' ');
	end
	
	%--
	% build import lib
	%--
    
	if exist(import{k}, 'file') && ~clean
        continue;
    end
	
	str = ['"', dlltool.file, '" --def "', def{k}, '" --output-lib "', import{k}, '"'];
	
	[status, result] = system(str); disp(str); %#ok<NASGU>
    
    if status
        error(result);
    end
	
	disp(import{k}); disp(' ');
	
end 

%--
% supress output
%--

if ~nargout
	clear def;
end

