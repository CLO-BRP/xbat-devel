function ext = all_mexext

% all_mexext - get all current mex extensions and some old ones
% -------------------------------------------------------------
%
% ext = all_mexext
%
% Output:
% -------
%  ext - mex extensions string cell array

ext = mexext('all'); ext = {ext.ext}; ext = union(ext, {'dll', 'mex.bin', 'mex', 'mexsg64', 'mexsg', 'mexsol', 'mexlx'});