function root = devkit_root(platform)

% devkit_root - location
% ----------------------
% 
% root = devkit_root(platform)
%
% Input:
% ------
%  platform - MEX extension (def: mexext)
%
% See also: create_devkit, install_devkit

if ~nargin
	platform = mexext;
end

root = fullfile(app_root, 'DEVKIT', platform);
