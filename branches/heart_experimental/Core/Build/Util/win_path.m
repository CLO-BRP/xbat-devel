function path = win_path;

% win_path - get windows path
% ---------------------------
%
% path = win_path
%
% Output:
% -------
%  path - cell array with elements of windows path

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 879 $
% $Date: 2005-03-31 18:20:23 -0500 (Thu, 31 Mar 2005) $
%--------------------------------

%--
% get path from system
%--

[ignore,path] = system('path');

%--
% pack string into cell array
%--

% NOTE: remove 'PATH=' prefix and 'end of line' the split at semicolons

path = strread(path(6:(end - 1)),'%s','delimiter',';');
