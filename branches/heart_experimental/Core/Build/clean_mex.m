function cleaned = clean_mex(root, opt)

% clean_mex - remove MEX files from their private locations
% ---------------------------------------------------------
%
% cleaned = clean_mex(root, opt)
%
% opt = clean_mex
%
% Input:
% ------
%  root - directory
%  opt - struct
%
% Output:
% -------
%  cleaned - directories
%  opt - struct
%
% NOTE: this function will actually clean all but a set of excluded extensions 

%--
% handle input
%--

if nargin < 2
	opt.verb = false;
	
	opt.exclude = {'.c', '.h', '.m', '.mat', '.txt'};
	
	% NOTE: possibly output clean_mex options
	
	if nargout && ~nargin
		cleaned = opt; return;
	end
end

if ~nargin
    root = app_root;
end

%--
% get MEX directories
%--

cleaned = scan_dir_match('MEX', root);

%--
% clean corresponding 'private' directories
%--

for k = 1:length(cleaned)
	
	% NOTE: these are the conventional MEX file locations, computed as up from 'MEX' down to 'private'
	
	target = fullfile(fileparts(cleaned{k}), 'private');
	
    clean(cleaned{k});
    
	if opt.verb
		disp(' '); disp(['At ', target]);
	end
	
	if exist(target, 'dir')
		clean(target, opt);
	end
end


%----------------------
% CLEAN
%----------------------

function clean(root, opt)

%--
% get directory contents and delete non-excluded files (not other directories)
%--

content = no_dot_dir(root);

for k = 1:length(content)
	
	%--
	% skip directories and excluded types
	%--
	
	if content(k).isdir
		continue;
	end 
	
	[ignore, ext] = strtok(content(k).name, '.'); %#ok<ASGLU>
	
    try
        if string_is_member(ext, opt.exclude)
            continue;
        end
    catch
        nice_catch;
    end
    
	%--
	% delete file
	%--
	
	file = fullfile(root, content(k).name);
	
	try
		if opt.verb
			disp(['  Deleting ''', content(k).name, ''' ...']);
		end
		
		delete(file);
	catch
		nice_catch;
	end
end