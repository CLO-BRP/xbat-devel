function [files, destination] = build_cuda

% build_cuda - MEX files in directory
% -----------------------------------
%
% files = build_cuda
%
% Output:
% -------
%  files - resulting from build
%
% NOTE: this is the analog of 'build'
%
% See also: build, build_cuda_mex

% TODO: finish this if it makes sense?

%--
% check we are in MEX directory
%--

here = pwd;

if here(end) == filesep
    here(end) = [];
end

if ~strncmp(fliplr(here), 'XEM', 3)
    disp(' ');
    disp('This is not a ''MEX'' directory, there is nothing to build!');
    disp(' ');
    
    return;
end

% NOTE: remove platform-specific existing files from MEX directory

delete(['*.', mexext]);

%--
% yield to custom build script
%--

private = fullfile(here, 'private');

%--
% invoke CUDA build script
%--

if exist(fullfile(private, 'build_cuda.m'), 'file')
	
    str_line(64, '_');
    disp(' ');
    disp(['CUSTOM CUDA build of ''', pwd, ''' ...']);
    str_line(64, '_');
    disp(' ');
    
    try
        cd(private); [files, destination] = build_cuda; cd(here); return;
    catch %#ok<CTCH>
		
        cd(here); nice_catch(lasterror, ['Custom CUDA build script failed for ''', here, '''.']); %#ok<LERR>
    end
end