function update_install(file, pretend)

% update_install - tree for platform-specific MEX files
% -----------------------------------------------------
%
% update_install(file, pretend)
%
% Input:
% ------
%  file - to add to MEX install tree
%
% See also: build_all, mex_checkout_root

%--
% handle input
%--

if nargin < 2
	pretend = false;
end

if ischar(file)
	file = {file};
end 

%-- 
% setup
%--

% NOTE: keep track of code root and platform-specific MEX install bundle root

persistent DESTINATION;

if isempty(DESTINATION)
	% TODO: we should have something to describe leaf directory name
	
	if isempty(mex_checkout_root)
		error('You must first set the ''mex_checkout_root''.');
	end
	
	DESTINATION = create_dir(fullfile(mex_checkout_root, [upper(computer), '_', mexext]));
	
	if isempty(DESTINATION)
		error('Unable to create install destination.');
	end
end

%--
% append file move commands to file
%--

for k = 1:length(file)
	
	% NOTE: we are moving files from where they are installed to the MEX install root
	
	output = strrep(file{k}, app_root, DESTINATION);
	
	if isempty(create_dir(fileparts(output)))
		continue;
	end

	if pretend
% 		[ignore, name] = fileparts(file{k}); 
		
		disp(['Copy ''', file{k}, ''' to ''', output, '''.']);
	else
		try
			disp(['Copying ''', file{k}, ''' to ''', output, ''' ...']);
			
			copyfile(file{k}, output);
		catch
			nice_catch(lasterror); continue;
		end
	end
end
