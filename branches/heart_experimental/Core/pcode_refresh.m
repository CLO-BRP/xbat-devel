function pcode_refresh(mode)

% pcode_refresh - refresh pcodes files
% ------------------------------------
% 
% pcode_refresh 'clear'
%
% pcode_refresh 'generate'

%--
% handle input
%--

if nargin < 1
	mode = 'clear';
end

%--
% set list of files to possibly pcode at startup
%--

% NOTE: do this dynamically, at the moment we have a short list

list = { ...
	'xbat_palette', ...
	'control_group', ...
	'palette_toggle', ...
	'browser_palettes', ...
	'browser_controls', ...
	'browser_display' ...
};

%--
% clear or generate pcode
%--

% disp(' '); db_disp(upper(mode));

switch mode

	% NOTE: this mode is best for development
	
	case 'clear'
		
		for k = 1:length(list)

			file = which(list{k});

			if file(end) == 'p'
				clear(list{k}); delete(file); rel_path(file);
			end

		end

	% NOTE: this mode should provide a slight speed improvement as we start
	
	case 'generate'

		for k = 1:length(list)
			clear(list{k}); pcode(list{k}, '-inplace'); rel_path(which(list{k}));
		end

	otherwise

		error('Unrecognized mode option.');

end

disp(' ');
