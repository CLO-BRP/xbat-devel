function value = is_taggable(in)

% is_taggable - check that input is taggable
% ------------------------------------------
%
% value = is_taggable(in)
%
% Input:
% ------
%  in - object to tag
%
% Output:
% -------
%  value - taggable object test result

%--
% strucures with a tags field are taggable
%--

value = isstruct(in) && isfield(in, 'tags');

if ~isstruct(in)
	return;
end

if isfield(in, 'tags')
	value = 1; return;
end

% NOTE: the following code is a thought experiment

% value = 0;
%
% if ~isfield(in, 'type'), return; end
% 
% type = in.type; getter = {[type, '_tags'], ['get_', type, '_tags']}
% 
% for k = 1:numel(getter)
% 	
% 	info = functions(str2func(getter{k}));
% 	
% 	if ~isempty(info.file)
% 		value = 1; return;
% 	end
% 	
% end
