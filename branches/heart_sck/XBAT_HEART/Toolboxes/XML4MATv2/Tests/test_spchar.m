function [result, out, recovered] = test_spchar(in)

% NOTE: we might update these functions, this test will be useful

out = spcharin(in); recovered = spcharout(out); result = strcmp(in, recovered);