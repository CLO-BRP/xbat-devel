function task = pick_task(store, daemon)

% pick_task - from store 
% ----------------------
%
% task = pick_task(store, daemon)
%
% Input:
% ------
%  store - to check
%  daemon - guid
%
% Output:
% -------
%  task - picked

% TODO: consider return of tasks atomized

%--
% handle input
%--

if ~nargin || isempty(store)
	error('Store input is required to pick task.');
end

%--
% pick task
%--

% NOTE: we make sure to decompose non-atomic tasks and ultimately pick an atomic task

task = pick_non_atomic(store, daemon);

if isempty(task)
	task = pick_atomic(store, daemon);
else
	atomize(store, task);

% TODO: resolve problem here before recursion, otherwise this works as is
	
% 	task = pick_task(store, daemon);
end


%----------------------
% ATOMIZE
%----------------------

function child = atomize(store, task)

%--
% get task handle extension and context
%--

ext = get_extensions('task', 'name', task.name);

% TODO: develop context concept for tasks

context.store = store; context.debug = true;

% TODO: this likely indicates a malformed task, consider response

if isempty(ext)
	return;
end

%--
% try to atomize task and pack results into the queue
%--

% NOTE: it is not certain that tasks returned will be atomic? they may need another round, like the asteroids

try
	child = ext.fun.atomize(task, ext.parameter, context);
catch
	extension_warning(ext, 'Task atomization failed.', lasterror);
end

% NOTE: the parent identifier is what we can reasonable set, other fields we cannot here

for k = 1:numel(child)
	child(k).parent_id = task.id;
end

db_disp; disp(child);

child = set_barn_objects(store, child, [], [], 'task');


%----------------------
% PICK_NON_ATOMIC
%----------------------

function task = pick_non_atomic(store, daemon)

task = pick_task_helper(store, daemon, false);


%----------------------
% PICK_ATOMIC
%----------------------

function task = pick_atomic(store, daemon)

task = pick_task_helper(store, daemon, true);


%----------------------
% PICK_TASK_HELPER
%----------------------

function task = pick_task_helper(store, daemon, atomic)

% pick_task_helper - helps daemon pick task
% -----------------------------------------
%
% task = pick_task_helper(store, daemon, atomic
% 
% Input:
% ------
%  store - with tasks
%  daemon - identifier
%  atomic - flag
%
% Output:
% -------
%  task - picked
  
%--
% we use a transaction and corresponding flag to select and mark a task
%--

% db_disp; store

switch lower(store.adapter)
	
	case 'mysql'
		% NOTE: this uses a special trick to avoid using a table in the UPDATE and FROM clauses

		% REF: http://www.xaprb.com/blog/2006/06/23/how-to-select-from-an-update-target-in-mysql/
		
		sql = { ...
			'BEGIN;', ...
			['  UPDATE task SET daemon = ''', daemon, ''''], ... 
			['  WHERE id = (SELECT id FROM (SELECT * FROM task) AS X WHERE atomic ', ternary(atomic, 'IS NOT', 'IS'), ' NULL AND completed_at IS NULL AND daemon IS NULL LIMIT 1);'], ...
			'COMMIT;' ...
		};
		
	otherwise
		% TODO: this is currently failing for SQLite with the JDBC driver
		
		sql = { ...
			'BEGIN;', ...
			['  UPDATE task SET daemon = ''', daemon, ''''], ... 
			['  WHERE id = (SELECT id FROM (SELECT * FROM task) AS X WHERE atomic ', ternary(atomic, 'IS NOT', 'IS'), ' NULL AND completed_at IS NULL AND daemon IS NULL LIMIT 1);'], ...
			'COMMIT;' ...
		};
end

query(store, sql_string(sql));

%--
% get transaction task
%--

task = get_database_objects_by_column(store, 'task', 'daemon', daemon);

if numel(task) > 1
	db_disp multiple-daemon-tasks; 
	
	task = task(1);
end


