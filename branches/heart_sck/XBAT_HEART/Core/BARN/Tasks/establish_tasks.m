function establish_tasks(store, force)

% establish_tasks - make sure that we have a task table
% -----------------------------------------------------
%
% establish_tasks(store, force)
%
% Input:
% ------
%  store - to update
%  force - creation of task table, drop current if it exists
%
% Output:
% -------
%  status - of query
%  result - from query

%--
% set no force default
%--

if nargin < 2
	force = 0;
end

%--
% drop if requested, then make sure we have a task table
%--

if force
	query(store, 'DROP TABLE task;');
end

query(store, create_barn_table('task'));