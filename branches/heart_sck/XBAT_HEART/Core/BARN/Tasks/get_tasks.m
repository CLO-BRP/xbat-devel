function task = get_tasks(store, state, limit)

% get_tasks - request tasks from store in various ways
% ----------------------------------------------------
%
% task = get_tasks(store, state, limit)
%
% Input:
% ------
%  store - with tasks
%  state - of tasks 'waiting', 'started', 'running', 'completed'
%  limit - for request
%
% Output:
% -------
%  task - list

% TODO: reconsider selectors, we want monitors here, we are not picking tasks here

%--
% handle input
%--

if nargin < 3
	limit = [];
end

if nargin < 2
	state = 'waiting';
end

if ~nargin
	store = local_barn;
end 

if isempty(store)
	task = []; return;
end

%--
% get tasks by type
%--
				
if ~is_known_state(state)
	error(['Unrecognized task state ''', state, '''.']);
end

[status, task] = query(store, get_state_query(state, limit)); %#ok<ASGLU>

% db_disp; disp(get_state_query(type, limit)); disp(task);

if ~nargout
	disp(task); clear task;
end


%--------------------------------
% KNOWN_STATES
%--------------------------------

function states = known_states

states = {'waiting', 'started', 'running', 'completed'};


%--------------------------------
% IS_KNOWN_COMMAND
%--------------------------------

function result = is_known_state(state)

result = string_is_member(state, known_states);


%--------------------------------
% GET_REQUEST_QUERY
%--------------------------------

function sql = get_state_query(state, limit)

%--
% handle input
%--

if nargin < 2
	limit = 1;
end

%--
% build common prefix and suffix parts of query
%--

if strcmp(state, 'completed')
	prefix = 'SELECT * FROM task WHERE completed_at IS NOT NULL '; 
else
	prefix = 'SELECT * FROM task WHERE completed_at IS NULL '; 
end

if isempty(limit)
	suffix = ';';
else
	suffix = [' LIMIT ', int2str(limit), ';'];
end

%--
% build query based on command
%--

switch state
	
	case 'waiting'
		sql = [prefix, 'AND started_at IS NULL ORDER BY created_at', suffix];
		
	case 'started'
		sql = [prefix, 'AND started_at IS NOT NULL ORDER BY started_at', suffix];
	
	case 'running'
		sql = [prefix, 'AND running = 1 ORDER BY started_at', suffix];
		
	case 'completed'
		sql = [prefix, 'ORDER by completed_at DESC', suffix];
		
end
