function [result, table] = has_barn_table(store, name)

sql = ['SELECT * FROM barn_master WHERE name = ''', name, ''';'];

[status, table] = query(store, sql);

result = ~isempty(table);