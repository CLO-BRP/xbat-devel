function [query, lines, obj] = create_barn_table(table, opt)

% create_barn_table - generate create query for a BARN object table
% -----------------------------------------------------------------
%
% [query, lines, obj] = create_barn_table(table, force)
%
%                     = create_barn_table(table, opt)
%
%                 opt = create_barn_table
%
% Input:
% ------
%  table - name
%  force - create
%  opt - create options
%
% Output:
% -------
%  query - string
%  lines - query representation
%  obj - prototype
%  opt - default create options
%
% See also: create_table, get_barn_prototype

%--
% set default create options and handle force input
%--

if nargin < 2
	opt = false; 
end

% NOTE: this implements the older force only option

if ~isstruct(opt)
	force = opt; opt = create_table; opt.force = force;
end

if ~nargin
	query = opt; return;
end

%--
% build object prototype
%--

% NOTE: the prototype includes structure fields and type hints

% TODO: get further description output here, then pass it to create_table

[obj, description] = get_barn_prototype(table); 

if ~trivial(description)
	opt.column = description;
end

%--
% generate create table query
%--

% NOTE: when no output is requested we display the query

if ~nargout
	create_table(obj, table, opt); return;
end

[query, lines] = create_table(obj, table, opt);


