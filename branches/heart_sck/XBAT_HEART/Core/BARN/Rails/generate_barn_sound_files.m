function generate_barn_sound_files(file)

info = sound_file_info(file);

output = transcode_file(file, 'wav', info);


%--------------------------------
% TRANSCODE_FILE
%--------------------------------

function output = transcode_file(file, format, info)

% NOTE: the output gets written to the same directory

% TODO: if we want to resample the file we should convert it to a sound first

%--
% handle input
%--

if nargin < 3
	info = sound_file_info(file);
end

[destination, filename, ignore] = fileparts(file);

%--
% determine paging parameter
%--

% NOTE: we intend to process the file a megabyte at a time

total_samples = info.samples * info.channels;

page.frames = floor(min(total_samples, 4 * 10^6) / info.channels);

%--
% build output file name or names
%--

% NOTE: up to stereo transcode for many channel files create single channel files as well

if info.channels < 2
	
	output = fullfile(destination, [filename, '.', format]);
else

	for channel = 1:info.channels
		output{channel} = fullfile(destination, [filename, '.', int2str(channel), '.', format]);
	end
	
end

%--
% page through sound and transcode to output file or files
%--

page.start = 0; 

% NOTE: make sure that we are in append mode

opt = sound_file_write(output); opt.append = 1;

while page.start < info.samples
	
	[samples, rate] = sound_file_read(file, page.start, page.frames);

	if info.channels <= 2 
		
		sound_file_write(output, samples, rate, opt);
		
	else
		
		for channel = 1:numel(info.channels)
			sound_file_write(output{channel}, samples(:, k), rate, opt);
		end
		
	end
	
end