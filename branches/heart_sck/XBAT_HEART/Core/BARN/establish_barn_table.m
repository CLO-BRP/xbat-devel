function table = establish_barn_table(store, table, type, sql)

% establish_barn_table - create and keep track in master
% ------------------------------------------------------
%
% table = establish_barn_table(store, table, type, sql)
%
% Input:
% ------
%  store - database
%  table - name
%  type - of table
%  sql - to create table
%
% Output:
% -------
%  table - row from master

%--
% handle input
%--

% NOTE: try to generate create query from name

if nargin < 4
	
	sql = create_barn_table(table);
	
	if isempty(sql) 
		error(['Unrecognized BARN table ''', table, '''.']);
	end
end

% NOTE: the default is a 'core' table

if nargin < 3 
	type = 'core';
end

% TODO: check table 'type' when these are well defined, types under consideration: 'core', 'relation', 'extension'

%--
% create table
%--

query(store, sql);

%--
% add entry to master
%--

if ~nargout
	set_barn_master(store, type, table, sql); clear table;
else
	table = set_barn_master(store, type, table, sql);
end



