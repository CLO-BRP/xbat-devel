function barn = get_active_barn

% get_active_barn - description
% -----------------------------
%
% barn = get_active_barn
%
% Output:
% -------
%  barn - store, server, and current user

[barn.store, barn.server] = local_barn;

barn.user = set_barn_user(barn.store, get_active_user);