function selection_patch_callback(obj, eventdata, sel, opt)

% selection_patch_callback - patch callback router
% ------------------------------------------------
%
% selection_patch_callback(obj, eventdata, sel, opt)
%
% Input:
% ------
%  obj, eventdata - matlab callback input
%  sel - selection
%  opt - selection options

%--
% get selection options from axes if needed
%--

if isempty(opt)
	opt = get_axes_selection_options(get(obj, 'parent'));
end

%--
% execute custom patch callback
%--

if double_click(obj)

	if ~isempty(opt.callback.patch.double_click)
		eval_callback(opt.callback.patch.double_click, obj, eventdata, sel, opt);
	end

else

	if ~isempty(opt.callback.patch.click)
		eval_callback(opt.callback.patch.click, obj, eventdata, sel, opt);
	end

end
