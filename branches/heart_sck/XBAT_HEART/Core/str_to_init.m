function init = str_to_init(str)

% string_to_initials - convert string typically name to initials
% --------------------------------------------------------------
%
% init = str_to_init(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  init - initials string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% remove leading and trailing blanks
%--

str = fliplr(deblank(fliplr(deblank(str))));

%--
% collapse multiple spaces to one
%--

len0 = length(str);
len1 = 0;

while (len0 ~= len1)
	len0 = length(str);
	str = strrep(str,'  ',' ');
	len1 = length(str);
end

%--
% find spaces and get initials
%--

ix = [0, findstr(str,' ')];

init = str(ix + 1);