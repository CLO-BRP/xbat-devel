function d = hist_divergence(p,q)

% hist_divergence - compute kullback divergence of histograms
% -----------------------------------------------------------
%
% d = hist_divergence(p,q)
%
% Input:
% ------
%  p,q - histograms over same events
%
% Output:
% -------
%  d - kullback divergence

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-12-15 13:52:40 -0500 (Thu, 15 Dec 2005) $
% $Revision: 2304 $
%--------------------------------

%--
% normalize histograms if needed
%--

s = sum(p(:)); 
if (s ~= 1)
	p = p / s;
end

s = sum(q(:));
if (s ~= 1)
	q = q / s;
end

%--
% remove p zero bins
%--

ix = find(p > 0);
p = p(ix);
q = q(ix);

%--
% handle q zero bins
%--

if (any(q == 0))
	warning('Kullback divergence may be infinite.');
end

ix = find(q > 0);
p = p(ix);
q = q(ix);

%--
% compute kullback divergence
%--

d = sum(p.*log2(p./q));




