function y = dexp_pdf(c,p)

% dexp_pdf - density function for double exponential distribution
% ---------------------------------------------------------------
%
% y = dexp_pdf(c,p)
%
% Input:
% ------
%  c - points of evaluation for density
%  p - distribution parameters
%
% Output:
% -------
%  y - density values

%--
% evaluate density for positive and negative values
%--

ixn = find(c < 0);
ixp = find(c >= 0);

y = zeros(size(c));

y(ixn) = exppdf(-c(ixn),p(1));
y(ixp) = exppdf(c(ixp),p(2));