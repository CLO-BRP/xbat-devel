function [p,ci] = dexp_fit(x,a)

% dexp_fit - fit double exponential distribution
% ----------------------------------------------
%
% [p,ci] = dexp_fit(x,a)
%
% Input:
% ------
%  c - points of evaluation for density
%  p - distribution parameters
%
% Output:
% -------
%  y - density values

%--
% evaluate density for positive and negative values
%--

ixn = find(c < 0);
