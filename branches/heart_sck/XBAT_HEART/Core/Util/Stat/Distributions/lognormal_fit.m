function p = lognormal_fit(X)

% lognormal_fit - fit lognormal distribution
% ------------------------------------------
%
% p = lognormal_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
% p - lognormal parameters [mean, deviation]

%--
% compute log of data
%--

% remove zeros from values

X = X(:);
ix = find(X == 0);
X(ix) = [];

% compute log transformation

X = log(X);

%--
% compute mean and deviation of data
%--

p(1) = nanmean(X);
p(2) = nanstd(X);
