function [index, cat] = cat2index(cat)

% cat2index - convert categorical array to indices
% ------------------------------------------------
%
% [index, cats] = cat2index(cat)
%
% Input:
% ------
%  cat - categorical array
%
% Output:
% -------
%  index - representation
%  cats - categories

[cat, ignore, index] = unique(cat);