function cat = index2cat(index, cats)

% index2cat - index representation of categorical array to strings
% ----------------------------------------------------------------
%
% cat = index2cat(index, cats)
%
% Input:
% ------
%  index - array
%  cats - categories
%
% Output:
% -------
%  cat - categorical string cell array

cat = cats(index);