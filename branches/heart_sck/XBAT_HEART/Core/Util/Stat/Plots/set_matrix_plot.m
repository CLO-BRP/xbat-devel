function set_matrix_plot(par, varargin)

% set_matrix_plot - properties
% ----------------------------
%
% set_matrix_plot(par, field, value, ... )
%
% Input:
% ------
%  par - matrix plot figure
%  field - name
%  value - for field

%--
% handle input and setup
%--

prefix = 'matrix_plot::';

[fields, values] = get_field_value(varargin); 

%--
% update properties
%--

for k = 1:numel(fields)
	
	field = fields{k}; value = values{k};
	
	switch field
		
		case 'title'
			handle = findobj(par, [prefix, 'title']);
			
			if ~isempty(handle)
				set(handle, 'string', value);
			end
			
		case 'xvars'
			
		case 'yvars'
			
		case 'gvar'
			
		case 'callback'
			
		otherwise
			
	end
end