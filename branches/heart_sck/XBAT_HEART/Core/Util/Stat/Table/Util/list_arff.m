function file = list_arff(root)

% list_arff - files starting at root
% ----------------------------------
%
% file = list_arff(root)
%
% Input:
% ------
%  root - directory for search (def: pwd)
%
% Output:
% -------
%  file - list
%
% See also: arff_to_table

if ~nargin
	root = pwd;
end

file = scan_dir_for('arff', root);

