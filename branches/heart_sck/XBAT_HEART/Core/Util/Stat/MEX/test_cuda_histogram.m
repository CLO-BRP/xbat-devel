function test_cuda_histogram(n, top)

% test_cuda_histogram - over a range of input types
% -------------------------------------------------
% 
% test_cuda_histogram(n, top)
%
% Input:
% ------
%  n - size of 'rand' matrix
%  top - value input to histogram, it is used to scale 'rand' values
%
% NOTE: this test fails for 'int8' if top > 127 because the values are beyond the range

%--
% handle input
%--

if nargin < 2
	top = 127;
end 

if ~nargin
	n = 1000;
end 

X = round(top * rand(n));

% NOTE: the pattern of casting some input and comparing the result for the various types will recur, factor

% NOTE: the function 'get_cast_types' is related to this, it needs some updates

types = {@uint8, @uint16, @uint32, @uint64, @int8, @int16, @int32, @int64, @single};

h0 = cuda_histogram256_mex(X, [0, 255]);

disp(' ');

for k = 1:numel(types)
	h1 = cuda_histogram256_mex(types{k}(X), [0, 255]);
	
	if n < 12
		types{k}(X)
	end
	
	if max(abs(h0 - h1)) > 0
		disp(upper(['Failed test for ', func2str(types{k}), ' !!!!!!!!!!!']));
	else
		disp(['Passed test for ', func2str(types{k})]);
	end
end

disp(' ');