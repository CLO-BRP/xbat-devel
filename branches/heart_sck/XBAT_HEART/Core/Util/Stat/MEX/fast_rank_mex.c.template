//----------------------------------------------
// INCLUDE FILES
//----------------------------------------------

#include "stdlib.h"

#include "math.h"

#include "mex.h"

//----------------------------------------------
// FUNCTIONS
//----------------------------------------------

// BEGIN-EXPAND-TYPES

#define ELEM_SWAP(a, b) {MEX_TYPE t = (a); (a) = (b); (b) = t;}

// NOTE: this is a k-th smallest computation, used in rank-statistics computations

MEX_TYPE kth_smallest_MEX_TYPE_NAME(MEX_TYPE *x, int N, int k);

MEX_TYPE kth_smallest_MEX_TYPE_NAME(MEX_TYPE *x, int N, int k)
{
	register int i, j, l, m;
	
	register MEX_TYPE tmp;
	
	l = 0; m = N - 1;
	
	while (l < m) {
		
		tmp = x[k];
		
		i = l; j = m;
		
		do {
			while (x[i] < tmp) i++;
			while (tmp < x[j]) j--;
			
			if (i<=j) {
				ELEM_SWAP(x[i], x[j]);
				i++;
				j--;
			}
		} while (i <= j);
		
		if (j < k) l = i;
		if (k < i) m = j;
	}
	
	return x[k];
}

#undef ELEM_SWAP

// END-EXPAND-TYPES


//----------------------------------------------
// MEX FUNCTION
//----------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	void *X, *Y, *v; int N, k, j;
	
	//-----------------
	// INPUT
	//-----------------
	
	X = mxGetPr(prhs[0]); N = mxGetM(prhs[0]) * mxGetN(prhs[0]);
		
	k = ((int) mxGetScalar(prhs[1])) - 1;
	
	if ((k < 0) || (k > N - 1)) {
		mexErrMsgTxt("Rank parameter is out of range.");
	}
	
	//-----------------
	// COMPUTE
	//-----------------
	
	switch (mxGetClassID(prhs[0]))
	{
		// BEGIN-EXPAND-TYPES
		
		case MEX_TYPE_CLASS:
			
			Y = mxCalloc(N, sizeof(MEX_TYPE));
			
			// NOTE: we copy data because this is an in-place computation
			
			for (k = 0; k < N; k++) {
				*((MEX_TYPE *) Y + k) = *((MEX_TYPE *) X + k);
			}
			
			v = mxGetPr(plhs[0] = mxCreateNumericMatrix(1, 1, MEX_TYPE_CLASS, mxREAL));
			
			*((MEX_TYPE *) v) = kth_smallest_MEX_TYPE_NAME(Y, N, k);

		// END-EXPAND-TYPES
	}
}
