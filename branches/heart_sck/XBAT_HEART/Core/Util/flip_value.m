function value = flip_value(value, pivot)

if nargin < 2
	pivot = 1; 
end 

value = 1 - value;
