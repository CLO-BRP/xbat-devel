function colors = similar_css_colors(color, count)

% similar_css_colors - get similar colors to color
% ------------------------------------------------
%
% colors = similar_css_colors(color, count)
%
% Input:
% ------
%  color - struct or triplet
%  count - of similar colors
%
% Output:
% -------
%  colors - similar to input

%--
% handle input
%--

if nargin < 2
	count = 12;
end

% NOTE: we accept a color struct or a color vector

if isstruct(color)
	rgb = color.rgb;
else
	rgb = color(:)';
end

%--
% load, rank, and output colors
%--

colors = load_css_colors; 

% TODO: this might be better if we use HSV instead of RGB

offset = sum(abs(repmat(rgb, numel(colors), 1) - struct_field(colors, 'rgb')), 2);

[offset, ix] = sort(offset);

colors = colors(ix(1:count));

