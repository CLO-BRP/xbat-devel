function color = get_color_by_name(name, match)

% get_color_by_name - from named CSS colors
% -----------------------------------------
%
% color = get_color_by_name(name, match)
%
% Input:
% ------
%  name - or part of name
%  match - 'full', 'begins', or 'contains'
%
% Output:
% -------
%  color - found

%--
% set default full match
%--

if nargin < 2
	match = 'full';
end

%--
% load colors and try to find some by name
%--

colors = load_css_colors; color = empty(colors);

% NOTE: we try fieldname, then name (requires spaces) in case insensitive way

field = {'fieldname', 'name'}; name = lower(name);

for k = 1:numel(field)

	candidates = lower({colors.(field{k})});

	switch match

		case 'full'
			selected = strcmp(candidates, name);

		case 'begins'
			selected = string_begins(candidates, name);

		case 'contains'
			selected = string_contains(candidates, name);

	end
	
	% NOTE: we return as soon as we find some colors
	
	if any(selected)
		color = colors(selected); return;
	end
	
end
