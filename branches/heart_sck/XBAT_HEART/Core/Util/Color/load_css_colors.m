function color = load_css_colors

% load_css_colors - from file
% ---------------------------
%
% color = load_css_colors
%
% Output:
% -------
%  color - description struct array

%--
% colors persist
%--

persistent COLORS;

if ~isempty(COLORS)
	color = COLORS; return;
end

%--
% read and parse colors
%--

% NOTE: the list of colors comes from http://www.w3schools.com/css/css_colornames.asp

lines = file_readlines('css-colors.txt');

color = struct;

for k = 1:numel(lines)
	
	part = str_split(lines{k}); part{2}(1) = [];
	
	color(k).fieldname = part{1};
	
	[color(k).name, color(k).parts] = split_camel(part{1}); 
	
	color(k).hex = part{2}; 
	
	color(k).rgb = hex_to_rgb(part{2});
	
	if any(isnan(color(k).rgb))
		disp(color(k));
	end 
end

%--
% persist colors
%--

COLORS = color;

