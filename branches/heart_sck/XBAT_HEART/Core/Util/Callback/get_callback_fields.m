function fields = get_callback_fields(handle)

% get_callback_fields - get callback fields implemented for handle
% ----------------------------------------------------------------
%
% fields = get_callback_fields(handle)
%
% Input:
% ------
%  handle - handle
%
% Output:
% -------
%  fields - handle implemented callback fields

% TODO: add 'java' option that forces consideration of the java handle, and provides it as output if needed

%--
% get handle type
%--

% TODO: watch out for 'UNKNOWN' type
	
type = get_handle_type(handle);
	
%--
% try to get fields from persistent store, these fields do not change
%--

persistent TYPE_CALLBACK_FIELDS; 

if ~isempty(TYPE_CALLBACK_FIELDS)
	
	if isfield(TYPE_CALLBACK_FIELDS, type)
		fields = TYPE_CALLBACK_FIELDS.(type); return;
	end
	
end

%--
% get fields
%--

fields = fieldnames(get(handle));

for k = length(fields):-1:1
	
	current = lower(fields{k});
	
	if ~isempty(strfind(current, 'fcn')) || (~isempty(strfind(current, 'callback')) && isempty(strfind(current, 'callbackdata')))
		continue;
	end
	
	fields(k) = [];
	
end

%--
% store fields
%--

TYPE_CALLBACK_FIELDS.(type) = fields;
