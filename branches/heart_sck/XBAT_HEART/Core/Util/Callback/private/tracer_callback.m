function tracer_callback(obj, eventdata, name) %#ok<INUSL>

% TODO: improve tracer display, add wrapping and profiling

type = get_handle_type(obj);

switch type
	
	case 'uicontrol'
		
		%--
		% store java handle in userdata
		%--
		
		% TODO: use the keypress function with the java handle to make 'edit' controls 'active'
		
		data = get(obj, 'userdata'); 
		
		% NOTE: we clobber any non-struct 'userdata', we also repeatedly try to get java handle if we fail
		
		if ~isstruct(data)
			jobj = findjobj(obj); clear data; data.jobj = jobj;
		else
			if isfield(data, 'jobj') && ~isempty(data.jobj)
				jobj = data.jobj;
			else
				jobj = findjobj(obj); data.jobj = jobj;
			end 
		end 
		
		set(obj, 'userdata', data);
		
		%--
		% display current state
		%--
		
% 		disp(get(obj, 'style')); jobj
		
		switch get(obj, 'style')
		
			% NOTE: the 'eventdata' contains key information in the case of 'keypressfcn' callback
			
			case 'edit'
				value = get(jobj, 'Text');
				
			otherwise
				value = '';
				
		end
	
	case 'TextFieldUI'
		
		value = get(obj, 'Text');
		
	otherwise

		value = '';
		
end

% get(obj) 

disp([type, '::', name, '::', value]);
