function [callback, field, value] = get_callbacks(handle)

% get_callbacks - get various callbacks for handle
% ------------------------------------------------
%
% [callback, field, value] = get_callbacks(handle)
%
% Input:
% ------
%  handle - handle
%
% Output:
% -------
%  callback - struct
%  field, value - decomposition of struct

field = get_callback_fields(handle); 

value = get(handle, field);

callback = pack_field_value(field, value);