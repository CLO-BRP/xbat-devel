function obj = create_timer(name, callback, rate, mode, replace, varargin)

% create_timer - with given name, singleton
% -----------------------------------------
%
% obj = create_timer(name, callback, rate, mode, varargin)
%
% Input:
% ------
%  name - of timer
%  callback - for timer
%  rate - for timer
%  mode - for execution
%  replace - set true to destroy and replace existing timer object
%
% Output:
% -------
%  obj - timer

%--
% handle input
%--

if ~nargin || isempty(name)
	error('Singleton timer must be named.');
end

if nargin < 5 || isempty(replace)
	replace = false;
end

if nargin < 4 || isempty(mode)
	mode = 'fixedRate';
end

if nargin < 3 || isempty(rate)
	rate = 1;
end

if nargin < 2 || isempty(callback)
	callback = @tracer;
end

%--
% check for timer, create if needed
%--

obj = timerfind('name', name);

if ~isempty(obj)
	if replace
		stop(obj); delete(obj);
	else
		% TODO: warn if requested properties different, or simply update these
		
		return;
	end
end

obj = timer;

set(obj, ...
	'Name', name, ...
	'ExecutionMode', mode, ...
	'BusyMode', 'drop', ...
	'Period', rate, ...
	'TimerFcn', callback, ...
	varargin{:} ...
);

%-----------------------
% TRACER
%-----------------------

function tracer(obj, eventdata)

db_disp; disp(obj); disp(eventdata);



