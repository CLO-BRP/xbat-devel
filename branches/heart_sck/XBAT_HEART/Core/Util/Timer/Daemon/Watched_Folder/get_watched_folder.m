function value = get_watched_folder(daemon, field)

% get_watched_folder - daemon property
% ------------------------------------
%
% value = get_watched_folder(daemon, field)
%
% Input:
% ------
%  daemon - name or timer
%  field - property
%
% Output:
% -------
%  value - for property

%--
% get daemon from name
%--

if ischar(daemon)
	daemon = timerfind('name', daemon);
end

%--
% get daemon property, which may be timer property
%--

switch field
	
	case 'name'
		value = get(daemon, 'name');
		
	case 'list'
		callback = get(daemon, 'timerfcn'); value = callback{2};
		
	case 'callback'
		callback = get(daemon, 'timerfcn'); value = callback{3};
		
	% NOTE: this is misnamed, however it should be changed starting with 'create_timer'
	
	case 'rate'
		value = get(daemon, 'period');
		
	% NOTE: when we get some other request pass it to the timer object 'get'
	
	otherwise
		value = get(daemon, field);
		
end

