function test_save_yaml

out_struct = load_yaml(which('database.example.yml'));

assert( isa(out_struct, 'struct') );

file = [mfilename('fullpath'), '.yml'];

% the output default status of saving a file is '0'
status = save_yaml(file, out_struct);

assert(status == 0);

yaml_struct = load_yaml(file);

assert( isstruct(yaml_struct.development) );
assert( ischar(yaml_struct.development.adapter) );
assert( isempty(yaml_struct.development.password) );

% cleanup
delete(file);