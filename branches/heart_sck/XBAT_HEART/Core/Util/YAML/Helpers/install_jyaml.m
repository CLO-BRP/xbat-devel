function installed = install_jyaml

% load_jyaml - utility to add jyaml to classpath
% ----------------------------------------------
%
% installed = install_jyaml
%
% Output:
% -------
%  installed - indicator

% NOTE: this means that the jar must be in the MATLAB path to start

jar = which('jyaml-1.3.jar');

if isempty(jar)
	installed = 0;
else
	append_classpath(jar); installed = 1;
end
