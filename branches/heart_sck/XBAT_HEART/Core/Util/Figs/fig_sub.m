function h = fig_sub(m, n, ix, h)

% fig_sub - like subplot but for figures
% --------------------------------------
%
% h = fig_sub(m, n, ix, h)
%
% Input:
% ------
%  m, n - rows and columns in tiling
%  ix - position in tiling
%  h - handle of figure to position (def: gcf)
%
% Output:
% -------
%  h - handle to figure

%--
% compute location of figure
%--

% NOTE: 'figs_tile' has a bug, 10000 is a workaround

[p, s] = figs_tile(m, n, 10000); 

%--
% position existing figure or create new figure and position
%--

if ix <= (m * n)

	%--
	% set figure handle if needed
	%--
	
	if (nargin < 4) || isempty(h)
		h = gcf;
	end
	
	%--
	% position figure
	%--
	
	units = get(h,'units');
	
	if ~strcmp(units, 'pixels')
		set(h, 'units', 'pixels'); 
	end
	
	set(h, 'Position', [p(ix,:), s]);
	
	set(h, 'units', units);
	
	%--
	% evaluate resize function if needed
	%--
	
	resize = get(h, 'resizefcn');
	
	if ~isempty(resize)
		
		if resize(end) == ';'
			resize = resize(1:end - 1);
		end
		
		feval(resize, h);	
	end
		
else

	error('Position index in figure tiling exceeds size of tiling.');
	
end
