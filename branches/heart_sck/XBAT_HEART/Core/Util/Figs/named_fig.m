function h = named_fig(name, par, varargin)

% named_fig - get named fig, create if needed
% -------------------------------------------
%
% h = named_fig(name, par, m, n, k)
%
% Input:
% ------
%  name - name
%  par - parent
%  m, n - figure rows and columns
%  k - position
%
% Output:
% -------
%  h - handle to figure

%--
% set no parent default
%--

if nargin < 2
	par = [];
end 

%--
% build name and check for fig
%--

tag =  ['FIG::', name];

h = findobj(0, 'type', 'figure', 'tag', tag);

if ~isempty(h)
	return;
end

%--
% create figure
%--

h = fig(varargin{:});

set(h, 'tag', tag);

%--
% add parent if needed
%--

if ~isempty(par)
	data = get(h, 'userdata'); data.parent = par; set(h, 'userdata', data);
end
