function result = ftl(varargin)

% ftl - fastlook indexed search
% -----------------------------
%
% ftl add, ftl add all, ftl add dir1 dir2 ...
%
% ftl query, result = ftl(query)
%
% Input:
% ------
%  query - string
%
% Output:
% -------
%  result - of query 

% TODO: after any update to the store recompute stats

% NOTE: stats should include counts for the various objects, possibly files by type, modification date related

% TODO: develop watched folder 'ftl_daemon' to maintain index current

% TODO: implement some utility commands such as 'hash' to display the current and stored hashes of a file or files in a directory

% TODO: add tagging to 'ftl'? if it is about finding thigs it could make sense, they must persist in a reasonable way through updates

verb = true;

switch numel(varargin)
	
	case 0
		% TODO: display list of indexed folders
		
	case 1
		switch varargin{1}
			
			case {'add', 'append'}
				result = ftl_add(pwd, verb);
			
				if verb, disp(' '); end
				
			case {'rm', 'del', 'delete'}
				result = ftl_del(pwd, verb);
				
				if verb, disp(' '); end
				
			case 'clean'
				result = ftl_clean(pwd, verb);
				
				if verb, disp(' '); end
				
			case 'sync'
				result = ftl_sync(pwd, verb);
				
				if verb, disp(' '); end
				
			otherwise
				search = parse_query(varargin); 
				
				if ~isfield(search, 'root')
					search.root = pwd;
				end
				
				% TODO: this could be refactored to a single call, not important
				
				result = run_query(search);
								
				if verb
					disp(' ');
					disp('  Updating database ...');
				end
				
				result = update_query(search, result, verb);
				
				if ~nargout
					ftl_disp(result);
				end	
		end
			
		if ~nargout
			clear result;
		end
		
	otherwise
		
		switch varargin{1}
			
			% NOTE: this implements 'ftl add all' and 'ftl add dir1 dir2 ...' and others
			
			case {'add', 'append', 'rm', 'del', 'delete', 'clean', 'sync'}, 
				
				if numel(varargin) == 2 && strcmp(varargin{2}, 'all')
					list = scan_dir(pwd);
				else
					list = varargin(2:end);
				end
								
				switch varargin{1}
					case {'add', 'append'}
						fun = @ftl_add;
						
					case 'clean'
						fun = @ftl_clean;
						
					case {'del', 'delete'}
						fun = @ftl_del;
						
					case 'sync'
						fun = @ftl_sync;
				end
				
				for k = 1:numel(list)					
					result(k) = fun(list{k}, verb); %#ok<*AGROW>
				end
				
				if ~nargout
					disp(' '); clear result;
				end
				
			otherwise							
				search = parse_query(varargin);
				
				if ~isfield(search, 'root')
					search.root = pwd;
				end
				
				result = run_query(search);
						
				if verb
					disp(' ');
					disp('  Updating database ...');
				end
				
% 				result = update_query(search, result, verb);
				
				if ~nargout
					ftl_disp(result);
				end				
		end
		
		if ~nargout
			clear result;
		end
end


%----------------------------
% PARSE_QUERY
%----------------------------

% TODO: implement a modification date based search option, we may need to update schema

function search = parse_query(args)

%--
% select options
%--

search = struct; line_types = {'source', 'comments'};

for k = 1:numel(args)
	
	current = args{k};
	
	% NOTE: determine whether we have a directive or pattern token
	
	if string_begins(current, 'in:')
		type = 'in'; start = 4;
		
	elseif string_begins(current, 'not:')
		type = 'not'; start = 5;
		
	elseif string_begins(current, 'root:')		
		type = 'root'; start = 6;
		
	else
		type = 'pattern';
	end
	
	% NOTE: here we pack the various directives and patterns
	
	switch type
		
		case {'in', 'not'}
			
			if ~isfield(search, type)
				search.(type) = struct;
			end 
			
			% TODO: reconsider this syntax it is ugly
			
			value = str_split(current(start:end), '|');
			
			% NOTE: the 'in' or 'not' directive may specify file types and source or comment lines
			
			if string_is_member(value, line_types)
				
				search.(type).comments = strcmp(value, 'comments');
			else
				if isfield(search.(type), 'type')
					search.(type).file_type = [search.(type).file_type; value];
				else
					search.(type).file_type = value;
				end
			end			
			
		case 'root'
			% NOTE: what should we do about the space problem? require quotes, hence the start
			
			search.root = current(start + 1:(end - 1));
			
		case 'pattern'
			% NOTE: we can ask for lines that match multiple patterns
			
			if isfield(search, 'pattern')
				search.pattern{end + 1} = current;
			else
				search.pattern = {current};
			end
	end	
end

% db_disp search; disp(flatten(search));


%----------------------------
% RUN_QUERY
%----------------------------

function result = run_query(search)

%--
% setup
%--

% TODO: allow configuration functional syntax is much easier to implement

% TODO: allow paging through results

limit = 500;

store = ftl_store;

search.store = store;

%--
% build query elements from search
%--

% TODO: consider letting the user handle the 'like' match

for k = 1:numel(search.pattern)
	search.pattern{k} = ['%', strrep(search.pattern{k}, '_', '\_'), '%'];
end

part = {'(', ['line.content LIKE ''', search.pattern{1}, ''' ESCAPE ''\''']};

% TODO: consider using the OR connective for patterns, typically we have one pattern

for k = 2:numel(search.pattern)
	part{end + 1} = ['AND line.content LIKE ''', search.pattern{k}, ''' ESCAPE ''\'''];
end

part{end + 1} = ')';

if isfield(search, 'in')
	
	if isfield(search.in, 'comments')
		part{end + 1} = ['AND ( line.comment = ', ternary(search.in.comments, '1', '0'), ' )'];
	end
	
	% NOTE: here we add the condition that the 'file.type' must be one of the specified ones
	
	if isfield(search.in, 'file_type')
		part{end + 1} = 'AND (';
		
		part{end + 1} = ['file.type = ''.', search.in.file_type{1}, ''''];
		
		for k = 2:numel(search.in.file_type)
			part{end + 1} = ['OR file.type = ''.', search.in.file_type{k}, ''''];
		end
		
		part{end + 1} = ')';
	end
end

% TODO: consider contradictions when we implement this

% if isfield(search, 'not');
% 	
% 	if isfield(search.not, 'comments')
% 		part{end + 1} = ['AND line.commment = ', ternary(search.in.comments, '0', '1')];
% 	end
% 	
% end

if ~isempty(search.root)
	% NOTE: we first escape existing backslashes, then escape underscore
	
	prefix = strrep(strrep(search.root, '\', '\\'), '_', '\_');
	
	part{end + 1} = ['AND ( location.location LIKE ''', prefix, '%'' ESCAPE ''\'' )'];
end

where = str_implode(strtrim(strcat({' '}, strtrim(part))), ' ');

% db_disp where; where

if isempty(search.root)
	from = 'line JOIN file ON line.file_id = file.id';
else
	from = 'line JOIN file ON line.file_id = file.id JOIN location ON file.location_id = location.id';
end

%--
% build and run search queries
%--

sql = ['SELECT COUNT(line.id) AS total FROM ', from, ' WHERE ', where];

search.query.count = sql;

[status, count] = query(store, sql); %#ok<ASGLU>

% NOTE: a '*' select in the next query crashes the MEX, try to figure out why!

sql = ['SELECT line.* FROM ', from, ' WHERE ', where, ' LIMIT ', int2str(limit)];

search.query.lines = sql;

[status, line] = query(store, sql); %#ok<ASGLU>

files = unique([line.file_id]);

if isempty(files)
	% NOTE: when search result is empty we pack partial result and return
	
	result.total = 0; result.root = search.root; result.limit = limit; result.search = search; return;
end

%--
% pack results with a file orientation
%--

% NOTE: loading 'file' relations retrieves corresponding 'location' objects

opt = get_database_objects; opt.load_relations = true;

file = get_database_objects(store, 'file', files, opt);

% NOTE: here we pack the found lines as children of their respective files

for k = 1:numel(file)
	file(k).line = line(file(k).id == [line.file_id]);
end

result.file = file;

result.total = count.total;

result.root = search.root;

result.limit = limit;

result.search = search;


%----------------------------
% UPDATE_QUERY
%----------------------------

function result = update_query(search, result, verb)

if nargin < 3
	verb = false;
end

location = [result.file.location];

location = unique({location.location});

% NOTE: this does not happen

if ischar(location)
	location = {location};
end

if verb < 2
	verb = false;
end 

for k = 1:numel(location)	
	ftl_sync(location{k}, verb);
end

result = run_query(search);
