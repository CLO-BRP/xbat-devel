function [deleted, status, result] = ftl_del(location, verb, clean)

% ftl_del - delete content from ftl database
% ------------------------------------------
%
% deleted = ftl_del(location, verb)
%
% Input:
% ------
%  location - string
%  verb - flag
%
% Output:
% -------
%  deleted - info
%
% See also: ftl, ftl_add, ftl_clean, ftl_sync

% TODO: consider handling of query failures

%--
% handle input
%--

% NOTE: setting this to true implements cleanup of missing files

if nargin < 3
	clean = false;
end

if nargin < 2
	verb = false;
end 

%--
% setup
%--

store = ftl_store;

if clean
	action.ing = 'cleaning'; action.ed = 'cleaned';
else
	action.ing = 'deleting'; action.ed = 'deleted';
end

%--
% check for location
%--

[status, location] = query(store, ['SELECT * FROM location WHERE location = ''', location, '''']);

deleted = struct;

if isempty(location)
	deleted.location = []; deleted.files = []; deleted.line_count = 0; return;
end

%--
% when location is available delete related files and corresponding lines
%--

deleted.location = location;

if verb
	disp(' '); disp(['(', location.location, ')']);
end

deleted.files = get_database_objects_by_column(store, 'file', 'location_id', location.id);

if clean
	for k = numel(deleted.files):-1:1
		% NOTE: we remove existing files from list of files to delete
		
		if exist(fullfile(location.location, deleted.files(k).name), 'file')
			deleted.files(k) = [];
		end
	end 
end

% db_disp; deleted.files, return

if verb
	for k = 1:numel(deleted.files)		
		disp([title_caps(action.ing), ' ', deleted.files(k).name, ' ...']);
	end
end

set = ['(', str_implode(unique([deleted.files.id]), ', ', @int2str), ')'];

% NOTE: here we count the number of lines deleted from the store

[status, result] = query(store, ['SELECT COUNT(id) AS count FROM line WHERE file_id in ', set]); %#ok<ASGLU>

deleted.line_count = result.count;

if verb	
	disp([int2str(deleted.line_count), ' lines ', action.ed, '.']);
end

% NOTE: here we actually delete the objects to be deleted

if clean
	[status, result] = query(store, { ...
		'BEGIN;', ...
		['DELETE FROM line WHERE file_id IN ', set, ';'], ...
		['DELETE FROM file WHERE id IN ', set, ';'], ...
		'COMMIT;' ...
	});
else
	[status, result] = query(store, { ...
		'BEGIN;', ...
		['DELETE FROM line WHERE file_id IN ', set, ';'], ...
		['DELETE FROM file WHERE id IN ', set, ';'], ...
		['DELETE FROM location WHERE id = ', int2str(location.id), ';'], ...
		'COMMIT;' ...
	});
end


