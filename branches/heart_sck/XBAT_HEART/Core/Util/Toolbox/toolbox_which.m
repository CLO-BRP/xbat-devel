function out = toolbox_which(varargin)

% toolbox_which - which in toolboxes
% ----------------------------------
%
% NOTE: this is meant to return which results from the local toolboxes

%--
% note get all which
%--

% NOTE: typically we want to get all versions of something, then select the toolbox one

try
	out = which(varargin{:}, '-all');
catch
	out = which(varargin{:});
end

%--
% filter results considering toolbox root
%--

out = out(strmatch(toolbox_root, out));