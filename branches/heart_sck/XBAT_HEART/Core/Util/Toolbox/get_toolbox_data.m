function [toolbox, file] = get_toolbox_data(name)

% get_toolbox_data - get toolbox data from file
% ---------------------------------------------
% 
% toolbox = get_toolbox_data(name)
%
% Input:
% ------
%  name - toolbox name
%
% Output:
% -------
%  toolbox - toolbox data struct

%--
% get file and root
%--

[file, root] = toolbox_data_file(name); 

[ignore, name, ignore] = fileparts(file); %#ok<NASGU>

%--
% get toolbox data from file
%-

if exist(file, 'file')
	
	d1 = pwd; cd(root); 
	
	try
		toolbox = feval(name);
	catch
		toolbox = struct; nice_catch(lasterror, 'WARNING: Failed to get toolbox data.');
	end
	
	cd(d1);
	
else
	
	toolbox = struct;
	
end

