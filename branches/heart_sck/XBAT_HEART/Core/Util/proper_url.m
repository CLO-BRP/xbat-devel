function value = proper_url(url)

% NOTE: if we fail to get the various parts we are not proper, some of the parts may be empty

try
	[protocol, host, path, ext] = urlparts(url); value = 1;
catch
	value = 0; return;
end

% TODO: check some parts are not empty, perhaps the validity of the protocol

