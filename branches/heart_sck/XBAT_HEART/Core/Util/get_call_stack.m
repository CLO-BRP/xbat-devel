function stack = get_call_stack(up)

if ~nargin
	up = 1;
end

stack = dbstack(up, '-completenames');