function root = tools_root(root)

% tools_root - where we find various external tools
% -------------------------------------------------
%
% root = tools_root(root)
%
% Input:
% ------
%  root - to set
% 
% Output:
% -------
%  root - current

% TODO: update XBAT startup to set tools root, this would help if the code was on a read-only medium

try
	root = fullfile(app_root, 'Tools');
catch
	if nargin
		set_env('tools_root', root);
	else
		root = get_env('tools_root');
	end
end