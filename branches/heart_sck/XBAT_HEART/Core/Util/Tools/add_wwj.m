function add_wwj

%--
% get sample WWJ application as tool
%--

tool = get_tool('worldwind.jar'); 

if isempty(tool) && ~install_wwj
	error('Failed to install WWJ.');
end

%--
% add jar to dynamic classpath
%--

% NOTE: in the case of a 'jar' file, the file is the path

javaaddpath(tool.file);

javaaddpath(fullfile(tool.root, 'jogl.jar'));

if ~nargout
	javaclasspath
end


%-----------------
% INSTALL_WWJ
%-----------------

function result = install_wwj

% NOTE: try to keep up with this development

url = 'http://worldwind.arc.nasa.gov/java/0.5.0/worldwind-release.0.5.0.zip'

result = install_tool('WWJ', url, 'urlread');