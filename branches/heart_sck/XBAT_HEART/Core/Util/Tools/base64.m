function [status, result] = base64(varargin)

% NOTE: this command-line utility only works with files

name = 'Base64'; file = 'base64.exe'; url = 'http://www.fourmilab.ch/webtools/base64/base64.zip';

[status, result] = generic_tool(name, file, url, 'urlwrite', varargin{:});