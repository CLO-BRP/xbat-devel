function tool = get_curl

% get_curl - get curl command-line tool
% -------------------------------------
%
% tool = get_curl
%
% Output:
% -------
%  tool - curl tool

% TODO: currently this is only available for PC, make it available on linux

if ispc
    file = 'curl.exe';
else
	file = 'curl';
end

%--
% get tool
%--

tool = get_tool(file);

%--
% install tool if needed
%--

if isempty(tool)
	url = 'http://xbat.org/downloads/installers/curl-7.19.7-ssl-sspi-zlib-static-bin-w32.zip';
	
	install_tool('cURL', url);

	tool = get_tool(file);
end

% TODO: what is the meaning of the various flags?

% curl -X -i -d -H


%-----------------------
% LATEST_CURL
%-----------------------

% TODO: the code below is not in use, it is simply an example of scraping

function url = latest_curl %#ok<DEFNU>

% TODO: consider platform information and SSL dependency

% TODO: install tool should store indicator of version, so that we may update tools when possible

%--
% get lines from listing
%--

listing = 'ftp://ftp.sunet.se/pub/www/utilities/curl/';

persistent CONTENT;

if isempty(CONTENT) || etime(clock, CONTENT.retrieved) > 3600
	
	lines = file_readlines(urlread(listing));
	
	for k = 1:numel(lines)
		part = str_split(lines{k}); lines{k} = part{end};
	end
	
	CONTENT.lines = lines(strmatch('curl', lines));
	
	CONTENT.retrieved = clock;
	
end

%--
% parse lines to get latest version
%--

lines = CONTENT.lines;

for k = numel(lines):-1:1
	
	% NOTE: we are looking for win32 binaries with SSL
	
	if isempty(findstr(lines{k}, 'win32')) || isempty(findstr(lines{k}, '-nossl.zip'))
		lines(k) = []; continue;
	end
	
	% NOTE: we are not interested in development versions
	
	if ~isempty(findstr(lines{k}, 'devel')) || ~isempty(findstr(lines{k}, 'mingw')) || ~isempty(findstr(lines{k}, '.asc'))
		lines(k) = []; continue;
	end
	
end

ver = zeros(numel(lines), 3);

for k = 1:numel(lines)
	
	% NOTE: the filename parts are separated with hyphens
	
	part = str_split(lines{k}, '-'); 
	
	% NOTE: the version numbers are separated with periods, we also evaluate these number strings
	
	part = iterate(@eval, str_split(part{2}, '.'))'; 
	
	% NOTE: some of the version strings contain no minor revisions
	
	ver(k, 1:numel(part)) = part;
	
end

[ver, ix] = sortrows(ver);

%--
% output full url
%--

url = [listing, lines{ix(end)}];



