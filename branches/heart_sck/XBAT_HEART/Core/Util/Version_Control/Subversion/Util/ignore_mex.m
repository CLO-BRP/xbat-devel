function ignore_mex(root)

if ~nargin
	root = fullfile(app_root, 'Core');
end
	
update = scan_dir(root, @(p)(ternary(string_contains(p, 'private'), p, [])));

iterate(@disp, update);

for k = 1:numel(update)
	svn('propset', 'svn:ignore', '-F ignore_mex.txt', update{k});
end
