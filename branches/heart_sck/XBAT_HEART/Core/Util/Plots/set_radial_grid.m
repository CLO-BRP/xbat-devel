function [handle, r, a, opt] = set_radial_grid(ax, r, a, opt, varargin)

% set_radial_grid - for axes
% --------------------------
%
% handle = set_radial_grid(ax, r, a, opt, 'field', value, ... )
%
%    opt = set_radial_grid
%
% Input:
% ------
%  ax - handle
%  r - radii to draw
%  a - angles to draw
%  opt - options
%
% Output:
% -------
%  handle - array
%  opt - options
%
% NOTE: the angles to draw are half-rays, to get a full 45 'ray' you need 45 and 135
%
% NOTE: field-value pairs allows last-word setting of any number of properties, use responsibly

%--
% handle input and setup
%--

if nargin < 4 || trivial(opt)
	opt.prefix = 'radial-grid:';
	
	opt.center = 0;

	opt.ray_start = 0;
	
	opt.n = 256;
	
	opt.color = [];
	
	opt.set_grid = false;
	
	if ~nargin && nargout
		handle = opt; return;
	end
end

% NOTE: we use parent axes properties to set defaults, then during the drawing

prop = get(ax);

if isempty(opt.color)
	opt.color = prop.XColor;
end

% NOTE: by default we draw grid rays at 45

if nargin < 3 || (ischar(a) && strcmpi(a, 'default'))
	a = 0:45:359;
end

% NOTE: by default we draw 4 rings spanning the radial range

% TODO: make sure this is right for off-center display

R = sqrt(max([(prop.XLim - real(opt.center)).^2, (prop.YLim - imag(opt.center)).^2]));

if nargin < 2 || (ischar(r) && strcmpi(r, 'default'))
	r = linspace(0, R, 5); r(1) = [];
end

% R = max(r);

%--
% display grid
%--

delete(find_prefixed_handles(ax, opt.prefix));

handle = [];

% display radial lines

for k = 1:numel(r)
	points = circle(opt.center, r(k), opt.n);
	
	handle(end + 1) = line('parent', ax, 'xdata', real(points), 'ydata', imag(points)); %#ok<AGROW>
	
	set(handle(end), 'tag', ['circle-', int2str(k)]);
end

% display angular rays

scale = 2 * pi / 360;

for k = 1:numel(a)	
	points = opt.center + [opt.ray_start, 2 * R] * exp(scale * 1i * a(k));

	handle(end + 1) = line('parent', ax, 'xdata', real(points), 'ydata', imag(points)); %#ok<AGROW>
	
	set(handle(end), 'tag', ['ray-', int2str(k)]);
end

set(handle, 'color', opt.color, 'linestyle', ':', 'hittest', 'off', varargin{:}); 

uistack(handle, 'top');

% NOTE: make sure we did not move the axes and that we can find handles

set(ax, 'xlim', prop.XLim, 'ylim', prop.YLim);

prefix_handle_tags(handle, opt.prefix);

