function [ax, par] = test_axes

par = fig; ax = axes;

axis equal; set(ax, 'box', 'on');