function test_complex_text

%--
% setup parent axes
%--

ax = test_axes;

%--
% using points on circle, display values with size and color, then display spiral
%--

z = circle(0, 1, 16); value = 1:16; 

map = complex_scatter; map.range = [12, 36];

complex_scatter(ax, z, value, [], 'markerfacecolor', 0.75 * ones(1, 3));

map.prop = 'markerfacecolor'; 

handle = complex_scatter(ax, z, value, map, 'markersize', 8);

uistack(handle, 'top');

w = z .* linspace(1.25, 2, numel(z));

value = strcat('string-', iterate(@int2str, value));


handle = complex_text(ax, 1.2 * z, value, 'fontsize', 14);

scale = 180 / pi;

for k = 1:numel(handle)
	set(handle(k), 'rotation', scale * angle(z(k)));
end

set(ax, 'xlim', [-2, 2], 'ylim', [-2, 2]);