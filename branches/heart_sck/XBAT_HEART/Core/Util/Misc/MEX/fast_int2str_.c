# include <string.h>
# include <stdlib.h>
# include "mex.h"
# include "matrix.h"

# define BUFSIZE 4096

char * fast_int2str (char *str, double in);

char * fast_int2str (char *str, double in) {
	
	// NOTE: handle infinite values and not a number
	
	sprintf (str, "%d  ", (int) in); 
	
	return strchr (str, '\0');
	
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
	
	double *T; 
	
	int m, n, N, ix;
					
	mxArray *C;
		
	char str[BUFSIZE];
	
	char * ptr;
	
	//--
	// get input numbers
	//--
	
	T = mxGetPr(prhs[0]);

	m = mxGetM(prhs[0]); n = mxGetN(prhs[0]);
		
	N = (m * n);
	
	if (N > 100) {
		mexErrMsgTxt("Input array must have less than 100 elements.");
	}
	
	//-----------------------
	// OUTPUT AND COMPUTATION
	//-----------------------
	
	//--
	// scalar input
	//--
	
	ptr = str;
	
	if (N == 1) {
		
		ptr = fast_int2str(ptr, *(T));
		
	} else {

		for (ix = 0; ix < N; ix++) {
			ptr = fast_int2str(ptr, T[ix]);
		}
			
	}
	
	*(ptr - 2) = '\0';
	
	plhs[0] = mxCreateString(&str);
	
}


	