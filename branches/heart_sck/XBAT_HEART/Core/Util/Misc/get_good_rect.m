function [n, R, AR] = get_good_rect(N, ar, tol)

% TODO: this should be used by 'figs_tile' as well

%--
% handle input
%--

if nargin < 3
	tol = 0.75;
end

if nargin < 2
	ar = get_screen_aspect_ratio;
end

if ~nargin
	N = 128; 
end

%--
% compute rectangles of min distortion for range
%--

n = 1:N; R = zeros(N, 2); AR = zeros(N, 1); d = zeros(N, 1);

for k = 1:N
	
	[Rk, ix, ARk, d(k)] = rect_factor(k, ar);
	
	if isempty(Rk)
		continue;
	end
	
	R(k, :) = Rk(ix, :); AR(k) = ARk(ix);
	
end

%--
% select good rectangles
%--

select = AR & (d < tol);

n = n(select); R = R(select, :); AR = AR(select);