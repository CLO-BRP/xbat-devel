function handle = big_centered_text(par, string, opt)

% big_centered_text - display big text centered on axes
% -----------------------------------------------------
%
% handle = big_centered_text(par, string, opt)
%
%    opt = big_centered_text
%
% Input:
% ------
%  par - parent axes 
%  string - display string
%  opt - options
%
% Output:
% -------
%  handle - text handle
%  opt - default options

% TODO: develop display with axes and figure parent

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set and possibly output default display options
%--

if nargin < 3
	opt.offset = true; 
	
	opt.color = zeros(1, 3);
	
	opt.highlight = true;
	
	opt.fontsize = 24;
	
	if ~nargin
		handle = opt; return;
	end
end

%--
% check parent input
%--

if ~ishandle(par) || (~strcmp(get(par, 'type'), 'axes') && (nargin > 1))
	error('Parent input for display must be axes handle.');
end

%--
% get handles to big text display
%--

% NOTE: ensure singleton condition for figure

text1 = findobj(ancestor(par, 'figure'), 'tag', 'BIG_CENTERED_TEXT');

if length(text1) > 1
	delete(text1(2:end)); text1 = text1(1);
end

text2 = findobj(ancestor(par, 'figure'), 'tag', 'BIG_CENTERED_HIGHLIGHT');

if length(text2) > 1
	delete(text2(2:end)); text2 = text2(1);
end

% NOTE: return handle for no further input

if nargin < 2
	handle = [text1, text2]; return;
end

%---------------------------
% SET TEXT
%---------------------------

%--
% compute axes center
%--

x = 0.5 * sum(get(par, 'xlim')); y = 0.5 * sum(get(par, 'ylim'));

%--
% set string properties
%--

if isempty(text1)
	text1 = text(x, y, string); 
end

if isempty(text2)
	text2 = text(x, y, string); 
end

if opt.offset
	% TEST CODE: compute highlight text offset in pixels

	pos = get_size_in(par, 'pixels'); xlim = get(par, 'xlim'); ylim = get(par, 'ylim');

	offx = 2 * diff(xlim) / pos(3); offy = 2 * diff(ylim) / pos(4);
else
	offx = 0; offy = 0;
end

set(text2, ...
	'fontsize', opt.fontsize, ...
	'position', [x + offx, y - offy, 0], ...
	'color', 0.9 * [1 1 1], ...
	'tag', 'BIG_CENTERED_HIGHLIGHT' ...
);

if ~opt.highlight
	set(text2, 'visible', 'off');
end

set(text1, ...
	'fontsize', opt.fontsize, ...
	'position', [x, y, 0], ...
	'color', [0 0 0], ...
	'tag', 'BIG_CENTERED_TEXT' ...
);

uistack(text1, 'top');

handle = [text1, text2];

set(handle, ...
	'parent', par, ...
	'string', string, ...	
	'horizontalalignment', 'center' ...
);