function [in, index] = nonempty(in)

% nonempty - elements of array
% ----------------------------
%
% [out, index] = nonempty(in)
%
% Input:
% ------
%  in - array
%
% Output:
% -------
%  out - non-empty elements
%  index - in array

index = 1:numel(in);

if iscell(in)
	
	for k = numel(in):-1:1
		
		if isempty(in{k})
			in(k) = []; index(k) = [];
		end
	end
else
	for k = numel(in):-1:1
		
		if isempty(in(k))
			in(k) = []; index(k) = [];
		end
	end
end