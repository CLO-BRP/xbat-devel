function value = set_env(name, value, verb)

% set_env - set environment variable
% ----------------------------------
%
% value = set_env(name, value, verb)
%
% Input:
% ------
%  name - of variable
%  value - for variable
%  verb - flag (def: false)
%
% Output:
% -------
%  value - set
%
% See also: get_env, persist_env, load_env

% TODO: update the environment variable framework to use application data

%--
% set display verb
%--

if (nargin < 3) || isempty(verb)
	verb = false;
end

%--
% get root userdata
%--

data = get(0, 'userdata');

%--
% check for existing environment variable structure
%--

field = genvarname(name);

if isfield(data, 'env')
	
	if isfield(data.env, field)
		
		data.env.(field) = value;	
	else
		data.env.(field) = value;

		if verb
			disp(['Environment variable ''', name, ''' created.']);
		end
	end
else
	data.env.(field) = value;

	if verb
		disp(['Environment variable ''', name, ''' created.']);
	end
end

%--
% update root userdata
%--

set(0, 'userdata', data);
