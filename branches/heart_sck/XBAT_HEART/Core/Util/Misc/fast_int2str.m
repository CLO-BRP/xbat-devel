function s = fast_int2str(x)

% fast_int2str - a faster int2str function
% ----------------------------------------
%
% s = fast_int2str(x) 
%
% Input:
% ------
%  x - integer array to convert
%
% Output:
% -------
%  s - int2str compatible output string

% NOTE: the MATLAB version is 3 times faster, the MEX is 10 times faster

s = fast_int2str_(x);


