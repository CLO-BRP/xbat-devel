function handles = get_highlighted_text(par, state)

% get_highlighted_text - get handles to highlighted text
% ------------------------------------------------------
%
% [handles, state] = get_highlighted_text(par, state)
%
% Input:
% ------
%  par - text parent
%  state - state desired 'on' or 'off'
%
% Output:
% -------
%  handles - highlighted text handles
%  state - highlight state of found handles 'on' or 'off'

% TODO: finish implementing described functionality

%--
% get text handles within parent
%--

handles = findobj(par, 'type', 'text'); 

if isempty(handles)
	return;
end

%--
% examine callback to determine text if highlighted
%--

callback = get(handles, 'buttondownfcn'); 

% NOTE: we select text handles with non-empty string callbacks to start

value = ~iterate(@isempty, callback) & iterate(@ischar, callback);

handles = handles(value); callback = callback(value);

% NOTE: if 'text_highlight' changes the following test must change as well

value = strmatch('text_highlight', callback);

handles = handles(value);


