function set_menu_check(handles, state)

% set_menu_check - set check state of menus
% -----------------------------------------
%
% set_menu_check(handles, state)
%
% Input:
% ------
%  handles - menu handles
%  state - check state

%--
% check input handles
%--

if any(~ishandle(handles)) || isempty(find(strcmp(get(handles, 'type'), 'uimenu')))
	error('Input must be valid menu handles.');
end

%--
% set menu check state
%--

switch state
	
	case {'on', 1}, set(handles, 'check', 'on');
		
	case {'off', 0}, set(handles, 'check', 'off');
		
	otherwise, error('Unrecognized state input.');
		
end