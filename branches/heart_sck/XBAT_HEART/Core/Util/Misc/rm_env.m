function rm_env(name, verb)

% rm_env - remove environment variable
% ------------------------------------
%
% rm_env(name, verb)
%
% Input:
% ------
%  name - of variable
%  verb - flag (def: 0)

% TODO: update the environment variable framework to use application data

%--
% set display verb
%--

if (nargin < 2) || isempty(verb)
	verb = 0;
end

%--
% get root userdata
%--

data = get(0, 'userdata');

%--
% check for existence of environment variable
%--

% NOTE: we report non-existent environment variable structure

if ~isfield(data, 'env') && verb

	warning('There is no environment variable structure available.'); return; %#ok<WNTAG>
	
end

%--
% check for variable to delete
%--

if ~isfield(data.env, name)
	
	% NOTE: if verbose we report missing variable
	
	if verb
		warning(['Environment variable ''' name ''' does not currently exist.']); %#ok<WNTAG>
	end
	
else
	
	%--
	% remove variable and update root userdata
	%--
	
	data.env = rmfield(data.env, name); set(0, 'userdata', data);
	
	% NOTE: if verbose we report removal of variable
	
	if verb
		warning(['Environment variable ''' name ''' removed.']); %#ok<WNTAG>
	end
	
end
	