function persist_env(file)

% persist_env - to file
% ---------------------
%
% persist_env(file)
%
% Input:
% ------
%  file - name
%
% See also: load_env, set_env, get_env

env = get_env;  %#ok<NASGU>

save(file);