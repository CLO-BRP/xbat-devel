function test_libsvm

load heart_scale.mat;

% Split Data
train_data = heart_scale_inst(1:150,:);
train_label = heart_scale_label(1:150,:);
test_data = heart_scale_inst(151:270,:);
test_label = heart_scale_label(151:270,:);

% Linear Kernel
opt = svm_train;
opt.kernel = 'linear';
model_linear = libSVMTrain(train_data, train_label, opt);
[predict_label_L, info_L] = libSVMPredict(test_data, model_linear);

accuracy_L = sum(predict_label_L == test_label) / size(test_label, 1);
fprintf('Accuracy Linear Kernel SVM %3.2f\n', accuracy_L * 100);

% RBF Kernel
opt = svm_train;
opt.kernel = 'rbf';
model_rbf = libSVMTrain([(1:150)', train_data*train_data'], train_label, opt);
[predict_label_R, info_R] = libSVMPredict([(1:120)', test_data*train_data'], model_rbf);

accuracy_R = sum(predict_label_R == test_label) / size(test_label, 1);
fprintf('Accuracy RBF Kernel SVM %3.2f\n', accuracy_R * 100);


