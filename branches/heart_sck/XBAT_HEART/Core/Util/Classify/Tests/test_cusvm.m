function [passed] = test_cusvm(N, C, kernel, stopcrit)

% test_cusvm - test cuSVM, CUDA enabled SVM routines
% --------------------------------------------------
%
% Input
% -----
% N - parameter for N fold cross validation, default 10
% C - scalar SVM regularization parameter, default 10
% kernel - scalar Gaussian kernel parameter, default 0.05
% stopcrit - stopping criterion, default 0.001
%
% NOTE: Default training parameters are somewhat arbitrary
% NOTE: Currently tests classification only, not regression
%
% Output
% ------
% passed - array of N percent passed = 1 if all passed, 0 if all failed
% alphas, beta, svs - SVM training results
%
% See also: cuSVMTrain, cuSVMPredict


% -------------
% Handle inputs
% -------------

opt = svm_train;

if nargin < 4
    opt.stopcrit = 0.001;
end

if nargin < 3
    opt.kernel = 0.05;
end

if nargin < 2
    opt.C = 10;
end

if ~nargin
    N = 10;
end

% -------------
% Get test data
%--------------
% NOTE: This is an XBAT sample data file
    
all_data = arff_to_table(which('diabetes.arff'));
[row, col] = size(all_data.data);

% Get scaling parameters

all_min = min(single(cell2mat(all_data.data(:, 1:end-1))));
all_max = max(single(cell2mat(all_data.data(:, 1:end-1))));

% Create split index groups

group = ceil(N * randperm(row)' / row);

% -------------------------------------------------
% Loop over N permuations of training and test data
% -------------------------------------------------

for fold = 1:N
    
    % Split data into training and test sets for cross validation
    
    train_idx = find(group ~= fold);
    train_data = all_data.data(train_idx, :);
    test_idx = find(group == fold);
    test_data = all_data.data(test_idx, :);
    
    % Inputs are first n-1 columns
    
    train_X = single(cell2mat(train_data(:, 1:end-1)));
    for row = 1:size(train_X,1)
        train_X(row,:) = (train_X(row,:) - all_min) / (all_max - all_min);
    end
    
    test_X = single(cell2mat(test_data(:, 1:end-1)));
    for row = 1:size(test_X,1)
        test_X(row,:) = (test_X(row,:) - all_min) / (all_max - all_min);
    end
    
    % Outputs are last column, convert strings to +/- 1
    
    train_y = single(strcmp('tested_positive', train_data(:, end)));
    train_y(train_y == 0) = -1;
    
    test_y = single(strcmp('tested_positive', test_data(:, end)));
    test_y(test_y == 0) = -1;
    
    % Train on test data

    [model, info] = cuSVMTrain(train_X, train_y, opt);
    
    model.cusvm.kernel = opt.kernel; % Needed for cuSVMPredict
        
    % Get predictions
    
    [test_yp, info] = cuSVMPredict(test_X, model);
    
    % Find percentage of correct predictions
    
    passed(fold) = sum(test_y == test_yp)/size(test_y, 1);
    
end

