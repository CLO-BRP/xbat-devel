function [model, info] = libSVMTrain(data, value, opt)

% libSVMTrain - Matlab interface to LIBSVM train routine
% ------------------------------------------------------
%
% [model, info] = libSVMTrain(label, data, opt)
%
% Inputs:
% data - matrix of training data corresponding to y
% value - vector of training outputs. If you are classifying,
%     these must be either 1 or -1.
% opt - options to LIBSVM
% line arguments, see libsvm documentation
%
% Outputs:
% model.libsvm.model - LIBSVM format model
%
% See also: svm_train

%--
% handle input
%--

if ~isfield(opt, 'kernel')
	
    switch opt.kernel
        case 'linear'
			argstring = '-t 0';
			
        case 'polynomial'
			argstring = '-t 1';
			
        case 'rbf'
			argstring = '-t 2';
			
        case 'sigmoid'
			argstring = '-t 3';
			
		otherwise
			error(['Unrecognized kernel ''', opt.kernel, '''.']);
    end
else
	
    argstring = '-t 2'; % NOTE: this default is RBF
end

info = struct;

%--
% call MEX helper to train SVM
%--

[model.libsvm.model] = libSVMTrain_mex(value, data, argstring);

% NOTE: we save the corresponding 'predict' function for this trainer

model.predict = @libSVMPredict;



