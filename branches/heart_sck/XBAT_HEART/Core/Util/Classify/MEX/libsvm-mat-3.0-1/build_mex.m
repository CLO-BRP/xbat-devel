% Clear functions

clear libSVMPredict_mex;
clear libSVMTrain_mex;

% Invoke make script provided with LIBSVM Matlab interface 

make;

% Move built mex files to proper private directory and rename so they do
% not collide with svm functions in Matlab bioinformatics toolbox

movefile(['./svmpredict.', mexext], ['../../private/libSVMPredict_mex.', mexext]);

movefile(['./svmtrain.', mexext], ['../../private/libSVMTrain_mex.', mexext]);

