function result = cross_validate(fun, data, value, k)

% cross_validate - classifier on data
% -----------------------------------
%
% result = cross_validate(fun, data, value, k)
%
% Input:
% ------
%  fun - classifier
%  data - to use
%  value - for data
%  k - folds
%
% Output:
% -------
%  results - of validation

%--
% handle input
%--

if nargin < 4
	k = 10;
end

%--
% perform cross-validation
%--

fold = get_fold_labels(size(data, 1), k);

for j = unique(fold)
	%--
	% separate data into training and validation
	%--
	
	selected = fold ~= j;
	
	train.data = data(selected, :); train.value = value(selected);
	
	validate.data = data(~selected, :); validate.value = value(~selected);
	
	%--
	% train
	%--
	
	switch class(fun.train)
		
		case 'function_handle'
			[model, train.info] = fun.train(train.data, train.label);
			
		case 'cell'
			if ~isa(fun.train{1}, 'function_handle')
				error('Train helper input is not a proper callback.');
			end
			
			[model, train.info] = fun.train{1}(train.data, train.label, fun.train{2:end});
	end

	%--
	% predict and test
	%--
	
	switch class(fun.predict)
		
		case 'function_handle'
			[value, predict.info] = fun.predict(validate.data, model);
			
		case 'cell'
			if ~isa(fun.predict{1}, 'function_handle')
				error('Predict helper input is not a proper callback.');
			end
			
			[value, predict.info] = fun.predict{1}(validate.data, model, fun.predict{2:end});
	end

	switch class(fun.loss)
		
		case 'function_handle'
			loss = fun.loss(value, validate.value);
			
		case 'cell'
			if ~isa(fun.loss{1}, 'function_handle')
				error('Loss helper input is not a proper callback.');
			end
			
			loss = fun.loss{1}(value, validate.value, fun.loss{2:end});
	end
	
	%--
	% pack results
	%--
	
	% NOTE: model and info are struct valued, this is part of the API
	
	result.train.model(j) = model; result.train.info(j) = train.info;
	
	result.predict.value{j} = value; result.predict.info(j) = predict.info;
	
	result.loss.value{j} = loss;
end

result.fold = fold;

% TODO: compute a simple summary of the loss

%--------------------------
% GET_FOLD_LABELS
%--------------------------

function label = get_fold_labels(N, k)

% TODO: make more deterministic in the case of small N?

label = ceil(k * rand(N, 1));

