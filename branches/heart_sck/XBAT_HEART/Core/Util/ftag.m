function out = ftag(fun, str)

% NOTE: this is an exercise 

%--
% get store location
%--

store = ftag_store;

if ~nargin
	out = store; return; 
end 

%--
% get function location
%--

% TODO: extend to function handles

location = which(fun);

if isempty(location)
	out = location; return; 
end

%--
% get or set tags
%--

switch nargin
	
	%--
	% get tags
	%--
	
	case 1
		
		file = get_database_objects_by_column(store, 'file', 'location', location);
		
		if isempty(file)
			return;
		end
		
		% TODO: consider establishing views and getting from view
	
	%--
	% set tags
	%--
	
	case 2
		
		file.name = fun; file.location = location; 
		
		if ischar(str)
			str = str_split(str);
		end
		
		for k = 1:numel(str) 
			tag(k).name = str{k};
		end
		
		file = set_database_objects(store, file); tag = set_database_objects(store, tag);
		
		insert_relation(store, 'file', file.id, 'tag', [tag.id]);

end


%------------------
% FTAG_STORE
%------------------

function store = ftag_store

%--
% get store file
%--

store = fullfile(fileparts(mfilename('fullpath')), 'ftag.sqlite3');

if exist(store, 'file')
	return;
end

%--
% create file table and establish file tags
%--

hint = column_type_hints; obj.id = hint.i; obj.name = hint.s; obj.location = hint.s;

sqlite(store, create_table(obj, 'file')); 

opt = establish_tags; opt.user_id = 0;

establish_tags(store, 'file', opt);


