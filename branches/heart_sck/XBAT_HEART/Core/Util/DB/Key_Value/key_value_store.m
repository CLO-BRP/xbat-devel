function [value, result, status] = key_value_store(store, key, value)

% key_value_store - database backed key value store
% -------------------------------------------------
%
% [value, result, status] = key_value_store(store, key, value)
%
% Input:
% ------
%  store - description
%  key - value
%  value - to store
% 
% Output:
% -------
%  value - stored
%  result - object
%  status - of query

%--
% handle input
%--

if nargin < 1 || isempty(store)
	store = store_file;
end
	
%--
% set key value pair
%--

% NOTE: the key and value hash will allow us to simply query using complex keys and values

if nargin > 1
	key_hash = sha1(key); 
	
	key = atomic_value_string(key);
end

if nargin > 2
	value_hash = sha1(value); 
	
	value = atomic_value_string(value);

	[status, result] = query(store, ...
		['INSERT OR REPLACE INTO store VALUES (' key, ', ', value, ', ''', key_hash, ''', ''', value_hash, ''');'] ...
	);
	
	if ~nargout
		return;
	end
end

%--
% get keyed value from store
%--

if nargin < 2
	[status, result] = query(store, 'SELECT * FROM store;');
else
	[status, result] = query(store, ['SELECT * FROM store WHERE key_hash = ''', key_hash, ''';']);
end

for k = 1:numel(result)
	if ~ischar(result(k).value)
		continue;
	end
	
	result(k).value = atomic_value_recovery(result(k).value);
end
	
if isempty(result)
	value = [];
else
	value = {result.value};
	
	try %#ok<TRYNC>
		value = cell2mat(value);
	end
end


%---------------------
% STORE_FILE
%---------------------

% NOTE: this is the default store

function store = store_file(store)

if ~nargin
	store = fullfile(fileparts(mfilename('fullpath')), 'key_value_store.sqlite');
end

% TODO: refine the schema to keep timestamps

% TODO: add tags

if ~exist(store, 'file')
	query(store, 'CREATE TABLE IF NOT EXISTS store (key STRING UNIQUE, value STRING, key_hash STRING, value_hash STRING);');
end


