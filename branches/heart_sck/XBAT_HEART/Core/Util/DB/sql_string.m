function str = sql_string(lines) 

% sql_string - get nice string from lines
% ---------------------------------------
%
% str = sql_string(lines)
%
% Input:
% ------
%  lines - sql lines
%
% Output:
% -------
%  str - sql string

if ischar(lines)
	str = lines;
else
	str = [lines{:}];
end

% NOTE: we just mess (the key word here) with some spacing to make the string nicer

str = strrep(strrep(strrep(str, '  ', ' '), '( ', '('), ' )', ')');