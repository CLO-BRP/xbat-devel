function note = note_prototype

hint = column_type_hints;

note.id = hint.i; 

note.guid = hint.s;

note.title = hint.s;

note.body = hint.text; 

% NOTE: this contains a hash of the title and body

note.content_hash = hint.s;

note.created_at = hint.ts;