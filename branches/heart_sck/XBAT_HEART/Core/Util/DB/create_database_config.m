function config = create_database_config(varargin)

% create_database_config - used by various database access functions
% ------------------------------------------------------------------
%
% config = create_database_config(field, value, ... )
%
%        = create_database_config(config)
%
% Input:
% -------
%  varargin - field and value pairs, or compatible struct
%
% Output:
% -------
%  config - struct for database access

%--
% initialize struct
%--

config = struct;

config.adapter = '';

config.database = '';

config.hostname = ''; 

config.port = ''; 

config.username = '';

config.password = '';

config.content_hash = '';

if ~nargin
	return;
end

%--
% set fields from input
%--

% TODO: lets make parse_inputs accept (struct, struct) or (struct, field, value, ...)

if nargin > 1
    % NOTE: the input is a list of strings    
	
    config = parse_inputs(config, varargin{:});
else
    % NOTE: the input is a single struct
    
    % NOTE: adjust the YAML rails default of 'host'
	
    if isfield(config, 'host')
        config.hostname = config.host;
    end
    
    config = struct_update(config, varargin{1});
end

%--
% check fields
%--

% NOTE: we can add any default values and or sanity checks here

if isempty(config.adapter)
	error('Adapter name input is required.');
end

if ~isfield(config, 'database') || isempty(config.database)
    error('A database is required.');
end

%--
% compile adapter specific fields
%--

% NOTE: this section of code was moved from store__parameter__compile

adapter = lower(config.adapter);

switch adapter
	
	% NOTE: rails uses 'sqlite3' in the YAML file
	
	case {'sqlite', 'sqlite3'}
         
        config.query = @sqlite; 

        config.adapter = 'sqlite';

		% NOTE: the meaning of the 'database' field changes here, the input is expected to be a full file
		
        config.file = config.database;

        config.root = fileparts(config.file);
		
		[ignore, config.database, ignore] = fileparts(config.database); %#ok<NASGU,ASGLU>
			
	case 'sqlite-jdbc'
		% TODO: consider refactor, there is some redundancy here
		
		% NOTE: the JDBC connection is peculiar for SQLite, we also use this as a backup
		
		config.adapter = 'sqlite';
		
		config.file = config.database;
		
		[ignore, config.database, ignore] = fileparts(config.database); %#ok<NASGU,ASGLU>
		
		config.root = fileparts(config.file);
		
		config.query = @jdbc;
		
		config.jdbc = ['jdbc:', lower(config.adapter),':', strrep(config.file, filesep, '/')];
		
		info = get_jdbc_driver_info(config.adapter); 

        config.driver = info.driver;
		
	case lower(known_jdbc_adapter)

		if isempty(config.hostname)
			config.hostname = 'localhost';
		end

		if isempty(config.username)
			config.username = 'root';
		end

		if isempty(config.port)
			
			switch adapter
				
				case 'mysql'
					config.port = 3306;
					
				case 'postgresql'
					config.port = 5432;
					
				case 'sqlite'
					config.port = '';
					
				case 'sqlserver'
					config.port = 1433;	
			end	
		end
		
		config.query = @jdbc;

        % Compile the JDBC connection string

		if ~ischar(config.port) 
			config.port = int2str(config.port);
		end
		
		% TODO: make sure JDBC works for SQLite, the connection string is of the form "jdbc:sqlite:C:/work/mydatabase.db"
		
		% NOTE: the above comes from http://www.xerial.org/trac/Xerial/wiki/SQLiteJDBC#HowtoSpecifyDatabaseFiles
		
		if isempty(config.port)
			config.jdbc = ['jdbc:', lower(config.adapter),'://', config.hostname, '/', config.database];
		else
			config.jdbc = ['jdbc:', lower(config.adapter),'://', config.hostname, ':', config.port, '/', config.database];
		end

        % Get the driver name, needed for instantiation of a JDBC connection object

        info = get_jdbc_driver_info(config.adapter); 

        config.driver = info.driver;   
        
	otherwise
		
		error(['Unknown adapter ''', config.adapter, '''.']);	
end
     
%--
% compute configuration content hash
%--

% TODO: consider whether to include the 'jdbc' and 'driver' strings as part of content

config.content_hash = md5(rmfield(config, {'query', 'content_hash'}));


