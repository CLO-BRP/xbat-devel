function obj = join_database_objects(store, obj, join, foreign_key, table)

% join_database_objects - starting from objects
% ---------------------------------------------
%
% obj = join_database_objects(store, obj, join, foreign_key, table)
%
% Input:
% ------
%  store - database
%  obj - to join 
%  join - field
%  foreign_key - for join
%  table - for join
%
% Output:
% -------
%  obj - with joined fields
%
% NOTE: to get hierarchical objects use the 'include' option in the 'get_database_objects' functions
%
% See also: get_database_objects, get_database_objects_by_column

%--
% handle and check input
%--

% NOTE: these are conventional values for keys and tables, singular table names are used

if nargin < 4
	table = join;
end

if nargin < 3
	foreign_key = strcat(table, '_id');
end

% NOTE: these are cheap checks on the input object fields

if any(isfield(obj, join))
	error('Some or all join fields already exist.');
end

if any(~isfield(obj, foreign_key))
	error('Some or all foreign keys are not available.');
end

if numel(join) ~= numel(foreign_key) || numel(foreign_key) ~= numel(table)
	error('Join, foreign key, and table inputs must match.');
end

% NOTE: a check for join table availability would be relatively expensive, we are lazy to fail

%--
% join each field in turn
%--

for k = 1:numel(join)
	
	id = unique([obj.(foreign_key{k})]);
	
	leaf = get_database_objects(store, table{k}, id);
	
	% NOTE: this test and clause uses a conventional primary-key assumption
	
	if numel(leaf) < numel(id)
		disp(['Failed to retrieve some ''', table{k}, ''' join objects']);
		
		id = [leaf.id];
	end

	for j = 1:numel(obj)
		obj(j).(join{k}) = leaf(id == obj(j).(foreign_key{k}));
	end
	
end



