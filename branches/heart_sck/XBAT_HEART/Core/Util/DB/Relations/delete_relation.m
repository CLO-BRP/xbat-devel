function [status, result] = delete_relation(file, varargin)

% delete_relation - delete relations
% ----------------------------------
%
%     [sql, lines] = delete_relation([], table1, id1, table2, id2, opt)
%
% [status, result] = delete_relation(file, table1, id1, table2, id2, opt)
%
% Input:
% ------
%  file - database
%  table1 - table
%  id1 - identifier
%  table2 - table
%  id2 - identifier
%  opt - options
%
% Output:
% -------
%  sql - string
%  lines - of query
%  status - of query
%  result - of query

%---------------------
% HANDLE INPUT
%---------------------

%--
% get options from input or default, possibly output
%--

if is_odd(numel(varargin))
	opt = varargin{end}; varargin(end) = []; 
end

if ~exist('opt', 'var')
	opt = establish_relation;
end 

if ~nargin
	status = opt; return; 
end

%---------------------
% SETUP
%---------------------

%--
% get name of and check for relation
%--

% NOTE: the relation fields are all but the 'value' field

[field, value] = get_field_value(varargin);

related = setdiff(field, 'value');

% NOTE: the relation table name is determined by what we learn from the input as well as the options

relation = relation_table_name(related{:}, opt);

% NOTE: we can't update a relation that does not exist

if ischar(file) && ~isempty(file) && ~database_has_table(file, relation) 
	
	error(['Proposed relation to update ''', relation, ''' is not available.']);
	
end

%---------------------
% DELETE
%---------------------

% TODO: add transaction information

% TODO: extend to the self-relation case

%--
% build query
%--

sql{1} = 'BEGIN;'; prefix = ['DELETE FROM ', relation, ' WHERE '];

% NOTE: the first two cases consider single-sided delete operations

if isempty(value{1})
	
	sql{2} = [prefix, field{2}, '_id IN (', str_implode(value{2}, ', ', @int2str), ');'];
	
elseif isempty(value{2})
	
	sql{2} = [prefix, field{1}, '_id IN (', str_implode(value{1}, ', ', @int2str), ');'];

% NOTE: this case considers the more specific relation deletion

else
	
	n1 = numel(value{1}); n2 = numel(value{2});
	
	if n1 == n2

		for k = 1:n1
			sql{end + 1} = [prefix, field{1}, '_id = ', int2str(value{1}(k)), ' AND ', field{2}, '_id = ', int2str(value{2}(k)), ';'];
		end

	elseif n1 == 1

		value1 = int2str(value{1});
		
		for k = 1:n2
			sql{end + 1} = [prefix, field{1}, '_id = ', value1, ' AND ', field{2}, '_id = ', int2str(value{2}(k)), ';'];
		end

	elseif n2 == 1

		value2 = int2str(value{2});
		
		for k = 1:n1
			sql{end + 1} = [prefix, field{1}, '_id = ', int2str(value{1}(k)), ' AND ', field{2}, '_id = ', value2, ';'];
		end

	else

		error('Relation identifiers must have matching elements or at most one may be plural.');

	end
	
end

sql{end + 1} = 'COMMIT;';

% NOTE: the query has not yet been executed, no relations deleted

count = 0;

%--
% execute query if possible
%--

if isempty(file)
	
	status = sql_string(sql); result = sql;
	
	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear;
	end
	
else
	
	if ischar(file)
		[status, result] = sqlite(file, sql);
	else
		[status, result] = file.query(file, sql);
	end
	
end


