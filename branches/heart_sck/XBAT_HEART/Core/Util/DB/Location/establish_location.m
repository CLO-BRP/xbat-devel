function [status, result] = establish_location(store)

% establish_location - add tables for location
% --------------------------------------------
%
%     [sql, lines] = establish_location
%
% [status, result] = establish_location(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  sql - string
%  lines - of query
%  status - of query request
%  result - of query

%--
% describe tables
%--

hint = column_type_hints;

% location

location.id = hint.integer;

location.guid = hint.string;

location.locatable_id = hint.real; 

location.locatable_type = hint.string;

location.lat = hint.real; 

location.lng = hint.real;

location.alt = hint.real;

% TODO: consider something about the source of the data or its accuracy

location.created_at = hint.timestamp;

location.modified_at = hint.timestamp; 

%--
% build query to create tag and location tables
%--

sql = create_table(location);

%--
% output query
%--

if ~nargin
	store = []; 
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql); 
end 