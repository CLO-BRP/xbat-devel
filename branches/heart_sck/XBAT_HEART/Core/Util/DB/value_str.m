function str = value_str(obj, fields, opt)

% value_str - create string representation of a struct
% ----------------------------------------------------
%
% str = value_str(obj, fields)
%
% Input:
% ------
%  obj - struct
%  fields - field names to process (def: all)
%
% Output:
% -------
%  str - string

%--
% set default options, return if requested
%--

if nargin < 3
	opt.escape = 1;
end

if ~nargin
	str = opt; return;
end

%--
% get object fieldnames
%--

if nargin < 2
	fields = fieldnames(obj);
end

%--
% handle multiple inputs using iterator
%--

if numel(obj) > 1
	str = iterate(mfilename, obj, fields); return;
end

%--
% create insert statement for each object
%--

% TODO: some analysis of input fields if available

if ~opt.escape
	
	% NOTE: if we do not pass field names to 'atomic_value_string' it will not escape
	
	for k = 1:length(fields)
		str{k} = atomic_value_string(obj.(fields{k}));
	end
	
else
	
	if iscellstr(opt.escape)
		
		% NOTE: in this calling form the default is NO ESCAPE, unless requested
		
		for k = 1:length(fields)
			
			if string_is_member(fields{k}, opt.escape)
				str{k} = atomic_value_string(obj.(fields{k}), fields{k});
			else
				str{k} = atomic_value_string(obj.(fields{k}));
			end
			
		end
		
	else
		
		% NOTE: this is the previous default behavior of this function, default is ESCAPE
		
		% NOTE: fields are not escaped if they are in 'no_escape_field', not the best approach
		
		for k = 1:length(fields)
			str{k} = atomic_value_string(obj.(fields{k}), fields{k});
		end
		
	end
	
end

str = ['(', str_implode(str, ', '), ')']; 


%--------------------------------
% (OLD) ATOMIC_VALUE_STRING
%--------------------------------

% NOTE: this is the old version of this function, and the function has been factored

function part = old_atomic_value_string(value) %#ok<DEFNU>

%--
% handle empty values
%--

% NOTE: this is a generic consideration, we can do better

if isempty(value) && ~ischar(value)

	part = 'NULL';

%--
% handle numeric values
%--

% NOTE: we are only handling scalar numeric fields here

elseif isnumeric(value)

	if value(1) == floor(value(1))
		part = int_to_str(value(1));
	else
		part = num2str(value(1));
	end

%--
% handle strings
%--

% single strings

elseif ischar(value)

	% TODO: implement escaping of strings
	
	part = ['''', value, ''''];

% string cell arrays

elseif iscellstr(value)

	% NOTE: this operation is now offered by 'str_implode'
	
	if length(value) > 1
		part = strcat(value, ';');
	else
		part = value;
	end

	part = ['''', part{:}, ''''];

end