function result = query_returns_result(sql)

% query_returns_result - infers whether statement returns result
% --------------------------------------------------------------
%
% result = query_returns_result(sql)
%
% Input:
% ------
%  sql - string
%
% Output:
% -------
%  result - boolean

% TODO: must account for VIEW as SELECT?

% NOTE: might be nice to have a manual override which allows a query to be specified as 'execute_update'

% NOTE: we may be missing some patterns here

pattern = {'CREATE VIEW\W', 'REPLACE VIEW\W', 'UPDATE\W'};

for k = 1:numel(pattern)
	executable = ~isempty(regexpi(sql, pattern{k})); 
	
	if executable, break; end
end

if executable
	result = false; return;
end

pattern = {'SELECT\W', '^PRAGMA\W', '^SHOW\W'};

for k = 1:numel(pattern)
	result = ~isempty(regexpi(sql, pattern{k})); if result, break; end
end


