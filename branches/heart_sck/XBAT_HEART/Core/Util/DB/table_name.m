function str = table_name(str)

% table_name - generate proper database table name from string
% ------------------------------------------------------------

str = strrep(str, ' ', '_');

str = strrep(str, '-', '_');

str = lower(str);