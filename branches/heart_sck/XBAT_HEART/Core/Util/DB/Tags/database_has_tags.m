function value = database_has_tags(store, table)

% database_has_tags - check for 'tag' and possibly tag relation table
% -------------------------------------------------------------------
%
% value = database_has_tags(store, table)
%
% Input:
% ------
%  store - database
%  table - table
%
% Output:
% -------
%  value - indicator
%
% See also: get_database_tags, rename_database_tags

%--
% check for general tags table
%--

value = has_table(store, 'tag'); 
	
if nargin < 2 || ~value
	return;
end 

%--
% check for tags relation table for table
%--

value = has_table(store, tag_relation_table(table));
