function [tags, id] = get_table_tags(file, table, id)

% get_table_tags - get tags for specified table items
% ---------------------------------------------------
%
% tags = get_table_tags(file, table, id)
%
% Input:
% ------
%  file - file
%  table - table
%  id - identifier array
%
% Output:
% -------
%  tags - cell of cells containing tags
%  id - matching identifiers

% TODO: refactor to use 'get_from_view'

%--
% handle input
%--

% NOTE: this is too expensive, let the SQLite MEX throw an error if the table is not found

% if ~has_table(file, [table, '_tag'])
% 	error(['Table ''', table, ''' is not currently taggable.']);
% end

if nargin < 3
	id = [];
end

%--
% build query to get tags
%--

% TODO: we want to build a query that will pull the tags in string representation

% NOTE: we use the 'o' alias to suggest object

% TODO: handle 'tags' table in a special way

sql = [ ...
	'SELECT o.id AS id, t.name AS tag ', ...
	'FROM ', table, ' o ', ... 
	'JOIN ', table, '_tag ot ON o.id = ot.', table, '_id ', ...
	'JOIN tag t ON ot.tag_id = t.id ' ...
];

% TODO: this little bit of code should perhaps be factored

switch numel(id)
	
	case 0
		sql = [sql, ';'];
		
	case 1
		sql = [sql, 'WHERE o.id = ', int2str(id), ';'];
		
	otherwise
		sql = [sql, 'WHERE o.id IN (', str_implode(id, ', ', @int2str), ');'];
		
end

if isempty(id)
	sql = [sql(1:end - 1), ' ORDER BY o.id;'];
end

%--
% get tags from database
%--

[status, result] = sqlite(file, sql);

if trivial(result)
	tags = repmat({{}}, size(id)); return;	
end

%--
% get tag array, and possibly build an identifier array
%--

% NOTE: we know there are no empty elements in this array, thus indexes match

tag = {result.tag};

if isempty(id)
	id = unique([result.id]);
end

%--
% collect tags
%--

% TODO: consider implementing a MEX collect operation

% NOTE: we can use a concatenation aggregate function, however tags come out as strings

for k = 1:numel(id)
	tags{k} = tag([result.id] == id(k));
end

%--
% display output if not captured
%--

if ~nargout
	
	disp(' ');
	
	for k = 1:numel(id)
		disp([int2str(id(k)), ':  ', str_implode(tags{k}, ', ')]);
	end

	disp(' ');
	
	clear tags id;
	
end




