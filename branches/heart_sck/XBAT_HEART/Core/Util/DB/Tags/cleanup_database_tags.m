function cleanup_database_tags(store)

% cleanup_database_tags - remove duplicates and update taggings
% -------------------------------------------------------------
%
% cleanup_database_tags(store)
%
% Input:
% ------
%  store - database
%
% See also: rename_database_tags

% TODO: eventually we also want to remove unused tags

%--
% get current tags and collect to known tags
%--

[current, id] = get_database_tags(store);

[known, ixc, ixk] = unique(current)

%--
% update taggings and delete current duplicates
%--

% remove = 
% 
% for k = 1:numel(remove)
% 	query(store, ['UPDATE tagging SET tag_id = ', int2str(), ' WHERE tag_id = ']);
% end
% 
% query(store, ['DELETE FROM tag WHERE tag_id IN ()'];



