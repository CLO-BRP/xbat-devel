function pal = database_tags_editor(file, par)

% database_tags_editor - graphical interface for tag property editing
% -------------------------------------------------------------------
%
% pal = database_tags_editor(file, par)
%
% Input:
% ------
%  file - file
%  par - parent
%
% Output:
% -------
%  pal - palette

% TODO: we should have a way of exporting and importing tags

% TODO: we should have a way of merging tags from different places

% TODO: we should indicate in the editor whether we are working with a log, user, or library tag collection

%-----------------------
% HANDLE INPUT
%-----------------------

if nargin < 2
	par = 0;
end

if ~database_has_tags(file)
	error('Database does not have tags.');
end 

%-----------------------
% CREATE CONTROLS
%-----------------------

control = empty(control_create);

control(end + 1) = header_control('Tags', 0);

control(end + 1) = control_create( ...
	'name', 'search', ...
	'onload', 1, ...
	'style', 'edit', ...
	'space', 0.5, ...
	'string', '' ...
);

% NOTE: this control filters the tag list using tag tags

string = get_tag_tags(file); string = {'All Tags', string{:}}; value = 1;

control(end + 1) = control_create( ...
	'name', 'filter', ...
	'style', 'popup', ...
	'space', 1.25, ...
	'string', string, ...
	'value', value ...
);

string = {'By Alpha', 'By Frequency'}; value = 1;

control(end + 1) = control_create( ...
	'name', 'order', ...
	'label', 0, ...
	'color', get(0, 'defaultuicontrolbackgroundcolor'), ...
	'style', 'popup', ...
	'width', 0.4, ...
	'align', 'right', ...
	'space', -0.5, ...
	'string', string, ...
	'value', value ...
);

% NOTE: this controls displays the current list of tags

string = get_database_tags(file); value = 1;

control(end + 1) = control_create( ... 
	'name', 'tags', ...
	'style', 'listbox', ...
	'string',string, ...
	'value', value, ...
	'min', 0, ...
	'max', 2, ...
	'lines', 5 ...
);

control(end + 1) = control_create( ...
	'name', 'name', ...
	'alias', 'tag', ...
	'style', 'edit', ...
	'space', 0.75, ...
	'string', '' ...
);

control(end + 1) = control_create( ...
	'name', {'add', 'rename', 'delete'}, ...
	'style', 'buttongroup', ...
	'lines', 1.75, ...
	'align', 'right' ...
);

%-----------------------
% RENDER CONTROLS
%-----------------------

opt = control_group; opt.width = 15; 

% NOTE: this is a hack to store some additional information in the palette state, formalize it

opt.file = file;

% TODO: consider adding file name to title

pal = control_group(par, @tags_editor_callback, 'Edit Tags ...', control, opt);

%-----------------------
% ADD MENUS
%-----------------------

top = uimenu(pal, 'label', 'File'); handle = top;

handle(end + 1) = uimenu(top, 'label', '(Tags)');

handle(end + 1) = uimenu(top, 'label', 'Export ...', 'tag', 'export');

handle(end + 1) = uimenu(top, 'label', 'Import ...', 'tag', 'import');

parenthetical_menus(handle);

set(handle, 'callback', @tags_editor_callback);

% NOTE: we position palette after creating the menus

position_palette(pal, 0);


%-----------------------
% TAGS_EDITOR_CALLBACK
%-----------------------

function tags_editor_callback(obj, eventdata) %#ok<INUSD>

%--
% get callback context
%--

callback = get_callback_context(obj, eventdata); pal = callback.pal;

% flatten(callback)

%--
% get file to operate on from palette store
%--

% NOTE: this is a hack we are looking to formalize

opt = get_palette_property(pal.handle, 'opt'); file = opt.file;

%--
% get search and filter values
%--

search = get_control(pal.handle, 'search', 'value');

filter = get_control(pal.handle, 'filter', 'value'); 

% NOTE: make sure to extract filter from cell if needed, and 'All Tags' is no filter

if iscell(filter), filter = filter{1}; end

if strcmp(filter, 'All Tags'), filter = ''; end

%--
% dispatch callback
%--

switch callback.control.name
		
	case {'search', 'filter'}
		
		update_tags_display(pal, file, search, filter);
		
	case 'tags'
		
		if numel(callback.value) == 1
			value = callback.value{1}; 
		else
			value = ''; 
		end
		
		set_control_value(pal.handle, 'name', value);
				
		% NOTE: the only operation available for multiple selected entries is 'delete'
		
		disable_control(pal.handle, {'add', 'rename'});
		
		enable_control(pal.handle, 'delete');
		
	case 'name'
	
		% NOTE: once we have edited the edit box the other operations become possible
		
		enable_control(pal.handle, {'add', 'rename'});
		
		disable_control(pal.handle, 'delete');
		
	case 'add'
		
		% NOTE: get string from name as tags and append to database
		
		tags = str_to_tags(get_control_value(pal.handle, 'name'));
		
		if isempty(tags)
			return;
		end
		
		append_database_tags(file, tags);
		
		update_tags_display(pal, file, search, filter);
		
	case 'rename'
	
	case 'delete'
	
		% TODO: this operation should check how many objects are currently tagged with this tag before discarding
		
	case 'color'

end


%-----------------------
% UPDATE_TAGS_DISPLAY
%-----------------------

function update_tags_display(pal, file, search, filter) %#ok<INUSD>

%--
% get tags considering search and filter states
%--

% TODO: implement 'tags' filtering in 'get_database_tags'

tags = get_database_tags(file, 'like', search);

%--
% update tags control
%--
			
control = get_control(pal.handle, 'tags');

set_control([], control, 'label', ['Tags (', int2str(numel(tags)), ')']);

% TODO: this 'set' is not currently working, fix it in 'set_control'
		
% set_control(pal.handle, 'tags', 'string', get_database_tags(file));

set(control.handles.obj, 'string', tags, 'value', []);

%--
% update related control states
%--

% NOTE: since no tag is selected, the 'name' control should be empty

set_control(pal.handle, 'name', 'value', '');

disable_control(pal.handle, {'add', 'rename', 'delete'});


