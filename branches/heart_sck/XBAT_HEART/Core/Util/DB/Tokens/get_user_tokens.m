function token = get_user_tokens(store, user, action)

% get_user_tokens - from database
% -------------------------------
%
% token = get_user_tokens(store, user, action)
%
% Input:
% ------
%  store - database
%  user - struct or identifier
%  action - of token (def: '', all user tokens)
%
% Output:
% -------
%  token - array

%--
% handle input
%--

if nargin < 3
	action = ''; 
end

if ~has_table(store, 'token')
	error('Tokens are not available.');
end

if isstruct(user)
	id = user.id;
else
	id = user;
end

%--
% select token(s)
%--

sql = ['SELECT * FROM token WHERE user_id = ', int2str(id)];

if ~trivial(action)
	sql = [sql, ' AND action = ''', action, ''''];
end

[status, token] = query(store, sql); %#ok<ASGLU>

if ~nargout
	disp(token); clear token;
end