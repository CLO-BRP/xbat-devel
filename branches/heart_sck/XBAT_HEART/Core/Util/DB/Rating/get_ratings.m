function [rating, relation] = get_ratings(store, obj, user, table)

% get_ratings - get object ratings from database
% ----------------------------------------------
%
% rating = get_ratings(store, obj, user, table)
%
%   rate = get_ratings(store)
%
% Input:
% ------
%  store - database
%  obj - tagged
%  user - tagger
%  table - name
%
% Output:
% -------
%  rating - values for objects
%  relation - objects

%--
% return all rate values
%--

if nargin < 2
	rating = [1 2 3 4 5];%get_database_objects_by_column(store, 'rate'); 
    return;
end

%--
% typically reflect table from input
%--

if nargin < 4
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%--
% get object identifiers
%--

% NOTE: we accept identified object array or identifier array input

if isstruct(obj)
	id = get_id_field(obj);
else
	id = obj;
end

%--
% return rating for objects
%--

% NOTE: we select the full rating relation with score attached, these are later separated into the two outputs of this function

sql = { ...
	'SELECT rating.*',... % rating.rate_id AS rate_id,', ...
	%' rating.rateable_type AS rateable_type, rating.rateable_id AS rateable_id,' ...
	%' rating.user_id AS user_id, rating.created_at AS created_at,', ...
	%' rate.score AS score ', ...
	' FROM rating', ...
	%' JOIN rate ON rating.rate_id = rate.id', ...
	[' WHERE rating.rateable_type = ''', table, ''' AND'] ...
};

% NOTE: user input means we only retrieve rating for that user 

if nargin > 2 && ~trivial(user)
	sql{end + 1} = [' rating.user_id = ', int2str(user.id), ' AND'];
end

if numel(id) > 1
	sql{end + 1} = [' rating.rateable_id IN (', str_implode(id, ', ', @int2str), ');'];
else
	sql{end + 1} = [' rating.rateable_id = ', int2str(id) ';'];
end

% NOTE: return the query if we have no database to query

if trivial(store)
	
	rating = sql;

	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear rating;
	end
	
	return;
	
end

[status, relation] = query(store, sql_string(sql));

%--
% extract values and pack results
%--

% NOTE: we filter result using the object identifier, and use comma-separated list to pack rating in cell array

rateable_id = [relation.rateable_id];

rating = cell(size(obj));

for k = 1:numel(id)
	
	selected = relation(rateable_id == id(k));
	
	if ~isempty(selected)
		rating{k} = selected.rating;
	end
	
end

% NOTE: this makes sure that the relation we get as output looks like a relation

% relation = rmfield(relation, 'score');




