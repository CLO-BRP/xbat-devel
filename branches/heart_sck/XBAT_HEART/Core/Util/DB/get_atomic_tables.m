function table = get_atomic_tables(store)

% get_atomic_tables - non-relation table names
% --------------------------------------------
%
% table = get_atomic_tables(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  table - names

% TODO: this selection is based on convention, perhaps convention should be a function 

% NOTE: currently this convention is insufficient

table = get_tables(store);

for k = numel(table):-1:1
	if findstr(table{k}, '_'), table(k) = []; end
end