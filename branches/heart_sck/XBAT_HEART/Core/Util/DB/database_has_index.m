function [value, indices] = database_has_index(file, index)

% database_has_index - check for index existence
% ----------------------------------------------
% 
% [value, indices] = database_has_index(file, index)
%
% Input:
% ------
%  file - database
%  index - index 
%
% Output:
% -------
%  value - indicator
%  indices - all indices

% TODO: extend to other database engines, call the engine method from here

indices = get_database_indices(file);

value = string_is_member(index, indices);