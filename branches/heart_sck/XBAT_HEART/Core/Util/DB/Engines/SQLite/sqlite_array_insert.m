function query = sqlite_array_insert(table, content, fields, opt)

% sqlite_array_insert - create query to insert struct array 
% -------------------------------------------------------
%
% query = sqlite_array_insert(table, content, fields, opt)
%
% Input:
% ------
%  table - name
%  content - struct array
%  fields  - to insert
%  opt - options, conflict resolution and transaction configuration 
%
% Output:
% -------
%  query - represented as string cell array

%----------------------
% HANDLE INPUT
%----------------------

%--
% set default options and possibly output
%--

% NOTE: the 'REPLACE' resolution is the most aggressive resolver

% NOTE: the empty transaction default is equivalent to 'DEFERRED'

if nargin < 4
	opt.resolve = 'REPLACE'; opt.transaction = ''; opt.escape = 1; opt.update = 0;
end 

if ~nargin
	query = opt; return;
end

%--
% check options
%--

if ~is_sqlite_resolver(opt.resolve)
	error(['Unrecognized conflict resolution algorithm ''', opt.resolve, '''.']);
end

if ~is_sqlite_transaction(opt.transaction)
	error(['Unrecognized transaction mode ''', opt.transaction, '''.']);
end

%--
% set all fields default
%--

if nargin < 3
	fields = {};
end

% TODO: we should check the fields are available

%----------------------
% BUILD QUERY
%----------------------

%--
% start query and declare transaction type
%--

query = cell(length(content) + 2, 1);

if isempty(opt.transaction)
	query{1} = 'BEGIN;';
else
	query{1} = ['BEGIN ', opt.transaction, ';'];
end
	
%--
% add content elements
%--

if opt.update
	
	fields = setdiff(fields, 'id');
	
	% TODO: include resolution in statement
	
	prefix = ['UPDATE ', table, ' SET '];
	
	for k = 1:numel(content)
		
		for j = 1:numel(fields)
			part{j} = [fields{j}, ' = ', atomic_value_string(content(k).(fields{j}))];
		end
		
		query{k + 1} = [prefix, str_implode(part, ', '), ' WHERE id = ', int2str(content(k).id), ';'];
	end
else
	% NOTE: we build the shared prefix once and iterate over the content

	% NOTE: we declare the conflict resolver in the prefix, possibly using the 'REPLACE' MySQL compatibility alias

	if isempty(opt.resolve)
		
		prefix = 'INSERT';
	else
		if strcmpi(opt.resolve, 'replace')
			prefix = 'REPLACE';
		else
			prefix = ['INSERT OR ', opt.resolve];
		end
	end

	% NOTE: the prefix includes a column list in the case of field selection

	if isempty(fields)
		prefix = [prefix, ' INTO ', table, ' VALUES ']; fields = fieldnames(content);
	else
		prefix = [prefix, ' INTO ', table, ' (', str_implode(fields, ', '), ') VALUES '];
	end

	% NOTE: we always provide fields to the value string helper

	for k = 1:numel(content)
		query{k + 1} = [prefix, value_str(content(k), fields, opt), ';'];
	end
end

%--
% commit transaction
%--

query{end} = 'COMMIT;';

%--
% display query
%--

if ~nargout
	disp(' '); sql_display(query); disp(' '); clear query;
end 

