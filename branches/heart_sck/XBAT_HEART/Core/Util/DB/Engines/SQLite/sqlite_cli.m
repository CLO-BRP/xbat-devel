function [status, result] = sqlite_cli(file, command)

% sqlite_cli - use SQLite command-line application sqlite3
% --------------------------------------------------------
%
% [status, result] = sqlite_cli(file, command)
%
% Input:
% ------
%  file - file
%  command - command
%
% Output:
% -------
%  status - status
%  result - result

%--
% get tool
%--

tool = get_tool('sqlite3.exe');

if nargin && isempty(tool)
	error('Unable to find tool.');
end

% NOTE: return tool if we are called with no input

if ~nargin
	status = tool; result = []; return;
end

%--
% use tool
%--

str = ['"', tool.file, '" "', file, '"'];

if (nargin > 1) && ~isempty(command)
	str = [str, ' ', command];
end

[status, result] = system(str);

%--
% display result if operation succeeded and output was not captured
%--

if ~nargout && ~status
	clear status; disp(' '); iterate(@disp, file_readlines(result)); disp(' ');
end


