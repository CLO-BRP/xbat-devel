function [available, jars] = has_jdbc_driver(adapter)

% has_jdbc_driver - test for adapter specific jdbc drivers
% --------------------------------------------------------
%
% [available, jars] = has_jdbc_driver(adapter)
%
% Output:
% -------
%  available - indicator
%  jars - found

%--
% handle input
%--

in = adapter; adapter = known_jdbc_adapter(adapter);

if isempty(adapter) 
	error(['Unknown JDBC adapter ''', in, '''.']);
end

%--
% setup and check persistent cache
%--

persistent JDBC;

if isempty(JDBC)
	JDBC = struct;
end 

if isfield(JDBC, adapter)
	available = JDBC.(adapter).available; jars = JDBC.(adapter).jars; return;
end

% NOTE: available means jar files in the right place and added to the classpath

%--
% check for jars in JDBC adapter driver directory
%--

jars = get_jars(jdbc_driver_root(adapter));

if isempty(jars)
	available = 0; return;
end

%--
% check classpath
%--

append_classpath(jars);

% NOTE: we persist the answer only when we've found what we were looking for

JDBC.(adapter).available = 1; JDBC.(adapter).jars = jars;


