function [element, info] = jdbc_get_elements(store, type)

% jdbc_get_elements - JDBC adapter for 'get_database_elements'
% ------------------------------------------------------------
%
% [element, info] = jdbc_get_elements(store, type)
%
% Input:
% ------
%  store - database
%  type - of element: 'TABLE', 'VIEW', or 'INDEX'
%
% Output:
% -------
%  element - names
%  info - for elements

%--
% handle input
%--

if ~isstruct(store)
	error('Sorry, store argument must be a struct.');
end

type = upper(type);

%--
% get elements
%--

metadata = get_jdbc_metadata(store);

switch type
	
	case {'TABLE', 'VIEW'}
		resultset = metadata.getTables([],'%', '%', type);
		
	case 'INDEX'
		error('JDBC index access has not been implemented yet!');
		
end

resultset = format_jdbc_resultset(resultset);

element = cell(1, numel(resultset)); % preallocate the cell array

for k = 1:numel(resultset)
	element{k} = resultset(k).TABLE_NAME;
end

%--
% get element info if needed
%--

if nargout < 2
	return;
end

% NOTE: current code makes sense for tables and views, not for indices

% TODO: get other info from table, consider using 'information_schema'

info = repmat(struct('column', {}), size(element)); % this pre-allocation really seems to make a difference!

for k = 1:numel(element)	
		
	info(k).column = get_table_columns(store, element{k});
end

% info = 'JDBC not returning info for this operation (yet)';

%value = string_is_member(table, info);

% NOTES ON DatabaseMetaData.getTables()

% public ResultSet getTables(String catalog,
%                            String schemaPattern,
%                            String tableNamePattern,
%                            String[] types)
%                    throws SQLException

% NOTE: common values for TABLE_TYPE

% "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY",
% "ALIAS", "SYNONYM"



