function adapter = known_jdbc_adapter(adapter)

%--
% get adapter names from info of known drivers
%--

info = get_jdbc_driver_info; adapters = {info.name};

% NOTE: we may return all known names 

if ~nargin
	adapter = adapters; return;
end

%--
% match and normalize adapter name
%--

match = find(iterate(@strcmpi, adapters, adapter));

if isempty(match)
	adapter = '';
else
	adapter = adapters{match(1)};
end
