function installed = install_jdbc_driver(adapter)

% install_jdbc_driver - download driver and add to java classpath
% ---------------------------------------------------------------
%
% installed = install_jdbc_driver(adapter)
%
% Input:
% ------
%  adapter - name
%
% Output:
% -------
%  installed - indicator

%--
% set adapter specific network and download location
%--

info = get_jdbc_driver_info(adapter);

if isempty(info)
	error(['Unknown JDBC adapter ''', adapter, '''.']);
end

% NOTE: this is the name of the downloaded file, use the name from the source

file = get_last_part(info.url);

% NOTE: this is the full path for the downloaded file

% NOTE: the adapter name directory just tries to keep things neater in the drivers directory

file = fullfile(fileparts(mfilename('fullpath')), 'Drivers', info.name, file);

root = create_dir(fileparts(file));

if isempty(root)
	error('Failed to create driver parent directory.');
end

%--
% check for jars in driver directory
%--

% NOTE: we assume that jars in the driver directory are drivers, no need to download

jars = get_jars(root);

if ~isempty(jars)
	append_classpath(jars); installed = 1; return;
end

%--
% download adapter jar and unzip if needed
%--

try
	
	disp(['Downloading and installing JDBC driver for ''', info.name, ''', this may take some time ...']);
	
	start = clock;
	
	%--
	% download using curl
	%--
	
	% TODO: the waitbar is broken in 'curl_get', in a stupid way!
	
% 	curl_get(url, file, ['Downloading ''', adapter, ''' JDBC drivers.']);

	[status, result] = curl_simple(info.url, ['-o "', file, '"']);	 %#ok<NASGU>

	%--
	% unzip if needed and add resulting tree to path
	%--
	
	% NOTE: 'fileparts' gives you the path part of the file, we unpack where we downloaded
	
	root = fileparts(file);
	
	if strcmp(file(end - 3:end), '.zip')	
		unzip(file, root); delete(file);
	end
	
	append_path(root);
	
	elapsed = etime(clock, start);
	
	disp(['Done. (', num2str(elapsed), ' sec)']);
	disp(' ');
	
catch
	
	nice_catch(lasterror); installed = 0; return;
	
end

%--
% append new jars to dynamic and static java classpath as needed
%--

% TODO: display which jars were found and which were added to the classpath

jars = get_jars(root);

if isempty(jars)
	installed = 0;
else
	append_classpath(jars); installed = 1;
end


%-----------------------
% GET_LAST_PART
%-----------------------

function last = get_last_part(str)

ix = find(str == '/', 1, 'last');

if isempty(ix) || ix == numel(str)
	last = '';
else
	last = str(ix + 1:end);
end

