function test_jdbc_has_table( adapter )

if ~nargin
	adapter = 'mysql';
end

info = get_jdbc_driver_info(adapter);

if isempty(info)
	error(['Unknown JDBC adapter ''', adapter, '''.']);
end

config = create_database_config( ... 
    'adapter', adapter, ...
    'database', 'xbat_test', ...
    'hostname', 'localhost', ...
    'username', 'root', ...
    'password', '' ...
);

if isequal(adapter, 'postgresql')
    config.username = 'postgres';
    config.password = 'password';
end

jdbc(config, 'DROP table IF EXISTS users;');

jdbc(config, 'CREATE TABLE users ( id integer NOT NULL AUTO_INCREMENT, name varchar(255) default NULL, bool boolean default false, PRIMARY KEY (id))');

assert_equal(has_table(config, 'users'), 0);

assert_equal(has_table(config, 'event'), 0);



