function [status, result] = jdbc_execute_batch(connection, queries)

% jdbc_execute_batch - of queries in transaction
% ----------------------------------------------
%
% [status, result] = jdbc_execute_batch(store, query)
%
% Input:
% ------
%  store - connection
%  query - sequence
% 
% Output:
% -------
%  status - of transaction
%  result - of transaction

%--
% setup
%--

% NOTE: while 'auto commit' is false the connection can timeout, since autoReconnect will not work

% TODO: write code here to handle the possibilty of a stale connection

connection.setAutoCommit(false);

%--
% create batch statement
%--

statement = get_jdbc_statement(connection);

for j = 1:numel(queries)
    statement.addBatch(queries(j));
end

%--
% execute statement
%--

% NOTE: 'executeBatch' does not support getting generated keys. JDBC allows only getting generated keys from single updates. 

status = statement.executeBatch(); result = get_last_id(statement); 

% connection.commit();

% TODO: FIGURE OUT A WAY TO ROLLBACK ON ERROR!!!!

% NOTE: clean up the connection for non-batched operations

connection.setAutoCommit(true);