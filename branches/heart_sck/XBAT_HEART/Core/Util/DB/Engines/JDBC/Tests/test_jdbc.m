function test_jdbc( adapter )

if ~nargin
	adapter = 'mysql';
end

info = get_jdbc_driver_info(adapter);

if isempty(info)
	error(['Unknown JDBC adapter ''', adapter, '''.']);
end

config = create_database_config( ... 
    'adapter', adapter, ...
    'database', 'xbat_test', ...
    'hostname', 'localhost', ...
    'username', 'root', ...
    'password', '' ...
);

if isequal(adapter, 'postgresql')
    config.username = 'postgres';
    config.password = 'password';
end

options.log = 0;

jdbc(config, 'DROP table IF EXISTS users;', options);

jdbc(config, 'CREATE TABLE users ( id integer NOT NULL AUTO_INCREMENT, name varchar(255) default NULL, bool boolean default false, PRIMARY KEY (id))', options);

%--
% SINGLE STATEMENT QUERY
%--

jdbc(config, 'INSERT into users (name) VALUES (''David'')', options);


jdbc(config, 'INSERT into users (name) VALUES (''Harold'')', options);

[status, result] = jdbc(config, 'SELECT count(*) as count FROM users', options);

[status, result] = jdbc(config, 'INSERT INTO users (name) VALUES (''H''), (''I''), (''J'');', options);

%-----
% BATCH MODE
%-----

jdbc(config, 'INSERT into users (name) VALUES (''Joey''); INSERT into users (name) VALUES (''Joey''); INSERT into users (name) VALUES (''Billy'');INSERT into users (name) VALUES (''Billy'');', options);

[status, result] = jdbc(config, 'SELECT count(*) as count FROM users', options);


