function metadata = get_jdbc_metadata(in)

% get_jdbc_metadata - return metadata
% -------------------------------------
% 
% metadata = get_jdbc_metadata(in)
%
% Input:
% ------
%  in - struct or com.mysql.jdbc.ConnectionImpl
%
% Output:
% -------
%  metadata - com.mysql.jdbc.DatabaseMetaData


% TODO: replace use of this function with 'database_has_table'

if isstruct(in)
    
    connection = jdbc_connection(in);

elseif isa(in, 'com.mysql.jdbc.ConnectionImpl')

    connection = in;

else
    
    error('I need a config struct or a JDBC connection instance to give you metadata');
    
end

%%% SCK change
% metadata = connection.getMetaData();