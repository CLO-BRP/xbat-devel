function [status, result] = delete_taggings_as_relation(store, taggable_id, taggable_type, tag_id, user_id)

%--
% build query
%--

prefix = ['DELETE FROM tagging WHERE taggable_type = ''', taggable_type, ''''];

if nargin > 4
	prefix = [prefix, ' AND user_id = ', int2str(user_id)];
end
	
if nargin < 4 || isempty(tag_id)
	
	%--
	% delete all tags for a set of objects
	%--
	
	if numel(taggable_id) == 1
		sql = [prefix, ' AND taggable_id = ', int2str(taggable_id), ';'];
	else
		sql = [prefix, ' AND taggable_id IN (', str_implode(taggable_id, ', ', @int2str), ');'];
	end
	
else
	
	%--
	% delete specific tags
	%--

	objects = numel(taggable_id); tags = numel(tag_id);

	code = (objects > 1) + 2 * (tags > 1);

	switch code

		case 0 % single object and tag
			
			sql = [prefix, ' AND taggable_id = ', int2str(taggable_id), ' AND tag_id = ', int2str(tag_id), ';'];
			
		case 1 % multiple objects single tag
			
			sql = [prefix, ' AND taggable_id IN (', str_implode(taggable_id, ', ', @int2str), ') AND tag_id = ', int2str(tag_id), ';'];
			
		case 2 % single object multiple tags

			sql = [prefix, ' AND taggable_id = ', int2str(taggable_id), ' AND tag_id IN (', str_implode(tag_id, ', ', @int2str), ');'];
			
		case 3 % multiple matching objects and tags
			
			if objects ~= tags
				error('Number of objects and tags must match or one must be scalar.');
			end
			
			sql = {'BEGIN;'};
			
			for k = 1:objects
				sql{end + 1} = [prefix, ' AND taggable_id = ', int2str(taggable_id(k)), ' AND tag_id = ', int2str(tag_id(k)), ';'];
			end
			
			sql{end + 1} = 'COMMIT;';
			
	end

end

%--
% output or execute query
%--

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql);
end
