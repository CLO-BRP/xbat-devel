function [tags, obj] = get_known_tags(store)

% get_known_tags - in store
% -------------------------
%
% Input:
% ------
%  store - configuration
%
% Output:
% -------
%  tags - known
%  obj - representation of tags

% TODO: consider 'user' input that would select only those tags used by that user

obj = get_taggings(store); tags = {obj.name};