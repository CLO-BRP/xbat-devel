function [status, result] = establish_tagging(store)

% establish_tagging - add tables for tagging
% ------------------------------------------
%
%     [sql, lines] = establish_tagging
%
% [status, result] = establish_tagging(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  sql - string
%  lines - of query
%  status - of query request
%  result - of query

%--
% create tag and tagging prototypes 
%--

hint = column_type_hints;

% TAG

tag.id = hint.i; 

tag.name = hint.s; 

tag.created_at = hint.ts;

% TAGGING

tagging.id = hint.i; 

tagging.tag_id = hint.i; 

tagging.taggable_type = hint.s; 

tagging.taggable_id = hint.i;

tagging.user_id = hint.i;

tagging.created_at = hint.ts;

% tag_set

tag_set.id = hint.integer;

tag_set.guid = hint.string;

tag_set.context_id = hint.integer;

tag_set.context_type = hint.string;

tag_set.prefix = hint.string;

tag_set.tags = hint.text;

tag_set.closed = hint.logical;

tag_set.public = hint.logical;

tag_set.created_at = hint.ts;

%--
% build query to create tag and tagging tables
%--

sql = {'BEGIN;', create_table(tag), create_table(tagging), create_table(tag_set), 'COMMIT;'};

%--
% output query
%--

if ~nargin
	store = []; 
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql); 
end 


