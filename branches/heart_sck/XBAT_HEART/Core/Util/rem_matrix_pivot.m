function A = rem_matrix_pivot(A, k) 

% rem_matrix_pivot - remove matrix row and column from pivot position
% -------------------------------------------------------------------
%
% A = rem_matrix_pivot(A, k) 
%
% Input:
% ------
% Input:
% ------
%  A - matrix to update
%  k - remove position

%--
% handle input
%--

[m, n] = size(A); d = min(m, n);

% NOTE: by default we remove last matrix pivot

if nargin < 2
	k = d;
else
	if (k ~= round(k)) || (k < 1) || (k > d)
		error('Pivot remove position must be an integer between 1 and the starting number of pivots.');
	end
end

%--
% update matrix
%--

A(k, :) = []; A(:, k) = [];