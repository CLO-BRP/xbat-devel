function x = list(X)

% list - convert array to row vector
% ----------------------------------
%
% x = list(X)
%
% Input:
% ------
%  X - input array
%
% Output:
% -------
%  x - row vector listing array elements

% NOTE: this allows easier use of the 'each' behavior of 'for'

x = X(:)';
