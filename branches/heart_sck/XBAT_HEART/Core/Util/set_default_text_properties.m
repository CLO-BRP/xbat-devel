function set_default_text_properties(font)

% set_default_font_properties - for various objects
% -------------------------------------------------
%
% set_default_text_properties(font)
%
% Input:
% ------
%  font - description struct

%--
% figure properties
%--

set(0, 'DefaultTextInterpreter', 'none');

%--
% axes properties
%--

set(0, ...
	'DefaultAxesFontName', font.name, ...
	'DefaultAxesFontSize', font.size ...
);

%--
% text properties
%--

set(0, ...
	'DefaultTextFontName', font.name, ...
	'DefaultTextFontSize', font.size ...
);

%--
% uicontrol properties
%--

set(0, ...
	'DefaultUicontrolFontName', font.name, ...
	'DefaultUicontrolFontSize', font.size ...
);