function [exts, names, types] = extensions_cache(exts)

% extensions_cache - provide and manage a persistent extensions store
% -------------------------------------------------------------------
%
% [exts, names, types] = extensions_cache(exts)
%
% Input:
% ------
%  exts - extensions to update in cache
%
% Output:
% -------
%  exts - current extensions cache
%  names - extension names
%  types - extension types

persistent CACHED NAMES TYPES;

%--
% return cached extensions
%--

if ~nargin
	exts = CACHED; return;
end

%--
% create initial extensions cache
%--

% NOTE: when the cache is empty we simply fill it

if nargin && isempty(CACHED)
	CACHED = exts; NAMES = {exts.name}; TYPES = {exts.subtype}; return;
end

%--
% update extensions cache
%--

% NOTE: when the cache has contents we use an update approach

for k = 1:length(exts)
	
	% NOTE: find cache element matching name and type
	
	ext = exts(k);
	
	ix = find(strcmp(ext.name, NAMES) & strcmp(ext.subtype, TYPES));
	
	% NOTE: we are not considering the duplicate index possibility

	if isempty(ix)	
		CACHED(end + 1) = ext; NAMES{end + 1} = ext.name; TYPES{end + 1} = ext.subtype;	
	else	
		CACHED(ix) = ext;
	end
	
end

%--
% provide cache contents as output
%--

exts = CACHED;

if nargout < 2
	return;
end

names = NAMES;

if nargout < 3
	return;
end 

types = TYPES;

