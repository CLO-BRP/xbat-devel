function [table, prefix] = extension_table_names(ext)

% extension_table_names - get table names for extension stores
% ------------------------------------------------------------
%
% [table, prefix] = extension_table_names(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  table - names struct
%  prefix - in table names
%
% See also: extension_classname, establish_extension_table


% TODO: consider making the convention a parameter, in some cases readability may trump generality

%--
% handle multiple extensions recursively
%--

if numel(ext) > 1
	[table, prefix] = iterate(mfilename, ext); return;
end

%--
% compute table prefix and full table names for values and parameters
%--

% TODO: the 'ext.fieldname' that comes in is not correct for project extensions

name = extension_fieldname(ext);

% prefix = [ext.subtype, '__', name];

% NOTE: candidates for table names that allow for versioning and distributed development

% prefix = [ext.subtype, '__', name, '__', strrep(ext.version, '.', '_')];

% prefix = [ext.subtype, '__', name, '__', strrep(ext.guid, '-', '_')];

prefix = ['ext__', strrep(ext.guid, '-', '_')];

table.value = [prefix, '__value'];

table.parameter = [prefix, '__parameter'];

table.prefix = prefix;

table.name = name;