function root = preset_dir(ext, type)

% preset_dir - compute presets directory for an extension
% -------------------------------------------------------
%
% root = preset_dir(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  root - presets directory

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set and check preset type
%--

if nargin < 2
	types = get_parameter_types(ext); type = types{1}; % type = 'compute';
end

if ~string_is_member(type, get_preset_types(ext))
	error(['Unrecognized preset type ''', type, '''.']);
end

%---------------------------
% COMPUTE PRESET ROOT
%---------------------------

%--
% get root presets directory
%--

% NOTE: get type based parameter create function handle

if strcmpi(type, 'compute')
	create = ext.fun.parameter.create;
else
	create = ext.fun.(type).parameter.create;
end

% NOTE: preset root is based on create function or main function

if ~isempty(create)
	fun = functions(create); root = [path_parts(path_parts(fun.file)), filesep, 'Presets'];
else
	fun = functions(ext.fun.main); root = [path_parts(fun.file), filesep, 'Presets'];
end

%--
% append preset type to root
%--

if ~isempty(type)
	root = [root, filesep, title_caps(type)];
end

%--
% make sure presets directory exists
%--

root = create_dir(root);

if isempty(root)
	error(['Failed to create preset directory for extension ''', ext.name, '''.']);
end

