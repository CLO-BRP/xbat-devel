function count = get_total_extension_count(type)

% get_total_extension_count - quick count of extensions, possibly of a type 
% -------------------------------------------------------------------------
% 
% count = get_total_extension_count(type)
%
% Input:
% ------
%  type - of extensions
%
% Output:
% -------
%  count - of extensions

%--
% set all types default
%--

if ~nargin
	type = ''; 
end 

%--
% get all projects
%--

project = all_projects; 

%--
% count extensions for core and each project
%--

count = zeros(1, 1 + numel(project)); count(1) = numel(extension_roots(type));

for k = 2:numel(count)
	count(k) = numel(extension_roots(type, project(k - 1)));
end

% count, {project.name}'

count = sum(count);