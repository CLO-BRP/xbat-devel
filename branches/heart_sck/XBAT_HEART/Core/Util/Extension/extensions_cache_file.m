function file = extensions_cache_file

% NOTE: this is a system level store, hence the location

file = fullfile(extensions_root, 'cache.mat');