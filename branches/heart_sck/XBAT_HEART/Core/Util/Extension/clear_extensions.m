function clear_extensions

% clear_extensions - release various persistent stores
% ----------------------------------------------------
%
% clear_extensions

% NOTE: this function makes sure that all relevant caches are destroyed

clear get_extension_types;

clear extension_signatures;

clear extensions_cache;
