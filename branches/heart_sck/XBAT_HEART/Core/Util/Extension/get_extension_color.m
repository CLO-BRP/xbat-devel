function color = get_extension_color(ext)

% get_extension_color - get extension palette color
% -------------------------------------------------
%
% color = get_extension_color(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  color - color for various components

% TODO: collect colors for other extension types in this function

% NOTE: look at 'control_colors' as well

%------------------------------------
% HANDLE INPUT
%------------------------------------

%--
% get extension type from input
%--

switch class(ext)

	case 'char', type = ext;

	case 'struct'

		if ~isfield(ext, 'subtype')
			error('Input struct must be extension struct.');
		end

		type = ext.subtype;

end

%--
% check input extension type
%--

if ~isempty(type) && ~is_extension_type(type) && ~strcmp(type, 'root')
	error('Input must be extension or extension type.');
end
	
%------------------------------------
% GET EXTENSION COLOR
%------------------------------------

%--
% declare constant base color
%--

light_gray = [192, 192, 192] / 255;	

%--
% get extension type color
%--

% NOTE: extensions type extensions should declare these, perhaps a parameter

switch (type)
	
	case 'sound_browser_palette'
		color = (2 * light_gray + 4 * [1 1 0.1]) / 6; 
		
	case 'image_filter'
		color = (2 * light_gray + 4 * [1 0.75 0.1]) / 6;
		
	case 'signal_filter'
		color = (2 * light_gray + 4 * [1 0.2 0.0]) / 6;
		
	case 'sound_feature'
		color = [0.4, 0.7, 0.65];
		
	case 'sound_detector'
		color = 0.9 * (2 * light_gray + 3 * [0.5 1 0.15]) / 5;
		
	case 'root'
		color = [0.4 0.4 0.7] + 0.2;
		
	otherwise
		color = light_gray;
		
end
