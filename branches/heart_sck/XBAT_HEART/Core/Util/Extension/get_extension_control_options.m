function opt = get_extension_control_options(ext, context)

%--
% consider type helper to get base options
%--

% TODO: this approach, also used in 'get_shared_extension_controls' should be evaluated

helper = str2func([ext.subtype, '_control_options']); 

if exist(helper) %#ok<EXIST>
	
	% NOTE: this helper will typically start by calling 'control_group'
	
	opt = helper(context);
	
else

	opt = control_group;
	
	% NOTE: these are generic extension defaults
	
	opt.width = 9; % helps to display feature name

	opt.top = 0; % header starts palette

	opt.bottom = 0; % extension controls determine bottom margin
	
end

if ~isempty(ext.fun.parameter.control.options)

	try
		update = ext.fun.parameter.control.options(context);
	catch
		update = struct; extension_warning(ext, 'Control options failed.', lasterror);
	end
	
	opt = struct_update(opt, update);

end

% NOTE: this is part of having a consistent interface

opt.header_color = get_extension_color(ext);

% NOTE: this has something to do with the magic

opt.ext = ext;
