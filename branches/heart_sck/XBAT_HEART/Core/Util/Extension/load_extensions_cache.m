function exts = load_extensions_cache %#ok<STOUT>

% NOTE: load extensions from file and load the into the persistent memory store

% NOTE: the variable 'exts' is loaded from the file, this operation is not as fast as you would want

load(extensions_cache_file);

% NOTE: this is where we load the file contents 

% extensions_cache(exts);