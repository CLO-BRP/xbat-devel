function ext = get_event_action_ext(varargin)

ext = get_extensions('event_action', 'name', varargin{:});
