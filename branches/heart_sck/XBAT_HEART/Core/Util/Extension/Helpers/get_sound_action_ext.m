function ext = get_sound_action_ext(varargin)

ext = get_extensions('sound_action', 'name', varargin{:});
