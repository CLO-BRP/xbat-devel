function ext = get_source_ext(varargin)

ext = get_extensions('source', 'name', varargin{:});
