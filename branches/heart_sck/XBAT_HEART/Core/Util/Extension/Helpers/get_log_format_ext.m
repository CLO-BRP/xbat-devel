function ext = get_log_format_ext(varargin)

ext = get_extensions('log_format', 'name', varargin{:});
