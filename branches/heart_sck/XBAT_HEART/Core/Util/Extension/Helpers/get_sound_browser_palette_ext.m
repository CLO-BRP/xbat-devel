function ext = get_sound_browser_palette_ext(varargin)

ext = get_extensions('sound_browser_palette', 'name', varargin{:});
