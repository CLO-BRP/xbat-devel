function ext = get_sound_detector_ext(varargin)

ext = get_extensions('sound_detector', 'name', varargin{:});
