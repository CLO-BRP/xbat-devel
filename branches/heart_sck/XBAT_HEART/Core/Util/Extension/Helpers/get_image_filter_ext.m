function ext = get_image_filter_ext(varargin)

ext = get_extensions('image_filter', 'name', varargin{:});
