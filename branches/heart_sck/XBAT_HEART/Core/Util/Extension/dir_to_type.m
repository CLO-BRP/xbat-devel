function [type, name] = dir_to_type(root)

% dir_to_type - directory to extension type and name transformation
% -----------------------------------------------------------------
%
% [type, name] = dir_to_type(root)
%
% Input:
% ------
%  root - directory to consider
% 
% Output:
% -------
%  type - of extension held here
%  name - of extension here
%
% See also: extensions_root, extension_root, extension_here, type_to_dir

%--
% setup and handle input
%--

% NOTE: by default we have found nothing

type = []; name = [];

if ~nargin
	root = pwd;
end

if isempty(strmatch(extensions_root, root))
	return;
end

%--
% we are within the all extensions root, deep enough?
%--

branch = strrep(root, extensions_root, ''); if branch(1) == filesep, branch(1) = []; end

part = str_split(branch, filesep);

if numel(part) < 2
	type = []; name = []; return;
end

%--
% get and check type and name if possible
%--

type = lower([part{2}, '_', part{1}(1:end - 1)]); 

if ~is_extension_type(type)
	error(['Unable to determine extension type. Inferred ''', type, ''' is not a valid type.']);
end

if numel(part) > 2
	name = part{3};
end
