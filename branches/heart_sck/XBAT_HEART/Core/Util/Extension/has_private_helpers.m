function value = has_private_helpers(ext)

% has_private_helpers - check whether an extension has private helpers
% --------------------------------------------------------------------
%
% value = has_private_helpers(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - helper indicator

%--
% handle multiple extensions recursively 
%--

if numel(ext) > 1
	value = iterate(mfilename, ext); return;
end

%--
% check for private helpers
%--

value = ~trivial(ext.fun.helper);
