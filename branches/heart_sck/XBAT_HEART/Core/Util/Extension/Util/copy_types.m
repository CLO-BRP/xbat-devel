function copy_types

root = fileparts(which('widget.m'));

types = get_field(what_ext(root, 'm'), 'm');

disp(' ');

for k = 1:length(types)
	
	%--
	% get source file to move and corresponding extension name
	%--
	
	% NOTE: the name is obtained through convention
	
	source = [root, filesep, types{k}]; name = title_caps(file_ext(types{k}));
	
	%--
	% make sure we have an extension to move to
	%--
	
	ext = get_extension('extension_type', name);
	
	disp([name, '::', int2str(~isempty(ext))]);
	
	if isempty(ext)
		
		ext = generate_extension('extension_type', name);
		
		if isempty(ext)
			disp(['Failed to create ''', name, '''.']); continue;
		end
		
		extensions_cache(ext);
		
	end
	
	%--
	% check for existing destination file and copy if needed
	%--
	
	destination = [extension_root(ext), filesep, 'private', filesep, 'signatures.m'];
	
	if exist(destination, 'file')
		disp(['Destination file ''', destination, ''' exists.']); continue;
	end
	
	copyfile(source, destination)
	
end

disp(' ');