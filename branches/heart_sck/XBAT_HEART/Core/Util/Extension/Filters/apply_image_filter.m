function X = apply_image_filter(X, ext, context)

% apply_image_filter - apply active image filter
% -----------------------------------------------
% 
% X = apply_image_filter(X, ext, context)
%
% Input:
% ------
%  X - input
%  ext - image filter
%  context - context
%
% Output:
% -------
%  X - filtered

% NOTE: extension compute functions are expected to handle single images

%--
% handle missing context
%--

if (nargin < 3) || isempty(context)
	
	context = struct;
	
	% NOTE: get debug flag from extension if possible
	
	if isfield(ext.control, 'debug')
		
		context.debug = ext.control.debug;
		
	end

end

% NOTE: get debug flag from extension if possible

if ~isfield(context, 'debug') && isfield(ext.control, 'debug')
	
	context.debug = ext.control.debug;
	
end

%--
% check for opacity parameter
%--

% NOTE: no opacity parameter leads to pure filtered image output

if ~isfield(ext.control, 'opacity')
	alpha = 1;
else
	alpha = ext.control.opacity;
end

%--
% filter image
%--

% NOTE: extension compute functions are expected to handle single images

% NOTE: here we transparently handle various forms of packing multiple images

X = filter_image(X, ext, context, alpha);


%------------------------------------------
% FILTER_IMAGE
%------------------------------------------

function X = filter_image(X, ext, context, alpha)

%--
% inpect image type and count number of images
%--

[type, count] = get_image_type(X);

%--
% compute according to storage type
%--

switch type

	case 'single'
		
		X = filter_and_blend(X, ext, context, alpha);
			
	case 'cells'

		for k = 1:count
			X{k} = filter_and_blend(X{k}, ext, context, alpha);
		end

	case 'sheets'

		for k = 1:count
			X(:, :, k) = filter_and_blend(X(:, :, k), ext, context, alpha);
		end

end


%------------------------------------------
% GET_IMAGE_TYPE
%------------------------------------------

function [type, count] = get_image_type(X)

%--
% cells containing images 
%--

if iscell(X)
	count = length(X); type = 'cells'; return;
end

%--
% volume image
%--

if ndims(X) == 3
	count = size(X, 3); type = 'sheets'; return;
end

%--
% single image
%--

count = 1; type = 'single';


%------------------------------------------
% FILTER_AND_BLEND
%------------------------------------------

function X = filter_and_blend(X, ext, context, alpha)

%--
% no filtering is needed in this case
%--

if alpha == 0
	return;
end 

%--
% apply filter, consider extension exception
%--

try
	
	if alpha ~= 1
		X = ext.fun.compute(X, ext.parameter, context);
	else
		X = (1 - alpha) * X + alpha * ext.fun.compute(X, ext.parameter, context);
	end
	
catch
	
	extension_warning(ext, 'Failed to apply image filter.', lasterror);

end



