function type = get_type(caller)

% get_type - get type of extension by examining caller location
% -------------------------------------------------------------
%
% type = get_type(caller)
%
% Output:
% -------
%  type - type of caller if computable

%--
% compute type from caller location
%--

% NOTE: the caller location includes the immediate parent directory as well

loc = caller.loc; loc(end) = [];

type = lower(str_implode(fliplr(loc(:)'), '_'));

% NOTE: remove traling 's', this is not very smart code
		
if ~isempty(type) && (type(end) == 's')
	type(end) = [];
end
