function ext = generate_extension(type, name, parent, description, project)

% generate_extension - generate extension skeleton
% ------------------------------------------------
%
% ext = generate_extension(type, name, parent, description)
%
% Input:
% ------
%  type - extension type
%  name - extension name
%  parent - parent name
%  description - short description of extension
%
% Output:
% -------
%  ext - extension

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% set and check project
%--

if nargin < 5
	project = '';
end

if ~isempty(project) && ischar(project) 
	project = get_project(project);
end 

%--
% set default description and parent
%--

if (nargin < 4) || isempty(description)
	description = '';
end

if (nargin < 3) || isempty(parent)
	parent = struct;
end

%--
% check and normalize type
%--

% NOTE: this function should not throw an error! this part is ugly

type = type_norm(type);

if isempty(type)
	
	disp(' ');
	disp('No extension created. Unrecognized extension type.');
	disp(' ');
	
	if nargout
		ext = [];
	end
	
	return;

end

%--
% check extension name
%--

if ~proper_filename(name)
	
	disp(' ');
	disp('No extension created. Name must be proper filename.');
	disp(' ');
	
	if nargout
		ext = [];
	end
	
	return;

end

%--
% get parent using name and type
%--

if ~trivial(parent)
	
	% NOTE: we discover the parent type extensions to give this the best chance
	
	ext0 = get_extensions([parent.type, '!'], 'name', parent.name);

	if isempty(ext0)
		
		disp(' ');
		disp('No extension created. Parent is not available.');
		disp(' ');

		if nargout
			ext = [];
		end
		
		return;

	end 
	
	parent.main_name = func2str(ext0.fun.main);
	
end

%----------------------------------------------------------
% GENERATE DIRECTORIES
%----------------------------------------------------------

%--
% get extension root from type and name
%--

% TODO: the root generation here is not DRY

if isempty(project)
	root = extensions_root;
else
	root = extensions_root('', project);
end

root = [root, filesep, type_to_dir(type), filesep, name];

% NOTE: if root directory exists, assume extension exists and return

% TODO: try to create extension instead

if exist(root, 'dir')
	
	disp(' ');
	disp('No extension created. Extension seems to exist.');
	disp(' ');
		
	if nargout
		ext = [];
	end
	
	return;
	
end

%--
% create extension directory tree 
%--

disp(' ');

% NOTE: add extension root to path immediately so we can create extension

mkdir(root); path(path, root);

child = get_extension_directories;

% TODO: consider adding these to path

for k = 1:length(child)
	
	mkdir(root, child{k}); rel_path([root, filesep, child{k}]);
	
end

%----------------------------------------------------------
% GENERATE MAIN
%----------------------------------------------------------

%--
% get name from extension name
%--

main_name = get_main_name(name, type);

%--
% create main
%--

% NOTE: we create this simple main to bootstrap extension creation

temp = [root, filesep, main_name, '.m']; rel_path(temp);

fid = fopen(temp, 'w');

if trivial(parent)
		
	fprintf(fid,'%s\n\n%s\n', ...
		['function ext = ', main_name], ...
		'ext = extension_create(mfilename);' ...
	);

else
	
	fprintf(fid,'%s\n\n%s\n', ...
		['function ext = ', main_name], ...
		['ext = extension_inherit(mfilename, ', parent.main_name, ');'] ...
	);
	
end

fclose(fid);

%----------------------------------------------------------
% CREATE EXTENSION
%----------------------------------------------------------

%--
% create extension
%--

% NOTE: move to extension root in case name is not unique, this should be avoided

curr = pwd; cd(root);

ext = eval(main_name);

cd(curr);

%--
% regenerate fuller main 
%--

ext.short_description = description;

if ~isempty(which('get_active_user'))
	user = get_active_user;
	
	ext.author = user.name; ext.email = user.email; ext.url = user.url;
else
	ext.author = ''; ext.email = ''; ext.url = '';
end

regenerate_main(ext);

% NOTE: for some reason we have to clear the function for the edits to show

clear(main_name); ext = eval(main_name);

%--
% generate compute, most extensions have a compute
%--

% generate_function(ext, 'compute');

%--
% update extensions cache and get extension
%--

% disp(' ');
% disp('Updating extensions cache ...');

extensions_cache(discover_extensions(ext.subtype, ext.name));

% NOTE: we test that get works on the newly generated extension

ext = get_extensions(ext.subtype, 'name', ext.name);

disp(' ');

if ~isempty(ext)
	disp('Extension succesfully created.');
else
	disp('Failed getting new extension.');
end

disp(' ');

%--
% add extensions directories to path, carefully!
%--

store = path;

try
	path(path, get_path_str(root, 'rec'));
catch
	path(store);
end

%--
% update browser extension stores
%--

try
	update_extension_store([], ext.subtype, ext.name);
catch
	db_disp 'TODO: update ''update_extension_store''';
end

%--
% we supress the command-line display of the extension
%--

if ~nargout
	clear ext;
end
