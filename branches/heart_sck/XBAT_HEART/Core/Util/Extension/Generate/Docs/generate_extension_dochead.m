function [lines, file] = generate_extension_dochead(ext)

% generate_extension_dochead - lines and possibly write file
% ----------------------------------------------------------
%
% [lines, file] = generate_extension_dochead(ext)
%
% Input:
% ------
%  ext - to generate dochead for
% 
% Output:
% -------
%  lines - of dochead
%  file - to write to

% TODO: there is more that can be generated for extensions, short description, family, methods ...

%--
% handle input
%--

if numel(ext) > 1
	[lines, file] = iterate(mfilename, ext); return; 
end

%--
% get default title and author
%--

switch ext.type
	case 'extension_type'
		title = [ext.name, ' API ', ext.version];
		
	otherwise
		title = [ext.name, ' ', ext.version];
		
end

% TODO: the case of multiple author

author = ext.author; email = ext.email;

lines = generate_dochead(title, author, email);

file = fullfile(extension_root(ext), 'Docs', 'DOCS', 'docs.tex');
