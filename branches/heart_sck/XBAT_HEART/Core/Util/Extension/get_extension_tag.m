function tag = get_extension_tag(ext, par)

% get_extension_tag - get tag for extension palette
% -------------------------------------------------
%
% tag = get_extension_tag(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  tag - tag for extension figure

% TODO: is this really a sufficient palette tag?

tag = ['XBAT_PALETTE::', upper(ext.subtype), '::', ext.name];

if nargin > 1
	tag = [tag, '::', int2str(par)];
end