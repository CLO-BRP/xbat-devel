function value = has_parameters(ext, type)

% has_presets - whether extension may have presets
% ------------------------------------------------
%
% value = has_presets(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - parameter type to consider (def: 'compute')
%
% Output:
% -------
%  value - indicator

%--
% set default parameter type
%--

if nargin < 2
	type = 'compute';
end

if ~string_is_member(type, get_parameter_types(ext))
	error(['Unrecognized parameter type ''', type, '''.']);
end

%--
% check if extension has parameters
%--

% TODO: consider how we may efficiently use an exist type test

if strcmp(type, 'compute')
	value = ~isempty(ext.fun.parameter.create);
else
	value = ~isempty(ext.fun.(type).parameter.create);
end