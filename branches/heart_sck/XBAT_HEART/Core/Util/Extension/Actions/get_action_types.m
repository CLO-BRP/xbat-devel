function types = get_action_types

% get_action_types - get action target types
% ------------------------------------------
%
% types = get_action_types
%
% Output:
% -------
%  types - action target types

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

types = get_extension_types(0, 'class', 'action');

types = get_extension_target_types(types);
