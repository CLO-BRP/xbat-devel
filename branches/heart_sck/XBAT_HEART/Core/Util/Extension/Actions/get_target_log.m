function log = get_target_log(target, context)

% get_target_log - get action target log from target name
% -------------------------------------------------------
%
% log = get_target_log(target, context)
%
% Input:
% ------
%  target - target name
%  context - action context
%
% Output:
% -------
%  log - target log

%--
% handle multiple inputs
%--

if iscellstr(target)
	log = iterate(mfilename, target, context); return; 
end

%--
% try to get log file
%--

% TODO: this should be a function and it may exist

file = [context.library.path, strrep(target, filesep, [filesep, 'Logs', filesep]), filesep, 'log.mat'];

% NOTE: this should not happen, handle error here

if ~exist(file, 'file')
	error('Unable to find log for action.');
end

%--
% load log from file
%--

log = log_load(file);
