function value = rand_state_blocks(m, n, init, method)

%--
% handle input
%--

if nargin < 4
	method = 'twister';
end

if nargin < 3
	init = 0; 
end

%--
% generate rand state blocks
%--

value = zeros(m, n);

for k = 1:n
	value(:, k) = rand_state(m, init + k - 1, method);
end