function [IMF, info] = emd(X, N, opt)

% emd - empirical mode decomposition for 1d signals
% -------------------------------------------------
%
% [IMF, info] = emd(X, n, opt)
%
% opt = emd
%
% Input:
% ------
%  X - signal(s) to decompose, one per column
%  n - number of intrinsic modes
%  opt - options
%
% Output:
% -------
%  IMF - matrix of intrinsic mode functions
%  info - on the computation
%  opt - default options

%--
% set and possibly output default options
%--

if nargin < 3
	opt.interp = 'cubic'; % interpolation method used by 'interp1'
	
	opt.tol = 0.3; % convergence tolerance for inner imf iteration
	
	opt.n = 100; % maximum number of inner imf iterations
	
	opt.TOL = 0.05; % convergence tolerance for outer decomposition steps iteration
	
	if ~nargin
		IMF = opt; return;
	end
end

% NOTE: by default we should compute modes until we converge

if nargin < 2 || isempty(N)
	N = 8;
end

%--
% compute emd
%--

IMF = X; info = empty(struct('sd', [], 'k', []));

e = inf; k = 0; 

while (e > opt.TOL) && (k < N)
	
	[imf, r, in] = compute_imf(IMF(:, end), opt.n, opt.tol);
	
	IMF(:, [end, end + 1]) = [imf, r]; info(end + 1) = in; %#ok<AGROW>
	
	% TODO: implement outer convergence test
	
	k = k + 1;
end

% db_disp; disp(info)

if ~nargout
	plot_emd(IMF);
end


%------------------------------
% COMPUTE_IMF
%------------------------------

function [IMF, R, info] = compute_imf(X, n, tol)

%--
% setup
%--

eps = 0.0000001;

opt = real_envelope; % opt.pad = 0;

%--
% iteration
%--

H0 = X;

sd = inf; k = 0; 

while sd > tol && k < n
	
	[U, L] = real_envelope(H0, opt);
	
	H1 = H0 - 0.5 * (U + L);
	
	sd = sum( (H1 - H0).^2 ./ (X + eps).^2 );
			
	H0 = H1; k = k + 1;
end

%--
% pack output
%--

IMF = H1; R = X - H1; 

if nargout > 2
	info.sd = sd; info.k = k;
end

