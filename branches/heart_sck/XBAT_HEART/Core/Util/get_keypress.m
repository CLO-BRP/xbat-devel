function key = get_keypress(handle)

% get_keypress - get keypress info from figure
% --------------------------------------------
%
% key = get_keypress(handle)
%
% Input:
% ------
%  handle - figure
%
% Output:
% -------
%  key - struct

if ~ishandle(handle) || ~strcmp(get(handle, 'type'), 'figure')
	key = []; return;
end

key.char = get(handle, 'currentcharacter');
	
key.code = double(key.char);
	
key.key = get(handle, 'currentkey');
