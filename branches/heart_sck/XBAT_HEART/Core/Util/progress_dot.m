function progress_dot(count, width)

% progress_dot - display for command-line waitbar
% -----------------------------------------------
%
% progress_dot(count, width)
%
% Input:
% ------
%  count - for dot
%  width - of dots

%--
% handle input
%--

if nargin < 2
	width = 64;
end 

persistent COUNT

% NOTE: count will be initialized to zero, it can be reset with input

if ~nargin
	if isempty(COUNT)
		COUNT = 0;
	end
else
	COUNT = count; return;
end

%--
% display dot
%--

fprintf(1, '.'); COUNT = COUNT + 1;

if COUNT > width
	fprintf(1, '\n'); COUNT = 0;
end