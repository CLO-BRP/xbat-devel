function str = linkify(file, label, fun)

% linkify - link a file in the command window
% -------------------------------------------
%
% str = linkify(file, label, fun)
%
% Input:
% ------
%  file - to link
%  label - to display
%  fun - to call (def: edit)
%
% Output:
% -------
%  str - link

%--
% handle input
%--

if nargin < 3
	fun = 'edit';
end

if nargin < 2 || isempty(label)
	label = file;
end

%--
% create link
%--

str = ['<a href="matlab:', fun, '(''', file, ''');">', label, '</a>'];

if ~nargout
	disp(str); clear str;
end 