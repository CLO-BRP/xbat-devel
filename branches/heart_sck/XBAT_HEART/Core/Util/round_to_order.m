function [value, relative] = round_to_order(value, order)

% round_to_order - round to get order of magnitude number
% -------------------------------------------------------
%
% [value, relative] = round_to_order(value, order)
%
% Input:
% ------
%  value - to round
%  order - in digits to remove or fraction of digits with some adaptation
%
% Output:
% -------
%  value - rounded to order
%  relative - relative error

%--
% handle input
%--

if nargin < 2
	order = 0.3;
end

if numel(value) > 1
	[value, relative] = iterate(mfilename, value, order); return;
end

fractional = (order < 1);

if fractional
	order = ceil((1 - order) * log10(value));
end 

%--
% round to order, with some adaptation in the case of 'fractional' order
%--

factor = 10^(order - 1); original = value; 

value = factor * round(original / factor);

offset = original - value;

% NOTE: this adaptation is rather ad hoc to provide numbers that humans like

% TODO: this can be slightly 'improved' with more splits in the range

if fractional
	
	test = abs(round(offset));
	
	if in_range(value, 10, 3000)

		if in_range(test, 2.5, 5)

			value = value + sign(offset) * 5;
			
		elseif in_range(test, 25, 50)
			
			value = value + sign(offset) * 50;
			
		end

	elseif in_range(value, 5000, 250000)
		
		if in_range(test, 250, 500)
			
			value = value + sign(offset) * 500;
			
		elseif in_range(test, 2500, 5000)
			
			value = value + sign(offset) * 5000;
			
		end
		

	end
	
end

offset = original - value;  relative = offset / original;
