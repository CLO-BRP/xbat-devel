function flag = proper_time_string(str)

% proper_time_string - determine whether a string may be a time
% -------------------------------------------------------------
%
% flag = proper_time_string(str)
%
% Input:
% ------
%  str - proposed time string
%
% Output:
% -------
%  flag - proper flag

flag = 0;

if ~ischar(str)
	return;
end

flag = ~isempty(clock_to_sec(str));
