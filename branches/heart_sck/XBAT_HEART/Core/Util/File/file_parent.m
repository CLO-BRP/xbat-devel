function par = file_parent(p, k)

% file_parent - get immediate parent directory
% --------------------------------------------
%
% par = file_parent(p, k)
%
% Input:
% ------
%  p - file path
%  k - levels to go up (def: 1)
%
% Output:
% -------
%  par - parent directory name

%--
% set immediate parent default
%--

if nargin < 2
	k = 1;
end

%--
% compute parent directory
%--

% TODO: handle asking for too many levels, implement directly square complexity

for k = 1:k
	p = path_parts(p);
end

% NOTE: this gets the immediate parent

[ignore, par] = path_parts(p);