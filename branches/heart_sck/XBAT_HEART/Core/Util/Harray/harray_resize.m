function flag = harray_resize(par, eventdata)

% harray_resize - resize function for harray display figures
% ----------------------------------------------------------
%
% flag = harray_resize(par,eventdata)
%
% Input:
% ------
%  par - parent figure handle
%  eventdata - not currently used
%
% Output:
% -------
%  flag - success flag

% TODO: this function needs work, sometimes it gets caught in strange recursive state

%--
% get harray data
%--

data = harray_data(par);

base = data.base;

layout = data.layout;

h = data.harray;

%--
% get parent figure size
%--

par_pos = get_size_in(par, base.units);

%--
% resize toolbar axes
%--

pos = [ ...
	0, par_pos(4) - base.tool.size, par_pos(3), base.tool.size ...
];

if ~base.tool.on
	pos(2) = par_pos(4) - 0.1 * pos(4);
end

% NOTE: here and below we should be able to use the layout state

ax = findobj(par, 'tag', 'HARRAY_TOOLBAR');

if ~isempty(ax)
	set(ax, ...
		'units', base.units, 'position', pos ...
	);

	set_tab_positions(ax, 'tool');
end

%--
% create status axes
%--

pos = [ ...
	0, 0, par_pos(3), base.status.size ...
];

if ~base.status.on
	pos(2) = -0.8 * pos(4);
end
	
ax = findobj(par, 'tag', 'HARRAY_STATUSBAR');

if ~isempty(ax)
	set(ax, ...
		'units', base.units, 'position', pos ...
	);

	str = findobj(ax, 'tag', 'HARRAY_STATUS_LEFT');

	if ~isempty(str)

		% TODO: factor this type of string placement, extend to other objects

		% NOTE: consider moving this to the status set

		pix = get_size_in(ax, 'pixels'); pos = get(str, 'position'); pos(1) = 8 / pix(3); set(str, 'position', pos);

	end

	set_tab_positions(ax, 'status');
end

%--
% resize colorbar axes
%--

% NOTE: we use a single colorbar since a figure has a single colormap

if base.color.on

	pos(1) = par_pos(3) - (base.color.size + base.margin(2) + layout(1).margin(2));
	
	pos(2) = base.margin(3) + ...
		(base.status.on * base.status.size) + ...
		layout(1).margin(3) ...
	; 

	pos(3) = base.color.size;
	
	pos(4) = par_pos(4) - ( ...
		base.margin(1) + base.margin(3) + ...
		(base.tool.on * base.tool.size) + (base.status.on * base.status.size) + ...
		layout(1).margin(1) + layout(1).margin(3) ...
	);

	set(findobj(par, 'tag', 'HARRAY_COLORBAR'), ...
		'units', base.units, 'position', pos ...
	);

end

%--
% resize base axes
%--

pos(1) = base.margin(4);

pos(2) = base.margin(3) + (base.status.on * base.status.size); 

pos(3) = par_pos(3) - ( ...
	base.margin(2) + base.margin(4) + ...
	(base.color.on * (base.color.size + base.color.pad)) ...
);

pos(4) = par_pos(4) - ( ...
	base.margin(1) + base.margin(3) + ...
	(base.tool.on * base.tool.size) + (base.status.on * base.status.size) ...
);

set(h(1).axes,...
	'units', base.units, 'position', pos ...
);

%--
% resize other axes in a top down way
%--

flag = 1;

for k = 1:length(layout)

	%--
	% find parent axes for this level
	%--
	
	lev = cell2mat({h.level}');
	
	ix = find(lev == (k - 1));
	
	for j = 1:length(ix)
	
		%--
		% compute new positions of children relative to parent
		%--
		
		pos = pos_rel(h(ix(j)).axes, layout(k));
		
		%--
		% update children positions
		%--
		
		for i2 = 1:size(pos, 2)
			
			for i1 = 1:size(pos, 1)

				if (any(pos{i1, i2} <= 0))
					flag = 0;
				end

				% NOTE: the max protects us from non-positive width and height

				set(h(ix(j)).children(i1, i2), ...
					'units', layout(k).units, 'position', max(pos{i1, i2}, eps) ...
				);

			end
			
		end

	end
	
end