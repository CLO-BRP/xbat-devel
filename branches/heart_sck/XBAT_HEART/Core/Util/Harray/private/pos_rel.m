function pos = pos_rel(par, layout)

% pos_rel - compute children positions relative to parent
% -------------------------------------------------------
%
% pos = pos_rel(par,layout)
%
% Input:
% ------
%  par - parent axes
%  layout - children layout
%
% Output:
% -------
%  pos - children positions relative to parent

%--
% get size of parent in margin units and update using margins
%--

par_pos = get_size_in(par, layout.units);

par_pos(1:2) = par_pos(1:2) + layout.margin([4, 3]);

par_pos(3) = par_pos(3) - (layout.margin(2) + layout.margin(4));

par_pos(4) = par_pos(4) - (layout.margin(1) + layout.margin(3));

%--
% compute positions of children based on level configuration
%--

% NOTE: even for uniform layouts pad and frac are vectors

dx = (par_pos(3) - sum(layout.col.pad)) .* layout.col.frac;

dy = (par_pos(4) - sum(layout.row.pad)) .* fliplr(layout.row.frac);

for j = 1:layout.col.size
	
	for i = 1:layout.row.size

		pos{i, j} = [ ...
			par_pos(1) + sum(layout.col.pad(1:j - 1)) + sum(dx(1:j - 1)), ...
			par_pos(2) + sum(layout.row.pad(1:i - 1)) + sum(dy(1:i - 1)), ...
			dx(j), dy(i) ...
		];

	end
	
end

pos = flipud(pos);
