function h = harray_create

% harray_create - create structure for harray elements
% ----------------------------------------------------
%
% h = harray_create

h.axes = [];

h.index = [];

% NOTE: a level is stored explicitly for convenience

h.level = [];

% NOTE: the harray elements are linked up and down

h.parent = [];

h.children = [];