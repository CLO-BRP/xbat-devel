function str = html_escape(str)

% html_escape - very basic HTML escape 
% -------------------------------------
%
% str = html_escape(str)
%
% Input:
% ------
%  str - string to escape
%
% Output:
% -------
%  str - escaped string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--
% set some replacement patterns
%--

% TODO: add other common entities, for example '&amp;'

table = { ...
	'<', '&lt;'; ...
	'>', '&gt;'; ... 
	'"', '&quot;'; ...
	'\', '\\' ...
}; 

%--
% perform replacement
%--

pat = table(:, 1); rep = table(:, 2);

for k = 1:length(pat)
	str = strrep(str, pat{k}, rep{k});
end
