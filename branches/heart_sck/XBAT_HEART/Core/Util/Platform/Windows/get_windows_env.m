function env = get_windows_env

% get_windows_env - get windows environment variables
% ---------------------------------------------------
%
% env = get_windows_env
%
% Output: 
% -------
%  env - environment variable struct

if ~ispc
	error('This function is only available for Windows.');
end

%--
% get full env string
%--

% NOTE: we get env string and parse elements into cells

[status, result] = system('set'); 

env = parse_env(result);


%----------------
% PARSE_ENV
%----------------

% NOTE: we could further parse some of the fields for convenience

function env = parse_env(result)

env = struct;

lines = file_readlines(result); 

if isempty(lines{end})
	lines(end) = []; 
end

for k = 1:length(lines)
	
	[field, value] = strtok(lines{k}, '='); 
	
	field = lower(strrep(field, ' ', '_')); value = strtrim(value(2:end));
	
	if ~isvarname(field) 
		field = genvarname(field);
	end
	
	try
		env.(field) = eval(value);
	catch
		env.(field) = value;
	end
	
end
