function value = get_value_through_callback(type, control, value)

% get_value_through_callback - get value of control considering callback
% ----------------------------------------------------------------------
% 
% value = get_value_through_callback(type, control, value)
%
% Input:
% ------
%  type - of value to get, 'get' and 'set' for example
%  control - parent
%  value - input
%
% Output:
% -------
%  value - obtained

%--
% use control value as default value
%--

if nargin < 3
	value = control.value;
end

%--
% check for 'set' behavior
%--

if ~string_is_member(type, {'get', 'set'})
	error(['Unrecognized value type ''', type, '''']);
end

% NOTE: this is a hack to possibly handle a control 'duck' used in 'get_control' and 'set_control'

if isfield(control, 'control')
	control = control.control;
end

% NOTE: we simply return the input or control value when there is no related callback

if ~isfield(control, type) || isempty(control.(type))
	return;
end

%--
% use 'set' callback
%--

callback = control.(type);

if iscell(callback)
	fun = callback{1}; args = callback(2:end);
else
	fun = callback; args = {};
end

% NOTE: the set callback signature is control, value, other args ... similar to the obj, eventdata, other args

try
	value = fun(control, value, args{:});
catch
	nice_catch(lasterror, ['''', title_caps(type), ''' access callback failed for ''', control.name, '''.']);
end