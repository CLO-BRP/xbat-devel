function [toggle, state] = get_toggle_by_label(pal, str)

% get_toggle_by_label - get toggle handle using associated label
% --------------------------------------------------------------
%
% [toggle, state] = get_toggle_by_label(pal, str)
%
% Input:
% ------
%  pal - palette
%  str - label string
%
% Output:
% -------
%  toggle - handle
%  state - current toggle state

%--
% get all toggles
%--

% NOTE: these can be used directly in programatic calls to 'palette_toggle'

toggles = findobj(pal, 'tag', 'header_toggle');

if isempty(toggles)
	return;
end

%--
% get toggle labels
%--

toggle = []; state = [];

for k = 1:numel(toggles) 
	
	% NOTE: the header may contain two text objects a 'toggle' and a 'label'
	
	current = toggles(k); label = setdiff(findobj(get(current, 'parent'), 'type', 'text'), current);
	
	if isempty(label)
		continue;
	end
	
	if strcmp(get(label, 'string'), str)
		
		toggle = current;
		
		if nargout > 1
			state = ternary(strcmp(get(toggle, 'string'), '+'), 'close', 'open');
		end
		
	end
	
end