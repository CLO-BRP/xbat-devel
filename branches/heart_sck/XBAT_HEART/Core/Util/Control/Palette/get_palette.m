function pal = get_palette(par, name, data)

% get_palette - get palette figure handle by name
% -----------------------------------------------
%
% pal = get_palette(par, name, data)
%
% Input:
% ------
%  par - parent handle
%  name - palette name
%  data - parent data (where palettes are registered)
%
% Output:
% -------
%  pal - palette handle

%--
% handle missing or root parent
%--

if isempty(par) || par == 0
	
	pal = get(0, 'children');
	
	% NOTE: we return if there are no open palettes or there is no name selection
	
	if isempty(pal) || (nargin < 2 || isempty(name))
		return;
	end 

	pal = select_by_name(pal, name); return;
	
end
	
%--
% get palettes using robust search if no selection
%--

if nargin < 2 || isempty(name)
	
	pal = get_xbat_figs('type', 'palette', 'parent', par); return;	

end
	
%--
% get registered palettes
%--

% NOTE: get parent data if needed, and remove stale handles 

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

try
	pal = data.browser.palettes;
catch
	pal = data.browser.palette.handle;
end

pal = pal(ishandle(pal));

% NOTE: if there are no registered palettes we try the more robust search

if isempty(pal)
	pals = get_xbat_figs('type', 'palette', 'parent', par);
end

% NOTE: if at this point we have found no candidate palettes we are done

if isempty(pal)
	pal = []; 
end

%--
% select palette by name
%--

pal = select_by_name(pal, name);


%-------------------------
% SELECT_BY_NAME
%-------------------------

function pal = select_by_name(pals, name)

tags = get(pals, 'tag');

% NOTE: this happens for the single handle case 

if ischar(tags)
	tags = {tags};
end 
	
pal = [];	

for k = 1:length(tags)	
	
	info = parse_tag(tags{k}, '::', {'header', 'type', 'name'});
	
	if strcmp(info.name, name)
		pal = pals(k); break;
	end

end

