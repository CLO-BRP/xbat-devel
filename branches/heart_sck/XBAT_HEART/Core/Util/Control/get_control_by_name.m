function [control, ix] = get_control_by_name(pal, name, controls)

% get_control_by_name - get control by name
% -----------------------------------------
%
% [control, ix] = get_control_by_name(pal, name, controls)
%
% Input:
% ------
%  pal - palette handle
%  name - control name
%  controls - palette controls
%
% Output:
% -------
%  control - control
%  ix - index

% TODO: consider check for non-unique control names

% TODO: consider output of control handles as well

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% get palette controls if needed
%--

% NOTE: palette input is required if we omit controls input

if nargin < 3
	controls = get_palette_controls(pal);
end

%----------------------------
% FIND CONTROL
%----------------------------

%--
% find control by name, get index
%--

% NOTE: consider simple and grouped controls

for ix = 1:length(controls)
	
	%--
	% select control
	%--
	
	control = controls(ix);
	
	%--
	% check for matching name
	%--
	
	switch class(control.name)
		
		case 'char'
			if strcmp(control.name, name)
				return;
			end
			
		case 'cell'
			if ~isempty(find(strcmp(name, control.name), 1))
				return;
			end
			
	end
	
end

%--
% return empty
%--

control = []; ix = [];
