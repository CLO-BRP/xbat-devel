function [value, name] = is_dialog(pal)

% is_dialog - determine whether figure is dialog
% ----------------------------------------------
%
% [value, name] = is_dialog(pal)
%
% Input:
% ------
%  pal - figure handle
%
% Output:
% -------
%  value - result of dialog test
%  name - name of dialog figure

%--
% test for dialog
%--

% NOTE: this function assumes a convention of using 'DIALOG' in tag

value = ~isempty(findstr(get(pal, 'tag'), 'DIALOG'));

%--
% return name for convenience
%--

if nargout > 1
	name = get(pal, 'name');
end
