function names = get_color_names(filter)

% NOTE: we always include 'None' to allow for custom colors

colors = load_css_colors; names = {'None', colors.name};

if ~nargin || isempty(filter)
	return;
end

select = string_contains(names, filter, 0); select(1) = 1; names = names(select);