function handle = test_controls(control, opt, callback)

%--
% handle input
%--

if (nargin < 3) || isempty(callback)
	callback = @talkback;
end 

if (nargin < 2) || isempty(opt)
	opt = control_group;
end

%--
% create test controls palette
%--

try
	handle = control_group([], callback, 'Test', control, opt);
catch
	nice_catch(lasterror, 'Failed to create test palette for controls.');
end


%--------------------
% TALKBACK
%--------------------

function talkback(obj, eventdata)

callback = get_callback_context(obj, eventdata);

callback.eventdata = eventdata;

db_disp talkback; flatten(callback)
