function pal = test_password_control

% TODO: the callback behavior is strange, and onload does not work

% TODO: disable does not yet work for the password control

% NOTE: the first can be handled by using the 'keytyped' callback to turn on the 'lostfocus' callback

% TODO: consider not allowing spaces or other punctuation

%--
% create controls
%--

control = empty(control_create); 

tabs = {'Full', 'Empty'};

% NOTE: the tabs to check that tab selection properly hides the password control

control(end + 1) = tabs_control(tabs);

control(end + 1) = control_create( ... 
	'tab', tabs{1}, ...
	'name', 'password', ... 
	'tooltip', 'this is my tooltip', ...
	'onload', 1, ...
	'string', 'password', ...
	'style', 'password' ...
);

% NOTE: this tests that we can read the password value

control(end + 1) = control_create( ...
	'tab', tabs{1}, ...
	'name', 'revealed', ... 
	'tooltip', 'this is my tooltip', ...
	'style', 'edit' ...
);

% NOTE: this tests that we can disable the password control, not working yet

control(end + 1) = checkbox_control('enable', 1, ...
	'tab', tabs{1} ...
);

%--
% render controls
%--

pal = control_group([], @callback, 'Password', control);


%------------------
% CALLBACK
%------------------

function callback(obj, eventdata) %#ok<INUSD>

callback = get_callback_context(obj, eventdata);

% NOTE: this is to see that we are getting a reasonable callback context

% db_disp password-test-callback; flatten(callback)

switch callback.control.name
	
	case 'password'
		
		set_control(callback.pal.handle, 'revealed', 'value', callback.value);
		
	case 'enable'
		
		other = {'password', 'revealed'};
		
		for k = 1:numel(other)
			set_control(callback.pal.handle, other{k}, 'enable', callback.value);
		end
		
end
