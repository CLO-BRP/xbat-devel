function result = scaffold_menu_callback(type, callback, context)

result = struct;

if strcmp(type, 'compute')
	fun = context.ext.fun.parameter.control.callback
else
	fun = context.ext.fun.(type).parameter.control.callback
end

if isempty(fun)
	return;
end

% extension_control_callback_router(obj, eventdata, ext, type, fun)