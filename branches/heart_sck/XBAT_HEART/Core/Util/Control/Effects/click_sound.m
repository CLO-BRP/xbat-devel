function click_sound

% click_sound - generate click (like the iPod wheel noise)
% --------------------------------------------------------
%
% click_sound

persistent CLICK_SOUND_DATA

%--
% check sound configuration
%--

if ~isequal(get_env('palette_sounds'), 'on')
	return;
end

%--
% create click data if needed
%--

if isempty(CLICK_SOUND_DATA)
	CLICK_SOUND_DATA = 0.025 * filter([1 -1], [1 0 -0.5], randn(1, 40));
end

%--
% play click
%--

wavplay(CLICK_SOUND_DATA, 'async');
