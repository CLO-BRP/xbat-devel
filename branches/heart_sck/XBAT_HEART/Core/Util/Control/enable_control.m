function enable_control(pal, control)

% enable_control - enable control
% -------------------------------
%
% enable_control(pal, name)
%
% enable_control(control)
%
% Input:
% ------
%  pal - palette
%  name - of control
%  control - control

if nargin < 2
	control = pal; pal = [];
end

if ~ischar(control) && numel(control) > 1
	iteraten(mfilename, 2, pal, control); return; 
end

set_control(pal, control, 'enable', 1);