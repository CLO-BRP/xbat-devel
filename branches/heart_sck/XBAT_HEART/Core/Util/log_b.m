function y = log_b(x, b)

% log_b - log base
% ----------------
% 
% y = log_b(x, b)
%
% Input:
% ------
%  x - input
%  b - base
%
% Output:
% -------
%  y - log of input

y = log(x) ./ log(b);
