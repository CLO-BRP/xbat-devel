function out = empty(in)

% empty - create an empty array for a type
% ----------------------------------------
%
% out = empty(in)
%
% Input:
% ------
%  type - type prototype
%
% Output:
% -------
%  out - empty array of prototype type

% TODO: extend 'empty' to take fieldnames and produce empty struct

% NOTE: why is this not a part of the language? because it is strange

if isempty(in)
	out = in; return;
end

out = in(1); out(1) = [];
