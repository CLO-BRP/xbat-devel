function str = md5(in)

% md5 - compute MD5 digest for matlab variable
% --------------------------------------------
%
% str = md5(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  str - MD5 digest string
%
% See also: hash, md5, sha1, to_str

if ~ischar(in)
	in = to_str(in);
end

str = hash(in, 'md5');