function [result, verify] = file_md5(file)

% file_md5 - compute hash for file
% --------------------------------
%
% result = file_md5(file)
%
% Input:
% ------
%  file - location
%
% Output:
% -------
%  result - hash

% NOTE: code here produces values different than on the ruby side

% TODO: is the above note still true? probably not

result = fast_md5(file, 1);

if nargout > 1
	verify = hash(file, 'md5', 1);
end
