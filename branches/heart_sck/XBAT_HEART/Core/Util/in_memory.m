function result = in_memory(fun)

% in_memory - test for function
% -----------------------------

result = string_is_member(fun, inmem);