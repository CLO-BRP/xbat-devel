function privatize(file)

%--
% set current directory default
%--

if ~nargin
	file = pwd;
end 

%--
% get m files and link them to be privatized
%--

if exist(file, 'dir')
	
	content = what(root); files = strcat(content.path, filesep, content.m);

	for k = 1:numel(files)
		disp(link_helper(files{k}));
	end

%--
% move file to private directory
%--

else
	
	move_helper(which(file));
	
end


%------------------------
% LINK_HELPER
%------------------------

function str = link_helper(file)

[p1, p2, p3] = fileparts(file); name = [p2, p3];

str = ['<a href="matlab:privatize(''', file, ''', 0);">', name ,'</a>'];


%------------------------
% MOVE_HELPER
%------------------------

function move_helper(file)

root = fileparts(file); destination = fullfile(root, 'private');

if exist(destination, 'dir')
	destination = create_dir(destination);
end

if isempty(destination)
	error('Failed to create ''private'' directory.');
end
	
move_file(file, destination, 1);
