function logger( level, message, options )

% logger - utility function to log system events
% ----------------------------------------
%
% logger( level, message, options )
%
% Input:
% -------
%  level   - {'fatal', 'error', 'warn', 'info', 'debug', 'all'}
%  message - strings  to be logged
%  options - for the moment, the only option is 'logname'

if ~nargin
	return;
end

% Set default level to DEBUG

if nargin < 2
    message = level;
    level = 'debug';
end

% Set default log to 'xbat'

if nargin < 3
    options = struct;
    options.logname = 'xbat';
end

% Catch unknown logger levels

if isempty(logger_levels(level))
    logger( 'warn', ['Not sure what level ''', level, '''', ' is! Logging in debug mode ...'] );
    logger( 'debug', message );
    return
end

logname = options.logname;

% For the moment, logging happens on 3 levels without discrimination
proceed = 0;

% ALL ON
if isnumeric(logging) && logging == 1
	proceed = 1;

elseif ischar(logging) && should_log(logging, level)
	proceed = 1;
        
elseif iscell(logging)
	logger_settings = logging; %create a temp
	for i=1:numel(logger_settings)
        if should_log(logger_settings{i}, level)
            proceed = 1; break;
        end
    end
    
end


% !!! THERE IS AN ERROR WHEN TRYING TO USE SQLITE MEX FUNCTION::
% IF YOU FEEND IN SQL IN A MESSAGE TO BE LOGGED, I THINK IT TRIES TO
% EXECUTE SAID SQL FOR YOU!
if proceed
    logger_to_terminal(level, message);
    %logger_to_db(logname, level, message);
    logger_to_file(logname, level, message);
end


function value = should_log(logger_setting, level)

    value = strcmp(logger_setting, level) || ... 
            strcmp(logger_setting, 'all');





