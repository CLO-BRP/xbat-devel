function result = clear_log( logname )

if nargin < 1
    error('You must specify a log to clear')
end

db = get_logger_db_fullfile( logname );

file = get_logger_file_fullfile( logname );

try

    if which(file)
        result = delete(file);
    end

    if which(db)
        db = get_logger_db( db );
        result = sqlite(db, 'DELETE from logs');
    end
    
    result = 1;

catch

    result = 0;

end