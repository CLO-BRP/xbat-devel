function B = add_matrix_pivot(A, k)

% add_matrix_pivot - add pivot to matrix after given pivot position
% -----------------------------------------------------------------
%
% A = add_matrix_pivot(A, k)
%
% Input:
% ------
%  A - matrix to update
%  k - add position
% 
% Output:
% -------
%  A - updated matrix

%--
% handle input
%--

[m, n] = size(A); d = min(m, n);

% NOTE: by default we append a pivot to the matrix

if nargin < 2
	k = d;
else
	if (k ~= round(k)) || (k < 0) || (k > d)
		error('Pivot add position must be an integer between 0 and the starting number of pivots.');
	end
end

%--
% update matrix
%--

B = zeros(m + 1, n + 1); 

B(1:k, 1:k) = A(1:k, 1:k); B(1:k, k + 2:end) = A(1:k, k + 1:end);

B(k + 2:end, 1:k) = A(k + 1:end, 1:k); B(k + 2:end, k + 2:end) = A(k + 1:end, k + 1:end);