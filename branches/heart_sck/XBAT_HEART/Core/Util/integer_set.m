function result = integer_set(operation, varargin)

% integer_set - operations
% ------------------------
%
% result = integer_set('intersect', set, ... )
%
% result = integer_set('union', set, ... )
%
% Input:
% ------
%  set - integer array

for k = 1:numel(varargin)
	varargin{k} = forward(varargin{k});
end

result = back(index_set(operation, varargin{:}));

result = back(result);


%------------------------
% INDEX_SET
%------------------------

function result = index_set(operation, varargin)

% NOTE: we build a sets indicator matrix

I = sparse([]);

for k = 1:numel(varargin)
	I(varargin{k}(:), k) = 1;
end

% NOTE: set operation collapses indicator

switch operation
	
	case 'intersect'
		result = find(prod(I, 2));
		
	case 'union'
		result = find(sum(I, 2) > 0);
	
end


%-------------------------------------
% INTEGER HELPERS
%-------------------------------------

% NOTE: the in-place maps below allow us to extend our solution to all integers

function n = forward(n)

% NOTE: positive numbers are mapped to even, non-positive to odd

pos = n > 0; 

n(pos) = 2 * n(pos); 

n(~pos) = -2 * n(~pos) + 1;


function n = back(n)

% NOTE: odd numbers are mapped to non-positive, even to positive

odd = mod(n, 2); 

n(odd) = -0.5 * (n(odd) - 1);

n(~odd) = 0.5 * n(~odd);


%-------------------------------------
% REAL HELPERS
%-------------------------------------

%------------------------
% SET_UNION
%------------------------

function result = set_union(varargin)

% set_union - for many sets
% -------------------------
%
% result = set_union(set, ... )
%
% Input:
% ------
%  set - to coalesce

total = [];

for k = 1:numel(varargin)
	total = [total; varargin{k}(:)]; %#ok<AGROW>
end

total = sort(total); 

index = [1; find(diff(total)) + 1];

% NOTE: the resulting set is ordered

result = total(index);


%------------------------
% SET_TO_INDEX
%------------------------

% TODO: currently considers one element, extend to set

function index = set_to_index(set, total)

% set_to_index - map
% ------------------
%
% index = set_to_index(sub, total)
%
% Input:
% ------
%  sub - set
%  total - set
%
% Output:
% -------
%  index - of subset elements in total set
%
% NOTE: the total set must be ordered

%--
% initialize index and bracket
%--

index = []; low = 1; high = numel(total); center = floor(0.5 * (low + high)); 

%--
% try to find index
%--

% NOTE: handle edge cases first

if set == total(1)
	index = 1; return; 
end

if set == total(end)
	index = numel(total); return; 
end 

% NOTE: bisect the ordered set

while isempty(index) && low + 1 < high
	
	if set < total(center)
		high = center;
	elseif set > total(center)
		low = center;
	else
		index = center;
	end

	center = floor(0.5 * (low + high));
		
end








