function file = list_images(root, type)

% list_images - found in directory scan
% -------------------------------------
%
% file = list_images(root, type)
%
% Input:
% ------
%  root - for scan (def: pwd)
%  type - of image, extension(s) (def: known_image_types)
%
% Output:
% -------
%  file - list
%
% See also: scan_dir_for, known_image_types

%--
% handle input
%--

if nargin < 2
	type = known_image_types;
else
	if any(~string_is_member(type, known_image_types))
		error('Unknown image type input.');
	end
end

% NOTE: we make the search case-insensitive

type{end + 1} = 'insensitive';

if ~nargin
	root = pwd;
end

%--
% scan for image types
%--

file = scan_dir_for(type, root);
