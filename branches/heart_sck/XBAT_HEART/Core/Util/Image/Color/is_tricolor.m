function [value, m, n] = is_tricolor(X)

% is_tricolor - checks for three-dimensional three sheet array
% ------------------------------------------------------------
%
% value = is_tricolor(X)
%
% Input:
% ------
%  X - image
%
% Output:
% -------
%  value - test indicator

value = ndims(X) == 3 && size(X, 3) == 3;

if nargout > 1
	[m, n, ignore] = size(X); %#ok<NASGU>
end