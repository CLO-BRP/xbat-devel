function Y = lab_to_rgb(X)

% lab_to_rgb - lab to rgb conversion
% ----------------------------------
%
% Y = lab_to_rgb(X)
%
% Input:
% ------
%  X - lab image
%
% Output:
% -------
%  Y - rgb image

Y = xyz_to_rgb(lab_to_xyz(X));
