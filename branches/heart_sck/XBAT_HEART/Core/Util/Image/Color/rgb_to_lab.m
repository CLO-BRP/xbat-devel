function Y = rgb_to_lab(X)

% rgb_to_lab - rgb to lab conversion
% ----------------------------------
%
% Y = rgb_to_lab(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - lab image

% NOTE: size checking happens in the called functions

Y = xyz_to_lab(rgb_to_xyz(X));
