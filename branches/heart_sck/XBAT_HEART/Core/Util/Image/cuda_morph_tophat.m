function Y = cuda_morph_tophat(X,SE,t,Z,b)

% cuda_morph_tophat - morphological tophat
% ----------------------------------------
% 
% Y = cuda_morph_tophat(X,SE,t,Z,b)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  t - type of tophat
%
%   -1 - self complementary (def)
%    0 - dark
%    1 - bright
% 
%  Z - computation mask image (def: [])
%  b - boundary behavior (def: -1)
%
% Output:
% -------
%  Y - tophat image

%--
% set boundary behavior
%--

if (nargin < 5)
	b = -1;
end

%--
% set mask
%--

if (nargin < 4)
	Z = [];
end

%--
% set type
%--

if (nargin < 3)
	t = -1;
end

SE = uint8(SE);

m = size(SE, 1); n = size(SE, 2);

%--
% compute tophat
%--

switch (t)
	
	%--
	% self complementary
	%--
		
	case (-1)
		
		[Y, status] = cuda_morph_filter_mex(X, SE, m, n, 0, 6);
		
        handle_cuda_failure(status)

    %--
	% dark
	%--
		
	
	case (0)
		
		[Y, status] = cuda_morph_filter_mex(X, SE, m, n, 0, 7);
				
        handle_cuda_failure(status)
        
	%--
	% bright
	%--
		
	case (1)
		
		[Y, status] = cuda_morph_filter_mex(X, SE, m, n, 0, 5);
				
        handle_cuda_failure(status)
		
end

