function Y = image_thresh(X, T, type, varargin)

% image_thresh - image thresholding
% ---------------------------------
%
% Y = image_thresh(X, T)
%
% Input:
% ------
%  X - input image
%  T - increasing thresholds
%
% Output:
% -------
%  M - thresholded image, pixel indicates number of thresholds cleared
%
% NOTE: the result of this computation is related to quantization

%--
% set mex helper
%--

% NOTE: this type argument is here only for testing! reconsider this,
% perhaps an environment variable for 'get_mex_helper'

if nargin < 3
	type = '';
end

helper = get_mex_helper(mfilename, type);

%--
% check threshold input
%--

if (length(T) > 1) && any(diff(T) <= 0)
	error('Thresholds must be increasing.');
end

%--
% convert input to double if needed
%--

% TODO: mex should handle a variety of types

if ~strcmp(class(X), 'double')
	X = double(X);
end

%--
% use mex helper for thresholding
%--

% NOTE: we may send additional arguments to the MEX using the variable arguments

Y = helper(X, T, varargin{:});

