function [X, handle] = get_image_data(par)

% get_image_data - get image cdata
% --------------------------------
%
% [X, handle] = get_image_data(par)
%
% Input:
% ------
%  par - handle to parent figure or axes (def: gcf)
%
% Output:
% ------
%  X - image data
%  handle - handle to image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:52-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set parent handle
%--

if nargin < 1
	par = gcf;
end

%--
% get image handle
%--

handle = get_image_handles(par);

if length(handle) >  1
		
	tmp = findobj(gca, 'type', 'image');
	
	if ~isempty(find(handle == tmp, 1))
		handle = tmp;
	else
		handle = handle(1);

		warning('Parent contains multiple images, first image selected.');
	end
end

%--
% get image data
%--

X = get(handle, 'CData');
