function [E, count] = image_extrema(X, SE, type, opt)

% image_extrema - computation
% ---------------------------
%
%  [E, count] = image_extrema(X, SE, type, opt)
%
%  opt = image_extrema
%
% Input:
% ------
%  X - image
%  SE - structuring element
%  type - of extrema: 1 or 'peaks', 0 or 'valleys', -1 or 'both' or 'all'
%  opt - options
% 
% Output:
% -------
%  E - extrema indicator matrix
%  count - of extrema
%  opt - default options
% 
% NOTE: when both types of extrema are computed the indicator matrix 'E'
% has 1 for peaks and -1 for valleys
%
% NOTE: when a 'count' output is requested for both extrema a two vector
% with a negative and a positive entry counts extrema of different types
%
% See also: morph_dilate, morph_erode, morph_min_max, morph_extrema

%--
% handle input
%--

if nargin < 4
	% NOTE: when set to 'false' this can be used to remove boundary extrema
	
	opt.boundary = true;
	
	if ~nargin && nargout
		E = opt; return;
	end
end

if nargin < 3
	type = -1;
end 

if nargin < 2
	SE = ones(3);
end

%--
% compute image extrema
%--

switch type

	case {1, 'peaks'}
		E = X == morph_dilate(X, SE);
		
		if nargout > 1
			count = sum(E(:));
		end
		
	case {0, 'valleys'}
		E = X == morph_erode(X, SE);
		
		if nargout > 1
			count = sum(E(:));
		end
		
	case {-1, 'all', 'both'}
		[L, U] = morph_min_max(X, SE); E = (X == L) - (X == U);
		
		if nargout > 1
			count = [-sum(E(:) == -1), sum(E(:) == 1)];
		end
		
	otherwise
		error('Unrecognized computation type.');
end

%--
% remove boundary if requested
%--

if ~opt.boundary
	E([1, end], :) = 0; E(:, [1, end]) = 0;
end


