function Z = mask_sparse(m,p)

% mask_sparse - create sparse mask
% --------------------------------
%
% Z = mask_sparse(m,p)
%
% Input:
% ------
%  m - size of mask
%  p - density of mask
%
% Output:
% -------
%  Z - mask image

%--
% create mask by thresholding random array
%--

Z = (rand(m) < p);
