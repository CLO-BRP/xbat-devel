function [Z,r,c] = mask_separable(m,r,c)

% mask_separable - create separable index masks
% ---------------------------------------------
%
% [Z,r,c] = mask_separable(m,r,c)
%         = mask_separable(m,r)
%
% Input:
% ------
%  m - size of mask
%  r - indices for rows or negative jump size
%  c - indices for columns or negative jump size (def: r)
%
% Output:
% -------
%  Z - separable index mask
%  r - indices for rows 
%  c - indices for columns

%--
% set column indexing
%--

if (nargin < 3)
	c = r;
end

%--
% create regular indexing from negative jumps
%--

if (r(1) < 0)
	r = 1:-r(1):m(1);
end

if (c(1) < 0)
	c = 1:-c(1):m(2);
end

%--
% index shift for mex
%--

r = r - 1;
c = c - 1;

%--
% compute mask
%--

Z = mask_separable_(m,r,c);
