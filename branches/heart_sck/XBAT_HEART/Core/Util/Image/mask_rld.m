function Z = mask_rld(r)

% mask_rld - decode mask run length code
% --------------------------------------
%
% Z = mask_rld(r)
%
% Input:
% ------
%  r - run length code for mask
%
% Output:
% -------
%  Z - mask image

%--
% allocate mask
%--

Z = -ones(r(1),r(2));

%--
% fill mask using code
%--

i1 = 1;
s = r(3);

for k = 4:length(r)
	i2 = i1 + r(k) - 1;
	Z(i1:i2) = s;
	i1 = i2 + 1;
	s = ~s;
end
