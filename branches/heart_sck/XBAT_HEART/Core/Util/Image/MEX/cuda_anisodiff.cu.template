#include <cuda.h>

#include "cuda_image_pad.h"

#define lorentz(x, scale) (2 * x) / (2 * scale * scale + x * x)
#define tukey(x, scale)   (fabs(x) > scale) ? 0 : x * (1 - (x / scale) * (x / scale)) * (1 - (x / scale) * (x / scale))
#define huber(x, scale)   (x >= scale) ? 1.0 : (x <= -scale) ? -1.0 : (x / scale)

// Tile local to this thread-block of pixels, including apron
extern __shared__ CUDA_TYPE tile_CUDA_TYPE_NAME[];

__global__ void anisodiff_CUDA_TYPE_NAME(CUDA_TYPE *Y, CUDA_TYPE *X, int M, int N, double scale, double step, int type)
{
    // Apron is zero pixels
    unsigned int rM = 1;
    unsigned int rN = 1;
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);

    // ------------------------------
    // Fill shared memory pixel tiles
    // ------------------------------
                
    // Fill one row per thread for first blockDim.x + rM2 threads

    unsigned int bM = blockDim.x * blockIdx.x;          // Block index M
	unsigned int bN = blockDim.y * blockIdx.y;          // Block index N
    unsigned int ti = threadIdx.y * blockDim.x + threadIdx.x;
    
    // Boundaries for east and south edge tiles
    int mLimit = min(blockDim.x, M - blockDim.x * blockIdx.x) + rM2;
    int nLimit = min(blockDim.y, N - blockDim.y * blockIdx.y) + rN2;
    
    if (ti < mLimit)
    {
        CUDA_TYPE *t1 = tile_CUDA_TYPE_NAME + ti;
        CUDA_TYPE *d = X + bN * (M + rM2) + bM + ti;
        
        #pragma unroll 8
        for (int l = 0; l < nLimit; l++)
        {
            *t1 = *d;
            t1 += blockDim.x + rM2;
            d += M + rM2;
        }
    }

    // Synchronize tile load for thread block
    
    syncthreads();
    
    // -----------------------------------
    // Compute results for this pixel block
    // -----------------------------------
    
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int index = (j + rN) * (M + rM2) + i + rM;
    int tindex = (threadIdx.y + rN) * (blockDim.x + rM2) + threadIdx.x + rM;
        
    double cN, cS, cW, cE;
    
    if (i >= M || j >= N) { return; }
        
    int tindexN = tindex - 1;
    int tindexS = tindex + 1;
    int tindexW = tindex - (blockDim.x + rM2);
    int tindexE = tindex + (blockDim.x + rM2);        
        
    double deltaN = (i>0)   ? tile_CUDA_TYPE_NAME[tindexN]-tile_CUDA_TYPE_NAME[tindex] : 0;
    double deltaS = (i<M-1) ? tile_CUDA_TYPE_NAME[tindexS]-tile_CUDA_TYPE_NAME[tindex] : 0;    
    double deltaW = (j>0)   ? tile_CUDA_TYPE_NAME[tindexW]-tile_CUDA_TYPE_NAME[tindex] : 0;    
    double deltaE = (j<N-1) ? tile_CUDA_TYPE_NAME[tindexE]-tile_CUDA_TYPE_NAME[tindex] : 0;   

    if (type == 0) // Linear
    {
        cN = deltaN;
        cS = deltaS;
        cW = deltaW;
        cE = deltaE;        
    }
    else if (type == 1) // Lorentz
    {
        cN = lorentz(deltaN, scale);
        cS = lorentz(deltaS, scale);
        cW = lorentz(deltaW, scale);
        cE = lorentz(deltaE, scale);
    }
    else if (type == 2) // Tukey
    {
        cN = tukey(deltaN, scale);
        cS = tukey(deltaS, scale);
        cW = tukey(deltaW, scale);
        cE = tukey(deltaE, scale);
    }
    else if (type == 3) // Huber
    {
        cN = huber(deltaN, scale);
        cS = huber(deltaS, scale);
        cW = huber(deltaW, scale);
        cE = huber(deltaE, scale);
    }
        
    Y[index] = tile_CUDA_TYPE_NAME[tindex] + step*(cN + cS + cW + cE);      
}

// ---------------------------------------------------------
// cuda_anisodiff
// ---------------------------------------------------------

extern "C" cudaError_t cuda_anisodiff_CUDA_TYPE_NAME (CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, double kappa, double lambda, unsigned int type, unsigned int iter);

cudaError_t cuda_anisodiff_CUDA_TYPE_NAME (
		CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, double kappa, double lambda, unsigned int type, unsigned int iter
)

{
    // Keep track of CUDA status
    
    cudaError_t cudaErr = cudaSuccess;
    
	// Dimensions
    
    unsigned int rM = 1;
    unsigned int rN = 1;
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);
    
    // Compute how many pixels + apron can we fit in availabe shared memory
    //    Shared memory is 16K, we reserve 512 bytes for local variables
    
    double bytes = (double) sizeof(CUDA_TYPE);
    double a = bytes;
    double b = bytes * (rM2 + rN2);
    double c = bytes * (rM2 * rN2) - (16384.0 - 512.0);
    int tpb = (-b + sqrt(b*b - 4.0*a*c)) / (2.0 * a);
    
    // Threads per block
    // There must be at least 1, CUDA docs suggest at least 32, max is 512
    
    if (tpb > 16) { tpb = 16; }
    if (tpb < 1)  { return cudaErrorLaunchOutOfResources; }
    
    dim3 threadsPerBlock(tpb, tpb);

    // Block dimensions for processing the image
    dim3 numBlocks;
    
    // Shared memory tile size
	unsigned int shMemSize;
    
	// Grid size for median filtering
	numBlocks.x = (M + threadsPerBlock.x - 1) / threadsPerBlock.x;
	numBlocks.y = (N + threadsPerBlock.y - 1) / threadsPerBlock.y;

    // Size of shared memory tile buffer
	shMemSize = (threadsPerBlock.x + rM2) * (threadsPerBlock.y + rN2) * sizeof(CUDA_TYPE);
    
    // Global memory buffer for image with apron
	CUDA_TYPE *in, *out, *temp;
    
    // Allocate padded image
    if (cudaErr == cudaSuccess)
    {
        cudaErr = cudaMalloc((void **)&in, (M + rM2) * (N + rN2) * sizeof(CUDA_TYPE));
    }
    if (cudaErr == cudaSuccess)
    {
        cudaErr = cudaMalloc((void **)&out, (M + rM2) * (N + rN2) * sizeof(CUDA_TYPE));
    }
    
	// Copy image into center of padded image
    if (cudaErr == cudaSuccess)
    {
        image_pad_center_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(in, X, M, N, rM, rN);
        cudaErr = cudaGetLastError();
    }
	// Iterate filter swapping buffers and recomputing padding as needed
    if (cudaErr == cudaSuccess)
    {
        for (int n = 0; n < iter; n++)
        {
            // Pad image (in place)
            cuda_image_pad_only_CUDA_TYPE_NAME(in, M, N, rM, rN);
    
            // Compute the filter
            anisodiff_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock, shMemSize>>>(out, in, M, N, kappa, lambda, type);
            cudaErr = cudaGetLastError();
            
            // Swap input and output buffers
            if (n < iter - 1)
            {
                temp = in;
                in = out;
                out = temp;
            }
        }
	}

    // Copy data out of center of padded image into result
    if (cudaErr == cudaSuccess)
    {
        image_unpad_center_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(Y, out, M, N, rM, rN);
        cudaErr = cudaGetLastError();
    }
    
    // Deallocate padded image buffers
    cudaFree(in);
	cudaFree(out);
    
    return cudaErr;
}
