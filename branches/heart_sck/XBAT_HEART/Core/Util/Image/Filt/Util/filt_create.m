function F = filt_create(varargin)

% filt_create - create filter structure
% -------------------------------------
%
%  F = filt_create
%
% Output:
% -------
%  F - filter structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE FILTER STRUCTURE
%---------------------------------------------------------------------

persistent FILTER_PERSISTENT;

if isempty(FILTER_PERSISTENT)
	
	%--------------------------------
	% DATA FIELDS
	%--------------------------------
	
	F.H = []; % filter mask
	
	F.S = []; % singular values
	
	F.X = []; % horizontal filters
	
	F.Y = []; % vertical filters
	
	F.c = []; % normalization constant
	
	F.t = []; % mean translation
	
	%--------------------------------
	% APPROXIMATION FIELDS
	%--------------------------------
		
	F.tol = []; % rank computation tolerance
	
	F.rank = []; % numerical rank of filter matrix

	F.normalize = 1; % norm to use in normalization

	F.zeromean = 0; % zero mean indicator flag
		
	%--------------------------------
	% METADATA FIELDS
	%--------------------------------
	
	F.error = []; % error for different rank approximations
	
	F.speed = []; % computational speed up from approximation
			
	%--------------------------------
	% USERDATA FIELD
	%--------------------------------
	
	F.userdata = []; % userdata field is not used by system
	
	%--
	% set persistent filter
	%--
	
	FILTER_PERSISTENT = F;
	
else
	
	%--
	% copy persistent filter and update creation date
	%--
	
	F = FILTER_PERSISTENT;
		
end


