function F = gabor_bank(sigma, theta, omega)

% gabor_bank - of filters
% -----------------------
%
% F = gabor_bank(sigma, theta, omega)
%
% Input:
% ------
%  sigma - scale
%  theta - angle
%  omega - frequency
%
% Output:
% -------
%  F - filter bank
%
% See also: filt_gabor

%--
% handle input
%--

if nargin < 3
	omega = [0.25, 0.5, 1]; 
end 

if nargin < 2
	theta = [0, 30, 45, 60, 90];
end

if ~nargin
	sigma = 3;
end

%--
% build bank 
%--

for j = 1:numel(omega)
	for k = 1:numel(theta)
		% NOTE: perhaps the filt_functions could provide the remaining info as second output
		
		current.filter = filt_gabor(sigma(1), theta(k), omega(j));
		
		% NOTE: these fields should help later layers that use a filter-bank computation
		
		current.type = 'gabor';
		
		current.parameter = struct('sigma', sigma(1), 'theta', theta(k), 'omega', omega(j));
		
		% NOTE: if we are interested in object-like filters, we should reconsider the signature
		
% 		current.generator = @filt_gabor;
		
		% NOTE: cells would allow for heterogenous packing, but it makes some operations cumbersome
		
		F(j, k) = current; %#ok<AGROW>
	end 
end 

%--
% display if output not captured
%--

if ~nargout
	% TODO: factor as display bank
	
	par = fig; layout = layout_create(numel(omega), numel(theta));
	
	harray(par, layout); handle = harray_select(par, 'level', 1);
	
	handle = reshape(handle, numel(omega), numel(theta));
	
	for j = 1:numel(omega)
		for k = 1:numel(theta)
			imagesc(F(j, k).filter, 'parent', handle(j, k)); 
		end
	end
	
	for j = 1:numel(omega)
		ylabel(handle(j, 1), num2str(omega(j)));
	end
	
	for k = 1:numel(theta)
		xlabel(handle(end, k), num2str(theta(k)));
	end
	
	axis(handle(:), 'image');
	
	set(handle, 'xtick', [], 'ytick', []);
	
	colormap(handle(1), gray);
	
	clear F;
end

