function [result, info] = bench_cuda_image_min(varargin)

% bench_cuda_image_min - benchmark cuda image pad against Matlab implementation
% -------------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'exhaustive' - run extensive benchmark instead of fast benchmark
%  'display' - display CUDA padded image for each test
%  'verbose' - print status output
%  'silent' - print nothing
%  anything else - assumed to be type conversion function handle ie. 'double' 
%
% Output
% ------
%  result = 1 if passed, 0 if failed

% NOTE: default is fast bench on double precision data with no messages
% and no display, equivalent to
% 'test_cuda_image_min('quick', 'silent', 'double')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0; display = 0;
type_func = @double;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case 'exhaustive', fast = 0;
            
        case 'silent', verbose = 0;
            
        case 'verbose', verbose = 1;
            
        case 'display', display = 1;
            
        otherwise, type_func = str2func(varargin{i});
    end
end

%----------------------
% SETUP
%----------------------

%--
% load test data
%--

% NOTE: we load data and cast to type we are testing

load clown; % NOTE: this creates the 'X' variable below

X = [X, X; X, X]; X = [X; X]; %#ok<NODEF>

db_disp size-of-image; size(X)

X = type_func(X);

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if isa(X, 'double') && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end
%--
% set parameter ranges
%--

% Padding sizes

if fast
    p = 1:4:9; q = p;  % Arbitrary values for 'quick' test

	iter = 1;
else
    p = 1:2:19; q = p;
	
    iter = 1:2:10;
end

%----------------------
% RUN TEST
%----------------------

%--
% Compare CUDA image pad to Matlab implementation over range of parameters
%--

if display
	par = fig;
end

for i = 1:numel(p)
    
    for j = 1:numel(q)
        
        for k = 1:numel(iter)
        
			if verbose
				fprintf('p %d/q %d/i %d', p(i), q(j), iter(k));
			end

			tic; [i0, e0] = cuda_image_min(X, ones(p(i),1), ones(q(j),1), iter(k)); cuda_time(i, j) = toc;

			if e0
				fprintf('CUDA Error: %d:%s\n', num2str(e0), cuda_decode_error(e0));
				result = 0;
			end

			if display
				figure(par);
				
				image_view(double(i0));
			end

			% Matlab image pad

			tic; i1 = morph_erode(X, ones(p(i),q(j)), iter(k)); ml_time(i, j) = toc;

			% Report speedup
			if verbose
				fprintf('  %1.2f\n', ml_time(i,j) / cuda_time(i,j));
			end
        
        end % for k
        
    end % for q
    
end % for p

result.cuda_time = cuda_time;

result.ml_time = ml_time;

fig; 

db_disp; 

speed = ml_time ./ cuda_time;

speed = alpha_trim(speed);

hist(speed(:), 25);






%----------------------------
% ALPHA_TRIM
%----------------------------

function value = alpha_trim(value, alpha)

if nargin < 2
	alpha = 0.05;
end

N = numel(value); n = round(alpha * N);

value = sort(value); value = value((n + 1):end - n);


