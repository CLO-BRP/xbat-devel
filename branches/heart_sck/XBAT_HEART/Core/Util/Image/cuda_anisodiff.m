function Y = cuda_anisodiff(X, type, scale, n)

% cuda_anisodiff - implementation
% -------------------------------
% 
% Y = cuda_anisodiff(X, type, scale, n)
% 
% Input:
% ------
%  X - input image
%  type - diffusion function type
%         'linear', 'lorentz', 'tukey', or 'huber'
%         default 'tukey'
%  scale - default 20.0
%  n - number of iterations, default 50
%
% Output:
% -------
%  Y - diffused image

%--
% handle input
%--

if nargin < 4
    n = 50;
end

if nargin < 3
    scale = 20.0;
end

if nargin < 2
    type = 'tukey';
end

step = 0.25;

%--
% convert string type to code
%--

switch lower(type)
	
	case 'linear'
		code = 0;
		
	case 'lorentz'
		code = 1;
		
	case 'tukey'
		code = 2;
		
	case 'huber'
		code = 3;
		
	otherwise
		error(['Unrecognized diffusion type ''' type '''.']);
end

%--
% process gray and color images
%--

switch ndims(X)

	case 2	
		[Y, status] = cuda_anisodiff_mex(X, scale, step, code, n);
				
		handle_cuda_failure(status)
        
	case 3
		Y = X;
		
		for k = 1:3
			[Y(:, :, k), status] = cuda_anisodiff_mex(X(:, :, k), scale, step, code, n);
						
			handle_cuda_failure(status)
		end
end
