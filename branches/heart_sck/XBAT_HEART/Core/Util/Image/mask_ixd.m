function Z = mask_ixd(ix)

% mask_ixd - decode mask index code
% ---------------------------------
%
% Z = mask_ixd(ix)
%
% Input:
% ------
%  ix - index code for mask
%
% Output:
% -------
%  Z - mask image

%--
% allocate mask
%--

m = ix(1,1);
n = ix(1,2);

Z = zeros(m,n);

%--
% fill mask using code
%--

j = ix(2:end,1) + m*(ix(2:end,2) - 1);

Z(j) = 1;
