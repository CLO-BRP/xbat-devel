function Y = box_filter(X, F, b, opt)

% box_filter - linear box filtering
% ---------------------------------
% 
% Y = box_filter(X, F, b, opt)
%   = box_filter(X, F, b, opt)
%
% Input:
% ------
%  X - input image
%  F - box filter
%  b - boundary behavior (def: -1, look at 'image_pad')
%  opt - pad option (def: 1)
%
% Output:
% -------
%  Y - box filtered image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-08-29 14:04:35 -0400 (Tue, 29 Aug 2006) $
% $Revision: 6339 $
%--------------------------------

% TODO: generalize function to handle multiple boxes

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% set default padding
%--

if (nargin < 4) || isempty(opt)
	opt = 1;
end 

%--
% set boundary behavior
%--

if nargin < 3
	b = -1;
end

%--
% ensure double image
%--

X = double(X); 

%--------------------------------------
% COMPUTE
%--------------------------------------

% NOTE: at the moment we can handle multiple plane images or multiple filters, but not both

%--
% handle multiple plane images
%--

if (ndims(X) > 2)
	
	[r, c, d] = size(X);
	
	for k = 1:d
		Y(:, :, k) = box_filter(X(:, :, k), F, b, opt);
	end	
	
%--
% scalar image
%--

else
		
	%--
	% create equivalent box filter
	%--
	
	% NOTE: any filter value information is discarded, the support is used
	
	[F, m, n] = get_box_filter(F);
	
	%--
	% pad image if needed
	%--
	
	% NOTE: the padding here is a function of the size of the filter
	
	if opt
		X = image_pad(X, [(max(m) - 1), (max(n) - 1)] ./ 2, b);
	end
	
	%--
	% integrate and use sparse linear filter
	%--
	
	% NOTE: the image padding here is an artifact of the integral image computation
	
	Y = zeros(size(X) + 2); Y(2:end - 1, 2:end - 1) = X;
		
	Y = integral_image_(Y);
	
	if ~iscell(F)
		
		Y = linear_filter_sparse_(Y, F);
	
	else
		
		for k = 1:numel(F)
			Y{k} = linear_filter_sparse(Y, F{k}); 
		end
		
	end
	
end


%----------------------------
% GET_BOX_FILTER
%----------------------------

function [F, m, n] = get_box_filter(B)

%--
% handle multiple filters
%--

if iscell(B)

	for k = 1:numel(B)
		[F{k}, m(k), n(k)] = get_box_filter(B{k});
	end
	
	return;
	
end 

%--
% get box equivalent integral image sparse filter
%--

% NOTE: any filter value information is discarded, the support is used

[m, n] = size(B);

F = zeros(m + 2, n + 2);

F(1, 1) = 1; F(m + 1, 1) = -1; F(1, n + 1) = -1; F(m + 1, n + 1) = 1;


