function C = brightness_contrast_map(fun, brightness, contrast, invert, n)

% brightness_contrast_map - brightness and contrast controlled colormap
% ---------------------------------------------------------------------
%
% C = brightness_contrast_map(fun, brightness, contrast, invert, resolution)
%
% Input:
% ------
%  fun - colormap function, takes as input resolution
%  brightness - value
%  contrast - value
%  invert - indicator
%  resolution - number of colors
% 
% Output:
% -------
%  C - colormap matrix

%-------------------
% HANDLE INPUT
%-------------------

%--
% set number of colors
%--

if (nargin < 5) || isempty(n)
	n = 256;
end

%--
% set invert
%--

if (nargin < 4) || isempty(invert)
	invert = 0;
end

%--
% set brightness and contrast
%--

if (nargin < 3) || isempty(contrast)
	contrast = 0;
end

if (nargin < 2) || isempty(brightness)
	brightness = 0.5;
end

%--
% set fun to control
%--

if (nargin < 1) || isempty(fun)
	fun = @gray;
end

if ischar(fun)
	fun = str2func(fun);
end

%-------------------
% CREATE COLORMAP
%-------------------

switch contrast
	
	case 0
		
		% NOTE: this creates a linear map over the full range for minimal contrast
		
		C = fun(n);

		if invert
			C = flipud(C);
		end
	
	case 1

		% NOTE: this creates a two color map for maximum contrast
		
		C = fun(2);

		if invert
			C = flipud(C);
		end

		n1 = round(brightness * n); n2 = n - n1;

		C = [ones(n1, 1) * C(1, :); ones(n2, 1) * C(2, :)];
		
	otherwise

		% NOTE: we create a 'linear' colormap over limited range for intermediate contrast

		m = round((1 - contrast) * n);
		
		C = fun(m);

		if invert
			C = flipud(C);
		end

		n1 = round(brightness * (n - m)); n2 = (n - m) - n1;

		C = [ones(n1, 1) * C(1, :); C; ones(n2, 1) * C(end, :)];
	
end


