function C = cmap_label(n, par)

% cmap_label - colormap for label images
% --------------------------------------
%
% C = cmap_label(n, par)
%
% Input:
% ------
%  n - number of levels
%  h - figure handle
%
% Output:
% -------
%  C - colormap

%---------------------
% HANDLE INPUT
%---------------------

%--
% get number of levels from figure images
%--

if ~nargin || isempty(n)

	if nargin < 2
		par = gcf;
	end

	n = get_number_of_colors(par);
	
	if ~nargout
		set(par, 'colormap', cmap_label(n));
	end

end

%---------------------
% CREATE COLORMAP
%---------------------

% NOTE: maybe use seed input to function to produce consistent results

p = randperm(n);

C = hsv(n);

% NOTE: add zero for the background to the colormap

C = [0 0 0; C(p,:)];


%----------------------------------
% GET_NUMBER_OF_COLORS
%----------------------------------

function n = get_number_of_colors(par)

%--
% set some default value
%--

n = 256;

%--
% get image handles
%--

handles = get_image_handles(par);

if isempty(handles) 
	return;
end

%--
% get range of image data
%--

for k = 1:length(handles)
	b(k, :) = fast_min_max(get(handles(k), 'CData'));
end

b = [min(b(:, 1)), max(b(:, 2))];

%--
% set number of levels
%--

if b(1) > 0
	n = b(2); n = floor(n / 2);
end
