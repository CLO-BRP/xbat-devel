function [T,b] = lut_fun(f,b,N,p)

% lut_fun - create a look up table for a function
% -----------------------------------------------
%
% [T,b] = lut_fun(f,b,N,p)
%
% Input:
% ------
%  f - function name, inline function, or function handle to evaluate
%  b - lower and upper limits for look up
%  N - size of look up table (def: 256)
%  p - function specific parameters
%
% Output:
% -------
%  T - look up table
%  b - lower and upper limits for look up

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-12 23:04:02 -0500 (Sun, 12 Dec 2004) $
% $Revision: 252 $
%--------------------------------

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set default size of table
%--

% NOTE: this table length can be slightly faster for uint8 valued images

if (nargin < 3)
	N = 256;
end

%-----------------------------
% CREATE TABLE
%-----------------------------

%--
% create grid points
%--

x = linspace(b(1),b(2),N);

%--
% compute function on table grid
%--

% NOTE: this function may now take function handles as well

if (nargin > 3)
	T = feval(f,x,p);
else
	T = feval(f,x);
end

