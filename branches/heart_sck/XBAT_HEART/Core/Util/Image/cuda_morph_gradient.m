function Y = cuda_morph_gradient(X,SE,t)

% cuda_morph_gradient - morphological gradient
% --------------------------------------------
%
% Y = cuda_morph_gradient(X,SE,t,Z)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  t - type of gradient
%
%   -1 - inner (dilation)
%    0 - symmetric (dilation - erosion) (default)
%    1 - outer (erosion)
%
% Output:
% -------
%  Y - gradient image

%--
% set type of gradient
%--

if (nargin < 3)
    t = 0;
end

%--
% color image
%--

if (ndims(X) > 2)
    
    [rr,cc,ss] = size(X);
    
    for k = 1:ss
        Y(:,:,k) = cuda_morph_gradient(X(:,:,k),SE,t);
    end
    
    %--
    % scalar image
    %--
    
else
    
    %--
    % structuring element
    %--
    
    B = se_mat(SE);
    
    B = uint8(B);
    m = size(B,1); n = size(B,2);
    
    %--
    % compute gradient using CUDA
    %--
    
    switch (t)
        
        %--
        % inner gradient
        %--
        
        case (-1)
            [Y, status] = cuda_morph_filter_mex(X, B, m, n, 0, 9);
            
            handle_cuda_failure(status)
            
        %--
        % symmetric gradient
        %--
            
        case (0)
            [Y, status] = cuda_morph_filter_mex(X, B, m, n, 0, 8);
            
            handle_cuda_failure(status)
            
        %--
        % outer gradient
        %--
            
        case (1)
            [Y, status] = cuda_morph_filter_mex(X, B, m, n, 0, 10);
            
            handle_cuda_failure(status)
            
    end
    
end