function t = is_stack(X)

% is_stack - check for image stack
% --------------------------------
%
% t = is_stack(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  t - stack indicator

%--
% input is not cell array
%--

if ~iscell(X)
	
	t = 0;
	
%--
% check size of planes
%--

else

	%--
	% get first plane shape
	%--
	
	s = size(X{1});
	d = length(s);
	
	%--
	% produce tentative indicator based on plane shape
	%--
	
	switch (d)
		
	% vector cell array, not stack
	
	case (1)
		t = 0;
		return;
	
	% grayscale image stack
	
	case (2)
		t = 1;
	
	% color image stack
	
	case (3)
		t = 2;
	
	% multiple plane image stack
	
	otherwise
		t = 3;
		
	end
	
	%--
	% compare plane shapes
	%--
	
	for k = 2:length(X)
		
		sk = size(X{k});
		dk = length(sk);
		
		if ((d ~= dk) | any(s ~= sk))
			t = 0;
			break;
		end
		
	end

end