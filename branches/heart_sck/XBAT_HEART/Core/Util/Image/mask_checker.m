function Z = mask_checker(m,t)

% mask_checker - create checkerboard masks
% ----------------------------------------
%
% Z = mask_checker(m,t)
%
% Input:
% ------
%  m - size of mask
%  t - parity of row and column sum, even (0) or odd (1)
%    
% Output:
% -------
%  Z - checkerboard mask

%--
% build mask according to type
%--

switch (t)

	%--
	% even row and column sum 
	%--
	
	case (0)
		Z = mask_parity(m,[0,0]) | mask_parity(m,[1,1]);
	
	%--
	% odd row and column sum
	%--
	
	case (1)
		Z = mask_parity(m,[0,1]) | mask_parity(m,[1,0]);

end	
		
		
