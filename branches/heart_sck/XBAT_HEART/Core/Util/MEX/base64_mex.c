#include "mex.h"

#include "matrix.h"

#include "base64.c"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    int direction, ok; 
	
	unsigned char *in, *out;
	
	size_t inlen, outlen;
	
	//--
	// get input string data
	//--
	
	inlen = mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1;
	
	// NOTE: we should really be using 'mxChar'
	
	in = mxCalloc(inlen, sizeof(char));
	
	mxGetString(prhs[0], in, inlen);
			
	//--
	// encode or decode
	//--
	
	direction = (int) mxGetScalar(prhs[1]);

	switch (direction) {
		
		case 1:

			out = encode_base64(inlen - 1, in);
			
			if (out == NULL)
				mexErrMsgTxt("Memory allocation error.");
			
			break;
			
		case -1:
			
			out = calloc(inlen, sizeof(char));
			
			if (out == NULL)
				mexErrMsgTxt("Memory allocation error.");
			
			decode_base64(out, in);
		
			break;
			
		default :

			mexErrMsgTxt("Mode should be 1 or -1 for 'encode' and 'decode' respectively.");
				
	}

    //--
	// pack output data
	//--
    
    plhs[0] = mxCreateString((const char *) out);
    
}
