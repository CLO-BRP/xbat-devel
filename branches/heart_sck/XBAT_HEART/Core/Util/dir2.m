function info = dir2(file)

% dir2 - get directory file info
% ------------------------------
%
% info = dir2(file)
%
% Input:
% ------
%  file - directory
%
% Output:
% -------
%  info - 'dir' type info for directory, rather than its contents

[parent, leaf] = fileparts(file);

info = dir(parent); ix = find(strcmp(leaf, {info.name}), 1); info = info(ix);

% NOTE: this resolves compatibility with older versions of MATLAB

if ~isfield(info, 'datenum')
	
	info.datenum = datenum(info.date);

end
