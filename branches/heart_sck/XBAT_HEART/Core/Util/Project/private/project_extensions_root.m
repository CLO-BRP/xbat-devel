function root = project_extensions_root(name, author)

% TODO: various other functions should do this

if nargin < 2 && isstruct(name)
	project = name;	
	
	author = project.author; name = project.name;
end

project_exists(name, author);

% NOTE: we ensure the existence of this directory if the project exists

[root, created] = create_dir(fullfile(project_root(name, author), 'Extensions'));