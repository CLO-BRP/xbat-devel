function fun = generate_project_function(project)

% generate_project_function - from project struct
% -----------------------------------------------
%
% fun = generate_project_function(project)
%
% Input:
% ------
%  project - struct
%
% Output:
% -------
%  fun - name
%
% NOTE: the project function is a type of manifest

%--
% get project function file
%--

file = project_function_file(project.name, project.author);

fun = project_function(project.name, project.author);

%--
% write file 
%--

field = fieldnames(project); fid = fopen(file, 'w');

% NOTE: we catch to not leave the file open

try
	fprintf(fid, 'function project = %s\n\n', strtok(fun, '.'));

	for k = 1:numel(field)
		% NOTE: we skip the self-referential function handle included in the project struct
		
		if strcmp(field{k}, 'fun')
			continue;
		end
		
		fprintf(fid, 'project.%s = ''%s'';\n\n', field{k}, project.(field{k}));
	end
catch
	nice_catch(lasterror, 'Failed to generate project function.');

	fclose(fid);
end

fclose(fid);

%--
% clear is required so we load the recently generated function
%--

clear(fun);

