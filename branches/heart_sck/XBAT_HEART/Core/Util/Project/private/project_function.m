function [name, fun] = project_function(name, author)

% NOTE: the function name is not a sufficient representation

name = [genvarname(name), '__project.m'];

fun = choose_fun(name(1:end - 2), author);