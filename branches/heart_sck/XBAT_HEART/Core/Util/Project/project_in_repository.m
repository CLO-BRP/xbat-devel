function [value, info] = project_in_repository(name, author)

if nargin < 2 && isstruct(name)
	project = name;
	
	name = project.name; author = project.author;
end

project_exists(name, author);

[value, info] = is_working_copy(project_root(name, author));

if ~nargout
	disp(info); clear; return;
end