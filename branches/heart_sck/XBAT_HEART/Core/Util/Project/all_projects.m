function project = all_projects(varargin)

% all_projects - get all currently installed projects
% ---------------------------------------------------
%
% project = all_projects
%
% Output:
% -------
%  project - list

%--
% check projects directory contents
%--

% NOTE: the second argument indicates we only get directory contents of root

author = no_dot_dir(projects_root, true);

%--
% load projects
%--

project = empty(project_create);

for k = 1:numel(author) 
	
	content = no_dot_dir(fullfile(projects_root, author(k).name), true);
	
	for j = 1:numel(content)
		% NOTE: here we see the directory organization convention
		
		try
			current = get_project(content(j).name, author(k).name);
			
			project(end + 1) = current; %#ok<AGROW>
		catch
% 			nice_catch(lasterror, ['Failed to load project ''', content(k).name, ''' by ''', author(k).name, '''.']);
		end
	end
	
end

%--
% filter projects by field
%--

project = filter_struct(project, varargin{:});

%--
% display projects
%--

if ~nargout && ~isempty(project)
	
	disp(project); clear project;
	
end 