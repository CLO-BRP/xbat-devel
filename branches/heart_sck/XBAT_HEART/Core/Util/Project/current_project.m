function project = current_project(project) 

% current_project - set or get current project
% --------------------------------------------
%
% project = current_project(project) 
%
% Input:
% ------
%  project - to set
%
% Output:
% -------
%  project - active

%--
% set or get based on input
%--

if ~nargin
	project = get_env('current_project');
else 
	project = set_env('current_project', project);
end

% NOTE: there is nothing more to do if there is no current project

if isempty(project)
	return;
end 

%--
% check for repository consistence
%--

% NOTE: consider factoring this 

[versioned, info] = project_in_repository(project);

if versioned && isempty(project.repository)
	
	%--
	% set project repository
	%--
	
	start = strfind(info.url, 'XBAT_PROJECTS');
	
	if isempty(start)
		error('Unrecognized repository location for project.');
	end
	
	project.repository = info.url(1:(start - 2));
	
	%--
	% regenerate function and update project if needed
	%--

	generate_project_function(project);
	
	if nargin
		set_env('current_project', project);
	end
	
end
	