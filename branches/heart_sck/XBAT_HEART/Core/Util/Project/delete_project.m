function delete_project(project)

%--
% handle input
%--

if isstruct(project)
	project = project.name;
end

if ~project_exists(project)
	return;
end

%--
% delete project
%--

root = project_root(project);

% TODO: make interaction obvious, indicate deletion or inaction

% TODO: create backup before deleting

% TODO: create 'delete_project_from_repository'

if ~project_in_repository(project)
	
	response = input(['\nProject ''', project, ''' is not in any repository, remove it? [yN] '], 's');

	disp(' ');
	
	if ~isequal(lower(response), 'y')
		return;
	end
	
end

remove_path(root); rmdir(root, 's');