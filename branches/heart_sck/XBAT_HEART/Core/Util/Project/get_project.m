function project = get_project(name, author)

% get_project - get project by name and author
% --------------------------------------------
%
% project = get_project(name, author)
%
% Input:
% ------
%  name - of project
%  author - of project
%
% Output:
% -------
%  project - description

%--
% handle input
%--

if nargin < 2
	name = cellfree(name); % NOTE: this should not be necessary
	
	if ischar(name)
		project = all_projects('name', name);
	else
		project = name;
	end
	
	author = project.author; name = project.name;
end

%--
% assert project exists
%--

% TODO: this function fails in a rather mysterious way when the project root is not on the path, improve this 

project_exists(name, author);

%--
% call project function to get project
%--

[ignore, fun] = project_function(name, author);

% TODO: we must assign project output when this condition fails
	
try
	project = fun(); project.fun = fun;
catch
	nice_catch(lasterror, 'Failed to load project.');
end