
#define	ARRAY_LEN(x) ((int) (sizeof (x) / sizeof ((x) [0])))

/*
 * get spline coefficients from file
 */

static double const spline_coeffs [] = 
{
#include "fast_spline_coefs.h"
};

/*
 * spline_lut - look up value of spline on interval [-2.0, 2.0]
 */

double spline_lut(double t)
{
    
    double ix, val, frac;
    
    /*
     * return zeros outside the range [-2 2]
     */
    
    if (t > 2.0 || t < -2.0){
        return 0.0;
    }
    
    /*
     * look up value using linear interpolation
     */  
    
    ix = ((t + 2.0) / 4.0) * (double) (ARRAY_LEN(spline_coeffs) - 1);
    
    frac = ix - (double)(int)ix;
    
    val = (1.0 - frac) * spline_coeffs[(int)ix] + frac * spline_coeffs[(int)ix + 1];
    
    return val;
    
}

/*
 * fast_spline_eval - evaluate the spline sequence b on the time points t
 */

int fast_spline_eval(double * s, double * t, int nT, double * b, int nB)
{
    
    int j, k, ix;
    
    double coeff;
    
    for (j = 0; j < nT; j++) {
        
        s[j] = 0.0; ix = (int)t[j];
        
        /*
         * combine spline values with surrounding coefficients
         */
        
        for (k = ix - 1; k <= ix + 2; k++) {
         
            /*
             * reflective boundary conditions
             */
            
            if (k >= nB){
                coeff = b[2*nB - k - 2];
            } 
            
            else if (k < 0){
                coeff = b[-k];
            }
            
            else{
                coeff = b[k];
            }
            
//             coeff = ((k < nB) && (k >= 0)) ? b[k] : 0.0; 
            
            s[j] += coeff * spline_lut(t[j] - (double) k);
            
        } 
        
    }
    
    return nT;
}
