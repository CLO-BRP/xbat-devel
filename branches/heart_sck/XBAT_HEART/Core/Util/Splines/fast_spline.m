function b = fast_spline(x, precision)

x = x(:);

if nargin < 2 || isempty(precision)
	precision = min(length(x), 10);
end

precision = min(precision, length(x));

b = fast_spline_mex(x, precision);
