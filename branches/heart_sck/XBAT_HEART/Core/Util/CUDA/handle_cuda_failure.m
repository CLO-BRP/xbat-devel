function handle_cuda_failure(status)

if status
	error(cuda_decode_error(status));
end