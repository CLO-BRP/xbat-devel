function out = iterate_struct(value, fun, err)

% iterate_struct - iterate over fields of struct and struct array
% ---------------------------------------------------------------
%
% out = iterate_struct(value, fun)
%
% Input:
% ------
%  value - struct array
%  fun - callback function
%
% Output:
% -------
%  out - result of calling function on every struct field

% TODO: consider adding options to flatten or not for example

%--
% handle input
%--

% NOTE: consider a non-silent catch default

% NOTE: the error handler takes 'lasterror' as first input

if nargin < 3
	err = [];
end 

% NOTE: use iterate to iterate over struct array

if numel(value) > 1
	out = iterate(mfilename, value, fun); return;
end

%--
% iterate over fields of struct
%--

value = flatten(value); field = fieldnames(value);

out = struct;

for k = 1:numel(field)
	
	% TODO: consider implementing error handler in iterate as well, that

	try
		out.(field{k}) = eval_callback(fun, value.(field{k}));
	catch
		if isempty(err)
			continue;
		else
			eval_callback(err, lasterror);
		end
	end
	
end

out = collapse(out);

