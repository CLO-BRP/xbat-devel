function y = vario_gauss(h,c0,c1,a)

% gaussian - gaussian variogram model
% -----------------------------------
%
% y = vario_gauss(h,c0,c1,a)
%   = vario_gauss(h,p)
%
% Input:
% ------
%  h - points of evaluation
%  c0,c1,a - model parameters
%  p - structure packed parameters (p.c0,p.c1,p.a)
%
% Output:
% -------
%  g - variogram values

%--
% unpack parameters
%--

if (nargin < 3)
	c1 = c0.c1;
	a = c0.a;
	c0 = c0.c0;
end

%--
% compute function values
%--

y = c0 + (c1 * (1 - exp(-h ./ a).^2));
