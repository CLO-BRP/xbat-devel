function h = graph_view(G, H)

% graph_view - display graph
% --------------------------
%
% h = graph_view(G, H)
%
% Input:
% ------
%  G, H - graphs
%
% Output:
% -------
%  h - handles to scattered vertices
%
 
%--
% display spatial graph
%--

	%--
	% scatter plot of vertices
	%--
	
	if nargin > 1
		h = scatter(G.X(:, 1), G.X(:, 2), 150, 'o');
	else
		h = scatter(G.X(:, 1), G.X(:, 2), 100, 'o', 'filled');
	end
	
	hold on;
	
	%--
	% plot edges
	%--
	
	gplot(edge_to_sparse(G.E), G.X, 'k:');
	
%--
% display non-spatial graph
%--

if nargin > 1
	
	%--
	% color vertices using scalar attribute
	%--
	
	for k = 1:length(h)
		
		set(h(k), ...
			'MarkerFaceColor', H.V(k) * ones(1, 3), ...
			'MarkerEdgeColor', zeros(1, 3) ...
		);
	
	end
	
	%--
	% plot edges
	%--
	
	gplot(edge_to_sparse(H.E), G.X, 'r');

end

%--
% set axes and figure properties
%--

axis('equal');

set(gca, 'visible', 'off');
% set(gca, 'units', 'normalized', 'position', [0 0 1 1]);

set(gcf, 'color', [0.9 0.9 0.5]);


