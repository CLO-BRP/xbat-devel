function D = dist_euclidean2(X, Y)

% dist_euclidean2 - squared euclidean distance matrix for vectors
% ---------------------------------------------------------------
%
% D = dist_euclidean2(X, Y)
%
% Input:
% ------
%  X - column vectors, row index
%  Y - column vectors, column index
%
% Output:
% ------
%  D - squared distance matrix
%
% See also: dist_diff, dist_euclidean

if nargin < 2
	Y = X;
end

% NOTE: compute distance matrix by collapsing squared differences

D = sum(dist_diff(X, Y).^2, 3);


