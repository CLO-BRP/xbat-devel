function D = fast_distance(X, Y, type)

% fast_distance - computation between vector collections
% ------------------------------------------------------
%
% D = fast_distance(X, Y, type)
%
% Input:
% ------
%  X, Y - column vector collections
%  type - of distance (def: 'euclidean')
%
% Output:
% -------
%  D - distance matrix, row index for X and column index for Y

%--
% handle input
%--

if nargin < 3
	type = 'euclidean';
end

if nargin < 2
	Y = X;
end

type = get_code_from_name(type);

%--
% compute using MEX
%--

D = fast_distance_mex(X, Y, type);


