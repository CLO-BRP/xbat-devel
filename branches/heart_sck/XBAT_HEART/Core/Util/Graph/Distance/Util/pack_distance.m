function d = pack_distance(D)

% pack_distance - matrix as index value vector
% --------------------------------------------
%
% d = pack_distance(D)
%
% Input:
% ------
%  D - distance matrix
%
% Output:
% -------
%  d - distance index, value points
%
% See also: unpack_distance
%
% NOTE: this is mostly a call to find with a little trick to remove the diagonal if needed

if is_sym(D)
	
	for k = 1:size(D, 1)
		D(k, k) = nan;
	end
end

[d(:, 1), d(:, 2), d(:, 3)] = find(D);

% NOTE: these distances are not available, some algorithms use infinite distances

d(isnan(d(:, 3)), :) = [];