function G = circle_graph(A, opt)

%--
% set and possibly output default options
%--

if nargin < 2
	opt.radius = [1, 0.2]; opt.angle = pi * [0.5, -3/2]; opt.circles = 3;
end

if ~nargin
	G = opt; return;
end

%--
% build concentric circles
%--

n = size(A, 1); f = ceil(n / opt.circles); p = zeros(opt.circles, f);
	
for k = 1:opt.circles
	
	p(k, :) = circle(f, opt.radius(1) + opt.radius(2) * k, opt.angle);
	
end

p = p(:); p = p(1:n);

%--
% assemble graph
%--

G.X = [real(p), imag(p)]; [G.E, G.W] = sparse_to_edge(A);