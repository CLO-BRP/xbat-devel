function heap = downheap(heap, k)

%--
% get length of heap
%--

n = size(heap, 2);

if ~n
	return;
end

%--
% keep value checked for swapping convenience
%--

val = heap(:, k);

%--
% while we can go down the heap and the children are larger move down
%--

k_max = bitshift(n, -1);

while k <= k_max
	
	%--
	% look at children and select largest
	%--
	
	j = bitshift(k, 1);
	
	if j < n
		if heap(1, j) < heap(1, j + 1)
			j = j + 1;
		end
	end
		
	%--
	% swap with child and update indices to check next children or end
	%--
	
	if val(1) < heap(1, j)
		heap(:, k) = heap(:, j); heap(:, j) = val; k = j;
	else
		return;
	end
	
end