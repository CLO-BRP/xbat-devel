function heap = upheap(heap, k)

%--
% keep value checked for swapping convenience
%--

val = heap(:, k);

%--
% while selected vertex has value larger than its parent move up
%--

% use sentinel to check for (j == 0)

j = bitshift(k, -1);

while (j > 0) && (heap(1, j) < val(1))
	
	%--
	% swap with parent and update indices to check next parent
	%--
	
	heap(:, k) = heap(:, j); heap(:, j) = val;

	k = j; j = bitshift(k, -1);
	
end