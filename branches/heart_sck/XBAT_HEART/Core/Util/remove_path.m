function dirs = remove_path(varargin)

% remove_path - remove directory or tree from path
% ------------------------------------------------
%
% dirs = remove_path(root, rec)
%
% Input:
% ------
%  root - tree root (def: pwd)
%  rec - follow tree (def: 1)
%
% Output:
% -------
%  dirs - directories removed to path
%
% See also: append_path, update_path

dirs = update_path('remove', varargin{:});
