function [handle, created] = create_menu(par, tag, varargin)

% TODO: check for parent availability, often a problem for menus

[handle, created]  = create_obj('uimenu', par, tag, varargin{:});
