function tags = get_handle_tags(handle)

% get_handle_tags - where multiple tags are conventionally separated
% ------------------------------------------------------------------
%
% tags = get_handle_tags(handle)
%
% Input:
% ------
%  handle - to get tags from
% 
% Output:
% -------
%  tags - used for handle
%
% NOTE: this function returns a cell array of strings not a tag string like the 'get'
%
% See also: find_handles_with_tag

if numel(handle) > 1
	tags = iterate(mfilename, handle); return;
end 

tags = str_split(get(handle, 'tag'), ',');