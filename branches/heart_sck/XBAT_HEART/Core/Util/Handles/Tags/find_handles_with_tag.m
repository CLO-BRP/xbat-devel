function handles = find_handles_with_tag(parent, tag, varargin)

% find_handles_with_tag - where multiple tags are used
% ----------------------------------------------------
%
% handles = find_handles_with_tag(parent, tag)
%
% Input:
% ------
%  parent - handle
%  tag - to seek
%
% Output:
% -------
%  handles - found
%
% See also: get_handle_tags

%--
% find candidate handles using input
%--

% NOTE: we accept a collection of candidates or a parent as handle input, currently undocumented

if numel(parent) > 1
	candidate = parent;
else
	candidate = findobj(parent, varargin{:});
end

%--
% get tag strings and search for tag
%--

tags = get(candidate, 'tag');

if ischar(tags)
	tags = {tags};
end

handles = zeros(size(candidate)); count = 0;

for k = 1:numel(tags)
	
	% NOTE: first a quick check then the complete check
	
	% NOTE: the second clause is not DRY with respect to 'get_handle_tags'
	
	if isempty(strfind(tags{k}, tag)) && ~string_is_member(tag, str_split(tags{k}, ','))
		continue;
	end 

	count = count + 1; handles(count) = candidate(k);
end

if count
	handles = handles(1:count);
else
	handles = [];
end
