function add_handle_tag(handle, tag)

if iscell(tag)
	tag = str_implode(tag, ', ');
end

for k = 1:numel(handle)
	
	current = get(handle(k), 'tag');
	
	if isempty(current)
		set(handle(k), 'tag', tag);
	else
		set(handle(k), 'tag', [current, ', ', tag]);
	end 
	
end