function prefix_handle_tags(handle, prefix)

% prefix_handle_tags - for findability
% ------------------------------------
%
% prefix_handle_tags(handle, prefix)
%
% Input:
% ------
%  handle - array
%  prefix - for tags
%
% See also: find_prefixed_handles

if isempty(prefix)
	return;
end

for k = 1:numel(handle)
	set(handle(k), 'tag', [prefix, get(handle(k), 'tag')]);
end