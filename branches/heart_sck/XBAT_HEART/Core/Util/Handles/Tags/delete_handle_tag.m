function delete_handle_tag(handle, tag)

if numel(handle) > 1
	iterate(mfilename, handle, tag); return;
end

tags = get_handle_tags(handle); 

if isempty(tags)
	return;
end

[found, ix] = string_is_member(tag, tags);

if ~found
	return; 
end

tags(ix) = []; set_handle_tags(handle, tags);