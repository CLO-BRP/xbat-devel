function rotate_handle(handle, center, angle, unit)

% rotate_handle - by specified angle around a center
% --------------------------------------------------
%
% rotate_handle(handle, angle, center)
%
% Input:
% ------
%  handle - to rotate
%  center - of rotation (def: origin)
%  angle - of rotation
%  unit - for anble (def: degree)

%--
% handle input
%--

if nargin < 4
	unit = 'degree';
end

if nargin < 3 || isempty(center)
	center = [0, 0];
end

if nargin < 2 || isempty(angle) || ~angle
	return;
end

if numel(handle) > 1
	% TODO: consider unique handle constrain to avoid equivocal multiple rotation
	
	for k = 1:numel(handle)
		rotate_handle(handle(k), center, angle);
	end
	
	return;
end

%--
% rotate handle considering center
%--

switch unit
	case 'degree'
		phase = exp(-pi * 1i * angle / 180);
		
	case 'radian'
		phase = exp(-pi * 1i * angle);
end

switch get(handle, 'type')
	
	case 'text'
		pos = get(handle, 'position'); pos = pos(1:2) - center;
		
		z = phase * (pos(1) + 1i * pos(2));
		
		set(handle, ...
			'position', [real(z), imag(z)] + center, ...
			'rotation', angle ...
		);
		
	otherwise
		x = get(handle, 'xdata') - center(1); y = get(handle, 'ydata') - center(2);
		
		z = phase * (x + (1i * y));
		
		set(handle, 'xdata', real(z) + center(1), 'ydata', imag(z) + center(2));
end
