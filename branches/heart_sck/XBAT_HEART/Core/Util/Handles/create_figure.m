function [handle, created] = create_figure(tag, varargin)

% create_figure - ensure existence of tagged figure, create if needed
% -------------------------------------------------------------------
%
% [handle, created] = create_figure(tag, varargin)
%
% Input:
% ------
%  tag - for figure
%  varargin - field value pairs
%
% Output:
% -------
%  handle - of figure
%  created - indicator

[handle, created]  = create_obj('figure', 0, tag, varargin);