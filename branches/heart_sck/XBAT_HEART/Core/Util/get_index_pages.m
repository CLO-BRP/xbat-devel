function [page, pages] = get_index_pages(total, part)

%--
% get number of full pages, and some partial page size and indicator
%--

full = floor(total / part); partial = rem(total, part);

%--
% build full pages, and possibly partial page
%--

page.start = 1; page.stop = part;

for k = 2:full
	current.start = (k - 1) * part + 1; current.stop = current.start + part - 1; page(end + 1) = current;
end

if partial
	page(end + 1).start = page(end).stop + 1; page(end).stop = total;
end

%--
% output the page count if asked for 
%--

if nargout > 1
	pages = numel(page);
end