function out = collapse(in, sep)

% collapse - collapse flattened struct
% ------------------------------------
%
% out = collapse(in, sep)
%
% Input:
% ------
%  in - flattened scalar struct
%  sep - field separator (def: '__', double underscore)
%
% Output:
% -------
%  out - collapsed struct

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set separator
%--

if nargin < 2
	sep = '__';
end

%--
% check for scalar struct
%--

if length(in) ~= 1
	error('Scalar struct input is required.');
end

%---------------------------------------
% COLLAPSE STRUCT
%---------------------------------------

% NOTE: the order of flattening and collapsing matters

field = fieldnames(in);

out = parse_collapse(in, field, sep);


%---------------------------------------
% EVAL_COLLAPSE
%---------------------------------------

function out = eval_collapse(in, field, sep) %#ok<STOUT,DEFNU,INUSL>

%--
% use string replacement and evaluation to append collapsed fields
%--

for k = 1:length(field)
	eval(['out.', strrep(field{k}, sep, '.'), ' = in.', field{k}, ';']);
end


%---------------------------------------
% PARSE_COLLAPSE
%---------------------------------------

function out = parse_collapse(in, field, sep)

%--
% initialize and append collapsed fields 
%--

out = struct;

for k = 1:numel(field)
	out = append_field(out, field{k}, in.(field{k}), sep);
end


%---------------------------------------
% APPEND_FIELD
%---------------------------------------

function out = append_field(out, field, value, sep)

%--
% parse flat fieldname into parts
%--

% NOTE: the two following calls are equivalent

% part = strread(strrep(field, sep , ' '), '%s', -1);

part = dataread('string', strrep(field, sep , ' '), '%s', -1);

%--
% append field using dynamic field assigment for sufficiently flat cases,
%--

switch numel(part)

	case 1, out.(part{1}) = value;

	case 2, out.(part{1}).(part{2}) = value;

	case 3, out.(part{1}).(part{2}).(part{3}) = value;

	case 4, out.(part{1}).(part{2}).(part{3}).(part{4}) = value;

	case 5, out.(part{1}).(part{2}).(part{3}).(part{4}).(part{5}) = value;

	case 6, out.(part{1}).(part{2}).(part{3}).(part{4}).(part{5}).(part{6}) = value;
		
	case 7, out.(part{1}).(part{2}).(part{3}).(part{4}).(part{5}).(part{6}).(part{7}) = value;
		
	case 8, out.(part{1}).(part{2}).(part{3}).(part{4}).(part{5}).(part{6}).(part{7}).(part{8}) = value;
		
	% NOTE: we hope that in practice the 'eval' solution is not typically needed
	
	otherwise, eval(['out.', strrep(field, sep, '.'), ' = value;']);
		
end



