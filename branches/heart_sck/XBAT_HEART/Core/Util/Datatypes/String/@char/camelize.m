function str = camelize(str, sep)

if nargin < 2
	sep = '_';
end

str = str_implode(str_split(str, sep), '', @capitalize);