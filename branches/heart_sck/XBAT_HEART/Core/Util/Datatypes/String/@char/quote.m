function str = quote(str, type)

if nargin < 2
	type = '"';
end

% NOTE: it is possible that there are internal double quotes

str = [type, str, type];