function time = dusk(varargin)

% dusk - time on given date and location
% --------------------------------------
%
% time = dusk(lon, lat, cal, zen)
%
% Input:
% ------
%  lon - longitude
%  lat - latitude
%  cal - date as [year, month, day] 
%  zen - zenith or 'official', 'civil', 'nautical', or 'astronomical'
%
% Output:
% -------
%  time - of dusk
%
% See also: dawn, sunrise, sunset, sunbase
%
% NOTE: this is just a convenience function calling 'sunbase'

time = sunbase('dusk', varargin{:});