function time = dawn(varargin)

% dawn - time on given date and location
% --------------------------------------
%
% time = dawn(lon, lat, cal, zen)
%
% Input:
% ------
%  lon - longitude
%  lat - latitude
%  cal - date as [year, month, day] 
%  zen - zenith or 'official', 'civil', 'nautical', or 'astronomical'
%
% Output:
% -------
%  time - of dawn
%
% See also: sunrise, sunset, dusk, sunbase
%
% NOTE: this is just a convenience function calling 'sunbase'

time = sunbase('dawn', varargin{:});