function [session, info] = lastfm_get_session(user, pass)

root = 'http://ws.audioscrobbler.com/radio/handshake.php?version=1.1.1&platform=linux';

url = [root, '&username=', user, '&passwordmd5=', md5(pass), '&debug=0&partner='];

str = urlread(url);

lines = file_readlines(str);

session = lines;


% void
% ScrobblerHandshakeRequest::request()
% {
%     QString timestamp = QString::number( QDateTime::currentDateTime().toTime_t() );
%     QString auth_token = UnicornUtils::md5Digest( (m_init.password + timestamp).toUtf8() );
% 
%     QString query_string = QString() +
%                             "?hs=true" +
%                             "&p=1.2" + //protocol version
%                             "&c=ass" + //AudioScrobbler Service
%                             "&v=" + m_init.client_version +
%                             "&u=" + QString(QUrl::toPercentEncoding( m_init.username )) +
%                             "&t=" + timestamp +
%                             "&a=" + auth_token;
% 
%     m_id = get( '/' + query_string );
% 
%     qDebug() << "GET:" << query_string;
% }