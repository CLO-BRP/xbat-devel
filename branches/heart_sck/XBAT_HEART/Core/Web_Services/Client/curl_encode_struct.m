function str = curl_encode_struct(in, fields, name)

% curl_encode_struct - encode struct as curl data string
% ------------------------------------------------------
%
% str = curl_encode_struct(in, fields, name)
%
% Input:
% ------
%  in - struct
%  fields - to include
%  name - of struct
% 
% Output:
% -------
%  str - encoding

%--
% handle input
%--

if nargin < 3
	name = inputname(1);
end

if isempty(name) 
	error('Unable to determine name from input.');
end

if nargin < 2
	fields = {};
end

%--
% compute string representation
%--

actual = fieldnames(in); check = ~isempty(fields); part = {};

for k = 1:numel(actual)
	
	if check && ~string_is_member(actual{k}, fields)
		continue;
	end
	
	% TODO: place more intelligent string conversion code here, we are missing urlencoding
	
	value = in.(actual{k});
	
	if iscell(value)
		value = value{1};
	end
	
	if ~ischar(value)
		try
			value = num2str(value); 
		catch
			nice_catch; value = 'FAILED';
		end
	end	
	
	part{end + 1} = [name, '[', actual{k}, ']=', value];
	
end

str = str_implode(part, '&');


