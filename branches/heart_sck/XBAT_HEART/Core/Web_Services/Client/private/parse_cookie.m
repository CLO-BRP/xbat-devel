function cookie = parse_cookie(str)

% http://en.wikipedia.org/wiki/HTTP_cookie

%--
% initialize cookie and pack full string
%--

cookie = struct; cookie.string = str;

%--
% parse cookie content
%--

part = str_split(str, ';');

% NOTE: we parse on the convention that the first part of the cookie is the 'name' and 'content' of the cookie

current = part{1};

ix = find(current == '=', 1); 

cookie.name = current(1:ix - 1); cookie.content = current(ix + 1:end);
	
% NOTE: the remaining cookie elements are conventional and indicate the context and expiration for the cookie

for k = 2:numel(part) 
	
	ix = find(part{k} == '=', 1); 

	field = part{k}(1:ix - 1); value = part{k}(ix + 1:end);
	
	cookie.(field) = value;
	
end
