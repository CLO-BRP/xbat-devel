function date = cookie_date(in)

% NOTE: this expresses a date and time as the typical cookie date

if ~nargin 
	in = now;
end

% NOTE: 'datestr' has a problem accepting the composite format string

date = strcat(datestr(in, 'ddd'), {', '}, datestr(in, 'dd-mmm-yyyy HH:MM:SS'), {' GMT'});