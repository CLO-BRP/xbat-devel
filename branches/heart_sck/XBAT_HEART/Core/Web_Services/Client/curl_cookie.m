function cookie = curl_cookie(request, url, cookie)

%--
% output cookie database for no input
%--

if ~nargin
	cookie = cookie_db; return;
end

%--
% set, get, or delete cookie data
%--

switch request

	case 'set'
		
		set_cookie(url, cookie);
		
		if nargout
			cookie = get_cookie(url);
		end
		
	case 'get'
		
		if nargin < 2
			cookie = get_all_cookies; return;
		end
		
		% NOTE: the empty path indicates that the url is of the form 'protocol://host'
		
		[ignore, ignore, path] = urlparts(url); 
		
		if isempty(path)
			url(end + 1) = '/';
		end 
		
		cookie = get_cookie(url);
		
	case 'delete'
		
		delete_cookie(url);
		
end


%-------------------------
% COOKIE_DB
%-------------------------

function file = cookie_db

% NOTE: make sure cookie database exists and return its location

% NOTE: if the persistent variable is not set, or the file does not exist

persistent FILE;

if isempty(FILE) || ~exist(FILE, 'file')
	
	FILE = fullfile(fileparts(mfilename('fullpath')), 'private', 'cookie.db');
	
	if ~exist(FILE, 'file')
		
		% NOTE: the 'unique' fields jointly represent cookie uniqueness
		
		opt = create_table; opt.unique = {'domain', 'path', 'name'};
		
		sqlite(FILE, create_table(cookie_prototype, 'cookie', opt));
	end
	
end

file = FILE;


%-------------------------
% SET_COOKIE
%-------------------------

function status = set_cookie(url, cookie)

%--
% pack addresss and get default domain and path if needed
%--

cookie.url = url;

if ~isfield(cookie, 'domain')
	
	[ignore, host] = urlparts(url); 
	
	if strncmp(host, 'www', 3)
		host = host(4:end);
	end
	
	cookie.domain = host;
	
end

if ~isfield(cookie, 'path')
	cookie.path = '/';
end

%--
% get available supported cookie fields
%--

fields = intersect(fieldnames(cookie), fieldnames(cookie_prototype));

if isempty(fields)
	status = 0; return;
end

%--
% insert available data without escaping
%--

opt = sqlite_array_insert; opt.escape = 0; 

query = sqlite_array_insert('cookie', cookie, fields, opt);

[status, result] = sqlite(cookie_db, query);


%-------------------------
% GET_ALL_COOKIES
%-------------------------

function cookie = get_all_cookies(url)

query = 'SELECT * FROM cookie;';

[status, cookie] = sqlite(cookie_db, query);


%-------------------------
% GET_COOKIE
%-------------------------

function cookie = get_cookie(url)

query = ['SELECT * FROM cookie WHERE ''', url, ''' LIKE ''%'' || cookie.domain || cookie.path || ''%'';'];

[status, cookie] = sqlite(cookie_db, query);


%-------------------------
% DELETE_COOKIE
%-------------------------

function status = delete_cookie(url, cookie)

% TODO: identify the cookie, by 'host', 'path', and 'name'?

query = [];

[status, result] = sqlite(cookie_db, query);


%-------------------------
% COOKIE_PROTOTYPE
%-------------------------

function cookie = cookie_prototype

% NOTE: we get type hints and build prototype to be used in a couple of places

hint = column_type_hints; s = hint.string;

% NOTE: consider this if the database ever gets more complex

% cookie.id = hint.integer;

% NOTE: these are the essential fields

cookie.domain = s;

cookie.path = s;

cookie.name = s;

cookie.content = s;

% TODO: this should be a timestamp, the HTTP date format is not compatible

cookie.expires = s;

% NOTE: these fields are useful for development or administrative

cookie.url = s;

cookie.string = s;

cookie.created = hint.timestamp;

