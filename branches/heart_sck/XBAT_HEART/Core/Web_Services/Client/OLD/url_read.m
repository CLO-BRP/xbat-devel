function [output, status, connection] = url_read(urlChar, method, varargin)

%---------------
% SETUP
%---------------

if ~usejava('jvm')
	error('MATLAB:urlread:NoJvm', 'URLREAD requires Java.');
end

import com.mathworks.mlwidgets.io.InterruptibleStreamCopier;

% NOTE: be sure the proxy settings are set.

com.mathworks.mlwidgets.html.HTMLPrefs.setProxySettings;

%---------------
% HANDLE INPUT
%---------------

%--
% handle parameter input
%--

switch numel(varargin)
	
	case 0, params = {};
		
	case 1
		
		if iscell(varargin{1})
			params = varargin{1};
		else
			error('Single input parameter must be field value cell array.'); 
		end
		
	otherwise
		
		if is_odd(numel(varargin))
			error('Field value pair input does not match.');
		end
		
		params = varargin;
		
end

%--
% set defualt method as get
%--

if nargin < 2 || isempty(method)
	method = 'get';	
end

%--
% set default outputs.
%--

output = ''; status = 0; connection = [];

%------------------
% CONNECT
%------------------

%--
% update URL for GET, parameter data is part of the URL
%--

if nargin > 1 && strcmpi(method, 'get')
	
	for i = 1:2:length(params)
		
		if i == 1
			separator = '?'; 
		else
			separator = '&'; 
		end

		param = char(java.net.URLEncoder.encode(params{i})); value = char(java.net.URLEncoder.encode(params{i + 1}));
		
		urlChar = [urlChar, separator, param, '=', value];
		
	end
	
end

%--
% create a connection
%--

connection = get_connection(urlChar);

%--
% finish creating connection for other methods
%--

switch method
	
	case 'get'

	case 'post'

		try

			connection.setDoOutput(true);

			connection.setRequestProperty('Content-Type', 'application/x-www-form-urlencoded');

			printStream = java.io.PrintStream(connection.getOutputStream);

			for i = 1:2:length(params)

				if i > 1, printStream.print('&'); end

				param = char(java.net.URLEncoder.encode(params{i}));

				value = char(java.net.URLEncoder.encode(params{i + 1}));

				printStream.print([param, '=', value]);

			end

			printStream.close;

		catch

			nice_catch(lasterror, 'POST failed.'); rethrow(lasterror);

		end
	
	case 'put'
		
	case 'head'
		
	case 'delete'
		
	otherwise, error(['Unrecognized HTTP method ''', method, '''.']);
		
end

%------------------
% READ
%------------------

try
	
	inputStream = connection.getInputStream;
	
	byteArrayOutputStream = java.io.ByteArrayOutputStream;
	
	% NOTE: this StreamCopier is unsupported by MATLAB and may change at any time
	
	isc = InterruptibleStreamCopier.getInterruptibleStreamCopier;
	
	isc.copyStream(inputStream, byteArrayOutputStream);
	
	inputStream.close; byteArrayOutputStream.close;
	
	output = native2unicode(typecast(byteArrayOutputStream.toByteArray', 'uint8'), 'UTF-8');

catch

	nice_catch(lasterror, 'Reading connection data failed.');
	
	if catchErrors
		return;
	else
		error('MATLAB:url_read:ConnectionFailed', 'Error downloading URL.');
	end

end

status = 1;


%------------------------
% GET_CONNECTION
%------------------------

function connection = get_connection(str, method)

%--
% get protocol handler
%--

protocol = str(1:find(str == ':', 1) - 1); handler = [];

try
	
	switch protocol

	case 'http'
		handler = sun.net.www.protocol.http.Handler;

	case 'https'
		handler = sun.net.www.protocol.https.Handler;

	end
	
catch
	
	nice_catch(lasterror, 'Failed to get protocol handler, setting empty handler.'); handler = [];

end

%--
% create the URL object
%--

try
	
	if isempty(handler)
		url = java.net.URL(str);
	else
		url = java.net.URL([], str, handler);
	end
	
catch

	nice_catch(lasterror, 'URL create failed.'); rethrow(lasterror);
	
end

%--
% determine the proxy
%--

proxy = [];

if ~isempty(java.lang.System.getProperty('http.proxyHost'))
	
	try
		ps = java.net.ProxySelector.getDefault.select(java.net.URI(str)); if ps.size > 0, proxy = ps.get(0); end
	catch
		proxy = [];
	end
	
end

%--
% open a connection to the URL
%--

if isempty(proxy)
	connection = url.openConnection;
else
	connection = url.openConnection(proxy);
end

%--
% set request method
%--

if nargin > 1	
	
	connection.setRequestMethod(method);

end

