function on__startup(config)

% on__startup - helper for local configuration
% --------------------------------------------
%
% on__startup(config)
%
% Input:
% ------
%  config - name

% TODO: some documentation as to how to use this

%------------------------------------
% SHARED configuration
%------------------------------------

% NOTE: this turns off CUDA related code

cuda_enabled false;

%------------------------------------
% NAMED configuration
%------------------------------------

% NOTE: return quickly if no named configuration is requested

if ~nargin
	return;
end

switch config
	
	case 'default'
		
		%--
		% database connection
		%--

		% NOTE: this is the default database configuration, uses SQLite as database
		
		store = get_barn_store('barn', 'SQLite'); 

		% NOTE: this is another typical configuration for the database server
		
		% NOTE: !!!!!!!! Please make sure to create the BARN database before invocation, or this will fail !!!!!!!!
		
% 		store = get_barn_store('barn', 'MySQL');
%
%		OR
% 		
% 		store = get_barn_store('barn', 'MySQL', 'localhost',...
%                                'username', 'root',...
%                                'password', 'getinnow');
		
		%--
		% web server interaction
		%--
		
		% NOTE: the command below creates the default server configuration
		
% 		server = rails_server;
		
        %This is a hint for the generation of models in the BARN space.
        server.root = '\\clo-ivoryfy07\TAMI_BARN';
        
        %This is a hint for where the assets (mp3, clip images) go.
        %server.files =  '';        
        server.files =  '\\clo-ivoryfy07\TAMI_BARN\public\files';
		
		% NOTE: the fields below are the configurable fields for server interaction
		
		server.host = 'localhost';
		
		server.port = 3000;
		
        %This is a hint for the generation of models in the BARN space.
		% example:
        %server.root = '\\systemname\barnroot';		
		server.root = fullfile(app_root, 'BARN', 'Server', 'barn');
		
		%This is a hint for where the assets (mp3, clip images) go.
		% example:
		%server.files =  '\\systemname\barnroot\public\files';
		server.files =  '';
		
		server.users =  '';
		
		%--
		% set environment variables 
		%--
		
		local_barn(store, server);
		
	% TODO: fill-in some typical options here?
		
	case 'localhost'
		
	case 'remotehost'
		
	case 'remotehost2'
		
	otherwise
		error(['Unknown configuration ''', config, '''.']);
		
end