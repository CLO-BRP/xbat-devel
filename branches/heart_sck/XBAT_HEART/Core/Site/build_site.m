function  [files, updated] = build_site(site, theme)

% BUILD_SITE build site
%
% [files, updated] = build_site(site, theme, opt)

% TODO: add printable CSS to site ... this may not be directly related to this code 

% TODO: define 'opt' and implement passing 

%--
% handle input
%--

if nargin < 2
	% NOTE: we try to call the theme helper conventionally
	
	helper = fullfile(site_root(site), 'Helpers', 'theme.m'); helpers = fileparts(helper);
		
	if exist(helper, 'file')
		start = pwd; cd(helpers);
		
		% NOTE: this is not very nice
		
		theme = feval('theme'); cd(start);
	else
		theme = [];
	end
end

%--
% start build
%--

start = clock;

disp(' ');
disp(['Starting to build site ''', site, ''' (', datestr(start), ')']);
disp(' '); 
disp(['Using ''', upper(theme.name), ''' theme.']); 
disp(' ');

sites_cache_clear;

%--
% get site model if needed
%--

% NOTE: we consider that input is either site name or model

if ischar(site)
	model = model_site(site);
else
	model = site;
end

model.theme = theme;

% %--
% % display fragments
% %--
% 
% disp(' '); disp('FRAGMENTS:'); disp(' ');
% 
% for k = 1:length(model.fragments)
% 	fragment_line(model, k);
% end

%--
% build pages
%--

N = length(model.pages); files = cell(N, 1); updated = zeros(N, 1);

% disp(' '); disp('PAGES:'); disp(' ');

for k = 1:N
	[files{k}, updated(k)] = build_page(model, model.pages(k));
	
	page_line(model, k, files{k}, updated(k));
end

disp(' ');

stop = clock;

disp(['Finished building site ''', site, ''' (', datestr(stop), ', ', sec_to_clock(etime(stop, start)), ')']);

disp(' ');

if ~nargout
	clear files;
end 


%------------------------------
% FRAGMENT_LINE
%------------------------------

function fragment_line(model, ix)

%--
% get name and file
%--

name = model.fragments(ix).name;

file = model.fragments(ix).file;

%--
% prepare and display linked fragment line message
%--

current = length(name) + 2;

if current < 72
	pad = char(double('.') * ones(1, 71 - current));
else
	pad = '';
end

str = [name, ' ', pad, ' ', edit_link(file)];

disp(str);


%------------------------------
% PAGE_LINE
%------------------------------

function page_line(model, ix, file, updated)

%--
% get name from file
%--

name = strrep(file, build_root(model.site), 'Build');

%--
% prepare and display linked page line message, considering update
%--

if ~updated
	prefix = 'Skipping';
else
	prefix = 'Updating';
end

current = length(prefix) + length(name) + 2;

if current < 72
	pad = char(double('.') * ones(1, 70 - current));
else
	pad = '';
end

str = [ ...
	prefix, ' <a href="matlab:web(''', file, ''', ''-browser'')">', name, '</a> ', ...
	pad, ' Edit ', edit_link(model.pages(ix).file, 'Page'), ' ', edit_link(file, 'Build') ...
];

disp(str);


%------------------------------
% EDIT_LINK
%------------------------------

function str = edit_link(file, name)

if nargin < 2
	name = 'Edit';
end

str = ['<a href="matlab:ted(''', file, ''');">', name, '</a>'];
