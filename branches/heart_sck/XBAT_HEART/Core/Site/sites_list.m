function sites = sites_list

% SITES_LIST list of available sites
%
% sites = sites_list

sites = get_folder_names(sites_root);
