function children = site_children

% SITE_CHILDREN site children directory names
%
% children = site_children

assets = strcat('Assets', filesep, title_caps(asset_types));

children = {assets{:}, 'Build', 'Helpers', 'Pages'};
