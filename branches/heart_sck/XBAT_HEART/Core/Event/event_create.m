function event = event_create(varargin)

% event_create - create event structure
% -------------------------------------
%
%  event = event_create
%
% Output:
% -------
%  event - event structure

%--
% get persistent event and update
%--

event = persistent_event;

% event.created = now;

%--
% set and check fields
%--

if ~isempty(varargin)
	
	%--
	% try to get field value pairs from input
	%--
	
	event = parse_inputs(event, varargin{:});
	
	%--
	% set event level
	%--
	
	event.level = event_level(event);
	
	%--
	% compute duration and bandwidth
	%--

	if ~isempty(event.time)
		event.duration = event_duration(event);
	end
	
	if ~isempty(event.freq)
		event.bandwidth = event_bandwidth(event);
	end

end


% NOTE: overall it is not clear that there is an advantage to this persistent strategy, this code organization seems better

%--------------------------------
% PERSISTENT_EVENT
%--------------------------------

function event = persistent_event

%--
% create persistent store and return quickly if available
%--

persistent EVENT_PERSISTENT;

if ~isempty(EVENT_PERSISTENT)
	
	event = EVENT_PERSISTENT; return;
	
end

%--
% create event and set persistent store
%--

%--------------------------------
% PRIMITIVE FIELDS
%--------------------------------

% NOTE: 'id' supports event hierarchy among other things

event.id = [];

event.guid = [];

% NOTE: tags, rating, and notes support simple annotation

event.tags = {};

event.rating = [];

event.notes = struct;

% NOTE: score (also rating to some extent) supports priority based navigation

event.score = [];

%--
% event fields
%--

event.channel = [];

event.time = [];

event.freq = [];

% NOTE: these will be replaced by accesor functions

event.duration = [];

event.bandwidth = [];

%--
% event data fields
%--

% event.page = struct;

% TODO: remove the following fields in favor of 'page', thus data is always in pages

event.samples = [];

event.unfiltered = [];

event.rate = [];

% NOTE: there is no point in pretending that the spectrogram is not a privileged representation

event.specgram = struct;

%--
% hierarchy event fields
%--

% TODO: consider adding other fields here, such as 'previous', 'next' for events in a chain

% NOTE: this would make it easy to get chains by looking for start events, empty 'previous and non-empty 'next'

% NOTE: the level it the max child level incremented by one

event.level = 1;

% NOTE: children and parents contain 'id'

event.children = [];

event.parent = 0;

% NOTE: these are volatile fields often useful during processing not stored to the log

event.previous = 0;

event.next = 0;

%--
% administrative fields
%--

% NOTE: consider separate authors for create and modify

event.author = '';

event.created = [];

event.modified = [];

%--
% userdata
%--

event.userdata = [];

%--------------------------------
% DATA FIELDS
%--------------------------------

% NOTE: the comments describe how data will be stored in these fields

% NOTE: generally speaking we are aiming for 'event.(type).(name).(field)'

% TODO: 'get_table_decomposition' should merge 'type' and 'name' into a table name

% event.annotation.(annotation).(field)

event.annotation = struct;

% event.detector.(detector).(field)

% event.detector.(detector).parameter_id

event.detector = struct;

% event.measure.(measure).(field)

% event.measure.(measure).parameter_id

event.measure = struct;

%--
% set persistent event
%--

EVENT_PERSISTENT = event;
