function remove = get_fields_to_remove

% NOTE: the fields must be removed from a normal event to get the prototype event used to build event table

% TODO: we may not remove these at first

not_explicit = {'author', 'level', 'children', 'parent', 'previous', 'next'};

not_needed = { ...
	'annotation', 'detection', 'measure', 'specgram', 'userdata','samples', 'rate' ...
};

remove = {not_needed{:}, not_explicit{:}};