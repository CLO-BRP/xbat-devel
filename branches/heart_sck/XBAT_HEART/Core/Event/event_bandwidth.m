function bandwidth = event_bandwidth(event, full)

% event_bandwidth - get event bandwidth
% -----------------------------------
%
% bandwidth = event_bandwidth(event, full)
%
% Input:
% ------
%  event - event 
%  full - force full evaluation of bandwidth, relevant for hierarchical events
%
% Output:
% -------
%  bandwidth - event bandwidth

if nargin < 2
	full = 0;
end

bandwidth = diff(event_freq(event, full), 1, 2);
