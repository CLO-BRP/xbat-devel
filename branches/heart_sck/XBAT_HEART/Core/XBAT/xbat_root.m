function root = xbat_root

% xbat_root - get XBAT root directory
% -----------------------------------
%
% root = xbat_root
%
% Output:
% -------
%  root - XBAT root directory

% TODO: remove this function altogether, consider keeping as shortcut

root = app_root;