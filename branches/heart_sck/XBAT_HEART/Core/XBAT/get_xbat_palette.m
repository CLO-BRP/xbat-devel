function pal = get_xbat_palette

pal = findobj(0, 'name', 'XBAT', 'tag', 'XBAT_PALETTE::CORE::XBAT');