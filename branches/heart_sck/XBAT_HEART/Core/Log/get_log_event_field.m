function [value, id] = get_log_event_field(log, field, id)

% get_log_event_field - get field from cache event
% ------------------------------------------------
%
% [value, id] = get_log_event_field(log, field, id)
%
% Input:
% ------
%  log - log
%  field - event field name
%  id - event ids to consider
%
% Output:
% -------
%  value - field values
%  id - corresponding id

%--
% handle input
%--

if nargin < 3
	id = [];
end

%--
% build sql
%--

% NOTE: we select id and possibly another field

if strcmp(field, 'id')
	sql = {'SELECT id FROM event'};
else
	sql = {['SELECT id, ', field, ' FROM event']};
end

% NOTE: we are possibly selecting these using an initial collection of id values

if ~isempty(id)
	sql{end + 1} = [' WHERE id IN (', str_implode(int_to_str(id), ', '), ')'];
end

sql{end + 1} = ';';

%--
% execute sql against log cache database
%--

result = sqlite(log_cache_file(log), 'prepared', sql);

% NOTE: we extract field value and try to convert to a matrix

value = {result.(field)};

try %#ok<TRYNC>
	value = cell2mat(value);
end

% NOTE: extract matching id values if asked to

% NOTE: typically these will match the input id values, verify this 

if nargout > 1
	id = [result.id];
end
