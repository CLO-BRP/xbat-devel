#include "mex.h"

#include "mat.h"

#include "matrix.h"

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
 
    MATFile *fp;
    
    char * file;
    
    const char *name;
    
    mxArray *log, *event;
    
       
    file = mxCalloc(mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1,  sizeof(char));
    
    mxGetString(prhs[0], file, mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1);
    
    fp = matOpen(file, "r");
    
    log = matGetNextVariable(fp, &name);
    
    event = mxGetField(log, 1, "event");
    
    
//     plhs[0] = mxCreateDoubleScalar((double) mxGetM(log_info) * mxGetN(log_info));
    
    mexPrintf("%d\n",mxGetNumberOfElements(event));
    
//    mxDestroyArray(log_info);
    
//    mxFree(file);
    
//    mxFree(name);
    
}
