function count = log_get_length(log)

% get_log_length - get number of events in log
% --------------------------------------------
%
% count = log_get_length(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  count - of log events

% NOTE: this creates a struct field with parenthesis! MEX black magic

% field = 'COUNT(id)'; sql = ['SELECT ', field, ' FROM event'];

sql = ['SELECT COUNT(id) AS length FROM event'];

[status, result] = sqlite(log_cache_file(log), sql);

count = result.length;

% NOTE: previously we would extract the desired field here

% count = result.(field);