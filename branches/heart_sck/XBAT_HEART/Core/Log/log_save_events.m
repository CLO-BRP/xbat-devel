function newlog = log_save_events(log, event, request)

% log_save_events - save events to a log
% --------------------------------------
%
% event = log_save_events(log, event, request)

%--
% handle input
%--

% NOTE: if no request description comes in, the active user is requesting we save all parts of event

if nargin < 3
	request.save = 'all'; request.user = get_active_user;
end

% NOTE: if the request comes in as a string we pack it and consider it coming from the active user

if ischar(request)
	request_save = request; clear request;
	
	request.save = request_save; request.user = get_active_user;
end

% NOTE: there are no events to save, we are done

if isempty(event)
	return;
end

% NOTE: not clear we should need this
% 
% log.store = log_store(log);

%--
% separate parent and non-parent events
%--

% NOTE: in case of hierarchical events we return, the work is done by function call in the conditional

if save_hierarchical_events(log, event, request)
	return; 
end

%--
% make sure events have file info
%--
% 
% % from Raven2XBAT
event_template = event_create_r6;
event_template.level = 1;
event_template.author = 'XBAT_Heart_tests'; %context.user.name;

% cut events if out of freq bounds
if isfield(request.user.parameter,'freq_low') && isfield(request.user.parameter,'freq_high')
    for i = length(event):-1:1
        f1 = event(i).freq(1);
        f2= event(i).freq(2);
        t1 = event(i).time(1);
        t2= event(i).time(2);
        if (f2 < request.user.parameter.freq_low) || (f1 > request.user.parameter.freq_high)
            % remove event if out of freq range
            event(i) = [];
        elseif (abs(f2-f1) > 4000) || (abs(t2-t1) < .02)
            % remove event if bandwidth too tall
            event(i) = [];
        elseif (abs(t2-t1) > .01) && (abs(t2-t1) < .045) && (abs(f2-f1) > 2000)
            % remove event if raindrop
            event(i) = [];
        else
            if (f1 < request.user.parameter.freq_low) && (f2 > (request.user.parameter.freq_low + 100))
                % change event bounds
                event(i).freq(1) = request.user.parameter.freq_low;
            end
            if (f2 > request.user.parameter.freq_high) && (f1 < (request.user.parameter.freq_high - 100))
                event(i).freq(2) = request.user.parameter.freq_high;
            end
        end
    end
end
event_out(1:length(event)) = event_template;

% revert if needed
% if log.length ==0
%     if ~isfield(event, 'file')
%         
%         for k = 1:numel(event)
%             event(k).file = get_event_file(event(k), log.sound);
%         end
%     end
% end

%--
% add events to log
%--

%%% SCK
if log.length ==0
    [format, context] = get_log_format(log);
    % NOTE: the context may have a request for the handler, we could expand this
    
    context.request = request;
    
    writer = format.fun.event.save;
    
    % NOTE: we simply return as this format cannot write events, we also do not update the cache
    
    if isempty(writer)
        return;
    end
    
end



% NOTE: a 'measure' request assumes the events are unchanged and have been previously saved

if strcmp(request.save, 'measure') && (numel([event.id]) == numel(event))
	
	id = [event.id];
	
else
	
	%--
	% update events store
	%--

	% NOTE: the writer is responsible for the event and basic annotation stores, tags and ratings
    %%% SCK
% 	
% 	try
% 		id = writer(log.store, event, context);
% 	catch
% 		extension_warning(format, 'Failed to save event.');
% 	end


% revert if needed
%     for k = log.length +1:log.length+1+numel(event)
%                 %%% SCK - FIX in future
% 		event(k).id =  k;  %id(k);
%         event(k).detection = struct();
%         event(k).annotation = struct();
%         event(k).measurement = struct();
%     end
    
    
  % From Raven2XBAT  
    for jj = 1:length(event)
        event_out(jj).time = event(jj).time;
        event_out(jj).duration = diff(event(jj).time);
        event_out(jj).freq = event(jj).freq;
        event_out(jj).bandwidth = event(jj).bandwidth;
        event_out(jj).id =  log.length + jj;
        event_out(jj).channel = event(jj).channel;
%         event_out(jj).tags = event(jj).tags;
        event_out(jj).score = event(jj).score;
%         event_out(jj).rating = event(jj).rating;
%         event_out(jj).notes = event(jj).notes;
    end
    
    
    % ranked_log = PRBA_log_create();
% ranked_log.length = length(ordered_inx);
% ranked_log.curr_id = length(ordered_inx) + 1;
% ranked_log.file = xbat_log_name;
% ranked_log.sound = figdata.log.sound;
% ranked_log.path = xbat_log_dir;
% ranked_log.author = 'SoundXT';
% 
% ranked_cnt = 0;
% ranked_log.event = event_create();

if log.length ==0
    
%     sound_data=load('info.mat');
%     sound_data=load('info2.mat');
    [FileName,PathName] = uigetfile('*.mat','Select the MATLAB code file');
    sound_data = load(FileName);
    newlog = new_log_r6(log.name,sound_data.context.user,sound_data.context.library,log.sound);
    newlog.type = 'log';
    newlog.version = 'R6';
%     newlog.path = 'C:\XBAT_R6\Users\NFCs\Libraries\Default\AEK_RavenComparison_20111024_2\Logs';
%     newlog.path = 'C:\XBAT_R6\Users\XBAT_Heart_tests\Libraries\Detectors\DANBY_20090912_194900_HR2330\Logs';
%     newlog.path = 'C:\XBAT_R6\Users\XBAT_Heart_tests\Libraries\Detectors\NSGDS_20110901_191800\Logs';
    [newlog.path] = uigetdir('C:\XBAT_R6\Users\XBAT_Heart_tests\Libraries\Detectors', 'Pick directory to save log in');
    newlog.file = char(log.name);
%     newlog.author = 'NFCs';
    newlog.author = 'XBAT_Heart_tests';
    newlog.open = 0;
    newlog.readonly = 0;
    newlog.autosave = 1;
    newlog.autobackup = 1;
    newlog.saved = 1;
    newlog.visible =1;
    newlog.userdata =struct(); newlog.time = []; newlog.freq = []; newlog.channel = []; newlog.event_id = 1;
    newlog.duration = []; newlog.bandwidth = []; newlog.generation =[];
    newlog.measurement = struct();
    newlog.annotation = struct();
    newlog.deleted_event = struct();
    newlog.selected_events = struct();
    try    newlog = rmfield(newlog, 'cache');
        newlog = rmfield(newlog, 'store');
        newlog = rmfield(newlog, 'format');
        newlog = rmfield(newlog, 'name');
    end
    
    newlog.event = event_out;
    newlog.length = length(event);
    newlog.curr_id = newlog.length + 1;
    
else
    newlog = log;
    new_events = newlog.event;
    ix1 = length(new_events)+1;
    ix2=ix1+length(event_out);
    for z = 1:length(event_out)
        add_event = event_out(1,z);
        new_events(end+1) = add_event;
    end
    newlog.event = new_events;
    newlog.length = length(new_events);
    newlog.curr_id = newlog.length + 1;

 end      

    
    
%     curr_dir = pwd;
%     cd(newlog.path)
%     log_save(newlog);
%     cd(curr_dir)
%     str1 = sprintf('New XBAT log saved: ''%s''\n',newlog.file);
%     disp(str1);
                    
% 	log_cache_update(log, 'update', event);
	
	%--
	% render barn resources if needed
	%--

	% NOTE: it is not clear that the final part of the test would pass right now

	if ~isempty(local_barn) && ~string_is_member(request.save, {'tags', 'rating'}) % && isequal(local_barn, log.store)

		% TODO: this should be deferred, we should submit a request to a daemon

		render_barn_resources('log', log, event);
	end

end

%--
% save event extension data if possible and needed
%--
if log.length == 0
    if isempty(format.fun.event.extension)
        return;
    end
end
%--
% make sure all events have legitimate id
%--

% NOTE: this happens when saving new events

%%% SCK
% if ~isequal(id, [event.id])
%    
%     for k = 1:length(event)
%         event(k).id = id(k);
%     end
%     
% end

%--
% extract extensions from all events and prepare them for storage
%--
% SCK 10-15-12
% [id, ext, data] = get_event_extension_data(event);

%--
% save the various types of extension data available
%--
    
% TODO: we need to properly check for this handle, the test above is suspect
% SCK 10-15-12
% for k = 1:length(ext)
% 
% 	% TODO: figure out what this result should contain and what we do with it
% 	
% 	try
% 		result = format.fun.event.extension.save(log.store, id{k}, ext(k), data{k}, context);
% 	catch
% 		extension_warning(format, ['Failed to save data for ', upper(title_caps(ext(k).subtype)), ' ''', ext(k).name, '''.']);
% 	end
% 	
% end


%---------------------------------
% SAVE_HIERARCHICAL_EVENTS
%---------------------------------

function count = save_hierarchical_events(log, event, request) %#ok<DEFNU>

%--
% check for hierarchical events
%--

free = iterate(@isempty, {event.children}); count = sum(~free);

if ~count
	return;
end

%--
% store free events
%--

event(free) = log_save_events(log, event(free), request);

%--
% store hierarchical events, parents and their children
%--

parent = event(~free); childless = rmfield(parent, 'children'); childless(1).children = [];

childless = log_save_events(log, childless, request);

for k = 1:numel(parent)

	%--
	% set children link to parent
	%--

	child = parent(k).children;

	for j = 1:numel(child)
		child(j).parent = childless(k).id;
	end

	%--
	% update parent id and children
	%--

	parent(k).id = childless(k).id;

	parent(k).children = log_save_events(log, child, request);

end

%--
% pack stored parents
%--

event(~free) = parent;

