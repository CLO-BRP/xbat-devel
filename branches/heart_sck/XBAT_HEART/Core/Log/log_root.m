function [root, exists] = log_root(name, varargin)

% log_root - get root directory for log
% -------------------------------------
%
% [root, exists] = log_root(name, sound, lib)
%
% Input:
% ------
%  name - log name
%  sound - parent sound
%  lib - parent library
%
% Output:
% -------
%  root - directory
%  exists - existence indicator

%--
% check name
%--

if ~proper_filename(name)
    error('Log name must be a proper filename.');
end

%--
% get root and check it exists
%--

root = [logs_root(varargin{:}), filesep, name];

if nargout > 1
    exists = exist_dir(root);
end 