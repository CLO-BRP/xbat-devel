function browser_log_open(par, log, data)

% browser_log_open - set browser menus to reflect open log
% --------------------------------------------------------
%
% browser_log_open(par, log, data)

%--
% handle input
%--

if nargin < 3 || isempty(data)
    data = get_browser(par);
end
    
%--------------------------------------------------------
% CREATE LOG RELATED MENUS
%--------------------------------------------------------

%--
% update userdata
%--

set(data.browser.log_menu.log,'enable','on');

%--
% Log
%--

file = file_ext(log.file);

L = { ...
	file, ...
	[file ' Options'] ...
};

n = length(L);

S = bin2str(zeros(1,n));
S{1} = 'on';

g1 = menu_group(data.browser.log_menu.log(1),'browser_log_menu',L,S);

set(g1,'tag',file);

%--
% Log information, annotation, and measurement
%--

L = { ...
	'Log', ...
	'Annotation', ...
	'Measurement' ...
};

n = length(L);

S = bin2str(zeros(1,n));
S{2} = 'on';

g2 = menu_group(g1(1),'browser_log_menu',L,S);

set(g2,'tag',file);

%--
% Log and Info
%--

if ((isempty(log.time) || isempty(log.freq) || isempty(log.channel)) && log.length)
	
	[time,freq,channel] = log_info_update(log);
	
	log.time = time;
	
	log.freq = freq;
	
	log.duration = diff(time);
	
	log.bandwidth = diff(freq);
	
	log.channel = channel;
	
end

[tstr,fstr] = log_labels(par,log);

L = { ...
	['Path:  ' log.path], ...
	['File:  ' log.file], ...
	['Number of Events:  ' int2str(log.length)], ...
	['Current ID:  ' int2str(log.curr_id)], ...
	['Channels:'], ...
	['Start Time:  ' tstr{1}], ...
	['End Time:  ' tstr{2}], ...
	['Duration:  ' tstr{3}], ...
	['Min Freq:  ' fstr{1}], ...
	['Max Freq:  ' fstr{2}], ...
	['Bandwidth:  ' fstr{3}], ...
	['Log Info:'] ...
};

n = length(L);

S = bin2str(zeros(1,n));
S{3} = 'on';
S{5} = 'on'; 
S{6} = 'on';
S{9} = 'on';
S{end} = 'on';

g3 = menu_group(get_menu(g2,'Log'),'',L,S);

set(g3,'tag',file);

if (~isempty(log.modified))
	L = { ...
		['Author:  ' log.author], ...
		['Created:  ' datestr(log.created)], ...
		['Modified:  ' datestr(log.modified)] ...
	};
else
	L = { ...
		['Author:  ' log.author], ...
		['Created:  ' datestr(log.created)] ...
	};
end

n = length(L);

S = bin2str(zeros(1,n));
S{2} = 'on';

tmp = menu_group(g3(end),'',L,S);

set(tmp,'tag',file);

%--
% Channel
%--

n = length(log.channel);

L = cell(1,n);
for k = 1:length(log.channel)
	L{k} = ['Channel ' int2str(log.channel(k))];
end

menu_group(get_menu(g3,'Channels:'),'',L);
	
%--
% Log Options
%--

L = { ...
	'Color', ...
	'Line Style', ...
	'Line Width', ...
	'Opacity', ...
	'Auto Save Log', ...
	'Save Log', ...
	'Backup Log', ...
	'Close' ...
};

n = length(L);

S = bin2str(zeros(1,n));
S{5} = 'on'; 
S{end} = 'on';

g2 = menu_group(g1(2),'browser_log_menu',L,S);

set(g2,'tag',file);

if (log.autosave)
	set(get_menu(g2,'Auto Save Log'),'check','on');
end

if (log.saved)
	set(get_menu(g2,'Save Log'),'enable','off');
end

set(get_menu(g2,'Backup Log'),'enable','off');
set(get_menu(g2,'Export Log ...'),'enable','off');

%--
% Color
%--

[L,S] = color_to_rgb;

tmp = menu_group(get_menu(g2,'Color'),'browser_log_menu',L,S);
	
set(tmp,'tag',file);

ix = find(strcmp(rgb_to_color(log.color),color_to_rgb));
if (~isempty(ix))
	set(tmp(ix),'check','on');
end

%--
% Line Style
%--

[L,S] = linestyle_to_str('','strict');

tmp = menu_group(get_menu(g2,'Line Style'),'browser_log_menu',L,S);		

set(tmp,'tag',file);

ix = find(strcmp(L,str_to_linestyle(log.linestyle)));
if (~isempty(ix))
	set(tmp(ix),'check','on');
end

%--
% Line Width
%--

L = {'1 pt','2 pt','3 pt','4 pt'};

tmp = menu_group(get_menu(g2,'Line Width'),'browser_log_menu',L);
	
set(tmp,'tag',file);
	
ix = find(log.linewidth == [1, 2, 3, 4]);
if (~isempty(ix))
	set(tmp(ix),'check','on');
end

%--
% Opacity
%--

L = { ...
	'Transparent', ...
	'1/8 Alpha', ...
	'1/4 Alpha', ...
	'1/2 Alpha', ...
	'3/4 Alpha', ...
	'Opaque' ...
};

n = length(L);

S = bin2str(zeros(1,n));
S{2} = 'on';
S{end} = 'on';

tmp = menu_group(get_menu(g2,'Opacity'),'browser_log_menu',L,S);

set(tmp,'tag',file);

ix = find(log.patch == [-1, 1/8, 1/4, 1/2, 3/4, 1]);
if (~isempty(ix))
	set(tmp(ix),'check','on');
end

%--------------------------------------------------------
% UPDATE RELATED MENUS UPON OPENING LOG
%--------------------------------------------------------

L = file_ext(struct_field(data.browser.log,'file'));

% try sorting these menus

tmp = length(data.browser.log);

if (tmp > 1)
	
	[L,ix] = sort(L);
	
	ms = find(data.browser.log_active == ix);
	
	v = struct_field(data.browser.log,'visible');
	v = v(ix);
	
else
	
	ms = data.browser.log_active;
	
	v = data.browser.log.visible;
	
end

%--
% update active menu
%--

tmp = get_menu(data.browser.log_menu.log,'Active');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_log_menu',L);
data.browser.log_menu.active = tmp;

% set(tmp(data.browser.log_active),'check','on');
set(tmp(ms),'check','on');

%--
% update display menu
%--

tmp = get_menu(data.browser.log_menu.log,'Display');
delete(get(tmp,'children'));

S = bin2str(zeros(length(L) + 2,1));
S{2} = 'on';
S{end} = 'on';

tmp = menu_group(tmp,'browser_log_menu',{'No Display',L{:},'Display All'},S);
data.browser.log_menu.display = tmp;

for k = 1:length(data.browser.log)
% 	if (data.browser.log(k).visible)
	if (v(k))
		set(tmp(k + 1),'check','on');
	else
		set(tmp(k + 1),'check','off');
	end
end

%--
% update browse menu
%--

tmp = get_menu(data.browser.log_menu.log,'Browse');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_log_menu',L);
data.browser.log_menu.browse = tmp;

%--
% update export menu
%--

tmp = get_menu(data.browser.log_menu.log,'Export');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_log_menu',L);
data.browser.log_menu.export = tmp;

%--
% update strip menu
%--

tmp = get_menu(data.browser.log_menu.log,'Strip');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_log_menu',L);
data.browser.log_menu.strip = tmp;
	
%--
% update copy to workspace menu
%--

tmp = get_menu(data.browser.log_menu.log,'Workspace');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_log_menu',L);
data.browser.log_menu.copy_to_workspace = tmp;
	
%--
% update log selection to menu
%--

tmp = get_menu(data.browser.edit_menu.edit,'Log Selection To');
delete(get(tmp,'children'));

tmp = menu_group(tmp,'browser_edit_menu',L);
data.browser.edit_menu.log_to = tmp;

% set(tmp(data.browser.log_active),'check','on');
set(tmp(ms),'check','on');

%--------------------------------------------------------
% SORT LOG INFORMATION MENUS
%--------------------------------------------------------

%--
% get parent log menu
%--

tmp = findobj(par,'type','uimenu','label','Log');

ix = find(cell2mat(get(tmp,'parent')) == par);

%--
% get children and select the ones to sort
%--

% assume the children come out in reverse order

ch = get(tmp(ix),'children');

ch = flipud(ch(:));

ch = ch(7:end);

%--
% sort the menus
%--

if (length(ch) > 2)

	[ignore,ix] = sort(get(ch(1:2:end),'label'));
	
	ch = reshape(ch,2,length(ch)/2)';
	ch = ch(ix,:)';
	ch = ch(:);
	
	% note that this depends on the number of static menu items in the log menu
	
	for k = 1:length(ch)
		set(ch(k),'position',k + 6);
	end
	
end

%--
% update measure menu, make measurements available
%--

set(data.browser.measure_menu.measure,'enable','on');

%--
% update annotate menu, make annotations available
%--

set(data.browser.annotate_menu.annotate,'enable','on');

%--
% update renderer mode and menus, eventually get rid of the renderer menus
%--

update_renderer(par, [], data);

%--
% update userdata
%-

set(par, 'userdata', data);

%--
% perform log related palette updates
%--

% NOTE: this does not affect the state of the browser on open, only on close

update_log_palette(par, data);

update_extension_palettes(par, data);

%--
% update find events if needed
%--

update_find_events(par, [], data);

%--
% update display 
%--

browser_display(par, 'events', data);
