function file = log_file(name, varargin)

% log_file - get full file name for log
% -------------------------------------
%
% file = log_file(name, sound, lib)
%
% Input:
% ------
%  name - name
%  sound - parent sound
%  lib - parent library
%
% Output:
% -------
%  file - filename

% NOTE: this 'log.mat' file is analogous to the XBAT sound file

file = [log_root(name, varargin{:}), filesep, 'log.mat'];