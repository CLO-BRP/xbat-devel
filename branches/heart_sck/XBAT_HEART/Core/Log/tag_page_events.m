function tag_page_events(par, log, page)

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end

% NOTE: there is no browser available, we are done

if isempty(par)
	return;
end

if nargin < 3
	page = get_browser_page(par);
end

if nargin < 2
	log = get_active_log(par);
end

%--
% setup for tagging dialog
%--

[event, ignore, container] = get_events_by_page(log, page, page.channel);

% NOTE: there are no relevant events in page, we are done

if isempty(event)
	return;
end

last_tags = get_env('last_page_tags'); last_rating = get_env('last_page_rating');

if isempty(last_tags)
	last_tags = ''; last_rating = 0;
end

%--
% create and render dialog
%--

control = header_control('Event', 0);

control(end + 1) = control_create( ...
	'name', 'tags', ...
	'style', 'edit', ...
	'space', 0.75, ...
	'string', last_tags ...
);

% TODO: consider the default rating color

% 'color', 0.9 * [1 0.85 0], ...
	
control(end + 1) = control_create( ...
	'name', 'rating', ...
	'style', 'rating', ...
	'marker', 'p', ...
	'value', last_rating, ...
	'space', 0.75, ...
	'max', 5, ...
	'width', 0.4 ...
);

% TODO: include current event tags and ratings in string

str = strcat(log_name(log), {' #'}, iterate(@int2str, [event.id]));

control(end + 1) = control_create( ...
	'name', 'events', ...
	'style', 'listbox', ...
	'string', str, ... 
	'max', 2, ...
	'value', 1:numel(str), ...
	'space', 1.5, ...
	'lines', 3 ...
);

opt = dialog_group; 

opt.header_color = get_extension_color('sound_browser_palette');

out = dialog_group('Tag & Rate Events ...', control, opt, @tag_dialog_callback, par);

% NOTE: empty values indicate cancel, we are done

if isempty(out.values)
	return;
end

%--
% update events and save
%--

tags = out.values.tags; rating = out.values.rating;

set_env('last_page_tags', out.values.tags); tags = str_to_tags(tags);

set_env('last_page_rating', out.values.rating);

% TODO: use event control value to select events to update

for k = 1:numel(event)
	event(k) = set_tags(event(k), tags); event(k).rating = rating;
end

log_save_events(log, event);

%--
% update browser display
%--

browser_display(par, 'events');


%----------------------------
% TAG_DIALOG_CALLBACK
%----------------------------

function tag_dialog_callback(obj, eventdata)



