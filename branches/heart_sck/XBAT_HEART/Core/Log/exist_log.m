function [value, valid, reason] = exist_log(name, varargin)

% exist_log - returns true if a particular log exists
% ---------------------------------------------------
%
% [value, valid, reason] = exist_log(name, sound, lib)
%
% Input:
% ------
%  name - log name
%  sound - parent sound
%  lib - parent library
%
% Output:
% -------
%  value - existence indicator
%  valid - test log is valid
%  reason - non-empty if log is not valid

%--
% check for existence of log connection file
%--

file = log_file(name, varargin{:});

value = exist(file, 'file');

%--
% validate if requested
%--

% NOTE: 'log_is_valid' may useful in 'get_logs', it should probably output the 'log'

if nargout > 1
	[valid, reason] = log_is_valid(file);
end
