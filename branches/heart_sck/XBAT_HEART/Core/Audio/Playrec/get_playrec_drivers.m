function driver = get_playrec_drivers

% get_playrec_drivers - get available playrec drivers
% ---------------------------------------------------
%
% driver = get_playrec_drivers
% 
% Output:
% -------
%  driver - names

device = get_playrec_devices; driver = unique({device.hostAPI});