function create_rating_menu(par, rating, highest, callback)

% create_rating_menu - create a menu for rating things
% ----------------------------------------------------
%
% create_rating_menu(par, rating, highest, callback)
%
% Input:
% ------
%  par - parent
%  rating - rating
%  highest - allowed
%  callback - callback 

%--
% handle input
%--

% TODO: set a simple tracer callback here

if nargin < 4
	callback = '';
end

if nargin < 3
	highest = 5;
end

delete(get(par, 'children'));

%--
% remove parent callback
%--

% NOTE: the basic rating menu is very simple and does not change

set(par, 'callback', []);

%--
% build base menu
%--

% uimenu(par, 'label', '(Rating)', 'enable', 'off');

str = char(repmat(double(' *'), 1, highest));

handle = uimenu(par, 'label', 'None', 'tag', '0');

for k = 1:highest
	handle(end + 1) = uimenu(par, 'label', str(1:(2 * k)), 'tag', int2str(k));
end

set(handle, 'callback', callback);

%--
% map rating state to menu state
%--

if isempty(rating)
	set(handle(1), 'check', 'on');
else
	set(handle(rating + 1), 'check', 'on');
end


