function ext = get_palette_extension(pal)

%--
% handle input
%--

if ~nargin
	pal = gcf;
end

if ~is_palette(pal)
	ext = []; return;
end

%--
% get palette info
%--

info = parse_extension_tag(get(pal, 'tag'));

ext = get_browser_extension(info.type, info.parent, info.name);