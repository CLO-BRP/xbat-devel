function browser_play_events(par)

% browser_play_events - play events in page
% -----------------------------------------
%
% browser_play_events(par)
%
% Input:
% ------
%  par - browser

% TODO: we need to get the events on the page in general

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end

if isempty(par)
	return; 
end

%--
% setup
%--

log = get_active_detection_log(par);

if isempty(log.event)
	return; 
end

% NOTE: the pause duration will be guided by the mean event duration but limited

duration = 0.25 * mean([log.event.duration]); duration = clip_to_range(duration, [0.5, 2]);

%--
% play events
%--

for k = 1:numel(log.event)
	
	set_browser_selection(par, log.event(k)); pause(duration); browser_play(par); 
	
end