function info = parse_browser_tag(tag)

% parse_browser_tag - parse browser tag to get sound, library, and user
% ---------------------------------------------------------------------
%
% info = parse_browser_tag(tag)
%
% Input:
% ------
%  tag - tag
%
% Output:
% -------
%  info - browser info

%--
% parse tag
%--

fields = {'type', 'user', 'library', 'sound'}; sep = '::';

info = parse_tag(tag, sep, fields);

% info.tag = tag;