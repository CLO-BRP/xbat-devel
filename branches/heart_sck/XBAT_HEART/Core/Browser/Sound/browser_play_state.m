function value = browser_play_state(par, varargin)

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end

if isempty(par)
	return;
end

% NOTE: we check based on the number of input arguments whether we are getting or setting

getting = numel(varargin) < 2; known = {'channel', 'speed'};

switch numel(varargin)
	
	case 0, getting = 1;
		
	case 1
		
		getting = 1; field = varargin{1}; 
		
		if ~string_is_member(field, known)
			error(['Unrecognized play state field ''', field, '''.']);
		end
		
	otherwise

		getting = 0; 
		
		[field, value] = get_field_value(varargin, known);

end
	
%--
% setup
%--

data = get_browser(par); play = data.browser.play; sound = data.browser.sound;

%--
% get state
%--

if getting
	
	value = play; 
	
	if exist('field', 'var')
		value = play.(field); 
	end

	return;
	
end

%--
% update state
%--

change = 0;

for k = 1:numel(field)
	
	switch field{k}

		case 'channel'

			% NOTE: currently we support only stereo playback, this could change

			if numel(value) ~= 2
				error('Two play channels must be specified.');
			end

			if any(value < 1) || any(value > sound.channels)
				error('Play channel values are not available.');
			end

			change = change || ~isequal(play.channel, value);
			
			play.channel = value;

		case 'speed'
			
			if value <= 0
				error('Only positive playback speeds are currently supported.');
			end
			
			change = change || play.speed ~= value;
			
			play.speed = value;
			
	end
	
end

%--
% return quickly if there was no change
%--

if ~change
	return; 
end

%--
% update state and generate event is play state changed
%--

play.channel = value;

set(par, 'userdata', data);


% TODO: implement 'play__state' event, other features can display this information

