function clear_selection_display(selection)

% clear_selection_display - clear selection display in browser
%
% clear_selection_display(selection)
%
% Input:
% ------
%  selection - selection
%
% NOTE: this is a low-level function that does not generate an event

%--
% clear selection display
%--

if all(ishandle(selection.handle))
	
	delete(selection.handle);

end

if isfield(selection, 'handles')

	iterate_struct(selection.handles, @delete);

end
