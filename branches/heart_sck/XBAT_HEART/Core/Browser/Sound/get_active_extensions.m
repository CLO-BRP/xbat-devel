function active = get_active_extensions(varargin) 

% get_active_extensions - get active extensions of all types
% ----------------------------------------------------------
%
% active = get_active_extensions(par, data)
%
% Input:
% ------
%  par - browser
%  data - browser state
%
% Output:
% -------
%  active - extensions struct, fieldnames are types

% TODO: consider other possible output forms in 'get_active_extension'

active = get_active_extension(get_extension_types, varargin{:});
