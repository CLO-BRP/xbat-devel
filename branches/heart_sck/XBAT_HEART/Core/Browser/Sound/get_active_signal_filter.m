function [ext, context, ix] = get_active_signal_filter(varargin)

[ext, ix, context] = get_active_extension('signal_filter', varargin{:});