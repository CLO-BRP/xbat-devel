function browser_time_slide(par, time, data, update)

% browser_time_slide - slide browser grid
% ---------------------------------------
%
% browser_time_slide(par, time, data, update)
%
% Inputs:
% -------
%  par - browser handle
%  time - page start time
%  data - browser state
%  update - update event indicator

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default update state
%--

if nargin < 4
	update = 1;
end

%--
% get browser time if needed
%--

if (nargin < 2) || isempty(time)
	slider = get_time_slider(par); time = slider.value;
end

%--
% get browser state and copy some fields
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

sound = data.browser.sound; page = data.browser.page;

ax = data.browser.axes; grid = data.browser.grid;

%--------------------------------
% DISPLAY UPDATE
%--------------------------------

start_update = clock;

% NOTE: this is meant to be a lightweight display for browser

%--
% update axes time position and grid
%--

limits = [time, time + page.duration];

set(ax, 'xlim', limits);

set_time_grid(ax, grid, limits, sound.realtime, sound);

%--
% display events
%--

% NOTE: event display was moved here because it was clearing boundaries

display_events(par, time, page.duration, data, 1);

%--
% display selection
%--

% NOTE: it's not clear that we need to redraw the selection on every call

selection = get_browser_selection(par, data);

if ~isempty(selection.event)
	selection_event_display(selection.event, [], data);
end

%--
% display file boundaries
%--

[times, files] = get_file_boundaries(sound, time, page.duration);

display_file_boundaries(ax, times, files, data);

%--
% display session boundaries
%--

[times, start] = get_session_boundaries(sound, time, page.duration);

display_session_boundaries(ax, times, start, [], data);

%--
% update navigate palette
%--

% TODO: this is broken at the moment

update_navigate_palette(par, data);


str = get_grid_time_string(data.browser.grid, time, data.browser.sound);

handle = big_centered_text(ax(1), str);

uistack(handle(1), 'top');


%--------------------------------
% UPDATE WIDGETS
%--------------------------------

% NOTE: we generate 'slide__start' and 'slide__update' here, it is not clear where we generate 'slide__stop'

% NOTE: the event is indicated by 'update' input, and may change if context has changed

if update
	event = 'slide__update';
else
	event = 'slide__start';
end

persistent LAST_PARENT LAST_CONTEXT WIDGET_DATA;

% NOTE: it is still possible for the context to be stale, we are trying to be lean

if isempty(LAST_PARENT) || ~isequal(LAST_PARENT, par) || (LAST_CONTEXT.page.duration ~= page.duration)
	
	event = 'slide__start';
	
	LAST_PARENT = par;

	LAST_CONTEXT = get_extension_context([], par, data);
	
	WIDGET_DATA = get_widget_data(par, event, data);
	
end

widgets = get_widgets(par);

for k = 1:length(widgets)

	% TODO: consider other data that this event may want
	
	% NOTE: this is the key variable element for the slide event, time has not been stored yet
	
	WIDGET_DATA.slide.time = time;

	result = widget_update(par, widgets(k), event, WIDGET_DATA, 0, LAST_CONTEXT);

	set_widget_status(widgets(k), result);
	
end

%--
% update browser status display
%--

% TODO: extend these ideas so we can display status for widgets

% TODO: the string computation is still too slow, most can be cached ... we can also use some persistent variable here

% update_browser_status(par, 'left', 'message', get_browser_status_string(par, start_update, data));


