function [width, height] = browser_imsize_to_figsize(width, height, ch, status, color, slider)

% browser_imsize_to_figsize - compute figure size for a given image size
%-----------------------------------------------------------------------
%
% Inputs:
% -------
% width - image width
% height - image height
% ch - number of channels
% sb - status bar flag (1 = status bar present)
% cb - color bar flag (1 = color bar present)
%
% Outputs:
% --------
% width - figure width
% height - figure height

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1935 $
% $Date: 2005-10-14 16:58:12 -0400 (Fri, 14 Oct 2005) $
%--------------------------------

[layout, tile] = browser_tile_layout(status, color, slider);

%--
% compute width and height
%--

% NOTE: image size is already in pixels, so we don't multiply by the tile size

height = (height * ch) + tile * ((layout.yspace * (ch - 1)) + (layout.bottom + layout.top));

width = width + tile * (layout.left + layout.right + layout.colorbar);
