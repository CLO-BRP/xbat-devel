function browser_warning(par, str, info)

% browser_warning - produce warning for extension failure
% ---------------------------------------------------------
%
% browser_warning(par, str, info)
%
% Input:
% ------
%  par - browser producing message
%  str - message string (def: none)
%  info - error info struct as provided by 'lasterror' (def: lasterror)

% TODO: use nice catch logging, consider local and centralized stores

%--
% get error info if needed
%--

% NOTE: this explains the order of the arguments

if nargin < 3
	info = lasterror;
end

if nargin < 2
	str = '';
end

% NOTE: consider if we could use the active browser to be the parent

%--
% put warning together using extension
%--

par = get_browser_info(par);

% TODO: include further information in message prefix

str = ['WARNING: In BROWSER ''', par.sound, '''. ', str];

%--
% call nice catch
%--

nice_catch(info, str);
	
