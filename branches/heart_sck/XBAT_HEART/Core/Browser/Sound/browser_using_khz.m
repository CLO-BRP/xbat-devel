function value = browser_using_khz(par, data)

% browser_using_khz - check whether browser is using khz grid
% -----------------------------------------------------------
%
% value = browser_using_khz(par, data)
%
% Input:
% ------
%  par - browser
%  data - browser state or related context
%
% Output:
% -------
%  value - indicator

% TODO: factor common part here and create functions to answer other questions

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end

if nargin < 2
	data = get_browser(par);
end

%--
% check for state or context and answer question
%--

% NOTE: the first case corresponds to context input, the later to browser state

% NOTE: we handle an exception and return undefined if we cannot get the grid state information

try
	
	if isfield(data, 'display')
		using = data.display.grid.freq.labels;
	else
		using = data.browser.grid.freq.labels;
	end
	
catch
	
	value = []; return;
	
end

value = strcmpi(using, 'kHz');

