function out = browser_svn_menu(par, str)

% browser_svn_menu - browser subversion menu
% ------------------------------------------
%
% flag = browser_svn_menu(par, str)
%
% Input:
% ------
%  par - figure handle (def: gcf)
%  str - menu command string (def: 'Initialize')
%
% Output:
% -------
%  out - command dependent output

%-----------------------------
% SETUP
%-----------------------------

%--
% return empty by default
%--

out = [];

% NOTE: this feature is for xbat developers

if ~xbat_developer
	return;
end

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set command string
%--

if nargin < 2
	str = 'Initialize';
end

%--
% set handle
%--

if nargin < 1
	par = gcf;
end

%--------------------------------
% COMMAND SWITCH
%--------------------------------

switch str

	%--
	% INITIALIZE
	%--
	
	case 'Initialize'

		%--
		% check for existing menu
		%--

		if get_menu(par, 'SVN')
			return;
		end

		%--
		% create menu
		%--

		L = { ...
			'SVN', ...
			'Update ...', ...
			'Status ...', ...
			'Commit ...', ...
			'About TSVN ...' ...
		};

		n = length(L); 
		
		S = bin2str(zeros(1, n)); S{3} = 'on'; S{end} = 'on';
		
		out = menu_group(par,'browser_svn_menu', L, S);
		
		if isempty(tsvn_root)
			set(out, 'enable', 'off');
		end

	%--
	% SVN COMMANDS
	%--
	
	otherwise

		%--
		% get actual command string
		%--
		
		str = strtok(str, ' '); str = lower(str);
		
		%--
		% update xbat
		%--
		
		if strcmp(str, 'update')
			
			xbat_update; return;
			
		end
		
		%--
		% execute blocking subversion command with tortoise
		%--
		
		% NOTE: we block in case we update the svn info
		
		tsvn_options('block', 1);
				
		status = tsvn(str, app_root);
	
		% NOTE: non-zero status indicates premature termination, no need for update
	
		if status
			return;
		end
		
		%--
		% perform required updates
		%--
		
		% TODO: develop an update function to call after command
		
		if strcmp(str, 'commit')

			% NOTE: this will clear a persistent store of the version information
			
			clear xbat_version;
			
			%--
			% update version display in browser windows
			%--
			
			par = get_xbat_figs('type', 'sound');

			for k = 1:length(par)
				set_browser_status_text(par(k), '', ['XBAT ', xbat_version]);
			end
			
		end

end
