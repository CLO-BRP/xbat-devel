function handles = browser_classify_menu(par)

% TODO: generalize this for all relevant extension types

%--
% get extensions
%--

ext = get_extensions('event_classifier');

handles = [];

if isempty(ext)
	return;
end

%--
% create top menu and extensions category menu
%--

top = uimenu(par, 'label', 'Classify'); 

handles(end + 1) = top;

extension_category_menu(top, ext);

set(top, 'position', 11);