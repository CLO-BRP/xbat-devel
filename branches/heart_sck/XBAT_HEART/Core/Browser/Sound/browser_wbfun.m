function browser_wbfun(obj,eventdata,type)

% browser_wbdfun - browser window button function
% -----------------------------------------------
%
%  browser_wbfun(obj,eventdata,state)
%
% Input:
% ------
%  obj - callback object handle
%  eventdata - system provided event data
%  type - type of button callback ('down','motion, or 'up')

%--
% check current object
%--

tmp = gco;

switch (type)
	
	%--
	% button down callback
	%--
	
	case ('down')
		
		%--
		% set motion callback
		%--
		
		set(obj,'windowbuttonmotionfcn',{@browser_wbfun,'motion'});
		
		
	%--
	% motion callback
	%--
	
	case ('motion')
		
		%--
		% compute depending on current object tag
		%--
				
% 		tag = get(tmp,'tag'); 
% 		
% 		if (~isempty(tag))
% 			
% 			switch (tag)
% 
% 				%--
% 				% scrolling display
% 				%--
% 
% 				case ('BROWSER_TIME_SLIDER')
% 
% 					browser_view_menu(get(tmp,'parent'),'Scrollbar');
% 
% 					drawnow; % is this required ???
% 
% 			end
% 			
% 		end
		
	%--
	% button up callback
	%--
		
	case ('up')
		
		%--
		% empty motion callback
		%--
		
		set(obj,'windowbuttonmotionfcn',[]);
		
end


disp(['|||' upper(type) '|||' get(tmp,'tag')]);
