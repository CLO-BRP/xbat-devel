function [log, ix] = get_active_log(par, data)

% get_active_log - get browser active log
% ---------------------------------------
%
% [log, ix] = get_active_log(par, data)
%
% Input:
% ------
%  par - parent browser
%  data - browser state
%
% Output:
% -------
%  log - active log
%  ix - log index

%--
% handle input
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end 

if isempty(par)
	log = []; ix = []; return;
end

if nargin < 2
	data = get_browser(par);
end

%--
% get active log
%--

% NOTE: we can check for an active log index before we actually get the logs

ix = data.browser.log_active; 

% NOTE: the active log index is zero when there are no active logs

if ix
	log = get_browser_log(par, [], data); log = log(ix);
else
	log = [];
end 