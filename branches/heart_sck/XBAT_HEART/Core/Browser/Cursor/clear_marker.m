function count = clear_marker(par)

handles = get_marker_handles(par);

count = numel(handles);

if count
	delete(handles);
end
