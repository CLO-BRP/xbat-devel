function event = get_marker_event(marker)

% get_marker_event - get marker as event
% --------------------------------------
%
% event = get_marker_event(marker)
%
%       = get_marker_event(par)
%
% Input:
% ------
%  marker - struct
%  par - browser
%
% Output:
% -------
%  event - representation of marker

%--
% handle input to allow interactive use
%--

% NOTE: try to get active browser

if ~nargin
	marker = get_active_browser;
end

if isempty(marker)
	event = empty(event_create); return;
end

% NOTE: assume parent browser input

if ~isstruct(marker)
	marker = get_browser_marker(marker);
end

%--
% get event from marker
%--

for k = 1:numel(marker)
	
	event(k) = event_create( ...
		'time', marker.time * ones(1, 2), 'channel', marker.channel, ...
		'tags', marker.tags, 'rating', marker.rating, 'notes', marker.notes ...
	);

end

