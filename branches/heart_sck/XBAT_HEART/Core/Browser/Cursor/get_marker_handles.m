function handles = get_marker_handles(par)

handles = findobj(par, 'tag', 'marker-handles');

if numel(handles) > 1
	type = get(handles, 'type'); [type, ix] = sort(type); handles = handles(ix);
end