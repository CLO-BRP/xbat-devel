function marker = set_marker(par, ax, time, data, widgets)

% set_marker - set browser marker
% -------------------------------
%
% marker = set_marker(par, ax, time, data)
%
% Input:
% ------
%  par - browser
%  ax - axes to display on, perhaps should be channel
%  time - time
%  data - browser state
% 
% Output:
% -------
%  marker - marker

% TODO: this was written for the buttondown context, reconsider interface

% NOTE: 'ax' and 'time' represent the marker struct

%--
% handle input
%--

no_data = (nargin < 4) || isempty(data); store = no_data || ~nargout;

if no_data 
	data = get_browser(par);
end

%--
% display marker
%--

% NOTE: this is like a co-function

marker = display_marker(par, ax, time, data);

if isempty(marker)
	return; 
end 

%--
% stop player
%--

% NOTE: this leads to a more desirable feel for marker cued playback

playing = stop_player;

%--
% get marker file
%--

marker.file = get_current_file(data.browser.sound, marker.time);

%--
% update and store parent state
%--

% NOTE: we also store if the marker state is lost otherwise, this is reasonable and useful but tricky

if store || ~nargout
	data.browser.marker = marker; set(par, 'userdata', data);
end

%--
% generate marker event
%--

% NOTE: when called with state event is 'marker__update', otherwise 'marker__create'

persistent MARKER_DATA MARKER_CONTEXT;

if store
	
	widgets = get_widgets(par, 'marker__create');

	if isempty(widgets)
		return; 
	end

	% NOTE: this context contains non-extension-specific context fields
	
	MARKER_CONTEXT = get_extension_context([], par, data);
	
	% TODO: active extensions are useful for other contexts, review contexts
	
	% NOTE: this is how 'get_active_extensions' should pack
	
	types = get_extension_types;
	
	for k = 1:length(types) 
		
		active = get_active_extension(types{k}, par, data);
		
		if ~isempty(active)
			MARKER_CONTEXT.active.(types{k}) = active;
		end
		
	end
	
	MARKER_DATA = get_widget_data(par, 'marker__create', data);
	
	MARKER_DATA.marker = marker;
	
	for k = 1:length(widgets)
		
		result = widget_update(par, widgets(k), 'marker__create', MARKER_DATA, 0, MARKER_CONTEXT);
		
		set_widget_status(widgets(k), result);
		
	end
	
else
	
	% NOTE: in the case of the move update event we can reliably cache the listening widgets
	
	if nargin < 5
		widgets = get_widgets(par, 'marker__move__update');
	end
	
	if isempty(widgets)
		return; 
	end
	
	MARKER_DATA.marker = marker;
	
	for k = 1:length(widgets)
		
		result = widget_update(par, widgets(k), 'marker__move__update', MARKER_DATA, 0, MARKER_CONTEXT);
		
		set_widget_status(widgets(k), result);
		
	end
	
end

%--
% resume playing if needed
%--

if playing
	browser_play(par, 'page');
end
