function [vertex, control, center] = get_event_display_points(event, nyq)
	
%--
% get event time and frequency limits
%--

start = event.time(1); stop = start + diff(event.time);

% NOTE: we probably want to consider this empty field further down as wel, we want to display a marker

if ~isempty(event.freq)
	low = event.freq(1); high = low + diff(event.freq);
else
	low = 0; high = nyq;
end

%--
% compute event vertices
%--

% NOTE: these are typically used to draw a single line

vertex.x = [start, stop, stop, start, start]'; 

vertex.y = [low, low, high, high, low]';

%--
% compute control points if needed
%--

if (nargout < 2)
	return; 
end

% NOTE: these are used to draw multiple marker lines
	
center.x = 0.5 * (start + stop);

center.y = 0.5 * (low + high);

% control = [ ...
% 	stop, center.y; ...
% 	stop, high; ...
% 	center.x, high; ...
% 	start, high; ...
% 	start, center.y; ...
% 	start, low; ...
% 	center.x, low; ...
% 	stop, low; ...
% 	center.x, center.y ...
% ];

% NOTE: this assignment seems slightly faster

control = [ ...
	stop * ones(1,2), center.x, start * ones(1, 3), center.x, stop, center.x; ...
	center.y, high * ones(1, 3), center.y, low * ones(1, 3), center.y ...
]';
