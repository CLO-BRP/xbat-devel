function [log, ix] = get_browser_log(par, log, data)

% get_browser_log - get a log from a browser by name
% --------------------------------------------------
%
% [log, ix] = get_browser_log(par, log, data)
%
% Input:
% ------
%  par - parent browser
%  log - log or log name
%  data - browser state
%
% Output:
% -------
%  log - log
%  ix - log index

% NOTE: this function can also get all logs

%--
% handle input
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end 

if isempty(par)
	log = []; ix = []; return;
end

if nargin < 3
    data = get_browser(par);
end

if nargin < 2
	log = [];
end

%--
% get all logs from browser and possibly select by name
%--

if isempty(log)
	
	log = data.browser.log; ix = 1:numel(log);
	
else

	if isstruct(log)
		log = log_name(log);
	end

	% NOTE: use logical indexing to select the log and then find the actual index 
	
	ix = strcmp(log_name(data.browser.log), log); log = data.browser.log(ix);

	ix = find(ix);
	
end
