function pass = test_specgram(opt)

if ~nargin
    
    opt.thresh.mag = 20*eps;
    opt.thresh.square = 0;
    opt.thresh.freq = 0;
    opt.thresh.time = 0;
    
    if ~nargout
        pass = opt; return;
    end
    
end

X = randn(100000, 1);

pass = 0;

param = fast_specgram();

%--
% compute matlab and "fast_specgram" spectrograms 
%--

[B_mat, f_mat, t_mat] = matlab_specgram(X, 1, 'norm', param);
[B_x, f_x, t_x] = fast_specgram(X, 1, 'norm', param);

%--
% make sure we're close to matlab.  probably won't be identical (WHY NOT!?)
%--

pass = pass | test_(B_mat, B_x, opt.thresh.mag, 'rms', {'our magnitude spectrum', 'MATLAB''s'});

%--
% verify that our time/frequeny grid is equal to matlab
%--

pass = pass | test_(f_mat, f_x, opt.thresh.freq, 'max', {'our frequency grid', 'MATLAB''s'});

pass = pass | test_(t_mat(:), t_x(:) + 255, opt.thresh.time, 'max', {'our time grid', 'MATLAB''s'});

%--
% verify that power spectrum is squared magnitude
%--

Bn = fast_specgram(X, [], 'norm', param);
Bp = fast_specgram(X, [], 'power', param);

pass = pass | test_(Bn, sqrt(Bp), opt.thresh.square, 'max', {'our norm spectrogram', 'the square root of our power spectrogram'});