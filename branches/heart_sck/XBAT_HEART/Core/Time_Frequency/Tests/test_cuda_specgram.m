function [all_passed, info] = test_cuda_specgram(varargin)

% test_cuda_specgram - test cuda specgram for correctness against FFTW
% --------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'exhaustive' - run extensive benchmark instead of fast benchmark
%  'single' - run with single precision data
%  'double' - run with double precision data
%  'verbose' - print status output
%  'silent' - print nothing
%
% Output
% ------
%  passed = 1 if passed, 0 if failed

% NOTE: default is fast test on double precision data with no messages,
% equivalent to 'test_cuda_specgram('quick', 'double', 'silent')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case 'exhaustive', fast = 0;
            
        case 'single', float = 1;
            
        case 'double', float = 0;
            
        case 'silent', verbose = 0;
            
        case 'verbose', verbose = 1;
    end
end

%----------------------
% SETUP
%----------------------

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if ~float && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end

%--
% initialize output
%--

all_passed = 1;

%--
% load test data
%--

% NOTE: we load data and cast according to input request

X = sound_file_read(which('LarkSparrow.flac')); % NOTE: this is an XBAT sample sound

if float
    X = single(X);
    
    leps = eps('single');
else
    leps = eps('double');
end

%--
% set parameter ranges
%--

% FFT sizes

if fast
    n = [512 1044 2048];  % Arbitrary values for 'quick' test
else
    n = better_fft_sizes(128, 4096, [2, 3, 5, 7, 11]);
end

% Window overlaps

if fast
    overlap = [0.33 0.5 0.875];
else
    overlap = 0.5:0.05:0.975;
end

%----------------------
% RUN TEST
%----------------------

% Display data type being tested

if verbose
    if float
        disp('Testing CUDA Specgram for SINGLE data type.');
    else
        disp('Testing CUDA Specgram for DOUBLE data type.');
    end
    
    % Display 'eps' for reference
    
    disp(['eps = ', num2str(leps)]);
    
    fprintf('\n');
end

%--
% verify that power spectrum is squared magnitude
%--

if verbose
    fprintf('Checking that power spectrum is correctly computed ...');
end

Bn = cuda_specgram(X, hann(512), 512, 256, 0);

Bp = cuda_specgram(X, hann(512), 512, 256, 1);

max_error = abs(Bn(:) - sqrt(Bp(:)));

if max_error > leps * 10
    if verbose
        fprintf('FAILED, max difference = %f\n', num2str(max(max_error)));
    end
    all_passed = 0;
else
    if verbose
        fprintf('PASSED.\n');
    end
end

%--
% Compare CUDA specgram to FFTW specgram over range of parameters
%--

% NOTE: Set cuda_enabled to 0 to prevent fast_specgram from invoking cuda_specgram

clear cuda_enabled;

cuda_enabled 0;

parameter = specgram_parameter;

for i = 1:numel(n)
    
    for j = 1:numel(overlap)
        
        passed = true;
        
        % CUDA specgram
        
        h = hann(n(i));
        
        o = round(n(i) * overlap(j));
        
        output = 0;
        
        try
            start = tic; Y2 = cuda_specgram(X, h, n(i), o, output); elapsed2 = toc(start);
        catch
            passed = false; elapsed2 = inf; nice_catch;
        end
        
        % FFTW specgram
        
        parameter.fft = n(i);
        
        parameter.win_length = n(i) / n(i);
        
        % NOTE: the following parameters do not appear explicitly in CUDA call
        
        parameter.win_type = 'Hann';
        
        parameter.hop = 1 - overlap(j);
        
        try
            start = tic; Y1 = fast_specgram(X, 1, 'norm', parameter); elapsed1 = toc(start);
        catch
            passed = false; elapsed1 = inf; nice_catch;
        end
        
        % Compare results
        
        spec_rms = sqrt(mean((Y1(:) - Y2(:)).^2));
        
        max_error = max(abs(Y1(:) - Y2(:)));
        
        % Set return code if we have an error
        
        if max_error > leps * 10
            passed = false;
        end
        
        % NOTE: this would probably be better using 'fprintf'
        
        if verbose
            disp([
                'FFT/Overlap = [', int2str(n(i)), ', ', num2str(overlap(j), '%3.2f'), '], passed = ', int2str(passed), ', max_error = ' num2str(max_error), ...
                ', elapsed = [', sec_to_clock(elapsed1), ', ', sec_to_clock(elapsed2), '], speed = ', num2str(elapsed1/elapsed2) ...
                ]);
        end
        
        if ~passed
            all_passed = false;
        end
         
    end
    
end

