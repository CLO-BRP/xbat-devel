function [files, destination] = build_cuda(opt)

% build_cuda - build cuda specgram mex files
% ------------------------------------------
%
% build_cuda(opt)
%
% Input:
% ------
%  opt - build options struct, obtained from build_cuda_mex

%--
% clear MEX files so that we can overwrite them
%--

clear cuda_specgram_mex_double;

clear cuda_specgram_mex_single;

%--
% get default options and modify as needed
%--

if ~nargin
    opt = build_cuda_mex;
end

% NOTE: disable default install, we manually install below

manual_install = opt.install;

opt.install = false;

% NOTE: disable default test, we manually test below

manual_test = opt.test;

opt.test = false;

%--
% build single
%--

% NOTE: these options specify single precision floating point and include CUFFT library

opt.nvcc = {'-DFLOAT'}; opt.mex = {'-lcufft', '-DFLOAT'}; 

build_cuda_mex('specgram', opt);

% NOTE: We must rename because we build *_single and *_double from the same source

if manual_install
    movefile(['../cuda_specgram_mex.', mexext], ['../../private/cuda_specgram_mex_single.', mexext])
end

%--
% test single
%--

if manual_test
    if ~test_cuda_specgram('single', 'fast', 'verbose')
        error('Test of cuda_specgram_single_mex FAILED');
    end
end

%--
% build double
%--

% NOTE: the empty 'nvcc' options is implicitly double and include CUFFT library

opt.nvcc = {}; opt.mex = {'-lcufft'};

build_cuda_mex('specgram', opt);

% NOTE: We must rename because we build *_single and *_double from the same source

if manual_install
    movefile(['../cuda_specgram_mex.', mexext], ['../../private/cuda_specgram_mex_double.', mexext])
end

%--
% test double
%--

if manual_test
	if ~test_cuda_specgram('double', 'fast', 'verbose')
        error('Test of cuda_specgram_double_mex FAILED');
	end
end

%--
% report created files and destination
%--

% TODO: the destination is redundant, it should not be needed as output

par = fileparts(fileparts(pwd));

destination = [par, filesep, 'private/cuda'];

% NOTE: the files to consider are files after they've been installed

files = { ...
	fullfile(destination, ['cuda_specgram_mex_double.', mexext]), ...
	fullfile(destination, ['cuda_specgram_mex_single.', mexext]) ...
};



