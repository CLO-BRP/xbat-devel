#include "math.h"

#include "mex.h"

#include "cuda_runtime.h"

#include <cufft.h>

#include "cuda_specgram.h"

//--------------------------------------
// COMPUTE MAGNITUDE ON HOST
//--------------------------------------

void cufft_to_mag (
	register C_TYPE *mag, register cufftComplex *c, int N
)

{
	int k;
	float re, im;
	
	for (k = 0; k < N/2+1; k++)
	{
		re = c[k].x;
		im = c[k].y;
		mag[k] = sqrtf(re * re + im * im);
	}
}

//--------------------------------------
// mxCreateScalar
// Create a matrix with a single value
//--------------------------------------

mxArray* mxCreateScalar(double x)
{
    mxArray* p = mxCreateDoubleMatrix(1,1,mxREAL);
    double*  ptr = mxGetPr(p);
    ptr[0] = x;
    return p;
}

//--------------------------------------
// MEX FUNCTION
//--------------------------------------

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	// result
	mwSize			numTransforms;
	C_TYPE			*hReal = NULL;
	C_TYPE			*hImag = NULL;
	C_TYPE			*dReal = NULL;
	C_TYPE			*dImag = NULL;

	// input
	mwSize			numSamples;
    C_TYPE			*hSamples;
	C_TYPE			*dSamples = NULL;
	
	// window function
	C_TYPE			*hFunction = NULL;
	C_TYPE			*dFunction = NULL;
    
	// transform size
	mwSize			transformSize;
		
	// window
	mwSize			windowSize;
	mwSize			overlap;
	
	int				comp_type;
	
	// CUDA errors
	cudaError_t		cudaErr = cudaSuccess;
    int             cudaHasDoubles = 0;
		
	//--
	// HANDLE INPUT
	//--
	
	if (nlhs < 1) {
        return;    
	}
	
	// Signal

	hSamples = (C_TYPE *) mxGetData(prhs[0]); 
	
	numSamples = (mwSize) (mxGetM(prhs[0]) * mxGetN(prhs[0]));
	
	// Transform size
	
    if (nrhs > 1) {
	 	transformSize = (mwSize) (mxGetScalar(prhs[1]) + 0.5);
	}
	else {
		transformSize = (mwSize) 256;
	}

	// Window size

	if (nrhs > 2)
	{
		if (mxGetM(prhs[2]) > 1 || mxGetN(prhs[2]) > 1)
		{
			windowSize = (mwSize) (mxGetM(prhs[2]) * mxGetN(prhs[2]));
			hFunction = (C_TYPE *) mxGetData(prhs[2]);
		}
		else
		{
			windowSize = (mwSize) (mxGetScalar(prhs[2]) + 0.5);
			hFunction = NULL;
		}
	}
	else
	{
		windowSize = (mwSize) transformSize;
	}

	// Overlap (round to nearest whole sample)
	
	overlap = nrhs > 3 ? (mwSize) (mxGetScalar(prhs[3]) + 0.5) : 0; // NOTE: get block overlap or set no overlap default

	// Output type
	
	comp_type = nrhs > 4 ? (int) mxGetScalar(prhs[4]) : 0; // NOTE: default is comp_type 0, magnitude

	//--
	// ALLOCATE OUTPUT
	//--
	
	numTransforms =  (numSamples - windowSize) / (windowSize - overlap) + 1;

	if (comp_type == 2)	// Complex result
	{
		plhs[0] = mxCreateNumericMatrix(0, 0, MX_CLASS, mxCOMPLEX);
		
		hImag = (C_TYPE *) mxMalloc(numTransforms * (transformSize/2 + 1) * sizeof(C_TYPE));
		
		mxSetImagData(plhs[0], hImag);
	}
	else				// Real result
	{
		plhs[0] = mxCreateNumericMatrix(0, 0, MX_CLASS, mxREAL);
	}
    
    mxSetM(plhs[0], transformSize/2 + 1); mxSetN(plhs[0], numTransforms);
    
    hReal = (C_TYPE *) mxMalloc(numTransforms * (transformSize/2 + 1) * sizeof(C_TYPE));
    
    mxSetData(plhs[0], hReal);

    // Determine if CUDA device supports doubles
	struct cudaDeviceProp deviceProp;
	int cudaDev;
		
	cudaGetDevice(&cudaDev);
	cudaGetDeviceProperties(&deviceProp, cudaDev);
    
    if (deviceProp.major >= 2 || deviceProp.minor >= 3)
    {
        cudaHasDoubles = 1;
    }
    else
    {
        cudaHasDoubles = 0;
    }
	
	//--
	// COMPUTE
	//--
	
	// Allocate device memory for data

	if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dSamples, numSamples * sizeof(C_TYPE)); }

	// Allocate device memory for result, real
	
	if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dReal, numTransforms * (transformSize/2+1) * sizeof(C_TYPE)); }
	
	// Allocate device memory for result, imaginary
	
	if (hImag != NULL)
	{
		if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dImag, numTransforms * (transformSize/2+1) * sizeof(C_TYPE)); }
	}
	
	// Allocate device memory for function, if needed
	
	if (hFunction != NULL)
	{
		if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dFunction, windowSize * sizeof(C_TYPE)); }
	}
	
	// Copy signal data to device

	if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(dSamples, hSamples, numSamples * sizeof(C_TYPE), cudaMemcpyHostToDevice); }
	
	// Copy function data to constant memory on device, if needed
	
	if (hFunction != NULL)
	{
		if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(dFunction, hFunction, windowSize * sizeof(C_TYPE), cudaMemcpyHostToDevice); }
        /*
        if (cudaErr == cudaSuccess)
        {
            if ((cudaHasDoubles && (MX_CLASS == mxDOUBLE_CLASS)) || ((!cudaHasDoubles) && (MX_CLASS == mxSINGLE_CLASS)))
            {
                cudaErr = cudaMemcpyToSymbol("c_func", hFunction, windowSize * sizeof(C_TYPE), 0, cudaMemcpyHostToDevice );
            }
            else
            {
                if (cudaHasDoubles)
                {
                    double *hFd = (double *) malloc(windowSize * sizeof(double));
                    for (int i = 0; i < windowSize; i++) { hFd[i] = (double) ((float *) hFunction)[i]; }
                    cudaErr = cudaMemcpyToSymbol("c_func", hFd, windowSize * sizeof(double), 0, cudaMemcpyHostToDevice );
                    free(hFd);
                }
                else
                {
                    float *hFf = (float *) malloc(windowSize * sizeof(float));
                    for (int i = 0; i < windowSize; i++) { hFf[i] = (float) ((double *) hFunction)[i]; }
                    cudaErr = cudaMemcpyToSymbol("c_func", hFf, windowSize * sizeof(float), 0, cudaMemcpyHostToDevice );
                    free(hFf);
                }
            }
        }
        */
	}

	// Compute specgram
	
	if (cudaErr == cudaSuccess) {
		cudaErr = cuda_specgram(dReal, dImag, dSamples, numSamples, dFunction, windowSize, overlap, transformSize, comp_type, deviceProp.maxGridSize[0]);
	}
	
	// Copy result from device, real
	if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(hReal, dReal, numTransforms * (transformSize/2+1) * sizeof(C_TYPE), cudaMemcpyDeviceToHost); }
	
	// Copy result from device, imaginary
	if (hImag != NULL)
	{
		if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(hImag, dImag, numTransforms * (transformSize/2+1) * sizeof(C_TYPE), cudaMemcpyDeviceToHost); }
	}
			
    // Return error code

    if (nlhs > 1)
    {
        plhs[1] = mxCreateScalar((double) cudaErr);
    }

	// Free device memory
			
	if (dSamples != NULL) { cudaFree(dSamples); }
			
	if (dFunction != NULL) { cudaFree(dFunction); }
	
	if (dReal != NULL) { cudaFree(dReal); }
	
	if (dImag != NULL) { cudaFree(dImag); }
}
