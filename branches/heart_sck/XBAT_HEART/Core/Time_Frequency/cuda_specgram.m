function [Y, e] = cuda_specgram(X, h, N, overlap, comp_type, sum_length, sum_type, sum_quality) %#ok<INUSD>

% cuda_specgram - CUDA-based spectrogram computation
% ------------------------------------------------------
%
% Y = cuda_specgram_mex(X, n, h, o, output)
%
% Input:
% ------
%  X - input signal
%  n - transform size
%  h - sampled window of size n
%  o - overlap as samples, should be less than n
%  output - mode (integer flag to 'indicate 'norm' 0, 'power' 1, 'complex' 2)
%
% Output:
% -------
%  Y - spectrogram magnitude
%  e - CUDA error or success code

% NOTE: we ignore the various summary parameters, we have them defined for signature compatibility

switch class(X)
	
	case 'single'
		[Y, e] = cuda_specgram_mex_single(X, N, single(h), overlap, comp_type);
		
	case 'double'
		if get_cuda_capability < 1.3
			[Y, e] = cuda_specgram_mex_single(single(X), N, single(h), overlap, comp_type);
		else
			[Y, e] = cuda_specgram_mex_double(X, N, double(h), overlap, comp_type);
		end

end

% Recover from error by forcing reload of MEX files

if e
	clear cuda_specgram_single_mex;
	clear cuda_specgram_double_mex;
end


