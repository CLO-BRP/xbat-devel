function rate = get_specgram_rate(parameter, rate)

% get_specgram_rate - get frame rate for spectrogram
% --------------------------------------------------
%
% rate = get_specgram_rate(parameter, rate)
%
% Input:
% ------
%  parameter - spectrogram computation parameters
%  rate - sound rate
%
% Output:
% -------
%  rate - spectrogram frame rate

% NOTE: this works for the traditional spectrogram computation used in the browser

rate = 1 / specgram_resolution(parameter, rate);