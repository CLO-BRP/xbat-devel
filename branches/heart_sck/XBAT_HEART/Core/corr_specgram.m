function BB = specgram_corr(f,t,dt,ch)

% specgram_corr - spectrogram based correlation computation
% ---------------------------------------------------------
%
% BB = specgram_corr(f,t,dt,ch)
%
% Input:
% ------
%  f - sound structure or type of sound
%  t - initial time
%  dt - time duration
%  ch - channels to correlate

%--
% create browser structure
%--

browser = browser_create(f);

%--
% read samples from file group
%--

X = sound_read(browser.sound,'time',t,dt,ch);

%--
% compute channels spectrograms
%--

% get spectrogram options

sopt = browser.specgram;
	
% compute spectrogram dB scaled power according to display options	

for k = 1:length(ch)
	
	[B{k},F{k},T{k}] = fast_specgram(X(:,k), ...
							sopt.fft, ...
							browser.sound.samplerate, ...
							feval(sopt.win.type,sopt.win.length), ...
							sopt.hop, ...
							'power' ...
						);			
% 	B{k} = lut_dB(B{k});
			
end

%--
% compute channels correlation
%--

for j = 1:length(ch)
for k = j:length(ch)
	BB{k,j} = B{k}'*B{j};
end
end
