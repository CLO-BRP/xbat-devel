function [status, result] = build_cuda_mex(name, opt)

% build_cuda_mex - relying on various conventions
% -----------------------------------------------
%
% [status, result] = build_cuda_mex(name, opt)
%
% opt = build_cuda_mex
% 
% Input:
% ------
%  name - of function 
%  opt - struct
%
%    arch - level (def: from current hardware)
%    test - run
%    install - to private
%    link - link or just build object files
%    nvcc - options
%    mex - options
%
% Output:
% -------
%  status - of build
%  result - message
%
% NOTE: we expect source to be composed of at least a cuda_[name].cu and cuda_[name]_mex.cpp file

% TODO: output resulting files, therefore reconsider output signature

%--
% set and possibly output default options
%--

if nargin < 2
    opt.install = true;
    
    opt.test = true;
    
    opt.link = true;
    
    opt.nvcc = {};
    
    opt.mex = {};
    
	try
		[ignore, opt.arch] = get_cuda_capability;
	catch
		opt.arch = []; disp(' '); db_disp 'Failed to get CUDA capability.';
	end
	
    if ~nargin
        status = opt; return;
    end
end

setup_vs90;

% TODO: we should refactor and move this to 'cuda_root'

info = get_windows_info; 

if isfield(info, 'cuda_bin_path')
	% NOTE: this is the way things should be!
	
	cuda_lib_root = info.cuda_lib_path;
	
	cuda_bin_root = info.cuda_bin_path;
else
	% NOTE: these may not be correct, they are inferences based on default observed installs on windows
	
	cuda_lib_root = fullfile(cuda_root, 'lib');

	cuda_bin_root = fullfile(cuda_root, 'bin');
	
	if info.arch == 64
		cuda_lib_root = [cuda_lib_root, '64'];
		
		cuda_bin_root = [cuda_bin_root, '64'];
	end
end

%--
% handle name or full file input
%--

[root, name] = fileparts(name);

if isempty(root)
	% NOTE: when given a name first look for template file, then look for cpp file
	
	file = which(['cuda_', name, '_mex.cpp.template']);
	
	mex_template = ~isempty(file);
	
	if ~mex_template
		file = which(['cuda_', name, '_mex.cpp']);
		
		if isempty(file)
			error(['Unable to find conventional CUDA MEX file with name ''', name, '''.']);
		end
	end
else
	% NOTE: the last statement trims the conventional prefix and suffix from the name
	
	file = name; [ignore, name, ignore] = fileparts(file); name = name(6:end - 4); %#ok<NASGU,*ASGLU>
end

root = fileparts(file);

%--
% expand template mex file if needed
%--

if mex_template
	generate_typed_mex(file);
	
	[root, file] = fileparts(file); file = fullfile(root, file);
end

%--
% get and process corresponding cu code
%--

template = fullfile(root, ['cuda_', name, '.cu.template']);

if exist(template, 'file')
	generate_typed_cu(template);
end

cu = fullfile(root, ['cuda_', name, '.cu']);
cfile = fullfile(root, ['cuda_', name, '.cu.cpp']);

%--
% nvcc build
%--

start = pwd; cd(root);

if exist(cu, 'file')
	
	[bin, include] = cc_root;
	
	command = ['"', fullfile(cuda_bin_root, 'nvcc'), '" -I"', include ,'" -cuda -ccbin "', bin, '" -arch ', opt.arch, ' "', cu, '" ', str_implode(opt.nvcc, ' ')];
	
	disp(command); disp(' ');
	
	[status, result] = system(command);
	
	if status
		error(result);
    end
	
 	command = ['"', fullfile(cuda_bin_root, 'nvcc'), '" -I"', include ,'" -c -ccbin "', bin, '" -arch ', opt.arch, ' "', cfile, '" ', str_implode(opt.nvcc, ' ')];
	
	disp(command); disp(' ');
	
	[status, result] = system(command);

	if status
		error(result);
	end
	
	if opt.link
        mex('-v', ['-I' fullfile(cuda_root, 'include')], ['-L' cuda_lib_root],'-lcudart', file, ['cuda_', name, '.cu.obj '], opt.mex{:});
    end
    
%--
% simple build
%--

% NOTE: the main examples here are the MEX files that get CUDA device properties

else
	str = ['mex -v "', file, '" -I', fullfile(cuda_root, 'include'), ' -L', cuda_lib_root, ' -lcuda -lcudart'];
		
	eval(str);
end

%--
% test if test is available
%--

testfile = ['test_cuda_', name, '.m'];

if opt.test && opt.link && exist(testfile, 'file')
	try
		feval(testfile(1:end - 2));
    catch
		nice_catch; return;
	end
end

%--
% move CUDA MEX to private directory
%--

if opt.install && opt.link
	out = ['cuda_', name, '_mex.', mexext];
	
	clear(out); movefile(out, '../private/.');
end

cd(start);