function tool = get_pexports

% get_pexports - tool, install if needed
% --------------------------------------
%
% tool = get_pexports
%
% Output:
% -------
%  tool - pexports

% TODO: include platform check and note when this does not make sense 

tool = get_tool('pexports.exe');

current = 'http://xbat.org/downloads/installers/pexports-0.43.zip';

if isempty(tool) && install_tool('pexports', current)

	tool = get_tool('pexports.exe');
end