function root = mingw_root(in)

% mingw_root - set and get root of mingw compiler
% -----------------------------------------------
%
% root = mingw_root(root)
%
% Input:
% ------
%  root - root to set
%
% Output:
% -------
%  root - value of mingw root

%--
% handle set, get, and error
%--

switch nargin

	case 0
		root = get_env('mingw_root');
		
		% NOTE: the basic reason we install MinGW is to use 'gcc', find it and determine the root
		
		if isempty(root)
			tool = get_tool('gcc.exe');
			
			if isempty(tool)
				root = ''; return;
			end 
			
			% NOTE: typically the tool is in the 'bin' directory, we go one level up
			
			root = fileparts(tool.root);
							
			set_env('mingw_root', root);
		end
		
		% NOTE: this is a lame backup plan. can we easily do better?
		
		if isempty(root)
			root = 'C:\MinGW';
			
			if exist(root, 'dir') == 7
				set_env('mingw_root', root);
			else
				root = '';
			end
		end

	case 1
		% NOTE: this works like set verify
		
		if ~ischar(in)
			error('Proposed path must be string.');
		end
		
		if ~exist(in, 'dir')
			error('Proposed path does not seem to exist.');
		end

		root = in; set_env('mingw_root', root);

	otherwise
		error('Improper number of input arguments.');

end
