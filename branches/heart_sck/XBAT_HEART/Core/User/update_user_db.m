function update_user_db(user, force)

%--
% set no force default
%--

if nargin < 2
	force = 0;
end

% NOTE: here we get the users from the filesystem

if ~nargin || isempty(user)
	user = get_users;
end

%--
% make sure we have a users database
%--

% HACK: we do this because of the unfortunately named field

remove = {'default', 'library'};

if force
	delete(user_db);
end 

if ~exist(user_db, 'file')
	
	% TODO: factor the database creation
	
	%--
	% create user table
	%--
	
	% NOTE: we don't flatten because we want user fields to be open
	
	opt = create_table; opt.flatten = 0;
	
	prototype = rmfield(get_active_user, remove);
	
	sqlite(user_db, create_table(prototype, 'users', opt));

	%--
	% create library table
	%--
	
	% NOTE: it is likely that this model may have other useful fields
	
	prototype = struct; prototype.id = 1; prototype.path = 'string';
	
	sqlite(user_db, create_table(prototype, 'libraries'));
	
	%--
	% establish subscription relation
	%--
	
	establish_relation(user_db, 'users', 'libraries');
	
end

%--
% insert libraries so that we may assign identifiers
%--

% library = {user.library};

user = rmfield(user, remove);

sqlite(user_db, sqlite_array_insert('users', user));


