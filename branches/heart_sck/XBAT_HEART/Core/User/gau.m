function handle = gau

% gau - get active user struct
% ----------------------------
%
% user = gau
%
% Output:
% -------
%  user - active user

handle = get_active_user;