function reopen_session

% reopen_session - close and reopen current session
% -------------------------------------------------
%
% reopen_session

save_session reopen-session;

close all;

open_session reopen-session;

