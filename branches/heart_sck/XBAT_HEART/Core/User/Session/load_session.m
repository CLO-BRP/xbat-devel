function session = load_session(name, user)

% load_session - load user session
% --------------------------------
%
% session = load_session(name, user)
%
% Input:
% ------
%  name - session name
%  user - user 
%
% Output:
% -------
%  session - session name and content

%--
% set default active user
%--

if nargin < 2
	user = get_active_user;
end 

%--
% select most recent session default
%--

% NOTE: this code is quirky, it should be leaner

if nargin < 1
	name = get_recent_session(user); 
end

in = get_sessions(name, user);

%--
% return if there is no session to load
%--

if isempty(in) || isempty(in{1}), session = []; return; end

%-------------------------------
% LOAD SESSION
%-------------------------------

%--
% create session struct
%--

session.user = user.name;

% NOTE: the name is stored in the filename, a similar pattern could be used elsewhere

session.name = name;

in = get_session_file(in{1}, user); 

opt = file_readlines; opt.comment = '%'; opt.skip = 1;

session.content = file_readlines(in, [], opt);

%--
% display contents if no output is requested
%--

if ~nargout
	disp(' '); iterate(@disp, session.content); disp(' '); clear session;
end 

