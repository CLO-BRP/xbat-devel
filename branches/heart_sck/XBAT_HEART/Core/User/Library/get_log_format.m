function [format, context] = get_log_format(log)

% get_log_format - get log format extension from log
% --------------------------------------------------
%
% [format, context] = get_log_format(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  format - format extension
%  context - context

%--
% get format name from input
%--

% NOTE: we could be a bit more paranoid here

if ischar(log)
	name = log; 
else
	name = log.format; 
end

%--
% get format extension from root browser
%--

if nargout < 2
    format = get_extensions('log_format', 'name', name); return;
end

% NOTE: this context may not be adequate, formats are a different beast

[format, ignore, context] = get_browser_extension('log_format', 0, name);

if isstruct(log)
	context.log = log;
end


