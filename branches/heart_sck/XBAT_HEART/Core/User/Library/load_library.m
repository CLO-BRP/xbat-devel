function [lib, info] = load_library(file)

% load_library - load library from file
% -------------------------------------
%
% lib = load_library(file)
%
% Input:
% ------
%  file - library file
%
% Output:
% -------
%  lib - library

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% check for library file
%--

% NOTE: return empty if there is no file to load from

if ~exist(file, 'file')
	lib = []; return;
end

%----------------------------
% LOAD LIBRARY FROM FILE
%----------------------------

%--
% load library
%--

content = load(file);

% NOTE: return empty if there is no library in file

if ~isfield(content, 'lib')
	lib = []; return;
end

lib = content.lib;

% TODO: don't add the filesep at the end and deal with the consequences

lib.path = [fileparts(file), filesep];

%--
% load info string
%--

% TODO: store info string library summary in file

if (nargout > 1)
	info = [];
end