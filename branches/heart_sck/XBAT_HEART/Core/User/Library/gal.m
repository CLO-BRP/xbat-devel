function handle = gal(varargin)

% gal - get active library struct
% -------------------------------
%
% handle = gal(user)
%
% Input:
% ------
%  user - user (def: active user)
%
% Output:
% -------
%  handle - active library

handle = get_active_library(varargin);