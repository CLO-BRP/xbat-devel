function [value, exists] = get_user_preference(user, field, strict)

% get_user_preference - get user preference value
% -----------------------------------------------
%
% [value, exists] = get_user_preference(user, field, strict)
%
% Input:
% ------
%  user - user
%  field - preference name
%  strict - be strict about preference availability
%
% Output:
% -------
%  value - preference value
%  exists - indicator

%--
% handle input
%--

% NOTE: we are loose by default

if nargin < 3
	strict = 0;
end 

if nargin < 1 || isempty(user) 
	user = get_active_user;
end

%--
% get user preference value
%--

if nargin < 2
	value = user.prefs; exists = 1; return;
end

% NOTE: an preference field not available will return empty or throw an error if we are 'strict'

if ~isvarname(field) 
	field = genvarname(field);
end 

try
	value = user.prefs.(field); exists = 1;
catch 
	if strict
		error(['Undefined user preference ''', field, '''.']);
	else
		value = []; exists = 0;
	end
end
