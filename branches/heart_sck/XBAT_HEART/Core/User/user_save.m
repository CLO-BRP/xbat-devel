function user = user_save(user)

% user_save - save user to file
% -----------------------------
%
% user = user_save(user)
%
% Input:
% ------
%  user - user structure
%
% Output:
% -------
%  user - saved user

%--
% update modified date
%--

user.modified = now;

%--
% save user information
%--

try
	save(get_user_file(user), 'user'); 
catch
	nice_catch(lasterror, ['Failed to save user ''', user.name, '''.']);
end

% NOTE: this is experimental code

% try
% 	update_user_db(user);
% catch
% 	nice_catch; 
% end