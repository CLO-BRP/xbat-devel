function value = views_root(value)

% views_root - views root path
% ----------------------------
%
% value = views_root(value)
%
% Input:
% ------
%  value - value to set
%
% Output:
% -------
%  value - value of environment variable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1631 $
% $Date: 2005-08-23 12:41:39 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

%--
% set name and default
%--

name = mfilename;

if (~nargin)
	
	default = [app_root, filesep, 'Core', filesep, 'Export', filesep, 'Views'];
	
	% NOTE: ensure that this views root directory exists
	
	default = create_dir(default);
	
	if (isempty(default))
		error(['Unable to create default views root.']);
	end
	
end

%--
% get or set value
%--

switch (nargin)
	
	case (0)

		value = get_env(name);
		
		if (isempty(value))
			value = default; set_env(name,value);
		end

	case (1)
		
		set_env(name,value);

end
	