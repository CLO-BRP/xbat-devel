function flag = set_selection_controls(h, event, mode, data)

% set_selection_controls - update selection palette on selection change
% ---------------------------------------------------------------------
%
% set_selection_controls(h, event, mode, data)
%
% Input:
% ------
%  h - parent figure
%  event - selection event
%  mode - update mode 'start' or 'move'
%  data - parent userdata
%
% Output:
% -------
%  flag - update success flag

% NOTE: at the moment the flag output is not very informative

%------------------------
% PERSISTENT STORES
%------------------------

persistent TIME_START TIME_SLIDER TIME_MODE;

persistent FREQ_MIN FREQ_SLIDER FREQ_MODE;

%------------------------
% HANDLE INPUT
%------------------------

%--
% get userdata if needed
%--

if (nargin < 4) || isempty(data)
	data = get_browser(h);
end

%--
% get palette
%--

pal = get_palette(h, 'Selection', data);

% NOTE: quick return if there are no controls to update

if isempty(pal)	
	flag = 0; return; 
end 

%--
% set mode if needed
%--

% NOTE: the default does more work, but ensures proper display

if (nargin < 3) || isempty(mode)
	mode = 'start';
end

%--
% get selection event if needed
%--

if (nargin < 2) || isempty(event)
	event = data.browser.selection.event;
end

% NOTE: quick return on empty selection

if isempty(event)
	flag = 0; return;
end

%------------------------
% UPDATE
%------------------------

switch mode
	
	%------------------------
	% START
	%------------------------
	
	% NOTE: this get handles and sets range properties for controls
	
	case 'start'
		
		% NOTE: we really don't need all control values, this is just simpler 

		values = get_control_values(pal);
		
		%------------------------
		% TIME CONTROLS
		%------------------------

		%--
		% get time controls handles and mode
		%--
		
		handles = get_control(pal, 'start_time', 'handles');
		
		TIME_START = handles.obj;

		handles = get_control(pal, 'time_slider', 'handles');
		
		TIME_SLIDER = handles.obj;
		
		TIME_MODE = strcmpi(values.time_mode{1}, 'duration');

		%--
		% get axes limits using event channel
		%--
		
		% NOTE: this assumes the event axes tag is the channel index
		
		ax = findobj(h, 'type', 'axes', 'tag', int2str(event.channel)); 
		
		xlim = get(ax, 'xlim');
		
		%--
		% set start time control range and value
		%--

		set(TIME_START, 'min', xlim(1), 'max', xlim(2)); slider_sync(TIME_START); 

		%--
		% set duration or end time range and value
		%--
		
		if TIME_MODE % duration 

			set(TIME_SLIDER, ...
				'min', 0, 'max', data.browser.page.duration ...
			);

		else % end time 

			set(TIME_SLIDER, ...
				'min', xlim(1), 'max', xlim(2) ...
			);

		end
		
		slider_sync(TIME_SLIDER);
		
		%------------------------
		% FREQUENCY CONTROLS
		%------------------------

		% NOTE: the frequency limits do not change as the time limits do
		
		%--
		% get frequency controls handles and mode
		%--

		handles = get_control(pal, 'min_freq', 'handles');
		
		FREQ_MIN = handles.obj;

		handles = get_control(pal, 'freq_slider', 'handles');
		
		FREQ_SLIDER = handles.obj;
		
		FREQ_MODE = strcmpi(values.freq_mode{1}, 'bandwidth');
		
		%------------------------
		% UPDATE VALUES
		%------------------------
		
		% NOTE: move only updates values, this call makes things a bit more dry
		
		set_selection_controls(h, event, 'move', data)
		
	%------------------------
	% MOVE
	%------------------------
	
	% NOTE: this only updates the values of the controls
	
	case 'move'

		%------------------------
		% TIME CONTROLS
		%------------------------

		%--
		% set start time control
		%--

		set(TIME_START, 'value', event.time(1)); slider_sync(TIME_START);

		%--
		% set duration or end time control
		%--

		if TIME_MODE
			value = diff(event.time);
		else
			value = event.time(2);
		end

		set(TIME_SLIDER, 'value', value); slider_sync(TIME_SLIDER);

		%------------------------
		% FREQUENCY CONTROLS
		%------------------------

		%--
		% set min frequency control
		%--

		set(FREQ_MIN, 'value', event.freq(1)); slider_sync(FREQ_MIN);

		%--
		% set bandwidth or max frequency control
		%--

		if FREQ_MODE
			value = diff(event.freq);
		else
			value = event.freq(2);
		end

		set(FREQ_SLIDER, 'value', value); slider_sync(FREQ_SLIDER);
		
end