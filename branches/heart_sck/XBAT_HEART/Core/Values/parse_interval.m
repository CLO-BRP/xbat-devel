function [int, type] = parse_interval(str)

% parse_interval - parse interval string
% --------------------------------------
%
% [int, type] = parse_interval(str)
%
% Input:
% ------
%  str - interval strings
%
% Output:
% -------
%  int - interval end points
%  type - interval type code

%---------------------------------------------
% HANDLE INPUT
%---------------------------------------------

%--
% handle multiple strings recursively
%--

if iscellstr(str)
	
	for k = 1:length(str)
		[int(k, :), type(k)] = parse_interval(str{k});
	end
	
	return;
	
end

%---------------------------------------------
% PARSE STRING
%---------------------------------------------
	
% NOTE: this function can parse time intervals

%--
% split interval string
%--

[a, b] = strtok(str, ',');

%--
% parse lower limit
%--

L = (a(1) == '['); 

a = a(2:end);

if findstr(a, ':')
	a = clock_to_sec(a);
else
	a = str2double(a); 
end

if isempty(a)
	int = [0, 0]; type = -1; return;
end

%--
% parse upper limit
%--

U = (b(end) == ']'); 

b = b(2:end - 1);

if findstr(b, ':')
	b = clock_to_sec(b);
else
	b = str2double(b); 
end

if isempty(b)
	int = [0, 0]; type = -1; return;
end

%--
% check interval order
%--

if a > b
	error('Improperly ordered interval.');
end

%--
% output endpoints and interval type
%--

int = [a, b]; type = (2 * L) + (1 * U);
	