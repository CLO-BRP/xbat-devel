function tree = metric_tree(data, opt)

% build_metric_tree - build metric tree from data
% -----------------------------------------------
%
% tree = build_metric_tree(data)
%
% Input:
% ------
%  data - data matrix
%
% Output:
% -------
%  tree - metric tree

% TODO: consider moving 'create_node' into this function to contain 'global'

global tree

%--
% set default options
%--

if nargin < 2
	
	% TODO: add points accesor function
	
	opt.distance = @distance;
	
	opt.split = 'median';
	
	opt.spill = 0;
	
	opt.stop = 16;
	
	if ~nargin
		tree = opt; return;
	end
	
end

%--
% initialize tree
%--

% NOTE: this clears the current global tree

tree = struct;

tree.data = data;

tree.opt = opt;

tree.id = 1;

ix = 1:size(data, 1);

tree.node = create_node([], get_random_point(ix), ix);

%--
% grow tree
%--

% NOTE: splitting the root node initiates a recursion that ends when the tree is done

tree = split_node(1);

%--
% compute tree centers
%--

% NOTE: computing the center of the root node initiates a recursion to get all centers

% TODO: compute node radii, is there an efficient way?

node_center(1);


%------------------------
% SPLIT_NODE
%------------------------

function tree = split_node(id)

global tree

%--
% return empty if we are done splitting
%--

[node, ix] = get_node(tree, id);

% NOTE: this leaves the tree unchanged

if numel(node.points) <= tree.opt.stop
	return;
end

%--
% get pivots and split current node points
%--

[p1, p2] = get_pivots(node.points, tree.data);

% NOTE: this function shows why we should have a tree object

[ix1, ix2, decision] = split_points(node.points, p1, p2, tree.data, tree.opt.split, tree.opt.spill);

%--
% create and add child nodes
%--

node1 = create_node(id, p1, ix1); tree.node(end + 1) = node1;

node2 = create_node(id, p2, ix2); tree.node(end + 1) = node2;

% NOTE: here we update decision function parameters and parent node links to children

node.decision = decision;

node.child.left = node1.id; node.child.right = node2.id;

tree.node(ix) = node;

%--
% split recently added child nodes (recursion)
%--

tree = split_node(node1.id);

tree = split_node(node2.id);


%------------------------
% NODE_CENTER
%------------------------

function [center, n] = node_center(id)

global tree

%--
% get node from tree
%--

% NOTE: we get the node index so we can modify the global tree

[node, ix] = get_node(tree, id);

%--
% compute node center
%--

if ~node_has_children(node)
	
	% NOTE: this computes the center of leaf nodes
	
	n = numel(node.points); points = get_points(node.points, tree.data); center = sum(points, 1) / n;
	
	% TODO: consider caching all distances here
	
	tree.node(ix).radius = max(distance(center, points));
	
else
	
	% NOTE: the center of higher level nodes is computed from the children
	
	[center1, n1] = node_center(node.child.left); [center2, n2] = node_center(node.child.right);
	
	n = n1 + n2; center = (n1 * center1 + n2 * center2) / n;
	
end

%--
% store node center
%--

tree.node(ix).center = center;


%------------------------
% NODE_POINTS
%------------------------

% TODO: this function is not currently used, significant refactoring is needed

function points = node_points(id)

global tree

%--
% get node
%--

node = get_node(tree, id);

%--
% get points
%--

if ~node_has_children(node)
	points = [node_points(node.child.left), node_points(node.child.right)];
else
	points = node.points;
end

