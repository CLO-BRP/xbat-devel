function [center, d, ix] = points_circle(ix, data)

%--
% get data points
%--

points = get_points(ix, data);

%--
% compute center 
%--

center = sum(points, 1) ./ numel(ix);

%--
% compute sorted distances to center
%--

d = distance(center, points);

[d, ix1] = sort(d, 'descend'); 

% NOTE: we dereference the local indices to get data indices

ix = ix(ix1);

