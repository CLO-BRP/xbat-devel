function value = node_has_children(node)

value = ~isempty(node.child.left) || ~isempty(node.child.right);