function format = format_libsndfile

% format_libsndfile - create format structure
% -------------------------------------------
%
% format = format_libsndfile
%
% Output:
% -------
%  format - generic format structure

%--
% create format structure
%--

format = format_create;

%--
% fill function and info fields of format structure (set)
%--

% NOTE: this is a base format, no extensions are associated to it

format.home = 'http://www.mega-nerd.com/libsndfile/';

format.info = @info_libsndfile;

format.read = @read_libsndfile;

format.write = @write_libsndfile;

format.seek = 1;

% NOTE: this is not true for all supported children formats

format.compression = 0;
