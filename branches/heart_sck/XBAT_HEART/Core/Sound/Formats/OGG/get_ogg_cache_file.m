function [cache, status, result] = get_ogg_cache_file(file, force)

% get_ogg_cache_file - get decoded ogg file from cache
% ----------------------------------------------------
%
% [cache, status, result] = get_ogg_cache_file(file, force)
%
% Input:
% ------
%  file - ogg file
%  force - decode even if file exists
%
% Output:
% -------
%  cache - decoded file from cache 
%  status - from decoder
%  result - from decoder

%--
% set no force default
%--

if nargin < 2
	force = 0;
end

%--
% build cache file name
%--

[p1, p2] = fileparts(file);

cache = fullfile(ogg_cache_root, [p2, '.wav']);

%--
% check for file, decode if needed
%--

% NOTE: the file exists, happily move on, output indicates no action

if exist(cache, 'file') && ~force
	status = []; result = ''; return;
end

% NOTE: we need to decode the file

persistent DECODER

if isempty(DECODER)
	
	DECODER = get_tool('oggdec.exe');

	if isempty(DECODER)
		error('Unable to find OGG decoder.');
	end
	
end

str = ['"', DECODER.file, '" -o "', cache, '" "', file, '"']; 

[status, result] = system(str);

%--
% check decoder succeeded
%--

% NOTE: we include the decoder call status and result, it may help when the following happens

if ~exist(cache, 'file')
	cache = '';
end

% TODO: consider integrating the ideas from 'ogg_cache_reduce' here
