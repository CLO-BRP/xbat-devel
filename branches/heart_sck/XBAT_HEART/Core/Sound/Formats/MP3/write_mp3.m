function out = write_mp3(f, X, r, opt)

% write_mp3 - write samples to sound file
% ----------------------------------------
%
%  opt = write_mp3(f)
%
% flag = write_mp3(f, X, r, opt)
%
% Input:
% ------
%  f - file location
%  X - samples to write to file
%  r - sample rate
%  opt - format specific write options
%
% Output:
% -------
%  opt - format specific write options
%  flag - success flag

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set and possibly output encoding options
%--

% NOTE: we get the options as if coming from WAV

if (nargin < 4) || isempty(opt)	
	opt = encode_mp3('temp.wav');
end 

%--
% return default options
%--
	
if nargin < 2
	out = opt; return;
end

%---------------------------------------
% ENCODE USING CLI HELPER
%---------------------------------------

%--
% create temporary file
%--

% TODO: consider hashing part of the data and not deleting the files

% NOTE: we could hash say a thousand samples from start and end along with the number of samples and rate

temp = [tempname, '.wav'];

% NOTE: the temporary file is created using default encoding options

write_libsndfile(temp, X, r);

%--
% encode temporary file to mp3
%--

out = sound_file_encode(temp, f, opt);

out = ~isempty(out);

%--
% delete temporary file
%--

delete(temp);