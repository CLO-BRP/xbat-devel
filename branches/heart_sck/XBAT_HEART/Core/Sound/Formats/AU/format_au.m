function format = format_au

format = format_libsndfile;

format.name = 'Audio File Format (AU)';

format.ext = {'au'};