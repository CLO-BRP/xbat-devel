function out = decode_flac(f1, f2)

% decode_flac - decode flac to wav
% --------------------------------
%
% com = decode_flac(f1, f2)
%
% Input:
% ------
%  f1 - input file
%  f2 - output file
%
% Output:
% -------
%  com - command to execute to perform decoding

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%--
% persist location of command-line helper
%--

persistent FLAC;

if isempty(FLAC)
	FLAC = get_tool('flac.exe');
end

%--
% build command string
%--

out = [ ...
	'"', FLAC.file, '" -d --output-name="', f2, '" "', f1, '"' ...
];
