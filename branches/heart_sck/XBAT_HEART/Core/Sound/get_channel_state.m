function state = get_channel_state(context, ext, channel)

% get_channel_state - get extension state for channel from context
% ----------------------------------------------------------------
%
% state = get_channel_state(context, ext, channel)
%
% Input:
% ------
%  context - context
%  ext - extension
%  channel - channel
%
% Output:
% -------
%  state - state for extension in channel from context

%--
% get extension from context
%--

ext = get_context_extension(context, ext.subtype);

% NOTE: this is a hack to resolve some problems with the packing of active extensions

ext = unpack_active_extension(ext);

%--
% get channel state from context extension
%--

state = struct;

if isfield(ext.state, 'channel')

	ix = find([ext.state.channel] == channel);

	% NOTE: state store has 'channel' and 'state' fields, both are used here
	
	if ~isempty(ix)
		state = ext.state(ix).state;
	end

end