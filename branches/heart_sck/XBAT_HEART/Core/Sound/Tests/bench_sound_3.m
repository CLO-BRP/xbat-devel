function result = bench_sound_3(file)

%--
% handle input
%--

if nargin < 2
	opt.trials = 32;
	
	if nargout && ~nargin
		result = opt; return;
	end
end

if ~nargin
	% NOTE: this file is in 'Samples'
	
	file = which('BlackCappedVireo.flac');
end

info = sound_file_info(file);

% TODO: compare the performance of multiple file versus multiple-channel file

if info.channels > 1
	error('Test file should be a single channel file.');
end

%--
% create test files to read
%--

disp(' '); 

disp(['Reading ', file]); disp(' ');

[samples, rate] = sound_file_read(file);

n = size(samples, 1); N = 10^6;

if n < N
	disp('Replicating file to achieve minimum number of samples ...'); 
		
	samples = repmat(samples, ceil(N / n), 1); n = size(samples, 1);
		
	disp(['samples = ', int2str(n)]); disp(' ');
end

formats = get_writeable_formats; here = create_dir(fileparts(mfilename('fullpath')), 'Files');

test = cell(size(formats));

for k = 1:numel(formats)
	test{k} = fullfile(here, ['test.', formats(k).ext{1}]);
	
	writing = zeros(1, opt.trials);
	
	disp(['Writing ', test{k}, ' ...']);
	
	for j = 1:numel(opt.trials)
		delete(test{k}); start = clock;
		
		info = sound_file_write(test{k}, samples, rate);
		
		writing(j) = etime(clock, start);
		
		if j == 1			
			disp(['samples = ', int2str(info.samples)]); disp(' ');
		end
	end
	
	% TODO: these numbers do not seem reliable!
	
	result.write.(formats(k).ext{1}).rate = n ./ (10^6 * writing);
end

%--
% time read for different file formats
%--

for k = 1:numel(formats)
	disp(['Reading ', test{k}, ' ...']);
	
	reading = zeros(1, opt.trials);
	
	for j = 1:opt.trials
		start = clock;
	
		sound_file_read(test{k});
				
		reading(j) = etime(clock, start);
	end
	
	disp(' ');
	
	% NOTE: it is probably not a good idea to compute the rate before the statistics
	
	result.read.(formats(k).ext{1}).rate = n ./ (10^6 * reading);
end

%--
% create plots
%--

par = fig; ax = harray(par, layout_create(1, 1));

types = fieldnames(result.read); X = zeros(opt.trials, numel(types));

for k = 1:numel(types)
	X(:, k) = result.read.(types{k}).rate;
end

boxplot(ax(end), X, 'labels', upper(types));

title('READ PERFORMANCE');

ylabel('MS/SEC');
