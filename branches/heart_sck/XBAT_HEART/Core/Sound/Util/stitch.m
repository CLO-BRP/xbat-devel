function result = stitch_files(file, files, rate, opt)

% TODO: this is part of a set of tools to manipulate sound files, create a little module

%------------------
% HANDLE INPUT
%------------------

%--
% set and possibly output default options
%--

if nargin < 4
	opt.get_rate = @mode; 
end

if ~nargin
	result = opt; return;
end

if nargin < 3
	rate = [];
end

%------------------
% SETUP
%------------------

%--
% get sound file info for input files and check number of channels
%--

for k = 1:numel(files)
	info(k) = sound_file_info(files{k});
end

if any([info.channels] > 1)
	error('Only single channel files are supported.');
end

%--
% figure out what the output rate will be and whether resampling is needed
%--

rates = unique([info.samplerate]);

if isempty(rate)
	
	resample = numel(rates) > 1;

	if resample
		rate = opt.get_rate([info.samplerate]);
	else
		rate = rates;
	end
	
else
	
	resample = any(rates ~= rate);
	
end

%------------------
% STITCH
%------------------

%--
% read from input and create stitched file
%--

ix = 0; n = 10 * info.samplerate; N = info.samples;

for k = 1:numel(files)
	X(:, k) = sound_file_read(files{k}, ix, n, N);
end

sound_file_write(file, X, rate);



