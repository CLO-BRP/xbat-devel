function playlist = create_playlist(playlist, source, format)

%--
% handle input
%--

if nargin < 3
	format = 'mp3';
end

if nargin < 2 || isempty(source)
	source = pwd;
end

if nargin < 1 || isempty(playlist)
	playlist = [tempname, '.m3u'];
end

%--
% create playlist
%--

content = what_ext(source, format); files = content.(format);

if isempty(files)
	playlist = ''; return;
end

output = get_fid(playlist, 'wt');

file_writelines(output.fid, {'#EXTM3U'});

% TODO: allow for the use of relative names if asked to

files = strcat(source, filesep, files);

file_writelines(output.fid, files);

fclose(output.fid);
