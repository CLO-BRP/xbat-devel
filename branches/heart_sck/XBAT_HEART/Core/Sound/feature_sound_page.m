function [page, context] = feature_sound_page(page, context)

% feature_sound_page - compute requested features on a sound page
% ---------------------------------------------------------------
%
% [page, context] = feature_sound_page(page, context)
%
% Input:
% ------
%  page - page
%  context - context
%
% Output:
% -------
%  page - page
%  context - context

%---------------------
% HANDLE INPUT
%---------------------

%--
% make sure that we have and exit with a feature field
%--

% NOTE: we initially create an empty feature array
	
if ~isfield(page, 'feature')
	
	page.feature.name = []; page.feature.feature = []; page.feature(1) = [];

end

%--
% get feature extension from context
%--

% TODO: this does not match our model for features, take full extension

[ext, in] = get_context_extension(context, 'sound_feature');

% NOTE: there is nothing to do if we do not have a proper feature in the context

if isempty(ext)
	return;
end

% TODO: this fails for multiple extensions! This should not be handled here

%---------------------
% FEATURE PAGE
%---------------------

%--
% check page for existing feature
%--

% NOTE: we assume that found features are already currently computed

if ~isempty(page.feature) && ismember(ext.name, {page.feature.name})
	return;
end

%--
% add feature to page
%--

page.feature(end + 1).name = ext.name; 

page.feature(end).feature = struct;

%--
% consider if feature handles multiple channels at once
%--

% NOTE: it is not clear that at this level we need to compile

if ext.multichannel
	
	% NOTE: this makes sure that the context extension is the filter
	
	context.ext = ext;
	
	%--
	% compile parameters
	%--
	
	if ~isempty(ext.fun.parameter.compile)
		
		try
			[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
		catch
			extension_warning(ext, 'Multi-Channel compilation failed.', lasterror);
		end
		
	end
	
	%--
	% compute feature signal
	%--

	try
		
		% NOTE: output should be a struct array with an entry for each channel
		
		[page.feature(end).feature, context] = ext.fun.compute(page, ext.parameter, context);
	
	catch
		
		extension_warning(ext, 'Multi-Channel compute failed.', lasterror);
	
		page.feature(end).feature = struct; 
		
	end
	
else
	
	base = context;
	
	context.ext = ext;
	
	for k = 1:length(page.channels)
		
		%--
		% set channel context
		%--
		
		channel = page.channels(k);
		
		context.page = get_channel_page(page, channel);
		
		% NOTE: we get channel state if available from base context store
		
		context.ext.state = get_channel_state(base, ext, channel);
		
		%--
		% compile parameters
		%--

		if ~isempty(ext.fun.parameter.compile)
			
			try
				[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
			catch
				extension_warning(ext, ['Channel ', int2str(channel), ' compilation failed.'], lasterror);
			end

		end

		%--
		% compute and pack feature
		%--

		try
			
			%--
			% compute feature
			%--
			
			% NOTE: the context page is the channel selected page
			
			[value, context] = ext.fun.compute(context.page, ext.parameter, context);
			
			% NOTE: check the channel field for proper use, we use 'channel' not 'channels'
			
			if isfield(value, 'channel') && (value.channel ~= channel)
				error('Feature extension uses reserved field ''channel''.');
			end
			
			%--
			% pack feature
			%--
			
			page.feature(end).feature(k).channel = channel;
			
			fields = fieldnames(value);
			
			for j = 1:length(fields)
				page.feature(end).feature(k).(fields{k}) = value.(fields{k});
			end

		catch
			
			% NOTE: if feature fails clear feature samples and flush state
			
			extension_warning(ext, ['Channel ', int2str(channel), ' compute failed.'], lasterror); 
		
			page.feature(end).feature = struct; context.ext.state = struct; return;
		
		end

		%--
		% store channel state
		%--
		
		switch in

			case 'main'

				base.ext.state(k).channel = channel;

				base.ext.state(k).state = context.ext.state;

			case 'active'

				% TODO: this does not match our model for features
				
				base.active.sound_feature.state(k).channel = channel;

				base.active.sound_feature.state(k).state = context.ext.state;

		end
		
	end
	
	context = base;
	
end









