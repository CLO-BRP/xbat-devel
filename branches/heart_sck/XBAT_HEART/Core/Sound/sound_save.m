function file = sound_save(lib, sound, state, opt)

% sound_save - save sound structure to file in library
% ----------------------------------------------------
%
% file = sound_save(lib, sound, state, opt)
%
% Input:
% ------
%  lib - library to save sound to (def: active library)
%  sound - sound to save
%  state - browser state to save
%  opt - refresh option
%
% Output:
% -------
%  file - file saved

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

%--
% set default refresh
%--

if (nargin < 4) || isempty(opt)
	opt = 1;
end 

%--
% set default empty state
%--

if nargin < 3
	state = [];
end

%--
% check that sound is in library
%--

if ischar(lib)
    file = fullfile(lib, sound_name(sound));
else
    file = get_library_sound_file(lib, sound_name(sound));
end
    
% NOTE: check for the sound root directory

[par, leaf] = fileparts(file);

if ~exist_dir(par)
	error(['Root directory for ''', leaf, ''' not found in library.']);
end

%-------------------------------------------
% SAVE SOUND AND STATE
%-------------------------------------------
	
try
	save(file, 'sound', 'state');
catch
	nice_catch(lasterror, 'Failed to save sound');
end

%--
% update library cache if needed
%--

% NOTE: this might seem extraneous, but we are storing in a library, not just a file

if opt && ~ischar(lib)
	get_library_sounds(lib, 'refresh');
end
