function [ax, top] = get_play_axes(par, data)

% get_play_axes - get axes for play display
% -----------------------------------------
%
% [ax, top] = get_play_axes(par)
%
% Input:
% ------
%  par - player parent
%
% Output:
% -------
%  ax - play axes
%  top - top indicator

% TODO: generalize to get relevant axes, so we can produce a light update of other views

%------------------------
% HANDLE INPUT
%------------------------

%--
% check parent input
%--

if ~is_browser(par)
	error('Input parent handle is not browser handle.');
end

%--
% get parent state if needed
%--

if nargin < 2
	data = get_browser(par);
end

%------------------------
% GET DISPLAY AXES
%------------------------

%--
% get parent figures
%--

pars = par;

% TODO: create type list and iterate

fig = get_xbat_figs('parent', par, 'type', 'widget');

if ~isempty(fig)
	pars = [pars, fig];
end

fig = get_xbat_figs('parent', par, 'type', 'view');

if ~isempty(fig)
	pars = [pars, fig];
end

fig = get_xbat_figs('parent', par, 'type', 'selection');

if ~isempty(fig)
	pars = [pars, fig];
end

%--
% get play channels
%--

channel = unique(data.browser.play.channel);

%--
% get channel axes in parents and collect time and axes handle
%--

ax = []; top = [];

for i = 1:length(pars)
	
	for j = 1:length(channel)

		[ax2, top2] = get_channel_axes(pars(i), channel(j));

		for k = 1:length(ax2)
			ax(end + 1,:) = ax2(k); top(end + 1) = top2(k);
		end

	end
	
end


%---------------------------------------------
% GET_CHANNEL_AXES
%---------------------------------------------

function [ax, top] = get_channel_axes(par, ch)

%--
% convert channel to string
%--

ch = int2str(ch);

%--
% get all axes and corresponding tags
%--

ax = findobj(par, 'type', 'axes'); 

tag = get(ax, 'tag');

if ischar(tag)
	tag = {tag};
end

%--
% select axes with proper tag
%--

% NOTE: this should develop into something simpler and more reliable

for k = length(ax):-1:1
	
	if isempty(tag{k})
		ax(k) = []; continue;
	end 
	
	[tok1, tok2] = strtok(tag{k}, ' ');
	
	if isempty(tok2)
		tok = tok1;
	else
		tok = tok2;
	end
	
	if isempty(tok) || ~isequal(ch, strtrim(tok))
		ax(k) = []; continue;
	end
	
end

%--
% get topness of axes
%--

top = zeros(size(ax));

for k = 1:length(ax)
	
	if is_top(ax(k))
		top(k) = 1;
	end
	
end
		
