function player_resume(player)

%--
% get current player
%--

if ~nargin
	player = get_current_player;
end

% NOTE: return if we can't find it or if it's not playing

if isempty(player) || isplaying(player)
	return;
end

%--
% resume player
%--

resume(player);
