function toggle_play_state

% toggle_play_state - start, pause, and resume play
% -------------------------------------------------
%
% toggle_play_state

%--
% get play daemon
%--

daemon = timerfind('name', 'PLAY_TIMER');
	
%--
% start play
%--

% NOTE: play is not ongoing if there is no daemon

if isempty(daemon)
	browser_play; return;
end

%--
% pause or resume play
%--

if get(daemon, 'running')
	set_env('PLAY_STATE', ~get_env('PLAY_STATE'));
end

