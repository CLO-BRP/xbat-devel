function [file, ix] = get_current_files(sound, start, stop)

% get_current_files - get files used in interval
% ----------------------------------------------
%
% [file, ix] = get_current_files(sound, start, stop)
%
% Input:
% ------
%  sound - sound
%  start - start time
%  stop - stop time
% 
% Output:
% -------
%  file - files
%  ix - file indices

%--
% handle input
%--

if stop <= start
	error('Stop time must be larger than start time.');
end

%--
% handle simple sound special cases
%--

% NOTE: here we skip the positive time test in 'get_current_file'

if ischar(sound.file)  
	file = sound.file; time = 0; return;
end

if iscell(sound.file) && numel(sound.file) == 1
	file = sound.file{1}; time = 0; return;
end

%--
% get files used in time interval
%--

[ignore, ix1] = get_current_file(sound, start);

[ignore, ix2] = get_current_file(sound, stop);

ix = ix1:ix2; file = sound.file(ix);

