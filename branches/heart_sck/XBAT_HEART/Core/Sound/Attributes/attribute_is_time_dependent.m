function value = attribute_is_time_dependent(sound, name)

% attribute_is_time_dependent - indicate for attribute time dependence
% --------------------------------------------------------------------
%
% value = attribute_is_time_dependent(sound, name)
%
% Input:
% ------
%  sound - sound
%  name - attribute name
%
% Output:
% -------
%  value - indicator

%--
% read attribute prototype
%--

% NOTE: this will be empty for attributes that are not available, not quite a prototype

attr = attribute_read(sound, name);

%--
% examine attribute prototype
%--

if trivial(attr) || ~isstruct(attr)
	value = 0; return;
end

value = isfield(attr, 'time');