function [file, ix] = get_next_file(sound, time)

% get_next_file - get next file from specific time
% ------------------------------------------------
%
% [file, ix] = get_next_file(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - sound time
%
% Output:
% -------
%  file - next file from sound time
%  ix - index of file in sound files

% NOTE: next file is next or the same

[file, ix] = get_current_file(sound, time);

if ix + 1 > get_file_count(sound) % ischar(sound.file) || (ix == length(sound.file))
	return;
end

ix = ix + 1; file = get_file_from_index(sound, ix);