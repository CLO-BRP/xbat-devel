function X = mbe(X, parameter)

%--
% set and possibly output default parameters
%--

if nargin < 2

	% NOTE: these are not the same as the extension parameters, due to lack of context mostly 
	
	parameter = struct;
	
	parameter.length = 12;

	parameter.background = 0;
	
end

if ~nargin
	X = parameter; return; 
end

%--
% compute background
%--

% NOTE: compute the running mimimum, then smooth

% SE = zeros(1, 2 * parameter.length + 1); SE(1, 1:parameter.length + 1) = 1;

SE = ones(1, 2 * parameter.length + 1);

B = morph_erode(X, SE);

% B = linear_filter(B, filt_binomial(1, 5));

%--
% compute background or residual
%--

if parameter.background;
	X = B;
else
	X = X - B;
end

%--
% smooth result
%--

% NOTE: we could smooth the estimate and/or we could smooth the result

% X = linear_filter(X, filt_binomial(3, 3));