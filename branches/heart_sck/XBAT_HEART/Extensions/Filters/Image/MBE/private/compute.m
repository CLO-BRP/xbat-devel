function [X, context] = compute(X, parameter, context)

% MBE - compute

% NOTE: we compute background estimate using helper, the context processing should have ocurred in the compile

X = mbe(X, parameter);