function ext = mbe_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Minimum Background Estimate';

ext.category = {'Statistical', 'Background'};

ext.version = '0.1';

ext.guid = 'c0fb9539-67ad-4b6f-9587-1ee5388fb159';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

