function SE = create_se(param)

% create_se - structuring element from extension parameters
% ---------------------------------------------------------
%
% SE = create_se(param)
%
% Input:
% ------
%  param - morphological base parameters
%
% Output:
% -------
%  SE - structuring element

%--
% allow some flexibility in style
%--

% NOTE: we accept a string or a cell container as well as insignificant space

if iscell(param.style)
	param.style = param.style{1};
end 

style = lower(strtrim(param.style));

%--
% compute based on style
%--

height = param.height; width = param.width;

% NOTE: there are remaining options for 'se_ball' not exposed

switch style

	case 'rectangle'
		SE = ones(2 * height + 1, 2 * width + 1);
		
	case 'disc'
		SE = se_ball([width, height], 2);
		
	case 'diamond'
		SE = se_ball([width, height], 1);

	case 'star'
		SE = zeros(2 * height + 1, 2 * width + 1); SE(height + 1, :) = 1; SE(:, width + 1) = 1;
		
end
