function ext = linear_base_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Base for linear image filters';

ext.category = {'Linear'};

ext.version = '0.1';

ext.guid = '8919726a-ebf7-4972-a1a3-8173817e6d70';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

