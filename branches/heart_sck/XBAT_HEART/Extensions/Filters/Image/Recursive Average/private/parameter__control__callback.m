function result = parameter__control__callback(callback, context)

% RECURSIVE AVERAGE - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'output'
		
		value = get_control(callback.pal.handle, 'output', 'value');
		
		set_control(callback.pal.handle, 'positive', 'enable', strcmpi(value, 'signal'));
		
end