function ext = erosion_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Morphological erosion (local min)';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '3239d582-bed8-4345-acaa-6d4acbff1d66';

% ext.author = '';

% ext.email = '';

% ext.url = '';

