function parameter = parameter__create(context)

% LOWPASS-BUTTERWORTH - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

nyq = 0.5 * context.sound.rate;

parameter.max_freq = 0.1 * nyq;

parameter.order = 6;

parameter.type = 'high';
