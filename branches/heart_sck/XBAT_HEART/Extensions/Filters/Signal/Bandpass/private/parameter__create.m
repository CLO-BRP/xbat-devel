function parameter = parameter__create(context)

% BANDPASS - parameter__create

%----------------
% SETUP
%----------------

%--
% inherit basic parameters from parent
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% get nyquist from context
%--

nyq = 0.5 * context.sound.rate;
		
%----------------
% PARAMETERS
%----------------

%--
% hidden parameters
%--

parameter.min_band = 0.05 * nyq;

% NOTE: this describes the desired band amplitudes

parameter.amplitude = [0, 1, 0];

%--
% band parameters
%--

parameter.min_freq = 0.25 * nyq;

parameter.max_freq = 0.75 * nyq;

parameter.transition = 0.1 * nyq;

%--
% design parameters
%--

parameter.length = 16;

parameter.estimate = 1;

parameter.pass_ripple = -60;

parameter.stop_ripple = -40;


