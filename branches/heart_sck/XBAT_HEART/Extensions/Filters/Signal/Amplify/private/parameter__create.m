function parameter = parameter__create(context)

% AMPLIFY - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @amplify;

parameter.gain = 10;

parameter.emphasis = true;

parameter.zero = 0.95;


function X = amplify(X, parameter)

X = parameter.gain .* X;

% NOTE: this is trivial range compression

X(X > 1) = 1; 

X(X < -1) = -1;

