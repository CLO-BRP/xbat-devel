function result = parameter__control__callback(callback, context)

% FILTER CHAIN - parameter__control__callback

result = struct;

pal = callback.pal;

switch callback.control.name
	
	case 'extensions' 
		
		% TODO: we must update the selected value here
		
		context.ext.parameter.selected = [];
		
		control = get_filter_controls(context.ext, context);
		
		{control.name}
		
		props = get_palette_property(pal.handle)
		
		control_group([], [], props.name, control, props.opt.fun)
		
	otherwise

		update_extensions(callback);
		
end


%-----------------------------------
% UPDATE_EXTENSIONS
%-----------------------------------

function update_extensions(callback)

%--
% get list control information
%--

pal = callback.pal;

control = get_control(pal.handle, 'extensions', 'all');

obj = control.handles.obj; extensions = get(obj, 'string'); selected = get(obj, 'value');

%--
% update extensions based on callback
%--

switch callback.control.name
	
	case 'add'
		
		append = get_control(pal.handle, 'add', 'value'); append = append{1};
		
		% NOTE: we update value to highlight new extension
		
		extensions = {extensions{:}, append}; selected = numel(extensions);
		
	% NOTE: we update value to keep focus on moved extension
	
	case 'move_up'
		
		if selected == 1
			return;
		end
		
		extensions(selected - 1:selected) = extensions(selected:-1:selected - 1); selected = selected - 1;
		
	case 'move_down'
		
		if selected == numel(extensions)
			return;
		end
		
		extensions(selected:selected + 1) = extensions(selected + 1:-1:selected); selected = selected + 1;
		
	case 'delete'
		
		extensions(selected) = [];
		
		if selected == 1
			selected = [];
		else
			selected = selected - 1;
		end
	
	otherwise, return;
		
end

%--
% update extensions control
%--

set(obj, 'string', extensions, 'value', selected);

% TODO: trigger control callback on new values
