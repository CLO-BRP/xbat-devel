function ext = impulsenoise_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Add impulsive noise to signal';

ext.category = {'Noise', 'Nonlinear'};

ext.version = '0.1';

ext.guid = '7700211e-19ad-46a8-9aff-9234d68ce934';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

