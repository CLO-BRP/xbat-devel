function control = parameter__control__create(parameter, context)

% POINT_OPERATION_BASE - parameter__control__create

control = empty(control_create);

control(end + 1) = control_create( ...
	'name', 'map', ...
	'style', 'axes', ...
	'onload', 1, ...
	'lines', 7, ...
	'value', parameter.map, ...
	'width', 0.8, ...
	'align', 'left', ...
	'space', 1.25 ...
);
