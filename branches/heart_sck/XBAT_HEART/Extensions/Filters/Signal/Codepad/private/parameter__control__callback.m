function result = parameter__control__callback(callback, context)

% CODEPAD - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'code'
		
		names = {'code'};

		for k = 1:length(names)

			handles = get_control(callback.pal.handle, names{k}, 'handles');

			set(handles.uicontrol.edit, ...
				'fontname', 'Courier' ...
			);
		
		end
		
end
