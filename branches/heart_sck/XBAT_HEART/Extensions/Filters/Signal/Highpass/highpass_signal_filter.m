function ext = highpass_signal_filter

ext = extension_inherit(mfilename, bandpass_signal_filter);

ext.short_description = 'Highpass signal using equiripple filter';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '1dde8231-857b-4a55-b8ee-255feb02a6ea';

% ext.author = '';

% ext.email = '';

% ext.url = '';

