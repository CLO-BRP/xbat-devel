function handles = display_freq_labels(ax, label, opt)

%--
% set and possibly output default options
%--

% TODO: the 'handles' and the 'tags' should allow us to reuse handles

if nargin < 3
	opt.alternate = 0; opt.fun = @num2str; opt.handles = []; opt.tags = [];
end 

if ~nargin
	handles = opt; return;
end

%--
% clear label and compute various display related numbers
%--

% NOTE: this approach does not provide any control over the text

% label(isnan(label)) = []; set(ax, 'label', label, 'xticklabel',	iterate(@num2str, label));

xlabel(ax, '');

% NOTE: this approach may allow the text to become the control

pix = get_size_in(ax, 'pixels'); pix = pix(4);

ylim = get(ax, 'ylim'); range = diff(ylim);

% NOTE: we are replacing a hard-coded 12, beware of its use elsewhere

conf = get_palette_settings; const = conf.fontsize;

offset = const * (range / pix);

ypos = ylim(1) - offset;

ypos2 = ylim(2) + offset;

%--
% display labels if needed
%--

% TODO: consider whether we should output a full array of handles or only the ones used

handles = []; 

if ~isnan(label(1))
	handles(end + 1) = text(label(1), ypos, opt.fun(label(1)), 'horizontalalignment', 'right');

	% TODO: this does not work as expected, the analogy with the last label fails
	
	st = get_size_in(handles(end), 'pixels');
	
	if st(1) < const
		set(handles(end), 'horizontalalignment', 'center');
	end
end

if ~isnan(label(2))
	handles(end + 1) = text(label(2), ypos, opt.fun(label(2)), 'horizontalalignment', 'center');

	if opt.alternate
		pos = get(handles(end), 'position'); pos(2) = ypos2; set(handles(end), 'position', pos);
	end
end

if ~isnan(label(3))
	handles(end + 1) = text(label(3), ypos, opt.fun(label(3)), 'horizontalalignment', 'left');

	% NOTE: this bit of code updates the alignment if we are falling off the figure
	
	st = get_size_in(handles(end), 'pixels'); sf = get_size_in(ancestor(handles(end), 'figure'), 'pixels');
	
	if st(1) + st(3) + 1.5 * const > sf(3)
		set(handles(end), 'horizontalalignment', 'center');
	end
end

set(handles, 'parent', ax, 'clipping', 'off');


