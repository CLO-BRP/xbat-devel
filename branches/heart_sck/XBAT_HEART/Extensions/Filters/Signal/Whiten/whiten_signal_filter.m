function ext = whiten_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Whiten using an AR-model inverse filter';

ext.category = {ext.category{:}, 'Enhance'};

ext.version = '0.2';

ext.guid = '67c81b0d-02b2-46d4-8310-10560d4ea0c9';

ext.author = 'Harold and Matt';

ext.email = '';

% ext.url = '';

