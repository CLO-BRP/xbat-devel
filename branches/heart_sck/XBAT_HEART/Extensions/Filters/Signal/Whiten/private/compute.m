function [X, context] = compute(X, parameter, context)

% WHITEN - compute

% NOTE: we probably want to get rid of this function

fun = parent_fun(mfilename('fullpath')); [X, context] = fun(X, parameter, context);
