function parameter = parameter__create(context)

% NOISE - parameter__create

parameter = struct;

parameter.distribution = [];

parameter.variance = 0.1;

parameter.band = 1;

parameter.filter = [];

