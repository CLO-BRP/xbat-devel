function [value, context] = compute(event, parameter, context)

% TIME WARP - compute

%--
% compute parent feature on event page
%--

page = get_event_page(event, context.sound, context);

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.state.feature = feature;

%--
% get event duration, bandwidth, and center frequency
%--

% NOTE: these are defined based on the event box, this may not correspond to the actual signal

% Get parameters for this event

lambda = event.time(2) - event.time(1);

alpha = event.freq(2) - event.freq(1);

mu = (event.freq(1) + event.freq(2)) / 2;

%----------------------------------------

%--
% Find frequencies that have the most power
%--


[peak.value, peak.index] = max(feature.spectrogram.value);

peak.freq = feature.freq(peak.index);

contrast = peak.value - min(feature.spectrogram.value); 



C = cumsum(feature.spectrogram.value, 1); C = C ./ repmat(C(end, :), size(C, 1), 1);

% percent = [0.05, 0.25, 0.5, 0.75, 0.95];

percent = [0.25, 0.5, 0.75];

for k = 1:size(C, 2)
	
	for j = 1:numel(percent)
		order.index(k, j) = find(C(:, k) >= percent(j), 1); 
	end
	
end

order.freq = feature.freq(order.index);


if context.debug
	
	%--
	% create and clear figure
	%--
	
	fig1 = create_obj('figure', 0, 'FIGURE_1'); figure(fig1); clf(fig1);

	%--
	% layout axes
	%--
	
	layout = layout_create(3, 1); layout.row.frac = [0.7, 0.15, 0.15]; layout.row.pad = 1.2 * ones(1, 2);

	ax = harray(fig1, layout);

	%--
	% display spectrogram and energy percentiles
	%--
	
	handle = imagesc(feature.time - feature.time(1), feature.freq, feature.spectrogram.value, 'parent', ax(2));
	
	set(ax(2), ...
		'ydir', 'normal', ...
		'ylim', [0, 400] ...
	);
	
	hold(ax(2), 'on');
	
	time = feature.time - feature.time(1);
	
% 	handle = plot(time, order.freq, 'linewidth', 4, 'color', 0.8 * ones(1, 3), 'parent', ax(2));
	
	handle = plot(time, order.freq, 'parent', ax(2));
	
	set(handle, 'color', 0.25 * [1 1 1], 'linestyle', '-');
	
	handle = plot(time, order.freq, 'parent', ax(2));
	
	set(handle, 'color', 1 * [1 1 1], 'linestyle', ':');
	
	handle = plot(time, peak.freq, 'parent', ax(2));
	
	set(handle, 'color', 0.75 * [1 0.25 0.25], 'linestyle', '-', 'marker', 'o');
	
	hold(ax(2), 'off');
	
	%--
	% display amplitude and bandwidth
	%--

	for k = 1:numel(feature.time)
	
		% NOTE: we are summing the energy from the 25 to 75 percentile frequency bins
		
		% NOTE: this creates an opaque dependency on the 'percentile' array
		
		amp(k) = sum(feature.spectrogram.value(order.index(k, 1):order.index(k, 3), k));
		
	end
	
	samp = smooth(amp, floor(0.5 * numel(feature.time)), 'rloess');
	
	plot(time, amp, 'parent', ax(3));
	
	hold(ax(3), 'on');
	
	plot(time, samp, ':', 'parent', ax(3));
	
	plot([time(1), time(end)], 0.5 * max(samp) * ones(1, 2), 'r:', 'parent', ax(3));
	
	
	band = (order.freq(:, 3) - order.freq(:, 1)); 
	
	sband = smooth(band, floor(0.5 * numel(feature.time)), 'rloess');
	
	axes(ax(3)); title('AMPLITUDE');
	
	plot(time, band, 'parent', ax(4));
	
	hold(ax(4), 'on');
	
	plot(time, sband, ':', 'parent', ax(4));
	
	plot([time(1), time(end)], 0.75 * max(sband) * ones(1, 2), 'r:', 'parent', ax(4));
	
	axes(ax(4)); title('BANDWIDTH');
	
	set(ax(3:4), 'xlim', [time(1), time(end)]);
	
% 	apply_theme(fig1);
	
	
% 	% Get scaled axes
% 	% TODO: make y axis positive in figure
% 
% 	x_axis = event.time - event.time(1);
% 
% 	y_axis = (event.freq / -alpha) + min(event.freq / alpha);
% 
%     % Plot event with normalized axes
%     fig1 = create_obj('figure', 0, 'FIGURE_1'); figure(fig1);
%     imagesc(x_axis, y_axis, feature.spectrogram.value)
% 
%     fig2 = create_obj('figure', 0, 'FIGURE_2'); figure(fig2);
%     plot(high, 'r')
%     hold on
%     plot(med, 'y')
%     plot(low, 'g')
%     title('Power Dist in Freq (25%, 50%, 75%)');
%     hold off

end


value.peak = peak; value.order = order;

return;


%Get 35 data points for each event
len = numel(peak.value);
step = len / 35;
for cnt = 1:35
   
    start = round(step*(cnt - 1));
    if(start == 0)
        start = 1;
    end    
    stop = round(step*cnt);
   if(stop == 0)
        stop = 1;
    end
    sums = 0; csums = 0; lsums=0; hsums=0; msums = 0; count = 0; 
    for i = 1:(stop - start + 1)      
        sums = sums + peak.value(start+i-1);
        csums = csums + contrast(start+i-1);
        hsums = hsums + high(start+i-1);
        lsums = lsums + low(start+i-1);
        msums = msums + med(start+i-1);
        count = count + 1;
    end    
  
    spectrace(cnt) = sums/count;
    contrace(cnt) =csums/count;
    lowtrace(cnt) = lsums/count;
    hightrace(cnt) = hsums/count;
    medtrace(cnt) = msums/count;
    
end


scale = 1/numel(spectrace):1/numel(spectrace):1;
    
% Get boundaries for frequency data
for j = 1:numel(spectrace)
   
%     upbound(j) = spectrace(j) + .05 * spectrace(j) / mean(spectrace);
%     lowbound(j) = spectrace(j) - .05 * spectrace(j) / mean(spectrace);
     upbound(j) = hightrace(j);
     lowbound(j) = lowtrace(j);   
    width(j) = upbound(j) - lowbound(j);

end

% Smooth boundaries
upbound = smooth(upbound,3);
lowbound = smooth(lowbound,3);


% Scale and plot spectrogram data
if context.debug
    fig3 = create_obj('figure', 0, 'FIGURE_3'); figure(fig3);  
    plot(scale, spectrace)
    ylim([0 1]);
    hold on;
    % Plot upper and lower bounds
    plot(scale, lowbound, 'r')
    plot(scale, upbound, 'r')
    plot(scale, medtrace, 'g')
    plot(scale, width, 'y')
    title('Power Dist, Max Power Trace and Width');
    hold off;  
    
    fig4 = create_obj('figure', 0, 'FIGURE_4'); figure(fig4);  
    plot(scale, contrace)
    title('Contrast');
    
end

size(lowbound)
size(medtrace)

%Save data to struct
value = struct('spectrace',spectrace','topsamples',upbound,...
    'bottomsamples',lowbound, 'medsamples', medtrace', 'contrast',contrace');

% Leave this out for now
% value.polynomial = polyfit(scale, spectrace, 2);


if context.debug
    flatten(value)  
    event
    
    name = 'TimeWarp';
    event.measure.(name).value = value;

    event.measure.(name).value
end
    