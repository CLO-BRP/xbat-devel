function [value, context] = compute(event, parameter, context)

% PEAK AMPLITUDE - compute

% value = struct;

db_disp; % stack_disp; event

page = get_event_page(event, context.sound, context); context.page = page;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.state.feature = feature;

%--
% compute measure value from feature
%--

% NOTE: from here on we use the computed feature to get the event measure

[peak, index] = max(feature.rms.value);

value.value = peak; value.time = feature.time(index);

dist = cumsum(feature.rms.value); dist = dist / dist(end);

% NOTE: this is fast, but we may want a five point list with 5 and 95

index = quartile_indices(dist); 

field = {'q1', 'q2', 'q3'};

for k = 1:numel(field)
	value.(field{k}) = feature.time(index(k));
end 

field = {'mean', 'median'};

for k = 1:numel(field)
	value.(field{k}) = feval(field{k}, feature.rms.value(index(1):index(end)));
end

db_disp; value
