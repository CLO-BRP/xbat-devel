function [parameter, context] = parameter__compile(parameter, context)

% TIME-FREQUENCY QUARTILES - parameter__compile

% NOTE: here we declare that the spectrogram computation parameters are measure parameters

parameter.specgram = context.sound.specgram;
