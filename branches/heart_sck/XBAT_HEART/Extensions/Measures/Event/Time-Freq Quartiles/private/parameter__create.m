function parameter = parameter__create(context)

% TIME-FREQUENCY QUARTILES - parameter__create

% parameter = struct;

parameter.specgram = struct_update(fast_specgram, context.sound.specgram);

% NOTE: this is a way to fully account for the event description during measurement

parameter.restrict_to_event_band = 0;

% NOTE: this is useful when the recording contains low-frequency noise

parameter.min_frequency = 0;