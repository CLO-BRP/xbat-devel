function [task, context] = atomize(task, parameter, context) %#ok<INUSL>

% SOUND SCAN - atomize

% NOTE: it is not clear that everything here belongs here, perhaps the name is 'prepare'

%--
% get detector and preset referenced
%--

% NOTE: the availability of the detector is a matter of installed software

% NOTE: extension distribution is managed through source-control

ext = get_extensions('sound_detector', 'guid', task.task.detector);

if isempty(ext)
	% TODO: try to install extension using BARN database representation
	
	% TODO: to include source-control address in BARN extension representation
	
	error(['Detector is not available. (', task.task.detector, ')']);
end

preset = get_barn_preset(context.store, task.task.preset); 

%--
% get sounds referenced
%--

% NOTE: how we parse the different sounds depends on the web-client, we could have gotten a cell array to start with

guid = str_split(task.task.sound, ' ');

for k = 1:numel(guid)
	
	sound(k) = get_database_objects_by_column(context.store, 'sound', 'guid', guid{k}); %#ok<AGROW>
end

db_disp; disp(sound)

%--
% create children tasks
%--

% NOTE: we are certainly splitting by sound, then we may split a single sound scan for long sounds

child = empty(task);

for k = 1:numel(sound)
	
	% NOTE: we start working on a current task focused on one sound
	
	current = task;
	
	current.description = ['Scan ''', sound.name, ''' using ''', ext.name, ''' with ''', preset.name, '''.'];
	
	current.task.sound = sound(k).guid;
	
	% NOTE: in what follows we will ensure producing atomic tasks
	
	current.atomic = true;
	
	% NOTE: here we consider whether the scan task should be split, this is controlled via a task parameter
	
	if get_sound_duration(sound(k)) < parameter.atomic_scan_length
		
		child(end + 1) = task; %#ok<*AGROW>
	else
		scan = get_sound_scan(sound(k), parameter.atomic_scan_length);
		
		[page, scan] = get_scan_page(scan);
		
		while ~isempty(page)
			child(end + 1) = current; child(end).task.scan = page;
			
			[page, scan] = get_scan_page(scan);
		end
	end
end

task = child;


%                  id: []
%                guid: []
%                name: []
%         description: []
%                task: []
%            priority: []
%             user_id: []
%          project_id: []
%              atomic: []
%           parent_id: []
%     parent_fraction: []
%              daemon: []
%          started_at: []
%             attempt: []
%            progress: []
%              report: []
%         last_update: []
%        completed_at: []
%          created_at: []
%         modified_at: []
