function cursor = get_cursor(ax, type, varargin)

%--
% get cursor tag from type
%--

switch type
	
	case {1, 'x', 'X'},
		tag = 'xcursor';
		
	case {2, 'y', 'Y'}
		tag = 'ycursor';
		
end

%--
% get cursor line handle
%--

cursor = create_line(ax, tag, varargin{:});