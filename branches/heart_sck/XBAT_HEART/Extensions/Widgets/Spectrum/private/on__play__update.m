function [handles, context] = on__play__update(widget, data, parameter, context)

% SPECTRUM - on__play__update

handles = [];

%--
% get and display time
%--

time = get_play_time(context.par);

% NOTE: we can use this time, this is slider time?

spectrum_widget_time_display(widget, time, context);

%--
% get display relevant data
%--

event.time = time; 

buffer = get_player_buffer;

% NOTE: we have nothing to do, when the buffer is empty we are done playing

if isempty(buffer)
	return;
end

opt = peak_display; opt.peaks = parameter.peaks;

%--
% update play lines
%--

for k = 1:length(buffer.channels) 

	channel = buffer.channels(k);
	
	%--
	% get display axes
	%--
	
	ax = spectrum_axes(widget, channel);
	
	if isempty(ax)
		continue;
	end
	
	%--
	% get slice data and update display
	%--

	event.channel = channel;
	
	[slice, freq] = get_spectrogram_slices(event, context);
	
	if isempty(slice)
		continue;
	end

	freq = linspace(freq(1), freq(2), numel(slice));
	
	play_line(ax, channel, ...
		'xdata', freq, ...
		'ydata', slice ...
	);

	% TODO: display peaks and peak hold line

	% NOTE: will this work with multiple channels?
	
	peak_display(ax, freq, slice, 'play_peaks', opt);
	
end

