function handle = selection_line(ax, channel, varargin)

handle = create_line(ax, ['selection_line::', channel], varargin{:});
