function [low, high] = frequency_guides(ax, channel, varargin)

channel = int2str(channel);

% NOTE: we force the linestyle by including it last

low = create_line(ax, ['low_freq_guide::', channel], varargin{:}, 'linestyle', ':');

high = create_line(ax, ['high_freq_guide::', channel], varargin{:}, 'linestyle', ':');

if nargout < 2
	low = [low, high];
end
