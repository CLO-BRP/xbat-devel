function handle = play_line(ax, channel, varargin)

handle = create_line(ax, ['play_line::' int2str(channel)], varargin{:});
