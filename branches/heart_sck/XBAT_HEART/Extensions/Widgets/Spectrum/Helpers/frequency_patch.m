function handle = frequency_patch(ax, selection)

event = selection.event;

%--
% get patch handle, create if needed
%--

handle = create_obj('patch', ax, ['selection_band::', int2str(event.channel)]);

%--
% update patch properties
%--

% start and stop frequencies

start = event.freq(1); stop = event.freq(2); 

xdata = [start, stop, stop, start, start]; 

% min and max displayed levels

ylim = get(ax, 'ylim'); offset = 0.025 * diff(ylim); low = ylim(1) + offset; high = ylim(2) - offset;

ydata = [low, low, high, high, low];

% compute color of patch to highlight without using transparency

color = get_patch_colors(ax, selection.color);

% update patch properties

set(handle, ...
	'xdata', xdata, 'ydata', ydata, 'facecolor', color.face, 'edgecolor', color.edge ...
);

% NOTE: this makes sure that the non-transparent patch does not occlude display

uistack(handle, 'bottom');

