function pos = get_display_positions(widget, clips)

%--------------------
% SETUP
%--------------------

%--
% get figure size and aspect ratio
%--

pos = get_size_in

%--
% get figure size
%--

[n1, R] = get_next_good_rect(clips);

% TODO: we want to array some axes, edit controls and rating controls, the latter perhaps can be integrated into clip display

