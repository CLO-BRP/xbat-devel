function [handles, context] = on__page(widget, data, parameter, context)

% SPECTROGRAM_BASE - on__page

handles = [];

%--
% get image data
%--

image = get_image_handles(context.par);

for k = 1:numel(image)
	value(k).image = image(k); value(k).channel = get(get(image(k), 'parent'), 'tag')
end

disp(value)

% X = [];
% 
% for k = 1:length(images)
% 	Xk = get(images(k), 'cdata'); X = [X; Xk(:)];
% end
