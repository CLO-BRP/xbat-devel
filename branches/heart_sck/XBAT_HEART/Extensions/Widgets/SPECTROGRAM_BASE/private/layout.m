function handles = layout(widget, parameter, context)

% SPECTROGRAM_BASE - layout

% NOTE: this is not typically a relevant layout, but it allows us to see the refresh rate for now

%--
% layout axes and get handles
%--

layout = layout_create(1); layout.margin(1) = 0.5;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

set(handles, 'tag', 'spectrogram_base_axes');
