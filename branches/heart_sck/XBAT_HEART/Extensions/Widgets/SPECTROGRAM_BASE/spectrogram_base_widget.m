function ext = spectrogram_base_widget

ext = extension_create(mfilename);

ext.short_description = 'Base for widgets that use the spectrogram';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'd07c96b4-b109-42ad-b376-b304b31b0505';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = '';

