function handles = layout(widget, parameter, context)

% CLOCK - layout

%--
% layout axes and get handles
%--

layout = layout_create(1); layout.margin(1) = 0.5;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

set(handles(1), 'tag', 'clock_axes', 'visible', 'off');