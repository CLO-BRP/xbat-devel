function [handles, context] = on__page(widget, data, parameter, context)

% PHOTO STREAM - on__page

%--
% get image file using time data on current sound file
%--

% NOTE: this is specific to the PALAOA set, we should have a file that contains the correspondence

current = get_current_file(context.sound, data.page.start);

prefix = get_date_time_prefix(current);

ix = strmatch(prefix, parameter.files);

if isempty(ix)
	handles = []; return;
end

file = fullfile(parameter.source, parameter.files{ix});

%--
% load image and metadata from file and display
%--

info = get_file_info(file); X = imread(file);

handles = findobj(widget, 'tag', 'image'); 

if ~isempty(handles)
	
	set(handles, 'cdata', X);
	
else
	
	par = findobj(widget, 'tag', 'container');

	if isempty(par)
		handles = []; return;
	end

	handles = image(X, 'parent', par);

	set(par, 'tag', 'container');

end

%--
% update context menu to include file info
%--

par = context_menu(handles);

delete(get(par, 'children'));

struct_menu(par, info);

drawnow;


%--------------------------
% GET_FILE_INFO
%--------------------------

function info = get_file_info(file)

%--
% get info from file
%--

info = imfinfo(file);

%--
% parse comment info field to get real info
%--

if isfield(info, 'Comment')
	
	%--
	% get comment lines
	%--
	
	lines = file_readlines(info.Comment{1});
	
	%--
	% parse and build info structure from comment string
	%--
	
	comment = struct; field = '';
	
	for k = 1:numel(lines)
		
		current = lines{k};
		
		%--
		% skip comments
		%--
		
		if current(1) == '#'
			continue;
		end

		%--
		% section indicators and data are discriminated by number of parts
		%--
		
		parts = str_split(current);

		if numel(parts) > 1

			% NOTE: a line with two parts may start or end a section

			% TODO: use a faster comparison, we only have to look at the first character
			
			if strcmpi(parts{1}, 'section')
				
				field = lower(parts{2});
				
			elseif strcmpi(parts{1}, 'endsection')
				
				field = '';
				
			end

		else

			% NOTE: values are stored as field value pair strings 'field=value'

			value = str_split(parts{1}, '='); value{1} = lower(value{1});
			
			% NOTE: this is heuristic test for whether the string may represent a number
			
			if ~isempty(setdiff(value{2}, '1234567890.-+'))
				
				comment.(field).(value{1}) = value{2};
				
			else
				
				try
					comment.(field).(value{1}) = eval(value{2});
				catch
					comment.(field).(value{1}) = value{2};
				end
			
			end
			
		end
		
	end
	
	info.Comment = comment;
		
end


%--------------------------
% GET_DATE_TIME_PREFIX
%--------------------------

function prefix = get_date_time_prefix(file)

prefix = strtok(file, ' '); prefix(end) = '0'; [ignore, prefix, ignore] = fileparts(prefix);
