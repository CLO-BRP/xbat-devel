function [handles, context] = on__slide(widget, data, parameter, context)

% BIG TIME - on__slide

%--
% get slide time
%--

slider = get_time_slider(context.par); 

time = slider.value;

%--
% display slide time
%--

handles = display_big_time(widget, time, context);