function [handles, context] = on__selection__create(widget, data, parameter, context)

% BIG SELECTION - on__selection__create

%--
% get container axes
%--

ax = findobj(widget, 'tag', 'selection_display_axes');

if isempty(ax)
	handles = []; return;
end

%--
% display selection event information
%--

event = data.selection.event;

str = {};

str{end + 1} = ['Time: ', sec_to_clock(event.time(1)), ' (', num2str(diff(event.time)), ')'];

str{end + 1} = ['Freq: [', num2str(event.freq(1)), ', ', num2str(event.freq(2)), ']'];

handles = big_centered_text(ax, str);