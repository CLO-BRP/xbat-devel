function parameter = parameter__create(context)

% SCOPE - parameter__create

parameter = struct;

% NOTE: upper parameter names should be used for useful constants

parameter.MIN_SAMPLES = 256; % round(context.sound.rate * 0.05);

parameter.MAX_SAMPLES = 1024; % 10 * parameter.MIN_SAMPLES;

parameter.PAGE_FRACTION = 0.02;

% TODO: the default value should depend on the sound rate

parameter.duration = 0.01;

parameter.adapt = 1;