function [handles, context] = on__play__update(widget, data, parameter, context)

% SCOPE - on__play__update

%--
% get current scope time
%--

time = get_play_time(context.par) - 0.5 * parameter.duration; 

%--
% get scope samples from buffer
%--

% TODO: factor this into a helper

% NOTE: return if we can't get data to display

if isempty(data.buffer)
	handles = []; return;
end

rate = get(data.player, 'samplerate'); n = floor(parameter.duration * rate / data.buffer.speed);

buffer = data.buffer;

% NOTE: shift the index by half the duration

n1 = floor(0.5 * n);

buffer.ix = buffer.ix - n1;

% TODO: reconsider 'underflow' representation to match that of 'overflow'

if buffer.ix <= 0
	underflow = -buffer.ix + 1;
else
	underflow = 0;
end

overflow = (buffer.ix + n - 1) - length(buffer.samples);

state = (underflow ~= 0) + 2 * (overflow > 0);

switch state
	
	case 0
		samples = buffer.samples(buffer.ix:(buffer.ix + n - 1), :);
		
	case 1
		samples = [zeros(underflow, size(buffer.samples, 2)); buffer.samples(1:(n - underflow), :)];
			
	case 2
		samples = [buffer.samples(buffer.ix:end, :); zeros(overflow, size(buffer.samples, 2))];
	
	case 3
		samples = [zeros(underflow, size(buffer.samples, 2)); buffer.samples; zeros(overflow, size(buffer.samples, 2))];

end

%--
% update scope display
%--

handles = update_scope_display(widget, time, parameter.duration, samples, context);

