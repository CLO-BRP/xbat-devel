function handles = update_scope_display(widget, start, duration, samples, context)

%--
% get scope axes
%--

ax = scope_axes(widget);

if isempty(ax)
	return;
end

%--
% update scope time display
%--

% TODO: this function needs context as input

% TODO: this time display does not consider grid options, we need context

set(get(ax(1), 'title'), ...
	'string', get_browser_time_string([], start, context), ...
	'fontsize', 12, ...
	'fontweight', 'bold' ...
);

%--
% update scope line display
%--

if isempty(duration) || isempty(samples)
	return;
end

time = linspace(start, start + duration, size(samples, 1));

handles = scope_line(ax);

% NOTE: consider only updating 'ydata' here, and no update on axes

set(handles(1), 'xdata', time, 'ydata', samples(:, 1));

if length(handles) > 1
	
	set(handles(2), 'xdata', time, 'ydata', samples(:, end));

end

%--
% display envelope
%--

% TODO: widget configuration and improvement of display for the envelope

display_envelope(ax(1), time, samples(:, 1), 1);

if length(handles) > 1

	display_envelope(ax(2), time, samples(:, end), 2);

end

% TODO: display lower envelope

%--
% update scope axes
%--

% NOTE: we have already found the axes

% scope_axes(widget, 'xlim', [start, start + duration]);

set(ax, 'xlim', [start, start + duration]);

center_line(ax, start + 0.5 * duration);


%-------------------------------
% CENTER_LINE
%-------------------------------

function handle = center_line(ax, time, varargin)

[handle, created] = create_line(ax(1), 'center::1', 'xdata', [time, time]);

if length(ax) > 1
	[handle(2), created(2)] = create_line(ax(2), 'center::2', 'xdata', [time, time]);
end

if created
	set(handle, 'ydata', [-2, 2], 'color', get(ax(1), 'xcolor'), 'linestyle', ':');
end

if ~isempty(varargin)
	set(handle, varargin{:});
end


%-------------------------------
% DISPLAY_ENVELOPE
%-------------------------------

function handle = display_envelope(ax, time, samples, k)

%--
% compute envelope
%--

[u, l] = real_envelope(samples);

%--
% display envelope
%--

ydata = [u', fliplr(l')]; xdata = [time, fliplr(time)];

opts = {'linestyle', '-', 'facecolor', 0.95 * ones(1, 3), 'edgecolor', 0.8 * ones(1, 3)}; 

[handle, created] = create_patch(ax, ['envelope::', int2str(k)], 'xdata', xdata, 'ydata', ydata, opts{:});

if created
	set(handle, 'clipping', 'off'); uistack(handle, 'bottom');
end
