function scope_drag_callback(obj, eventdata, state) %#ok<INUSL>

persistent AX SCALE FIG START PAR MARKER CURRENT;

switch state
	
	case 'start'
		
		%--
		% keep axes handle and compute motion scale factor
		%--
		
		AX = obj; 
		
		pos = get_size_in(AX, 'pixels'); SCALE = diff(get(AX, 'xlim')) / pos(3);
		
		%--
		% keep widget handle and starting point, and set motion and stop callbacks	
		%--
		
		FIG = ancestor(AX, 'figure'); 
		
		START = get(FIG, 'currentpoint'); START = START(1);
		
		set(FIG, ...
			'windowbuttonmotionfcn', {@scope_drag_callback, 'drag'}, 'windowbuttonupfcn', {@scope_drag_callback, 'stop'} ...
		);
	
		%--
		% get parent and marker
		%--
		
		PAR = get_widget_parent(FIG); MARKER = get_browser_marker(PAR); CURRENT = MARKER.time;
		
	case {'drag', 'stop'}
		
		%--
		% consider widget motion, scale to axes and update marker
		%--
		
		current = get(FIG, 'currentpoint'); current = current(1); 
		
		MARKER.time = CURRENT - SCALE * (current - START); set_browser_marker(PAR, MARKER);
		
		%--
		% finish drag if stop, reset widget callbacks
		%--
		
		if strcmp(state, 'stop')	
			set(FIG, 'windowbuttonmotionfcn', [], 'windowbuttonupfcn', []);
		end
		
end


