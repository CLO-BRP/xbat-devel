function [handles, context] = on__marker__create(widget, data, parameter, context)

% AR SPECTRUM - on__marker__create

%--
% display something
%--

ax = findobj(widget, 'type', 'axes', 'tag', 'spectrum');

if isempty(ax)
	return;
end

% cla(ax);

%--
% get marker samples
%--

marker = data.marker;

duck.duration = parameter.frame;

if isfield(marker, 'samples') && ~isempty(marker.samples)
	samples = marker.samples; tag = 'selection';
else
	samples = get_marker_samples(data.marker, duck, context); tag = 'marker';
end

%--
% create and display AR model
%--

a = lpc(samples, parameter.order);

% p = angle(roots(a));

[h, f] = freqz(1, a, 512, context.sound.rate);

handles = create_line(ax, tag, ...
	'xdata', f, ...
	'ydata', 20 * log(abs(h) + eps), ...
	'color', marker.color ...
);

% flatten(context.display.grid)

if isempty(data.page.freq)
	xlim = [0, 0.5 * context.sound.rate];
else
	xlim = data.page.freq;
end

set(ax, 'xlim', xlim);
