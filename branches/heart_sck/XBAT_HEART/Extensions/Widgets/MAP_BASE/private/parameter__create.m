function parameter = parameter__create(context)

% MAP_BASE - parameter__create

parameter = struct;

parameter.type = 'aerial';

% NOTE: this parameter is roughly equivalent to altitude for this type of map

parameter.level = 6;

% NOTE: these should be the coordinates of the center

parameter.lat = 42.443551;

parameter.lon = -76.501980;