function root = cache_root

persistent ROOT;

if isempty(ROOT)
	
	ROOT = create_dir(fullfile(fileparts(mfilename('fullpath')), 'Cache'));
	
	if isempty(ROOT)
		error('Failed to create cache root.');
	end
	
end

root = ROOT;