function parameter = parameter__create(context)

% LIBSNDFILE_BASE - parameter__create

if ~nargin || ~isfield(context, 'file') 
	parameter = struct;
else
	parameter = write_libsndfile(context.file);
end 
