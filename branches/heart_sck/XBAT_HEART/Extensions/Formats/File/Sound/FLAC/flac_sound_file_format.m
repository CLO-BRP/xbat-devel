function ext = flac_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Free Lossless Audio Codec';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'fla', 'flac'};

% ext.version = '';

ext.guid = '0d59c797-ba2b-4a65-b451-cb5eaa72b427';

% ext.author = '';

% ext.email = '';

% ext.url = '';

