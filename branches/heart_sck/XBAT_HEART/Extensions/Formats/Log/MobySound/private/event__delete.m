function result = event__delete(store, id)

% MOBYSOUND - event__delete

result = struct;

lines = file_readlines(store.file);

lines{id} = '';

file_writelines(store.file, lines);
