function [result, id] = event__extension__save(store, id, ext, data, context)

% BARN - event__extension__save

%-----------------
% SETUP
%-----------------

result = struct;

%--
% get extension table names
%--

table = extension_table_names(ext);

%--
% flatten data to store
%--

for k = 1:length(id)
	
	data(k).parameter = flatten(data(k).parameter); 
	
	data(k).value = flatten(data(k).value); 
	
	% NOTE: the auxiliary 'event_id' is the foreign key that links event and values
	
	data(k).value.event_id = id(k);
	
	% NOTE: these are standard fields to support simple addressability and administration of rows
	
	data(k).value.id = []; data(k).value.created_at = []; data(k).value.modified_at = [];
end

parameter = [data(:).parameter]; value = [data(:).value];

%-----------------
% SAVE
%-----------------

%--
% insert parameters if necessary
%--

% NOTE: not all extensions have parameters

if ~trivial(parameter)
	
	% NOTE: parameters are set by a user

	user = set_barn_user(store, get_active_user);
	
	%--
	% establish extension parameters if needed
	%--
	
	% NOTE: the 'prototype' code below just helps our view of the table placing the identifier columns first
	
	if ~has_barn_table(store, table.parameter)
	
		prototype = struct; field = fieldnames(parameter);
		
		prototype.id = 1; prototype.content_hash = 'string';
		
		prototype.created_at = []; prototype.modified_at = [];
		
		for k = 1:numel(field)
			prototype.(field{k}) = parameter.(field{k});
		end

		% NOTE: the 'user_id' is the foreign key that links users, parameters, and values through parameters
		
		prototype.user_id = user.id;
		
		establish_barn_table(store, table.parameter, 'extension', create_table(prototype, table.parameter));
	end
	
	%--
	% append parameters with various keys, the true key is the content hash
	%--

	for k = 1:numel(id)
		
		parameter(k).id = []; parameter(k).content_hash = md5(parameter(k)); 
	
		parameter(k).user_id = user.id; 
	end
	
	%--
	% consider unique values as append candidates, check whether they are stored, append missing
	%--
	
	[current, append] = unique({parameter.content_hash}); append = parameter(append);
	
	for k = numel(append):-1:1
		
		stored = get_database_objects_by_column(store, table.parameter, 'content_hash', append(k).content_hash);
		
		if ~isempty(stored)
			append(k) = [];
		end
		
	end 
	
	if ~isempty(append)
		set_database_objects(store, append, [], fieldnames(append), table.parameter);
	end
	
	%--
	% get current parameters with identifiers and set value foreign key
	%--
	
	current = get_database_objects_by_column(store, table.parameter, 'content_hash', current);
	
	% NOTE: we are breaking the typical foreign key name convention, which would require [table, '_id'], too ugly
	
	for k = 1:length(id)
		
		ix = find(strcmp(parameter(k).content_hash, {current.content_hash}), 1);
		
		value(k).parameter_id = current(ix).id;	
	end
end

%--
% insert values
%--

if ~has_barn_table(store, table.value)
	% NOTE: we are establishing an extension table for the extension values, making sure that we add the event foreign key
	
	opt = create_table; opt.column(end + 1) = column_description('event_id', 'INTEGER', 'UNIQUE NOT NULL');
	
	establish_barn_table(store, table.value, 'extension', create_table(value, table.value, opt))
end

set_database_objects(store, value, [], fieldnames(value), table.value);




