function store = store__create(name, parameter, context)

% BARN - store__create

%--------------------------
% SETUP
%--------------------------

% NOTE: the store is largely the configuration parameters

store = parameter;

%--
% get location and possibly establish parent BARN database schema
%--

result = establish_barn_schema(store);

if ~result.exists
	error('BARN database does not exist.');
end

%--
% get browser info
%--

info = get_browser_info(context.parent);

%--------------------------
% CREATE LOG STORE
%--------------------------

%--
% make sure we have log author, sound, and sound library in database
%--

% NOTE: the next two lines imply that the first person to annotate a sound owns it, not exactly right

user = set_barn_user(store);

sound = set_barn_sound(store, context.sound, user);

% NOTE: this means that a single sound will be shared by various libraries

library = get_barn_library(store, get_library_from_name(info.library));

insert_relation(store, 'sound', sound.id, 'library', library.id);

% NOTE: we also declare that the user is subscribed to the library

insert_relation(store, 'library', library.id, 'user', user.id);

%--
% create new log in database
%--

log.name = name; log.user_id = user.id; log.sound_id = sound.id; log.events_count = 0;

log = set_barn_objects(store, log);

% TODO: currently a log can belong to many sounds, perhaps we want to enforce single sound parentage

% insert_relation(store, 'log', log.id, 'sound', sound.id);

%--
% output database log information as part of store
%--

% NOTE: this is critical, it is where we store the log identifier!

store.log = log;


