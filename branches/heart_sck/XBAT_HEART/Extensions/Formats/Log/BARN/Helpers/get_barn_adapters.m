function [adapters, default] = get_barn_adapters

adapters = setdiff(known_jdbc_adapter, 'Derby');

% NOTE: we are hiding derby, it is not implemented and it is not clear that it will be (under that name at least)

adapters = setdiff({'SQLite', adapters{:}}, 'Derby'); 

% TODO: when we release we should set 'SQLite' as default

default = 1;