function sound = get_barn_sound(store, sound)

% get_barn_sound - ensure local representation of barn sound
% ----------------------------------------------------------
%
% sound = get_barn_sound(store, sound)
%
% Input:
% ------
%  sound - in barn
%
% Output:
% -------
%  sound - local

%--
% get full sound representation from store
%--

% NOTE: the sound has many recordings

sound.recording = get_sound_recordings(store, sound);






%----------------------------
% GET_SOUND_RECORDINGS
%----------------------------

function recording = get_sound_recordings(store, sound)

% get_sound_recordings - representation from store by sound
% ---------------------------------------------------------
%
% recording = get_sound_recordings(store, sound)
%
% Input:
% ------
%  store - database
%  sound - parent
%
% Output:
% -------
%  recording - array for sound

recording_sound = get_database_objects_by_column(store, 'recording_sound', 'sound_id', sound.id);

recording = get_database_objects(store, 'recording', [recording_sound.recording_id]);
