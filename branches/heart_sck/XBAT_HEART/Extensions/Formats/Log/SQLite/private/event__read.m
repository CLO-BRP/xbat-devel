function event = event__read(store, id, order, context)

% SQLITE - event__read

%--
% get events from database
%--

sql = 'SELECT * FROM event;';

if ~isempty(id)
	sql = [sql(1:end - 1), ' WHERE id IN (', str_implode(id, ', ', @int2str), ');'];
end

[status, basic] = sqlite(store.file, sql);

% NOTE: the 'full' event is the typical MATLAB event

event = get_full_event(basic); id = [event.id];

%--
% get core related data, this is in contrast to extension related data
%--

% TAGS

tags = get_table_tags(store.file, 'event', id);

for k = 1:numel(event)	
	event(k) = set_tags(event(k), tags{k});
end

% AUTHOR

user = get_database_users(store.file);

for k = 1:numel(event)
	event(k).author = user(event(k).id == [user.id]).name;
end

% RATING

result = get_event_rating(store.file, id, 'value');

for k = 1:numel(event)
	event(k).rating = result(event(k).id == [result.id]).value;
end

% TODO: implement 'list' relations and 'parent' core relations, to store event sequences and hierarchies

