function store = store__create(name, parameter, context)

% SQLITE - store__create

%--
% build store struct
%--

store = struct;

store.root = create_dir(log_root(name, context.sound, context.library));

store.file = [store.root, filesep, 'events.db'];

% TODO: use the parameters for database configuration 'PRAGMA' commands, look at the Firefox SQLite extension

store.parameter = parameter;

%--
% create basic event and user tables, their relation establishes author
%--

sqlite(store.file, create_event_table);

sqlite(store.file, create_user_table);

establish_relation(store.file, 'event', 'user');

%--
% establish rating, notes, and tags
%--

% RATINGS

% NOTE: setup ratings for events, the user value means each user gets to rate each event

% NOTE: the 'unique' constraint means we allow one rating per event per user

value = struct; value.user_id = 1;

opt = establish_rating_; opt.unique = {'event_id', 'user_id'};

establish_rating_(store.file, 'event', [], value, opt);

% NOTES

% NOTE: this will allow titled-note sets, perhaps a bit better than free-form notes

% NOTE: note there is a relation between events and notes, but also between notes and users

% TODO: the implicit notes and users relation is not discovered by 'select_related' and does not become a view

% NOTE: the same holds above for the relation between users and ratings

sqlite(store.file, create_note_table);

establish_relation(store.file, 'event', 'note', value);

% TAGS

% NOTE: setup tags for events and tags, the latter allow the construction of groups and hierarchies

establish_tags(store.file, {'event', 'tag'});

%--
% create views to make access simpler
%--

% NOTE: this should also help with use by other applications, we capture to suppress display

view = establish_related_views(store.file); %#ok<NASGU>

% DEBUG: while we develop the schema show the database file on creation

if sqlite_trace
	
	show_file(store.file); 

end

