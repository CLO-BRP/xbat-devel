function user = get_log_user(log)

active = get_active_user;

user = get_database_user(log.store.file, active.name);

if isempty(user)
	user = set_database_user(log.store.file, active);
end
