function [status, result] = get_from_view(file, view, id, field, where, order)

% get_from_view - get view columns for selected base identifiers
% --------------------------------------------------------------
% 
% [status, result] = get_from_view(file, view, id, field, where, order)
%
%		    result = get_from_view(file, view, id, field, where, order)
%
%              sql = get_from_view('', view, id, field, where, order)
%
% Input:
% ------
%  file - database
%  view - name
%  id - base identifiers
%  field - column names
%  where - combined through and 
%  order description
%
% Output:
% -------
%  status - of query
%  result - of query
%  sql - query
%
% NOTE: the 'base' table is the first table named in the view
%
% NOTE: tables are also views

%--------------------------
% HANDLE INPUT AND SETUP
%--------------------------

%--
% check for view availability
%--

if nargin < 2 || isempty(view)
	error('View to select from must be provided as input.'); 
end

% TODO: check for view, check for availability of columns

%--
% set default identifier order
%--

% NOTE: conventionally all views will have an identifier column

% NOTE: the order input should only contain the order description, not the known prefix

if nargin < 6 || isempty(order)
	order = 'ORDER BY id';
else
	order = ['ORDER BY ', order];
end

%--
% set no further where constraints
%--

if nargin < 5 || isempty(where)
	where = '';
end

%--
% build output field or column and identifier string
%--

if nargin < 4 || isempty(field)
	fields = '*'; 
else

	if ischar(field), field = {field}; end
	
	% NOTE: we ensure that the base identifier is part of the output
	
	if ~string_is_member('id', field)
		field = {'id', field{:}};
	end
	
	fields = str_implode(field, ', ');

end

%--
% get where part of query from identifier selection
%--

if nargin < 3 || isempty(id);

	where_id = '';

else
	
	if numel(id) == 1
		where_id = ['WHERE id = ', int2str(id)];
	else
		where_id = ['WHERE id IN (', str_implode(id, ', ', @int2str), ')'];
	end
	
end
	
if isempty(where)
	where = where_id;
else
	where = [where_id, ' AND ', where];
end

%--------------------------
% BUILD QUERY AND GET DATA
%--------------------------

%--
% build full query
%--

sql = ['SELECT ', fields, ' FROM ', view, ' ', where, ' ', order, ';'];

% NOTE: we output the query if we have no file to apply to

if isempty(file)
	status = sql; return;
end 

%--
% execute query
%--

[status, result] = sqlite(file, sql);

% NOTE: we keep the result in the case of only one output

if nargout < 2
	status = result;
end 

