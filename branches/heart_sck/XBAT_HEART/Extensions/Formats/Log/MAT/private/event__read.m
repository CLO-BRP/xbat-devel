function event = event__read(store, id, order)

% MAT - event__read

if nargin < 2
    id = [];
end

%--
% get events
%--

event = event_cache(store);

%--
% select by id
%--

% NOTE: we may want to do selection in a way that preserves order

if ~isempty(id)
    event = event(ismember([event.id], id));
end

%--
% sort events by order
%--

if nargin < 3 || isempty(order)
    return;
end