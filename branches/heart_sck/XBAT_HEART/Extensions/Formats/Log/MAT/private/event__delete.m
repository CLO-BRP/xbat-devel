function result = event__delete(store, id)

% MAT - event__delete

result = struct;

%--
% get events from cache
%--

% NOTE: do we want to keep the deleted events?

event = event_cache(store);

%--
% delete events
%--

for k = 1:length(id)
    
    event([event.id] == id(k)) = []; %#ok<NASGU> (used as string input to save)

end

%--
% store result
%--

save(store.file, 'event');

%--
% update cache for this log
%--

event_cache(store, true);
