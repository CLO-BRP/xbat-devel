function store = store__create(name, parameter, context)

% MAT - store__create

%--
% build store struct
%--

store = struct;

root = log_root(name, context.sound, context.library);

root = create_dir(root);

store.file = [root, filesep, 'events.mat'];

store.parameter = parameter;

%--
% create store file
%--

event = empty(event_create); 

save(store.file, 'event', parameter.mat_format); %#ok<NASGU>