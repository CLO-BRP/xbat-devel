function users = get_database_users(file)

% NOTE: this is not so useful as the cache is currently cleared for all files in 'set_database_user'

%--
% establish and check persistent cache
%--

persistent CACHE;

if isempty(CACHE)
	CACHE = {file, get_database_user(file)};
end

ix = find(strcmp(file, CACHE(:, 1))); 

if ~isempty(ix)
	users = CACHE{ix, 2}; return;
end 

%--
% get users from database and update cache
%-- 

users = get_database_user(file);

CACHE{end + 1, 1} = file; CACHE{end, 2} = users;
