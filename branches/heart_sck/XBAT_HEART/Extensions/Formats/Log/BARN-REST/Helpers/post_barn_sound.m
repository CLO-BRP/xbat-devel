function [sound, result] = post_barn_sound(sound, user, server)

%--
% set default local server
%--

if nargin < 2
	server = 'localhost:3000';
end

%--
% create sound resource
%--

% NOTE: the sound is a container for an ordered collection of recordings and related metadata

sound.name = sound_name(sound);

response = curl_rest( ...
	'POST', ['http://', server, '/sounds/create'], ...
	{'Authorization', base64_encode([user.email, ':', user.password])}, ...
	curl_encode_struct(sound, get_barn_prototype('sound'), 'sound') ...
);
	

tool = get_curl;

request = [...
	tool.file, ...
	' -H "Authorization:', base64_encode([user.email, ':', user.password]), '"', ...
	' -X POST http://', server, '/sounds/create', ...
	' -d "sound[samplerate]=22050&sound[duration]=120&sound[samples]=120000"' ...
];

result.request = request; [result.status, result.response] = system(request);

sound = struct;

if ~nargout
	disp(' '); disp(result.response); clear sound; 
end
