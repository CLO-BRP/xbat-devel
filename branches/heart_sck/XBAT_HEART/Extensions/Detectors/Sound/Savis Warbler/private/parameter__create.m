function parameter = parameter__create(context)

% SAVIS WARBLER - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% pext = get_extension('sound_detector','Periodicity'); pfun = ext.fun.parameter.create;


% ----
% set detection / segmentation parameters according to savi's warbler's
% song properties
% ----
% 300fps spectrogram
% ---
parameter.fft = 512;
parameter.frame = 1/150; 
parameter.advance = 0.5;
parameter.max_repfreq = min(parameter.max_repfreq, 60); %Hz

% ---
% NPS resolution
% ---
parameter.temp_res = 20;

% ---
% detection parameters
% ---
parameter.min_repfreq = 44; %Hz

parameter.max_repfreq = min(parameter.max_repfreq, 60); %Hz

parameter.pitch_tolerance = 8; % Hz

parameter.min_domination = 0.15; % 0-1 (%/100)

parameter.max_err = 0.4; % 0-1

parameter.min_segsize = 2.5; % seconds

% frequency bands: flanking [] sw1   sw2  sw3   sw4
parameter.bands = [500 1500 3800 4050  4300  4550 4800];
