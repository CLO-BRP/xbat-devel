function parameter = parameter__load(store, context)

% DATA TEMPLATE - parameter__load

db_disp custom-preset-load

% NOTE: this is the default initialization, in this case it also prevents 'parameter unassigned' warning

parameter = struct;

store = [store, '.mat'];

% NOTE: this implicitly loads 'parameter' variable

load(store);
	