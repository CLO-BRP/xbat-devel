#include <time.h>
#include <fpc.h>

#define BUFFER_LATENCY 1

/*
 * typedefs
 */

//typedef short sample_t;
typedef int offset_t;
typedef float score_t;
typedef float freq_t;
typedef int (*IFP)();

typedef struct { 
	offset_t t_in; 
	offset_t t_dt;
	freq_t f_lo;
	freq_t f_df;
	score_t score;
	char * data;
} event_t;

typedef struct {
	int rate;
	int N;
} sound_setup;

/*
 * global detector parameter settings
 */

float get_report_thresh();

void set_report_thresh(float t);

float get_clip_thresh();

void set_clip_thresh(float t);

float get_store_thresh();

void set_store_thresh(float t);


/*
 * event buffer operations
 */

int get_n_events();

void set_n_events(int n);

void inc_n_events(int n);

event_t * get_events();

time_t get_time();

void set_time(time_t time);

/*
 * sample buffer operations
 */

int initialize_sample_buffer(int N, int spb);

int get_sample_buffer_index();

sample_t * get_this_sample_buffer();

sample_t * get_offset_sample_buffer(int offset);

void write_to_sample_buffer(sample_t * buf);

int write_sample_buffer_file(const char * fname, int samplerate, int ix1, int ix2, int big_k);

/*
 * mutex operations
 */

int mutex_unlock();

int mutex_lock_try();

int mutex_lock_block();

int initialize_mutex();

int mutex_destroy();

/*
 * write_events - write out static event array to sqlite database
 */

void write_events(int rate);

/*
 * setup_sound - get audio engine params from plugin
 */

sound_setup * setup_sound();

/*
 * detect_gateway - algorithm processing gateway
 */

int detect_gateway(sample_t * buf, int nbuf, time_t tbuf);

/*
 * initialize_gateway - algorithm initialization gateway
 */

int initialize_gateway(const char * name, const char * config_file);

/*
 * cleanup_detector - clean up everything
 */

int cleanup_detector(void * parameters, void * state);

/*
 * parameters and state access methods
 */

void * parameters_access(int mode, const char * config_file);

void * state_access(int mode);








