function parameter = parameter__create(context)

% EMBEDDED - parameter__create

parameter.path = fileparts(mfilename('fullpath'));

parameter.detector = '';

parameter.config_file = '';

parameter.gain = 0;
