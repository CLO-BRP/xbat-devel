function [event, value, context] = compute(page, parameter, context)

% AMPLITUDE - compute

%-------------------------------------
% SETUP
%-------------------------------------

%--
% initialize empty event array 
%--

event = empty(event_create);

%--
% compute parent feature
%--

% NOTE: this allows us to use this detection strategy on every generalized amplitude

if ~isfield(page, 'feature')
	fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);
else
	feature = page.feature;
end

%-------------------------------------
% DETECT
%-------------------------------------

% NOTE: we pack various intermediate computations into feature, these are passed to the view

%--
% compute signal estimate
%--

% NOTE: this is based on the amplitude smoothed to the temporal scale of signal

feature.rms.smooth.value = get_smooth(feature, parameter);

%--
% compute noise estimate
%--

noise = get_amplitude_noise_estimate(feature, parameter); 

feature.rms.noise.value = noise;

%--
% peak selection process
%--

% TODO: improve output of peak analysis to help with smooth versus raw peak consideration

peak = get_peaks(feature);

feature.rms.smooth.peak = peak;

peak = select_peaks(peak, noise, parameter);

%--
% pack view data before we possibly return because there are no relevant peaks
%--

% NOTE: if there are no significant peaks, there are no events and we are done

context.view.data = feature;

if ~numel(peak.value)	
	 return;
end

%--
% set common event frequency bounds possibly using parameters
%--

% NOTE: this is a service we provide for our children

available = isfield(parameter, 'min_freq') && isfield(parameter, 'max_freq');

if available
	freq = [parameter.min_freq, parameter.max_freq];
else
	% TODO: consider this arbitrary band selection
	
	freq = [0, 0.88 * 0.5 * context.sound.rate];
end

%--
% create events from peaks
%--

% NOTE: these events are amplitude atoms at the set observation scale, these are frequently notes or fractions

for k = 1:numel(peak.value)
	
	%--
	% compute event time interval based on smoothed amplitude peak analysis
	%--
	
	% TODO: there is a discrepancy here, figure out what the source of this is

	% [feature.time_step, parameter.block]

	% NOTE: we have to subtract the page start, the detector outputs page relative time
	
	time = feature.time(peak.index(k)) + feature.time_step * ([-1, 1] .* peak.width(:, k)') - page.start;
	
	%--
	% create event
	%--
	
	% TODO: improve event scoring, we should consider more than just peak, some kind of extent or concentration should be useful
	
	% NOTE: be careful, the score should match the semantics of the detector
	
	event(end + 1) = event_create( ...
		'channel', page.channels, 'time', time, 'freq', freq, 'score', get_snr_score(peak.ratio(k)) ...
	);
	
end

% db_disp 'set scores'; snr = peak.ratio', [event.score]

%-------------------------------------
% GROUP
%-------------------------------------

%--
% return quickly if we are not performing any joining
%--

% NOTE: this is an undocumented way of not filling gaps, it returns the raw output of simple detection

if ~parameter.join_contiguous && (parameter.break == 1)
	return;
end

%--
% we simply join events that are contiguous
%--
	
if parameter.join_contiguous
	
	event = merge_events(event, parameter);

%--
% here coalesce contiguous events considering various properties
%--

else
	
	[event, feature] = coalesce_events(event, peak, parameter, feature); 

end
	
%--------------------
% PACK VIEW
%--------------------

context.view.data = feature;


db_disp test-detector-values

value = repmat(struct, size(event));


%-------------------------------
% SELECT_PEAKS
%-------------------------------

% NOTE: this function is tied to the parameters and the packing of the peaks

function peak = select_peaks(peak, noise, parameter)

%--
% perform tests on peak quality
%--

% TODO: consider how to turn something like this into a score

% NOTE: currently we are selecting based on the smoothed value not the raw value

% TODO: consider using a 'max' instead of the addition for the 'noise_floor'

ratio = (peak.value ./ (noise(peak.index) + parameter.noise_floor)).^2;

t1 = ratio >= parameter.snr; % yes

if ~parameter.threshold

	ix = t1;
	
else
	
	t2 = ratio >= 2 * parameter.snr; % yes
	
	t3 = peak.value >= 2 * parameter.threshold; % yes

	t4 = peak.value < parameter.threshold; % no

	% NOTE: accept if we exceed twice the SNR threshold, or if we exceed the SNR threshold along with a min peak amplitude

	ix = t1 & (t2 | t3) & ~t4;
	
end

%--
% select peaks based on the result of tests
%--

peak.value = peak.value(ix); 

peak.index = peak.index(ix); 

peak.height = peak.height(:, ix); 

peak.width = peak.width(:, ix);

peak.raw.value = peak.raw.value(ix);

peak.raw.index = peak.raw.index(ix);

peak.ratio = ratio(ix);


%-------------------------------
% GET_SMOOTH
%-------------------------------

% TODO: we want to extend this to a scale-space computation

function value = get_smooth(feature, parameter)

%--
% get smoothing filter length
%--

% NOTE: the 'duration' parameter indicates the time-scale of interest

n = get_next_odd(parameter.duration / feature.time_step); 

if n == 1, return; end

%--
% apply smoothing filter to simplify feature signal
%--

% NOTE: we want filtering that is causal in the sense that it does not create new extrema

value = linear_filter(feature.rms.value, filt_binomial(n, 1));

% X = median_filter(X, ones(n, 1));


%-------------------------------
% GET_PEAKS
%-------------------------------

% TODO: reconsider the packing in this function, it will affect this extension and children

function peak = get_peaks(feature)

X = feature.rms.value; Y = feature.rms.smooth.value;

%--
% compute peaks in smoothed feature signal
%--

[peak.index, peak.height, peak.width] = fast_peak_valley(Y, 1); peak.value = Y(peak.index); 

%--
% compute peak value as the largest raw value in peak basin
%--

peak.raw.value = zeros(size(peak.value)); peak.raw.index = zeros(size(peak.value));

for k = 1:numel(peak.index)	
	% NOTE: we can also get an offset, this is more like the analysis used in template correlation, factor
	
	start = peak.index(k) - peak.width(1, k); stop = peak.index(k) + peak.width(2, k);
	
	[peak.raw.value(k), offset] = max(X(start:stop)); peak.raw.index(k) = start + offset - 1;
end


%-------------------------------
% GET_NOISE_ESTIMATE
%-------------------------------

% TODO: do our own padding of the feature, use a relatively quiet section of the current page

function noise = get_noise_estimate_OLD(feature, cutoff) %#ok<DEFNU>

% NOTE: we assume a feature of a given rate and a cutoff in Hertz

noise = feature.rms.value;

%--
% design lowpass filter and filter
%--

% TODO: refine our choice of filter, and that we may iteratively use the same filter

cutoff = cutoff * 2.^(-(1:3));

% cutoff = cutoff * 2.^([1:-1: 0, -(1:3)])

for k = 1:numel(cutoff)

	[b, a] = butter(4, cutoff(k) / (0.5 * feature.rate));

% 	n = impzlength(b, a);
% 	
% 	if n > 2 * length(feature.rms.value)
% 		break;
% 	end
	
	% NOTE: at this coarse scale phase offset may matter
		
	noise(:, k) = filtfilt(b, a, feature.rms.value);
	
	% NOTE: disregard negative estimates, 'inf' will not be selected by the 'min' computation
	
	noise(noise(:, k) < 0, k) = inf;
	
end

% db_disp 'noise'; noise

% noise = mean(noise, 2);

noise = min(noise, [], 2); 

% db_disp 'noise summary'; five = [min(noise), fast_rank(noise, 0.25), median(noise), fast_rank(noise, -0.25), max(noise)]
	

%-------------------------------
% MERGE_EVENTS
%-------------------------------

function event = merge_events(event, parameter)

%--
% simply join contiguous events
%--

for k = numel(event):-1:2

	gap = event(k).time(1) - event(k - 1).time(2);

	% NOTE: we are not using the 'fill gaps' parameter, perhaps it should be used here

	if gap < 4 * parameter.duration
		event(k - 1).time(2) = event(k).time(2); event(k - 1).score = max(event(k - 1).score, event(k).score); event(k) = [];
	end

end

event = update_event_duration(event);

%--
% indicate that events are the result of a merge operation
%--

% event = set_tags(event, 'MERGED');


%-------------------------------
% COALESCE_EVENTS
%-------------------------------

function [event, feature] = coalesce_events(event, peak, parameter, feature)

%--
% loop over events considering linkage
%--

feature.breaks = [];

for k = numel(event):-1:2
	
	%--
	% get event gap and check for contiguous events
	%--
	
	gap = event(k).time(1) - event(k - 1).time(2);
	
	% NOTE: for LARGE gaps there is nothing to do, continue
	
	if gap > 4 * parameter.duration
		continue; 
	end
			
	%--
	% update contiguous event boundaries to fill gaps
	%--

	% NOTE: below we find a valley between the peaks to place the event break
	
% 	event(k).time(1) = event(k).time(1) - 0.5 * gap; event(k - 1).time(2) = event(k - 1).time(2) + 0.5 * gap;

	% NOTE: here we declare that the k and k - 1 are linked
	
	event(k - 1).next = k; event(k).previous = k - 1;

	%--
	% find minimum amplitude between the two event peaks
	%--

	% NOTE: we use the raw RMS signal not the smoothed version here to get the location
	
	%valley = min(feature.rms.value(peak.index(k - 1):peak.index(k)));
	
	[valley, ix] = min(feature.rms.value(peak.index(k - 1):peak.index(k)));
	
	% NOTE: we have to derefence the index from the 'min' computation
	
	ix = ix(1); ix = ix + peak.index(k - 1) - 1; feature.breaks(end + 1) = ix;

	% NOTE: we use the smooth in the value comparison
	
	valley = feature.rms.smooth.value(ix);
	
	% NOTE: we move the event break to the valley location
	
	center = feature.time_step * ix;
	
	event(k).time(1) = center; event(k - 1).time(2) = center;
	
	%--
	% test whether there is a relative quiet moment between two contiguous events
	%--

	% NOTE: we check the valley amplitude ratio with respect to both neighboring peaks

	% NOTE: when the weaker peak-valley ratio exceeds required contrast we skip join
	
	% TODO: consider this test carefully by looking at various kinds of data
	
	ratio = [peak.value(k), peak.value(k - 1)] ./ valley;
	
	contrast = 2; % 1 / parameter.break

	if min(ratio) < contrast && (max(ratio) / min(ratio)) < 2 * contrast

		event(k - 1).time(2) = event(k).time(2);
		
		for j = k + 1:numel(event)

			current = event(j);

			if current.previous, current.previous = current.previous - 1; end

			if current.next, current.next = current.next - 1; end

			event(j) = current;

		end

		event(k - 1).next = max(0, event(k).next - 1); 
		
		event(k - 1).score = max(event(k - 1).score, event(k).score);
		
		event(k) = [];

	end

end

event = update_event_duration(event);
%--
% indicate that events are the result of coalescing
%--

% event = set_tags(event, 'COALESCED');

