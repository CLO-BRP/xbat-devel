function parameter = parameter__create(context)

% RENYI DETECTOR - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.threshold = .73;