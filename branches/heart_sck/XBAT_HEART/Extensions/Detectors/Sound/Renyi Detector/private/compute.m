function [event, context] = compute(page, parameter, context)

% RENYI DETECTOR - compute

% BEAR, BADGER, contextual-hysteresis, band, entropy, amplitude, recurrence, grouping

%--
% compute parent feature and initialize event array
%--

event = empty(event_create);

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% analyze entropy sequence to determine event time boundaries
%--

entropy = feature.reduction;

selection = entropy.value < parameter.threshold;

% TODO: add some flexibility to the interval evaluation to make robust to noise

% NOTE: this may involve significant peak detection, smarter thresholding, and some kind of hysteresis

[start, stop] = get_intervals(selection);

freq = [0, 0.4 * context.sound.rate];

for k = 1:numel(start)

	% TODO: consider if we should extend the intervals to start and end a sample earlier and later respectively
	
	time = feature.time([start(k), stop(k)]) - page.start;

	event(end + 1) = event_create( ...
		'channel', page.channels, 'time', time, 'freq', freq ...
	); %#ok<AGROW>

end

% TODO: the detector handler should make event data available to the view

context.view.data = feature;


%-----------------------
% GET_INTERVALS
%-----------------------

function [start, stop, duration] = get_intervals(signal)

%--
% get start and stop of binary signal
%--

step = diff(signal); start = find(step == 1) + 1; stop = find(step == -1);

% NOTE: handle simple edge effects for intervals that start and end at page boundaries

try
    
    if numel(start) > numel(stop)
        
        stop(end + 1) = numel(signal);
        
    elseif numel(start) < numel(stop)
        
        start = [1, start];
        
    elseif start(1) > stop(1)
        
        start = [1, start]; stop(end + 1) = numel(signal);
        
    end
    
end
%--
% compute duration if needed
%--

if nargout > 2, duration = stop - start + 1; end


