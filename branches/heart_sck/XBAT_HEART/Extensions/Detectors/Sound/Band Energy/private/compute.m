function [event, out_value, context] = compute(page, parameter, context)

% BAND ENERGY - compute

%--
% initialize output and some convenience variables
%--

event = empty(event_create); out_value = struct;

nyq = 0.5 * context.sound.rate;

%-------------
% SETUP
%-------------

%--
% compute band energies and get Amplitude compute workhorse function
%--

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

ext = get_extensions('sound_detector','name','Amplitude'); fun = ext.fun.compute;

%-------------
% DETECT
%-------------

% NOTE: we simply call Amplitude detector for each of the Band Energy curves, this requires some adaptation

%--
% select channel band energies if needed
%--

if length(page.channels) > 1
    value = feature.band_energy.value{page.channel};
else
    value = feature.band_energy.value;
end

%--
% detect on each band
%--

% NOTE: we observe some time shiftig during the amplitude detector.
% this is probably caused by an spectrogram offset

timediff = page.start-feature.time(1);

for j = 1:size(value,1)

    % NOTE: we copy the band energy results to a foreign field
	
    feature.rms.value = value(j,:)';
	
    % NOTE: we pack the 'amplitude' feature for the amplitude detector 
	
    page.feature = feature;

    % NOTE: setting the 'min_freq' and 'max_freq' parameters transfers these to the events
	
    parameter.min_freq = parameter.band_details(1,j) * 1.05 * nyq;
	
    parameter.max_freq = parameter.band_details(3,j) * 0.95 * nyq;

    %--
	% call Amplitude compute on band and append results
	%--
    [b_event, b_context] = fun(page, parameter, context);
    
    % NOTE: ATTENTION: Here, a spectrogram offset is subtracted!
    
	for k=1:numel(b_event)
        b_event(k).time=b_event(k).time-timediff;
    end
	
    firstev=numel(event)+1;
	event = [event, b_event];
    lastev=numel(event);
    
    for k=firstev:lastev
        out_value(k).band_nr=j;
    end
end

%--
% compute event network
%--
feature.event.event = event;
feature.event.value=out_value;

% get SCC's for the events
feature.event.component=get_event_component(feature);

context.view.data=feature;

%-----------------------------
% GET_EVENT_COMPONENT
%-----------------------------

function label = get_event_component(feature)
% G = graph_event_overlap(feature);
% G = graph_event_coincident(feature);
 G = graph_event_similarspec(feature);
[step, label] = graph_dfs(G);


%-----------------------------
% Various Event Feature Graphs
%-----------------------------

function G = graph_event_overlap(feature)
% this calculates a vey basic graph depending on the overlap of elements

A = event_intersect(feature.event.event);

G.E = sparse_to_edge(double(A ~= 0));


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

function G = graph_event_coincident(feature)
% here, we are interrested in the temporal coincidence of elements

% get event times
times = [feature.event.event.time]; 

%split times into start and stops
starts=times(1:2:end);
stops=times(2:2:end);
durations=[feature.event.event.duration];

% compute start + stop similarity
start_match=zeros(numel(starts));
stop_match=zeros(numel(stops));
for j= 1:numel(starts)
    for k=1:numel(starts)
        
        % NOTE: we use absolute values here
        tstart_match=abs(starts(j)-starts(k));
        tstop_match=abs(stops(j)-stops(k));
        if ~(tstart_match > durations(j))
            start_match(j,k) = 1-tstart_match/durations(j);
        end
        if ~(tstop_match > durations(j))
            stop_match(j,k) = 1-tstop_match/durations(j);
        end
    end
end

G.E = sparse_to_edge((start_match > 0.6) & (stop_match > 0.6));


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------


function G = graph_event_similarspec(feature)   
% this should give correlation based similarity measurements for events
% within the same band

% for each band: compare events
A=zeros(numel(feature.event.event));
normcoeff=zeros(1,numel(feature.event.event));

for k=1:size(feature.band_energy.value,1)
    ix = find([feature.event.value.band_nr] == k);
    
    %note: we just save correlations for half of all the combinations.
    %later, symmetry will be used to fill the full matrix.
    for i=1:numel(ix)
        act_event=feature.event.event(ix(i));
        
        % get event position in spectrogram
        [null, timepos(1)] = min(abs(feature.time-feature.time(1)-act_event.time(1)));
        [null, timepos(2)] = min(abs(feature.time-feature.time(1)-act_event.time(2)));
        [null, freqpos(1)] = min(abs(feature.freq-act_event.freq(1)));
        [null, freqpos(2)] = min(abs(feature.freq-act_event.freq(2)));
        
        % NOTE: we ensure that one of the images has an odd number of
        % columns to fix the grid position
        if ~mod(diff(freqpos)+1, 2), freqpos(2)=freqpos(2)-1; end
        if ~mod(diff(timepos)+1, 2), timepos(2)=timepos(2)-1; end
                
        
        % cut event out of specgram
        spec1=feature.spectrogram.value(freqpos(1):freqpos(2),timepos(1):timepos(2));
        
        % NOTE: correlation coefficients are norma relative to spec1, thus
        % similarity is not symmetric
        % here, we just save the normalizaion coefficient
        normcoeff(ix(i))= sum(sum(spec1));
        
        for j=1:i-1
            act_event2=feature.event.event(ix(j));
            [null, timepos(1)] = min(abs(feature.time-feature.time(1)-act_event2.time(1)));
            [null, timepos(2)] = min(abs(feature.time-feature.time(1)-act_event2.time(2)));
            spec2=feature.spectrogram.value(freqpos(1):freqpos(2),timepos(1):timepos(2));
            
            % get event correlation
            A(ix(i),ix(j)) = compare_event_specs(spec1,spec2,1);
        end
    end
end

% add symmetric part of the matrix
A=A+A';

G.E = sparse_to_edge(A > 0.7); 


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------


function corr=compare_event_specs(spec1,spec2,normcoeff)

%--
% compute normalized correlation
%--

% paratererize correlation function
opt = image_corr; opt.pad_row = 0; opt.pad_col= 1;

[corr, ix] = max(image_corr(spec1, spec2, opt));


% ---
% legacy routine:
%  --
% we get a normalized maximum cross-correlation coeffitioent here.
% denominator for normailzation is the energy in the first spectrum
%  --

% if nargin < 3
%    normcoeff= sum(sum(spec1.^2));
% end
% % get 2dimensional cross-correlation
% allcorr=xcorr2(spec1,spec2);
% 
% % restrict autocorrelation to temporal axis
% % --
% % NOTE: we expect both spetra to have the same amount of rows.
% %--
% allcorr=allcorr(size(spec1,1),:);
% [corr, ix] = max(allcorr);
% 
% %--
% % normalize correlation
% %--
% 
% corr=corr./normcoeff;


