function [parameter, context] = parameter__compile(parameter, context)

% AMPLITUDE DETECTOR II - parameter__compile

% NOTE: this actually comes from the grandparent

fun = parent_fun(mfilename('fullpath')); [parameter, context] = fun(parameter, context);

%--
% DESIGN FILTER
%--

%--
% get normalized frquency specification
%--

band = [parameter.min_freq, parameter.max_freq] / (0.5 * context.sound.rate); 

%--
% no filtering
%--

% NOTE: when both band edges are extremes no filtering is needed, no filter is appended to parameters

if isequal(band, [0, 1])
	return;
end 

%--
% low or high pass
%--

% NOTE: when one of the band edges is an extreme we have a special case

if band(1) == 0
	[parameter.filter.b, parameter.filter.a] = butter(parameter.butter_order, band(2), 'low'); return;
end

if band(2) == 1
	[parameter.filter.b, parameter.filter.a] = butter(parameter.butter_order, band(1), 'high'); return; 
end

%--
% general case
%--

% NOTE: this may currently fail for some 'band' values, these values should be limited

% db_disp 'at some point this field is scalar' ; caller = get_caller; stack_disp(caller.stack); parameter.filter

[parameter.filter.b, parameter.filter.a] = butter(parameter.butter_order, band);
