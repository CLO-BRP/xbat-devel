function event = weld_event_chain(chain)

%--
% handle multiple chains
%--

if numel(chain) > 1
	event = iterate(mfilename, chain); return;
end

%--
% get and weld chain elements
%--

% NOTE: we output an event that starts with the first element and ends with the last

event = chain.element(1); event(1).time(2) = chain.element(end).time(2); event = update_event_duration(event);