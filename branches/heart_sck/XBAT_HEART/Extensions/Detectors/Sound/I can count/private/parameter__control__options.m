function opt = parameter__control__options(context)

% I CAN COUNT - parameter__control__options

% opt = struct;

fun = parent_fun(mfilename('fullpath')); opt = fun(context);

opt.width = 14;
