function ext = zcdetector_sound_detector

ext = extension_inherit(mfilename, zcrate_sound_feature);

ext.short_description = 'Detect interesting events using ZC-Rate';

% ext.category = {ext.category{:}};

ext.version = '';

ext.guid = 'b85d4e15-4c9d-404f-a8bd-72326ae672d4';

ext.author = 'Harold and Holger';

ext.email = '';

ext.url = '';

