function control = parameter__control__create(parameter, context)

% ZC-DETECTOR - parameter__control__create

%--
% get parent controls
%--

fun = parent_fun(mfilename('fullpath')); inherited = fun(parameter, context);

% NOTE: to understand this code you should look at the parent code

%--
% update tab organization
%--

control = empty(control_create);

control(end + 1) = inherited(1); inherited(1) = [];

tabs = {'Duration', control(end).tab{:}};

control(end).tab = tabs;

%--
% create duration controls
%--

% TODO: perhaps there is a reasonable minimum based on the rate and block size

control(end + 1) = control_create( ... 
	'name', 'min_duration', ...
	'style', 'slider', ... 
	'tab', tabs{1}, ...
	'min', 0.005, ... 
	'max', 10, ...
	'value', parameter.min_duration ...
);

control(end + 1) = control_create( ... 
	'name', 'max_duration', ...
	'style', 'slider', ... 
	'tab', tabs{1}, ...
	'min', 0.05, ... 
	'max', 10, ...
	'value', parameter.max_duration ...
);
	
	control(end + 1) = control_create( ... 
	'name', 'tolerance_level', ...
	'style', 'slider', ... 
	'tab', tabs{1}, ...
	'min', 0.0000001, ... 
	'max', 1, ...
	'value', parameter.tol ...
);
	
%--
% reattach parent controls to control array
%--

control = [control, inherited];

