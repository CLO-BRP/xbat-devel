function sessions = get_sound_sessions(sound, collapse)

% get_sound_sessions - get the recording session boundaries
% ---------------------------------------------------------
%
% sessions = get_sound_sessions(sound, collapse)
%
% Inputs:
% -------
% sound - sound
% collapse - collapse to recording time
%
% Outputs:
% --------
% sessions - session array

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% return empty for no sessions
%--

if isempty(sound.time_stamp) || ~isfield(sound.time_stamp, 'table') || isempty(sound.time_stamp.table)
	sessions = []; return;
end

%--
% get collapse from sound if needed
%--

% NOTE: collapse input allows override of sound collapse state

if nargin < 2 || isempty(collapse)
	collapse = sound.time_stamp.collapse;
end

%-------------------------
% BUILD SESSIONS
%-------------------------

%--
% compute session durations
%--

duration = diff([sound.time_stamp.table(:, 1); sound.duration]);

%--
% compute session start and stop times
%--

if collapse
	start = sound.time_stamp.table(:, 1);
else
	start = sound.time_stamp.table(:, 2);
end

stop = start + duration;

%--
% pack sessions
%--

for k = 1:length(start)
	sessions(k).start = start(k); sessions(k).end = stop(k);
end

