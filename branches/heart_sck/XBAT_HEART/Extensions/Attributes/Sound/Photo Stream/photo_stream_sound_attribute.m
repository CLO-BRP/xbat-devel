function ext = photo_stream_sound_attribute

ext = extension_create(mfilename);

ext.short_description = 'Photos associated with sound';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'cd942acf-def4-4891-9820-f939b0521cde';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

