function [result, context] = prepare(parameter, context)

% EXPORT CSV - prepare

result = struct;

context.state.out = get_fid([tempname, '.csv'], 'wt');