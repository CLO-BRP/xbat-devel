function parameter = parameter__create(context)

% EXPORT CSV - parameter__create

parameter = struct;

parameter.prepare = @prepare_csv_event;