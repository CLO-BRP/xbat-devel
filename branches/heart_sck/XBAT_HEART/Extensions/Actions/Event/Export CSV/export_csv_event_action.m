function ext = export_csv_event_action

ext = extension_create(mfilename);

ext.short_description = 'Export basic event information to CSV file';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'dd9044d4-4eef-489b-ae43-9b81e35e47aa';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

