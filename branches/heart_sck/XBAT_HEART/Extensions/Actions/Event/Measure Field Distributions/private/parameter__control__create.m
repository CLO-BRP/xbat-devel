function control = parameter__control__create(parameter, context)

% MEASURE FIELD DISTRIBUTIONS - parameter__control__create

control = empty(control_create);

ext = get_extensions('event_measure'); measures = {ext.fieldname};

ix = find(strcmp(measures, parameter.measure));

if isempty(ix)
	ix = 1;
end

control(end + 1) = control_create( ...
	'name', 'measure', ...
	'style', 'popup', ...
	'string', measures, ...
	'value', ix ...
);