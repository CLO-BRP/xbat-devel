function parameter = parameter__create(context)

% CLIP - parameter__create

parameter.format = 'WAV';

parameter.output = get_clip_directory;

parameter.show_files = 1;

parameter.padding = 0.5;

parameter.taper = 0;

