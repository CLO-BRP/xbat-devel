function parameter = parameter__create(context)

% EVENT ACTION - parameter__create

parameter = struct;

actions = get_extension_names('event_action');

% NOTE: this is the first action in alphabetical order

if isempty(actions)
	action = '';
else
	action = actions{1};
end

parameter.action = action; 