function [parameter, context] = parameter__compile(parameter, context)

% ITERATE EVENT ACTION - parameter__compile

%--
% check for control values
%--

% NOTE: we only need to compile after the action dialog is presented and we have non-trivial control values

if trivial(context.ext.control)
	return; 
end

%--
% get event action extension
%--

% NOTE: this should not be required, the parent of a log action is typically the root

if isempty(context.par)
	context.par = 0;
end 

% NOTE: this should not be required, 'popup' controls should return strings!

if iscell(parameter.action)
	action = parameter.action{1};
else
	action = parameter.action;
end 

[ext, ignore, context2] = get_browser_extension('event_action', context.par, action);

%--
% configure event action extension using dialog values
%--

% NOTE: we update using all dialog values, these contain the hosted extension values

ext.parameter = struct_update(ext.parameter, context.ext.control);

ext.control = context.ext.control;

context2.ext = ext;

if ~isempty(ext.fun.parameter.compile)

	try
		[ext.parameter, context2] = ext.fun.parameter.compile(ext.parameter, context2);
	catch
		extension_warning(ext, 'Parameter compilation failed.', lasterror);
	end

end

%--
% add compiled event action extension and context to compiled parameters
%--

parameter.ext = ext;

parameter.context = context2;

