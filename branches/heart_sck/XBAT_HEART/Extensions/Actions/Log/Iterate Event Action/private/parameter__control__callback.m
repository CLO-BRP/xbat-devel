function result = parameter__control__callback(callback, context)

% EVENT ACTION - parameter__control__callback

result = struct;

%--
% unpack input for some convenience
%--

pal = callback.pal; ext = context.ext;

%--
% get hosted event action name
%--

action = get_control(callback.pal.handle, 'action', 'value'); action = action{1};

%--
% dispatch callback 
%--

switch callback.control.name
	
	case 'action'
	
		% NOTE: this gets both this action's controls as well as the event action controls
		
		ext = context.ext; parameter = context.ext.parameter; parameter.action = action;
		
		[control, opt] = get_action_controls(ext, parameter, context);
		
		%--
		% reload dialog
		%--
		
		% TODO: figure out how to handle options
		
		% NOTE: we get dialog properties so that we may preserve some
		
		props = get_palette_property(pal.handle);
			
		dialog_group(props.name, control, [], props.opt.fun, [], pal.handle);
		
	otherwise
		
		% TODO: pass these callback to the hosted event action extension callback
		
		if isempty(context.par)
			context.par = 0;
		end
		
		[ext, ignore, context2] = get_browser_extension('event_action', context.par, action);
		
		if ~isempty(ext.fun.parameter.control.callback)
			
			try
				result = ext.fun.parameter.control.callback(callback, context2);
			catch
				extension_warning(ext, 'Callback failed.', lasterror);
			end
			
		end
		
end

