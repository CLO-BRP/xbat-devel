function [result, context] = compute(log, parameter, context)

% TEMPORAL DISTRIBUTION - compute

result = struct;

parameter.threshold = 0.4;


db_disp; context.sound

%--
% determine tags used in log
%--

% TODO: this should be factored as 'get_log_events_by_tag', it uses low-level database access code

% NOTE: currently this gets all tags in log store, not just the log

tags = get_log_tags(log); 

% NOTE: this should not be needed when the above function works as promised

for k = numel(tags):-1:1

	[ignore, event] = find_by_tag(log.store, tags{k}, [], 'event');
	
	if all([event.log_id] ~= log.store.log.id)
		tags(k) = [];
	end
	
end

%--
% setup for plotting
%--

color = plot_colors(numel(tags));

time = linspace(0, get_sound_duration(log.sound), 1024);

width = [3600, 600, 300]; linestyle = {':', '-', '-'}; linewidth = [1, 1, 2];

%--
% create and layout display figure
%--

par = fig; 

layout = layout_create(numel(tags), 2); 

layout.margin(4) = 1.5; 

layout.col.frac = [0.75, 0.25]; layout.col.pad = 1;

layout.row.pad = 1.25 * ones(1, numel(tags) - 1);

% TODO: 'harray_select' should output a matrix of handles arrayed as the axes

harray(par, layout); ax = harray_select(par, 'level', 1); ax = reshape(ax(:), numel(tags), 2);

%--
% compute density estimates and display
%--

for k = 1:numel(tags)
	
	%--
	% get log events with tag
	%--
	
	[ignore, event] = find_by_tag(log.store, tags{k}, [], 'event');
	
	event = event([event.log_id] == log.store.log.id);
	
	% NOTE: make sure that we have some score when events are manual events, assume manual events are correct
	
	for j = 1:numel(event)	
		if isempty(event(j).score), event(j).score = 1; end	
	end
	
	% NOTE: censor low quality events for density estimation
	
	handle = [];
	
	
	
	
	
	
	for j = 1:numel(width)
	
		% NOTE: use reflecting boundary condition to estimate edges
		
		data = [event.start];
		
		weights = [event.score] > parameter.threshold; count = sum(weights);
		
		
		
		% TODO: there is a more general way of imputing the time values
		
		ix = find(data < width(j), 1); 
		
		if ~isempty(ix)
			prefix = -fliplr(data(1:ix)); data = [prefix, data]; weights = [ones(size(prefix)), weights];
		end
		
		ix = find(data > (time(end) - width(j)), 1); 
		
		if ~isempty(ix)
			postfix = time(end) + fliplr(data(ix:end)); data = [data, postfix]; weights = [weights, ones(size(postfix))];
		end
		

		

		density{j} = count * ksdensity(data, time, 'width', width(j), 'weights', weights);

		handle(j) = line( ...
			'parent', ax(k, 1), ...
			'xdata', time, ... 
			'ydata', density{j}, ...
			'color', color(k, :), ...
			'linestyle', linestyle{j}, ...
			'linewidth', linewidth(j) ...
		);

	end
	
	legend(handle, '1HR', '10M', '5M');
	
	points = [event.start]; points = points([event.score] > parameter.threshold);
	
	display_data(ax(k, 1), points, 'color', color(:, k));

	axes(ax(k, 1));
	
	title([tags{k}, ', N(SCORE > ', num2str(parameter.threshold),') = ', int2str(count)]);
	
	ylabel('CALL RATE');
	
	set(ax(k, 1), ...
		'xlim', time([1, end]) ...
	);

	if k == numel(tags)
		xlabel('TIME');
	end
	
	%--
	% display score distribution
	%--
	
	score = ksdensity([event.score], linspace(0, 1));
		
	line( ...
		'parent', ax(k, 2), ...
		'xdata', linspace(0, 1), ... 
		'ydata', score, ...
		'color', color(k, :), ...
		'linewidth', linewidth(end) ...
	);

	display_data(ax(k, 2), [event.score], 'color', color(:, k));
	
	line( ...
		'parent', ax(k, 2), ...
		'xdata', parameter.threshold * ones(1, 2), ... 
		'ydata', get(ax(k, 2), 'ylim'), ...
		'color', zeros(1, 3), ...
		'linestyle', ':', ...
		'linewidth', 1 ...
	);

	axes(ax(k, 2));
	
	title(['N = ', int2str(numel(event))]);
	
	set(ax(k, 2), ...
		'yaxislocation', 'right' ...
	);

	ylabel('COUNT');
	
	if k == numel(tags)
		xlabel('SCORE');
	end
	
end

%--
% add some behavior
%--

zoom(par, 'xon');

linkaxes(ax(:, 1));

linkaxes(ax(:, 2));


%-------------------------------
% DISPLAY_DATA
%-------------------------------

function handle = display_data(ax, points, varargin)

xdata = repmat(points(:)', 2, 1); xdata(3, :) = nan;

ylim = get(ax, 'ylim'); ylim = [ylim(1), ylim(1) + 0.1 * diff(ylim)];

ydata = repmat(ylim(:), 1, numel(points)); ydata(3, :) = nan;

handle = line( ...
	'parent', ax, ...
	'xdata', xdata(:), ...
	'ydata', ydata(:), ...
	varargin{:} ...
);


