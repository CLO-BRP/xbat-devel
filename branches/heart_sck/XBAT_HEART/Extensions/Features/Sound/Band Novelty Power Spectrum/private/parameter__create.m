function parameter = parameter__create(context)

% NOVELTY POWER SPECTRUM - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);


% set new parameters
% 

parameter.temp_res = min(25, 2000/(context.page.duration)); %frames per second

% parameter.freq_res = 3; % Hz

parameter.min_repfreq = 5; % Hz

% TODO: get specgram time resolution via right function

fps=1/(parameter.frame*parameter.advance);

parameter.max_repfreq = min(100, fps/3);

% todo - get nice startup parameters
parameter.bands = linspace(0.2,0.4,2);