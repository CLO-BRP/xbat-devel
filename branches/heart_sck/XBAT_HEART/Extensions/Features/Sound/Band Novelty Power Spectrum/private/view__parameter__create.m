function parameter = view__parameter__create(context)

% NOVELTY POWER SPECTRUM - view__parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.normalize = 1;