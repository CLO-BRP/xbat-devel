function ext = zcrate_sound_feature

ext = extension_inherit(mfilename, generalized_amplitude_sound_feature);

ext.short_description = 'Zero-Crossings Rate';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'ff543bb3-e246-4cba-bf61-8fa2a44b33d3';

% ext.author = '';

ext.email = 'harold.figueroa@gmail.com';

% ext.url = '';

