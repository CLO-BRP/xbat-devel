function ext = band_energies_sound_feature

ext = extension_inherit(mfilename, spectrogram_sound_feature);

ext.short_description = 'Band energy measurements';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '85a57ed0-5de1-41f9-bb02-c9e0e416a020';

ext.author = 'Daniel';

ext.email = 'wolffd.mail@googlemail.com';

% ext.url = '';

