function parameter_plot(pal, parameter, context)

% NOTE: this function is a hacked up version of some old code, it should be cleaned up

%---------------
% SETUP
%---------------

%--
% get display axes handle and prepare
%--

handles = get_control(pal, 'plot', 'handles');

% NOTE: return quickly if display is not there

if isempty(handles)
	return;
end

ax = handles.axes; delete(get(ax, 'children'));

%---------------
% DISPLAY
%---------------

%--
% compute window and shifted window
%--

% NOTE: we use 'nan' padding so that it will not display in lines

parameter.samples = round(parameter.frame * context.sound.rate);

padwin = get_window(parameter); padwin(padwin == 0) = nan;

nfft = parameter.fft;

next = floor(parameter.advance * parameter.samples);

k = 1;

while k * next < nfft && k < 8
	nextwin{k} = [nan(k * next, 1); padwin(1:(nfft - k * next))]; k = k + 1;
end

%--
% display window and shifted window
%--

handles = line('xdata', 1:nfft, 'ydata', padwin, 'color', zeros(1, 3));

base = get(ax, 'color');

for k = numel(nextwin):-1:1
	handles(end + 1) = line('xdata', 1:nfft, 'ydata', nextwin{k}, 'linestyle', '-', 'color', base - 2^(-0.75 * k));
end

handles(end + 1) = line('xdata', [1, nfft], 'ydata', [0, 0], 'linestyle', ':');

handles(end + 1) = line('xdata', [1, nfft], 'ydata', [1, 1], 'linestyle', ':');

set(handles(end - 1:end), 'color', 0.5 * ones(1, 3));

set(handles, 'parent', ax);

set(ax, 'xlim', [1, nfft], 'ylim', [-0.2, 1.2]);


%---------------------------
% GET_WINDOW
%---------------------------

function [h, n] = get_window(parameter)

% get_window - compute window from parameters
% -------------------------------------------
%
% [h, n] = get_window_and_overlap(parameter)
%
% Input:
% ------
%  parameter - spectrogram parameters
%
% Output:
% -------
%  h - window vector
%  n - actual window length, no padding

%--
% rename fft and window length
%--

N = parameter.fft;

n = parameter.samples;

%--
% get window function and parameters from window name
%--

name = parameter.window;

fun = window_to_fun(name);

if isempty(fun)
	name = 'hann'; fun = window_to_fun(name);
end

% param = window_to_fun(name, 'param');

%--
% compute base window
%--

h = fun(n);

% TODO: implement parametrized windows

% if isempty(param)
% 	h = fun(n);
% else
% 	h = fun(n, parameter.win_param);
% end

%--
% zero pad window to fft length if needed
%--

if n < N
	h((n + 1):N) = 0;
end