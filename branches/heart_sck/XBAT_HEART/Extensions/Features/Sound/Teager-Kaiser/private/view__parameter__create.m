function parameter = view__parameter__create(context)

% TEAGER-KAISER - view__parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.color = 'bright red';

parameter.symmetric = 0;
