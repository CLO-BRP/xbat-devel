function [parameter, context] = view__parameter__compile(parameter, context)

% SPECTROGRAM - view__parameter__compile

% NOTE: we put this here to create an easily usable representation of the orientation

if iscell(parameter.orientation)
	parameter.orientation = parameter.orientation{1};
end

parameter.horizontal = strcmpi(parameter.orientation, 'horizontal');


