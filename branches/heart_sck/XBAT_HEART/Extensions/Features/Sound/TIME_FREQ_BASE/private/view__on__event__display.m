function [handles, context] = view__on__event__display(widget, data, parameter, context)

% SPECTROGRAM - view__on__event__display

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);
