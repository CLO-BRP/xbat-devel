function arg = arg_create(varargin)

%--
% initialize arg struct
%--

arg.name = '';

% NOTE: types should be real, integer, or string

arg.type = '';

arg.default = [];

% NOTE: we store this as cell so that we can uniformly handle all cases

arg.range = {};

% NOTE: this is only used in the case of 'real' and 'integer' arguments when creating a relevant control

arg.step = [];

%--
% consider inputs
%--

if isempty(varargin)
	return;
end 

arg = parse_inputs(arg, varargin{:});

%--
% sanity checks and some defaults
%--

% TODO: try to set a reasonable type default

if isempty(arg.type)
	
	if isempty(arg.default)
		
		arg.type = 'real';
		
	else
		switch class(arg.default)

			case 'char', arg.type = 'string';

			otherwise, arg.type = 'real';

		end
	end
	
end

if ~string_is_member(arg.type, {'real', 'integer', 'string'})
	
	error(['Unrecognized argument type ''', arg.type, '''.']);
	
end

% TODO: try to set a reasonable range default

if isempty(arg.range) && isnumeric(arg.default)

	arg.range = {0, max(1, arg.default)};

end
		