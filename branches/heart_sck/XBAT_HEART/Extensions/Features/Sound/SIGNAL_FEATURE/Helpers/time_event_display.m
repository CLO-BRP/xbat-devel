function handles = time_event_display(ax, event, opt)

%--
% handle input
%--

if nargin < 3
	
	opt.control = 0; opt.pad = 0.05;
	
	opt.color = [1 0 0]; opt.linestyle = '-'; opt.linewidth = 1;
	
	if ~nargin
		handles = opt; return;
	end
	
end

%--
% get various coordinates
%--

if ~isfield(event, 'display')
	xlim = event.time;
else
	xlim = event.display.time;
end

ylim = get(ax, 'ylim');

start = xlim(1); stop = xlim(2); 

low = ylim(1); high = ylim(2);

pad = opt.pad * diff(ylim);

%--
% display rectangle
%--

handles = [];

x = [start, start, stop, stop, start]; y = [high - pad, low + pad, low + pad, high - pad, high - pad];

handles(end + 1) = patch( ...
	'xdata', x, 'ydata', y, ...
	'edgecolor', opt.color, 'facecolor', 'none', ...
	'linestyle', opt.linestyle, 'linewidth', opt.linewidth ...
);

color = get_patch_colors(ax, opt.color);

handles(end + 1) = patch( ...
	'xdata', x, 'ydata', y, ...
	'edgecolor', color.edge, 'facecolor', color.face, ...
	'linestyle', opt.linestyle, 'linewidth', opt.linewidth ...
);

%--
% display control points
%--

% NOTE: getting these to work will require some work

if opt.control
	
	%--
	% create callback template
	%--
	
	% NOTE: the second argument is the control code, look in 'selection_event_display'
	
	% TODO: we need to get the 'log' as input to this function, do this by passing the selection
	
	if ~isempty(event.id)
		callback = {@selection_edit_callback, []}; % callback = {@selection_edit_callback, [], log, event.id};
	else
		callback = {@selection_edit_callback, []};
	end
	
	%--
	% left control point
	%--
	
	x = start; y = mean(ylim); callback{2} = 5;

	handles(end + 1) = line( ...
		'xdata', x, 'ydata', y, 'marker', 's', 'buttondownfcn', callback ...
	);

	%--
	% center control point
	%--
	
	% TODO: we cannot immediately use the typical callback here
	
	x = mean(xlim); % callback{2} = 9;

	handles(end + 1) = line( ...
		'xdata', x, 'ydata', y, 'marker', '+' ... % , 'buttondownfcn', callback ...
	);

	%--
	% right control point
	%--
	
	x = stop; callback{2} = 1;

	handles(end + 1) = line( ...
		'xdata', x, 'ydata', y, 'marker', 's', 'buttondownfcn', callback ...
	);

	set(handles(3:end), 'color', opt.color);

end

%--
% set various collective properties
%--

set(handles, 'parent', ax);




%---------------------------------
% SELECTION_EDIT_CALLBACK
%---------------------------------

function selection_edit_callback(obj, eventdata, control, log, id) %#ok<INUSL>

if nargin < 5
	selection_edit('start', control);
else
	selection_edit('start', control, log, id);
end
