function ext = signal_feature_sound_feature

ext = extension_create(mfilename);

ext.short_description = 'Base for signal type features';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'f910cc10-fd16-4001-b443-29add1cf96d4';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

