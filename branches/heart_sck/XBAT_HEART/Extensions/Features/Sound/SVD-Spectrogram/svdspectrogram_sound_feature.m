function ext = svdspectrogram_sound_feature

ext = extension_inherit(mfilename, spectrogram_sound_feature);

ext.short_description = 'Compute SVD-projected spectrogram';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'c232e95d-c2ee-43f9-bfe4-976f57225b05';

% ext.author = '';

% ext.email = '';

% ext.url = '';

