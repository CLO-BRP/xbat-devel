function parameter = parameter__create(context)

% MEL BAND ENERGY - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.num_lin = 4;

parameter.num_log = 2;

parameter.trans_freq = 0.35;

parameter.bank_type = 'triangular';

parameter.side_bands = 0;

% NOTE: the interpeter function derives the band descriptions from parameters

parameter.interpretfun = @mel_bands;