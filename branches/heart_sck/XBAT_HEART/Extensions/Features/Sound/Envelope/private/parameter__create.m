function parameter = parameter__create(context)

% ENVELOPE - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @envelope; 


function [X, info] = envelope(X, parameter) %#ok<INUSD>

% TODO: add some kind of lowpass filtering when we compute an envelope

X = abs(X); info.range = [0, 1];
