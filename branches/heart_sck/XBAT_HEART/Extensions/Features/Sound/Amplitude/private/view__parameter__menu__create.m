function handles = view__parameter__menu__create(par, parameter, context)

% AMPLITUDE - view__parameter__menu__create

handles = scaffold_menu(par, 'view', context);

% %--
% % create color selection menus
% %--
% 
% head = menu_control(par, 'color'); handles = head;
% 
% colors = sort(color_to_rgb); current = find(strcmpi(colors, parameter.color));
% 
% for k = 1:numel(colors)
% 	handles(end + 1) = menu_control(head, colors{k}, current == k); 
% end