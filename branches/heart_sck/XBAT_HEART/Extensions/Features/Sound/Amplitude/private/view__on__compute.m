function [handles, context] = view__on__compute(par, feature, parameter, context)

% AMPLITUDE - view__display

%-------------------
% SETUP
%-------------------

%--
% compute common time sequence, and sequence for patch display
%--

% NOTE: this time distortion helps with visible gaps in feature display

% TODO: a better solution would be to update the data associated with the patch display and display a single patch

% NOTE: this is a relatively complex operation that would have to be factored

delta = diff(feature.time(1:2));

feature.time(1) = feature.time(1) - 0.5 * delta; feature.time(end) = feature.time(end) + 0.5 * delta;

x = [feature.time; flipud(feature.time)];

%--
% get some colors
%--

% NOTE: this is some kind of compilation for view parameters

beta = 1 - parameter.alpha;

% NOTE: this makes the axes seem transparent

color = get_amplitude_display_colors(parameter);

%--
% get scale and offset for display
%--

% NOTE: 'get_field' tries to get the named field value, otherwise the default value results

scale = get_field(parameter, 'scale', 1);

offset = get_field(parameter, 'offset', 0);

%-------------------
% DISPLAY
%-------------------

%--
% alias page and initialize handles
%--

% TODO: the page we get for features is not the same in detectors!

% NOTE: this page currently has a 'channels' field rather than channel

% page = context.page;

page = get_browser_page(context.par);

% HACK: this is a way to currently patch the page discrepancy mentioned above

if ~isequal(context.page.channels, page.channel)
	page.channel = context.page.channels;
end

handles = [];

%--
% loop over page channels
%--

axs = [];

for k = 1:length(page.channel)
	
	%--
	% get and update axes
	%--
	
	ax = get_channel_axes(par, page.channel(k));
	
	% NOTE: there is no place to display channel information, continue;
	
	if isempty(ax)
		continue; 
	end
	
	set(ax, 'color', color_to_rgb(parameter.background));
	
	axs(end + 1) = ax;
	
	% NOTE: this does not need to happen on every iteration
	
	set(ax, ...
		'xlim', [page.start, page.start + page.duration] ...
	);

	% TODO: getting this right is crucial to a high quality amplitude display
	
	% NOTE: maybe this happens in the page callback, not here
	
	try %#ok<TRYNC>
		
		%--
		% compute reasonable limits for the display
		%--
		
		% NOTE: these used to be min and max, the censored range provides a better display
		
		% TODO: factor this into some more general function
		
		tail = 0.025;
		
		if isfield(feature, 'max')
			guide = 'max';
		else
			guide = 'rms';
		end
		
		low = fast_rank(feature.(guide).value(:, 1, k), tail); high = fast_rank(feature.(guide).value(:, 2, k), -tail);

% 		low = min(feature.max.value(:, 1, k)); high = max(feature.max.value(:, 2, k));

		range = high - low; low = low - 0.05 * range; high = high + 0.05 * range;

		%--
		% update y limits in a non-contractive way
		%--
		
		page.first_call_time = feature.time(1);
		
		if first_call(page, context.ext)
			set(ax, 'ylim', [low, high]);
		else
			ylim = get(ax, 'ylim'); set(ax, 'ylim', [min(low, ylim(1)), max(high, ylim(2))]);
		end
		
	end
	
	%--
	% display min and max sequences
	%--

	% TODO: this indexing here is causing problems when reduce is active!
	
	if isfield(feature, 'max')
		
		y = [feature.max.value(:, 1, k); flipud(feature.max.value(:, 2, k))];
		
		y = affine_map(y, scale, offset);
		
		handles(end + 1) = patch(x, y, color.max.facecolor);

		set(handles(end), ...
			'parent', ax, ...
			'tag', 'max', ...
			'visible', ternary(parameter.max, 'on', 'off'), ...
			'facecolor', color.max.facecolor, ...
			'edgecolor', color.max.edgecolor ...
		);
	
	end 
	
	%--
	% display RMS sequence
	%--
	
	% NOTE: this is a single positive sequence, present it as symmetric

	if isfield(feature, 'rms') && parameter.symmetric
		
		y = [-feature.rms.value(:, k); flipud(feature.rms.value(:, k))];

		y = affine_map(y, scale, offset);
		
		handles(end + 1) = patch(x, y, color.rms.facecolor);

		set(handles(end), ...
			'parent', ax, ...
			'tag', 'rms', ...
			'facecolor', color.rms.facecolor, ...
			'edgecolor', color.rms.edgecolor ...
		);

	end
	
	%--
	% display positive and negative mean sequences
	%--
	
	if isfield(feature, 'abs')
		
		y = [feature.abs.value(:, 1, k); flipud(feature.abs.value(:, 2, k))];

		y = affine_map(y, scale, offset);

		handles(end + 1) = patch(x, y, color.mean.facecolor);

		set(handles(end), ...
			'parent', ax, ...
			'tag', 'abs', ...
			'facecolor', color.mean.facecolor, ...
			'edgecolor', color.mean.edgecolor ...
		);

	end
	
	%--
	% display more details during zoom
	%--
		
	% TODO: improve reduce interpolation so that we may increase this value
	
	% NOTE: reduction is not currently applied when using the feature from a detector, this is interesting
	
	if isfield(feature, 'amplitude') % && isfield(feature.amplitude, 'reduction') && feature.amplitude.reduction < 2
		
		%--
		% display actual amplitude
		%--
        
		handles(end + 1) = line(feature.amplitude.time, affine_map(feature.amplitude.value(:,k), scale, offset));

		set(handles(end), ...
			'parent', ax, 'color', 0.75 * color.mean.edgecolor ...
		);
	
		%--
		% enhanced sample display when we have few samples
		%--
		
		if numel(feature.amplitude.value) < 384
			set(handles(end), 'marker', 'o'); 
		end	
		
	end
		
end

set(handles, 'clipping', 'off', 'hittest', 'off');


%-------------------------
% FIRST_CALL
%-------------------------

% NOTE: this function helps us determine the first call of this function per page, for each extension

function value = first_call(page, ext)

persistent TABLE

%--
% initialize table
%--

if isempty(TABLE)
	
	TABLE(1).ext = ext.name; TABLE(1).start = page.start; TABLE(1).last_time = page.first_call_time; 
	
	value = 1; return;

end

%--
% check table
%--

ix = find(strcmp(ext.name, {TABLE.ext}));

% NOTE: we are not in the table yet

if isempty(ix)
	
	TABLE(end + 1).ext = ext.name; TABLE(end).start = page.start; TABLE(end).last_time = page.first_call_time; 
	
	value = 1; return;
	
end

% NOTE: we are in the table, compare page to last call page and update

value = (TABLE(ix).start ~= page.start) || (TABLE(ix).last_time >= page.first_call_time); 

TABLE(ix).start = page.start; TABLE(ix).last_time = page.first_call_time;
