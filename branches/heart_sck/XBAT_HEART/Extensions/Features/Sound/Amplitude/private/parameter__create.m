function parameter = parameter__create(context)

% AMPLITUDE - parameter__create

%--
% parent (SIGNAL_FEATURE) parameters
%--

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% NOTE: at the moment the basic reduction parameter is a compute parameter, not a view parameter

parameter.reduce.target = 256;

%--
% amplitude parameters
%--

% NOTE: this implements a model constraint

block = get_block_range(context.sound.rate);

parameter.block = clip_to_range(0.01, block);

parameter.auto = 1;

parameter.overlap = 0.5;
