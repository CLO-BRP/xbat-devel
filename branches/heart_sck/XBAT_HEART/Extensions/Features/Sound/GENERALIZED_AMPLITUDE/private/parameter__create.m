function parameter = parameter__create(context)

% GENERALIZED_AMPLITUDE - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @identity;


% NOTE: this indicates the signature for the map function

% NOTE: this extension will understand a cell 'X' output as multiple signals

function [X, info] = identity(X, parameter) %#ok<INUSD>

% NOTE: the info struct may contain label and unit information

info = struct;
