function [handles, context] = view__on__deactivate(widget, data, parameter, context)

% BINARY SPECTROGRAM - view__on__deactivate

handles = [];

context_figure('close', context, 'debug');

