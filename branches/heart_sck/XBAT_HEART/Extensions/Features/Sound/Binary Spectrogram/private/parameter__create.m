function parameter = parameter__create(context)

% BINARY SPECTROGRAM - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.alpha = 2;
