function [handles, context] = view__on__compute(widget, data, parameter, context)

% SPECTRAL CENTROID - view__on__compute

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% display centroid overlay
%--

page = context.page;

for k = 1:length(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(k)));
	
	if isempty(ax)
		continue;
	end
	
	%--
	% display centroid
	%--
	
	% TODO: select a good color
	
	if iscell(data.centroid.value)
		value = data.centroid.value{k}; deviation = data.centroid.deviation{k};
	else
		value = data.centroid.value; deviation = data.centroid.deviation;
	end
	
	% NOTE: we compute these before mapping the value
	
	% TODO: make the bandwidth display optional
	
	low = map_frequency(value - deviation, parameter, context, false);
	
	high = map_frequency(value + deviation, parameter, context, false);
	
	value = map_frequency(value, parameter, context, false);
	
	color = [1 0 0]; color2 = 0.5 * (color + 1);
	
	handles(end + 1) = line( ...
		'parent', ax, ...
		'xdata', data.centroid.time, ...
		'ydata', value, ...
		'linewidth', 2, ...
		'color', color ...
	); %#ok<*AGROW>
	
	handles(end + 1) = line( ...
		'parent', ax, ...
		'xdata', data.centroid.time, ...
		'ydata', low, ...
		'color', color2 ...
	);
	
	handles(end + 1) = line( ...
		'parent', ax, ...
		'xdata', data.centroid.time, ...
		'ydata', high, ...
		'color', color2 ...
	);

% 	set(handles(end - 2:end), 'clipping', 'off');
end

