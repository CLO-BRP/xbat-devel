function parameter = parameter__create(context)

% SPECTRAL CENTROID - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @map;


%----------------------
% MAP
%----------------------

function reduction = map(feature, parameter, context)

if ~iscell(feature.spectrogram.value)
	
	[reduction.value, reduction.deviation] = centroid(feature.spectrogram.value, feature.freq);
	
else
	for k = 1:numel(feature.spectrogram.value)
		[reduction.value{k}, reduction.deviation{k}] = centroid(feature.spectrogram.value{k}, feature.freq);
	end
end

reduction.time = feature.time';


%----------------------
% SPECTRAL CENTROID
%----------------------

function [center, scale] = centroid(P, freq)

% NOTE: we first normalize each column of the spectrogram to use as weights

W = P .* repmat(1 ./ sum(P, 1), size(P, 1), 1); 

center = freq(:)' * W;

% NOTE: we compute the deviation from the mean for each column

if nargout > 1
	deviation = repmat(center, size(P, 1), 1) - repmat(freq(:), 1, size(P, 2));
	
	scale = sqrt(sum(W .* deviation.^2, 1));
end



