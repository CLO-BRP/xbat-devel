function parameter = parameter__create(context)

% OCTAVE FILTER-BANK - parameter__create

% parameter = struct;

%--
% amplitude and generalized amplitude parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @octave_filter_bank; 

%--
% filter bank parameters
%--

parameter.min_freq = 90; 

parameter.max_freq = 6000;

% NOTE: this should be 'signal' or 'energy', another?

parameter.output = 'signal'; 

parameter.order = 3;

% TODO: this should be passed during the compilation phase, NOT HERE!

% NOTE: perhaps the signature for the helpers should allow for context!

parameter.rate = context.sound.rate;


% TODO: consider allowing the use of the various weighings

function [Y, info] = octave_filter_bank(X, parameter)

% TODO: the multirate considerations should be rate-aware

%--
% setup
%--

info = struct;

[center, info.labels] = ansi_center_frequencies; % ([], parameter.max_freq);

% [center, info.labels] = ansi_center_frequencies([], parameter.max_freq);

%--
% filter signal to get octave band signals
%--

Y = cell(size(center)); 

for k = numel(center):-1:13
	
	% NOTE: above 1600 Hz band, we use a direct implementation of filtering
	
	if ~in_range(center(k), parameter.min_freq, parameter.max_freq)
		continue;
	end 
	
	[B, A] = oct3dsgn(center(k), parameter.rate, parameter.order); Y{k} = filter(B, A, X); 
	
end

% NOTE: for 1250 Hz to 100 Hz, we use multirate filtering

[Bu, Au] = oct3dsgn(center(15), parameter.rate, parameter.order); 	% Upper 1/3-oct. band in last octave.

[Bc, Ac] = oct3dsgn(center(14), parameter.rate, parameter.order); 	% Center 1/3-oct. band in last octave.

[Bl, Al] = oct3dsgn(center(13), parameter.rate, parameter.order); 	% Lower 1/3-oct. band in last octave.

for j = 3:-1:0

	for k = 1:size(X, 2)
		X2(:, 2) = decimate(X(:, k), 2);
	end
	
	% NOTE: we interpolate back to the original rate as we compute, this produces a better display

	if in_range(center(j*3 + 3), parameter.min_freq, parameter.max_freq)
		Y{j*3 + 3} = quick_interp(filter(Bu, Au, X2), 2^(4 - j));
	end

	if in_range(center(j*3 + 2), parameter.min_freq, parameter.max_freq)
		Y{j*3 + 2} = quick_interp(filter(Bc, Ac, X2), 2^(4 - j));
	end

	if in_range(center(j*3 + 1), parameter.min_freq, parameter.max_freq)
		Y{j*3 + 1} = quick_interp(filter(Bl, Al, X2), 2^(4 - j));
	end

	X = X2; clear X2;
	
end

%--
% check which ones we did not compute and prune the arrays
%--

ix = find(iterate(@isempty, Y));

Y(ix) = []; info.labels(ix) = [];

%--
% compute energy
%--

% TODO: when we do this, we should also output range information

if strcmpi(parameter.output, 'energy')
	
	for k = 1:numel(Y)
		Y{k} = Y{k}.^2;
	end

	% TODO: allow for words and intervals as range descriptors, and for single or individual range description

	info.range = 'positive';

end


%-------------------------
% QUICK_INTERP
%-------------------------

% TODO: make interpolation better and faster using spline interpolation

function Z = quick_interp(Y, rate)

% NOTE: there is sometimes a discrepancy in the output length for the current and the next method

for k = 1:size(Y, 2)
	Z(:, k) = interp(Y(:, k)', rate)';
end

%--------------------------

% TODO: this is faster, but there are some edge problems

% NOTE: upsample and interpolate, the interpolating filter is a simple triangular filter

% Y(1:rate:(size(Y, 1) * rate), :) = Y; h = intfilt(rate, 1, 'lagrange')'; 
% 
% Y = linear_filter(Y, h);

%--------------------------

% NOTE: the 'q' in this function is not accurate, this is much slower than 'interp'

% n = size(Y, 1); Y = interp1q((1:n)', Y, linspace(1, n, rate * n)');


