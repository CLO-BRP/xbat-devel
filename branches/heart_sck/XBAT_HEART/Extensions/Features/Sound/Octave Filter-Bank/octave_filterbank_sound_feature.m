function ext = octave_filterbank_sound_feature

ext = extension_inherit(mfilename, generalized_amplitude_sound_feature);

ext.short_description = 'Octave filter-bank outputs';

ext.category = {ext.category{:}, 'Frequency'};

% ext.version = '';

ext.guid = 'f009bfee-7e2b-4245-a026-5085e63be0da';

% ext.author = '';

ext.email = 'harold.figueroa@gmail.com';

% ext.url = '';

