function [fun, info] = log_action

% log_action - function handle structure
% --------------------------------------
%
% [fun, info] = log_action
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun.prepare = {{'result', 'context'}, {'parameter', 'context'}};

fun.compute = {{'result', 'context'}, {'log', 'parameter', 'context'}};

fun.conclude = {{'result', 'context'}, {'parameter', 'context'}};

fun.parameter = param_fun;