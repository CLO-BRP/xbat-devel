function context = detector_context(ext, sound, scan, channels, log, par, data)

% detector_context - pack context
% -------------------------------
%
% context = detector_context(ext, sound, scan, channels, log)
%
% context = detector_context(ext, sound, scan, channels, log, par, data)
%
% Input:
% ------
%  ext - detector extension
%  sound - sound to scan
%  scan - scan
%  channels - channels to scan
%  log - output log
%  par - parent browser
%  data - parent state
%
% Output:
% -------
%  context - context

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% check for parent browser
%--

if nargin < 6
	par = [];
end

%--
% handle other input considering parent availability
%--

if ~isempty(par)
	
	%--
	% check browser input and get state if needed
	%--

	if ~is_browser(par)
		error('Input handle is not browser handle.');
	end

	if (nargin < 7) || isempty(data)
		data = get_browser(par);
	end

	%--
	% get active log if needed
	%--

	% NOTE: there is a single sound per parent, but possibly multiple logs
	
	if nargin < 5
		log = get_active_log(par, data);
	end

	%--
	% get sound from parent
	%--

	if (nargin < 2) || isempty(sound)
		sound = data.browser.sound;
	end
	
else
	
	%--
	% get sound from log if needed
	%--
	
	if (nargin < 2) || isempty(sound)
		
		if (nargin < 5) || isempty(log)
			error('A context log is needed if no parent browser is available.');
		end

		sound = log.sound;

	end
	
	if nargin < 5
		log = [];
	end
	
end

%--
% get default scan
%--

if (nargin < 2) || isempty(scan)
	scan = get_sound_scan(sound);
end

%--------------------------
% COMPILE CONTEXT
%--------------------------

% EXTENSION CONTEXT

%--
% get generic extension context
%--

if isempty(par)
	context = get_extension_context(ext, par);
else
	[ext, ix, context] = get_browser_extension(ext.subtype, par, ext.name);
end

% DECORATE EXTENSION CONTEXT

%--
% region to scan and output log fields
%--

% NOTE: scan and channels determine which parts of the sound we look at

sound.rate = get_sound_rate(sound);

context.sound = sound;

context.scan = scan;

context.channels = channels;

% NOTE: this will be the output log for the detection results

context.log = log;

%--
% active extensions
%--

% NOTE: possibly browser dependent fields, however they fit better here

types = get_extension_types;

if isempty(par)
	
	for k = 1:length(types)
		context.active.(types{k}) = [];
	end
	
else
	
	% NOTE: this is how this should be done, consider integrating both branches here

% 	context.active = get_active_extensions(par, data);
	
	for k = 1:length(types)
		context.active.(types{k}) = get_active_extension(types{k}, par, data);
	end
	
end

%--
% view fields
%--

% NOTE: the extension view state should not be copied

if ~isfield(ext.control, 'view')
	ext.control.view = 0;
end

context.view.on = ext.control.view;

context.view.fig = [];

context.view.data = [];

context.view.parameter = [];
