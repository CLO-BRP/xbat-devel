function out = detector_scan(ext, sound, scan, channels, log, context)

% detector_scan - scan sound with detector
% ----------------------------------------
%
% out = detector_scan(ext, sound, scan, channels, log, context)
%
% out = detector_scan(context)
% 
% Input:
% ------
%  ext - detector
%  sound - sound
%  scan - scan (def: default sound scan)
%  channels - channels to scan (def: [])
%  log - log to append events to (def: [])
%  context - context
%
% Output:
% -------
%  out - events or updated log

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% CONTEXT INPUT
%--

% NOTE: simply rename and unpack context

if nargin < 2
	
	context = ext;
	
	try
		ext = context.ext; sound = context.sound; channels = context.channels; log = context.log;
	catch	
		nice_catch(lasterror, 'Problems unpacking context.');
	end
	
%--
% COMPONENT INPUT
%--

else
	%--
	% get default scan
	%--

	if (nargin < 3) || isempty(scan)
		scan = get_sound_scan(sound);
	end

	%--
	% set and check channels
	%--

	if nargin < 4 || isempty(channels)
		channels = 1:sound.channels;
	end

	if ~proper_channels(channels, sound)
		error('Improper scan channels input.');
	end

	%--
	% set default empty log
	%--

	if nargin < 5
		log = [];
	end

	%--
	% build best context we can
	%--

	if (nargin < 6) || isempty(context)
		context = detector_context(ext, sound, scan, channels, log);
	end
end

%--------------------------
% SETUP
%--------------------------

%--
% get grid from parent if possible
%--

if isfield(context, 'par') && ~isempty(context.par)
	data = get_browser(context.par); grid = data.browser.grid; axes = data.browser.axes;
else
	grid = []; axes = []; ext.control.view = 0;
end

context.grid = grid; context.axes = axes;
	
%--
% set view and waitbar flags
%--

% NOTE: these should not be exclusive

if ~isfield(ext.control, 'view')
	ext.control.view = 0;
end

viewing = ext.control.view && ~isempty(ext.fun.view.on.compute);

waiting = ~viewing;

%--
% build save request
%--

request.save = 'all'; request.user = ext;

%--
% waitbar setup
%--

if waiting
	pal = detector_waitbar(context);

	waitbar_update(pal, 'PROGRESS', 'message', 'Starting ...');
end

%--
% view setup
%--

% TODO: this should not be required here !!! this happens during 'create_widget'

% NOTE: create the view figure, parameters, and layout figure if needed

if viewing
	%--
	% create view figure
	%--
	
	context.view.figure = create_widget(context.par, context.ext, context);
	
	%--
	% get view parameters
	%--
	
	% NOTE: the default is a length 1 struct with no fields, so no parameters
	
	context.view.parameter = struct;
	
	if ~isempty(ext.fun.view.parameter.create)
		try
			context.view.parameter = ext.fun.view.parameter.create(context);
		catch
			extension_warning(ext, 'View parameter creation failed.', lasterror);
		end
	end
		
	if ~isempty(ext.fun.view.parameter.compile)
		try
			context.view.parameter = ext.fun.view.parameter.compile(context.view.parameter, context);
		catch
			extension_warning(ext, 'View parameter compilation failed.');
		end
	end
	
	%--
	% layout view figure
	%--
	
	if ~isempty(ext.fun.view.layout)	
		try
			ext.fun.view.layout(context.view.figure, context.view.parameter, context);
		catch
			extension_warning(ext, 'Explain layout failed.', lasterror);
		end	
	end
end

%--
% get actions for processing
%--

if isfield(ext.control, 'on_detect_process')
	
	processing = ext.control.on_detect_process;
else
	processing = 0;
end

if processing
	% NOTE: this is for the 'popup', eventually we will have a 'listbox'
	
	[action, ignore, action.context] = get_browser_extension('event_action', context.par, ext.control.on_detect_action{1});
	
	% NOTE: for an active scan the context log is empty, the action context log (the current active log) needs to be over-written
	
	% NOTE: in this case we create a dummy log named as the sound, not unreasonable
	
	if isempty(context.log)
		
		action.context.log = log_create(sound, 'name', sound_name(sound));
	end
end

%--------------------------
% PREPARE SCAN
%--------------------------

if ~isempty(ext.fun.prepare)
	try
		[result, context] = ext.fun.prepare(parameter, context); %#ok<NASGU>
	catch
		extension_warning(ext, 'Prepare failed.');
	end
	
	% TODO: develop a convention for result so that we can act if we need to
end

%--------------------------
% DETECTOR SCAN
%--------------------------

%--
% prepare for processing
%--

if processing && ~isempty(action.fun.prepare)
	try
		[result, action.context] = action.fun.prepare(action.parameter, action.context);
	catch
		extension_warning(action, 'Prepare failed.');
	end
end

%--
% compile to configure pager
%--

if ~isempty(ext.fun.parameter.compile)
	try
		[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
	catch
		extension_warning(ext, 'Parameter compile for context update failed.', lasterror);
	end
end 

%--
% page through sound
%--

contexts = [];

event_store = empty(event_create); value_store = [];

last_time.record = []; last_time.wall = clock;

while 1
	% NOTE: we get the next page and break if we are done 
	
	[page, context.scan] = get_scan_page(context.scan);
	
	if isempty(page)
		break;
	end
	
    %--
    % handle no-man's land pages
    %--
    
    if isempty(page.start)
        try
			ext.fun.view.layout(context.view.figure, context.view.parameter, context);
		catch
			extension_warning(ext, 'Explain layout failed.', lasterror);
        end
        
        break;
    end
    
    if isempty(last_time.record)	
        last_time.record = page.start;
    end
    
	%--
	% READ PAGE
	%--
		
	page = read_sound_page(sound, page, channels, min(1, 0.1 * page.duration)); 
	
	% NOTE: we skip zero-length pages
	
	if isempty(page.samples)
		continue;
	end
	
	% NOTE: note from here on we are processing a non-trivial page, time detection processing
	
	start = clock;
	
	%--
	% PROCESS PAGE
	%--
	
	page = filter_sound_page(page, context);
	
% 	page = feature_sound_page(page, context);
	
	%--
	% SCAN PAGE FOR EVENTS
	%--
	
	% TODO: check context, channel computation, and state passing are working
	 
    if page.start+page.duration <  page.sound.duration
        [event, value, contexts] = detector_page_scan(ext, page, context, contexts, viewing, waiting);
    end
	if ~trivial(value)
		% NOTE: we pack detector values as we do measures
		
		for k = 1:numel(event)
			event(k).detector.(ext.fieldname).value = value(k);
			
			event(k).detector.(ext.fieldname).parameter = ext.parameters.compute;
		end
	else
		for k = 1:numel(event)
			
			event(k).detector = struct;
		end
	end
	
	%--
	% PROCESS EVENTS
	%--
	
	% TODO: make sure that we test for a 'compute' when we set 'processing'
	
	if processing
		
		for k = length(event):-1:1
			target = event_sound_read(event(k), sound);
			
			try
				[result, action.context] = action.fun.compute(target, action.parameter, action.context);
			catch
				extension_warning(action, 'Failed to process event.'); continue;
			end
			
			% NOTE: an improperly designed action could be nasty
			
			% TODO: factor discard behavior and make other options available
			
			% NOTE: we process the result to understand system interaction
			
			if isfield(result, 'event')
				% NOTE: we indicate that we want to discard an event, by creating an empty event field
				
				if isempty(result.event)
					event(k) = [];
				else
					result.event.samples = []; result.event.rate = []; event(k) = result.event;
				end
			end
		end
		
	end
	
	% NOTE: stop timing detector processing
	
	elapsed = etime(clock, start); 
		
	%--
	% adapt scan and get next page
	%--
	
% 	context.scan = adapt_page_duration(context.scan, elapsed);
    
    %--
    % skip to next page if there are no events
    %--
    
    if isempty(event)
		continue;
    end
    
if not(isempty(event))
    %--
    % make sure events are a column and append to store
    %--
	
% 	db_disp; event_store, event
	
    event_store = [event_store; event(:)];
	
	if isempty(value_store)
		value_store = value(:);
	else
		value_store = [value_store; value(:)];
	end
    
    %--
    % append events to log if there are enough
    %--
    
    % NOTE: the empty log condition indicates active detection: we don't need to store
    
    if isempty(log)
        continue;
    end
    
    % NOTE: 1000 events in store, 1 hour of recording time, or 5 minutes of wall time trigger append
    
	% NOTE: the append indicator is coded in case we want to refine decision
	
    append = ...
        1 * ( numel(event_store) > 1000 ) + ...
		2 * ( page.start - last_time.record > 2 * 3600 ) + ...
        4 * ( etime(clock, last_time.wall) > 30 ) + ...
        size(event,1) ...
	;
    
    if append    
		% NOTE: append events, flush event store, record last recording time considered and wall time
		
        if exist('out')
%             log.event = out;
%             log.length = length(out);

             out = log_save_events(out, event_store, request); event_store = empty(event_create);
          
%              if (out.length > 1000)
%                  pause
%              end
        else
            out = log_save_events(log, event_store, request); event_store = empty(event_create);
        end
        
		% TODO: save detection values to store
		
		if ~isempty(value_store)
			value_store = [];
		end
		
		last_time.record = page.start; last_time.wall = clock;
        
    end
end
end

%   curr_dir = pwd;
%     cd(newlog.path)
%     log_save(newlog);
%     cd(curr_dir)
%     str1 = sprintf('New XBAT log saved: ''%s''\n',newlog.file);
%     disp(str1);
if ~isempty(log) && ~isempty(event_store)
	
    out = log_save_events(log, event_store, request);	
end

if out.length > 0
    newlog = out;
    newlog.event = newlog.event';
   
    curr_dir = pwd;
    cd(newlog.path)
    log_save(newlog);
    cd(curr_dir)
    str1 = sprintf('New XBAT log saved: ''%s''\n',newlog.file);
    disp(str1);
end


%--
% conclude processing
%--

if processing && ~isempty(action.fun.conclude)
	
	try
		[result, context] = action.fun.conclude(action.parameter, action.context);
	catch
		extension_warning(action, 'Conclude failed.');
	end
	
end

%--------------------------
% CONCLUDE SCAN 
%--------------------------

if ~isempty(ext.fun.conclude)
	
	try
		[result, context] = ext.fun.conclude(parameter, context);
	catch
		extension_warning(ext, 'Conclude failed.');
	end
	
end

%--------------------------------------------
% FINISH SCAN
%--------------------------------------------
	
%--
% make sure view objects are visible
%--

if viewing
	
	%--
	% get view handles
	%--
	
	view_all = findobj(context.view.figure); 
	
	view_axes = findobj(context.view.figure, 'type', 'axes');
	
	%--
	% make sure objects are displayed and axes grids visible
	%--
	
	set(setdiff(view_all, view_axes), 'visible', 'on');
	
	set(view_axes, 'layer', 'top');
	
end

%--
% update waitbar and close if needed
%--

if waiting
	
	waitbar_update(pal, 'PROGRESS', ...
		'value', context.scan.position, 'message', 'Done.' ...
	);

	if get_control(pal, 'close_after_completion', 'value')
		close(pal);
	end

end

%--
% output events or append to log and output log
%--

if isempty(log)
	out = event_store; return;
end


%--------------------------
% PROPER_CHANNELS
%--------------------------

function value = proper_channels(channels, sound)

if isempty(channels)
	value = 1; return;
end

value = ~(any(channels ~= floor(channels)) || any(channels < 1) || any(channels > sound.channels));




