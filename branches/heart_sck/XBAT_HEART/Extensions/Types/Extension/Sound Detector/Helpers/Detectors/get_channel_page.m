function page = get_channel_page(page, channel)

% get_channel_page - extract channel page from page
% -------------------------------------------------
%
% page = get_channel_page(page, channel)
%
% Input:
% ------
%  page - page
%  channel - channel to extract
%
% Output:
% -------
%  page - channel page

% TODO: also consider extraction by index not channel

%--
% get channel page index
%--

ix = find(page.channels == channel);

% TODO: consider if we want to throw an error or output empty page

if isempty(ix) 	
	error('Channel is not available in page.');
end
	
%--
% select basic channel data
%--

page.channels = channel;

page.samples = page.samples(:, ix);

if ~isfield(page, 'before')
	return;
end

if ~isempty(page.before.samples)
	page.before.samples = page.before.samples(:, ix);
end

if ~isempty(page.after.samples)
	page.after.samples = page.after.samples(:, ix);
end

%--
% select filtered channel samples
%--

% TODO: cleanup this function's logic

if ~isempty(page.filtered)
	
	% NOTE: as we compute a page we may not have filtered sample
	
	if ix <= size(page.filtered, 2)
		
		page.filtered = page.filtered(:, ix);
		
		if ~isempty(page.before.samples)
			page.before.filtered = page.before.filtered(:, ix);
		end

		if ~isempty(page.after.samples)
			page.after.filtered = page.after.filtered(:, ix);
		end
		
	end
	
end

%--
% select channel features
%--

if isfield(page, 'feature') && ~isempty(page.feature)
	
	% TODO: how we select feature channel results, depends on how we store
	
	% NOTE: this should be easy to handle when we pack channel features
	
	for k = 1:length(page.feature)
% 		try
% 
% 		catch
% 			extension_warning(ext, 'Unable
% 		end
	end
	
end

%--
% select channel events
%--

