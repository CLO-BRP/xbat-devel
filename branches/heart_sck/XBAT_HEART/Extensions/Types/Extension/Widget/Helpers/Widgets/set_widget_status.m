function set_widget_status(widget, result)

fps = round(1 / result.elapsed);

if fps < 0 || fps > 1000
	return;
end

harray_status(widget, [int_to_str(fps), ' FPS']);