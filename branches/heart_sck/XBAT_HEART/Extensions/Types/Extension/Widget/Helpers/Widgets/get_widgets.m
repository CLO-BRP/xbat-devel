function [widgets, info] = get_widgets(par, event)

% get_widgets - get child widgets listening to all or a specific event
% --------------------------------------------------------------------
%
% [widgets, info] = get_widgets(par, event)
%
% Input:
% ------
%  par - parent browser
%  event - widget event name (def: '', all events)
%
% Output:
% -------
%  widgets - widget handles
%  info - widget info

% TODO: this function should be renamed, perhaps to 'get_browser_widgets'

%-----------------
% HANDLE INPUT
%-----------------

%--
% check event input
%--

% NOTE: consider all events as default

if nargin < 2
	event = ''; 
else
	if isempty(event)
		error('Provided widget event must be non-empty.');
	end
end

if ~isempty(event) && ~ismember(event, get_widget_events)
	error('Unrecognized widget event.');
end

%--
% check browser input and get info
%--

[proper, par_info] = is_browser(par);

if ~proper
	widgets = []; info = []; return; % error('Input handle is not browser handle.');
end

%-----------------
% GET WIDGETS
%-----------------

%--
% get all widget figures
%--

% NOTE: widgets are not registered with the parent for now 

widgets = get_xbat_figs('type', 'widget');

if isempty(widgets)
	info = []; return;
end

%--
% parse widget tags to get widget info
%--

tags = get(widgets, 'tag');

if ischar(tags)
	tags = {tags};
end

for k = 1:length(tags)
	info(k) = parse_widget_tag(tags{k});
end

%--
% match widget and parent info
%--

% TODO: this could be faster and less fragile, the 'isequal' internal test hides both problems

widget_info = rmfield(info, {'header', 'type', 'name', 'listen', 'sig'});

par_info = rmfield(par_info, 'type');

for k = length(info):-1:1
		
	if ~isequal(par_info, widget_info(k))
		info(k) = []; widgets(k) = [];
	end
	
end 

%--
% check if we listen
%--

if ~isempty(event)
	
	for k = length(info):-1:1
		
		listen = get_field(info(k).listen, event);
		
		if ~listen
			info(k) = []; widgets(k) = [];
		end

	end

end
