function state = get_widget_state(widget)

% get_widget_state - get widget state struct including listening indicators
% -------------------------------------------------------------------------
% 
% state = get_widget_state(widget)
% 
% Input:
% ------
%  widget - widget
%
% Output:
% -------
%  state - widget state struct

%--
% handle multiple handles
%--

if numel(widget) > 1
	state = iterate(mfilename, widget); return;
end

%--
% compute and pack state
%--

state.name = get(widget, 'name');

% state.handle = widget;
% 
% state.tag = get(widget, 'tag');

state.info = parse_widget_tag(get(widget, 'tag')); state.info = rmfield(state.info, {'header', 'type', 'user', 'listen'});

state.position = get(widget, 'position');