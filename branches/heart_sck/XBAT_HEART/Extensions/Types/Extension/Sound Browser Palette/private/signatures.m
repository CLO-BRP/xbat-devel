function fun = signatures

% TODO: make sense of the below comments and come up with a new description

% NOTE: a palette represents a collection of controllable parameters

% NOTE: think about the spectrogram parameters to understand 'compile'

% fun.parameter = param_fun;

fun = get_extension_type_signatures('widget');

% NOTE: these events may not make sense for palettes, removing them however complicates the listener availability code

% fun = rmfield(fun, {'layout', 'position'});
% 
% fun.on = rmfield(fun.on, {'compute', 'cursor'});

% fun.palette = control_fun;