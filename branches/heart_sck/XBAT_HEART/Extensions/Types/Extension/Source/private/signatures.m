function [fun, info] = source

% source - function handle structure
% ----------------------------------
%
% [fun, info] = source
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

% NOTE: a source extension generates synthetic data

fun.compute = {{'data'}, {'parameter', 'context'}};

fun.parameter = param_fun;