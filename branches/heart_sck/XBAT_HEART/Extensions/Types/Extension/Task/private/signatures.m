function [fun, info] = signatures

% TASK - signatures

fun = struct; info = [];

fun.atomize = {{'task', 'context'}, {'task', 'parameter', 'context'}};

fun.execute = {{'result', 'context'}, {'task', 'parameter', 'context'}};

% NOTE: the controls and menus may be used to configure Task handler?

fun.parameter = param_fun;
