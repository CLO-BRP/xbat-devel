function [helpers, args]  = event_find_helpers

% event_find_helpers - get description of event find helpers
% ----------------------------------------------------------
%
% [helpers, args] = event_find_helpers
%
% Output:
% -------
%  helpers - names
%  args - arguments

%-------------------
% INSPECT EVENT
%-------------------

%--
% get relevant event fields
%--

% NOTE: the resulting fields are sorted in alphabetical order

fields = fieldnames(event_create); remove = {'detection', 'annotation', 'measurement', 'rate', 'samples', 'userdata'};

fields = setdiff(fields, remove);

%-------------------
% GET HELPERS
%-------------------

%--
% initialize helper description arrays
%--

helpers = {}; args = {};

%--
% build generic helpers
%--

helpers{end + 1} = 'by__fields'; args{end + 1} = {{'id'}, {'store', 'varargin'}};

%--
% build attribute helpers
%--

% NOTE: find on a single attribute, considering a relation and order, and starting from a subset

for k = 1:length(fields) 
    helpers{end + 1} = ['by__', fields{k}]; args{end + 1} = {{'id'}, {'store', fields{k}, 'relation', 'id'}};
end
