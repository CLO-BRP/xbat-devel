function result = event_measure_dispatcher(callback, context)

ext = context.ext;

disp(['Dispatch ''', callback.control.name, ''' request for ''', ext.subtype, '::', ext.name, '''']);

%--
% initialize result to no-field struct
%--

result = struct; 

%--
% handle callback request
%--

% NOTE: the request is the control name

switch callback.control.name
	
	% NOTE: we could get these case names from the shared control create function
	
	% NOTE: this means we could orient the type extension (controller) towards the actions
	
	case 'active'
		
	case 'view'
		
	case 'output_log'
		
	case 'start'
		
	otherwise
		
end
