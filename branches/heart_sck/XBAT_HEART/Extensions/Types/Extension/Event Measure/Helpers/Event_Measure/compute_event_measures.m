function [event, context] = compute_event_measures(event, ext, context, wait)

% compute_event_measures - for a collection of events
% ----------------------------------------------------
%
% [event, context] = compute_event_measures(event, ext, context, wait)
%
% Input:
% ------
%  event - events
%  ext - measure extensions
%  context - corresponding contexts
%  wait - waitbar state description
%
% Output:
% -------
%  event - measured events
%  context - updated contexts

% NOTE: this function, and the various measures rely on the context, which points to the sound and log

% NOTE: therefore this works for event collections within a log

%--
% set no waitbar default
%--

if nargin < 4
	wait.handle = [];
end

%--
% remove measures that do not compute
%--

for k = numel(ext):-1:1
	if isempty(ext(k).fun.compute), ext(k) = []; end
end 

%--
% check whether we need samples
%--

for k = 1:numel(ext)
	if ext(k).uses.samples, need_samples = 1; break; end
end

% NOTE: we try to determine whether filter state has changed

if ~need_samples
	
	force_read = 0;

else

	% NOTE: this may be more trouble than it's worth, just read samples again it's typically fast

	if isdeployed
		force_read = 1;
	else
		caller = get_caller; force_read = any(strmatch('filter_dispatch', {caller.stack.name}));
	end

end

%--
% compute measures
%--

% NOTE: this part of the code mostly manages the looping and the waitbar

if isempty(wait.handle)
	
	%--
	% loop over events
	%--
	
	for j = 1:numel(event)
	
		%--
		% get event samples if needed
		%--
		
		% NOTE: here we get event samples that may be used by all extensions
		
		% NOTE: there is an assumption here that all contexts are the same in what matters for reading samples
		
		if force_read || (need_samples && isempty(event(j).samples))
			event(j) = get_event_samples(event(j), context(1).sound, context(1));
		end
		
		%--
		% loop over and compute all measures
		%--
		
		for k = 1:numel(ext)	
			[event(j), context(k)] = compute(event(j), ext(k), context(k));
		end
		
		%--
		% clear samples when measuring a collection of events
		%--
		
		% NOTE: we do this to prevent keeping samples for a large collection of events in memory
		
		% NOTE: in the case of a single event, which typically corresponds to active measurement, we keep the samples
		
		if j > 1
			event(j).samples = []; event(j).unfiltered = [];
		end
		
	end

else

	for j = 1:numel(event)
		
		%--
		% get event samples if needed
		%--
		
		% NOTE: here we get event samples that may be used by all extensions
		
		% NOTE: there is an assumption here that all contexts are the same in what matters for reading samples
		
		if force_read || (need_samples && isempty(event(j).samples))
			event(j) = get_event_samples(event(j), context(1).sound, context(1));
		end
		
		%--
		% loop over and compute all measures
		%--
		
		% NOTE: this is where all the waitbar code is concentrated
		
		partial = wait.start + (j - 1) * numel(ext); prefix = ['Event #', int2str(event(j).id), ': '];
		
		for k = 1:numel(ext)

			[event(j), context(k)] = compute(event(j), ext(k), context(k));

			waitbar_update(wait.handle, 'PROGRESS', ...
				'value', (partial + k) / wait.total, 'message', [prefix, 'Computing ''', ext(k).name, ''''] ...
			);

		end
		
		%--
		% clear samples when measuring a collection of events
		%--
		
		% NOTE: we do this to prevent keeping samples for a large collection of events in memory
		
		% NOTE: in the case of a single event, which typically corresponds to active measurement, we keep the samples
		
		if j > 1
			event(j).samples = []; event(j).unfiltered = [];
		end

	end

end


%---------------------------
% COMPUTE
%---------------------------

function [event, context] = compute(event, ext, context)

%--
% try to compute measure
%--

% NOTE: failure to compute leaves the event unchanged

try
	[value, context] = ext.fun.compute(event, ext.parameter, context);
catch
	extension_warning(ext, 'Compute failed.', lasterror); return;
end

%--
% pack measure result
%--

event.measure.(ext.fieldname).parameter = ext.parameter;

event.measure.(ext.fieldname).value = value;

