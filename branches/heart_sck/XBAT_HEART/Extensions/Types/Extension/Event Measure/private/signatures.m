function [fun, info] = signatures

% EVENT MEASURE - signatures
% --------------------------
%
% [fun, info] = signatures
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun.compute = {{'value', 'context'}, {'event', 'parameter', 'context'}};

fun.parameter = param_fun;

fun.value = value_fun;

% TODO: we want to add distance computation to a measure, however the signature is odd 

% NOTE: the parameters here would be the distance parameters

fun.distance.compute = {{'value'}, {'A', 'B', 'parameter', 'context'}};

fun.distance.parameter = param_fun;

fun.view = get_extension_type_signatures('widget');

% NOTE: this is to provide a compatibility layer for older measurements

% NOTE: the signature is not the same as the previous display functions 'widget' is renamed as 'par' as it is the parent browser handle

% TODO: consider passing context here as well

fun.browser_event_display = {{'handles'}, {'par', 'data', 'context'}};


