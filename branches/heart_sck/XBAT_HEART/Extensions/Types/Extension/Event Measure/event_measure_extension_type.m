function ext = event_measure_extension_type

ext = extension_create;

ext.short_description = 'Event measurement extensions';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'fadaa80f-1619-4201-b2c9-a170bfb1bf08';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

