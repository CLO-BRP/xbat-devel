function [result, context] = dispatch(request, parameter, context)

% SOUND FEATURE - dispatch

result = struct;

switch request
	
	% NOTE: 'active' and 'open' typically initiate in the browser extension menus
	
	case 'active'
		result.action = toggle_active_extension(context.ext, context.par, context);
		
	case 'open'
		result.pal = open_extension_palette(context.ext, context, context.par);
	
	otherwise
		error('XBAT:ExtensionType:UnrecognizedRequest', ['Unrecognized request ''', request, '''.']);
		
end


%--------------------------------
% TOGGLE_ACTIVE_EXTENSION
%--------------------------------

function action = toggle_active_extension(ext, par, context)

action = ternary(~extension_is_active(ext, par), 'add', 'del');

set_active_extension(action, ext, par);




