function result = parameter__control__callback(callback, context)

% SOUND FEATURE - parameter__control__callback

result = struct; 

switch callback.control.name
	
	case 'active'
		
		if callback.value
			activate_sound_feature(context.ext, context.par);
		else
			deactivate_sound_feature(context.ext, context.par);
		end
		
	case 'render'
		
		db_disp 'start rendering'
		
		render_sound_feature(context.sound, context.ext, context);
		
		return;
		
		context.active = get_active_filters(context.par);
		
		%--
		% setup output location, get sound scan and pages, create waitbar
		%--
		
		% TODO: use a parameter hash to identify the rendering, figure out which
			
		% TODO: also include a log file that contains the relevant parameters explicitly
			
		root = feature_render_root(context);
		
		scan = get_sound_scan(context.sound, context.page.duration);
		
		[page, scan] = get_scan_page(scan); pages = page;
		
		while ~isempty(page)
			pages(end + 1) = page; [page, scan] = get_scan_page(scan); 
		end
		
		wait = render_waitbar(context);
		
		bpage = get_browser_page(context.par);
		
		%--
		% page through sound and render
		%--

		for k = 1:numel(pages)
			
			%--
			% get current page samples, possibly filtered
			%--
			
			page = pages(k);
			
			page = read_sound_page(context.sound, page, bpage.channel);
			
			page = filter_sound_page(page, context);
		
			%--
			% compute feature on page and render to files
			%--
			
			feature = context.ext.fun.compute(page, context.ext.parameters.compute, context);

			% NOTE: the rendered may share some code with the view, these should be factored as helpers
			
			% NOTE: the 'view' is the 'display output device', when we 'render' we are thinking about a filesystem representation
			
			% TODO: think about rendering in the context of computational caching as well
			
			% TODO: think about whether we want a render file name input, or whether some time information should be used 
			
			prefix = fullfile(root, int_to_str(k, numel(pages))); context.prefix = prefix;
			
			try
				result.render(k) = context.ext.fun.render(feature, context);
			catch
				extension_warning(context.ext, 'Failed to render.', lasterror);
			end
			
			%--
			% add matching audio files to rendering if requested
			%--
			
			% TODO: the format will be obtained from parameters, and options updated
			
			file = [prefix, '.mp3']; opt = sound_file_write(file);
			
			result.audio{k} = sound_file_write(file, page.samples, context.sound.rate, opt);
			
			%--
			% create simple rendering page
			%--
			
			% NOTE: at the moment this seems to specifically tied to this application
			
			% TODO: implement this is a helper, consider using javascript
			
			result.page = feature_render_page(result, context);
			
			waitbar_update(wait, 'PROGRESS', 'value', k / numel(pages));
		
		end

		%--
		% close waitbar and show output location if needed
		%--
		
		if get_control(wait, 'close_after_completion', 'value')
			close(wait);
		end
		
		show_file(root);
		
end


%-------------------------
% RENDER_WAITBAR
%-------------------------

function pal = render_waitbar(context)

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Overall progress bar', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'update_rate', [], ...
	'space', 2 ...
);
	
control(end + 1) = control_create( ...
	'name', 'close_after_completion', ...
	'style', 'checkbox', ...
	'value', 1, ...
	'space', 1 ...
);

name = ['Rendering ', sound_name(context.sound), ' ', context.ext.name]; opt = waitbar_group;

pal = waitbar_group(name, control, [], [], opt);


%-------------------------
% FEATURE_RENDER_PAGE
%-------------------------

function file = feature_render_page(result, context)

file = [context.prefix, '.html'];

% % TODO: look at other html rendering code in the system ...
% 
% for k = 1:numel(result.render)
% 	
% 	if ~isfield(result.render, 'image')
% 		continue;
% 	end
% 
% end


