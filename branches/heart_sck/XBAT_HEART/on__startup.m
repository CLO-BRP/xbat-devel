function on__startup(config)

% on__startup - helper for local configuration
% --------------------------------------------
%
% on__startup(config)
%
% Input:
% ------
%  config - name

% TODO: some documentation as to how to use this

%------------------------------------
% SHARED configuration
%------------------------------------

% NOTE: this turns off CUDA related code

cuda_enabled false;

%------------------------------------
% NAMED configuration
%------------------------------------

% NOTE: return quickly if no named configuration is requested

if ~nargin
	return;
end

switch config
	
	case 'default'
		
		%--
		% database connection
		%--

		% NOTE: this is the default database configuration, uses SQLite as database
		
%		store = get_barn_store('barn', 'SQLite'); 

		% NOTE: this is another typical configuration for the database server
		
 		store = get_barn_store('barn', 'MySQL');
% 		
 		store.password = 'fligh7';
		
		%--
		% web server interaction
		%--
		
		% NOTE: the command below creates the default server configuration
		
% 		server = rails_server;
		
		% NOTE: the fields below are the configurable fields for server interaction
		
		server.host = 'localhost';
		
		server.port = 3000;
		
		server.root = fullfile(app_root, 'BARN', 'Server', 'barn');
		
		server.files =  '';
		
		server.users =  '';
		
		%--
		% set environment variables 
		%--
		
% 		local_barn(store, server);
		
	% TODO: fill-in some typical options here?
		
	case 'localhost'
		
	case 'remotehost'
		
	case 'remotehost2'
		
	otherwise
		error(['Unknown configuration ''', config, '''.']);
		
end