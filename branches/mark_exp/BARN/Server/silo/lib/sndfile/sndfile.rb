require 'platform'

class Sndfile
  
  attr_accessor :file
  
  #--
  # setup on load
  #--
  
  root = File.join(File.expand_path(File.dirname(__FILE__)), 'bin')
  
  if Platform.is_windows?
    helpers = {
	  'info' => File.join(root, 'win32', 'sndfile-info.exe'),
	  'get' => File.join(root, 'win32', 'sndfile-metadata-get.exe'),
	  'set' => File.join(root, 'win32', 'sndfile-metadata-set.exe')
	}
  elsif Platform.is_mac?
    helpers = {
	  'info' => File.join(root, 'osx', 'sndfile-info'),
	  'get' => File.join(root, 'osx', 'sndfile-metadata-get'),
	  'set' => File.join(root, 'osx', 'sndfile-metadata-set')
	}
  else
    helper = {
	  'info' => 'sndfile-info',
	  'get' => 'sndfile-metadata-get',
	  'set' => 'sndfile-metadata-set'
	}
  end

  @@sndfile = helpers
  
  # TODO: add the various other helpers we well
  
  #--
  # instance methods
  #--
  
  def initialize(file)
    @file = file
  end
  
  # NOTE: this is the parsed output from sndfile-info
  
  def info
    parse_helper_output `#{@@sndfile['info']} "#{file}"`
  end
  
  def metadata
	parse_helper_output `#{@@sndfile['get']} "#{file}"`
  end
  
end

def parse_helper_output(output)
  info = {}

  output.each_line do |line|
	  parts = line.split(':').each {|part| part.strip!}
	  
	  next if parts.size < 2
	  
	  key = parts.first.gsub(/ /, '').downcase
	  
	  if parts.size == 2
		  value = parts.last
	  elsif parts.size > 2
		  value = parts[1..parts.size].join(':')
	  end
	  
	  info[key] = value
  end

  return info
end
