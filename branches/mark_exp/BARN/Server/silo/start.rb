#!/usr/bin/ruby

root = File.expand_path(File.dirname(__FILE__))
						
require File.join(root, 'vendor', 'platform', 'lib', 'platform')

#--
# start server
#--

puts "Silo is up and available at http://localhost:9292"

options = "-D --pid #{File.join(root, 'tmp', 'rack.pid')}"

rackup = File.join(root, 'vendor', 'rack', 'bin', 'rackup')

if !Platform.is_windows?
	`#{rackup} config.ru -E development`
else
	`ruby #{rackup} config.ru -E development`
end

