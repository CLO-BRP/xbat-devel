require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SharedController do
  #fixtures :user, :sound
  before :all do
      @bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
      @ted  = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
      
      @bills_public_sound  = default_sound(:name => "Bill's Public Sound", :public => 1, :user_id => @bill.id)
			@bills_private_sound = default_sound(:name => "Bill's Private Sound", :public => 0, :user_id => @bill.id)
      @teds_private_sound   = default_sound(:name => "Ted's Private Sound", :public => 0, :user_id => @ted.id)
  end
  
  describe "POST" do 
    integrate_views
        
    it "should allow sharing on owned objects" do
      login_as(@bill) # authorize_as(:default)
      post :create, {:type => "sound", :sound_id => @bills_public_sound.id, :login_or_email => "ted"}
      response.headers["Status"].should =~ /302/ # Redirected to requesting page

      post :create, {:type => "sound", :sound_id => @bills_public_sound.id, :login_or_email => "ted", :format => 'js'}
      response.headers["Status"].should =~ /201/ # Created
      
      Sharing.count.should equal(1)
      Shared.count.should equal(1)
    end
    
    it "should NOT allow sharing on objects that are not owned or previously shared and forwardable" do
      login_as(@ted)
      # quentin does not own sound one...
      #post :create, {:type => "sound", :sound_id => @bills_public_sound.id, :login_or_email => "xbat", :format => 'js'}
      #response.headers["Status"].should =~ /422/ # Unprocessable entity
      
      post :create, {:type => "sound", :sound_id => @bills_public_sound.id, :login_or_email => "ted", :format => 'html'}
      #response.flash[:error].should =~ /Error/
      #response.headers["Status"].should =~ /302/ # Redirected to requesting page
    end
      
  end # POST
  
  describe "DESTROY" do
    it "should delete an share that you created" do
      login_as(@bill)
      # create somethign - this sound is owned by default
      post :create, {:type => "sound", :sound_id => @bills_public_sound.id, :login_or_email => "ted", :format => 'js'}
      response.headers["Status"].should =~ /201/ # Created
      @sharing = assigns[:sharing]
      Sharing.count.should equal(1)
      
      delete :destroy, {:type => "sound", :sound_id => @bills_public_sound.id, :id => @sharing.id, :format => 'js'}
      Sharing.count.should equal(0)
    end
    
    it "should NOT allow destroying a share that is not yours" do
      # write this one
    end
  end

end