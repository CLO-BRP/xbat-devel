=begin

THIS TEST IS LARGELY UNNECESSARY.
RESPONSES TO THE AUTHORIZATION INTEROGATION BY THE GATEKEEPER ARE TESTED IN MODEL SPECS.
THIS SHOULD REALLY BE REPLACED BY A AUTHORIZATION APOCALYPSE SPEC

State table for access to a Sound resource.
In cells marked, additional conditions apply based on the state of another resource.

	
								|	public		|	private		|
	---------------------------------------
	not logged in	|						|						|
	---------------------------------------
	logged in			|	+					|	++				|
	---------------------------------------
	
	+  User logged in and Sound public
		
		* Owner of Sound?
		
	++ User logged in and Sound private

		* Owner of Sound?
		* Sound shared with User?
		* Accessed through a Project?
			- User a Project member?

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SoundsController do
	
	#----------
	# User NOT logged in
	#----------
	describe "User NOT logged in (possibly as the sound's owner)" do
		
		#----------
		# Sound is PUBLIC (x1,y1)
		#----------
		describe "Sound is PUBLIC (x1,y1)" do
			it "should have the correct authorization" do
				@sound = default_sound(:user => mock_model(User), :public => 1)
				
				# READ
				get :show, :id => @sound.id
				response.should be_success
				# UPDATE
				put :update, :id => @sound.id
				response.should be_redirect
				# DELETE
				put :destroy, :id => @sound.id
				response.should be_redirect
			end
		end

		#----------
		# Sound is PRIVATE (x2,y1)
		#----------		
		describe "Sound is PRIVATE (x2,y1)" do
			it "should have the correct authorization" do
				@sound = default_sound(:user => mock_model(User), :public => 0)
				
				# READ
				get :show, :id => @sound.id
				response.should be_redirect
				# UPDATE
				put :update, :id => @sound.id
				response.should be_redirect
				# DELETE
				put :destroy, :id => @sound.id
				response.should be_redirect
			end
		end
	end
	
	#----------
	# User logged in
	#----------
	describe "User logged in (possibly as the sound's owner)" do
		before do
			@user  = default_user
			@owner = default_user(:login => 'other', :email => "your@ss.com")
		end
		
		#----------
		# Sound is PUBLIC (x1,y2)
		#----------
		describe "Sound is PUBLIC (x1,y2)" do
			it "should have the correct authorization" do
				@sound = default_sound(:user => @owner, :public => 1)
				login_as @user
				
				# READ
				get :show, :id => @sound.id
				response.should be_success
				# UPDATE
				put :update, :id => @sound.id
				response.should be_redirect
				# DELETE
				put :destroy, :id => @sound.id
				response.should be_redirect
			end
		end
		
		#----------
		# Sound is PRIVATE (x2,y2)
		#----------
		describe "Sound is PRIVATE (x2,y2)" do
			before do
				@sound = default_sound(:user => @owner, :public => 0)
				@project = default_project(:public => 1, :user_id => @user.id)			
				@project.add_assets(Sound, @sound)
			end
			
			it "logged in as user should have the correct authorization" do
				login_as @user
				# READ
				get :show, :id => @sound.id
				response.should be_redirect
				# READ
				get :show, :id => @sound.id, :project_id => @project.id
				response.should be_success
			end
			
			it "logged in as owner should have the correct authorization" do
				login_as @owner
				# READ
				get :show, :id => @sound.id
				response.should be_success
				# UPDATE
				put :update, :id => @sound.id, :format => "js"
				# puts # response.headers["Status"]
				response.should be_success
				# DESTROY
				put :destroy, :id => @sound.id, :format => "js"
				response.should be_success
			end
		end
	end
end
