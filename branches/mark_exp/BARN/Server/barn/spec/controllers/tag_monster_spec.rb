require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

# This is really an all encompassing test for tags and tagging facilities for multiple controllers
# In order for this test to run, that is, for controller_name to be defined, 
# it must be in the spec/controllers folder :(
# this is fucking lame on the part of rspec

describe "TagMonster and friends" do
	
	before :all do
		@bernie  = default_user(:login => 'bernie.krause', :email => 'bernie.krause@email.com')
	end
	
	# We are testing the SoundsController here for the highest level tags behaviors since it is a prominent controller.
	# Additionally, the Tags selection mostly happens in the NamedScopesHelper as it retrieves tags based on the request.

	describe "SoundsController public listing" do
		controller_name :sounds
		
		before :all do
				@sound_one   = default_sound(:user_id => @bernie.id, :name => "Sound One", :public => 1)
				@sound_two   = default_sound(:user_id => @bernie.id, :name => "Sound Two", :public => 1)
				@sound_three = default_sound(:user_id => @bernie.id, :name => "Sound Three", :public => 1)
				@sound_four	 = default_sound(:user_id => @bernie.id, :name => "Sound Four", :public => 1)

				Tag.destroy_all
				Tagging.destroy_all
				
				# It is necessary to set this since we track who tagged something
				Tagging.current_user = @bernie
				
				@all_tags = %w{bird pigeon whale}
				
				@sound_one.update_attribute(:tag_list, "bird")
				@sound_two.update_attribute(:tag_list, "bird, pigeon")
				@sound_three.update_attribute(:tag_list, "pigeon")
				@sound_four.update_attribute(:tag_list, "whale")
		end
		
		it "should set the @tags variable" do
			get :index
					
	    assigns[:tags].should_not be_nil
		end
		
		it "should set both tags variable when filtered" do
			get :index, :query => 'bird'
			
			assigns[:tags].should_not be_nil
		end
		
		it "should have only tags of the resulting collection" do
			get :index, :query => 'bird'

			# setup
 			filtered_tags = Set.new(assigns[:tags].map{|tag| tag.name})
			expected_filtered_tags = Set.new(%w{bird pigeon})
			
			# assertions
			assigns[:sounds].size.should equal(2)
			#filtered_tags.should == expected_filtered_tags
			# This needs to be rewritten account for the precence of filtered tags in the filtered_counts, not an object
		end
	end	
	
	describe "LogsController public listing" do
		controller_name :logs
		before :all do
		end
		# INCOMPLETE
	end
	
end