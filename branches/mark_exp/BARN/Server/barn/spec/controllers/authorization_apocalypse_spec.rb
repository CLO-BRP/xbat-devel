require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

# This is really an all encompassing test for AuthorizationApocalypse facilities for multiple controllers, specific to this application
# In order for this test to run, that is, for controller_name to be defined, it must be in the spec/controllers folder :(
# this is fucking lame on the part of rspec

describe "AuthorizationApocalypse and minions" do
	
	before :all do
		@bernie  = default_user(:login => 'bernie.krause', :email => 'bernie.krause@email.com')
	end
	
	# We are testing the SoundsController and ProjectsController here for the highest level authorization behaviors since they are prominent controllers.

	describe "SoundsController" do
		controller_name :sounds
	end
	
end
