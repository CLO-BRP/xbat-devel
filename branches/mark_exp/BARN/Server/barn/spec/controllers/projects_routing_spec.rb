require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ProjectsController do

	# SET NOTATION TESTS
	describe "set notation routing" do	
		it "/projects/{:id} should hit the index method, not the show method" do
			params_from(:get, "/projects/{1}").should == {:controller => "projects", :action => "index", :id => "{1}"}
		end
	
		%w{js xml html}.each do |format| 
			it "/projects/{:id}.format should hit the index method, not the show method" do
				params_from(:get, "/projects/{1}.#{format}").should == {:controller => "projects", :action => "index", :id => "{1}", :format => format}
			end
		end
	end

	# ASSOCIATONS  
	describe 'route recognition for associations in set notation' do
		
		%w{sounds logs}.each do |resource| 
			it "/projects/1/#{resource}/{1} should hit the index method, not the show method" do
				params_from(:get, "/projects/1/#{resource}/{1}").should == {:controller => "#{resource}", :action => "index", :id => "{1}", :project_id => "1"}
			end
	
			%w{js xml html}.each do |format| 
				it "/projects/1/#{resource}/{1}.format should hit the index method, not the show method" do
					params_from(:get, "/projects/1/#{resource}/{1}.#{format}").should == {:controller => "#{resource}", :action => "index", :id => "{1}", :project_id => "1", :format => format}
				end
			end
		end
		
	end
	
end