require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RatingsController do
  fixtures :user, :sound, :rating
  @@formats = %w{json js xml}
  
  describe "GET" do 
    integrate_views

    describe "with user NOT logged in" do
     @@formats.each do |format| # loop through all the expected response formats
        it "should deny the request the denied" do
          get :index, :format => format
          response.headers["Status"].should =~ /401/ # Unauthorized
        end
      end
    end
    
    describe "with user logged in" do
      before :each do
        login_as(:default)
      end

      it "should not have an HTML representation" do        
        get :index, :format => 'html'
        response.headers["Status"].should =~ /406/ # Unacceptable
        get :index, :sound_id => 3, :format => 'html'
        response.headers["Status"].should =~ /406/ # Unacceptable
      end
      
      @@formats.each do |format| # loop through all the expected response formats  
      
        it "should be success as a resource" do
          login_as(:default) # authorize_as(:default)
          get :index, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end
      
        it "should be success as a nested (Sound) resource" do
          login_as(:default)
          get :index, :sound_id => 3, :format => format
          response.headers["Status"].should =~ /200/ # OK
        end
      
        it "should return all ratings if not requested as a nested resource" do
          get :index, :format => format # js and xml should also do the 
          response.headers["Status"].should =~ /200/ # OK
          # there are 2 ratings in the fixture made by default user
          assigns[:ratings].size.should equal(2) 
        end
      
        it "should return only ratings from the parent resource" do
          # the type and sound_id params are added automatically from the routes. 
          # in the spec, we must specify it manually
          get :index, :sound_id => 3, :type => 'Sound', :format => format
          # there is 1 rating in the fixture made by default user for Sound three
          assigns[:ratings].size.should equal(1)
        end
        
      end # formats
    end # "with user logged in"
  end # "GET"
  
  describe "POST" do 
    integrate_views
    
    describe "with user logged in" do
      before :each do
        login_as(:aaron)
      end

      @@formats.each do |format| # loop through all the expected response formats
        it "should create a new rating" do        
          post :create, :rating => {:rateable_id => sound(:four).id, :rateable_type => 'sound', :rating => '5'}, :format => format
          sound(:four).ratings_by_user(user(:aaron)).size.should equal(1)
          sound(:four).ratings_by_user(user(:aaron)).first.rating.should equal(5)
          response.headers["Status"].should =~ /201/ # Created
        end
        
        it "should update a new rating if it already exists" do        
          post :create, :rating => {:rateable_id => sound(:four).id, :rateable_type => 'sound', :rating => '4'}, :format => format
          sound(:four).ratings_by_user(user(:aaron)).size.should equal(1)
          sound(:four).ratings_by_user(user(:aaron)).first.rating.should equal(4)
          response.headers["Status"].should =~ /201/ # Created
        end
        
        it "should update a new rating if it already exists" do 
          post :create, :rating => {:rateable_id => 4, :rateable_type => 'sound', :rating => '2'}, :format => format
          post :create, :rating => {:rateable_id => 4, :rateable_type => 'sound', :rating => '3'}, :format => format
          post :create, :rating => {:rateable_id => 3, :rateable_type => 'sound', :rating => '2'}, :format => format
          sound(:three).ratings_by_user(user(:aaron)).size.should equal(1)
          sound(:four).ratings_by_user(user(:aaron)).size.should equal(1)
          # sound(:four).ratings_by_user(user(:aaron)).last.rating.should equal(2)
          response.headers["Status"].should =~ /201/ # Created
        end
      end
    end # describe "with user logged in"
  end # POST


end # describe RatingsController
