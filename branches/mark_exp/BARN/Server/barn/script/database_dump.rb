#!/usr/bin/ruby
require File.dirname(__FILE__) + '/../config/boot'

require "yaml"

config = YAML.load(File.open("#{RAILS_ROOT}/config/database.yml"))

db   = config['production']['database']
user = config['production']['username']
pass = config['production']['password']
dir  = File.join(RAILS_ROOT, "backup")

FileUtils.mkdir_p dir

command = "mysqldump -u#{user} -p#{pass} #{db} >#{dir}/#{db}_backup.sql"

result = `#{command}`
