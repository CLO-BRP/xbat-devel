#! /usr/local/bin/ruby

if ARGV.size < 3
	puts "ERROR: You must have 3 args: username, password, home directory"
	
else
	username = ARGV[0]
	password = ARGV[1]
	home_dir = ARGV[2]
	
	# Save the password to a file for later encryption
	puts cmd = "touch #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	puts cmd = "chmod 600 #{home_dir}/passwd.txt"
	result = puts `#{cmd}` # not sure if this is necessary
	
	puts cmd ="echo '#{password}' > #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	# Now for a little tomfoolery: use makepasswd to create an encrypted password from the one in the file,
	# and save it to a variable 'ph' - then, modify the users password to be the saved variable. 
	# This is done because Debian (and others) will not allow unencrypted passwords to be entered from stdin
	puts cmd = "ph=$(sudo makepasswd --clearfrom=#{home_dir}/passwd.txt --crypt-md5 | awk '{print $2}') && sudo usermod -p $ph #{username}"
	result = `#{cmd}`
	
	# Clean up the password
	puts cmd ="rm #{home_dir}/passwd.txt"
	result = `#{cmd}`
	
	puts "User #{username}'s password has been changed"
end

