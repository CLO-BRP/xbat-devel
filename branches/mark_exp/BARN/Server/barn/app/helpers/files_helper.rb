module FilesHelper
	
	def columns_default
		%w[file extension size created modified] # score] # created modified]
	end
  
	def column_markup(column, file)
		
		case column
			
		when 'file'
			
			# NOTE: the directory is displayed as a header
			
			# NOTE: '.files-listing' means that the content will be loaded in the '#files-listing' container
			
			# TODO: the 'heading_selector' part is not working
			
			if file.directory?
				link_to(
					"<h3 style='border: none; margin-bottom: 0em; padding-bottom: 0em;'>#{file.basename}</h3>",
					"/files?partial=files/listing&dirname=#{u params[:dirname]}/#{u file.basename}&heading_selector=#files-header",
					:class => 'render files-listing'
				)
			else
				file.basename
			end
		
		when 'size'
			file.size.to_s + ' bytes' # TODO: convert to a suitable unit
			
		else
			super
			
		end
		
  end
  
  def column_sorting(column, type)
				
		case column

		when 'file'
		  {:order => 'name', :direction => toggle_sort_direction("name")}
			
		else
			super
			
		end
		
  end
  
end
