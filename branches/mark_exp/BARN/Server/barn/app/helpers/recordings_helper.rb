module RecordingsHelper
  
  def columns_default
		
		if params.include? :sound_id
			%w[position file duration samplerate channels size created]
		else
			%w[file duration samplerate channels size sounds created]
		end
		
  end
  
  def column_markup (column, recording)
		
		case column
			when 'sounds'
				html = ""
				recording.sounds.map{ |sound| 
					positions 		= recording.recording_sounds.find_all{|rs| rs.sound_id == sound.id}.map{|rs| rs.position}
					positions_txt = (positions.blank?) ? ": (#{positions.join(', ')})" : ""
					#sound.name
					html << link_to( sound.name, sound_url(sound) ) << positions_txt
				}
				return html
			
			when 'file'
				html = link_to(recording.file, (recording.location || recording.public_filename))
					
				#html << "<ul class='graphic'><li>" + link_to('PLAY', recording.location, :class => 'render-inline-player') + "</li></ul>"
				
			when 'position'
				recording.recording_sounds.detect{|r| r.recording_id == recording.id }.position.to_s
				
			when 'bytes', 'size'
				size_to_bytes recording.bytes
				
			when 'samplerate'
				rate_to_hertz(recording.samplerate)
				
			when 'duration'
				sec_to_clock(recording.samples.to_f / recording.samplerate.to_f) unless recording.samples.blank? || recording.samplerate.blank?
				
			else
				super
		end
		
  end
  
  def column_sorting(column, type)
		
		case column
			when 'position'
				{:order => 'recording_sound.position', :direction => toggle_sort_direction('recording_sound.position')}
				
			when 'size'
				{:order => 'bytes', :direction => toggle_sort_direction('bytes')}
				
			when 'duration'
				{:order => 'samples', :direction => toggle_sort_direction('samples')}	
				
			else
				super
		end
		
  end
  
end