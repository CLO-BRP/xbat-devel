module NotesHelper

	def columns_default
		%w[title body]
	end

	def column_markup(column, note)
		case column
			when 'title'
				if @parents.blank?
					link_to( note.title, note_path(note))
				else
					link_to( note.title, polymorphic_path([@parents.first, note]))
				end
				
			when 'body'
				note.body
				
			else
				super
		end
	end

	def column_sorting (column, type)		
		params = {}
		
		case column
			when 'title'
				params = {:order => 'title', :direction => toggle_sort_direction('title')}
			when 'body'
				params = {:order => 'body', :direction => toggle_sort_direction('body')}		
			else
				return super
		end
		
		return params
	end

end
