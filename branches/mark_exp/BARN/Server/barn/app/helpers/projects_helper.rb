module ProjectsHelper
	
	def columns_default
		
		%w{project description rating tags created modified}		
	end
		
	def column_markup (column, project)
			
		case column
		when 'project'
			# mark up role and publicity
			if logged_in?
				role 			= (project.user_id == current_user.id)? "Owner" : "Member"; 
				publicity = (project.public)? "Public" : "Private"
				status = (project.public?)? 'unlocked' : 'locked'
				#icon = image_tag("#{status}.png", :alt => status, :title => status.capitalize, :class => 'status_symbol')
				icon = jquery_icon(status)
			else
				if project.owner.nil?
					role = "owner removed"
				else
					name = (project.owner.name.blank?)? project.owner.login : project.owner.name
					role = link_to(name, user_path(project.owner))
				end
				publicity = 'Public'
			end
		
			# TODO: write this using interpolated string syntax, that should be the standard here
			str		= '<b>%s %s</b> %s<br/><div style="display:none;margin: 0.75em 0em 1em"></div>'
			
			name	= link_to(h(project.name), project_path(project))
			
			# description = '('+ role + ', ' + publicity + ')'
			description = " (#{role})"

			return sprintf(str, icon, name, description)
		else
			super
		end # case
		
	end # column_markup
	 
	def column_sorting (column, type)
			
		case column

		when 'project'
			{:order => 'name', :direction => toggle_sort_direction('name')}
				
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating')}
			
		else
			return super
			
		end

	end

end
