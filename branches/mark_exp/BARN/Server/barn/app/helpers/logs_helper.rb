module LogsHelper
  
  def columns_default
		
		%w[log events rating tags created modified]
		
  end
  
  def column_markup (column, log)
	
		case column
			
		when 'log'
			link_to(log.name, log_path(log))
			
		when 'events'
			log.events.count
			
		else
			super
			
		end
	
  end
  
  def column_sorting(column, type)
	
		case column
			
		when 'log'
			{:order => 'name', :direction => toggle_sort_direction('name')}
		
		when 'events'
			{}
			
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating')}
		
		else
			super
		
		end
		
  end
  
end
