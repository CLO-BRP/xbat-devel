module SoundsHelper
	
  def columns_default
	  if logged_in?
			%w[name duration channels samplerate rating tags]
		else
			%w[name duration channels samplerate contributor rating tags]
		end 
  end
  
  def column_markup(column, sound)
		
		case column
			
		when 'name'
			html = ""
			if logged_in?
				status = (sound.public?)? 'unlocked' : 'locked'
				html << jquery_icon(status)
				#html << image_tag("#{status}.png", :alt => status, :class => 'status_symbol') 
			end
			if params[:project_id]
				html << link_to(sound.name, project_sound_path(params[:project_id],sound))
			else
		  	html << link_to(sound.name, sound_path(sound))
			end
			
		when 'duration'
		  sec_to_clock(sound.duration)
		
		when 'contributor'
			sound.user.login if sound.user
			
		else
			super
			
		end
		
  end
	
  def column_sorting (column, type)		
	  params = {}
	  
	  case column
		  when 'log'
			  params = {:order => 'log.name', :direction => toggle_sort_direction('log.name')}
		  else
			  return super
	  end
	  
	  return params
  end
	
end
