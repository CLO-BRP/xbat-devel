module EventsHelper
  
  def columns_default
		
		if params[:partial] == 'events/listing'
			
			#NOTE: these are listing table columns
			
			if params.include? :sound_id
				%w[event log time freq rating tags]
				
			elsif params.include? :log_id
				%w[event time freq rating tags]
				
			else
				%w[event time freq rating tags] # TODO: include a sound/log display column, not yet defined
				
			end
		else
			
			#NOTE: these are 'sort by' fields in clips display
			
			if params.include? :sound_id
				%w[log time duration freq rating created_at]
				
			elsif params.include? :log_id
				%w[time duration low high rating created_at]
				
			else
				%w[time duration low high rating created_at] # TODO: include a sound/log display column, not yet defined
				
			end
		end
			
  end
  
  def column_markup(column, event)
		
		case column
		
		#--
		# event displays
		#--
		
		#TODO: develop small, medium, large event displays
		
		when 'event'
			
			#--
			# setup
			#--
			
			tip_id = "event_#{event.id}_tip" #rand_id
			
			if event.duration.blank?
				time = sec_to_clock(event.start)
			else
				time = sec_to_clock(event.start) + '&mdash;' + sec_to_clock(event.start + event.duration) 
			end
			
			if params.include? :log_id
				title = "#{event.log.name} &mdash; ##{event.id}"
			else
				# TODO: this fails in the pure events view, the sound is 'nil'
				#title = "#{event.log.sound.name} &mdash; #{event.log.name} &mdash; ##{event.id}"
				title = "#{event.log.name} &mdash; ##{event.id}"
			end
			
			#--
			# create base event markup for the different views
			#--
			
			if params[:partial] == 'events/listing'
			  
			  html =
			  "
				#{link_to(title, event_url(event), :class => 'load-local-tip', :rel => '#' + tip_id, :title => title)}
			  
			  <div class='thumb local-tip' id='#{tip_id}'>
				  <a class='render-inline-player' href='#{files_root_url(event, 'clip.mp3')}'>
					  <div class='progress' style='height: 129'></div>
					  <img src='#{files_root_url(event, 'thumb.256.gray-light.png')}'/>
				  </a>
			  </div>
			  "
			else
				p = params.merge({:id => event.id, :action => 'show', :partial => 'events/details_hover'})
				
				html = render(:partial => "widgets/inline_player", :locals => {
					:resource => event,
					:tip_location => url_for(p),
					:tip_title => title
				})
				
				# html << render(:partial => "events/details_hover", :locals => {:event => event})				
			end
		# when 'event'
		
		#--
		# time displays
		#--
		
		when 'start'
			sec_to_clock(event.start)
			
		when 'duration'
			sec_to_clock(event.duration)
			
		when 'time'
			if event.duration.blank?
				sec_to_clock(event.start)
			else
				sec_to_clock(event.start) + '&mdash;' + sec_to_clock(event.start + event.duration) 
			end
		
		#--
		# frequency displays
		#--
		
		when 'low', 'high'
			rate_to_hertz(event.send(column))
			
		when 'freq'
			if event.low.blank?
				''
			else
				rate_to_hertz(event.low) + '&mdash;' + rate_to_hertz(event.high)
			end
			
		when 'log'
			link_to(event.log.name, log_path(event.log))
	
		# NOTE: we use the application helper default
		
		else
			super
			
		end
		
  end
  
  def column_sorting(column, type)
				
		case column

		when 'event'
			{:order => 'duration', :direction => toggle_sort_direction("duration")}
			
		when 'time'
		  {:order => 'start', :direction => toggle_sort_direction("start")}
		
		when 'freq'
		  {:order => 'low', :direction => toggle_sort_direction("low")}
		  
		when 'log'
		  {:order => 'log.name', :direction => toggle_sort_direction("log.name"), :include => :log}
		
		# NOTE: sorting information is provided by application helper
		
		else
			super
			
		end
		
  end

end
