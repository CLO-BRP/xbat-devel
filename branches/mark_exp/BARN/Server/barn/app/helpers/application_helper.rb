# NOTE: methods added to this helper will be available to all templates in the application

module ApplicationHelper
	
	include ServerStatistics
	
	# NOTE: this include is not working as a way of including the 'tag_cloud' helper
	
	include TagsHelper
	
	def tag_cloud(tags, classes)
		return if tags.blank?

		# NOTE: remove the plus 1 when we are done
		
		max_count = tags.sort_by(&:count).last.count.to_f + 1
		
		tags = tags.sort_by(&:name)
		
		tags.each do |tag|
		  index = ((tag.count.to_i / max_count) * (classes.size - 1)).round
		  
		  yield tag, classes[index]
		end
	end
	
	
	def jquery_icon (type, opt = {})
		
		# NOTE: the 'icon' is a div.jquery-icon
		
		html = ""
		
		html = "<ul style='list-style: none; margin: 0px; padding: 0px; float: left'><li style='float: left; margin: 0px; margin-right: 1em;' id='#{opt[:id]}' class='#{opt[:class]} jquery-icon ui-state-default ui-corner-all'>"
		
		# NOTE: the actual icon is an icon div child with class 'ui-icon'
		
		html << "<span uri='#{opt[:uri]}' class='ui-icon ui-icon-#{type}'></span>"
	
		# NOTE: so the icon can have associated targets (we use class selection) or target (id selection)
		
		# TODO: consider moving this to use attributes of the container div
		
		if !opt[:targets].blank?
			html << "<span style='display: none;' 'class='targets'>" + opt[:targets] + "</span>"
		end
		
		if !opt[:target].blank?
			html << "<span style='display: none;' 'class='target'>" + opt[:target] + "</span>"
		end
		
		html << "</li></ul>"
		
	end
	
	# TODO: what we are trying to do is create a simple way to create a background load toggle
	
	def toggle_control (target, uri)
		
		jquery_icon 'plus', {:class => 'toggle-control', :targets => target, :uri => uri}
	
	end
	
	def render_flat(options = {})
		render(options).gsub(/\n/,'')
	end
	
	def toggle_sort_direction(column = nil)
		if column and column == params[:order]
			if params[:direction].blank? or params[:direction].downcase == "desc"
				"asc"
			else
				"desc"
			end
		else
			"asc" 
		end
	end
	
	# TODO: check whether this is in use anywhere, deprecated in favor of random identifiers
	
	def id_from_uri(uri)
		
		return nil if uri.nil? # otherwise tests will fails because of the way we are determining path information
		
		#--
		# Make the container ID to which AJAX responses from this partial are targeted
		#--
		
		path_array = uri.split('/')
		path_array.delete_at(0) # this returns the deleted element, thus no chaining...
		path = path_array.join('-') 
		
		#--
		# account for possible format extension
		#--
		
		if path.split('.').size > 1
			path_array = path.split('.')
			path_array.pop
			path = path_array.join("")
		end
		
		return path
	
	end
	
	def rand_id
		'id_' + rand(999999999).to_s
	end
	
	def filesep
		Platform.is_windows? ? '\\' : '/'
	end
	
	def attachment_path(id)
		("%08d" % id).scan(/..../).join('/')
	end

	#---------------------
	# DISPLAY HELPERS
	#---------------------
	
	def title_caps (title)
		part = title.gsub(/_/, ' ').split(' '); part[0].capitalize!; part.join(' ')
	end
	
	def sec_to_clock (sec)
		return "0:00" if sec.nil?
		
		clock = [sec/3600, sec/60 % 60].map{|t| t.floor.to_s.rjust(2, '0')}.join(':') + ':'
		
		sec = sec % 60
		
		if sec >= 10
			clock += sprintf('%0.3f', sec)
		else
			clock += '0' + sprintf('%0.3f', sec)
		end
	end
	
	def rate_to_hertz (rate)
		return if rate.blank? 
		
		if rate > 1000
			sprintf('%.2f kHz', rate/1000)
		else
			sprintf('%.2f Hz', rate)
		end
	end
	
	def score_to_percent (score)
		if score.blank?
			return '' 
		else
			return sprintf('%.2f', 100 * score) + '%'
		end
	end
	
	def size_to_bytes (size)
		return if size.nil? 
		
		frac = 1
		
		if size > frac.terabytes
			sprintf('%.2f', size / 1.terabyte.to_f) + ' TB'
		
		elsif size > frac.gigabytes
			sprintf('%.2f', size / 1.gigabyte.to_f) + ' GB'
			
		elsif size > frac.megabytes
			sprintf('%.2f', size / 1.megabyte.to_f) + ' MB'
			
		elsif size > frac.kilobytes
			(size / 1.kilobyte).to_s + ' kB'
			
		else
			size.to_s + ' B'
			
		end
	end
	
	#---------------------
	# LISTING STUFF
	#---------------------
	
	def get_listing_display_options(locals, params)
		
		# TODO: this needs work
		
		options = {:heading => 0, :caption => 1, :selection => 1, :expansion => 1}
		
		options.merge(locals).merge(params)
				
	end
	
	def column_markup (column, object)
	
		case column
		
		when 'rating', 'rating.rating'
				
			"
			<div class='render simple-rating' target='#{send(object.class.to_s.downcase + '_path', object)}'>
				#{object.rating}
			</div>
			"
			#render(:partial => 'ratings/ratings', :locals => {:rateable => object})
		
		when 'score'
			score_to_percent(object.score)
	
		when 'tags'
			render(:partial => 'tags/tags', :locals => {:taggable => object})
			
		when 'bytes'
			size_to_bytes object.bytes
				
		when 'samplerate'
			rate_to_hertz(object.samplerate)
		
		when 'created', 'modified'
			column = column + '_at';
			"
				<span class='at-time-ago'>
					#{time_ago_in_words(object.send(column)) + ' ago' unless object.send(column).nil?}
				</span>
				<span class='at-time'>
					#{object.send(column) if object.respond_to?(column)}
				</span>
			"

		else
			begin
				h(object.send(column))				
			rescue
				'failed to markup ' + column
			end
			
			
		end
			
	end
	  
	def column_sorting (column, type)
				
		case column

		when 'rating'
		  {:order => 'rating.rating', :direction => toggle_sort_direction('rating.rating'), :include => 'ratings'}
		  
		when 'tags'
			{}
		
		when 'created', 'modified'
			column = column + '_at'; {:order => column, :direction => toggle_sort_direction(column)}
			
		else
			{:order => column, :direction => toggle_sort_direction(column)}
				
		end
			
	end
	
	
	#---------------------
	# FEED STUFF
	#---------------------
	
	# TODO: consider using an SVG icon
	
	def feed_title(type)
		if params.include?("query")
			title = "#{type.capitalize} matching query '#{params['query']}'"
		else
			title = "#{type.capitalize}"
		end
		
		return title
	end
	
	
	def feed_link(type)		
		remove = %w{heading_markup heading_selector partial per_page page}
		
		rss_params = params.reject{|key,val| remove.include?(key)}
			
		rss_params['format'] = :rss
			
		link = link_to(
			image_tag('feed-icon.png', :border => 0, :style =>'padding-top:3px;'),
			rss_params,
			:title => feed_title(type) + ' RSS Link'
		)
		
		return "<div style='float:left;'>#{link}</div>"
	end
	
	#---------------------
	# FILES LINKS
	#---------------------
	
	# NOTE: the file location convention uses local and global identifiers
	
	def files_root_url(obj, file = '')    
		url = File.join("files", obj.class.to_s.downcase, attachment_path(obj.id), obj.guid)
		
		url = File.join(url, file) unless file.empty?
		
		# NOTE: we need this to indicate that this is a root-based url
		
		url = '/' + url
				
		return url
	end
	
	alias_method :files_root_path, :files_root_url
	

	def files_full_url(obj, file='')
		File.join(RAILS_ROOT, "public", files_root_url(obj, file))
	end
	
	alias_method :files_full_path, :files_full_url

end
