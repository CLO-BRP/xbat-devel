module SharesHelper
  
  def columns_default
		
		%w[position file bytes format channels samplerate duration created]
		
  end
  
  def column_markup (column, recording)
	
		case column
		
		when 'position'
			recording.recording_sounds.detect{|r| r.recording_id == recording.id }.position.to_s
		
		when 'samplerate'
			rate_to_hertz(recording.samplerate)
			
		when 'duration'
			sec_to_clock(recording.samples.to_f / recording.samplerate.to_f) unless recording.samples.blank? || recording.samplerate.blank?
		
		else
			super
			
		end
	
  end
  
  def column_sorting(column, type)
	
		case column
			
		when 'duration'
			{:order => 'samples', :direction => toggle_sort_direction('samples')}

		else
			super
		
		end
		
  end
  
end