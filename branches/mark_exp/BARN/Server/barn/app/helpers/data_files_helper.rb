module DataFilesHelper
	
	def columns_default
		%w[in_ftp file extension size created modified] # score] # created modified]
	end
  
	def column_markup(column, file)
		case column
			when 'extension'
				file.ext

			when 'in_ftp'
				file.in_ftp
				
			when 'created'
				file.ctime
				
			when 'modified'
				file.mtime
			
			when 'file'
				if file.directory?
					link_to(
						"<h3 style='border: none; margin-bottom: 0em; padding-bottom: 0em;'>#{file.name}</h3>",
						"/data_files?partial=data_files/listing&dirname=#{u params[:dirname]}/#{u file.name}&heading_selector=#data-files-header",
						:class => 'render files-listing'
					)
				else
					file.name # + "</br><pre class='hash'>#{file.content_hash}</pre>"
				end
		
			when 'size'
				file.size.to_s + ' bytes' # TODO: convert to a suitable unit
			
			else
				super			
		end
  end
  
  def column_sorting(column, type)				
		case column
			when 'file'
			  {:order => 'name', :direction => toggle_sort_direction("name")}
			else
				super
			end
  end
  
end
