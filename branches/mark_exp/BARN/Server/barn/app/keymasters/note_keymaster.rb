module NoteKeymaster
  module ClassMethods	
    def indexable_by?(user, parents = nil)
  		# If requested through a parent(s), check the parents readability
  		(parents && !parents_readable_by?(user, parents)) ? false : true
    end

  	def creatable_by?(user, parent = nil)
  		# TODO: check for user state
  		return false unless parent
  		return (user)? true : false
    end
  end
  
  def readable_by?(user, parents = nil)
		return true if (user && self.notings.any?{|n| n.user_id == user.id})
		return true if parents && parents_readable_by?(user, parents)
		return false
  end
	
	# TODO!
  def updatable_by?(user, parent = nil)
		(user && user.id == self.user.id) ? true : false
  end

	# TODO!
  def deletable_by?(user, parent = nil)
		(user && user.id == self.user.id) ? true : false
  end
end