module RecordingKeymaster
  # NOTE: recordings do not have a user_id, nor an owner
	# they MUST currently be accessed through a parent!
  
  module ClassMethods
    def indexable_by?(user, parents = nil)
      if parents_readable_by?(user, parents)
        true
      else
        user ? true : false
      end
    end

  	def creatable_by?(user, parent = nil)
      user ? true : false
    end
  end
	
  def readable_by?(user, parents = nil)
    #owned_by?(user) ||
    parents_readable_by?(user, parents)
  end

  def updatable_by?(user, parent = nil)
		#owned_by?(user)
  end

  def deletable_by?(user, parent = nil)
		#owned_by?(user)
  end

  #def owned_by?(user)
  #  return false if user.blank?
  #  
  #  (user_id == user.id)
  #end
end