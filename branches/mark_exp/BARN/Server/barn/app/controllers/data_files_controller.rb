class DataFilesController < ApplicationController
	
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	
	# before_filter :has_permission?

	def index
		#--
		# set default page params
		#--
		
		page = params[:page] || 1
		
		per_page = params[:per_page] || 10
		
		#--
		# consider file hierarchy
		#--
		
		if params[:dirname].blank?
			
			conditions = ['user_id = ? AND parent_id is NULL', @user.id]
			
		else
			parent = DataFile.find(:first, :conditions => ["user_id = ? AND location = ?", @user.id, File.join(@user.ftp_root, params[:dirname])])

			if parent.blank?
				render :status => 404
				return
			end
			
			conditions = ['user_id = ? AND parent_id = ?', @user.id, parent.id]			
		end
		
		#--
		# consider query
		#--
		
		if params[:query]
			query = "name LIKE '%#{params[:query]}%'"
		end
		
		#--
		# get data files and count
		#--
		
		@data_files = DataFile.in_set( params[:id] ).with_conditions( conditions ).with_conditions(query).ordered_and_paginated(params)

		@metadata = {
			:total_entries => DataFile.with_conditions( conditions ).with_conditions(query).count,
			:per_page => per_page,
			:page => page
		}

		#--
		# respond
		#--
		
		response_for( @data_files, {:metadata => @metadata}.merge(params) )
	end

end