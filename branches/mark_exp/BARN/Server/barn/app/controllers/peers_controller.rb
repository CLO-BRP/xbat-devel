class PeersController < ApplicationController
	before_filter :login_required
	
	def index
		results = Peer.paginate_by_user_with_metadata(@user, params)

		@peers 		= results[:collection]
		@metadata = results[:metadata]
		@tags     = results[:tags]

		response_for(@peers, {:metadata => @metadata}.merge(params))
	end
	
end