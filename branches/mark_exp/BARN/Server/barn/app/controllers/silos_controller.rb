class SilosController < ApplicationController
	verify :method => :post, 		:only => [ :create, :register, :deferred_response ]
	verify :method => :put, 		:only => [ :update ]
	verify :method => :delete, 	:only => [ :destroy ]
	
	before_filter :login_required
	
	def index
		@silos = Silo.find(:all, :conditions => ["user_id = ?", current_user.id])
	end
	
	# also #register
	# registration, pass back a GUID
	def create
		# Set the host to something reasonable
		if ['localhost', '127.0.0.1'].include?(request.remote_ip)
			@host = 'localhost'
		else	
			@host = request.remote_ip
		end
    
    # Indescriminate creation of a Silo entry
		@silo = Silo.create(
		  :host => @host,
		  :port => params[:port],
		  :user_id => current_user.id,
		  :created_at => Time.now
		)

		if @silo.errors.blank?
		  # Respond with a Created status
		  render :text => @silo.to_json, :status => 201
	  else
	    render :text => @silo.errors.to_json, :status => 500
    end
	end
	
	def show
	end
	
	def update
	end
	
	def destroy
	end
	
	#---------------------
	# Bot-net Silo methods
	#---------------------
	
	# GET silos/:guid/:request
	
	# First try to connect to the remote Silo.
	# If it cannot be reached, then save the deferred request
	
	def remote_request
		render(:text => "GUID required", :status => 500) and return unless params[:guid]

		@silo = Silo.find_by_guid(params[:guid]) 
		
		if @silo.blank? || @silo.user.id != current_user.id
			render(:text => "No such Silo #{params[:guid]}", :status => 500) and return
		end
				
		# Now lets try to send the request to the Silo
		# This request is **synchronous**, that is, the action blocks until a response is recieved.
		
		#TODO: make sure this times out
		request = (params[:format]) ? "#{params[:request]}.#{params[:format]}" : params[:request]
		
		@response = @silo.get(request, {
			:error 		=> lambda{|error| 
				logger.error(error)
				create_deferred_request(params[:request], @silo.id, current_user.id)
			},
			:complete => lambda{|response|
				# Server Timeout.. maybe there are other things to catch
				if [408, 503, 504].include?(response.code.to_i)
					create_deferred_request(params[:request], @silo.id, current_user.id)
				end
			}
		})
		
		render :text => (@response) ? @response.body : "There has been an error connecting to the server."
	end
	
	def create_deferred_request(request, silo_id, user_id)
		deferred_request = DeferredRequest.create(
			:request => params[:request], 
			:silo_id => @silo.id, 
			:user_id => current_user.id, 
			:submitted_at => Time.now
		) 
		
		return (deferred_request.errors.blank?) ? true : false 
	end
	
	# GET silos/:guid/request
	# returns a deferred request from the queue
	def deferred_request
		@silo = Silo.find_by_guid(params[:guid])
		
		@request = DeferredRequest.find(:first, 
			:conditions => ["silo_id = ?", @silo.id], 
			:order => {:submitted_at => "ASC"}
		)
		
		render :text => @request.to_json
	end
	
	# POST silos/:guid/response
	def deferred_response
		render :text => "true"
	end
end