# NOTE: this is necessary because somehow the namespace is getting polluted with Rake::Task

require 'task'

class TasksController < ApplicationController
	
	#include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	
	def index
		@tasks = Task.all.paginate(:page => params[:page] || 1, :per_page => params[:per_page] || 10)
		
		@metadata = {:total_entries => Task.count}
		
		response_for(@tasks, {:metadata => @metadata}.merge(params))
	end
	
	def create
		ids = params[:id].split(",")
		
		@xml = Builder::XmlMarkup.new
		
		@xml.comment! "M2XML"
		
		@xml.task(:class=>'struct', :size=> '1 1') {
			@xml.type(params[:type], :class=>'char', :size=> '1 1')
			@xml.action( params[:task_action], :class=>'char', :size=> '1 1')
			@xml.id(ids.join(" "), :class=>'double', :size=>"1 #{ids.size}") 
		}
	
		# TODO: something is fishy, 'created_at' is not automatically set ...
	
		@task = Task.create(
			:task => @xml.target!, 
			:priority => 1,
			:user_id => @user.id,
			:created_at => Time.now
		)
		
		flash[:notice] = "A task has been created to import files: #{ids.join(', ')}"
		
		redirect_to "/#{params[:redirect]}" || '/'
	end
	
	def update
		
	end
	
	def destroy
	
	end
	
end