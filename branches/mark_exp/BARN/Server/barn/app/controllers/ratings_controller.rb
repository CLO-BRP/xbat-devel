class RatingsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required
	before_filter :has_permission?

	skip_before_filter :verify_authenticity_token

	# GET /ratings
	# GET /ratings.xml
	
	def index
		# TODO: this should be a limited set
		# for the moment, we are filtering ratings returned by the user, 
		# thus only returning ratings made by a user
		@ratings = if parent_resource_class
			parent_resource_class.find(parent_id).ratings_by_user(current_user)
		else
			Rating.find(:all, :conditions => ["user_id = ?", current_user.id])
		end
		
		respond_to do |format|
			format.xml { render :xml => @ratings }
			format.any(:json, :js)	{ render :json => @ratings }
		end
	end


	# GET /rating/1
	# GET /rating/1.xml

	def show
		@rating = Rating.find(params[:id])

		respond_to do |format|
			format.xml { 
				if @events.blank?
					render :xml => @ratings
				else
					render :text => @ratings.to_xml(:dasherize => false) 
				end
			}
			format.any(:json, :js)	{ render :json => @rating }
		end
	end

	
	# GET /rating/new
	
	def new
		@rating = Rating.new

		respond_to do |format|
			format.xml { render :text => @ratings.to_xml(:dasherize => false) }
			format.any(:json, :js)	{ render :json => @rating }
		end
	end

	
	# POST /rating
	
	# This method now supports 2 formats of rating
	
	# POST /events/1/ratings
	# value=5
	
	def create
		
		if params[:value]
			parent = params.find{ |key, value| key.to_s.include?("_id") }.to_a.flatten
			
			if parent.size > 0	
				# We are only honoring single parent nesting
				type = parent[0].split("_")[0].capitalize
				id 	 = parent[1]
				# make this compatible with the rest of the script
				params[:rating] = {
					:rating 			 => params[:value],
					:rateable_type => type, 
					:rateable_id 	 => id
				}
			end
		end
		
		if params[:rating]
			rateable_id	 = params[:rating][:rateable_id]
			rateable_type = params[:rating][:rateable_type].capitalize
		end
		
		# If we do not have any valid parameters, return an error
		if rateable_type.nil? and rateable_id.nil?
			respond_to do |format|
				format.xml { render :xml => {:error => 'Missing Parameters'}, :status => :unprocessable_entity }
				format.any(:json, :js)	{ render :text => '{error:"Missing Parameters"}', :status => :unprocessable_entity }
			end 
			return
		end
			
		@rating = Rating.find(:first, 
			:conditions => "user_id = #{current_user.id} 
				AND rateable_type = LOWER('#{rateable_type}') 
				AND rateable_id = #{rateable_id}"
		)
	
		# If there is an existing rating, update it, otherwise create a new one.. 
		# it is hard to know this from the browsers perspective
		# ... but think about ways to make this happen with PUTs from browser
		if @rating
			@rating.attributes = params[:rating]
		else
			@rating = Rating.new(params[:rating])
			@rating.user_id = current_user.id
		end
	
		respond_to do |format|
			if @rating and @rating.save
				format.xml { render :xml => @rating, :status => :created, :location => @rating }
				format.any(:json, :js) { render :json => @rating, :status => :created, :location => @rating }
			else
				format.xml { render :xml => @rating.errors, :status => :unprocessable_entity }
				format.any(:json, :js)	{ render :json => @rating.errors, :status => :unprocessable_entity }
			end
		end
	end

	
	# PUT /rating/1
	
	def update
		@rating = Rating.find(params[:id])

		respond_to do |format|
			if @rating.update_attributes(params[:rating])
				format.xml { head :ok }
				format.js	{ head :ok }
			else
				format.xml { render :xml => @rating.errors, :status => :unprocessable_entity }
				format.js	{ render :json => @rating.errors, :status => :unprocessable_entity }
			end
		end
	end

	
	# DELETE /rating/1

	def destroy
	
		@rating = Rating.find(params[:id])	 
														
		logger.debug @rating.inspect
		@rating.destroy

		respond_to do |format|
			format.xml { head :ok }
			format.js	{ head :ok }
		end
	end
	
end
