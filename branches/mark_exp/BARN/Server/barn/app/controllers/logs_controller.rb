class LogsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
  before_filter :login_required, :except => [:index, :show]
  before_filter :has_permission?

  # Example standard URIs
  # GET /logs
  # GET /logs.(html|xml|js|json)
  # GET /logs/{1}
  # GET /logs/{1-10, 15}
  # GET /logs.xml?public=1&page=1&order=name

  # Example Nested URIs
  # GET /sounds/1/logs
  # GET /sounds/{1,3,5-10}/logs
  
  def index
    user = (logged_in? && params[:public].blank?) ? current_user : nil
		
	# We do not want to limit the logs result by user-owned logs if we have parentage
	
	results = if requested_through_parents?
		Log.paginate_with_metadata( params )
	else
		Log.paginate_by_user_with_metadata( user, params )
	end
	
	@logs     = results[:collection]
	@metadata = results[:metadata]
	@tags     = results[:tags]
		
    response_for( @logs, {:metadata => @metadata}.merge(params) )
  end
  

  # GET /logs/1

  def show
    @log = Log.find(params[:id])
    
	response_for( @log, params )
  end

  # GET /logs/new

  def new
    # THIS CANNOT BE ACCESSED WITHOUT A SOUND ID!
    if params[:sound_id].blank?
      flash[:error] = "Logs cannot be added without a Sound"
      redirect_to sounds_path 
			return
    end

    @log = Log.new :sound_id => params[:sound_id]
    
		response_for( @log, params )
  end

  # GET /logs/1/edit

  def edit
    @log = Log.find(params[:id])

		respond_to do |format|
      format.html
      # is this method really needed in other formats??
    end
  end

  # POST /logs

  def create
    @log = Log.new(params[:log])
    @log.user = current_user

    respond_to do |format|
      if @log.save
        flash[:notice] = 'Log was successfully created.'
        format.html { 
          redirect_to sound_path(@log.sound)
        }
        format.xml  { render :xml => @log, :status => :created, :location => @log }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @log.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /logs/1

  def update
    @log = Log.find(params[:id])

    respond_to do |format|
      if @log.update_attributes(params[:log])
        format.xml { head :ok }
        format.any(:js, :json)  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @log.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /logs/1

  def destroy
		@logs = Log.in_set(params[:id])
    
		@logs.each do |log|
			begin
				log.destroy
			rescue Exception => e
				logger.error e
			end
		end

    respond_to do |format|
      format.html { redirect_to logs_url }
      format.xml  { head :ok }
      format.js  { head :ok }
      format.json  { head :ok }
    end
  end
end
