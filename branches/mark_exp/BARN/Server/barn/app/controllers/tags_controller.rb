class TagsController < ApplicationController
	
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	
	before_filter :has_permission?
	
	def autocomplete
	
	end

	# GET /tags

	def index
		
		# TODO: write this condition into with_parent named_scope, IT SHOULD LOOK LIKE THIS:
		
		# results = klass.paginate_by_user_with_metadata(user, params)
		
		# @tags = results[:tags]
        
		if params[:parent]
			
			filter = params[:query]
			
			begin
				klass = params[:parent].classify.constantize
			rescue NameError => e
				barn_logger :info, trace, "#{params[:parent].classify} is not taggable: #{e}"
				
				render :file => "#{RAILS_ROOT}/public/404.html", :status => 404 and return
			end
			
			conditions = {:conditions => "tag.name LIKE '%#{filter}%'"}
			
			if params[:parent_id]
				@tags = klass.in_set(params[:parent_id]).tag_counts(conditions)
			else
				@tags = klass.tag_counts(conditions)
			end
			
			@metadata = {
				:total_entries => @tags.size,
				:per_page => params[:per_page] || Tag.per_page,
				:page => params[:page] || 1
			}
			
		end
	
		# NOTE: this is where respond to autocomplete request
		
		if params[:autocomplete]
			render :text => @tags.map{|tag| tag.name}.join("\n"); return
		end
		  
		response_for(@tags, {:metadata => @metadata}.merge(params))
	end


	# GET /tag/1

	def show
		@tag = Tag.find(params[:id])
		
		respond_to do |format|
			format.xml { render :xml => @tag }
			format.js  { render :json => @tag }
		end
	end


  # NOTE: the rails generators use 2 space indentation, perhaps we should use the same throughout
  
  # GET /tag/new

  def new
    @tag = Tag.new

    respond_to do |format|
      format.xml { render :xml => @tag }
      format.js  { render :json => @tag }
    end
  end

 
  # POST /tag

  def create
    @tag = Tag.new(params[:tag])

    respond_to do |format|
      if @tag.save
        format.xml { render :xml => @tag, :status => :created, :location => @tag }
        format.js  { render :json => @tag, :status => :created, :location => @tag }
      else
        format.xml { render :xml => @tag.errors, :status => :unprocessable_entity }
        format.js  { render :json => @tag.errors, :status => :unprocessable_entity }
      end
    end
  end


  # PUT /tag/1

  def update
    @tag = Tag.find(params[:id])

    respond_to do |format|
      if @tag.update_attributes(params[:tag])
        format.xml { head :ok }
        format.js  { head :ok }
      else
        format.xml { render :xml => @tag.errors, :status => :unprocessable_entity }
        format.xml { render :js => @tag.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /tag/1

  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy

    respond_to do |format|
      format.xml { head :ok }
      format.js { head :ok }
    end
  end
end
