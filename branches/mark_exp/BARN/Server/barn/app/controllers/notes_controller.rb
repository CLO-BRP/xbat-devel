class NotesController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:index, :show]
	before_filter :has_permission?
		
	# GET /notes
	# GET /notes.(html|xml|js|json)
	# GET /notes/{1}
	# GET /notes/{1-10, 15}
	# GET /notes.xml?public=1&page=1&order=name

	# GET /sounds/1/notes
	# GET /sounds/{1,3,5-10}/notes

	def index
		user = (logged_in? && params[:public].blank?) ? current_user : nil
		
		# Since notes is almost purely polymorphic in its parentage, there is no way to specify 
		# a public named_scope with any meaning, so we trust the parents who are accessible are filtered
		# by the authorized? method
		
		if requested_through_parents?
			results = Note.paginate_with_metadata( params )
			
			@notes, @metadata, @tags = results[:collection], results[:metadata], results[:tags]
			
			response_for @notes, {:metadata => @metadata }.merge( params )
			return
		end
		
		if logged_in?
 			results = Note.paginate_by_user_with_metadata( current_user, params )
	
			@notes, @metadata, @tags = results[:collection], results[:metadata], results[:tags]

			response_for @notes, {:metadata => @metadata }.merge( params )
			return
		end
		
		# Public users may not access a listing of notes
		
		resource_access_denied :status => :forbidden
	end


	# GET /notes/1
	# GET /sounds/1/notes/1
 
	def show
		# NOTE: Even though this is asking for in_set.. it should not recieve sets of notes since that is handled in the routes
		# in_set is used here to find the id..
		
		# You may have access to the requested parents (checked by the parent permissions), 
		# but the parents may not have this note as a child!
		
		if requested_through_parents?
			@note = Note.with_parent( params ).in_set( params[:id] ).first

			# parents might be accessible, but are not the parent of this note
			if @note.blank?
				resource_access_denied(:status => :precondition_failed)
				return
			else			
				response_for( @note, params )	
				return
			end
		end
		
		# If you are logged in and the note is yours or is accessible, 
		# you can see it without a parent... why not
		
		if logged_in?
			@note = Note.readable_by( current_user ).in_set( params[:id] ).first

			response_for( @note, params )
			return
		end
		
		# Public users may not view a note on its own since there is no way to verify permissions
		
		resource_access_denied :status => :forbidden
	end


	# GET /note/new

	def new
		@note = Note.new

		response_for( @note, params )
	end


	# POST /note

	#	 curl 'http://localhost:3000/sounds/{1,2}/notes' -X POST -H 'Content-Type: application/xml'	-d '
	# 
	#	 <note>
	#		 <title>A test note</name>
	#		 <body>Hello Nurse</channels>
	#	 </note>
	#	 '

	def create
		@notes = []
		updateable_parents = @parents.find_all{ |parent| parent.updatable_by?(current_user) }
		
		updateable_parents.each do |parent|		
			@notes << parent.add_notes_by_user( params[:note], current_user )
		end
		
		@notes.flatten!
		
		if @notes.blank?
			respond_to do |format|
				format.html { 
					flash[:error] = "Nothing was added"
					redirect_to polymorphic_url(writeable_parents)
				}
				format.xml	{ render :xml =>	@notes, :status => :bad_request }
				format.js	 { render :json => @notes, :status => :bad_request }
				format.json { render :json => @notes, :status => :bad_request }		
			end
		else
			# try to save the note
			respond_to do |format|
				format.html { 
					flash[:notice] = "The following were successfully created and added: \"#{@notes.map{|n| n.title}.join(',')}\""
					# redirect_to "/#{@parent_class.table_name.pluralize}/{#{writeable_parents.map{|p|p.id}.join(',')}}/notes"
				}
				format.xml	{ render :xml => @notes,	:status => :created, :location => @notes }
				format.js	 { render :json => @notes, :status => :created, :location => @notes }
				format.json { render :json => @notes, :status => :created, :location => @notes }
			end
		end
		# if @notes.blank?
	end


	# PUT /note/1

	def update
		@note = Sound.find(params[:id])

		respond_to do |format|
			if @note.update_attributes(params[:note])
				format.xml { head :ok }
				format.js	{ head :ok }
			else
				format.xml { render :xml	=> @note.errors, :status => :unprocessable_entity }
				format.js	{ render :json => @note.errors, :status => :unprocessable_entity }
			end
		end
	end


	# DELETE /note/1

	def destroy
		@note = Sound.in_set(params[:id])

		@notes.each do |log|
			begin
				log.destroy
			rescue Exception => e
				logger.error e
			end
		end

		respond_to do |format|
			format.xml { head :ok }
			format.js	{ head :ok }
		end
	end


end