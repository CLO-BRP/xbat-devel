class ProjectsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required, :except => [:index, :show]
	before_filter :has_permission?
	# Example standard URIs
	# GET /projects
	# GET /projects.(html|xml|js|json)
	# GET /projects/{1}
	# GET /projects/{1-10, 15}
	# GET /projects.xml?public=1&page=1&order=name

	# Example Nested URIs
	# GET /users/1/projects
	# GET /users/{1,3,5-10}/projects
	 
	def index		
		# see named_scope_helper.rb for paginate_by_user_with_metadata	
 		results = Project.paginate_by_user_with_metadata( @user, params )
		
		@projects = results[:collection]
		@metadata = results[:metadata]
		@tags     = results[:tags]
		
		# see application.rb for response_for_resource
		response_for( @projects, {:metadata => @metadata}.merge(params) )
	end
	

	# GET /projects/1
	# GET /projects/1.(html|xml|js|json)
	
	def show
		@project = Project.find(params[:id]) unless @project # loaded by check_permissions

		response_for( @project, params )
	end
	

	# GET /project/new
	# GET /project/new.(html|xml|js|json)
	
	def new
		@project = Project.new

		response_for( @project, params )
	end

	
	# GET /project/1/edit
	
	def edit
		@project = Project.find(params[:id]) unless @project # loaded by check_permissions
		respond_to do |format|
			format.html
			# is this method really needed in other formats??
		end
	end

	
	# POST /project
	# POST /project.(html|xml|js|json)
	
	def create
		@project = Project.new(params[:project])
		
		@project.user_id = current_user.id
				
		@project.attach_images(params[:images])
		
		respond_to do |format|
			if @project.save
				@project.add_users(current_user, 'Administrator')
				
				flash[:notice] = "Project \"#{@project.name}\" was successfully created."
				
				format.html { redirect_to(projects_path) }
				format.xml	{ render :xml => @project, :status => :created, :location => @project }
				format.js	 	{ render :json => @project, :status => :created, :location => @project	}
				format.json { render :json => @project, :status => :created, :location => @project	}
			else
				format.html { render :action => "new" }
				format.xml	{ render :xml => @project.errors, :status => :unprocessable_entity }
				format.js	 	{ render :json => @project.errors, :status => :unprocessable_entity }
				format.json { render :json => @project.errors, :status => :unprocessable_entity }
			end
		end
	end


	# PUT /project/1
	# PUT /project/1.xml
	# PUT /projects/{1,2,3}

	# Projects can also be updated in batch, that is, update params are applied to many projects simultaneously.
	
	def update
		
		# We first indiscriminately get the projects requested as a set,
		# which are later filtered
		
		@projects = Project.in_set(params[:id])
		
		# Update the project assets if they are sent -
		# This includes the adding or removing of sounds, logs, images, and soon, documents
		
		projects = @projects.find_all{ |project| project.assets_updatable_by?(current_user) }
		
		projects.each do |project|
			project.attach_images(params[:images])
			project.update_assets(params)
			project.update_attribute(:tag_list, params[:project][:tag_list] ) if params[:project] && params[:project][:tag_list]
		end
		
		# So now, update project memberships and the projects themselves (name etc...)
		
		projects = @projects.find_all{ |project| project.updatable_by?(current_user) }

		projects.each do |project|
			project.update_memberships( params )

			# The following will raise an exception if there are errors.. 
			# so beware and maybe catch this in the future
			project.update_attributes!( params[:project] )
		end
		
		respond_to do |format|
			format.html {
				redirect_to(@projects.size > 1) ? "/projects/{#{@projects.map{|p| p.id}.join(',')}}" : project_url(@projects)
			}
			format.xml  { render :xml	=> @projects, :status => :ok }
			format.js	  { render :json => @projects, :status => :ok }
			format.json { render :json => @projects, :status => :ok }
		end
		
	end

	# DELETE /project/1
	# DELETE /project/1.xml
	# DELETE /projects/{1,2,3}

	# Projects may be delete one at a time or in sets (though I'm not sure why you would want to delete a set of projects).
	
	def destroy
		@projects = Project.in_set(params[:id])
		
		@projects.each do |project|
			begin
				project.destroy
			rescue Exception => e
				logger.error e
			end
		end

		respond_to do |format|
			format.html { redirect_to projects_url }
			format.xml	{ head :ok }
			format.js		{ head :ok }
			format.json { head :ok }
		end
	end	

	 
end
