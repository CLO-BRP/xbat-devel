# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
require 'aasm'

class ApplicationController < ActionController::Base
	#include SetNotationHelper
	include AuthenticatedSystem
	include ServerStatistics

	before_filter :set_user
	before_filter :set_current_user # REMOVE THIS EVENTUALLY!
		
	before_filter :use_layout?

	session :cookie_only => false

	# See ActionController::RequestForgeryProtection for details
	# Uncomment the :secret if you're not using the cookie session store
	# protect_from_forgery # :secret => '42a566434328fc0b0f8c21c4029d8bea'
 
	# See ActionController::Base for details 
	# Uncomment this to filter the contents of submitted sensitive data parameters
	# from your application log (in this case, all fields with names like "password"). 
	# filter_parameter_logging :password

	# this may soon be unneeded
	def use_layout?
		@use_layout = (params[:layout] and params[:layout] == "false") ? false : true
	end

 
protected  

	# The following 2 methods are used in polymorphic routes as a convenience for accessing the parent object.
	def parent_resource_class
	  @parent_class = (params[:type]) ? params[:type].classify.constantize : nil
	end  
	alias :parent_class :parent_resource_class

	def parent_id
		@parent_id = (parent_resource_class) ? params["#{ parent_resource_class.to_s.downcase }_id"] : nil
	end
	
	def set_user
		@user = (logged_in? && params[:public].blank?) ? current_user : nil
	end
	
	# NOTE: this method lets models know about current user which is helpful for finders
	# THIS METHOD IS DEPRECATED... PLEASE REMOVE!
	def set_current_user
		# Sound.current_user     = self.current_user
		Tagging.current_user   = self.current_user
		Recording.current_user = self.current_user
		Rating.current_user    = self.current_user # acts_as_rateable model
		Image.current_user     = self.current_user
	end


	# This is a trickly little function.
	# The main trickiness is in the fact that params come in from routes to the controller as 
	# a HashWithIndifferentAccess - though the params are keyed by String.
	# 
	# In most of the controllers that we use this function it is called in this form:
	# 	response_for( @sounds, {:metadata => @metadata}.merge(params) )
	# which creates a Hash out of the merged params, not a HashWithIndifferentAccess
	# One consequence of this in particular is that the string keys in the params from the controllers are 
	
	def response_for(resource, params = {})

		metadata = params[:metadata]
	  action   = params["action"] || action_name
	  locals	 = params[:locals] || {}
	  layout   = params[:layout] || true

	  resource = resource || {}	# this catches nil values
	  resource_name  = params[:name] || controller_name
	  resource_name  = resource_name.singularize if !resource.is_a?(Enumerable)
	  resource_class = resource_name.classify.constantize

	  includes = sanitize_associations(params["include"], resource_class)
	  except   = sanitize_columns(params["except"], resource_class)		

	  # Finally, lets respond to this thing appropriately

		respond_to do |format|
			
			# HTML
			format.html {
				# HashWithIndifferentAccess is a dangerous beast... beware!
				# sink = {resource_name.to_sym => resource}
				# debug_hash 'sink', sink
				# debug_hash 'locals', locals
				# debug_hash 'sink < locals', sink.merge(locals)
				# debug_hash 'locals < sink', locals.merge(sink)
				render_action_or_partial(
					:locals => {resource_name.to_sym => resource}.merge(locals),
					:action => action,
					:layout => layout
				)
			} 
			
			# XML
			format.xml { 
				if resource.is_a?( Enumerable)
					render :text => { 
						resource_name.to_sym => resource, :metadata => metadata 
						}.to_xml(
						:root => "result", :include => includes, :except => except
					)
				else
					render :text => resource.to_xml(:include => includes, :except => except)
				end
			}
			
			# JS
			format.any(:js, :json) {
				json = { resource_name.to_sym => resource, :metadata => metadata }.to_json( 
					:include => includes, :except => except
				)
				# JSON/P
				json = "#{params['callback']}(#{json})" if params.include?("callback") 

				render :text => json
			}
			
			# RSS 2.0
			format.rss {
				render :type => 'application/rss+xml', :layout => false
			}
			
			# CSV
			format.csv {
				csv_string = FasterCSV.generate do |csv|
					# first reject unwanted columns		
					columns = resource_class.column_names.reject{|c| except.find{|e| e == c.to_sym} }			

					# header row
					csv << columns

					# data rows
					if resource.is_a? Enumerable
						resource.each do |object|
							csv << columns.map{ |column_name| object.send(column_name.to_sym) }
						end
					else
						csv << columns.map{ |column_name| resource.send(column_name.to_sym) }
					end
				end # FasterCSV.generate

				send_data( csv_string, 
					:type => 'text/csv; charset=iso-8859-1; header=present',
					:disposition => "attachment; filename=#{resource_name}.csv"
				)
			}
 
		end 
		# respond_to
  
	end
	# response_for

	# used by response_for
	def sanitize_associations(includes, klass)
		# make sure all includes are symbols or arrays of symbols
		includes = convert_to_array_of_symbols(includes)
		
		includes = if includes.include?(:all)
			klass.associations
		else
			Set.new(includes).intersection( Set.new(klass.associations) )
		end
		
		# return the intersection of the sets - for some reason, to_a is not needed
		return includes
	end

	# used by response_for	
	def sanitize_columns(columns, klass)
		columns = convert_to_array_of_symbols(columns)
		
		# return the intersection of the sets - for some reason, to_a is not needed		
		return Set.new(columns).intersection( Set.new(klass.column_names) )
	end
	
	# naming of this method??
	def convert_to_array_of_symbols(input)
		if input.is_a?(String)
			array = input.split(",") 
			input = array.map{|i| i.strip.to_sym } 
		elsif input.is_a?(Array)
			# presumably an array of strings or symbols
			input = input.map{|i| i.to_sym if i.is_a?(String) }
		end
		# make sure it is an array, since its possible to send in a symbol
		input = [input] if !input.is_a?(Array)
		return input
	end

	# This needs to be reviewed
  def render_action_or_partial(options = {})
		if !params[:partial].blank?
			begin
				render :partial => params[:partial], :locals => options[:locals] 
				return
			rescue ActionView::MissingTemplate => e
				render :text => "Missing '#{params[:partial]}' partial." 
				return
			end
		end

		layout = (options[:layout] and options[:layout] == "false") ? false : true
    
		if options[:action]
			render :action => options[:action], :layout => layout 
			return
		end
    
		render :layout => layout
	end
  
	# This is used in various perimissioning checks like SoundsController and ProjectsController
	def resource_access_denied(options = {})
		status = options[:status] || :forbidden
		url = options[:url] || '/'
		
		respond_to do |format|
			format.html { 
				redirect_to(url)
			}
			format.xml { 
				render :text => status.to_s.humanize, :status => status 
			}
			format.js  { 
				render :text => status.to_s.humanize, :status => status 
			}
			format.json{ 
				render :text => status.to_s.humanize, :status => status 
			}
			return
		end
		return # this does not always work, depends on context - best to return in the calling function as well
	end


	#--------------------------
	# UTIL
	#--------------------------
	
	def debug_hash(label = 'HASH: ', options = {})
		logger.debug '----------------------------------------------------------------'
		logger.debug ' ' + label + ' (' + options.class.to_s + ')'
		logger.debug '----------------------------------------------------------------'

		options.each{ |key, value|
			logger.debug "#{key} (#{key.class}) => #{value.to_s} #{value.class}"
		}
		
		logger.debug '----------------------------------------------------------------'
		logger.debug ' '
	end
	

end
