# This controller handles the login/logout function of the site.  
class SessionsController < ApplicationController

  # render new.rhtml
  def new
  end

  # If XBAT creates the user (which eventually should not happen on the server side),
	# logging into the system is automatic. That is, when an XBAT created user logs into
	# the application from the Web end for the first time, its UNIX user account must be
	# created and a temporary password is issued. 
	
  def create
    logout_keeping_session!
    
    user = User.find_by_login(params[:login])
		
		# XBAT created users only
		
    if !user.nil? and user.crypted_password.nil?
			password = "password"
      user.password = password
      user.password_confirmation = password

  		user.save
      user.register!
      user.activate!
			user.establish_unix_account(password)
			
      params[:password] = "password"
      flash[:notice] = "You have been given the password of '#{user.password}'. Please change this!"
    end
    
    user = User.authenticate(params[:login], params[:password])
    
    if user
      # Protects against session fixation attacks, causes request forgery
      # protection if user resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset_session
      self.current_user = user
      new_cookie_flag = (params[:remember_me] == "1")
      handle_remember_cookie! new_cookie_flag
      flash[:notice] = "You have been logged in." if flash[:notice].blank?
      redirect_back_or_default('/')
      return
    else
      note_failed_signin
      @login       = params[:login]
      @remember_me = params[:remember_me]
      render :action => 'new'
    end
  end

  def destroy
    logout_killing_session!
    flash[:notice] = "You have been logged out."
    redirect_back_or_default('/')
  end

protected
  # Track failed login attempts
  def note_failed_signin
    flash[:error] = "Couldn't log you in as '#{params[:login]}'"
    logger.warn "Failed login for '#{params[:login]}' from #{request.remote_ip} at #{Time.now.utc}"
  end
end
