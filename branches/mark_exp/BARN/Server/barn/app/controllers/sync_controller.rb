require 'hpricot'
require 'open-uri'
require 'base64'
require 'asset_bundle'

class SyncController < ApplicationController
	# include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:pull_responder, :push_responder]
	# before_filter :has_permission?
	
	before_filter :sync_login_required, :only => [:pull_responder, :push_responder]
	before_filter :validate_request, :only => [:pull_requester, :push_requester]

	before_filter :log_sync_request, :only => [:pull_requester, :pull_responder, :push_responder]
	
	# we need some methods from here
	include ApplicationHelper
	
	def index
		# nothing to see here yet
	end

	#---------------
	# pull_requester
	#---------------
	
	# GET /sync/request
	#
	# Initiate a sync from a browser request to another server.
	# Valid params["sync"]["host"], params["sync"]["login"], params["sync"]["password"] are required.
		
	def pull_requester
		start = Time.now		
		pretending = !params[:pretend].blank? 
		
		# Initiate the request to the remote server.
		# Catch HTTPErrors and return them to the client.
		begin
			doc = Hpricot(
				open("http://#{ @host }/sync", {
					:http_basic_authentication => [@login, @password],
					"User-Agent" => "BARN Server (#{$svn_revision})"
				})
			)
		rescue OpenURI::HTTPError => e
			text = "The remote server could not authorize you." if e.to_s == "403 Forbidden"
			text = "There was an error on the remote server." if e.to_s == "500 Internal Server Error"
			render :text => "ERROR: HTTP #{e} \n\n #{text}", :layout => false
			return
		end
		
		# Parse and save all the resources
		
		sounds = parse_resource_xml(doc,
			:class 		=> Sound, 
			:selector => "sound", 
			:pretend 	=> pretending
		)
		
		# Get the asset bundles and retrieve the remote files
				
		doc.search("asset-bundles > asset-bundle").each do |asset_bundle|
			asset_bundle = AssetBundle.new.from_xml(asset_bundle.to_html)
			# Saving an AssetBundle fires off a remote request... 
			unless asset_bundle.retrieve(:host => @host)
				barn_logger("ERROR: Could not save the AssetBundle #{asset_bundle.inspect}!", :error)
			end
		end unless pretending
		
		barn_logger("SyncController#pull_requester finished in #{Time.now - start} seconds.")
				
		render :text => doc.to_html, :layout => false
	end
		
	#---------------
	# pull_responder
	#---------------
	
	# GET /sync
	
	# Respond to a pull request from another BARN server, *not* a browser
	
	def pull_responder
		start = Time.now
		# a little setup
		@asset_bundles 	= []
		date		= (@last_pull.nil?)? @last_pull.created_at : Time.utc(2000, "jan", 1, 20, 15, 1)
		
		# Get the resources to dump, for the moment, its only sounds
		@sounds = Sound.find_for_sync(current_user, date)
		
		# Create the asset bundles for our resource dump
		@sounds.each do |sound|
			sound.events.each do |event|
				@asset_bundles << AssetBundle.new.from_resource(event)
			end
		end
		
		# Prepare the metadata for the XML payload
		@metadata = {
			:last_pulled => (@last_pull)? @last_pull.created_at : nil,
			:last_pushed => (@last_push)? @last_push.created_at : nil,
			:remote_ip_address => local_ip,
			:request_ip_address => request.headers["REMOTE_ADDR"]
		}

		barn_logger("SyncController#pull_responder finished in #{Time.now - start} seconds.")

		render :template => "sounds/dump.xml.builder", :layout => false
	end
	
	
	# PUT /sync/request
		
	def push_requester
		render :text => "pull_requester method not yet implemented.", :layout => false
	end
	
	# PUT /sync
	
	# For map.connect, methods are not well enforced like they are for resources
	# we protect this one because it modifies data.
	
	def push_responder
		if !request.put?
			render :text => "Method Not Allowed.\n", :status => :method_not_allowed
			return
		end
		
		render :text => "#{request.headers.to_xml(:root => 'HTTP-HEADERS')}", :template => false
	end
	
protected
	
	#---------------
	# sync_login_required
	#---------------
	
	def sync_login_required
		unless authorized?
			render( :text => "Could not authorize you.", :status => 403)
			return
		end
	end
	
	#---------------
	# log_sync_request
	#---------------

	def log_sync_request(action = "")
		# get a peer if it exists, otherwise
		@peer = Barn.find_or_create_by_host(request.headers["REMOTE_ADDR"])
		
		@peer_user = PeerUser.find(:first, :conditions => ["peer_id = ? AND user_id = ?", @peer.id, current_user.id])
		@peer_user = PeerUser.create(:peer_id => @peer.id, :user_id => current_user.id) if @peer_user.blank? 
		
		@sync = Sync.create(
			:http_method => request.headers["REQUEST_METHOD"],
			:http_user_agent => request.headers["HTTP_USER_AGENT"],
			:http_request_headers => request.headers.to_xml(:root => 'HTTP-HEADERS'),
			:peer_user_id => @peer_user.id
		)
		
		@last_pull = Sync.find(:first, :conditions => ["peer_user_id = ? && http_method = 'GET'", @peer_user.id], :order => "created_at ASC")
		@last_push = Sync.find(:first, :conditions => ["peer_user_id = ? && http_method = 'PUT'", @peer_user.id], :order => "created_at ASC")
	end
	
	#---------------
	# validate_request
	#---------------
	
	def validate_request
		@host, @login, @password = params["sync"]["host"], params["sync"]["login"], params["sync"]["password"] 
		
		# check for necessary params	
		@error = ""
		@error << " Host not specified. " if @host.blank?
		@error << " Login not specified. " if @login.blank?
		@error << " Password not specified. " if @password.blank?
		
		if !@error.blank?
			render :text => "ERROR: #{@error}", :layout => false
			return
		end
	end
	
	#---------------
	# parse_resource_xml
	#---------------
	
	# This really needs to be factored out into some seperate methods... or classes
	
	def parse_resource_xml(doc, options)
 		# make sure we are dealing with the correct input
		doc = Hpricot(doc) unless doc.class.to_s.include?('Hpricot')

		klass 	 	= options[:class]
		selector 	= options[:selector]
		resources	= []
		pretending = options[:pretend] || false
		
		doc.search( selector ).each do |element|
			# An assumption is made here that anything the root of a sync has a guid
			
			guid = element.search("> guid").inner_html
			
			# Find an existing resource to update,
			# or save a new resource if nothing has been found
			
			existing_resource = klass.find_by_guid(guid)
		
			new_resource = klass.new.from_xml <<-XML
				<#{klass.to_s.downcase}> 
					#{ element.search("> *:not([@type='array'])").to_html.strip } 
				</#{klass.to_s.downcase}>")		
			XML
	
			if existing_resource
				resource = merge_resources(existing_resource, new_resource)
			else
				resource = new_resource
			end			
			
			
			unless pretending		
				resource.user_id = current_user.id if resource.respond_to?(:user_id)
				logger.info "Saving resource: #{resource.class.to_s} - #{resource.guid}"
				
				unless resource.save	
					logger.error resource.errors.inspect
				end
				# associate the user
				if resource.class == Event
					ev = EventUser.find(:all, :conditions => ["event_id = ? AND user_id = ?", resource.id, current_user.id])
					EventUser.create(:event_id => resource.id, :user_id => current_user.id) if ev.blank?
				end
			end

			resources << resource
			
			# Now, if the klass in question has syncable items defined, handle them
			
			klass.syncable.each do |child|
				child_node	= child.to_s
				child_class = child_node.classify.constantize
				
				# NOTES
				if child_class == Note
					notes = parse_resource_xml(element, 
						:class => child_class, 
						:selector => "> notes > note", 
						:pretend => 	pretending 
					)	
					# the add_notes method check for duplicate notes and associations					
					# for some reason calling this fucks up the validations of Sound!
					resource.add_notes_by_user(notes, current_user) unless notes.blank? || pretending
					next
				end
					
				# RATINGS
				if child_class == Rating
					rating = element.search("> ratings > rating > rating").inner_html
 					resource.rating = rating unless rating.blank?
					resource.save unless pretending
					next
				end
				
				# TAGS
				if child_class == Tag
					tags = Set.new(resource.tag_list.to_a)
					element.search("> tags > tag > name").each { |name| tags.add name.inner_html }
					resource.tag_list = tags.to_a.join(", ")
					resource.save unless pretending
					next
				end	
				
				# EVERY OTHER ASSOCIATION
				child_resources = parse_resource_xml( element,
					:class => child_class, 
					:selector => "> #{child_node} > #{child_node.singularize}",
					:pretend => pretending
				)	
				
				# make the association if necessary
				child_resources.each { |cr|
					begin
						resource.send(child_node.to_sym).find(cr.id)
					rescue ActiveRecord::RecordNotFound => e								
						resource.send(child_node.to_sym) << cr unless pretending
					end
				}
				
			end if klass.respond_to?(:syncable) # klass.syncable.each
			
		end # doc.search( selector ).each
		
		return resources
	end
	
	#---------------
	# merge_resources
	#---------------
		
	def merge_resources(existing_resource, new_resource)
		resources = [existing_resource, new_resource]
		# We would like to sort by modification date since this is the currency of merge,
		# but some resources do not have modified_at or created_at set
		resources.each { |resource|
			resource.created_at  = Time.now if resource.respond_to?(:created_at) && resource.created_at.nil?
			resource.modified_at = resource.created_at if resource.respond_to?(:modified_at) && resource.modified_at.nil?
		}
		
		# not all objects have a modified_at
		field = (existing_resource.respond_to?(:modified_at))? :modified_at : :created_at
		
		if existing_resource.send(field) < new_resource.send(field)
			existing_resource.attributes = new_resource.attributes
			# existing_resource.save
		end
		
		return existing_resource
	end
	
end