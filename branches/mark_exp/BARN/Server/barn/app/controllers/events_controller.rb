# NOTE: The 'edit' action has been removed since it has not relevance in the context of the api
# additionally, all requests for HTML responses have been removed

class EventsController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:index, :show]
	before_filter :has_permission?
  # GET /events
  
  # TODO: document the family of requests handled here
  
  def index
    
    # user = (logged_in? && params[:public].blank?) ? current_user : nil
		
    results = Event.paginate_by_user_with_metadata( @user, params )
		
    @events 	= results[:collection]
    @metadata = results[:metadata]
    @tags 		= results[:tags]

    response_for( @events, {:metadata => @metadata}.merge(params) )
    
    return
    
    
    # NOTE: the code below, is 'traditional' code for a controller like this

    #--
    # handle include, sorting, and nested resource conditions
    #--
    
    # NOTE: the above are linked to the extent that some sorting requires includes
    
    if params[:include].blank?
      include = Set.new;
      
    else
      include = Set.new params[:include].split(',').map!{|s| s.strip}
      
    end
    
    if params[:order].blank?
      order = nil
      
    elsif params[:order].split('.').size > 1
      order = "#{params[:order]} #{params[:direction]}"
      
    else
      order = "#{Event.table_name}.#{params[:order]} #{params[:direction]}"
      
    end
    
    conditions = {}; joins = {}
    
    if !params[:sound_id].blank?
      conditions = 'log.sound_id =' + params[:sound_id]; include.add 'log'
      
      joins = 'INNER JOIN log ON log.id = event.log_id'
            
    elsif !params[:log_id].blank?
      conditions = 'event.log_id = ' + params[:log_id];
      
    end

    #--
    # find events with pagination
    #--
    
    include = (include.length == 0)? nil : include.to_a
    
    if params[:format] == "csv"
      
      @events = Event.readable_by( current_user || nil ).filtered_by(params).find(
        :all, :order => order, :conditions => conditions, :include => include
      )
       
      @metadata = {
        :total_entries => Event.readable_by( current_user || nil ).filtered_by(params).count(:all, :conditions => conditions, :include => include)
      }
        
    else
            
      # TODO: remove non-associated includes
 
      @events = Event.readable_by( current_user || nil ).filtered_by(params).paginate(
        :page => params[:page],
        :per_page => params[:per_page] || Event.per_page,
        :order => order,
        :conditions => conditions,
        :include => include
      )
            
      @metadata = {
        :page => params[:page],
        :per_page => params[:per_page] || Event.per_page,
        :total_entries => Event.readable_by( current_user || nil ).filtered_by(params).count(:all, :conditions => conditions, :include => include)
      }
                 
      @tags = Event.tag_counts(
        :conditions => conditions,
        :joins => joins
      )

    end
    
    #--
    # respond to request according to format
    #--
    
    if !params[:log_id].blank?
      locals = {
        :log => Log.find(params[:log_id])
      }
    
    elsif !params[:sound_id].blank?
      locals = {
        :sound => Sound.find(params[:sound_id])
      }
      
    end

    response_for(@events, {:metadata => @metadata, :locals => locals}.merge(params))
      
  end


  # GET /events/1

  def show
		@event = Event.find(params[:id])
		
		response_for( @event, params )    
  end


  # GET /events/new
  
  def new
    @event = Event.new

    respond_to do |format|
      format.html { render :text => "I'm a teapot.", :status => 418 }
      format.xml { render :text => @event.to_xml( :dasherize => false ) }
      format.any(:js, :json)  { render :json => @event }
    end
  end

  
  # POST /events
  
  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        # XML
        format.xml do 
          render :xml => @event, :status => :created, :location => @event
        end
        # JS/JSON
        format.any(:js, :json) do 
          render :json => @event, :status => :created, :location => @event 
        end
        format.html { render :text => "I'm a teapot.", :status => :im_a_teapot }
      else
        format.xml { render :xml => @event.errors, :status => :unprocessable_entity }
        format.any(:js, :json){ render :json => @event.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # PUT /events/1
  
  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @event.update_attributes(params[:event])
        format.xml { render :xml => @event }
        format.any(:js, :json) { render :json => @event }
      else
        format.xml { render :xml => @event.errors, :status => :unprocessable_entity }
        format.any(:js, :json) { render :json => @event.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # DELETE /event/1
  
  def destroy
    @events = Event.in_set(params[:id])
    @events.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.xml { head :ok }
      format.any(:js, :json)  { head :ok }
    end
  end
  
end
