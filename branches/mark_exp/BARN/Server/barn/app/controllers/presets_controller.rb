class PresetsController < ApplicationController
  def index
    @presets = ExtensionPreset.find(:all)
    render :layout => false
  end
end