class SoundsController < ApplicationController	
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required, :except => [:index, :show]
	
	before_filter :has_permission?
	
	# GET /sounds
	# GET /sounds.(html|xml|js|json)
	# GET /sounds/{1}
	# GET /sounds/{1-10, 15}
	# GET /sounds.xml?public=1&page=1&order=name

	# GET /users/1/sounds
	# GET /projects/{1,3,5-10}/sounds
	
	def index
		# We do not want to limit the sounds result by user-owned sounds 
		# if we have parentage (through a project presumably)
				
		results = if requested_through_parents?
			Sound.paginate_with_metadata( params )
		else
			Sound.paginate_by_user_with_metadata( @user, params )			
		end
				
		@sounds   = results[:collection]
		@metadata = results[:metadata]
		@tags     = results[:tags]

        @extensions = Extension.find(:all, :conditions => "type = '" + ExtensionType::SOUND_DETECTOR + "'", :order => "name" )

		# see application.rb for response_for
		response_for( @sounds, {:metadata => @metadata}.merge(params) )
	end
	

	# GET /sounds/:id
	
	def show
		@sound = Sound.find(params[:id])
		@logs	 = @sound.logs.paginate(:page => 1) # revisit this

		response_for( @sound, params )
	end


	# GET /sound/new
	
	def new
		@sound = Sound.new

		response_for( @sound, params )
	end
	

	# POST /sound
	
	#	 curl 'http://localhost:3000/sounds' -X POST -H 'Content-Type: application/xml'	-d '
	# 
	#	 <sound>
	#		 <name>A test sound</name>
	#		 <channels>2</channels>
	#		 <samplerate>22050.00</samplerate>
	#		 <duration>12.555</duration>
	#		 <samples>4512679</samples>
	#		 <!-- The user should not be sent with the POST, it should be gathered from the session -->
	#		 <user_id>1</user_id>
	#	 </sound>
	#	 '

	def create
		@sound = Sound.new(params[:sound])
		@sound.user = current_user
		
		# TODO:
		# 1. create a content hash
		# 2. check to see if the sound already exists - if so, only create a UserSound relation.
		
		respond_to do |format|
			if @sound.save
				format.html { redirect_to sound_path(@sound) }
				format.xml	{ render :template => 'sounds/show.xml.builder', :status => :created }
				format.any(:js, :json) { render :json => @sound, :status => :created, :location => @sound }
			else
				format.html {
					flash[:error]	= "Something went wrong."
					render :action => 'new'
				}
				format.xml	{ render :xml => @sound.errors, :status => :unprocessable_entity }
				format.any(:js, :json)	 { render :json => @sound.errors, :status => :unprocessable_entity }
			end
		end
	end


	# GET /sound/:id/edit
	
	def edit
		@sound = Sound.find(params[:id])

		respond_to do |format|
			format.html
			# is this method really needed in other formats??
		end
	end


	# PUT /sound/:id
	# PUT /sound/{:id}
	
	# should we allow the updating of sound sets?
	def update
		@sounds = Sound.in_set(params[:id])
	
		success = false
		@sounds.each do |sound|
			success = sound.update_attributes(params[:sound])
		end
		
		respond_to do |format|
			if success
				# HTML
				format.html {		
					flash[:notice] = "Sound saved sucessfully"
					redirect_to sound_path(@sounds.first) 
				}
				# XML
				format.xml	{ render :template => 'sounds/show.xml.builder', :status => :ok }
				# JSON
				format.js	 { render :json => @sounds, :status => :ok }
				format.json { render :json => @sounds, :status => :ok }
			else 
				format.html { 
					flash[:error] = "The was a problem saving your record"
					render :action => 'edit' # presumably we came from the edit action
				} 
				format.xml	{ render :xml	=> @sounds.map{|s|s.errors}, :status => :unprocessable_entity }
				format.js	 { render :json => @sounds.map{|s|s.errors}, :status => :unprocessable_entity }
				format.json { render :json => @sounds.map{|s|s.errors}, :status => :unprocessable_entity }
			end
		end # respond_to
		
	end # update

	# DELETE /sound/:id
	# DELETE /sound/{:id}
	
	def destroy
		# NOTE: we only destroy the user sound association not the sound resource
		
		@sounds = Sound.in_set(params[:id])
				
		@sounds.each do |sound|
      sound.sound_users.each do |relation|
        relation.destroy if relation.user_id == current_user.id
			end
		end

		respond_to do |format|
			format.html { redirect_to sounds_url }
			format.xml { head :ok }
			format.any(:js, :json)	{ head :ok }
		end
	end
	
	
		
end
