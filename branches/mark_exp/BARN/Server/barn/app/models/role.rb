class Role < ActiveRecord::Base
  #has_many :role_users
  #has_many :users, :through => :role_users
  has_and_belongs_to_many :users
  has_and_belongs_to_many :project_users
  
  def self.default_roles
    %w{ Administrator Member Guest }
  end

	def self.DEFAULTS
		default_roles
	end
end
