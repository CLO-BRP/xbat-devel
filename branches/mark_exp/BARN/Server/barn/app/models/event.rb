class Event < ActiveRecord::Base
  
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	#include AccessControlHelper
	
	#extend SetNotationHelper
	
	logical_parents :project, :sound, :log, :recording
	
	before_save GUIDGenerator.new
  
	#---------------
	# Associations
	#---------------
  
	belongs_to :log
	
	belongs_to :recording
  
	# a fake sound association
	def sound
		log.sound
	end
	
	has_one :event_user
	
	has_one :user, :through => :event_user
	
	# this works too :|
=begin
	has_many :sound, :finder_sql =>
	      'SELECT sound.* ' +
	      'FROM sound ' +
				'INNER JOIN log ON log.sound_id = sound.id ' +
				'INNER JOIN event ON event.log_id = log.id ' +
	      'WHERE log.id = event.log_id AND event.id = #{id}'
=end	

	# Modules
	
	acts_as_rateable
	
	acts_as_taggable  
	
	acts_as_annotated
	
	shareable
	
	#---------------
	# Validations
	#---------------
	
	validates_presence_of :log_id
	
	validates_presence_of :recording_id
  
	#---------------
	# Filters
	#---------------

	before_save GUIDGenerator.new
	
	#---------------------------
	# INSTANCE METHODS
	#---------------------------
	
	attr_reader :measure
	#@measure
	
	def measure
		require 'ostruct'
		@measure = @measure || OpenStruct.new(:keys => [])

		# Here is another option
		# @measure = {}
		return @measure
	end
	
	def find_measure(name)
				
    measure = ('event_measure_' + name.to_s).camelize.constantize
  
    measure.find(:first, :conditions => {:event_id => id})
    
	end
	
	#------------------
	# Named Scopes
	#------------------
	
	# The following method is defined in AccessControlHelper, but are invalid there
	# because events are associated to users via a many-to-many join table, event_user, 
	# but each event has a single owner ... through its recording!
	
	#----------
	# readable_by
	#----------
	
	named_scope :readable_by, lambda { |*args| 
=begin
		if args.nil? || args.first.nil?
			scope = Recording.public.proxy_options
			scope[:include] = nil
			# join events table to sound
			scope[:joins] = "
				LEFT OUTER JOIN #{Recording.table_name} ON #{Recording.table_name}.id = #{Event.table_name}.recording_id 
				LEFT OUTER JOIN #{RecordingSound.table_name} ON #{RecordingSound.table_name}.recording_id = #{Recording.table_name}.id
				LEFT OUTER JOIN #{Sound.table_name} ON #{RecordingSound.table_name}.sound_id = #{Sound.table_name}.id
			"
			return scope
		end
=end		
		
		# Remove this when you figure out what EventUser is not being correctly populated
		return {}
		
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		scope = Log.public.proxy_options
		scope[:include] = [:log, :user]
		scope[:conditions] << " OR #{EventUser.table_name}.user_id = #{user.id}"
		
		return scope 
	}
	
	#----------
	# public
	#----------
	
	# Here publicity is based on the parent Log
	
	named_scope :public, lambda { |*args| 
		scope = Log.public.proxy_options
		scope[:include] = :log
		# TODO: THIS IS WRONG!
		return scope
		# {
		# 			:joins => " INNER JOIN sound ON log.sound_id = sound.id ", 
		# 			:conditions => ["#{Sound.table_name}.public = 1"]
		# 		}
		# raise scope.inspect
	}

	#----------
	# with_parent
	#----------
	
	# This named_scope overwrites the one in NamedScopeHelper because ActiveRecord 
	# cannot handle this relationship: event -> log -> sound 
	# that, is Event belongs_to :sound, :through => :log
	
	named_scope :with_parent, lambda { |*args|
		# Setup variables
		params 	 = args.first || {}
		sound_id = params[:sound_id]
		log_id 	 = params[:log_id]
		
		# Lets not even bother if we dont have the information we need
		return {} if sound_id.blank? && log_id.blank?

		# Please note, this does not handle 'deep nesting'
    if sound_id
      conditions = "log.sound_id = #{sound_id}"
      #joins = 'INNER JOIN log ON log.id = event.log_id'     
    elsif log_id
      conditions = "event.log_id = #{log_id}";
    end
		
		{
			:include => :log,
			:conditions => conditions
		}
		
	}
  
	# LEGACY - STILL USED
	
	def self.find_by_set_notation(params)
		
		sound_id  = params[:sound_id]
    log_id    = params[:log_id]
    order     = params[:order] || "created_at"
    direction = params[:direction] || "asc"
    sort      = "#{order} #{direction}"
    @metadata = {}
    @conditions = ""
    @messages = {}
    set_limit = 100

    unless is_set_or_range?(params[:id])
      return Event.find(params[:id]), @metadata
    end

    # Associations

    if sound_id
      log_ids    = Sound.find(sound_id, :include => :logs).logs.collect{|log| log.id}
      # this should not ever be the case, but we catch it just in case we return empty handed
      if log_ids.size == 0 or log_ids.nil?
        return [], @metadata
      end
      @conditions = (log_ids.size > 1) ? "log_id in (#{log_ids.join(',')})" : "log_id = #{log_ids}"
    end

    if log_id
      @conditions = "log_id = #{log_id}"
    end

    # Handle Sets and Ranges

    if is_range?(params[:id])
      @limit, @offset = convert_range_to_limits(params[:id])
    end

    if is_set?(params[:id])
      set = parse_set(params[:id])
			set = set.to_a.join(',')
			
      @conditions << "#{" AND" unless @conditions.empty?} event.id IN (#{set})"  
      @limit = @offset = nil # these are not used
    end

    # Now that conditions are set, retrieve the events

    if order == 'rating'
      # Ratings are a slightly special case.
      @events = Event.find_by_sql( <<-SQL
        SELECT event.*, avg(rating.rating) AS rating
        FROM event
        LEFT JOIN rating ON event.id = rating.rateable_id
        WHERE (rating.rateable_type = lower('Event')  OR rating.rateable_type IS NULL)
        #{"AND #{@conditions}" if !@conditions.empty?}
        GROUP BY event.id
        ORDER BY rating #{direction}
        #{"LIMIT #{@offset}, #{@limit}" unless is_set?(params[:id])}
        SQL
      )
    else
      @events = Event.find(:all,
        :limit  => @limit,
        :offset => @offset, 
        :order => sort, 
        :conditions => @conditions, 
        :include => ['log', 'ratings', 'tags']
      )
    end


    @metadata = {
        :total_entries => Event.count(:conditions => @conditions),
        :score_min     => Event.minimum(:score, :conditions => @conditions),
        :score_max     => Event.maximum(:score, :conditions => @conditions),
        :score_mean    => Event.average(:score, :conditions => @conditions),
        :messages      => @messages
    }

    return @events, @metadata
  end


end
