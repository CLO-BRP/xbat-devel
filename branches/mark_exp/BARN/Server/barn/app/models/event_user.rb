# This model implies that a user has_many events, and a event also has_many users, but this is not really the case.
# A user has_one event, and an event belongs_to a user ... it would be better if the event table had a user_id

class EventUser < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
end