class ProjectAsset < ActiveRecord::Base
	belongs_to :project
  
	belongs_to :asset, :polymorphic => true
  
	acts_as_taggable
	
	acts_as_annotated
	
	acts_as_rateable
	
	# NOTE: we make sure to not add the same asset to a project multiple times
  
	# NOTE: this will actually just throw an exception since its happening on an implicit record
	
	validates_uniqueness_of :asset_id, :scope => [:project_id, :asset_type]
end