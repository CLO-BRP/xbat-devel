class Sound < ActiveRecord::Base
	include AuthorizationApocalypse::Keymaster
	include WillPaginateHelper
	include NamedScopeHelper
	include Syncable
	
	# Modules (plugins)
	acts_as_rateable
	acts_as_taggable
	acts_as_annotated
	shareable
	
	logical_parents :project
	
	syncs :recordings, :logs, :notes, :tags, :ratings

	#---------------
	# Callbacks
	#---------------
	
	before_save GUIDGenerator.new
	
	after_create do |sound|
		# Add an entry in the sound_user table
		SoundUser.create(:user_id => sound.user_id, :sound_id => sound.id)
	end
	
	#---------------
	# Associations
	#---------------

	belongs_to :user
	belongs_to :owner, :foreign_key => "user_id", :class_name => "User"
	
	has_many :sound_users
	has_many :users, :through => :sound_users
	
	has_many :logs
	
	# has_many :events, :through => :logs
	
	# This created an events method on a sound instance 
	# where otherwise there would be no *direct* association.
	# In essence, this preforms a complex join on the tables but in Ruby rather than SQL
	
	def events
		logs.collect{ |log| log.events }.flatten
	end
	
	# Recordings association	
	has_many :recording_sounds
	has_many :recordings, :through => :recording_sounds
	
	# Projects association, but a Sound could theoretically be attached to anything
	has_many :project_assets, :as => :asset#, :dependent => :destroy
	has_many :projects, :through => :project_assets#, :source => :attachable, :source_type => "Project", :class_name => "Project"
 
  
	#------------------
	# Named Scopes
	#------------------
	
	# this one might be redundant
	named_scope :owned_by, lambda { |*args| {:conditions => ["#{table_name}.user_id = ?", args.first ]} }
	
	named_scope :readable_by, lambda { |*args| 
		return {} if args.nil? || args.first.nil?
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		{
			:include => [:user], 
			:conditions => ["#{table_name}.user_id = ?", user.id ]
		} 
	}
end
