class Log < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	include WillPaginateHelper
	include NamedScopeHelper
	include Syncable
	
	# Modules	
	acts_as_rateable
	acts_as_taggable
	acts_as_annotated
	shareable
	
	logical_parents :project, :sound
	
	syncs :events, :notes, :tags, :ratings
	
	before_save GUIDGenerator.new
	
	#---------------
	# Associations
	#---------------
	
	belongs_to :user
	belongs_to :sound
	has_many   :events
	
	# Projects association, but a Sound could theoretically be attached to anything
	has_many :project_assets, :as => :asset
	has_many :projects, :through => :project_assets
		
	#---------------
	# Validations
	#---------------
	
	validates_presence_of :name
	validates_presence_of :user_id
	validates_presence_of :sound_id	
	
  
	#------------------
	# Named Scopes
	#------------------
	
	named_scope :readable_by, lambda { |*args| 
		return {} if args.nil? || args.first.nil?
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		{
			:include => [:user], 
			:conditions => ["#{table_name}.user_id = ?", user.id ]
		} 
	}
	
end
