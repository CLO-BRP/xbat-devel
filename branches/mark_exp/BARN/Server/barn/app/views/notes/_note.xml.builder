xml.note {
  xml.id(note.id, :type => 'integer')
  xml.title(note.title)
  xml.body(note.body)
  xml.content_hash(note.content_hash)
  
  if local_assigns[:include]
    # User association (there should ALWAYS be a user association)
    if include.find{|association| association == :user}
      xml << render(:partial => 'users/user', :locals => {:user => note.user})
    end
  end
  
  xml.created_at(note.created_at, :type => 'timestamp')
}