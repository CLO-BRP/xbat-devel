
# This template hands either a single note from the 'show' action or multiple notes
# via the index action and show action called with a set notation

xml.instruct! :xml, :version=>"1.0"

# Notes can come into this template as either a single event object or a collection of notes
unless @notes.is_a?(Enumerable)
  # Render a single note
  (@notes.nil?) ? xml.note : xml << render(:partial => 'note', :locals => {:note => @notes})
else
  # Render an array of notes
  xml.tag!(:notes, :type => 'array') {
    # METADATA
    if @metadata.nil?
      xml.tag!(:metadata) # an empty tag
    else
      @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :builder => xml, :dasherize => false)
    end
    # NOTES
    # This notation iterates over the events collection rendering the partial for each event
    xml << render(:partial => 'note', :collection => @notes)
  }
end