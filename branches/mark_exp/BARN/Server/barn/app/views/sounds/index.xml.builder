xml.instruct! :xml, :version=>"1.0"

# NOTE: a single sound is represented in a simple way, a set of sounds is a result set packed with some metadata

xml.tag!(:result) {
  # metadata
  @metadata = {} if @metadata.blank?
  xml << @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :dasherize => false)
  
  #data    
  xml.tag!(:sounds, :type => 'array') {
    xml << render(:partial => 'sound', :collection => @sounds)
  }
}
