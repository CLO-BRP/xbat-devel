# The following XML Builder template was distilled from this original code:

# sounds = @sounds.to_xml(:dasherize => false) do |xml|
#   @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :builder => xml, :dasherize => false)
# end
# render :text =>  sounds

# This template hands either a single sound from the 'show' action or multiple sounds
# via the index action and show action called with a set notation

xml.instruct! :xml, :version=>"1.0"

# NOTE: a single sound is represented in a simple way, a set of sounds is a result set packed with some metadata


xml << render(:partial => 'sound', :locals => {:sound => @sounds})
