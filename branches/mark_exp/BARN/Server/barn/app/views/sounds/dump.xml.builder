# The following XML Builder template was distilled from this original code:

# events = @events.to_xml(:except => :rating, :dasherize => false, :include => {:tags => {:only => ['name', 'id']}}, :methods => :rating) do |xml|
#   @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :builder => xml, :dasherize => false)
# end

# By coding this by hand, we have far more control over the format of the XML

# This template hands either a single event from the 'show' action or multiple events
# via the index action and show action called with a set notation

xml.instruct! :xml, :version=>"1.0"

xml.tag!(:result) {	
	xml << @metadata.to_xml(:skip_instruct => true, :root => 'metadata')
	xml << @asset_bundles.to_xml(:skip_instruct => true, :root => 'asset-bundles')
	xml << @sounds.to_xml(:skip_instruct => true, :include => Sound.sync_includes_hash)
}

