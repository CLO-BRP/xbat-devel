xml.user {
  xml.id(user.id, :type => 'integer')
  xml.email(user.email)
}