xml.log {
  xml.id(log.id, :type => 'integer')
  xml.name(log.name)
  
  # rating
  rating = log.rating_by_user(@user)
  if !rating.blank?
    xml.user_rating(rating, :type => 'float')
    xml.user_rating_id(rating.id, :type => 'integer')
  else
    xml.user_rating(nil, :type => 'float')  
    xml.user_rating_id(nil, :type => 'integer')
  end
  
  xml.average_rating(log.rating, :type => 'float')
  xml.number_of_ratings(log.ratings.size, :type => 'float')
  
  xml.tag_list(log.tag_list, :type => 'string')
  xml.created_at(log.created_at, :type => 'timestamp')
  xml.modified_at(log.modified_at, :type => 'timestamp')
}

  # ASSOCIATIONS
  
  #if local_assigns[:include]
  #  # User association (there should ALWAYS be a user association)
  #  if include.find{|association| association == :user}
  #    xml << render(:partial => 'users/user', :locals => {:user => log.user})
  #  end
  #  # Sound association 
  #  if include.find{|association| association == :sounds}    
  #    xml.sounds do
  #      xml << render(:partial => 'sounds/sound', :collection => log.sounds)
  #    end
  #  end
  #end
  
  # NOTE: the use of zero as a default value currently helps with the view code, but is wrong
