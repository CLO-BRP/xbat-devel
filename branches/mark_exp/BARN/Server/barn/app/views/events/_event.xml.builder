# xml.tag!(:event) == xml.event
xml.event {
  
  # TODO: this is the provenance and book-keeping information, we want sound info and more 'perma' type information through the use of 'guid'
  
  xml.id(event.id, :type => 'integer')
  xml.log_id(event.log_id, :type => 'integer')
  
  xml.created_at(event.created_at, :type => 'timestamp')
  xml.modified_at(event.modified_at, :type => 'timestamp')
  
  #--
  # basic event info
  #--
  
  # NOTE: this is the box
  
  xml.start(event.start, :type => 'float')
  xml.duration(event.duration, :type => 'float')
  xml.low(event.low, :type => 'float')
  xml.high(event.high, :type => 'float')
  
  # NOTE: the channel is frequently irrelevant, for all single-channel sounds
  
  xml.channel(event.channel, :type => 'integer')
  
  #--
  # annotation information
  #--
 
  rating = event.rating_by_user(@user)
  
  if !rating.blank?
    xml.user_rating(rating, :type => 'float')
    xml.user_rating_id(rating.id, :type => 'integer')
  else
    xml.user_rating(nil, :type => 'float')  
    xml.user_rating_id(nil, :type => 'integer')
  end
  
  xml.rating(event.rating, :type => 'float')
  xml.average_rating(event.rating, :type => 'float')
  xml.number_of_ratings(event.ratings.size, :type => 'float')  
  
  # NOTE: the score may not be available in the case of manually logged events
  
  xml.score(event.score, :type => 'float')
  
  # TODO: tag information is richer perhaps, like the rating information
  
  xml.tag_list(event.tag_list, :type => 'string')
  
  #--
  # rendered resource information
  #--
  
  # TODO: here we have links to the images and clips
  
}