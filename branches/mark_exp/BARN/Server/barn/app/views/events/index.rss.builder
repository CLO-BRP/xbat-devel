xml.instruct!

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do

    xml.title       "#{$server_domain_name || local_ip}: Events"
    xml.link        url_for :only_path => false, :controller => 'events'
    xml.pubDate     CGI.rfc1123_date(@events.first.created_at) if !@events.blank? && !@events.first.created_at.blank?
    xml.description "Events recently added to BARN"

    @events.each do |event|
      xml.item do
        xml.title       "#{event.guid}"
        xml.link        url_for :only_path => false, :controller => 'events', :action => 'show', :id => event.id
        xml.description "#{event.log.name if event.log} #{event.start}, #{sec_to_clock(event.duration)}, #{event.low} Hz, #{event.high} Hz, score:#{event.score}<br />#{event.guid}"
        xml.pubDate     CGI.rfc1123_date event.created_at if !event.created_at.blank?
        xml.guid        url_for :only_path => false, :controller => 'events', :action => 'show', :id => event.guid
      end
    end

  end
end
