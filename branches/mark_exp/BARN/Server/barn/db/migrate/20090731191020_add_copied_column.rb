class AddCopiedColumn < ActiveRecord::Migration
  def self.up
		add_column :data_file, :copied, :boolean, :default => false
  end

  def self.down
		remove_column :data_file, :copied
  end
end
