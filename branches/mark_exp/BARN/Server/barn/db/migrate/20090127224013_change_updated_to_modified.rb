class ChangeUpdatedToModified < ActiveRecord::Migration
  @tables = %w{ sound log rating recording recording_sound role role_user user}
  
  def self.up
    remove_column :page, :updated_at
    @tables.each do |table|
      rename_column table, :updated_at, :modified_at
    end
  end

  def self.down
    @tables.each do |table|
      rename_column table, :modified_at, :updated_at
    end
    add_column :page, :modified_at
  end
end
