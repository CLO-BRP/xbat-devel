class CreateTask < ActiveRecord::Migration
  
  def self.up
    create_table :task do |t|
      t.string :name
      t.string :description
      t.text :task, :null => false
      t.integer :priority
      t.integer :user_id, :null => false
      t.integer :project_id
      t.datetime :started_at
      t.string :worker
      t.float :progress
      t.text :report
      t.datetime :last_update
      t.datetime :completed_at
      t.datetime :created_at, :null => false
      t.datetime :modified_at
    end
  end

  def self.down
    drop_table :task
  end
  
end

