class AddPublicFlagToLog < ActiveRecord::Migration
  def self.up
		add_column :log, :public, :boolean, :default => true
  end

  def self.down
		remove_column :log, :public
  end
end
