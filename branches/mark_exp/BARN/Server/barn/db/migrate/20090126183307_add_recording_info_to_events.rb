class AddRecordingInfoToEvents < ActiveRecord::Migration
  def self.up
    add_column :event, :recording_id, :integer
    add_column :event, :recording_start, :float # in time
  end

  def self.down
    remove_column :event, :recording_start
    remove_column :event, :recording_id
  end
end
