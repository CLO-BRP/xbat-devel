class CreateRoles < ActiveRecord::Migration
  def self.up
    begin drop_table "role" rescue true end
    create_table :role do |t|
      t.column :name, :string
      t.timestamps
    end
    
    begin drop_table "role_user" rescue true end
    create_table :role_user do |t|
      t.column :role_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.timestamps
    end
    
    Role.create(:name => 'Administrator')
    
    admin_role = Role.find_by_name('Administrator')
    admin = User.find_by_login('default')
    admin.roles << admin_role
  end

  def self.down
    drop_table :role_user
    drop_table :role
  end
end
