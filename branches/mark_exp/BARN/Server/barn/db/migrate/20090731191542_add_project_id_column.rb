class AddProjectIdColumn < ActiveRecord::Migration
  def self.up
		add_column :data_file, :project_id, :integer 
  end

  def self.down
		remove_column :data_file, :project_id
  end
end
