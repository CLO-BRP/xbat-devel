class CreateProjectAssets < ActiveRecord::Migration
  def self.up
    begin drop_table "project_asset" rescue true end

    create_table :project_asset do |t|
      t.column :project_id, :integer
      t.column :asset_id, :integer
      t.column :asset_type, :string
      t.column :created_at, :datetime
      t.column :modified_at, :datetime
    end
  end

  def self.down
    drop_table :project_asset
  end
end
