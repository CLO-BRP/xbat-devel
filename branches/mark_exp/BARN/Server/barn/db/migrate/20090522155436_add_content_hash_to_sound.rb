class AddContentHashToSound < ActiveRecord::Migration
  def self.up
    add_column :sound, :content_hash, :string
  end

  def self.down
    remove_column :sound, :content_hash
  end
end
