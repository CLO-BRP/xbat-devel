class CreateSoundsAndRecordings < ActiveRecord::Migration
  def self.up
    begin drop_table "sound" rescue true end
    # Sounds
    create_table :sound do |t|
      t.column :name,     :string
      t.column :user_id,  :integer
      
      # These are summary properties, computed from 'recordings'
      # these leave open the possibility for mixed formats and for dropping and adding files
      t.column :channels,  :integer
      t.column :samplerate,  :float
      # these limit the adding and dropping of files
      t.column :duration,  :float
      t.column :samples,  :integer
      
      t.timestamps
    end
    
    # Recordings
    create_table :recording do |t|
      t.column :name, :string
      t.column :file, :string
      t.column :date, :date
      t.column :bytes, :integer
      t.column :format, :string
      t.column :channels, :integer
      t.column :samplerate, :float
      t.column :samplesize, :integer
      t.column :samples, :integer
      t.column :duration, :float
      t.timestamps
    end
    
    # Recording Sound
    create_table :recording_sound do |t|
      t.column :recording_id,  :integer
      t.column :sound_id,  :integer
      t.column :position,  :integer
      t.timestamps
    end
  end

  def self.down
    drop_table :recording_sound
    drop_table :recording
    drop_table :sound
  end
end
