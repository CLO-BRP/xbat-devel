class AddFieldsToProject < ActiveRecord::Migration
  def self.up
		add_column :project, :volume_id, :integer
		add_column :project, :files_path, :string
  end

  def self.down
		remove_column :project, :files_path
		remove_column :project, :volume_id
  end
end
