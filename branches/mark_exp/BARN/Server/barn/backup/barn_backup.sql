-- MySQL dump 10.13  Distrib 5.1.42, for Win32 (ia32)
--
-- Host: localhost    Database: barn
-- ------------------------------------------------------
-- Server version	5.1.42-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `attachable_id` int(11) NOT NULL,
  `attachable_type` varchar(255) DEFAULT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barn_master`
--

DROP TABLE IF EXISTS `barn_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barn_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `create_sql` text,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barn_master`
--

LOCK TABLES `barn_master` WRITE;
/*!40000 ALTER TABLE `barn_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `barn_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_file`
--

DROP TABLE IF EXISTS `data_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `directory` tinyint(4) DEFAULT NULL,
  `in_ftp` tinyint(4) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `content_hash` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_file`
--

LOCK TABLES `data_file` WRITE;
/*!40000 ALTER TABLE `data_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `start` float DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `channel` int(11) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `log_id` int(11) NOT NULL,
  `recording_id` int(11) NOT NULL,
  `recording_start` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_measure__musical_range__value`
--

DROP TABLE IF EXISTS `event_measure__musical_range__value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_measure__musical_range__value` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `UpperBound` varchar(255) DEFAULT NULL,
  `LowerBound` varchar(255) DEFAULT NULL,
  `Span` varchar(255) DEFAULT NULL,
  `error__UpperBound` varchar(255) DEFAULT NULL,
  `error__LowerBound` varchar(255) DEFAULT NULL,
  `error__Span` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_measure__musical_range__value`
--

LOCK TABLES `event_measure__musical_range__value` WRITE;
/*!40000 ALTER TABLE `event_measure__musical_range__value` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_measure__musical_range__value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_measure__peak_amplitude__parameter`
--

DROP TABLE IF EXISTS `event_measure__peak_amplitude__parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_measure__peak_amplitude__parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_hash` varchar(255) DEFAULT NULL,
  `reduce__method` varchar(255) DEFAULT NULL,
  `reduce__target` int(11) DEFAULT NULL,
  `block` float DEFAULT NULL,
  `auto` int(11) DEFAULT NULL,
  `overlap` float DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_measure__peak_amplitude__parameter`
--

LOCK TABLES `event_measure__peak_amplitude__parameter` WRITE;
/*!40000 ALTER TABLE `event_measure__peak_amplitude__parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_measure__peak_amplitude__parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_measure__peak_amplitude__value`
--

DROP TABLE IF EXISTS `event_measure__peak_amplitude__value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_measure__peak_amplitude__value` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` float DEFAULT NULL,
  `time` float DEFAULT NULL,
  `q1` float DEFAULT NULL,
  `q2` float DEFAULT NULL,
  `q3` float DEFAULT NULL,
  `mean` float DEFAULT NULL,
  `median` float DEFAULT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_measure__peak_amplitude__value`
--

LOCK TABLES `event_measure__peak_amplitude__value` WRITE;
/*!40000 ALTER TABLE `event_measure__peak_amplitude__value` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_measure__peak_amplitude__value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_measure__time_freq_quartiles__parameter`
--

DROP TABLE IF EXISTS `event_measure__time_freq_quartiles__parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_measure__time_freq_quartiles__parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_hash` varchar(255) DEFAULT NULL,
  `specgram__fft` int(11) DEFAULT NULL,
  `specgram__hop` float DEFAULT NULL,
  `specgram__hop_auto` int(11) DEFAULT NULL,
  `specgram__win_type` varchar(255) DEFAULT NULL,
  `specgram__win_param` text,
  `specgram__win_length` int(11) DEFAULT NULL,
  `specgram__sum_type` varchar(255) DEFAULT NULL,
  `specgram__sum_quality` varchar(255) DEFAULT NULL,
  `specgram__sum_length` int(11) DEFAULT NULL,
  `specgram__sum_auto` int(11) DEFAULT NULL,
  `restrict_to_event_band` int(11) DEFAULT NULL,
  `min_frequency` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_measure__time_freq_quartiles__parameter`
--

LOCK TABLES `event_measure__time_freq_quartiles__parameter` WRITE;
/*!40000 ALTER TABLE `event_measure__time_freq_quartiles__parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_measure__time_freq_quartiles__parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_measure__time_freq_quartiles__value`
--

DROP TABLE IF EXISTS `event_measure__time_freq_quartiles__value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_measure__time_freq_quartiles__value` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `time__low` float DEFAULT NULL,
  `time__median` float DEFAULT NULL,
  `time__high` float DEFAULT NULL,
  `time__range` float DEFAULT NULL,
  `time__asymmetry` float DEFAULT NULL,
  `frequency__low` float DEFAULT NULL,
  `frequency__median` float DEFAULT NULL,
  `frequency__high` float DEFAULT NULL,
  `frequency__range` float DEFAULT NULL,
  `frequency__asymmetry` float DEFAULT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_measure__time_freq_quartiles__value`
--

LOCK TABLES `event_measure__time_freq_quartiles__value` WRITE;
/*!40000 ALTER TABLE `event_measure__time_freq_quartiles__value` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_measure__time_freq_quartiles__value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_user`
--

DROP TABLE IF EXISTS `event_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_user` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `event_id` (`event_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_user`
--

LOCK TABLES `event_user` WRITE;
/*!40000 ALTER TABLE `event_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extension`
--

DROP TABLE IF EXISTS `extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `input_type` varchar(255) DEFAULT NULL,
  `output_type` varchar(255) DEFAULT NULL,
  `tablename__parameter` varchar(255) DEFAULT NULL,
  `tablename__value` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1938 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extension`
--

LOCK TABLES `extension` WRITE;
/*!40000 ALTER TABLE `extension` DISABLE KEYS */;
INSERT INTO `extension` VALUES (1762,'sound_detector','Renyi Detector','038c3e62-ee1b-414d-82a4-c43d4fdc0af1','xyzzy','plugh','ext__038c3e62_ee1b_414d_82a4_c43d4fdc0af1__parameter','ext__038c3e62_ee1b_414d_82a4_c43d4fdc0af1__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1763,'extension_type','Sound Attribute','085562a9-4a9b-489a-a940-37a87a82ee9c','xyzzy','plugh','ext__085562a9_4a9b_489a_a940_37a87a82ee9c__parameter','ext__085562a9_4a9b_489a_a940_37a87a82ee9c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1764,'widget','Histogram','08625d56-23c3-4f92-be4c-15d068cb94f6','xyzzy','plugh','ext__08625d56_23c3_4f92_be4c_15d068cb94f6__parameter','ext__08625d56_23c3_4f92_be4c_15d068cb94f6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1765,'sound_detector','random','094bbe02-e724-45bb-9562-10d85d8030e6','xyzzy','plugh','ext__094bbe02_e724_45bb_9562_10d85d8030e6__parameter','ext__094bbe02_e724_45bb_9562_10d85d8030e6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1766,'widget','Spectrum','0a7dc0da-746d-4996-a048-9d4c4f87ae3a','xyzzy','plugh','ext__0a7dc0da_746d_4996_a048_9d4c4f87ae3a__parameter','ext__0a7dc0da_746d_4996_a048_9d4c4f87ae3a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1767,'image_filter','Bandpass in Time','0c636b85-14a2-464e-91b9-89e994675037','xyzzy','plugh','ext__0c636b85_14a2_464e_91b9_89e994675037__parameter','ext__0c636b85_14a2_464e_91b9_89e994675037__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1768,'event_measure','Peak Amplitude','0d0c0164-2adf-4e62-bbd7-2222b9698502','xyzzy','plugh','ext__0d0c0164_2adf_4e62_bbd7_2222b9698502__parameter','ext__0d0c0164_2adf_4e62_bbd7_2222b9698502__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1769,'image_filter','Top-hat','0d346cc1-b188-4708-98fc-8a7607fa4fff','xyzzy','plugh','ext__0d346cc1_b188_4708_98fc_8a7607fa4fff__parameter','ext__0d346cc1_b188_4708_98fc_8a7607fa4fff__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1770,'sound_file_format','FLAC','0d59c797-ba2b-4a65-b451-cb5eaa72b427','xyzzy','plugh','ext__0d59c797_ba2b_4a65_b451_cb5eaa72b427__parameter','ext__0d59c797_ba2b_4a65_b451_cb5eaa72b427__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1771,'signal_filter','Clip','0dfe5dcb-6cf1-492c-a9b9-f7e2443be7e3','xyzzy','plugh','ext__0dfe5dcb_6cf1_492c_a9b9_f7e2443be7e3__parameter','ext__0dfe5dcb_6cf1_492c_a9b9_f7e2443be7e3__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1772,'sound_file_format','AIFF','0ea0a2cc-d7e5-4ace-baaf-b203534ec8bc','xyzzy','plugh','ext__0ea0a2cc_d7e5_4ace_baaf_b203534ec8bc__parameter','ext__0ea0a2cc_d7e5_4ace_baaf_b203534ec8bc__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1773,'sound_browser_palette','Event 2','113fa59f-075d-4e31-a814-0ba4f2ee6bb2','xyzzy','plugh','ext__113fa59f_075d_4e31_a814_0ba4f2ee6bb2__parameter','ext__113fa59f_075d_4e31_a814_0ba4f2ee6bb2__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1774,'sound_file_format','WAV','13359bc6-acbe-4ebe-9aaa-1db64471ba21','xyzzy','plugh','ext__13359bc6_acbe_4ebe_9aaa_1db64471ba21__parameter','ext__13359bc6_acbe_4ebe_9aaa_1db64471ba21__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1775,'event_action','Plot','15fa7ffb-ba8a-4180-9962-e54ee7c44c4f','xyzzy','plugh','ext__15fa7ffb_ba8a_4180_9962_e54ee7c44c4f__parameter','ext__15fa7ffb_ba8a_4180_9962_e54ee7c44c4f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1776,'signal_filter','Amplify','16012601-9391-49ef-96d0-57fd4ee60f5b','xyzzy','plugh','ext__16012601_9391_49ef_96d0_57fd4ee60f5b__parameter','ext__16012601_9391_49ef_96d0_57fd4ee60f5b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1777,'widget','AR Spectrum','1a0d64e3-c88f-4ff6-876e-832a5e58a79e','xyzzy','plugh','ext__1a0d64e3_c88f_4ff6_876e_832a5e58a79e__parameter','ext__1a0d64e3_c88f_4ff6_876e_832a5e58a79e__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1778,'sound_feature','TIME_FREQ_BASE','1a71e32c-8a61-4f03-b474-2075d7450efc','xyzzy','plugh','ext__1a71e32c_8a61_4f03_b474_2075d7450efc__parameter','ext__1a71e32c_8a61_4f03_b474_2075d7450efc__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1779,'sound_feature','SPECTRAL_REDUCTION','1c166692-bc66-4fbe-bea9-7d94910c983b','xyzzy','plugh','ext__1c166692_bc66_4fbe_bea9_7d94910c983b__parameter','ext__1c166692_bc66_4fbe_bea9_7d94910c983b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1780,'log_action','Measure Field Distributions','1d8c9cc1-88ed-4f42-bdd8-107096735924','xyzzy','plugh','ext__1d8c9cc1_88ed_4f42_bdd8_107096735924__parameter','ext__1d8c9cc1_88ed_4f42_bdd8_107096735924__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1781,'sound_feature','Entropy','1dd6b18f-b4b8-4a47-8fb5-5048cd56f268','xyzzy','plugh','ext__1dd6b18f_b4b8_4a47_8fb5_5048cd56f268__parameter','ext__1dd6b18f_b4b8_4a47_8fb5_5048cd56f268__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1782,'signal_filter','Highpass','1dde8231-857b-4a55-b8ee-255feb02a6ea','xyzzy','plugh','ext__1dde8231_857b_4a55_b8ee_255feb02a6ea__parameter','ext__1dde8231_857b_4a55_b8ee_255feb02a6ea__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1783,'sound_feature','Spectrogram','1de318a1-d581-4517-b18f-be1e59b4bed6','xyzzy','plugh','ext__1de318a1_d581_4517_b18f_be1e59b4bed6__parameter','ext__1de318a1_d581_4517_b18f_be1e59b4bed6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1784,'sound_feature','Z-Spectrogram','2102f2b6-f854-49cb-a12b-87960b632696','xyzzy','plugh','ext__2102f2b6_f854_49cb_a12b_87960b632696__parameter','ext__2102f2b6_f854_49cb_a12b_87960b632696__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1785,'event_measure','Time-Freq Moments','21508053-3591-4576-b98d-95f25c280b55','xyzzy','plugh','ext__21508053_3591_4576_b98d_95f25c280b55__parameter','ext__21508053_3591_4576_b98d_95f25c280b55__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1786,'sound_feature','Spectral Flux','22c346f6-77db-402c-9472-426dce4cafd0','xyzzy','plugh','ext__22c346f6_77db_402c_9472_426dce4cafd0__parameter','ext__22c346f6_77db_402c_9472_426dce4cafd0__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1787,'sound_feature','Binary Spectrogram','254ae8c2-a4b5-465f-9e8b-19b13e7d5a8f','xyzzy','plugh','ext__254ae8c2_a4b5_465f_9e8b_19b13e7d5a8f__parameter','ext__254ae8c2_a4b5_465f_9e8b_19b13e7d5a8f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1788,'event_action','Lasso','2730705f-d6a4-4b1c-9a1c-741dc149dd71','xyzzy','plugh','ext__2730705f_d6a4_4b1c_9a1c_741dc149dd71__parameter','ext__2730705f_d6a4_4b1c_9a1c_741dc149dd71__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1789,'image_filter','Slice Normalization','27d59752-e03d-490b-b42f-9f4b34815f42','xyzzy','plugh','ext__27d59752_e03d_490b_b42f_9f4b34815f42__parameter','ext__27d59752_e03d_490b_b42f_9f4b34815f42__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1790,'widget','Similarity Matrix','2a66dcaa-7871-4453-8beb-6d66f854a1f0','xyzzy','plugh','ext__2a66dcaa_7871_4453_8beb_6d66f854a1f0__parameter','ext__2a66dcaa_7871_4453_8beb_6d66f854a1f0__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1791,'signal_filter','Reverb','2ade115d-7e4b-422d-b8ba-9ba14ed8414d','xyzzy','plugh','ext__2ade115d_7e4b_422d_b8ba_9ba14ed8414d__parameter','ext__2ade115d_7e4b_422d_b8ba_9ba14ed8414d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1792,'sound_file_format','AU','2d030f17-00fd-414c-b7f6-ea721fe11f32','xyzzy','plugh','ext__2d030f17_00fd_414c_b7f6_ea721fe11f32__parameter','ext__2d030f17_00fd_414c_b7f6_ea721fe11f32__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1793,'log_format','BARN','2d3ebead-918a-41f3-a38f-e1949e032efa','xyzzy','plugh','ext__2d3ebead_918a_41f3_a38f_e1949e032efa__parameter','ext__2d3ebead_918a_41f3_a38f_e1949e032efa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1794,'event_measure','Freq Quartiles','302934b4-6355-4fd3-ac93-13e1e73a9936','xyzzy','plugh','ext__302934b4_6355_4fd3_ac93_13e1e73a9936__parameter','ext__302934b4_6355_4fd3_ac93_13e1e73a9936__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1795,'image_filter','Erosion','3239d582-bed8-4345-acaa-6d4acbff1d66','xyzzy','plugh','ext__3239d582_bed8_4345_acaa_6d4acbff1d66__parameter','ext__3239d582_bed8_4345_acaa_6d4acbff1d66__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1796,'extension_type','Event Action','3448d0bd-7348-4670-a02d-978cc2c8943d','xyzzy','plugh','ext__3448d0bd_7348_4670_a02d_978cc2c8943d__parameter','ext__3448d0bd_7348_4670_a02d_978cc2c8943d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1797,'image_filter','Binomial','359aae7d-19e5-4519-b38d-18a1b644308b','xyzzy','plugh','ext__359aae7d_19e5_4519_b38d_18a1b644308b__parameter','ext__359aae7d_19e5_4519_b38d_18a1b644308b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1798,'sound_feature','Band Amplitude','369b7feb-7684-46cb-b57c-508805de643f','xyzzy','plugh','ext__369b7feb_7684_46cb_b57c_508805de643f__parameter','ext__369b7feb_7684_46cb_b57c_508805de643f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1799,'signal_filter','Filter Chain','385214e6-88a8-49ea-b9d0-e262f05a6445','xyzzy','plugh','ext__385214e6_88a8_49ea_b9d0_e262f05a6445__parameter','ext__385214e6_88a8_49ea_b9d0_e262f05a6445__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1800,'log_format','MAT','38860c97-79d5-41bd-b84b-224f704cc578','xyzzy','plugh','ext__38860c97_79d5_41bd_b84b_224f704cc578__parameter','ext__38860c97_79d5_41bd_b84b_224f704cc578__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1801,'sound_feature','Log-Frequency Spectrogram','3e952a69-3b10-4103-9c1e-8dc98e4ec80f','xyzzy','plugh','ext__3e952a69_3b10_4103_9c1e_8dc98e4ec80f__parameter','ext__3e952a69_3b10_4103_9c1e_8dc98e4ec80f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1802,'image_filter','Dilation','4013d896-1b10-4475-b09b-49c3887fe37f','xyzzy','plugh','ext__4013d896_1b10_4475_b09b_49c3887fe37f__parameter','ext__4013d896_1b10_4475_b09b_49c3887fe37f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1803,'extension_type','Widget','41a8dc84-aa53-4ad0-b9c9-ed06831e1bed','xyzzy','plugh','ext__41a8dc84_aa53_4ad0_b9c9_ed06831e1bed__parameter','ext__41a8dc84_aa53_4ad0_b9c9_ed06831e1bed__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1804,'signal_filter','Pre-Emphasis','423729d2-e236-4b95-abb8-a8fd09ebf8d6','xyzzy','plugh','ext__423729d2_e236_4b95_abb8_a8fd09ebf8d6__parameter','ext__423729d2_e236_4b95_abb8_a8fd09ebf8d6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1805,'sound_detector','Band Energy','43d10150-ef2b-4fa7-8535-8a8a58fbd6ee','xyzzy','plugh','ext__43d10150_ef2b_4fa7_8535_8a8a58fbd6ee__parameter','ext__43d10150_ef2b_4fa7_8535_8a8a58fbd6ee__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1806,'sound_browser_palette','Log 2','44ab6e8f-518a-46f3-a1a2-ee2c98690c3a','xyzzy','plugh','ext__44ab6e8f_518a_46f3_a1a2_ee2c98690c3a__parameter','ext__44ab6e8f_518a_46f3_a1a2_ee2c98690c3a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1807,'signal_filter','Quantize','44d2be08-bd5d-4fff-9430-5235403e7abe','xyzzy','plugh','ext__44d2be08_bd5d_4fff_9430_5235403e7abe__parameter','ext__44d2be08_bd5d_4fff_9430_5235403e7abe__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1808,'image_filter','Closing','450d7a9a-7174-4b8b-a79a-a29a04590c1b','xyzzy','plugh','ext__450d7a9a_7174_4b8b_a79a_a29a04590c1b__parameter','ext__450d7a9a_7174_4b8b_a79a_a29a04590c1b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1809,'extension_type','Sound Feature','48f70819-a5f6-4e36-a286-bb623da7184d','xyzzy','plugh','ext__48f70819_a5f6_4e36_a286_bb623da7184d__parameter','ext__48f70819_a5f6_4e36_a286_bb623da7184d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1810,'sound_feature','Mel-Band Energies','4b8ec923-f63f-41a2-9d4e-a378f60cd863','xyzzy','plugh','ext__4b8ec923_f63f_41a2_9d4e_a378f60cd863__parameter','ext__4b8ec923_f63f_41a2_9d4e_a378f60cd863__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1811,'sound_detector','Amplitude','4c002510-4473-49aa-9d87-e6654a4d7270','xyzzy','plugh','ext__4c002510_4473_49aa_9d87_e6654a4d7270__parameter','ext__4c002510_4473_49aa_9d87_e6654a4d7270__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1812,'extension_type','Sound Annotation','4dc12eb4-72e0-4677-a82f-4af9d0ea8585','xyzzy','plugh','ext__4dc12eb4_72e0_4677_a82f_4af9d0ea8585__parameter','ext__4dc12eb4_72e0_4677_a82f_4af9d0ea8585__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1813,'image_filter','Rank Filter','4ddd0a3d-135b-4c18-b634-cdd16fa413ec','xyzzy','plugh','ext__4ddd0a3d_135b_4c18_b634_cdd16fa413ec__parameter','ext__4ddd0a3d_135b_4c18_b634_cdd16fa413ec__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1814,'widget','Big Time','513f03e2-4d14-4068-89ed-8cbfa96d5fe8','xyzzy','plugh','ext__513f03e2_4d14_4068_89ed_8cbfa96d5fe8__parameter','ext__513f03e2_4d14_4068_89ed_8cbfa96d5fe8__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1815,'extension_type','Sound File Format','5755c135-a6fb-4af7-96ad-9d0c2162ae02','xyzzy','plugh','ext__5755c135_a6fb_4af7_96ad_9d0c2162ae02__parameter','ext__5755c135_a6fb_4af7_96ad_9d0c2162ae02__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1816,'log_format','SQLite','5861ca99-3871-47c8-a346-7672e26588db','xyzzy','plugh','ext__5861ca99_3871_47c8_a346_7672e26588db__parameter','ext__5861ca99_3871_47c8_a346_7672e26588db__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1817,'signal_filter','Bandstop','5db3e88b-4763-4888-a2e7-215d6293f872','xyzzy','plugh','ext__5db3e88b_4763_4888_a2e7_215d6293f872__parameter','ext__5db3e88b_4763_4888_a2e7_215d6293f872__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1818,'event_action','Clip','5db67070-e46c-46cc-835c-6bc1eeb166d2','xyzzy','plugh','ext__5db67070_e46c_46cc_835c_6bc1eeb166d2__parameter','ext__5db67070_e46c_46cc_835c_6bc1eeb166d2__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1819,'signal_filter','Butterworth','5ea7db21-07c1-48fa-b963-9d007a0f4b48','xyzzy','plugh','ext__5ea7db21_07c1_48fa_b963_9d007a0f4b48__parameter','ext__5ea7db21_07c1_48fa_b963_9d007a0f4b48__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1820,'sound_detector','Components','5fed09d4-3c97-4188-a6e9-c4fc190b6289','xyzzy','plugh','ext__5fed09d4_3c97_4188_a6e9_c4fc190b6289__parameter','ext__5fed09d4_3c97_4188_a6e9_c4fc190b6289__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1821,'sound_attribute','sensor_geometry','6282273c-bb04-4ebc-830c-a53353b52e21','xyzzy','plugh','ext__6282273c_bb04_4ebc_830c_a53353b52e21__parameter','ext__6282273c_bb04_4ebc_830c_a53353b52e21__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1822,'image_filter','Max','64589766-5ed9-43c9-aee3-4aa3c1416a75','xyzzy','plugh','ext__64589766_5ed9_43c9_aee3_4aa3c1416a75__parameter','ext__64589766_5ed9_43c9_aee3_4aa3c1416a75__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1823,'sound_detector','Periodicity','64e237dd-782b-4198-aa55-c7aedeb07327','xyzzy','plugh','ext__64e237dd_782b_4198_aa55_c7aedeb07327__parameter','ext__64e237dd_782b_4198_aa55_c7aedeb07327__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1824,'signal_filter','Whiten','67c81b0d-02b2-46d4-8310-10560d4ea0c9','xyzzy','plugh','ext__67c81b0d_02b2_46d4_8310_10560d4ea0c9__parameter','ext__67c81b0d_02b2_46d4_8310_10560d4ea0c9__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1825,'sound_feature','Renyi Entropy','69493b53-6f74-4821-afcc-6e01b0466a7c','xyzzy','plugh','ext__69493b53_6f74_4821_afcc_6e01b0466a7c__parameter','ext__69493b53_6f74_4821_afcc_6e01b0466a7c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1826,'source','Noise','6b2f8df2-0afe-4738-a20b-ab2599b7edec','xyzzy','plugh','ext__6b2f8df2_0afe_4738_a20b_ab2599b7edec__parameter','ext__6b2f8df2_0afe_4738_a20b_ab2599b7edec__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1827,'extension_type','Extension Type','6c9f20b3-1ab7-444d-9773-bd0e45fcd8aa','xyzzy','plugh','ext__6c9f20b3_1ab7_444d_9773_bd0e45fcd8aa__parameter','ext__6c9f20b3_1ab7_444d_9773_bd0e45fcd8aa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1828,'signal_filter','Difference','6e955877-ab4c-4ab9-bf26-8eb51c3f7028','xyzzy','plugh','ext__6e955877_ab4c_4ab9_bf26_8eb51c3f7028__parameter','ext__6e955877_ab4c_4ab9_bf26_8eb51c3f7028__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1829,'sound_detector','Amplitude Activity','6f3b6741-2dff-4021-a1fb-fa2a53db8bec','xyzzy','plugh','ext__6f3b6741_2dff_4021_a1fb_fa2a53db8bec__parameter','ext__6f3b6741_2dff_4021_a1fb_fa2a53db8bec__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1830,'extension_type','Log Action','7117adb0-5b05-425c-8c89-bf7fa4f01e3c','xyzzy','plugh','ext__7117adb0_5b05_425c_8c89_bf7fa4f01e3c__parameter','ext__7117adb0_5b05_425c_8c89_bf7fa4f01e3c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1831,'image_filter','TIME_FILTER_BASE','734e18ea-4807-42ff-8948-1aa8400f4f41','xyzzy','plugh','ext__734e18ea_4807_42ff_8948_1aa8400f4f41__parameter','ext__734e18ea_4807_42ff_8948_1aa8400f4f41__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1832,'sound_action','SCAN_BASE','735246b5-210e-4756-a3f6-a062757b7143','xyzzy','plugh','ext__735246b5_210e_4756_a3f6_a062757b7143__parameter','ext__735246b5_210e_4756_a3f6_a062757b7143__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1833,'extension_type','Source','76752d0d-eef0-44f0-a776-268966660193','xyzzy','plugh','ext__76752d0d_eef0_44f0_a776_268966660193__parameter','ext__76752d0d_eef0_44f0_a776_268966660193__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1834,'extension_type','Sound Browser Palette','76dc8fa8-1e6a-4567-b472-442b308f9117','xyzzy','plugh','ext__76dc8fa8_1e6a_4567_b472_442b308f9117__parameter','ext__76dc8fa8_1e6a_4567_b472_442b308f9117__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1835,'signal_filter','Impulse-Noise','7700211e-19ad-46a8-9aff-9234d68ce934','xyzzy','plugh','ext__7700211e_19ad_46a8_9aff_9234d68ce934__parameter','ext__7700211e_19ad_46a8_9aff_9234d68ce934__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1836,'image_filter','Average','7ba6ab19-7ba0-4aa1-964a-548fabc38d11','xyzzy','plugh','ext__7ba6ab19_7ba0_4aa1_964a_548fabc38d11__parameter','ext__7ba6ab19_7ba0_4aa1_964a_548fabc38d11__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1837,'sound_file_format','MP3','7beef146-d66c-4986-a475-7f8eed71f5fa','xyzzy','plugh','ext__7beef146_d66c_4986_a475_7f8eed71f5fa__parameter','ext__7beef146_d66c_4986_a475_7f8eed71f5fa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1838,'widget','Events','7d4ad61e-0e85-494d-8052-7655a92f7bc3','xyzzy','plugh','ext__7d4ad61e_0e85_494d_8052_7655a92f7bc3__parameter','ext__7d4ad61e_0e85_494d_8052_7655a92f7bc3__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1839,'sound_feature','Novelty Power Spectrum','7de28912-5e15-4ebd-95c3-ed68527f187d','xyzzy','plugh','ext__7de28912_5e15_4ebd_95c3_ed68527f187d__parameter','ext__7de28912_5e15_4ebd_95c3_ed68527f187d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1840,'log_action','Strip','7fed7b5a-defb-428d-8159-521bd0704f94','xyzzy','plugh','ext__7fed7b5a_defb_428d_8159_521bd0704f94__parameter','ext__7fed7b5a_defb_428d_8159_521bd0704f94__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1841,'sound_feature','SPECTRAL_MAP','816171c6-30a1-4602-aaea-21cfab562035','xyzzy','plugh','ext__816171c6_30a1_4602_aaea_21cfab562035__parameter','ext__816171c6_30a1_4602_aaea_21cfab562035__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1842,'event_measure','ZC-Rate','82436d96-8abd-4fd5-95ee-ff3a08df4599','xyzzy','plugh','ext__82436d96_8abd_4fd5_95ee_ff3a08df4599__parameter','ext__82436d96_8abd_4fd5_95ee_ff3a08df4599__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1843,'image_filter','Center','83c74924-54e1-4343-a393-272cf1c41322','xyzzy','plugh','ext__83c74924_54e1_4343_a393_272cf1c41322__parameter','ext__83c74924_54e1_4343_a393_272cf1c41322__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1844,'signal_filter','Codepad','84c11ff5-c6c6-4ba4-b3fa-a803082349b1','xyzzy','plugh','ext__84c11ff5_c6c6_4ba4_b3fa_a803082349b1__parameter','ext__84c11ff5_c6c6_4ba4_b3fa_a803082349b1__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1845,'sound_feature','Band Energies','85a57ed0-5de1-41f9-bb02-c9e0e416a020','xyzzy','plugh','ext__85a57ed0_5de1_41f9_bb02_c9e0e416a020__parameter','ext__85a57ed0_5de1_41f9_bb02_c9e0e416a020__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1846,'image_filter','Opening','87364d43-0cc5-4778-aefe-c2650fcdd5e9','xyzzy','plugh','ext__87364d43_0cc5_4778_aefe_c2650fcdd5e9__parameter','ext__87364d43_0cc5_4778_aefe_c2650fcdd5e9__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1847,'image_filter','x_edge','87670dd1-b3ed-49de-a36d-a637d3c15faa','xyzzy','plugh','ext__87670dd1_b3ed_49de_a36d_a637d3c15faa__parameter','ext__87670dd1_b3ed_49de_a36d_a637d3c15faa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1848,'signal_filter','Reduce-Static','884ff07a-3934-404e-ade8-d27e458d29a8','xyzzy','plugh','ext__884ff07a_3934_404e_ade8_d27e458d29a8__parameter','ext__884ff07a_3934_404e_ade8_d27e458d29a8__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1849,'sound_feature','Band Novelties','8862222e-9acf-4a14-a2aa-94301b87a130','xyzzy','plugh','ext__8862222e_9acf_4a14_a2aa_94301b87a130__parameter','ext__8862222e_9acf_4a14_a2aa_94301b87a130__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1850,'image_filter','LINEAR_BASE','8919726a-ebf7-4972-a1a3-8173817e6d70','xyzzy','plugh','ext__8919726a_ebf7_4972_a1a3_8173817e6d70__parameter','ext__8919726a_ebf7_4972_a1a3_8173817e6d70__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1851,'extension_type','Log Format','8aed5ff8-3436-48e7-8e20-80631dad797d','xyzzy','plugh','ext__8aed5ff8_3436_48e7_8e20_80631dad797d__parameter','ext__8aed5ff8_3436_48e7_8e20_80631dad797d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1852,'signal_filter','Notch','8af88c1d-d1aa-4b3f-bb14-8c53c5b78647','xyzzy','plugh','ext__8af88c1d_d1aa_4b3f_bb14_8c53c5b78647__parameter','ext__8af88c1d_d1aa_4b3f_bb14_8c53c5b78647__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1853,'sound_attribute','time_stamps','8b4304b3-6d01-4674-b14b-7d668223aac5','xyzzy','plugh','ext__8b4304b3_6d01_4674_b14b_7d668223aac5__parameter','ext__8b4304b3_6d01_4674_b14b_7d668223aac5__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1854,'widget','Aerial Map','8c01e8bd-814d-4c79-a226-174510a8d577','xyzzy','plugh','ext__8c01e8bd_814d_4c79_a226_174510a8d577__parameter','ext__8c01e8bd_814d_4c79_a226_174510a8d577__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1855,'sound_feature','FROST','8dfc5fbb-9fca-4295-b5e9-6548a4c8994b','xyzzy','plugh','ext__8dfc5fbb_9fca_4295_b5e9_6548a4c8994b__parameter','ext__8dfc5fbb_9fca_4295_b5e9_6548a4c8994b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1856,'image_filter','Morphological Gradient','8eb6f24f-338d-4a15-a1d3-213e3f32df6a','xyzzy','plugh','ext__8eb6f24f_338d_4a15_a1d3_213e3f32df6a__parameter','ext__8eb6f24f_338d_4a15_a1d3_213e3f32df6a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1857,'sound_file_format','LIBSNDFILE_BASE','90a30487-41b3-42ee-9e97-df7c9a81d5f6','xyzzy','plugh','ext__90a30487_41b3_42ee_9e97_df7c9a81d5f6__parameter','ext__90a30487_41b3_42ee_9e97_df7c9a81d5f6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1858,'extension_type','Sound Format','91035235-0c85-4864-9bb5-9f7fb4e07994','xyzzy','plugh','ext__91035235_0c85_4864_9bb5_9f7fb4e07994__parameter','ext__91035235_0c85_4864_9bb5_9f7fb4e07994__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1859,'log_format','DATABASE_BASE','9126d167-a3cf-4bf0-a890-cd1e72bdd8e7','xyzzy','plugh','ext__9126d167_a3cf_4bf0_a890_cd1e72bdd8e7__parameter','ext__9126d167_a3cf_4bf0_a890_cd1e72bdd8e7__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1860,'signal_filter','WaveShrink','959176dd-7f19-4f7e-9a6b-8ae91a51ff53','xyzzy','plugh','ext__959176dd_7f19_4f7e_9a6b_8ae91a51ff53__parameter','ext__959176dd_7f19_4f7e_9a6b_8ae91a51ff53__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1861,'log_action','Create Digest','972089aa-6af8-4fb1-8226-4123bf18a1a1','xyzzy','plugh','ext__972089aa_6af8_4fb1_8226_4123bf18a1a1__parameter','ext__972089aa_6af8_4fb1_8226_4123bf18a1a1__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1862,'image_filter','Recursive Average','9792861b-80e8-4a89-bbba-ee3c0ea7dfbb','xyzzy','plugh','ext__9792861b_80e8_4a89_bbba_ee3c0ea7dfbb__parameter','ext__9792861b_80e8_4a89_bbba_ee3c0ea7dfbb__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1863,'log_format','BARN-REST','98ad89e9-35cd-44b2-a7d2-dec3e4fbed54','xyzzy','plugh','ext__98ad89e9_35cd_44b2_a7d2_dec3e4fbed54__parameter','ext__98ad89e9_35cd_44b2_a7d2_dec3e4fbed54__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1864,'image_filter','TIME_FIR_BASE','99fa3b5e-1cb0-498e-9f02-2710b9eb1810','xyzzy','plugh','ext__99fa3b5e_1cb0_498e_9f02_2710b9eb1810__parameter','ext__99fa3b5e_1cb0_498e_9f02_2710b9eb1810__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1865,'sound_detector','Noise','9ac852cb-6579-40df-9d0d-d643d8c7e90a','xyzzy','plugh','ext__9ac852cb_6579_40df_9d0d_d643d8c7e90a__parameter','ext__9ac852cb_6579_40df_9d0d_d643d8c7e90a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1866,'sound_detector','CDSS','9da9df22-ec1d-48e3-b595-d46602307c77','xyzzy','plugh','ext__9da9df22_ec1d_48e3_b595_d46602307c77__parameter','ext__9da9df22_ec1d_48e3_b595_d46602307c77__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1867,'sound_attribute','sound_speed','9df44102-c56e-4f00-925c-d9b37eb77f8f','xyzzy','plugh','ext__9df44102_c56e_4f00_925c_d9b37eb77f8f__parameter','ext__9df44102_c56e_4f00_925c_d9b37eb77f8f__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1868,'log_action','Clone Log','9e9fa053-88cd-4adf-9d84-25adc7b0ce86','xyzzy','plugh','ext__9e9fa053_88cd_4adf_9d84_25adc7b0ce86__parameter','ext__9e9fa053_88cd_4adf_9d84_25adc7b0ce86__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1869,'sound_detector','Band Amplitude','9ed5933a-34ad-42af-a6aa-11922de09fc9','xyzzy','plugh','ext__9ed5933a_34ad_42af_a6aa_11922de09fc9__parameter','ext__9ed5933a_34ad_42af_a6aa_11922de09fc9__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1870,'sound_action','Detect','9f80a1fe-18e3-48fb-b5e7-a59f3c506f20','xyzzy','plugh','ext__9f80a1fe_18e3_48fb_b5e7_a59f3c506f20__parameter','ext__9f80a1fe_18e3_48fb_b5e7_a59f3c506f20__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1871,'sound_attribute','sensor_calibration','9ffdbbfe-e0b5-4aa3-ad4a-33287a1571fb','xyzzy','plugh','ext__9ffdbbfe_e0b5_4aa3_ad4a_33287a1571fb__parameter','ext__9ffdbbfe_e0b5_4aa3_ad4a_33287a1571fb__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1872,'image_filter','Toggle Contrast','a08e4979-c6c4-4f63-8c19-39939fe8d023','xyzzy','plugh','ext__a08e4979_c6c4_4f63_8c19_39939fe8d023__parameter','ext__a08e4979_c6c4_4f63_8c19_39939fe8d023__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1873,'sound_feature','Amplitude','a2816ecf-2866-4759-b4ce-a5385146f946','xyzzy','plugh','ext__a2816ecf_2866_4759_b4ce_a5385146f946__parameter','ext__a2816ecf_2866_4759_b4ce_a5385146f946__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1874,'event_action','Measure Field Distributions','a5748802-965e-4ede-a482-774824919ef7','xyzzy','plugh','ext__a5748802_965e_4ede_a482_774824919ef7__parameter','ext__a5748802_965e_4ede_a482_774824919ef7__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1875,'signal_filter','FFT Truncate','a74d1d75-b086-41af-af7b-69497152f5c5','xyzzy','plugh','ext__a74d1d75_b086_41af_af7b_69497152f5c5__parameter','ext__a74d1d75_b086_41af_af7b_69497152f5c5__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1876,'sound_detector','I can count','a989ead8-c6a7-436b-b9cd-8ca71569f4a5','xyzzy','plugh','ext__a989ead8_c6a7_436b_b9cd_8ca71569f4a5__parameter','ext__a989ead8_c6a7_436b_b9cd_8ca71569f4a5__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1877,'extension_type','Sound Detector','aa08acaa-667e-4aae-aa97-0c6fe74a070c','xyzzy','plugh','ext__aa08acaa_667e_4aae_aa97_0c6fe74a070c__parameter','ext__aa08acaa_667e_4aae_aa97_0c6fe74a070c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1878,'log_action','Event Score Summary','aec659dc-d182-4033-8395-ce3d389c083c','xyzzy','plugh','ext__aec659dc_d182_4033_8395_ce3d389c083c__parameter','ext__aec659dc_d182_4033_8395_ce3d389c083c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1879,'extension_type','Log Annotation','afddc672-77b5-4093-ab5b-b4660b5c950c','xyzzy','plugh','ext__afddc672_77b5_4093_ab5b_b4660b5c950c__parameter','ext__afddc672_77b5_4093_ab5b_b4660b5c950c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1880,'log_action','Iterate Event Action','b1806a16-06c0-41d2-b13f-187ea8ed9d82','xyzzy','plugh','ext__b1806a16_06c0_41d2_b13f_187ea8ed9d82__parameter','ext__b1806a16_06c0_41d2_b13f_187ea8ed9d82__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1881,'image_filter','Highpass in Time','b3b1acf3-9b35-4357-bc41-1c14a84d46b8','xyzzy','plugh','ext__b3b1acf3_9b35_4357_bc41_1c14a84d46b8__parameter','ext__b3b1acf3_9b35_4357_bc41_1c14a84d46b8__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1882,'event_action','Correlate Test','b3fe661b-ab47-427a-8dfe-7ee68c5ca6c7','xyzzy','plugh','ext__b3fe661b_ab47_427a_8dfe_7ee68c5ca6c7__parameter','ext__b3fe661b_ab47_427a_8dfe_7ee68c5ca6c7__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1883,'sound_file_format','W64','b490b66a-6b0d-4ece-aea0-495d05d4da33','xyzzy','plugh','ext__b490b66a_6b0d_4ece_aea0_495d05d4da33__parameter','ext__b490b66a_6b0d_4ece_aea0_495d05d4da33__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1884,'sound_detector','Data Template','b5c2cf6c-e2d7-4588-957a-b1b4d96a7ac8','xyzzy','plugh','ext__b5c2cf6c_e2d7_4588_957a_b1b4d96a7ac8__parameter','ext__b5c2cf6c_e2d7_4588_957a_b1b4d96a7ac8__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1885,'widget','Scope','b619185d-f290-4425-a56a-d91d04ade943','xyzzy','plugh','ext__b619185d_f290_4425_a56a_d91d04ade943__parameter','ext__b619185d_f290_4425_a56a_d91d04ade943__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1886,'extension_type','Event Classifier','b6f5f6cb-02d0-4951-9a8d-3cb078751338','xyzzy','plugh','ext__b6f5f6cb_02d0_4951_9a8d_3cb078751338__parameter','ext__b6f5f6cb_02d0_4951_9a8d_3cb078751338__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1887,'image_filter','Min','b811bd58-9476-43f1-903c-47d142674546','xyzzy','plugh','ext__b811bd58_9476_43f1_903c_47d142674546__parameter','ext__b811bd58_9476_43f1_903c_47d142674546__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1888,'sound_detector','ZC-Detector','b85d4e15-4c9d-404f-a8bd-72326ae672d4','xyzzy','plugh','ext__b85d4e15_4c9d_404f_a8bd_72326ae672d4__parameter','ext__b85d4e15_4c9d_404f_a8bd_72326ae672d4__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1889,'signal_filter','Resonant','b8caefd8-66e2-4614-a51d-6ca39b6164d5','xyzzy','plugh','ext__b8caefd8_66e2_4614_a51d_6ca39b6164d5__parameter','ext__b8caefd8_66e2_4614_a51d_6ca39b6164d5__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1890,'image_filter','Median','ba3f01ba-239d-455d-863e-25fcd606707a','xyzzy','plugh','ext__ba3f01ba_239d_455d_863e_25fcd606707a__parameter','ext__ba3f01ba_239d_455d_863e_25fcd606707a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1891,'image_filter','Components','bff72677-19c0-4122-92b9-7b628551a058','xyzzy','plugh','ext__bff72677_19c0_4122_92b9_7b628551a058__parameter','ext__bff72677_19c0_4122_92b9_7b628551a058__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1892,'sound_action','Configure Spectrogram','c0138e2c-bd4a-46f6-bcae-bd48949e9b5d','xyzzy','plugh','ext__c0138e2c_bd4a_46f6_bcae_bd48949e9b5d__parameter','ext__c0138e2c_bd4a_46f6_bcae_bd48949e9b5d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1893,'signal_filter','Root Distortion','c03b53dd-f219-427a-83e1-719b15948977','xyzzy','plugh','ext__c03b53dd_f219_427a_83e1_719b15948977__parameter','ext__c03b53dd_f219_427a_83e1_719b15948977__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1894,'image_filter','MBE','c0fb9539-67ad-4b6f-9587-1ee5388fb159','xyzzy','plugh','ext__c0fb9539_67ad_4b6f_9587_1ee5388fb159__parameter','ext__c0fb9539_67ad_4b6f_9587_1ee5388fb159__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1895,'sound_feature','SVD-Spectrogram','c232e95d-c2ee-43f9-bfe4-976f57225b05','xyzzy','plugh','ext__c232e95d_c2ee_43f9_bfe4_976f57225b05__parameter','ext__c232e95d_c2ee_43f9_bfe4_976f57225b05__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1896,'sound_feature','Spectral Centroid','c25206d3-6fbe-4405-ad41-a3ce187765af','xyzzy','plugh','ext__c25206d3_6fbe_4405_ad41_a3ce187765af__parameter','ext__c25206d3_6fbe_4405_ad41_a3ce187765af__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1897,'sound_action','Read Test','c441ce7a-70da-47ff-8c34-7d4322ec0d56','xyzzy','plugh','ext__c441ce7a_70da_47ff_8c34_7d4322ec0d56__parameter','ext__c441ce7a_70da_47ff_8c34_7d4322ec0d56__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1898,'extension_type','Event Annotation','c787bdca-7c07-44ea-a747-8eca2560adaa','xyzzy','plugh','ext__c787bdca_7c07_44ea_a747_8eca2560adaa__parameter','ext__c787bdca_7c07_44ea_a747_8eca2560adaa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1899,'signal_filter','LINEAR_BASE','c82fc83d-6832-4e32-a518-c4508e5d7c3e','xyzzy','plugh','ext__c82fc83d_6832_4e32_a518_c4508e5d7c3e__parameter','ext__c82fc83d_6832_4e32_a518_c4508e5d7c3e__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1900,'extension_type','Sound Action','cadf3023-3a2e-4a85-816e-f3d5d622f20b','xyzzy','plugh','ext__cadf3023_3a2e_4a85_816e_f3d5d622f20b__parameter','ext__cadf3023_3a2e_4a85_816e_f3d5d622f20b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1901,'widget','MAP_BASE','cd3c790f-d34e-4a72-a3f7-0b7d97ac52ed','xyzzy','plugh','ext__cd3c790f_d34e_4a72_a3f7_0b7d97ac52ed__parameter','ext__cd3c790f_d34e_4a72_a3f7_0b7d97ac52ed__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1902,'sound_attribute','Photo Stream','cd942acf-def4-4891-9820-f939b0521cde','xyzzy','plugh','ext__cd942acf_def4_4891_9820_f939b0521cde__parameter','ext__cd942acf_def4_4891_9820_f939b0521cde__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1903,'log_format','MobySound','cef6a0e0-d023-4eeb-90c4-45309112203c','xyzzy','plugh','ext__cef6a0e0_d023_4eeb_90c4_45309112203c__parameter','ext__cef6a0e0_d023_4eeb_90c4_45309112203c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1904,'sound_detector','Embedded','d0626377-980b-449f-b3c4-9c681f2ca20d','xyzzy','plugh','ext__d0626377_980b_449f_b3c4_9c681f2ca20d__parameter','ext__d0626377_980b_449f_b3c4_9c681f2ca20d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1905,'widget','SPECTROGRAM_BASE','d07c96b4-b109-42ad-b376-b304b31b0505','xyzzy','plugh','ext__d07c96b4_b109_42ad_b376_b304b31b0505__parameter','ext__d07c96b4_b109_42ad_b376_b304b31b0505__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1906,'image_filter','MORPHOLOGICAL_BASE','d0c14c86-e6f5-432a-ad24-47089504b2ff','xyzzy','plugh','ext__d0c14c86_e6f5_432a_ad24_47089504b2ff__parameter','ext__d0c14c86_e6f5_432a_ad24_47089504b2ff__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1907,'log_action','Temporal Distribution','d1619117-0eba-4c00-972c-f45c4e60962a','xyzzy','plugh','ext__d1619117_0eba_4c00_972c_f45c4e60962a__parameter','ext__d1619117_0eba_4c00_972c_f45c4e60962a__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1908,'sound_feature','Mel Band Autocorrelation','d4faae1b-0b57-4103-861d-a3d42c94bbd1','xyzzy','plugh','ext__d4faae1b_0b57_4103_861d_a3d42c94bbd1__parameter','ext__d4faae1b_0b57_4103_861d_a3d42c94bbd1__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1909,'sound_attribute','Simple Sound Speed','d5cc004d-9936-40a8-8c16-c9f24aa7a41d','xyzzy','plugh','ext__d5cc004d_9936_40a8_8c16_c9f24aa7a41d__parameter','ext__d5cc004d_9936_40a8_8c16_c9f24aa7a41d__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1910,'sound_detector','Salient Events','d82ac506-5719-446a-b30f-0076322d3f58','xyzzy','plugh','ext__d82ac506_5719_446a_b30f_0076322d3f58__parameter','ext__d82ac506_5719_446a_b30f_0076322d3f58__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1911,'signal_filter','POINT_OPERATION_BASE','d99808e5-d472-4561-b65e-609a0cd6d1ee','xyzzy','plugh','ext__d99808e5_d472_4561_b65e_609a0cd6d1ee__parameter','ext__d99808e5_d472_4561_b65e_609a0cd6d1ee__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1912,'widget','Big Selection','d9ab8fce-e1a9-4c67-b437-2052fb3e4e5e','xyzzy','plugh','ext__d9ab8fce_e1a9_4c67_b437_2052fb3e4e5e__parameter','ext__d9ab8fce_e1a9_4c67_b437_2052fb3e4e5e__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1913,'sound_feature','Envelope','d9d054a7-47bd-47eb-8ee8-fbb5c9e3d804','xyzzy','plugh','ext__d9d054a7_47bd_47eb_8ee8_fbb5c9e3d804__parameter','ext__d9d054a7_47bd_47eb_8ee8_fbb5c9e3d804__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1914,'extension_type','Log Measure','dc5315b8-e896-4f32-94ef-e1e68a930f7e','xyzzy','plugh','ext__dc5315b8_e896_4f32_94ef_e1e68a930f7e__parameter','ext__dc5315b8_e896_4f32_94ef_e1e68a930f7e__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1915,'event_measure','Time-Freq Quartiles','dd0f4688-8f10-48b6-af02-e0dd7dc84010','xyzzy','plugh','ext__dd0f4688_8f10_48b6_af02_e0dd7dc84010__parameter','ext__dd0f4688_8f10_48b6_af02_e0dd7dc84010__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1916,'event_action','Export CSV','dd9044d4-4eef-489b-ae43-9b81e35e47aa','xyzzy','plugh','ext__dd9044d4_4eef_489b_ae43_9b81e35e47aa__parameter','ext__dd9044d4_4eef_489b_ae43_9b81e35e47aa__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1917,'sound_feature','GENERALIZED_AMPLITUDE','df2fb346-ce49-4368-bf80-006c51fb0de8','xyzzy','plugh','ext__df2fb346_ce49_4368_bf80_006c51fb0de8__parameter','ext__df2fb346_ce49_4368_bf80_006c51fb0de8__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1918,'signal_filter','Bandpass','e4e39ef8-3aec-4a67-9ee1-1da465372d62','xyzzy','plugh','ext__e4e39ef8_3aec_4a67_9ee1_1da465372d62__parameter','ext__e4e39ef8_3aec_4a67_9ee1_1da465372d62__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1919,'signal_filter','Butterworth Band','e5fd8ecf-6b4d-4900-829b-0525a441d517','xyzzy','plugh','ext__e5fd8ecf_6b4d_4900_829b_0525a441d517__parameter','ext__e5fd8ecf_6b4d_4900_829b_0525a441d517__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1920,'extension_type','Image Filter','e97b84b7-4d31-4a9d-9cb8-8725fdba2d48','xyzzy','plugh','ext__e97b84b7_4d31_4a9d_9cb8_8725fdba2d48__parameter','ext__e97b84b7_4d31_4a9d_9cb8_8725fdba2d48__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1921,'event_measure','Formants','ec9c2191-5003-4ca8-b7cc-1ee4532bc963','xyzzy','plugh','ext__ec9c2191_5003_4ca8_b7cc_1ee4532bc963__parameter','ext__ec9c2191_5003_4ca8_b7cc_1ee4532bc963__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1922,'extension_type','Signal Filter','ed59597f-3ef1-4b42-b80f-4314343be279','xyzzy','plugh','ext__ed59597f_3ef1_4b42_b80f_4314343be279__parameter','ext__ed59597f_3ef1_4b42_b80f_4314343be279__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1923,'widget','Photo Stream','ed6d7a3e-b970-408a-8e95-f1c816b046b6','xyzzy','plugh','ext__ed6d7a3e_b970_408a_8e95_f1c816b046b6__parameter','ext__ed6d7a3e_b970_408a_8e95_f1c816b046b6__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1924,'sound_detector','Amplitude Groups','ee43d15f-0d8f-41dd-8f14-8c0633ca335e','xyzzy','plugh','ext__ee43d15f_0d8f_41dd_8f14_8c0633ca335e__parameter','ext__ee43d15f_0d8f_41dd_8f14_8c0633ca335e__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1925,'sound_feature','Octave Filter-Bank','f009bfee-7e2b-4245-a026-5085e63be0da','xyzzy','plugh','ext__f009bfee_7e2b_4245_a026_5085e63be0da__parameter','ext__f009bfee_7e2b_4245_a026_5085e63be0da__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1926,'image_filter','Midrange','f0d20c3f-c6ef-4900-bec1-7b243cd74f83','xyzzy','plugh','ext__f0d20c3f_c6ef_4900_bec1_7b243cd74f83__parameter','ext__f0d20c3f_c6ef_4900_bec1_7b243cd74f83__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1927,'sound_feature','Teager-Kaiser','f53b3dfe-0cf6-419c-8197-caff5ec78e1c','xyzzy','plugh','ext__f53b3dfe_0cf6_419c_8197_caff5ec78e1c__parameter','ext__f53b3dfe_0cf6_419c_8197_caff5ec78e1c__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1928,'sound_file_format','CAF','f5596fb7-8a0d-4711-bae3-9810a37d5e94','xyzzy','plugh','ext__f5596fb7_8a0d_4711_bae3_9810a37d5e94__parameter','ext__f5596fb7_8a0d_4711_bae3_9810a37d5e94__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1929,'sound_attribute','date_time','f78d71d6-a340-49f9-a02d-08f2b91e8b6b','xyzzy','plugh','ext__f78d71d6_a340_49f9_a02d_08f2b91e8b6b__parameter','ext__f78d71d6_a340_49f9_a02d_08f2b91e8b6b__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1930,'sound_feature','SIGNAL_FEATURE','f910cc10-fd16-4001-b443-29add1cf96d4','xyzzy','plugh','ext__f910cc10_fd16_4001_b443_29add1cf96d4__parameter','ext__f910cc10_fd16_4001_b443_29add1cf96d4__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1931,'sound_feature','Surprisingly Loud','f98750a6-85cc-47f4-a07f-9f491399dede','xyzzy','plugh','ext__f98750a6_85cc_47f4_a07f_9f491399dede__parameter','ext__f98750a6_85cc_47f4_a07f_9f491399dede__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1932,'extension_type','Event Measure','fadaa80f-1619-4201-b2c9-a170bfb1bf08','xyzzy','plugh','ext__fadaa80f_1619_4201_b2c9_a170bfb1bf08__parameter','ext__fadaa80f_1619_4201_b2c9_a170bfb1bf08__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1933,'sound_action','Clip Log','fccc1448-4a2d-485a-b4b8-c2730d348d61','xyzzy','plugh','ext__fccc1448_4a2d_485a_b4b8_c2730d348d61__parameter','ext__fccc1448_4a2d_485a_b4b8_c2730d348d61__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1934,'log_action','Correlate Events','fdc5ffe9-5968-419d-8c4d-9e6444ed1e00','xyzzy','plugh','ext__fdc5ffe9_5968_419d_8c4d_9e6444ed1e00__parameter','ext__fdc5ffe9_5968_419d_8c4d_9e6444ed1e00__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1935,'signal_filter','Lowpass','fe5e112f-0491-4df3-85d0-62fbd32991a4','xyzzy','plugh','ext__fe5e112f_0491_4df3_85d0_62fbd32991a4__parameter','ext__fe5e112f_0491_4df3_85d0_62fbd32991a4__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1936,'widget','Clock','fe8c3380-0ef6-45c8-a63d-05908f82a084','xyzzy','plugh','ext__fe8c3380_0ef6_45c8_a63d_05908f82a084__parameter','ext__fe8c3380_0ef6_45c8_a63d_05908f82a084__value','2010-02-01 19:32:57','2010-02-01 19:32:57'),(1937,'sound_feature','ZC-Rate','ff543bb3-e246-4cba-bf61-8fa2a44b33d3','xyzzy','plugh','ext__ff543bb3_e246_4cba_bf61_8fa2a44b33d3__parameter','ext__ff543bb3_e246_4cba_bf61_8fa2a44b33d3__value','2010-02-01 19:32:57','2010-02-01 19:32:57');
/*!40000 ALTER TABLE `extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_server`
--

DROP TABLE IF EXISTS `file_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `initial_path` varchar(255) DEFAULT NULL,
  `protocol` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_server`
--

LOCK TABLES `file_server` WRITE;
/*!40000 ALTER TABLE `file_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library`
--

DROP TABLE IF EXISTS `library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library`
--

LOCK TABLES `library` WRITE;
/*!40000 ALTER TABLE `library` DISABLE KEYS */;
/*!40000 ALTER TABLE `library` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_sound`
--

DROP TABLE IF EXISTS `library_sound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_sound` (
  `library_id` int(11) NOT NULL,
  `sound_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `library_id` (`library_id`,`sound_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_sound`
--

LOCK TABLES `library_sound` WRITE;
/*!40000 ALTER TABLE `library_sound` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_sound` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_user`
--

DROP TABLE IF EXISTS `library_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `library_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `library_id` (`library_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_user`
--

LOCK TABLES `library_user` WRITE;
/*!40000 ALTER TABLE `library_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sound_id` int(11) NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `content_hash` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noting`
--

DROP TABLE IF EXISTS `noting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_id` int(11) NOT NULL,
  `annotated_type` varchar(255) DEFAULT NULL,
  `annotated_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noting`
--

LOCK TABLES `noting` WRITE;
/*!40000 ALTER TABLE `noting` DISABLE KEYS */;
/*!40000 ALTER TABLE `noting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peer`
--

DROP TABLE IF EXISTS `peer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peer`
--

LOCK TABLES `peer` WRITE;
/*!40000 ALTER TABLE `peer` DISABLE KEYS */;
/*!40000 ALTER TABLE `peer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peer_user`
--

DROP TABLE IF EXISTS `peer_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peer_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `peer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `peer_id` (`peer_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peer_user`
--

LOCK TABLES `peer_user` WRITE;
/*!40000 ALTER TABLE `peer_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `peer_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `visible` int(11) DEFAULT NULL,
  `public` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_asset`
--

DROP TABLE IF EXISTS `project_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_asset`
--

LOCK TABLES `project_asset` WRITE;
/*!40000 ALTER TABLE `project_asset` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_user`
--

DROP TABLE IF EXISTS `project_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `project_id` (`project_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_user`
--

LOCK TABLES `project_user` WRITE;
/*!40000 ALTER TABLE `project_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rateable_type` varchar(255) DEFAULT NULL,
  `rateable_id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recording`
--

DROP TABLE IF EXISTS `recording`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recording` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `bytes` int(11) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `channels` int(11) DEFAULT NULL,
  `samplerate` int(11) DEFAULT NULL,
  `samplesize` int(11) DEFAULT NULL,
  `samples` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `content_hash` varchar(255) DEFAULT NULL,
  `location` text,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recording`
--

LOCK TABLES `recording` WRITE;
/*!40000 ALTER TABLE `recording` DISABLE KEYS */;
/*!40000 ALTER TABLE `recording` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recording_sound`
--

DROP TABLE IF EXISTS `recording_sound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recording_sound` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recording_id` int(11) NOT NULL,
  `sound_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `cumulative_samples` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `recording_id` (`recording_id`,`sound_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recording_sound`
--

LOCK TABLES `recording_sound` WRITE;
/*!40000 ALTER TABLE `recording_sound` DISABLE KEYS */;
/*!40000 ALTER TABLE `recording_sound` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `role_id` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('0');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shared`
--

DROP TABLE IF EXISTS `shared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shareable_type` varchar(255) DEFAULT NULL,
  `shareable_id` int(11) NOT NULL,
  `forwardable` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shareable_id` (`shareable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shared`
--

LOCK TABLES `shared` WRITE;
/*!40000 ALTER TABLE `shared` DISABLE KEYS */;
/*!40000 ALTER TABLE `shared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sharing`
--

DROP TABLE IF EXISTS `sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sharing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shared_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sharing`
--

LOCK TABLES `sharing` WRITE;
/*!40000 ALTER TABLE `sharing` DISABLE KEYS */;
/*!40000 ALTER TABLE `sharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sound`
--

DROP TABLE IF EXISTS `sound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sound` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `channels` int(11) DEFAULT NULL,
  `samplerate` float DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `samples` float DEFAULT NULL,
  `content_hash` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sound`
--

LOCK TABLES `sound` WRITE;
/*!40000 ALTER TABLE `sound` DISABLE KEYS */;
/*!40000 ALTER TABLE `sound` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sound_detector__band_amplitude__parameter`
--

DROP TABLE IF EXISTS `sound_detector__band_amplitude__parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sound_detector__band_amplitude__parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_hash` varchar(255) DEFAULT NULL,
  `reduce__method` varchar(255) DEFAULT NULL,
  `reduce__target` int(11) DEFAULT NULL,
  `block` float DEFAULT NULL,
  `auto` int(11) DEFAULT NULL,
  `overlap` float DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `snr` int(11) DEFAULT NULL,
  `noise_floor` int(11) DEFAULT NULL,
  `threshold` int(11) DEFAULT NULL,
  `break` float DEFAULT NULL,
  `stop` float DEFAULT NULL,
  `join_contiguous` int(11) DEFAULT NULL,
  `fill_gaps` int(11) DEFAULT NULL,
  `whiten` int(11) DEFAULT NULL,
  `min_freq` int(11) DEFAULT NULL,
  `max_freq` int(11) DEFAULT NULL,
  `butter_order` int(11) DEFAULT NULL,
  `filter__a` text,
  `filter__b` text,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sound_detector__band_amplitude__parameter`
--

LOCK TABLES `sound_detector__band_amplitude__parameter` WRITE;
/*!40000 ALTER TABLE `sound_detector__band_amplitude__parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `sound_detector__band_amplitude__parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sound_detector__band_amplitude__value`
--

DROP TABLE IF EXISTS `sound_detector__band_amplitude__value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sound_detector__band_amplitude__value` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `start` float DEFAULT NULL,
  `stop` float DEFAULT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sound_detector__band_amplitude__value`
--

LOCK TABLES `sound_detector__band_amplitude__value` WRITE;
/*!40000 ALTER TABLE `sound_detector__band_amplitude__value` DISABLE KEYS */;
/*!40000 ALTER TABLE `sound_detector__band_amplitude__value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sound_user`
--

DROP TABLE IF EXISTS `sound_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sound_user` (
  `sound_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `sound_id` (`sound_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sound_user`
--

LOCK TABLES `sound_user` WRITE;
/*!40000 ALTER TABLE `sound_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `sound_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sync`
--

DROP TABLE IF EXISTS `sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `http_method` varchar(255) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `http_request_headers` varchar(255) DEFAULT NULL,
  `added` varchar(255) DEFAULT NULL,
  `updated` varchar(255) DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `added_count` int(11) DEFAULT NULL,
  `updated_count` int(11) DEFAULT NULL,
  `deleted_count` int(11) DEFAULT NULL,
  `peer_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sync`
--

LOCK TABLES `sync` WRITE;
/*!40000 ALTER TABLE `sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tagging`
--

DROP TABLE IF EXISTS `tagging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tagging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `taggable_type` varchar(255) DEFAULT NULL,
  `taggable_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tagging`
--

LOCK TABLES `tagging` WRITE;
/*!40000 ALTER TABLE `tagging` DISABLE KEYS */;
/*!40000 ALTER TABLE `tagging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `task` text,
  `priority` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `worker` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `progress` float DEFAULT NULL,
  `report` text,
  `last_update` datetime NOT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `crypted_password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `remember_token_expires_at` datetime NOT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `activated_at` datetime NOT NULL,
  `state` varchar(255) DEFAULT 'passive',
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-02-01 14:59:11
