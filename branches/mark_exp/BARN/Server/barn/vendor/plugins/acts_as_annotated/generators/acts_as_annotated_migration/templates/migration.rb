class ActsAsAnnotatedMigration < ActiveRecord::Migration
  def self.up
    create_table :notes do |t|
      t.column :title, :string
      t.column :body, :text
      # hash is a reserved keyword for activerecord
      t.column :content_hash, :text
      t.column :created_at, :timestamp
    end

    create_table :annotations do |t|
      t.column :note_id, :integer
      t.column :annotated_id, :integer

      # You should make sure that the column created is
      # long enough to store the required class names.
      t.column :annotated_type, :string

      t.column :user_id, :integer
      t.column :created_at, :timestamp
    end

    add_index :annotation, :note_id
    add_index :annotation, [:annotated_id, :annotated_type]
  end

  def self.down
    drop_table :annotations
    drop_table :notes
  end
end
