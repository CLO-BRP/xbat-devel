# ActsAsRateable
module HKSINTL
  module Acts #:nodoc:
    module Rateable #:nodoc:

      def self.included(base)
        base.extend ClassMethods  
      end

      module ClassMethods
        def acts_as_rateable
          has_many :ratings, :as => :rateable, :dependent => :destroy
          include HKSINTL::Acts::Rateable::InstanceMethods
          extend HKSINTL::Acts::Rateable::SingletonMethods

          after_save :save_rating
        end
      end
      
      # This module contains class methods
      module SingletonMethods
        
        # This assumes both WillPaginate and ActsAsRateable be present
        # This also overrides the WillPaginate method attached by the same name!
        def paginate_by_rating(options = {})
          table = self.table_name
          klass = ActiveRecord::Base.send(:class_name_of_active_record_descendant, self).to_s
          paginate_by_sql("
            SELECT DISTINCT #{table}.*, avg(rating.rating) AS rating
            FROM #{table}
            #{options[:joins] if options[:joins]}
            LEFT JOIN rating ON #{table}.id = rating.rateable_id
            WHERE (LOWER(rating.rateable_type) = '#{klass}' OR rating.rateable_type IS NULL)
            #{"AND #{options[:conditions]}" if !options[:conditions].blank?}
            GROUP BY #{table}.id
            ORDER BY rating #{options[:direction] || 'asc'}", :page => options[:page])
        end
        
        # Helper method to lookup for ratings for a given object.
        # This method is equivalent to obj.ratings
        def find_ratings_for(obj)
          rateable = ActiveRecord::Base.send(:class_name_of_active_record_descendant, self).to_s
         
          Rating.find(:all,
            :conditions => ["rateable_id = ? and rateable_type = ?", obj.id, rateable],
            :order => "created_at DESC"
          )
        end
        
        # Helper class method to lookup ratings for
        # the mixin rateable type written by a given user.  
        # This method is NOT equivalent to Rating.find_ratings_for_user
        def find_ratings_by_user(user) 
          rateable = ActiveRecord::Base.send(:class_name_of_active_record_descendant, self).to_s
          
          Rating.find(:all,
            :conditions => ["user_id = ? and rateable_type = ?", user.id, rateable],
            :order => "created_at DESC"
          )
        end
        
        # Helper class method to lookup rateable instances
        # with a given rating.
        def find_by_rating(rating)
          rateable = ActiveRecord::Base.send(:class_name_of_active_record_descendant, self).to_s
          ratings = Rating.find(:all,
            :conditions => ["rating = ? and rateable_type = ?", rating, rateable],
            :order => "created_at DESC"
          )
          rateables = []
          ratings.each { |r|
            rateables << r.rateable
          }
          rateables.uniq!
        end
      end
      
      # This module contains instance methods
      module InstanceMethods
        # Helper method that defaults the current time to the submitted field.
        def add_rating(rating)
          ratings << rating
        end
        
        def rating_by_user(user)
          if user
            return Rating.find(:first, :conditions => ["rateable_type = ? and rateable_id = ? and user_id = ?", self.class.to_s.downcase, self.id, user.id])
          else
            return nil  
          end
        end
        
        # This method is used in the tests to ensure that a user gets only one rating per object
        def ratings_by_user(user)
          if user 
            return  Rating.find(:all, :conditions => ["rateable_type = ? and rateable_id = ? and user_id = ?", self.class.to_s.downcase, self.id, user.id])
          else
            return nil
          end
        end
        # Helper method that returns the average rating
        # 
        def rating
          average = 0.0
          
          ratings.each { |r|
            average = average + r.rating if !r.blank? and !r.rating.blank?
          } 
          
          if ratings.size != 0
            average = average / ratings.size 
          end
          average
        end
        
        def rating=(value)          
          u = Rating.current_user
          raise "Rating cannot determine current_user. Please set this value." if u.blank?

          @user_rating = rating_by_user(u)
          if @user_rating
            @user_rating.rating = value
          else
            @user_rating = Rating.new(
              :rating => value, 
              :rateable_type => self.class.to_s, 
              :rateable_id => id, 
              :user_id => u.id
            )
          end
        end
        
        def save_rating
          return unless @user_rating
          @user_rating.save!
          true
        end
        
        # Check to see if a user already rated this rateable
        def rated_by_user?(user)
          rtn = false
          if user
            self.ratings.each { |b|
              rtn = true if user.id == b.user_id
            }
          end
          rtn
        end
      end
    end
  end
end
