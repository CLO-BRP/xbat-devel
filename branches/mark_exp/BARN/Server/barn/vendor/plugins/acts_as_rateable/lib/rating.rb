class Rating < ActiveRecord::Base

  include AuthorizationApocalypse::Keymaster
  
  cattr_accessor :current_user
  
  belongs_to :rateable, :polymorphic => true
  belongs_to :user
  
  before_save :capitalize_type
  
  # Helper class method to lookup all ratings assigned
  # to all rateable types for a given user.
  def self.find_ratings_by_user(user)
    find(:all,
      :conditions => ["user_id = ?", user.id],
      :order => "created_at DESC"
    )
  end
  
  #------------------
	# RESTful ACL methods
	#------------------
	
	def self.indexable_by?(user, parents = nil)
    (user)? true : false
  end

	def self.creatable_by?(user, parent = nil)
    (user)? true : false
  end

  def readable_by?(user, parents = nil)
    true
  end

  def updatable_by?(user, parent = nil)
		(user && user.id == self.user.id) ? true : false
  end

  def deletable_by?(user, parent = nil)
		(user && user.id == self.user.id) ? true : false
  end
  
  protected
  
  def capitalize_type
    self.rateable_type.capitalize!
  end
end