// BARN Sound resource

BARN.Sound = new BARN.Resource('Sound', {
    response_format: 'xml',
    schema: {
        resultNode: "sound",
        fields: [
            {key:'id', parser: "number"},
            {key:'name', parser: "string"},
            {key:'channels', parser: "number"},
            {key:'samplerate', parser: "number"},
            {key:'duration', parser: "number"},
            {key:'samples', parser: "number"},
            {key:'created_at', parser: "string"},
            {key:'modified_at', parser: "string"}
        ],
        metaNode: 'metadata',
        metaFields: {
            total_entries:'total_entries'
        }
    }
});


var SoundsController = {
    renderMetadata: function(params){
        var target   = params.target || 'sounds_metadata';
        var metadata = params.metadata;
        
        if(metadata.total_entries){
            document.getElementById(target).innerHTML = "Total sound entries: " + metadata.total_entries + "<br />";    
        }
    },
    
    renderSoundData: function(params){
        var id = params.id;
        YAHOO.util.Connect.asyncRequest('GET', BARN.Sound.generateURI({id:id, format:'js'}), {
            timeout: 7000,
            success: function(response){
                console.log(response.responseText)
                var result = YAHOO.lang.JSON.parse(response.responseText);
                var sound  = result.sound
                console.log(result.sound)
                document.getElementById('sounds_metadata').innerHTML = "" +
                    "<div style='margin-bottom:7px;font-weight:bold;'>" + sound.name + "</div>" +
                    "Channels: " + sound.channels + "<br />" +
                    "Duration: " + sound.duration + "<br />" +
                    "Sample rate: " + sound.samplerate + "<br />" +
                    "<br />Tags: " + sound.tags + "<br />" +
                    '<br />';
            },
            failure: function(response){
                    
            }
        });
    },
  
    renderTreeNodes: function(node, fnLoadComplete){
        
        var uri = BARN.Sound.generateURI({format:'js'});
        
        YAHOO.util.Connect.asyncRequest('GET', uri, {
            timeout: 7000,
            
            // this object is passed back with the response object on success or failure 
            argument: {
                "node": node,
                "fnLoadComplete": fnLoadComplete
            },
            
            success: function(response) {    
                YAHOO.log(response.responseText,'info');
                // responseText here is assumed to be JSON
                var json     = YAHOO.lang.JSON.parse(response.responseText);
                var results  = json.sounds;
                var metadata = json.metadata;
                
                // handle null result case, we are done
            
                if (!results || results.length == 0) {
                  response.argument.fnLoadComplete(); 
                  return;
                }

                // handle non-trivial response

                for (var i=0, j=results.length; i<j; i++) {

                    var node_data = { label: results[i].sound.name, sound_id: results[i].sound.id };
                    
                    var logs_node = new YAHOO.widget.TextNode(node_data, node, false);
                    
                    logs_node.setDynamicLoad( function(node, func){
                        SoundsController.renderSoundData({id:node.data.sound_id}) 
                        LogsController.renderTreeNodes(node, func);
                    }, 1 );
                }
                
                // Mark up some Sound meta
                SoundsController.renderMetadata({metadata:metadata});
                
                // Mark up an Events table for all Sounds 
                EventsController.renderTable({size:15});

                // TODO: figure out what the fuck this is doing
                response.argument.fnLoadComplete();
            },
        
            failure: function(response) {
                YAHOO.log("Failed to process XHR transaction.", "info", "example");
                response.argument.fnLoadComplete();
            }
        }); // YAHOO.util.Connect.asyncRequest
  }
  
};

