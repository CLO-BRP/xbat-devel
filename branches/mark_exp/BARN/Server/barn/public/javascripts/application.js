//-----------------------
// CONFIGURE
//-----------------------

// TODO: consider adding a message to the activity indicator

$.ajaxSetup({
	beforeSend: function (request) {      
		$('#activity-indicator').show();
		//console.log(request);
  },
	complete: function() {
		$('#activity-indicator').hide();
	},
	success: function (request, status) {
		$('#activity-indicator').hide(); // NOTE: this is sometimes called, and complete is not ... for instance when a listing lazy loads
	},
  error: function (request, status) {
    // NOTE: this gets called erroneously ... how ironic
	}
});


//-----------------------
// FLASH NOTICES
//-----------------------

function flash(type, text){
    var flash = $("#flash-" + type);
  
    // stop the running animations
    flash.stop(true); 
  
    // show the notice
    flash.animate({opacity: 1.0}, 0, function(){
        flash.html(text);
    }).show('fast');

    // hide the notice after a 2.5 second delay
    flash.animate({opacity: 1.0}, 2500).hide('slow', function(){$(this).html("")});
};

function flash_notice(text){ 
    flash('notice', text); 
};

function flash_error(text){ 
    flash('error', text); 
};

function close_dialog(dialog_div){
    if (dialog_div.dialog('isOpen')) dialog_div.dialog('close');  
}

function refresh_container(container_id){
    try { 
        $("#refresh-" + container_id).trigger('click'); 
    } catch(e) { 
        console.log(e) 
    }
}


//-----------------------
// RATING
//-----------------------

function enable_ratings(options) {
	options = (typeof options == 'undefined') ? {} : options;

	// Define the options we are using
	var callback = options.callback;
	  
	// NOTE: I have removed livequery for the moment, it was causing a delay in the rendering of the ratings so you would see options for a second, then ratings
    
	$('.render-ratings').each( function() {

		$(this).removeClass('render-ratings');
    
		$(this).rating({
			readOnly: $(this).parent().hasClass("read-only"), 
			 
			callback: function(value, link) {
				var form = this.form;
				form['rating[rating]'].value = value;
				
				var field_data = $("form#" + form.id + " input[name^='rating[']").fieldSerialize();        
				var base_uri   = form.action;
                
				// SAVE OR UPDATE RATING
                      
				if(value){
					$.post(base_uri + '?' + field_data, null, function(data) { 
						var rating = data.rating
						form['rating[id]'].value = rating.id;
						if(callback) callback(rating);
					}, 'json');
					return;
				}
                
				// DELETE RATING
				
				var rateable_id = $("form#" + form.id + " input[name='rating[rateable_id]']").fieldValue();        
				var rating_id   = $("form#" + form.id + " input[name='rating[id]']").fieldValue();
				var uri = '/ratings/' + rating_id + '?_method=delete'
                
				if(rating_id[0] != ''){
					$.post(uri, null, callback);
				}
				
				// Make sure the rating value is cleared from the form
				form['rating[id]'].value = null;
			}
        
		});   
		// $(this).rating
	});  
	// $('.render-ratings').each
}


function render_simple_ratings() {
    
	// NOTE: the first selector means that the element has both classes
    
	$('.render.simple-rating').removeClass('render').each( function () {
        
		//--
		// we get the rating target object and current value from the element
		//--
        
		var target = $(this).attr('target'); var current = $(this).html();
        
		//--
		// create rating markup and replace this element
		//--
        
		var rating = $('<div class="simple-rating">');
        
		for (k = 0; k <= 5; k++) {
			
			if (k == 0) {
				hook = 'class="delete"'; 
			} else if (k <= current) {
				hook = 'class="star-on"';
			} else {
				hook = '';
			}
			
			var link = $('<a ' + hook + '" href="' + target + '/ratings?value=' + k + '" title="' + k + '">' + k + '</a>'); 
						
			// Set the click behavior of the star
						
			$(link).click(function(event){

				$(this).siblings().removeClass('star-on');
				
				$(this).addClass('star-on').prevAll().addClass('star-on');
				
				$.post($(this).attr('href'), null, null, "json");
				
				return false;
			});
			
			// Add the newly created link element to the rating div

			rating.append(link);
			
		}
        
		$(this).replaceWith(rating);
        
		//--
		// set children hover and click behavior
		//--
        
		$('.simple-rating a').hover ( function () {
			$(this).prevAll().addClass('hover');
		}, function () {
			$(this).prevAll().removeClass('hover');
		});
        
	});   
	  
}




//-----------------------
// TABS
//-----------------------

// NOTE: the idempotency 'render' convention may be all we need, not 'livequery'

function render_ajax_tabs () {
    
    $('.render-ajax-tabs').each ( function () {
			
        $(this).removeClass('render-ajax-tabs');
        
        // NOTE: get tabs, update selected, load reference with load indication, and select first tab
        
        var id = this.id;
        
        var tabs = $("#" + id + " .tabs-list li a");
    
        $(tabs).click( function () {

            $(this).parent().addClass('selected').siblings().removeClass('selected');

            $("#" + id + " > .tabs-target").html('Loading ...').load(this.href);
                        
            return false;
        });
                    
        $(tabs[0]).click();

    });
    
}


//-----------------------
// TAGGING
//-----------------------

function enable_editable_tags() {


        
	var submit_tags = function(value, settings) {
        
		// parse the container id to get singular type taggable and object id
  
		var taggable_type = this.id.split("-")[0]; 
        
		var taggable_id   = this.id.split("-")[1];
        
		// pack post request parameters and post
        
		var params = {}; 
        
		params[taggable_type + '[tag_list]'] = value; 
        
		params['_method'] = 'put';
        
		// this is making an assumption about the controller name and is very brittle
        
		$.post( '/' + taggable_type + 's/' + taggable_id, params, function(data){}, 'json');
                
		return(value);
        
	}
    
    
	$('.render-editable-tags').livequery( function() {
    
		$(this).removeClass('render-editable-tags');
		
		var taggable_type = this.id.split("-")[0]; 
		var taggable_id   = this.id.split("-")[1];
		    
    $(this).editable( submit_tags, {
      type: 'text', 
      width: 'auto', 
      tooltip: 'Click to edit',
          
      // NOTE: when it's time to edit we get the form values from this function
          
      data: function (value, settings) {
        var id = '#' + this.id + '-hidden';
        return $(id).text().strip();
      },
          
      // NOTE: after submitting the tags we call this function
                  
      callback: function (value, settings) {
        wrap_tags(this);
      },
          
      autocomplete: {
        url: "/" + taggable_type + "s/tags?autocomplete=true", 
        options: {
          multiple: true,
          minChars:2,
          matchContains:true, 
          matchSubset:true,
          max:50, 
          cacheLength:20,
          width: 260,
          selectFirst: false
        }
      } 
    });
    // $(this).editable  
      
    wrap_tags(this);  
  });
  // $('.render-editable-tags').livequery
}

//-----------------------
// VERBS
//-----------------------

/**
 * Configure the modal jQuery dialog. Requires jQuery dialog plugin.
 *
 * @param {jQuery} form 
 * This is a jQuery object containing the target form
 * @param {Object} options 
 * JSON object containing dialog options
 */
function configure_dialog(form, options){
  	  
	var defaults = {
		'closeOnEscape': true,
		'bgiframe': true,
		'height': 300,
		'width': 500,
		'modal': true,
		'title': 'BARN dialog'
	};
	
	var options = (options) ? options : defaults;
	
	// Loop through all the options to mark up, falling back to defaults as necessary
	$.each(['bgiframe', 'height', 'width', 'closeOnEscape', 'modal', 'title'], function(index, value){  
		if(options[value]){
			form.dialog('option', value, options[value]);
		}
		else{ 
			form.dialog('option', value, defaults[value]);
			options[value] = defaults[value];
		}
	});

	// This seems to actually execute the set options and open the dialog
	form.dialog(options);
}

/**
 * Enable the 'verbs' on the page
 *
 * @param {String} container_id 
 * The DOM id of the containing div where we can find the verbs
 */
function enable_verbs(container_id){

  $('#' + container_id + ' a.verb').click( function () {
	  // idempotentency
	  // this is only removing on click ...
	  $(this).removeClass("render-verb"); 

		var selected = get_selected_checkboxes('#' + container_id); 

		var ids = [];

		$.each(selected, function(k, val){
      var split  = val.split("-");
      var id     = split[split.length - 1];
  		ids.push(id);
		});
		
		// Construct the URI
    
    var path  = $.url.setUrl(this.href).attr("path");
    var query = $.url.setUrl(this.href).attr("query");
    var set   = "{" + ids.join(',') + "}";
    var uri   = path + "/" + set + "?" + query;

		// There was nothing selected, return.
		
    if(set == "{}"){
      flash_error("Nothing selected.");
      return false;
    }

		// Prepare the dialog window
		
		var form = $('#verb-form');
		var link = $(this);
		
		// Options are held as a JSON string in a link attribute
    var options  = link.attr('dialog_options')  
    options = (options) ? $.parseJSON(options) : {};
    // Add the title
    options['title'] = link[0].innerHTML;
    
    // Now make the request for the intermediary form...

		$.get(uri, function (data) {			
			form.html(data); 
			configure_dialog(form, options);

  		// open the form if its closed
  		if( form.dialog( 'isOpen' ) == false) form.dialog( 'open' );        
		}, 'text');
		
		return false;
	});
}

// TODO: we should allow for deleting of individual tags

function wrap_tags (context) {
    
    // we may wrap through a selector or for a particular element (the latter is used in the callback
    
    if (context) {
        targets = context;
    } else {
        targets = $('.tags');
    }
        
    $(targets).each( function () {
			
        // get the tag list string
        
        var tag_list = $(this).html();
        
        // split the tag list string and wrap in tag classed spans
        
        var tag = tag_list.split(","); 
        var output = '';
        
        output += '<ul class="tag_list">';
        
        for (k = 0; k < tag.length; k++) {
          if(tag[k].strip() != ''){
            output += '<li class="tag">' + tag[k] + '</li>';	 
          }
        }
        
        output += '</ul>';
        
        // store the tag list string in the hidden element, the editing representation
        
        if (tag_list != 'Click to edit') {
            key = '#' + this.id + '-hidden'; 
            $(key).html(tag_list);
        } else {
            output = '<ul class="tag_hint"><li>Click to edit</li></ul>';
        }
        
        // display the span wrapped and styled version of the tags
        
        $(this).html(output);
			
	});
    
}

//-----------------------
// FILTERING
//-----------------------

// NOTE: this code relies on a naming convention applied to the result target div

function enable_filter_forms () {
		
    $('.render-filter-form').livequery( function () {
        
        $(this).removeClass('render-filter-form').submit( function () {
            
            var action = $(this).attr('action');
            
            var selector = '#' + $(this)[0].id.split('-')[1];
						
            $.get(action, {query: $('#query', this).val()}, function(response){
                $(selector).replaceWith(response)
            });
            
            return false;
        
        });
		
	});
}

//-----------------------
// TOGGLE AND SELECTION
//-----------------------

// NOTE: currently these rely on similar aspects of the markup

function enable_toggle_controls() {

    $('.render-toggle-control').livequery( function () {
            
        $(this).removeClass('render-toggle-control');
        
        $('span.ui-icon', this).click( function () {
            
            var state = $(this).hasClass('ui-icon-plus');
            
            if (state) {
                 $(this).removeClass('ui-icon-plus').addClass('ui-icon-minus');
            } else {
                 $(this).removeClass('ui-icon-minus').addClass('ui-icon-plus');
            }
            
            // TODO: this can be simplified using attributes rather than the additional span markup

            var config = $(this).next();
                                    
            if (config.hasClass('targets')) {
                    
                var targets = $('.' + config[0].innerHTML);
                
                var children = targets.children();
                
                if (state) {
                    targets.show(); $(children[1]).load($(this).attr("uri"));
                } else {
                    targets.hide();  $(children[1]).html("<p>Loading ...</p>");
                }
                    
            }
        });
            
    });

}

function enable_row_selection() {
    
		$("input:checkbox.render-select-row").livequery('click', function () {
				row_selection(this);
		});

}

function row_selection (cx) {

		var row = $(cx).parent().parent();
		
		var related = '.' + $(cx).attr('targets');

		if (cx.checked) {
			row.addClass('selected'); $(related).addClass('selected');
		} else {
			row.removeClass('selected'); $(related).removeClass('selected');
		}		

}

//--
// CHECKBOX_SELECT_ALL and CHECKBOX_SELECT_NONE - do just what they say to properly classed checkboxes
//--

function checkbox_select_all (context) {
		
		var selector = '.selection-checkbox';
		
		if (context) {
				selector = context + ' ' + selector;
		}
		
		$.each($(selector), function(k, item) {
			item.checked = true; row_selection(item);
		});
		
}

function checkbox_select_none (context) {
		
		var selector = '.selection-checkbox';
		
		if (context) {
				selector = context + ' ' + selector;
		}
		
		$.each($(selector), function(k, item) {
			item.checked = false;	row_selection(item);
		});
		
}

//--
// GET_SELECTED_CHECKBOXES - return array of selected checkbox identifiers
//--

function get_selected_checkboxes(container_id) {
		
		var selected = $(container_id +' .selection-checkbox:checked'); var labels = [];
		
		$.each(selected, function(k, item) {
				labels[labels.length] = item.id;
		});
				
		return labels;

}

function add_to_project() {
		
    //--
    // get selected checkboxes, return if no selection
    //--
    
    var labels = get_selected_checkboxes();
    
    if (labels.length == 0) {
        return false;
    }
    
    //--
    // build request template from selection
    //--
            
    // NOTE: the identifiers are current of the form 'selected-{type}-{type_id}'
    
    for (k = 0; k < labels.length; k++) {		
        var part = labels[k].split('-'); labels[k] = 'add_sounds[]=' + part[2];
    }
        
    uri = '/projects/{project_id}?_method=put&' + labels.join('&');
        
    //--
    // present form to get project
    //--
    
    $('body').append('<div style="display: block;" id="add-to-project" title="Add to project"></div>')
        	
    $.get('/projects?partial=projects/add_to_project', {uri: uri}, function (data) {
        $('#add-to-project').html(data); 
    }, 'text');
    
    $("#add-to-project").dialog({
        bgiframe: true,
        height: 300,
        width: 500,
        modal: true,
        closeOnEscape: true
    });
		
	return false;
		
}

//-----------------------
// UTILITY
//-----------------------

// NOTE: we user this to turn paths into proper identifiers that we can use in selectors

function dasherize_path(uri){
    
    // This is used because paths begining with / are interpreted with full protocol ('http://')
    var path_array = $.url.setUrl(uri).attr("path").split('/');
    
    // splice returns the removed element, so no chaining
    path_array.splice(0,1); 
    
    var path = path_array.join("-");
    
    // account for a possible format extension (.html)
    if( path.split(".").length > 1 ){
        path_array = path.split(".");
        path_array.pop();
        path = path_array.join("");
    }

    return path;
};

/*
    This function works specifically with the will_paginate helper method
    Looks for 2 options: 
    target_id - the DOM element to be replaced with the results of the page request, presumably a container of some sort
    partial - the partial to render, since we do not want the default view
*/
function hijack_pagination_links(options){
    var target_id     = options.target_id;
    var partial_param = "&partial=" + options.partial;
    
    var selector  = '#' + target_id + ' .pagination a';

    $(selector).click( function(){
        // If the partial param is already attached, dont reattach
        if ($.url.setUrl(this.href).param("partial")){
            partial_param = ""
        }
        
        var id = '#' + dasherize_path(this.href);
        
        $.get(this.href + partial_param, null, function(response){
            $(id).replaceWith(response)
        });

    	return false;
    });
}

// NOTE: this was formerly used as a poor man's template engine, very poor

function replace_with_data (template, data) {
		
    var output = template;
    
    for (var field in data) {
        var match = new RegExp('{' + field.toUpperCase() + '}', 'g'); output = output.replace(match, data[field]);
    }

    return output;

}

//--
	// inline player
	//--
	
	// NOTE: the various classes are targeted here, and in inlineplayer.css

function render_inline_player() {
		
		$('a.render-inline-player').removeClass('render-inline-player').addClass('sm2_link').click( function() {
			
		//--
		// handle click
		//--
		
		// NOTE: if we have a player and the url has not changed toggle pause state
				
		if (inline_player && (inline_player.url == this.href)) {

			inline_player.togglePause(); return false;

		}
		
		// NOTE: cleanup and start a new sound
		
		soundManager.destroySound('inline-player');
			
		$('a.sm2_playing').removeClass('sm2_playing');
		
		$('a.sm2_paused').removeClass('sm2_paused');
							
		//--
		// create and play sound
		//--
		
		// NOTE: 'this' is tricky here, there are two. the 'parent' is the container dom node, this allows easy update of various properties
				
		inline_player = soundManager.createSound({
			
			id: 'inline-player', url: this.href,
            
            parent: this, child: $(this).children(),
			
			onplay: function() {
				$(this.options.parent).addClass('sm2_playing');
			},
			whileplaying: function () {                
				var progress = Math.round(this.options.child[1].width * (this.position / this.duration)) + 'px';
				
                // TODO: 800 is the 'max-width' of glider thumbs, this test 'fails' for images near the limit size
                
                // TODO: this switch creates something more reasonable, but the players are not quite uniform yet
                
                if (this.options.child[1].width < 800) {
                    this.options.child[0].style.width = progress;
                } else {
                    this.options.child[1].style.marginRight = progress;
                }
                
                this.options.child[1].style.marginLeft = '-' + progress;
			},
			onpause: function() {
				$(this.options.parent).addClass('sm2_paused');
			},
			onresume: function() {
				$(this.options.parent).removeClass('sm2_paused');
			},
			onfinish: function() {
				$(this.options.parent).removeClass('sm2_playing');
           
				this.options.child[0].style.width = '0px';
				
				this.options.child[1].style.marginLeft = '0px';
                
                // TODO: the resetting of the original margin is coupled with the CSS, this is a problem
                
                if (this.options.child[1].width > 800) {
                    this.options.child[1].style.marginRight = '0.5em';
                }
            }
		});
				
		inline_player.play(); return false;
	
	});
	
}

// NOTE: this is a utility to pause execution

function lapse (interval) {
    var start = new Date();
    
    var current = null;

    do { current = new Date(); } while (current - start < interval);
} 


String.prototype.strip = function(){
  return this.replace(/^\s+/, '').replace(/\s+$/, '');
}








