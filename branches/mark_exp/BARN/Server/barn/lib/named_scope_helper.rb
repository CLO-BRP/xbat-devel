module NamedScopeHelper
	
	def self.included(base)
		base.class_eval do
	
			extend TagMonster::ClassMethods
			
			#------------
			# is_project_asset
			#------------
			
			named_scope :is_project_asset, lambda { |*args|
				return  {}
				
				params = args.first || {}
				
				return {} unless params[:project_id] && self.has_association?(:projects)
				
				{
					:include => [:projects, :project_assets],
					:conditions => "project.id = #{params[:project_id]} AND project_asset.asset_type = #{self.class}"
				}
			}
			
			#------------
			# readable_by
			#------------
			
			# This should typically be overridden in a Model
			named_scope :readable_by, lambda { |*args| 
				return {} if args.nil? || args.first.nil?
				user = (args.first.is_a?(User))? args.first : User.find(args.first)
				{
					:include => [:users], 
					:conditions => ["#{PeerUser.table_name}.user_id = ?", user.id]
				} 
			}

			#------------
			# filtered_by
			#------------

			named_scope :filtered_by, lambda { |*args|
				params = args.first || {}
				
				filter = params[:query]

				return {} if filter.blank?

				# This accounts for a comma seperated list coming from the tags input autocompleter,
				# in effect, ignoring all the previous entries - but could 
				
				filter = filter.split(' ').each {|part| part.strip!};

				# TODO: consider field-match mini-language 'field:value', this requires a richer parse representation

				#--
				# build conditions from parsed query
				#--

				# NOTE: this uses no grouping and OR throughout, more terms means more lenient

				# TODO: consider grouping and connective

				conditions = ""
				
				columns = Set.new column_names

				filter.each do |part|

					if conditions.empty?
						conditions = "#{Tag.table_name}.name LIKE '%#{part}%'"
					else
						conditions << " OR #{Tag.table_name}.name LIKE '%#{part}%'"
					end

					if columns.member? 'name'
						conditions << " OR #{table_name}.name LIKE '%#{part}%'" 
					end

				end

				if !conditions.empty?
					conditions = '(' + conditions + ') AND '
				end

				conditions << "#{Tagging.table_name}.taggable_type = '#{class_name}'";

				#--
				# return scope hash
				#--

				{
					#:include => [:tags, :taggings],
					:joins => [
						"INNER JOIN #{Tagging.table_name} ON #{table_name}.id = tagging.taggable_id",
						"INNER JOIN #{Tag.table_name} ON #{Tagging.table_name}.tag_id = #{Tag.table_name}.id"						
					].join(" "),
					
					:conditions => conditions
				}

			}

			#----------
			# public
			#----------
			
			# PUBLIC SHOULD BE OVERWRITTEN IN THE MODEL IF THERE IS NOT PUBLIC FLAG!!!
			named_scope :public, lambda { |*args|
				{
					:conditions => ["#{table_name}.public = 1 AND #{table_name}.public IS NOT NULL"]					
				}
			}
			
			#----------
			# order_by
			#----------
					
			named_scope :order_by, lambda { |*args|
				params			= args[0] || {}
				order			 = params[:order] || "#{self.table_name}.id"
				direction	 = params[:direction] || "asc"
				includes		= Set.new
				order_array = order.split(".")

				# handle ordering on association field
				if order_array.size > 1
					table = order_array[0]
					order = order_array[1]
					
					 if order_array[0] != self.table_name
						# find the association (finds first, not all)
						association = self.reflect_on_all_associations.find { |a| 
							# polymorphic associations are apparently not candidates for eager loading... ie includes.. and if so, should be handled differenly
							# This is a particular problem with Notes and Annotations and should be revisited
							# TODO: find out why Annotations have a problem with this...
							if a.options[:polymorphic] != true
								a.class_name.constantize.table_name == table
							end
						}
					
						# add it to the includes set
						includes.add(association.name) unless association.blank?
					end
					
					order_clause = "#{table}.#{order} #{direction}"
				end
				
				if order_array.size == 1
					order_clause = "#{self.table_name}.#{order} #{direction}"
				end
				
				case order
					when 'rating'	
						includes.add(:ratings)
						order_clause = "rating.rating #{direction}"
				end

				# magic block used by named_scope ...
				{ 
					:include => includes.to_a,
					:order => order_clause
				}
			}
	
			
			
			#----------
			# with_parent
			#----------
			
			# This will not work for resources that are polymorphically associated, 
			# since there is no explicit association, we cannot properly 'include' them in the query
			# This will, however, work for nested resources that are not polymorphic
			
			named_scope :with_parent, lambda { |*args|
				
				#--
				# initialize and curate conventional parent key params
				#--
				
				params = args.first || {}
								
				parent_id = params.reject do |key, value|
					!key.to_s.include?("_id") || value.blank?
				end
				
				return {} if parent_id.blank?

				# NOTE: we only get rid of the project parentage in the case of multiple parents
				
				if parent_id.keys.size > 1
					parent_id = parent_id.reject {|key, value| key.to_s == "project_id"}
				end
				
				@parent_key	= []; @parent_value = []
				
				parent_id.each{ |key, value|
					@parent_key	<< key; @parent_value << value
				}

				@parent_key	= @parent_key.first; @parent_value = @parent_value.first
				
				# Check to see if the class has been defined at all before trying to scope it
				# it is possible that a param has been sent with an "_id" unknowingly
				
				class_str = @parent_key.to_s.split("_")[0].classify
				
				begin
					parent_class = class_str.constantize
				rescue NameError => e
					barn_logger :info, trace, "Association class constant is strangely unavailable: #{class_str} - #{e}"
					return {} # you suck
				end
				
				association = self.reflect_on_all_associations.find{ |a|		
					if a.options[:polymorphic] != true
						a.class_name == parent_class.to_s 
					end
				}
				
				# If there are no associations explicitly found, return nothing
				if association.blank?
					barn_logger :error, trace, "#{self} was requested with a parent scope of #{class_str} which could not be found. Sorry."
					return {} 
				end
				
				@parent_value = parse_set(@parent_value) if is_set?(@parent_value)
				
				{
					:include => association.name,
					:conditions => "#{parent_class.table_name}.id IN (#{@parent_value.to_a.join(',')})",
				}
			}
			
			#-----------
			# with_conditions
			#-----------
			
			# This is here as a convenience. There may be another way to do this, but 
			# this seemed to be the most immediate - used by data_file_controller
			
			named_scope :with_conditions, lambda { |*args|
				conditions = args.first
				
				{
					:conditions => conditions
				}
			}			
			
			#-----------
			# with_parent_in_set
			#-----------
			
			def self.with_parent_in_set(params = {})
				with_parent( params ).in_set( params[:id] )
			end
						
			#-----------
			# ordered_and_paginated
			#-----------
			
			def self.ordered_and_paginated(params = {})
				page = params[:page] || 1
                
				per_page = params[:per_page] || self.per_page

				if params[:format] == "csv"
					order_by( params )
				else
					order_by( params ).paginate(:page => page, :per_page => per_page)
				end
			end
			
			
			
			
			
			
			#-----------
			# COMPOSITE NAMED SCOPES - these are essentially finders
			
			# Both paginate_by_user and find_by_user will default to public if no user is given.
			# These methods handle resource scoping, set notation, and sorting, (and pagination) if the appropriate params exist. 
			#-----------
	
			#-----------
			# paginate_by_user_with_metadata
			#-----------

			# This method returns a Hash containing the collection, its metadata, and a tags object with counts...

			def self.paginate_by_user_with_metadata(user, params = {})

				# NOTE: for tags, we want the finder BEFORE it filters, otherwise we just get the tag we filtered on

				collection = self.is_project_asset(params).paginate_by_user(:user => user, :params => params)
				
				metadata = pack_metadata(self.is_project_asset(params).find_by_user(:user => user, :params => params).count, params)
				
				tags = self.is_project_asset(params).find_by_user(:user => user, :params => params.merge(:filter => false)).tag_counts_rescoped

				# If there is a query param, we also need to get tag counts for the new filtered collection
				# This section may be able to replace tag_counts_rescoped because it leans on the original tag_counts...
				
				if params[:query]					
					scope = find_by_user(:user => user, :params => params).scope(:find)

					scope[:select] = "#{self.table_name}.id"

					scope[:joins]  = scope[:joins] || ""

					# NOTE: 'construct_finder_sql' concerns itself with SQL concepts, the abstract AR 'include' must be first converted to a 'join'
										
					# this little piece of code is shared by TagMonster.. find a way to collapse it.
					
					if !scope[:include].nil?
						converted_joins = convert_includes_to_joins(scope[:include]) 
						scope[:joins] = self.send(:merge_joins, scope[:joins], converted_joins) unless converted_joins.blank?
						scope[:include] = nil
						# Flatten the joins 
						scope[:joins] = scope[:joins].join(" ") if scope[:joins].is_a?(Array)
					end

					subselect = self.send(:construct_finder_sql, {:select => "#{self.table_name}.id"}.merge(scope))

					filtered_tags = self.tag_counts(:conditions => "tagging.taggable_id IN (#{subselect})")				
				end

				if filtered_tags
					tags.each do |tag|
						filtered_tag = filtered_tags.find{ |filtered_tag| filtered_tag.name == tag.name }
						tag.filtered_count = filtered_tag.count if filtered_tag
					end
				end
								
 				{
					:collection => collection, 
					:metadata => metadata, 
					:tags => tags
				}				
			end		
			
			#-----------
			# paginate_with_metadata
			#-----------

			# This one is only used by the NotesController currently... why? maybe delete?

			def self.paginate_with_metadata(params = {})
				collection = self.is_project_asset(params).with_parent_in_set( params ).filtered_by( params ).ordered_and_paginated( params )
				
				metadata = pack_metadata(self.is_project_asset(params).with_parent_in_set( params ).filtered_by( params ).count, params)
				
				tags = self.is_project_asset(params).with_parent_in_set( params ).tag_counts_rescoped

				{
					:collection => collection, 
					:metadata => metadata, 
					:tags => tags
				}
			end
		
			#-----------
			# paginate_by_user
			#-----------

			def self.paginate_by_user(options = {})
				params = options[:params] || {}
				user	 = options[:user]	 || nil

				# If the user in nil, then return public... just a convenience
				return paginate_by_public( params ) if user.blank?

				# NOTE: 'readable_by' is in the AccessControl helper which should be included along with the NamedScopesHelper				
				with_parent_in_set( params ).readable_by( user ).filtered_by( params ).ordered_and_paginated( params )
			end
			
			#-----------
			# paginate_by_public
			#-----------
			
			def self.paginate_by_public(params)
				with_parent_in_set( params ).public( params ).filtered_by( params ).ordered_and_paginated( params )
			end
			
			#----------
			# find_by_user
			#----------
			
			# User owned, or member projects
			def self.find_by_user(options = {})
				params = options[:params] || {}
				user	 = options[:user]	 || nil
				filter = (!params[:filter].nil? && params[:filter] == false) ? false : true
				
				# If the user in nil, then return public... just a convenience
				return find_by_public( params ) if user.blank?

				if filter
				# readable_by can be found in the AccessControl helper which should be included with the NamedScopesHelper
					with_parent_in_set( params ).readable_by( user ).filtered_by( params ).order_by( params )		
				else
					with_parent_in_set( params ).readable_by( user ).order_by( params )		
				end
			end

			#----------
			# find_by_public
			#----------
			
			# Publically accessible projects
			def self.find_by_public(params)
				filter = (!params[:filter].nil? && params[:filter] == false) ? false : true
				
				if filter
					with_parent_in_set( params ).filtered_by( params ).public( params ).order_by( params )
				else
					with_parent_in_set( params ).public( params ).order_by( params )
				end
			end
				
			#----------	
			# pack_metadata
			#----------
			
			def self.pack_metadata(total_entries, params = {})
				{
					:total_entries => total_entries,
					:per_page => params[:per_page] || self.per_page,
					:page		 => params[:page] || 1
				}
			end
		
			
		end # base.class_eval
	end
end
