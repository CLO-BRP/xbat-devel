# TagMonster will eat your offending methods
# This class is currently mixed-in to NamedScopeHelper, who is in turn mixed into ActiveRecord models.
# It depends upon activerecord_extentions.rb for the convert_includes_to_joins method.

module TagMonster
	
	module ClassMethods
		
		#-----------
		# tag_counts_rescoped
		#-----------
		
		# This method is used as a replacement for the acts_as_taggable tag_counts class method
		# When applied at the end of a scoping chain, it transforms the scope to a Tag finder,
		# returning a set of tags and thier counts for the class and its conditions.
		
		# A primary reason that tag_counts does not work is because it cannot (or does not) convert :include to :joins
		
		def tag_counts_rescoped(options = {})
			scope = scope(:find)

			# SELECT
			
			scope[:select] = "tag.id, tag.name, COUNT(*) AS count"
			
			# CONDITIONS
			
			conditions = "tagging.taggable_type = #{quote_value(name)}"
			scope[:conditions] = self.send(:merge_conditions, conditions, scope[:conditions])

			# GROUP
			
			scope[:group]	= "tag.id, tag.name HAVING COUNT(*) > 0"
			
			# JOINS
			#barn_logger :info, scope[:joins]
			scope[:joins] = scope[:joins].join(" ") if scope[:joins].is_a?(Array)
			scope[:joins] = "" if scope[:joins].nil?			
			
			tagging_joins = "INNER JOIN tagging ON tag.id = tagging.tag_id " +
											"INNER JOIN #{self.table_name} ON #{self.table_name}.id = tagging.taggable_id "
			

			contains_tagging = !scope[:joins].blank? && scope[:joins].is_a?(String) && scope[:joins].match(/INNER JOIN tagging/)

			if contains_tagging
				scope[:joins] = tagging_joins
			else
				scope[:joins] = self.send(:merge_joins, tagging_joins, scope[:joins])			
			end			
	
			#barn_logger :info, "scope[:joins] #{scope[:joins]}"

			# Convert the includes to joins
			if !scope[:include].nil?
				converted_joins = convert_includes_to_joins(scope[:include]) 
				scope[:joins] = self.send(:merge_joins, scope[:joins], converted_joins) unless converted_joins.blank?
				scope[:include] = nil
			end

			# Flatten the joins 
			scope[:joins] = scope[:joins].join(" ") if scope[:joins].is_a?(Array)

			# Create the SQL from the new scoping and execute it

			sql  = Tag.send(:construct_finder_sql, scope)
			tags = Tag.find_by_sql(sql)
			
			return tags
		end

		
		
	end
	# module ClassMethods
	
	module InstanceMethods
		# unused for the moment
	end
	
end