require 'simple_uuid'

class GUIDGenerator
	def before_save(record)
    set_guid(record)
	end
	
	def before_validation(record)
	  set_guid(record)
  end
  
  def set_guid(record)
    if record.respond_to?(:guid) && record.guid.blank?
			record.guid = SimpleUUID.generate
		end
  end
end