module PolymorphicNamedScopeHelper
  
	module ClassMethods
		def parent_resources(params)
			@parent_join_table  = params[:join_table].to_s
 			@parent_label 		  = params[:as].to_s
			# This should always be plural from the standpoint of the through association
			@parent_association = params[:association] || @parent_join_table.pluralize
 		end 

		def extract_parent(params)
			# grab only the params with _id (implying )
			parent_id = params.reject{ |key, value| !key.to_s.include?("_id") }

			# return unmodified if nothing to do, 
			# beware, this returning is as good as a find(:all)
			return [nil, nil] if parent_id.blank?

			# we dont currently support multiple nesting
			if parent_id.size > 1 
				logger.error "You have specified too many association ids. This only supports single nesting."
				parent_id = parent_id.first
			end

			parent_id.each{ |k, v|
				@parent_key   = k
				@parent_value = v
			}

			parent_class = @parent_key.to_s.split("_")[0].classify.constantize

			return @parent_value, parent_class
		end
	end
	
	def self.included(base)
		base.extend ClassMethods
		base.extend SetNotationHelper
		
    base.class_eval do

			# assume the publicity of the parent
			# named_scope :public, lambda { |*args| 
			# 	raise args.inspect
			# 	params = args.first
			# 
			# 	@parent_value, @parent_class = extract_parent(params)
			# 	scope = @parent_class.public( params ).proxy_options.merge({
			# 		:joins => :sounds
			# 	})
			# 	raise scope.inspect
			# 	return scope
			# }


		
			named_scope :with_parent, lambda { |*args|	
	
				if @parent_label.blank? 
					raise "Please use parent_resources(:table_name => x, :as => y) to initialize PolymorphicNamedScopeHelper!"
				end
				
				params = args.first || {}

				@parent_value, @parent_class = extract_parent(params)
				
				return {} if @parent_value.nil? || @parent_class.nil?
				
				@parent_value = parse_set(@parent_value) if is_set?(@parent_value)
				
				if @parent_join_table.blank?
					joins = nil
					conditions = ["#{@parent_label}_type = ? AND #{@parent_label}_id IN (#{@parent_value})", @parent_class.to_s, @parent_value ]
				else
					joins = @parent_association.to_sym
					conditions = ["#{@parent_join_table}.#{@parent_label}_type = ? AND #{@parent_label}_id  IN (#{@parent_value})", @parent_class.to_s ]
				end
				
				{
					:joins => joins,
					:conditions => conditions
				}
			}			
		end
		
		
		
	end
end