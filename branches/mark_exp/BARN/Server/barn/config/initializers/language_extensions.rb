class String
  def to_range
    Range.new(*self.split("-").map{|i|i.to_i})
  end
  def to_boolean
    return true if self == true || self =~ /^true$/i
    return false if self == false || self.nil? || self =~ /^false$/i
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end
