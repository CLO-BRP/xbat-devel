function [parameter, context] = parameter__compile(parameter, context)

% NOISE - parameter__compile

parameter.filter.b = 1;

parameter.filter.a = [1, -(1 - parameter.band)];

