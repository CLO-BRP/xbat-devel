function handles = display_point_operation(pal, parameter)

% display_point_operation - display map function for point operation
% ------------------------------------------------------------------
%
% handles = display_point_operation(pal, parameter)
%
% Input:
% ------
%  pal - palette
%  parameter - parameter
%
% Output:
% -------
%  handles - display objects

%--
% set up axes and grid
%--

handles = get_control(pal, 'map', 'handles'); ax = handles.obj;

scale = 1.05; xlim = scale * [-1, 1]; ylim = xlim;

set(ax, ...
	'xlim', xlim, ...
	'ylim', ylim ...
);

%--
% display guides
%--

handles = [];

color = 0.5 * ones(1, 3);

handles(end + 1) = create_line(ax, 'xguide', ...
	'xdata', xlim, ... 
	'ydata', [0, 0] ...
);

handles(end + 1) = create_line(ax, 'yguide', ...
	'ydata', ylim, ... 
	'xdata', [0, 0] ...
);

set(handles, ...
	'color', color, ...
	'linestyle', ':' ...
);

%--
% display map function
%--

handles(end + 1) = create_line(ax, 'point_operation');

xdata = linspace(-1, 1); 

if ~isempty(parameter.map)
	ydata = parameter.map(xdata, parameter);
else
	ydata = xdata;
end

set(handles(end), ...
	'xdata', xdata, ...
	'ydata', ydata ...
);
