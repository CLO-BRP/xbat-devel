function parameter = parameter__create(context)

% NOTCH - parameter__create

%--
% get initial parameters from parent
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

nyq = context.sound.rate / 2;

parameter.center_freq = 0.5 * nyq;

parameter.order = 48;
