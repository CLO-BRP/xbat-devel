function parameter = parameter__create(context)

% WHITEN - parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% filter design parameters
%--

parameter.order = 16;

parameter.auto_order = 0;

% NOTE: the next two parameters regularize the inverse filter in different ways

parameter.regularize = 0;

nyq = 0.5 * get_sound_rate(context.sound);

parameter.max_freq = 0.75 * nyq;

parameter.lowpass = 0;

%--
% noise description parameters
%--

parameter.noise_log = [];

parameter.use_log = 0;



