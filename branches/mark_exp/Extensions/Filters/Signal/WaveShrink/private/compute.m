function [X, context] = compute(X, parameter, context)

% WAVESHRINK - compute

%--
% pad if needed to get power of two
%--

n = size(X, 1); N = 2^nextpow2(n); pad = N - n;

if pad
	X = [X; zeros(pad, size(X, 2))];
end

%--
% denoise each channel
%--

qmf = MakeONFilter(cellfree(parameter.wavelet), parameter.index);

for k = 1:size(X, 2)
	
	[X(:,k), c(k)] = NormNoise(X(:, k), qmf);
	
	X(:, k) = WaveShrink(X(:, k), parameter.method, 4, qmf) ./ c(k); 
	
end

%--
% keep only relevant part if pad was used
%--

if pad
	X = X(1:n, :);
end