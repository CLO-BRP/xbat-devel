function control = extension_chain_controls(type, parameter, context)

parameter

%----------------------
% SHARED CONTROLS
%----------------------

control = empty(control_create);

control(end + 1) = control_create( ...
	'name', 'extensions', ...
	'alias', title_caps(type), ...
	'style', 'listbox', ...
	'string', parameter.extensions, ...
	'value', parameter.selected, ...
	'space', 0.75, ...
	'lines', 5 ...
);

control(end + 1) = control_create( ...
	'name', {'move_up', 'move_down', 'delete'}, ... 
	'alias', {'Up', 'Down', 'X'}, ...
	'style', 'buttongroup', ...
	'width', 3/4, ...
	'align', 'right', ... 
	'space', -0.25, ...
	'lines', 1.5 ...
);
	
names = get_available_extension_names(type);

control(end + 1) = control_create( ...
	'name', 'add', ...
	'style', 'popup', ...
	'string', names, ... 
	'value', 1 ...
);

%----------------------
% EXTENSION SPECIFIC
%----------------------

if isempty(parameter.extensions)
	return;
end

[ext, ix, context2] = get_browser_extension(type, context.par, parameter.extensions{1});

if isempty(ext.fun.parameter.control.create)
	return;
end

specific = ext.fun.parameter.control.create(ext.parameter, context2);

if isempty(specific)
	return;
end

control(end + 1) = control_create( ... 
	'style', 'separator' ...
);

control = [control(:); specific(:)]';


%-----------------------------------
% GET_AVAILABLE_EXTENSION_NAMES
%-----------------------------------

function names = get_available_extension_names(type)

%--
% get all names
%--

names = get_extension_names(type);

%--
% remove abstract names using capitalization convention
%--

% NOTE: this currently removes an acronym named extension

% TODO: extend convention to require say a composite name with an underscore

% NOTE: base extensions have the pattern '_BASE' in their name

for k = numel(names):-1:1
	
	if strcmp(names{k}, upper(names{k}))
		names(k) = [];
	end
	
end




