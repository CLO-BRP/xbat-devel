function [parameter, context] = parameter__compile(parameter, context)

% BANDPASS - parameter__compile

% [caller, str] = get_caller; disp(['COMPILE: ', str]);

%--
% get rate and nyquist from context
%--

nyq = 0.5 * context.sound.rate;

%--
% compute band description from parameters
%--

LC = parameter.min_freq;

HC = parameter.max_freq;

% NOTE: this handles a call to compile before the callback gets a chance to check validity

% NOTE: this is a failure of the system to fully implement MVC

% NOTE: there are alternatives, ask matt ... they look weird though, ask harold

if LC > (HC - parameter.min_band)
	return;
end

F = [LC - parameter.transition, LC, HC, HC + parameter.transition] ./ nyq;

%--
% set band amplitude and distortion parameters
%--

A = parameter.amplitude;

if ~A(1)
	D = [10^(parameter.stop_ripple / 20), 10^(parameter.pass_ripple / 20), 10^(parameter.stop_ripple / 20)];
else
	D = [10^(parameter.pass_ripple / 20), 10^(parameter.stop_ripple / 20), 10^(parameter.pass_ripple / 20)];
end

%--
% get length
%--

L = round(parameter.length);

if parameter.estimate
	L = [];
end

%--
% design filter
%--

[b, a, L] = design_fir_filter(F, A, D, L);

%--
% store results of compilation
%--

parameter.length = L;

parameter.filter.a = a;

parameter.filter.b = b;


