function ext = bandpass_signal_filter

ext = extension_inherit(mfilename, linear_base_signal_filter);

ext.short_description = 'Bandpass signal using equiripple filter';

ext.category = {ext.category{:}, 'Enhance', 'Frequency', 'Noise-Reduction'};

% ext.version = '';

ext.guid = 'e4e39ef8-3aec-4a67-9ee1-1da465372d62';

ext.author = 'Harold and Matt';

ext.email = '';

% ext.url = '';

