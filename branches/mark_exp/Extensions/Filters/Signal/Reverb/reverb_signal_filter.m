function ext = reverb_signal_filter

ext = extension_create(mfilename);

ext.short_description = '';

ext.category = {'Reverb', 'Multipath'};

ext.version = '0.1';

ext.guid = '2ade115d-7e4b-422d-b8ba-9ba14ed8414d';

ext.author = 'Matt Robbins';

ext.email = '';

ext.url = '';

