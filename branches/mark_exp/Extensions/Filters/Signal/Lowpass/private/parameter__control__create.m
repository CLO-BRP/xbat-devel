function control = parameter__control__create(parameter, context)

% LOWPASS - parameter__control__create

fun = parent_fun(mfilename('fullpath')); control = fun(parameter, context);

[remove, ix] = has_control(control, 'min_freq'); 

if remove
	control(ix) = [];
end
