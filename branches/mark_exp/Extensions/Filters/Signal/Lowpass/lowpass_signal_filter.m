function ext = lowpass_signal_filter

ext = extension_inherit(mfilename, bandpass_signal_filter);

ext.short_description = 'Lowpass signal using equiripple filter';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'fe5e112f-0491-4df3-85d0-62fbd32991a4';

% ext.author = '';

% ext.email = '';

% ext.url = '';

