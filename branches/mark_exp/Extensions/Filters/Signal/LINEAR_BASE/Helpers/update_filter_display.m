function update_filter_display(callback, context, opt)

% TODO: we could improve the display of highpass and lowpass by adding a transition label

% TODO: clean up this code as it develops

%-------------------------
% SETUP
%-------------------------

if nargin < 3
	opt.label = 1;
end

pal = callback.pal.handle;

ext = context.ext;

if isfield(ext.parameter, 'label_response');
	opt.label = ext.parameter.label_response;
end

%--
% get 'filter' control parent axes handle
%--

ax = findobj(pal, 'type', 'axes', 'tag', 'filter');
 
% NOTE: this should perhaps show up as an error

if isempty(ax)
	return;
end

% rate = get_sound_rate(context.sound);

rate = context.sound.rate;

%------------------------------------------------
% UPDATE CONTROL VALUES
%------------------------------------------------

%--
% update filter control
%--

set_control(pal, 'filter', 'value', ext.parameter.filter);

%------------------------------------------------
% UPDATE FILTER DISPLAY
%------------------------------------------------

%-------------------------------
% SETUP
%-------------------------------

%--
% set display colors
%--

% NOTE: eventually consider axes display here

guide_color = 0.5 * ones(1,3);

%--
% copy filter for convenience
%--

parameter = ext.parameter;

filter = parameter.filter;

%--
% prepare display axes
%--

axes(ax); hold on;

delete(get(ax, 'children'));

%-------------------------------
% DISPLAY
%-------------------------------

% NOTE: update plot based on display control state

[ignore, type] = control_update([], pal, 'display'); 

% NOTE: in some cases this control will not exist, default to 'frequency' response display

if isempty(type)
	type = 'frequency'; db = 0;
else
	db = strcmpi(type{1}(end - 2:end - 1), 'db'); type = lower(strtok(type{1}, ' '));
end

if opt.label
	xtick = nan * ones(1, 3);
end

switch type
	
	%-------------------------------
	% FREQUENCY RESPONSE
	%-------------------------------
	
	case 'frequency'
		
		%--
		% compute and plot frequency response
		%--
		
		[h, f] = freqz(filter.b, filter.a, 512, rate);
		
		response = abs(h);
		
		if db
			response = 10 * log10(response); 
		end
		
		plot(f, response, 'k-');
		
		%--
		% compute and set axes limits
		%--
		
		% NOTE: the vertical limits are based on the distortion specified
		
		xlim = [0, 0.5 * rate]; 
		
		if db
			ylim = clip_to_range(fast_min_max(response), [-80, 10]) + 6 * [-1, 1];
		else
			ylim = [-0.2, max(1.2, 1.2 * max(abs(h)))];
		end
		
		set(ax, ...
			'ylim', ylim, 'xlim', xlim ...
		);

		%--
		% display basic guides
		%--
		
		% NOTE: display one and zero lines
		
		if db
			
			plot(xlim, [0 0], ':', 'color', guide_color); 
			
			if isfield(parameter, 'stop_ripple')
				
				plot(xlim, 0.5 * parameter.stop_ripple * [1 1], ':', 'color', guide_color);
				
			else
				
				level = -10 * [4, 8, 12, 16];
				
				for k = 1:numel(level)
					plot(xlim, 0.5 * level(k) * [1 1], ':', 'color', guide_color);
				end
				
			end
			
		else
			
			plot(xlim, [1 1], ':', 'color', guide_color); plot(xlim, [0 0], ':', 'color', guide_color);
		
		end
		
		%--
		% display special guides
		%--
		
		% NOTE: it may not be the control we check for

		if has_control(pal, 'min_freq')

			min_freq = get_control(pal, 'min_freq', 'value');
			
			handle = line(min_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);

			make_guide_control(handle, 'min_freq');
			
			if has_control(pal, 'transition')
				
				trans = get_control(pal, 'transition', 'value');
				
				handle = line((min_freq - trans) * [1 1], ylim, ...
					'parent', ax, 'linestyle', ':', 'color', guide_color ...
				);

				make_guide_control(handle, 'transition::min_freq');
				
			end
			
			if opt.label
				xtick(1) = min_freq;
			end
			
		end

		if has_control(pal, 'max_freq')

			max_freq = get_control(pal, 'max_freq', 'value');
			
			handle = line(max_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);

			make_guide_control(handle, 'max_freq');
			
			if has_control(pal, 'transition')
				
				trans = get_control(pal, 'transition', 'value');
				
				handle = line((max_freq + trans) * [1 1], ylim, ...
					'parent', ax, 'linestyle', ':', 'color', guide_color ...
				);
			
				make_guide_control(handle, 'transition::max_freq');
			
			end
			
			if opt.label
				xtick(3) = max_freq;
			end

		end
		
		if has_control(pal, 'center_freq')

			center_freq = get_control(pal, 'center_freq', 'value');

			handle = line(center_freq * [1 1], ylim, ...
				'parent', ax, 'linestyle', ':', 'color', guide_color ...
			);
		
			make_guide_control(handle, 'center_freq');
			
			if opt.label
				xtick(2) = center_freq;
			end

		end
		
		%--
		% display labels
		%--
		
		if opt.label	
			display_freq_labels(ax, xtick);
		end
		
	%-------------------------------
	% IMPULSE RESPONSE
	%-------------------------------
	
	case 'impulse'
	
		%--
		% plot fir impulse response
		%--
		
		[h, t] = impz(filter.b, filter.a);
		
		plot(t, h, 'k-');
		
		%--
		% compute and set axes limits
		%--
		
		% NOTE: we update axes limits to be centered and fairly tight

		if (length(t) > 1)
			xlim = fast_min_max(t); ylim = fast_min_max(h) + 0.1 * [-1, 1];
		else
			xlim = [-0.5, 0.5]; ylim = [0, h] + 0.1 * [-1, 1];
		end
		
		set(ax, 'ylim', ylim, 'xlim', xlim);

		%--
		% display guides
		%--
		
		% NOTE: display zero line

		tmp = plot(xlim, [0 0], ':'); set(tmp, 'color', guide_color);
		
		%--
		% display label
		%--
		
		if opt.label
			xlabel(ax, ['N = ', int2str(impzlength(filter.b, filter.a))]);
		end
		
end


%--------------------------------------
% MAKE_GUIDE_CONTROL
%--------------------------------------

function make_guide_control(handle, control)

% TODO: we may want to enforce a unique control constraint in parent axes

% NOTE: we update the plot data so we can add a marker at the center point

xdata = get(handle, 'xdata'); xdata = xdata(1) * ones(1, 3);

ydata = get(handle, 'ydata'); ydata = ydata + [-10, 10]; ydata = [ydata(1), mean(ydata), ydata(2)];

set(handle, ...
	'tag', control, ...
	'clipping', 'on', ...
	'xdata', xdata, ...
	'ydata', ydata, ...
	'marker', 'none', ...
	'buttondownfcn', {@guide_control_callback, 'start', control} ...
);


%--------------------------------------
% GUIDE_CONTROL_CALLBACK
%--------------------------------------

function guide_control_callback(obj, eventdata, phase, control) %#ok<INUSL>

%--
% create persistent stores for edit
%--

persistent GUIDE_HANDLE GUIDE_AXES GUIDE_RANGE;

persistent RELATED_HANDLE;

persistent CURRENT_VALUE;

%--
% consider phase
%--

switch phase
	
	% NOTE: the callback object here is the guide line
	
	case 'start'
		
		%--
		% get various handles and values needed for editing
		%--
		
		% NOTE: get values from palette controls
		
		pal = ancestor(obj, 'figure');
		
		value = get_control_values(pal);
		
		ax = ancestor(obj, 'axes'); 
		
		xlim = get(ax, 'xlim'); nyq = xlim(2);
		
		need = {'min_freq', 0; 'max_freq', nyq; 'transition', 0};
		
		for k = 1:size(need, 1)
			
			if ~isfield(value, need{k})
				value.(need{k, 1}) = need{k, 2};
			end	
			
		end
		
		%--
		% compute edit range and get related handle if available
		%--
		
		switch control
			
			case 'min_freq'	
				range = [value.transition, value.max_freq]; related = 'transition::min_freq';
				
			case 'center_freq'
				range = get_control(pal, control, 'range'); related = [];
				
			case 'max_freq'
				range = [value.min_freq, nyq - value.transition]; related = 'transition::max_freq';
				
			case 'transition::min_freq'
				range = [0, value.min_freq]; related = 'transition::max_freq';
				
			case 'transition::max_freq'
				range = [value.max_freq, nyq]; related = 'transition::min_freq';
				
		end
		
		if ~isempty(related)
			related = findobj(ax, 'tag', related);
		end
		
		%--
		% set persistent stores for use during move and stop
		%--
		
		GUIDE_HANDLE = obj; GUIDE_AXES = ax; GUIDE_RANGE = range;
		
		RELATED_HANDLE = related; 
		
		CURRENT_VALUE = value;
		
		%--
		% set window button callbacks
		%--
		
		set(pal, ...
			'pointer', 'left', ...
			'WindowButtonMotionFcn', {@guide_control_callback, 'move', control}, ...
			'WindowButtonUpFcn',     {@guide_control_callback, 'stop', control} ...
		);
		
	% NOTE: the callback object here is the palette figure
	
	case 'move'
		
		%--
		% compute frequency position considering range
		%--
		
		value = get(GUIDE_AXES, 'CurrentPoint'); 
		
		value = clip_to_range(value(1), GUIDE_RANGE);
		
		%--
		% update guide display
		%--
		
		set(GUIDE_HANDLE, 'xdata', value * ones(1, 3));
		
		%--
		% update related control and guide display
		%--
		
		switch control

			case 'min_freq', related = value - CURRENT_VALUE.transition;

			case 'max_freq', related = value + CURRENT_VALUE.transition;

			case 'transition::min_freq'

				value = CURRENT_VALUE.min_freq - value; control = 'transition';

				related = CURRENT_VALUE.max_freq + value;

			case 'transition::max_freq'

				value = value - CURRENT_VALUE.max_freq; control = 'transition';

				related = CURRENT_VALUE.min_freq - value;

		end

		if ~isempty(RELATED_HANDLE)
			set(RELATED_HANDLE, 'xdata', related * ones(1, 3));
		end

		set_control(obj, control, 'value', value);

		% EXPERIMENTAL CODE, mostly works
		
		% TODO: implement this in a clean way, integrate singleton into display helper
		
% 		delete(findobj(GUIDE_AXES, 'tag', 'freq_labels'));
% 		
% 		h = display_freq_labels(GUIDE_AXES,  [nan, round(value), nan]);
% 		
% 		set(h, 'tag', 'freq_labels');
		
		%--
		% update frequency response
		%--
		
		% TODO: we could get the line, design the filter, and update
				
	case 'stop'
		
		%--
		% clear window button callbacks
		%--
		
		set(obj, ...
			'pointer', 'arrow', ...
			'WindowButtonMotionFcn', [], 'WindowButtonUpFcn', [] ...
		);
	
		%--
		% get line frequency position
		%--

		freq = get(GUIDE_HANDLE, 'xdata'); freq = freq(1);

		%--
		% set control
		%--

		% TODO: factor this and use during 'move' without callback
		
		% TODO: it should be possible to only update the response during 'move'
		
		switch control

			case {'min_freq', 'center_freq', 'max_freq'}

				set_control(obj, control, 'value', freq);
				
				control_callback([], obj, control);
				
			case {'transition::min_freq', 'transition::max_freq'}
				
				guide = parse_tag(control, '::', {'guide', 'side'});
				
				if strcmp(guide.side, 'min_freq')
					transition = CURRENT_VALUE.min_freq - freq;
				else
					transition = freq - CURRENT_VALUE.max_freq;
				end
				
				set_control(obj, 'transition', 'value', transition);
				
				control_callback([], obj, 'transition');

		end
		
end
