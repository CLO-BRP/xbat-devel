function result = parameter__control__callback(callback, context)

% LINEAR_BASE - parameter__control__callback

result = struct;

%--
% compile an updated extension and put it in context
%--

context.ext = get_callback_extension(callback);

%--
% consider the FV callback
%--

if strcmpi(callback.control.name, 'fvtool')
	get_fvtool(callback, context, 1);
end

%--
% update filter display and possibly fvtool
%--

% NOTE: these happen for any control

update_filter_display(callback, context);

% NOTE: the behavior of these figures is different from other figures

update_fvtool(callback, context);


%--------------------------------------
% GET_FVTOOL
%--------------------------------------

function tool = get_fvtool(callback, context, force)

%--
% check if we should force creation
%--

if nargin < 3
	force = 0;
end

%--
% get extension and build tag
%--

ext = context.ext; 

tag = ['FVTOOL::', ext.name, '::', int2str(callback.par.handle)];

%--
% find tool, create if not found
%--

tool = findobj(0, 'tag', tag);

if ~isempty(tool)
	return;
end

if isempty(tool) && force
	tool = fvtool(ext.parameter.filter.b, ext.parameter.filter.a); set(tool, 'tag', tag);
end


%--------------------------------------
% UPDATE_FVTOOL
%--------------------------------------

function update_fvtool(callback, context)

%--
% get extension
%--

ext = context.ext;

%--
% get fvtool figure
%--

% NOTE: this is the parent figure handle

tool = get_fvtool(callback, context);

if isempty(tool)
	return;
end

%--
% update fvtool state and display
%--

% NOTE: this code actually works with the API

data = getappdata(tool);

% TODO: move the exceoption handler to cover the whole function

try
	tool = data.fvtool.handle;
catch
	nice_catch(lasterror, 'WARNING: Failed to get ''fvtool'' handle.'); return;
end

filt = get(get(tool, 'Filters'), 'Filter');

set(filt, 'Numerator', ext.parameter.filter.b);

set(filt, 'Denominator', ext.parameter.filter.a);

setfilter(tool, filt);



