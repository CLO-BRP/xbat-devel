function ext = linear_base_signal_filter

ext = extension_create(mfilename);

ext.short_description = 'Base for linear signal filters';

ext.category = {'Linear'};

ext.version = '0.25';

ext.guid = 'c82fc83d-6832-4e32-a518-c4508e5d7c3e';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

