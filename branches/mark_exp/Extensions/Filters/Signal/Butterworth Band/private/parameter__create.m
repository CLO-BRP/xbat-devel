function parameter = parameter__create(context)

% BUTTERWORTH BAND - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% band parameters
%--

nyq = 0.5 * context.sound.rate;

parameter.min_freq = 0.25 * nyq;

parameter.max_freq = 0.75 * nyq;

%--
% design parameters
%--

parameter.order = 6;
