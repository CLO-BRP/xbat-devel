function parameter = parameter__create(context)

% DIFFERENCE - parameter__create

%--
% get parent parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% add difference order parameter
%--

parameter.order = 1;

parameter.normalize = 1;
