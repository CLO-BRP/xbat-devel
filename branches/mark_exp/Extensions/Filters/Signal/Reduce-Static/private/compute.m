function [X, context] = compute(X, parameter, context)

% REDUCE-STATIC - compute

% TODO: develop true multiple channel computation

switch lower(parameter.quality{1})
	
	case 'low'
		
		% NOTE: we can test whether we need to remove edges, however this is cheap
			
		ix = find(abs(X) > parameter.level); ix = ix(2:end);
	
		X(ix) = 0.5 * (X(ix - 1) + X(ix + 1));
		
	case 'medium'
		
		% TODO: this code has problems ... not clear yet what
		
		ix = find(abs(X) > parameter.level); N = length(X); 
		
		kix = setdiff(1:N, ix); Y = X(kix);
		
		X(ix) = interp1q(kix', Y, ix);
		
	case 'high'
		
end

% NOTE: careful impulse selection is yet to be considered in this code
