function ext = amplify_signal_filter

ext = extension_inherit(mfilename, point_operation_base_signal_filter);

ext.short_description = 'Linearly amplify signal';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '16012601-9391-49ef-96d0-57fd4ee60f5b';

% ext.author = '';

% ext.email = '';

% ext.url = '';

