function ext = midrange_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Local midrange';

ext.category = {ext.category{:}, 'Statistical'};

% ext.version = '';

ext.guid = 'f0d20c3f-c6ef-4900-bec1-7b243cd74f83';

% ext.author = '';

% ext.email = '';

% ext.url = '';

