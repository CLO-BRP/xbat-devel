function parameter = parameter__create(context)

% MORPHOLOGICAL_BASE > parameter__create

%--
% structuring element type
%--

% NOTE: the default type value is coded in se_types

[types,ix] = se_types;

parameter.style = {types{ix}};

%--
% structuring element dimensions
%--

parameter.width = 3;

parameter.height = 3;
