function [X, context] = compute(X, parameter, context)

% SLICE NORMALIZATION - compute

X = normalize_dim(X, 1, parameter);