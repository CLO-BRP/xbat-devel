function ext = time_filter_base_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Base to adapt signal filters for use as image filters';

% ext.category = {};

ext.version = '';

ext.guid = '734e18ea-4807-42ff-8948-1aa8400f4f41';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

