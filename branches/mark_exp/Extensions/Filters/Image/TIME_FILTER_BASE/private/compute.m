function [X, context] = compute(X, parameter, context)

% TEMPORAL_FILTER_BASE - compute

% NOTE: various of the temporal base methods simply adapt the hosted extension method

% NOTE: the hosted signal filter instance has independent parameters

%--
% return if there is hosted extension available
%--

if trivial(parameter.ext)
	return; 
end

%--
% apply the signal filter along the time dimension of the image
%--

% NOTE: the two transposes here, we should reconsider how we arrange images

[X, context] = parameter.ext.fun.compute(X', parameter, context); X = X';