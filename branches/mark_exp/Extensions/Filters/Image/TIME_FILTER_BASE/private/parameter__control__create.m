function control = parameter__control__create(parameter, context)

% TEMPORAL_FILTER_BASE - parameter__control__create

%--
% return no controls when hosted extension is not available or does not have controls
%--

control = empty(control_create);

if trivial(parameter.ext)
	return;
end 

fun = parameter.ext.fun.parameter.control.create;

if isempty(fun)
	return;
end

%--
% get hosted extension controls
%--

% NOTE: typically filters want to know the rate when creating parameters or controls

% NOTE: we have done this when creating parameters, this is paranoia

context.sound.rate = get_specgram_rate(context.sound.specgram, get_sound_rate(context.sound));

control = fun(parameter, context);