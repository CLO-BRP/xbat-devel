function parameter = parameter__create(context)

% TEMPORAL BANDPASS - parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.hosted = 'Bandpass';

parameter = get_hosted_signal_filter_parameters(parameter, context);
