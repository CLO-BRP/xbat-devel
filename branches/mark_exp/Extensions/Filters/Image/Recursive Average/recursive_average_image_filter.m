function ext = recursive_average_image_filter

ext = extension_create(mfilename);

ext.short_description = 'Simple recursive background estimation and subtraction';

ext.category = {'Linear', 'Statistical'};

ext.version = '0.1';

ext.guid = '9792861b-80e8-4a89-bbba-ee3c0ea7dfbb';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

