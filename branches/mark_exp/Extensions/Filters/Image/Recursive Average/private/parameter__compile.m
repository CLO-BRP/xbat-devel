function [parameter, context] = parameter__compile(parameter, context)

% RECURSIVE AVERAGE - parameter__compile

% NOTE: convert half-life to mixing constant

dt = specgram_resolution( ...
	context.sound.specgram, get_sound_rate(context.sound) ...
);

L = parameter.half_life / dt;

parameter.lambda = 1 - exp(-log(2) / L);
