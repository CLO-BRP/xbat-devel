function [parameter, context] = parameter__compile(parameter, context)

% BINOMIAL - parameter__compile

% NOTE: express binomial filter as decomposed filter from the start

H = filt_binomial(2 * parameter.height + 1, 2 * parameter.width + 1);

parameter.mask = filt_decomp(H);
