function ext = binomial_image_filter

ext = extension_inherit(mfilename, linear_base_image_filter);

ext.short_description = 'Local average using binomial mask';

ext.category = {ext.category{:}, 'Blur', 'Statistical'};

% ext.version = '';

ext.guid = '359aae7d-19e5-4519-b38d-18a1b644308b';

% ext.author = '';

% ext.email = '';

% ext.url = '';

