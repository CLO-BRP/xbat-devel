function ext = min_image_filter

ext = extension_inherit(mfilename, erosion_image_filter);

ext.short_description = 'Local min (erosion)';

ext.category = {ext.category{:}, 'Statistical'};

% ext.version = '';

ext.guid = 'b811bd58-9476-43f1-903c-47d142674546';

% ext.author = '';

% ext.email = '';

% ext.url = '';

