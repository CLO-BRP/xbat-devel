function parameter = parameter__create(context)

% HIGHPASS IN TIME - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.hosted = 'Highpass';

parameter = get_hosted_signal_filter_parameters(parameter, context);
