function ext = dilation_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Morphological dilation (local max)';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '4013d896-1b10-4475-b09b-49c3887fe37f';

% ext.author = '';

% ext.email = '';

% ext.url = '';

