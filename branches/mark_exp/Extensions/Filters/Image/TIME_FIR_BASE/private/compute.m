function [X, context] = compute(X, parameter, context)

% TIME_FIR_BASE - compute

fun = parent_fun(mfilename('fullpath')); [X, context] = fun(X, parameter, context);

% NOTE: we are trying to compensate for the filtering offset, this needs improvement

lag = ceil(numel(parameter.filter.b) / 2);

if ~iscell(X)
	X = shift_back(X, lag);
else
	X = iterate(X, lag);
end


%-----------------
% SHIFT_BACK
%-----------------

function X = shift_back(X, lag)

X = [ X(:, lag:end), zeros(size(X, 1), lag - 1)];
