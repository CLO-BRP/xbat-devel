function fun = signatures

% NOTE: these are the signatures for the children extensions

fun.signatures = {{'fun', 'info'}, {}};

% NOTE: these are the allowed parent extension types for children extensions

fun.parents = {{'types'}, {}};

% NOTE: these are supported actions, analogous to widget events

fun.actions = {{'action'}, {'context'}};

% NOTE: the actions are then parametrized and dispatched

fun.dispatch = {{'result', 'context'}, {'request', 'parameter', 'context'}};

fun.parameter = param_fun;
