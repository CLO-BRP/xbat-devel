function parameter = parameter__create(context)

% SIGNAL FILTER - parameter__create

parameter = struct;

ext = get_active_extension(context.ext.subtype, context.par);

parameter.active = ~isempty(ext) && ismember(context.ext.name, {ext.name});

parameter.fade = 1;
