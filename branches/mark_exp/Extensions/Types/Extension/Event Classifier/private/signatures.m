function [fun, info] = signatures

% EVENT CLASSIFIER - signatures

%--------------
% INFO
%--------------

info.version = '0.2';

info.short_description = '';

%--------------
% API
%--------------

fun.prepare = {{'result'}, {'parameter', 'context'}};

% NOTE: the event is a simple event at the moment

fun.compute = {{'tags', 'value', 'context'}, {'event', 'parameter', 'context'}};

fun.conclude = {{'result'}, {'parameter', 'context'}};

fun.parameter = param_fun;

fun.view = get_extension_type_signatures('widget');

