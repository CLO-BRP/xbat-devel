function fun = signatures

% SIGNATURES - function handle structure
% --------------------------------------
%
% fun = signatures
%
% Output:
% -------
%  fun - structure for extension type API

% NOTE: this will require updating 'extension_create', and possibly 'extension_inherit'

fun.prepare = {{'result', 'context'}, {'parameter', 'context'}};

fun.compute = {{'result', 'context'}, {'event', 'parameter', 'context'}};

fun.conclude = {{'result', 'context'}, {'parameter', 'context'}};

fun.parameter = param_fun;	
