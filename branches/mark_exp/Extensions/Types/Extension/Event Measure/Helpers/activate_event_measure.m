function activate_event_measure(ext, par, active)

%--------------------------------
% HANDLE INPUT
%--------------------------------

if nargin < 2 || isempty(par)
	par = get_active_browser;
end

% NOTE: the default is to toggle the active state

if nargin < 3
	active = ~extension_is_active(ext, par);
end 

%--------------------------------
% UPDATE EXTENSION STATE
%--------------------------------

%--
% set active measure extension
%--

action = ternary(active, 'add', 'del');

set_active_extension(action, ext, par);

%--
% update widget and control palette
%--

pal = get_extension_palette(ext, par);

if ~active
	
	close(find_widget(par, ext));
	
else
	
	% TODO: create the view if the extension state asks us to, when the palette is not available
	
	if ~isempty(pal) && get_control(pal, 'view', 'value')
		create_widget(par, ext);
	end
	
end

if ~isempty(pal)
	set_control(pal, 'active', 'value', active);
	
	set_control(pal, 'view', 'enable', active);
end

%--
% update active menu
%--

% NOTE: this code is coupled with the 'extension_category_menu', this needs to be resolved

top = findobj(par, 'type', 'uimenu', 'label', 'Measure', 'parent', par);

if ~isempty(top)
	
	top = findobj(top, 'type', 'uimenu', 'label', 'Active');

	child = findobj(get(top, 'children'), 'label', ext.name);
	
	set(child, 'check', ternary(active, 'on', 'off'));
	
end

%--
% reset selection to reflect active measure state
%--

[sel, count] = get_browser_selection(par);

if count
	set_browser_selection(par, sel.event);
end