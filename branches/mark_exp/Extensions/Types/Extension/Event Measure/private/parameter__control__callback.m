function result = parameter__control__callback(callback, context)

% EVENT MEASURE - parameter__control__callback

%--
% initialize trivial output and unpack input
%--

result = struct;

% NOTE: check for tabs callback and return promptly

if is_tabs_control(callback.control)
	return; 
end

ext = context.ext; par = callback.par; pal = callback.pal;

%--
% dispatch callback
%--

switch callback.control.name

	case 'active'
		
		activate_event_measure(ext, par.handle, get(callback.obj, 'value'));
		
	case 'start'
		
		%--
		% compute measure for output log
		%--
		
		[log, ix] = get_browser_log(par.handle, ext.control.output_log);
		
		% NOTE: check whether any context fields should be updated, perhaps 'context.log' if it exists
		
		context.active = get_active_extension(get_extension_types, par.handle);
	
		log = compute_log_event_measures(log, [], ext, context);
		
		% NOTE: the call to 'log_save' timestamps the last changes to the log, it may not be needed
		
		%--
		% save results
		%--
		
		set_browser_log(par.handle, log_save(log), ix);
		
		%--
		% update display
		%--
		
		% TODO: implement display refresh that is lighter than 'browser_refresh'
		
	case 'view'
		
		%--
		% delete view widget
		%--
		
		if ~get(callback.obj, 'value')
			close(find_widget(par.handle, ext)); return;
		end
		
		%--
		% initialize view widget
		%--
		
		% NOTE: we also check whether the extension is active here
		
		if get_control(pal.handle, 'active', 'value')
			create_widget(par.handle, ext, context);
		end
		
	case 'overlay'
		
		% TODO: do we need persistence code for the marker?
		
		selection = get_browser_selection(par.handle);
		
		marker = get_browser_marker(par.handle);
		
		browser_display(par.handle, 'events');
		
		if numel(selection.event)
			set_browser_selection(par.handle, selection.event); 
		end
		
		set_browser_marker(par.handle, marker);
		
	otherwise
		
		% NOTE: this is here for now to remind us of unimplemented callbacks 
		
		db_disp(['CALLBACK: ', callback.control.name]); stack_disp; flatten(callback)
		
end
