function attribute = create(context)

% SENSOR_GEOMETRY - create

if ~has_toolbox('M_Map')   
    install_toolbox('M_Map');    
end

attribute = struct;

channels = context.sound.channels;

attribute.local = default_geometry(channels);

attribute.global = [];

attribute.offset = [];

attribute.ellipsoid = [];