function attribute = load(store, context)

% SENSOR_GEOMETRY - load

if ~has_toolbox('M_Map')   
    install_toolbox('M_Map');    
end

attribute = sensor_geometry(store);

if isempty(attribute)
	return;
end

if size(attribute.local, 2) < 3
	attribute.local(:, 3) = 0;
end

if ~isfield(attribute, 'global')
	return;
end

if ~isempty(attribute.global) && size(attribute.global, 2) < 3
	attribute.global(:, 3) = 0;
end