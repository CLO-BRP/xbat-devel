function [geometry, ref, cancel] = georeference_geometry(geometry, ref)

if nargin < 2 || isempty(ref)
    ref = 1;
end

if ~has_toolbox('M_Map')
    cancel = 1; return;
end

cancel = 0;

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
    'style', 'separator', ...
    'type', 'header', ...
    'string', 'Coordinates' ...
);

channel_str = {};

for k = 1:size(geometry.local, 1)
	channel_str{end + 1} = int2str(k);
end

control(end + 1) = control_create( ...
    'name', 'reference', ...
    'style', 'popup', ...
    'string', channel_str, ...
    'value', ref ...
);

control(end + 1) = control_create( ...
    'name', 'lat', ...
    'style', 'edit' ...
);

control(end + 1) = control_create( ...
    'name', 'lon', ...
    'style', 'edit' ...
);
    
result = dialog_group('Set Reference Coordinates ...', control);

if ~strcmp(result.action, 'ok')
    cancel = 1; return;
end

ref = str2double(result.values.reference);

%--
% store local geometry with (possibly) new reference
%--

local = geometry.local - ones(size(geometry.local, 1), 1)*geometry.local(ref, :);

%--
% use sync to get reference
%--

geometry.global(1) = str2double(result.values.lat);

geometry.global(2) = str2double(result.values.lon);

geometry.global(3) = 0;

geometry.local = [0 0 0]; geometry.ellipsoid = 'wgs84'; geometry.offset = [];

geometry = sync_geometry(geometry, 'global', 1);

%--
% update other channels also sync
%--

geometry.local = local;

geometry.global(ref, :) = geometry.global;

geometry = sync_geometry(geometry, 'local', ref);










