function attribute = create(context)

% TIME_STAMPS - create

attribute = struct;

attribute.table = [0, 0];

attribute.enable = 1;

attribute.collapse = 0;