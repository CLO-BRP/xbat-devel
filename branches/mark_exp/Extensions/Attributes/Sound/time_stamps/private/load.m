function attribute = load(store, context)

% TIME_STAMPS - load

attribute = struct;

attribute = time_stamps(store);

if isfield(context.sound.time_stamp, 'enable') && isfield(context.sound.time_stamp, 'collapse')
	
	attribute.enable = context.sound.time_stamp.enable;
	
	attribute.collapse = context.sound.time_stamp.collapse;
	
else
	
	attribute.enable = 1;
	
	attribute.collapse = 0;

end