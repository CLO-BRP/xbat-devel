function save(attribute, store, context)

% TIME_STAMPS - save

lines{1} = 'sound time, stamp time';

lines{2} = 'stamps';

table = attribute.table;

for k = 1:size(table, 1)	
	lines{end + 1} = [num2str(table(k, 1)), ', ', num2str(table(k, 2))];
end

file_writelines(store, lines);

%--
% save configuration options
%--

if isempty(context.library)
    return;
end

sound = context.sound;

sound.time_stamp = attribute;

sound_save(context.library, sound, get_sound_state(sound, context.library));
