function parameter = parameter__create(context)

% EVENT 2 - parameter__create

parameter = struct;

%--
% search control values
%--

% parameter.log = log_name(get_active_log(context.par));

parameter.search = '';

parameter.start = 0;

parameter.stop = get_sound_duration(context.sound);

parameter.order = 'score';

parameter.sql = '';

parameter.limit = 500;

% NOTE: this is the parameter used for paging through results

parameter.offset = 0;

% parameter.rating.value = [];
% 
% parameter.rating.relation = '>';
% 
% parameter.score.value = [];
% 
% parameter.score.relation = '>';
