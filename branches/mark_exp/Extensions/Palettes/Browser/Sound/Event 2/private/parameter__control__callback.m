function result = parameter__control__callback(callback, context)

% EVENT 2 - parameter__control__callback

% TODO: this code should work with multiple logs with the same meaning

%--
% setup
%--

result = struct; pal = callback.pal;

%--
% perform callback
%--

switch callback.control.name
	
	case {'search', 'start', 'stop', 'order'}
		
		%--
		% reset offset unless we are told not to
		%--
		
		if ~isfield(callback, 'no_offset_reset'), current_state(pal.handle, 0); end
		
		%--
		% get basic sql and update sql control
		%--
		
		% NOTE: this does not consider the paging, the LIMIT and OFFSET parts of the query
		
		sql = get_basic_sql(pal.handle);
		
		set_control(pal.handle, 'sql', 'value', sql.query);
		
		%--
		% update query to include paging and execute to get events
		%--
		
		% NOTE: we are working with a single database query for simplicity

		[sql.query, limit, offset] = append_limit_and_offset(sql.query, pal.handle);
		
		log = get_browser_logs(context.par);
		
		for k = 1:numel(log)
			
			file = log_cache_file(log(k)); count(k) = sqlite(file, sql.count); 
			
			if ~count(k).total
				continue;
			end
			
			[status, event{k}]  = sqlite(file, sql.query);
		
		end
		
		db_disp; 
		
		current_state(pal.handle, [], count(1).total); 
		
		%--
		% produce event strings
		%--
		
		L = {};

		for k = 1:numel(log)
			
			if ~count(k).total
				continue;
			end
			
			part = event_to_palette_string(log(k), event{k}); L = {L{:}, part{:}};
			
		end
		
		%--
		% update events control and label, as well as buttons
		%--
		
		handle = get_control_handles(pal.handle, 'events');
		
		set(handle.obj, 'string', L, 'value', 1);

		set_control(pal.handle, 'events', ...
			'label', ['Events (', int2str(offset + 1), '-', int2str(min(count(1).total, offset + limit)), ' of ' int2str(count(1).total), ')'] ...
		);

		update_buttons(pal.handle);
		
	case 'limit'
		
		callback.control.name = 'search';
		
		callback.no_offset_reset = 1;
		
		parameter__control__callback(callback, context);
		
		update_buttons(pal.handle);
		
	case {'previous', 'next'}
		
		%--
		% update offset
		%--
		
		limit = get_control_value(pal.handle, 'limit'); limit = eval(limit{1});
	
		db_disp
		
		current = current_state(pal.handle);
		
		if strcmp(callback.control.name, 'previous')
			current_state(pal.handle, current.offset - limit);
		else
			current_state(pal.handle, current.offset + limit);
		end
		
		update_buttons(pal.handle);
		
		%--
		% updata display
		%--
		
		callback.control.name = 'search';
		
		callback.no_offset_reset = 1;
		
		parameter__control__callback(callback, context);
		
	case 'events'
		
		value = get_control_value(pal.handle, 'events'); 
		
		db_disp 'update palette event display'
		
		if numel(value) > 1
			return;
		end
		
		% TODO: the 'goto_event' function should consider the visibility of the event
		
		if double_click(callback.obj)
			value = str_split(strtok(value{1}, ':'), '#'); goto_event(context.par, value{1}, value{2});
		end
		
	case {'known_tags', 'tags', 'rating'}
		
		%--
		% get and update selection
		%--
		
		selection = get_browser_selection(context.par);
		
		switch callback.control.name

			case 'known_tags'
				
				selection.event.tags = union(selection.event.tags, callback.value); request = 'tags';
				
			case 'tags'
				
				selection.event.tags = str_to_tags(callback.value); request = 'tags';

			case 'rating'
				
				selection.event.rating = callback.value; request = 'rating';
				
		end
		
		selection.event = log_save_events(selection.log, selection.event, request);
		
		%--
		% update display
		%--
		
		browser_display(context.par, 'events');
		
		set_browser_selection(context.par, selection.event, selection.log);	
		
	otherwise
		
		db_disp(['default callback for ', callback.control.name]); stack_disp; disp(' ');
		
end


%--------------------------
% UPDATE_BUTTONS
%--------------------------

function update_buttons(handle)

%--
% get state and current limit
%--

state = current_state(handle);

limit = get_control_value(handle, 'limit'); limit = eval(limit{1});

%--
% update button enable states reasonably
%--

set_control(handle, 'previous', 'enable', state.offset ~= 0);

% TODO: this will have to be updated from the 'limit' callback

set_control(handle, 'next', 'enable', state.offset + limit < state.total);


%--------------------------
% GET_BASIC_SQL
%--------------------------

function sql = get_basic_sql(handle)

% TODO: at the moment normalization is overkill, in particular we consider including 'score' and 'rating' as columns

%--
% get palette control values
%--

parameter = get_control_values(handle);

%--
% get string parts from parameter
%--

start = num2str(parameter.start); stop = num2str(parameter.stop);

order = lower(parameter.order);

if iscell(order)
	part = str_split(order{1}); order = part{2};
end

switch order
	
	case {'rating', 'score'}
		direction = 'DESC';
		
	case 'time'
		order = 'start'; direction = '';
		
	otherwise
		direction = '';
		
end

%--
% build query 
%--

sql.query = [ ...
	'SELECT * FROM event WHERE event.start + event.duration > ', start, ' AND event.start < ' stop, ' ORDER BY event.' order, ' ', direction, ';' ...
];

sql.count = strrep(sql.query, '*', 'COUNT(event.id) AS total');


%---------------------------------
% APPEND_LIMIT_AND_OFFSET
%---------------------------------

function [sql, limit, offset] = append_limit_and_offset(sql, handle)

%--
% get page relevant information from palette
%--

limit = get_control_value(handle, 'limit'); limit = eval(limit{1});

state = current_state(handle); offset = state.offset;

%--
% append query and update page state indicators
%--

% NOTE: remove final semi-colon and possibly trailing space

sql(end) = []; 

if sql(end) == ' '
	sql(end) = []; 
end

sql = [sql, ' LIMIT ', int2str(limit), ' OFFSET ', int2str(offset), ';']; 


%---------------------------------
% CURRENT_STATE
%---------------------------------

function state = current_state(handle, offset, total) 

%--
% get store handle
%--

store = findobj(handle, 'tag', 'events', 'style', 'text');

%--
% get state value, set the first time around
%--

if nargin < 2
	
	state = get(store, 'userdata');
	
	if isempty(state)
		state.offset = 0; state.total = 0; set(store, 'userdata', state);
	end

%--
% set offset value
%--

else
	
	state = current_state(handle);
	
	% NOTE: there is no meaning for negative offsets
	
	if exist('offset', 'var') && ~isempty(offset)
		state.offset = max(0, offset);
	end
	
	if exist('total', 'var') && ~isempty(total)
		state.total = total;
	end
	
	set(store, 'userdata', state);
	
end
