function S = event_to_palette_string(log, event, fields)

% TODO: this needs quite a bit of work, this is taken from 'log_event_str'

%--
% set default fields
%--

if (nargin < 3) || isempty(fields)
	fields = { ...
		'score', 'label', 'channel', 'start_time' ...
	};
end

%--
% generate event string for log
%--

% NOTE: this part of the string is invariant to the fields input

S = strcat(log_name(log), {' # '}, int_to_str([event.id]), {':  '});

%--
% loop over fields included in string
%--

for k = 1:length(fields)
	
	%--
	% handle event fields according to content
	%--
	
	% NOTE: putting strings in cell arrays preserves whitespace in 'strcat'
	
	switch fields{k}

		%------------------------------
		% CODE
		%------------------------------
		
		% NOTE: this is part of making simple annotation a standard rather than an extensible annotation
		
		case 'score'
			
			% prepare fractional percent score string
			
			score = struct_field(event, 'score');
			
			score = round(1000 * score) / 10;
			
			score = cellstr(strcat(num2str(score), '%'))';
			
			S = strcat(S, score);
			
		%------------------------------
		% LEVEL
		%------------------------------
		
		case 'level'
			
			S = strcat(S, {'L = '}, int_to_str(struct_field(event, 'level')));
			
		%------------------------------
		% LABEL
		%------------------------------
		
		case 'label'
			
			%--
			% check for tags
			%--
			
			if ~isfield(event, 'tags')
				continue;
			end
			
			% OLDER COMMENTS
			
			% TODO: use a marked tag as the label '*label tag1 tag2'
			
			% NOTE: allow a single marked tag
			
			T = cell(size(S));
			
			for j = 1:length(event)
				
				if ~isempty(event(j).tags)
					T{j} = tags_to_str(event(j).tags);
				else
					T{j} = '';
				end
				
			end
						
			S = strcat(S, T);
			
		%------------------------------
		% CHANNEL
		%------------------------------
		
		case 'channel'
			
			S = strcat(S, {'Ch = '}, int_to_str([event.channel]));
			
		%------------------------------
		% TIME
		%------------------------------
		
		case 'start_time'
			
			%--
			% extract start time from event array
			%--
			
			t = map_time(log.sound, 'real', 'record', [event.start]);

			start_time = get_grid_time_string([], t, log.sound);
			
			%--
			% append time string to event string
			%--
			
			S = strcat(S, {'T = '}, start_time);

	end
	
	%--
	% separate fields using comma
	%--
	
	if (k < length(fields))
		S = strcat(S, {',  '}); 
	end
		
end