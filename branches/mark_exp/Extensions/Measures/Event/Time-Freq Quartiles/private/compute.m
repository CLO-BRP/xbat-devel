function [value, context] = compute(event, parameter, context)

% TIME-FREQUENCY QUARTILES - compute

%-------------------
% SETUP
%-------------------

value = struct;

context.sound.specgram = parameter.specgram; 

% TODO: we are now getting filtered samples, we do not want this

event = event_specgram(event, context.sound, context);

% NOTE: we use the context to store the computed spectrogram for display

context.state.event = event;

%-------------------
% TIME QUARTILES
%-------------------

%--
% compute local time grid and energy-based weights
%--

% NOTE: scaling by the event rate gives us local time in seconds

local = (1:numel(event.samples)) / event.rate; 

% NOTE: this could be smoother

weight = event.samples.^2; total = sum(weight);

%--
% compute quartiles and express non-median values as offsets
%--

ix = quartile_indices(cumsum(weight) / total);

value.time.low = diff(local(ix([2, 1])));

value.time.median = local(ix(2));

value.time.high = diff(local(ix([2, 3])));

% NOTE: this value is redundant, therefore the corresponding store is not normalized

value.time.range = value.time.high - value.time.low;

% NOTE: this is the width of the second quartile over the inter-quartile range

value.time.asymmetry = -value.time.low / value.time.range;

% NOTE: the median is now shifted to coincide with the actual time

value.time.median = value.time.median + event.time(1);


%-------------------
% FREQ QUARTILES
%-------------------


% event.specgram.value = value;
% 
% event.specgram.freq = freq; event.specgram.time = time;
% 
% event.specgram.rows = [start, stop];


%--
% consider restriction to event band
%--

if parameter.restrict_to_event_band
	
	start = event.specgram.rows(1); stop = event.specgram.rows(2);
	
	event.specgram.freq = event.specgram.freq(start:stop);
	
	event.specgram.value = event.specgram.value(start:stop, :);
	
end

%--
% consider minimum frequency restriction
%--

if parameter.min_frequency
	
	
end

%--
% select section of spectrogram to use for energy distribution
%--

local = event.specgram.freq;

weight = sum(event.specgram.value, 2); total = sum(weight);

%--
% compute quartiles and express non-median values as offsets
%--

ix = quartile_indices(cumsum(weight) / total);

% NOTE: this could perhaps be done with a 'deal'

value.frequency.low = local(ix(1));

value.frequency.median = local(ix(2));

value.frequency.high = local(ix(3));

% NOTE: this value is redundant, therefore the corresponding store is not normalized

value.frequency.range = value.frequency.high - value.frequency.low;

% NOTE: this is the width of the second quartile over the inter-quartile range

value.frequency.asymmetry = (value.frequency.median - value.frequency.low) / value.frequency.range;

%--
% debug display
%--

if context.debug
	
	db_disp(int2str(event.id)); flatten(value)
	
end



