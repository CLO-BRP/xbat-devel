function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% TIME-FREQUENCY QUARTILES - view__on__selection__create

db_disp; stack_disp;

handles = [];

%-------------------
% SETUP
%-------------------

%--
% get event spectrogram
%--

% TODO: consider callback and widget event handling so that we can get the event in the context state

% TODO: the context here does not contain active extension information!

% NOTE: if we don't get the event in the context we must recompute

if isfield(context.state, 'event')
	event = context.state.event; 
else
	event = event_specgram(data.selection.event, context.sound, context);
end

%--
% get measure value
%--

% NOTE: this should come as part of the data, otherwise we have this long yet obvious line everywhere

value = data.selection.event.measure.(context.ext.fieldname);

%-------------------
% DISPLAY
%-------------------

handle = harray_select(widget, 'level', 1);

imagesc(event.specgram.value, 'parent', handle(1));
