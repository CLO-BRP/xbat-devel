function [handles, context] = view__on__selection__create(widget, data, parameter, context)

% FORMANTS - view__on__selection__create

handles = [];

% TODO: this should be a top field in the data input

measure = data.selection.event.measure.(context.ext.fieldname);

%--
% display something
%--

handles = [];

ax = findobj(widget, 'type', 'axes', 'tag', 'top');

if ~isempty(ax)
	
	for k = 1:size(measure.value.freq, 1)
		
		handles(end + 1) = line('parent', ax, ...
			'xdata', measure.value.time, ...
			'ydata', measure.value.freq(k, :), ...
			'linestyle', 'none', ...
			'marker', 'o', ...
			'color', [0.5, 0.16, 0.83].^(1/k) ...
		);
		
	end
	
end