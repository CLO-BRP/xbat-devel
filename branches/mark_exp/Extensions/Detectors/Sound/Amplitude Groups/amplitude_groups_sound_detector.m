function ext = amplitude_groups_sound_detector

ext = extension_inherit(mfilename, band_amplitude_sound_detector);

ext.short_description = 'Consider event grouping in results';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'ee43d15f-0d8f-41dd-8f14-8c0633ca335e';

% ext.author = '';

% ext.email = '';

% ext.url = '';

