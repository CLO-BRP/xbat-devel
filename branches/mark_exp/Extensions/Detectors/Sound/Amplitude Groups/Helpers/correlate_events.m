function C = correlate_events(event, parameter, context)

%--------------------
% SETUP
%--------------------

%--
% set default correlation parameters if needed
%--

if nargin < 2 || isempty(parameter)
	parameter = image_corr; parameter.pad_row = 0; parameter.pad_col = 1;	
end

%--
% extract some context fields
%--

page = context.page; sound = context.sound;

%--
% check for page and required shift
%--

if nargin < 3
	page = [];
end

shift = ~isempty(page); 

%--
% make sure we have all spectrograms
%--

for k = 1:numel(event)
	
	%--
	% compute spectrogram using 'event_specgram'
	%--
	
	% NOTE: spectrogram computation needs proper event record time, in case we need to read sound
	
	% NOTE: this is needed because this function is called within the detector, which keeps events in page time
	
	if shift
		event(k).time = event(k).time + page.start;
	end

	event(k) = event_specgram(event(k), sound, context);

	if shift
		event(k).time = event(k).time - page.start;
	end
	
	% NOTE: append a row and or column to ensure odd sized spectrograms
	
	[rows, cols] = size(event(k).specgram.value);
	
	if ~mod(rows, 2), event(k).specgram.value(end + 1, :) = 0; end
	
	if ~mod(cols, 2), event(k).specgram.value(:, end + 1) = 0; end
	
end
	
%--
% compute spectrogram correlations
%--

for j = 1:numel(event)

	outer = event(j);
	
	for k = j:numel(event)
		
		[C(j, k), P(j, k)] = max(image_corr(event(j).specgram.value, event(k).specgram.value, parameter.opt));
		
	end
	
end

%--
% restore times if we needed to shift
%--
