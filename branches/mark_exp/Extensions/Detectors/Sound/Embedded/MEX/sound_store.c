#include <sndfile.h>
#include <string.h>
#include <stdlib.h>
#include "detect.h"

/*
 * the sample buffer
 */

struct sample_buf {
	sample_t * buf;
	unsigned n_buf;
	int n_k;
	int k;
	int spb;
	sample_t ** buf_k;
} big_buf;

/*
 * mod - modulus function that works the way I expect it to
 *-----------------------------------------------------------*/

static int mod(int k, int M){
	
	int out;
	
	out = k % M;
	
	if (out < 0)
		out = M + out;
	
	return out;
	
}

/*
 * initialize_sample_buffer
 *------------------------------------------------------------*/

int initialize_sample_buffer(int N, int spb){
	
	int k;
	
	/*
	 * give us the next largest multiple of samples_per_buffer 
	 */
	
	big_buf.n_k = (N / spb) + 1;
	
	big_buf.n_buf = big_buf.n_k * spb;
	
	big_buf.buf = calloc(big_buf.n_buf,  sizeof(sample_t));
	
	if (!big_buf.buf)
		return 1;
	
	/*
	 * allocate the circular pointer array and distribute the pointers
	 */
	
	big_buf.buf_k = malloc(big_buf.n_k * sizeof(sample_t *));
	
	if (!big_buf.buf_k)
		return 1;
	
	for (k = 0; k < big_buf.n_k; k++){
		big_buf.buf_k[k] = big_buf.buf + k * spb;
	}
	
	big_buf.spb = spb;
	
	big_buf.k = 0;
	
	return 0;
	
}

int get_sample_buffer_index(){
	return big_buf.k;
}

/* 
 * get_this_sample_buffer - get the current buffer pointer
 *--------------------------------------------------------*/

sample_t * get_this_sample_buffer(){
	
	return big_buf.buf_k[big_buf.k];
	
}

/*
 * get_offset_sample_buffer - get the buffer pointer OFFSET away from the "current" one
 *-------------------------------------------------------------------------------------*/

sample_t * get_offset_sample_buffer(int offset){
	
	int k;
	
	k = big_buf.k + offset;
	
	while (k > big_buf.n_k)
		k -= big_buf.n_k;
	
	while (k < 0)
		k += big_buf.n_k;
	
	return big_buf.buf_k[k];
	
}


/*
 * write_to_sample_buffer
 *------------------------------------------------------------*/

void write_to_sample_buffer(sample_t * buf){
	
	memcpy(big_buf.buf_k[big_buf.k], buf, big_buf.spb * sizeof(sample_t));
	
	big_buf.k++;
	
	if (big_buf.k >= big_buf.n_k)
		big_buf.k = 0;
	
}

/*
 * write_sample_buffer_file
 *------------------------------------------------------------*/

int write_sample_buffer_file(const char * fname, int samplerate, int ix1, int ix2, int big_k){
	
	SNDFILE * fp;
	SF_INFO sfinfo;
	
	sample_t * sp;
	
	int N, k, start_k, end_k;
	
	/*
	 * set up to write a 16-bit, single channel, AIFF file
	 */
	
	sfinfo.samplerate = samplerate; sfinfo.channels = 1;
	
	sfinfo.format = SF_FORMAT_AIFF | SF_FORMAT_PCM_16;
	
	fp = sf_open( fname, SFM_WRITE, &sfinfo );

	if (!fp)
		printf("failed to open sound file!\n");
	
	/*
	 * get range of sample circular buffer indices
	 */
	
	start_k = mod(big_k - 1 + (ix1 / big_buf.spb) - (ix1 < 0 ? 1 : 0), big_buf.n_k);
	
	end_k = mod(big_k - 1 + (ix2 / big_buf.spb) - (ix2 < 0 ? 1 : 0), big_buf.n_k);
	
	/*
	 * write buffer segments out to file
	 */
	
	k = start_k;
	
	while (1) {
		
		sp = big_buf.buf_k[k];
		
		N = big_buf.spb;
		
		if (k == start_k){
			sp += mod(ix1, big_buf.spb); N -= mod(ix1, big_buf.spb);
		}
		
		if (k == end_k){
			N = big_buf.buf_k[k] + mod(ix2, big_buf.spb) - sp;
		}
		
		sf_write_short (fp, sp, N);
		
		if (k == end_k)
			break;
		
		k = mod((k + 1), big_buf.n_k);
		
	}
	
	/*
	 * clean up and finish
	 */
	
	sf_close(fp);
	
	return 0;
	
}
