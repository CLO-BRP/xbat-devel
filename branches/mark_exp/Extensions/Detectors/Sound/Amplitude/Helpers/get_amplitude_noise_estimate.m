function noise = get_amplitude_noise_estimate(feature, parameter)

%--
% get low level parameters from parameter input
%--

% TODO: compute the width based on time parameters

% db_disp; flatten(parameter) 

%--
% compute noise estimate
%--

width = 21; SE = ones(width, 1); order = 6;

F = filt_binomial(width, 1);

noise = feature.rms.value;

for k = 2:8
	noise = rank_filter(noise, SE, order); noise = linear_filter(noise, F);
end