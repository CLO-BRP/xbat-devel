function control = parameter__control__create(parameter, context)

% AMPLITUDE - parameter__control__create

control = empty(control_create);

%--
% create tabs for event detection and feature computation parameters
%--

tabs = {'Signal', 'Frame'};

control(end + 1) = control_create( ...
	'style', 'tabs', ...
	'tab', tabs ...
);

%--
% add detection controls
%--

control = [control, amplitude_detection_controls(parameter, context, tabs{1})];

%--
% feature computation controls
%--

fun = parent_fun(mfilename('fullpath')); inherited = fun(parameter, context);

% NOTE: this removes the tabs control of the parent 

inherited(1) = [];

for k = 1:numel(inherited)
	inherited(k).tab = tabs{2};
end

control = [control, inherited];
