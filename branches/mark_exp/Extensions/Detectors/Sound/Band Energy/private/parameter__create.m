function parameter = parameter__create(context)

% BAND ENERGY - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% --
% set easy parameters for band filters
% --

parameter.bands = [0.2, 0.6];

parameter.bank_type = 'rectangular';

%--
% set default detector parameters from amplitude detector
%--

% NOTE: we do not use adaptive frame size as default for detection

parameter.auto = 0;

parameter = amplitude_detection_parameters(parameter, context);

