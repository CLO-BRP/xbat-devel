function parameter = view__parameter__create(context)

% DATA TEMPLATE - view__parameter__create

parameter = struct;

parameter.correlation = 0;

parameter.smooth = 1;

parameter.stem = 1;

parameter.balloon = 1;

parameter.label = 1;