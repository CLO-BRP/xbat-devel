function result = parameter__control__callback(callback, context)

% DATA TEMPLATE - parameter__control__callback

%----------------------------
% SETUP
%----------------------------

%--
% unpack callback
%--

control = callback.control; pal = callback.pal; par = callback.par; 

%--
% set update flags
%--

result.update = 1; update_clip = 1;

%----------------------------
% CALLBACKS
%----------------------------

switch control.name
		
	%----------------------------
	% TAGS
	%----------------------------
		
	case 'code'
		
		result.update = 0;
		
		%--
		% get code and templates
		%--
		
		code = get_control(pal.handle, 'code', 'value');
		
		templates = get_control(pal.handle, 'templates', 'value');
		
		if isempty(templates)
			return;
		end
		
		%--
		% update code of current template
		%--
		
		templates.clip(templates.ix).code = code;
		
		set_control(pal.handle, 'templates', 'value', templates);
		
		%--
		% update template_select
		%--
		
		han = get_control(pal.handle, 'template_select', 'handles');
		
		select_str = get(han.obj, 'string');
		
		select_str{templates.ix} = ['Clip ', int2str(templates.ix), ': ', code];
		
		set(han.obj, 'string', select_str, 'value', templates.ix);
		
	

	%---------------------------------------------------------------
	% TEMPLATE
	%---------------------------------------------------------------
		
	case 'template_select'
		
		result.update = 0;
		
		%--
		% update template index
		%--
        
        templates = get_control(pal.handle, 'templates', 'value');
		
        if isempty(templates)
			return;
        end
		
        ix = get_control(pal.handle, 'template_select', 'index');
        
        templates.ix = ix;
		
		set_control(pal.handle, 'templates', 'value', templates);
		
		%--
		% update code
		%--
		
		% NOTE: it is not clear why the separate line of code is needed
				
        set_control(pal.handle, 'code', 'value', templates.clip(ix).code);

        %--
        % update template mode control
        %--
        
        % NOTE: the "+ 1" is due to the 0-indexed nature of modes
               
        set_control(pal.handle, 'template_mode', 'value', templates.clip(ix).mode + 1);
		
	%---------------------------------------------------------------
	% PREVIOUS AND NEXT TEMPLATE
	%---------------------------------------------------------------
	
	case {'previous_template', 'next_template'}
		
		result.update = 0;
		
		%--
		% get template control handles
		%--
		
        ix = get_control(pal.handle, 'template_select', 'index');
        
        n = numel(get_control(pal.handle, 'template_select', 'string'));
		
		%--
		% update control and execute callback
		%--
        
		switch (control.name)

			case ('previous_template')

				if (ix == 1)
					return;
				else
					ix = ix - 1;
				end

			case ('next_template')

				if (ix == n)
					return;
				else
					ix = ix + 1;
				end

		end
		
		set_control(pal.handle, 'template_select', 'value', ix);
        
        control_callback(par.handle, pal.handle, 'template_select');
		
	%---------------------------------------------------------------
	% TEMPLATE MODE
	%---------------------------------------------------------------
	
	% NOTE: this is used to set and edit a code for the current template
		
	case 'template_mode'
		
        templates = get_control(pal.handle, 'templates', 'value');
		
        if isempty(templates)
			return;
        end

		%----------------------
		% UPDATE OLD TEMPLATES
		%----------------------
		
		% NOTE: the default mode is 'Keep (Exclusive)' the pre-existing behavior
		
		% NOTE: we are also adding an 'id' field to clips
        
        % NOTE: this is hardly the place to be doing this
		
		if ~isfield(templates.clip(1), 'mode')
			
			%--
			% add mode and id to each clip
			%--
			
			for k = 1:length(templates.clip)
				
				templates.clip(k).mode = 1;
				templates.clip(k).id = k;
				
			end
			
			%--
			% set current id
			%--
			
			templates.curr_id = k + 1;
			
		end        
        
        %--
		% get mode
		%--
        
        mode = get_control(pal.handle, 'template_mode', 'index');
        
		%--
		% update mode of current template
		%--
		
        templates.clip(templates.ix).mode = mode;
        
		%--
        % store templates
        %--
        
        set_control(pal.handle, 'templates', 'value', templates);
    
		
	%---------------------------------------------------------------
	% TEMPLATES
	%---------------------------------------------------------------
		
	case 'templates'

		%--
		% handle play clip on double click
		%--
		
		% NOTE: we play on double click, the interval is slower than the default

		ax = findobj(pal.handle,'tag','templates','type','axes');
		
		if double_click(ax, 0.5)
			play_clip(pal.handle); return;
		end			
		
		%--
		% get templates
		%--
		
		templates = get_control(pal.handle, 'templates', 'value');
		
		if isempty(templates)
			return;
		end
		
		%--
		% update the template select control
		%--
        
        [L, ix] = template_select_strings(templates);
        
        set_control(pal.handle, 'template_select', 'string', L);
        
        set_control(pal.handle, 'template_select', 'value', ix);
		
		result.update = 0;
	
		return;
		
	%---------------------------------------------------------------
	% COPY
	%---------------------------------------------------------------
	
	% NOTE: this is to get extent parameters from selection
	
	case {'copy', 'INTERCEPT_SELECTION_COPY'}
		
		%--
		% return on selection copy if intercept is off
		%--
		
		% NOTE: return if we were called by the checkbox
		
        if strcmp(get(gco, 'tag'), 'INTERCEPT_SELECTION_COPY')
            return;
        end
       		
        %--
        % also return early if we are not intercepting
        %--
        
        value = get_control(pal.handle, 'INTERCEPT_SELECTION_COPY', 'value');

        if (value == 0)
            return;
        end
	
		%--
		% get selection from parent, return if no selection (this should not happen)
		%--
		
		par = get_field(get(pal.handle, 'userdata'), 'parent');
				
		if isempty(par)
			return;
		end
					
		%--
		% get relevant clip data from parent
		%--
		
		% NOTE: when selection derives from selected event other information may be contained 
		
		data = get(par, 'userdata');
		
		event = data.browser.selection.event;
		
		if isempty(event) || isempty(event.time)
			return;
		end
			
		rate = data.browser.sound.samplerate;
			
		%--
		% get template structure from clip axes
		%--
		
		ax = findobj(pal.handle,'type','axes','tag','templates');
		
		templates = get(ax, 'userdata');
		
		%--
		% compile relevant data to create clip
		%--
		
		% TODO: develop a generic clip structure as basic part of system
		
        % NOTE: create a clip for this event with empty tags and a mode of
        % 1 (exclusive keep)
        
		clip = clip_create(event, rate, [], '', 1);
		
		%--
		% read enough samples to fully re-generate the event spectrogram (2x event duration)
		%--
		
		clip.data = sound_read( ...
			data.browser.sound, 'time', event.time(1), 2*diff(event.time), event.channel ...
		);
				
		%--
		% update templates based on copy mode
		%--
		
        mode = get_control(pal.handle, 'copy_mode', 'value');
						
		switch lower(mode{1})
			
			%--
			% add new template
			%--
			
			case 'add'
								
				if isempty(templates)
					
					clip.id = 1;
					templates.clip = clip;
					
					templates.ix = 1;
					templates.length = 1;
					templates.curr_id = 2;
					
				else
					
					n = length(templates.clip);
					
					clip.id = templates.curr_id;
					
					% HACK: this resolves a problem dealing with
					% concatenating compiled and new clips!
					
					clip = struct_update(templates.clip(end), clip);
					
					templates.clip(n + 1) = clip;
					
					templates.ix = n + 1;
					templates.length = n + 1;
					templates.curr_id = templates.curr_id + 1;
					
				end
				
			%--
			% replace current template
			%--
			
			% TODO: consider defining the replace behavior differently (as a delete and an add)
			
			case 'replace'
								
				if isempty(templates)
					
					clip.id = 1;
					templates.clip = clip;
					
					templates.ix = 1;
					templates.length = 1;
					templates.curr_id = 2;
					
				else
					
					ix = templates.ix;
					templates.clip(ix) = clip;
					
				end
				
		end
		
		set(ax,'userdata',templates);
		
		%--
		% update the template select control
		%--
		
        [L, ix] = template_select_strings(templates);
        
        set_control(pal.handle, 'template_select', 'string', L);
        
        set_control(pal.handle, 'template_select', 'value', ix);
        
        %--
        % update template mode control
        %--
        
        % NOTE: the "+ 1" is due to the 0-indexed nature of modes
               
        set_control(pal.handle, 'template_mode', 'value', clip.mode + 1);
	
		%--
		% update edit control
		%--
		
        set_control(pal.handle, 'code', 'string', clip.code);
		
		%--
		% update clip display
		%--
		
		plot_clip(pal.handle);
				
	%----------------------------
	% CORRELATION
	%----------------------------
	
	%--
	% test toggles
	%--
	
	case {'thresh_test', 'deviation_test'}
		
		%--
		% enable or disable related threshold slider
		%--
		
		related = strtok(callback.control.name, '_');

		enable = bin2str(get(callback.obj, 'value'));
		
		set_control(callback.pal.handle, related, 'enable', enable);
		
		update_clip = 0;
		
	%----------------------------
	% MASK
	%----------------------------
	
	%--
	% masking toggle
	%--
	
	case 'mask'

		%--
		% toggle enable state of mask percentile control
		%--
		
		enable = bin2str(get(callback.obj, 'value'));
		
		set_control(callback.pal.handle, 'mask_percentile', 'enable', enable);
				
end

%--
% update clip display if needed
%--

if update_clip

	% NOTE: resolve the use of the function handle input
	
	plot_clip(pal.handle);

end


%-----------------------------------------------------------
% TEMPLATE_SELECT_STRINGS
%-----------------------------------------------------------

function [L, ix] = template_select_strings(templates)

% template_select_strings - create template select strings for control
% --------------------------------------------------------------------
%
% [L, ix] = template_select_strings(templates)
%
% Input:
% ------
%  templates - templates
%
% Output:
% -------
%  L - cell of select strings
%  ix - current template index

%--
% return quickly on no templates
%--

if ~length(templates.clip)
	L = {'(No Available Templates)'}; ix = 1; return;
end

%--
% build template select strings
%--

for k = 1:length(templates.clip)

	prefix = ['Clip ' int2str(k) ':  '];
	
	if ~isempty(templates.clip(k).code)
		L{k} = [prefix, templates.clip(k).code];
	else
		L{k} = [prefix, '( NO TAGS )'];
	end

end

ix = templates.ix;





