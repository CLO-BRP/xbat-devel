function [event, value, context] = compute(page, parameter, context)

% PERIODICITY II - compute

event = empty(event_create); value = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.view.data = feature;


% TODO: parameters considered for gui access
pitch_mem = 5;
max_bad_pctle = 30;

ri = feature.novelty_power_spec.rel_idx;

ref_energy = feature.novelty_power_spec.ref_energy;
     
freq = feature.novelty_power_spec.freq;

% we have to get the tolerance level in bins
pitch_tolerance = ceil(parameter.pitch_tolerance/...
    diff(freq(1:2)));

% nyquist frequency for event information
nyq = 0.5 * context.sound.rate;

% we deal with page-relative time, here!
time = feature.novelty_power_spec.time - page.start;

% --
% deal with multiple channels: select active
% --
if length(page.channels) > 1
    nps = feature.novelty_power_spec.value{page.channel};

    ref_energy = feature.novelty_power_spec.ref_energy{page.channel};
else
    nps = feature.novelty_power_spec.value;
    
    ref_energy = feature.novelty_power_spec.ref_energy;
end

% ---
% here, we cutoff the irrelevant frequencies from the novelty power
% spectrum
% ---
% [nps, freq] = cutoff(nps_full, freq, rel_idx);

% this is for the case of just one frequency band
if ~iscell(nps) 
    nps = {nps};
    ref_energy = {ref_energy};
end

% --
% deal with multiple frequency bands
% --
fixed_pitches = zeros(numel(nps),size(nps{1},2));
fp_dom = zeros(numel(nps),size(nps{1},2));
outli_dom = zeros(numel(nps),size(nps{1},2));
for j=1:numel(nps)
    
    % ---
    % calculate pitch candidates
    % ---
    for k=1:size(nps{j},2)
        
        % ---
        % TODO: use a peak picking algo for the selection of several
        % candidate peaks. this would make it more easy to discard segments
        % having no peak in the given range of repetition frequencies
        % ---
        
        % the max version is intended as first attempt
        % we want to use more sophisticated peak picking
        % algorithms for this
        
        % ---
        % NOTE: using pitches_max, we accept almost every tiny peak in the given
        % range :(
        % --- 
        % pitches{j}{k} = pitches_max(nps{j}(ri(1):ri(2),k)) + ri(1) - 1;
        
        pitches{j}{k} = pitches_peaks(nps{j}(:,k));
      
        % measure domination values for all candidates
        for l = 1:numel(pitches{j}{k}) 
            [dominations{j}{k}(l) pitches{j}{k}(l)] = domination(nps{j}(:,k), ref_energy{j}(k),...
                pitches{j}{k}(l), round(pitch_tolerance/2));
        end
        
        % ---
        % these outlying pitches have been detected for comparing the 
        % relative domination measurements 
        % ---
        outlip = find((pitches{j}{k} < ri(1)) | (pitches{j}{k} > ri(2))); 
        
        % try to avoid a constant zero-freq peak
        % outlip = outlip(pitches{j}{k}(outlip) > 3);
        
        if ~isempty(outlip)
            outli_dom(j,k) = max(dominations{j}{k}(outlip));
        end
        
        % ---
        % exclude pitches not in the specified range
        % ---
        idx = find((pitches{j}{k} >= ri(1)) & (pitches{j}{k} <= ri(2))); 
        
        pitches{j}{k} = pitches{j}{k}(idx);
        dominations{j}{k} = dominations{j}{k}(idx);
        
        % sort pitches according to domination measurements
        [dominations{j}{k}, idx] = sort(dominations{j}{k},'descend');
        pitches{j}{k} = pitches{j}{k}(idx);
    end
    
    % ---
    % make pitch decisions
    % ---
    
    for k=1:size(nps{j},2)
        
        % we look at our old decisisons and
        % try to stay somewhat near them
        last_pitches = fixed_pitches(j,max(1,k-pitch_mem):k-1);
        
        % note: zero-pitches overlooked at this stage
        last_pitches = last_pitches(last_pitches > 0);
        
        if ~isempty(last_pitches) && ~isempty(pitches{j}{k})
            
            % compare the new candidates to history
            lastmat = repmat(last_pitches,numel(pitches{j}{k}),1);
            newmat = repmat(pitches{j}{k}',1,numel(last_pitches));
            
            distances = abs(lastmat - newmat);
            
            % and sum over past times
            distances = sum(distances,2);

            % sort according to history conformity
            [distances, idx] = sort(distances);
            
            % test if one of the candidates fits history
            if distances(1) <= pitch_tolerance
                
                % save the pitch with smallest historical distance
                % and the corresponding domination value
                fixed_pitches(j,k) = pitches{j}{k}(idx(1));
                fp_dom(j,k) = dominations{j}{k}(idx(1));
                
            else % save the pitch with biggest domination
                
                % OK, we stay with the peak having 
                % superior domination
                fixed_pitches(j,k) = pitches{j}{k}(1);
                fp_dom(j,k) = dominations{j}{k}(1);
            end
            
        elseif ~isempty(pitches{j}{k})
            
            % OK, we stay with the peak having 
            % superior domination
            fixed_pitches(j,k) = pitches{j}{k}(1);
            fp_dom(j,k) = dominations{j}{k}(1);
            
        else
            % there is no pitch to save
            fp_dom(j,k) = 0;
        end
    end
    
    % --- 
    % segments of almost constant periods are extracted
    % TODO: note zero-pitch segments and use this information for segment
    % intersection / classification
	% ---
    
    pdmpos = max(1,round((max_bad_pctle/100*(pitch_mem*pitch_mem-1))));
    isseg = 0;
    segctr = 0;
    segs{j}=[];
    for k=2:size(nps{j},2)
        last_pitches = fixed_pitches(j,max(1,k-pitch_mem+1):k);
        last_pitches = last_pitches(last_pitches > 0);
 
        pitch_diff=[];
        for l = 1:numel(last_pitches)-1
            pitch_diff=[pitch_diff last_pitches - circshift(last_pitches',l)'];
        end
        
        pdm = sort(abs(diff(pitch_diff)),'descend');
        % ---
        % check if we have enough nice pitches 
        % ( f.e. 30% in past have been the same)
        if ~isempty(pdm) && pdm(min(pdmpos,numel(pdm))) <= pitch_tolerance
            
            % create new segment or update existing segment's
            % length
            if isseg 
                o = k;
            else
                isseg = true;
                a = k;
                o = k;
            end
            
        elseif isseg 
             % finalize segment
             isseg = false;
             
             % test if new segment meets user requirements
             
             % ---
             % TODO: build segment joining step and perform these 
             % tests after joining nearby segments
             % ---
             
             % --
             % calculate per-frame relative dominations. 
             % do we want this to be a "meaned" thing from here ?
             % -- 
             old_rels = fp_dom(j,a:o) ./ outli_dom(j,a:o);
             old_rels(old_rels == Inf) = 1;
             meanor = mean(old_rels);
             
             domsum = mean(fp_dom(j,a:o));
             if (diff(time([a o])) > parameter.min_segsize) && ...
                    meanor > parameter.rel_domination;
                    %domsum  > parameter.min_domination && ...
                 % save new segment
                 segctr = segctr+1;
                 segs{j}(segctr,:) = [a o];
                 repfreq{j}(segctr) = median(fixed_pitches(j,a:o));
                 segdom{j}(segctr) = domsum;
                 segreldom{j}(segctr) = meanor;
             end
        end  
    end
end

% --
% now we save our segments into an event struct array
% -- 

for j=1:numel(nps)
    min_freq = parameter.band_details(1,j) * 1.05 * nyq;
    max_freq = parameter.band_details(3,j) * 0.95 * nyq;
    for k=1:size(segs{j},1)
        
        etime = time(segs{j}(k,:))';
        efreq = [min_freq max_freq];
        erepfreq = repfreq{j}(k);
        
        % create event
        event(end+1) = event_create( ...
		'channel', page.channels, 'time', etime, 'freq', efreq, ...
        'score', segdom{j}(k), 'tags', str_to_tags(sprintf('%3.1fHz',repfreq{j}(k))));
    end
end
% auto-fill the segment's duration

if ~isempty(event)
    event = update_event_duration(event);
end


