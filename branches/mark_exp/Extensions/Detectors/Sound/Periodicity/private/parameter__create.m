function parameter = parameter__create(context)

% PERIODICITY II - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% ----
% overwrite inherited parameters
% ----

% power spectrum: frames per second (Hz)

% ---
% NOTE: the original implementation uses a fixed fftlen regardless
% of the temporal resolution needed
% ---
parameter.temp_res = 20;

% parameter.freq_res = 1; % Hz

parameter.min_repfreq = 5; %Hz

% ---
% we set the fft parameters to values 
% corresponding to the default max_repfreq values.
% values are oriented towards the tsa applet implementation
% ---
parameter.fft = 512;
parameter.frame = 1/150; 
parameter.advance = 0.5;
parameter.max_repfreq = min(parameter.max_repfreq, 150); %Hz

% ----
% set detection / segmentation parameters
% ----

parameter.pitch_tolerance = 4; % Hz

parameter.rel_domination = 0.5; % 0-1 (%/100)

parameter.min_domination = 0.05; % 0-1

parameter.min_segsize = 0.5; % seconds

