function [out, corrected_pitch] = domination(in, ref_energy, peakpos, hillwidth)
% calculates the domination value for a specified peak in a nps feature
% column.

if nargin < 4
    hw = 1;
else 
    hw = hillwidth;
end


% ---
% Instead of looking at a single point, a comb-like filter is used to catch
% all the possible harmonics of the selected peak.
% ---
vr = [];
for h = 1:min(6, floor(numel(in) / peakpos))
    % adapt peakpos to actual harmonic maximum
    hipos = round(h * peakpos(h));
    
    if (hipos - hw) > numel(in); 
        break; 
    end

    [null, peakpos(h + 1)] = max(in( ...
        max(1, hipos - hw):min(numel(in), hipos + hw)));
    
    peakpos(h + 1) = peakpos(h + 1) + max(1, h * peakpos(h) - hw);
    peakpos(h + 1) = peakpos(h) + 0.5 * (peakpos(h)-((peakpos(h + 1) / h)));
    
    % save the new peakpos
    hipos = round(h * peakpos(h + 1));
    
    newpeaks = max(1,hipos-hw):min(numel(in), hipos + hw);
    vr = [vr newpeaks];
end

estab = sum(in(vr));

% ---
% NOTE: is it better to compare with the whole periodical energy?
% ---
polp = sum(in);

% ---
% here, we calculate the ratio of the given position with respect to all
% other fields
% ---
out = estab / polp;

corrected_pitch = mean(peakpos);
% debugging

% h = fig;
% plot(in); hold on
% plot(vr,mean(in));
% pause
% close(h);