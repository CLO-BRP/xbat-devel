function pitches = pitches_max(in)
% generates pitch candidates using the max function and assuming harmonical
% relations

[val, pitches] = max(in);
    
% by now, 3 candidates sound reasonable
pitches=[pitches pitches*2 round(pitches/2)];
pitches=pitches(...
    (pitches > 0) & ...
    (pitches < numel(in)));