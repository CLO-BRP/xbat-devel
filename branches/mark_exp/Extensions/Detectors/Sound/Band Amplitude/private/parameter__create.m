function parameter = parameter__create(context)

% BAND AMPLITUDE - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

nyq = 0.5 * context.sound.rate;

parameter.whiten = 1;

parameter.min_freq = 0.05 * nyq;

parameter.max_freq = 0.5 * nyq;

parameter.butter_order = 4;

parameter.filter.a = 1;

parameter.filter.b = 1;