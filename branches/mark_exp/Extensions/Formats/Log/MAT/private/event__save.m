function result = event__save(store, event, context)

% MAT - event__save

result = struct;

%----------------------------------
% APPEND EVENTS
%----------------------------------
'hellko'
log = load(store.file);

if ~isfield(log, 'curr_id')
    log.curr_id = 1;
end

id = log.curr_id;

for k = 1:length(event)
	
    event.modified = now;
    
    %--
    % just replace events that already exist
    %--
    
    if ~isempty(event(k).id)
    
        ix = find([log.event(:).id] == event(k).id);
        
        if ~isempty(ix)         
            log.event(ix) = event(k); continue;   
        end   
        
    end
    
    %--
    % otherwise add events to the end of the log
    %--
        
    event(k).id = id; id = id + 1;

    if isempty(log.event)
        log.event = event(k);
    else
        log.event(end + 1) = event(k);
    end

end

%--
% update information fields and save
%--

log.curr_id = id; 

save(store.file, '-struct', 'log');

%--
% update cache for this log
%--

event_cache(store, true);

