function parameter = store__parameter__create(context)

% MAT - store__parameter__create

parameter = struct;

[names, ix] = get_mat_formats;

parameter.mat_format = names{ix};