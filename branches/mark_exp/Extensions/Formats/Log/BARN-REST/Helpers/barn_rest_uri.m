function uri = barn_rest_uri(parameter, varargin)

prefix = ['http://', parameter.server];

switch varargin{1}
	
	otherwise
		uri = [prefix, '/', varargin{1}, '/'];

end