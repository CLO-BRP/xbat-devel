function [user, response] = post_barn_user(server, user)

% post_barn_user - post user to BARN server
% -----------------------------------------
%
% [user, response] = post_barn_user(server, user)
%
% Input:
% ------
%  server - address
%  user - user to post
%
% Output:
% -------
%  user - created on server
%  response - from server

%--
% set default server
%--

if nargin < 1
	server = 'localhost:3000'; 
end

%--
% use dialog to get user input
%--

if ~nargin
	
	out = post_user_dialog(server);
	
	if isempty(out.values)
		user = struct; request = struct; return;
	end
	
	user.login = out.values.name; user.email = out.values.email; user.password = out.values.password;
	
	server = out.values.server;
	
end

%--
% prepare user
%--

user.password = 'perhaps';

user.password_confirmation = user.password;

%--
% prepare and submit request
%--

response = curl_rest( ...
	'POST', ['http://', server, '/users'], ...
	{'Authorization', 'ZGVmYXVsdDpwYXNzd29yZA==', 'Accept', 'text/xml'}, ...
	curl_encode_struct(user) ...
);

% tool = get_curl;
% 
% request.string = [ ...
% 	tool.file, ' --silent'...
% 	' -H "Authorization:ZGVmYXVsdDpwYXNzd29yZA=="', ...
% 	' -H "Accept:text/xml,text/javascript,application/json"', ...
% 	' -X POST http://', server, '/users' ...
% 	' -d "user[login]=', user.login, ...
% 	'&user[password]=', user.password, ...
% 	'&user[password_confirmation]=', user.password, ...
% 	'&user[email]=', user.email, '"' ...
% ];
% 
% [request.status, request.result] = system(request.string);
% 
% if ~nargout
% 	disp(' '); disp(request.result); clear status;
% end


%----------------------------------------------
% POST_USER_DIALOG
%----------------------------------------------

function out = post_user_dialog(server)

%----------------------------------
% CREATE CONTROLS
%----------------------------------

%--
% header and name
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', 'User' ...
);

% TODO: get list of known servers and present this as popup

control(end + 1) = control_create( ...
	'name', 'server', ...
	'space', 1, ...
	'string', server, ...
	'onload', 1, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'name', ...
	'space', 1, ...
	'onload', 1, ...
	'style', 'edit' ...
);

%--
% email, password, and url
%--

% TODO: consider setting the validator as default given name

control(end + 1) = control_create( ...
	'name', 'email', ...
	'type', 'email', ...
	'space', 0.75, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'confirm_email', ...
	'type', 'email', ...
	'space', 1.25, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'password', ...
	'style', 'password', ...
	'space', 0.75, ...
	'width', 0.75 ...
);

control(end + 1) = control_create( ...
	'name', 'confirm_password', ...
	'style', 'password', ...
	'space', 1.5, ...
	'width', 0.75 ...
);

%----------------------------------
% CREATE DIALOG
%----------------------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 16;

opt.header_color = get_extension_color('root');

opt.text_menu = 1;

%--
% create dialog
%--

name = 'New BARN User ...';

try
	out = dialog_group(name, control, opt, @post_user_callback);
catch
	out.values = []; nice_catch(lasterror);
end


%----------------------------------------------
% POST USER CALLBACK
%----------------------------------------------

function post_user_callback(obj, eventdata) %#ok<INUSD>

[control, pal] = get_callback_context(obj);

switch control.name

	case {'name', 'email', 'confirm_email', 'password', 'confirm_password'}
		
		result = validate_basic(pal.handle);

		set_control(pal.handle, 'OK', 'enable', result);
		
		% NOTE: in this case we are editing the email, make sure we confirm
		
		if strcmp(control.name, 'email') && ~result	
			
			set_control(pal.handle, 'confirm_email', 'enable', 1, 'value', '');
			
		end
		
		if strcmp(control.name, 'password') && ~result	
			
			set_control(pal.handle, 'confirm_password', 'enable', 1, 'value', '');
			
		end
		
end

%-------------------------------------------
% VALIDATE_BASIC
%-------------------------------------------

function result = validate_basic(pal)

%--
% get control values
%--

name = get_control_value(pal, 'name');

email = get_control_value(pal, 'email');

confirm_email = get_control_value(pal, 'confirm_email');

password = get_control_value(pal, 'password'); 

confirm_password = get_control_value(pal, 'confirm_password');

%--
% test proper name and proper and confirmed email and password
%--

test(1) = proper_filename(name);

proper_email(email), strcmp(email, confirm_email)

test(2) = proper_email(email) && strcmp(email, confirm_email);

% test(3) = proper_password(password) && strcmp(password, confirm_password);

result = all(test);


%-------------------------------------------
% PROPER_PASSWORD
%-------------------------------------------

% TODO: factor this and use in 'new_user_dialog' as well

function result = proper_password(password)

result = length(password) > 6;


