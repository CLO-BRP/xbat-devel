function sound = get_barn_sound(store, sound, user)

% get_barn_sound - get sound representation from BARN
% ---------------------------------------------------
%
% sound = get_barn_sound(store, sound, user)
%
% Input:
% ------
%  store - for BARN
%  sound - to consider
%  user - in case we are doing this for the first time
%
% Output:
% -------
%  sound - in BARN

%--
% get active user and sound default
%--

if nargin < 3
	user = get_barn_user(store, get_active_user);
end 

if nargin < 2
	sound = get_active_sound;
end

%--
% handle multiple sound input
%--

if numel(sound) > 1
	sound = iteraten(mfilename, 2, store, sound, user); return;
end

%--
% check for and possibly set sound
%--

% TODO: currently we are compute the content_hash of files twice, fix this

hash = get_sound_hash(sound);

current = get_database_objects_by_column(store, 'sound', 'content_hash', hash);

if ~isempty(current)
	sound = current; insert_relation(store, 'sound', sound.id, 'user', user.id); return;
end

source = sound;

sound.user_id = user.id; 

sound.content_hash = hash;

% NOTE: the 'samples' field in the source struct does not have the same meaning

cumulative_samples = sound.cumulative;

sound.samples = cumulative_samples(end);

sound = set_barn_objects(store, sound);

insert_relation(store, 'sound', sound.id, 'user', user.id); 

%--
% get sound recording info
%--

if ischar(source.file)
	files = {source.file};
else 
	files = source.file;
end

for k = 1:numel(files)
	
	file = fullfile(source.path, files{k});
	
	info = sound_file_info(file); 
	
	db_disp file-content-hash; start = clock;
	
	info.content_hash = file_md5(file);
	
	disp(sec_to_clock(etime(clock, start)));
	
	% NOTE: 'file' is the original file name, 'name' is the name part of the filename
	
	info.file = files{k}; 
	
	info.name = file_ext(files{k});
	
	recording(k) = info;
	
end

%--
% move the files to the local barn if needed
%--

% NOTE: the parent directory matches the first parent sound guid

% NOTE: he proper test is most likely '~isequal(local_barn, store)', make sure it works

if ~isempty(local_barn)

	destination = create_dir(local_barn_files(sound));

	disp(['Moving files to ''', strrep(destination, public_root, ''), '''.']);
	
	for k = 1:numel(files)

		file = fullfile(source.path, files{k});

		% NOTE: we copy the file to its 'location'

		try
			copyfile(file, destination, 'f'); disp(['Copied ''', file, '''.']);
			
% 			generate_barn_sound_files(destination, files{k});
		catch
			nice_catch(lasterror, ['Failed to copy ''', file, '''.']);
		end
		
		% NOTE: the stored location is the server relative path, the address beyond the public root

		location = strrep(fullfile(destination, files{k}), public_root, '');

		location = strrep(location, filesep, '/');

		recording(k).location = location;

	end

end

%--
% store the recording objects and the recording sound relations
%--

recording = set_barn_objects(store, recording); 

% db_disp 'we need to escape backslashes before storing'; disp(recording);

% NOTE: the recording sound relation contains an essential position and an auxiliary cumulative samples field

value = struct; value.position = 1;

for k = 1:numel(recording)
	
	value.position = k; value.cumulative_samples = cumulative_samples(k);
	
	insert_relation(store, 'sound', sound.id, 'recording', recording(k).id, 'value', value);
	
end

%--
% set sound attributes
%--

% TODO: this requires that we review and develop the sound attribute API




