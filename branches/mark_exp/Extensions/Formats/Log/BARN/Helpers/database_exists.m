function result = database_exists(config)

% database_exists - checks for database availability
% --------------------------------------------------
%
% result = database_exists(config)
%
% Input:
% ------
%  config - for database access
%
% Output:
% -------
%  result of test

% TODO: consider adding a third output value for unknown

% TODO: move this out of the BARN log format helpers

switch lower(config.adapter)
	
	case 'sqlite'
		
		result = exist(config.file, 'file');
		
	case lower(known_jdbc_adapter)

		result = 0; % NOTE: this means that we will revisit the schema establishment every time

end