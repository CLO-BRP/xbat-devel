function result = event__delete(store, id, context)

% BARN - event__delete

result = struct;

if nargin < 2 || isempty(id)
    return;
end

%--
% delete event and basic annotation
%--

prefix = 'DELETE FROM event WHERE id';

if numel(id) == 1
	sql = [prefix, ' = ', int2str(id), ';'];
else
	sql = [prefix, ' IN (', str_implode(id, ', ', @int2str), ');'];
end

query(store, sql);

% NOTE: this is not a private operation, others may have rated or tagged these events, all relations must be discarded

[ignore, rating] = get_ratings(store, id, [], 'event');

delete_database_objects(store, rating);

% TODO: we try to delete empty taggings and this throws an exception

[ignore, tagging] = get_taggings(store, id, [], 'event');

delete_database_objects(store, tagging);













% TODO: this still needs some work

% [ignore, annotation] = get_notes(store, id, [], 'event');
% 
% delete_database_objects(store, annotation);

%--
% delete extension data entries
%--

% TODO: factor, this and related queries merit a function

% NOTE: these tables should have an 'event' foreign key

% sql = 'SELECT * FROM barn_master WHERE extension_type LIKE ''%event%'' AND table_name LIKE ''%__value'';';
% 
% table = query(store, sql);
% 
% for k = 1:numel(table)
% 	
% end