function parameter = store__parameter__create(context) %#ok<INUSD>

% BARN - store__parameter__create

% TODO: why are we packing parameters into context?

if ~nargin || ~isfield(context, 'adapter')
	context.adapter = 'SQLite';
end

if ~nargin || ~isfield(context, 'database')
	context.database = 'barn';
end

% NOTE: check for presence of local barn, in this case we take defaults from there

store = local_barn;

if ~trivial(store)
	parameter = store;
else
	parameter = create_database_config( ...
		'adapter', context.adapter, 'database', context.database ...
	);
end