function result = store__parameter__control__callback(callback, context)

% BARN - store__parameter__control__callback

result = struct; 

pal = callback.pal;

switch callback.control.name
	
	case 'adapter'
		
		adapter = lower(cellfree(callback.control.value));
		
		%--
		% update server adapter controls
		%--
		
		enable = ~strcmp(adapter, 'sqlite');
		
		toggle = {'hostname', 'port', 'username', 'password'};
		
		for k = 1:numel(toggle)
			
			set_control(pal.handle, toggle{k}, 'enable', enable);
			
			% NOTE: we also clear the values of the server fields for SQLite
			
			if ~enable
				set_control(pal.handle, toggle{k}, 'value', '');
			end
			
		end
		
		%--
		% set some reasonable server defaults
		%--
		
		if enable
			
			set_control(pal.handle, 'hostname', 'value', 'localhost');
			
			set_control(pal.handle, 'username', 'value', 'root');
			
			% TODO: this belongs in the adapter info function, move it there somehow
			
			switch adapter
				
				case 'mysql'
					value = '3306'; 
					
				case 'postgresql'
					value = '5432';
					
				otherwise
					value = '1527';
					
			end
			
			set_control(pal.handle, 'port', 'value', value);
			
		end
		
	case 'show_password'
		
		% TODO: develop some way of revealing the password
		
		db_disp not-currently-implemented
		
		control = get_control(pal.handle, 'password') 
		
			
		
end 