function id = event__save(store, event, context)

% BARN - event__save

%---------------------------------
% SETUP
%---------------------------------

user = get_barn_user(store, context.request.user);

active_user = get_barn_user(store, get_active_user);

if user.id  == active_user.id
    robot = [];
else
    robot = user; user = active_user;
end

if ~strcmp(context.request.save, 'all')
	
	id = [event.id];
	
	% NOTE: we should not need these in the future
	
	if numel(id) ~= numel(event)
		
		% NOTE: the set of identifiers can only be smaller than the set of events
		
		if isempty(id)
			error('Events are missing identifiers in partial save request.');
		end

		error('Some events are missing identifiers in partial save request.');

	end
	
end

%---------------------------------
% HANDLE SAVE REQUEST
%---------------------------------

switch context.request.save
	
	% NOTE: we assume partial save requests always occur on saved events with identifiers
	
	case 'tags'
		
		set_taggings(store, id, {event.tags}, user, 'event');
		
	case 'rating'
		
		set_ratings(store, id, {event.rating}, user, 'event');
		
	case 'notes'
		
		% TODO: make sure that save notes handles identifier input
		
% 		set_notes(store, id, {event.notes}, user);
		
	case 'all'
		
		%--
		% keep basic metadata fields
		%--

		% NOTE: we intentionally lose these in the process of storing the basic event

		tags = {event.tags}; ratings = {event.rating}; notes = {event.notes};

		%--
		% save basic event
		%--
		
		event = prepare_events_for_store(store, event);
		
		% NOTE: the comma-separated list input limits this operation to only new or only known events
		
		event = set_barn_objects(store, event, [event.id]); id = [event.id];

		%--
		% get user and save author relation, tags, rating, and notes
		%--

		insert_relation(store, 'event', id, 'user', user.id);

        if ~isempty(robot)
            insert_relation(store, 'event', id, 'user', robot.id);
        end
        
		set_taggings(store, event, tags, user);

		set_ratings(store, event, ratings, user);

		% NOTE: soon to be finished

% 		set_notes(store, event, notes, user);
		
	otherwise
		
		error(['Unrecognized request to save ''', context.request.save, '''.']);
		
end


%-------------------------------------
% PREPARE_EVENTS_FOR_STORE
%-------------------------------------

function event = prepare_events_for_store(store, event)

%--
% get event-relevant recording info from store
%--

% NOTE: we ask only for recordings that contain event starts

file = [event.file]; position = unique([file.position]);

sql = [ ...
	'SELECT * FROM recording_sound', ...
	' WHERE sound_id = ', int2str(store.log.sound_id), ...
	' AND position IN (', str_implode(position, ', ', @int2str), ');' ...
];

[ignore, relation] = query(store, sql);

% NOTE: we translate the sound file position in the input event into a recording identifier

position = [relation.position];

for k = 1:numel(event)
	recording_id(k) = relation(position == event(k).file.position).recording_id;
end

recording_start = [file.start];

%--
% get table event and append BARN-specific foreign-keys
%--

% NOTE: this function is also used to store events in cache.db

event = get_table_event(event(:));

% NOTE: an event is contained in a log and starts in a recording

for k = 1:numel(event)

	event(k).recording_id = recording_id(k); event(k).recording_start = recording_start(k);

	event(k).log_id = store.log.id;

end
