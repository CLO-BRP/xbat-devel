function event = event__read(store, id, order, context)

% BARN - event__read

%--
% get events from database
%--

sql = ['SELECT * FROM event WHERE log_id = ', int2str(store.log.id), ';'];
	
if ~isempty(id)
	sql = [sql(1:end - 1), ' AND id IN (', str_implode(id, ', ', @int2str), ');'];
end

% NOTE: we get 'basic' events from store the 'full' event is the typical MATLAB event

[status, basic] = query(store, sql);

event = get_full_event(basic); id = [event.id];

% NOTE: there are no events on page, we are done

if isempty(event)
	return;
end

%--
% get core related data, this is in contrast to extension related data
%--

% AUTHOR

relation = get_relations(store, 'event', [event.id], 'user');

db_disp;

if ischar(relation(1).user_id)
    user_id = iterate(@str2int, unique({relation.user_id}));
else
    user_id = unique([relation.user_id]);
end

% user_id = iterate(@str2int, unique({relation.user_id}));

users = get_barn_users(store, user_id);

for k = 1:numel(event)

	% NOTE: logical indexing seems to be fail below!
	
	author = [relation(find(event(k).id == [relation.event_id])).user_id]; %#ok<FNDSB>
	
	for j = 1:numel(author)
		event(k).author{j} = users(author(j) == [users.id]).name;
	end
	
end

% TAGS

% NOTE: we only load our own tags and rating, we can get all tags for by omitting the 'user' input 

user = get_barn_user(store);

tags = get_taggings(store, event, user);

for k = 1:numel(event)	
	event(k) = set_tags(event(k), tags{k});
end

% RATING

% NOTE: we need to use a cell to store 'ratings' because some may be empty

rating = get_ratings(store, event, user);

for k = 1:numel(event)
	event(k).rating = rating{k};
end

% TODO: implement 'list' relations and 'parent' core relations, to store event sequences and hierarchies

