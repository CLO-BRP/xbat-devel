function id = find_by_attribute(log, attribute, value, relation, id)

% find_by_attribute - find log events by attribute values
% -------------------------------------------------------
%
% id = find_by_attribute(log, attribute, value, relation, id)
%
% Input:
% ------
%  log - log
%  attribute - name
%  value - to use in relation
%  relation - of attribute to value
%  id - scope of find
%
% Output:
% -------
%  id - found

%------------------
% HADNLE INPUT
%------------------

%--
% set empty default scope and get string
%--

if nargin < 5
	id = [];
end

if ~isempty(id)
	id =  ['(', to_str(id, ', '), ')'];
end

%--
% set default relation
%--

% TODO: we may be able to do this by inspecting value

%--
% get value string
%--

% NOTE: we quote string values

% TODO: handle array values, such as a cell array of strings

if ischar(value)
	value = ['''', value, ''''];
else
	value = to_str(value);
end 

%------------------
% FIND
%------------------

%--
% build statement
%--

% NOTE: we always select from 'id' from 'events' in find methods

if isempty(id)
	sql = ['SELECT id FROM events WHERE ', attribute, ' ', relation, ' ', value, ' ORDER BY id'];
else
	sql = ['SELECT id FROM events WHERE ', attribute, ' ', relation, ' ', value, ' AND id IN ', id, ' ORDER BY id'];
end

disp(sql);

%--
% execute statement
%--

% NOTE: this may not be correct, we need the context format method

id = event__find__by__sql(log, sql);
