function sound = set_database_sound(file, sound, id)

if nargin < 3
	id = [];
end

sound = set_database_object(file, sound, 'sound', {'name', 'user_id'}, id);