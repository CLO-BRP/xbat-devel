function user = get_database_user(file, name, like)

%--
% handle input
%--

if nargin < 3, like = 0; end

if nargin < 2, name = ''; end

%--
% get user
%--

opt = get_database_objects_by_column; opt.like = like;

user = get_database_objects_by_column(file, 'user', 'name', name, opt);