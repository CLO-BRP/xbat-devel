function result = event__extension__save(store, id, ext, data, context)

% SQLITE - event__extension__save

%-----------------
% SETUP
%-----------------

result = struct;

%--
% get extension table names
%--

table = extension_table_names(ext);

%--
% flatten data and attach event id to values
%--

for k = 1:length(id);

	data(k).parameter = flatten(data(k).parameter);

	% NOTE: the auxiliary 'event_id' is a foreign key that links event and values
	
	data(k).value = flatten(data(k).value); data(k).value.event_id = id(k);

end

%-----------------
% SAVE
%-----------------

%--
% insert parameters if necessary
%--

% NOTE: not all extensions have parameters

if ~isempty([data(:).parameter])

	% NOTE: we currently use parameter hash as parameter primary-key and value foreign-key
	
	for k = 1:length(id)
		data(k).parameter.id = []; data(k).parameter.hash = md5(data(k).parameter); 
	end
	
	% NOTE: we create the parameter table if needed
	
	if ~has_table(store.file, table.parameter)

		sqlite(store.file, 'exec', create_table(data(1).parameter, table.parameter));

	end

	opt = sqlite_array_insert; opt.resolve = 'ignore';
	
	sql = sqlite_array_insert(table.parameter, [data(:).parameter], fieldnames(data(1).parameter), opt);

	sqlite(store.file, 'exec', sql);
	
	% NOTE: here we establish the relation of data parameters and values
	
	sql = ['SELECT id, hash FROM ', table.parameter];
	
	result = sqlite(store.file, 'prepared', sql);
	
	% NOTE: we are breaking the typical foreign key name convention, which would require [table, '_id'], too ugly
	
	% NOTE: we are also breaking the typical value convention, the key value is a hash of the parameter values
	
	foreign_key = 'parameter_id'; 
	
	for k = 1:length(id)
		
		ix = find(strcmp(data(k).parameter.hash, {result.hash}), 1);
		
		data(k).value.(foreign_key) = result(ix).id;
		
	end
	
end

%--
% insert values
%--

if ~has_table(store.file, table.value)

	opt = create_table;
	
	opt.column(end + 1) = column_description('event_id', 'INTEGER', 'UNIQUE NOT NULL');
	
	sqlite(store.file, 'exec', create_table(data(1).value, table.value, opt));

end

sql = sqlite_array_insert(table.value, [data(:).value]);

sqlite(store.file, 'exec', sql);



