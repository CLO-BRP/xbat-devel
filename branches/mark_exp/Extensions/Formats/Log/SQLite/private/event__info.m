function info = event__info(store, id, order, context)

% SQLITE - event__info

% NOTE: This is not used at the moment, 'log_get_info' does this using the event cache for the log

if nargin < 3
    order = [];
end

if nargin < 2
    id = [];
end

sql = 'SELECT id, tags, score, rating, time, channel FROM events';

select = '';

if ~isempty(id)
    
    select = ' WHERE id IN (';
    
    for k = 1:length(id)
        select = [select, int2str(id(k))]; select = ternary(k < length(id), [select, ', '], [select, ')']);  %#ok<AGROW>
    end
        
end

if nargin > 2 && ~isempty(order) 
    sql = [sql, ' ORDER BY ', get_order_str(order)];  
end

sql = [sql, select, ';'];

[status, info] = sqlite(store.file, 'prepared', sql);
    

%-------------------------------
% GET_ORDER_STR
%-------------------------------

function order_str = get_order_str(order)

persistent ORDER;

if isempty(ORDER)
    ORDER  = {'time', 'score DESC', 'rating DESC', 'channel'};
end

ix = strmatch(order, ORDER);

if ix ~= 1
    
    tmp = ORDER{ix};
    
    ORDER(ix) = [];
    
    ORDER = {tmp, ORDER{:}};
    
end

order_str = '';

for k = 1:length(ORDER)
    
    order_str = [order_str, ORDER{k}];
    
    if k < length(ORDER)
        order_str(end + 1) = ',';
    end
    
end


    
    
    

