function id = event__save(store, event, context)

% SQLITE - event__save

%--
% insert basic event and get correspoding identifiers
%--

sqlite(store.file, sqlite_array_insert('event', get_table_event(event(:))));

% TODO: this is something that a lower level insert should provide

% NOTE: we infer the assigned these values based on the 'auto-increment' property of the column

result = sqlite(store.file, 'SELECT max(id) AS max_id FROM event;');

fill = cellfun(@isempty, {event(:).id});

id(~fill) = [event(:).id]; 

id(fill) = (result.max_id - sum(fill) + 1):result.max_id;

%--
% store core event related information
%--

% USER RELATION
%--------------------------

% NOTE: many users can edit a given event, and each user may edit many events

active = get_active_user; user = get_database_user(store.file, active.name);

if isempty(user)
	user = set_database_user(store.file, active);
end

insert_relation(store.file, 'event', id, 'user', user.id);


% RATING RELATION
%--------------------------

% NOTE: an event may have a rating from each user

% NOTE: the rating system is fixed when rating is established

has_rating = ~iterate(@isempty, {event.rating});

if any(has_rating)
	
	% NOTE: in this case the value is simple, in cases where the value is complex this notation helps
	
	% NOTE: normalizing values, suggested in some comment, does not hold if values are essentially unique
	
	value.user_id = user.id;
	
	insert_relation(store.file, 'event', id(has_rating), 'rating', [event.rating], 'value', value);

end


% TAG RELATION
%--------------------------

% NOTE: an event may have many tags, a tag may annotate many events

delete_relation(store.file, 'event', id, 'tag', []);

set_table_tags(store.file, 'event', id, {event.tags}, user.id);


% NOTES RELATION
%--------------------------

% NOTE: an event may have notes from each user







