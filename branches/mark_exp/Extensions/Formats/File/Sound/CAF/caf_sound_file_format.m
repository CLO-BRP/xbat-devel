function ext = caf_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Core Audio Format (Apple)';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'caf'};

% ext.version = '';

ext.guid = 'f5596fb7-8a0d-4711-bae3-9810a37d5e94';

% ext.author = '';

% ext.email = '';

% ext.url = '';

