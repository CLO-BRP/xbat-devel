function ext = mp3_sound_file_format

ext = extension_create(mfilename);

ext.short_description = 'MP3';

% ext.category = {};

ext.ext = {'mp3'};

ext.version = '0.1';

ext.guid = '7beef146-d66c-4986-a475-7f8eed71f5fa';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = '';

