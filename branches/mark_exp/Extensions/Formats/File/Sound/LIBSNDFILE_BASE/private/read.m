function [samples, options] = read(file, start, duration, channels, options)

% LIBSNDFILE_BASE - read

[samples, options] = read_libsndfile(file, start, duration, channels, options);
