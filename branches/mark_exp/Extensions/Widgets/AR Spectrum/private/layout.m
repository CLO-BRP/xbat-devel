function handles = layout(widget, parameter, context)

% AR SPECTRUM - layout

harray(widget, layout_create(1));

handles = harray_select(widget, 'level', 1);

set(handles, 'tag', 'spectrum');