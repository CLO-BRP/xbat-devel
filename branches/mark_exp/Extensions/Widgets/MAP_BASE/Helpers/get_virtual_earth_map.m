function map = get_virtual_earth_map(lat, lon, level, type, varargin)

% get_virtual_earth_map - get map using virtual earth service
% -----------------------------------------------------------
%
% map = get_virtual_earth_map(lat, lon, level, type, varargin)
%
% Input:
% ------
%  lat - latitude point or range
%  lon - longitude point or range
%  level - zoom
%  type - 'aerial', 'hybrid', or 'road'
%
% Output:
% -------
%  map - result
  
% TODO: setup options struct, integrate waitbar with url2image, create widget with sliding and zooming

%--
% handle input
%--

if nargin < 4
	type = 'aerial';
end

if ~string_is_member(type, {'aerial', 'road', 'hybrid'})
	error(['Unavailable map type ''', type, '''.']);
end

if nargin < 3 || isempty(level)
	level = 11;
end

if level ~= floor(level) || level < 1 || level > 23
	error('Zoom level must be an integer in the range 1-23.');
end

if nargin < 2 || isempty(lon)
	lon = -76.45 + 0.5 * [-1 1];
end

if nargin < 1 || isempty(lon)
	lat = 42.47 + 0.25 * [-1 1];
end

%--
% create output prototype, pack request and output information
%--

map.request.lat = lat; map.request.lon = lon; map.request.level = level; map.request.type = type;

start = clock;

% NOTE: these are output fields and the declaration allows us to order them

map.image = []; map.lat = []; map.lon = [];

%--
% get map images
%--

% TODO: implement the 'hard-coded' field value pairs as options

[map.image, map.lon, map.lat] = url2image('tile2img', lon, lat, level, 'what', type, 'verbose', 'true', 'cache', cache_root);

map.request.elapsed = etime(clock, start);

flatten(map)

%--
% display map if needed
%--

if ~nargout
	view_map(map);
end


% NOTE: the computations below are described at http://msdn.microsoft.com/en-us/library/bb259689.aspx

%--------------------------
% GET_LAT_LON_PIXEL
%--------------------------

function [i, j] = get_lat_lon_pixel(lat, lon, level)

% NOTE: the longitude is on a linear scale, the latitude is not

j = ((lon + 180) / 360) * map_full_size(level);

slat = sin(lat * pi/180);

i = (0.5 - log((1 + slat) / (1 - slat)) / (4 * pi)) * map_full_size(level);


%--------------------------
% MAP_GROUND_RES
%--------------------------

function res = map_ground_res(lat, level)

% NOTE: this is the number of meters per pixel in the horizontal

res = (cos(lat * pi/180) * 2 * pi * 6378137) / map_full_size(level);


%--------------------------
% MAP_FULL_SIZE
%--------------------------

function total = map_full_size(level)

if level < 1 || level > 23
	error('Level must be an integer between 2 and 19.');
end

total = 256 * 2^level;


