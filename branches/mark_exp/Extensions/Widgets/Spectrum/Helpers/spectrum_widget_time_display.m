function [handle, str] = spectrum_widget_time_display(widget, time, context)

%--
% find display object
%--

ax = spectrum_axes(widget, context.page.channels(1));

if isempty(ax) 
	return;
end

handle = get(ax, 'title');

if isempty(handle)
	return; 
end

%--
% display browser time
%--

str = get_browser_time_string(context.par, time, context);

set(handle, 'string', str);
