function [handles, context] = on__marker__create(widget, data, parameter, context)

% SPECTRUM - on__marker__create

handles = [];

%--
% clear previous marker display
%--

on__marker__delete(widget, data, parameter, context);

%--
% get channel spectrogram slices data
%--

% NOTE: the marker is a proper event duck

[slice, freq] = get_spectrogram_slices(data.marker, context);

if isempty(slice)
	return;
end

freq = linspace(freq(1), freq(2), numel(slice));

%--
% display marker line
%--

% NOTE: we may have to delete marker lines before we do this

ax = spectrum_axes(widget, data.marker.channel);

if isempty(ax)
	return;
end 

% NOTE: often simple spectral peaks are easier to appreciate in linear units

spectrum_marker_line(ax, data.marker.channel, ...
	'color', data.marker.color, ...
	'xdata', freq, ...
	'ydata', slice ...
);

%--
% display spectral peaks
%--

% TODO: add configuration parameters, whether to display for example

% NOTE: also configure the display of guides and the number of peaks to display

opt = peak_display; opt.color = data.marker.color; opt.peaks = parameter.peaks; opt.fast = 0;

handles = [handles(:)', peak_display(ax, freq, slice, 'marker_peaks', opt)];

%--
% update peak label to contain time, frequency, and intensity information
%--

% NOTE: this depends on the way handles are packed by 'peak_display'

value = get(handles(end), 'userdata');

if isempty(value)
	return;
end 

str = get_peak_value_string(data.marker.time, value, context);

set(handles(end), 'string', str);


