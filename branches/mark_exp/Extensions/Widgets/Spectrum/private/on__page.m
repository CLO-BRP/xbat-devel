function [handles, context] = on__page(widget, data, parameter, context)

% SPECTRUM - on__page

handles = [];

%--
% display spectra
%--

channels = context.page.channels;

for k = 1:length(channels)
	
	data.channel = channels(k); 
	
	display_channel_spectrum(widget, data, parameter, context);

end

% NOTE: this clears the spectrogram image cache, consider creating shortcut

clear get_spectrogram_slice_mean;


%----------------------------------
% DISPLAY_CHANNEL_SPECTRUM
%----------------------------------

function handles = display_channel_spectrum(widget, data, parameter, context)

handles = [];

%--
% get channel spectrogram slices data
%--

channel = data.channel;

event.time = []; event.channel = channel;

% NOTE: we need to get these axes for the helper, this should be formalized

event.axes = get_channel_axes(context.par, event.channel);

[slices, freq] = get_spectrogram_slices(event, context);

if isempty(slices)
	return;
end

%--
% get display handles
%--

ax = spectrum_axes(widget, channel); [center, low, high] = page_line(ax, channel);

handles = [center(:); low(:); high(:)];

%--
% compute spectrum deviation estimates
%--

[rows, cols] = size(slices);

S = sum(slices, 2) ./ cols;

D = sqrt(sum((slices - S * ones(1, cols)).^2, 2) ./ (cols - 1));

%--
% display lines
%--

grid = linspace(freq(1), freq(2), rows);

set(center,'xdata', grid, 'ydata', S);

set(low, 'xdata', grid, 'ydata', S - D);

set(high, 'xdata', grid, 'ydata', S + D);

%--
% set spectrum y axis limits according to page
%--

% NOTE: we can get this axes because the slices were obtained there

clim = get(get_channel_axes(context.par, channel), 'clim');

set(ax, 'ylim', clim, 'xlim', data.page.freq);






