function [handles, context] = on__play__stop(widget, data, parameter, context)

% SPECTRUM - on__play__stop

handles = [];

%--
% delete time display
%--

set(spectrum_widget_time_display(widget, [], context), 'string', '');

%--
% delete play lines
%--

channels = context.page.channels;

for k = 1:length(channels)

	channel = channels(k);
	
	ax = spectrum_axes(widget, channel);

	delete(play_line(ax, channel));
	
end

%--
% delete peak display
%--

delete_obj(widget, 'play_peaks');

%--
% restore peak display
%--

label = findobj(widget, 'tag', 'PEAK_LABEL_TEXT');

if ~isempty(label)
	set(label, 'visible', 'on');
end

% TODO: the last line causes a problem where play does not stop

% NOTE: this is needed because currently the play update peak display clobbers the marker information in the label

% set_browser_marker(context.par);
