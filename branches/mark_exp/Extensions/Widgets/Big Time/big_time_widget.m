function ext = big_time_widget

ext = extension_create(mfilename);

ext.short_description = 'Big display of current time';

% ext.category = {};

ext.version = '0.1';

ext.guid = '513f03e2-4d14-4068-89ed-8cbfa96d5fe8';

ext.author = 'Harold and Matt';

ext.email = '';

ext.url = 'http://xbat.org';

