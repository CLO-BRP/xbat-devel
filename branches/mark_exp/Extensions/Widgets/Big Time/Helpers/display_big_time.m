function handle = display_big_time(widget, time, context)

% display_big_time - big time display helper
% ------------------------------------------
%
% handle = display_big_time(widget, time, context)
%
% Input:
% ------
%  widget - widget
%  time - time
%  context - context
%
% Output:
% -------
%  handle - text handle

%--
% find display axes
%--

ax = findobj(widget, 'tag', 'big_time_axes');

if isempty(ax)
	handle = []; return;
end 

%--
% display big time string
%--

str = get_browser_time_string([], time, context);

handle = big_centered_text(ax, str);
