function [handles, context] = on__play__stop(widget, data, parameter, context)

% BIG TIME - on__play__stop

%--
% reset the page marker display
%--

% NOTE: now all pages have a marker, it is more like a time cursor

marker = get_browser_marker(context.par); data.marker = marker;

[handles, context] = on__marker__create(widget, data, parameter, context);
