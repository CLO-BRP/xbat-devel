function [handles, context] = on__marker__create(widget, data, parameter, context)

% SCOPE - on__marker__create

%--
% get marker samples
%--

marker = data.marker;

samples = get_marker_samples(marker, parameter, context);

%--
% update scope display
%--

handles = update_scope_display(widget, marker.time, parameter.duration, samples, context);

set(handles, 'color', marker.color);

%--
% scale axes for a better display
%--

% TODO: add parameters to adjust scaling, consider: fixed, fixed range, and fully adaptive

% NOTE: the fixed range would presumably float the center of the display

ylim = get_scope_ylim(samples, parameter);

if diff(ylim)
	scope_axes(widget, 'ylim', ylim);
end