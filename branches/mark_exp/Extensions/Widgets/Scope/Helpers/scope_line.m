function [scope, created] = scope_line(ax, varargin)

[scope(1), created(1)] = create_line(ax(1), 'scope_line::1', varargin{:});

% NOTE: in the case of single channel files we have a single scope axes

if length(ax) > 1
	[scope(2), created(2)] = create_line(ax(2), 'scope_line::2', varargin{:});
end

if any(created)
	set(scope, 'clipping', 'off');
end