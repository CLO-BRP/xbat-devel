function samples = get_marker_samples(marker, parameter, context)

% NOTE: this can probably more general, but this is a little helper

%--
% get marker page
%--

page.start = marker.time - 0.5 * parameter.duration; 

% NOTE: should this derive from a marker property or scope parameter?

page.duration = parameter.duration;

%--
% get marker samples from context sound
%--

page = read_sound_page(context.sound, page, marker.channel);

page = filter_sound_page(page, context);

if isempty(page.filtered)
	samples = page.samples;
else
	samples = page.filtered;
end