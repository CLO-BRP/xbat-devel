function [handles, context] = on__timer(widget, data, parameter, context)

% CLOCK - on__timer

ax = findobj(widget, 'tag', 'clock_axes');

if isempty(ax)
	handles = []; return;
end 

handles = big_centered_text(ax, datestr(clock));
