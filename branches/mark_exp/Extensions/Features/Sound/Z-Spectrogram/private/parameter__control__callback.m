function result = parameter__control__callback(callback, context)

% Z-SPECTROGRAM - parameter__control__callback

result = struct;

pal = callback.pal;

switch callback.control.name
	
	case 'threshold_on'
		
		set_control(pal.handle, 'threshold', 'enable', get(callback.obj, 'value'));
		
	case 'score'
		
		value = get_control(pal.handle, 'score', 'value');
		
		set_control(pal.handle, 'threshold', 'min', value);
		
end