function [handles, context] = view__on__compute(widget, data, parameter, context)

% Z-SPECTROGRAM - view__on__compute

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%-------------------------
% DISPLAY COMPONENTS
%-------------------------

page = context.page;

handles = zeros(size(data.component.value)); ix = 1;

for k = 1:length(page.channels)
	
	channel = page.channels(k);	ax = get_channel_axes(widget, channel);
	
	if isempty(ax)
		continue;
	end
	
	component = data.component.value([data.component.value.channel] == channel);
	
	for j = 1:length(component)
		handles(ix) = plot_component(ax, component(j), parameter, context); ix = ix + 1;
	end
	
end


%-------------------------
% PLOT_COMPONENT
%-------------------------

function handles = plot_component(ax, component, parameter, context)

%--
% display component box
%--

start = component.time(1); stop = start + component.duration;

low = component.freq(1); high = low + component.bandwidth;

x = [start, stop, stop, start, start]; y = [low, low, high, high, low];

handles = patch( ...
	x, y, 'b'); 

set(handles, ...
	'parent', ax, ...
	'edgecolor', 0.75 * ones(1, 3), ...
	'linestyle', '-', ...
	'facecolor', 'none' ...
);

