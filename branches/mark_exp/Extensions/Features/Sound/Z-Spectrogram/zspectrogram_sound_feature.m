function ext = zspectrogram_sound_feature

ext = extension_inherit(mfilename, spectrogram_sound_feature);

ext.short_description = 'Convert spectrogram values to Z-score';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '2102f2b6-f854-49cb-a12b-87960b632696';

ext.author = 'Harold Figueroa';

% ext.email = '';

% ext.url = '';

