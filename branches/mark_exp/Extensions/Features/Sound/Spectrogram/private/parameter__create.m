function parameter = parameter__create(context)

% SPECTROGRAM - parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% parameter = struct;

%--
% frame parameters
%--

% TODO: the default values should be more strongly based on the context sound

[ignore, default] = resolution_ranges(context); 

parameter.frame = default.time;

parameter.advance = 0.25;

parameter.adapt = 0;

parameter.slices = 1000;

%--
% fft parameters
%--

parameter.fft = default.fft;

parameter.window = 'Hann';

%--
% smoothing parameters
%--

parameter.smooth = 1;

parameter.method = 'binomial';

% NOTE: this is arbitrary at the moment

parameter.width = 5;

%--
% common spectrogram for children
%--

% NOTE: this means that all children do not have to declare this meta-parameter

% NOTE: typically a child will create a control for this parameter to make use of the service

parameter.use_common_spectrogram = 0;
