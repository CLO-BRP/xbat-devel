function duration = required_page_duration(page, parameter, context) %#ok<INUSL>

%--
% compute required page duration
%--

dt = specgram_resolution(parameter, page.rate);

hop = (parameter.fft - round((1 - parameter.hop) * parameter.fft));

duration = ((page.duration / dt) * hop * parameter.sum_length + parameter.fft) / page.rate;