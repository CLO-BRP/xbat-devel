function result = render(feature, context)

% SPECTROGRAM - render

result = struct;

%--
% prepare image and colormap
%--

% NOTE: flip row order, compute decibel scaling and map to typical image range

X = flipud(feature.spectrogram.value); X = lut_range(lut_db(X));

% TODO: this should be computed using the current view settings

C = get_spectrogram_colormap(context.ext.parameters.view, X);

%--
% write file and pack output
%--

file = [context.prefix, '.jpg']; 

imwrite(X, C, file);

result.image = imfinfo(file);
