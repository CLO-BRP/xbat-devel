function [handles, context] = view__on__marker__create(widget, data, parameter, context)

% SPECTROGRAM - view__on__marker__create

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

% db_disp;

if parameter.cursor && parameter.track
	
	%--
	% try to estimate fundamental
	%--
	
	fundamental = fundamental_estimate(widget, data, parameter, context);
	
	if isempty(fundamental)
		return;
	end
	
	%--
	% update control and parameter, then update display
	%--
	
	control = set_control(context.pal, 'fundamental', 'value', fundamental);
	
	parameter.fundamental = control.value;
	
	handles = display_harmonic_cursor(widget, parameter, context);
	
end