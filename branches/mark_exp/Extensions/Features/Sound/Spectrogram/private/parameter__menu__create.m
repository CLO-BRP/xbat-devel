function handles = parameter__menu__create(par, parameter, context)

% SPECTROGRAM - parameter__menu__create

handles = scaffold_menu(par, 'compute', context, {'plot_type'});