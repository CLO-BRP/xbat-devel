function [handles, context] = view__on__event__display(widget, data, parameter, context)

% SPECTROGRAM - view__on__event__display

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

freq = map_frequency(data.event.freq, parameter, context, 0);

ydata = freq(2) * ones(1, 5); ydata(2:3) = freq(1);

for k = 1:numel(handles)
	set(handles(k), 'ydata', ydata);
end
