function result = view__parameter__control__callback(callback, context)

% SPECTROGRAM - view__parameter__control__callback

%--
% setup
%--

result = struct;

pal = callback.pal; parameter = context.ext.parameters.view;

%--
% perform callback
%--

if is_tabs_control(callback.control)
	return; 
end

switch callback.control.name
	
	% FREQUENCY CONTROLS
	
	case 'scale'
			
		scale = cellfree(callback.value)

		prop = 'enable'; state = strcmpi(scale, 'linear-log');

		set_control(pal.handle, 'split', prop, state);

		set_control(pal.handle, 'split_freq', prop, state);
		
	case {'cursor', 'fundamental', 'harmonics'}
		
		widget = find_widget(context.par, context.ext);

		display_harmonic_cursor(widget, parameter, context);
		
		return;
		
	case 'track'
		
		% TODO: call tracking code, which must be factored, then update display
		
		if ~callback.value
			return;
		end
		
		db_disp('call tracking code')
		
		widget = find_widget(context.par, context.ext);

		display_harmonic_cursor(widget, parameter, context);
		
		return;
	
	% VALUE CONTROLS
	
	case 'adapt_range'
		
		state = ~parameter.adapt_range;
		
		set_control(pal.handle, 'min_value', 'enable', state);
		
		set_control(pal.handle, 'max_value', 'enable', state);
		
	% COLORMAP CONTROLS
	
	case {'colormap', 'invert', 'contrast', 'brightness'}
		
		widget = find_widget(context.par, context.ext);

		set_spectrogram_colormap(widget, parameter);
		
		return;
		
	case 'adapt_contrast'
		
		state = ~parameter.adapt_contrast;
		
		set_control(pal.handle, 'brightness', 'enable', state);
		
		set_control(pal.handle, 'contrast', 'enable', state);

		widget = find_widget(context.par, context.ext);

		% map = get_spectrogram_colormap(context.ext.view.parameters,

		set_spectrogram_colormap(widget, parameter);

		return;

end

%--
% update feature view
%--

feature_refresh(context);





