function f0 = fundamental_estimate(widget, data, parameter, context)

db_disp; stack_disp; context

%--
% get spectral estimate
%--

% TODO: do this using the already computed spectrogram

% NOTE: this is a test, we intend to use the displayed spectrogram

marker = data.marker; parameter.duration = 1024 / context.sound.rate;
	
marker.samples = get_marker_samples(marker, parameter, context);

value = abs(fft(hilbert(diff(marker.samples))));

freq = linspace(0, context.sound.rate, numel(value));	

% slice = get_marker_slice(marker, widget, context);

if context.debug
	
	par = create_figure('TRACK_TEST'); clf(par); ax = axes('parent', par);

	axes(ax); plot(freq, value); set(ax, 'xlim', [parameter.min_freq, parameter.max_freq]);

	[m, ix] = max(value); hold on; plot(freq(ix), m, 'rx', 'markersize', 20);

end


%--
% compute fundamental frequency estimate
%--

% NOTE: we get an initial estimate, then we refine it a few times and compare the scores

[ix0, score, ixh{1}] = get_estimate_index(value);

if isempty(ix0)
	f0 = []; return;
end

for k = 2:10
	[ix0(k), score(k), ixh{k}] = get_estimate_index(value, round(ix0(1) / k));
end

[score, ix] = max(score); f0 = freq(ix0(ix));

if context.debug
	
	db_disp; freq(ix0), score

	color = flipud(jet(numel(ixh)));

	for k = 1:numel(ixh)
		axes(ax); plot(freq(ixh{k}), value(ixh{k}), 'o', 'markersize', 8 + 3 * k^1.25, 'color', color(k, :));
	end

end

%--
% veto an estimate that is more than half the nyquist rate
%--

% NOTE: in this case we can only observe a single harmonic

if f0 > 0.25 * context.sound.rate
	f0 = [];
end 


%--------------------------------
% GET_ESTIMATE_INDEX
%--------------------------------

function [ix0, score, ixh] = get_estimate_index(value, ix0)

% TODO: smooth spectral information and/or move off the grid through interpolation

consider = 16;

%--
% get initial estimate for fundamental
%--

% NOTE: we get max amplitude, a peak except for the degenerate case of edges, ignore this possibility

if nargin < 2
	[ignore, ix0] = max(value);
end

% NOTE: we allow the peaks to be offset by 5 percent of the fundamental, quantized

max_offset = round(0.05 * ix0);

%--
% get top peaks and match to imputed harmonics
%--

ix1 = fast_peak_valley(value, 1);

ixh = ix0:ix0:numel(value);

if numel(ixh) > consider
	ixh(consider + 1:end) = [];
end

ix1(ix1 > ixh(end) + max_offset + 1) = [];

h = 1:numel(ixh);

for k = numel(ixh):-1:1
	
	% NOTE: we search for a nearby matching peak for the imputed harmonic
	
	[offset, ixk] = min(abs(ixh(k) - ix1)); 
	
	% NOTE: we may either update the position of the proposed harmonic or discard it
	
	if offset <= max_offset
		ixh(k) = ix1(ixk);
	else
		ixh(k) = []; h(k) = [];
	end
	
end

if isempty(ixh)
	score = 0; return;
end

weight = value(ixh); score = sum(weight);  weight = weight' / score;

% TODO: reconsider the weighing

ix0 = round(sum(weight .* (ixh./ h))); score = sum(value(ixh));


%--------------------------------
% GET_MARKER_SLICE
%--------------------------------

% NOTE: most of this code is taken from the 'get_spectrogram_slices' in the Spectrum widget

function [slice, freq] = get_marker_slice(marker, widget, context)

slice = []; freq = [];

%-------------------
% HANDLE INPUT
%-------------------

time = marker.time;

%-------------------
% SETUP
%-------------------

%--
% get spectrogram data from image
%--

% TODO: make sure we always have the axes

if isfield(marker, 'axes')
	ax = marker.axes;
else
	ax = get_channel_axes(widget, marker.channel);
end

im = findobj(ax, 'type', 'image');

if isempty(im)
	return;
end

X = get(im, 'cdata'); cols = size(X, 2); times = get(im, 'xdata'); 

%--
% get frequency bounds in Hz
%--

freq = get(im, 'ydata');

% NOTE: this is a perhaps undesirable link between view and computation, we are getting data from the view

if context.display.grid.freq.labels(1) == 'k'
	freq = 1000 * freq;
end

%--
% compute slice index from slice and image times
%--

ix = round(cols * (time - times(1)) / diff(times));

% NOTE: this is a strange condition, we don't want to infer start and end

if any(ix < 1) || any(ix > cols)
	return;
end
	
%--
% get slices
%--

switch length(ix)
	
	% NOTE: if no selection criteria are proposed we will get all slices
	
	case 0, slice = X;
		
	case 1, slice = X(:, ix);
		
	case 2, slice = X(:, ix(1):ix(2));
		
end


