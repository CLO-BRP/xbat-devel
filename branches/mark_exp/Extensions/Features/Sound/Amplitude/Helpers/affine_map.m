function X = affine_map(X, scale, offset)

%--
% set trivial affine map default
%--

if nargin < 3 || isempty(offset)
	offset = 0;
end

if nargin < 2 || isempty(scale)
	scale = 1;
end

%--
% compute map
%--

% NOTE: the higher type requires more computation

type = (scale ~= 1) * 2 + (offset ~= 0);

switch type
	
	case 0, return;
		
	case 1, X = X + offset;
		
	case 2, X = scale * X; 
		
	case 3, X = scale * X + offset;
		
end