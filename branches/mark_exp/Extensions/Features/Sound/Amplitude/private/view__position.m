function widget = view__position(widget, parameter, context)

% AMPLITUDE - view__position

%--
% set signal feature position
%--

fun = parent_fun(mfilename('fullpath')); widget = fun(widget, parameter, context);

%--
% update for typical amplitude
%--

pos = get(widget, 'position'); pos(4) = 0.7 * pos(4);

set(widget, 'position', pos);

position_palette(widget, context.par, 'bottom right');
