function ext = amplitude_sound_feature

ext = extension_inherit(mfilename, signal_feature_sound_feature);

ext.short_description = 'Various signal amplitude measures';

ext.category = {ext.category{:}, 'Amplitude'};

% ext.version = '';

ext.guid = 'a2816ecf-2866-4759-b4ce-a5385146f946';

% ext.author = '';

% ext.email = '';

% ext.url = '';

