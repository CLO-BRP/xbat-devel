function parameter = view__parameter__create(context)

% ENVELOPE - view__parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.color = 'dark green';
