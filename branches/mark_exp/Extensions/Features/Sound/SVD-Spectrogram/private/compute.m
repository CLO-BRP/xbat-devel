function [feature, context] = compute(page, parameter, context)

% SVD-SPECTROGRAM - compute

% feature = struct;

%--
% compute parent spectrogram
%--

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% compute partial decomposition, project, and possibly subtract
%--

X = feature.spectrogram.value;

if parameter.normalize
	X = normalize_dim(X, 1);
end

[U, S, V] = svds(X, parameter.rank);

if parameter.residual
	X = X - U * S * V';
else 
	X = U * S * V';
end

%--
% pack feature
%--

feature.spectrogram.value = X;

feature.svd.U = U; feature.svd.S = S; feature.svd.V = V;