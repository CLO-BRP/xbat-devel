function [in new_freq] = cutoff(in, freq_in,rel_idx)
% cuts off irrelevant repetition data

if ~iscell(in)
    [in, new_freq] = cutoff_int(in, freq_in, rel_idx);
else
    for k = 1:numel(in)
        [in{k}, new_freq] = cutoff_int(in{k}, freq_in, rel_idx);
    end
end

function [in new_freq] = cutoff_int(in, freq,rel_idx)

a = rel_idx(1);
b = rel_idx(2);

new_freq = freq(a:b);
if iscell(in)
    for i = 1:numel(in)
        in{i} = in{i}(a:b,:);
    end
else
    in = in(a:b,:);
end 
