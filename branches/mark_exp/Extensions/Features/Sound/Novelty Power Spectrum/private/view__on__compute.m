function [handles, context] = view__on__compute(widget, data, parameter, context)

% NOVELTY POWER SPECTRUM - view__on__compute

%--
% unpack feature for convenience
%--

R = data.novelty_power_spec.value;

time = data.novelty_power_spec.time;

freq_in = data.novelty_power_spec.freq;

rel_idx = data.novelty_power_spec.rel_idx;

if ~iscell (R)
    num_channels = 1;
    num_bands = 1;
else
    if iscell(R{1})
        num_channels = numel(R);
        num_bands = numel(R{1});
    else
        num_channels = 1;
        num_bands = numel(R);
    end
end

page = get_browser_page(context.par);

if ~isequal(context.page.channels, page.channel)
	page.channel = context.page.channels;
end

% ---
% cutoff frequencies for correct display
% ---
[R, freq] = cutoff(R, freq_in, rel_idx);

% ---
% get frequency axis for display
% ---
freq_axis = repmat(freq,1,num_bands);

freq_pos = round(linspace(1,numel(freq),8));
freq_ticks = freq_pos;
for i=1:num_bands-1
    freq_ticks = [freq_ticks freq_pos + i * numel(freq)];
end
freq_axis = freq_axis(floor(freq_ticks));


handles = [];
for k = 1:num_channels

	%--
	% setup
	%--
	
	if num_channels == 1
        if num_bands == 1
            val{1} = R;
        else
            val = R;
        end
    else
        val = R{k};
    end
    
    

	ax = get_channel_axes(widget, page.channel(k));

	if isempty(ax)
		continue;
	end
	
	%--
	% pack band spectra as single image
	%--   
    
    % TODO: we should eventually reveal the scaling as a view option, not
    % there yet
    if parameter.normalize
        for i = 1:numel(val)
            val{i} = val{i} ./ max(val{i}(1,:));
        end
    end

	val = cell2mat(val');

	%--
	% display nps image
	%--
	
	% TODO: note that we are not ready to resolve the frequency axes display with this view strategy
	
	handles(end + 1) = image( ...
		'parent', ax, ...
		'xdata', time, ...
		'ydata', 1:size(val, 1), ...
		'cdata', val, ...
		'cdatamapping', 'scaled' ...
	); 
	context.view.paging(k) = 1;

	set(ax, ...
		'ydir', 'normal', ...
		'xlim', [time(1), time(end)], ...
		'ylim', [1, size(val, 1)], ...
        'YTick', freq_ticks, ...
        'YTickLabel', freq_axis ...
	);

end


