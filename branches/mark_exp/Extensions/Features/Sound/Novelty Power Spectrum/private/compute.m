function [feature, context] = compute(page, parameter, context)

% NOVELTY POWER SPECTRUM - compute

% get as many samples as we can

page = extend_page_with_buffer(page);

% feature = struct;

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--------------------------

% TODO: much of this belongs in a parameter compile function

% get energy "sampling rate"
fps = 1/feature.resolution.time;


%--------------------------
% consider multiple channels

if ~iscell(feature.spectrogram.value)
    
    [R, freq, time, ucsum] = get_nps (feature.band_novelty.value', fps, parameter);

    feature.novelty_power_spec.value = R;
    
    % ---
    % here, we save the overall spectral energy for later use in domination
    % access etc.
    % ---
    feature.novelty_power_spec.ref_energy = ucsum;
else
    for k=1:numel(feature.spectrogram.value)
        
        [R, freq, time, ucsum] = get_nps(feature.band_novelty.value{k}', fps, parameter);
        
        feature.novelty_power_spec.value{k} = R;
        
        feature.novelty_power_spec.ref_energy{k} = ucsum;
    end
end

% save timing information etc.

feature.novelty_power_spec.freq=freq;

feature.novelty_power_spec.time = time + page.start;

feature.novelty_power_spec.time_step = time(2)-time(1);

feature.novelty_power_spec.rate = 1/(time(2)-time(1));

feature.novelty_power_spec.rel_f = [parameter.min_repfreq parameter.max_repfreq];

feature.novelty_power_spec.rel_idx = [ ...
    find(freq >= parameter.min_repfreq,1,'first') ...
    find(freq <= parameter.max_repfreq,1,'last')];
    

% ---------------------------------------------------------
% here, we calculate the power spectrum of novelty curves
% ---------------------------------------------------------
function [R, freq, time, ucsum] = get_nps (val, fps, parameter)

% set FFT parameters
fft_params = fast_specgram;
fft_params.sum_auto = 0;
fft_params.hop_auto = 0;

% ---
% note:
% this is a difficult tradeoff between the needed window length for
% detecting long-time repetitions and blurring for higher frequencies.
% min_repfreq should be set as high as possible for good performance and
% less blurred features
% ---
fft_params.fft = ceil(fps*max(1,1/parameter.min_repfreq));

% set hop size
fft_params.hop = (fps/parameter.temp_res)/(fft_params.win_length*fft_params.fft);

  
% get windowed spectrogram
[R, freq, time] = fast_specgram(val, fps, 'norm', fft_params);

% get reference sum 
if iscell(R)
    for k = 1:numel(R);
        ucsum{k} = sum(R{k},1);
    end
else
    ucsum = sum(R,1);
end

% NOTE: the cutoff is done in the visualization, as there is too much need
% for the full spectrum in following applications
% cutoff irrelevant frequencies
% [R, freq] = cutoff(R, freq, parameter);



% TODO: this function is likely to be factored, if it does not already exist

function page = extend_page_with_buffer(page)

if ~page.buffer
	return;
end

%--
% extend page
%--

page.start = page.start - page.buffer;

page.duration = page.duration + 2 * page.buffer;

page.samples = [page.before.samples; page.samples; page.after.samples];

%--
% remain consistent
%--

page.before.samples = []; page.after.samples = []; page.buffer = 0;




