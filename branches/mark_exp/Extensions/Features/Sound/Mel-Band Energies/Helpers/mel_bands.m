function band_details = mel_bands(parameter)
% mel_matrix - compute mel bands

%--
% compute filter centers and edges
%--

f1 = parameter.num_lin;
f2 = parameter.num_log;
t = parameter.trans_freq;

c1 = linspace(0, t ,f1+2);

c2 = logspace(log10(c1(end-1)), log10(1), f2+2);

c1(end)=c2(2);

if parameter.side_bands
    c1=[0 c1];
    c2=[c2 1];
    band_details=zeros(3,numel(c1)+numel(c2)-4);
end

band_details=zeros(3,numel(c1)+numel(c2)-4);

for k=1:numel(c1)-2
    band_details(:,k)=c1(k:k+2);
end
for k=1:numel(c2)-2
    band_details(:,numel(c1)-2+k)=c2(k:k+2);
end



