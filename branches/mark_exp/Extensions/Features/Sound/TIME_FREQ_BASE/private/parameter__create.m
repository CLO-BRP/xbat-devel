function parameter = parameter__create(context)

% TIME_FREQ_BASE - parameter__create

%--
% inherit parent parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% describe distribution in own parameters
%--

parameter.fun = @choi_williams1;

rate = context.sound.rate;

parameter.args = arg_create( ...
	'name', 'sigma', 'default', 0.25 * rate, 'range', {0.05 * rate, rate} ...
);