function control = parameter__control__create(parameter, context)

% TIME_FREQ_BASE - parameter__control__create

control = empty(control_create);

if isempty(parameter.args)
	return;
end 

% NOTE: here we generate some controls based on the parameter 'argument' description

% TODO: figure out how to use the context in 'arg_to_control' or have we consumed the context in 'parameter__create'

for k = 1:numel(parameter.args)
	control(end + 1) = arg_to_control(parameter.args(k));
end

control(end).space = 1.5;