function [handles, context] = view__on__compute(widget, data, parameter, context)

% SPECTROGRAM - view__on__compute

% TODO: fix discrepancy between image display and axes, in frequency scale

%-----------------
% SETUP
%-----------------

handles = [];

%--
% set some variables for clarity
%--

% TODO: when we toggle the display, the page is not the same as when we page, we are missing the 'freq' field

page = context.page;

time = [data.time(1), data.time(end)]; 

freq = [data.freq(1), data.freq(end)]; % NOTE: this is the feature freq range, not the display

%--
% partially handle orientation
%--

if parameter.horizontal
	xdata = time; ydata = freq;
else
	xdata = freq; ydata = time;
end

%-----------------
% DISPLAY
%-----------------

for k = 1:length(page.channels)
	
	%--
	% get and update axes
	%--
	
	ax = findobj(widget, 'type', 'axes', 'tag', int2str(page.channels(k)));
	
	if isempty(ax)
		continue;
	end
	
	im = findobj(ax, 'type', 'image');
	
	%--
	% get spectrogram image data
	%--
	
	% TODO: factor contingent display code
	
	if ~isfield(data, 'spectrogram')
		
% 		handle = big_centered_text(ax, 'No spectrogram available at this resolution.'); 
% 		
% 		set(handle, 'tag', 'no-data-message');
		
		if ~isempty(im)
			set(im, 'visible', 'off');
		end
		
		continue;	
		
	end
	
	switch class(data.spectrogram.value)

		case 'double', X = data.spectrogram.value;

		case 'cell',  X = data.spectrogram.value{k};

	end

	X = lut_dB(X);

	if ~parameter.horizontal
		X = X';
	end
	
% 	delete(findobj(ax, 'tag', 'no-data-message'));

	if ~isempty(im)
		set(im, 'visible', 'on');
	end
	
	%--
	% display spectrogram image
	%--

	if isempty(im)
		
		% NOTE: the 'get' and 'set' of callbacks is because 'imagesc' has a habit of clearing these
		
		callback = get_callbacks(ax);
		
		imagesc(xdata, ydata, X, 'parent', ax); context.view.paging(k) = 1;
		
		set_callbacks(ax, callback);
		
	else
		
		paging = isfield(context.view, 'paging') && numel(context.view.paging) >= k && context.view.paging(k);
		
		if ~paging
			
			set(im, 'xdata', xdata, 'ydata', ydata, 'cdata', X); context.view.paging(k) = 1;
			
		else
			
			X0 = get(im, 'cdata'); start = get(im, 'xdata'); start = start(1);
			
			if parameter.horizontal
				X = [X0, X]; time = [start, time(2)]; field = 'xdata';
			else
				X = [X0; X]; time = [start, time(2)]; field = 'ydata';
			end
			
			set(im, field, time, 'cdata', X);
			
		end
			
	end
	
	% NOTE: we should consider using this strategy above
	
	if parameter.horizontal
		prefix.freq = 'y'; prefix.time = 'x';
	else
		prefix.freq = 'x'; prefix.time = 'y';
	end 
	
	set(ax, ...
		'tag', int2str(page.channels(k)), ...
		[prefix.time, 'lim'], time, ...
		[prefix.freq, 'lim'], [parameter.min_freq, parameter.max_freq], ...
		[prefix.freq, 'dir'], 'normal' ...
	);

end
