function ext = time_freq_base_sound_feature

ext = extension_inherit(mfilename, signal_feature_sound_feature);

ext.short_description = 'Base for Time-Freq Distribution Features';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '1a71e32c-8a61-4f03-b474-2075d7450efc';

% ext.author = '';

% ext.email = '';

% ext.url = '';

