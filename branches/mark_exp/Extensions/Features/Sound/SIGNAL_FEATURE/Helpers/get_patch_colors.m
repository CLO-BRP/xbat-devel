function color = get_patch_colors(ax, base)

% TODO: this can be parametrized

ax_color = get(ax, 'color'); highlight = base + 0.5;

n = 16; color.face = ((n - 2) * ax_color + highlight) ./ n;

n = 4; color.edge = ((n - 2) * ax_color + highlight) ./ n;

% color.edge = base;