function [handles, context] = view__on__selection__edit__start(widget, data, parameter, context)

% SIGNAL_FEATURE - view__on__selection__edit__start

%--
% clear previous selection display
%--

tag = 'selection_rubber_band';

delete(findobj(widget, 'tag', tag));

%--
% get selection
%--

sel = data.selection;

%--
% get axes
%--

% TODO: this could be a get channel axes function

ax = get_channel_axes(widget, sel.event.channel);

if isempty(ax)
	return;
end 

%--
% display selection
%--

opt = time_event_display; 

opt.color = sel.color; opt.control = 0; opt.linestyle = ':'; % opt.pad = 2 * opt.pad;

handles = time_event_display(ax, sel.event, opt);

uistack(handles(2), 'bottom'); % NOTE: this relies on handle packing within 'time_event_display'

set(handles, 'tag', tag);
