function parameter = parameter__create(context)

% SIGNAL_FEATURE - parameter__create

parameter = struct;

parameter.reduce.method = 'linear';

parameter.reduce.target = [];