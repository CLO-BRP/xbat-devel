function parameter = parameter__create(context)

% SPECTRAL_REDUCTION - parameter__create

% parameter = struct;

% NOTE: this gets the default parameters for the parent extension

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.use_common_spectrogram = 0;

parameter.map = @map;



function reduction = map(feature, parameter, context)

% db_disp reduction; flatten(feature)

if ~iscell(feature.spectrogram.value)
	
	reduction.value = sqrt(sum(feature.spectrogram.value.^2, 1));
	
else
	
	for k = 1:numel(feature.spectrogram.value)
		reduction.value{k} = sqrt(sum(feature.spectrogram.value{k}.^2, 1));
	end
	
end

reduction.time = feature.time';