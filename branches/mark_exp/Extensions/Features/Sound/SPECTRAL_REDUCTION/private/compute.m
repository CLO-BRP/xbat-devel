function [feature, context] = compute(page, parameter, context)

% SPECTRAL_REDUCTION - compute

% feature = struct;

%--
% compute parent feature
%--

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);

%--
% apply reduction
%--

feature.reduction = parameter.map(feature, parameter, context);