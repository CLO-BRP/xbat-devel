function parameter = view__parameter__create(context)

% ZCR - view__parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.color = 'magenta';
