function [handles, context] = view__on__compute(widget, data, parameter, context)

% ZC-RATE - view__on__compute

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

set(handles, 'clipping', 'on');