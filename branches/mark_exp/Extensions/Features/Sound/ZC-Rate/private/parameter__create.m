function parameter = parameter__create(context)

% ZCR - parameter__create

%--
% inherit parent parameters and set map
%--

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @zero_crossing;

%--
% define and set revealed map parameters
%--

nyq = 0.5 * context.sound.rate;

% NOTE: these are the min and max frequencies of the band we are interested in observing

parameter.min_freq = 0.01 * nyq;

parameter.max_freq = 0.05 * nyq;

parameter.max_freq_active = 1;

parameter.noise_gate = 0.9 * nyq;

%--
% set currently hidden parameters
%--

% NOTE: there should be some reasonable way of setting this value as well

parameter.min_bandwidth = parameter.min_freq;

parameter.order = 4;

% NOTE: this is still a problem, and does not belong here!

parameter.nyq = nyq;


%-----------------------------
% ZERO_CROSSING
%-----------------------------

function [X, info] = zero_crossing(X, parameter) %#ok<INUSD>

info.range = [0, 0.5];

%--
% filter signal
%--

% NOTE: in this case we focus on a band, stop another containing clutter, and let high-frequency noise through to increase rate contrast

if parameter.max_freq_active
	
	% NOTE: we apply a high-pass and band-stop filter to achieve the desired response
	
	[b1, a1] = butter(parameter.order, parameter.min_freq / parameter.nyq, 'high');

	[b2, a2] = butter(parameter.order, [parameter.max_freq, parameter.noise_gate] ./ parameter.nyq, 'stop');

	X = filter(b2, a2, filter(b1, a1, X));

% NOTE: in this case we simply create a base frequency for observation by limiting low-frequency information

else
	
	[b1, a1] = butter(parameter.order, parameter.min_freq / parameter.nyq, 'high');
	
	X = filter(b1, a1, X);
	
end

%--
% compute zero crossings
%--

X(end + 1, :) = X(end, :);

X = double(X(1:end - 1, :) .* X(2:end, :) <= 0);

%--
% compute rate of zero crossings
%--

% TODO: the frame size for zero-crossing rate estimation is currently not parametrized

% NOTE: this is not the same as the amplitude feature frame size

n = ceil(0.06 * 2 * parameter.nyq); n = n + ~mod(n, 2); F = ones(n, 1); 

X = box_filter(X, F) ./ n; X(isnan(X)) = 0;


