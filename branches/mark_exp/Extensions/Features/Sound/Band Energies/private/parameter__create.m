function parameter = parameter__create(context)

% band energy - parameter__create

% parameter = struct;

%--
% parent spectrogram parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.use_common_spectrogram = 1;

%--
% band parameters
%--

parameter.bank_type = 'hann';

parameter.bands = [0.2, 0.4, 0.6, 1];

parameter.normalize = 1;

% NOTE: we can focus all the bands to this range

parameter.start_freq = 0; parameter.stop_freq = 0.75;

% NOTE: the interpeter function helps derives band descriptions from parameters

parameter.interpretfun = @bands_interpreter;
