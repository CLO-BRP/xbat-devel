function parameter = parameter__create(context)

% RENYI ENTROPY - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @map;

parameter.exponent = 2;


%------------------
% MAP
%------------------

function reduction = map(feature, parameter, context)

if ~iscell(feature.spectrogram.value)
	
	reduction.value = entropy(feature.spectrogram.value, parameter.exponent);
	
else
	
	for k = 1:numel(feature.spectrogram.value)
		reduction.value{k} = entropy(feature.spectrogram.value{k}, parameter.exponent);
	end
	
end

reduction.time = feature.time';


%------------------
% ENTROPY
%------------------

function H = entropy(P, alpha)

% NOTE: this sets the column sums to 1 for the full matrix

% NOTE: some tests indicate the second line is 8 times faster than the first

% P = P * diag(1 ./ sum(P, 1));

% P = P ./ repmat(sum(P, 1), size(P, 1), 1);

% NOTE: the 1 case is the Shannon entropy, computed differently

if alpha == 1
	
	P = P ./ repmat(sum(P, 1), size(P, 1), 1); H = sum(-P .* log2(P), 1);

else
	
	% TODO: normalization should be used here as well, but there is something interesting about the unnormalized computation
	
	H = (1 / (1 - alpha)) * log2(sum(P.^alpha, 1));

end


