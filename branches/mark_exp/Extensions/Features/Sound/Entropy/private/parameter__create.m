function parameter = parameter__create(context)

% ENTROPY - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.exponent = 1;