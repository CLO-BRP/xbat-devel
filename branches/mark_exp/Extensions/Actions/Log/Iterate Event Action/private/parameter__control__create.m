function control = parameter__control__create(parameter, context)

% EVENT ACTION - parameter__control__create

control = empty(control_create);

%--
% create action selection control
%--

if iscell(parameter.action)
	parameter.action = parameter.action{1};
end 

% NOTE: with some work we should be able to extend this to multiple actions using 'listbox'

actions = get_extension_names('event_action');

if isempty(actions) 
	actions = '(No Event Actions Available)'; value = 1;
else
	value = find(strcmp(actions, parameter.action));
end

control(end + 1) = control_create( ...
	'name', 'action', ...
	'alias', 'event action', ...
	'style', 'popup', ...
	'string', actions, ...
	'space', 1.5, ...
	'value', value ...
);

%--
% append corresponding event action controls
%--

% TODO: we should really use 'get_action_controls', and also get the options

% NOTE: it seems like the hosted extension options should control the display

append = get_event_action_controls(parameter.action, context);

if isempty(append)
	return; 
end

control(end).space = 1.75;

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', [parameter.action, ' Parameters'] ...
);

control(end) = adjust_control_space(control(end), append(1));

% TODO: update event action parameter values

control = [control, append];
