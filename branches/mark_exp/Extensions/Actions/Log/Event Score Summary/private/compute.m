function [result, context] = compute(log, parameter, context)

% EVENT SCORE SUMMARY - compute

result = struct;

%--
% get log events and extract relevant event data for summary
%--

% NOTE: the result will also be available in the base MATLAB workspace

event = log_get_events(log);

time = struct_field(event, 'time'); start = time(:, 1); duration = time(:, 2) - time(:, 1); score = [event.score];

data.start = start; data.duration = duration; data.score = score; result.data = data;

%--
% compute and display summaries
%--

% NOTE: since 'hist' accepts axes input we should use 'harray' or something that resizes nicely

figure; 

ax = subplot(2, 1, 1); hist(start, 200); title(['Start Time, N = ', int2str(numel(event))]);

set(ax, 'xlim', fast_min_max(start));

ax = subplot(2, 2, 3); hist(duration, 20); title('Event Duration');

set(ax, 'xlim', fast_min_max(duration));

ax = subplot(2, 2, 4); hist(score, 20); title('Event Score');

set(ax, 'xlim', [0, 1]);