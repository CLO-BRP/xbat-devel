function [result, context] = compute(sound, parameter, context)

% CLIP LOG - compute

result = struct;

% TODO: consider early return conditions, single file or channel

%--
% get start times and durations for events
%--

[start, duration] = get_file_times(sound); stop = start + duration; 

start = start + 0.05 * duration; stop = stop - 0.05 * duration;

%--
% create clip events using file start times and durations
%--

nyq = 0.5 * get_sound_rate(sound);

event = empty(event_create); 

for k = 1:numel(start)
	event(end + 1) = event_create('time', [start(k), stop(k)], 'freq', [0, nyq]);
end 

%--
% add file tag to events if needed
%--

% TODO: refactor to allow the creation of a derived 'NFC Clip Log' sound action, or perhaps, the NFC action is an event action

% TODO: make the tags parser parameters, store with helpers, give name

if parameter.add_file_tags
	
	for k = 1:numel(event)
		
% 		event(k) = set_tags(event(k), ['file:', strrep(sound.file{k}, ' ', '_')]);
		
		event(k) = set_tags(event(k), parse_nfc_tags(sound.file{k}));
	end
	
end
	
%--
% create new log and store events
%--

result.log = new_log(sound, parameter.name);

log_append(result.log, event);