function [result, context] = compute(sound, parameter, context)

% DETECT - compute

result = struct;

%----------------------
% SETUP
%----------------------

%--
% produce local context so that we may get a compiled extension
%--

% NOTE: the key element of the context here is the sound

local = get_extension_context; 

sound.rate = get_sound_rate(sound);

local.sound = sound; 

local.user = context.user; 

local.library = context.library;

%--
% get extension and preset for detection task
%--

% NOTE: we first get a basic extension so that we can load the preset

basic = get_extensions('sound_detector', 'name', parameter.detector{1}); 

preset = preset_load(basic, parameter.preset{1});

% NOTE: now we can compile the extension using the current context

ext = get_extension_using_context('', preset.ext, local);

% NOTE: make sure that the detector view is turned off

ext.control.explain = 0;

%--
% check if we need to process
%--

% TODO: look at detector scan to see how this is done

ext.control.on_detect_process = 0;

%----------------------
% DETECT
%----------------------

%--
% create new output log
%--

output = parameter.output;

output = token_replace(output, '%', ...
	'SOUND_NAME', sound_name(sound), 'PRESET_NAME', parameter.preset{1}, 'DETECTOR_NAME', ext.name, 'TIME',	datestr(now, 'yyyymmdd_HHMMSS') ...
);

log = new_log(sound, output, 'SQLite', struct);

%--
% run detector scan and output to new log 
%--

% NOTE: 'detector_scan' saves the log so we don't need to

log = detector_scan(ext, sound, [], [], log);

% TODO: report something about the log

%--
% delete detector waitbar
%--

name = [preset.ext.name, ' - ', sound_name(sound)];

pal = find_waitbar(name);

delete(pal);


%--------------------
% TOKEN_REPLACE
%--------------------

function str = token_replace(str, sep, varargin)

[field, value] = get_field_value(varargin);

for k = 1:length(field)

	str = strrep(str, [sep, field{k}, sep], value{k});
	
end