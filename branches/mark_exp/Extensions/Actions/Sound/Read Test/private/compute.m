function [result, context] = compute(sound, parameter, context)

% READ TEST - compute

%--
% setup
%--

result = struct;

scan = get_sound_scan(sound, 5);

[page, scan] = get_scan_page(scan);

%--
% read through sound, considering possible failure
%--

rate = [];

while ~isempty(page) && page.start < 3600
	try
		tic;
		
		page = read_sound_page(sound, page);
		
		rate(end + 1) = numel(page.samples) / (10^6 * toc);
		
		disp(['reading at', sec_to_clock(page.start), ' (', num2str(rate(end)), ')']);
	catch
		disp('Failed to read sound page.');
		
		nice_catch;
		
		return;
	end

	[page, scan] = get_scan_page(scan);
end

%--
% estimate performance
%--

result.rate.values = rate; result.rate.mean = mean(rate); result.rate.std = std(rate);

disp(flatten(result));

figure; hist(rate); xlabel(['mean = ', num2str(result.rate.mean), ', std = ', num2str(result.rate.std)]);

