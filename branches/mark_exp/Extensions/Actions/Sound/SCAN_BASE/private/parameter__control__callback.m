function result = parameter__control__callback(callback, context)

% SCAN_BASE - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'duration'
		
		value = get(callback.obj, 'string');
		
		% NOTE: when the input is not a clock string, we try to evaluate it as seconds
		
		if isempty(clock_to_sec(value))
			
			value = sec_to_clock(eval(value));
			
			if ~isempty(value)
				set(callback.obj, 'string', value);
			end
		
		end
		
end