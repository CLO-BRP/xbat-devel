function [status, result] = ted(varargin)

% ted - text edit file in an appropiate editor
% -------------------------------------------
%
% ted file
%
% [status, result] = ted(file)
%
% Input:
% ------
%  file - file
%
% Output:
% -------
%  status - status
%  result - result

%-----------------
% HANDLE INPUT
%-----------------

%--
% handle multiple files
%--

% TODO: consider if this is a pattern we can factor

if numel(varargin) > 1
	[status, result] = iterate(@ted, varargin); return;
end

if iscell(varargin{1})
	[status, result] = iterate(@ted, varargin{1}); return;
end

if size(varargin{1}, 1) > 1
	file = cellstr(varargin{1}); [status, result] = ted(file{:}); return;
end

% NOTE: at this point we have a single input that is a one row string

file = varargin{1};

%--
% check input is simple file name, not full name
%--

name = isempty(strfind(file, filesep));

if name
	file0 = file;
end

%--
% check for file
%--

% TODO: try to find the file in the current directory first

% file = which(file);

% NOTE: create file if needed, consider asking for confirmation

if isempty(file) && name
	file = [pwd, filesep, file0]; fclose(fopen(file, 'w'));
end

if isempty(file) || ~exist(file, 'file')
	error('Unable to find file for editing.');
end

%-----------------
% HANDLE INPUT
%-----------------

%--
% select editor using file extension
%--

[ignore, ignore, ext] = fileparts(file); ext = lower(ext);

switch ext
	
	%--
	% matlab editor
	%--
	
	% NOTE: scite may be better for these, but it is not built-in
	
	case {'.c', '.m', '.java'}, edit(file); status = 1; result = '';
		
	%--
	% hxd
	%--
	
	% NOTE: throw anything we know to be binary to 'hxd'
	
	case '.exe', [status, result] = hxd(file);
	
	case strcat('.', all_mexext), [status, result] = hxd(file);
		
	case strcat('.', get_formats_ext), [status, result] = hxd(file);
		
	%--
	% scite
	%--
	
	% NOTE: throw anything we can't recognize at trusty 'scite'
	
	otherwise, [status, result] = scite(file);
		
end

