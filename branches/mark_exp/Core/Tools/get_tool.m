function tool = get_tool(name, scan)

% get_tool - get tool struct from name
% ------------------------------------
%
% tool = get_tool(name, scan)
%
% Input:
% ------
%  name - tool name
%  scan - scan for help indicator or hint
%
% Output:
% -------
%  tool - tool struct
%
% NOTE: This function typically requires the tool be in the MATLAB path.

% TODO: add license field and use help scan strategy to get license info

%-----------------
% HANDLE INPUT
%-----------------

%--
% set default help scan
%--

% NOTE: the scan input may be a hint for where to get help

hint = '';

if nargin < 2
	scan = 1;
end

if ischar(scan)
	hint = scan; scan = 1; 
end 

% NOTE: clear hint when it is not an existing directory

if ~isempty(hint) && ~exist_dir(hint)
	hint = '';
end

%-----------------
% GET TOOL
%-----------------

%--
% create tool
%--

tool = create_tool;

%--
% get tool location and type
%--

% NOTE: the file redundantly contains root for convenience

if ispc

	% TOOLS IN MATLAB PATH 
	
	tool.file = which(name);

	% NOTE: this is insidious coupling, however it is not tight...

	% MIKTEX TOOLS 
	
	if isempty(tool.file) && ~isempty(miktex_root)

		file = fullfile(miktex_root, name);

		if exist(file, 'file')
			tool.file = file; hint = miktex_root('doc'); scan = 1;
		end

	end

	% MINGW TOOLS
	
	if isempty(tool.file) && ~isempty(mingw_root)

		file = fullfile(mingw_root, 'bin', name);

		if exist(file, 'file')
			tool.file = file; scan = 0;
		end

	end
	
	% RUBY TOOLS
	
	if isempty(tool.file) && ~isempty(ruby_root)

		file = fullfile(ruby_root, name);

		if exist(file, 'file')
			tool.file = file; scan = 0;
		end

	end
	
	% TODO: consider how to get tools installed in 'Program Files'

else

	[status, tool.file] = system(['which "', name, '"']);
	
	if ~isempty(tool.file)
		tool.file(end) = []; 
	end

end

% NOTE: this allows for full filenames not in MATLAB path to work

if isempty(tool.file) && exist(name, 'file')
	tool.file = name;
end

if isempty(tool.file)
	tool = []; return;
end

% NOTE: for unix scripts we could inspect the first line of the file to get type

[tool.root, tool.name, tool.type] = fileparts(tool.file);

%--
% scan for various fields
%--

if ~scan
	return;
end

tool.help = get_tool_help(tool, hint);

% NOTE: we may have to set the 'version' and 'url' fields manually

% tool.version = get_tool_version(tool);

% tool.url = get_tool_url(tool);


%--------------------------------
% CREATE_TOOL
%--------------------------------

function tool = create_tool

tool.name = '';

tool.version = '';

tool.root = '';

tool.file = '';

tool.type = '';

tool.help = {};

tool.url = '';


%--------------------------------
% GET_TOOL_HELP
%--------------------------------

function help = get_tool_help(tool, hint)

% NOTE: this is not very useful in the non-PC case, we typically have 'man'

if ~ispc
	help = {}; return;
end

% NOTE: this function tries to infer which files in tool root are help files

%--
% setup
%--

% NOTE: we declare name cues and file types typical as help

cues = { ...
	'changes', ...
	'changelog', ...
	'doc', ...
	'docs', ...
	'faq', ...
	'history', ...
	'help', ...
	'how to', ...
	'how-to', ...
	'howto', ...
	'install', ...
	'read me', ...
	'read-me', ...
	'readme', ...
	lower(tool.name) ...
};

types = { ...
	'', ...
	'.chm', ...
	'.dvi', ...
	'.htm', ...
	'.html', ...
	'.pdf', ...
	'.tex', ...
	'.txt' ...
}; 

%--
% handle input
%--

start = tool.root;

% NOTE: if a non-empty hint was provided use it

if (nargin > 1) && ~isempty(hint)
	start = hint;
end

% TODO: consider searching in both places if available

%--
% scan near root for help files
%--

[above, current] = fileparts(start);

% NOTE: move help search root up when starting directory is 'bin'

if strcmpi(current, 'bin')
	root = above;
else
	root = start;
end

%--
% check for help files among candidate directories
%--

dirs = scan_dir(root);

help = {}; score = [];

for i = 1:length(dirs)
	
	content = no_dot_dir(dirs{i});
	
	for k = 1:length(content)
		
		if content(k).isdir
			continue;
		end
		
		file = [dirs{i}, filesep, content(k).name]; cue = 0; [ignore, ignore, type] = fileparts(file);
		
		for j = 1:length(cues) - 1
			cue = cue + length(findstr(file, cues{j}));
		end
		
		cue = cue + 2 * length(findstr(file, cues{end}));
		
		% TODO: make these full names for convenience and flexibility

		if cue && ismember(type, types)
			help{end + 1} = file; score(end + 1) = cue;
		end
		
	end
	
end

%--
% sort list by score, pruning if needed
%--

help = help(:); score = score(:); 

if length(help) > 10
	cutoff = 3; help(score < cutoff) = []; score(score < cutoff) = [];
end

% NOTE: this seems a bit more verbose than needed

[score, ix] = sort(score); help = help(ix); 

help = flipud(help); score = flipud(score);

% for k = 1:length(help)
% 	disp([int2str(score(k)), ' - ' help{k}]);
% end


% TODO: these functions will depend on the type of tool

%--------------------------------
% GET_TOOL_VERSION
%--------------------------------

function version = get_tool_version(tool)

version = '';

%--------------------------------
% GET_TOOL_URL
%--------------------------------

function url = get_tool_url(tool)

url = '';


%--------------------------------
% IS_HELP_DIR
%--------------------------------

function value = is_help_dir(content, tool)

value = 0;

if ~content.isdir
	return;
end

prefix = {'doc', 'help', 'html', 'pdf', tool.name}; name = lower(content.name);

[ignore, name] = fileparts(name);

for k = 1:length(prefix)

	if ~isempty(strmatch(name, prefix{k}))
		value = 1; return;
	end

end

