function tool = get_unxutils

% get_unxutils - some unix command-line tools for windows 
% -------------------------------------------------------
%
% tool = get_unxutils(name)
%
% Input:
% ------
%  name - of tool (ex. 'ln') (def: '', return all tools)
%
% Output:
% -------
%  tool - from unxutils group

%--
% make sure we have tools
%--

tool = get_tool('ln.exe');

url = 'http://sourceforge.net/projects/unxutils/files/unxutils/current/UnxUtils.zip/download';

if isempty(tool) && install_tool('UnxUtils', url, 'urlwrite')
	tool = get_tool('ln.exe');
end

%--
% get all tools, or tool by name
%--
