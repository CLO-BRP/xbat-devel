function [status, result] = curl_get(url, file, message, opt)

% curl_get - get file using curl
% ------------------------------
%
% [status, result] = curl_get(url, file, message, opt)
%
% Input:
% ------
%  url - source
%  file - destination
%  message - wait message
%  opt - struct

% NOTE: this function returns immediately, the download happens as a separate process

%-----------------
% SETUP
%-----------------

%--
% set and possibly return options
%--

if nargin < 4
	opt.wait = 1; 
end 

if ~nargin
	status = opt; return;
end 

%--
% get curl tool
%--

tool = get_curl;

if isempty(tool)
	error('curl is not available.');
end

%-----------------
% HANDLE INPUT
%-----------------

%--
% set default file
%--

if (nargin < 2) || isempty(file)
	[p1, p2, p3] = fileparts(url); file = [p2, p3];
end

%--
% set default message
%--

if (nargin < 3) || isempty(message)
	message = ['Downloading ', file];
end

%-----------------
% GET
%-----------------

%--
% use curl to get size and fetch file
%--

% NOTE: this line avoids an overwrite warning

name = file; clear file;

file.name = name; 

if opt.wait
	
	% TODO: the message function may be a callback function

	file.message = message;

	file.total = curl_get_length(url, tool);

end

str = ['"', tool.file, '" -X GET -o "', file.name, '" ', url];

[status, result] = system(str)

%--
% create waitbar
%--

% TODO: it may be that we have to create the waitbar before calling curl

% TODO: this should take a type of 'curl' call input, say 'download' and 'upload'

if opt.wait
	
	curl_waitbar(file);

end