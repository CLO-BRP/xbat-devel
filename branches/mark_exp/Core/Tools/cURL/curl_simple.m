function [status, result] = curl_simple(varargin)

% curl_simple - direct calls to curl
% ----------------------------------
%
% [status, result] = curl_simple(varargin)
%
% Input: 
% ------
%  varargin - arguments to 'curl' command-line tool
%
% Output:
% -------
%  status, result - of system call

tool = get_curl; 

if isempty(tool)
	error('cURL is not available.');
end

str = ['"', tool.file, '" ', str_implode(varargin, ' ')];

switch nargout
	
	case 0
		system(str)
	
	case 1
		status = system(str); 
		
	otherwise
		[status, result] = system(str);
		
end