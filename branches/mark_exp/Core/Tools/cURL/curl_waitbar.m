function pal = curl_waitbar(file, pal)

%--
% create waitbar if needed
%--

if nargin < 2

	control = control_create( ...
		'name', 'PROGRESS', ...
		'style', 'waitbar', ...
		'confirm', 1, ...
		'update_rate', [] ...
	);
	
	control(end + 1) = control_create( ...
		'name', 'close_on_completion', ...
		'style', 'checkbox', ...
		'value', 1 ...
	);
	
	pal = waitbar_group('CURL', control); curl_daemon(file, pal);
	
	waitbar_update(pal, 'PROGRESS', 'message', file.message);
	
end

%--
% update waitbar
%--

% TODO: this is the update for the 'download' modality of the function

info = dir(file.name);

if isempty(info)
    info(1).bytes = 0;
end

value = info.bytes / file.total;

waitbar_update(pal, 'PROGRESS', ...
	'value', value ...
);

% [bytes, unit] = get_unit_bytes(info.bytes); bytes = integer_unit_string(bytes, upper(unit), 0);

bytes = int2str(info.bytes);

waitbar_update(pal, 'PROGRESS', ...
	'message', [file.message, '  (', bytes, ')'] ...
);

%--
% consider if we need to close the waitbar
%--

close_on_completion = get_control_value(pal, 'close_on_completion');

if (value == 1) && ~isempty(close_on_completion) && close_on_completion
	close(pal);
end
