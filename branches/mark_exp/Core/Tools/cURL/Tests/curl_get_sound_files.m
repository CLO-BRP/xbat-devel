function [files, result] = curl_get_sound_files(source, destination)

% curl_get_sound_files - get sound files from url using curl
% ----------------------------------------------------------
%
% [files, result] = curl_get_sound_files(source, destination)
%
% Input:
% ------
%  source - source url
%  destination - where to download files to
%
% Output:
% -------
%  files - files found at source
%  result - result

% TODO: factor as web-service

%-----------------
% HANDLE INPUT
%-----------------

%--
% set no destination default, no download only get info
%--

% TODO: perhaps there should be a default destination a cache

if nargin < 2
	destination = '';
end

%--
% set source URL
%--

% TODO: find other locations like this, set some up

if ~nargin || isempty(source)
	source = 'http://www.fonozoo.com/archivos/';
end

%-----------------
% GET FILES
%-----------------

%--
% get and scrape page for files
%--

page = urlread(source);

% TODO: factor scraping, consider scraping with multiple expressions and joining

str = scrape_regexp;

tokens = regexpi(page, str, 'tokens');

files = cell(size(tokens));

for k = 1:length(tokens)
	files{k} = tokens{k}{1};
end

%--
% build URL for files, this is the result and download files to destination
%--

for k = 1:length(files)
	result{k} = fullfile(source, files{k});
end

files = files(:); result = result(:);

% TODO: with no destination we currently return, get file info with curl

if isempty(destination)
	return;
end


%---------------------
% SCRAPE_REGEXP
%---------------------

% TODO: determine whether this scraper regular expression is useful and improve it

function str = scrape_regexp

ext = get_formats_ext;

str = ['>(', str_implode(strcat('[\S]*.', get_formats_ext), '|'), ')'];


	