function [status, result, str] = scite(file)

% scite - use scite to edit file
% ------------------------------
%
% [status, result, str] = scite(file)
%
% Input:
% ------
%  file - file to edit
%
% Output:
% -------
%  status - status
%  result - result
%  str - command string

%--------------------
% SETUP
%--------------------

%--
% get tool
%--

tool = get_tool('Scite.exe');

% url = 'http://downloads.sourceforge.net/scintilla/wscite174.zip?big_mirror=0';

url = 'http://sourceforge.net/projects/scintilla/files/SciTE/2.01/wscite201.zip/download';

if isempty(tool) && install_tool('SciTE', url, 'urlwrite')
	tool = get_tool('Scite.exe');
end

%--------------------
% HANDLE INPUT
%--------------------

% TODO: this may be factored, it needs a name

if nargin

	%--
	% it is an error to use an unavailable tool
	%--
	
	if isempty(tool)
		error('Tool is not available.');
	end

else

	%--
	% output info or open scite
	%--

	if nargout
		status = tool;
	else
		if ~isempty(tool)
			system(['"', tool.file, '" &']);
		end
	end
	
	return;

end

%--
% find file
%--

% NOTE: this may not be the right thing to do

if isempty(strfind(file, filesep))
	file = which(file);
end

%--------------------
% OPEN
%--------------------

%--
% open file for editing
%--

% NOTE: we quote tool and file to avoid space problems

str = ['"', tool.file, '" "', file, '" &'];

[status, result] = system(str);

% NOTE: don't output when called as command

if ~nargout
	clear status;
end
