function [status, result] = sphinx_service(task, varargin)

% sphinx_service - related tasks
% ------------------------------
% 
% [status, result] = sphinx_service(task, varargin)
%
% Input:
% ------
%  task - name
%
% Output:
% -------
%  status - of task
%  result - of task

%--
% get tool for the task and persist
%--

persistent searchd;

if isempty(searchd)
	searchd = get_tool('searchd.exe');
end

%--
% build task command and perform
%--

switch task
	
	case 'install'
		command = [quote(searchd.file), ' --install --config ', quote(fullfile(searchd.root, 'sphinx.conf')), ' --servicename SphinxSearch'];
	
	case {'delete', 'uninstall'}
		command = [quote(searchd.file), ' --delete --servicename SphinxSearch'];
		
	case {'start', 'stop'}
		command = ['net ', task, ' SphinxSearch'];
		
	otherwise
		
end

[status, result] = system(command);

%--
% display result if no capture
%--

if ~nargout
	disp(' '); disp(result); clear status; clear result;
end


%-----------------
% QUOTE
%-----------------

function str = quote(str)

str = ['"', str, '"'];
