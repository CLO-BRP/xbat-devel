function root = miktex_root(type)

% miktex_root - get miktex root
% -----------------------------
%
% root = miktex_root(type)
%
% Input:
% ------
%  type - root type 'bin' or 'doc'
%
% Output:
% -------
%  root - root, empty if directory does not exist
% 
% NOTE: to see and change directories edit this file

%--
% set default root type
%--

if ~nargin
	type = 'bin';
end

%--
% set default root
%--

% NOTE: this could to be more intelligent, not depend so strongly on a detailed version number

base = strcat({'C:\Program Files\MikTex '}, {'2.8', '2.7', '2.6', '2.5'});

switch type
	
	case 'bin'
		candidate = strcat(base, '\miktex\bin');
		
	case 'doc'
		candidate = strcat(base, '\doc');
		
end

%--
% check for existence of root
%--

root = '';

for k = 1:length(candidate)
	
	if exist_dir(candidate{k}), root = candidate{k}; return; end
	
end