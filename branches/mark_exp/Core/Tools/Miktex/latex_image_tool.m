function handle = latex_image_tool

% NOTE: various requirements here will impact the engine function 'latex_image'

% TODO: add resolution control

% TODO: consider adding a transparent background option

% TODO: pass parameters to preview function and set figure background to equation background

%--
% check for existing tool, center and focus if it exists
%--

tag = 'LATEX_IMAGE_TOOL';

handle = findobj(0, 'tag', tag, 'type', 'figure');

if ~isempty(handle)
	position_palette(handle, 0, 'center'); return;
end 

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'string', 'Equation' ...
); 

root = create_dir(fullfile(get_desktop, 'EQNS'));

if isempty(root)
	root = get_desktop;
end

known = strcat(get_known_equations(root), '.png');

control(end + 1) = control_create( ...
	'name', 'known', ...
	'color', get(0, 'defaultuicontrolbackgroundcolor'), ...
	'label', 0, ...
	'space', -0.5, ...
	'string', known, ...
	'width', 2/3, ...
	'align', 'right', ...
	'style', 'popup' ...
);

control(end + 1) = control_create( ...
	'name', 'file', ...
	'string', 'my-equation.png', ...
	'space', 0.5, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'source', ...
	'lines', 6, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'render', ...
	'alias', 'RENDER', ...
	'style', 'buttongroup', ...
	'align', 'right', ...
	'space', 1.25, ...
	'lines', 1.75 ...
);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.11, ...
	'string', 'Options' ...
);

tabs = {'Render', 'Output'}

control(end + 1) = control_create( ...
	'style', 'tabs', ...
	'tab', tabs ...
);

control(end + 1) = control_create( ...
	'name', 'dpi', ...
	'tab', tabs{1}, ...
	'alias', 'DPI', ...
	'string', '300', ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'get_foreground', ...
	'alias', 'Choose ...', ...
	'tab', tabs{1}, ...
	'style', 'buttongroup', ...
	'width', 0.3, ...
	'align', 'right', ... 
	'lines', 1.75, ...
	'space', -2 ...
);

control(end + 1) = control_create( ...
	'name', 'foreground', ...
	'width', 0.65, ...
	'space', 1.1, ...
	'tab', tabs{1}, ...
	'string', '0 0 0', ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'get_background', ...
	'alias', 'Choose ...', ...
	'tab', tabs{1}, ...
	'style', 'buttongroup', ...
	'width', 0.3, ...
	'align', 'right', ...
	'lines', 1.75, ...
	'space', -2.1 ...
);

control(end + 1) = control_create( ...
	'name', 'background', ...
	'tab', tabs{1}, ...
	'string', '1 1 1', ...
	'width', 0.65, ...
	'space', 0.75, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'directory', ...
	'tab', tabs{2}, ...
	'string', root, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'rerender', ...
	'alias', 'RENDER ALL', ...
	'tab', tabs{2}, ...
	'style', 'buttongroup', ...
	'align', 'right', ...
	'space', 1.25, ...
	'lines', 1.75 ...
);

% control(end + 1) = control_create( ...
% 	'name', 'snippet', ...
% 	'alias', 'snippet file', ...
% 	'tab', tabs{2}, ...
% 	'value', 1, ...
% 	'style', 'checkbox' ...
% );

% control(end + 1) = control_create( ...
% 	'name', 'transparent', ...
% 	'style', 'checkbox', ...
% 	'value', 0, ...
% 	'onload', 1 ...
% );

%--
% create tool figure
%--

opt = control_group; opt.width = 16; opt.bottom = 0;

handle = control_group(0, @latex_image_callback, 'Latex Image Tool', control, opt);

text_menu(handle);

set(handle, 'closerequestfcn', @latex_image_tool_close);

%--
% position tool
%--

position_palette(handle, 0, 'center');

% set_toggle_by_label(handle, 'Options', 'close');


%---------------------------------
% GET_KNOWN_EQUATIONS
%---------------------------------

function known = get_known_equations(root) 

content = no_dot_dir(root, -1); known = {};

for k = 1:numel(content) 
	
	name = content(k).name;
	
	if numel(name) > 4 && strcmp(name(end - 3:end), '.tex')
		known{end + 1} = name(1:end - 4);
	end
	
end


%---------------------------------
% LATEX_IMAGE_TOOL_CLOSE
%---------------------------------

function latex_image_tool_close(obj, eventdata)

child = findobj(0, 'tag', 'LATEX_IMAGE_PREVIEW', 'type', 'figure');

if ~isempty(child)
	close(child);
end

closereq;


%---------------------------------
% LATEX_IMAGE_CALLBACK
%---------------------------------

function latex_image_callback(obj, eventdata) %#ok<INUSD>

%--
% compile and respond to specific callback
%--

callback = get_callback_context(obj, 'pack'); pal = callback.pal;

switch callback.control.name

	case 'rerender'
		
		root = get_control_value(pal.handle, 'directory');
		
		known = get_known_equations(root); handle = get_control_handles(pal.handle, 'known');
		
		for k = 1:numel(known)
			
			%--
			% set known, file, and source controls
			%--
			
			set(handle.obj, 'value', k);

			set_control(pal.handle, 'file', 'value', [known{k}, '.png']);
			
			lines = file_readlines(fullfile(root, file_ext(known{k}, 'tex')));

			if numel(lines) == 1
				lines = lines{1};
			end

			set_control(pal.handle, 'source', 'string', lines);

			%--
			% render image
			%--
			
			values = get_control_values(pal.handle);

			render_image(values, 1);

		end
		
	case 'known'
		
		%--
		% update controls
		%--
		
		known = get_control_value(pal.handle, 'known'); known = known{1};
		
		set_control(pal.handle, 'file', 'value', known);
		
		root = get_control_value(pal.handle, 'directory');
		
		lines = file_readlines(fullfile(root, file_ext(known, 'tex')));
		
		if numel(lines) == 1
			lines = lines{1};
		end
		
		set_control(pal.handle, 'source', 'string', lines);
		
		%--
		% render equation
		%--
		
		callback.control.name = 'render';
		
	case 'source'
		
		% NOTE: this is nice, but it creates the typical 'edit' focus problem
		
% 		set_control(pal.handle, 'render', 'enable', ~isempty(get(callback.obj, 'string'))); 

		return;
		
	case 'file'
		
		value = get_control(pal.handle, 'file', 'value');
		
		proper =  proper_filename(value);
		
		set_control(pal.handle, 'render', 'enable', proper);
		
		if ~proper
			return;
		end
		
		try %#ok<TRYNC>

			if ~strcmpi(value(end - 3:end), '.png')
				set_control(pal.handle, 'file', 'value', strcat(value, '.png'));
			end

		end
		
	case {'get_foreground', 'get_background'}
		
		type = callback.control.name(5:end);
		
		color = uisetcolor(title_caps(['select ', type]));
		
		if isequal(color, 0)
			return;
		end
		
		set_control(pal.handle, type, 'value', color_string(color));
		
	case 'transparent'
		
		state = ~get(callback.obj, 'value');
		
		% TODO: consider iteration within 'set_control'
		
		set_control(pal.handle, 'background', 'enable', state);
		
		set_control(pal.handle, 'get_background', 'enable', state);

end

%--
% return for all non-render commands
%--

if ~strcmp(callback.control.name, 'render')
	return;
end

%--
% render image
%--

values = get_control_values(pal.handle);

out = render_image(values, 1);

%--
% update known equations
%--

known = strcat(get_known_equations(fileparts(out)), '.png');

handle = get_control_handles(pal.handle, 'known');

ix = find(strcmp(values.file, known));

if isempty(ix)
	ix = 1;
end

set(handle.obj, 'string', known, 'value', ix);


%---------------------------------
% RENDER_IMAGE
%---------------------------------

function file = render_image(values, preview)

%--
% handle input
%--

if nargin < 2
	preview = 0;
end

% NOTE: in this case we have handle input

if ~isstruct(values)
	values = get_control_values(values);
end

%--
% compile image generation options and generate image
%--

opt = latex_image; 

opt.text = string_color(values.foreground); 

opt.body = string_color(values.background);

opt.dpi = values.dpi;

if isfield(values, 'transparent')
	opt.transparent = values.transparent;
end

% NOTE: using an output name seemed to be causing problems

out = latex_image(values.source, [], opt);

if preview
	preview_latex_image(out, opt, values.file);
end

%--
% move image to output location
%--

root = create_dir(values.directory);

if isempty(root)
	root = get_desktop;
end

file = fullfile(root, values.file);

copyfile(out, file);

if 1 % values.snippet
	
	if ischar(values.source)
		lines = {values.source};
	else
		lines = values.source;
	end
	
	file_writelines(fullfile(root, file_ext(values.file, 'tex')), lines);
	
end


%---------------------------------
% STRING_COLOR
%---------------------------------

function color = string_color(str)

% string_color - get color array from space-separated color string

if iscell(str)
	str = str{1};
end

color = eval(['[', str, ']']);


%---------------------------------
% COLOR_STRING
%---------------------------------

function str = color_string(color)

% color_string - get space-separated color string from color array

remove = '[],';

str = mat2str(color, 4);

for k = 1:numel(remove)
	str = strrep(str, remove(k), '');
end


%---------------------------------
% PREVIEW_LATEX_IMAGE
%---------------------------------

% TODO: this can be clearly factored into a simple image preview function

function par = preview_latex_image(file, opt, name)

if nargin < 3
	name = 'SET NAME';
end

pad = 16;

%--
% get image info
%--

try
	info = imfinfo(file);
catch
	nice_catch; return;
end

%--
% create figure and container axes
%--

% NOTE: check for existing figure and get current position

par = create_obj('figure', 0, 'LATEX_IMAGE_PREVIEW'); 

pos = get(par, 'position'); close(par); 

% NOTE: create new figure and update its position as needed

par = create_obj('figure', 0, 'LATEX_IMAGE_PREVIEW', ...
	'backingstore', 'on', ...
	'doublebuffer', 'on', ...
	'numbertitle', 'off', ...
	'name', ['Latex Image Preview:  ', name] ...
); 

pos(3:4) = [info.Width, info.Height] +  pad * [2, 3]; set(par, 'position', pos);

% NOTE: we do not need as much management of these objects if we are recreating the figure each time

ax = create_obj('axes', par, 'PREVIEW_AXES', ...
	'units', 'pixels', ... 
	'position', [pad, 2 * pad, info.Width, info.Height], ...
	'ydir', 'reverse', ...
	'xtick', [], ...
	'ytick', [], ...
	'xlim', [0, 1], ...
	'ylim', [0, 1] ...
);

im = create_obj('image', ax, 'PREVIEW_IMAGE', ...
	'xdata', [0, 1], ...
	'ydata', [0, 1] ...
);

%--
% get and display figure
%--

switch info.ColorType
	
	case 'indexed', [X, map] = imread(file); set(im, 'cdata', X); colormap(map);
		
end

%--
% set figure color to match background
%--

% TODO: we should also make the 'bounding-box visible

set(par, 'color', opt.body);

