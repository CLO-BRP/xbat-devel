function [out, status, result] = latex_image(in, out, opt)

% latex_image - create image from latex expression
% ------------------------------------------------
%
% [out, status, result] = latex_image(in, out, opt)
%
% Input:
% ------
%  str - latex expression
%  out - output file name
%  opt - image creation options
%
% Output:
% -------
%  out - image created, empty on failure
%  status - last helper status
%  result - last helper result

% TODO: factor cache file code as needed

% TODO: add any options, such as font package to hash

% TODO: allow for additional inclusion of macros file

%-------------------
% SETUP
%-------------------

persistent ELEMENTS;

if isempty(ELEMENTS)
	
	%--
	% get tools
	%--
	
	% NOTE: we make sure 'ELEMENTS' is empty after an error to ensure rescan on next attempt
	
	current = miktex_tools('texify.exe'); 
	
	if isempty(current)
		ELEMENTS = []; error('Unable to find required ''texify'' tool.'); %#ok<NASGU>
	end
	
	ELEMENTS.texify = current;

	current = miktex_tools('dvipng.exe'); 
	
	if isempty(current)
		ELEMENTS = []; error('Unable to find required ''dvipng'' tool.');  %#ok<NASGU>
	end
	
	ELEMENTS.dvipng = current;
	
	%--
	% load default fragments
	%--
	
	% TODO: we may want to process these considering the parameters, so these may not belong here
	
	% NOTE: we expect these to be as available as this function, so no worries
    
    ELEMENTS.pre = file_readlines(which('pre.tex'));
    
	ELEMENTS.macros = file_readlines(which('macros.tex'));
	
    ELEMENTS.post = file_readlines(which('post.tex'));

end

%-------------------
% HANDLE INPUT
%-------------------

%--
% set and possibly output default options
%--

if nargin < 3
    
    % NOTE: below we can include our own macros, set image options, and verbosity of execution
    
    opt.pre = []; opt.macros = []; opt.post = [];
    
	opt.text = ''; opt.body = ''; opt.transparent = 0; opt.dpi = 300; 
    
    opt.verb = 0;
	
	if ~nargin
		out = opt; return;
	end
	
end

if ~ischar(opt.dpi)
	opt.dpi = int2str(opt.dpi);
end

%--
% check input and pack string in cell if needed
%--

% NOTE: this function prefers string cell array input, 'lines' input

if ~ischar(in) && ~iscellstr(in)
	error('Improper input.');
end

if ischar(in)
	lines = {in};
else
	lines = in;
end

%-------------------
% GET IMAGE
%-------------------

%--
% get cache directory
%--

cache = create_dir([fileparts(mfilename('fullpath')), filesep, 'Cache']);

if isempty(cache)
	error('Failed to create cache directory.');
end

%--
% create color options string
%--

if ~isempty(opt.text)
	fg = ['-fg ', latex_color(opt.text), ' '];
else
	fg = '';
end

if opt.transparent
	bg = '-bg Transparent';
else	
	if ~isempty(opt.body)
		bg = ['-bg ', latex_color(opt.body), ' '];
	else
		bg = '';
	end
end

%--
% create cache name using expression and options hash
%--

% NOTE: consider string normalization

if ~ischar(lines)
	hash = md5([lines{:}, fg, bg, opt.dpi]);
else
	hash = md5([lines, fg, bg, opt.dpi]);
end

cached_name = [cache, filesep, hash];

%--
% check for cached image
%--

cached_out = [cached_name, '.png'];

if (nargin < 2) || isempty(out)
	out = cached_out;
end

% NOTE: if the file exists we copy cached file to output file if needed

if exist(cached_out, 'file')
	
	if ~strcmp(cached_out, out)
		copyfile(cached_out, out); 
	end
	
	return;
	
end

%--
% select environment checking for array
%--

box = 'displaymath';

for k = 1:numel(lines)

	if length(lines{k}) < 2
		continue;
	end
	
	if strcmp(lines{k}(end - 1:end), '\\')
		box = 'eqnarray*'; break;
	end

end
%--
% create latex input file
%--

cached_in = [cached_name, '.tex'];

in = get_fid(cached_in, 'wt');

if isempty(opt.pre)
    file_process(in.fid, ELEMENTS.pre);
else 
    file_process(in.fid, file_readlines(opt.macros))
end

if isempty(opt.macros)
    file_process(in.fid, ELEMENTS.macros);
else
    file_process(in.fid, file_readlines(opt.macros))
end

file_process(in.fid, ['\begin{', box, '}']);

file_process(in.fid, strcat({'  '}, lines));

file_process(in.fid, ['\end{', box, '}']);

if isempty(opt.post)
    file_process(in.fid, ELEMENTS.post);
else
    file_process(in.fid, file_readlines(opt.post))
end

fclose(in.fid);

if opt.verb
    iterate(@disp, file_readlines(cached_in));
end

%--
% process file to get image
%--

pi = pwd; cd(cache);

str = ['"', ELEMENTS.texify.file, '" --batch --clean "', cached_in, '"']; 

if opt.verb
    disp(str);
end 

[status, result] = system(str);

if opt.verb
    disp([to_str(status), result]);
end 

if status
	out = ''; return;
end

str = ['"', ELEMENTS.dvipng.file, '" -D ', opt.dpi, ' -T tight ', fg, bg, '"', cached_name, '.dvi"'];

if opt.verb
    disp(str);
end 

for k = 1:4
	[status, result] = system(str);
end

if opt.verb
    disp([to_str(status), result]);
end

if status
	out = ''; return;
end

% NOTE: this resolves renaming that 'dvipng' does so we can find cached image

file = dir([hash, '*.png']); copyfile(file.name, cached_out); delete(file.name);

cd(pi);

if ~strcmp(cached_out, out)
	copyfile(cached_out, out);
end

%--
% clean up
%--

try %#ok<TRYNC>
	delete(cached_in); delete([cached_name, '.dvi']);
end


%----------------------
% LATEX_COLOR
%----------------------

function str = latex_color(color)

%--
% get color value string
%--

if ~ischar(color)
	
	str = num2str(color, 1);
	
else
	
	% TODO: extend this to use some further named colors
	
	switch lower(color)
		
		case 'white', str = '1.0 1.0 1.0';
			
		case 'gray', str = '0.5 0.5 0.5';
			
		case 'black', str = '0.0 0.0 0.0';
			
		case 'transparent', str = 'Transparent';
			
		otherwise, error(['Unrecognized color name ''', color, '''.']);
			
	end 

end

%--
% prefix and wrap string
%--

str = ['"rgb ', str, '"'];
