function desc = set_canonical_names(desc,value)

% set_canonical_names - set canonical names for structure values
% --------------------------------------------------------------
%
% value = set_canonical_names(desc,value)
%
% Input:
% ------
%  desc - structure description structure
%  value - value structure
%
% Output:
% -------
%  desc - updated description

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% get fieldnames from value structure
%--

% NOTE: we assume that input is either value struct or names

if (isstruct(value))
	names = fieldnames(flatten_struct(value));
else
	names = value;
end

%------------------------------
% SET NAMES
%------------------------------

%--
% set canonical names in description structure
%--

% NOTE: flatten and unflatten achieves this

desc = flatten_struct(desc);

for k = 1:length(names)
	desc.([names{k}, '__name']) = names{k};
end

desc = unflatten_struct(desc);
