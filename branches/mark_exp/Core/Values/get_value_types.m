function types = get_value_types

types = {'continuous', 'continuous-interval', 'discrete', 'discrete-subset', 'element', 'subset'};