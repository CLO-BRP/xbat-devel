function [lines, head] = get_dochead(file)

% get_dochead - from file
% -----------------------
%
% [lines, head] = get_dochead(file)
%
% Input:
% ------
%  file - to examine
%
% Output:
% -------
%  lines - of dochead
%  head - values

head = struct; lines = file_head(file, 32);

if isempty(lines)
	return;
end

lines = strtrim(lines); within = false;

for k = 1:numel(lines)
	current = lines{k};
		
	if ~within
		within =  strmatch('%% BEGIN-DOCHEAD', current);
	end
	
	if ~within || ~strncmp(current, '%% ', 3)
		continue;
	end 
	
	if strmatch('%% END-DOCHEAD', current)
		break;
	end
	
	% NOTE: when we are within the DOCHEAD block and our line begins with '%%' we contain a field-value pair
	
	[field, value] = strtok(current, '='); field = lower(strtrim(field)); value = strtrim(value(2:end));
	
	if ~isempty(value)
		head.(field(4:end)) = value;
	end
end

lines = lines(1:k);