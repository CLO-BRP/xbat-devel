function [output, docs] = build_docs(root, type, rescan)

% build_docs - from sources found in root
% ---------------------------------------
%
% build_docs(root, type, output, rescan)
%
% Input:
% ------
%  root - for build scan
%  type - of document 'article' or 'report'
%  rescan - request
%
% Output:
% -------
%  output - file
%  docs - source directories and files used

%--
% handle input and setup
%--

if nargin < 4
	rescan = false;
end

if nargin < 2 || isempty(type)
	type = 'article';
end

if ~nargin
	root = pwd;
end

persistent PART HELPER;

if isempty(PART)
	here = fileparts(mfilename('fullpath')); 
	
	PART.preamble = file_readlines(fullfile(here, 'preamble.tex'));
	
	texify = get_tool('texify.exe');
	
	if isempty(texify)
		error('Unable to find build helper ''texify.exe'', make sure MikTeX is installed.');
	end
	
	HELPER.texify = texify;
end

flags = {'--clean', '--pdf', '--quiet', '--run-viewer'};

%--
% get docs sources
%--

docs = get_docs(root, rescan);

%--
% build document from sources
%--

start = pwd; output = cell(size(docs));

for k = 1:numel(docs)
	current = docs(k); 

	file = fullfile(docs(k).path, 'docs.tex');
	
	[ignore, head] = get_dochead(file); lines = file_readlines(file);
	
	% NOTE: this should not happen in 'verbatim' environments?
	
	build = create_dir(fullfile(current.path, 'Build'));
	
	output{k} = fullfile(build, 'output.tex');
	
	if isempty(head.author)
		author = '';
	else
		if ~isfield(head, 'email') || isempty(head.email)
			author = ['\author{', head.author, '}'];
		else
			author = ['\author{', head.author, '\\ \href{mailto:', head.email, '}{', head.email, '}}'];
		end
	end
	
	lines = { ...
		['\documentclass[11pt]{', type, '}'], ...
		PART.preamble{:}, ...
		['\title{', head.title,'}'], ...
		author, ...
		'\begin{document}', ...
		'\maketitle' ...
		lines{:}, ...
		'\end{document}' ...
	}; %#ok<*CCAT>

	lines = inject_includes(lines);
	
	file_writelines(output{k}, lines);
	
	cd(build); use_tool(HELPER.texify, quote(output{k}), flags{:});
end

cd(start);


%-------------------
% INJECT_INCLUDES
%-------------------

function lines = inject_includes(lines)

inject = [];

for k = 1:numel(lines)
	current = lines{k};
	
	if ~isempty(strmatch('\input{', current))
		inject = k; break;
	end
end
	
if isempty(inject)
	return;
end

file = lines{inject}(find(current == '{') + 1:end - 1);

included = file_readlines(file);

lines = [vec(lines(1:inject - 1)); included; vec(lines(inject + 1:end))];

lines = inject_includes(lines);
	

