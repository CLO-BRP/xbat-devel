function docs = get_docs(root, rescan)

% get_docs - source directories and files
% ---------------------------------------
%
% docs = get_docs(root, rescan)
%
% Input:
% ------
%  root - for docs scan
%  rescan - request
%
% Output:
% -------
%  docs - directories and files

%--
% handle input
%--

if nargin < 2
	rescan = false;
end

if ~nargin
	root = pwd;
end

%--
% check for sources, managing persistent store
%--

persistent SOURCE;

if isempty(SOURCE)
	SOURCE = struct;
end

key = hash_varname(root);

if isfield(SOURCE, key) && ~rescan
	source = SOURCE.(key);
else
	disp(' '); disp(['Scanning ''', root, ''' for DOCS ...']); start = clock;
	
	source = sort(scan_dir(root, @select_docs)); SOURCE.(key) = source;

	disp([int2str(numel(source)), ' directories found. (', sec_to_clock(etime(clock, start)), ')']); disp(' ');
end

%--
% find source contents and build
%--

% TODO: consider processing this into a tree

docs = empty(struct('path', [], 'files', []));

for k = 1:numel(source)

	files = what_ext(source{k}, 'tex');

	% NOTE: we skip adding when there are no relevant files
	
	if ~isfield(files, 'tex') 
		continue;
	end

	docs(end + 1).path = source{k}; %#ok<*AGROW>
	
	docs(end).files = files.tex;
end


%------------------------
% SELECT_DOCS
%------------------------

% NOTE: this is a callback helper for 'scan_dir'

function result = select_docs(current)

if string_ends(current, 'DOCS')
	result = current;
	
	disp(result);
else
	result = [];
end