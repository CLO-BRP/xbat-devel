function remove_tracer_callback(handle)

%--
% get callbacks
%--

[ignore, field, value] = get_callbacks(handle);

%--
% examine callbacks to see if they are tracers
%--

% NOTE: this is easy as all tracers have the same function handle at the start
