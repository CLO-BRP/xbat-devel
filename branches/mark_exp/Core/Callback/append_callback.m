function result = append_callback(handle, fun)

% append_callback - append control callback function array
% --------------------------------------------------------
%
% result = append_callback(handle, fun)
%
% Input:
% ------
%  handle - control
%  fun - callback function
%
% Output:
% -------
%  result - length of callback function array

result = add_callback_function(handle, 'callback', fun);

