function [callback, count] = get_callback(handle)

% get_callback - get callbacks from handle
% ----------------------------------------
%
% [callback, count] = get_callback(handle)
%
% Input:
% ------
%  handle - handle
%
% Output:
% -------
%  callback - handle callbacks struct
%  count - available callback count

%--
% handle input
%--

if isempty(handle) || ~ishandle(handle)
	callback = struct; count = 0; return;
end

%--
% get handle callbacks and count them
%--

callback = get_callbacks(handle);

count = length(fieldnames(callback));


%------------------------------
% GET_CALLBACKS
%------------------------------

function callback = get_callbacks(handle)

fields = get_callback_fields(handle);

callback = struct;

for k = 1:length(fields)
	callback.(fields{k}) = get(handle, fields{k});
end


%------------------------------
% GET_CALLBACK_FIELDS
%------------------------------

function fields = get_callback_fields(handle)

fields = fieldnames(get(handle));

for k = length(fields):-1:1
	
	if ~isempty(strfind(lower(fields{k}), 'fcn')) || strcmpi(fields{k}, 'callback')
		continue;
	end

	fields(k) = [];
	
end