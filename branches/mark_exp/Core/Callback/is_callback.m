function [value, type] = is_callback(callback)

% is_callback - test input is callback and if so get type
% -------------------------------------------------------
%
% [value, type] = is_callback(callback)
%
% Input:
% ------
%  callback - proposed callback
%
% Output:
% -------
%  value - callback indicator
%  type - callback type

% TODO: this function could take a second input for the callback type and perform that test as well

%--
% set failure default values
%--

value = 0; type = '';

%--
% evaluate callback input
%--

switch class(callback)
	
	%--
	% string callback
	%--
	
	case 'char', value = 1; type = 'string';
		
	%--
	% simple callback
	%--
	
	case 'function_handle', value = exist(callback); type = 'simple';
		
	%--
	% complex callbacks
	%--
	
	case 'cell'
		
		%--
		% parametrized callback
		%--
		
		value = is_parametrized_callback(callback);
		
		if value
			
			type = 'parametrized';
		
		else
			
			% TODO: chains are not well developed
			
			%--
			% callback chain
			%--
			
			for k = 1:length(callback)
				value(k) = is_callback(callback{k});
			end

			value = all(value);
			
			if value
				type = 'chain';
			end
		
		end
		
end


%---------------------------------------
% IS_PARAMETRIZED_CALLBACK
%---------------------------------------

function value = is_parametrized_callback(callback)

%--
% check we have a cell array with more than one element
%--

% NOTE: a parametrized callback contains a function handle and arguments

if ~iscell(callback) || (length(callback) < 2)
	value = 0; return;
end

%--
% check that the first element is a function handle to an existing function
%--

fun = callback{1};

if ~isa(fun, 'function_handle')
	value = 0; return;
end

value = exist(fun); %#ok<EXIST>
