function n = selection_delete(ax, sel)

% selection_delete - delete selections in axes
% --------------------------------------------
% 
% selection_delete(ax, sel)
%
% Input:
% ------
%  ax - selection axes
%  sel - selections (def: all selections in axes)
%
% Output:
% -------
%  n - number of selections deleted

%--
% get all axes selections
%--

if nargin < 2
	sel = get_axes_selections(ax);
end

%--
% delete selections
%--

n = length(sel);

for k = 1:n
	try
		delete(findobj(ax, 'tag', sel(k).tag));
	catch
		n = n - 1;
	end
end