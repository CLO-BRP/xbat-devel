function event = selection_log(par, log, data)

% selection_log - log selection to chosen or active log
% -----------------------------------------------------
% 
% event = selection_log(par, log, data)
%
% Input:
% ------
%  par - browser
%  log - log to append
%  data - browser state
%
% Output:
% -------
%  event - resulting event

%------------------
% HANDLE INPUT
%------------------

% NOTE: with default settings we can use this function as a command-line toy

%--
% set default browser and get browser state if needed
%--

if ~nargin || isempty(par)
	par = get_active_browser; 
end

if isempty(par)
	event = []; return; 
end

if nargin < 3 || isempty(data)
	data = get_browser(par);
end

%--
% get log index from log input
%--

logs = get_browser(par, 'log', data);

% NOTE: with no input we set active log

if (nargin < 2) || isempty(log)
	k = data.browser.log_active;
end 

% NOTE: with log name input we get log index

if ~exist('k', 'var') && ischar(log)
	k = find(strcmp(log_name(logs), log));
end

% NOTE: otherwise we have a log index as input

if ~exist('k', 'var');	
	k = log;
end
	
%------------------
% LOG SELECTION
%------------------

%--
% get event from selection or marker
%--

[selection, count] = get_browser_selection(par, data);

if count
	event = selection.event;
else
	event = get_marker_event(get_browser_marker(par, data));
end

if isempty(event)
	return;
end

%--
% append event to log
%--

% TODO: verify 'selection' and 'marker' are expressed in the same time units

event = map_events(event, data.browser.sound, 'slider', 'record');

[log, event] = log_append(data.browser.log(k), event);

%------------------
% UPDATE DISPLAY
%------------------

browser_display(par, 'events'); % NOTE: we do this because the available data is not current

%--
% update event palette if available
%--

pal = get_palette(par, 'Event');

if ~isempty(pal)
   
	handles = get_control(pal, 'find_events', 'handles');

	browser_controls(par, 'find_events', handles.obj);	

end

%--
% select logged selection event and reset marker 
%--

% NOTE: we are restoring selection and marker state here, these are lost when we do a simple display, this may not be right

% NOTE: the possibility of multiple selections exists

% db_disp; par, event, log

if count
	set_browser_selection(par, event, log);
end

% event_bdfun(par, log, [event.id]);

set_browser_marker(par);

