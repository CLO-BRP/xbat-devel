function lines = get_audio_player_snippet(root, file, varargin)

% build_audio_player_snippet - build page snippet to link audio player
% --------------------------------------------------------------------
%
% lines = get_audio_player_snippet(root, file, varargin)
%
% Input:
% ------
%  root - server root
%  file - audio file
%  field, value - option fields and values
%
% Output:
% -------
%  lines - snippet lines

%--
% handle input
%--

if nargin < 3
	opt = get_default_options;
end 

% TODO: parse inputs, consider color conversion

if nargin < 2
	file = 'winning-a-battle';
end

if ~nargin
	lines = opt; return;
end

%--
% load template and replace variables
%--

lines = file_readlines('audio-player.html');

for k = 1:numel(lines)
	
	in = {'<%root%>', '<%file%>'}; out = {root, file};
	
	for j = 1:numel(in)
		lines{k} = strrep(lines{k}, in{j}, out{j});
	end
	
end

%--
% display lines if no output is requested
%--

if ~nargout
	disp(' '); iterate(@disp, lines); clear lines;
end


%----------------------------------
% GET_DEFAULT_OPTIONS
%----------------------------------

function opt = get_default_options(opt)

%--
% page options
%--

opt.id = 1; % NOTE: this is used to build the identifier string for the player

opt.header = 1; % NOTE: determine whether we want to include the script, only needed once per page

%--
% player options
%--

opt.bg = 'f8f8f8';

opt.leftbg = 'eeeeee';

opt.lefticon = '666666';

opt.rightbg = 'cccccc';

opt.rightbghover = '999999';

opt.righticon = '666666';

opt.righticonhover = 'ffffff';

opt.text = '666666';

opt.slider = '666666';

opt.track = 'FFFFFF';

opt.border = '666666';

opt.loader = '9FFFB8';

