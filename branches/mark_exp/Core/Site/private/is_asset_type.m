function value = is_asset_type(type)

% IS_ASSET_TYPE check input is asset type
%
% value = is_asset_type(type)

value = ischar(type);

if value
	value = ismember(type, asset_types);
end
