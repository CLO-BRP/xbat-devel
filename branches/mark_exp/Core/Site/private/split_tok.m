function tok = split_tok(line)

%--
% hide quoted spaces from split parser
%--

ix = strfind(line, '"');

% NOTE: there are an even number of quotes

quoted = ~isempty(ix) && ~mod(length(ix), 2);

if quoted
	
	rep = char(12);
	
	for k = 1:2:length(ix)
		line(ix(k) + 1:ix(k + 1) - 1) = strrep(line(ix(k) + 1:ix(k + 1) - 1), ' ', rep);
	end

end

%--
% split on spaces
%--

tok = str_split(line, ' ');

%--
% reset hidden spaces if needed
%--

if quoted
	
	tok = strrep(tok, rep, ' ');

end