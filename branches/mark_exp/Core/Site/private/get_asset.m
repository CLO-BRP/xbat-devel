function asset = get_asset(site, type, name)

% GET_ASSET get named asset of specified type from site
%
% asset = get_asset(site, type, name)

%--
% get all site assets of type
%--

assets = get_assets(site, type);

%--
% match and output asset
%--

name = strrep(name, '/', filesep);

ix = strmatch(name, assets);

switch length(ix)
	
	case 0
		asset = '';
		
	case 1
		name = strrep(assets{ix}, '/', filesep);
		
		asset = [assets_root(site, type), filesep, assets{ix}]; 
 
	otherwise
		name = cell(length(ix), 1);
		
		for k = 1:length(ix)
			name{k} = strrep(assets{ix(k)}, '/', filesep);
		end
		
		asset = strcat(assets_root(site, type), filesep, name);
		
end 
