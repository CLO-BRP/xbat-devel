function location = get_attachment_location(obj, root)

% get_attachment_location - attachment directory or fragment
% ----------------------------------------------------------
%
% location = get_attachment_location(obj, root)
%
% Input:
% ------
%  obj - attached or id
%  root - of attachment files
%
% Output:
% -------
%  location - of attachment or part if no root

%--
% handle input
%--

if nargin < 2
	root = '';
end

if isstruct(obj) && ~isfield(obj, 'id')
	error('Input is missing identifier.');
end

if isnumeric(obj)
	if any(round(obj) ~= obj)
		error('Input is not proper set of identifiers.');
	end

	obj = struct('id', obj);
end

% NOTE: handle multiple object input

if numel(obj) > 1
	location = iterate(mfilename, obj, root); return;
end

% NOTE: we check two conventional fields for file name

file = ''; 

% if isempty(file) && isfield(obj, 'file')
% 	file = obj.file;
% end
% 
% if isempty(file) && isfield(obj, 'filename'); 
% 	file = obj.filename;
% end

%--
% build attachment location
%--

% NOTE: this is the location construction used in the 'attachment_fu' rails plugin

id_str = int_to_str(obj.id, 10^7, '0');

% TODO: make sure we are using singular names for the type roots, the only plural is at the top

location = fullfile(root, id_str(1:4), id_str(5:8), file);

