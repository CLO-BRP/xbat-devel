function model = generate_barn_extension(ext, server, pretend, force)

% generate_barn_extension - model files
% -------------------------------------
%
% model = generate_barn_extension(ext, server, pretend, force)
%
% Input:
% ------
%  ext - robot
%  server - to write to
%  pretend - and display files to generate
%  force - file creation
%
% Output:
% -------
%  model - files
%
% See also: extension_classname, extension_table_names

% TODO: generalize to 'generate_rails_extension', this will require a different approach to parametrization

%--
% handle input
%--

if nargin < 4
	force = 0;
end 

if nargin < 3
	pretend = 1;
end

if nargin < 2 || isempty(server)
	[ignore, server] = local_barn;
end

if trivial(server)
	return;
end

if numel(ext) > 1
	iterate(mfilename, ext, server); return;
end

%--
% generate models for extension
%--

disp(' '); db_disp(['extension models for: ', ext.name]);


% NOTE: we are hard-coding two space indentation of generated lines

table = extension_table_names(ext);

fieldname = extension_fieldname(ext, 1);

relations = relation_lines(ext, table);

% VALUE MODEL

lines = { ...
	['class ', camelize(fieldname), ' < ActiveRecord::Base'], ...
	['  set_table_name "', table.value, '"'], ...
	'  ', ...
	 relations{:}, ...
	'end' ...
};

model = fullfile(get_barn_extension_root(ext), [fieldname, '.rb']);

if pretend
	disp(model); disp(' '); iterate(@disp, lines); disp(' ');
else
	disp(model);
	
	if exist(model, 'file') && ~force
		disp('Skipping, file exists.'); disp(' ');
	else 
		file_writelines(model, lines);
	end
end

% PARAMETER MODEL

if isempty(ext.fun.parameter.create)
	disp(' '); return;
end

lines = { ...
	['class ', camelize(fieldname), 'Parameter < ActiveRecord::Base'], ...
	['  set_table_name "', table.parameter, '"'], ...
	'  ', ...
	['  has_many :', fieldname, ', :foreign_key => :parameter_id, :class_name => "', camelize(fieldname), '"'], ...
	'  ', ...
	['  alias_method :values, :', fieldname], ...
	'end' ...
};

model2 = fullfile(get_barn_extension_root(ext), [fieldname, '_parameter.rb']);

if pretend
	disp(model2); disp(' '); iterate(@disp, lines); disp(' ');
else
	disp(model2);
	
	if exist(model2, 'file') && ~force
		disp('Skipping, file exists.');
	else 
		file_writelines(model2, lines);
	end
	
	disp(' ');
end

model = {model, model2};

%-----------------------
% RELATION_LINES
%-----------------------

function lines = relation_lines(ext, table)

% NOTE: a measure belongs to what it measures and possibly to a parameter

fieldname = extension_fieldname(ext, 1);

if string_contains(ext.type, 'measure')
	
	part = str_split(ext.type, '_'); lines = {['  belongs_to :', part{1}]};

	if ~isempty(ext.fun.parameter.create)
		lines{end + 1} = ['  belongs_to :', fieldname, '_parameter, :foreign_key => :parameter_id'];
		lines{end + 1} = '  ';
		lines{end + 1} = ['  alias_method :parameter, :', fieldname, '_parameter'];
	end
	
end


