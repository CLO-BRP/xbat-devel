function root = public_root

% public_root - for rails server
% ------------------------------
%
% root = public_root
%
% Output:
% -------
%  root - for public files

root = fullfile(rails_root, 'public');