function [store, server] = local_barn(store, server)

% local_barn - set and get store and server
% -----------------------------------------
%
% [store, server] = local_barn(store, server)
%
% Input:
% ------
%  store - description
%  server - description
% 
% Output:
% -------
%  store - current
%  server - current

% NOTE: ensure that the store is proper when setting, something more sophisticated is needed

if nargin
	establish_barn_schema(store);
end

%--
% set or get
%--

switch nargin
	
	% NOTE: here we get the current local barn configuration as values or through the GUI
	
	case 0
		if ~nargout
			local_barn_dialog;
		end
		
		store = get_env('local_barn_store');
		
		server = get_env('local_barn_server');
		
	% NOTE: the next two cases set the store and possibly the server
	
	case 1
		set_env('local_barn_store', store);
		
	case 2
		set_env('local_barn_store', store);
	
		set_env('local_barn_server', server);

end

if ~nargout
	disp(store); disp(server); clear store;
end

%-----------------------------------
% LOCAL_BARN_GUI
%-----------------------------------

function local_barn_dialog

%--
% describe controls
%--

control = empty(control_create);

control(end + 1) = header_control('Configuration');

[ext, ignore, context] = get_browser_extension('log_format', [], 'BARN');

control = [control, get_extension_controls(ext, context)];


%--
% configure and render dialog
%--

out = dialog_group('BARN', control);

%--
% handle dialog output
%--

if isempty(out)
	
end

