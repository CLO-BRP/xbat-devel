function [store, exists] = get_barn_store(name, adapter, hostname)

% get_barn_store - configuration for given name and adapter
% ---------------------------------------------------------
%
% [store, exists] = get_barn_store(name, adapter)
%
% Input:
% ------
%  name - of barn
%  adapter - used
%
% Output:
% -------
%  store - configuration
%  exists - indicator

%--
% handle input
%--

if ~nargin
	name = 'barn';
end

if nargin < 2
	adapter = 'SQLite';
end

if nargin < 3
	hostname = 'localhost';
end

%--
% create store config
%--

store = create_database_config( ... 
	'adapter', adapter, 'database', name, 'hostname', hostname ...
);

if strcmpi(adapter, 'sqlite')
	store.database = barn_database_file(store.database);
end

store = create_database_config(store);

%--
% determine if database exists when requested
%--

% TODO: this only works currently for the SQLite adapter

if nargout < 2
	return;
end

exists = database_exists(store);

