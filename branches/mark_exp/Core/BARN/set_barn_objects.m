function obj = set_barn_objects(store, obj, id, fields, table)

% set_barn_objects - store to database
% ------------------------------------
%
% obj = set_barn_objects(store, obj, id, fields, table)
%
% Input:
% ------
%  store - description
%  obj - to store
%  id - of object, when we are updating
%  fields - to store
%  table - destination, may be inferred from object variable name
%
% Output:
% -------
%  obj - stored
%  
% NOTE: this function helps with the less dynamic part of the schema
%
% See also: set_database_objects

%------------------
% HANDLE INPUT
%------------------

%--
% we typically reflect the table name from the object variable name
%--

if nargin < 5
	table = inputname(2);
end 

if isempty(table)
	error('Unable to determine objects table from input.');
end

%--
% create and check persistent field store
%--

persistent FIELDS;

if isempty(FIELDS)
	FIELDS = struct;
end 

% NOTE: we use 'create_barn_table' to get a prototype object with all the fields

if ~isfield(FIELDS, table)
	[ignore, ignore, prototype] = create_barn_table(table); FIELDS.(table) = fieldnames(prototype);
end

known = FIELDS.(table);

%--
% process objects to store
%--

% TODO: this should be factored it does not belong here

switch table
	
	case 'user'
		
		if isfield(obj, 'id')
			for k = 1:numel(obj)
				obj(k).id = [];
			end
		end
		
	case 'sound'
		
		obj(1).name = '';
		
		for k = 1:numel(obj)
			obj(k).name = sound_name(obj(k));
		end
		
end

%--
% check object fields against prototype fields
%--

% NOTE: get all provided fields, filter requested fields with provided and known

provided = fieldnames(obj);

if nargin < 4 || isempty(fields)
	fields = provided;
else
	fields = provided(string_is_member(provided, fields));
end

fields = known(string_is_member(known, fields));

% NOTE: empty field input is ok, however at this point the fields should not be empty

if isempty(fields)
	error('Unable to find any known fields in object input.');
end 

%--
% set new object default
%--

if nargin < 3
	id = [];
end

%------------------
% SET OBJECTS
%------------------

%--
% set guid if needed
%--

% NOTE: a guid is needed if it is one of the known fields and the objects are coming in without identifier (new objects)

if string_is_member('guid', known) && isempty(id)
	
	if ~isfield(obj, 'guid')
		for k = 1:numel(obj)
			obj(k).guid = generate_uuid;
		end
	else
		for k = 1:numel(obj)
			if isempty(obj(k).guid), obj(k).guid = generate_uuid; end
		end
	end
		
	fields = union(fields, 'guid');
	
end

%--
% store objects to database, or at least generate query
%--

% NOTE: the object output request reads the stored objects from the database

if ~nargout
	set_database_objects(store, obj, id, fields, table);
else
	obj = set_database_objects(store, obj, id, fields, table);
end

