function robot = get_barn_robots(type)

%--
% handle input 
%--

if ~nargin
	type = barn_robot_types;
else
	if ~string_is_member(type, barn_robot_types)
		error(['Unrecognized robot type ''', type, '''.']);
	end
end

%--
% get robot extensions
%--

robot = get_extensions(type);

if iscell(robot)
	robot = collapse_cell(robot);
end
