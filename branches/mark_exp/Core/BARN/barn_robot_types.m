function robots = get_barn_robot_types

% get_barn_robot_types - extensions types that may become users
% -------------------------------------------------------------
%
% types = get_barn_robot_types
%
% Output:
% -------
%  types - of robots

types = get_barn_extension_types; 

robots = types(~string_contains(types, {'annotation', 'attribute', 'feature'}));