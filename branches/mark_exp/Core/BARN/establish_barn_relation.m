function table = establish_barn_relation(store, table1, table2, value, opt)

% establish_barn_relation - create relation table and keep track in master
% ------------------------------------------------------------------------
%
% table = establish_barn_relation(store, table1, table2, value, opt)
%
% Input:
% ------
%  store - database
%  table1, table2 - tables to relate
%  value - of relation
%  opt - for relation
%
% Output:
% -------
%  table - row from master

%--
% handle input
%--

if nargin < 5 
	opt = establish_relation;
end

if nargin < 4
	value = [];
end

%--
% establish and register relation
%--

% NOTE: this is the same function used by 'establish_relation' to compute the relation table name

table = relation_table_name(table1, table2, opt); 

sql = establish_relation([], table1, table2, value, opt);

if ~nargout
	establish_barn_table(store, table, 'relation', sql); clear table;
else
	table = establish_barn_table(store, table, 'relation', sql);
end