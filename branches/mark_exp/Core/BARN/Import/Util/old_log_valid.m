function valid = old_log_valid(file)

% old_log_valid - test whether file is old log
% --------------------------------------------
%
% valid = old_log_valid(file)
%
% Input:
% ------
%  file - candidate
%
% Output:
% -------
%  valid - indicator

%--
% handle multiple file input
%--

if iscellstr(file) && numel(file) > 1
	valid = iterate(mfilename, file); return;
end

%--
% perform test
%--

% NOTE: the 'load' operation could be slow, consider something faster or load and keep log

try
	names = fieldnames(load(file));

	% TODO: explain what the test is here
	
	ix = union(strmatch('Log_', names), strmatch('log', names));
	
	valid = length(ix) == 1;
catch
	valid = 0;
end