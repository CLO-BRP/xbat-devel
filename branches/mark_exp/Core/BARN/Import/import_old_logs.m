function import_old_logs(source)

% import_old_logs - into barn
% ---------------------------
%
% import_old_logs(source, store, server)
%
% Input:
% ------
%  source - old logs
%  store - for barn
%  server - for barn

% NOTE: eventually we can create a dialog

% TODO: consider adding a project input

%--
% handle input
%--

if nargin < 3
	[store, server] = local_barn;
end

%--
% handle multiple sources
%--

if iscell(source) && numel(source) > 1
	iterate(mfilename, source); return;
end

%----------------------------------
% COLLECT IMPORT LOGS 
%----------------------------------

%--
% find logs
%--

content = no_dot_dir(source);

for k = numel(content):-1:1
	
	% TODO: check for mat file and load and check for log, and keep
	% otherwise discard
	
	% NOTE: this discards
	content(k) = [];
	
end

% collect sound file source locations

% NOTE: check logs for sound and see whether they are accesible

%----------------------------------
% BARN IMPORT
%----------------------------------

store = local_barn;

user = get_barn_user;

% create BARN project

project.name = 'Test project'; 

project.description = 'Testing the import functions';

project.user_id = user.id; 

project = set_barn_objects(store, project);

% collect users and create users

% add users to projects

% HARDER

% create BARN sounds and logs

% add sounds and logs to project

% add events to BARN logs, how to deal with rendering

%---------------------------------
% FUNCTIONS
%---------------------------------

% TODO: consider getting some functions from the old version and putting
% them here as sub-functions



