function varargout = generate_barn_rails

%--
% set rails root and check that it exists
%--

root = fullfile(barn_root, 'Server', 'rails');

% NOTE: the existence of the root directory is a proxy for a proper rails app

if exist_dir(root)
	return;
end 

%--
% generate new rails app
%--

str = ['rails "', root, '"'];

if ~nargout
	system(str);
else
	[varargout{:}] = system(str);
end 