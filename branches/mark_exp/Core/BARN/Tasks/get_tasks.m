function task = get_tasks(store, type, limit)

% get_tasks - request tasks from store in various ways
% ----------------------------------------------------
%
% task = get_tasks(store, type, limit)
%
% Input:
% ------
%  store - with tasks
%  type - of tasks
%  limit - for request
%
% Output:
% -------
%  task - list

%--
% handle input
%--

if nargin < 3
	limit = [];
end

if nargin < 2
	type = 'waiting';
end

if ~nargin
	store = local_barn;
end 

if isempty(store)
	task = []; return;
end

%--
% get tasks by type
%--
				
if ~is_known_request(type)
	error(['Unrecognized task type ''', type, '''.']);
end

[status, task] = query(store, get_request_query(type, limit));

% db_disp; disp(get_request_query(type, limit)); disp(task);

if ~nargout
	disp(task); clear task;
end


%--------------------------------
% KNOWN_COMMANDS
%--------------------------------

function known = known_requests

known = {'waiting', 'started', 'running', 'completed'};


%--------------------------------
% IS_KNOWN_COMMAND
%--------------------------------

function result = is_known_request(type)

result = string_is_member(type, known_requests);


%--------------------------------
% GET_REQUEST_QUERY
%--------------------------------

function sql = get_request_query(type, limit)

%--
% handle input
%--

if nargin < 2
	limit = 1;
end

%--
% build common prefix and suffix parts of query
%--

if strcmp(type, 'completed')
	prefix = 'SELECT * FROM task WHERE completed_at IS NOT NULL '; 
else
	prefix = 'SELECT * FROM task WHERE completed_at IS NULL '; 
end

if isempty(limit)
	suffix = ';';
else
	suffix = [' LIMIT ', int2str(limit), ';'];
end

%--
% build query based on command
%--

switch type
	
	case 'waiting'
		sql = [prefix, 'AND started_at IS NULL ORDER BY created_at', suffix];
		
	case 'started'
		sql = [prefix, 'AND started_at IS NOT NULL ORDER BY started_at', suffix];
	
	case 'running'
		sql = [prefix, 'AND running = 1 ORDER BY started_at', suffix];
		
	case 'completed'
		sql = [prefix, 'ORDER by completed_at DESC', suffix];
		
end
