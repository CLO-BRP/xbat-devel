function daemon = task_daemon(store, server)

% task_daemon - for BARN
% ----------------------
%
% daemon = task_daemon(store, server)
%
% Input:
% ------
%  store - with tasks
%  server - with files
%
% Output:
% -------
%  daemon - timer

%--
% set local barn store default
%--

if ~nargin
	[store, server] = local_barn;
end

%--
% create barn daemon to check on store
%--

daemon = create_timer('BARN task daemon', {@on__timer, store, server});


%------------------------
% ON__TIMER
%------------------------

function on__timer(obj, eventdata, store, server) %#ok<INUSL>

%--
% select tasks from store
%--

task = get_tasks(store);

if isempty(task)
	return;
end

%--
% dispatch tasks
%--

for k = 1:numel(task)
	
	current = task(k); 

	user = get_database_objects(store, 'user', current.user_id);

	%--
	% handle task
	%--
	
	% TODO: given that actions may have quite a bit of code we need to encapsulate, perhaps extensions
	
	switch current.task.action

		case 'create_sound'

			file = get_database_objects(store, 'data_file', current.task.id);
			
			db_disp; disp(file)
			
			for j = 1:numel(file)
				sound = create_sound_from_file(file(j), user, store, server)
				
% 				add_sounds
			end
			
			update_task(store, 'id', current.id, ...
				'progress', 1, ...
				'report', ['Sound created.'] ...
			);

		case 'scan_sound'
			
		case 'render_event'
			
		case 'measure_log';
			
		case 'measure_event'
			
		case 'classify_event'
			
	end
	
end 


%-----------------------------------------
% HELPERS
%-----------------------------------------

%-----------------------------
% CREATE_SOUND_FROM_FILE
%-----------------------------

function sound = create_sound_from_file(file, user, store, server)

%--
% make sure we have access to the file
%--

db_disp; file

if ~data_file_available(file)
				
	file.location = map_data_file_location(file, user, store, server);
	
	disp(file)
	
	if ~data_file_available(file)
		return;
	end
end

db_disp; file

%--
% create sound
%--

if file.directory
	sound = sound_create('file stream', file.location);
else
	sound = sound_create('file', file.location);
end

%--
% add to default user library
%--

% NOTE: this is the sound we need to compute upon

lib = get_libraries(get_xbat_user(user), 'name', 'Default');

add_sounds(sound, lib);

%--
% add to barn
%--

sound = get_barn_sound(store, sound, user);


%-----------------------------
% DATA_FILE_AVAILABLE
%-----------------------------

function available = data_file_available(file)

if file.directory
	available = exist(file.location, 'dir');
else
	available = exist(file.location, 'file');
end


%-----------------------------
% MAP_DATA_FILE_LOCATION
%-----------------------------

function location = map_data_file_location(file, user, store, server)

% NOTE: the file locations are web server centric, how the web application acceses the files
			
% NOTE: these locations must be transformed to make sense in the context of the compute server
			
% NOTE: this is the user ftp file transformation

start = strfind(file.location, user.email);

location = fullfile(user_files_root(user, store, server), strrep(file.location(start + numel(user.email):end), '/', filesep));

