function task = update_task(store, varargin)

% TODO: we are having problems getting the same time as the server, fix this

[status, result] = query(store, 'SELECT CURRENT_TIMESTAMP;'); last_update = struct_pop(result);

switch length(varargin)
	
	case 0
		task = empty(create_task); return;
		
	case 1
		task = varargin{1};
		
		if numel([task.id]) < numel(task)
			error('Tasks to update must contain identifier.');
		end
		
		for k = 1:numel(task)
			task(k).last_update = last_update;
		end 
		
		task = set_barn_objects(store, task);
		
	otherwise
		[task, field] = create_task(varargin{:}); task.last_update = last_update; field = union(field, 'last_update');
		
		task = set_barn_objects(store, task, [task.id], field);
		
end
