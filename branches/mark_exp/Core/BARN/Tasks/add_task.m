function task = add_task(store, varargin)

% add_task - list to store
% ------------------------
% 
% task = add_task(store, task)
%
%      = add_task(store, field, value, ... )
%
% Input:
% ------
%  store - for tasks
%  task - list
%  field - of task
%  value - for field
%
% Output:
% -------
%  task - list submitted

%--
% handle input variety
%--

switch length(varargin)
	
	case 0
		task = empty(create_task); return;
		
	case 1
		task = varargin{1};
		
	otherwise
		task = create_task(varargin{:});
		
end

%--
% set basic task fields
%--

% NOTE: we need a user identifier, the worker address, and start and update stamps

user = get_barn_user(store); worker = get_ip; current_timestamp = get_current_timestamp(store); 

for k = 1:numel(task)
	task(k).user_id = user.id; task(k).worker = worker; task(k).last_update = current_timestamp;
end

%--
% add tasks
%--

task = set_barn_objects(store, task);

if ~nargout
	disp(task); clear task;
end 