function types = get_barn_extension_types(filter)

%--
% set no filter default
%--

if ~nargin
	filter = '';
end

%--
% filter list of extension types using BARN relevance and input filter
%--

types = get_extension_types; 

for k = numel(types):-1:1

	% NOTE: the types filtered here are not represented by tables in the BARN schema
	
	if string_contains(types{k}, {'widget', 'format', 'palette', 'action', 'filter', 'extension', 'source'})
		types(k) = []; continue;
	end

	if ~isempty(filter) && ~string_contains(types{k}, filter)
		types(k) = [];
	end
	
end