function [ result ] = establish_extension_values( store, destructive )

% establish_extension_values - in store
% --------------------------------
%
% result = establish_extension_values(store)
%
% Input:
% ------
%  store - database
%  destructive - set true to clear the table before insert
%
% Output:
% -------
%  result - of operation


% args management 
    if nargin < 2
        destructive = false;
    end


% load structures from extensions installed on disk
    extensions_installed = get_extensions;
    [extension_t, ignore] = get_barn_prototype('extension');    
    extension_t = empty(extension_t);

    
% obtain extensions persisted on DB
    extensions_persisted = get_database_objects_by_column(store, 'extension', 'guid' );
    
    
% if requested, clear the extension table in DB   
    if (destructive == true)
        delete_database_objects(store, extensions_persisted, 'extension');
        extensions_persisted = empty(extensions_persisted);
    end
    
    
% transform installed extensions into persistent extension structs.    
    for i = 1:numel(extensions_installed)
        
        curr_ext = extensions_installed(i);

        %extension_t(end+1).id = extensions_known
        extension_t(end+1).type = curr_ext.type;
        extension_t(end).name = curr_ext.name;
        extension_t(end).guid = curr_ext.guid;
        extension_t(end).input_type = 'xyzzy';
        extension_t(end).output_type = 'plugh';
         
        curr_names = extension_table_names(curr_ext);
        
        extension_t(end).tablename__parameter = curr_names.parameter;
        extension_t(end).tablename__value = curr_names.value;
    end


% determine difference between extension lists by GUID.    
    [ignore, idiff] = setdiff({extension_t.guid}, {extensions_persisted.guid}); ext_difference = extension_t(idiff);

    
% persist difference to DB and return
    if ~isempty(ext_difference)	
        set_database_objects(store, ext_difference, [], fieldnames(ext_difference), 'extension');
    end    

    result = ext_difference    
    
end

