function [root, created] = barn_root

[root, created] = create_dir(fullfile(xbat_root, 'BARN'));

if isempty(root)
	error('Failed to create BARN root.');
end