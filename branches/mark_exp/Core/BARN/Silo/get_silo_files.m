function files = get_silo_files(silo, condition)

% get_silo_files - from scanner database
% --------------------------------------
%
% files = get_silo_files(silo, condition)
%
% Input:
% ------
%  silo - description
%  condition - for selection
%
% Output:
% -------
%  files - from silo

% TODO: we should be able to get files from database and server

%--
% handle input
%--

if nargin < 2
	condition = {};
end

% NOTE: an empty silo means use the local silo database

if ~nargin
	silo = [];
end

%--
% get files
%--

if isempty(silo)
	
	database = fullfile(silo_root, 'files.sqlite3')

	if ~exist(database, 'file')
		error('Silo file database does not exist, run scanner.');
	end

	[status, files] = query(database, 'SELECT * FROM recording');

else
	
end