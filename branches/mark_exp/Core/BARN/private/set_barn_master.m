function table = set_barn_master(store, type, table, sql)

%--
% pack input
%--

barn_master.type = type;

barn_master.name = table;

barn_master.create_sql = sql;

%--
% store in 'barn_master'
%--

% NOTE: we output the master table entry

if ~nargout
	set_database_objects(store, barn_master);
else
	table = set_database_objects(store, barn_master);
end