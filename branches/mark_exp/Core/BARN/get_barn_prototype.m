function [obj, description] = get_barn_prototype(table)

% get_barn_prototype - barn object description
% --------------------------------------------
%
% [obj, description] = get_barn_prototype(table)
%
% Input:
% ------
%  table - name
% 
% Output:
% -------
%  obj - prototype
%  description - of constraints

% TODO: consider packing the output into a 'model'

% NOTE: the prototype includes strcture fields and type hints

hint = column_type_hints; obj.id = hint.integer; obj.guid = hint.string;

description = empty(column_description);

switch table
	
	%-------------------------
	% DATA TABLES
	%-------------------------
	
	case 'user'

		% NOTE: this is the original simple user
		
% 		obj.name = hint.string; 
% 		
% 		obj.email = hint.string;
% 
% 		opt.constraints = {'UNIQUE (name, email)'};
		
		% NOTE: this is the server capable user 
		
		obj.login = hint.string;
		
		obj.name = hint.string; 
      
		obj.email = hint.string;
		
		% NOTE: this makes sure that the first timestamp is 'created_at', a MySQL thing
		
		obj.created_at = hint.timestamp; obj.modified_at = hint.timestamp;
		
		obj.crypted_password = hint.string;
		
      	obj.salt = hint.string;
		
		% NOTE: 'created_at' and 'modified_at' are appended to all tables
		
		% t.column :created_at, :datetime
		
% 		obj.updated_at = hint.timestamp;
		
		obj.remember_token = hint.string;
		
		obj.remember_token_expires_at = hint.timestamp;
		
		obj.activation_code = hint.string;
		
		obj.activated_at = hint.timestamp;
		
		% t.column :state, :string, :null => :no, :default => 'passive'
		
		% TODO: we currently do not have a way of describing contraints here
		
		
		obj.state = hint.string;
		
		description(end + 1) = struct('name', 'state', 'type', 'VARCHAR(255)', 'constraints', 'DEFAULT "passive"');
		
		obj.deleted_at = hint.timestamp;
		
	case 'role'
		
		obj.name = hint.string;
		
		obj = rmfield(obj, 'guid');
		
	case 'library'
		
		obj.name = hint.string;

		obj.user_id = hint.integer;
		
	case 'project'
		
		obj.name = hint.string;

		obj.description = hint.text;
		
		obj.visible = hint.integer;
		
		obj.public = hint.integer;
		
		obj.data_file_root = hint.string;
		
		obj.user_id = hint.integer;

	case 'project_asset'
		
		% NOTE: this does not comfortably fit here, or with the relation or 'tagging' like frameworks. why?
		
		obj = rmfield(obj, 'guid');
		
		obj.project_id = hint.integer;
		
		obj.asset_id = hint.integer;
		
		obj.asset_type = hint.string;
		
	case 'sound' 
		
		obj.name = hint.string;

		obj.user_id = hint.integer;
		
		obj.public = hint.logical;
		
		% NOTE: the below summary properties are computed from the 'recordings'
		
		% NOTE: these leave open the possibility for mixed formats and for dropping and adding files
		
		obj.channels = hint.integer;
		
		obj.samplerate = hint.real;
		
		% NOTE: these limit the adding and dropping of files
		
		obj.duration = hint.real;
		
		obj.samples = hint.real;
		
		obj.content_hash = hint.string;
		
		description(end + 1) = struct('name', 'public', 'type', 'BOOLEAN', 'constraints', 'DEFAULT true');
		
	case 'recording'
		
		% NOTE: currently we have 'name' and 'file' with the same content, the first does not need an extension
		
		% NOTE: this 'info_create' is from the 'sound_file_info' world;
		
		obj = struct_merge(obj, sound_file_info('sample_audio_file.wav'));
		
		obj.samplerate = hint.real;
		
		obj.duration = hint.real;
		
		obj.samples = hint.real;
		
		obj.data_file_id = hint.integer;
		
		% NOTE: if the data file is available, these are not needed
		
        obj.content_hash = hint.string;
        
		obj.location = hint.text;
		
		description(end + 1) = struct('name', 'data_file_id', 'type', 'INTEGER', 'constraints', '');
		
% 		obj.path = hint.string;
	
	case 'log'
		
		obj.name = hint.string;
		
		obj.user_id = hint.integer;
		
		% NOTE: this would make certain things simpler
		
		obj.sound_id = hint.integer;
		
		obj.public = hint.logical;
% 		
		description(end + 1) = struct('name', 'public', 'type', 'BOOLEAN', 'constraints', 'DEFAULT true');
		
% 		opt.constraints = {'UNIQUE (name, user_id, sound_id)'};
		
	case 'event' 
		
		obj = rmfield(obj, {'id', 'guid'});
		
		obj = struct_merge(obj, build_prototype(event_table_fields));
		
		% NOTE: the 'log_id' is added to the SQLite table for BARN logs
		
		obj.log_id = hint.integer;
		
		obj.recording_id = hint.integer;
		
		obj.recording_start = hint.real;
		
	%-------------------------
	% METADATA TABLES
	%-------------------------
		
	case 'barn_master'
		
		obj.type = hint.string; 
		
		obj.name = hint.string;
		
		obj.create_sql = hint.text;
		
		obj = rmfield(obj, 'guid');
		
	case 'task'
		
      obj.name = hint.string;
	  
      obj.description = hint.string;
	  
      obj.task = hint.text;
	  
	  obj.priority = hint.integer;
	  
      obj.user_id = hint.integer;
	  
	  obj.project_id = hint.integer;
	  
      obj.started_at = hint.timestamp;
	  
      obj.worker = hint.string;
	  
	  obj.attempt = hint.integer;
	  
      obj.progress = hint.real;
	  
	  obj.report = hint.text;
	  
	  obj.last_update = hint.timestamp;
	  
      obj.completed_at = hint.timestamp;
	  
% 	  description
	  
	  description(end + 1) = struct('name', 'project_id', 'type', 'INTEGER', 'constraints', '');
	  
	  nulled = {'started_at', 'completed_at'};
	  
	  for k = 1:numel(nulled)
		  description(end + 1) = struct('name', nulled{k}, 'type', 'DATETIME', 'constraints', 'DEFAULT NULL');
	  end
		
	case 'image'
		
		obj.filename = hint.string;
		
		obj.size = hint.integer;
		
		obj.content_type = hint.string;
		
		obj.thumbnail = hint.string;
		
		obj.parent_id = hint.integer;
		
		obj.height = hint.integer;
		
		obj.width = hint.integer;
		
		obj.user_id = hint.integer;
		
		obj.created_at = hint.datetime;
		
		description(end + 1) = struct('name', 'parent_id', 'type', 'INTEGER', 'constraints', 'DEFAULT NULL');
		
	case 'data_file'
		
		obj = add_fields(obj, {'location', 'name', 'ext', 'ctime', 'mtime'});

		obj.size = hint.integer;
		
		obj.directory = hint.boolean;
		
		obj.in_ftp = hint.boolean;
		
		obj.parent_id = hint.integer;
		
		obj.project_id = hint.integer;
		
		obj.user_id = hint.integer;
		
		obj.content_hash = hint.string;
		
		nulled = {'ctime', 'mtime'};

		for k = 1:numel(nulled)
			description(end + 1) = struct('name', nulled{k}, 'type', 'DATETIME', 'constraints', '');
		end
		
		% TODO: make a shortcut for nullable foreign keys
		
		description(end + 1) = struct('name', 'parent_id', 'type', 'INTEGER', 'constraints', '');
		
		description(end + 1) = struct('name', 'project_id', 'type', 'INTEGER', 'constraints', '');
		
	case 'peer'
		
		obj = rmfield(obj, 'guid');
		
		obj.host = hint.string;
		
	case 'sync'
		
		obj = rmfield(obj, 'guid');
		
		obj = add_fields(obj, {'http_method', 'http_user_agent', 'http_request_headers', 'added', 'updated', 'deleted'});
		
		% TODO: we want these to have zero default value
		
		obj = add_fields(obj, strcat({'added', 'updated', 'deleted'}, '_count'), 'integer');
		
		% NOTE: this is a foreign key to a peer user combination, we have to establish this relation
		
		obj.peer_user_id = hint.integer;

	case 'extension'
		
		obj = rmfield(obj, 'guid');
		
		% NOTE: extensions have category labels, consider using machine tags for these
		
		obj = add_fields(obj, {'type', 'name', 'guid', 'input_type', 'output_type', 'tablename__parameter', 'tablename__value'}, 'string');
		
	case 'extension_preset'
		
% 		obj.extension_id = hint.integer;
		
		obj.name = hint.string;
		
		obj.parameter_id = hint.integer; % NOTE: this selects a row within the parameter table
		
		obj.parameter_type = hint.string; % NOTE: this points us to the parameter table
		
		obj.user_id = hint.integer;
        
    case 'file_server'
        
        obj = rmfield(obj, 'guid');
        
        obj = add_fields(obj, {'server', 'username', 'password', 'initial_path', 'protocol'});
        
        obj = add_fields(obj, {'port', 'user_id'}, 'integer');
		
	%-------------------------
	% UNKNOWN
	%-------------------------
		
	otherwise
		
		% NOTE: we do not throw an error but return all trivial
		
		query = ''; lines = {}; obj = struct; 
		
		if ~nargout
			disp(' '); disp(['  Unknown table ''', table, '''.']); disp(' '); clear query; 
		end

		return;
		
end

obj.created_at = hint.timestamp; obj.modified_at = hint.timestamp;


%--------------------------
% ADD_FIELDS
%--------------------------

function obj = add_fields(obj, field, type)

%--
% handle input and get type hint
%--

if nargin < 3
	type = 'string';
end

% NOTE: this checks that the type is correct through the hint fields

hint = column_type_hints; hint = hint.(type);

%--
% add fields
%--

for k = 1:numel(field)
	obj.(field{k}) = hint;
end
		

