function update_sound_user(store)

if ~nargin
	store = local_barn;
end

if isempty(store)
	return;
end

% NOTE: we make sure that sounds are at least associated to their creators, this is simplest thing we can do

for sound = get_barn_sounds(store)
	
	relation = get_relations(store, 'sound', sound.id, 'user', sound.user_id);
	
	if isempty(relation)
		insert_relation(store, 'sound', sound.id, 'user', sound.user_id);
	end
	
end