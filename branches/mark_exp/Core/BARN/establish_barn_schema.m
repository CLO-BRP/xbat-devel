function result = establish_barn_schema(store)

% establish_barn_schema - in store
% --------------------------------
%
% result = establish_barn_schema(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  result - of operation

%-------------------------------------
% SETUP
%-------------------------------------

% TODO: create a little GUI for the creation of new BARNS

% NOTE: the above would typically use a command-line utility for the adapter, or perhaps we can use JDBC generically

result = struct; result.exists = 0; result.created = 0;

% TODO: check whether such configured database exists in all cases

if database_exists(store)
	result.exists = 1; return;
end

if has_table(store, 'barn_master')
	result.exists = 1; return;
end

%-------------------------------------
% CREATE SCHEMA
%-------------------------------------

establish_barn_table(store, 'barn_master', 'core');

%--
% establish BARN object tables
%--

core = { ...
	'user', ...
	'role', ...
	'library', ...
	'project', ...
	'project_asset', ...
	'image', ...
	'sound', ...
	'recording', ...
	'log', ...
	'event', ...
	'peer', ...
	'sync', ...
	'file_server', ...
	'data_file', ...
	'task', ...
    'extension', ...
    'extension_preset' ...
};

for k = 1:numel(core)
	
	try
		establish_barn_table(store, core{k}, 'core');
	catch
		db_disp(['Failed to create ''', core{k}, ''' table.']); rethrow(lasterror);
	end
	
end

%--
% establish BARN relation tables
%--

%--------------

opt = establish_relation; opt.id = 1; % NOTE: we make sure that the relation has an identifier

% NOTE: this is the experimental zone

establish_barn_relation(store, 'user', 'role');

value = struct; value.state = 'string'; value.role_id = 1;

establish_barn_relation(store, 'user', 'project', value, opt);

establish_barn_relation(store, 'user', 'peer', [], opt);

% NOTE: this relation is particularly important, as sounds their associated resources will not be visible if this is missing

establish_barn_relation(store, 'user', 'sound', [], opt);


%--------------

% NOTE: this is the 'library subscription' relation 

opt = establish_relation; opt.id = 1;

establish_barn_relation(store, 'library', 'user', [], opt);

% NOTE: this is the 'sound in library' relation

establish_barn_relation(store, 'library', 'sound');

% NOTE: this is the 'sound is composed of recordings' relation

% NOTE: the cumulative samples, like other sample fields, are real to deal with very large numbers

% TODO: add 'not null' constraint to position

value = struct; value.position = 1; value.cumulative_samples = sqrt(2);

establish_barn_relation(store, 'sound', 'recording', value);

% NOTE: this is the 'log annotates sound' relation

% TODO: consider replacing this relation with a sound foreign key for logs

% establish_barn_relation(store, 'log', 'sound');

% NOTE: this is the 'event has author' relation

establish_barn_relation(store, 'event', 'user');

%--
% establish various polymorphic relations: tags, ratings, notes, ...
%--

% TODO: at the moment these tables are not registered with master or used to compute views

establish_tagging(store);

establish_rating(store);

establish_notes(store);

establish_sharing(store);

establish_attachment(store);

%--
% infer and create relation views to make access simpler
%-- 

% NOTE: this should also help with use by other applications, we capture output to suppress display

% NOTE: this only discovers shallow relations, a recursive extension could uncover further relations

% TODO: extend this to other adapters, the critical function is 'select_related'

if strcmpi(store.adapter, 'sqlite')

	view = establish_related_views(store); %#ok<NASGU>
	
end


%--
% populate extensions registry
%--

establish_extension_values(store);


result.exists = 1; result.created = 1;

