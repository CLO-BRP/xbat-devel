function pal = update_log_palette(par, data)

% update_log_palette - update log palette when opening or closing logs
% --------------------------------------------------------------------
%
% pal = update_log_palette(par, data)
%
% Input:
% ------
%  par - browser
%  data - browser state
%
% Output:
% -------
%  pal - handle of updated palette figure

%----------------------
% HANDLE INPUT
%----------------------

%--
% set figure if needed
%--

if nargin < 1
	par = get_active_browser;
end

%--
% get userdata if needed
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%--
% check for palette to update
%--

pal = get_palette(par, 'Log', data);

if isempty(pal)
	return;
end

%----------------------
% UPDATE PALETTE
%----------------------

%--
% get open log information
%--

if isempty(data.browser.log)
	
	L = {'(No Open Logs)'};
	
	% NOTE: 'popupmenu' value cannot be empty, 'listbox' value should be empty
	
	active = 1;
	
	visible = [];
	
else
	
	L = log_name(data.browser.log);
	
	active = data.browser.log_active;
	
	visible = struct_field(data.browser.log, 'visible');
	
	%--
	% sort the results
	%--
	
	if length(data.browser.log) > 1
		
		[L, ix] = sort(L);
		
		% NOTE: map active log index and visible using permutation
		
		active = find(ix == active);
		
		visible = visible(ix);
		
	end
	
	visible = find(visible);

end

%--
% update active, display and log controls
%--

handles = get_control(pal, 'Active', 'handles');

set(handles.obj, ...
	'string', L, 'value', active ...
);

handles = get_control(pal, 'Display', 'handles');

set(handles.obj, ...
	'string', L, 'value', visible ...
);

handles = get_control(pal, 'Log', 'handles');

set(handles.obj, ...
	'string', L, 'value', active ...
);

%--
% update log option controls
%--

if ~isempty(data.browser.log)
	
	log = data.browser.log(active); % this is the case when the logs are sorted
	
	set_control(pal, 'Color', 'value', rgb_to_color(log.color));
	
	set_control(pal, 'Line Style', 'value', lt_to_linestyle(log.linestyle));
	
	set_control(pal, 'Line Width', 'value', log.linewidth);

	set_control(pal, 'Opacity', 'value', log.patch);
	
else
	
	set_control(pal, 'Color', 'value', 'Red');
	
	set_control(pal, 'Line Style', 'value', 'Solid');
	
	set_control(pal, 'Line Width', 'value', 1);

	set_control(pal, 'Opacity', 'value', 0);
	
end

%--
% enable and disable controls
%--

enable = ~isempty(data.browser.log);

% TODO: there is a function to do this

control = get_palette_property(pal, 'control');

for k = 1:length(control)
	
	if strcmp(control(k).style, 'separator') || strcmp(control(k).style, 'tabs')
		continue;
	end
	
	if ~iscell(control(k).name)
		
		set_control(pal, control(k).name, 'enable', enable);
		
	else
		
		for j = 1:numel(control(k).name)
			set_control(pal, control(k).name{j}, 'enable', enable);
		end
		
	end
	
end

set_control(pal, 'New Log', 'enable', 1);

set_control(pal, 'Open Log', 'enable', 1);

if enable
	set(findobj(pal, 'tag', 'Display', 'style', 'pushbutton'), 'enable', 'off');
end