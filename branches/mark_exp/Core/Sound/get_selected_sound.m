function [sound, lib] = get_selected_sound(pal)

% get_selected_sound - get sound selected in XBAT palette
% -------------------------------------------------------
%
% [sound, lib] = get_selected_sound(pal)
%
% Input:
% ------
%  pal - palette
%
% Output:
% -------
%  sound - sound
%  lib - library containing sound

%--
% initialize output
%--

lib = get_active_library;

%--
% check for XBAT palette
%--

if ~nargin || isempty(pal)
	pal = get_palette(0, 'XBAT');
end

% NOTE: no palette no selection

if isempty(pal)
	sound = []; return;
end

%--
% get selected sounds in XBAT palette
%--

name = get_control(pal, 'Sounds', 'value'); sound = {};

for k = 1:length(name)

	try
		contents = sound_load(lib, name{k}); sound{k} = contents.sound;
	catch
		nice_catch(lasterror, ['Problem loading sound ''', name{k}, '''.']);
	end

end

sound = [sound{:}];

