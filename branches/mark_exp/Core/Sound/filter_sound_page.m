function [page, context] = filter_sound_page(page, context)

% filter_sound_page - filter a sound page using context
% -----------------------------------------------------
%
% [page, context] = filter_sound_page(page, context)
%
% Input:
% ------
%  page - page
%  context - context
%
% Output:
% -------
%  page - page
%  context - context

%---------------------
% HANDLE INPUT
%---------------------

%--
% get filter extension from context
%--

[ext, in] = get_context_extension(context, 'signal_filter');

% NOTE: there is nothing to do if we do not have a proper filter in the context

if isempty(ext)
	return;
end

% NOTE: this is a hack to resolve some problems with the packing of active extensions

ext = unpack_active_extension(ext);

%--
% keep initial page context
%--

% NOTE: we will probably need this in the 'get_page_samples' function

page.context = context;

%---------------------
% GET PAGE SAMPLES
%---------------------

if ~isfield(page, 'buffer')
	
	samples = page.samples; page.buffer = 0;
	
else
	
	if ~page.buffer

		samples = page.samples;

	else

		n1 = size(page.before.samples, 1); n2 = size(page.samples, 1); n3 = size(page.after.samples, 1);

		samples = [page.before.samples; page.samples; page.after.samples];

	end
	
end

%---------------------
% FILTER PAGE
%---------------------

%--
% consider if filter handles multiple channels at once
%--

if ext.multichannel
	
	% NOTE: this makes sure that the context extension is the filter
	
	context.ext = ext;
	
	%--
	% compile parameters
	%--
	
	if ~isempty(ext.fun.parameter.compile)
		
		try
			[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
		catch
			extension_warning(ext, 'Multi-Channel compilation failed.', lasterror);
		end
		
	end
	
	%--
	% filter signal
	%--

	try
		[page.filtered, context] = ext.fun.compute(samples, ext.parameter, context);
	catch
		extension_warning(ext, 'Multi-Channel compute failed.', lasterror);
	end
	
else
	
	base = context; 
	
	% NOTE: this makes sure that the context extension is the filter
	
	context.ext = ext;
	
	page.filtered = [];
	
	if isempty(page.channels)
		page.channels = 1:size(page.samples, 2);
	end
	
	for k = 1:numel(page.channels)
		
		%--
		% set channel context
		%-- 
		
		channel = page.channels(k);
		
		context.page = get_channel_page(page, channel);
		
		% NOTE: we get channel state if available from base context store
		
		context.ext.state = get_channel_state(base, ext, channel);
		
		%--
		% compile parameters
		%--

		if ~isempty(ext.fun.parameter.compile)
			
			try
				[ext.parameter, context] = ext.fun.parameter.compile(ext.parameter, context);
			catch
				extension_warning(ext, ['Channel ', int2str(channel), ' compilation failed.'], lasterror);
			end

		end

		%--
		% filter signal
		%--

		try
			
			[page.filtered(:, k), context] = ext.fun.compute(samples(:, k), ext.parameter, context);
			
		catch
			
			extension_warning(ext, ['Channel ', int2str(channel), ' compute failed.'], lasterror); 
		
			% NOTE: if filter fails clear filtered, flush state, and return
			
			page.filtered = []; context.ext.state = struct; return;
		
		end

		%--
		% store channel state
		%--
		
% 		base = set_channel_state(base, ext, channel, context.ext.state);
		
		switch in

			case 'main'

				base.ext.state(k).channel = channel;

				base.ext.state(k).state = context.ext.state;

			case 'active'

				base.active.signal_filter.state(k).channel = channel;

				base.active.signal_filter.state(k).state = context.ext.state;

		end

	end
	
	context = base;
	
end

%------------------------------------
% SEPARATE BUFFER
%------------------------------------

if page.buffer 
	
	page.before.filtered = page.filtered(1:n1, :);
	
	page.after.filtered = page.filtered(n1 + n2 + 1:end, :);
	
	page.filtered = page.filtered(n1 + 1:n1 + n2, :);
	
% 	t1 = isequal(size(page.samples), size(page.filtered))
% 	
% 	t2 = isequal(size(page.before.samples), size(page.before.filtered))
% 	
% 	t3 = isequal(size(page.after.samples), size(page.after.filtered))
	
end


%------------------------------------
% SET_CHANNEL_STATE
%------------------------------------

function context = set_channel_state(context, ext, channel, state)

% set_channel_state - store extension state for channel in context
% ----------------------------------------------------------------
%
% context = set_channel_state(context, ext, channel, state)
%
% Input:
% ------
%  context - store context
%  ext - extension
%  channel - channel
%  state - state
%
% Output:
% -------
%  context - updated store context

%--
% get extension location in context
%--

type = ext.subtype;

[ext, in] = get_context_extension(context, type);

%--
% store channel state with proper extension in context
%--

% TODO: clear existing channel context if needed

switch in
	
	case 'main'

		context.ext.state(end + 1).channel = channel;

		context.ext.state(end).state = state;

	case 'active'

		context.active.(type).state(end + 1).channel = channel;

		context.active.(type).state(end).state = state;

end





