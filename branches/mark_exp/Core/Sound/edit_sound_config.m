function sound = edit_sound_config(sound, lib)

% edit_sound_config - sound configuration editing
% -----------------------------------------------
%
% sound = edit_sound_config(sound, lib)
%
% Input:
% ------
%  sound - sound to edit
%  lib - parent library (def: active library)
%
% Output:
% -------
%  sound - edited sound

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default active library
%--

if nargin < 2
	lib = get_active_library;
end

%--
% check if we can configure
%--

% NOTE: we can only configure a single closed sound

if (numel(sound) ~= 1) || sound_is_open(sound)
	return;
end

%--
% update sound attributes
%--

sound = sound_attribute_update(sound, lib);

%-------------------
% EDIT SOUND
%-------------------

%--
% present edit dialog
%--

out = sound_config_dialog(sound);

values = out.values;

if isempty(values)
	return;
end

% NOTE: this was required when some dialog callbacks updated the sound

out = sound_load(lib, sound_name(sound)); sound = out.sound;

%--
% update sound
%--

sound.output.class = lower(values.class{1});

if values.resample && (sound.samplerate ~= values.samplerate)
	sound.output.rate = values.samplerate;
else
	sound.output.rate = [];
end

% NOTE: this code was used when time-stamps were edited through this dialog

if isfield(values, 'enable_time_stamps')
	sound.time_stamp.enable = values.enable_time_stamps;
end

if isfield(values, 'collapse_sessions')
	sound.time_stamp.collapse = values.collapse_sessions;
end

sound = set_tags(sound, str_to_tags(values.tags));

sound.notes = values.notes;
	
%--
% discard potentially problematic parts of view frequency range
%--

sound.view.page.freq = []; sound.view.grid.freq.spacing = [];

%--
% save sound in library
%--

% NOTE: we may consider discarding parts of the state

sound_save(lib, sound, out.state);
