function [time, duration] = get_file_times(sound, file)

% get_file_times - get file related times
% ---------------------------------------
%
% [time, duration] = get_file_times(sound, file)
%
% Input:
% ------
%  sound - sound
%  file - sound file
%
% Output:
% -------
%  time - file start sound time
%  duration - file duration in sound

%---------------------
% HANDLE INPUT
%---------------------

%--
% check file input and handle multiple file input recursively
%--

if nargin > 1
	
	if ~ischar(file) && ~iscellstr(file)
		error('File input must be a string or string cell array.');
	end
	
	if iscell(file) && numel(file) > 1	
		[time, duration] = iteraten(mfilename, 2, sound, file); return;
	end
	
end

%--
% single sound files are still special
%--

% NOTE: this may not be the best test

if ischar(sound.file)
	time = 0; duration = get_sound_duration(sound); return;
end 

%---------------------
% GET FILE TIMES
%---------------------

%--
% get file boundary times in REAL TIME, this means consider sessions if required
%--

% NOTE: use native sound samplerate because we are using native samples.

time = [0; sound.cumulative(1:end)] / sound.samplerate;

time = map_time(sound, 'real', 'record', time);

%--
% get file durations
%--

duration = diff(time); time(end) = [];

% NOTE: return if there is no selection

if nargin < 2
	return;
end

%--
% select input file start time
%--

ix = get_file_index_from_name(sound, file); 

if isempty(ix) || isnan(ix)
	error('Input file is not part of sound files.');
end

time = time(ix); duration = duration(ix);



