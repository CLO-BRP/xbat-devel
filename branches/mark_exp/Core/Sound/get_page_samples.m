function [samples, page] = get_page_samples(page, start, duration, channels)

% get_page_samples - get samples from page 
% ----------------------------------------
%
% samples = get_page_samples(page, start, duration, channels)
%
% Input:
% ------
%  page - page
%  start - start time
%  duration - page duration
%  channels - page channels
%
% Output:
% -------
%  samples - samples

% NOTE: this function can be used to select page data or to add page data

% NOTE: we are not in a position to take advantage of the updated page, disregard for now

%------------------------
% HANDLE INPUT
%------------------------

%--
% handle trivial call to get current page samples
%--

% NOTE: we get filtered samples if they are available, consider non-filtered option

if nargin < 2
	
	if isempty(page.filtered)
		samples = page.samples;
	else
		samples = page.filtered;
	end
	
end

%--
% consider channels input
%--

% NOTE: no change requested in available channels

if nargin < 4 || isempty(channels)
	
	channels = page.channels;
	
else
	
	% NOTE: validate channels requested and check for new channels

	if ~isempty(setdiff(channels, 1:sound.channels))
		error('Requested channels are not available in sound.');
	end

	new_channels = setdiff(channels, page.channels);
	
end

%--
% consider duration and start
%--

if nargin < 3 || isempty(duration)
	duration = page.duration;
end

duration_change = duration - page.duration;

if isempty(start)
	start = page.start;
end

start_change = start - page.start;

%------------------------
% GET PAGE SAMPLES
%------------------------

% NOTE: what remains is not trivial but will be very useful

% NOTE: we probably have to filter the page when we modify it




