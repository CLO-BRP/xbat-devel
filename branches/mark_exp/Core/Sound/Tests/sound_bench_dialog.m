function result = sound_bench_dialog

% sound_bench_dialog - run sound file read and write benchmarks

%--
% create controls
%--

control = header_control('Sound', 0, 'space', .11);

tabs = {'File', 'Sound'};

control(end + 1) = tabs_control(tabs);

% TODO: there is a problem using negative space in the layout here

control(end + 1) = checkbox_control('read', 1, 'tab', tabs{1});

control(end + 1) = checkbox_control('write', 0, 'tab', tabs{1});

control(end + 1) = control_separator([], [], 'tab', tabs{1});

rates = iterate(@int2str, [2000, 8000, 16000, 24000, 32000, 44100, 48000, 88200, 96000]); value = 6;

control(end + 1) = control_create( ...
	'name', 'rate', ...
	'tab', tabs{1}, ...
	'style', 'popup', ...
	'string', rates, ...
	'value', value ...
);

control(end + 1) = control_create( ...
	'name', 'channels', ...
	'tab', tabs{1}, ...
	'style', 'slider', ...
	'type', 'integer', ...
	'max', 16, ...
	'value', 1 ...
);

durations = strcat(iterate(@int2str, [1, 5, 10, 20, 30, 60]), 'M'); value = 1;

control(end + 1) = control_create( ...
	'name', 'duration', ...
	'tab', tabs{1}, ...
	'style', 'popup', ...
	'type', 'integer', ...
	'string', durations, ...
	'value', value ...
);

formats = get_writeable_formats; names = {formats.name};

control(end + 1) = control_create( ...
	'name', 'format', ...
	'tab', tabs{1}, ...
	'style', 'listbox', ...
	'string', names, ...
	'lines', 6, ...
	'min', 0, ...
	'max', 2, ...
	'value', 1 ...
);

%--
% create dialog
%--

opt = dialog_group; opt.width = 14;

out = dialog_group('Bench ...', control, opt, @callback);

if isempty(out.values)
	result = struct; return;
end

%--
% configure and run performance test
%--

opt = read_compare;

% NOTE: we will evaluate one rate, channel, duration combination

opt.rate = eval(cellfree(out.values.rate));

opt.channels = out.values.channels; 

% NOTE: this duration is in minutes

duration = cellfree(out.values.duration); duration = eval(duration(1:end - 1));

opt.length = opt.rate * 60 * duration;

% NOTE: we may evaluate various formats at the same time, possibly all implementations

opt.formats = out.values.format;

opt.compare = opt.functions;

result = read_compare([], opt);


%--------------------------------------------
% CALLBACK FUNCTION
%--------------------------------------------

function callback(obj, eventdata)


callback = get_callback_context(obj, 'pack');


switch callback.control.name
	
	case 'formats'
		
end

	