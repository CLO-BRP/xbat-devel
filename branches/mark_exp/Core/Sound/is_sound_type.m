function [value,type] = is_sound_type(type)

% is_sound_type - check and normalize sound type string
% -----------------------------------------------------
%
% [value,type] = is_sound_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - valid type indicator
%  type - normalized type string

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% check type
%--

if ~ischar(type)
	value = 0; type = ''; return;
end

%--
% add some type string flexibility
%--

type = lower(strtrim(type));

%----------------------------
% CHECK AND NORMALIZE
%----------------------------

types = sound_types;

ix = find(strcmp(types,type), 1);

if isempty(ix)
	value = 0; type = ''; return;
end

value = 1;