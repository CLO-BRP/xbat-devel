function sound = consolidate_sound(sound, lib, root)

% consolidate_sound - copy sound data files into library
% ------------------------------------------------------
%
% sound = consolidate_sound(sound, lib, root)
%
% Input:
% ------
%  sound - sound
%  lib - library
%  root - sound .mat-file location
%
% Output:
% -------
%  sound - consolidated sound

%-------------------
% HANDLE INPUT
%-------------------

%--
% set active library default
%--

if nargin < 2
    lib = get_active_library;
end

%--
% set default root in active library
%--

if nargin < 3 || isempty(root)
    root = library_sound_root(sound, lib);
end

%-------------------
% CONSOLIDATE
%-------------------

%--
% create directory for sound data
%--

% NOTE: add sound name to data path for file streams, to keep 'sound_name' functional

if strcmpi(sound.type, 'file stream')
	data = fullfile(root, 'Data', sound_name(sound));
else
	data = fullfile(root, 'Data');
end

%--
% try to create the directory, error if we can't
%--

data = create_dir(data);

if isempty(data)
   error('Failed to create data directory.');
end

if data(end) ~= filesep
	data(end + 1) = filesep;
end

%--
% copy files
%--

switch lower(sound.type)

	case 'file stream', source = sound.path;

	case 'file', source = fullfile(sound.path, sound.file);

end

[success, message, id] = copyfile(source, data);

% TODO: this message displays when adding a 'Digest' generated sound to a library

if ~success && ~strcmp(id, 'MATLAB:COPYFILE:SourceAndDestinationSame')
	
	disp(['XBAT encountered a problem copying files.  ', message]);
	
end

%--
% modify sound
%--

sound.path = data; sound.consolidated = 1;

%--
% save sound if no outputs requested
%--

if ~nargout
    sound_save(lib, sound, get_sound_state(sound, lib));
end

