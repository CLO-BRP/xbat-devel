function page = read_sound_page(sound, page, channels, buffer)

% read_sound_page - read scan page from sound
% -------------------------------------------
%
% page = read_sound_page(sound, page, channels, buffer)
%
% Input:
% ------
%  sound - sound
%  page - scan page
%  channels - channels
%  buffer - number of pages to buffer before and after
%
% Output:
% -------
%  page - sound page (contains samples and channels information)

%-------------------
% HANDLE INPUT
%-------------------

%--
% set no buffer default
%--

if nargin < 4
	buffer = 0;
end

%--
% set default to read all channels
%--

if nargin < 3
	channels = [];
end

%-------------------
% READ SOUND PAGE
%-------------------

% TODO: this will facilitate the creation of a 'get_page_samples' function

% NOTE: we will probably want fields to store buffered start and duration

page.sound = sound;

%--
% add channels to page
%--

page.channels = channels;

%--
% read page samples and add to page, also add sample rate
%--

% NOTE: we need to map into slider time because that's how sound_read works

start = map_time(sound, 'slider', 'real', page.start);

page.samples = sound_read(sound, 'time', start, page.duration, page.channels);

page.filtered = [];

page.rate = get_sound_rate(sound);

%--
% add buffer fields and possibly get buffer data
%--

page.buffer = buffer;

page.before.samples = [];

page.before.filtered = [];

page.after.samples = [];

page.after.filtered = [];

if buffer
	
	% NOTE: the buffer is a time duration in seconds, we also guarantee a minimal buffer
	
	pad = buffer;
	
	if pad * page.rate < 4096
		pad = 4096 / page.rate;
	end
	
	page.before.samples = sound_read(sound, 'time', start - pad, pad, page.channels);
	
	page.after.samples = sound_read(sound, 'time', start + page.duration, pad, page.channels);	
	
end

