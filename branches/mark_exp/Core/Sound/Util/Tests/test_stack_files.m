function test_stack_files

%--
% set up file list
%--

% NOTE: we are describing the source

source = { ...
	'F:\031017021341.pjt', ...
	'F:\031017052005.pjt', ...
	'F:\031017082629.pjt', ...
	'F:\031017113253.pjt' ...
};

name = {'1.wav', '2.wav'};

% NOTE: we are describing the output

destination = 'G:\Ping4ch'; prefix = 'pray';

%--
% stack file
%--

serial = 1;

for k = 1:numel(source)
	out = fullfile(destination, prefix);
	
	files = strcat(source{k}, filesep, name);
	
	[result, serial] = stack_files(out, 'wav', 300, serial, files{:}); 
end