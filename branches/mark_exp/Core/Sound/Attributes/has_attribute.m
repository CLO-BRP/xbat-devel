function value = has_attribute(sound, name)

% has_attribute - indicate whether sound has attribute
% ----------------------------------------------------
%
% value = has_attribute(sound, name)
%
% Input:
% ------
%  sound - sound
%  name - attribute name
%
% Output:
% -------
%  value - indicator

% NOTE: this read call produces a prototype attribute for those available, otherwise it is empty

value = ~isempty(attribute_read(sound, name));
