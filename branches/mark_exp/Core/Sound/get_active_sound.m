function [sound, lib] = get_active_sound(par, pal)

% get_active_sound - get the active sound
% ---------------------------------------
%
% [sound, lib] = get_active_sound(par, pal)
%
% Input:
% ------
%  par - browser handle (def: active browser)
%  pal - palette handle
%
% Output:
% -------
%  sound - sound
%  lib - parent library

%--
% handle input
%--

if nargin < 1 || isempty(par)
	par = get_active_browser;
end

% NOTE: we don't really need this input

if nargin < 2 || isempty(pal)
	pal = get_palette(0, 'XBAT');
end

%--
% get sound and library from open browser
%--

if ~isempty(par)
	
	sound = get_browser(par, 'sound'); info = parse_browser_tag(get(par, 'tag'));
	
	[ignore, libname] = strtok(info.library, filesep); libname = libname(2:end);   
	
	lib = get_libraries(get_users('name', info.user), 'name', libname);
	
	return;

end

%--
% otherwise, get sound from xbat palette if possible
%--

% NOTE: this is some definition of the root browser sound

% NOTE: it seems we should return multiple sounds if selection is multiple

[sound, lib] = get_selected_sound(pal);

if length(sound) > 1
	sound = [];
end





	
	
	
