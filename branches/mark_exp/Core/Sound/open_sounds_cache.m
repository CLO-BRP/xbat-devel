function tags = open_sounds_cache(tag)

% open_sounds_cache - a cache of opening sound browser tags
% ---------------------------------------------------------
%
% tags = open_sounds_cache(tag)
%
% Input:
% ------
%  tag - browser tag(s), if a cell array, sets the entire cache.
%
% Output:
% -------
%  tags - the current contents of the cache

persistent OPENING_SOUNDS;

if isempty(OPENING_SOUNDS)
	OPENING_SOUNDS = {};
end

if ~nargin
	tag = [];
end

if ischar(tag)
	OPENING_SOUNDS = union(OPENING_SOUNDS, tag);
end

if iscell(tag)
	OPENING_SOUNDS = tag;
end

tags = OPENING_SOUNDS;
