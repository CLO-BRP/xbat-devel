function result = page_through_sound(sound, duration, overlap, callback)

%--
% handle input
%--

if nargin < 4
	callback = @echo_page;
end

if nargin < 3
	overlap = 0;
end

if nargin < 2
	duration = min(get_sound_duration(sound), 30);
end

%--
% page through sound
%--

result = struct;

scan = get_sound_scan(sound, duration, overlap);

result.page = empty(get_scan_page(scan));

while 1
	
	[page, scan] = get_scan_page(scan);

	if isempty(page)
		break;
	end
	
	result.page(end + 1) = page;

	page = read_sound_page(sound, page);

	if ~isempty(callback)
		
		if isfield(result, 'result')
			result.result{end + 1} = eval_callback(callback, page);
		else
			result.result{1} = eval_callback(callback, page);
		end
		
	end
	
end


%--------------------
% ECHO_PAGE
%--------------------

% TODO: consider callback signature

function result = echo_page(page)

result = struct
