function info = sound_file_info(file, fields, cache)

% sound_file_info - get sound file information
% --------------------------------------------
%
% info = sound_file_info(file, fields, cache)
%
% Input:
% ------
%  file - full filename or many
%  fields - info fields to retrieve
%  cache - use cache indicator
%
% Output:
% -------
%  info - normalized sound file info

% TODO: add source of info indicator to output

%--------------------
% HANDLE INPUT
%--------------------

if nargin < 3
	cache = 0;
end

%--
% set all fields default
%--

% NOTE: an empty fields input indicates we should get all

if nargin < 2 || isempty(fields)
	fields = {'*'};
end

%--
% handle multiple files
%--

if iscell(file)
	
	%-------------------------
	% GET INFO FROM FILES
	%-------------------------
	
	% NOTE: handling multiple files when not hitting the cache is an independent operation
	
	if ~cache
		info = iterate(mfilename, file, fields, cache, '__WAITBAR__'); return;
	end
	
	%-------------------------
	% GET INFO FROM CACHE
	%-------------------------
	
	% NOTE: speed gains are most likely when getting info for many files
	
	%--
	% check cache for available info
	%--
	
	% NOTE: if we are not getting all fields, ensure we get 'file', it should not matter that we output this field as well
	
	if ~isequal(fields, {'*'})
		fields = union('file', fields);
	end
	
	part = sound_file_info_cache(file, fields);
	
	%--
	% all info was in cache, we are done!
	%--
	
	% TODO: consider the possibility that the retrieved results are larger, and cleanup cache
	
	if numel(part) == numel(file) % NOTE: this assumes that files are unique
		info = part; return;
	end
	
	%--
	% no info was in cache, get it all from filesystem
	%--
	
	if isempty(part)
		info = iterate(mfilename, file, fields, 0, '__WAITBAR__'); return;
	end
	
	%--
	% partial info was in cache, get remaining info from filesystem
	%--
	
	% NOTE: we first get the missing info from the filesystem
	
	missing = setdiff(file, {part.file});
	
	if isempty(missing)
		append = [];
	else
		append = iterate(mfilename, missing, fields, 0, '__WAITBAR__');
	end
		
	% NOTE: then we interleave the arrays, this is done by appending and permuting
	
	info = [part(:); append(:)];
	
	[ignore, pf] = sort(file); [ignore, pi] = sort({info.file});
	
	% NOTE: the info permutation sorts the right side, the files permutation ensures file order
	
	info(pf) = info(pi);
	
	return;
	
end
		
%--------------------
% GET FORMAT INFO
%--------------------

%--
% get full filename and filesystem info
%--

in = file;
	
% NOTE: try to get full filename, rollback if it fails, in this case the cache will have some garbage

if no_filesep(file)

	file = which(file);
	
	if isempty(file)
		file = in;
	end

end

sys = dir(file);

%--
% check cache for sound file info
%--

% TODO: use the file modification date during cache retrieval

if cache
	
	info = sound_file_info_cache(file, fields);

	% NOTE: we happily return with info from the cache
	
	if ~isempty(info)
		return;
	end

end

%--
% get format handler and get file info if needed
%--

format = get_file_format(file);

% TODO: evaluate whether error handling makes sense

try
	
	info = format.info(file); failed = 0;
	
catch

	failed = 1; str = ['Failed to get sound file info from ''', file, '''.'];
	
	% HACK: this is related to the adaptation of OLD formats to EXTENSION formats
	
	if isfield(format, 'subtype')
		extension_warning(format, str, lasterror);
	else 
		nice_catch(lasterror, str);
	end 
	
end

%--------------------
% NORMALIZE INFO
%--------------------

% NOTE: we choose the first file extension associated with format as name

% HACK: this is a hack related to the adaptation of OLD formats to EXTENSION formats

% NOTE: to understand this look at 'get_file_format', it should be clear

if isfield(format, 'ext')
	info.format = format.ext{1};
else
	info.format = format.name;
end 

% NOTE: the only requirement for this to work is that info fields are named the same

ninfo = info_create;

ninfo_field = fieldnames(ninfo);

info_field = fieldnames(info);

%--
% copy common fields from info to normalized info
%--

% TODO: consider making this type of copy a function

for k = 1:length(info_field)
	
	if ~string_is_member(info_field{k}, ninfo_field)
		continue;
	end
	
	ninfo.(info_field{k}) = info.(info_field{k});
		
end

%--
% copy format specific fields into the 'info' output field
%--

% NOTE: these fields are not really used

% format_field = setdiff(info_field, ninfo_field);
% 
% if ~isempty(format_field)
% 	
% 	for k = 1:length(format_field)
% 		ninfo.info.(format_field{k}) = info.(format_field{k});
% 	end
% 	
% end

%--
% output standardized info structure
%--

info = ninfo;

% NOTE: pack system information as well, this helps caching

info.file = file;

% NOTE: the name can be used for 'loose' matching in cache using only name!

info.name = sys.name;

info.date = isodate(sys.date);

info.bytes = sys.bytes;

%--------------------
% UPDATE CACHE
%--------------------

% NOTE: we don't want to store bad sound file info into cache

% NOTE: we also want to cache, whenever caching was requested or output was not

% the last idea was too much trouble in the code

if ~failed && cache % && (cache || ~nargout)
	
	sound_file_info_cache(info);

end

%--------------------
% SELECT FIELDS
%--------------------

% NOTE: provided we are not asking for all fields, determine fields to remove and remove

if ~isequal(fields, {'*'})
	
	remove = setdiff(fieldnames(info), fields);
	
	if ~isempty(remove)
		info = rmfield(info, remove);
	end
	
end


%---------------------------
% NO_FILESEP 
%---------------------------

function value = no_filesep(str)

value = isempty(find(str == filesep, 1));

