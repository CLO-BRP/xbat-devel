function format = format_aac

% format_aac - create format structure
% -----------------------------------
%
% format = format_aac
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% create format structure
%--

format = format_create;

%--
% fill format structure (set)
%--

format.name = 'Advanced Audio Coding (AAC)';

format.ext = {'aac', 'mp4', 'm4a', 'm4b'};

format.home = 'http://www.audiocoding.com/';

format.info = [];

format.read = [];

format.write = [];

format.seek = 0;

format.compression = 1;


