function ext = get_formats_ext(format)

% get_formats_ext - get extensions from formats
% ---------------------------------------------
%
% ext = get_formats_ext(format)
%
% Input:
% ------
%  format - format array
%
% Output:
% -------
%  ext - cell array of strings containing file extensions

%-------------------------------------------------
% HANDLE INPUT
%-------------------------------------------------

%--
% set default formats
%--

if (nargin < 1) || isempty(format)
	format = get_formats;
end

%-------------------------------------------------
% PACK EXTENSIONS
%-------------------------------------------------

% NOTE: all we do is pack all the strings in a single cell array

ext = cell(0);

for k = 1:length(format)
	ext = {ext{:}, format(k).ext{:}};
end

ext = lower(ext);
