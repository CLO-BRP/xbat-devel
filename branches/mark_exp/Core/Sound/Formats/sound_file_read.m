function [X, r] = sound_file_read(f, ix, n, N, ch, opt)

% sound_file_read - read samples from sound file
% ----------------------------------------------
%
% [X, r] = sound_file_read(f, ix, n, N, ch, opt)
%
% opt = sound_file_read
%
% Input:
% ------
%  f - file name or info
%  ix - initial sample
%  n - number of samples
%  N - total samples in file
%  ch - channels to select
%  opt - conversion options
%
% Output:
% -------
%  X - samples from sound file
%  r - sample rate
%  opt - default conversion options
%
% See also: sound_file_write, sound_file_encode

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% output default conversion options structure
%--

% TODO: these options don't belong here

if ~nargin
	opt.rate = []; opt.class = []; X = opt; return;
end

%--
% handle variable type file info input
%--

switch class(f)

	%--
	% file name
	%--
	
	case 'char'
		
		% NOTE: we create fake info struct with file name
		
		info = info_create; info.file = f;
	
	%--
	% file info struct
	%--
	
	case 'struct'
		
		% NOTE: input is file info structure
		
		if ~isfield(f, 'file')
			error('Input file struct does not have ''file'' field.');
		end
		
		info = f; f = info.file;
	
	%--
	% error
	%--
	
	otherwise, error('Input must be a file name or info struct.');
		
end

%--
% set default modification options
%--

% NOTE: use simple empty for no conversion options

if nargin < 6
	opt = [];
end

%--
% set default channels if none given
%--

if nargin < 5
	ch = [];
end

%--
% set total samples in file if needed
%--

if (nargin < 4) || isempty(N)
	
	% NOTE: this should be a rare case that causes some ugly code, reconsider
	
	if isempty(info.samples)
		
		try
			
			info = sound_file_info(f);
			
		catch
			
			if ~exist('ch', 'var') || ~exist('n', 'var') || isempty(ch) || isempty(n)
				error('File is unavailable and read specification is insufficient.');
			end

			X = dump_zeros(n, ch, f);
			
		end
		
	end

	N = info.samples;

end

%--
% set default start and duration if needed
%--

if (nargin < 2) || isempty(ix)
	ix = 0;
end

if (nargin < 3) || isempty(n)	
	n = N - ix;
else	
	if (ix + n) > N
		error('Required samples exceed end of sound file.');
	end
end

%----------------------------------
% READ SAMPLES FROM FILE
%----------------------------------

%--
% try to get format handler
%--

format = get_file_format(f);

%--
% read file using handler
%--

% TODO: the format reader convention used to be that all channels were read on empty channel descriptor

if isempty(ch)
	ch = 1:info.channels;
end

try
	
	[X, opt] = format.read(info, ix, n, ch, opt);

catch
	
	nice_catch(lasterror);
	
	% NOTE: in this case the file is unavailable and we can't generate zeros

	if ~exist('ch', 'var') || ~exist('n', 'var') || isempty(ch) || isempty(n)
		error('File is unavailable and read specification is insufficient.');
	end

	X = dump_zeros(n, ch, f);

end

%----------------------------------
% GET SAMPLERATE
%----------------------------------

%--
% get samplerate from file info if needed
%--

% NOTE: we get this if output is requested or we need it for resample

if (nargout > 1) || (~isempty(opt) && ~isempty(opt.rate))
	
	%--
	% get file rate
	%--
	
	% NOTE: we may have file info already if total samples were not input
	
	if ~exist('info', 'var') || isempty(info.samplerate)
		info = sound_file_info(f);
	end
	
	r_file = info.samplerate;
	
	%--
	% return output rate
	%--
	
	% NOTE: return converted rate if conversion was peformed
	
	if isempty(opt)
		r = r_file;
	else
		r = opt.rate;
	end
	
end

%----------------------------------
% HANDLE CONVERSION REQUESTS
%----------------------------------

% NOTE: we think of the options fields as conversion requests

if ~isempty(opt)
	
	%--
	% resample to conversion rate
	%--
	
	if ~isempty(opt.rate)
		ratio = opt.rate / r_file; [p, q] = rat(ratio); X = resample(X, p, q);
	end
	
	%--
	% cast to conversion class
	%--
	
	% NOTE: create and apply class casting function handle
	
	if ~isempty(opt.class)
		fun = str2func(opt.class); X = fun(X);
	end
	
end


%------------------------------------------------------
% DUMP_ZEROS
%------------------------------------------------------

function X = dump_zeros(n, ch, f)

% NOTE: this is a non-disruptive way of handling missing files

X = zeros(n, length(ch));

%--
% produce fast visible warning of zeros
%--

disp(' ');
disp(['WARNING: Padding with zeros. Reading from ''' f ''' failed.']);


