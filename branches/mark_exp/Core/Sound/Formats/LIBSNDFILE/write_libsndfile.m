function out = write_libsndfile(file, samples, rate, opt)

% write_libsndfile - write sound samples to file
% ----------------------------------------------
%
%  opt = write_libsndfile(file)
%
% flag = write_libsndfile(file, samples, rate, opt)
%
% Input:
% ------
%  file - file
%  samples - samples to write
%  rate - rate
%  opt - format specific write options
%
% Output:
% -------
%  opt - format specific write options
%  flag - success flag

%----------------------------
% CREATE PERSISTENT TABLES
%----------------------------

% TODO: factor this so that we can use it elsewhere, for example when building interfaces

persistent FORMAT_TABLE ENCODING_TABLE;

if isempty(FORMAT_TABLE)
	
	% NOTE: the order of the formats and encodings is taken from 'sndfile.h'
	
	% TODO: generate using mex or provide as mex input
	
	%--
	% set format table
	%--
	
	FORMAT_TABLE = { ...
		'WAV',	0; ...
		'AIF',	1; ...
		'AU',	2; ...
		'W64',	3; ...
		'FLA',  4; ...
		'FLAC', 4 ...
	};

	%--
	% set encoding table
	%--
	
	ENCODING_TABLE = { ...
		'PCM_U8',		0; ...
		'PCM_S8',		1; ...
		'PCM_16',		2; ...
		'PCM_24',		3; ...
		'PCM_32',		4; ...
		'FLOAT',		5; ... 
		'DOUBLE',		6; ...
		'ULAW',			7; ...
		'ALAW',			8; ...
		'IMA_ADPCM',	9; ...
		'MS_ADPCM',		10; ...
		'GSM610',		11 ...
	};

end

%----------------------------
% HANDLE INPUT
%----------------------------

% TODO: consider factoring this, it would get called in 'parameter__create'

if (nargin < 4) || trivial(opt)
	
	%--
	% set default encoding options based on file extension 
	%--
		
	[ignore, ext] = file_ext(file);

	ext = upper(ext);
	
	% TODO: replace with 'ismember'? or is this faster
	
	if ~isempty(find(strcmpi(ext, FORMAT_TABLE(:, 1)), 1))
		opt.format = upper(ext);
	else
		error(['Unsupported file extension ''', ext, '''.']);
	end
		
	%--
	% set default write encoding
	%--
	
	opt.encoding = 'PCM_16'; opt.append = 1;
	
end

%--
% output option structure if needed
%--
	
if nargin == 1
	out = opt; return;
end

%------------------------------------------
% WRITE FILE
%------------------------------------------

%--
% get format code from table
%--

ix = find(strcmp(opt.format, FORMAT_TABLE(:, 1)));

if isempty(ix)
	error(['Unsupported format ''' opt.format '''.']);
end

format = FORMAT_TABLE{ix, 2};

%--
% get encoding code from table
%--

ix = find(strcmp(opt.encoding, ENCODING_TABLE(:, 1)));

if isempty(ix)
	error(['Unsupported encoding ''' opt.encoding '''.']);
end

encoding = ENCODING_TABLE{ix, 2};

%--
% convert file string to C string
%--

% TODO: consider if this needs to be platform-specific

% NOTE: this string replacement handles windows network files

file = strrep(file, '\\', '??');

file = strrep(file, '\', '\\');

file = strrep(file, '??', '\\');

%--
% write file using mex
%--

sound_write_(samples', rate, file, format, encoding, opt.append); % note the 'samples' transpose

%--
% report on operation
%--

% TODO: get written file info

% NOTE: output success flag

out = 1;
