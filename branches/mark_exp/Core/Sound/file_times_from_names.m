function time = file_times_from_names(name, pat)

% file_times_from_names - get file times from specially encoded file names
% ------------------------------------------------------------------------
%
% time = file_times_from_names(name)
%
% Input:
% ------
%  name - single name string or cell array of strings
%  
% Output:
% -------
%  time - time (in seconds) of file start times

%--
% set default pattern if none given
%--

if nargin < 2
	pat = 'yyyymmdd_HHMMSS';
end

%--
% handle cell array input
%--

time = [];

if iscell(name)
	
	time = {};
	
	for k = 1:numel(name)
		
		time{end + 1} = file_times_from_names(name{k}, pat);
		
		if isempty(time{end})
			break;
		end
		
	end
	
	time = [time{:}];
	
	%--
	% subtract off minimum time
	%--
	
	time = time - min(time);
	
	return;
	
end

%--
% compute the time in seconds using file_datenum
%--

time = file_datenum(name, pat);

if isempty(time)
	return;
end

% NOTE: Get time in seconds:  there are 86400 seconds in a day

time = time * 86400;






