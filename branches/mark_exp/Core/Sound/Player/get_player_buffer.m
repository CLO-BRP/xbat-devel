function [buffer, player] = get_player_buffer

% get_player_buffer - access samples from current player
% ------------------------------------------------------
%
% [buffer, player] = get_player_buffer
%
% Output:
% -------
%  buffer - player buffer struct
%  player - current player

% NOTE: consider renaming to 'get_play_buffer' and adding parent input

%--
% get player
%--

player = get_current_player;

if isempty(player)
	buffer = []; return;
end

%--
% get player data
%--

data = get(player, 'userdata');

%--
% pack buffer
%--

% NOTE: the buffer is becoming the data

buffer.channels = data.ch;

buffer.rate = data.samplerate;

buffer.speed = data.speed;

buffer.samples = data.buffer;

buffer.length = length(buffer.samples);

buffer.ix = get(player, 'currentsample');


