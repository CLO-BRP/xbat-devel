function  ao = sound_play(sound, adaptor, ix, n)

if isempty(sound)
    ao = []; return;
end

if nargin < 4
    n = sound.cumulative(end);
end

if nargin < 3 || isempty(ix)
    ix = 1;
end

if nargin < 2 || isempty(adaptor)
    adaptor = 'nidaq';
end

sound.output.rate = [];

%--
% setup analog output object
%--

ao = analog_output(sound, ix, n, adaptor); 

if isempty(ao)
    return;
end

%--
% start playing
%--

start(ao);


%--------------------------------------------
% ananlog_ouput
%--------------------------------------------

function ao = analog_output(sound, ix, n, adaptor)

%--
% create (of find) analog output object
%--

ao = daqfind('tag', 'daq_player');

if isempty(ao)

    info = daqhwinfo;

    adaptors = intersect(adaptor, info.InstalledAdaptors);

    if isempty(adaptors)
        ao = []; return;
    end
    
    info = daqhwinfo(adaptors{1});

    ao = analogoutput(adaptors{1}, info.InstalledBoardIds{end});

    set(ao, 'tag', 'daq_player');

else
    
    ao = ao{1}; delete(ao.channel);

end

%--
% add channels
%--

info = daqhwinfo(ao);

ch = min(sound.channels, info.TotalChannels);
    
addchannel(ao,(1:ch) - 1);

if ch < sound.channels
    disp('Warning: hardware does not support the full number of channels.');
end

%--
% set up sample rate
%--

info = propinfo(ao, 'samplerate');

rate = clip_to_range(sound.samplerate, info.ConstraintValue);

try
    rate = setverify(ao, 'SampleRate', rate);
end
   
userdata.rate = [];

if rate ~= sound.samplerate
    userdata.rate = rate; disp('Warning: data is being resampled to play at proper speed.');
end

%--
% setup queue
%--

buffer_length = 40000;

set(ao, ...
    'SamplesOutputFcnCount', 20000, ...
    'SamplesOutputFcn', {@reload_buffer, sound, buffer_length, n, 1:ch} ...
);

try
    set(ao, 'StandardSampleRates', 'on');
end

%--
% store some important information
%--

userdata.ix = 1;

set(ao, 'userdata', userdata);

%--
% initialize buffer
%--

reload_buffer(ao, [], sound, buffer_length, n, 1:ch);


%-----------------------------
% reload_buffer
%-----------------------------

function reload_buffer(ao, eventdata, sound, buffer_length, n, ch)

available = get(ao, 'SamplesAvailable');

userdata = get(ao, 'UserData'); ix = userdata.ix;

n_read = min(n - ix, buffer_length - available);

if n_read <= 0
    return;
end

buffer = sound_read(sound, 'samples', ix, n_read, ch);

if ~isempty(userdata.rate)
    
    ratio = userdata.rate/sound.samplerate;
    
    buffer = spline_resample(buffer, ratio, ix);
      
end

putdata(ao, buffer);

userdata.ix = ix + n_read; 

set(ao, 'Userdata', userdata);






    
    





