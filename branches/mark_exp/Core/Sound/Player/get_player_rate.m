function rate = get_player_rate(player)

% NOTE: this function does not work when the player is playing, we have to wait for a pause

if ~nargin
	player = get_current_player;
end

if isempty(player)
	rate = []; return; 
end

rate = get(player, 'SampleRate');

