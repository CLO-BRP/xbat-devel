function player_start(player)

%--
% access global player
%--

global BUFFERED_PLAYER_1;

%--
% get samples for player
%--

[X, player] = get_data(player);

player.buffer = X;

%--
% create player
%--

BUFFERED_PLAYER_1 = audioplayer(X, player.samplerate);

set(BUFFERED_PLAYER_1, ...
	'tag', 'BUFFERED_PLAYER_1', ...
	'startfcn', @start_cb, ...
	'stopfcn', @stop_cb, ...
	'userdata', player ...
);

%--
% start player
%--

play(BUFFERED_PLAYER_1);
