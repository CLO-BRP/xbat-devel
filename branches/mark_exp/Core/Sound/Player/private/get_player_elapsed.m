function elapsed = get_player_elapsed

%--
% get current audioplayer
%--

current = get_current_player;

% NOTE: return empty when there is no player

if isempty(current)
	elapsed = []; return;
end

%--
% get player
%--

player = get(current, 'userdata');

%--
% compute elapsed time from player
%--

% NOTE: this is the play resample conversion ratio

warp = (player.speed * player.sound.samplerate) / player.samplerate;

ix = player.bix + warp * get(current, 'currentsample');

% NOTE: convert samples to time

elapsed = (ix - player.ix) / player.sound.samplerate;

