function Y = src_resample(X, ratio, method)

if nargin < 3
    method = 'sinc';
end

if nargin < 2 || isempty(ratio)
    ratio = 1;
end

switch method
    
    case 'sinc'
        
        method = 2;
        
    case 'linear'
        
        method = 4;
        
    case {'hold', 'zoh'}
        
        method = 3;
        
end

Y = double(src_resample_mex(single(X)', ratio, method)');