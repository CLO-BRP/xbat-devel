function name = get_playrec_device_fullname(device)

% TODO: this does not work yet, look at 'select_play_device' and 'select_rec_device'

% NOTE: the tricky part is that the name may include both input and output description in this general case

for k = 1:length(device)
    name{k} = sprintf(' %2d) %s (%s) %d channels', device(k).deviceID, device(k).name, device(k).hostAPI, device(k).inputChans);
end