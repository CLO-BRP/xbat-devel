function types = palaoa_resource_types

% palaoa_resource_types - known PALAOA resource types
% ---------------------------------------------------
%
% types = palaoa_resource_types
%
% Output:
% -------
%  types - known PALAOA resource types

types = {'stream', 'webcam', 'webcam-avi', 'met', 'syslog'};