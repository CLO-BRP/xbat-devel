function root = palaoa_root(type, base)

% palaoa_root - root for various PALAOA resource types
% ----------------------------------------------------
%
% root = palaoa_root(type, base)
%
% Input:
% ------
%  type - resource type
%  base - base
%
% Output:
% -------
%  root - root for PALAOA resource type

%-----------------
% HANDLE INPUT
%-----------------

%--
% set default base 
%--

if nargin < 2
	
	% NOTE: 'Q' is the conventional name for this mapped drive in AWI
	
	% NOTE: the second and third choices are local bases
	
	base = {'Q:\PALAOA\DATEN', 'E:\PALAOA', 'C:\PALAOA'};

	% NOTE: this is just used when working locally
	
% 	base = fliplr(base);
	
	% NOTE: the flag makes sure that the directory is accessible, it is slower
	
	for k = 1:numel(base)
		
		if exist_dir(base{k}, 1)
			base = base{k}; break;
		end

	end
	
	if iscell(base)
		error('Unable to set default PALAOA base directory.');
	end
	
end

%--
% return global root, this is just the base
%--

if ~nargin || isempty(type)
	
	root = base; return; 

end

%--
% check resource type
%--

if ~string_is_member(type, palaoa_resource_types)
	
	error('Unknown PALAOA resource type.');

end
	
%-----------------
% BUILD TYPE ROOT
%-----------------

%--
% build resource type root
%--

% NOTE: root should be simply constructed from base and resource name

% TODO: consider the URI type access to the resources

root = fullfile(base, upper(type));

if ~exist_dir(root, 1)
	error('Resource root is not available.');
end


