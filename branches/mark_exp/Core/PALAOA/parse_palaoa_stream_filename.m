function info = parse_palaoa_stream_filename(file)

% parse_palaoa_stream_filename - get information from filename
% ------------------------------------------------------------
%
% info = parse_palaoa_stream_filename(file) 
%
% Input:
% ------
%  file - palaoa stream filename
%
% Output:
% -------
%  info - struct containing filename info

% NOTE: this function is meant to be simple and fast, not too careful

% NOTE: it might work better as a sub-function or as private

%--
% handle multiple files
%--

if iscell(file) && numel(file) > 1
	info = iterate(mfilename, file); return;
end

if iscell(file)
	file = file{1};
end

%--
% get information from filename
%--

% NOTE: get and remove file extension

ix = find(file == '.', 1, 'last');

ext = file((ix + 1):end); file = file(1:(ix - 1)); 

% NOTE: separate date, time and metadata information

part0 = str_split(file, ' '); part1 = str_split(part0{1}, '-');

%--
% pack parsed information
%--

% NOTE: we output the date and the time as numbers, this helps when we need to compare

info.date = eval(part1{1});

info.time = eval([part1{2}, '00']); 

info.location = part0{2}; 

info.source = part0{3};

info.ext = ext;


