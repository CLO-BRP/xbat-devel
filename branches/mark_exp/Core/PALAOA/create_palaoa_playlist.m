function playlist = create_palaoa_playlist(playlist, start, stop, accept, root)

% create_palaoa_playlist - create playlist for PALAOA date time interval
% ----------------------------------------------------------------------
%
% playlist = create_palaoa_playlist(playlist, start, stop, accept, root)
%
% Input:
% ------
%  playlist - playlist filename
%  start - start date and time 
%  stop - stop date and time
%  accept - accept file indicator callback (can be used to censor files)
%  root - PALAOA stream root
%
% Output:
% -------
%  playlist - playlist filename
%
% NOTE:
% -----
%  - The format for date time information is 'YYYYMMDD HH:MM:SS', time may be omitted.
%  - Lines produced may be immediately written to an 'm3u' file.

% TODO: consider creating a full system cache, this is very similar to what currently exists

%----------------
% HANDLE INPUT
%----------------

%--
% set default PALAOA stream root
%--

if nargin < 5
	root = palaoa_root('stream');
end

%--
% set no acceptance test default
%--

if nargin < 4
	accept = [];
end 

%--
% parse start and stop times and check for increasing condition
%--

% TODO: consider other date formats to allow for compatibility with other date and time code

start = parse_datetime(start); stop = parse_datetime(stop);

if etime(stop.clock, start.clock) < 0
	error('Stop time must exceed start time.');
end

%--
% set default playlist name
%--

if isempty(playlist)
	playlist = [tempname, '.m3u'];
end

%----------------
% CREATE LIST
%----------------

disp(' ');
disp(['Creating PALAOA playlist from ', isodate(datenum(start.clock)), ' to ', isodate(datenum(stop.clock)), ' ...']);
disp(' ');

%--
% get relevant directories
%--

% NOTE: the flag indicates we are only interested in directory contents, consider a shortcut function

content = no_dot_dir(root, 1); 

% NOTE: directory names should be numbers, we can evaluate them to compare them

available = iterate(@eval, {content.name}); 

% NOTE: when we get to caching some of this information then we may consider this

% modified = {content.date};

relevant = available((available >= start.date) & (available <= stop.date));

if isempty(relevant)
	playlist = ''; return;
end

% NOTE: we convert directory names back to strings, above we sought clarity

relevant = iterate(@int_to_str, relevant);

%--
% create list file and open for writing
%--

% NOTE: this may be helpful for very large lists

output = get_fid(playlist, 'wt');

% NOTE: this is the first line of a playlist file

file_writelines(output.fid, {'#EXTM3U'});

%--
% get relevant files from first relevant directory
%--

% NOTE: actually both the start and stop relevant directories are special

current = fullfile(root, relevant{1});

if numel(relevant) == 1
	files = get_relevant_files(current, accept, start.time, stop.time);
else
	files = get_relevant_files(current, accept, start.time);
end

% TODO: add extension file info lines, this will double the size of the file

file_writelines(output.fid, strcat(current, filesep, files));

if numel(relevant) == 1
	fclose(output.fid); return;
end

%--
% get relevant files remaining directories
%--

for k = 2:(numel(relevant) - 1)
	
	current = fullfile(root, relevant{k});
	
	% NOTE: for internal relevant directories we get all files, no need for start and stop
	
	files = get_relevant_files(current, accept);
	
	file_writelines(output.fid, strcat(current, filesep, files));
	
end

current = fullfile(root, relevant{end});

files = get_relevant_files(current, accept, [], stop.time);

file_writelines(output.fid, strcat(current, filesep, files));

%--
% close temp playlist file
%--

fclose(output.fid); % show_file(output.name);


%------------------------
% PARSE_DATETIME
%------------------------

function result = parse_datetime(str)

% parse_datetime - parse date and time string
% -------------------------------------------
%
% result = parse_datetime(str)
%
% Input:
% ------
%  str - proposed date and time string
% 
% Output:
% -------
%  result - contains string, condensed date and time, and matlab clock vector

% TODO: check that all relevant content is digits, is there an efficient function

result.str = str;

%--
% get basic string parts
%--

[part, parts] = str_split(str, ' ');

if ~parts || parts > 2
	error('Date and time must be provided in ''YYYYMMDD HH:MM:SS'' format, time may be omited.');
end

%--
% parse date part
%--

if numel(part{1}) ~= 8
	error('Date required in ''YYYYMMDD'' format.');
end

date = part{1};

result.clock = [eval(date(1:4)), eval(date(5:6)), eval(date(7:8)), 0, 0, 0];

result.date = eval(date);

if numel(part) == 1
	result.time = ''; return;
end

%--
% parse time part
%--

[part, parts] = str_split(part{2}, ':');

% NOTE: we do not accept fractional seconds, each field has two characters

if parts > 3 || any(iterate(@numel, part) ~= 2)
	error('Time required in ''HH:MM:SS'' format, seconds and minutes may be omited.');
end

switch numel(part)
	
	case 1, part{2} = '00'; part{3} = '00';
	
	case 2, part{3} = '00';

end

time = strcat(part{:}); result.time = eval(time);

result.clock(4) = eval(time(1:2)); result.clock(5) = eval(time(3:4)); result.clock(6) = eval(time(5:6)); 
 


%------------------------
% GET_RELEVANT_FILES
%------------------------

function files = get_relevant_files(source, accept, start, stop)

% get_relevant_files - get sound files from directory based on time
% -----------------------------------------------------------------
% 
% files = get_relevant_files(source, accept, start, stop)
%
% Input:
% ------
%  source - source directory
%  accept - acceptance callback
%  start - start time
%  stop - stop time
%
% Output:
% -------
%  files - list of relevant files

%--
% set default empty accept callback, start, and stop constraints
%--

if nargin < 4
	stop = [];
end

if nargin < 3
	start = [];
end

if nargin < 2
	accept = [];
end

%--
% get all relevant files contained in source directory
%--

% TODO: add scan for 'ogg' files as well when this is ready

disp(['Scanning ''', source, ''' ...']);

content = what_ext(source, 'mp3'); files = content.mp3;

%--
% consider acceptance
%--

if ~isempty(accept)

	% TODO: take advantage of a vectorized 'accept' function, profile the effect of this callback
	
	% NOTE: if the acceptance callback returns zero, we censor the file
	
	for k = numel(files):-1:1	
		
		if ~eval_callback(accept, fullfile(source, files{k}))
			disp(['Failed to accept ''', files{k}, '''.']); files(k) = [];
		end
		
	end
	
end

%--
% select files relevant considering start and stop times
%--

% NOTE: if there is no time selection needed we are done, also if all files are censored

if nargin < 3 || isempty(files)
	
	disp([int2str(numel(files)), ' relevant files found.']); disp(' '); return;

end

info = parse_palaoa_stream_filename(files);

if nargin < 3 || isempty(stop)
	
	% NOTE: we want all files beyond a certain time

	if ~isempty(start)
		relevant = [info.time] > start - 60;
	else
		relevant = 1:numel(info);
	end
	
	% TODO: the following line failed here because of empty 'start', the current code is a hack

	% files = get_relevant_files(current, accept, [], stop.time);
	
else

	% NOTE: we want all files up to a certain time or we select an interval
	
	if isempty(start)
		relevant = stop > [info.time];
	else
		relevant = (stop > [info.time]) & ([info.time] > start - 60);
	end
	
end

files = files(relevant);

disp([int2str(numel(files)), ' relevant files found.']); disp(' ');



