function [comment, lines] = load_palaoa_comments(file)

% load_palaoa_comments - load comments from text file into database
% -----------------------------------------------------------------
%
% [comment, lines] = load_palaoa_comments(file)
%
% Input:
% ------
%  file - comment log text file
%
% Output:
% -------
%  comment - parsed comment array
%  lines - corresponding lines from input text file

%--
% handle input
%--

if ~nargin || isempty(file)	
	file = fullfile(palaoa_database_root, 'syslogComment.txt');
end

if ~exist(file, 'file')
	error('Unable to access proposed comments file.');
end

%--
% load and parse file
%--

[comment, lines] = load_palaoa_syslog(file, 'comments', @parse_comment_line);



%--
% load and parse file
%--

lines = file_readlines(file);

comment = iterate(@parse_comment_line, lines, '__WAITBAR__');

return; 






%--------------------------
% COMMENT_INIT
%--------------------------

function comment_init(comment)

% NOTE: we flatten the comment and get separate columns for the author name and location

opt = create_table; 

% TODO: figure out the right uniqueness condition for this table

opt.column(end + 1) = column_description('file', 'VARCHAR', 'UNIQUE NOT NULL');
	
sqlite(palaoa_database_file, 'prepared', create_table(comment, 'comments', opt));


%--------------------------
% SAVE_COMMMENTS
%--------------------------

function save_comments(comment)

sqlite(palaoa_database_file, 'exec', sqlite_array_insert('comments', comment));
