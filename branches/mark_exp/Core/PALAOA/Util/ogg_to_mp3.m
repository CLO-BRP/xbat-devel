function ogg_to_mp3(source, destination)

% ogg_to_mp3 - convert a directory of OGG files to MP3
% ----------------------------------------------------
%
% ogg_to_mp3(source, destination)
%
% Input:
% ------
%  source - source directory
%  destination - destination directory

% TODO: allow for encoder options, in particular bitrate

% NOTE: this may be a temporary utility until OGG support is moved to MEX interface

% NOTE: performance tests should be performed after OGG is moved to MEX

%--
% get source directory interactively
%--

if nargin < 1
	
	% NOTE: we get the source directory interactively
	
	source = uigetdir(palaoa_root('stream'), 'Select OGG files directory to convert');

	if ~source
		return;
	end
	
end

%--
% set default destination ensuring it exists
%--

if nargin < 2	
	
	% NOTE: we create a conventional output location
	
	[ignore, leaf, ignore] = fileparts(source); %#ok<NASGU>
	
	% NOTE: this mantains the naming convention in the contained directory
	
	destination = create_dir(fullfile(source, leaf));
	
	if isempty(destination)
		error('Failed to create default output directory.');
	end
	
end

%--
% find ogg files in source directory
%--

input = what_ext(source, 'OGG');

% NOTE: there is nothing to convert and we are done

if isempty(input.OGG)
	return;
end

%--
% convert ogg files to mp3 using 'oggdec' and 'lame'
%--

input = input.OGG;

% TODO: improve progress reporting

for k = 1:numel(input)

	start = clock;
	
	str = get_translation_commands(source, destination, input{k});
	
	for j = 1:numel(str)
		disp(str{j}); system(str{j});
	end
	
	disp(sec_to_clock(etime(clock, start)));
	
	disp(' ');
	
end


%---------------------------------
% GET_TRANSLATION_COMMANDS
%---------------------------------

function str = get_translation_commands(source, destination, input)

%--
% setup
%--

tool = get_translation_tools; 

center = fullfile(destination, file_ext(input, 'wav'));

input = fullfile(source, input);

output = file_ext(center, 'mp3');

%--
% build commands
%--

% NOTE: this function only works on PC currently

str = {};

str{end + 1} = ['"', tool.decode.file, '" -Q "', input, '" -o "', center, '"'];

str{end + 1} = ['"', tool.encode.file, '" --quiet -h "', center, '" -o "', output, '"'];

str{end + 1} = ['del "', center, '"'];


%---------------------------------
% GET_TRANSLATION_TOOLS
%---------------------------------

% TODO: make sure we have the latest versions of these tools

function tool = get_translation_tools

persistent TOOL;

if ~isempty(TOOL)
	
	tool = TOOL;
	
else
	
	TOOL.decode = get_tool('oggdec.exe');
	
	% NOTE: we empty the persistent variable TOOL on failure because so that the search will happen again
	
	if isempty(TOOL.decode)
		TOOL = []; error('Decoding helper is not available.'); %#ok<NASGU>
	end
	
	TOOL.encode = get_tool('lame.exe');
	
	if isempty(TOOL.encode)
		TOOL = []; error('Encoding helper is not available.'); %#ok<NASGU>
	end
	
	tool = TOOL;
	
end




