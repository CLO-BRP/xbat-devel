function value = sound_file_with(file, varargin)

%--
% check and get field value pairs
%--

if mod(numel(varargin), 2) 
	error('Fields and values come in pairs.');
end

if ~all(iterate(@ischar, varargin(1:2:end)))
	error('Fields must be strings.');
end

field = varargin(1:2:end); value = varargin(2:2:end);

%--
% check fields are proper info fields
%--

if ~isempty(setdiff(field, fieldnames(info_create)))
	
	error('Some fields are not sound file info fields.');
	
end

%--
% check properties
%--

% NOTE: this type of factoring allows us to iterate, yet perform the input tests once

value = sound_file_with_in(file, field, value); 


%----------------------------------
% SOUND_FILE_WITH_IN
%----------------------------------

function value = sound_file_with_in(file, fields, values)

% NOTE: the try will also reject inaccesible files, this is implicit

try
	
	% NOTE: innocent until proven guilty
	
	value = 1; info = sound_file_info(file); 
	
	for k = 1:numel(fields)
		
		if ~isequal(info.(fields{k}), values{k})
			value = 0; return;
		end
		
	end
	
catch
	
	value = 0; 

end

