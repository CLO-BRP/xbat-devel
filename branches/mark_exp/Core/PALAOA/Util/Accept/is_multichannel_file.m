function [value, channels] = is_multichannel_file(file, target)

% is_multichannel_file - checks a file has a target number of channels
% --------------------------------------------------------------------
%
% [value, channels] = is_multichannel_file(file, target)
%
% Input:
% ------
%  file - file
%  target - number of channels
%
% Output:
% -------
%  value - result of test
%  channels - actual number of channels

% TODO: an interesting problem is to vectorize while allowing identification of 'corrupt' files

if iscell(file) && numel(file) > 1
	[value, channels] = iterate(mfilename, file, target); return;
end

try
	info = sound_file_info(file); channels = info.channels; value = (channels == target);
catch
	nice_catch(lasterror); value = 0; channels = [];
end 
