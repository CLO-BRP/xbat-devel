function [value, channels] = is_stereo_file(file)

[value, channels] = is_multichannel_file(file, 2);
