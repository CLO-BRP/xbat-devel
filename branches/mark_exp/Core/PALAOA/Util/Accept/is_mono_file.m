function [value, channels] = is_mono_file(file)

[value, channels] = is_multichannel_file(file, 1);
