function simple_name_rename(source, pretend)

% simple_name_rename - rename to simple name containing date time
% ---------------------------------------------------------------
%
% simple_name_rename(source, pretend)
%
% Input:
% ------
%  source - directory containing sound files
%  pretend - show commands to be executed

%--
% set default source directory
%--

if nargin < 2
	pretend = 1;
end

if ~nargin
	source = pwd;
end 

%--
% get mp3 files and rename 
%--

% NOTE: this assumes that the simple name is unique

input = what_ext(source, 'mp3'); 

input = strcat(source, filesep, input.mp3);

for k = 1:numel(input)
	
	[name, ext] = file_ext(input{k}); name = strtok(name, ' '); simple = [name, '.', ext]; 
	
	str = ['rename "', input{k}, '" "', simple, '"']; disp(str);

	if pretend
		continue;
	end

	system(str);

end 
