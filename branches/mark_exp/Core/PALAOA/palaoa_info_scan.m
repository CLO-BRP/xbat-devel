function palaoa_info_scan

%--
% get day folders to scan
%--

root = palaoa_root('stream'); 

days = no_dot_dir(root, 1); days = {days.name};

days = strcat(root, filesep, days);

%--
% scan directories gathering 
%--

disp(' ');

for k = 1:numel(days)
	
	content = what_ext(days{k}, 'mp3'); files = content.mp3;
	
	disp(['Getting info for ''', days{k}, ''' ... ']); start = clock;
	
	sound_file_info(strcat(days{k}, filesep, files));
	
	disp([int2str(numel(files)), ' relevant files. (', num2str(etime(clock, start), 5), ' sec)']); disp(' ');
	
end