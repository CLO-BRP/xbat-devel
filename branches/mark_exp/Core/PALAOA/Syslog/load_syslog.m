function [content, lines] = load_palaoa_syslog(file, parser)

% load_palaoa_syslog - load contents from text file into database
% -----------------------------------------------------------------
%
% [content, lines] = load_palaoa_syslog(file, parser)
%
% Input:
% ------
%  file - content log text file
%  parser - line parser
%
% Output:
% -------
%  content - parsed syslog content array
%  lines - corresponding lines from input text file

%--
% handle input
%--

if ~nargin || isempty(file)	
	file = fullfile(palaoa_database_root, 'syslogComment.txt');
end

if ~exist(file, 'file')
	error('Unable to access proposed syslog file.');
end

%--
% load and parse file
%--

lines = file_readlines(file);

content = iterate(@parser, lines, '__WAITBAR__');

return; 

%--
% write to sqlite database
%--

if ~exist(palaoa_database_file, 'file') || ~has_table(palaoa_database_file, 'contents')
	content_init(content);
end

% TODO: add some kin of paging and waitbar here

save_contents(content);


%----------------------------
% PARSE_COMMENT_LINE
%----------------------------

function content = parse_content_line(str, fun)

% NOTE: these are here for documentation

% fields = {'date', 'time', 'source', 'text'};

part = str_split(str, ' ');

content.created_at = str_implode(part(1:2), ' ');

content.author = parse_author(part{3});

content.text = str_implode(part(4:end), ' ');


%----------------------------
% PARSE_AUTHOR
%----------------------------

function author = parse_author(str)

str(end) = []; part = str_split(str, '@'); 

author.name = part{1}; author.location = part{2};


%--------------------------
% COMMENT_INIT
%--------------------------

function content_init(content)

% NOTE: we flatten the content and get separate columns for the author name and location

opt = create_table; 

% TODO: figure out the right uniqueness condition for this table

opt.column(end + 1) = column_description('file', 'VARCHAR', 'UNIQUE NOT NULL');
	
sqlite(palaoa_database_file, 'prepared', create_table(content, 'contents', opt));


%--------------------------
% SAVE_COMMMENTS
%--------------------------

function save_contents(content)

sqlite(palaoa_database_file, 'exec', sqlite_array_insert('contents', content));
