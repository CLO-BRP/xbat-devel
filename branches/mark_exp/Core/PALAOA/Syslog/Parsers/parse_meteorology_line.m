function data = parse_meteorology(str)

persistent
fields = {'Zeit', 'WHL', 'Sonne', 'Global', 'Reflex', 'RG8', 'OG1', 'Diffus', 'Direkt', 'UV', 'Gegen', 'Aus', 'GegenTemp', 'AusTemp', 'FF2', 'DD2', 'FF10', 'DD10', 'T2', 'T10', 'RF1', 'RF2', 'Druck', 'GegTD', 'GegTD', 'GegTD', 'AusTD', 'AusTD', 'AusTD', 'fmax', 'TempBernd', 'Def', 'Def', 'Def', 'Sicht'};
