function comment = parse_comment_line(str)

% NOTE: these are here for documentation

% fields = {'date', 'time', 'source', 'text'};

part = str_split(str, ' ');

comment.created_at = str_implode(part(1:2), ' ');

comment.author = parse_author(part{3});

comment.text = str_implode(part(4:end), ' ');


%----------------------------
% PARSE_AUTHOR
%----------------------------

function author = parse_author(str)

str(end) = []; part = str_split(str, '@'); 

author.name = part{1}; author.location = part{2};