function [event, marker] = get_full_event(basic)

% get_full_event - get full event from basic table event
% ------------------------------------------------------
%
% [event, marker] = get_full_event(basic)
%
% Input:
% ------
%  basic - table event
%
% Output:
% -------
%  event - event
%  marker - indicator

%--
% setup
%--

event = repmat(event_create, numel(basic), 1); 

marker = zeros(size(event));

field = event_table_fields;

%--
% loop over events
%--

for k = 1:length(basic)

	if isfield(basic, 'guid')
		event(k).guid = basic(k).guid;
	end
	
	% NOTE: this is the inverse of 'build_prototype'

	for j = 1:numel(field.column)
		event(k).(field.name{j}) = basic(k).(field.column{j});
	end

	% NOTE: the code here is OBSCURE outside of the 'event_table_fields' context

	% TODO: integrate this translation into the construction step
	
	if isempty(event(k).duration)
		event(k).duration = 0; marker(k) = 1;
	end
	
	event(k).time(2) = event(k).time(1) + event(k).duration;

	% NOTE: 'bandwidth' has actually been taken from the 'high' frequency field
	
	if ~marker(k)
	
		event(k).freq(2) = event(k).bandwidth;

		% NOTE: the bandwidth needs to be computed

		event(k).bandwidth = diff(event(k).freq);
		
	end

end



