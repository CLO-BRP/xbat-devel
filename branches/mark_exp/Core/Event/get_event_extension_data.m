function [id, ext, data] = get_event_extension_data(events)

% get_event_extension_data - to help save to store
% ------------------------------------------------
%
% [id, ext, data] = get_extension_data(events)
%
% Input:
% ------
%  events - to store
%
% Output:
% -------
%  id - for event with extension data
%  ext - corresponding

%--
% set event extension types
%--

types = get_extension_class_types(get_event_extension_data_types);

types = setdiff(types, 'annotation');

%--
% extract event extension data
%--

id = {}; ext = {}; data = {};

for event = list(events)

	for j = 1:length(types)

		%--
		% check for data from an extension type
		%--
		
		type = types{j};
		
		if ~isfield(event, type)
			continue;
		end 
		
		%--
		% check for specific extension data within the current type
		%--
		
		available = fieldnames(event.(type));
		
		for k = 1:numel(available)
			
			%--
			% get extension index
			%--
			
			tag = ext_tag(type, available{k});
			
			ix = find(strcmp(tag, ext), 1);

			if isempty(ix)
				ext{end + 1} = tag; ix = numel(ext);
			end
			
			%--
			% collect extension data
			%--
			
			% NOTE: we can only increment the contained array after it exists, lameness
			
			% NOTE: we should not need to set the 'parameter' and 'value' fields independently
			
			if ix > numel(id)
				
				id{ix} = event.id;
				
				data{ix}.parameter = event.(type).(available{k}).parameter;
				
				data{ix}.value = event.(type).(available{k}).value;
				
			else
				
				id{ix}(end + 1) = event.id; 
				
				data{ix}(end + 1).parameter = event.(type).(available{k}).parameter;
				
				data{ix}(end).value = event.(type).(available{k}).value;
				
			end
	
		end
		
	end

end

%--
% get extensions using constructed extension tags
%--

% NOTE: this means that the events contained no extension data

if isempty(ext)
	return;
end 

ext = get_tag_ext(ext);


%------------------------
% EXT_TAG
%------------------------

function tag = ext_tag(type, fieldname)

tag = [type, '::', fieldname];


%------------------------
% GET_TAG_EXT
%------------------------

function ext = get_tag_ext(tag)

if iscell(tag)
	ext = iterate(@get_tag_ext, tag); return;
end

info = parse_tag(tag, '::', {'type', 'fieldname'});

if strcmp(info.type, 'detector')
	ext = get_extensions('sound_detector', 'fieldname', info.fieldname);
else
	ext = get_extensions(['event_', info.type], 'fieldname', info.fieldname);
end
