function log = get_page_events(par, page)

% get_page_events - get events in page
% ------------------------------------
%
% log = get_page_events(par, page)
%
% Input:
% ------
%  par - browser
%  page - page
%
% Output:
% -------
%  log - collection of logs with events in page

%--
% handle input
%--

if ~nargin
	par = get_active_browser;
end 

% NOTE: here we initialize trivial output

log = empty(log_create);

if isempty(par)
	return;
end

if nargin < 2
	page = get_browser_page(par);
end

%--
% get logs and respective events in page
%--

log = get_browser_logs(par);

for k = 1:numel(log)
	
	[part, position] = get_events_by_page(log(k), page, page.channel);
	
	% TODO: consider whether we censor events based on position
	
	event{k} = part;
	
end

%--
% get active log, all the events are on the page
%--

active = get_active_detection_log(par);

if ~isempty(active.event), log(end + 1) = active; end




