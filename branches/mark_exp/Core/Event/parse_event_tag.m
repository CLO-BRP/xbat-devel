function [name, id] = parse_event_tag(tag)

% parse_event_tag - parse event tag into log name and id
% ------------------------------------------------------
%
% [name, id] = parse_event_tag(tag)
%
% Input:
% ------
%  tag - event tag
% 
% Output:
% -------
%  name - log name
%  id - event id

%--
% handle multiple tags if needed
%--

if iscellstr(tag)
	[name, id] = iterate(mfilename, tag); return;
end

%--
% parse tag
%--

part = str_split(tag, '::');

name = part{2}; id = str2double(part{3});

