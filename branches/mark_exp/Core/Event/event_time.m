function time = event_time(event, full)

% event_time - get time extent of event
% -------------------------------------
%
% time = event_time(event, full)
%
% Input:
% ------
%  event - event
%  full - force full evaluation of time, relevant for hierarchical events
%
% Output:
% -------
%  time - start and stop time

%--
% set no full evaluation default
%--

if nargin < 2
	full = 0;
end

%--
% handle multiple events
%--

if numel(event) > 1
	
	time = zeros(numel(event), 2);
	
	for k = 1:numel(event)
		time(k ,:) = event_time(event(k));
	end 
	
	return;
	
end

%--
% get get time quickly if available
%--

% NOTE: this allows us to cache this computation for hierarchical events

if ~isempty(event.time) && ~full

	time = event.time; return;

end 

%--
% check for hierarchical event
%--

count = event_has_children(event);

if count
	
	time = zeros(count, 2);
	
	for k = 1:count
		time(k, :) = event_time(event.children(k));
	end
	
	time = [min(time(:, 1)), max(time(:, 2))];
	
	return;
	
end
	
%--
% get simple event time
%--

time = event.time; 
	
	