function hint = column_type_hints

% column_type_hints - create struct for hinting of database column types
% ----------------------------------------------------------------------
%
% hint = column_type_hints
%
% Output:
% -------
%  hint - struct

%--
% create and check for persitent hint struct
%--

persistent HINT;

if ~isempty(HINT)
	hint = HINT; return;
end

%--
% set hint struct and persist
%--

hint.integer = int32(1); hint.i = hint.integer;

% NOTE: this is MySQL specific, not used generally

hint.bigint = int64(1); hint.bi = hint.bigint;

hint.real = sqrt(2); hint.r = hint.real; hint.float = hint.real;

hint.string = 'string'; hint.s = hint.string;

hint.text = {'text'}; hint.t = hint.text;

% NOTE: these indicate that matrices and cell are stored as text

hint.matrix = hint.text; hint.cell = hint.text;

hint.timestamp = datestr(now, 31); hint.ts = hint.timestamp; hint.datetime = hint.timestamp;

hint.date = date; hint.d = hint.date;

hint.boolean = true; hint.logical = true; hint.binary = true;

HINT = hint;