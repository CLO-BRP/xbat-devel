function event = update_event_duration(event, full) 

% NOTE: we update event durations assuming that times are properly set

if nargin < 2
	full = 0;
end

if numel(event) > 1
	event = iterate(mfilename, event, full); return;
end

event.duration = event_duration(event, full);