function freq = event_freq(event, full)

% event_freq - get freq extent of event
% -------------------------------------
%
% freq = event_freq(event, full)
%
% Input:
% ------
%  event - event
%  full - force full evaluation of freq, relevant for hierarchical events
%
% Output:
% -------
%  freq - start and stop freq

%--
% set no full evaluation default
%--

if nargin < 2
	full = 0;
end

%--
% handle multiple events
%--

if numel(event) > 1
	
	freq = zeros(numel(event), 2);
	
	for k = 1:numel(event)
		freq(k ,:) = event_freq(event(k));
	end 
	
	return;
	
end

%--
% check for quick freq 
%--

if ~isempty(event.freq) && ~full
	
	freq = event.freq; return;
	
end

%--
% check for hierarchical event
%--

count = event_has_children(event);

if count
	
	freq = zeros(count, 2);
	
	for k = 1:count
		freq(k, :) = event_freq(event.children(k));
	end
	
	freq = [min(freq(:, 1)), max(freq(:, 2))];
	
	return;
	
end
	
%--
% get simple event freq
%--

freq = event.freq; 
	
	