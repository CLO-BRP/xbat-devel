function event = get_event_samples(event, sound, context)

% TODO: perform some check on the size of the event

%--
% read samples
%--

% TODO: move 'page' related code to the 'samples', 'unfiltered' nomenclature

if nargin < 3
	page = get_event_page(event, sound);
else
	page = get_event_page(event, sound, context);
end

% NOTE: there is no need to store the unfiltered samples if no filtering is happening

if isempty(page.filtered)
	event.samples = page.samples; event.unfiltered = [];
else
	event.samples = page.filtered; event.unfiltered = page.samples;
end

event.rate = page.rate;