function name = event_name(event, log)

% event_name - get event name
% ---------------------------
%
% name = event_name(event, log)
%
% Input:
% ------
%  event - event
%  log - log name
%
% Output:
% -------
%  name - event name

%------------------------
% HANDLE INPUT
%------------------------

%--
% set default prefix
%--

if nargin < 2 || isempty(log)
	log = 'Event';
end

%--
% handle multiple events recursively
%--

if numel(event) > 1
	
	for k = 1:numel(event)
		name{k} = event_name(event(k), log);
	end
	
	return;
	
end

%------------------------
% GET NAME
%------------------------

name = [log, ' # ', int2str(event.id)];