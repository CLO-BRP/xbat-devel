function selection_update(par, data, sel)

% selection_update - update selection related display
% ---------------------------------------------------
%
% selection_update(par, data, sel)
%
% Input:
% ------
%  par - parent
%  data - parent state
%  sel - selection to consider

%-------------------------------------------
% HANDLE INPUT
%-------------------------------------------

%--
% get userdata if needed
%--

if nargin < 2 || isempty(data)
	data = get(par, 'userdata');
end

if nargin < 3 || isempty(sel)
	sel = data.browser.selection;
end

%-------------------------------------------
% UPDATE SELECTION ZOOM DISPLAY
%-------------------------------------------

if ~sel.zoom
	
	%--
	% delete selection zoom if it exists 
	%--
	
	handle = get_xbat_figs( ...
		'parent', par, 'type', 'selection' ...
	);

	if ~isempty(handle)
		delete(handle);
	end
	
end

if isempty(sel.handle)
	
	% NOTE: if we add a close request function to this figure change to 'close'
	
	handle = get_xbat_figs( ...
		'parent', par, 'type', 'selection' ...
	);

	if ~isempty(handle)
		set(handle, ...
			'color', get(par, 'color'), 'colormap', get(par, 'colormap') ...
		);
	end
	
	return;
	
end
	
%-------------------------------------------
% UPDATE SELECTION DISPLAY
%-------------------------------------------

%--
% pull out some browser fields
%--

sound = data.browser.sound;

page = data.browser.page; 

page.start = data.browser.time;

%--
% if there is no selection or it is out of page, return
%--

if isempty(sel.event) || ~event_in_page(sel.event, page, sound)
	return;
end

%--
% select new or logged selection
%--

if isempty(sel.event.id)

	browser_bdfun(sel.event);

else

	% NOTE: we try to get the log and event indices to persist selection

	try
		
		if ~isempty(sel.log)
			event_bdfun(par, sel.log, sel.event.id);
		end
		
	catch

		nice_catch(lasterror, 'WARNING: Failed to persist event selection.');

	end

end

	



