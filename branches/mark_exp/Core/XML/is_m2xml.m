function [valid, simple] = is_m2xml(str)

% is_m2xml - check whether string was produced by m2xml
% -----------------------------------------------------
%
% [valid, simple] = is_m2xml(str)
%
% Input:
% ------
%  str - string of string cell array to test
%
% Output:
% -------
%  valid - indicator
%  simple - indicator

%--
% handle input
%--

% NOTE: we handle multiple strings if needed

if iscell(str)
	[valid, simple] = iterate(mfilename, str); return;
end

valid = 0; simple = 0;

if ~ischar(str)
	return;
end

%--
% compare start with prefix
%--

valid = strncmp('<!--M2SML-->', str, 12) + 2 * strncmp('<!-- M2SML -->', str, 14);

if valid
	simple = 1; return;
end

valid = strncmp('<!--M2XML-->', str, 12) + 2 * strncmp('<!-- M2XML -->', str, 12);

