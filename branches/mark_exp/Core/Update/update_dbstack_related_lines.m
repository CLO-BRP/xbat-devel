function updated = update_dbstack_related_lines(ext, pretend)

% update_dbstack_related_lines - refactor to reduce dbstack reliance
% ------------------------------------------------------------------
%
% updated = update_dbstack_related_lines(ext, pretend)
%
% Input:
% ------
%  ext - extensions to consider
%  pretend - report proposed changes without making them
%
% Output:
% -------
%  updated - files updated during refactoring

% TODO: implement using 'iterate'

% TODO: scan through the method code for calls to 'parent_fun'

%--------------------
% HANDLE INPUT
%--------------------

%--
% set default pretend, this is pretty dangerous stuff
%--

if nargin < 2
	pretend = 1;
end

%--
% get all extensions to consider
%--

if ~nargin
	ext = get_extensions;
end

%--------------------
% UPDATE
%--------------------

%--
% scan through extensions files and check for updates
%--

updated = {};
 
for k = 1:numel(ext)
	
	%--
	% skip extension type extensions for now
	%--
	
	% TODO: there is a problem when modifying the main function in this case
	
	% NOTE: inheritance is not currently used much for this type, surely there are opportunities for this
	
	if strcmp(ext(k).subtype, 'extension_type')
		continue; 
	end
	
	disp(upper(ext(k).name));
	
	%--
	% get and read lines from main extension function file looking for 'extension_create'
	%--
	
	main = functions(ext(k).fun.main); file = main.file;
	
	lines = file_readlines(file);
	
	for j = 1:numel(lines)
		
		% TODO: these extensions have no parent, they do not need to be considered for 'parent_fun' search
		
		update1 = strcmp(lines{j}, 'ext = extension_create;');
		
		update2 = strfind(lines{j}, 'extension_inherit(');
		
		% NOTE: this eliminates the dependence of 'extension_create' on 'dbstack'
		
		if update1
			lines{j} = 'ext = extension_create(mfilename);'; break;
		end 
		
		% NOTE: this eliminates the dependence of 'extension_inherit' on 'dbstack'
		
		if update2
			match = lines{j}; lines{j} = [match(1:update2 - 1), 'extension_inherit(mfilename, ', match(update2 + 18:end)]; break;
		end 

	end
	
	if update1
		
		if ~pretend
			file_writelines(file, lines);
		end
		
		% NOTE: in this case a detailed description is not necessary, the modification is fixed
		
		disp(['Updated main ''', file, '''.']); 
		
		updated{end + 1} = file;
	
	end
	
	if update2
		
		if ~pretend
			file_writelines(file, lines);
		end

		% NOTE: in this case a detailed description is not necessary, the modification is fixed

		disp(['  Updated ''', file, '''.']);
		disp(['    ', match]);
		disp(['    ', lines{j}]);

		updated{end + 1} = file;

	end
	
	%--
	% check method functions for 'parent_fun' calls
	%--
	
	root = fullfile(extension_root(ext(k)), 'private');
	
	files = no_dot_dir(root, -1); 
	
	if ~isempty(files)
		files = strcat(root, filesep, {files.name});
	end
	
	for i = 1:numel(files)
		
		%--
		% get method lines
		%--
		
		lines = file_readlines(files{i});
		
		%--
		% check for and update calls to 'parent_fun'
		%--
		
		% NOTE: we assume that there is a single call to 'parent_fun', this is the way it should be
		
		for j = 1:numel(lines)

			if isempty(lines{j})
				continue;
			end
			
			update = strfind(lines{j}, 'parent_fun;');
			
			if update
				match = lines{j}; lines{j} = [match(1:update - 1), 'parent_fun(mfilename(''fullpath''));', match(update + 11:end)]; break;
			end
			
		end
		
		if update

			if ~pretend
				file_writelines(files{i}, lines);
			end

			% NOTE: in this case a detailed description is not necessary, the modification is fixed

			disp(['  Updated ''', files{i}, '''.']);
			disp(['    ', match]);
			disp(['    ', lines{j}]);
			
			updated{end + 1} = files{i};

		end

	end
	
	disp(' ');
	
end

updated = updated(:);
