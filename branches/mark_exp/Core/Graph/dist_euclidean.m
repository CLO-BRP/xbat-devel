function D = dist_euclidean(X, Y)

% dist_euclidean - euclidean distance matrix for vectors
% ------------------------------------------------------
%
% D = dist_euclidean(X, Y)
%
% Input:
% ------
%  X - column vectors, row index
%  Y - column vectors, column index
%
% Output:
% ------
%  D - distance matrix

if nargin < 2
	Y = X;
end

% NOTE: compute distance matrix by compute square-root of squared differences

D = sqrt(dist_euclidean2(X, Y));


