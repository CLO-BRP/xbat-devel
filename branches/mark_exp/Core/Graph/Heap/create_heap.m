function [heap, ix] = create_heap(items)

%--
% output empty heap
%--

if ~nargin || isempty(items)
	heap = []; ix = []; return; 
end

%--
% push items onto heap
%--

[heap, ix] = push_heap([], items);