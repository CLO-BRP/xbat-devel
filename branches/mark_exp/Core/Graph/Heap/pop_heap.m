function [items, heap, ix] = pop_heap(heap, count)

if nargin < 2
	count = 1;
end

%--
% enforce maximum elements to remove and flag for remove all
%--

total = size(heap, 2);

if count >= total
	count = total;
end

%--
% remove elements from heap
%--
	
% NOTE: we output top element, move last element of heap to top, shorten heap, and enforce heap condition

heap = [heap; 1:numel(heap)];

for k = 1:count
	items(:, k) = heap(:, 1); heap(:, 1) = heap(:, end); heap = heap(:, 1:end - 1); heap = downheap(heap, 1);	
end

items = items(1, :); ix = heap(2, :); heap = heap(1, :);