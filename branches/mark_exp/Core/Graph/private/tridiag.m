function T = tridiag(n,d)

% tridiag - sparse tridiagonal matrix
% -----------------------------------
%
% T = tridiag(n,d)
% 
% Input:
% ------
%  n - size of matrix
%  d - diagonal elements
%
% Output:
% -------
%  T - sparse tridiagonal matrix
%

T = spdiags(ones(n,1)*d,-1:1,n,n);
