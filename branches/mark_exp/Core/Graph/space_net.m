function G = space_net(n, b, d)

% space_net - random proximity network
% ------------------------------------
%
% G = space_net(n, b, d)
%
% Input:
% ------
%  n - size of lattice
%  b - distance interval for linking (def: '[0,1.1]')
%  d - lattice distortion parameter (def: 0.7)
%
% Output:
% -------
%  G - lattice and proximity network

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%--
% set default lattice distortion parameter
%--

if (nargin < 3) || isempty(d)
	d = 0.7;
end

%--
% set default distance-linking activity interval
%--

if (nargin < 2) || isempty(b)
	b = '[0,1.1]';
end

%--
% create jittered lattice network
%--

% NOTE: the lattice network has positions 'X' and edges 'E'

if length(n) == 1
	G{1} = graph_lattice(n);
else
	G{1} = graph_lattice(n(1), n(2));
end

G{1} = jitter_graph(G{1}, d);
	
%--
% overlay proximity network
%--

% NOTE: the proximity network has values 'V' and edges 'E'

% NOTE: in this case the values match the jittered lattice positions

G{2} = graph_distance(G{1}.X, @dist_plane, b);


%----------------------------------------
% DISPLAY NETWORK
%----------------------------------------

% TODO: factor and generalize. essentially we have 'positions', 'values', and 'edges' to consider

fig;

%--
% display proximity graph
%--

gplot(edge_to_sparse(G{2}.E), G{1}.X, 'r');

hold on;

H = scatter('v6', G{1}.X(:, 1), G{1}.X(:, 2), 64, 'o');

if numel(n) == 1
	axis([0, (n + 1), 0, (n + 1)]); 
else
	axis([0, (n(2) + 1), 0, (n(1) + 1)]); 
end

axis('equal');

set(gca, 'visible', 'off', 'units', 'normalized', 'position', [0, 0, 1, 1]);

hold on;

[step, comp, branch] = graph_dfs(G{2});

nc = max(comp);

C = hsv(nc);

C = C(randperm(nc), :);

% C = max((C(randperm(nc), :) - 0.1), zeros(nc, 3));

for k = 1:prod(n)
	
	tmp = text(G{1}.X(k, 1) + 0.2, G{1}.X(k, 2), int2str(step(k)));
	
	set(tmp, 'fontsize', 7, 'color', 0.6 * ones(1, 3));
	
	if (branch(k) > 0)
		set(H(k), 'markersize', 12);
	end
	
end

% gplot(edge_to_sparse(G{1}.E), G{1}.X, 'k:');


% create gray markers

for k = 1:length(H)
	set(H(k), 'MarkerFaceColor', C(comp(k), :));
	set(H(k), 'MarkerEdgeColor', ones(1, 3));
end

set(gcf, 'color', 0 * ones(1, 3));



