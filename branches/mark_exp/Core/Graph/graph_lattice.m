function G = graph_lattice(m, n, t)

% graph_lattice - create lattice graph
% ------------------------------------
%
% G = graph_lattice(m, n, t)
%
% Input:
% ------
%  m - number of rows
%  n - number of columns (def: m)
%  t - type of lattice (def: 'sq')
%    'tri' - triangular
%    'sq' - square 
%    'hex' - hexagonal   
%
% Output:
% -------
%  G - lattice graph

%--
% handle input
%--

if nargin < 3
	t = 'sq';
end

if nargin < 2
	n = m;
end

%--
% create lattice
%--

switch t

	case 'sq'
	
		%--
		% compute positions and edges
		%--
		
		[X, Y] = meshgrid(1:n, 1:m);
		
		A = kron(speye(n), tridiag(m, [1, 0, 1])) | kron(tridiag(n, [1, 0, 1]), speye(m));
		
		%--
		% pack graph
		%--
		
		G.X = [X(:), Y(:)]; G.E = sparse_to_edge(A);
	
	% TODO: everything below needs to be written or rewritten

	case 'tri'
	
		%--
		% compute positions and edges
		%--
		
		h = 0.5 * sqrt(3);
		
		[X, Y] = meshgrid(1:n, 1:h:(h * (m + 1)));
		
 		S = repmat([zeros(1, n); 0.5 * ones(1, n)], [ceil(0.5 * m), 1]); S = S(1:m, :);
		
		X = X - S;
		
		X = [X(:)'; Y(:)'];
			
		% NOTE: we use distance computations to get edges
		
		G = graph_distance(X, 'dist_euclidean', '[0.95, 1.05]');
		
		%--
		% pack graph
		%--
		
		% NOTE: the values used in the distance computations are actual positions
		
		% TODO: consider whether we should empty the field or simply remove it
		
		G.X = G.V'; G.V = [];

	case 'hex'
	
end
