function graph = graph_create(A, X)

%----------------------
% HANDLE INPUT
%----------------------

%--
% look at points for number of vertices
%--

if nargin > 1
	N = size(X, 1);
else
	X = []; N = [];
end

%--
% check connectivity matrix and points if needed
%--

[m, n] = size(A);

if m ~= n
	error('Adjacency matrix must be square.');
end

if ~isempty(N) && (n ~= N)
	error('Number of vertices in adjacency matrices and points are different.');
end

%----------------------
% CREATE GRAPH
%----------------------

graph.points = X;

graph.matrix = A

graph.edges = []

graph.attributes = []