function [value, info] = is_browser(par)

% is_browser - check browser handles
% ----------------------------------
%
% [value, info] = is_browser(par)
%
% Input:
% ------
%  par - proposed browser handles
%
% Output:
% -------
%  value - browser indicator
%  info - browser info

% TODO: make this leaner for the simple most common case, like 'is_palette'

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% handle multiple handles recursively
%--

if numel(par) > 1
	[value, info] = iterate(mfilename, par); return;
end

%--
% check for figure handles
%--

if isempty(par) || (par && ~is_handles(par, 'figure'))
	value = 0; info = []; return;
end

%--------------------------------
% TEST HANDLE
%--------------------------------

%--
% get browser info and check type
%--

info = get_browser_info(par);

value = ~isempty(strfind(upper(info.type), 'BROWSER'));
