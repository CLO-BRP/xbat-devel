function [file, time, duration] = get_browser_page_file(par, page)

% get_browser_page_file - get files used in browser page
% ------------------------------------------------------
%
% [file, time, duration] = get_browser_page_file(par, page)
%
% Input:
% ------
%  par - parent browser
%  page - browser page
%
% Output:
% -------
%  files - files in page
%  time - file start times
%  duration - file durations

% TODO: get start and stop times for files

%--
% handle input
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

if isempty(par)
	file = {}; time = []; duration = []; return;
end 

if nargin < 2
	page = get_browser_page(par);
end

%--
% get browser sound
%--

sound = get_browser(par, 'sound');

if numel(sound.file) == 1
	file = sound.file{1}; time = 0;
end

%--
% get files in current page
%--

% TODO: this could change as sound types become more abstract

file = get_current_files(sound, page.start, page.start + page.duration);

[time, duration] = get_file_times(sound, file);
