function marker = set_browser_marker(par, marker, visible, data)

% set_browser_marker - what it says
% ---------------------------------
%
% marker = set_browser_marker(par, marker, visible, data)
%
% Input:
% ------
%  par - browser
%  marker - marker 
%  visible - not sure about the meaning of this anymore, may disappear
%  data - browser state
%
% Output:
% -------
%  marker - set

%--
% handle input
%--

% NOTE: when called without arguments we are interested in refreshing the marker display

if ~nargin
	par = get_active_browser;
end

if isempty(par)
	return;
end

if nargin < 2
	marker = get_browser_marker(par);
end

% NOTE: this should no longer happen, clean up this code eventually

if isempty(marker)
	return;
end

if nargin < 4
	data = get_browser(par);
end

if nargin < 3
	visible = 0;
end

%--
% set marker
%--

% NOTE: we only want to set visible markers, check marker is in page

[in_page, page] = marker_in_page(marker, par, data);

if visible && ~in_page
	
	ax = get_channel_axes(par, page.channel(1));
	
	marker = set_marker(par, ax, page.start); return;

end
	
ax = get_channel_axes(par, marker.channel);

if ~isempty(ax)
	marker = set_marker(par, ax, marker.time);
end