function marker = get_event_marker(event)

% get_event_marker - get marker from event
% ----------------------------------------
%
% marker = get_event_marker(event)
%
% Input:
% ------
%  marker - struct
%
% Output:
% -------
%  marker - corresponding to event

marker = marker_create( ...
	'time', event.time(1), 'channel', event.channel, ...
	'tags', event.tags, 'rating', event.rating, 'notes', event.notes ...
);