function window_plot(pal, parameter)

% window_plot - create and update window display
% ----------------------------------------------
% 
% window_plot(pal, parameter)

%--
% get display axes handle
%--

handles = get_control(pal, 'Window_Plot', 'handles');

% NOTE: this is a temporary situation, eventually there will be one name

if isempty(handles)
	handles = get_control(pal, 'window_plot', 'handles');
end

% NOTE: return quickly if display is not there

if isempty(handles)
	return;
end

ax = handles.axes;

%--
% prepare display
%--

axes(ax); hold on;

delete(get(ax, 'children'));

%--
% compute window and shifted window
%--

padwin = get_window(parameter);

nfft = parameter.fft;

next = floor(parameter.hop * nfft);

nextwin = [zeros(next,1); padwin(1:(nfft - next))];

%--
% display window and shifted window
%--
	
tmp = plot(padwin,'k');

set(tmp,'linewidth',1);

hold on;

tmp = plot(nextwin, ':k');

tmp = plot([1, nfft], [0, 0], ':');

set(tmp,'color',0.5 * ones(1,3));

tmp = plot([1, nfft], [1, 1], ':');

set(tmp,'color',0.5 * ones(1,3));

set(ax, ...
	'xlim', [1, nfft], 'ylim', [-0.2, 1.2] ...
);
