function time = get_browser_time(par)

if ~nargin
	par = get_active_browser; 
end

slider = get_time_slider(par); time = slider.value;

if ~nargout
	disp(sec_to_clock(time)); clear;
end