function [log, pos] = get_browser_logs(par, varargin)

% get_browser_logs - get browser logs
% -----------------------------------
%
% log = get_browser_logs(par, field, value, ... , data)
%
% Input:
% ------
%  par - parent browser
%  field - field name
%  value - field value
%  data - browser state
%
% Output:
% -------
%  log - browser logs

% TODO: add log index to output

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set default parent
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

% NOTE: return if there is no available parent

log = []; pos = [];

if isempty(par)
	return;
end

%--
% check parent input
%--

if ~is_browser(par)
	error('Input handle is not browser handle');
end 

%--
% get parent state
%--

% NOTE: if argument list length is odd, last argument is state

if mod(length(varargin), 2)
	data = varargin{end}; varargin(end) = [];
else
	data = get_browser(par);
end

%--
% get all browser logs
%--

log = data.browser.log;

% NOTE: return if there is no selection

if isempty(log)
	return;
end

%---------------------------------
% SELECT LOGS
%---------------------------------

%--
% get selection field value pairs
%--

if numel(varargin)
	
	[field, value] = get_field_value(varargin);

	ix = ones(1, length(log));

	for k = 1:numel(field)

		switch field{k}

			%--
			% LOG TYPE SELECTION
			%--

			% TODO: the goal of this is to update the store of active detection logs

			case 'type'

				types = {'file', 'active'};

				if ~ismember(value{k},types)
					error(['Unrecognized log type ''', value{k}, '''.']);
				end

				switch value{k}

					case 'active'

					case 'file'

				end

			%--
			% LOG FIELD SELECTION
			%--

			% COMPUTED FIELDS

			case 'name'

				name = log_name(log);

				ix = ix & strcmpi(name,value{k});

			% ACTUAL FIELDS

			otherwise

				if ~isfield(log, field{k})
					continue;
				end

				% NOTE: we only use string and numeric fields for selection

				switch class(log(1).(field{k}))

					case {'struct', 'cell'}

					case 'char', ix = ix & strcmpi({log.(field{k})}, value{k});

					otherwise, ix = ix & ([log.(field{k})] == value{k});

				end

		end

	end

	% TODO: consider empty index array possibility

	pos = find(ix);

	log = log(pos);

end

%---------------------------------
% NORMALIZE LOGS
%---------------------------------

%--
% append fields if needed
%--

% NOTE: this patches a temporary failure, we may be able to remove this in the future

for k = 1:numel(log)
	
	if ~isfield(log(k), 'event')
		log(k).event = empty(event_create);
	end
	
	if isempty(log(k).event) || ~isstruct(log(k).event)
		log(k).event = empty(event_create);
	end
	
	if ~isfield(log(k), 'length')
		log(k).length = numel(log(k).event);
	end
	
end
