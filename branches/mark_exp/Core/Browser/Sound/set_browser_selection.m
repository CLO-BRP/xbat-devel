function set_browser_selection(par, event, log, id) 

% set_browser_selection - set browser selection
% ---------------------------------------------
%
% set_browser_selection(par, event, m, id)
%
% Input:
% ------
%  par - browser
%  event - selection event
%  m - log index
%  ix - event id

%--
% check browser input
%--

if ~is_browser(par)
	return;
end

%--
% set selection
%--

% NOTE: in this case the function works as an alias

if (nargin < 3) || isempty(log)
	
	figure(par); browser_bdfun(event); return; 
	
end

%--
% handle special case of active detection log selection
%--

% TODO: we are trying to test for a log 'duck' and we should create a log 'duck'

if ~isfield(log, 'file') && isfield(log, 'name') && string_begins(log.name, 'ACTIVE_DETECTION_LOG')
	
	log = get_active_detection_log(par);
	
	ix = find(id == [log.event.id]);
	
	if isempty(ix)
		return;
	end
	
	% TODO: FIGURE OUT THE SOURCE OF THIS BUG, IN PRINCIPLE WE ARE NOT DRY!
	
	event = log.event(ix); event.duration = diff(event.time);
	
	figure(par); browser_bdfun(event); return; 
	
end

%--
% set event selection
%--

if (nargin < 4) || isempty(id)
	
	id = event.id;
    
    if isempty(id)
        return;
    end
	
end

event_bdfun(par, log, id);


