function handles = browser_measure_menu_2(par)

% TODO: generalize this for all relevant extension types

%--
% get extensions
%--

ext = get_extensions('event_measure');

handles = [];

if isempty(ext)
	return;
end

%--
% create top menu and extensions category menu
%--

top = uimenu(par, 'label', 'Measure'); 

handles(end + 1) = top;

extension_category_menu(top, ext);

set(top, 'position', 10);