function browser_keypress_callback(obj, eventdata, par) %#ok<INUSL>

% browser_keypress_callback - callback helper for browser keypress function
% -------------------------------------------------------------------------
%
% browser_keypress_callback(obj, eventdata, par)

%--
% get browser from callback handle
%--

if nargin < 3
	
	% NOTE: this should not be typicae, we should pass the parent
	
	if is_browser(obj)
		par = obj; 
	else
		try
			par = get_palette_parent(obj); 
		catch
			return;
		end
	end
	
end

%--
% get keypress information
%--

% NOTE: we get the keypress of the callback object

key = get_keypress(obj);

%--
% intercept keypress to implement some palette behaviors
%--

% NOTE: look at 'palette_kpfun' to get a better understanding of what we may want

if ~isequal(obj, par) && ismember(key.char, {'m', 'M', 'w'})
	
	switch key.char
		
		% NOTE: the 'palette_minmax' function should take the handle first
		
		case 'm', palette_minmax('min', obj);
			
		case 'M', palette_minmax('max', obj);
			
		case 'w', close(obj);
			
	end
	
	return;
	
end

%--
% call existing browser keypress function
%--

result = browser_kpfun(par, key);

%--
% focus update
%--

% NOTE: in many cases, but not all, it makes sense to keep focus on caller

if result.keep_focus
	
	figure(obj); 

end

