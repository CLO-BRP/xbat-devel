function par = test_new_menus

%--
% create parent
%--

par = figure;

%--
% get types and labels
%--

types = {'signal_filter', 'sound_feature', 'sound_detector'};

names = {'Filter', 'Feature', 'Detect'};

%--
% discover types and create menus
%--

for k = 1:length(types)
	
	ext = discover_extensions(types{k});
	
	if ~isempty(ext)
		top = uimenu(par, 'label', names{k}); extension_category_menu(top, ext, @test_callback);
	end
	
end


%-----------------
% TEST_CALLBACK
%-----------------

% NOTE: this callback implements a mini-API in requiring the name and type

function test_callback(obj, eventdata, request, type, name)

disp(str_implode({get(obj, 'label'), name, type}, ' :: '));


