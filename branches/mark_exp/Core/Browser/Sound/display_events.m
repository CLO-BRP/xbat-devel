function display_events(par, time, duration, data, light)

% display_events - display events in time range
% ---------------------------------------------
%
% display_events(par, time, duration, data, light)
%
% Input:
% ------
%  par - parent browser handle
%  time - page beginning
%  duration - page duration
%  data - parent browser state
%  light - lightweight display flag
%
% Output:
% -------
%  (none)

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% set light display 
%--

if (nargin < 5) || isempty(light)
	light = 0;
end

%--
% get browser state
%--

if (nargin < 4) || isempty(data)
	data = get_browser(par);
end

%---------------------------
% SETUP
%---------------------------

%--
% convenience variables
%--

ch = get_channels(get_browser(par, 'channels', data));

%--
% get axes 
%--

ax = data.browser.axes;

%--
% delete current selection objects
%--

tmp = data.browser.selection.handle;

delete(tmp(ishandle(tmp))); % this selection may not be needed

% TODO: this should be factored, and perhaps made smarter

% NOTE: that we will be uniformly tagging event and measure display through the API

%--
% remove currently displayed events
%--

type = {'line', 'patch', 'text'};

for k = 1:length(type)	

	h1 = findall(ax, 'type', type{k});

	n1 = length(h1);

	%--
	% get patch children
	%--

	if strcmp(type{k}, 'patch') && n1

		%--
		% get context menus and menu children
		%--

		if (n1 > 1)
			tmp1 = cell2mat(get(h1, 'uicontextmenu'));
		else
			tmp1 = get(h1, 'uicontextmenu');
		end

		tmp2 = findobj(tmp1, 'type', 'uimenu');

		%--
		% delete patch menus
		%--

		delete(tmp2);
		
		delete(tmp1);

	end

	% TODO: there is a problem with invalid handles in the findall during active scrolling

	h2 = findall(h1, 'flat', 'tag', 'file_boundary');

	if ~isempty(h2)
		h1 = setdiff(h1, h2);
	end

	h2 = findall(h1, 'flat', 'tag', '');

	if ~isempty(h2)
		h1 = setdiff(h1, h2);
	end

	delete(h1);

end

%--
% disable selection options
%--

if isfield(data.browser, 'sound_menu')

	tmp = data.browser.sound_menu.play;

	set(get_menu(tmp, 'Selection'), 'enable', 'off');
	
end

%--
% clear widget event displays
%--

widget = get_widgets(par, 'event__display');

clear_previous_widget_display(widget, 'event');

%---------------------------
% DISPLAY EVENTS
%---------------------------

%--
% display active detection selected events
%--

% NOTE: perhaps the idea of active logs should be extended to have multiple active detectors

if ~isempty(data.browser.sound_detector.active)

	active_log = data.browser.active_detection_log;

	if active_log.length
		
		event_view('sound', par, '__ACTIVE_DETECTION__', 1:active_log.length, ax, data, light);
	
% 		active_log.format = 'active';
% 		
% 		event_view_2(par, active_log.event, active_log);
		
	end

end

%--
% display page-visible events from logs
%--

page.start = time; page.duration = duration; page.channel = ch;

for k = 1:length(data.browser.log)

	%--
	% skip invisible or empty logs
	%--
    
	if ~data.browser.log(k).visible
		continue;
	end
	
	%--
	% select events in page
	%--

	% TODO: use 'light' flag to indicate that we should not query the log beyond the cache
	
	% NOTE: 'browser_time_slide' makes a 'light' call, it should be fast
	
	[events, position] = get_events_by_page(data.browser.log(k), page, page.channel, light); ghost = (sum(position, 2) == 0);
	
    %--
    % display events
    %--
	
    event_view_2(par, events, data.browser.log(k), light | ghost);

end

%--
% get out for light display
%--

if light
	return;
end

%--
% update event palette for events in page display
%--

% NOTE: we try to skip over code as fast as possible

%--
% check for open event palette
%--

pal = get_palette(par, 'Event', data);

if ~isempty(pal)

	%--
	% check page events only control
	%--

	% NOTE: this control is currently not available
	
	[ignore, value] = control_update([], pal, 'page_events_only', [], data);

	%--
	% update event palette display
	%--

	% TODO: consider we know displayed events for efficiency

	if value
		update_find_events(par, [], data);			
	end

end
