function [active, par] = get_active_filters(par, data)

% get_active_filters - get currently active filters
% -------------------------------------------------
%
% active = get_active_filters(par, data)
%
% Input:
% ------
%  par - parent browser
%  data - browser data
%
% Output:
% -------
%  active - filter extensions and context

% TODO: the packing in this function causes is strange, refactor this

%--
% set and check browser input
%--

if ~nargin
	par = get_active_browser;
end

if ~is_browser(par)
	error('Input handle is not browser handle.'); 
end

%--
% get browser userdata if needed
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par); 
end

%--
% get active filters
%--

[active.signal_filter, ignore, active.signal_context] = get_active_extension('signal_filter', par, data);

[active.image_filter, ignore, active.image_context] = get_active_extension('image_filter', par, data);


