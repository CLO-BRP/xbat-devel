function set_browser_page(par, mode, varargin)

% set_browser_page - set browser page properties
% ----------------------------------------------
%
% set_browser_page(par, mode, 'field', 'value', ...)
%
% Input:
% ------
%  par - parent
%  mode - time mode
%  field - field name: 'time', 'duration', or 'overlap'
%  value - field value
%
% Output:
% -------
%  time - slider time
%  duration - page duration

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% get active browser if needed and possible
%--

if isempty(par)
	par = get_active_browser;
end

if isempty(par)
	return;
end
	
%--
% set real time as default time mode
%--

if isempty(mode)
	mode = 'real';
end

if ~ismember(mode, time_modes)
	error(['Unrecognized sound time mode ''', mode, '''.']);
end

%--
% get field value pairs
%--

[field, value] = get_field_value(varargin);

%-----------------------
% SET PAGE
%-----------------------

%--
% stop daemon and get browser data
%--

stop(scrolling_daemon);

data = get_browser(par);

%--
% set overlap
%--

ix = find(strcmp('overlap', field));

if ~isempty(ix)
	data = set_overlap(par, value{ix}, data);
end

%--
% set duration
%--

ix = find(strcmp('duration', field));

if ~isempty(ix)
	data = set_duration(par, value{ix}, data);
end

%--
% set time
%--

ix = find(strcmp('time', field));

% TODO: figure out why we are not updating the browser state in this function

if ~isempty(ix)
	data = set_time(par, mode, value{ix}, data); %#ok<NASGU>
end

%-----------------------
% UPDATE DISPLAY
%-----------------------

%--
% refresh browser display
%--

% TODO: implement skip on no change

% NOTE: calling refresh here, suggests refresh keep track of 'undo'

browser_refresh(par);

%--
% start daemon
%--

daemon = scrolling_daemon;

if ~strcmp(daemon.running, 'on')
	start(scrolling_daemon);
end


%--------------------------------
% SET_TIME
%--------------------------------

function [data, change] = set_time(par, mode, time, data)

%--
% handle input
%--

if ischar(time)
	time = clock_to_sec(time);
end

% NOTE: it is not clear that the error message is correct

if isempty(time)
	error('Improper time string');
end

%--
% map time
%--

time = map_time(data.browser.sound, 'slider', mode, time);

%--
% set time if needed 
%--

% NOTE: the output data may be stale on change

change = (time ~= data.browser.time);

if change
	set_time_slider(par, 'value', time); data.browser.time = time;
end


%--------------------------------
% SET_DURATION
%--------------------------------

function [data, change] = set_duration(par, duration, data)

%--
% handle input
%--

if ischar(duration)
	duration = clock_to_sec(duration);
end

if isempty(duration)
	error('Improper time string');
end

%--
% check for change
%--

change = (data.browser.page.duration ~= duration);

if ~change
	return;
end

%--
% get current time and total sound duration
%--

time = data.browser.time;

total = get_sound_duration(data.browser.sound);

%--
% update time if current time plus page exceeds total duration
%--

% full = (data.browser.page.duration == total);

if duration > total
	duration = total;
end

if (time + duration) > total
	data = set_time(par, 'real', total - duration, data);
end

%--
% update state
%--

data.browser.page.duration = duration;

data = update_specgram_param(par, data, 1);

update_time_slider(par, data);

%--
% update page palette if needed
%--

pal = get_palette(par, 'Page');

if ~isempty(pal)
	set_control(pal, 'Duration', 'value', duration);
end

%--
% update menus
%--

% NOTE: this is really old stuff, these menus are rarely used

handles = get(get_menu(par, 'Page Duration'), 'children');

set(handles, 'check', 'off');

ix = find(duration == [1, 2, 3, 4, 10, 20, 30, 60]);

if ~isempty(ix)
	set(handles(ix), 'check', 'on');
else
	set(handles(end), 'check', 'on');
end


%--------------------------------
% SET_OVERLAP
%--------------------------------

function [data, change] = set_overlap(par, overlap, data)

%--
% handle input
%--

change = (data.browser.page.overlap ~= overlap);

if ~change
	return;
end

%--
% update userdata
%--

data.browser.page.overlap = overlap;

set(par, 'userdata', data);

update_time_slider(par, data);

%--
% update controls
%--

pal = get_palette(par, 'Page', data);

if ~isempty(pal)
	set_control(pal, 'Overlap', 'value', overlap);
end

%--
% update menus
%--

% NOTE: this is really old stuff, these menus are rarely used

handles = data.browser.view_menu.page_overlap;

set(handles, 'check', 'off');

ix = find(overlap == [0, 1/2, 1/4, 1/8]); % change these to the actual

if ~isempty(ix)
	set(handles(ix), 'check', 'on');
else
	set(handles(end), 'check', 'on');
end




