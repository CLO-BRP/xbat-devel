function [ext, ix, context] = get_cached_browser_extension(session, type, par, name)
	
% get_cached_browser_extension - cached get of browser extension
% --------------------------------------------------------------
%
% [ext, ix, context] = get_cached_browser_extension(session, type, par, name)
%
% Input:
% ------
%  session - session name

%--
% declare and initialize cache 
%--

persistent BROWSER_EXTENSION;

if isempty(BROWSER_EXTENSION) || ~nargin
	
	BROWSER_EXTENSION = struct;
	
	if ~nargin
		return;
	end
	
end

%--
% clear session if requested
%--

if nargin == 1
	
	if isfield(BROWSER_EXTENSION, session)
		BROWSER_EXTENSION = rmfield(BROWSER_EXTENSION, session); return;
	end
	
end

%--
% check for session in cache using key
%--

key = get_request_key(type, par, name);

if ~isfield(BROWSER_EXTENSION, session)

	%--
	% initialize session when it does not exist and store first extension
	%--
	
	[ext, ix, context] = get_browser_extension(type, par, name);
	
	BROWSER_EXTENSION.(session).(key) = {ext, ix, context};
	
	return;

end

%--
% check for session extension in cache
%--

if isfield(BROWSER_EXTENSION.(session), key)

	result = BROWSER_EXTENSION.(session).(key);

	ext = result{1}; ix = result{2}; context = result{3};

	return;

end

%--
% add extension to cache
%--

[ext, ix, context] = get_browser_extension(type, par, name);

BROWSER_EXTENSION.(session).(key) = {ext, ix, context};


%-----------------------------
% GET_REQUEST_KEY
%-----------------------------

function key = get_request_key(type, par, name)

% NOTE: we could make this faster by skipping 'md5'

key = ['key', md5([type, num2str(par), name])];

