function h = browser_time_nav_bar(g)

% browser_time_nav_bar - time navigation buttons for browser
% ----------------------------------------------------------
%
% h = browser_time_nav_bar(g)
%
% Input:
% ------
%  g - handle to figure to control
%
% Output:
% -------
%  h - handle to toolbar figure

%--
% set figure to control and ensure that it has image_menu
%--

if (nargin < 1)
	g = gcf;
end

%--
% create toolbar
%--

h = figure;

T = {'Move backwards in time (Ctrl+B)', ...
	'Move forward in time (Ctrl+F)', ...
	'Change page duration and overlap' ...
};

button_group(h, ...
	'browser_sound_menu', ...
	'Time Navigation', ...
	{'Previous Page','Next Page','Page Options ...'}, ...
	[],{'B','F',''},g,T);

%--
% close toolbar when controlled figure is closed
%--

close_fun = get(g,'CloseRequestFcn');
set(g,'CloseRequestFcn',['delete(' num2str(h) '); ' close_fun]);
