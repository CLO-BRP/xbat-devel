function types = get_browser_types

% get_browser_types - list of available browser types
% ---------------------------------------------------
%
% types = get_browser_types
%
% Output:
% -------
%  types - browser types

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

types = { ...
	'clip', ...
	'log', ...
	'sound' ...
}';