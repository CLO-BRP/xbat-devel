function color = simple_css_colors

% simple_css_colors - list
% ------------------------
%
% color = simple_css_colors
%
% Output:
% -------
%  color - description struct array

color = load_css_colors; 

color = color([color.parts] == 1);
