function color = complex_css_colors(parts)

% complex_css_colors - list
% ------------------------
%
% color = complex_css_colors
%
% Input:
% ------
%  parts - minimum
%
% Output:
% -------
%  color - description struct array

if nargin < 2
	parts = 2;
end

color = load_css_colors; 

color = color([color.parts] >= parts);
