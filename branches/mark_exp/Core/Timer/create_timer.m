function obj = create_timer(name, callback, rate, mode, varargin)

% create_timer - with given name, singleton
% -----------------------------------------
%
% obj = create_timer(name, callback, rate, mode, varargin)
%
% Input:
% ------
%  name - of timer
%  callback - for timer
%  rate - for timer
%  mode - for execution
% 
% Output:
% -------
%  obj - timer

%--
% handle input
%--

if ~nargin || isempty(name)
	error('Singleton timer must be named.');
end

if nargin < 4 || isempty(mode)
	mode = 'fixedRate';
end

if nargin < 3 || isempty(rate)
	rate = 1;
end

if nargin < 2 || isempty(callback)
	callback = @tracer;
end

%--
% check for timer, create if needed
%--

obj = timerfind('name', name);

% TODO: warn if requested properties different

if ~isempty(obj)
	return;
end

obj = timer;

set(obj, ...
	'name', name, ...
	'executionmode', mode, ...
	'busymode', 'drop', ...
	'period', rate, ...
	'timerfcn', callback, ...
	varargin{:} ...
);

%-----------------------
% TRACER
%-----------------------

function tracer(obj, eventdata)

db_disp; disp(obj); disp(eventdata);



