function note = note_create(varargin)

%--
% initialize note
%--

persistent NOTE;

if isempty(NOTE)
	
	NOTE.title = ''; 
	
	NOTE.body = ''; 

end

note = NOTE;

if ~nargin
	return;
end

%--
% set note fields from input
%--
	
note = parse_inputs(note, varargin{:});