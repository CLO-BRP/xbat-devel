function package_xbat(location, revision, destination, platform)

% package_xbat - export and clean up the xbat working copy for distribution
% -------------------------------------------------------------------------
%
% package_xbat(location, revision, destination)
%
% Input:
% ------
%  location - working copy or URL (def: xbat_root)
%  revision - revision number (def: HEAD)
%  destination - where to put the package (def: xbat_root/Package)
%  platform - the platform to package for

%--
% handle input
%--

if nargin < 4
    platform = computer;
end

if nargin < 3 || isempty(destination)
    destination = fullfile(xbat_root, 'Package');
end

if nargin < 2
    revision = [];
end

if ~nargin || isempty(location)
    location = xbat_root;
end

if ~ischar(revision)
    revision = int2str(revision);
end

%--
% clean up current destination directory
%--

if exist(destination, 'dir') == 7
	
	disp('Cleaning up existing package.');
	
	rmdir(destination, 's');
	
end

%--
% export
%--

str = ['"', location, '"', ' "', destination, '"'];

if ~isempty(revision)
    str = ['-r ', revision, str];
end

disp('Exporting SVN tree.  This may take some time ...');

[status, result] = svn('export', str);

disp(result);

if status
	return;
end

%-------------------------
% CLEAN UP
%-------------------------

disp('Cleaning up.');

%--
% remove not-needed root-level directories
%--

not_needed = { ...
    'Samples', ...
    'Tools', ...
    'Toolboxes', ...
    'Sites' ...
};

for k = 1:length(not_needed) 
    rmdir(fullfile(destination, not_needed{k}), 's');
end
    
%--
% get annoying 'svn_info' file
%--

copyfile( ...
    fullfile(xbat_root, 'Core', 'Subversion', 'svn_info.m'), ...
    fullfile(destination, 'Core', 'Subversion', 'svn_info.m') ...
);

%--
% remove mex-files from core functions
%--

scan_dir(fullfile(destination, 'Core'), @delete_mex); 

disp('Packaging complete.');


%------------------------
% DELETE_MEX
%------------------------

function out = delete_mex(p)

out = [];

ext = {'mexw32', 'mexglx', 'mexmac', 'dll'};

for k = 1:length(ext)
	delete(fullfile(p, '*.', ext{k}));
end

