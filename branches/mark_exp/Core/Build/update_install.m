function update_install(file)

%--
% setup
%--

persistent ROOT1 ROOT2;

if isempty(ROOT1)
	
	ROOT1 = xbat_root; 
	
	ROOT2 = create_dir(fullfile(xbat_root, 'MEX', [upper(computer), '_', mexext]));

	if isempty(ROOT2)
		error('Unable to create install root.');
	end
	
end

%--
% append file move commands to file
%--

for k = 1:length(file)
	
	output = strrep(file{k}, ROOT1, ROOT2); 
	
	if isempty(create_dir(fileparts(output)))
		continue;
	end
	
	try
		copyfile(file{k}, output);
	catch
		continue;
	end
	
end
