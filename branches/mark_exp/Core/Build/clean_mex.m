function dirs = clean_mex(root)

% clean_mex - remove MEX files from their private locations
% ---------------------------------------------------------
%
% dirs = clean_mex(root)
%
% Input:
% ------
%  root - scan root
%
% Output:
% -------
%  dirs - directories cleaned

% NOTE: this function will actually clean all but a set of excluded extensions 

if ~nargin
    root = xbat_root();
end

%--
% get MEX directories
%--

dirs = get_dirs(root, 'MEX');

%--
% clean corresponding 'private' directories
%--

for k = 1:length(dirs)
	
	target = fullfile(fileparts(dirs{k}), 'private');
	
    clean(dirs{k});
    
% 	disp(' '); disp(['At ', target]);
	
	if exist(target, 'dir')
		clean(target);
	end
	
end


%----------------------
% CLEAN
%----------------------

function clean(root)

%--
% set extensions to exclude from cleaning
%--

exclude = {'.c', '.h', '.m', '.mat', '.txt'};

%--
% get directory contents and delete non-excluded files (not other directories)
%--

content = no_dot_dir(root);

for k = 1:length(content)
	
	%--
	% skip other directories and excluded files
	%--
	
	if content(k).isdir
		continue;
	end 
	
	[ignore, ext] = strtok(content(k).name, '.');
	
	if ismember(ext, exclude)
		continue;
	end
	
	%--
	% delete file
	%--
	
	file = fullfile(root, content(k).name);
	
	try
% 		disp(['  Deleting ''', content(k).name, ''' ...']); 
		delete(file);
	catch
		
	end
	
end