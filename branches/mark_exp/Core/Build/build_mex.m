function [status, result] = build_mex(files, libs, opt)

% build_mex - build a mex file from C source using MinGW
% ------------------------------------------------------
% output = build_mex(files, libs, opt)
%
% Input:
% ------
%  files - cell array or space-separated string list of source files
%  libs - cell array or space-separated string list of library dependancies
%  opt - options structure, def: returned by "opt = build_mex()"
%
% Output:
% -------
%  status - numeric exit code from gcc
%  result - result from gcc

%--
% handle input
%--

if nargin < 3
    opt.prefix = libs_root(); opt.cflags = ''; opt.ldflags = ''; opt.outname = ''; opt.verbose = 1;
end

if nargin < 2 || isempty(libs)
    libs = [];
end

if ~nargin  
    status = opt; result = []; return;  
end

%--
% setup
%--

if ~iscell(files)
    files = str_split(files);
end

if ~iscell(libs)
    libs = str_split(libs);
end

if ispc && isempty(mingw_root)
	error('Unable to find MinGW in "%s".', mingw_root);
end

[def_files, mex_libs] = create_import_libs(fullfile(opt.prefix, 'lib'));

%--
% get compiler
%--

gcc = get_tool('gcc.exe');

if isempty(gcc)
    error('''get_tool'' can''t find gcc.');
end

%--
% build compiler command
%--

name = [ternary(isempty(opt.outname), file_ext(files{1}), opt.outname), '.', mexext];

clear(name);

include_paths = { ...
    '.', ...
    fullfile(opt.prefix, 'include'), ...
    fullfile(matlabroot, 'extern', 'include') ...
};

lib_paths = {fullfile(opt.prefix, 'lib')};

command = [ ...
    gcc.file ' -shared -o ', name, ' ', str_implode(files), ' -DMATLAB_MEX_FILE ', ...
    str_implode(opt.cflags), ' ', str_implode(opt.ldflags), ' ', ...
    gcc_search_path(include_paths, '-I'), gcc_search_path(lib_paths, '-L'), ...
    gcc_lib_shortcut(mex_libs), gcc_lib_shortcut(libs) ...
];

if opt.verbose
    disp(command);
end

%--
% run it
%--

[status, result] = system(command);

if status && nargout < 2
	
	disp(' ');
	
	lines = file_readlines(result);
	
	for k = 1:numel(lines)
		disp(['   ', lines{k}]);
	end
	
	if ~nargout 
		disp(' '); clear status result;
	end
	
end


%--------------------------------------------------------
% PARSE_RESULT
%--------------------------------------------------------

% TODO: this will likely depend on the compiler, keep up to date

function str = parse_result(line)

part = str_split(line, ':');

file = part{1};

str = '';


%--------------------------------------------------------
% GCC_LIB_SHORTCUT 
%--------------------------------------------------------

% NOTE: a library linking shorthand for a cell array of libraries

function str = gcc_lib_shortcut(libs)

if ~iscell(libs)
    libs = str_split(libs);
end

str = '';

for k = 1:length(libs)
    [p, n] = fileparts(libs{k}); str = [str, strrep(n, 'lib', '-l'), ' '];
end


%--------------------------------------------------------
% GCC_SEARCH_PATH
%--------------------------------------------------------

% NOTE: get gcc switch for lib or include search path

function str = gcc_search_path(paths, type)

if ~iscell(paths)
    paths = str_split(paths);
end

str = '';

for k = 1:length(paths)
    str = [str, type, '"', paths{k}, '" '];
end

