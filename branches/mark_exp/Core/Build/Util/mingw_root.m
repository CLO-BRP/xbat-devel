function root = mingw_root(in)

% mingw_root - set and get root of mingw compiler
% -----------------------------------------------
%
% root = mingw_root(root)
%
% Input:
% ------
%  root - root to set
%
% Output:
% -------
%  root - value of mingw root

%--
% handle set, get, and error
%--

switch nargin

	%--
	% get root
	%--

	case 0

		%--
		% get environment variable and try to set default if empty
		%--
		
		root = get_env('mingw_root');
		
		if isempty(root)
			
			% NOTE: this is the recommended installation path
			
			% TODO: consider checking for the default 'C:\' install path
			
			root = [xbat_root, filesep, 'Tools', filesep, 'MinGW'];
			
			if exist(root,'dir')
				set_env('mingw_root', root);
			else
				root = '';
			end
			
		end
		
		if isempty(root)
			
			root = 'C:\MinGW';
			
			if exist(root, 'dir') == 7
				set_env('mingw_root', root);
			else
				root = '';
			end
			
		end

	%--
	% set root
	%--

	% NOTE: this works like set verify

	case 1

		%--
		% check input 
		%--
		
		if ~ischar(in)
			error('Proposed path must be string.');
		end
		
		if ~exist(in, 'dir')
			error('Proposed path does not seem to exist.');
		end
		
		%--
		% set path
		%--
		
		root = in; set_env('mingw_root', root);
		
	%--
	% error
	%--

	otherwise, error('Improper number of input arguments.');

end
