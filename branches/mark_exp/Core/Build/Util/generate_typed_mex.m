function result = generate_typed_mex(source, type, terse)

% generate_typed_mex - code from template code
% --------------------------------------------
%
% result = generate_typed_mex(source, type, terse)
%
% Input:
% ------
%  source - template file
%  type - description for replacement
%  terse - output without comments or empty lines
%
% Output:
% -------
%  result - file info

%--
% handle input
%--

if nargin < 3
	terse = true;
end

% TODO: this is shared with 'generate_typed_cu', factor eventually

if nargin < 2
	type.name = { ...
		'int8', 'int16', 'int32', 'int64', ...
		'uint8', 'uint16', 'uint32', 'uint64', ...
		'single', 'double' ...
	};
	
	type.type = { ...
		'char', 'short int', 'int', 'long long int', ...
		'unsigned char', 'unsigned short int', 'unsigned int', 'unsigned long long int', ...
		'float', 'double' ...
	};
	
	if ~nargin
		result = type; return;
	end
end

%--
% check for template source file
%--

[root, file, ext] = fileparts(source);

if ~strcmp(ext, '.template')
	error('Input source file should be a template file.');
end

if isempty(root)
	source = which(source); root = fileparts(source);
end

% NOTE: we could check whether file ends in .cpp

file = fullfile(root, file); 

%--
% generate file with properly typed code starting from template
%--

lines = file_readlines(source);

within = false; out = get_fid(file, 'wt'); block = {};

for k = 1:numel(lines)
	current = lines{k}; trimmed = strtrim(current);
		
	if ~within
		within = ~isempty(strmatch('// BEGIN-EXPAND-TYPES', trimmed));
		
		if terse && (isempty(trimmed) || ~isempty(strmatch('//', trimmed)))
			
		else
			fprintf(out.fid, '%s\n', current);
		end
		
		if within, continue; end
	end
	
	if ~isempty(strmatch('// END-EXPAND-TYPES', trimmed))
		% NOTE: expand block for each requested type
		
		for j = 1:numel(type.type)
			% NOTE: perform template type replacements for each line in block
			
			for i = 1:numel(block)
				fprintf(out.fid, '%s\n', type_replace(block{i}, type.name{j}, type.type{j}));
			end
		end
		
		within = false; block = {};
	end
	
	if within
		if terse && (isempty(trimmed) || ~isempty(strmatch('//', trimmed)))
		
		else
			block{end + 1} = current; %#ok<AGROW>
		end
		
		continue; 
	end
end
	
fclose(out.fid);

%--
% return filesystem info for file
%--

result = dir(file); result.root = root;


%--------------------
% TYPE_REPLACE
%--------------------

function line = type_replace(line, name, type)

line = strrep(line, 'MEX_TYPE_NAME', name);

line = strrep(line, 'MEX_TYPE_CLASS', ['mx', upper(name), '_CLASS']); 

line = strrep(line, 'MEX_TYPE', type);

