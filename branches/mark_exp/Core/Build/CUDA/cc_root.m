function root = cc_root(root)

% cc_root - where the C compiler tools live
% -----------------------------------------
%
% root = cc_root
%
% Output:
% -------
%  root - directory

persistent ROOT

% NOTE: we set the root when it is empty or when we are asked to

if isempty(ROOT) || nargin
	if nargin
		ROOT = root;
	else
		% NOTE: these are the default installation directories
		
		if ispc
			ROOT = 'C:\Program Files\Microsoft Visual Studio 9.0\VC\bin';
		else
			[status, ROOT] = system('which gcc');
		end
	end
	
	if ~exist(ROOT, 'dir')
		ROOT = []; error(['Proposed root ''', ROOT, ''' does not exist.']);
	end
end

root = ROOT;

