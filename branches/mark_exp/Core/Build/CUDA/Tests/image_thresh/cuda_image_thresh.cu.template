#include <cuda.h>

extern "C" void cuda_image_thresh_CUDA_TYPE_NAME (CUDA_TYPE *Y, CUDA_TYPE *X, int N, CUDA_TYPE *T, int P, int threadsPerBlock);

// multi_thresh - multiple threshold computation
// ---------------------------------------------
// 
// Input:
// ------
//  *X - input image
//  *T - candidate threshold array
//   P - number of candidate thresholds
//
// Output:
// -------
//  *Y - multiple threshold image

__global__ void multi_thresh_CUDA_TYPE_NAME (
	CUDA_TYPE *Y, CUDA_TYPE *X, CUDA_TYPE *T, int P, int N
)

{
	register int i = blockDim.x * blockIdx.x + threadIdx.x; register int k = 0;
	
	register CUDA_TYPE value;
	
	//--
	// compute multiple threshold image
	//--
	
	// NOTE: code assumes thresholds are ordered (for correctness)
	
	if (i < N)
	{
		value = X[i];
	
		while ((value > *(T + k)) && (k < P)) k++;
	
		Y[i] = k;
	}
}


// cuda_image_thresh - multiple image thresholds computation
// ---------------------------------------------------------

void cuda_image_thresh_CUDA_TYPE_NAME (
	CUDA_TYPE *Y, CUDA_TYPE *X, int N, CUDA_TYPE *T, int P, int threadsPerBlock
)

{
	CUDA_TYPE *Xd, *Yd, *Td;

	int blocksPerGrid;

	// Allocate memory on device
	
	cudaMalloc((void **) &Xd, N * sizeof(CUDA_TYPE) );
	cudaMalloc((void **) &Yd, N * sizeof(CUDA_TYPE) );
	cudaMalloc((void **) &Td, P * sizeof(CUDA_TYPE) );

	// Transfer from host to device
	
	cudaMemcpy(Xd, X, N * sizeof(CUDA_TYPE), cudaMemcpyHostToDevice );
	cudaMemcpy(Td, T, P * sizeof(CUDA_TYPE), cudaMemcpyHostToDevice );
	
	// Compute

	blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;
	multi_thresh_CUDA_TYPE_NAME<<<blocksPerGrid, threadsPerBlock>>>(Yd, Xd, Td, P, N);
	
	// Transfer from device to host
	
	cudaMemcpy(Y, Yd, N * sizeof(CUDA_TYPE), cudaMemcpyDeviceToHost );
	
	// Free memory
	
	cudaFree( Xd ); cudaFree( Yd ); cudaFree( Td );
}
