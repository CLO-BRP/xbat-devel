function root = cuda_root(root)

% cuda_root - where the various tools live
% ----------------------------------------
%
% root = cuda_root
%
% Output:
% -------
%  root - directory

persistent ROOT

% NOTE: we set the root when it is empty or when we are asked to

if isempty(ROOT) || nargin
	if nargin
		ROOT = root;
	else
		% NOTE: these are the default installation directories
		
		if ispc
			ROOT = 'C:\CUDA';
		else
			ROOT = '/usr/local/cuda';
		end
	end
	
	if ~exist(ROOT, 'dir')
		ROOT = []; error(['Proposed root ''', ROOT, ''' does not exist.']);
	end
end

root = ROOT;

