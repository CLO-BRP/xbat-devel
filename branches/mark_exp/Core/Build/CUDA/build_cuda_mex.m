function build_cuda_mex(name, move, varargin)

% build_cuda_mex - relying on various conventions
% -----------------------------------------------
%
% build_cuda_mex(name, move, varargin)
% 
% Input:
% ------
%  name - of parent function
%  move - to private (def: true)
% 
% NOTE: we are expecting our CUDA MEX files to be composed of at least a
% cuda_[name].cu file along with a cuda_[name]_mex.cpp file

if nargin < 2 || isempty(move)
	move = false;
end 

% TODO: output resulting files

setup_vs90;

%--
% handle name or full file input
%--

[root, name] = fileparts(name);

if isempty(root)
	% NOTE: when given a name first look for template file, then look for cpp file
	
	file = which(['cuda_', name, '_mex.cpp.template']);
	
	mex_template = ~isempty(file);
	
	if ~mex_template
		file = which(['cuda_', name, '_mex.cpp']);
		
		if isempty(file)
			error(['Unable to find conventional CUDA MEX file with name ''', name, '''.']);
		end
	end
else
	% NOTE: the last statement trims the conventional prefix and suffix from the name
	
	file = name; [ignore, name, ignore] = fileparts(file); name = name(6:end - 4); %#ok<NASGU,*ASGLU>
end

root = fileparts(file);

%--
% expand template mex file if needed
%--

if mex_template
	generate_typed_mex(file);
	
	[root, file] = fileparts(file); file = fullfile(root, file);
end

%--
% get and process corresponding cu code
%--

template = fullfile(root, ['cuda_', name, '.cu.template']);

if exist(template, 'file')
	generate_typed_cu(template);
end

cu = fullfile(root, ['cuda_', name, '.cu']);

%--
% nvcc build
%--

start = pwd; cd(root);

if exist(cu, 'file')
	% TODO: consider the possiblity of multiple devices, add 'arch' input
	
	% NOTE: having problems on a 1.1 'GeForce 8600M GT' device, building for the architecture has not helped
	
    [ignore, arch] = get_cuda_capability;
    
	[status, result] = system(['nvcc -c -ccbin "', cc_root, '" -arch ', arch, ' "', cu, '"']);
	
	if status
		error(result);
	end
	
	mex('-v', ['-I' fullfile(cuda_root, 'include')], ['-L' fullfile(cuda_root, 'lib')],'-lcudart', file, ['cuda_', name, '.obj'], varargin{:});

%--
% simple build
%--

% NOTE: the main examples here are the MEX files that get CUDA device properties

else
	str = ['mex -v "', file, '" -I', fullfile(cuda_root, 'include'), ' -L', fullfile(cuda_root, 'lib'), ' -lcuda -lcudart'];
	
	eval(str);
end

%--
% test if test is available
%--

test = ['test_cuda_', name, '.m'];

if exist(test, 'file')
	try
		feval(test(1:end - 2));
	catch
		nice_catch; return;
	end
end

%--
% move CUDA MEX to private directory
%--

if move
	out = ['cuda_', name, '_mex.', mexext];
	
	clear(out); movefile(out, '../private/.');
end

cd(start);