function store = log_store(log) 

% HACK: the way we are doing this for SQLite does not make sense, points to a problem

if strcmpi(log.format, 'sqlite')
	
	file = strrep(log_cache_file(log), 'cache.db', 'events.db');

	store.root = fileparts(file); 
	
	store.file = file; 
	
	store.parameter = struct;
	
else
	
	store = log.store;
	
end