function p = log_path(log)

if numel(log) > 1
	p = iterate(mfilename, log); return;
end

p = fileparts(log.file);