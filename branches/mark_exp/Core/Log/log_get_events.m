function event = log_get_events(log, id, full)

% log_get_events - get events from a log by id
% --------------------------------------------
%
% event = log_get_events(log, id, full)
%
% Input:
% ------
%  log - to get events from
%  id - for events
%  full - load indicator
% 
% Output:
% -------
%  event - array
%
% NOTE: in the case of a 'non-full' load extension data will not be loaded

%----------------
% HANDLE INPUT
%----------------

%--
% set full event extraction default
%--

% NOTE: a full event contains extension data

if nargin < 3
    full = 1;
end

%--
% get all events by default
%--

if nargin < 2
    id = [];
end

% NOTE: it is not clear that we should need this, trace it again to see where we are using it

log.store = log_store(log);

%----------------
% GET EVENTS
%----------------

format = get_log_format(log);

%--
% get base events
%--

event = empty(event_create);

reader = format.fun.event.read;

if ~isempty(reader)
	
	try
		event = reader(log.store, id);
	catch
		extension_warning(format, 'Failed to read events.');
	end
	
end

% db_disp 'getting events'; stack_disp; event

%--
% assign id if we were getting all events
%--

if isempty(id)
    id = [event.id];
end

%--
% if we don't need extension data, we're done
%--

if ~full || isempty(event)
    return;
end

%--
% get event extension data for each type
%--

reader = format.fun.event.extension.read; 

if isempty(reader)
	return;
end

% TODO: we probably need a format method asking which extensions have stored data

switch log.format
	
	case 'BARN'

		tables = get_barn_tables(log.store, 'extension'); tables = {tables.name};

		if isempty(tables)
			return;
		end
		
end

types = get_event_extension_data_types;

for j = 1:length(types)
    
	%--
	% get type and class
	%--
	
	type = types{j}; part = str_split(type, '_'); class = part{2};
	
	%--
	% select extensions of type that have stored data
	%--
	
    ext = get_extensions(type);
   
	switch log.format
		
		case 'BARN'
			
			for k = numel(ext):-1:1

				table = extension_table_names(ext(k));

				if ~string_is_member(table.value, tables)
					ext(k) = [];
				end

			end

			if isempty(ext)
				continue;
			end
			
	end
	
    %--
    % read extension data
    %--
	
    for k = 1:length(ext)
		
        try
            [data, event_id] = reader(log.store, id, ext(k));
        catch
            data = []; extension_warning(format, ['Failed to read ', upper(type), ' extension data.']); 
        end
        
        if isempty(data)
            continue;
        end
        
        %--
        % join by extension.event_id = event.id
        %--
		
        for l = 1:length(data)         
			
			ix = find(id == event_id(l), 1);
				
			event(ix).(class).(ext(k).fieldname) = data(l);
			
        end
        
    end
    
end

