function log = new_log(sound, name, format, parameter, lib, user)

% new_log - create new log
% ------------------------
%
% log = new_log(par, name, format, values)
%
%     = new_log(sound, name, format, values, lib, user)
%
% Input:
% ------
%  par - browser handle
%  name - for log
%  format - of log
%  values - to consider as parameters
%  sound - log annotates
%  lib - containing sound
%  user - authoring log
%
% Output:
% -------
%  log - created


%---------------------------------

% TODO: change signature to allow the following calling forms:

% new_log(name), new_log(name, format), new_log(name, format, parameter)

% new_log(name, par), new_log(name, format, par), new_log(name, format, parameter, par)

% NOTE: the format inference from user options; the format parameters from
% default format parameters; the user, library, and sound from current
% active state or current browser info

% NOTE: flexible use of this facility is important as log creation is a
% basic unit used in sound annotation

%---------------------------------


%------------------
% HANDLE INPUT
%------------------

%--
% set active user default
%--

if nargin < 6
	user = get_active_user;
end

%--
% consider first input to discriminate two calling forms
%--

if ishandle(sound) 
	
	par = sound; 
	
	if ~is_browser(par)
		error('Input handle must be a browser handle.');
	end
	
	sound = get_browser(par, 'sound');
	
	info = get_browser_info(par);
	
	lib = get_library_from_name(info.library);
	
else
	
	par = get_active_browser;
	
	if nargin < 5 || isempty(lib)
		lib = get_active_library;
	end
	
end

%--
% set default format 
%--

if nargin < 3 || isempty(format)
    format = user.prefs.new_log_format;
end

%--
% set default name or check name
%--

if nargin < 2 || isempty(name)
    name = 'New_Log';
end

if ~proper_filename(name)
	error('Log name should be proper filename.');
end

%--
% check whether a log with this name already exists
%--

log = empty(log_create);

if exist_log(name, sound, lib)
    return;
end

%------------------
% SETUP
%------------------

%--
% get log format extension
%--

[ext, context] = get_log_format(format);

context.sound = sound; 

context.parent = par;

%--
% check for needed parameters if needed
%--

if nargin < 4 || isempty(parameter)

	if has_any_parameters(ext)
		error('Need parameters to create new log.');
	else
		parameter = struct;
	end
	
end

%------------------
% CREATE NEW LOG
%------------------

%--
% create log struct
%--

log = log_create(sound, 'name', name, 'format', format, 'author', user.name);

context.log = log;

%--
% create and save to log store
%--

create = ext.fun.store.parameter.create;

if ~isempty(create)
	
	try
		default = create(context);
	catch
		extension_warning(ext, 'Failed to create store parameters.');
	end
	
	parameter = struct_update(default, parameter);
	
end 

compile = ext.fun.store.parameter.compile;

if ~isempty(compile)
	
	try
		[parameter, context] = compile(parameter, context);
	catch
		extension_warning(ext, 'Failed to compile store parameters.');
	end
	
end 

% NOTE: some part of the system should check for the availability of create

create = ext.fun.store.create;

try
    log.store = create(name, parameter, context);
catch
    extension_warning(ext, 'Failed to create store.', lasterror); return;
end

%--
% save log
%--

log.file = log_file(name, context.sound, context.library);

create_dir(log_path(log));

log_save(log);
