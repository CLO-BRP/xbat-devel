function [event, position, ix] = get_events_by_page(log, page, channel, light)

% get_events_by_page - select events from logs by page
% ---------------------------------------------------
%
% [events, position, log] = get_events_by_page(log, page, channel, light)
%
% Input:
% ------
%  log - logs
%  page - page
%  channel - channels
%  light - display indicator
%
% Output:
% -------
%  event - event array
%  position - in-page indicator matrix for event boundaries
%  log - log index array

% NOTE: the in-page indicator matrix is binary with event row index and start and stop column indices

% NOTE: when start is not in-page, the event starts before; when stop is not, the event ends after

% NOTE: relate and possibly reconsider 'event_in_page'

%---------------
% HANDLE INPUT
%---------------

if nargin < 4
	light = 0;
end

%--
% set no channel selection default
%--

if nargin < 3
	channel = [];
end

%--
% handle multiple logs with iterator
%--

persistent LOG_IX;

if numel(log) > 1
	
	LOG_IX = 1; [event, position, ix] = iterate(mfilename, log, page, channel); LOG_IX = []; return; %#ok<NASGU>

end

%---------------
% SELECT EVENTS
%---------------

%--
% select events from cache
%--

% NOTE: the cache will be created if it does not exist

% NOTE: in the case of a light display we get basic events here, otherwise we just get identifiers

[status, result] = sqlite(log_cache_file(log), get_page_query(page, channel, light));

% NOTE: return quickly on empty 

if isempty(result)
	event = empty(event_create); position = [nan, nan]; ix = []; return;
end

%--
% get events from basic cache events or full events from log
%--

% TODO: the problem here is a discrepancy between the cache and log

% NOTE: the previous query talked to the cache, as well as the 'light' call

if light
	event = get_full_event(result);
else
	event = log_get_events(log, [result.id]);
end

% TODO: reconsider output for empty event, examine callers

if isempty(event)
	position = [nan, nan]; ix = []; return;
end

%--
% get position
%--

position = struct_field(event, 'time');

position(:, 1) = position(:, 1) > page.start; 

position(:, 2) = position(:, 2) < page.start + page.duration;

%--
% get log index
%--

ix = ones(size(event));

if isempty(LOG_IX)
    return;
end

ix = LOG_IX * ix;

LOG_IX = LOG_IX + 1;


%----------------------
% GET_PAGE_QUERY
%----------------------

function sql = get_page_query(page, channel, light)

%--
% build time selection part of query
%--

start = num2str(page.start); stop = num2str(page.start + page.duration);

% NOTE: for 'light' display get the basic event from cache, otherwise get identifiers and full events from log

if light
	what = '*';
else
	what = 'id';
end 

% NOTE: the second clause of the WHERE considers the case of markers

sql = [ ...
	'SELECT ', what, ' FROM event WHERE ', ...
	' (event.start + event.duration > ', start, ' AND event.start < ', stop, ')' ...
	' OR', ...
	' (event.duration is NULL AND event.start > ', start, ' AND event.start < ', stop, ')' ...
];

%--
% append channel selection to query
%--
			
switch numel(channel)
	
	case 0
		sql = [sql, ';'];

	case 1
		sql = [sql, ' AND event.channel = ', int2str(channel), ';'];
	
	otherwise
		sql = [sql, ' AND event.channel IN (', str_implode(channel, ', ', @int2str), ');'];

end
	




