function log_delete_events(log, id)

% log_delete_events - delete events in a log by id 
% ------------------------------------------------
%
% log_delete_events(log, id)
%
% Input:
% ------
%  log - XBAT log structure
%  id - a list of event ids

%--
% handle input
%--

if isempty(id)
    return;
end

if iscellstr(id)
    
    ids = zeros(size(id));
    
    for k = 1:length(id)
        ids(k) = str2double(id(k));
    end
    
    id = ids;
    
end

if ischar(id)
    id = str2double(id);
end

%--
% update log
%--

format = get_log_format(log);

try
    format.fun.event.delete(log.store, id);
catch
    extension_warning(format, 'Failed to delete events.');
end

%--
% update cache
%--

log_cache_update(log, 'delete', id);

%--
% update event palette
%--

par = log_is_open(log);

if ~isempty(par)
   
    pal = get_palette(par, 'Event');
    
    if ~isempty(pal)      
        control_callback(par, pal, 'find_events');     
    end
    
end
    
    



