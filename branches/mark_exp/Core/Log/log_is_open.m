function [par, handle] = log_is_open(log, handle)

% log_is_open - try to see whether log is open
% --------------------------------------------
%
% par = log_is_open(log, handle)
%
% Input:
% ------
%  log - log
%  handle - candidates
%
% Output:
% -------
%  par - open log parent
%  handle - candidates 

%--
% set figures to check
%--

% NOTE: at the moment we only check the sound browser windows, this could change

if nargin < 2
	handle = get_xbat_figs('type', 'sound');
end

%--
% return if there are no candidate windows open
%--

par = []; 

if isempty(handle)
	return;
end

%--
% loop over figures checking for log
%--
	
for k = 1:length(handle)
	
	%--
	% get open logs from current browser
	%--
	
	logs = get_browser_logs(handle(k));
	
	% NOTE: no open logs nothing to check
	
    if isempty(logs)
        continue;
    end
		
    %--
    % check sound and logs names
    %--

	if ~strcmp(sound_name(log.sound), sound_name(logs(1).sound))
		continue;
	end
	
	if ~any(strcmp(log_name(logs), log_name(log)));
		continue;
	end

	par = handle(k); return;
		
end
	
