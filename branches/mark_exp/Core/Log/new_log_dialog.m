function log = new_log_dialog(par, name)

% new_log_dialog - dialog to create new logs
% ------------------------------------------
%
% log = new_log_dialog(par, name)
%
% Input:
% ------
%  par - browser
%  name - of log
%
% Output:
% -------
%  log - log

%--
% set persistent cache for last dialog values
%--

persistent LAST_VALUES

if isempty(LAST_VALUES)
	LAST_VALUES = struct; 
end

%------------------------------
% HANDLE INPUT AND SETUP
%------------------------------

%--
% set default empty name
%--

if nargin < 2 || isempty(name)
	name = '';
end 

%--
% get browser if needed
%--

if ~nargin
	par = get_active_browser;
end

% NOTE: we are unable to get required information without browser

if isempty(par)
	return;
end

%--
% get relevant information from browser
%--

info = get_browser_info(par);

lib = get_library_from_name(info.library);

sound = get_library_sounds(lib, 'name', info.sound);

% NOTE: we need a sound to create a new log, but this is not thorough in checking for faults

log = empty(log_create);

if isempty(sound)
	return;
end

%------------------------------
% CREATE CONTROLS
%------------------------------

%--
% create base controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', ['Log  (', sound_name(sound), ')'] ...
);

control(end + 1) = control_create( ...
	'name', 'name', ...
	'space', 1, ...
	'style', 'edit', ...
	'type', 'filename', ... 
	'space', 0.5, ...
	'string', name, ...
	'onload', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'tags', ...
	'style', 'edit', ...
	'string', '', ...
	'space', 1.5, ...
	'color', ones(1,3) ...
);

%--
% get format specific controls
%--

% NOTE: we get the log format extension based on user preference

[names, ix] = get_log_format_names; 

[format, context] = get_log_format(names{ix});

if has_any_presets(format)
	
	% NOTE: here we create controls for any controllable parameters

	specific = get_extension_controls(format, context);
	
	if ~isempty(specific)	
		control(end).space = 1.5;
	end
	
	% NOTE: this makes the leading header non-collapsible, consider if we want header
	
% 	tabs = {format.name, 'Metadata'};
	
	tabs = {format.name};
	
	control(end + 1) = control_create('style', 'separator', 'space', 0.1);
	
	control(end + 1) = control_create( ...
		'style', 'tabs', ...
		'tab', tabs ...
	);
	
	specific(1) = [];
	
	for k = 1:length(specific)
		specific(k).tab = tabs{1}; control(end + 1) = specific(k);
	end
	
	% NOTE: separate controls from dialog buttons
	
	control(end).space = 1.5;
	
	%--
	% tags tab controls
	%--

% 	control(end + 1) = control_create( ...
% 		'name', 'tags', ...
% 		'style', 'edit', ...
% 		'string', '', ...
% 		'tab', tabs{2}, ...
% 		'space', 0.75, ...
% 		'color', ones(1,3) ...
% 	);
% 
% 	control(end + 1) = control_create( ...
% 		'name', 'notes', ...
% 		'style', 'edit', ...
% 		'string', '', ...
% 		'tab', tabs{2}, ...
% 		'color', ones(1,3), ...
% 		'lines', 4, ...
% 		'space', 1.5 ...
% 	);
	
	%--
	% check persistent cache for previous control values
	%--
	
	key = genvarname(format.name);
	
	if isfield(LAST_VALUES, key)
		control = update_control_values(control, LAST_VALUES.(key));
	end

end

%------------------------------
% CREATE DIALOG
%------------------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 12;

% NOTE: consider using different color depending on where we are called from

opt.header_color = get_extension_color('sound_browser_palette');

opt.text_menu = 1;

opt.ext = format;

name = 'New ...';

%--
% create dialog
%--

out = dialog_group(name, control, opt, {@new_log_callback, format}, par);

if strcmpi(out.action, 'cancel')
	return;
end

if exist('key', 'var')
	LAST_VALUES.(key) = out.values;
end

%--
% create new log
%--

% NOTE: log creation uses the active user, so it is the same for tags

% TODO: these assumptions are obscure and should be clearer

log = new_log(par, out.values.name, format.name, out.values);

if ~isempty(out.values.tags)
	
	set_taggings(log.store, log.store.log, out.values.tags, get_barn_user(log.store), 'log');
	
end

% TODO: possibly create a 'creation' note, we should use 'machine titles' for notes

%--
% consider opening of new log
%--

if ~isempty(par)
	log_open(par, log);
end

%---------------------------------
% UPDATE VISIBLE STAT
%---------------------------------

% TODO: we need to make this operatio fast so we can call it

%--
% update XBAT palette if available
%--

xbat_palette('find_sounds');


%------------------------------
% NEW LOG CALLBACK
%------------------------------

function result = new_log_callback(obj, eventdata, format) %#ok<INUSD>

result = struct;

%--
% get callback context and control value
%--

callback = get_callback_context(obj, 'pack');

%--
% switch callback on control name
%--

% NOTE: we only handle the log name here, otherwise we yield to the format callback

switch callback.control.name
	
	case 'name'
		
		set_control(callback.pal.handle, 'OK', 'enable', proper_filename(get(obj, 'string')));
		
	otherwise
		
		fun = format.fun.store.parameter.control.callback;
		
		if ~isempty(fun)
			
			%--
			% get format context
			%--

			par = get_active_browser;

			[ext, ignore, context] = get_browser_extension(format.type, par, format.name);
			
			%--
			% yield to callback
			%--
			
			try
				result = fun(callback, context);
			catch
				extension_warning(format, 'Control callback failed.', lasterror);
			end
			
		end
		
end




