function log = log_create_in(type, varargin)

if nargin < 1 || isempty(type)
    type = 'log';
end

%--
% create empty log strucuture
%--

log.type = type;

% at the moment there are logs out there with no version field and with
% version field and equal to 1. this should be enough to discriminate older 
% logs and apply an update.

log.version = xbat_version;

%--------------------------------
% PRIMITIVE FIELDS
%--------------------------------

%--
% id for log
%--

rand('state',sum(100*clock));

log.id = round(rand * 10^16);

%--
% location of log file
%--

log.path = [];

log.file = [];

%--
% other fields
%--

log.author = '';

log.created = now;

log.modified = [];

%--------------------------------
% DATA AND METADATA FIELDS
%--------------------------------

%--
% sound annotated in log
%--

log.sound = [];

log.annotation = annotation_create;

%--
% events contained in log
%--

event = empty(event_create);

log.event = event;

log.deleted_event = event;

%--
% selected event subsets
%--

% create a structure to represent event selections

log.selected_events = selected_create;

%--
% extent fields
%--

log.length = 0;

log.curr_id = 1;

log.channel = [];

log.time = [];

log.freq = [];

log.duration = [];

log.bandwidth = [];

%--------------------------------
% METADATA FIELDS
%--------------------------------

log.measurement = measurement_create;	% log measurements

log.annotation = annotation_create;		% log annotations

log.generation = [];					% log generation summary, extension computation summaries

%--------------------------------
% DISPLAY FIELDS
%--------------------------------

log.visible = 1;

log.color = color_to_rgb('Green');

log.linestyle = linestyle_to_str('Solid');

log.linewidth = 2;

log.patch = -1;

log.event_id = 1;

%--------------------------------
% DOCUMENT INTEGRITY FIELDS
%--------------------------------

log.open = 0;

log.readonly = 0;

log.autosave = 1;

log.autobackup = 1;

log.saved = 0;

%--
% userdata field
%--

log.userdata = [];

%-------------------------------
% USE INPUT
%-------------------------------

if ~size(varargin)
    return;
end

log = parse_inputs(log, varargin{:});
    


