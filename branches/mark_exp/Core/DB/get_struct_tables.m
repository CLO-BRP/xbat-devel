function tables = get_struct_tables(in, name, parent)

% get_struct_tables - get struct of struct arrays that map to tables
% ------------------------------------------------------------------
%
% tables = get_struct_tables(in)
%
% Input:
% ------
%  in - struct
%
% Output:
% -------
%  tables - struct of struct arrays that map to tables

% NOTE: this function suggests measures perhaps be stored in struct fields rather than an array

% NOTE: the field can be the normalized name, consider moving to 'genvarname'

%--------------------
% HANDLE INPUT
%--------------------

%--
% check input
%--

if ~isstruct(in)
    error('Input must be struct.');
end

%--
% set default name and parent name
%--

if nargin < 2
    name = inputname(1);
end

if isempty(name)
	error('Table name is not available from input, explicitly or implicitly.');
end

if nargin < 3
    parent = '';
end

%--
% set up parent id field name
%--

% NOTE: if we don't have a parent, we need an 'id' primary key

id = 'id';

% NOTE: when we have a parent, we need a 'parent_id' foreign key

if ~isempty(parent)
    id = [parent, '_', id];
end

% NOTE: the second command 'deals' the cell array into the struct array field

if ~isfield(in, id)  
    values = num2cell(1:length(in)); [in(:).(id)] = values{:};   
end

%--
% iterate over input fields
%--

tables = struct; field = fieldnames(in);

for k = 1:length(field)

	%--
	% get contents of field
	%--
	
	% NOTE: concatenation fails for non-homogenous field contents
	
	try
		value = [in.(field{k})];
	catch
		continue;
	end
	
	if ~isstruct(value)
		continue;
	end

	%--
	% recurse on struct field contents and remove field from this level
	%--
	
    next = get_struct_tables(value, field{k}, name);

    next_field = fieldnames(next);

    for j = 1:length(next_field)
		
% 		if ~isfield(tables, next_field{j})
			tables.(next_field{j}) = next.(next_field{j});
% 		else
% 			tables.(next_field{j}) = [tables.(next_field{j})(:)', next.(next_field{j})(:)'];
% 		end
		
    end

    in = rmfield(in, field{k});

end

%--
% add struct array field to struct
%--

% if ~isfield(tables, name)
	tables.(name) = in;
% else
% 	tables.(name) = [tables.(name)(:)', in(:)'];
% end


