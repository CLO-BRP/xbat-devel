function obj = set_created_at(varargin)

% set_created_at - field of struct array
% --------------------------------------
%
% obj = set_created_at(obj, stamp)
%
% Input:
% ------
%  obj - array
%  stamp - time
%
% Output:
% -------
%  obj - updated array

obj = set_timestamp('created_at', varargin{:});
