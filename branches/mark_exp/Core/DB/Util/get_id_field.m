function id = get_id_field(obj, require)

% get_id_field - from struct array
% --------------------------------
%
% id = get_id_field(obj, require)
%
% Input:
% ------
%  obj - struct array
%  require - value (default: 1)
%
% Output:
% -------
%  id - values
%
% NOTE: if values are not required, cell output indicates missing values

%--
% handle input
%--

if nargin < 2
	require = 1;
end

if ~isfield(obj, 'id')
	error('Input objects do not have ''id'' field.');
end

%--
% get content of field
%--

id = [obj.id];

% NOTE: if the array collapse reveals missing values we either error (on required value) or pack as cell 

if numel(id) < numel(obj)
	
	if require
		error('Some objects were missing identifier.');
	end

	id = {obj.id};
	
end


