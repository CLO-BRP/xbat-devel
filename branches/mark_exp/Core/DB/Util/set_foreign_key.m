function obj = set_foreign_key(obj, key, value)

% set_foreign_key - field of struct
% ---------------------------------
%
% obj = set_foreign_key(obj, key, value)
%
% Input:
% ------
%  obj - array
%  key - table
%  value - for foreign key
%
% Output:
% -------
%  obj - updated array

obj = set_existing_field(obj, [key, '_id'], value);