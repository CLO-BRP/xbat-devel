function [element, info] = get_database_elements(store, type)

% get_database_elements - get database elements and some information
% --------------------------------------------------------------
%
% [element, info] = get_database_elements(store, type)
%
% Input:
% ------
%  store - database
%  type - element type: 'TABLE', 'VIEW', 'SYSTEM TABLE', 'GLOBAL TEMPORARY', 'LOCAL TEMPORARY',
% 'ALIAS', 'SYNONYM'
%
% Output:
% -------
%  element - names
%  info - for elements

%--
% handle database file input
%--

if ischar(store)

	if nargout < 2
		element = sqlite_get_elements(store, type);
	else
		[element, info] = sqlite_get_elements(store, type);
	end

	return;

end

%--
% create persistent store for adapter helpers
%--

persistent FUN; 

if isempty(FUN)
	FUN = struct;
end

if known_jdbc_adapter(store.adapter)

	[element, info] = jdbc_get_elements(store, type);

else

	adapter = lower(store.adapter); key = genvarname(adapter);

	if isfield(FUN, key)
		fun = FUN.(key);
	else

		helper = [adapter, '_get_elements']; fun = str2func(helper);

		if ~exist(fun) %#ok<EXIST>
			error(['Missing database adapter helper ''', helper, '''.']);
		end

		FUN.(key) = fun;

	end

	if nargout < 2
		element = fun(store, type);
	else
		[element, info] = fun(store, type);
	end
	
end

  
