function [tagging, obj] = find_by_tag(store, tags, logic, table)

% find_by_tag - find tagged objects
% ---------------------------------
%
% [tagging, obj] = find_by_tag(store, tags, logic, table)
%
% Input:
% ------
%  store - database
%  tags - to search for
%  logic - used for multiple tags
%  table - to consider
%  condition - expression
%
% Output:
% -------
%  tagging - relation objects
%  obj - tagged objects

%--
% handle input
%--

% NOTE: get all types of taggesd objects by default

if nargin < 4
	table = ''; obj = [];
end

% NOTE: the default logic is 'or' the more natural query it seems

if nargin < 3 || isempty(logic)
	logic = 'or';
end

% NOTE: make sure that we have a cell array representation for the tags

if ischar(tags)
	tags = str_split(tags, ' ');
end

%--
% build and execute tagging query
%--

% NOTE: we first get the identifiers of 'query' tags found in store

known = get_taggings(store);

tag_id = get_tag_id(intersect({known.name}, unique_tags(tags)), known);

% TODO: figure out how to produce the 'and' query

sql = {'SELECT * FROM tagging '};

if numel(tag_id) == 1
	sql{end + 1} = [' WHERE tagging.tag_id = ', int2str(tag_id)];
else
	sql{end + 1} = [' WHERE tagging.tag_id IN (', str_implode(tag_id, ', ', @int2str), ')'];
end

% TODO: consider writing single object query as single query with subselect

if ~isempty(table)
	sql{end + 1} = [' AND tagging.taggable_type = ''', table, ''';'];
end

[status, tagging] = query(store, sql_string(sql));

if isempty(table)
	return;
end

%--
% get objects in the case of a single type
%--

obj = get_database_objects_by_column(store, table, 'id', [tagging.taggable_id]);



