function taggings = find_taggings_by_tag(store, tags, known)

if nargin < 3
	known = get_taggings(store); 
end

id = get_tag_id(tags, known);

if numel(id) == 1
	sql = ['SELECT * FROM taggings WHERE tag_id = ', int2str(id), ';'];
else
	sql = ['SELECT * FROM taggings WHERE tag_id IN (', str_implode(id, ', ', @int2str), ');'];
end

[status, taggings] = query(store, sql);