function id = get_tag_id(tags, known)

% NOTE: we get the known tag identifier of the tag name match

if isempty(tags)
	id = []; return;
end

for k = 1:numel(tags)
	id(k) = known(strcmp(tags{k}, {known.name})).id;
end