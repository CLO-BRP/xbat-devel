function [value, tables] = database_has_table(store, table)

% database_has_table - check for table existence
% ----------------------------------------------
% 
% [value, tables] = database_has_table(store, table)
%
% Input:
% ------
%  store  - database
%  table - table 
%
% Output:
% -------
%  value  - indicator
%  tables - all tables

tables = get_database_tables(store);
 
value = string_is_member(table, tables);
