function type = build_prototype(field, source)

type = struct;

%--
% produce a default prototype
%--

if nargin < 2

	for k = 1:numel(field.column)
		type.(field.column{k}) = field.hint{k};
	end
	
%--
% build a prototype using provided a value source
%--

% NOTE: the value source uses not the column names, but the field names!

else

	% TODO: check that the source contains required fields
	
	for k = 1:numel(field.column)
		type.(field.column{k}) = source.(field.name{k});
	end
	
end

