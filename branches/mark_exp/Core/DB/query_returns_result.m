function result = query_returns_result(sql)

% query_returns_result - infer whether statement returns result
% -------------------------------------------------------------
%
% result = query_returns_result(sql)
%
% Input:
% ------
%  sql - string
%
% Output:
% -------
%  result - boolean

pattern = {'SELECT\W', '^PRAGMA\W', '^SHOW\W'};

for k = 1:numel(pattern)
	result = ~isempty(regexpi(sql, pattern{k})); if result, break; end
end