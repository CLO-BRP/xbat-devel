function result = establish_rating_(file, table, rating, value, opt)

% establish_rating - create database elements to support rating
% -------------------------------------------------------------
%
% establish_rating(file, table, rating, value, opt);
%
% Input:
% ------
%  file - file
%  table - table
%  rating - value list
%  value - of rating relation
%  opt - options

%--------------------
% HANDLE INPUT
%--------------------

%--
% set default options and possibly output
%--

if nargin < 5
	opt = establish_relation; opt.sort = 0;
end

if ~nargin
	result = opt; return;
end 

%--
% set no value default
%--

% NOTE: frequently we will add a 'user_id' value to the rating relation

if nargin < 4 || isempty(value)
	value = struct;
end 

%--
% set default rating values
%--

% NOTE: the rating values are stored in a cell

if nargin < 3 || isempty(rating)
	rating = num2cell(1:5);
end

%--
% generate relation table create query
%--

% NOTE: if we don't have a file we cannot establish relation

% NOTE: we have also not checked for the existence of or created a 'rating' table

if isempty(file)

	[sql, lines] = establish_relation('', table, 'rating', value, opt);

	if ~nargout
		disp(' '); sql_display(lines); disp(' ');
	else
		result = sql;
	end

	return;
	
end

%--
% check rating table
%--

% NOTE: currently we are using one global rating table, we could extend this
	
if has_table(file, 'rating') && ~opt.force
	
	%--
	% check existing rating table is compatible with currently proposed rating
	%--
	
	% TODO: create basic 'get_database_table' function, to get data and perhaps schema
	
	% NOTE: if we get the schema we can compare it with the current generated schema
	
	result = sqlite(file, 'SELECT * FROM rating;');
	
	% NOTE: the set exclusive or will result in values that are not common two both sets
	
	if ~isempty(setxor(rating, {result.value}))
		error('Current and proposed rating values are not compatible.');
	end
	
else
	
	%--
	% create rating table
	%--
	
	for k = 1:numel(rating)
		content(k).id = k; content(k).value = rating{k};
	end
	
	% NOTE: the relation and table options are rather different, but they share a 'force' field
	
	topt = create_table; topt.unique = 1; topt.force = opt.force;
	
	sqlite(file, create_table(content(1), 'rating', topt));
	
	sqlite(file, sqlite_array_insert('rating', content));
	
end

%--
% establish relation
%--

establish_relation(file, table, 'rating', value, opt);
	