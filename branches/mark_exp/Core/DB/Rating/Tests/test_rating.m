function test_rating(verb)

if ~nargin
	verb = 0;
end

%--
% setup test database 
%--

% TODO: extend to other adapters, through 'get_test_database'

store = rating_test_file;

if exist(store, 'file')
	delete(store);
end

%--
% store some objects and create a couple of users
%--

object.id = 1; object.name = 'name';

query(store, create_table(object));

for k = 1:9
	object(k).id = []; object(k).name = ['object', int2str(k)];
end 

object = set_database_objects(store, object);

% NOTE: this is the only field we need for rating

user.id = 1; user(2).id = 2;

%--
% make taggable and perform various tag operations
%--

establish_rating(store);

values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

set_ratings(store, [], values);

rate = get_ratings(store);

assert_equal(values, {rate.score});

% rate some objects

set_ratings(store, object, 6, user(1));

set_ratings(store, object(1), 1, user(1), 'object');

set_ratings(store, object(3:5), {5 ,4 ,3}, user(1), 'object');

set_ratings(store, object(1), 2, user(2), 'object');

set_ratings(store, [object(3:5).id], 7, user(2), 'object');

for k = 1:numel(user)
	
	[rating, relation] = get_ratings(store, object, user(k)); %#ok<NASGU>
	
	if verb
		
		for j = 1:numel(object)
			object(j).rating = rating{j}; 
		end

		disp(user(k)); disp(object);
		
	end
	
end

% TODO: add some assertions

