function [tables, info] = get_database_tables(config)

% get_database_tables - get database tables and some information
% --------------------------------------------------------------
%
% [tables, info] = get_database_tables(file)
%
% Input:
% ------
%  config - database config
%
% Output:
% -------
%  tables - cellarray of table names
%  info - for tables - dont know what info this is??

if nargout < 2
	tables = get_database_elements(config, 'table'); 
else
	[tables, info] = get_database_elements(config, 'table'); 
end