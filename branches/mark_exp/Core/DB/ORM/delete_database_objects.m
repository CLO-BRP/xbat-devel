function [status, result] = delete_database_objects(file, obj, table)

% delete_database_objects - by identifier and table
% -------------------------------------------------
%
% [status, result] = delete_database_objects(file, obj, table)
%
% Input:
% ------
%  file - database
%  obj - set or identifiers
%  table - name
%
% Output:
% -------
%  status - of query
%  result - of query

%--
% typically inspect table name
%--

if nargin < 3
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%--
% delete objects using identifier
%--

% NOTE: we expect objects or an identifier vector

if isstruct(obj)
	id = [obj.id]; 
else
	id = obj;
end 

if numel(id) == 1
	sql = ['DELETE FROM ', table, ' WHERE id = ', int2str(id), ';'];
else
	sql = ['DELETE FROM ', table, ' WHERE id IN (', str_implode(id, ', ', @int2str), ');'];
end

[status, result] = query(file, sql);

