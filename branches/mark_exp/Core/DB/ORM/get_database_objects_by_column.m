function out = get_database_objects_by_column(store, table, column, value, opt)

% get_database_objects_by_column - simple selection of rows 
% ---------------------------------------------------------
%
% obj = get_database_objects_by_column(store, table, column, value, opt)
%
% sql = get_database_objects_by_column('', table, column, value, opt)
%
% Input:
% ------
%  store - database
%  table - name
%  column - name
%  value - to match
%  opt - struct
%
% Output:
% -------
%  obj - from database
%  sql - string

% TODO: consider a way of returning a query we can cache then just add the value

% TODO: consider the case of multiple values as set (implemented) and range

% NOTE: the two above tasks should be considered together

%--
% set and possibly output default options
%--

if nargin < 5
	opt.check = 1; % check for table availability
	
	opt.fields = {}; opt.include = {}; % get specific fields, possibly join some tables
	
	opt.escape = 1; opt.like = 0; % find like or exact match for possibly escaped string
end

if ~nargin
	out = opt; return;
end 

%--
% set find all default
%--

% NOTE: in this case the column input is also irrelevant

if nargin < 4
	value = '';
end

%--
% perform checks if requested and possible
%--

if opt.check && ischar(store) && ~isempty(store)
	
	if ~has_table(store, table)
		error(['Database does not have ''', table, ''' table.']);
	end
	
	if numel(opt.include)
		foreign_key = strcat(opt.include, '_id');
		
		available = string_is_member(foreign_key, get_table_columns(store, table));
		
		if any(~available)
			error('Missing some foreign keys in base table.'); 
		end
	end
end

%--
% build query string
%--

if isempty(opt.fields)
	if isempty(opt.include)
		fields = '*';
	else
		fields = str_implode(strcat(table, '.', get_table_columns(store, table)), ', ');
	end
else
	fields = str_implode(opt.fields, ', ');
end

%--------------

% NOTE: here we add include columns as 'include__field', after flatten the result is a nested struct

join_fields = cell(1, numel(opt.include));

for k = 1:numel(opt.include)
	join = opt.include{k}; columns = get_table_columns(store, join);
	
	join_fields{k} = str_implode(strcat(join, '.', columns, {' AS '}, join, '__', columns), ', ');
end

if numel(join_fields)
	fields = [fields, ', ', str_implode(join_fields, ', ')];
end

%--------------

sql = ['SELECT ', fields, ' FROM ', table, ';'];

%--------------

% NOTE: the join statement fragments assume all includes tables contain conventional foreign key

join = cell(1, numel(opt.include));

for k = 1:numel(opt.include)
	join{k} = ['JOIN ', opt.include{k}, ' ON ', table, '.', opt.include{k}, '_id = ', opt.include{k}, '.id'];
end

if numel(join)
	sql = [sql(1:end - 1), ' ', str_implode(join, ' '), ';'];
end

%--------------

if ~isempty(value)
	
	% NOTE: the 'escape' and 'like' considerations only apply to string values
	
	switch class(value)
		
		case 'cell'
			op = ' IN '; value = ['(', str_implode(iterate(@prepare_string, value, opt), ', '), ')']; 
			
		case 'char'	
			op = ' = '; value = prepare_string(value, opt);
			
		otherwise
			if ~isnumeric(value) || ~isreal(value)
				error('Non-string values must be numeric reals.');
			end
			
			if numel(value) == 1
				op = ' = '; value = num_to_str(value);
			else
				op = ' IN '; value = ['(', num_to_str(value, ', '), ')'];
			end
			
	end

	% NOTE: we keep all but the final semicolon and append the where clause
	
	sql = [sql(1:end - 1), ' WHERE ', table, '.', column, op, value, ';'];
	
end

%--
% output or possibly display query or objects
%--

if isempty(store)
	out = sql;
	
	if ~nargout
		disp(' '); sql_display(out); disp(' '); clear out;
	end
else
    [status, out] = query(store, sql);
	
	% NOTE: when tables are joined we flatten to get the desired hierarchy and discard the now redundant foreign key
	
	if numel(opt.include)
		out = iterate(@unflatten, out);
		
		for k = 1:numel(opt.include)
			foreign_key = [opt.include{k}, '_id'];
			
			if isfield(out, foreign_key)
				out = rmfield(out, foreign_key);
			end
		end
	end
	
	if ~nargout
		disp(' '); disp(out); disp(' '); clear out;
	end
end


%-----------------------------
% PREPARE_STRING
%-----------------------------

% TODO: evaluate the role of this function, don't we always 'escape' now that we are using trivial escaping

function str = prepare_string(str, opt)

% NOTE: this is the SQLite and MySQL 'like' string notation

if opt.like
	str = ['%', str, '%'];
end

if opt.escape
	str = atomic_value_string(str);
else 
	str = ['''', str, ''''];
end


