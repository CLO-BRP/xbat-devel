function [tag, id, created] = get_database_tags(file, varargin)

% get_database_tags - get all tags from database
% ----------------------------------------------
%
% [tag, id, created] = get_database_tags(file, field, value, ... )
%
% Input:
% ------
%  file - file
%  field - field
%  value - value
%
% Output:
% -------
%  tag, id, created - tag, identifier, and time stamp matching arrays

% TODO: implement ordering by frequency

%--
% return empty if table is not taggable
%--

tag = {}; id = []; created = {};

% TODO: we want the MEX file to throw an error so we can skip this

if ~database_has_tags(file)
	return;
end

%--
% handle input
%--

if isempty(varargin)
	opt = struct;
else
	[field, value] = get_field_value(varargin, {'in', 'like', 'order', 'tags'}); opt = pack_field_value(field, value);
end

% NOTE: this is a way to set a default value using this parameter input approach

if ~isfield(opt, 'order')
	opt.order = 'A';
end

%--
% get tag names and identifiers
%--

% TODO: the 'tags' filtering and the 'order' by frequency require complex queries, consider doing here

switch upper(opt.order(1))
	
	% NOTE: the case-insensitive ascending order is typical and most intuitive
	
	case 'A'
		
		%--
		% build query considering selection, outputs and order
		%--
		
		where = '';
		
		% NOTE: these are exclusive with like taking precedence?
		
		if isfield(opt, 'like')
			
			where = ['WHERE name LIKE ''%', opt.like, '%'''];
			
		elseif isfield(opt, 'in')
			
			where = ['WHERE name IN (''', str_implode(opt.in, ''', '''), ''')'];
			
		end

		query = ['SELECT ', ternary(nargout > 1, '*', 'name'), ' FROM tag ', where, ' ORDER BY LOWER(name);'];
	
		
		[status, result] = sqlite(file, query);
		
	case 'F' % short for frequency
		
		% TODO: implement order by frequency of occurrence, implement and use a 'get_tags_stats'
		
end

if trivial(result)
	return; 
end

% NOTE: unpack to produce output arrays

tag = {result.name};

if nargout > 1
	id = [result.id];
end

if nargout > 2 
	created = {result.created_at};
end




