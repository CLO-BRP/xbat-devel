function update = rename_database_tags(file, current, desired)

% rename_database_tags - rename tags from database
% ------------------------------------------------
%
% update = rename_database_tags(file, current, desired)
%
% Input:
% ------
%  file - file
%  current - name
%  desired - name
%
% Output:
% -------
%  update - indicator

%--
% handle input
%--

if ischar(current)
	current = {current};
end

if ischar(desired)
	desired = {desired}; 
end

if numel(current) ~= numel(desired)
	error('Current and desired name arrays must have matching elements.');
end

%--
% get known tags and rename current tags
%--

% NOTE: this is not efficient, however currently it is concise and correct

[tags, id] = get_database_tags(file); update = zeros(size(current));

for k = 1:numel(current) 
	
	[update(k), ix] = string_is_member(current{k}, tags);
	
	if ~update(k)
		continue;
	end 
	
	[ignore, actual] = valid_tag(desired{k});

	% NOTE: above we obtained the identifier index, here we must dereference
	
	sqlite(file, ['INSERT OR REPLACE INTO tag VALUES (', int2str(id(ix)), ',''', actual,''');']);
	
end
