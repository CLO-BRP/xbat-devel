function value = database_has_tags(file, table)

% database_has_tags - check for 'tag' and possibly tag relation table
% -------------------------------------------------------------------
%
% value = database_has_tags(file, table)
%
% Input:
% ------
%  file - file
%  table - table
%
% Output:
% -------
%  value - indicator

%--
% check for general tags table
%--

value = has_table(file, 'tag'); 
	
if nargin < 2 || ~value
	return;
end 

%--
% check for tags relation table for table
%--

value = has_table(file, tag_relation_table(table));
