function [name, info] = get_tables(file)

% get_tables - get database tables and some information
% -----------------------------------------------------
%
% [name, info] = get_tables(file)
%
% Input:
% ------
%  file - database
%
% Output:
% -------
%  name - of tables
%  info - for tables
%
% NOTE: this is an alias to 'get_database_tables'

[name, info] = get_database_tables(file);