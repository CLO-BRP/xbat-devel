function id = get_last_id( statement )

resultset = statement.getGeneratedKeys();

id = []; % preallocate the array

j = 1;
if ~isempty(resultset)
    while resultset.next()
        id(j) = resultset.getInt(1);
        j = j + 1;
    end
end

% cleanup
resultset.close();

%% FROM http://java.sun.com/products/jdbc/faq.html

% 18. There is a method getColumnCount in the JDBC API. 
% Is there a similar method to find the number of rows in a result set?

% No, but it is easy to find the number of rows. 
% If you are using a scrollable result set, rs, you can call the methods rs.last and then rs.getRow to find out how many rows rs has. If the result is not scrollable, you can either count the rows by iterating through the result set or get the number of rows by submitting a query with a COUNT column in the SELECT clause.

% Java sucks.