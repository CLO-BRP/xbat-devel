function [results, info] = jdbc_get_elements(config, type)

% jdbc_get_elements - JDBC adapter for get_database_elements()
% --------------------------------------------------------------
%
% [results, info] = jdbc_get_elements(config, type)
%
% Input:
% ------
%  config - database config
%  type - element type: 'TABLE', 'VIEW', 'INDEX'
%
% Output:
% -------
%  results - cellarray of element names
%  info - for elements

if ~isstruct(config)
    error('Sorry, config argument must be a struct.');
end

metadata = get_jdbc_metadata(config);

switch upper(type)
    
    case {'TABLE', 'VIEW'}
        
        resultset = metadata.getTables([],'%', '%', upper(type));
        
    case 'INDEX'
        
        error('JDBC index access has not been implemented yet!');
        
end


resultset = format_jdbc_resultset(resultset);

results = cell(1, numel(resultset)); % preallocate the cell array
for i=1:numel(resultset)
    results{i} = resultset(i).TABLE_NAME;
end

info = 'JDBC not returning info for this operation (yet)';

%value = string_is_member(table, info);

% NOTES ON DatabaseMetaData.getTables()

% public ResultSet getTables(String catalog,
%                            String schemaPattern,
%                            String tableNamePattern,
%                            String[] types)
%                    throws SQLException

% NOTE: common values for TABLE_TYPE

% "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY",
% "ALIAS", "SYNONYM"



