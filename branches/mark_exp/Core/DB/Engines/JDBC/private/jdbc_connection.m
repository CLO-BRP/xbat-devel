function [connection, problem] = jdbc_connection(config, action)

% get_jdbc_connection - create database connection from database config
% ---------------------------------------------------------------------
%
% [connection, config] = jdbc_connection(config)
%
% Input:
% ------
%  config - struct
%
% Output:
% -------
%  connection - object
%  config - updated

problem = [];

%--
% handle input
%--

if nargin < 2
	action = 'get';
end 

%--
% create and check persistent connection pool
%--

persistent CONNECTION

if isempty(CONNECTION)
	CONNECTION = struct;
end

key = get_key(config.jdbc);

if isfield(CONNECTION, key)	&& ~strcmp(action, 'reset')    
	connection = CONNECTION.(key); return;
end

logger('info', 'Creating new JDBC connection ...')
%--
% create and configure connection
%--

% NOTE: we can create handles to java classes as well as MATLAB functions

% NOTE: that calling 'functions' on this type of handle is useless

driver = str2func(config.driver); driver = driver();

properties = get_connection_properties(config, driver);

try
	connection = driver.connect(config.jdbc, properties);
catch
	problem = lasterror; connection = []; return;
end

%--
% add connection to persistent store
%--

% here is where the magic happens

CONNECTION.(key) = connection;



%-----------------------
% HELPER FUNCTIONS (utility)
%-----------------------

%-----------------------
% GET_KEY
%-----------------------

function key = get_key(str)

key = ['key_', md5(str)];

%-----------------------
% GET_CONNECTION_PROPERTIES
%-----------------------

function properties = get_connection_properties(config, driver)

properties = java.util.Properties;

properties.setProperty('user', config.username); 

properties.setProperty('password', config.password);

% autoReconnect property catches stale connections with otherwise throw an error
% NB: THIS PROPERTY DOES NOT WORK IF AUTOCOMMIT IS TURNED OFF!

properties.setProperty('autoReconnect', 'true');

% account for MySQL's lovely default handling of 'null' datetime/timestamp
% values (which is by default an exception)

if driver_has_property(config, driver, 'zeroDateTimeBehavior')
    properties.setProperty('zeroDateTimeBehavior', 'convertToNull');
end

%-----------------------
% DRIVER_HAS_PROPERTY
%-----------------------

function result = driver_has_property(config, driver, property)
    
info = get_driver_property_info(config, driver);

result = false;

if isfield(info, property)
    result = true;
end
    
%-----------------------
% GET_DRIVER_PROPERTY_INFO
%-----------------------

function info = get_driver_property_info(config, driver)

driver_info = driver.getPropertyInfo(config.jdbc, []);

info = struct;

for i=1:numel(driver_info)
    name = char(driver_info(i).name);
    info.(name)            = struct;
    info.(name).name       = char(driver_info(i).name);
    info.(name).isRequired = driver_info(i).required;
    info.(name).value      = char(driver_info(i).value);
    info.(name).desc       = char(driver_info(i).description);
    info.(name).choices    = driver_info(i).choices;
end



