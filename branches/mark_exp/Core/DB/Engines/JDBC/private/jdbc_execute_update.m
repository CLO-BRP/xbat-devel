function [status, result] = jdbc_execute_update(connection, sql)


    
    connection.setAutoCommit(true);
    
    statement = connection.createStatement(java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);

    % 'status' is either the row count for INSERT, UPDATE  or DELETE statements, or 0 for SQL statements that return nothing
    status = statement.executeUpdate(sql, java.sql.Statement.RETURN_GENERATED_KEYS); 
    
    % AUTOCOMMIT IS TURNED ON FOR THE CONNECTION BY DEFAULT.
    % ONLY jdbc_execute_batch SETS IT TO FALSE, THEN RESETS IT TO TRUE WHEN
    % IT IS FINISHED 
    
    % connection.commit();
    
    result = get_last_id( statement );
    

    
    % If the rollback is failing and looks something like this:
    %
    % ??? Java exception occurred: 
    % com.mysql.jdbc.exceptions.MySQLNonTransientConnectionException: 
    % Connection.close() has already been called. Invalid operation in this state.
    
    % chances are that the connection is stale and the error originated in
    % the previous attempt to execute a query on a stale connection, which
    % is caught here. Then we attempt to rollback this query on a closed
    % connection .... This has been fixed by adding a property to the
    % connection in get_jdbc_connection()
    
    % NOTE: the previous makes me wonder if a rollback is needed if a
    % statement fails - so connection.commit() might never be called

    % connection.rollback();


