function [status, result] = jdbc_execute_batch(connection, queries)

%--
% setup
%--

% NOTE: settting 'auto commit' to false means the connection is susceptible to
% conection timeouts, since autoReconnect will not work under this
% condition. 

% TODO: write code here to handle the possibilty of a stale connection.

connection.setAutoCommit(false);

%--
% create batch statement
%--

statement = connection.createStatement(java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);

for j = 1:numel(queries)
    statement.addBatch(queries(j));
end

%--
% execute statement
%--

% NOTE: 'executeBatch' does not support getting generated keys. JDBC spec allows only getting generated keys from single updates. 

status = statement.executeBatch(); result = get_last_id(statement); 

connection.commit();

% TODO: FIGURE OUT A WAY TO ROLLBACK ON BATCH ERROR!!!!

% NOTE: clean up the connection for non-batched operations

connection.setAutoCommit(true);