function [ status, result ] = jdbc_execute_query( connection, sql )

% NOTE: typically these are SELECT queries
statement = connection.createStatement(java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);

resultset = statement.executeQuery(sql);

% AUTOCOMMIT IS TURNED ON FOR THE CONNECTION BY DEFAULT.
% ONLY jdbc_execute_batch SETS IT TO FALSE, THEN RESETS IT TO TRUE WHEN
% IT IS FINISHED

% connection.commit();

result = format_jdbc_resultset(resultset); % now a private function

status = 0;

% cleanup
resultset.close();