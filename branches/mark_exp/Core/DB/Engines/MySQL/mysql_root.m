function root = mysql_root

%--
% check persistent store
%--

persistent ROOT;

if ~isempty(ROOT)
	root = ROOT; return;
end 

% TODO: add other candidates, consider platform (this is only for windows)

%--
% examine to find root
%--

if ~ispc

	% NOTE: this is the directory that contains the executable command line client
	
	root = fileparts(which('mysql'));

else

	% NOTE: these are typical and suggested install directories, there is non-trivial tree below
	
	candidate = { ...
		'C:\Program Files\MySQL', fullfile(tools_root, 'MySQL') ...
	};

	root = '';
	
	for k = 1:numel(candidate)

		if ~exist_dir(candidate{k})
			continue;
		end

		root = candidate{k}; break;

	end

end

%--
% set persistent
%--

% NOTE: from above we will always examine when we have not found

ROOT = root;

