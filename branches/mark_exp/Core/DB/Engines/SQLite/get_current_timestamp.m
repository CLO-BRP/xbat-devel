function value = get_current_timestamp(store)

% TODO: move this elsewhere

sql = 'SELECT CURRENT_TIMESTAMP as value;';

% NOTE: the simple sqlite query returns the UTC timestamp, particular stores can be configured differently

if ~nargin
	result = sqlite('', sql);
else
	[ignore, result] = query(store, sql);
end

value = result.value;