function format = sqlite_timestamp_format

% sqlite_timestamp_format - what it says
% --------------------------------------
%
% format = sqlite_timestamp_format
%
% Output:
% -------
%  format - date format string

format = 'yyyy-mm-dd HH:MM:SS';

