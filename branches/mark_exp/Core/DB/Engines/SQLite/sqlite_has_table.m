function [value, names] = sqlite_has_table(file, name)

% sqlite_has_table - cheap check for table existence
% --------------------------------------------------
% 
% [value, names] = sqlite_has_table(file, name)
%
% Input:
% ------
%  file - sqlite database file
%  name - table name to check for
%
% Output:
% -------
%  value - true if table exists
%  names - available table names

% TODO: extend to other database engines, call the engine method from here

[status, result] = sqlite(file, 'prepared', 'SELECT name FROM sqlite_master WHERE type = ''table'';');

names = {result.name}; 

value = string_is_member(name, names);