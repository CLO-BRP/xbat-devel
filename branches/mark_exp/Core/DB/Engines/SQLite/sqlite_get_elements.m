function [results, info] = sqlite_get_elements(config, type)

% sqlite_get_elements - SQLite adapter for get_database_elements()
% --------------------------------------------------------------
%
% [results, info] = sqlite_get_elements(config, type)
%
% Input:
% ------
%  config - database config
%  type - element type: 'TABLE', 'VIEW', 'INDEX'
%
% Output:
% -------
%  results - cellarray of element names
%  info - for elements

%--
% get element names
%--

[status, result] = sqlite(config, ['SELECT name FROM sqlite_master WHERE type = ''', type, ''';']);

results = {result.name};

%--
% get element info
%--

if nargout > 1
    info = cell(1, numel(results));
	switch type

		case {'table', 'view'}
			 
			for k = 1:numel(results)
				info{k} = sqlite(config, ['PRAGMA table_info(', results{k}, ');']);
			end
			
		case 'index'
			
			for k = 1:numel(results)
				info{k} = sqlite(config, ['PRAGMA index_info(', results{k}, ');']);
			end
			
		% NOTE: perhaps there is some way of defining 'info' for other elements
		
		otherwise, info{k} = [];

	end
	
end


% %--
% % update cache
% %--
% 
% if ~exist('finfo', 'var')
% 	finfo = dir(file);
% end
% 
% CACHE{end + 1, 1} = file; CACHE{end, 2} = type; CACHE{end, 3} = name; CACHE{end, 4} = info; CACHE{end, 5} = finfo.datenum;
