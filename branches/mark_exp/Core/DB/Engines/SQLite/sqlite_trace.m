function value = sqlite_trace(value)

% sqlite_trace - control trace behavior
% -------------------------------------
%
% sqlite_trace
%
% sqlite_trace(value)
%
% value = sqlite_trace
%
% Input:
% ------
%  value - trace level to set
%
% Output:
% -------
%  value - current trace level
%
% NOTE: call without arguments to get a graphical interface

% TODO: add option to consider SQL when determining whether to trace

% NOTE: consider 'listen' and 'ignore' options, these will be best set using the GUI

%--
% get value
%--

if ~nargin

	value = get_env('sqlite_trace');

	if isempty(value)
		value = 0; set_env('sqlite_trace', value);
	end

	if nargout
		return;
	end
	
%--
% set value
%--

else
	
	switch value
		
		case {0, 1}
			set_env('sqlite_trace', value);

		case {'0', '1'}
			set_env('sqlite_trace', eval(value));
			
	end

end

%--
% update existing graphical interface to display trace state
%--

% db_disp; 

pal = findobj(0, 'tag', 'sqlite_trace_gui');

% TODO: this is not working, an appropiate 'get_control_value' call returns empty

if ~isempty(pal)
	set_control_value(pal, 'trace', value);
end

%--
% create graphical interface to display and control trace state
%--

if ~nargin && ~nargout

	if ~isempty(pal)
		position_palette(pal, 0, 'top left'); return;
	end

	control = [ ... 
		header_control('Trace'), ...
		checkbox_control('trace', value, 'alias', 'On'), ...
		header_control('Advanced'), ...
		edit_control('listen'), ...
		edit_control('ignore', '', 'space', 1.5) ...
	];

	opt = control_group; opt.bottom = 0; opt.width = 12;

	pal = control_group([], @callback, 'SQLite', control, opt);

	set(pal, 'tag', 'sqlite_trace_gui');

	position_palette(pal, 0, 'top left');

	clear value;

end
	

%-----------------------
% CALLBACK
%-----------------------

function callback(obj, eventdata)

callback = get_callback_context(obj, 'pack');

switch callback.control.name
	
	case 'trace'
		sqlite_trace(get(obj, 'value'));
		
	case 'listen'
		set_env('sqlite_trace_listen', get(obj, 'string'));
		
	case 'ignore'
		set_env('sqlite_trace_ignore', get(obj, 'string'));
		
end


