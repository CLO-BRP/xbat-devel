function [status, result] = query(store, sql)

% query - database server or file-based database
% ----------------------------------------------
%
% [status, result] = query(store, sql)
%
% Input:
% ------
%  store - configuration or file-based database
%  sql - query
%
% Output:
% -------
%  status - of query
%  result - from query

if trivial(store)

	% NOTE: we hope for a cell array representation in 'sql', but this is not guaranteed

	status = sql_string(sql); result = sql;

	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear status;
	end

else

	% NOTE: quickly check if we are tracing and start clock
	
	trace = query_trace;

	if trace
		start = clock;
	end
	
	%--
	% perform query
	%--
	
	if isstruct(store)
		[status, result] = store.query(store, sql);
	else
		[status, result] = sqlite(store, sql);
	end

	%--
	% trace display
	%--
	
	if trace
		elapsed =  etime(clock, start); rate = round(1 / elapsed);
		
		disp(' '); sql_display(sql); disp(' ');
		
		if trace > 1
			stack_disp([], 3);
			disp(' ');
		end
		
		if trace > 2
			disp(store);
			disp(' ');
		end
		
		% TODO: create 'query_trace_message' to display information about the higher level interaction being represented by the trace
		
		if isinf(rate)
			speed = 'Completed instantly';
		else
			speed = ['Completed in ', num2str(elapsed, 6), ' (', int2str(rate), ' reqs/sec)'];
		end
		
		if isfield(store, 'jdbc')
			server = store.jdbc;
		else
			server = [store.adapter, ': ', store.database];
		end
		
		disp(['   ', speed, ' @ ', server]);
		
		str_line(72, '_');
	end
	
end
