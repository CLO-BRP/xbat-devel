function test_set_notes

% ---------------------------
% SETUP
% ---------------------------

% TODO: extend to other adapters, through 'get_test_database'

store = fullfile(fileparts(mfilename('fullpath')), 'test.sqlite');

if exist(store, 'file')
	delete(store);
end

%--
% store some objects and create a couple of users
%--

% OBJECTS

object.id   = []; 
object.name = 'My first object';

object(2).id   = []; 
object(2).name = 'My second object';

query(store, create_table(object));

object = set_database_objects(store, object, []);


% USERS

user.id    = 1; 
user(2).id = 2;


% NOTES/ANNOTATIONS

establish_notes(store);


% ---------------------------
% TEST 1
% ---------------------------

% Set the notes without association to an object

% --
% SETUP
% --

note = note_create( ...
	'title', 'note', ...
	'body', 'this is a note' ...
);

note(end + 1) = note_create( ...
	'title', 'Note 2, electric boogaloo', ...
	'body', 'This is another note' ...
);

note(end + 1) = note_create( ...
	'title', 'Note 3', ...
	'body', 'Nothing else clever to say here' ...
);

% NOTE: we set and get notes, check that we properly stored them

set_notes(store, [], note);

note = get_notes(store);

assert(numel(note) == 3);

% --
% CLEANUP
% --

notes_to_delete = find_notes(store);

delete_database_objects(store, notes_to_delete, 'note');

notes = get_notes(store);

assert(numel(notes) == 0);


% ---------------------------
% TEST 2
% ---------------------------

% Set multiple notes to a single object

set_notes(store, object(1), note, user(1), 'object');

% --
% ASSERTION
% --

notes = get_notes(store, object(1), [], 'object');

assert(numel(notes{1}) == 3);

% --
% CLEANUP
% --

notes_to_delete = find_notes(store, [], 'type', 'object');

delete_database_objects(store, notes_to_delete, 'note');


% ---------------------------
% TEST 2
% ---------------------------

% Set multiple notes to multiple objects

set_notes(store, object, note, user(1), 'object');

% --
% ASSERTION
% --

notes = get_notes(store, object(1), [], 'object');

assert(numel(notes{1}) == 3);

notes = get_notes(store, object(2), [], 'object');

assert(numel(notes{1}) == 3);

set_notes(store, object(1), empty(note), user(1), 'object');

notes = get_notes(store, object(1), [], 'object');

assert(isempty(notes{1}));


% --
% CLEANUP
% --

notes_to_delete = find_notes(store, [], 'type', 'object');

delete_database_objects(store, notes_to_delete, 'note');



