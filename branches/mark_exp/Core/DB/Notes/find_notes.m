function [notes, annotations] = find_notes(store, object, varargin)

% SETUP
%---------------------------

notes  = [];
annotations  = [];

options.conditions = '';
options.limit      = '';
options.join       = '';
options.table      = '';
options.type       = '';

% HANDLE INPUTS
%---------------------------
if nargin < 2
    notes = get_database_objects_by_column(store, 'note'); 
    annotations = [];
    return
end

if nargin > 2 && isstruct(varargin{1})
    notes    = varargin{1};
    varargin = {varargin{2:end}};
end

options = parse_inputs(options, varargin{:});

if ~isempty(options.type)
    options.table = options.type;
end

if nargin > 1 && (isempty(options.table) || isempty(options.type))
    options.table = inputname(2);
end

% CONTRUCT SQL
%---------------------------

sql = { ...
	'SELECT note.*, ',... 
    'annotation.annotated_type, ',... 
    'annotation.annotated_id, ',...
    'annotation.note_id, ', ...
    'annotation.id as annotation_id ', ...
	'FROM note ', ...
	'JOIN annotation ON note.id = annotation.note_id ' ...
};

% CONDITIONS 
%---------------------------

if ~isempty(options.conditions)
    sql{end + 1} = ['WHERE ', options.conditions , ' '];
end

if ~isempty(object)
    % right now we are assuming that the incoming object has an id. 
    % This could be changed in the future
    for i=1:numel(object)
        if isempty(object(i).id)
            error('The object(s) you passed in doesnt have an ID.');
        end
    end
 
    sql{end + 1} = and_or_where_keyword( options.conditions );
    sql{end + 1} = ['annotated_type = ''', options.table, ''' '];

    if numel(object) > 1
        sql{end + 1} = ['AND annotated_id IN (''', str_implode([object.id], ', ', @int2str), ''') '];
    else
        sql{end + 1} = ['AND annotated_id = ''', int2str(object.id) , ''' ' ];
    end
    
end

if isstruct(notes)
    
	sql{end + 1} = and_or_where_keyword( (~isempty(object) || ~isempty(options.conditions)) );

    hash = get_note_hash(notes);

    if numel(notes) > 1
        sql{end + 1} = ['hash IN (''', str_implode(hash, ''', ''', @char), ''') '];
    else
        sql{end + 1} = ['hash = ''', hash , ''' ' ];
    end
end

% LIMIT 
%---------------------------

if options.limit
    sql{end + 1} = [' LIMIT ', options.limit];
end



% CALLED WITH NO STORE, RETURN QUERY
% --------------------------

if trivial(store)
   	notes = sql;
    disp(' '); sql_display(sql); disp(' ');
    return;
end

% EXECUTE QUERY
% --------------------------

[status, notes] = query(store, sql_string(sql));

if ~isempty(object)
    for i=1:numel(notes)
        annotations(i).id = notes(i).annotation_id;
        annotations(i).annotated_type = notes(i).annotated_type;
        annotations(i).annotated_id = notes(i).annotated_id;
        annotations(i).note_id = notes(i).note_id;
    end
    % [status, annotations] = find_annotations(store, object, note);
else
    notes = rmfield(notes, {'annotation_id', 'annotated_type', 'annotated_id', 'note_id'});
end


% A little helper function
function keyword = and_or_where_keyword(flag)

keyword = 'WHERE ';

if flag
    keyword = 'AND ';
end
