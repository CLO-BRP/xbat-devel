function set_notes(store, obj, note, user, table)

% set_notes - annotate objects in database
% --------------------------------------
%
% set_notes(store, [], note)
%
% set_notes(store, obj, note, user, table)
%
% Input:
% ------
%  store - database
%  obj - to annotate
%  note - sets
%  user  - annotating
%  table - to annotate

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% typically reflect table from input name
%--

if nargin < 5
	table = inputname(2);
end

%--
% check for sufficient input for object annotation
%--

% NOTE: to annotate objects we must know their table

if isempty(table) && ~isempty(obj)
	error('Table to annotate is not available explicitly or implicitly from input.');
end

% NOTE: to annotate objects we must know who is annotating

if ~isempty(obj) && isempty(user)
	error('Please provide the user that is responsible for these annotations.');
end

%---------------------------
% SETUP
%---------------------------

% NOTE: input is struct cell array (as a single set of notes to set) or cell of notes struct arrays (a matching set of notes to set)

%--
% consider non-cell input to be a single note struct array
%--

if ~iscell(note)
	note = {note};
end

%--
% discover unique proposed notes and make sure they are all stored
%--

% NOTE: make sure all notes are aligned as rows and contatenate them

for k = 1:numel(note)
	
	note{k} = note{k}(:)';
	
	for j = 1:numel(note{k})
		note{k}(j).content_hash = get_note_hash(note{k}(j));
	end
	
end

proposed = [note{:}];

[ignore, selected] = unique({proposed.content_hash}); 

proposed = proposed(selected);

if numel({proposed.content_hash}) > 1
    sql = ['SELECT * FROM note WHERE note.content_hash IN (''', str_implode({proposed.content_hash}, ''', '''), ''');'];
else
    sql = ['SELECT * FROM note WHERE note.content_hash = ''', proposed.content_hash, ''''];
end

[ignore, known]  = query(store, sql);

% Return indices of note hashes that are proposed, but not 'known'

[ignore, append] = setdiff({proposed.content_hash}, {known.content_hash}); 

% Now use this vector of indices to filter the proposed vector

proposed = proposed(append);

added = set_database_objects(store, proposed, [], {'title', 'body', 'content_hash'}, 'note');

% NOTE: this was a request for note storage if we have no objects to annotate, we are done

known = [known, added];

if isempty(obj)
	note = known; return;
end

% NOTE: here we replicate scalar input 'note' (which is a struct) to match the number of objects

% TODO: make sure that this is not a costly operation, delayed copy

if numel(note) == 1
    note = repmat(note, 1, numel(obj));
end

if numel(note) ~= numel(obj)
	error('Notes must be scalar or match the number of objects.');
end

%--
% map input notes to note identifiers
%--

content_hash = {known.content_hash}; note_id = cell(size(note));

for k = 1:numel(note)
	
	for j = 1:numel(note{k})
		note_id{k}(j) = known(strcmp(content_hash, note{k}(j).content_hash)).id;
	end

end

%---------------------------
% ANNOTATE OBJECTS
%---------------------------

%--
% get current object notes and compare with proposed to determine action (update/save/delete)
%--

[current, relation] = get_notes(store, obj, user, table);

% NOTE: initialize relations to remove and relations to update arrays

prototype = annotation_prototype;

add = empty(prototype); remove = empty(prototype); update = empty(prototype);

for k = 1:numel(obj)
	
	%--
	% handle simple special cases
	%--
	
	% NOTE: all notes for this object will be removed
	
	if isempty(note{k})
		
		remove = [remove, relation{k}]; continue;
	
	end
			
	% NOTE: all notes for this object are new
	
	if isempty(current{k})
		
		for j = 1:numel(note{k})
			
			add(end + 1).note_id    = note_id{k}(j);
			add(end).annotated_type = table;
			add(end).annotated_id   = obj(k).id;
			add(end).user_id        = user.id;
			
		end

		continue;
		
	end
	
	%--
	% handle general case
	%--
	
	% NOTE: add all relations not in current set but in desired notes
	
	[ignore, append] = setdiff({note{k}.content_hash}, {current{k}.content_hash}); 
	
	for j = 1:numel(append)
		
		add(end + 1).note_id    = note_id{k}(append(j));
		add(end).annotated_type = table;
		add(end).annotated_id   = obj(k).id;
		add(end).user_id        = user.id;

	end
	
	% NOTE: remove all relations in current set but not in desired notes
	
	[ignore, release] = setdiff({current{k}.content_hash}, {note{k}.content_hash}); 
	
	if ~isempty(release)
		remove = [remove, relation{k}(release)];
	end
	
end

%--
% add annotations
%--

if ~isempty(add)	
	set_database_objects(store, add, [], fieldnames(add), 'annotation');
end

%--
% delete annotations
%--

if ~isempty(remove)
	delete_database_objects(store, remove, 'annotation');
end


