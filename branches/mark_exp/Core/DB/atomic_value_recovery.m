function value = atomic_value_recovery(value)

if is_m2xml(value)
	
	value = xml2m(value);
	
% NOTE: this string test resolves (hides) non-uniform recovery problem described above

elseif ischar(value)
	
	% NOTE: we are currently only escaping quotes
	
	value = strrep(value, '''''', '''');
	
% 	value = spcharout(value);
	
end
