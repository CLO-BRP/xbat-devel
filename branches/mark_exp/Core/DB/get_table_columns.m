function column = get_table_columns(store, table)

% get_table_columns - from database
% ---------------------------------
%
% column = get_table_columns(store, table)
%
% Input:
% ------
%  store - database
%  table - name
% 
% Output:
% -------
%  column - names

% NOTE: we simply get an object, whatever the engine we are already getting column names

[status, obj] = query(store, ['SELECT * FROM ', table, ' LIMIT 1;']);

column = fieldnames(obj);
