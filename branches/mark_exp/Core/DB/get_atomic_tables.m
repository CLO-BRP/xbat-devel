function name = get_atomic_tables(file)

% get_atomic_tables - get non-relation table names
% ------------------------------------------------
%
% name = get_atomic_tables(file)
%
% Input:
% ------
%  file - database
%
% Output:
% -------
%  name - of tables

name = get_tables(file);

for k = numel(name):-1:1
	if findstr(name{k}, '_'), name(k) = []; end
end