function [status, result] = establish_attachment(store)

% establish_attachment - create table to support generic attachment
% -----------------------------------------------------------------
%
%     [sql, lines] = establish_attachment
%
% [status, result] = establish_attachment(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  status - of query request
%  result - of query

hint = column_type_hints;

attachment.attachable_id = hint.integer;

attachment.attachable_type = hint.string;

attachment.asset_id = hint.integer;

attachment.asset_type = hint.string;

attachment.created_at = hint.ts; 

attachment.modified_at = hint.ts;

sql = create_table(attachment);

%--
% OUTPUT OR EXECUTE QUERY
%--

if ~nargin
	store = [];
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql);
end

