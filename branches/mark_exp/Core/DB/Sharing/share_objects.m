function [sharing, shared] = share_objects(store, obj, recipient, user, type)

% share_object - share an object in a shareable database
% ------------------------------------------------------
%
% [sharing, shared] = share_object(store, obj, recipient, user, type)
%
% Input:
% ------
%  store - to consider
%  obj - to share
%  recipient - of object
%  user - sharing
%  type - of object shared
%
% Output:
% -------
%  sharing - instance
%  shared - object

%--------------------
% HANDLE INPUT
%--------------------

%--
% make sure we have object type from input
%--

if nargin < 5
	type = inputname(2);
end

if isempty(type)
	error('Unable to determine type of shared object from input.');
end

% NOTE: this confuses levels of the code, this default does not belong here

%--
% make sure we have user sharing
%--

if nargin < 4
	user = get_barn_user(store);
end

%--
% make sure we have needed identifiers
%--

has_id = iterate(@isfield, {obj, recipient, user}, 'id');

if any(~has_id)
	error('Object to share, recipient, and user sharing must have ''id'' field.');
end

%--
% make sure we have properly matching objects and recipients
%--

if (numel(obj) > 1 && numel(recipient) > 1) && numel(obj) ~= numel(recipient)
	
	error('Objects and recipients in sharing must match in number, or one must be singular.');
	
end

%--------------------
% SHARE OBJECTS
%--------------------

%--
% get shared representation of objects
%--

shared = get_shared_objects(store, obj, type);

%--
% create acts of object sharing
%--

if numel(obj) == 1 && numel(recipient) > 1

	% single object to multiple recipients
	
	for k = 1:numel(recipient)
		sharing(k) = struct('shared_id', shared.id, 'user_id', user.id, 'recipient_id', recipient(k).id);
	end
	
elseif numel(obj) > 1 && numel(recipient) == 1

	% multiple objects to single recipient
	
	for k = 1:numel(obj)
		sharing(k) = struct('shared_id', shared(k).id, 'user_id', user.id, 'recipient_id', recipient.id);
	end

else

	% matching objects and recipients
	
	for k = 1:numel(obj)
		sharing(k) = struct('shared_id', shared(k).id, 'user_id', user.id, 'recipient_id', recipient(k).id);
	end

end

sharing = set_database_objects(store, sharing);


