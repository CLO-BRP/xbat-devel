function [name, info] = get_database_views(file)

% get_database_views - get database views and some information
% ------------------------------------------------------------
%
% [name, info] = get_database_views(file)
%
% Input:
% ------
%  file - database
%
% Output:
% -------
%  name - of views
%  info - for views

if nargout < 2
	name = get_database_elements(file, 'view'); 
else
	[name, info] = get_database_elements(file, 'view'); 
end