function result = recover_values(result, opt)

% recover_values - recover possibly encoded values from database strings
% ----------------------------------------------------------------------
%
% result = recover_values(result, opt)
%
% Input:
% ------
%  result - struct array from database query
%  opt - struct
%
% Output:
% -------
%  result - after recovery
% 
% See Also: sqlite, mysql

%--
% set default recovery to be uniform recovery
%--

% NOTE: the non-uniform recovery it slower, and should be a rare case

if nargin < 2
	opt.uniform = 1;
end

field = fieldnames(result);

%--
% non-uniform recovery
%--

if ~opt.uniform
	
	for j = 1:numel(result)
		
		for k = 1:numel(field)

			if ischar(result(j).(field{k}))
				result(j).(field{k}) = atomic_value_recovery(result(j).(field{k}));
			end

		end
		
	end

%--
% uniform recovery
%--

else
	
	% NOTE: remove fields that need no recovery from iteration

	for k = numel(field):-1:1
		
		if ~ischar(result(1).(field{k}))
			field(k) = [];
		end
		
	end

	if isempty(field)
		return;
	end

	% TODO: we are having problems with uniform recovery when a column is not uniform, go figure
	
	for j = 1:numel(result)
		
		for k = 1:numel(field)
			result(j).(field{k}) = atomic_value_recovery(result(j).(field{k}));
		end
		
	end

end