function [value, views] = database_has_view(file, view)

% database_has_view - check for view existence
% --------------------------------------------
% 
% [value, views] = database_has_view(file, view)
%
% Input:
% ------
%  file - database
%  view - view 
%
% Output:
% -------
%  value - indicator
%  views - all views

% TODO: extend to other database engines, call the engine method from here

views = get_database_views(file);

value = string_is_member(view, views);