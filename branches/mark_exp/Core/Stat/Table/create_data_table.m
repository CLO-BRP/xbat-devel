function table = create_data_table(varargin)

% create_data_table - that supports export and plot operations
% ------------------------------------------------------------
%
% table = create_data_table('field', value, ... )
%
% Input:
% ------
%  field - name
%  value - of field
%
% Output:
% -------
%  table - for data

%--
% create prototype
%--

table.name = '';

table.fieldname = '';

table.column.names = {};

table.column.types = {};

table.data = [];

table.id = [];

%--
% update from input
%--

if numel(varargin)
	table = parse_inputs(table, varargin{:}, 'flatten');
end

% TODO: check consistency of various input arguments

% TODO: consider the empty case

if ~iscell(table.data) && ~isnumeric(table.data)
	error('Table data must be a cell or matrix.');
end

% NOTE: we only consider size constraints when they make sense, when we have non-trivial data tables

if ~isempty(table.data) && (size(table.data, 2) ~= numel(table.column.names))
	error('Data columns and column names must be the same length.');
end

if ~isempty(table.id) && (size(table.data, 1) ~= numel(table.id))
	error('Row identifiers must match data rows.'); 
end

