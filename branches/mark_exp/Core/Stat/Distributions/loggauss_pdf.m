function y = loggauss_pdf(x,p)

% loggauss_pdf - compute lognormal pdf
% -------------------------------------
% 
% y = loggauss_pdf(x,p)
%
% Input:
% ------
%  x - points to evaluate
%  p - lognormal parameters [mean, deviation]
%
% Output:
% -------
%  y - pdf values

%--
% evaluate pdf at points
%--

z = (log(x) - p(1)) ./ p(2);
p(1:2) = 1;
y = gauss_pdf(z,p);

fig; plot(x,y); axes_scale_bdfun(gca);
