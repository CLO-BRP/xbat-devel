function ax = get_matrix_axes(par, varargin)

% TODO: generalize to select based on row or column, not just on both

ax = findobj(par, ...
	'type', 'axes', 'tag', fields2tag(varargin{:}) ...
);