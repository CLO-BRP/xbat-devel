function tag = fields2tag(varargin)

if numel(varargin) == 1
	fields = varargin{1};
else
	fields = varargin;
end

tag = str_implode(fields, '::');