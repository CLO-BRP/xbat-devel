function var = create_var(varargin)

% create_var - description struct

%--
% create template
%--

var.column = '';

var.label = '';

var.type = 'continuous'; 

%--
% parse and handle input
%--

var = parse_inputs(var, varargin{:});

if isempty(var.label)
	var.label = var.column;
end

if ~string_is_member(var.type, {'continuous', 'categorical', 'ordinal', 'discrete'})
	error(['Unrecognized variable type ''', var.type, '''.'])
end