function R = bench_cuda_histogram(X,c,b,n)

% bench_cuda_histogram - benchmark cuda accelerated histogram code
% -----------------------------------------------------
%
% R = test_cuda(X,T,n)
%
% Input:
% ------
%  X - input image
%  c - bin count
%  b - bounds [min, max]
%  n - number of iterations
%
% Output:
% -------
%  R - timing and accuracy results

if ((nargin < 2) || isempty(c))
	c = 256;
end

if ((nargin < 3) || isempty(b))
	b = [0,255];
end

if ((nargin < 4) || isempty(n))
	n = 10;
end

sX = single(X);

for k = 1:n

	tic; Y1 = hist_1d_(X, c, b); t1(k) = toc;
	tic; Y2 = cuda_histogram_mex(sX, c, b);  t2(k) = toc;

	E(k,:) = fast_min_max(uint32(Y1) - Y2);
end

disp(' ');
disp('TIME, CUDA TIME, SPEEDUP, ERROR');

R = [t1', t2', (t1./t2)', E]
