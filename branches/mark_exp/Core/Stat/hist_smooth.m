function hist_smooth

% hist_smooth - compute smoothed histogram
% ----------------------------------------
%
% [h,c,v] = hist_smooth(X,e,n,b);
% 
% Input:
% ------
%  X - input image
%  e - epsilon parameter
%  n - number of bins (def: 256)
%  b - bounds for values (def: extreme values)
%
% Output:
% -------
%  h - bin counts
%  c - bin centers
%  v - bin breaks

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-09-16 01:32:06-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% compute histogram
%--
		
[h,c,v] = hist_1d(X,n,b)		
	
%--
% smooth histogram
%--
	
O = v(1);
w = c(2) - c(1);

m = round(E / w);
LUT = conv2(h,ones(1,2*m + 1),'same');
