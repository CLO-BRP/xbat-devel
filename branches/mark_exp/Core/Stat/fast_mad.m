function [scale, center] = fast_mad(X, center)

% fast_mad - fast median absolute deviation
% -----------------------------------------
%
% [scale, center] = fast_mad(X, center)
%
% Input:
% ------
%  X - input data
%  center - median
%
% Output:
% -------
%  scale - median absolute deviation
%  center - median

% NOTE: we use fast median computation to compute median absolute deviation

% NOTE: clearly the input may not be the actual median

if nargin < 2
	center = fast_median(X);
end 

scale = fast_median(abs(X - center));
