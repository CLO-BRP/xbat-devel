function test_peak_valley

% TODO: extend to test and exemplify valley, and joint peak valley processing

for k = 1:9
	
	if k == 6
		x = 1;
		
	elseif k == 7
		x = []; % NOTE: unchecked this causes a segfault in the MEX
		
	elseif k == 8
		x = ones(128, 1);
		
	elseif k == 9
		x = (1:128);
		
	else
		x = linear_filter(randn(128, 1), filt_binomial(63, 1));
		
	end
	
	[ix, h, w] = fast_peak_valley(x, 1); 
	
	fig; 
	
	% NOTE: we display the signal and the peak locations
	
	plot(x); hold on; plot(ix, x(ix), 'ro');
	
	% NOTE: we display the peak widths
	
	for j = 1:numel(ix)
		plot(ix(j) + w(:, j) .* [-1, 1]', x(ix(j)) * ones(1, 2), 'g-o');
	end
	
end