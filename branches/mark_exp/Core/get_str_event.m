function [event, log] = get_str_event(par, str, data)

% get_str_event - get event corresponding to string
% -------------------------------------------------
%
% [event, log] = get_str_event(par, str, data)
%
% Input:
% ------
%  par - browser handle
%  str - event string
%  data - browser state
%
% Output:
% -------
%  event - event
%  log - log

%--
% get state of browser if needed
%--

if nargin < 3 || isempty(data)
	data = get_browser(par);
end

if isempty(str)
    event = []; log = []; return;
end

%--
% parse and group event strings
%--

if ischar(str)
	str = {str};
end

[name, id] = parse_event_str(str);

[name, ignore, ix] = unique(name);

%--
% get events from logs
%--

for k = 1:length(name)
	
    %--
    % get current log from name and events from log
    %--
	
	log(k) = get_browser_log(par, name{k}, data);
	
	% NOTE: the identifiers are selected using the name index and 'unique' reconstruction array
	
    part{k} = log_get_events(log(k), id(ix == k));

end

%--
% interleave events from different logs and expand log
%--

% TODO: at the moment we are not respecting the input string order

event = repmat(event_create, size(str));

for k = 1:numel(name)
	
	pix = find(ix == k);
	
	for j = 1:numel(pix)
		event(pix(j)) = part{k}(j);
	end
	
end

log = log(ix);
