function name = get_name(caller)

% get_name - get name of extension by examining caller location
% -------------------------------------------------------------
%
% name = get_name(caller)
%
% Output:
% -------
%  name - name of extension if computable

%--
% compute name from caller location
%--

% NOTE: this is the name of the parent directory of the caller

name = caller.loc{end};

