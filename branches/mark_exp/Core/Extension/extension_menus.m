function value = extension_menus(pal, ext, context)

% extension_menus - add extension menus to palettes
% -------------------------------------------------
%
% value = extension_menus(pal)
%
% Input:
% ------
%  pal - palette handle
%
% Output:
% -------
%  value - extension menus indicator

%--
% check palette is extension palette and get extension
%--

if nargin < 2
	[value, ext] = is_extension_palette(pal);
else
	value = is_extension_palette(pal);
end

%--
% add extension menus
%--

if value
	
	%--
	% add extension user menus
	%--
	
	% NOTE: this test is standing in as a widget test, make sure the semantics are right
	
	if nargin > 2
		
		file_menu(pal);
		
		parameter_menu(pal, ext, context);
		
	end
	
	preset_menu(pal, ext);
	
	help_menu(pal, ext);
	
	% EXPERIMENTAL CODE
	
	% NOTE: the state query required for the state is relatively expensive
	
% 	if ext.active && ~is_widget(ext)
% 		
% 		par = get_palette_parent(pal);
% 
% 		if ~isempty(par)
% 			toggle_menu(pal, ext, get_extension_active_state(par, ext));
% 		end
% 		
% 	end
	
	%--
	% add extension developer menus
	%--
	
	pal_menu(pal);
	
end


%-------------------------------------------
% FILE_MENU
%-------------------------------------------

function file_menu(pal)

top = uimenu(pal, 'label', 'File');

handles = [];


handles(end + 1) = menu_control(top, '(Figure)');

handles(end + 1) = menu_control(top, {'save', 'save figure'});

handles(end + 1) = menu_control(top, {'save_as', 'save figure as ...'});

handles(end + 1) = menu_control(top, {'export', 'export figure ...'});

handles(end + 1) = menu_control(top, '(Print)');

handles(end + 1) = menu_control(top, {'setup', 'page setup ...'});

handles(end + 1) = menu_control(top, {'preview', 'print preview ...'});

handles(end + 1) = menu_control(top, {'print', 'print ...'}, 0, 'accelerator', 'p');

handles(end + 1) = menu_control(top, 'close', 0, 'separator', 'on', 'accelerator', 'w');

set(handles, 'callback', @file_menu_callback);

parenthetical_menus(handles);


%-------------------------------------------
% FILE_MENU_CALLBACK
%-------------------------------------------

function file_menu_callback(obj, eventdata)

widget = ancestor(obj, 'figure');

switch get(obj, 'tag')
	
	case 'save'
		
	case 'save_as'
		
	case 'export'
		
	case 'setup'
		
		pagesetupdlg(widget);
		
	case 'preview'
		
		printpreview(widget);
		
	case 'print' 
		
		printdlg(widget);
		
	case 'close'
		
		% TODO: reconsider the mapping of the single key 'w' for closing
		
		% TODO: factor this widget query in some way, consider refactoring the whole operation
		
		data = get(widget, 'userdata'); 
		
		par = data.parent; ext = data.opt.ext;
		
		switch ext.type
			
			% TODO: generalize this function to work for various types
			
			case 'sound_feature', deactivate_sound_feature(ext, par);
		
			case 'widget', close(widget);
				
			otherwise, return;
				
		end 
		% NOTE: conventionally the extension types use an 'active' control
		
		pal = get_extension_palette(ext, par);
		
		if ~isempty(pal)
			set_control(pal, 'active', 'value', 0);
		end
		
end

	
%-------------------------------------------
% GET_EXTENSION_ACTIVE_STATE
%-------------------------------------------

% TODO: we should bring some display of active state available when the palette is collapsed

function state = get_extension_active_state(par, ext, data)

%--
% get browser state if needed
%--

if nargin < 3
	data = get_browser(par);
end

%--
% get active extension of relevant type and test
%--

active = data.browser.(ext.subtype).active;

if isempty(active)
	state = 0; return;
end

if ischar(active) 
	active = {active};
end

state = string_is_member(ext.name, active);


