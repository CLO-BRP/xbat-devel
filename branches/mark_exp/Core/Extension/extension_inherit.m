function ext = extension_inherit(fun, par)

% extension_inherit - create extension from parent
% ------------------------------------------------
%
% ext = extension_inherit(fun, par)
%
% Input:
% ------
%  fun - child main
%  par - parent
%
% Output:
% -------
%  ext - new child extension

% TODO: consider extending this function to take extension-field pairs as input

% NOTE: the goal is to implement multiple-inheritance using preset-type scoping

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% get caller and computable fields
%--

caller = get_caller(fun);

if ~isempty(caller)
	
	% NOTE: these 'get' functions are private because of the fragility of stack use
	
	type = get_type(caller);
	
	name = get_name(caller);
	
% 	info = get_info(caller); modified = info.date;
	
	info = dir2(fileparts(caller.file)); 
	
	% NOTE: in this case, loaded will always exceed modified
	
	modified = info.datenum; loaded = now;
	
	main = get_main(caller);

else
	
	% NOTE: this should never happen!
	
	type = []; name = ''; modified = ''; main = [];
	
end

%--
% check parent type
%--

% TODO: implement type hierarchy, use it here and in the create interface

% NOTE: for the moment we are suspending this condition

% if ~strcmp(par.subtype, type)
% 	error('Parent and child must have the same type.');
% end

%----------------------------
% CREATE EXTENSION
%----------------------------

%--
% inherit from same type
%--

if strcmp(par.type, type)
	
	%--
	% copy parent
	%--
	
	% NOTE: inherit first, we get everything
	
	ext = par;

	%--
	% create link to parent
	%--

	% NOTE: storing name and handle to parent allows multiple ways of access

	ext.parent.name = par.name;

	ext.parent.type = par.subtype;
	
	ext.parent.main = par.fun.main;

	%--
	% update parent functions with child functions
	%--

	% NOTE: we update parent functions with implemented child functions

	fun = flatten_struct(ext.fun);

	fun_new = flatten_struct(attach_fun(main, type));

	field = fieldnames(fun_new);

	for k = 1:length(field)

		if isfield(fun, field{k}) && ~isempty(fun_new.(field{k}))
			fun.(field{k}) = fun_new.(field{k});
		end

	end

%--
% inherit from different type
%--

else
	
	%--
	% create new extension 
	%--
	
	ext = extension_create(caller.name);
	
	%--
	% create link to parent
	%--

	% NOTE: storing name and handle to parent allows multiple ways of access

	ext.parent.name = par.name;
	
	ext.parent.type = par.subtype;

	ext.parent.main = par.fun.main;
	
	%--
	% check for available parent functions
	%--
	
	% TODO: do we need a cross-type adaptation here?
	
	fun = flatten_struct(ext.fun);

	fun_new = flatten_struct(par.fun);

	field = fieldnames(fun_new);

	for k = 1:length(field)

		% NOTE: we get the parent function if it is available and we have not implemented it
		
		if isfield(fun, field{k}) && isempty(fun.(field{k})) && ~isempty(fun_new.(field{k}))
			fun.(field{k}) = fun_new.(field{k});
		end

	end
	
end

%--
% add helper field if missing
%--

% TODO: figure out why this is needed

if ~isfield(fun, 'helper')
	fun.helper = struct;
end

%--
% collapse handles to allow hierarchical access
%--

ext.fun = collapse(fun);

%--
% update relevant fields
%--

ext.name = name;

ext.fieldname = genvarname(name);

ext.subtype = type;

% NOTE: this declares when this extensions' code was modified, not the parents

% NOTE: this means that an parent based update of children must be triggered by the parents

ext.modified = modified;

ext.loaded = loaded;

ext.fun.main = main;

