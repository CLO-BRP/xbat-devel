function preset = load_preset_dialog(ext, type)

% load_preset_dialog - allow user to select from available presets to load
% ------------------------------------------------------------------------
%
% preset = load_preset_dialog(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  preset - loaded preset

%------------------------
% HANDLE INPUT
%------------------------

%--
% return if extension has no presets
%--

% NOTE: there is nothing to load return

if ~has_any_presets(ext)
	preset = []; return;
end

%--
% set and verify default type
%--

if nargin < 2
	types = get_parameter_types(ext); type = types{1}; % type = 'compute';
end

types = get_preset_types(ext);

value = find(strcmp(types, type), 1);

if isempty(value)
	value = 1; type = types{1};
end 

%--
% get starting type preset names
%--

[names, count] = get_preset_names(ext, type);

% NOTE: there is nothing to load return

if ~count && (length(types) < 2)
	preset = []; return; 
end

%------------------------
% CREATE CONTROLS
%------------------------

%--
% header
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', ['Preset  (', ext.name, ')'] ...
);

%--
% type
%--

types = title_caps(types);

control(end + 1) = control_create( ...
	'name', 'type', ...
	'style', 'popup', ...
	'width', 2/3, ...
	'string', types, ...
	'value', value ...
);

%--
% name
%--

control(end + 1) = control_create( ...
	'name', 'name', ... 
	'style', 'popup', ...
	'onload', 1, ...
	'string', names, ...
	'value', 1 ...
);

%--
% info
%--

control(end + 1) = control_create( ...
	'name', 'info', ...
	'style', 'listbox', ...
	'max', 2, ...
	'lines', 3 ...
);

%------------------------
% CREATE DIALOG
%------------------------

%--
% configure dialog
%--

opt = dialog_group; 

opt.width = 12;

opt.text_menu = 1;

% TODO: consider a helper that gets the parent color and add parent input

opt.header_color = get_extension_color(ext.subtype);

%--
% present dialog
%--

out = dialog_group('Load ...', control, opt, {@load_preset_callback, ext});

% NOTE: if dialog was cancelled or aborted return

values = out.values;

if isempty(values)
	preset = []; return;
end

%--
% load preset
%--

% NOTE: the name value is packed in a cell

preset = preset_load(ext, cellfree(values.name), lower(cellfree(values.type)));


%------------------------------------
% GET_PRESET_NAMES
%------------------------------------

function [names, count] = get_preset_names(ext, type)

[files, store] = get_preset_files(ext, type);

if strcmp(store, 'mat')
	names = file_ext(files);
else
	names = files; 
end

count = length(names);

if isempty(names)
	names = {'(No Presets Found)'};
end


%------------------------------------
% LOAD_PRESET_CALLBACK
%------------------------------------

function load_preset_callback(obj, eventdata, ext)

%--
% get callback context
%--

[control, pal] = get_callback_context(obj);

%--
% handle control by name
%--

switch control.name
	
	case 'type'
		
		%--
		% get type and name handles
		%--
		
		type = get_control(pal.handle, 'type', 'value');
		
		handles = get_control(pal.handle, 'name', 'handles');
		
		%--
		% update name control and info
		%--
		
		[names, count] = get_preset_names(ext, lower(type{1}));
		
		set(handles.obj, 'string', names, 'value', 1);
		
		% NOTE: this should update the info display when it is available
		
		if count
			load_preset_callback(handles.obj, eventdata, ext);
		end
		
	case 'name'
		
		%--
		% get preset name and info handles
		%--
		
		name = get_control(pal.handle, 'name', 'value');
		
		handles = get_control(pal.handle, 'info', 'handles');

		%--
		% load preset and update info control
		%--
		
		% NOTE: this failure is typically due to a no presets indicator string
		
		try
			str = preset_info_str(preset_load(ext, name{1}));
		catch
			str = {};
		end
		
		% TODO: we really want a preset info string function

		set(handles.obj, 'string', str, 'value', []);
		
	case 'info'

end


%------------------------------------
% PRESET_INFO_STR
%------------------------------------

function str = preset_info_str(preset) %#ok<INUSD>

% TODO: use a somewhat generic 'struct_info_str' for the preset

str = cell(0); 


