function presets = get_presets(ext, type, varargin)

% get_presets - get extension presets
% -----------------------------------
%
% presets = get_presets(ext, type, 'field', value, ...)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%  field - field name
%  value - field value
%
% Output:
% -------
%  presets - presets

%--------------------------------------
% GET PRESET FILES
%--------------------------------------

%--
% set default type
%--

if (nargin < 2) || isempty(type)
	type = 'compute';
end

%--
% initialize presets
%--

% NOTE: this is not currently used, in the non-trivial case

presets = empty(preset_create);

%--
% get preset files and root 
%--

[files, root] = get_preset_files(ext, type);

if isempty(root) || isempty(files)
	return;
end

names = file_ext(files);

%--------------------------------------
% LOAD PRESETS
%--------------------------------------

for k = 1:length(names)
	
	try
		preset = preset_load(ext, names{k});
	catch
		continue;
	end
	
	% NOTE: the struct udpate provides a weak normalization
	
	opt = struct_update; opt.flatten = 0;
	
	presets(end + 1) = struct_update(preset_create, preset, opt);
	
end

%---------------------------------------------------------------
% SELECT PRESETS
%---------------------------------------------------------------

% TODO: add name filtering to this function

% TODO: this kind of selection will be factored, expanded, and improved

%--
% consider selection when we have something to select and criteria
%--

if ~isempty(varargin) && ~isempty(presets)
	
	%--
	% get field value pairs from input
	%--
	
	[field, value] = get_field_value(varargin);
	
	%--
	% loop over fields
	%--
	
	for j = 1:length(field)
				
		%--
		% check preset field
		%--
				
		if isfield(presets, field{j})

			for k = length(presets):-1:1
								
				if ~isequal(presets(k).(field{j}), value{j})
					presets(k) = [];
				end
				
			end
			
		%--
		% check preset values field
		%--
		
		elseif isfield(presets(1).values, field{j})

			for k = length(presets):-1:1
				
				if ~isequal(presets(k).values.(field{j}), value{j})
					presets(k) = [];
				end
				
			end
			
		end
		
		
	end
	
end
