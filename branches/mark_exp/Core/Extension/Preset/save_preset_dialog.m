function [info, store] = save_preset_dialog(preset, type)

% save_preset_dialog - create preset menu
% --------------------------------------
%
% [info, store] = save_preset_dialog(preset, type)
%
% Input:
% ------
%  preset - preset to save
%  type - preset type
%
% Output:
% -------
%  info - preset store info
%  store - preset store

%------------------
% HANDLE INPUT
%------------------

%--
% set default preset type
%--

if nargin < 2
	type = 'compute';
end

%------------------
% CREATE CONTROLS
%------------------

control = empty(control_create);

%-----------------
% INFO
%-----------------

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', ['Preset  (', preset.ext.name, ')'] ...
);

%--
% type
%--

types = title_caps(get_preset_types(preset.ext));

value = find(strcmp(types, type));

if isempty(value)
	value = 1;
end 

control(end + 1) = control_create( ...
	'name', 'type', ...
	'style', 'popup', ...
	'width', 2/3, ...
	'string', types, ...
	'value', value ...
);

%--
% name
%--

if isfield(preset.ext.parameter, 'preset')
	name = preset.ext.parameter.preset; 
else
	name = '';
end

control(end + 1) = control_create( ...
	'name', 'name', ...
	'space', 1, ...
	'onload', 1, ...
	'style', 'edit', ...
	'string', name, ...
	'type', 'filename' ...
);

%--
% tags
%--

control(end + 1) = control_create( ...
	'name', 'tags', ...
	'style', 'edit', ...
	'space', 0.75, ...
	'color', ones(1,3) ...
);

%--
% notes
%--

control(end + 1) = control_create( ...
	'name', 'notes', ...
	'style', 'edit', ...
	'color', ones(1,3), ...
	'lines', 3 ...
);

%------------------
% CREATE DIALOG
%------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 12;

opt.header_color = get_extension_color(preset.ext.subtype);

opt.text_menu = 1;

name = 'Save ...';

%--
% create dialog
%--

out = dialog_group(name, control, opt, @save_preset_callback);

if strcmpi(out.action, 'cancel')
	info = []; store = ''; return;
end

%--
% update preser and return
%--

values = out.values;

[info, store] = preset_save(preset, values.name, lower(values.type{1}), values.tags, values.notes);


%----------------------------------
% SAVE_PRESET_CALLBACK
%----------------------------------

function save_preset_callback(obj, eventdata) %#ok<INUSD>

[control, pal] = get_callback_context(obj);

switch control.name
	
	case 'name'
		
		set_control(pal.handle, 'OK', 'enable', proper_filename(get(obj, 'string')));
		
	case 'tags'
		
		value = tags_to_str(str_to_tags(get_control(pal.handle, 'tags', 'value')));
		
		set_control(pal.handle, 'tags', 'value', value);
		
	case 'notes'
	
end

