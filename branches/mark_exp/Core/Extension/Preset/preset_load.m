function preset = preset_load(ext, name, type, par)

% preset_load - load preset from file
% -----------------------------------
%
% preset = preset_load(ext, name, type, par)
%
% Input:
% ------
%  ext - extension
%  name - preset name
%  type - preset type
%  par - parent
%
% Output:
% -------
%  preset - preset or name

% TODO: consider exception and error handling in application context

% NOTE: helpers failing to load preset output an empty preset

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% set no parent and context default
%--

if nargin < 4
	par = []; context = struct;
end

% NOTE: get fresh extension and context from parent

if ~isempty(par)
	[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name); % context = get_extension_context(ext, par);
end

%--
% set default main parameter type
%--

if nargin < 2
	types = get_parameter_types(ext); type = types{1}; % type = 'compute';
end

%-------------------------
% LOAD PRESET
%-------------------------

%--
% get preset store and file name 
%--

% NOTE: this provides a name, we should rename perhaps

store = preset_file(ext, name, type); file = [store, '.mat'];

%--
% get load helper
%--

if strcmp(type, 'compute')
	loader = ext.fun.parameter.load;
else
	loader = ext.fun.(type).parameter.load;
end

% NOTE: we allow for two types of presets, MAT files and directories

%--
% directory preset
%--

if exist(store, 'dir')
	
	if isempty(loader)
		error('Unable to load directory preset without helper.');
	end
	
	try
		preset = loader(store, context); preset.name = name;
	catch
		preset = []; extension_warning(ext, 'Failed to load directory preset.', lasterror);
	end
	
%--
% file preset
%--

elseif exist(file, 'file')
	
	%--
	% load file possibly using helper
	%--
	
	% NOTE: we handle the failure to simply read a MAT file for uniformity
	
	if isempty(loader)
		try
			load(store); preset.name = name;
		catch
			preset = []; extension_warning(ext, 'Failed to load preset file.', lasterror);
		end
	else
		try
			preset = loader(store, context); preset.name = name;
		catch
			preset = []; extension_warning(ext, 'Failed to load file preset.', lasterror);
		end
	end

%--
% unable to find preset store
%--

else

	error('Unable to find preset file or directory.');

end
