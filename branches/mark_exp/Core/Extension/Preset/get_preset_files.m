function [files, store, root] = get_preset_files(ext, type)

% get_preset_files - get preset files and root
% --------------------------------------------
%
% [files, store, root] = get_preset_files(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%
% Output:
% -------
%  files - preset filenames
%  store - storage type 'mat' or 'dir'
%  root - presets root

%--
% handle input
%--

if nargin < 2
	type = 'compute';
end

%--
% get preset directory
%--

root = preset_dir(ext, type);

% NOTE: if the root is empty, there are no preset files and the store type is unknown

if isempty(root)
	files = {}; store = ''; return;
end

%--
% get preset files
%--

% NOTE: we first try to get MAT file presets, then directories

% TODO: consider if we want the output to include the file extension

content = what_ext(root, 'mat');

if ~isempty(content.mat)
	
	%--
	% MAT file presets
	%--
	
	files = content.mat; store = 'mat'; 
	
else
	
	%--
	% directory presets
	%--
	
	for k = length(content.dir):-1:1
		
		% TODO: consider using 'isstrprop' here, this is a new function
		
		if (content.dir{k}(1) == '.') || (content.dir{k}(1) == '_')
			content.dir(k) = [];
		end
		
	end
	
	files = content.dir; store = 'dir';
	
end

%--
% sort files if needed
%--

% NOTE: in this case the store type is also unknown

if isempty(files)
	store = ''; return;
end

files = sort(files);
