function types = get_preset_types(ext)

% get_preset_types - get preset types for extension
% -------------------------------------------------
%
% types = get_preset_types(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  types - preset types

% NOTE: we declare parameters as potential preset types, having parameters is enough

% NOTE: 'has_presets' checks control availability, it uses this function to determine types to examine

types = get_parameter_types(ext);