function root = extensions_root(type, project)

% extensions_root - extensions root directory
% ------------------------------------------
%
% root = extensions_root(type, project)
%
% Input:
% ------
%  type - extension type (def: '', get root of all extensions)
%  project - name
% 
% Output:
% -------
%  root - root directory for extensions

% TODO: develop 'set' and 'get', and allow multiple roots

%--
% all extensions root
%--

if nargin < 2 || isempty(project)
	
	root = fullfile(xbat_root, 'Extensions');
	
else
	base = project_root(project);
	
	if exist(base, 'dir')
		root = fullfile(base, 'Extensions');
	else
		error(['Project directory ''', base, ''' not found.']);
	end
end

%--
% specific extension type root
%--

if nargin && ~isempty(type)
	
	if ~ischar(type) || ~is_extension_type(type)
		error('Unrecognized extension type.');
	end
	
	root = fullfile(root, type_to_dir(type));
end

