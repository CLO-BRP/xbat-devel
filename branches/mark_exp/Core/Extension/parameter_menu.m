function parameter_menu(widget, ext, context)

% parameter_menu - create parameter editing menu for widgets
% ----------------------------------------------------------
%
% parameter_menu(widget, ext, context)
%
% Input:
% ------
%  widget - handle
%  ext - parent extension
%  context - context

%--
% check we are a widget, and that we can have presets
%--

% NOTE: having presets is a backwards proxy for controllable parameters

if ~is_widget_figure(widget) || ~has_presets(ext)
	return;
end

%--
% create top menu
%--

top = uimenu(widget, 'label', 'Edit');

%--
% create parameter menus
%--
		
types = get_preset_types(ext);

for k = 1:numel(types)
	
	%--
	% create parameter type header 
	%--
	
	% TODO: we might want to supress this in the case of pure 'widgets'

	uimenu(top, 'label', ['(', title_caps(types{k}), ')']);

	%--
	% get menu creation function handle and relevant parameters
	%--
	
	if strcmp(types{k}, 'compute')
		fun = ext.fun.parameter.menu.create; parameter = ext.parameters.compute;
	else
		fun = ext.fun.(types{k}).parameter.menu.create; parameter = ext.parameters.(types{k});
	end

	%--
	% create parameter type menus
	%--

	callback = {@parameter_menu_dispatcher, ext, types{k}};

	if ~isempty(fun)

		try
			handles = fun(top, parameter, context);
		catch
			handles = []; extension_warning(ext, 'Menu creation failed.', lasterror);
		end

		if ~isempty(handles)
			set(handles, 'callback', callback);
		end

	end

	% NOTE: the callback of the 'Edit All ...' opens and focuses the extension palette

	uimenu(top, ...
		'label', 'Edit All ...', 'tag', 'edit_all', 'callback', {@edit_all_callback, ext, types{k}} ...
	);


end

parenthetical_menus(get(top, 'children'));


%-------------------------------------------
% EDIT_ALL_CALLBACK
%-------------------------------------------

function edit_all_callback(obj, eventdata, ext, type) %#ok<INUSL>

%--
% get widget and info
%--

widget = ancestor(obj, 'figure'); info = get_widget_info(widget);

%--
% get fresh extension and open palette
%--

[ext, ignore, context] = get_browser_extension(ext.type, info.parent, ext.name);

pal = open_extension_palette(ext, context, info.parent, widget);

%--
% focus on desired panel
%--

palette_minmax('min', pal); 

set_toggle_by_label(pal, title_caps(type), 'open');


%-------------------------------------------
% PARAMETER_MENU_DISPATCHER
%-------------------------------------------

function parameter_menu_dispatcher(obj, eventdata, ext, type) %#ok<INUSL>

%--
% get callback context
%--

callback = get_callback_context(obj, 'pack');

%--
% dispatch callback
%--

% NOTE: we try to get a callback handle, get context if one found, and dispatch

if strcmp(type, 'compute')
	fun = ext.fun.parameter.menu.callback;
else
	fun = ext.fun.(type).parameter.menu.callback;
end

if isempty(fun)
	return;
end

[ignore, ignore, context] = get_browser_extension(ext.type, callback.par.handle, ext.name);

% TODO: the callback can output a result, what should this be used for

try
	fun(callback, context);
catch
	extension_warning(ext, 'Menu callback failed.', lasterror);
end



% NOTE: the code below will not be used, something maybe salvaged

%-------------------------------------------
% EDIT_WIDGET_PARAMETERS
%-------------------------------------------

function edit_widget_parameters(obj, eventdata, widget, ext)

%--
% get parameters from widget
%--

ext = get_widget_extension(widget);

parameter = ext.parameter;

%--
% build and present parameter editing dialog
%--

% TODO: eventually move to 'extension_dispatcher' call

context = get_extension_context(ext, get_widget_parent(widget));

control = get_extension_controls(ext, context);

% NOTE: we fix the header string and make it non-collapsible

control(1).string = ['Widget  (', ext.name, ')']; control(1).min = 1;

out = dialog_group('Configure', control);

values = out.values;

if isempty(values)
	return;
end

% TODO: we need some code to update the parameters using the controls, compile?

%--
% update extension parameters in widget
%--

ext.parameter = parameter;

set_widget_extension(widget, ext);

