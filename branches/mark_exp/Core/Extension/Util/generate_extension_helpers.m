function generate_extension_helpers

% generate_extension_helpers - create helper functions
% ----------------------------------------------------
%
% generate_extension_helpers
%
% NOTE: refactoring to make these tasks simpler may trump code generation

%--
% get types
%--

types = get_extension_types;

%--
% make sure we have somewhere to put the helpers
%--

root = create_dir([fileparts(which('get_extensions')), filesep, 'Helpers']);

if isempty(root)
	error('Unable to create extension helpers directory.');
end

%--
% generate helpers
%--

for k = 1:length(types)

	type = types{k}; 

	%--
	% get extension helper
	%--
	
	helper = ['get_', type, '_ext'];
	
	file = [root, filesep, helper, '.m']; rel_path(file);

	fid = fopen(file, 'w');

	fprintf(fid,'%s\n\n%s\n', ...
		['function ext = ', helper, '(varargin)'], ...
		['ext = get_extensions(''', type, ''', ''name'', varargin{:});'] ...
	);

	fclose(fid);
	
% 	%--
% 	% extension names helper
% 	%--
% 	
% 	helper = [type, '_names'];
% 	
% 	file = [root, filesep, helper, '.m']; rel_path(file);
% 
% 	fid = fopen(file, 'w');
% 
% 	% TODO: consider an expression to match input, refactor as suggested below
% 	
% 	% NOTE: a function to get names of a type which match a pattern is probably useful
% 	
% 	fprintf(fid,'%s\n\n%s\n\n%s\n', ...
% 		['function [names, exts] = ', helper, '(varargin)'], ...
% 		['exts = get_extensions(''', type, ''');'], ...
% 		['if ~isempty(exts), names = {exts.name}; else, names = {}; end'] ...
% 	);
% 
% 	fclose(fid);
	
end