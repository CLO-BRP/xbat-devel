function ext = get_event_measure_ext(varargin)

ext = get_extensions('event_measure', 'name', varargin{:});
