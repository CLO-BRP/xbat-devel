function ext = get_log_measure_ext(varargin)

ext = get_extensions('log_measure', 'name', varargin{:});
