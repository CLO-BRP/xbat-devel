function ext = get_sound_attribute_ext(varargin)

ext = get_extensions('sound_attribute', 'name', varargin{:});
