function ext = get_widget_ext(varargin)

ext = get_extensions('widget', 'name', varargin{:});
