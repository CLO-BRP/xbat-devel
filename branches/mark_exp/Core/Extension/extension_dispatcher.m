function [result, info] = extension_dispatcher(type, name, request, par)

% extension_dispatcher - dispatch extension requests
% --------------------------------------------------
%
% [result, info] = extension_dispatcher(type, name, request, par)
%
% Input:
% ------
%  type - type
%  name - name
%  request - request
%  par - browser
%
% Output:
% -------
%  result - result
%  info - info

%--------------------
% HANDLE INPUT
%--------------------

%--
% initialize output for convenience
%--

result = struct; info = struct;

%--
% default active browser
%--

if nargin < 4
	par = get_active_browser; 
end 

if isempty(par)
	return;
end 

%--
% get debug state
%--

debug = get_user_preference(get_active_user, 'DEBUG');

%--------------------
% SETUP
%--------------------

%--
% get browser extension and context
%--

[ext, ignore, context] = get_browser_extension(type, par, name);

%--
% get parent type extension
%--

type = get_extensions('extension_type', 'name', title_caps(ext.subtype));

parameter = struct;

if has_any_parameters(type)
	
	% NOTE: we should be storing the computed parameters in the type extension
	
	try
		parameter = type.fun.parameter.create(context);
	catch
		parameter = struct; extension_warning(type, 'Failed to create parameters', lasterror);
	end
	
end

%--------------------
% HANDLE REQUEST
%--------------------

% NOTE: we handle the missing handle condition with an exception to reuse extension warning

info.started = clock;

try
	
	if ~isempty(type.fun.dispatch)
		result = type.fun.dispatch(request, parameter, context); info.status = 'done';
	else
		error('XBAT:ExtensionType:MissingDispatcher', 'Type dispatcher is not defined.');
	end
	
catch
	
	extension_warning(type, 'Failed to dispatch request.', lasterror); info.status = 'failed';

end

info.elapsed = etime(clock, info.started);

%--------------------
% DEBUG DISPLAY
%--------------------

% TODO: refine debug display and integrate error with debug when needed

if debug

	str = [ ...
		' DISPATCHING: ''', upper(request), ''' request for ''', ...
		upper(ext.name), ''' ''', upper(type.name) ''' extension. ' ...
	];
	
	disp(' ');
	str_line(str, '_');
	disp(' ');
	disp(str);
	str_line(str, '_');
	disp(' ');
	
	disp(' RESULT:')
	if ~trivial(result)
		disp(' ');
		disp(['   ', to_str(result)]);
	end
	disp(' ');
	
	disp(' STACK:');
	disp(' ');
	stack = dbstack('-completenames'); stack(1:2) = [];
	if ~isempty(stack)
		stack_disp(stack);
		disp(' ');
	end
	
	disp(' TIME:');
	disp(' ');
	time = [datestr(info.started), ' (', sec_to_clock(info.elapsed), ')'];
	disp(['   ', time]);
	disp(' ');
	
	str_line(str, '_');	
	disp(' ');
	
end



