function test_extension_names

% NOTE: currently 'get_extension_names' does not handle project extensions

types = get_extension_types;

for k = 1:numel(types) 
	
	names1 = get_extension_names(types{k}); [ignore, names2] = extension_roots(types{k}); 
	
	problem = setdiff(names1, names2);
	
	if ~isempty(problem)
		problem %#ok<NOPRT>
	end
	
end