function ext = extension_rename_dialog(ext)

% extension_rename_dialog - dialog to rename extension
% ----------------------------------------------------
%
% ext = extension_rename_dialog(ext)
%
% Input:
% ------
%  ext - extension to rename
%
% Output:
% -------
%  ext - renamed extension, if dialog not cancelled

%--
% handle string input
%--

% NOTE: we convert the tag into a duck

if ischar(ext)	
	
	tag = ext; 
	
	ext = parse_tag(tag, ...
		'::', {'subtype', 'name', 'parent'} ...
	);

	ext = get_extensions(ext.subtype, 'name', ext.name);

	if isempty(ext)
		error(['Unable to find extension described by ''', tag, ''' for renaming.']);
	end
	
end 

%--
% check for children
%--

% NOTE: rename is not currently supported for extensions with children

if has_children(ext)
	return;
end

%--
% build dialog controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', [title_caps(ext.subtype), '  (', ext.name, ')'] ...
);

control(end + 1) = control_create( ...
	'name', 'name', ...
	'style', 'edit', ...
	'type', 'filename', ...
	'onload', 1, ...
	'string', '' ...
);

%--
% configure and present dialog
%--

opt = dialog_group; 

opt.width = 13;

opt.header_color = get_extension_color(ext);

out = dialog_group('Rename ...', control, opt, {@dialog_callback, ext});

if isempty(out.values)
	return;
end

%--
% rename extension
%--

ext = extension_rename(ext, out.values.name);


%------------------
% DIALOG_CALLBACK
%------------------

function dialog_callback(obj, eventdata, ext) %#ok<INUSL>

%--
% get callback context
%--

[control, pal] = get_callback_context(obj);

%--
% handle control callback
%--

switch control.name
	
	case 'name'

		% TODO: develop validate callback framework for 'edit' controls
		
		% TODO: implement this as a custom validate callback
		
		name = get(obj, 'string'); valid = proper_filename(name);
		
		if valid
			exts = get_extensions(ext.subtype); names = setdiff({exts.name}, ext.name);
		end
		
		% NOTE: the shortcut means names are considered only when the name is valid
		
		set_control(pal.handle, 'OK', 'enable', valid && ~ismember(name, names));

		
end
