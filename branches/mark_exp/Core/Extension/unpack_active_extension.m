function [ext, ix, context] = unpack_active_extension(active)

% NOTE: this is the structure of a packed active extension

if isfield(active, 'ext') && isfield(active, 'ix') && isfield(active, 'context')
	
	ext = active.ext;
	
	if nargout > 1
		ix = active.ix;
	end
	
	if nargout > 2
		context = active.context;
	end

else
	
	ext = active; ix = []; context = [];

end