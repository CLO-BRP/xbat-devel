function [page_events, page_values, contexts] = detector_page_scan(ext, page, context, contexts, viewing, waiting)

%------------------------------
% HANDLE INPUT
%------------------------------

if nargin < 6
	waiting = 0;
end

if nargin < 5
	viewing = 0;
end

if nargin < 4
	contexts = [];
end

%------------------------------
% DETECT EVENTS IN PAGE
%------------------------------

%--
% compute considering whether extension handles multiple channels
%--

% COMPUTE ALL CHANNELS

if ext.multichannel

	context.page = rmfield(page, {'samples', 'filtered'});

	% TODO: include some page information in message

	page_events = empty(event_create);

	try
		switch nargout(ext.fun.compute)
			
			case 2
				[page_events, context] = ext.fun.compute(page, ext.parameter, context); page_values = [];
				
			case 3
				[page_events, page_values, context] = ext.fun.compute(page, ext.parameter, context);
				
			otherwise
				error('Compute function has improper signature, it must have 2 or 3 outputs.');
				
		end
	catch
		extension_warning(ext, 'Multi-channel compute failed on page.', lasterror);
	end

% LOOP OVER CHANNELS

else

	M = length(page.channels);

	%--
	% establish and update individual channel contexts
	%--
	
	if isempty(contexts)
		for k = 1:M
			contexts{k} = context;
		end
	else
		for k = 1:M
			contexts{k}.scan = context.scan;
		end
	end

	page_events = cell(1, M);
	
	page_values = cell(1, M);

	%--
	% compute and collapse page events
	%--

	for k = 1:M

		channel_page = get_channel_page(page, page.channels(k));

		contexts{k}.page = rmfield(channel_page, {'samples', 'filtered'});

		page_events{k} = empty(event_create);

		% TODO: include page and channel information in message

		db_disp; functions(ext.fun.compute)
		
		try
			switch nargout(ext.fun.compute)

				case 2
					[page_events{k}, contexts{k}] = ext.fun.compute(channel_page, ext.parameter, contexts{k}); page_values{k} = [];

				case 3
					[page_events{k}, page_values{k}, contexts{k}] = ext.fun.compute(channel_page, ext.parameter, contexts{k});

				otherwise
					error('Compute function has improper signature, it must have 2 or 3 outputs.');

			end
		catch
			extension_warning(ext, 'Compute failed.', lasterror);
		end

	end

	page_events = collapse_cell(page_events);

	page_values = collapse_cell(page_values);	
end

%-----------------------
% TRIM EVENTS
%-----------------------

if ~isempty(page_events)

	%--
	% consider page boundaries
	%--

	% NOTE: when start is session boundary don't allow negative page times

	if mod(page.interval, 2)

		for k = 1:numel(page_events)
			page_events(k).time = max(page_events(k).time, 0);
		end

	end

	% NOTE: when stop is session boundary don't allow time to exceed page duration

	if page.interval > 1

		for k = 1:numel(page_events)
			page_events(k).time = min(page_events(k).time, page.duration);
		end

	end

	%--
	% add page offset
	%--

	% TODO: we have to map back to record time since the scan time is real time
    
    recording_page_start = map_time(context.sound, 'record', 'real', page.start);
    
	for k = 1:numel(page_events)
		page_events(k).time = page_events(k).time + recording_page_start;
	end

end

%----------------------------------
% UPDATE VIEW
%-----------------------------------

if viewing

	% NOTE: view considering whether the extension handled multiple channels

	if ~ext.multichannel

		for k = 1:M

			view = contexts{k}.view;
			
			view.handles{k} = [];

			% TODO: the API calls should be done by 'update_widgets'
			
			try
				
				view.handles{k} = ...
					ext.fun.view.on.compute(view.figure, view.data, view.parameter, contexts{k});
				
			catch
				
				extension_warning(ext, 'Explain widget page update failed.', lasterror);
			
			end

		end

	else

		view = context.view;

		view.handles = [];

		try
			view.handles = ...
				ext.fun.view.on.compute(view.figure, view.data, view.parameter, context);
		catch
			extension_warning(ext, 'Multi-channel view failed.', lasterror);
		end

	end

	% NOTE: waitbar update calls drawnow

% 	if ~waiting
% 		drawnow;
% 	end
	
end

%----------------------------
% UPDATE WAITBAR (IF NEEDED)
%----------------------------

if waiting

	pal = detector_waitbar(context);
	
	waitbar_update(pal, 'PROGRESS', ...
		'value', context.scan.position, ...
		'message', page_time_string(page, context.sound, context.grid) ...
	);

	str = get_control(pal, 'events', 'string');
	
	han = get_control(pal, 'events', 'handles');
	
	if isempty(han)
		return;
	end
	
	new_str = [ ...
		page_time_string(page, context.sound, context.grid, 1), ...
		'  ', integer_unit_string(length(page_events), 'event') ...
	];

	if ~isempty(str)
		str{end + 1} = new_str;
	else
		str = {new_str};
	end

	str(1:end-30) = [];

	set(han.obj, 'string', str, 'value', numel(str));

end




