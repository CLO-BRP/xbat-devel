function str = page_time_string(page, sound, grid, interval)

% page_time_string - generate string to describe a scan page
% ----------------------------------------------------------
%
% str = page_time_string(page, sound, grid, interval)
%
% Input:
% ------
%  page - the page
%  sound - the sound (for time mapping)
%  grid - the grid, for string format
%  interval - 0: display only start time, 1: display interval
%
% Output:
% -------
%  str - string

if nargin < 4
	interval = 0;
end

if (nargin < 3) || isempty(grid)
	grid = [];
end

start_time = page.start; end_time = page.start + page.duration;

%--
% generate start time string using grid
%--

if isempty(grid)
	start_str = sec_to_clock(start_time);
else
	start_str = get_grid_time_string(grid, start_time, sound);
end

if ~interval
	str = start_str; return;
end

%--
% generate end time string
%--

if isempty(grid)
	end_str = sec_to_clock(end_time);
else
	end_str = get_grid_time_string(grid, end_time, sound);
end

%--
% concatenate strings together
%--

str = ['[' start_str, ' - ', end_str, ']']; 
