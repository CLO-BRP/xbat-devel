function name = get_extension_action_name(ext)

% TODO: this is wrong, this header should be produced by '$type_shared_controls

switch ext.subtype
	
	case {'image_filter', 'signal_filter'}, name = 'Filter';
		
	case {'sound_detector'}, name = 'Detect';
		
	case {'event_measure'}, name = 'Measure';
		
	case {'sound_feature'}, name = 'Feature';
		
	case 'sound_format', name = 'Sound';
		
	otherwise, name = title_caps(ext.subtype);
		
end
