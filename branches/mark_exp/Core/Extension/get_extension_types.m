function types = get_extension_types(refresh, field, value)

% get_extension_types - get names of extension types
% --------------------------------------------------
% 
% types = get_extension_types(refresh)
%
%       = get_extension_types(refresh, 'target', value)
%
%       = get_extension_types(refresh, 'class', value)
%
% Input:
% ------
%  refresh - force refresh flag
%
% Output:
% -------
%  types - names of extension types, possibly of given type or class
%
% NOTE:
% -----
%  Type refers to the object reference in extension type. Class to action.

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set no refresh default
%--

if ~nargin
	refresh = 0;
end

%----------------------------------
% GET EXTENSION TYPES
%----------------------------------

%--
% get extension type extensions
%--

if refresh
	type = 'extension_type!';
else
	type = 'extension_type';
end

types = get_extensions(type);

%--
% get extension type names from extensions
%--

% NOTE: this is largely for compatibility with earlier type system

if isempty(types)
	types = {}; return;
end

persistent EXT_TYPES;

if isempty(EXT_TYPES) || refresh
	EXT_TYPES = sort(lower(strrep({types.name}, ' ', '_')));
end

types = EXT_TYPES;

%----------------------------------
% SELECT TYPES
%----------------------------------

% NOTE: this selection is not available at the extensions level, why?

% NOTE: extension type strings consist of a target and class

if nargin > 1
	
	switch field
		
		%--
		% select based on class or target
		%--
		
		% TODO: consider developing class and target lists
		
		case {'class', 'target'}

			for k = length(types):-1:1
				
				if isempty(strfind(types{k}, value))
					types(k) = [];
				end
				
			end
			
		%--
		% unrecognized selection field
		%--
		
		otherwise, error('Unrecognized extension selection field.');

	end
	
end
	
