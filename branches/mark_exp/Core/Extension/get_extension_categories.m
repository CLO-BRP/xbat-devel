function category = get_extension_categories(ext, append)

% get_extension_categories - get category name and children list
% --------------------------------------------------------------
%
% category = get_extension_categories(ext, append)
%
%          = get_extension_categories(type, append)
%
% Input:
% ------
%  ext - extension array
%  type - extension type
%  append - append menu categories
%
% Output:
% -------
%  category - category name and children list

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set append default
%--

if nargin < 2
	append = 1;
end

%--
% get extensions
%--

if (nargin < 1) || isempty(ext)
	ext = get_extensions;
end

if ischar(ext)

	type = type_norm(ext);
	
	if ~is_extension_type(type)
		error('Unrecognized extension type.');
	end
	
	ext = get_extensions(type);
	
end
	
%--
% return empty if there are no extensions
%--

if isempty(ext) 
	category = []; return;
end

%--------------------------------------------
% GET CATEGORY NAMES
%--------------------------------------------

% NOTE: each extension may belong to and contribute zero or more categories
	
[name, other] = get_category_names(ext);

if append
	name = {'All', name{:}, 'Other'};
end

if isempty(name)
	category = []; return;
end

%-------------------------
% GET CATEGORY EXTENSIONS
%-------------------------
	
%--
% create all extension list
%--

[children{1}, pix] = sort({ext.name});

types{1} = {ext.subtype}; types{1} = types{1}(pix);

[children{1}, types{1}] = move_base(children{1}, types{1});
	
%--
% create actual category extension lists
%--

for k = 2:(length(name) - 1)

	ix = [];

	for j = 1:length(ext)
		
		if find(strcmp(name{k}, ext(j).category))
			ix(end + 1) = j;
		end
		
	end

	[children{k}, pix] = sort({ext(ix).name});
	
	types{k} = {ext(ix).subtype}; types{k} = types{k}(pix);
	
	[children{k}, types{k}] = move_base(children{k}, types{k});
	
end

%--
% create other extension list
%--

n = length(name);

if isempty(other)
	
	children{n} = cell(0); types{n} = cell(0);
	
else
	
	[children{n}, pix] = sort({other.name}); 
	
	types{n} = {other.subtype}; types{n} = types{n}(pix);
	
	[children{n}, types{n}] = move_base(children{n}, types{n});
	
end

%-------------------------
% PACK OUTPUT
%-------------------------
	
for k = 1:n
	
	category(k).name = name{k}; 
	
	category(k).children = children{k}; 
	
	category(k).types = types{k};
	
end
		

%----------------------------
% GET_CATEGORY_NAMES
%----------------------------

function [names, other] = get_category_names(ext)

%--
% initialize outputs
%--

names = {}; other = empty(extension_create('widget'));

%--
% get category names and extensions without categories
%--

for k = 1:length(ext)
	
	part = squeeze(unique(ext(k).category));
	
	if isempty(part)
		other(end + 1) = ext(k);
	else
		names = {names{:}, part{:}};
	end
	
end

names = unique(names);


%----------------------------
% SQUEEZE
%----------------------------

function value = squeeze(value)

value(cellfun('isempty', value)) = [];


%----------------------------
% MOVE_BASE
%----------------------------

function [children, types] = move_base(children, types)

%--
% get indices of names to move
%--

ix = [];

for j = 1:numel(children)
	if is_base_name(children{j}), ix(end + 1) = j; end
end

% NOTE: return if there is nothing to move

if isempty(ix)
	return;
end

%--
% move base extensions to the end of the list
%--

base = children(ix); 

children(ix) = [];  children = {children{:}, base{:}};

base = types(ix); 

types(ix) = [];  types = {types{:}, base{:}};




