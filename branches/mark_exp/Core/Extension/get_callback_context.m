function [control, pal, par] = get_callback_context(obj, mode)

% get_callback_context - get control callback context information 
% ---------------------------------------------------------------
%
% [control, pal, par] = get_callback_context(obj)
%
%            callback = get_callback_context(obj, 'pack')
%
% Input:
% ------
%  obj - callback object
%
% Output:
% -------
%  control - name and object handles associated to control
%  pal - control parent name and handle
%  par - palette parent tag and handle
%  callback - all outputs packed into a structure

% TODO: update this to consider the eventdata input available as part of the callback

% NOTE: this function is used as part of the extension callback framework

% NOTE: the outputs go from proximate to distal

%----------------------------------------
% HANDLE INPUT
%----------------------------------------

%--
% set default mode to empty (no packing)
%--

if nargin < 2
	mode = '';
end

%----------------------------------------
% GET CONTEXT
%----------------------------------------

%--
% get control parent handle and name
%--

pal.handle = ancestor(obj, 'figure');

% HACK: this happens for java handles, the fallback is not perfect

if isempty(pal.handle)
	pal.handle = gcf;
end

pal.name = get(pal.handle, 'name');

pal.tag = get(pal.handle, 'tag');

%--
% get control name, kind, and handles
%--

% TODO: extend to properly handle tab, separator, and menu controls

control.handle = obj; 

% HACK: this fails for java components, we are making up the type

try
	type = get(obj, 'type');
catch
	type = 'javacomponent';
end

switch type
	
	% NOTE: in the case of 'tabs' controls the tagging is different, this accounts for that 
	
	case 'rectangle'
		
		control.name = get(ancestor(obj, 'axes'), 'tag'); control.kind = 'tabs';
		
	case {'uimenu', 'uicontextmenu'}
		
		% NOTE: how to do this depends on developing some menu creation conventions
		
		control.name = get(obj, 'tag'); control.label = get(obj, 'label'); control.kind = 'menu';
		
	case 'javacomponent'
		
		% NOTE: these objects don't have a 'tag' property
		
		control.name = get(obj, 'name'); control.kind = 'java';
		
	otherwise

		control.name = get(obj, 'tag'); control.kind = 'control';
		
end

control.handles = findobj(pal.handle, 'tag', control.name);

%--
% get parent information if needed
%--

% NOTE: some might consider this information to be too intimate

flag = strcmp(mode, 'pack');

if (nargout > 2) || flag
	
	%--
	% get parent handle if possible
	%--
	
	try
		par.handle = get_field(get(pal.handle, 'userdata'), 'parent');
	catch
		par.handle = 0;
	end
	
	if isempty(par.handle)
		par.handle = 0;
	end
	
	%--
	% get parent tag if possible
	%--
	
	% TODO: it should be that we can produce values for the root browser
	
	if isempty(par.handle) || ~par.handle
		par.name = ''; par.tag = '';
	else
		par.name  = get(par.handle, 'name'); par.tag = get(par.handle, 'tag');
	end
		
end

%--
% pack results if needed
%--

if flag
	
	%--
	% pack output into callback context
	%--
	
	callback.obj = obj; 
	
	% NOTE: getting the value is worth it, we are working with the human interface and it helps the developer
	
	switch type
		
		case 'uimenu'
			
			control.value = ternary(strcmp(get(obj, 'check'), 'off'), 0, 1);
			
		case 'rectangle'
			
			handle = findobj(control.handles, 'type', 'text', 'tag', get(obj, 'tag'));
			
			if ~isempty(handle)
				control.value = lower(get(handle, 'string'));
			else
				control.value = [];
			end
			
		otherwise
			
			control.value = get_control_value(pal.handle, control.name);
			
	end
			
	callback.control = control;
	
	callback.value = control.value;
	
	callback.pal = pal;
	
	callback.par = par;
	
	%--
	% output callback context
	%--
	
	control = callback;
	
end
