function control = get_extension_controls(ext, context)

% get_extension_controls - get controls for extension given context
% -----------------------------------------------------------------
%
% control = get_extension_controls(ext, context)
%
% Input:
% ------
%  ext - extension
%  context - context
%
% Output:
% -------
%  control - extension control array

%--
% get shared controls
%--

% NOTE: these are the extension type shared controls

shared = get_shared_extension_controls(ext, context); 

%--
% create panels for various types of controls 
%--

type = get_preset_types(ext); panel = {};

for j = 1:length(type)

	%--
	% check we own controls for type
	%--

	[owned, fun] = get_extension_controls_of_type(type{j}, ext, context);
	
	if isempty(owned)
		continue;
	end
	
	%--
	% set callbacks if needed
	%--
	
	% TODO: move callback setting to get_extension_controls_of_type'
	
	if ~isempty(fun.callback)
		
		for k = 1:length(owned)
			owned(k).callback = {@extension_control_callback_router, ext, type{j}, fun.callback};
		end

	end
	
	%--
	% build control panel for type
	%--
	
	header = control_create( ...
		'string', title_caps(type{j}), ...
		'style', 'separator', ...
		'type', 'header' ...
	);

	% TODO: adjust header spacing
	
	header = adjust_control_space(header, owned(1));
	
	panel{end + 1} = [header, owned(:)']; %#ok<AGROW>

end

%--
% concatenate shared controls and panels
%--

% HACK: disregard shared controls for 'log_format' extensions

if strcmp(ext.subtype, 'log_format')
	control = empty(control_create);
else
	control = shared(:);
end 

for k = 1:length(panel)
	control = [control; panel{k}(:)];
end

control = control';

