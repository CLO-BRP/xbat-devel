function [fun, par] = parent_fun(caller, level)

% parent_fun - get handle of equivalent parent function
% -----------------------------------------------------
%
% [fun, par] = parent_fun(caller, level)
%
% Input:
% ------
%  caller - full name of caller, full file name
%  level - generation, 1 parent, 2 grandparent, etc.
%
% Output:
% -------
%  fun - parent extension function handle
%  par - parent extension

%--
% set level
%--

if nargin < 2
	level = 1; 
end

%--
% get caller from input or stack
%--

if ~nargin
	
	stack = dbstack('-completenames');

	% NOTE: return empty when we cannot determine caller from stack, command-line call
	
	if length(stack) < 2
		fun = []; par = []; return;
	end

	caller = stack(2); caller = caller.file;
	
end

%--
% handle higher levels recursively
%--

if level > 1
	
	[fun, par] = parent_fun(caller, 1); 
	
	if ~isempty(fun)
		info = functions(fun); caller = info.file; [fun, par] = parent_fun(caller, level - 1);
	end
	
	return;
	
end

%--
% try to get parent function from cache
%--

persistent PARENT_REGISTRY;

if isempty(PARENT_REGISTRY), PARENT_REGISTRY = struct; end

key = get_caller_key(caller);

if isfield(PARENT_REGISTRY, key)
	
	fun = PARENT_REGISTRY.(key).fun; 
	
	if nargout > 1
		par = PARENT_REGISTRY.(key).par;
	end
	
	return;
	
end

%--
% get extension type, name, and method from caller location
%--

% TODO: consider updating code to use 'extensions_root' and possibly other extension helpers

part = path_to_cell(caller);

type = [part{end - 3}, ' ', part{end - 4}]; name = part{end - 2}; method = file_ext(part{end});

%--
% get extension and parent if possible
%--

ext = get_extensions(type, 'name', name);

% NOTE: return empty when extension has no parent extension

if isempty(ext.parent)
	fun = []; par = []; return;
end

par = get_extensions(ext.parent.type, 'name', ext.parent.name);

%--
% get parent method
%--

% TODO: flattening the whole structure is not needed, we only need to get one function

fun = flatten_struct(par.fun);

fun = fun.(method);

%--
% update persistent registry
%--

PARENT_REGISTRY.(key).fun = fun;

PARENT_REGISTRY.(key).par = par;


%---------------------
% GET_CALLER_KEY
%---------------------

function key = get_caller_key(caller, level)

if nargin < 2
	level = 1;
end

key = ['CALL_', int2str(level), '_', md5(caller)];


%---------------------
% PATH_TO_CELL
%---------------------

% TODO: rename this or the public version of this function

function out = path_to_cell(str, delimiter)

% path_to_cell - separate path parts into cell
% --------------------------------------------
%
% out = path_to_cell(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  out - cell array with path contents

%--
% handle file separator
%--

if nargin < 2 || isempty(delimiter)

	if strcmp(filesep, '\')
		delimiter = '\\';
	else
		delimiter = filesep;
	end

end

%--
% parse string into cell
%--

out = strread(str, '%s', 'delimiter', delimiter);


