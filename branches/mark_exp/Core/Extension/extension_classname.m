function name = extension_classname(ext, full)

% extension_classname - for use in Rails
% --------------------------------------
%
% name = extension_classname(ext, full)
%
% Input:
% ------
%  ext - extension
%  full - name indicator
%
% Output:
% -------
%  name - for class
%
% NOTE: this simply implements the camelize convention
%
% See also: extension_table_names, generate_barn_extension

%--
% handle input
%--

if nargin < 2
	full = false;
end 

if numel(ext) > 1
	name = iterate(mfilename, ext, full); return;
end

% NOTE: classname is the 'camel' version of the fieldname

name = camelize(extension_fieldname(ext, full));