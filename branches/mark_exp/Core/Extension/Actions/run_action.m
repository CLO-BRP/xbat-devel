function result = run_action(type, name, target)

% run_action - from command-line
% ------------------------------
% 
% result = run_action(type, name, target)
%
% Input:
% ------
%  type - of action
%  name - of action
%  target - array
%
% Output:
% -------
%  result - of action

% NOTE: this is a hack to investigate how we may call an action from the command-line

% NOTE: ultimately we want to be able to efficiently call actions programmatically

% TODO: add some displays about what is going on

%--
% try to get default target, return if we have no target
%--

if nargin < 3
	[target, par] = get_default_targets(type);
end

if isempty(target)
	return;
end

%--
% get action extension
%--

db_disp

[ext, index, context] = get_browser_extension([type, '_action'], par, name);

if isempty(ext)
	return;
end

%--
% pack ingredients and call 'action_dispatch'
%--

obj.par = par; obj.ext = ext; obj.context = context; obj.target = target;

result = action_dispatch(obj, [], type);

if ~nargout
	disp(' '); disp(result); disp(flatten(result.output));
end


%----------------------------
% GET_DEFAULT_TARGETS
%----------------------------

% TODO: reconsider the default target selection, this is the first iteration

function [target, par] = get_default_targets(type)

switch type
	
	case 'sound'
		
		% NOTE: try to get 'active' open sound, otherwise use selected sound
		
		target = get_active_sound;
		
		if isempty(target)
			target = get_selected_sound; par = 0; return;
		end
		
		par = get_active_browser;
		
		if isempty(par)
			par = 0;
		end
		
	case 'event'
		
		% NOTE: try to get current browser selection event
		
		par = get_active_browser; 
		
		if isempty(par)
			target = []; return;
		end

		target = get_browser_selection(par);
		
	case 'log'
		
		% NOTE: try to get current browser active log, otherwise use selected log
		
		par = get_active_browser;
		
		if ~isempty(par)
			target = get_active_log;
		else
			target = [];
		end
		
		if isempty(target)
			target = get_selected_log; par = 0;
		end
		
end