function result = has_browser_display(ext)

% has_browser_display - test for extensions
% -----------------------------------------
%
% result = has_browser_display(ext)
%
% Input:
% ------
%  ext - extension
% 
% Output:
% -------
%  result - of test

% NOTE: we can handle multiple extension input

if numel(ext) > 1
	result = iterate(mfilename, ext); return; 
end

result = isfield(ext.fun, 'browser_event_display') && ~isempty(ext.fun.browser_event_display);