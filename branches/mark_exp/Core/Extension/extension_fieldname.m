function name = extension_fieldname(ext, full)

%--
% handle input
%--

if nargin < 2
	full = 0;
end 

if isstruct(ext) && numel(ext) > 1
	name = iterate(mfilename, ext, full); return;
end

%--
% compute fieldname
%--

if isstruct(ext)
	name = ext.name;
elseif ischar(ext)
	name = ext;
else
	error('Input must be extension or extension name.');
end

% NOTE: we replace punctuation with underscore and make sure we have no underscore runs

name(~isstrprop(name, 'alphanum')) = '_';

while 1
	updated = strrep(name, '__', '_'); 
	
	if numel(updated) == numel(name)
		 break;
	end
	
	name = updated;
end

if isempty(name)
	return;
end

% NOTE: strip any trailing underscores, we currently have one example 'Pitch (Yin)'

while name(end) == '_', name(end) = []; end 

name = lower(name);

if isempty(name)
	return;
end

if full
	name = [ext.type, '_', name];
end
