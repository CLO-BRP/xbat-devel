function context = get_extension_context(ext, par, data)

% get_extension_context - get extension context
% ---------------------------------------------
%
% context = get_extension_context(ext, par, data)
%
% Input:
% ------
%  ext - extension to get context for
%  par - parent browser
%  data - browser state
%
% Output:
% -------
%  context - extension context

%---------------------
% HANDLE INPUT
%---------------------

%--
% get active browser if needed
%--

if nargin < 2
	par = get_active_browser;
end

%--
% get browser state if needed and possible
%--

if nargin < 3
	
	% NOTE: a non-empty and non-zero parent is a sound browser with state
	
	if ~isempty(par) && par
		data = get_browser(par);
	else
		data = [];
	end
	
end

%---------------------
% BUILD CONTEXT
%---------------------

%--
% create empty context
%--

context = context_create;

if ~nargin
	return;
end

%--
% set user and library depending if we have some parent
%--

% TODO: the 'get_browser_info' operation is slower (very) for the root browser, fix this

if ~isempty(par)
	
	info = get_browser_info(par); context.user = info.user; context.library = info.library;
	
end

%--
% set parent browser fields
%--

% NOTE: the second condition has not been tested

if (~isempty(par) && par) || (isempty(par) && isempty(ext) && ~isempty(data))
	
	% NOTE: it seems like we could get a relevant library and user from parent

	context.par = par;
	
	%--
	% sound
	%--
	
	context.sound = sound_update(data.browser.sound, data); 
	
	context.sound.rate = get_sound_rate(context.sound);

	%--
	% log
	%--
	
	context.log = get_active_log(par, data);
	
	%--
	% page
	%--
	
	context.page.start = data.browser.time;

	context.page.duration = data.browser.page.duration;

	context.page.channels = get_channels(data.browser.channels);
	
	context.page.freq = data.browser.page.freq;
	
	%--
	% miscellaneous
	%--
	
	% NOTE: these are often useful in extension displays

	context.display.grid = data.browser.grid;
	
	context.display.colormap = data.browser.colormap;
	
	context.play = data.browser.play;
	
end

%--
% set extension
%--

context.ext = ext; 


%--------------------------
% CONTEXT_CREATE
%--------------------------

function context = context_create

%--
% system related fields
%--

context.ext = [];

context.user = [];

context.library = [];

%--
% browser related fields
%--

context.par = [];

context.pal = [];

context.widget = [];

context.sound = [];

% NOTE: this is the active log

context.log = [];

context.page = struct;

context.display = struct;

context.play = struct;

%--
% miscellaneous and user fields
%--

context.debug = 0;

% NOTE: the following two fields are recommended places to keep state information

context.state = struct;

context.userdata = [];


