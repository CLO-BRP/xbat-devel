function context = get_channel_contexts(context, n)

% get_channel_contexts - replicate context for each channel
% ---------------------------------------------------------
%
% context = get_channel_contexts(context, n)
%
% Input:
% ------
%  context - context
%  n - number of context when channels are not available
%
% Output:
% -------
%  context - replicated context if possible

%--
% check if we need to split
%--

if length(context) > 1
	return;
end

%--
% check for page channels so we can split
%--

if ~isfield(context, 'page') || ~isfield(context.page, 'channels')

	if nargin < 3
		return;
	end

	channels = [];

else

	channels = context.page.channels; n = length(channels);

end

%--
% create a replicate context for each channel
%--

base = context;

% NOTE: this is the critical part of the context we update as we loop

for k = 1:n
	
	context(k) = base;
	
	if ~isempty(channels)
		context(k).page.channels = channels(k);
	end
	
end
