function [value, parameter] = get_event_measure_as_tables(event, field)

% get_event_measure_as_tables - for use in code that requires them
% ----------------------------------------------------------------
%
% [value, parameter] = get_event_measure_as_tables(event, field)
%
% Input:
% ------
%  event - containers
%  field - name of measure
%
% Output:
% -------
%  value - table
%  parameter - table
%
% NOTE: the value and parameter tables have an 'id' column to match the source event

% NOTE: this is the null output

value = empty(create_data_table);

%--
% handle input
%--

if nargin < 2
	field = collect_fieldnames({event.measure});
end
	
if isempty(field)
	return;
end

if iscell(field) 
	value = iteraten(mfilename, 2, event, field); return;
end

% NOTE: remove events that do not contain named measure from further consideration

for k = numel(event):-1:1
	if ~isfield(event(k).measure, field)
		event(k) = [];
	end
end

if isempty(event)
	return;
end

%--
% extract named measure as tables
%--

% NOTE: extract values table first

measure = [event.measure]; measure = [measure.(field)];

values = iterate(@flatten, [measure.value]);

% TODO: factor most of the data value packaging and reuse for parameter data

ext = get_extensions('event_measure', 'fieldname', field);

columns = fieldnames(values)'; types = cell(size(columns));

data = cell(numel(event), numel(columns));

for k = 1:numel(columns)
	data(:, k) = {values.(columns{k})};
	
	if iscellstr(data(:,k))
		types{k} = 'string';
	else
		types{k} = 'numeric';
	end
end

try
	% NOTE: we skip string cell arrays in our coercion attempt
	
	if ~iscellstr(data)
		data = cell2mat(data);
	end
catch
	% NOTE: the cell array contains non-numeric data, hence it cannot be coerced to matrix
end

value = create_data_table( ...
	'name', ext.name, ...
	'fieldname', field, ...
	'column.names', columns, ...
	'column.types', types, ...
	'id', [event.id]', ...
	'data', data ...
);



% NOTE: we skip the parameter extraction if not captured

if nargout < 2
	return;
end

% TODO: finish parameter extraction

% parameter = measure.parameter;



