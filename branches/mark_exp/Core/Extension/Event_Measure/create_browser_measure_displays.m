function handles = create_browser_measure_displays(par, event, exts, contexts)

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% get extensions and contexts if needed
%--

if nargin < 4
	
	[id, exts, data] = get_event_extension_data(event);
	
	parent = ancestor(par, 'figure'); data = get_browser(parent);
	
	for k = 1:numel(exts)
		[exts(k), ignore, contexts(k)] = get_browser_extension(exts(k).subtype, parent, exts(k).name, data);
	end
	
end
	
%----------------------------
% CREATE MENUS
%----------------------------

%--
% produce displays for the various measures if needed
%--

handles = cell(size(exts));

for k = 1:numel(exts)
	
	ext = exts(k); context = contexts(k);
	
	%--
	% check whether selection implements display and whether event has data to display
	%--
	
	% TODO: add this method to sound detectors ...
	
	if ~isfield(ext.fun, 'browser_event_display')
		continue;
	end
	
	fun = ext.fun.browser_event_display;
	
	if isempty(fun)
		continue;
	end 
	
	if ~isfield(event.measure, ext.fieldname)
		continue;
	end
	
	%--
	% collect data required for display
	%--
	
	try
		data.event = event; data.value = event.measure.(ext.fieldname).value;
	catch
		continue;
	end
	
	%--
	% try to produce display
	%--
	
	try
		handles{k} = fun(par, data, context);
	catch
		handles{k} = []; extension_warning(ext, 'Failed to produce browser event measure display.', lasterror);
	end

end

%--
% collect handles of created objects
%--

handles = [handles{:}];

% NOTE: measurement displays should not interfere with other interaction and they typically represent non-editable quantities

set(handles, 'hittest', 'off');
