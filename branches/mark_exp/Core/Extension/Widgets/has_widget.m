function value = has_widget(ext)

% NOTE: this does not currently check for an implementation, only for the potential

if numel(ext) > 1
	value = iterate(mfilename, ext); return; 
end

value = strcmp(ext.type, 'widget') || (isfield(ext.fun, 'view') && isfield(ext.fun.view, 'on'));