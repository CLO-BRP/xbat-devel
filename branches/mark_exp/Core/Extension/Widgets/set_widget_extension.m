function set_widget_extension(widget, ext, context)

% NOTE: widgets are storing extension state, why? fast response for some events, is this true?

data = get(widget, 'userdata');

data.opt.ext = ext;

% NOTE: it is not clear that we need or that we should use this context

if nargin > 2
	data.context = context;
end 

set(widget, 'userdata', data);
