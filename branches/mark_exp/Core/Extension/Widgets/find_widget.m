function [widget, tag] = find_widget(par, ext)

% find_widget - get widget figure handle
% --------------------------------------
%
% [widget, tag] = find_widget(par, ext)
%
% Input:
% ------
%  par - parent figure
%  ext - widget extension
%
% Output:
% -------
%  widget - widget figure handle
%  tag - widget tag

tag = build_widget_tag(par, ext);

widget = findobj(0, 'tag', tag, 'type', 'figure');