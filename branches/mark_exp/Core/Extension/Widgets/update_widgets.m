function [widgets, elapsed] = update_widgets(par, event, data, context, layout)

% update_widgets - update parent widgets on event
% -----------------------------------------------
%
% widgets = update_widgets(par, event, data, context, layout)
%
% Input:
% ------
%  par - widget parent
%  event - parent event
%  data - parent state
%  context - context
%  layout - request
%
% Output:
% -------
%  widgets - widgets updated

% TODO: currently we have a concept of widget standing in for a concept of listener

% NOTE: the update of measures for example should be implemented as a listener

% TODO: update the output of this function to be more informative

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set no layout default
%--

if nargin < 5
	layout = 0;
end

%--
% get parent event widgets
%--

widgets = get_widgets(par, event);
	
if isempty(widgets)
	return;
end


% db_disp(event); 
% 
% tags = get(widgets, 'tag'); if ~iscell(tags), tags = {tags}; end
% 
% iterate(@disp, tags); disp(' ');


%--
% get parent data if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%-----------------------
% UPDATE WIDGETS
%-----------------------

% HACK: we are trying to clear a persistent cache

if strcmp(event, 'play__start')
	clear sound_player;
end

%--
% get widget data from parent data
%--

data = get_widget_data(par, event, data);

%--
% get parent context
%--

% NOTE: this context contains non-extension-specific context fields

if (nargin < 4) || isempty(context)
	
	context = get_extension_context([], par, data);

	% TODO: do this efficiently, solve recursion in 'get_extension_context'
	
	% NOTE: consider writing a 'get_widget_context' function?
	
	types = {'signal_filter', 'image_filter'}; % types = get_extension_types;

	for k = 1:length(types)

		active = get_active_extension(types{k}, par, data);

		if ~isempty(active)
			context.active.(types{k}) = active;
		end

	end
	
end

%--
% update widgets
%--

% TODO: time, time budget, and trace

% NOTE: data depends on parent and event, it is the same for all widgets

elapsed = zeros(size(widgets));

for k = length(widgets):-1:1
	
	% TODO: check if we need to update a widget using an update rate concept
	
	%--
	% update widget and time
	%--
	
	result = widget_update(par, widgets(k), event, data, layout, context);
	
	set_widget_status(widgets(k), result);
	
	elapsed(k) = result.elapsed;
	
	% NOTE: the event or marker are updated when we read samples during the widget update
	
	% NOTE: this approach allows for simpler extension code and the reuse of read samples by all widgets that may need them
	
	if isfield(result, 'UPDATED_EVENT')
		data.selection.event = result.UPDATED_EVENT;
	end
	
	if isfield(result, 'UPDATED_MARKER');
		data.marker = result.UPDATED_MARKER;
	end
	
	% NOTE: remove widget from updated list if update fails
	
	if ~result.updated
		widgets(k) = []; elapsed(k) = [];
	end
	
end

