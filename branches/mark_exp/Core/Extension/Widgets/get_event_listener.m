function listener = get_event_listener(ext, event, fast)

% get_event_listener - handle from extension
% ------------------------------------------
%
% listener = get_event_listener(ext, event, fast)
%
% Input:
% ------
%  ext - containing widget
%  event - requested
%  fast - so ignore checks
%
% Output:
% -------
%  listener - handle
%  own - handle, not inherited

%--
% handle input
%--

if nargin < 3
	fast = false;
end 

if ~fast
	if ~is_widget_event(event)
		error(['Unrecognized widget event ''', event, '''.']);
	end
	
	switch ext.type
		case 'widget'
			failed = ~isfield(ext.fun, 'on');
			
		otherwise
			failed = ~isfield(ext.fun.view, 'on');
	end
	
	if failed
		error(['Extension ''', extension.type '::', extension.name, ''' does not respond to events.']);
	end
end

%--
% get listener
%--

% NOTE: the 'get_field' function is cryptic

switch ext.type
	case 'widget'
		listener = get_field(ext.fun.on, event);
		
	otherwise
		listener = get_field(ext.fun.view.on, event);
end



