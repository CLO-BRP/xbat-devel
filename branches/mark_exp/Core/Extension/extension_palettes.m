function pal = extension_palettes(par, name, type)

% extension_palettes - open an extension palette
% ----------------------------------------------
%
% pal = extension_palettes(par, name, type)
%
% Input:
% ------
%  par - parent browser
%  name - extension name
%  type - extension type
%
% Output:
% -------
%  pal - palette

%-------------------
% HANDLE INPUT
%-------------------

%--
% initialize output get types
%--

pal = []; types = get_extension_types;

%--
% try to get extension type from name
%--

if (nargin < 3) || isempty(type)

	%--
	% get all possible types
	%--
	
	type = {};

	for j = 1:length(types)

		if ~isempty(get_extensions(types{j}, 'name', name))
			type{end + 1} = types{j};
		end

	end

	if isempty(type)
		return;
	end
	
	% TODO: produce some kind of warning
	
	if length(type) > 1
		return;
	end 
	
	% NOTE: unpack type to string
	
	type = type{1};
	
end

% NOTE: return empty on failure to find matching extension

type = lower(type);

if ~ismember(type, types)
	error(['Unrecognized extension type ''', type, '''.']);
end

%--
% open extension palette
%--

switch type

	% TODO: annotations, measures

	case {'signal_filter', 'image_filter'}

		pal = browser_filter_menu(par, [name, ' ...'], type);

	case 'sound_detector'

		pal = browser_detect_menu(par, [name, ' ...']);
		
	case 'widget'
		
		pal = browser_window_menu(par, name);
		
	otherwise
		
		% NOTE: an accidental call here created 'sound_file_format' palettes, use them
		
		% TODO: extension dispatcher will output a struct, this requires we extract handle here
		
		try
			result = extension_dispatcher(type, name, 'open', par); pal = result.pal;
		catch
			pal = [];
		end

end
