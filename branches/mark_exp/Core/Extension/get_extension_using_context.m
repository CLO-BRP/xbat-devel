function [ext, context] = get_extension_using_context(type, name, context, pal)

% NOTE: in this function the context typically comes from another extension

%------------------------
% HANDLE INPUT
%------------------------

%--
% set no palette default
%--

if nargin < 4
	pal = [];
end

%------------------------
% GET EXTENSION
%------------------------

%--
% get extension from system
%--

if isempty(type)
	ext = name;
else
	ext = get_extensions(type, 'name', name);
end 

% NOTE: extensions do not typically use themselves through the context for parameter creation, still

context.ext = ext;

%--
% get fresh parameters using context
%--

% TODO: factor this code, it is used here and in 'get_browser_extension'

[value, types] = has_any_parameters(ext);

for k = 1:length(types)
	
	%--
	% get type, parameter function branch, and currently stored parameters
	%--
	
	type = types{k};

	if strcmp(type, 'compute')
		fun = ext.fun.parameter; parameter = ext.parameter;
	else
		fun = ext.fun.(type).parameter; parameter = ext.parameters.(type);
	end 
	
	% NOTE: in what follows we do what we need to get current parameters

	%--
	% we have non-trivial parameters and no palette to update them with, compile with fresh context
	%--
	
	if ~trivial(parameter) && isempty(pal)

		% NOTE: if we compile, we compile to ensure fresh context

		if ~isempty(fun.compile)

			try
				[parameter, context] = fun.compile(parameter, context);
			catch
				extension_warning(ext, 'Parameter compilation failed.', lasterror);
			end

		end

		% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
		if strcmp(type, 'compute')
			ext.parameter = parameter; ext.parameters.(type) = parameter;
		else
			ext.parameters.(type) = parameter;
		end

		continue;

	end

	%--
	% we have trivial parameters, create default parameters given context
	%--

	if trivial(parameter)

		try
			parameter = fun.create(context);
		catch
			extension_warning(ext, 'Parameter creation failed.', lasterror); continue;
		end
		
		% NOTE: trivial parameters need no update, we assume they need no compilation

		if trivial(parameter)
			continue;
		end

	end

	%--
	% update parameters from matching controls if we have a palette
	%--
	
	% NOTE: this may fail if we have name conflicts

	if ~isempty(pal)

		% NOTE: update must be configured to be shallow

		opt = struct_update; opt.flatten = 0;
		
		parameter = struct_update(parameter, ext.control, opt);

	end

	%--
	% compile current parameters if needed
	%--
	
	if ~isempty(fun.compile)

		try
			[parameter, context] = fun.compile(parameter, context);
		catch
			extension_warning(ext, 'Parameter compilation failed.', lasterror);
		end

	end
	
	%--
	% store current compiled parameters of type in extension
	%--
	
	% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
	if strcmp(type, 'compute')
		ext.parameter = parameter; ext.parameters.(type) = parameter;
	else
		ext.parameters.(type) = parameter;
	end

end

%--
% update context extension
%--

context.ext = ext;
