function info = parse_extension_tag(tag)

% parse_extension_tag - get info from extension palette tag
% ---------------------------------------------------------
%
% info = parse_extension_tag(tag)
%
% Input:
% ------
%  tag - tag
%
% Output:
% -------
%  info - extension info

%--
% handle multiple tags using iterator
%--

if iscell(tag)
	info = iterate(mfilename, tag); return;
end 

%--
% parse tag
%--

% NOTE: reconsider the name of the first field, the first field is used by 'get_xbat_figs'

info = parse_tag(tag, '::', {'figure', 'subtype', 'name', 'parent'}); 

% TODO: this should not be required, update 'get_extension_tag'

% NOTE: we should be able to discontinue the subtype field

info.subtype = lower(info.subtype);

info.type = info.subtype;

% TODO: we should be able to omit the field test

if isfield(info, 'parent')
	info.parent = sscanf(info.parent, '%d');
end

