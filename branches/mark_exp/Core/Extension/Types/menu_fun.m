function fun = menu_fun(in)

% menu_fun - menu functions structure
% -----------------------------------
%
% fun = menu_fun(in)
%
% Input:
% ------
%  in - name of controlled values
% 
% Output:
% -------
%  fun - function structure

if nargin < 1
	in = 'value';
end

% NOTE: the first cell contains output names, the second cell input names

fun.create = {{'handles'}, {'par', in, 'context'}};

fun.callback = {{'result'}, {'callback', 'context'}};
