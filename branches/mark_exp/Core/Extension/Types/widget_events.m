function events = widget_events

% NOTE: events correspond to observables (event generators) extensions implement listeners

% NOTE: the cursor is the widget cursor, it is intimate with widget

% NOTE: the marker is the parent 'cursor', only time is necessary

events = { ...
	'activate', ... 
	'deactivate', ...
	'compute', ...
	'cursor__create', ...
	'cursor__delete', ...
	'cursor__move__start', ...
	'cursor__move__update', ...
	'cursor__move__stop', ...
	'event__display', ...
	'log__open', ... 
	'log__update', ...
	'log__close', ...
	'marker__create', ...
	'marker__delete', ...
	'marker__move__start', ...
	'marker__move__update', ...
	'marker__move__stop', ...
	'page', ...
	'play__start', ...
	'play__update', ...
	'play__stop', ...
	'resize', ...
	'selection__create', ...
	'selection__delete', ...
	'selection__edit__start', ...
	'selection__edit__update', ...
	'selection__edit__stop', ...
	'slide__start', ...
	'slide__update', ...
	'slide__stop', ...
	'keypress', ...
	'timer' ...
};
