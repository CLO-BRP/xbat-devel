function handles = event_measure_menu(handle, event, par, data)

% event_measure_menu - event measure context menu
% -----------------------------------------------
% 
% handles = event_measure_menu(handle, event, par, data)
%
% Input:
% ------
%  handle - menu parent handle
%  event - event
%  par - browser
%  data - state
%
% Output:
% -------
%  handles - menu handles

% TODO: this menu should be rebuilt after event editing

%------------------
% HANDLE INPUT
%------------------

%--
% get browser
%--

% NOTE: in this case the handle will be tested by 'get_browser'

if nargin < 3
	par = ancestor(handle, 'figure');
end

%--
% get browser state 
%--

if nargin < 4
	data = get_browser(par);
end

%------------------
% SETUP
%------------------

%--
% get event measure extensions
%--

ext = get_extensions('event_measure');

%--
% get available and addable event measures
%--

measure = event.measure;

[available, ix] = sort({measure.name});

measure = measure(ix);

addable = setdiff({ext.name}, available);

%------------------
% CREATE MENUS
%------------------

%--
% AVAILABLE
%--

% NOTE: these are menus for existing event measures

handles = [];

for k = 1:length(available)
	
	%--
	% create top measure menu
	%--
	
	top = uimenu(handle, 'label', available{k});

	handles(end + 1) = top;
	
	if length(handles) > 1
		set(top, 'separator', 'on');
	end
	
	%--
	% create measure value menu
	%--
	
	ext = get_extensions('event_measure', 'name', available{k});
	
	% NOTE: if the extension is not available, this is the best we can do
	
	if isempty(ext)
		
		struct_menu(top, value);
		
		handles(end + 1) = uimenu(handle, ...
			'enable', 'off', ...
			'label', '(Measure Not Available)' ...
		);
		
		continue;
	end
	
	% NOTE: when the extension offers a method for building we try
	
	if isempty(ext.fun.value.menu)
		
		struct_menu(top, value);
		
	else
		
		[ext, ix, context] = get_browser_extension('event_measure', par, available{k}, data);
		
		try
			ext.fun.value.menu(top, measure(k).value, context);
		catch
			extension_warning(ext, 'Failed to create value menu.', lasterror); struct_menu(top, value);
		end
		
	end

	%--
	% create edit measure menu, this means edit the parameters
	%--
	
	% TODO: we cannot edit if there are no editable parameters, reflect this
	
	handles(end + 1) = uimenu(handle, ...
		'label', 'Edit ...' ...
	);

end

%--
% ADDABLE
%--

% NOTE: these are menus for event measures we may add to event

top = uimenu(handle, 'label', 'Add Measure');

handles(end + 1) = top;

if length(handles) > 1
	set(top, 'separator', 'on');
end

for k = 1:length(addable)
	
	handles(end + 1) = uimenu(top, ...
		'label', [addable{k}, '...'] ...
	);
	
end


%----------------------------------------------
% EVENT_MEASURE_MENU_CALLBACK
%----------------------------------------------

function event_measure_menu_callback(obj, eventdata, event)



