function data = update_extension_store(par, types, names, data)

% update_extension_store - update browser extension store
% -------------------------------------------------------
%
% data = update_extension_store(par, types, names, data)
%
% Input:
% ------
%  par - browser
%  types - to update
%  names - to update
%  data - browser state
%
% Output:
% -------
%  data - updated browser state

% NOTE: this leaves a browser with open or active extensions in an incongruous state

% TODO: this should call 'set_browser_extension' which solves the above problem

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set all types default
%--

if nargin < 2
	types = get_extension_types;
end 

if ischar(types)
	types = {types};
end

%--
% set all sound browsers default
%--

% NOTE: we have no parent input and no state data to update

if (nargin < 1) || (isempty(par) && (nargin < 4))
	
	par = get_xbat_figs('type', 'sound');
	
	if isempty(par)
		data = []; return;
	end
	
end

%--
% set all names default
%--

if nargin < 3
	names = {};
end

if ischar(names)
	names = {names};
end

if ~iscellstr(names) 
	error('Names of extensions to update must be a string or string cell array.');
end

%--
% handle multiple handles input
%--

% NOTE: we are not considering the possible input of data here

if (numel(par) > 1) && all(ishandle(par)) 
	
	for k = 1:length(par) 
		update_extension_store(par(k), types, names);
	end
	
	return;
	
end
	
%--
% handle different input to get browser state
%--

if nargin < 4
	data = get_browser(par);
end

%--
% get context
%--

context = get_extension_context([], par, data);

% NOTE: these used to be available but apparently not used by anyone

% context.user = get_active_user;
% 
% context.library = get_active_library;

%--
% update store for specific types
%--

for k = 1:numel(types)
	
	%--
	% normalize type
	%--
	
	type = type_norm(types{k});
	
	%--
	% get and initialize all extensions of type
	%--

	if isempty(names)
		
		%--
		% update extensions and store
		%--
		
		exts = get_extensions(type);

		if ~isempty(exts)
			exts = extension_initialize(exts, context);
		end
		
		% NOTE: this destroys state for all extensions of type

		data.browser.(type).ext = exts;
		
	%--
	% get and initialize only named extensions
	%--
	
	else
		
		for j = 1:length(names)
			
			ext = get_extensions(type, 'name', names{j});
			
			if isempty(ext)
				continue;
			end
			
			ext = extension_initialize(ext, context);
				
			% NOTE: this has problems when adding the first extension of a type
			
			current = {data.browser.(type).ext.name};
			
			ix = find(strcmp(ext.name, current));
			
			if ~isempty(ix)
				data.browser.(type).ext(ix) = ext;
			else
				data.browser.(type).ext(end + 1) = ext;
			end

		end
		
	end

	%--
	% clear active extension names for type
	%--
	
	data.browser.(type).active = '';
	
end

%--
% update browser state
%--

% NOTE: we only update when the state will otherwise be lost

if ~nargout
	set(par, 'userdata', data);
end
