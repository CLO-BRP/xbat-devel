function value = has_view(ext)

% has_view - check whether an extension has any view handlers
% -----------------------------------------------------------
%
% value = has_view(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - result of test

value = 0; 

%--
% check for view branch in functions structure
%--

if ~isfield(ext.fun, 'view')
	return;
end

%--
% check for any available view handlers
%--

view = flatten(ext.fun.view);

fields = fieldnames(view);

for k = 1:length(fields)
	
	if ~isempty(view.(fields{k}))
		value = 1; return;
	end
	
end 
