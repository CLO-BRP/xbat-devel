function pal = get_extension_palette(ext, par)

% get_extension_palette - get extension palette of parent
% -------------------------------------------------------
% 
% pal = get_extension_palette(ext, par)
%
% Input:
% ------
%  ext - extension
%  par - browser
%
% Output:
% -------
%  pal - palette handle
% 
% NOTE: this function finds orphaned palettes

%--
% get parent
%--

% NOTE: we try to get the parent from the context input

if (nargin < 2) || isempty(par)
	par = get_active_browser;
end

% NOTE: return if we have no parent or the 'root' parent

if isempty(par) || ~par
	pal = []; return;
end

%--
% try to find extension palette
%--

% TODO: consider if the pre-selection gains any speed, otherwise remove it

% TODO: we should be able to move this convention into 'get_xbat_figs'

% NOTE: the parent selection in 'get_xbat_figs' is slow, we have replaced it here with a tag that includes the parent

% pal = get_xbat_figs('type', 'palette', 'parent', par);

%--
% try to get cached handle using key
%--

tag = get_extension_tag(ext, par);

%-------

% NOTE: some such idea may be applied in the 'get_xbat_figs' function

% key = strrep(strrep(strrep(tag, ':', '_'), ' ', '_'), '-', '_');

key = strip_punctuation(tag);

persistent CACHE;

if isempty(CACHE)
	CACHE = struct;
end

if isfield(CACHE, key)	
	pal = CACHE.(key);

	if ~isempty(pal) && ishandle(pal) && strcmp(get(pal, 'tag'), tag) % NOTE: in this case we have found the handle we are looking for
		return;
	end
end

%-------

% pal = key_value(key);
% 
% if ~isempty(pal) && ishandle(pal)
% 	return;
% end

%--
% get handle and cache
%--

pal = get_xbat_figs('type', 'palette');

if isempty(pal)
	return;
end

pal = findobj(pal, 'tag', tag);

%-------

% key_value(key, pal);

%-------

CACHE.(key) = pal;