function save_extensions_cache

% NOTE: we get the persistent memory store and save it to a file

exts = extensions_cache; %#ok<NASGU>

save(extensions_cache_file, 'exts'); 