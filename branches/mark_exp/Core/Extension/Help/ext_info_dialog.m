function ext_info_dialog(ext)

control = empty(control_create);

control(end + 1) = control_create( ...
    'style', 'separator', ...
    'type', 'header', ...
    'string', title_caps(ext.name) ...
);

control(end + 1) = control_create( ...
    'name', 'info', ...
    'style', 'listbox', ...
    'lines', 4, ...
    'string', ext_info_str(ext), ...
    'value', [], ...
    'min', 0, ...
    'max', 5 ...
);

opt = dialog_group; opt.dialog_type = 'ok';

dialog_group(['Extension Information'], control, opt);


%------------------------------
% EXT_INFO_STR
%------------------------------

function str = ext_info_str(ext)

str = {[ext.name, ' (', title_caps(ext.subtype), ')']};

str{end + 1} = '--';

if ~isempty(ext.version)
    
    str{end + 1} = ['Version: ', ext.version];
    
end

if ~isempty(ext.author)

    str{end + 1} = ['Author: ', ext.author];
    
end

if ~isempty(ext.email)
    
    str{end + 1} = ['Email: ', ext.email];
    
end

if ~isempty(ext.url)
    
    str{end + 1} = ['Url: ', ext.url];
    
end

if ~isempty(ext.short_description)

    str{end + 1} = '--';
    
    str{end + 1} = ext.short_description;

end




