function tool = get_hg(local)

% get_hg - get mercurial tool, from network if needed
% ---------------------------------------------------
%
% tool = get_hg
%
% Output:
% -------
%  tool - hg tool

%--
% get tool in linux
%--

if ~ispc
	tool = get_tool('hg'); return;
end

%--
% get tool for PC
%--

% NOTE: if we set the local flag, we force the use of a local version of subversion

if ~nargin
	local = 0;
end 

% NOTE: we use the system version if available and no local request

if ~local

	tool = get_windows_hg;

	if ~isempty(tool)
		return;
	end
	
end

tool = get_tool('hg.exe'); 

% TODO: display linked message on how to get mercurial and where to install it

current = 'http://mercurial.berkwood.com/';



