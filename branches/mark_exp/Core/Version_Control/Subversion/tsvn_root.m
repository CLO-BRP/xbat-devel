function root = tsvn_root

% tsvn_root - get 'TortoiseSVN' install root
% ------------------------------------------
%
% root = tsvn_root
%
% Output:
% -------
%  root - 'TortoiseSVN' install path
%
% NOTE: edit this file if 'TortoiseSVN' is installed and this returns empty

%--
% set tentative TortoiseSVN install root
%--

% NOTE: this is the default location on english language windows systems

root = 'C:\Program Files\TortoiseSVN';

% root = '';

%--
% test that the directory exist, otherwise return empty
%--

if isempty(root) || ~exist_dir(root)
	root = '';
end