function str = tags_to_str(tags)

% tags_to_str - convert tags to string
% ------------------------------------
%
% str = tags_to_str(tags)
%
% Input:
% ------
%  tags - tags
%
% Output:
% -------
%  str - tag string

%--
% check input for tags
%--

if ~is_tags(tags)
	error('Input is not proper tags.');
end

%--
% return empty string for empty tags
%--

if isempty(tags)
	str = ''; return;
end

%--
% create string from cell if needed
%--

if ischar(tags)
	str = tags; return;
end 

str = tags{1};

for k = 2:length(tags)
	str = [str, ' ', tags{k}];
end
