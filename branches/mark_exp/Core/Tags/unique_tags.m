function tags = unique_tags(obj)

% unique_tags - get unique tags from tagged objects
% -------------------------------------------------
%
% tags = unique_tags(obj)
%
% Input:
% ------
%  obj - tagged objects
%
% Output:
% -------
%  tags - unique tags

%--
% get object tags
%--

part = get_tags(obj);

%--
% get unique tags
%--

tags = {};

for k = 1:length(part)
	tags = union(tags, part{k});
end