function [valid, tag] = valid_tag(tag, rep)

% valid_tag - check tag validity and enforce it
% ---------------------------------------------
%
% [valid, tag] = valid_tag(tag, rep)
%
% Input:
% ------
%  tag - tag
%  rep - space replacement (def: '-')
%
% Output:
% -------
%  valid - indicator
%  tag - valid tag

%--
% check for string input
%--

if ~ischar(tag)
	error('Tags are strings.');
end

%--
% check for initial validity, enforce validity if needed
%--

% TODO: consider other possible restrictions

valid = ~any(isspace(tag));

if ~valid
	
	% NOTE: we set 'hyphen' as default space replacement
	
	if nargin < 2
		rep = '-';
	end
	
	tag = strrep(strtrim(tag), ' ', rep);

end

