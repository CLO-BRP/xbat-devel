function tags = get_tags(varargin)

% get_tags - get tags from tagged objects
% ---------------------------------------
%
% tags = get_tags(obj)
%
% Input:
% ------
%  obj - tagged objects
%
% Output:
% -------
%  tags - object tags

tags = update_tags('get', varargin{:});
