function label = get_label(obj) 

% get_label - get tagged objects label
% ------------------------------------
%
% label = get_label(obj)
%
% Input:
% ------
%  obj - taggable object array
%
% Output:
% -------
%  label - object label array

%--
% handle input
%--

if ~is_taggable(obj)
	error('Input must be taggable.');
end

if numel(obj) > 1
	label = iterate(mfilename, obj); return;
end

%--
% compute label string from tags
%--

tags = get_tags(obj); 

if isempty(tags)
	label = ''; return;
end

if numel(tags) == 1
	label = tags{1}; 
else
	label = [sprintf('%s\n', tags{1:end - 1}), tags{end}];
end 
