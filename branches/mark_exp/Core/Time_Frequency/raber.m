function [X, E] = raber(X, alpha, start)

% raber - simple recursive-average background estimator
% -----------------------------------------------------
%
% [A, E] = raber(X, alpha, start)
%
% Input:
% ------
%  X - column sequence matrix
%  alpha - mixing constant
%  start - starting background estimate
%
% Output:
% -------
%  A - recursive-average background estimate
%  E - error estimate

%--
% set no initial state for background
%--

% NOTE: if there is no starting estimate we use the minimum estimate

% NOTE: previously we used the first column as estimate by leaving start empty, this was a problem

if nargin < 3
	start = 0.5 * (min(X, [], 2) + median(X, 2));
end

%--
% compute simple recursive-average background estimate
%--

% NOTE: the output variable naming tries to take advantage of in-place computation

if nargout < 2
	X = raber_simple(X, alpha, start);
else
	Y = raber_simple(X, alpha, start); E = sqrt(raber_simple((X - Y).^2, alpha)); X = Y;
end


%-----------------------
% RABER_SIMPLE
%-----------------------

function X = raber_simple(X, alpha, start)

if ~isempty(start)
	X(:, 1) = (1 - alpha) * start + alpha * X(:, 1);
end

for k = 2:size(X, 2)
	X(:, k) = (1 - alpha) * X(:, k - 1) + alpha * X(:, k);
end
