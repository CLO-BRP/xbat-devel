function overlap = get_window_overlap(window, offset)

overlap = [];

if nargin < 2
	offset = 8;
end

copies = floor(length(window) / offset);

offsets = offset * [-copies:-1, 1:copies];

%--
% compute and plot sum and distortion
%--

fig; ax = subplot(2, 1, 1); 

total = window; 

handle = plot(window); set(handle, 'linewidth', 2); 

xlim = [1, numel(window)]; set(ax, 'xlim', xlim); hold on; 

for k = 1:numel(offsets)
	
	current = shift_vector(window, offsets(k)); total = total + current;
	
	plot(current);
	
end

% NOTE: we compute the total mean for improved display

total = total ./ (numel(offsets) + 1);

handle = plot(total); set(handle, 'color', 'red', 'linewidth', 2);

center = mean(total); offset = 100 * (total - center) ./ center;

ax = subplot(2, 1, 2); plot(offset); set(ax, 'xlim', xlim, 'ylim', fast_min_max(offset));