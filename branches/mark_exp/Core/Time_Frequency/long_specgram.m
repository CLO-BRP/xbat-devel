function [B, time] = long_specgram(sound, t, dt, ch, opt, filt, block_size)

% long_specgram - compute long term spectrograms
% ----------------------------------------------
%
% B = long_specgram(sound, t, dt, ch, opt, filt, block_size)
%
% Input:
% ------
%  sound - sound
%  t - start time
%  dt - duration
%  ch - channels
%  opt - spectrogram options
%  active - active filters struct
%  block_size - samples to process per block
%
% Output:
% -------
%  B - spectrogram(s)

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default block size
%--

% NOTE: the block size is the number of double frames

if (nargin < 7) || isempty(block_size)

	bytes = 2^22; block_size = floor((bytes / 8) / length(ch));
	
end

%--
% set default empty filter state
%--

if nargin < 6
	filt = [];
else
	context = filt.signal_context;
end

%--------------------------------
% SETUP
%--------------------------------

%--
% get some important values
%--

nch = length(ch); rate = get_sound_rate(sound);

%--
% compute block duration and number
%--

block_duration = block_size / rate;

blocks = ceil(dt / block_duration);
		
%---------------------------
% COMPUTE VIEW
%---------------------------

%--
% create waitbar
%--

pal = long_wait(dt); 

waiting = ~isempty(pal);

origin = offset_date(sound.realtime, t);

%--
% get parent from slider to aid in interrupt
%--

if waiting
	par = get_palette_parent(pal);
else
	par = get_active_browser;
end

if ~isempty(par)
	slider = get_time_slider(par);
end

%--
% setup for time-lapse waveform
%--

% time_lapse.duration = 20;
% 
% if isempty(ch)
% 	m = sound.channels;
% else
% 	m = numel(ch);
% end
%  
% time_lapse.n = ceil((time_lapse.duration / dt) * rate * dt);
% 
% time_lapse.samples = zeros(2 * time_lapse.n, m);
% 
% time_lapse.ramp = time_lapse.samples;
% 
% time_lapse.rate = rate;

%--
% initialize output cells
%--

B = cell(1, nch);

for block = 1:blocks
	
	%-------------------------------------------
	% READ SOUND
	%-------------------------------------------

	%--
	% calculate duration
	%--
	
	start_time = t + ((block - 1) * block_duration);

	duration = min([ ...
		get_sound_duration(sound) - start_time, ...
		t + dt - start_time, ...
		dt, ...
		block_duration ...
	]);

	%--
	% calculate duration necessary to have correct number of spectrogram blocks
	%--
	
	change = specgram_duration(opt, rate, duration) - duration;
	
	duration = duration + change;
	
	% NOTE: this does not fix the offset time problem
	
% 	if block == 1 && change
% 		start_time = start_time - change; duration = duration + change;
% 	end
		
	%--
	% read samples
	%--

	X = sound_read(sound, 'time', start_time, duration, ch);
    
    if any(size(X)) == 0
        continue;
    end

	%-------------------------------------------
	% COMPUTE VIEW
	%-------------------------------------------

	% TODO: consider the fact that we may place view results in parent axes
	
	%--
	% filter signal
	%--

	if ~isempty(filt.signal_filter)		
		
		% NOTE: this provides display page page adaptation
		
		page.start = start_time; page.duration = duration; page.channels = ch;
		
		page.samples = X;
		
		context.page = page;	
		
		page.buffer = 0;
		
		[page, context] = filter_sound_page(page, context);
	
		if ~isempty(page.filtered)
			X = page.filtered;
		end
		
	end

	%--
	% compute summary waveform
	%--
	
% 	if (dt / time_lapse.duration) > 3
% 		
% 		% TODO: change the shape of the fading and normalize
% 		
% 		keep = min(size(X, 1), round(2 * rate));
% 		
% 		fade = round(0.25 * rate);  ramp = linspace(0, 1, fade)' * ones(1, m);
% 		
% 		start = ceil((block / blocks) * time_lapse.n); stop = start + keep - 1;
% 		
% 		time_lapse.samples(start:stop, :) = time_lapse.samples(start:stop, :) + ...
% 			[ramp .* X(1:fade, :); X(fade + 1:(keep - fade), :); flipud(ramp) .* X((keep - fade + 1):keep, :)];
% 		
% 		temp = ones(size(X));
% 		
% 		time_lapse.ramp(start:stop, :) = time_lapse.ramp(start:stop, :) + ...
% 			[ramp; temp(fade + 1:(keep - fade), :); flipud(ramp)];
% 		
% 	end
	
	%--
	% compute view
	%--

	% TODO: eventually render view here as well?
	
	% TODO: consider multiple channel case up front
    
	[C{block}, ignore, time{block}] = fast_specgram(X, rate, 'norm', opt);

	time{block} = time{block} + start_time;
	
	%--
	% check for interrupt from parent
	%--
	
	if ~isempty(par)
		
		current = get_time_slider(par);

		if ~isequal(slider.value, current.value)
			
			if waiting
				close(pal);
			end

			% NOTE: this is a special return that the caller understands
			
			B = []; return;
		
		end

	end
		
	%--------------------------------------
	% UPDATE WAITBAR
	%--------------------------------------
	
	if waiting
		
		%--
		% compute waitbar position
		%--

		value = block / blocks;

		%--
		% compute waitbar message based on time horizon
		%--

		% TODO: make this message depend on the time grid display setting

		if ~isempty(origin)

			horizon = offset_date(origin, value * dt); message = datestr(horizon);

		else

			horizon = t + (value * dt);

			if dt > 5
				horizon = round(horizon);
			end

			message = sec_to_clock(horizon);

		end

		%--
		% update waitbar, or break out of computation loop
		%--

		% HACK: this is to decide if the waitbar has been closed

		try
			waitbar_update(pal, 'PROGRESS', ...
				'message', message, 'value', value ...
			);
		catch
			block = block - 1; break;
		end
		
	end

end 

%--
% store time lapsed waveform
%--

% samples = time_lapse.samples;
% 
% % TODO: select only the play channels
% 
% if size(samples, 2) > 2
% 	samples = time_lapse.samples(:, 1:2);
% end
% 
% sound_file_write(fullfile(xbat_root, 'TLW.wav'), lut_range(samples(1:time_lapse.n, :), [0, 1]), rate); 

%--
% check for early exit due to closed waitbar, and pad with zeros
%--

blocks_remaining = blocks - block;

if blocks_remaining
	
	blocks = block;
	
	if block > 0
		C{block} = zero_specgram(opt, rate, blocks_remaining * block_duration, nch);
	end
	
end

%--
% remove an empty block at the end
%--

% NOTE: this is required when the last block does not have enough data

if ~numel(time{end})
	time(end) = []; C(end) = []; blocks = numel(C);
end

%--
% concatenate long view
%--

if nch < 2
	
	B = {[C{:}]};

else
	
	%--
	% create anonymous concatenate function
	%--
	
	% TODO: create a function that returns a function
	
	str = '@(C,k) [';
	
	for j = 1:blocks
		str = [str, 'C{', int2str(j), '}{k},'];
	end

	str(end) = []; str = [str, ']'];
	
	try
		cat_fun = eval(str);
	catch
		nice_catch(lasterror, 'WARNING: Problem concatenating long spectrogram.'); delete(pal); return;
	end
	
	%--
	% concatenate channels
	%--
	
	for k = 1:nch
		B{k} = cat_fun(C, k);
	end
	
end

%--
% delete waitbar if needed
%--

if ~isempty(pal) && ishandle(pal), delete(pal); end


%--------------------------------------------------
% LONG_WAIT
%--------------------------------------------------

function pal = long_wait(duration)

% long_wait - waitbar for long view display
% -----------------------------------------
%
% pal = long_wait(duration)
%
% Input:
% ------
%  duration - real duration
%
% Output:
% -------
%  pal - waitbar handle

%--
% check page duration to see if we need waitbar
%--

% NOTE: this is five minutes, the test should consider rate

if duration < 30
	pal = []; return;
end

% TODO: use the sound to inform a real time multiplier display

%--
% progress waitbar
%--

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Chunked Computation', ...
	'style', 'waitbar', ...
	'units', duration, ...
	'confirm', 1, ...
	'lines', 1.1, ...
	'space', 0.5 ...
);

%--
% ensure singleton waitbar
%--

name = ['Computing Spectrogram ...']; 

pal = find_waitbar(name);

if ~isempty(pal)
	delete(pal);
end

%--
% create waitbar
%--

par = get_active_browser;

if isempty(par)
	pal = waitbar_group(name, control); return;
end

%--
% create waitbar with parent
%--

opt = waitbar_group; opt.show_after = 1;

pal = waitbar_group(name, control, par, 'bottom right', opt);


%--------------------------------------------------
% SLIDER_INTERRUPT
%--------------------------------------------------

function value = slider_interrupt(pal)

% slider_interrupt - indicate parent slider interrupt for computation
% -------------------------------------------------------------------
%
% value = slider_interrupt(pal)
%
% Input:
% ------
%  pal - computation waitbar handle
%
% Output:
% -------
%  value - interrupt indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3084 $
% $Date: 2005-12-20 18:54:15 -0500 (Tue, 20 Dec 2005) $
%--------------------------------

% NOTE: default to no interrupt

value = 0;

%--
% waitbar with parent is required for interrupt
%--

if isempty(pal)
	return;
end

par = get_palette_parent(pal);

if isempty(par)
	return;
end

%--
% interrupt happens if slider has changed since waitbar was created
%--

slider = get_time_slider(par); 

pal = get(pal, 'userdata'); % NOTE: create function to get palette state in usable form

elapsed = etime(slider.modified, pal.created);

if elapsed > 0
	value = 1;
end
