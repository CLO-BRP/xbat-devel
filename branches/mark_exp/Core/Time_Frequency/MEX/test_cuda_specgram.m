function result = test_cuda_specgram(X, n, w, overlap, output)

% test_cuda_specgram - for correctness and speed
% ----------------------------------------------
% 
% test_cuda_specgram(X, n, w, overlap, comp_type)
%
% Input:
% ------
%  X - input (default rand(1024) )
%  n - size of FFT (default 256)
%  w - size of window (default 256)
%  overlap - as fraction of transform size (default: 0)
%  output - 0 'norm', 1 'power', or 2 'complex'

%--
% handle input
%--

if ~nargin
	X = sound_file_read(which('LarkSparrow.flac')); % NOTE: this is an XBAT sample sound
end

X = single(X);

if nargin < 2
	n = better_fft_sizes(128, 1024, [2, 3, 5, 7, 11]);
end 

if nargin < 3
	w = n;
end

if nargin < 4
	overlap = 0.5:0.05:0.975;
end

if nargin < 5
	output = 0;
end

count = 10;

%--
% run tests
%--

parameter = fast_specgram;

% NOTE: for each transform size and overlap pair we run the MEX and CUDA
% code a 'count' number of times and collect these

start = clock;

for i = 1:numel(n)
	
	disp(int2str(n(i)));
	
	for j = 1:numel(overlap)
		
		fprintf('%2.1f,', 100 * overlap(j));
		
		for k = 1:count

			% cuda_specgram_mex - CUDA-based spectrogram computation
			% ------------------------------------------------------
			%
			% Y = cuda_specgram_mex(X, n, h, o, output)
			%
			% Input:
			% ------
			%  X - input signal
			%  n - transform size
			%  h - sampled window of size n
			%  o - overlap as samples, should be less than n
			%  output - mode (integer flag to 'indicate 'norm' 0, 'power' 1, 'complex' 2)
			%
			% Output:
			% -------
			%  Y - spectrogram magnitude
			
			tic; h0 = cuda_specgram_mex(X, n(i), single(hann(w(i))), round(n(i) * overlap(j)), output); cuda_time(i,j,k) = toc;
		end
		
		parameter.fft = n(i); parameter.win_length = w(i) / n(i);
		
		parameter.win_type = 'Hann'; parameter.hop = 1 - overlap(j); % NOTE: these do not appear explicitly above
		
		for k = 1:count
			tic; h1 = fast_specgram(X, 1, 'norm', parameter); mex_time(i,j,k) = toc;
		end
		
		fprintf('%1.2f  ', mex_time(i,j,k) / cuda_time(i,j,k));
	end
	
	fprintf('\n\n');
end

elapsed = etime(clock, start)

%--
% report results
%--

% NOTE: output all collected performance data

result.cuda_time = cuda_time; result.mex_time = mex_time;

cuda_time = mean(cuda_time, 3); mex_time = mean(mex_time, 3);

speedup = mex_time ./ cuda_time;

% fig; pcolor(speedup); colorbar; title SPECGRAM

fig; handle = plot(n, speedup); title SPECGRAM

set(handle, 'linewidth', 2);

for k = 1:numel(handle)
	temp = text(n(end), speedup(end, k), num2str(overlap(k)));
	
	set(temp, 'color', get(handle(k), 'color'), 'fontweight', 'bold');
end

% fig; imagesc(log(h0 + 1)); title CUDA; colormap(jet);
% 
% fig; imagesc(log(h1 + 1)); title MEX; colormap(jet);
% 
% disp('   Mex Time  CUDA Time Speedup');
% disp([mexEnd', cudaEnd', mexEnd./cudaEnd']);
	
