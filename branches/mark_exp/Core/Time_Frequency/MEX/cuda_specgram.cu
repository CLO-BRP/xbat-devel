//------------------------------------
// Spectragram on overlapping windows.
//------------------------------------

#include <cuda.h> 
#include <cufft.h>
#include "cuda_specgram.h"

// NOTE: max threads is hardcoded to 512 for current generation of hardware
#define MAX_THREADS 512

//--------------------------------------------------------------
// Copy data into windows for transform in place (single thread)
//--------------------------------------------------------------

__global__ void windowKernel_serial(cufftComplex *d_fft, float *d_data, unsigned int numTransforms, unsigned int transformSize, unsigned int w, unsigned int o)
{
	unsigned int i, j;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < transformSize; j++)
		{
			d_fft[i*transformSize + j].x = d_data[i*(w-o) + j];
			d_fft[i*transformSize + j].y = 0.0;
		}
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//---------------------------------------------------------------

__global__ void windowKernel_single(cufftComplex *d_fft, float *d_data, unsigned int N, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//   and apply window function
//---------------------------------------------------------------

__global__ void windowKernelFunc_single(cufftComplex *d_fft, float *d_data, float *d_func, unsigned int N, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx] * d_func[inc];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
		inc += MAX_THREADS;
	}
}

//----------------------------------------------------
// Compute magnitude of complex values (single thread)
//----------------------------------------------------

__global__ void magnitudeKernel_serial(float *d_mag, cufftComplex *d_fft, unsigned int numTransforms, unsigned int transformSize)
{
	unsigned int i, j;
	unsigned int hop = (transformSize >> 1) + 1;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < hop; j++)
		{
			d_mag[i * hop + j] = sqrtf(d_fft[i * transformSize + j].x * d_fft[i * transformSize + j].x +
					                   d_fft[i * transformSize + j].y * d_fft[i * transformSize + j].y) / transformSize;
		}
	}
}

//-----------------------------------------------------
// Compute magnitude of complex values (multi threaded)
//-----------------------------------------------------

__global__ void magnitudeKernel_single(float *d_Mag, cufftComplex *d_FFT, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int midx = blockIdx.x * ((transformSize>>1) + 1) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = ((transformSize>>1) + 1 - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Mag[midx] = sqrtf(d_FFT[fidx].x * d_FFT[fidx].x + d_FFT[fidx].y * d_FFT[fidx].y) / (float) transformSize;
		midx += MAX_THREADS;
		fidx += MAX_THREADS;
	}
}

//-----------------------------------------------------
// Compute power of complex values (multi threaded)
//-----------------------------------------------------

__global__ void powerKernel_single(float *d_Pow, cufftComplex *d_FFT, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int pidx = blockIdx.x * ((transformSize>>1) + 1) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = ((transformSize>>1) + 1 - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Pow[pidx] = (d_FFT[fidx].x * d_FFT[fidx].x + d_FFT[fidx].y * d_FFT[fidx].y) / (float) transformSize;
		pidx += MAX_THREADS;
		fidx += MAX_THREADS;
	}
}

//-----------------------------------------------------
// Change interleave of complex values (multi threaded)
//-----------------------------------------------------

__global__ void complexKernel_single(float *d_Rea, float *d_Img, cufftComplex *d_FFT, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int idx = blockIdx.x * ((transformSize>>1) + 1) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = ((transformSize>>1) + 1 - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Rea[idx] = d_FFT[fidx].x;
		d_Img[idx] = d_FFT[fidx].y;
		idx += MAX_THREADS;
		fidx += MAX_THREADS;
	}
}

//---------------------------------------------
// Compute FFT of windowed data
//---------------------------------------------

extern "C" void cuda_specgram_single(float *d_real, float *d_imag, float *d_data, unsigned int N, float * d_func, unsigned int w, unsigned int o, unsigned int transformSize, int comp_type);

void cuda_specgram_single(float *d_real, float *d_imag, float *d_data, unsigned int N, float *d_func, unsigned int w, unsigned int o, unsigned int transformSize, int comp_type)
{
    cufftHandle fp;
	cufftComplex *d_fft;
	unsigned int numTransforms;
	
	// Number of windows on which we will do transforms
	numTransforms = (N - o) / (w - o) + 1;

	// Initialize FFT library
    cufftPlan1d(&fp, transformSize, CUFFT_C2C, numTransforms);
    
	// Allocate temporary result buffer
	cudaMalloc((void **) &d_fft, numTransforms * transformSize * sizeof(cufftComplex));
	
	// Copy data into fft buffer (parallel)
	if (d_func == NULL)
	{
		windowKernel_single<<<numTransforms, MAX_THREADS>>>(d_fft, d_data, N, w, o, transformSize);
	}
	else
	{
		windowKernelFunc_single<<<numTransforms, MAX_THREADS>>>(d_fft, d_data, d_func, N, w, o, transformSize);
	}

	// Copy data into fft buffer (serial)
	// windowKernel_serial<<<1, 1>>>(d_fft, d_data, numTransforms, transformSize, w, o);
	
	// Transform	
	cufftExecC2C(fp, d_fft, d_fft, CUFFT_FORWARD);
	
	// Calculate result
	if (comp_type == 0)
	{
		// Compute magnitude of complex result (serial)
		// magnitudeKernel_serial<<<1, 1>>>(d_mag, d_fft, numTransforms, transformSize);

		// Compute magnitude of complex result (parallel)
		magnitudeKernel_single<<<numTransforms, MAX_THREADS>>>(d_real, d_fft, w, o, transformSize);
	}
	else if (comp_type == 1)
	{
		// Compute power of complex result (parallel)
		powerKernel_single<<<numTransforms, MAX_THREADS>>>(d_real, d_fft, w, o, transformSize);
	}
	else /* if (comp_type == 2) */
	{
		// Transform interleave of complex result (parallel)
		complexKernel_single<<<numTransforms, MAX_THREADS>>>(d_real, d_imag, d_fft, w, o, transformSize);
	}
	
	// Free device memory
	cudaFree(d_fft);
	
	// CUFFT cleanup
    cufftDestroy(fp);
}

