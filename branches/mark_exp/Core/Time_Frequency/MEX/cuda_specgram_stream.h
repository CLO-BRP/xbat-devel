#define MAX_THREADS 512
#define NUM_STREAMS 8

extern "C" void cuda_specgram_stream_single(float *h_transforms, float *h_data, unsigned int N, unsigned int w, unsigned int o, unsigned int transformSize);