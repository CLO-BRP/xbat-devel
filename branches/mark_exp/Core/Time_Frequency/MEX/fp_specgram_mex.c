#include <mex.h>
#include <fpc.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	
    sample_t * in = NULL;
	sample_t * out = NULL;
	
	int j, nbins;
	
	int out_p = 0;
    
    int nfft, nlap, nbuf, n_slices;
    
    sample_t * out_r;
    
    nbuf = mxGetN(prhs[0])*mxGetM(prhs[0]);
    in = (sample_t *) mxGetData(prhs[0]);
    
    nfft = (int) mxGetScalar(prhs[1]);
    
    nlap = (int) mxGetScalar(prhs[2]);
    
    if (nrhs > 3)
        set_point_pos((char) mxGetScalar(prhs[3]));
	
	/*
	 * allocate FFT buffers
	 */
	
    nbins = (nfft / 2) + 1;
    
	out = specgram_alloc( &n_slices, nbuf, nfft, nlap );
	
	specgram(out, in, nbuf, nfft, nlap, HANNING);
    
    /*
     * create output array
     */
    
    plhs[0] = mxCreateNumericMatrix(nbins, n_slices, mxINT16_CLASS, mxREAL);
    
    out_r = mxGetData(plhs[0]);
    
    memcpy(out_r, out, nbins * n_slices * sizeof(sample_t));
	
    free(out);
    
}	
    