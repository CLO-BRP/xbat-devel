#include "math.h"

#include "mex.h"

#include "cuda_runtime.h"

#include <cufft.h>

#include "cuda_specgram_stream.h"

//--------------------------------------
// MEX FUNCTION
//--------------------------------------

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	// result
	unsigned int	numTransforms;
	float			*hTransforms;
	float			*dTransforms;

	// input
	unsigned int	numSamples;
    float			*hSamples;
	float			*dSamples;
	
	// window function
	float			*hFunction;
	float			*dFunction = NULL;
    
	// transform size
	unsigned int	transformSize;
	
	// window
	unsigned int	windowSize;
	unsigned int	blockSize, blockOverlap;
	
	//--
	// HANDLE INPUT
	//--
	
	if (nlhs < 1) {
        return;    
	}
	
	// Input signal

	hSamples = (float *) mxGetData(prhs[0]); 
	
	numSamples = mxGetM(prhs[0]) * mxGetN(prhs[0]);
	
	// Transform size
	
	if (nrhs > 1) {
		transformSize = (int) mxGetScalar(prhs[1]);
	}
	else {
		transformSize = 256;
	}
	
	// Window size and overlap

	if (nrhs > 2) {
		windowSize = (int) mxGetScalar(prhs[2]);
	}
	else {
		windowSize = 256;
	}
	blockSize = windowSize;

	blockOverlap = nrhs > 3 ? (int) mxGetScalar(prhs[3]) : 0; // NOTE: get block overlap or set no overlap default

	//--
	// HANDLE OUTPUT
	//--
	
	// Size and create Matlab output variable
	
	numTransforms =  (numSamples - blockSize) / (blockSize - blockOverlap) + 1;
		
	plhs[0] = mxCreateNumericMatrix(0, 0, mxSINGLE_CLASS, mxREAL);
    
    hTransforms = (float *) mxMalloc(numTransforms * (transformSize/2 + 1) * sizeof(float));
    
    mxSetM(plhs[0], transformSize/2 + 1); mxSetN(plhs[0], numTransforms);
    
    mxSetData(plhs[0], hTransforms);
	
	// Compute
	
	cuda_specgram_stream_single(hTransforms, hSamples, numSamples, blockSize, blockOverlap, transformSize);

}
