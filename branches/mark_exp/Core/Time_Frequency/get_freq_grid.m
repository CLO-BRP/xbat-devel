function freq = get_freq_grid(sound, parameter)

% get_freq_grid - frequency grid for spectrogram computation
% ----------------------------------------------------------
% 
% freq = get_freq_grid(sound, parameter)
%
% Input:
% ------
%  sound - considered
%  parameter - for spectrogram
%
% Output:
% -------
%  freq - grid

%--
% handle input
%--

if nargin < 2
    if ~isfield(sound, 'specgram') || trivial(sound.specgram)
        error('Unable to get spectrogram parameters from sound.');
    end
    
    parameter = sound.specgram;
end

freq = (0:floor(0.5 * parameter.fft))' * get_sound_rate(sound) / parameter.fft;

   