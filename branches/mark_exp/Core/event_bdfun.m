function event_bdfun(par, log, id, flag)

% event_bdfun - event display button down function
% ------------------------------------------------
%
% event_bdfun(par, m, ix, flag)
%
% Input:
% ------
%  par - figure handle
%  log - log or log name
%  id - event id
%  flag - update flag

%--------------------------------------------------
% HANDLE INPUT
%--------------------------------------------------

% DEBUG: trying to determine how frequently this function gets called and by whom

% caller = get_caller; db_disp(['''', caller.name, ''' is calling.']);

%--
% set default update
%--

if nargin < 4 || isempty(flag)
	flag = 1;
end

%--
% get figure handle and log and event indices
%--

if ~nargin

	%--
	% hold current figure handle
	%--
	
	par = get_active_browser;
	
	if ~is_log_browser(par)
		return;
	end
	
	%--
	% get log and event indices looking at the patch tag
	%--
	
	tag = get(gco, 'tag');
	
	[id, log] = parse_event_tag(tag);
    
    if isempty(id) || isempty(log)
        return;
    end
	
end

%--------------------------------------------------
% GET EVENT DATA AND UPDATE BROWSER SELECTION
%--------------------------------------------------

%--
% get figure userdata
%--

data = get_browser(par);

% TODO: is this the right place for this?

data = delete_selection(par, data);

%--
% get log
%--

if ischar(log)
    log = get_browser_log(par, log, data);
end

%--
% get cache event
%--

% HACK: this deals with cache schema transition

try
	fast = get_full_event(get_cache_events(log, id));
catch
	delete(log_cache_file(log)); fast = get_full_event(get_cache_events(log, id));
end

if isempty(fast)
    return;
end

%--
% get display axes and display selection
%--

% NOTE: this includes browser state and display elements

% TODO: there is a bug lurking here

if ~is_log_browser(par)
	
	ax = findobj(data.browser.axes, 'tag', num2str(fast.channel));
	
else
	
	TAGS = get(data.browser.axes, 'tag');
	
	tag = event_tag(fast, log);
	
	ax = data.browser.axes(strcmp(TAGS, tag));

end

% clear_selection_display(data.browser.selection);

delete(findobj(par, 'tag', 'SELECTION_HANDLES'));

g = selection_event_display(fast, ax, data, log, id);

set(g, 'tag', 'SELECTION_HANDLES');

%--
% get full event
%--

event = log_get_events(log, id);

%--------------------------------------------------
% UPDATE EVENT PALETTE
%--------------------------------------------------

%--
% check for open event palette
%--

pal = get_palette(par, 'Event', data);

%--
% update palette display if needed
%--

% NOTE: the number of arguments and flag help distinguish code and GUI calls

if ~isempty(pal) && (~nargin || flag) 
			
	%--
	% get event strings from palette
	%--
	
	handles = get_control(pal, 'event_display', 'handles');
	
	listbox = handles.uicontrol.listbox;
	
	str = get(listbox, 'string');
	
	%--
	% find event string among listbox strings
	%--
	
	% NOTE: we rely on strings containing log name and event id
	
	% NOTE: the first pattern is the log name the second the event id
	
	pat = ['"', log_name(log), ' #" "# ', int2str(event.id), ':"'];
	
	sel = filter_strings(str, pat, 'and');
	
	%--
	% get index of event string in string list if needed and update palette
	%--
	
	if ~isempty(sel)

		% TODO: consider how this may be part of control_update
		
		ix2 = strmatch(sel, str);

		set(listbox, ...
			'value', ix2, 'listboxtop', ix2 ...
		);		
    
        drawnow;
	
% 		set_control(pal, 'event_display', 'value', ix2);
	
		double_click('off');
		
		control_callback([], pal, 'event_display');
		
		double_click('on');
		
	end
	
end

%--
% turn on selection edit functions
%--

if isempty(data.browser.parent)
	
	%--
	% update menus
	%--
	
	set(get_menu(data.browser.sound_menu.play, 'Selection'), 'enable', 'on');
	
	tmp = data.browser.edit_menu.edit;
	
	set(get_menu(tmp,'Cut Selection'),'enable','on');
	
	set(get_menu(tmp,'Copy Selection'),'enable','on');
	
	set(get_menu(tmp,'Log Selection ...'),'enable','on');
	
	set(get_menu(tmp,'Delete Selection'),'enable','on');
	
	%--
	% update controls
	%--
	
	control_update(par,'Sound','Selection','__ENABLE__',data);
	
else
		
	tmp = data.browser.sound_menu.sound;
	
	set(get_menu(tmp,'Play Event'),'enable','on');
	
	set(get_menu(tmp,'Play Clip'),'enable','on');
	
	tmp = data.browser.edit_menu.edit;

	set(get_menu(tmp,'Delete Selection'),'enable','on');
	
end

%--
% compute active measures and update measure menus
%--

% TODO: try to develop the idea of a displaying and managing a commited and active version of a measurement

% TODO: the signature can be improved by packing the selection earlier, and improving the 'handle' packing as well

% TODO: this is where selection triggered display goes

event_patch = findobj(ax, 'type', 'patch', 'tag', event_tag(event, log));

if isempty(event_patch)
	top = [];
else
	come = get(event_patch, 'uicontextmenu'); top = get_menu(get(come, 'children'), 'Measurement');
end

[event, handles] = compute_active_event_measures(par, event, data, ax, top);

%--
% refresh figure
%--

refresh(par);

%--
% make displayed event current selection in sound
%--

% TODO: check that this solves the double annotation problems

% NOTE: consider whether this is the right behavior. it seems like 'id', measurements, and annotations should be cleared

% NOTE: it seems that the 'id' is being used somewhere to distinguish logged event selection from simple selections

event.measurement = empty(measurement_create);

event.annotation = empty(annotation_create);

event.time = map_time(data.browser.sound, 'slider', 'record', event.time);

data.browser.selection.event = event;

% NOTE: this is the field that should be used to distinguish logged event selections from simple selections

data.browser.selection.log = log;

data.browser.selection.handle = g;

data.browser.selection.handles = handles;

%--
% update userdata
%--

set(par, 'userdata', data);

%--
% update widgets
%--

% NOTE: the code above should be replaced by a 'set_browser_selection' call

update_widgets(par, 'selection__create');

%--------------------------------------------------
% UPDATE ZOOM DISPLAY
%--------------------------------------------------

if ~is_log_browser(par)
	
	opt = selection_display; opt.show = 1;

	selection_display(par, event, opt, data);
	
end

%--------------------------------------------------
% UPDATE VARIOUS PALETTES
%--------------------------------------------------

%--
% update selection palette controls
%--

set_selection_controls(par,event,'start',data);

%----------------------------
% BRING MARKER TO FRONT
%----------------------------

% NOTE: we want to do this as the marker does not affect the editability of the selections

marker = get_browser_marker(par);

if ~isempty(marker.handles)
	
	try %#ok<TRYNC>
		uistack(marker.handles, 'top');
	end

end


