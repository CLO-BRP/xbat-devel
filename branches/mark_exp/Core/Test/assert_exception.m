function [result, caught] = assert_throws_exception(varargin)

% assert_throws_exception - check whether call throws exception
% -------------------------------------------------------------
%
% [result, caught] = assert_throws_exception(fun, arg, ... )
%
% Input:
% ------
%  fun - function name or handle
%  arg - argument
%
% Output:
% -------
%  result - of test
%  caught - exception
%
% NOTE: the 'caught' output on failure to throw exception is a trivial struct

%--
% check if we throw exception
%--

try
	feval(varargin{:}); result = 0; caught = struct;
catch
	caught = lasterror; result = 1;
end

%--
% throw assertion error if we failed to throw exception
%--

if ~nargout && ~result
	error('Failed to throw exception.');
end