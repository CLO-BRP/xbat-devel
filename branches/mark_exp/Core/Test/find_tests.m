function files = find_tests(root, exclude)

% find_tests - contained in 'Tests' directories
% ---------------------------------------------
%
% files = find_tests(root, exclude)
%
% Input:
% ------
%  root - of scan for test files
%  exclude - functions that contain this pattern in their name
%
% Output:
% -------
%  files - found

if nargin < 2
	exclude = {'dialog'};
end

%--
% start scan where we are
%--

if ~nargin
	root = pwd;
end

%--
% scan for test files
%--

files = scan_dir(root, {@callback, exclude});

if ~nargout
	
	disp(' ') ; disp(root); disp(' '); 
	
	if isempty(files)
		disp('  No test files found.');
	else
		iterate(@disp, strcat({'  '}, files));
	end

	disp(' '); clear files;
	
end


%-----------------------------------
% CALLBACK
%-----------------------------------

function result = callback(root, exclude)

% NOTE: check we are in a 'Tests' directory and get files

if ~strcmp(root(end - 4:end), 'Tests')
    result = {};
else
	content = what(root); result = strcat(root, filesep, content.m);
end

if isempty(result)
	return; 
end

% NOTE: we heuristically filter functions we think are GUI functions

for k = numel(result):-1:1
	
	for j = 1:numel(exclude)
		
		if isempty(findstr(exclude{j}, result{k}))
			continue;
		else
			result(k) = []; break;
		end
		
	end

end


