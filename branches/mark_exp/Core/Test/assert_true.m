function result = assert_true(x)

% assert_true - determines if something is interpreted as true
% ------------------------------------------------------------
%
% result = assert_true(x)
%
% Input:
% ------
%  x - value
%
% Output:
% -------
%  result - of truth test
%
% NOTE: this test is weaker than 'assert_equal' to 1

if x
	result = 1;
else
	result = 0;
end

if ~nargout && ~result
	error('Value is not true.');
end 