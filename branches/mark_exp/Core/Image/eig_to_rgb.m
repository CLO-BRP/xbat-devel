function Y = eig_to_rgb(X,V,c) 

% eig_to_rgb - eigencolor to rgb conversion
% -----------------------------------------
%
%  Y = eig_to_rgb(X,V,c) 
%
% Input:
% ------
%  X - eigencolor image
%  V - eigencolor basis
%  c - mean color
%
% Output:
% ------
%  X - rgb image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:51-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	error('Input image is not a multi-channel image.');
end

%--
% make double image
%--

X = double(X);

%--
% convert to rgb
%--

Y = rgb_reshape(rgb_vec(X)*inv(V) + ones(m*n,1)*c,m,n);

%--
% enforce positivity
%--

Y(find(Y < 0)) = 0;

