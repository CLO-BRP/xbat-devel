function Y = image_flip_ver(X)

% image_flip_ver - flip an image along a vertical axis
% ----------------------------------------------------
%
% Y = image_flip_ver(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  Y - flipped image

%--
% scalar or color  image
%--

d = ndims(X);

switch (d)

	case (2)
	
%       Y = X;
% 		Y = fliplr(Y);
			
        Y = fliplr(X);
        
	case (3)
		
		Y = X;
		for k = 1:3
			Y(:,:,k) = fliplr(X(:,:,k));
		end
		
end
