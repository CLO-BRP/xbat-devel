function SE1 = se_loose(SE,d)

% se_loose - increase support of structuring element
% --------------------------------------------------
%
% SE1 = se_loose(SE,d)
%
% Input:
% ------
%  SE - structuring element
%  d - support increment
%
% Output:
% -------
%  SE1 - larger support structuring element 

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% set increment
%--

if (length(d) == 1)
	d = [d d];
end

%--
% get support of input SE
%--

[p,q] = se_supp(SE);

%--
% create SE with larger support
%--

Zr = zeros(d(1),2*q + 1 + 2*d(2));
Zc = zeros(2*p + 1,d(2));

SE1 = [Zr; Zc, se_mat(SE), Zc; Zr];
	
