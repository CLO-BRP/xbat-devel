function Y = rgb_to_opp(X)

% rgb_to_opp - rgb to opponent colors conversion
% ----------------------------------------------
%
% Y = rgb_to_opp(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - opponent colors image

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% make double image
%--

X = double(X);

%--
% rgb to opponent color transformation
%--

V = [ ...
	1/2, 1/4, 1/3; ...
	-1/2, 1/4, 1/3; ...
	0, -1/2, 1/3 ...
];

%--
% apply transformation
%--

Y = rgb_reshape(rgb_vec(X)*V,m,n);
