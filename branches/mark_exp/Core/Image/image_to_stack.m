function Y = image_to_stack(X)

% image_to_stack - put multi plane images in stack
% ------------------------------------------------
%
% Y = image_to_stack(X)
%
% Input:
% ------
%  X - multi plane image
%
% Output:
% -------
%  Y - image stack

%--
% get number of planes
%--

d = size(X,3);

%--
% put image planes in stack
%--

if (d > 1)
	for k = 1:d
		Y{k} = X(:,:,k);
	end
else
	Y{1} = X;
end
		
