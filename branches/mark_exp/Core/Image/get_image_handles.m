function [handles, tags] = get_image_handles(par)

% get_image_handles - get image handles from figure
% -------------------------------------------------
%
% [handles, tags] = get_image_handles(par)
%
% Input:
% ------
%  par - parent (def: gcf)
%
% Output:
% -------
%  handles - image handles, excluding colorbar images
%  tags - image tags

%--
% set figure
%--

if nargin < 1
	par = gcf;
end

%--
% get image handles
%--

handles = findobj(par, 'type', 'image');

%--
% remove colorbars if needed
%--

% NOTE: this is fragile bacause it depends on a tag which may change

ix = find(strcmp(get(handles, 'tag'), 'TMW_COLORBAR'));

if ~isempty(ix)
	handles(ix) = [];
end

%--
% get tags of remaining images
%--

if nargout > 1
	tags = get(handles, 'tag');
end
