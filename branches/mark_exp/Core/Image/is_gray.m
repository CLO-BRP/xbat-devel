function t = is_gray(X)

% is_gray - check for grayscale image
% -----------------------------------
%
% t = is_gray(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  t - grayscale indicator

%--
% check dimensions and positivity
%--

if ((ndims(X) == 2) & ~(any(X(:) < 0)))
	t = 1;
else
	t = 0;
end