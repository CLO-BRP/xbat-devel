function Y = lut_minus_log(X,Z)

% lut_minus_log - fast computation of negative logarithm
% ------------------------------------------------------
%
% Y = lut_minus_log(X,Z)
%
% Input:
% ------
%  X - input image
%  Z - mask image (def: [])
%
% Output:
% -------
%  Y - output image

%--
% set mask
%--

if (nargin < 2)
	Z = [];
end

%--
% get limits
%--

c = fast_min_max(X);

%--
% create and apply lut
%--

Y = lut_apply(X,-lut_fun('log',c,1024),c,Z);
