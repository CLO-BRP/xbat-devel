function SE1 = se_tight(SE)

% se_tight - make structuring element support minimal
% ---------------------------------------------------
%
% SE1 = se_tight(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  SE1 - minimal support structuring element

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% produce SE with minimal support
%--

if (se_rep(SE) == 'mat')
	SE1 = se_mat(se_vec(SE));
end
