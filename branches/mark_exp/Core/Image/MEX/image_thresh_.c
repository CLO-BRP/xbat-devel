//----------------------------------------------
// INCLUDE FILES
//----------------------------------------------
	
#include "mex.h"

//----------------------------------------------
// FUNCTIONS
//----------------------------------------------

// single_thresh - single threshold computation
// --------------------------------------------
// 
// Input:
// ------
//  *X - input image
//   N - number of elements in input image
//   T - threshold value
//
// Output:
// -------
//  *Y - thresholded image

void single_thresh (double *Y, double *X, int N, double T);
	
void single_thresh (double *Y, double *X, int N, double T)
{
	register int i;
		
	// NOTE: the output image is initialized to zeros
	
	for (i = 0; i < N; i++) {
		if (*(X + i) > T) {
			*(Y + i) = 1.0;
		}
	}	
}

// multi_thresh - multiple threshold computation
// ---------------------------------------------
// 
// Input:
// ------
//  *X - input image
//   N - number of elements in input image
//  *T - threshold array
//   P - number of thresholds
//
// Output:
// -------
//  *Y - multiple threshold image

void multi_thresh (double *Y, double *X, int N, double *T, int P);
	
void multi_thresh (double *Y, double *X, int N, double *T, int P)
{
	register int i, k = 0;
	
	register double value;
	
	//--
	// compute multiple threshold image, how many thresholds are satisfied
	//--
		
	// NOTE: code is correct when thresholds are increasingly ordered
	
	for (i = 0; i < N; i++)
	{	
		value = *(X + i); 
		
		// naive algorithm
		
		// k = 0; while ((value > *(T + k)) && (k < P)) k++;
		
		// correlated values algorithm
		
		// NOTE: this code may be slightly more efficient when values are correlated
				 
		if (value > *(T + k)) {
			while ((k < P) && (value > *(T + k))) k++;
			
		} else {
			while ((k > 0) && (value <= *(T + k - 1))) k--;
		}
		
		*(Y + i) = k;
	}
}

//----------------------------------------------
// MEX FUNCTION
//----------------------------------------------

// multi_thresh_ - multiple image thresholds computation
// -----------------------------------------------------
//
// Y = multi_thresh_(X,CT);
//
// Input:
// ------
//  X - input image
//  CT - candidate threshold values
//
// Output:
// -------
//  Y - multiply thresholded image

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
	
	register int m, n, N, P;
	
	register double *X, *CT, T, *Y;
	
	//-----------------------
	// INPUT
	//-----------------------

	//--
	// input image
	//--
	
	X = mxGetPr(prhs[0]);

	m = mxGetM(prhs[0]); n = mxGetN(prhs[0]);
	
	N = m * n;
	
	//--
	// candidate thresholds
	//--
	
	CT = mxGetPr(prhs[1]);
	
	P = mxGetM(prhs[1]) * mxGetN(prhs[1]);
	
	// get single threshold
	
	if (P == 1) {
		T = *CT;
	}
	
	//-----------------------
	// OUTPUT 
	//-----------------------

	//--
	// multiply thresholded image
	//--
	
	Y = mxGetPr(plhs[0] = mxCreateDoubleMatrix(m, n, mxREAL));
	
	//-----------------------
	// COMPUTATION
	//-----------------------
	
	if (P == 1) {
		single_thresh (Y, X, N, T);
	} else {
		multi_thresh (Y, X, N, CT, P);
	}

}
