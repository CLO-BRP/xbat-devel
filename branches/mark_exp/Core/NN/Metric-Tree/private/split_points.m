function [ix1, ix2, decision] = split_points(ix, p1, p2, data, type, spill)

%----------------
% HANDLE INPUT
%----------------

%--
% set default spill and split type
%--

if nargin < 6
	spill = 0;
end

if nargin < 5
	type = 'median';
end

%--
% get points to split and parents
%--

points = get_points(ix, data); n = size(points, 1);

p1 = get_points(p1, data); 

p2 = get_points(p2, data);

%--
% translate points, get parent path unit vector, and compute projection
%--

points = points - repmat(p1, n, 1);

[vector, separation] = normalize(p2 - p1);

value = points * vector(:);

%--
% split points according to split type and spill
%--

if ~spill

	switch type

		case 'median', threshold = median(value);

		case 'mid-point', threshold = 0.5 * separation; 

	end

	% NOTE: we have to dereference local indices to get global indices

	ix1 = value <= threshold;
	
	ix2 = ~ix1;

	ix1 = ix(ix1); ix2 = ix(ix2);
	
else
	
	% TODO: finish implementing 'spill'
	
	% NOTE: we interpret the spill parameter differently for 'median' and 'mid-point'
	
	switch type

		case 'median', threshold = median(value);

		case 'mid-point', threshold = 0.5 * separation; 

	end

end

%--
% pack decision if needed
%--

if nargout > 2
	decision.vector = vector; decision.separation = separation; decision.threshold = threshold;
end


%------------------------
% NORMALIZE
%------------------------

function [u, r] = normalize(x, p)

if nargin < 2
	p = 2;
end

r = norm(x, p); u = x ./ r;