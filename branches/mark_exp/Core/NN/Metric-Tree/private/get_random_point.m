function ix0 = get_random_point(ix)

% NOTE: the node points array contains point identifiers

ix0 = ix(ceil(rand(1) * numel(ix)));