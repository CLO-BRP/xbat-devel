function ax = plot_metric_tree(tree, par)

% plot_metric_tree - produce 2d plot of metric tree
% -------------------------------------------------
%
% par = plot_metric_tree(tree, par)
%
% Input:
% ------
%  tree - metric tree
%  par - parent figure
%
% Output:
% -------
%  par - parent figure

% TODO: extend to handle high-dimensional trees through projection

%--
% create or clear figure
%--

if nargin < 2
	par = fig;
else
	clf(par);
end

set(par, 'color', zeros(1, 3));

%--
% get actual points for display
%--

% NOTE: we need to do this for points and pivots

for k = 1:length(tree.node)

	node(k).points = tree.data(tree.node(k).points, :);

	if ~isempty(tree.node(k).pivot)
		node(k).pivot = tree.data(tree.node(k).pivot, :);
	else
		node(k).pivot = [];
	end
	
	% NOTE: in the current implementation we are storing a non-point center
	
	% NOTE: this is different from an 'anchor' point, which is probably better
	
	node(k).center = tree.node(k).center;

end

%--
% display tree structure
%--

for k = 1:length(tree.node)

	color = random_color;
	
	%--
	% display tree relations
	%--

	if ~isempty(tree.node(k).parent)

		[parent, ix] = get_node_by_id(tree, tree.node(k).parent);

		if ~isempty(parent) || ~isempty(parent.center)

			P = [node(ix).center; node(k).center];

			plot(P(:, 1), P(:, 2), ...
				'tag', get_type_tag('tree'), ...
				'linewidth', 1, ...
				'color', ones(1, 3) ...
			);

		end

	end

	hold on;

	%--
	% display pivots and centers
	%--

	if ~isempty(node(k).pivot)

		plot(node(k).pivot(:, 1), node(k).pivot(:, 2), 'o', ...
			'tag', get_type_tag('pivot'), ...
			'markersize', 16, ...
			'linewidth', 1, ...
			'markeredgecolor', ones(1, 3) ...
		);

	end
	
	if ~isempty(node(k).center)
	
		plot(node(k).center(:, 1), node(k).center(:, 2), 'o', ...
			'tag', get_type_tag('center'), ...
			'markersize', 8, ...
			'linewidth', 1, ...
			'markerfacecolor', [1 1 1], ...
			'markeredgecolor', 0 * [1 1 1] ...
		);

	end
	
	%--
	% display node points
	%--

	% NOTE: we only need to draw points for the leaves

	if node_has_children(tree.node(k))
		continue;
	end

	plot(node(k).points(:, 1), node(k).points(:, 2), 'o', ...
		'tag', get_type_tag('point'), ...
		'markersize', 8, ...
		'markeredgecolor', 0.25 * ones(1, 3), ...
		'markerfacecolor', color ...
	);

	plot(node(k).pivot(:, 1), node(k).pivot(:, 2), 'o', ...
		'tag', get_type_tag('pivot'), ...
		'markersize', 24, ...
		'linewidth', 1, ...
		'markeredgecolor', [1 1 0] ...
	);

	if ~isempty(node(k).center)

% 		plot(node(k).center(:, 1), node(k).center(:, 2), 'x', ...
% 			'tag', get_type_tag('center'), ...
% 			'markersize', 16, ...
% 			'linewidth', 1, ...
% 			'markeredgecolor', ones(1, 3) ...
% 		);
	
		plot(node(k).center(:, 1), node(k).center(:, 2), 'o', ...
			'tag', get_type_tag('center'), ...
			'markersize', 24, ...
			'linewidth', 1, ...
			'markeredgecolor', 0.75 * [1 1 1] ...
		);
	
		P = [node(k).center; node(k).pivot];

		plot(P(:, 1), P(:, 2), ...
			'tag', get_type_tag('pivot'), ...
			'linestyle', ':', ...
			'linewidth', 1, ...
			'color', ones(1, 3) ...
		);
	
		set(plot_circle(node(k).center, tree.node(k).radius), ...
			'tag', get_type_tag('circle'), ...
			'linestyle', ':', ...
			'color', 0.5 * ones(1, 3) ...
		);

	end

end

set(gca, ...
	'color', 'none', ...
	'xcolor', 'white', ...
	'ycolor', 'white', ...
	'units', 'normalized', ...
	'position', [0.1 0.1 0.8 0.8] ...
);

uistack(findobj(gca, 'tag', get_type_tag('tree')), 'top');

uistack(findobj(gca, 'tag', get_type_tag('center')), 'top');

axis equal;

zoom on;

tree_view_menu(par);

set(par, 'closerequestfcn', @close_request);


%--------------------
% CLOSE_REQUEST
%--------------------

function close_request(obj, eventdata)

% NOTE: this seems to solve the crash on close problem for these plots

clf(obj); closereq;


%--------------------
% TREE_VIEW_MENU
%--------------------

function tree_view_menu(par)

%--
% create top menu
%--

top = uimenu(par, 'label', 'Tree');

%--
% create option menus
%--

% types = {'median', 'mid-point'};
% 
% for k = 1:length(types)
% 	
% 	uimenu(top, ...
% 		'label', title_caps(types{k}), ...
% 		'callback', @menu_callback, ...
% 		'accelerator', int2str(k), ...
% 		'checked', 'on' ...
% 	);
% 
% end

uimenu(top, 'label', 'Axes', 'callback', @menu_callback, 'checked', 'on');

%--
% create toggle menus for various parts of the display
%--

types = get_display_types;

for k = 1:length(types)
	
	% NOTE: the numerical accelerator limits the display types to 9
	
	uimenu(top, ...
		'label', title_caps(types{k}), ...
		'callback', @menu_callback, ...
		'accelerator', int2str(k), ...
		'checked', 'on' ...
	);

end


%--------------------
% MENU_CALLBACK
%--------------------

function menu_callback(obj, eventdata)

%--
% get parent and type
%--

par = ancestor(obj, 'figure');

type = get(obj, 'label');

%--
% consider menu commmand type in callback
%--

switch lower(type)
	
	case get_display_types
		
		%--
		% get new state
		%--

		if strcmp('on', get(obj, 'checked'))
			state = 'off';
		else
			state = 'on';
		end

		%--
		% update visibility or relevant objects and toggle object state
		%--

		% TODO: factor visibility setting

		set(findobj(par, 'tag', get_type_tag(type)), 'visible', state);

		set(obj, 'checked', state);
		
	case 'axes'
		
		ax = findobj(par, 'type', 'axes');
		
		if isempty(ax)
			return;
		end
		
		visible = get(ax, 'visible');
		
		if strcmpi(visible, 'on');
			visible = 'off';
		else
			visible = 'on';
		end
		
		set(ax, 'visible', visible);
		
	otherwise
		
		% TODO: for now this will contain the split type, the callback recomputes and displays
		
end


%--------------------
% GET_DISPLAY_TYPES
%--------------------

function types = get_display_types

% NOTE: keep these sorted

types = {'center', 'circle', 'pivot', 'point', 'tree'};


%--------------------
% GET_TYPE_TAG
%--------------------

function tag = get_type_tag(type)

tag = ['tree_', lower(type), '_handles'];


%--------------------
% RANDOM_COLOR
%--------------------

function color = random_color(n, lambda)

% NOTE: this selects a random color from a colormap palette and possibly lightens it

if nargin < 2
	lambda = 0.25;
end

if ~nargin
	n = 128;
end

palette = jet(n); color = palette(ceil(n * rand), :);

if lambda
	color = (1 - lambda) * color + lambda * zeros(1, 3);
end


%--------------------
% PLOT_CIRCLE
%--------------------

function handle = plot_circle(center, radius, ax)

if nargin < 3
	ax = gca;
end

persistent CIRCLE_GRID %#ok<USENS>

if isempty(CIRCLE_GRID)
	CIRCLE_POINTS = exp(i * linspace(0, 2 * pi, 64));
end

if isempty(radius)
	handle = []; return;
end

points = radius * CIRCLE_POINTS + (center(1) + i * center(2));

handle = line( ...
	'parent', ax, 'xdata', real(points), 'ydata', imag(points) ...
);

