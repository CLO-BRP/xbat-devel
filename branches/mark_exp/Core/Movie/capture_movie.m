function pal = capture_movie

% TODO: develop to integrate in the widget 'File' menu

% TODO: add output types, add file output considering the play rate

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% check for existing tool
%--

pal = findobj(0, 'tag', 'PALETTE::CAPTURE_TOOL');

if ~isempty(pal)
	position_palette(pal); return;
end

%--------------------------------
% CREATE CONTROLS
%--------------------------------

control = empty(control_create);

%--
% output file and capture section
%--

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.75, ...
	'string', 'Movie' ...
);

control(end + 1) = control_create( ...
	'name', 'file', ... 
	'style', 'edit', ... 
	'type', 'filename', ...
	'string', 'movie.avi', ... 
	'space', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'CAPTURE', ... 
	'style', 'buttongroup', ... 
	'space', -(1 + 0.75 * 0.5), ...
	'width', 0.4, ...
	'lines', 1.75 ...
);

control(end + 1) = control_create( ...
	'name', 'view_on_completion', ... 
	'style', 'checkbox', ... 
	'width', 0.5, ...
	'align', 'right', ...
	'value', 1, ... 
	'space', 1.5 + 0.75 * 0.5 ...
);

%--
% options section
%--

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'space', 0.1, ...
	'string', 'Options' ...
);

tabs = {'Basic', 'Advanced'};

control(end + 1) = control_create( ...
	'style', 'tabs', ... 
	'tab', tabs ...
);

string = {'Page', 'Scrolling'};

control(end + 1) = control_create( ...
	'name', 'mode', ... 
	'style', 'popup', ... 
	'tab', tabs{1}, ...
	'string', string, ...
	'value', 2, ... 
	'space', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'duration', ... 
	'style', 'edit', ... 
	'type', 'time', ...
	'tab', tabs{1}, ...
	'string', '00:00:01', ... 
	'width', 0.5, ...
	'space', -2 ...
);

string = strcat({'0.25', '0.5', '1', '2', '4', '8'}, 'x');

control(end + 1) = control_create( ...
	'name', 'rate', ... 
	'style', 'popup', ... 
	'tab', tabs{1}, ...
	'width', 0.4, ...
	'align', 'right', ...
	'string', string, ...
	'value', 3, ... 
	'space', 1.5 ...
);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'tab', tabs{1}, ...
	'space', 0.75 ...
);
	
[string, value, state] = get_figure_content;

control(end + 1) = control_create( ...
	'name', 'figure', ... 
	'style', 'popup', ... 
	'tab', tabs{1}, ...
	'string', string, ... 
	'value', value, ...
	'space', 1, ...
	'initialstate', state ...
);

[string, value, state] = get_axes_content;

control(end + 1) = control_create( ...
	'name', 'axes', ... 
	'style', 'popup', ... 
	'tab', tabs{1}, ...
	'string', string, ... 
	'value', value, ...
	'space', 1.5, ...
	'initialstate', state ...
);

string = iterate(@int2str, [15, 20, 30, 60]); 

value = find(strcmp('30', string)); %#ok<EFIND>

if isempty(value)
	value = 1;
end

control(end + 1) = control_create( ...
	'name', 'frames_per_second', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'string', string, ...
	'space', 1.5, ...
	'value', value ...
);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'tab', tabs{2} ...
);

if ispc
	string = {'Indeo3', 'Indeo5', 'Cinepak', 'MSVC', 'RLE', 'None'}; value = 2; state = 1;
else 
	string = {'None'}; value = 1; state = 0;
end

control(end + 1) = control_create( ...
	'name', 'compression', ... 
	'style', 'popup', ... 
	'tab', tabs{2}, ...
	'string', string, ... 
	'value', value, ...
	'initialstate', ternary(state, '__ENABLE__', '__DISABLE__') ...
);

string = iterate(@int2str, [25, 50, 75:5:100]);

value = find(strcmp('75', string)); %#ok<EFIND>

if isempty(value)
	value = 1;
end

control(end + 1) = control_create( ...
	'name', 'quality', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'string', string, ...
	'value', value ...
);

%--------------------------------
% RENDER CONTROLS
%--------------------------------

%--
% create capture palette
%--

opt = control_group; opt.width = 13; opt.bottom = 0;

pal = control_group(0, @capture_callback, 'Capture', control, opt);

set(pal, 'tag', 'PALETTE::CAPTURE_TOOL');

position_palette(pal, 0, 'center');

%--
% start capture daemon and setup its demise
%--

% NOTE: this daemon should be paused during capture, and destroyed when the tool is closed

set(pal, 'CloseRequestFcn', @capture_closereq);

start(capture_daemon);


%-----------------------------------------------------
% CAPTURE_CALLBACK
%-----------------------------------------------------

function capture_callback(obj, eventdata) %#ok<INUSD>

%--
% compile callback context
%--

callback = get_callback_context(obj, 'pack'); pal = callback.pal;

%--
% dispatch callback
%--

switch callback.control.name
	
	%--
	% movie section
	%--
	
	% TODO: this should be validated in some way, we want a time in seconds
	
	case 'file'
		
		value = get_control(pal.handle, 'file', 'value');
		
		proper =  proper_filename(value);
		
		set_control(pal.handle, 'CAPTURE', 'enable', proper);
		
		if ~proper
			return;
		end
		
		try %#ok<TRYNC>

			if ~strcmpi(value(end - 3:end), '.avi')
				set_control(pal.handle, 'file', 'value', strcat(value, '.avi'));
			end

		end
		
	case 'CAPTURE'
		
		%--
		% get and 'parse' parameters
		%--

		% TODO: consider this type of code in view of API design, typically this is 'parameter compile'
		
		% NOTE: we will also update the control framework
		
		parameter = get_control_values(pal.handle);

		parameter = struct_simplify_values(parameter, {'figure', 'axes'});

		parameter.duration = clock_to_sec(parameter.duration);

		parameter.rate = eval(parameter.rate(1:end - 1));
		
		parameter.figure = parameter.figure{1}; parameter.axes = parameter.axes{1};
		
		%--
		% perform capture
		%--
		
		perform_capture(parameter);
		
	%--
	% options section
	%--
	
	case 'mode'
		
		value = get_control(pal.handle, 'mode', 'value');
		
		set_control(pal.handle, 'duration', 'enable', ~strcmpi(value, 'page'));
		
	case 'duration'
		
		value = get(callback.obj, 'string');
		
		if all(isstrprop(value, 'digit'))
			
			value = eval(value);
			
			if value >= 0
				set_control(pal.handle, 'duration', 'value', sec_to_clock(value));
			end		
			
		end
		
	case 'compression'
		
		value = get_control(pal.handle, 'compression', 'value');
		
		set_control(pal.handle, 'quality', 'enable', ~strcmpi(value, 'none'));

end


%-----------------------------------------------------
% REFRESH_SOURCE
%-----------------------------------------------------

function refresh_source(current)

%--
% check for capture tool
%--

pal = findobj(0, 'tag', 'PALETTE::CAPTURE_TOOL');

if isempty(pal)
	return;
end

%--
% set current figure
%--

if ~nargin
	current = gcf;
end 

%--
% update controls in palette
%--

name = 'figure'; [string, value, state] = get_figure_content(current);

set_control(pal, name, 'string', string);

set_control(pal, name, 'value', value);

set_control(pal, name, 'enable', strcmp(state, '__ENABLE__'));


name = 'axes'; [string, value, state] = get_axes_content(current);

set_control(pal, name, 'string', string);

set_control(pal, name, 'value', value);

set_control(pal, name, 'enable', strcmp(state, '__ENABLE__'));


%-----------------------------------------------------
% GET_FIGURE_CONTENT
%-----------------------------------------------------

function [string, value, state] = get_figure_content(current)

%--
% set current figure and initialize output
%--

if ~nargin
	current = gcf;
end

string = {'(No Figures Available)'}; value = 1; state = 0;

%--
% get list of capturable figures
%--

% TODO: factor figure selection

figs = setdiff(get_xbat_figs, get_xbat_figs('type', 'palette')); 

figs = figs(strcmpi('on', get(figs, 'visible')));

%--
% update state and value based on figures
%--

state = ~isempty(figs);

if state

	string = get(figs, 'name'); 
	
	if ischar(string)
		string = {string};
	end
	
	value = find(figs == current); 
	
	if isempty(value)
		value = 1;
	end
	
end

state = ternary(state, '__ENABLE__', '__DISABLE__');


%-----------------------------------------------------
% GET_AXES_CONTENT
%-----------------------------------------------------

function [string, value, state] = get_axes_content(par)

%--
% set current figure as parent and initialize output
%--

if ~nargin
	par = gcf;
end

string = {'(No Axes Available)'}; value = 1; state = 0;

%--
% get capturable axes
%--

axs = findobj(par, 'type', 'axes', 'visible', 'on');

%--
% update state and value based on axes
%--

state = ~isempty(axs);

if state

	string = get(axs, 'tag'); 

	if ischar(string)
		string = {string};
	end
	
	ix = find(axs == get(par, 'CurrentAxes'));
	
	if ~isempty(ix)
		
		value = ix;
		
		% NOTE: this provides a visual cue to the current capture axes
		
		flash_update(axs(ix), {'xcolor', 'ycolor'}, {[1 0 0], [1 0 0]}, 0.4);
		
	end
	
end

state = ternary(state, '__ENABLE__', '__DISABLE__');


%-----------------------------------------------------
% CAPTURE_DAEMON 
%-----------------------------------------------------

function daemon = capture_daemon

%--
% configure timer
%--

name = 'XBAT Capture Daemon'; mode = 'fixedRate'; rate = 1; 

%--
% get singleton timer
%--

daemon = timerfind('name', name);

if ~isempty(daemon)
	return;
end

daemon = timer;

set(daemon, ...
	'name', name, ...
	'executionmode', mode, ...
	'busymode', 'drop', 'period', rate, ...
	'timerfcn', @capture_daemon_callback ...
);


%-----------------------------------------------------
% CAPTURE_DAEMON_CALLBACK
%-----------------------------------------------------

function capture_daemon_callback(obj, eventdata) %#ok<INUSD>

%--
% get current figure
%--

current = gcf; 

%--
% skip capture control and palettes
%--

[value, name, type] = is_palette(current);

if strcmp(get(current, 'tag'), 'PALETTE::CAPTURE_TOOL') || (value && ~strcmp(type, 'widget'))
	return;
end 

%--
% update capture source controls
%--

refresh_source(current);


%-----------------------------------------------------
% CAPTURE_CLOSEREQ
%-----------------------------------------------------

function capture_closereq(obj, eventdata) %#ok<INUSD>

daemon = capture_daemon; stop(daemon); delete(daemon);

closereq;


%-----------------------------------------------------
% PERFORM_CAPTURE
%-----------------------------------------------------

function avi = perform_capture(parameter)

%--------------
% SETUP
%--------------

% NOTE: we also parse the mode into a scrolling indicator for simplicity

scrolling = strcmpi(parameter.mode, 'scrolling');

%--
% get source figure and axes
%--

% NOTE: this only works when we are not showing others

source = findobj('type', 'figure', 'name', parameter.figure, 'visible', 'on');

% NOTE: in this case we are unable to determine the source figure

if numel(source) ~= 1
	return; 
end

ax = findobj(source, 'type', 'axes', 'tag', parameter.axes);

if isempty(ax)
	return;
end

%--
% get browser, sound, and time sequence
%--

par = get_active_browser; sound = get_active_sound(par); page = get_browser_page(par); marker = get_browser_marker(par);

start = page.start;

if scrolling
	stop = start + parameter.duration;
else
	stop = start + page.duration;
end

step = parameter.rate / parameter.frames_per_second;

time = start:step:stop;

if time(end) < stop
	time(end + 1) = time(end) + step;
end

%--
% initialize movie 
%--

file = fullfile(get_desktop, parameter.file);

avi = avifile(file, ...
	'fps', parameter.frames_per_second, 'compression', parameter.compression, 'quality', parameter.quality ...
);

%--
% step browser and capture
%--

try
	
	disp(' '); disp('starting capture');

	for k = 1:numel(time)

		if ~scrolling
			set_marker(par, marker.axes, time(k));
		else
			set_browser_time(par, time(k)); set_marker(par, marker.axes, time(k) + 0.5 * page.duration);
		end
		
		drawnow;
		
% 		frame = getframe(ax); avi = addframe(avi, frame);

		disp([int2str(100 * (k / numel(time))), '% done']);

	end

	disp('done'); disp(' ');

%--
% finalize movie
%--

% NOTE: we put this command both in the catch as well to avoid lingering open files

catch
	
	nice_catch; avi = close(avi); return
	
end

avi = close(avi);

%--
% create sound file
%--

% NOTE: this only works for single channel audio for the moment
	
file = file_ext(file, 'wav'); rate = get_sound_rate(sound);

sound_file_write(file, sound_read(sound, 'time', start, stop - start, 1), rate);
	