function offset = get_utc_offset

% get_utc_offset - from system
% -----------------------------
%
% offset = get_utc_offset
%
% Output:
% -------
%  offset - in hours

offset = get_utc_offset_mex; 