function [ax, h] = two_levels(n)

if ~nargin
	n = 12;
end

%--
% create layouts
%--
	
[n, R] = get_next_good_rect(n);

layout(1) = layout_create(R(1), R(2));

layout(2) = layout_create(2, 1); layout(2).margin = zeros(1, 4);

layout(2).row.frac = [0.75, 0.25];

%--
% create harray
%--

par = fig; [ax, h] = harray(par, layout); 

%--
% color and display level and position information
%--

% NOTE: the first axes are the base axes

for k = 2:length(ax)

	%--
	% set color
	%--
	
	set(ax(k), 'color', 1 - 0.75 * rand(1, 3));
	
	%--
	% display level and position indices
	%--
	
	% TODO: make this into a function
	
	i = 2 * h(k).level - 1; j = i + 1; i = h(k).index(i); j = h(k).index(j);
	
	set(get(ax(k), 'title'), ...
		'string',['L', int2str(h(k).level), ' (', int2str(i), ',', int2str(j), ')'] ...
	);

end

% NOTE: this makes the display clearer

set(ax, ...
	'box', 'on', 'xticklabel', [], 'yticklabel', [] ...
);

%--
% hide first level axes
%--

set(harray_select(par, 'level', 1), 'visible', 'off');

%--
% display tool and status bar
%--

harray_toolbar(par, 'on'); harray_statusbar(par, 'on');

