function [status, handle] = harray_status(par, status, varargin)

% harray_status - set and get status string
% -----------------------------------------
%
% [status, handle] = harray_status(par, status, varargin)
%
% Input:
% ------
%  par - handle
%  status - string to set
%  varargin - text properties to set
%
% Output:
% -------
%  status - string
%  handle - to text

%--
% get tagged handle
%--

switch get(par, 'type')

	% NOTE: we could should check whether this handle is properly tagged, but the goal here is speed
	
	case 'text', handle = par;
	
	otherwise, handle = findobj(par, 'tag', 'HARRAY_STATUS_LEFT');
		
end

% NOTE: the empty for missing handle it not a string

if isempty(handle)
	status = []; return;
end

%--
% get or set status
%--

if nargin < 2

	status = get(handle, 'string');

else

	if isempty(varargin)
		set(handle, 'string', status);
	else
		set(handle, 'string', status, varargin{:});
	end

end





