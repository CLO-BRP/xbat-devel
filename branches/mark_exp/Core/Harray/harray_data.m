function data = harray_data(par, data)

% harray_data - harray data get and set
% -------------------------------------
%
% data = harray_data(par, data)
%
% Input:
% ------
%  par - parent figure handle
% 
% Output:
% -------
%  data - harray data structure

%--
% set default figure
%--

if nargin < 1
	par = gcf;
end

%--
% try to find support axes
%--

ax = findobj(par, 'tag', 'HARRAY_BASE_AXES');

if isempty(ax)
	data = []; return;
end 

%--
% get data
%--

if nargin < 2
	data = get(ax, 'userdata'); return;
end

%--
% set data
%--
	
old_data = get(ax, 'userdata');

% NOTE: what may fail here is the 'resizefcn' evaluation

try
	set(ax, 'userdata', data); eval_callback(get(par, 'resizefcn'), par, []);
catch
	nice_catch(lasterror, 'Set failed, reverted to previous state.'); set(ax, 'userdata', old_data);
end


