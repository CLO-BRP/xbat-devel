function user = default_user

% default_user - defines and creates default user
% -----------------------------------------------
%
% user = default_user
%
% Output:
% -------
%  user - default user

%--
% create default user structure
%--

user = user_create( ...
	'id',0, ...
	'name','Default', ...
	'alias','Default' ...
);

%--
% add default user to file system
%--

user = add_user(user);