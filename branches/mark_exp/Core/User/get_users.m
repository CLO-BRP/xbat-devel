function users = get_users(varargin)

% get_users - get and select available users
% ------------------------------------------
%
% users = get_users
%       = get_users('field', value, ...)
%
% Input:
% ------
%  field - user field name
%  value - user field value
%
% Output:
% -------
%  users - selected available users

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2025 $
% $Date: 2005-10-27 16:35:03 -0400 (Thu, 27 Oct 2005) $
%--------------------------------

%---------------------------------------------------------------
% GET AVAILABLE USERS
%---------------------------------------------------------------

%--
% get user root directories
%--
 
roots = no_dot_dir(users_root, 1);

% NOTE: if we come up empty we make sure to have a default user

if isempty(roots)
	users = default_user; return;
end

%--
% load users from user files
%--

% NOTE: handle corrupt and missing user files with an exception

users = empty(user_create);

for k = 1:length(roots)

	try
		load([user_root(roots(k).name), filesep, roots(k).name, '.mat']); users(end + 1) = user;
	catch
		warning(['Failed to load user ''' roots(k).name ''' from user file.  ', lasterr]);
	end
	
end

%--
% return empty on no users
%--

% NOTE: should we try to create the default user here?

if isempty(users)
	users = []; return;
end

%--
% return if there are no selection criteria
%--

if isempty(varargin)
	
	if nargout < 1
		user_disp(users)
	end

	return;
	
end

%---------------------------------------------------------------
% SELECT FROM AVAILABLE USERS
%---------------------------------------------------------------

%--
% extract selection field value pairs
%--

[field, value] = get_field_value(varargin);

%--
% loop over selection fields
%--

for j = 1:length(field)
	
	%--
	% use field if it is in fact a library field
	%--
	
	if isfield(users(1), field{j})
		
		%--
		% check library fields for value match
		%--
		
		for k = length(users):-1:1
			if ~isequal(users(k).(field{j}), value{j})
				users(k) = [];
			end
		end
		
	end
end

if (nargout < 1)
	user_disp(users)
end


%---------------------------------------------------------------
% USER_DISP
%---------------------------------------------------------------

function user_disp(users)

% user_disp - user display function
% ---------------------------------

%--
% get active user
%--

active = get_active_user;

% TODO: improve active user display, and display overall

disp(' ')
disp('------------');
disp(' XBAT USERS ');
disp('------------');
disp(' ')

for k = 1:length(users)
	
	user = users(k); library = get_libraries(user);
	
	%--
	% display user info
	%--
	
	str = user.name;
	
	if isfield(user, 'email') && ~isempty(user.email)
		str = [str, ' (', user.email, ')'];
	end
	
	if strcmp(user.name, active.name)
		str = [str, ' **ACTIVE**'];
	end
	
	disp(str);
	str_line(length(str));

	%--
	% display user libraries info
	%--
	
	for j = 1:length(library)
		
		% TODO: library summary should be computed once for each unique library
		
		info = get_library_summary(library(j));
		
		if j > 1
			disp('  ');
		end
		
		disp(['  ', library(j).name, '(', int2str(info.sounds), ', ', sec_to_clock(info.total_duration) ')']);
		disp(['  ', strrep(library(j).path, xbat_root, '$XBAT_ROOT')]);
		
	end
	
	disp(' ');
	
end

disp(' ');
