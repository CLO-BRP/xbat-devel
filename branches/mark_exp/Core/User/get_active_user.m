function user = get_active_user(field)

% get_active_user - get currently active user
% -------------------------------------------
%
% user = get_active_user
%
% Output:
% -------
%  user - currently active user

%--
% get active user
%--

user = get_env('xbat_user');
	
%--
% consider no active user case
%--

if isempty(user)
	user = set_active_user_default;
end
	
%--
% check user still exists
%--

% NOTE: this is the built-in fast check for the existence of a directory

if ~exist(user_root(user), 'dir')

	%--
	% report and clear missing active user
	%--

% 	disp(['WARNING: User ''' user.name ''' no longer exists.']); 
	
	rm_env('xbat_user', 0);

	%--
	% get active user make sure we have one, this always ends	
	%--

	user = get_active_user;

end

%--
% select field if requested
%--

if nargin
	user = user.(field); 
end


%-----------------------------------
% SET_ACTIVE_USER_DEFAULT
%-----------------------------------

function user = set_active_user_default

%--
% check for available users
%--

users = get_users;

if isempty(users)

	%--
	% create default user and set to active user when there are no users
	%--

	user = default_user;

	if isempty(user)
		error('Failed to create default user.');
	end

else

	%--
	% set default user or first user
	%--

	user = get_users('name', 'Default');

	if isempty(user)
		user = users(1);
	end

end

%--
% set active user
%--

set_env('xbat_user', user(1));
