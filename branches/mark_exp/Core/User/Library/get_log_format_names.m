function [formats, ix] = get_log_format_names(user)

%--
% get active user
%--

if ~nargin
	user = get_active_user;
end

%--
% get log format names
%--

% TODO: implement extension disabling and disable formats not currently updated

formats = get_able_extensions(get_extensions('log_format'), 0); formats = sort({formats.name});

%--
% get default format index using user preferences if possible
%--

% NOTE: we could have problems here if the preference holds a bogus value

format = get_user_preference(user, 'new_log_format');

if isempty(format)
	format = 'BARN';
end

ix = find(strcmpi(formats, format));