function logs = get_logs(lib, sounds, names)

% get_logs - get logs from library
% --------------------------------
%
% logs = get_logs(lib, sounds, names)
%
% Input:
% ------
%  lib - parent library
%  sounds - parent sounds
%  names - log names
%
% Output:
% -------
%  logs - library logs

%-------------------
% HANDLE INPUT
%-------------------

%--
% default no name filter
%--

if nargin < 3
    names = {}; 
end

if ischar(names)
	names = {names};
end

%--
% set all sounds default
%--

if nargin < 2
    sounds = [];
end

%--
% get active library
%--

if nargin < 1 || isempty(lib)
    lib = get_active_library;
end

%--
% get sounds from library 
%--

if ischar(sounds) || iscellstr(sounds)
    
    sounds = get_library_sounds(lib, 'name', sounds);
    
    if isempty(sounds)
        logs = []; return;
    end 
    
end

if isempty(sounds)
    sounds = get_library_sounds(lib);
end

%-------------------
% GET LOGS
%-------------------

opt = struct_update; opt.flatten = 0;

%--
% get sound logs
%--

logs = empty(log_create);

for k = 1:length(sounds)

    %--
    % get logs root directory log content
    %--
	
	content = no_dot_dir(logs_root(sounds(k), lib)); content = {content.name};
    
	%--
	% load logs from log files
	%--
	
	for l = 1:length(content)
		
		%--
		% load log from file if possible
		%--
		
		if ~isempty(names) && ~ismember(content{l}, names)
			continue;
		end
		
		file = log_file(content{l}, sounds(k), lib);
		
        if ~exist(file, 'file')
			continue;
        end
		
        file_contents = load(file); log = file_contents.log;
        
        try     
            logs(end + 1) = struct_update(log_create, log, opt);          
        catch          
            continue;         
        end
        
		%--
		% update log fields to reflect current state
		%--
		
		% NOTE: when a log is in memory, its 'file' field indicates where it was loaded from
		
		logs(end).name = content{l};
		
		% TODO: finish update of stale directory information 
		
		logs(end).file = file;
		
		% NOTE: we also check store fields for stale directory information
		
		% NOTE: the following is designed considering the SQLite format, probably not general
		
		% TODO: the current API does not allow use of relative directory address, it should be possible
		
		previous_root = fileparts(log.file); current_root = fileparts(file);
		
		if ~strcmp(previous_root, current_root)
			
			field = fieldnames(logs(end).store);

			for m = 1:numel(field)

				current = logs(end).store.(field{m});

				if ~ischar(current)
					continue;
				end

				logs(end).store.(field{m}) = strrep(current, previous_root, current_root);

			end
			
		end
		
	end

end
