function [log, sound] = get_log_from_name(name, lib)

% get_log_from_name - get a log from a name and library
% -----------------------------------------------------
%
% [log, sound] = get_log_from_name(name, lib)
%
% Input:
% ------
%  name - full log name
%  lib - the library (def: gal)
% 
% Output:
% -------
%  log - the log
%  sound - the log's sound

%--
% handle input
%--

if nargin < 2
    lib = get_active_library;
end

%--
% get log
%--

info = parse_tag(name, filesep, {'sound', 'log'});

log = get_logs(lib, info.sound, info.log);

if isempty(log)
    return;
end

if numel(log) > 1
    log = log(1);
end

%--
% optionally get sound
%--

if nargout > 1
    
    if isempty(info.sound)
        info.sound = sound_name(log.sound);
    end

    sound = get_library_sounds(lib, 'name', info.sound);
    
end