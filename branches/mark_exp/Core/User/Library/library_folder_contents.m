function [sounds, logs] = library_folder_contents(lib_path)

%--
% recursive for multiple inputs
%--

if iscell(lib_path)
	
	sounds = {}; logs = {};
	
	for k = 1:numel(lib_path)
		
		[newsounds, newlogs] = library_folder_contents(lib_path{k});
		
		sounds = {sounds{:}, newsounds{:}}; logs = {logs{:}, newlogs{:}};
		
	end
	
	return;
	
end

%--
% get sound names
%--

sounds = get_folder_names(lib_path);

logs = {};

for k = 1:length(sounds)
	
	log_path = fullfile(lib_path, sounds{k}, 'Logs');
	
	contents = what_ext(log_path, 'mat');
	
	logs = {logs{:}, contents.mat{:}};
	
end
	

