function [lib, user] = get_active_library(user)

% get_active_library - get currently active library
% -------------------------------------------------
%
% lib = get_active_library
%
% Input:
% ------
%  user - user (def: active user)
%
% Output:
% -------
%  lib - active library

% TODO: this should be fast, caching libraries may be a reasonable approach

if ~nargin && nargout < 2
    
    lib = get_env('xbat_library');
    
    if ~isempty(lib)
        return;
    end
    
end

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% get active user as default
%--

if (nargin < 1) || isempty(user)
	user = get_active_user;
end

% NOTE: return empty when no user is available

if isempty(user)
	lib = []; return;
end

%--
% handle multiple users recursively
%--

if numel(user) > 1
	
	for k = 1:numel(user)
		lib(k) = get_active_library(user(k));
	end
	
	return;
	
end

%------------------------------
% GET ACTIVE LIBRARY
%------------------------------

%--
% get active library from user
%--

% NOTE: the 'active' user field contains the active library index


[libs, user] = get_libraries(user); active = user.active;

if active > numel(libs)
	active = 1;
end

lib = libs(active);

set_env('xbat_library', lib);

