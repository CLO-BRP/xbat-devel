function [lib, user] = get_library_from_name(name, user)

% get_library_from_name - get library from its full user/library name
% -------------------------------------------------------------------
%
% [lib, user] = get_library_from_name(name, user)
%
% Input:
% ------
%  name - full library name string
%  user - user in context
%
% Output:
% -------
%  lib - library
%  user - user

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1632 $
% $Date: 2005-08-23 13:09:06 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

if nargin < 2
	user = get_active_user;
end

%--
% handle multiple inputs recursively
%--

if iscell(name)
	
	% NOTE: consider output of multiple users? 
	
	lib = empty(library_create); users = empty(user_create);
	
	for k = 1:length(name)	
		[lib(end + 1), users(end + 1)] = get_library_from_name(name{k}, user);
	end
	
	user = users; return;

end
	
%--
% parse string
%--

info = parse_tag(name, filesep, {'author', 'libname'});

% NOTE: in this case the library name is simple, it does not contain the user

if isempty(info.libname)
	lib = get_libraries(user, 'name', info.author); return;	
end

%--
% get user and then library
%--

% NOTE: in this case user and library name are obtained from 'name' string input

user = get_users('name', info.author); 

lib = get_libraries(user, 'name', info.libname);

