function info = get_log_info(log)

% get_log_info - get normalized info for log
% ------------------------------------------
%
% info = get_log_info(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  info - normalized log info

%--
% get log format extension
%--

format = get_log_format(log);

%--
% get log info using format
%--

specific = struct;

if ~isempty(format.fun.log.info)
	
	try
		specific = format.fun.log.info(log);
	catch
		extension_warning(format, 'Failed to get log info.', lasterror);
	end
	
end

%--
% normalize info
%--

% TODO: create normalized info and update using format specific info

info = struct_update(normalized_info, specific);

info.connection = log.connection;

info.specific = specific;


%-----------------------
% NORMALIZED INFO
%-----------------------

function info = normalized_info

fields = {'events', 'connection', 'specific', 'author', 'created', 'modified'};

for k = 1:length(fields)
	info.(fields{k}) = [];
end