function [exists, value] = has_preference(varargin)

% has_preference - check whether a user has a given preference
% ------------------------------------------------------------
%
% [exists, value] = has_preference(user, field)
%
% Input:
% ------
%  user - user
%  field - preference name
%
% Output:
% -------
%  exists - indicator
%  value - preference value

[value, exists] = get_user_preference(varargin{:});
