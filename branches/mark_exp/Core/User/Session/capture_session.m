function content = capture_session

% capture_session - capture session contents
% ------------------------------------------
%
% content = capture_session
%
% Output:
% -------
%  content - session content

% NOTE: the tags contain user, library, and sound name information

figs = get_xbat_figs('type', 'sound');

if isempty(figs)
	content = []; return;
end

content = get(figs, 'tag');

if ischar(content)
	content = {content};
end 
