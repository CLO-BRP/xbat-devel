function par = open_session(varargin)

% open_session - open session browsers
% ------------------------------------
%
% par = open_session(name, user)
%
% Input:
% ------
%  session - session struct
%
% Output:
% -------
%  par - browser handles

% TODO: consider a different signature, perhaps same as load session. use varargin

% NOTE: this makes sure we verify working status at least once

clear xbat_version

%--
% load session
%--

% NOTE: this function handles the variable input

if isempty(varargin)
	session = load_session(varargin);
else
	session = load_session(varargin{:});
end

% NOTE: return if we can't find session

if isempty(session)
	par = []; return;
end

%--
% open session sounds
%--

par = [];

for k = 1:length(session.content)
	
	info = parse_browser_tag(session.content{k});
	
	lib = get_user_library(info.library);
	
	par(end + 1) = open_library_sound(info.sound, lib);
	
end

%--
% nice no output condition
%--

if ~nargout
	clear par;
end
