function [session, saved] = get_recent_session(user)

% get_recent_session - get users most recent session
% --------------------------------------------------
%
% [session, saved] = get_recent_session(user)
%
% Input:
% ------
%  user - user
%
% Output:
% -------
%  session - name
%  saved - date

%--
% set active user default
%--

if ~nargin
	user = get_active_user;
end

%--
% get most recent session
%--

sessions = get_all_sessions(user);

if isempty(sessions)
	session = []; return;
end

% TODO: consider allowing 'iterate' to take a cell array of function handles as input

info = iterate(@dir, iterate(@get_session_file, sessions));

[saved, ix] = max(datenum({info.date}')); session = sessions{ix};
