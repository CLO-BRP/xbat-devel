function value = get_volume

%--
% setup
%--

value = [];

tool = nircmd;

if isempty(tool)
	return;
end

%--
% get volume from system
%--
