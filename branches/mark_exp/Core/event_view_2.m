function event_view_2(par, events, log, light)

% event_view - display event boxes for browser
% --------------------------------------------
%
% event_view(par, events, log, light)
%
% Input:
% ------
%  par - the browser
%  events - events
%  log - the log containing the events
%  light - display ghost events if true 

%--------------------
% HANDLE INPUT
%--------------------

%--
% set light flag to zero for all event display requests
%--

if nargin < 4 || isempty(light)
	light = 0;
end

%--------------------
% SETUP
%--------------------

%--
% get browser state and get short names or compute used state elements
%--

data = get_browser(par);

sound = data.browser.sound;

ch = get_channels(data.browser.channels);

khz_flag = strcmp(data.browser.grid.freq.labels, 'kHz');

%--
% get widgets and generic context
%--

widget = get_widgets(par, 'event__display');

% HACK: we want to implement this more carefully 

for k = numel(widget):-1:1
	info = get_widget_info(widget(k));
	
	if ~strcmp(info.type, 'SOUND_FEATURE')
		widget(k) = [];
	end
end

context = get_extension_context([], par, data);

%--
% figure out if this is a special log
%--

active_flag = strcmp(log.format, 'active');

%--
% get fresh relevant event measures
%--

if ~light
	
	%--
	% get extensions that could potentially display
	%--
	
	exts1 = get_extensions('event_measure');
	
	for k = numel(exts1):-1:1
	
		if isempty(exts1(k).fun.browser_event_display)
			exts1(k) = [];
		end
		
	end
	
	%--
	% get current versions of these extensions
	%--
	
	for k = 1:numel(exts1)
		[exts1(k), ignore, contexts1(k)] = get_browser_extension(exts1(k).subtype, par, exts1(k).name, data);
	end

	% NOTE: remove the ones not producing overlay
	
	for k = numel(exts1):-1:1
		
		if ~isfield(exts1(k).control, 'overlay') || ~exts1(k).control.overlay
			exts1(k) = []; contexts1(k) = [];
		end
		
	end
	
end

%--------------------
% DISPLAY EVENTS
%--------------------

for k = 1:length(events)

	%--
	% get event
	%--

	event = events(k);
	
    tag = event_tag(event, log);

	%--
	% compute event coordinates in axes
	%--

	[x, y] = axes_coordinates(event, sound, khz_flag);

	%--
	% get display axes
	%--
	
	dax = data.browser.axes(ch == event.channel);

	if isempty(dax)
		continue;
	end
	
	%---------------------------------------------------
	% DISPLAY EVENT BOX
	%---------------------------------------------------

	%--
	% create event display objects
	%--

	style = log.linestyle; width = log.linewidth;

	%----------------------------
	% ACTIVE DETECTION DISPLAY
	%----------------------------
	
	if active_flag

		%----------------------------
		% SOUND BROWSER DISPLAY
		%----------------------------

		active_colors = 'ants';

		switch active_colors

			case ('candy') 	
				C1 = [1 0 0]; 
				C2 = [1 1 1];

			case ('fire')
				C1 = [1 0 0];
				C2 = [1 1 0];

			case ('ants')
				C1 = [0 0 0]; 
				C2 = [1 1 1];

			case ('blue-yellow')
				C1 = [1 1 0]; 
				C2 = [0 0 1];

			case ('red-green')
				C1 = [1 0 0]; 
				C2 = [0 1 0];

		end

		color = [0, 0, 0];
		
		width = data.browser.selection.linewidth;

		g(k,1) = line( ...
			'parent',dax, ...
			'xdata',x, ...
			'ydata',y, ...
			'linestyle','-', ...
			'linewidth',width, ...
			'color',C1 ...
		);

		g(k,2) = line( ...
			'parent',dax, ...
			'xdata',x, ...
			'ydata',y, ...
			'linestyle',':', ...
			'linewidth',width, ...
			'color',C2 ...
		);

		% display patch to enable selection

		g(k,3) = patch( ...
			'parent',dax, ...
			'xdata',x, ...
			'ydata',y, ...
			'EdgeColor','none', ...
			'FaceColor','none', ...
			'Erasemode','none' ...
		);

		set(g(k,:),'tag',tag);

	%----------------------------
	% LOG DISPLAY
	%----------------------------

	else

		color = log.color;
		
		if light
			color = (0.5 + 0.25 * color);
		end

		alpha = log.patch;

		if light
			hit_test = 'off';
		else
			hit_test = 'on';
		end
		
        if (alpha > 0) & ~light %#ok<AND2>
            g(k,1) = patch( ...
                'parent',dax, ...
                'xdata',x, ...
                'ydata',y, ...
                'linestyle',style, ...
                'linewidth',width, ...
                'hittest', hit_test, ...
                'EdgeColor',color, ...
                'FaceColor',color, ...
                'FaceAlpha',alpha ...
            );	
        else
            g(k,1) = patch( ...
                'parent',dax, ...
                'xdata',x, ...
                'ydata',y, ...
                'linestyle',style, ...
                'linewidth',width, ...
                'hittest', hit_test, ...
                'EdgeColor',color, ...
                'FaceColor','none', ...
                'Erasemode','none' ...
            );
        end

        %--
        % display event id text
        %--

        xlim = get(dax, 'xlim'); ylim = get(dax, 'ylim');

        dax_size = get_size_in(dax, 'pixels');

        pos = [x(1), y(3), 0];

        if pos(1) < xlim(1)
            pos(1) = xlim(1) + 20 * (data.browser.page.duration / dax_size(3));
        end

        g(k,2) = text( ...
            'parent',dax, ...
            'position',pos, ...
            'string',['#' int2str(event.id)], ...
            'clipping','on', ...
            'color',color, ...
            'edgecolor',color, ...
            'linestyle','-', ...
            'margin',2, ...
            'HorizontalAlignment','right', ...
            'VerticalAlignment','bottom', ...
            'fontweight','bold', ...
            'rotation',0 ...
        );

        % NOTE: this is the same color used in 'text_highlight'

        if ~light
            text_highlight(g(k,2));
        end

        set(g(k,:), 'tag', tag);

	end
	
	% NOTE: this early exit precludes the display of light events by widgets
	
	%--------------------
	% WIDGET DISPLAY
	%--------------------
	
	% NOTE: this should not happen on the log browser event display
	
	if ~isempty(widget)
		
		%--
		% collect widget data
		%--

		% TODO: figure out what the data should really be

		temp.event = event; 

		% NOTE: we may not need this, or it may be stale

		temp.event.id = event.id;

		temp.event.display.time = map_time(sound, 'slider', 'record', event.time);

		% NOTE: we could perhaps simply pass this as the log color
		
		temp.event.display.color = color;

		temp.event.display.light = light;
		
		temp.log = log;

		%--
		% handle special active detection log case
		%--
		
		if active_flag
			temp.log.name = 'ACTIVE_DETECTION_LOG'; temp.log.index = 0; temp.event.id = k;
		end
			
		%--
		% update widget
		%--

		for j = 1:length(widget)
			
% 			db_disp; disp(get(widget(j), 'tag'));
			
			result = widget_update(par, widget(j), 'event__display', temp, 0, context);
		
			% NOTE: we are always dealing with a single event here
			
			if ~isempty(result.handles)
				set(result.handles, 'tag', ['previous-display ', event_tag(temp.event, temp.log)]);
			end
			
		end
		
	end
	
	%---------------------------------------------------
	% EARLY EXIT FOR LIGHTWEIGHT MODE
	%---------------------------------------------------
	
	% NOTE: this early exit precludes the display of light events by widgets
	
	if light
		continue;
	end	
	
	%---------------------------------------------------
	% DISPLAY LABEL (FROM TAGS)
	%---------------------------------------------------
	
	handles = display_label( ...
		dax, event, data.browser.sound, data.browser.grid, color, active_flag ...
	);
	
	set(handles, 'tag', tag);
	
	%---------------------------------------------------
	% DISPLAY EVENT MEASUREMENTS
	%---------------------------------------------------

	% TODO: exceptions must be handled here and extension warning produced
	
% 	measurement_display(par, m, ix(k), data);

	event.axes = dax;
	
	handles = create_browser_measure_displays(par, event, exts1, contexts1);

	set(handles, 'tag', tag);
	
	%---------------------------------------------------
	% ATTACH CONTEXT MENU ACTION
	%---------------------------------------------------	

	%--
	% attach event menu to patch context menu
	%--

	if active_flag

		set(g(k,3), ...
			'buttondownfcn', {@active_bdfun, par, khz_flag} ...
		);

		continue;

	end

    if ~ishandle(g(k, 1))
        continue;
    end

    set(g(k,1), ...
        'buttondownfcn', {@event_menu_bdfun, 'sound', par, log, event.id} ...
    ); 


end


%--------------------------------------------------------
%  DISPLAY_LABEL
%--------------------------------------------------------

function handles = display_label(dax, event, sound, grid, color, active_flag)

% display_label - create graphical display of label tag
% -----------------------------------------------------

%---------------------------------------------
% INITIALIZATION
%---------------------------------------------

handles = [];

%--
% get label and rating
%--

label = get_label(event);

rating = event.rating;

if isempty(label) && isempty(event.rating)
	return;
end

%--
% build display string from label and rating
%--

str = {};

if ~isempty(label)
	str{end + 1} = label;
end

% TODO: display rating using a marker line, consider making it editable

% NOTE: we will likely also make this a toggle, since it will take considerable real estate

if ~isempty(event.rating) && event.rating
	str{end + 1} = strcat(str_line(rating, '*'), str_line(5 - rating, ' '));
end

%--
% compute text position using event
%--

axes_time = map_time(sound, 'slider', 'record', event.time);

x = 0.5 * sum(axes_time) + 0.375 * diff(axes_time);

% TODO: text padding takes up half of the display time, make this faster

pad = get(dax, 'ylim'); 

pad = 0.0125 * pad(2);

if strcmp(grid.freq.labels, 'Hz')
	y = event.freq(2) + pad;
else
	y = (event.freq(2) / 1000) + pad;
end

%---------------------------------------------
% DISPLAY LABEL
%---------------------------------------------

if active_flag
	
	handles(end + 1) = text( ...
		'parent', dax, ...
		'position', [x, y, 0], ...
		'clipping', 'off', ...
		'string', str, ...
		'margin', 2, ...
		'edgecolor', [1 1 1], ...
		'linewidth', 1, ...
		'linestyle', '-', ...
		'color', color, ...
		'HorizontalAlignment', 'left', ...
		'VerticalAlignment', 'middle' ...
	);

else

	handles(end + 1) = text( ...
		'parent', dax, ...
		'position', [x, y, 0], ...
		'clipping', 'off', ...
		'string', str, ...
		'color', color, ...
		'HorizontalAlignment', 'left', ...
		'VerticalAlignment', 'middle' ...
	);

end

% TODO: make other modes available

mode = 'Diagonal';

switch mode
	
	case 'Horizontal'
		set(handles, 'rotation', 0);

	case 'Diagonal'
		set(handles, 'rotation', 25);
		
	case 'Vertical'
		set(handles, 'rotation', 90);

end

%--
% add highlight to display text
%--

% TODO: make text highlight consider multiple states, or create new function to allow text as controls

% NOTE: perhaps a 'text_control' function that implements suitable value and callback concepts

opt = text_highlight; opt.initial_state = 1;

text_highlight(handles(1), '', opt);

