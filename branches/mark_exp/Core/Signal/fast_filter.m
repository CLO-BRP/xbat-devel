function X = fast_filter(X, b)

% fast_filter - fast transform based filter computation
% -----------------------------------------------------
%
% X = fast_filt(X, b)
%
% Input:
% ------
%  X - data
%  b - filter coefficients
%
% Output:
% -------
%  X - filtered data

% NOTE: look at the LINEAR_BASE compute, that function runs a test

%--
% setup
%--

N = size(X, 1); n = impzlength(b, 1) - 1;

%--
% pad filter and select valid section
%--

X = [flipud(X(1:n, :)); X; flipud(X(end - n + 1:n, :))];

X = fast_conv(X, b); 

X = X(n + 1: n + N, :);