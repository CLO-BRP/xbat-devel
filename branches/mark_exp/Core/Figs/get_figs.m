function handles = get_figs(candidate, prefix)

% get_figs - get fig handles
% --------------------------
%
% handles = get_figs(candidate, prefix)
%
% Input:
% ------
%  candidate - figures (def: all)
%  prefix - of figure tag (def: 'FIG', for figures created with 'fig')
%
% Output:
% -------
%  handles - found

%--
% set default prefix and candidates
%--

if nargin < 2
	prefix = 'FIG';
end 

if ~nargin
	candidate = findobj(0, 'type', 'figure');
end

%--
% select candidate figures with matching tag prefix
%--

handles = candidate(strncmp(prefix, get(candidate, 'tag'), numel(prefix)));


