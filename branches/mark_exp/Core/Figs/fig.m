function handle = fig(m, n, k)

% fig - create figure with fig menu
% ---------------------------------
%
% handle = fig(m, n, k)
%
% Input:
% ------
%  m, n - array rows and columns
%  k - figure position
%
% Output:
% -------
%  handle - to figure
%
% NOTE: position increases along rows, order is left to right and down

%--
% create and configure figure
%--

handle = figure;

set(handle, ...
	'tag', 'FIG', ...
	'doublebuffer', 'on', ...
	'backingstore', 'on' ...
);

%--
% position if needed
%--

if nargin == 3
	fig_sub(m, n, k, handle);
end

%--
% add fig tools menu 
%--

fig_menu(handle);