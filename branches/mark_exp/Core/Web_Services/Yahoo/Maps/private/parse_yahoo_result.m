function url = parse_yahoo_result(result)

% NOTE: this xml result contains a url where we may get the requested image

lines = file_readlines(result); line = lines{2};

start = strfind(line, '>http') + 1; stop = strfind(line, '</') - 1;

url = line(start:stop);