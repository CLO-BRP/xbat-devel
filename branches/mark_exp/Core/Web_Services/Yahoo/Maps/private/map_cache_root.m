function root = map_cache_root

% map_cache_root - get root of map cache
% --------------------------------------
%
% root = map_cache_root
%
% Output:
% -------
%  root - root for map cache

%--
% check persistent store
%--

persistent MAP_CACHE_ROOT;

if ~isempty(MAP_CACHE_ROOT)
	root = MAP_CACHE_ROOT; return;
end

%--
% check filesystem for root
%--

root = create_dir(fullfile(fileparts(mfilename('fullpath')), 'Cache'));

if ~isempty(root)
	MAP_CACHE_ROOT = root;
else
	error('Unable to get or create map cache directory.');	
end