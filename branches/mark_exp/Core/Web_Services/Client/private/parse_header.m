function header = parse_header(lines)

% http://www.w3.org/Protocols/

% http://en.wikipedia.org/wiki/List_of_HTTP_headers

% http://en.wikipedia.org/wiki/MIME

%--
% initialize header and pack lines
%--

header = struct; header.lines = lines;

%--
% get basic http information from first line
%--

part = str_split(lines{1}, ' '); 

header.version = part{1}; header.status.code = part{2}; header.status.name = str_implode(part(3:end));

%--
% get headers information
%--

% TODO: handle MIME header

for k = 2:numel(lines)
	
	ix = find(lines{k} == ':', 1); 
	
	field = header_to_field(lines{k}(1:ix - 1)); value = lines{k}(ix + 2:end);
	
	switch field

		case 'set_cookie'
			header.cookie = parse_cookie(value);

		otherwise
			header.(field) = value;
			
	end
	
end