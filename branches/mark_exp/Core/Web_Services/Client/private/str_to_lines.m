function lines = str_to_lines(str)

% NOTE: should this function replace the string reader in 'file_readlines'?

%--
% get length of string and line-breaks
%--

n = numel(str); ix = [0, find(double(str) == 10)];

if ix(end) ~= n
	ix(end + 1) = n + 1; 
end

%--
% get lines from string
%--

lines = cell(numel(ix) - 1, 1);

for k = 1:(numel(ix) - 1)
	lines{k} = str(ix(k) + 1:ix(k + 1) - 1);
end