function [response, status, result] = curl_rest(method, url, headers, data, opt)

% curl_rest - use curl to make rest calls, that user all http commands
% --------------------------------------------------------------------
%
% [response, status, result] = curl_rest(method, url, headers, data)
%
% Input:
% ------
%  method - HTTP request method: GET, POST, PUT, DELETE
%  url - address of resource
%  headers - field and value pairs
%  data - field and value pairs
%
% Output
% ------
%  response - to request
%  status - of curl
%  result - from curl

% NOTE: this function may become a lower level function called by method specific functions

%-----------------------------
% SETUP
%-----------------------------

persistent TOOL; 

if isempty(TOOL)

	if ispc
		bin = 'curl.exe';
	else
		bin = 'curl';
	end

	TOOL = get_tool(bin);

end

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set and possibly output default options
%--

if nargin < 5
	opt.cookie = 1;
end

if ~nargin
	response = opt; return;
end

%--
% set no data and headers default
%--

if nargin < 4
	data = {};
end 

% NOTE: the headers is a cell array of field value pairs, fields are not proper variable names

if nargin < 3
	headers = {};
end

%--
% check location string
%--

% NOTE: a little bit paranoid, be a bit more so

url = strtrim(url);

%-----------------------------
% BUILD REQUEST AND CURL
%-----------------------------

%--
% set the request method and address in command string
%--

% NOTE: 'X' allows us to set request method. we also set other flags: 's' removes progress time stamps, the 'i' flag includes response headers

request = ['"', TOOL.file, '" -s -i -X ', upper(method), ' ', url];

%--
% if we are supporting cookies check if we have a relevant one and append the headers
%--

% TODO: check for cookie expiration

if opt.cookie
	
	cookie = curl_cookie('get', url);
		
	if ~isempty(cookie)
		headers{end + 1} = 'Cookie'; headers{end + 1} = [cookie.name, '=', cookie.content];
	end
	
end 

%--
% append headers and data to command string
%--

if ~isempty(headers)
	
	% NOTE: the internal 'strcat' builds each header, the outer appends the command-line flag, the implode concatenates

	request = [request, ' ', str_implode(strcat({'-H '}, strcat('"', headers(1:2:end), {': '}, headers(2:2:end)), '"'))];
	
end

if ~isempty(data) 
	
	if ischar(data)
		request = [request, ' -d "', data, '"'];
	else
		request = [request, ' ', str_implode(strcat({'-d '}, strcat(data(1:2:end), {': '}, data(2:2:end))))];
	end
	
end

%--
% try to curl
%--

try
	[status, result] = system(request);
catch
	nice_catch(lasterror, 'CURL failed.');
end

%-----------------------------
% PARSE AND BUILD RESPONSE
%-----------------------------

%--
% add location and raw result to response
%--

response.url = url;

response.request = request;

response.result = result;

%--
% HEADER
%--

% NOTE: first we get the headers

lines = str_to_lines(result); header = {};

for k = 1:numel(lines)
	
	if isempty(lines{k})
		break;
	else
		header{end + 1} = lines{k};
	end
	
end

response.header = parse_header(header);

%--
% CONTENT
%--

% NOTE: then we get the content, considering the header

% TODO: we may have lost some characters from the content at this point

content = lines(k + 1:end);

response.content.lines = content;

% NOTE: we store the body here as a string so that callers of this function can process the response

response.body = char(content);

% TODO: we should finally parse content lines to MATLAB native struct

return;

% TODO: the status is not being fully parsed

%--
% redirect moved resources
%--

% NOTE: redirect when the resource is moved and we are given a location

redirect = string_is_member(response.header.status.code, {'301', '302'}) && isfield(response.header, 'location');

if redirect
	[response, status, result] = curl_rest(method, response.header.location, headers, data, opt);
end

% NOTE: indicate that the response is from a new location

response.redirect = redirect;

if redirect
	response.original = url;
end

%--
% set cookie if needed
%--

if opt.cookie && isfield(response.header, 'cookie')
	
	curl_cookie('set', response.url, response.header.cookie);
	
end
