function ip = get_ip(force)

% get_ip - get own IP address
% ---------------------------
%
% ip = get_ip(force)
%
% Input:
% ------
%  force - refresh, otherwise the address is cached
%
% Output:
% -------
%  ip - address

% TODO: create server script to get this information remotely as well, add 'method' parameter 'local' versus 'remote'

if ~nargin
	force = 0; 
end

persistent IP;

if ~isempty(IP) && ~force
	ip = IP; return; 
end

%--
% call system to get ip
%--

% TODO: generalize to other operating systems

[status, result] = system('ipconfig');

%--
% parse output of command to get address
%--

% TODO: add some fault-tolerance to the parsing

opt = file_readlines; opt.skip = 1;

lines = file_readlines(result, @strtrim, opt);

for k = 1:numel(lines)
	
	% NOTE: we get the first displayed IP
	
	if strmatch('IP', lines{k})
		part = str_split(lines{k}, ':'); ip = strtrim(part{2}); break;
	end
	
end

% NOTE: store result persistently

IP = ip;