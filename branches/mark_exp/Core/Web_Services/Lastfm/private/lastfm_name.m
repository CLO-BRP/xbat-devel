function name = lastfm_name(name)

% NOTE: this is a simple way for us to normalize resource names

name = strrep(lower(strtrim(name)), ' ', '+');