function url = lastfm_url(field)

%--
% create persistent URL hash
%--

persistent URL;

if isempty(URL)

	URL.post_handshake = 'http://post.audioscrobbler.com/';

	URL.radio_handshake = 'http://ws.audioscrobbler.com/radio/handshake.php';

	URL.userfeed = 'http://ws.audioscrobbler.com/1.0/user';

	URL.xmlrpc = 'http://ws.audioscrobbler.com/1.0/rw/xmlrpc.php';

	URL.radio_xspf = 'http://ws.audioscrobbler.com/radio/xspf.php';

	URL.radio_adjust = 'http://ws.audioscrobbler.com/radio/adjust.php';
	
	URL.fields = fieldnames(URL);

end

%--
% select a field if needed
%--

if ~nargin
	url = URL; return;
end

if ismember(field, URL.fields)
	url = URL.(field);
else
	error(['Unrecognized Last.fm URL ''', field, '''.']);
end