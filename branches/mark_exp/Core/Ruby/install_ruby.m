function install_ruby

% NOTE: this will create a local tool installation, 'get_tool' will get to this before getting the system installed one

% NOTE: this will not work at the moment, 'install_tool' needs to handle executables

% url = 'http://rubyforge.org/frs/download.php/29263/ruby186-26.exe';

url = 'ftp://ftp.ruby-lang.org/pub/ruby/binaries/mswin32/ruby-1.8.7-i386-mswin32.zip';

install_tool('Ruby', url, 'curl');