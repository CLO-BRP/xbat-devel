function [status, result] = ruby(varargin)

%--
% get tool
%--

% NOTE: this will get the local ruby before the system ruby if the former is installed

tool = get_tool('ruby.exe');

if isempty(tool)
	error('Need to install ruby on the system.');
end

%--
% use tool
%--

