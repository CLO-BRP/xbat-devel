function str = xbat_version(name)

% xbat_version - output XBAT version string
% -----------------------------------------
%
% str = xbat_version(name)
%
% Input:
% ------
%  name - version name
%  info - subversion info
%
% Output:
% -------
%  str - version string

%-----------------------
% HANDLE INPUT
%-----------------------

copy = is_working_copy(xbat_root);

%--
% set default name
%--

% NOTE: if we are not a working copy, surely we are using the release

% TODO: consider subversion details, to handle a working copy of the release branch!

if nargin < 1
	if copy
		name = 'DEVEL';
	else
		name = 'PRE R5';
	end
end

%-----------------------
% VERSION STRING
%-----------------------

%--
% get matlab version info
%--

rel = ver('matlab'); rel = rel.Version;

%--
% build version string
%--

% NOTE: this is used in figure titles and structure version fields

str = [name, '  (MATLAB ', rel, ')'];

%-----------------------
% SVN VERSION STRING
%-----------------------

%--
% check SVN info is relevant
%--

if ~copy % || isempty(tsvn_root)
	return;
end

%--
% get svn info
%--

info = get_svn_info(xbat_root);

if isempty(info) % || ~isfield(info, 'rev')
	return;
end

%--
% create string with svn info 
%--

str = ['SVN', upper(strrep(info.url, info.repository_root, '')), ' (', int2str(info.last_changed_rev), ')  MATLAB ', rel, ' '];

% str = [name, '  (SVN ', int2str(info.rev), ')  (MATLAB ', rel, ')'];
