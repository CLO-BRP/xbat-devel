function value = xbat_developer(user)

% xbat_developer - xbat developer state
% -------------------------------------
%
% value = xbat_developer(user)
%
% Input:
% ------
%  user - user
%
% Output:
% -------
%  value - developer

%--
% get active user as default
%--

if ~nargin
    user = get_active_user;
end

%--
% get user preference
%--

value = get_user_preference(user, 'developer');

if isempty(value)
    value = 0; set_user_preference(user, 'developer', value); 
end
