function root = html_cache_root

% html_cache_root - place to store HTML cached files
% --------------------------------------------------
%
% root = html_cache_root
%
% Output:
% -------
%  root - path to HTML cache root

root = create_dir([fileparts(mfilename('fullpath')), filesep, 'Cache']);

if isempty(root)
	error('Unable to create HTML cache root.');
end
