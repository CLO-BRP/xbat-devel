function pal = xbat_palette(str)

% xbat_palette - main palette to provide access to files
% ------------------------------------------------------
%
% pal = xbat_palette
%
%     = xbat_palette(str)
%
% Input:
% ------
%  str - command string
%
% Output:
% -------
%  pal - palette handle

%--------------------------------------------------------------
% CHECK FOR PALETTE
%--------------------------------------------------------------

pal = findobj(0, 'type', 'figure', 'name', 'XBAT');

%--
% position palette and possibly perform command
%--

if ~isempty(pal)

	if nargin
		control_callback([], pal, str);
	end
		
	return;
	
end

if nargout
	return;
end

%--------------------------------------------------------------
% CREATE PALETTE
%--------------------------------------------------------------

%--
% get user
%--

user = get_active_user;

%--
% create palette controls
%--

control = xbat_palette_controls(user);

%--
% configure palette controls
%--

opt = control_group;

opt.width = 14;

opt.top = 0;

opt.bottom = 1.25;

opt.header_color = get_extension_color('root');

%--------------------------------------------------------------
% RENDER PALETTE
%--------------------------------------------------------------

%--
% create palette
%--

pal = control_group([], @xbat_controls_callback, 'XBAT', control, opt);

%--
% add menus
%--

xbat_menu(pal);

user_menu(pal);

session_menu(pal, user);

if xbat_developer
	
	browser_svn_menu(pal);

	project_menu(pal);

end

% text_menu(pal);

%--
% tag and position palette
%--

set(pal, 'tag', 'XBAT_PALETTE::CORE::XBAT');

position_palette(pal, 0, 'top right');

%--------------------------------------------------------------
% ADD CONTEXT MENUS
%--------------------------------------------------------------

% NOTE: the context menus contain many important commands, document this

%--------------------
% Sounds
%--------------------

%--
% main context menu
%--

g = findobj(pal,'tag','Sounds','style','listbox');

% NOTE: this two step process is cumbersome, create shortcut

temp = uicontextmenu('parent',pal); set(g,'uicontextmenu',temp);

L = { ...
	'(Basic)', ...
	'Open', ...
	'Open in New Window', ...
	'Configure ...', ...
	'(Annotation)', ...
	'Tags', ...
	'Rating', ...
	'Annotation', ...
	'Attributes', ...
	'(Action)', ...
	'Actions', ...
	'Copy to Library', ...
	'Show Files ...', ...
	'Delete ...' ...
};

temp = menu_group(temp, @xbat_controls_callback, L);

set(temp, 'tag', 'Sounds');

parenthetical_menus(temp);

%--
% add libraries to menu
%--

tmp2 = menu_group( ...
	get_menu(temp, 'Copy to Library'), @xbat_controls_callback, library_name_list(user) ...
);

set(tmp2, 'tag', 'Sounds');
	
%-------------------------
% Logs
%-------------------------

%--
% main
%--

g = findobj(pal,'tag','Logs','style','listbox');

tmp = uicontextmenu('parent', pal);

set(g,'uicontextmenu', tmp);

L = { ...
	'(Basic)', ...
	'Open', ...
	'Open in New Window', ...
	'(Action)', ...
	'Actions', ...
	'Export Clips ...', ...
	'Show Files ...', ...
	'Delete ...' ...
};

tmp = menu_group(tmp, @xbat_controls_callback, L);

set(tmp, 'tag', 'Logs');

parenthetical_menus(tmp);

%--
% library
%--

% NOTE: should the copy to library function should be extended to logs?

% L = struct_field(user.library,'name');
% 
% tmp = menu_group(get_menu(tmp,'Copy to Library'),@xbat_controls_callback,L);
% 
% set(tmp,'tag','Sounds');

%--------------------
% Attach Actions
%--------------------

type = {'sound', 'log'};

for k = 1:length(type)

	%--
	% find parent action menu
	%--

	name = title_caps([type{k}, 's']);
		
	handles = get_control(pal, name, 'handles');

	par = findobj(get(handles.obj, 'uicontextmenu'), 'label', 'Actions');

	if isempty(par)
		continue;
	end

	%--
	% attach actions menu
	%--
	
	action_menu(par, type{k});
	
end

%--------------------------------------------------------------
% UPDATE PALETTE
%--------------------------------------------------------------

pal = xbat_palette('User');


%--------------------------------------------------------------
% XBAT PALETTE CALLBACK FUNCTION
%--------------------------------------------------------------

function xbat_controls_callback(obj, eventdata) %#ok<INUSD>

par = ancestor(obj, 'figure'); current = get(par, 'pointer');

set(par, 'pointer', 'watch');

try
	xbat_controls_actions(obj, eventdata);
catch
	nice_catch(lasterror, 'XBAT palette action failed.');
end 

try
	set(par, 'pointer', current);
end


function xbat_controls_actions(obj, eventdata) %#ok<INUSD>

%--
% get context from object handle
%--

if strcmp(get(obj, 'type'), 'uimenu')
	
	%--
	% handle palette menus, typically contextual menus
	%--
	
	pal = xbat_palette; % palette figure handle
	
	tag = get(obj, 'tag'); % the callback is largely determined by the parent controls, its name is in the tag
	
	action = get(obj, 'label'); % this is the label on the menu item
	
else
	
	%--
	% handle other controls in palette
	%--
	
	pal = xbat_palette; % palette figure handle
	
	tag = get(obj, 'tag'); % this is the name of the control
	
	action = ''; % the action is typically provided by the control tag, no need for action
	
end

%--
% perform action
%--

switch tag
	
	%------------------------------------------------------------------
	% CHANGE USER
	%------------------------------------------------------------------
	
	case 'User', set_active_user(get_control(pal, 'User', 'value'));

	%------------------------------------------------------------------
	% ADD USER
	%------------------------------------------------------------------
	
	case 'add_user', set_active_user(new_user_dialog);

	%------------------------------------------------------------------
	% EDIT USER
	%------------------------------------------------------------------		
		
	case 'edit_user', set_active_user(new_user_dialog(get_active_user));
			
	%------------------------------------------------------------------
	% DELETE USER
	%------------------------------------------------------------------
	
	case 'Delete User', delete_user(get_active_user);
		
	%------------------------------------------------------------------
	% LOAD LIBRARY (SET ACTIVE LIBRARY)
	%------------------------------------------------------------------
	
	case 'Library'
		
		set_control(pal, 'edit_library', 'enable', 0);
		
		%--
		% get library from control
		%--
		
		name = get_control(pal, 'Library', 'value');
		
		lib = get_library_from_name(name);
		
		%--
		% Set Active Library
		%--
		
		set_active_library(lib);
		
		%--
		% Update Context Menus
		%--
		
		update_context_menus(pal);
	
	%------------------------------------------------------------------
	% NEW_LIBRARY
	%------------------------------------------------------------------
		
	case 'new_library', user_subscribe(new_library_dialog, get_active_user);
		
	case 'edit_library', edit_library_dialog;
				
	%------------------------------------------------------------------
	% SUBSCRIBE
	%------------------------------------------------------------------
		
	case 'subscribe_user', user_subscribe;
		
	%------------------------------------------------------------------
	% UNSUBSCRIBE
	%------------------------------------------------------------------
		
	case 'unsubscribe_user', user_unsubscribe(get_active_library, get_active_user);		
				
	%------------------------------------------------------------------
	% FIND SOUNDS
	%------------------------------------------------------------------
	
	case 'find_sounds'
		
		handles = get_control(pal, 'Sounds', 'handles');
		
		sound_names = sound_name(get_library_sounds);
		
		set(handles.uicontrol.listbox, 'string', sound_names);
		
		%--
		% filter available sounds
		%--
		
		listbox_search(pal, 'sound');
			
		%--
		% Update Log Search
		%--
		
		xbat_palette('find_logs');
		
		xbat_palette('Sounds');
		
		%--
		% Update Context Meuns
		%--
		
		update_context_menus(pal);
		
	%------------------------------------------------------------------
	% SOUNDS ACTIONS
	%------------------------------------------------------------------
	
	case 'Sounds'
			
		%--
		% perform action
		%--
		
		switch (action)
			
			%------------------------------------------------------------------
			% CONFIGURE
			%------------------------------------------------------------------
			
			% NOTE: configure source in generic and format specific ways
			
			case 'Configure ...'
				
				%--
				% edit configuration for selected sound
				%--
				
				edit_sound_config(get_selected_sound);				
				
			%--------------------------------------------------------------
			% OPEN AND OPEN IN NEW WINDOW
			%--------------------------------------------------------------
			
			% NOTE: one or many sounds may be opened at the same time
			
			case {'Open', 'Open in New Window'}
					
				%--
				% get selected sounds 
				%--
				
				sounds = get_selected_sound;
				
				%--
				% configure open and open
				%--
				
				opt = ~strcmp(action, 'Open'); lib = get_active_library;
				
				for k = 1:length(sounds)
					
					open_library_sound(sound_name(sounds(k)), lib, opt);
					
				end
								
			%--------------------------------------------------------------
			% SHOW FILES ...
			%--------------------------------------------------------------

			case 'Show Files ...'
				
				sounds = get_selected_sound;
				
				%--
				% match sound and open directory containing files
				%--
				
				for k = 1:length(sounds)
						
					if strcmpi(sounds(k).type, 'file')
						file = [sounds(k).path, sounds(k).file];
					else
						file = sounds(k).path;
					end

					show_file(file);
					
				end
			
			%------------------------------------------------------------------
			% DELETE ...
			%------------------------------------------------------------------
					
			case 'Delete ...'
				
				delete_sounds;

			%------------------------------------------------------------------
			% SELECTION
			%------------------------------------------------------------------
			
			% selection updates the sound info control 
			
			otherwise				
				
				%--
				% update log list based on selection
				%--
				
				xbat_palette('find_logs');
				
				%--
				% get selected sounds in sounds control
				%--
								
				sounds = get_selected_sound;
				
				%--
				% disable edit button for multiple selection
				%--
				
				enable = (length(sounds) == 1) && ~sound_is_open(sounds);
				
				set_control(pal, 'edit_sound', 'enable', enable);
							
				%--
				% open sound on double-click and single selection
				%--

				if double_click(obj) && length(sounds) == 1	
					open_library_sound(sound_name(sounds)); return;	
				end

				%--
				% update sound info
				%--
				
				if isempty(sounds)
					sounds = get_library_sounds;
				end
				
				if isempty(sounds)
					return;
				end
				
				S = sound_info_str(sounds);

				g = findobj(pal,'tag','sound_info','style','listbox');

				set(g, ...
					'string',S, ...
					'value',[], ...
					'listboxtop',1, ...
					'enable','on' ...
				);

				%-------------------------
				% UPDATE CONTEXT MENUS
				%-------------------------
				
				update_context_menus(pal);
				
		end	
		
	%------------------------------------------------------------------
	% NEW_SOUND
	%------------------------------------------------------------------
		
	case 'new_sound'
		
		new_sound_dialog;
		
	%------------------------------------------------------------------
	% NEW_SOUND
	%------------------------------------------------------------------	
	
	case 'edit_sound'
		
		edit_sound_config(get_selected_sound);
		
	%------------------------------------------------------------------
	% FIND LOGS
	%------------------------------------------------------------------
	
	case 'find_logs'
		
		%--
		% get filtered sounds
		%--
		
		handles = get_control(pal, 'Sounds', 'handles'); 
		
		filtered_sounds = get(handles.obj, 'string');
			
		selected_sounds = get_control(pal, 'Sounds', 'value');
		
        if ~isempty(selected_sounds);
			filtered_sounds = selected_sounds;
        end
        
		%--
		% generate unfiltered log list
		%--
		
		handles = get_control(pal, 'Logs', 'handles');
        
        logs = get_logs(get_active_library, filtered_sounds);
        
        log_names = log_name(logs, [], 1);
            
		set(handles.uicontrol.listbox, 'string', log_names);
		
		%--
		% filter list based on search string
		%--
			
		listbox_search(pal, 'log');
		
		%--
		% update context menus
		%--
		
		update_context_menus(pal);
		
	%------------------------------------------------------------------
	% LOGS ACTIONS
	%------------------------------------------------------------------
	
	case 'Logs'
				
		switch (action)
			
			%------------------------------------------------------------------
			% OPEN
			%------------------------------------------------------------------
			
			% open checks for windows that may currently be browsing the
			% parent sound and opens the logs in these, otherwise a new
			% sound browser is opened that displays the sound at the point
			% where the log was last closed
			
			case {'Open', 'Open in New Window'}
								
				%--
				% get string value from associated control
				%--

				logs = get_selected_log;
                
				%--
				% open logs
				%--
				
				% set open in new window option
				
                opt = ~strcmp(action, 'Open');
				
				% open selected logs in proper mode
				
				for k = 1:length(logs)
					open_library_log(logs(k),opt);
				end
				
			%------------------------------------------------------------------
			% EXPORT CLIPS ...
			%------------------------------------------------------------------
			
            case 'Export Clips ...'
		
				%--
				% ask for output directory
				%--
				
				top_dir = uigetdir;

				if (~top_dir)
					return;
				end
				
				%--
				% get logs
				%--
				
				logs = get_selected_log;
                
				%--
				% configure export log
				%--
				
				opt = export_log; opt.image.create = 1;
				
				%--
				% export logs
				%--
				
				% NOTE: consider using a waitbar display for the collection
				
				for k = 1:length(logs)		
					export_log(logs(k), opt, top_dir);		
				end
						
			%------------------------------------------------------------------
			% DELETE
			%------------------------------------------------------------------
					
			case 'Delete ...'
				
				delete_logs();
								
			%------------------------------------------------------------------
			% SHOW FILES ...
			%------------------------------------------------------------------
			
			% selection updates the sound info control as well as filters
			% the log control contents
			
			case 'Show Files ...'
				
				%--
				% get string value from associated control
				%--
				
				logs = get_selected_log;
				
				%--
				% show files
				%--
				
				for k = 1:length(logs)
					show_file(logs(k).file);		
				end
										
			%------------------------------------------------------------------
			% SELECTION
			%------------------------------------------------------------------
			
			% selection in the logs control updates the log info control
			
			otherwise
				
				%--
				% get selected logs in logs control
				%--
				
				value = get(obj,'string'); ix = get(obj,'value');
				
				%--
				% set and check timer to trigger double click action
				%--
					
				flag = double_click(obj);
				
				%--
				% handle the update with multiple logs selected
				%--
				
				if length(ix) > 1
					
					%-------------------------
					% UPDATE LOGS
					%-------------------------
					
					%--
					% update log info
					%--
					
					g = findobj(pal,'tag','log_info','style','listbox');
					
					set(g, ...
						'string',[], ...
						'value',[], ...
						'listboxtop',1, ...
						'enable','off' ...
					);
					
					%-------------------------
					% UPDATE CONTEXT MENUS
					%-------------------------
					
					update_context_menus(pal);
					
					return;
					
				end
				
				%--
				% handle the update with single log selected
				%--
				
				value = value{ix};
				
				%--
				% open log file
				%--
            
                log = get_log_from_name(value);
             
                if isempty(log)
                    return;
                end
                
				%-------------------------
				% UPDATE LOGS
				%-------------------------
								
				%--
				% create log info string 
				%--
                
				info = log_info_str(log);
						
				%--
				% update log info control
				%--
				
				g = findobj(pal, 'tag', 'log_info', 'style', 'listbox');
				
				set(g, ...
					'string', info, ...
					'value', [], ...
					'listboxtop', 1, ...
					'enable', 'on' ...
				);
				
				%--
				% open log based on double click
				%--
				
				if flag
					open_library_log(value); % this is equivalent to open log
				end
				
				%-------------------------
				% UPDATE CONTEXT MENUS
				%-------------------------
				
				update_context_menus(pal);
				
		end
				
end


%---------------------------
% UPDATE_CONTEXT_MENUS
%---------------------------

function update_context_menus(pal)

%--
% update sounds
%--

top = findobj(pal, 'tag', 'Sounds', 'style', 'listbox');
		
if isempty(get(top, 'value'))
	state = 'off';
else
	state = 'on';
end

handles = get(get(top, 'uicontextmenu'), 'children');

set(handles, 'enable', state);

parenthetical_menus(handles);

if strcmp(state, 'on')
	
	%--
	% create tags and rating menus
	%--
	
	par = get_menu(handles, 'Rating');

	create_rating_menu(par, [], 5, @rate_sound_callback);
		
	%--
	% update 'attributes'
	%--

	par = findobj(handles, 'label', 'Attributes');

	if ~isempty(par)
		attribute_menu(par);
	end

	%--
	% update 'copy to library'
	%--

	par = findobj(handles, 'label', 'Copy to Library');

	delete(get(par, 'children'));

	user = get_active_user;

	list = library_name_list(user); active_lib = list{user.active};

	L = setdiff(list, active_lib);

	if ~isempty(L)
		set(par, 'enable', 'on'); menu_group(par, @copy_to_library, L);
	else
		set(par, 'enable', 'off');
	end

end

%--
% update logs
%--

top = findobj(pal, 'tag', 'Logs', 'style', 'listbox');

if isempty(get(top, 'value'))
	state = 'off';
else
	state = 'on';
end

handles = get(get(top, 'uicontextmenu'), 'children');

set(handles, 'enable', state);

parenthetical_menus(handles);


%------------------------------------------------------------
% COPY TO LIBRARY
%------------------------------------------------------------

function copy_to_library(obj,eventdata) %#ok<INUSD>

%--
% get path of library to copy to
%--

[name, usr] = parse_libname(get(obj,'label'));

if isempty(usr)
	usr = get_active_user; usr = usr.name;
end

dest = get_libraries(get_users('name', usr),'name',name);

%--
% get active library and sounds to copy
%--

src = get_active_library;

sounds = sound_name(get_selecte_sound);

%--
% copy sound directories
%--

% add check for the existence of the sounds in the destination library and
% indicate that this is the case, thus there are two type of warnings
% issued

%--
% create waitbar
%--

%-------------------------------------------
% WAITBAR CODE
%-------------------------------------------
	
h = wait_bar(0); set(h,'name','XBAT > Copy to Library');
	
m = 0;

for k = 1:length(sounds)
	
	%--
	% check that sound exists
	%--
	
	if (exist([dest.path sounds{k}],'dir'))
		flag = 2;
	else
		flag = copyfile([src.path sounds{k}],[dest.path sounds{k}]); m = m + 1;
	end
		
	%--
	% display confirmation
	%--
	
	%-------------------------------------------
	% WAITBAR CODE
	%-------------------------------------------
	
	switch (flag(k))
		
		case (0)
			wait_bar(k/n,h,['Failed to copy ''' sounds{k} ''' to ''' name '.']);
			
		case (1)
			wait_bar(k/n,h,['''' sounds{k} ''' copied to ''' name '''']);
			
		case (2)
			wait_bar(k/n,h,['''' sounds{k} ''' already exists in ''' name '''']);
			
	end
	
	pause(0.1);
	
end

%--
% report results and delete waitbar
%--

%-------------------------------------------
% WAITBAR CODE
%-------------------------------------------

wait_bar(1,h,[int2str(m) ' sounds copied to ''' name ''' library']);

pause(1);

delete(h);

get_library_sounds(dest,'refresh');


% % this code tests the idea of having a single button that serves as both
% % cancel and confirm using the keypress function, in principle the user
% % presses a key before pressing the button providing the button and the
% % corresponding callback function some context information. the operation
% % performed by the button upon pressing is indicated by the changing icon
% % (in this case text) on the button 'X' for cancel and '+' for confirm
% 
% 
% function xbat_palette_kpfun(obj,eventdata)
% 
% % %--
% % % code to display codes available
% % %--
% % 
% % disp('-----------------------');
% % key = get(gcf,'currentkey')
% % chr = get(gcf,'currentcharacter')
% % code = double(chr)
% 
% % %--
% % % code to toggle the button appearing during edit
% % %--
% % 
% % % g = findobj(gcf,'tag','New User Cancel')
% % % 
% % % if (~isempty(g))
% % % 	
% % % 	if (strcmp(key,'shift'))	
% % % 		set(g,'string','X','fontsize',7,'fontname','Comic Sans MS');	
% % % 	else
% % % 		set(g,'string','+','fontsize',10,'fontname','Courier New');
% % % 	end
% % % 	
% % % end

%-------------------------------------------------------------------------
% DELETE_LOGS
%-------------------------------------------------------------------------

function delete_logs(logs)

% delete_logs - callback for delete logs
% ----------------------------------------
%
% delete_logs(logs)
%
% Input:
% ------
%  pal - XBAT palette figure handle

%-------------------------------------------
% get names of logs to delete
%-------------------------------------------

if ~nargin
    logs = get_selected_log;
end
    
if isempty(logs)
    return;
end

n = numel(logs);
								
%-------------------------------------------
% create waitbar
%-------------------------------------------

%--
% progress waitbar
%--

control = control_create( ...
	'name','PROGRESS', ...
	'alias','Deleting Logs ...', ...
	'style','waitbar', ...
	'confirm',1, ...
	'lines',1.15, ...
	'space',0.5 ...
);

name = 'Delete Logs ...';

h = waitbar_group(name, control);

%-------------------------------------------
% delete logs
%-------------------------------------------

m = 0;

for k = 1:length(logs)
	
	%--
	% try to delete log file if not open
	%--
	
    if ~isempty(log_is_open(logs(k)))
        
        message = ['''' log_name(logs(k)) ''' is open and was not deleted'];
        
    else
        
        % NOTE: this will not be sufficient for some database logs
        
        try
            
			% TODO: replace with a call to 'log_root' or whatever it's called
			
            rmdir(fileparts(logs(k).file), 's'); m = m + 1;
      
            message = ['''' log_name(logs(k)) ''' was deleted'];
            
        catch
            
            message = ['''' log_name(logs(k)) ''' was not deleted.'];
            
        end
        
    end
		  
    waitbar_update(h,'PROGRESS', ...
        'value',k/n, ...
        'message', message ...
    );  
		
end

%--
% update library display
%--

xbat_palette('find_sounds');

%--
% report results and delete waitbar
%--

%-------------------------------------------
% WAITBAR CODE
%-------------------------------------------

lib = get_active_library;

waitbar_update(h,'PROGRESS', ...
	'value', 1, ...
	'message', [int2str(m), ' logs deleted from ''', lib.name, ''' library'] ...
);

pause(1);

delete(h);


%--------------------------------------------
% DELETE SOUNDS
%--------------------------------------------

% TODO: factor deletion part to the same place as 'add_sounds'

function delete_sounds(sounds)

% delete_sounds - callback for delete sounds
% ------------------------------------------
%
% delete_sounds(pal)
%
% Input:
% ------
%  pal - XBAT palette figure handle

%--
% get selected sounds
%--

if ~nargin
    sounds = get_selected_sound;
end

%--
% progress waitbar
%--

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Deleting Sounds ...', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'lines', 1.15, ...
	'space', 0.5 ...
);

name = 'Deleting Sounds ...';

h = waitbar_group(name, control);

%-------------------------------------------
% delete sounds
%-------------------------------------------

lib = get_active_library;

m = 0; n = length(sounds);

for k = 1:length(sounds)

    name = sound_name(sounds(k));
    
	[is_open, par] = sound_is_open(sounds(k));
	
	if par
		close(par)
	end	
	
	%--
	% backup sound and remove
	%--
	
% 	sound_backup(lib, sounds(k));
	
    % NOTE: remove sym-link before deleting sound.  Otherwise we delete the data too!

    if isunix
        
        symlink = fullfile(lib.path, name, name);
        
        if symlink(end) == filesep
            symlink(end) = [];
        end
        
        system(['rm ', symlink]);
        
    end

	flag = rmdir([lib.path, name], 's'); % note that the 's' option removes non-empty directories
	
	%--
	% update waitbar
	%--

    if flag
        message = ['''' name ''' removed from ''' lib.name '''']; m = m + 1;
	else
		message = ['Failed to remove ''', name, ''' from ''', lib.name, ''''];
    end
    
    waitbar_update(h,'PROGRESS', ...
        'value', k/n, 'message', message  ...
	);
		
end

%--
% update waitbar to report results and delete waitbar
%--

waitbar_update(h, 'PROGRESS', ...
	'value', 1, 'message', [int2str(m), ' sounds deleted from ''', lib.name, ''' library'] ...
);

%--
% update library cache
%--

get_library_sounds(lib, 'refresh');

%--
% update library display
%--

% NOTE: this should not be required, at least not here

xbat_palette('Library');

set(findobj(xbat_palette, 'type', 'uicontrol', 'tag', 'Sounds'), 'listboxtop', 1);

pause(0.5);

delete(h);


%-------------------------------------------
% xbat_menu
%-------------------------------------------

function xbat_menu(par)

%----------------------
% SETUP
%----------------------

%--
% clear former menu if needed
%--

top = findobj(par, 'type', 'uimenu', 'tag', 'TOP_XBAT_MENU');

if ~isempty(top)
	delete(allchild(top));
end

%----------------------
% CREATE MENUS
%----------------------

%--
% create top menu if needed
%--

if isempty(top)
	top = uimenu(par, 'label', 'File', 'tag', 'TOP_XBAT_MENU');
end 

%--
% migrate
%--

uimenu(top, ...
	'label', 'Migrate ...', ...
	'separator', 'on', ...
	'callback', @migrate_callback ...
);

%--------------------------------------
% MIGRATE_CALLBACK
%--------------------------------------

function migrate_callback(obj, eventdata) %#ok<INUSD>

migrate_xbat;
