function root = projects_root

persistent ROOT;

if isempty(ROOT)
	
	ROOT = create_dir(fullfile(xbat_root, 'Projects'));

	% NOTE: a little paranoia here, this directory typically exists as part of the distribution
	
	if isempty(ROOT)
		error('Failed to create ''Projects'' root directory.');
	end
	
end

root = ROOT; 
