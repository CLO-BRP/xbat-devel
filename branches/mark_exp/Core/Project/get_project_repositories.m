function [repos, file] = get_project_repositories

% get_project_repositories - list
% -------------------------------
% 
% [repos, file] = get_project_repositories
%
% Output:
% -------
%  repos - list
%  file - containing list

% TODO: improve the file format to include user and password if needed

% NOTE: credentials will then be needed in a few places

%--
% get file repositories if available
%--

file = fullfile(projects_root, 'repositories.txt');

if exist(file, 'file')
	opt = file_readlines; opt.skip = 1; opt.pre = '%'; lines = file_readlines(file, [], opt);
else
	lines = {};
end 

%--
% get project repositories if available
%--

project = all_projects; other = {project.repository};

%--
% get unique
%--

repos = unique({lines{:}, other{:}})';

% NOTE: this removes the empty that results from having non-versioned projects

for k = numel(repos):-1:1

	if isempty(repos{k})
		repos(k) = [];
	end
	
end

%--
% create repository file if needed
%--

if ~exist(file, 'file')
	file_writelines(file, lines);
end

