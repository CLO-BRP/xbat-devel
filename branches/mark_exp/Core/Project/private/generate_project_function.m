function fun = generate_project_function(project)

%--
% get project function file
%--

file = project_function_file(project.name, project.author);

fun = project_function(project.name, project.author);

%--
% write file 
%--

field = fieldnames(project); fid = fopen(file, 'w');

% NOTE: we catch to not leave the file open

try
	fprintf(fid, 'function project = %s\n\n', fun);

	for k = 1:numel(field)
		fprintf(fid, 'project.%s = ''%s'';\n\n', field{k}, project.(field{k}));
	end
catch
	fclose(fid);
end

fclose(fid);

%--
% clear is required so we load the recently generated function
%--

clear(fun);