function [repo, repos] = new_repository(repo)

%--
% handle input
%--

if ~nargin || isempty(repo)
	repo = new_repository_dialog;
end

current = get_project_repositories;

if isempty(repo)
	repo = ''; repos = current; return;
end 

%--
% add repository to repository list
%--

current{end + 1} = repo; repos = unique(current);

% NOTE: we check that in fact we have added a new repository

added = numel(repos) > numel(current);

%--
% write to and read from file
%--

file_writelines(fullfile(projects_root, 'repositories.txt'), repos);

% NOTE: this reads the repositories from file and keeps us honest

repos = get_project_repositories;