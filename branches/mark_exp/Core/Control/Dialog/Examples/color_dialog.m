function out = color_dialog(name, head, color)

% color_dialog - color selection dialog
% -------------------------------------
%
% out = color_dialog(name, head, color)
%
% Input:
% ------
%  name - dialog name
%  head - header message
%  color - initial color
%
% Output:
% -------
%  out - dialog output structure

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set default color
%--

if (nargin < 3) || isempty(color)
	color = get_color_by_name('Sea Green');
end

%--
% set default head
%--

if (nargin < 2) || isempty(head)
	head = 'Color';
end

%--
% set default name
%--

if (nargin < 1) || isempty(name)
	name = 'Select Color';
end

%----------------------------------
% CREATE CONTROLS
%----------------------------------

%--
% dialog header
%--

control(1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', head ...
);

%--
% color display axes
%--

% NOTE: the number of lines along with the dialog group options produce a square

control(end + 1) = control_create( ...
	'name', 'color', ...
	'alias', 'current', ...
	'style', 'axes', ...
	'label', 0, ...
	'width', 0.5, ...
	'align', 'left', ...
	'space', 1, ...
	'lines', 4 ...
);

% NOTE: display when we have two colors to compare

compare = 1; % nargin > 2;

if compare
	
	control(end - 1).space = 0.75;
	
	control(end).label = 1; control(end).width = 0.45; control(end).space = -5;
	
	control(end + 1) = control_create( ...
		'name', 'initial', ...
		'style', 'axes', ...
		'width', 0.45, ...
		'align', 'right', ...
		'space', 1, ...
		'lines', 4 ...
	);

end

%--
% color rating
%--

% NOTE: this control style is in development

% control(end + 1) = control_create( ...
% 	'name', 'rating', ...
% 	'style', 'rating', ...
% 	'space', 1.5, ...
% 	'value', 3, ...
% 	'max', 6 ...
% );

%--
% color setting controls
%--

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'space', 0.11 ...
);

tabs = {'Select', 'Value'};

control(end + 1) = tabs_control(tabs);

names = get_color_names; value = find(strcmp(names, color.name), 1);

control(end + 1) = control_create( ...
	'name', 'name', ... 
	'tab', tabs{1}, ...
	'style', 'popup', ...
	'string', names, ...
	'space', 0.75, ...
	'value', value ...
);

control(end + 1) = control_create( ...
	'name', 'filter', ...
	'onload', 1, ...
	'tab', tabs{1}, ...
	'space', 1, ...
	'style', 'edit' ...
);

control(end + 1) = control_create( ...
	'name', 'similar', ...
	'tab', tabs{1}, ...
	'style', 'axes', ...
	'label', 1, ...
	'space', 2, ...
	'lines', 4.55 ...
);

%--
% color controls
%--

names = {'red', 'green', 'blue'};

for k = 1:length(names)
	
	control(end + 1) = control_create( ...
		'name', names{k}, ...
		'tab', tabs{2}, ...
		'style', 'slider', ...
		'min', 0, ...
		'max', 1, ...
		'space', 1, ...
		'value', color.rgb(k) ...
	);

end

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'hidden_header' ...
);

control(end).space = 2;

% TODO: this property should be handled using an observer

control(end).active = 1;

%----------------------------------
% CREATE DIALOG
%----------------------------------

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 10;

%--
% create dialog
%--

out = dialog_group(name, control, opt, @dialog_callback);

if ~isempty(out.values)
	
	flatten(out)
	
	out.values.name = cellfree(out.values.name);
end


%----------------------------------
% DIALOG_CALLBACK
%----------------------------------

function result = dialog_callback(obj, eventdata)

result = [];

callback = get_callback_context(obj, 'pack'); pal = callback.pal;

switch callback.control.name
	
	case 'filter'
		
		handles = get_control_handles(pal.handle, 'name');
		
		names = get_color_names(callback.value); 
		
		if numel(names) < 2
		
			value = 1;
		
		else

			name = get_control_value(pal.handle, 'name');
			
			value = find(strcmp(names, name));
			
			if isempty(value)
				value = 2;
			end
			
		end
		
		set(handles.obj, 'string', names, 'value', value);
		
		set_control(pal.handle, 'name', 'label', ['Names (', int2str(numel(names) - 1), ')']);
		
		dialog_callback(handles.obj); return;
		
	case 'name'
		
		color = get_color_by_name(cellfree(callback.value));
		
		if isempty(color)
			return;
		end 
		
		set_control(pal.handle, 'red', 'value', color.rgb(1));
		
		set_control(pal.handle, 'green', 'value', color.rgb(2));
		
		set_control(pal.handle, 'blue', 'value', color.rgb(3));
		
	case {'red', 'green', 'blue'}
		
		set_control(pal.handle, 'name', 'value', 1);
		

end

%--
% update color display
%--

values = get_control_values(pal.handle);

color = values_to_color(values);

colors = display_similar(pal.handle, color);

color_display(pal.handle, colors(1));

%--
% store color in color display axes control
%--

control_update([], pal.handle, 'color', color);
		

%----------------------------------
% VALUES_TO_COLOR
%----------------------------------

function color = values_to_color(values)

color(1) = values.red;

color(2) = values.green;

color(3) = values.blue;


%----------------------------------
% GET_COLOR_NAMES
%----------------------------------

function names = get_color_names(filter)

% NOTE: we always include 'None' to allow for custom colors

colors = load_css_colors; names = {'None', colors.name};

if ~nargin || isempty(filter)
	return;
end

select = string_contains(names, filter, 0); select(1) = 1; names = names(select);


%----------------------------------
% COLOR_DISPLAY
%----------------------------------

function color_display(pal, color)

% color_display - update color display in color dialog
% ----------------------------------------------------
%
% color_display(pal, color)
%
% Input:
% ------
%  pal - palette figure
%  color - color to display

%--
% get color display axes handle
%--

% NOTE: select axes object from axes control handles

handles = get_control(pal, 'color', 'handles'); ax = handles.obj;

%--
% get handle to or create image in color display axes
%--

obj = findobj(pal, 'tag', 'color_image');

if isempty(obj)
		
	obj = image( ...
		'tag', 'color_image', ...
		'parent', ax, ...
		'xdata', [0, 1], ...
		'ydata', [0, 1] ...
	);

	% NOTE: the layer top setting is critical for proper axes display
	
	set(ax, ...
		'layer', 'top', ...
		'xtick', [], ...
		'ytick', [], ...
		'xlim', [0, 1], ...
		'ylim', [0, 1] ...
	);

end

%--
% set image data according to color
%--

% NOTE: we create a one pixel color image, we need to use the third dimension

for k = 1:3
	value(:, :, k) = color.rgb(k);
end

set(obj, 'cdata', value);

set_control(pal, 'color', 'value', color);



%----------------------------------
% DISPLAY_SIMILAR
%----------------------------------

function colors = display_similar(pal, color)

%--
% get axes size and palette tilesize
%--

handles = get_control_handles(pal, 'similar'); ax = handles.obj;

pos = get_size_in(ax, 'pixels');

opt = get_palette_settings; tilesize = opt.tilesize;

%--
% determine precise size and number of color patches
%--

tiles = floor(pos(3:4) / (1.5 * tilesize)); 

space = (pos(3) - tiles(1) * tilesize) / (tilesize * (tiles(1) - 1));

%--
% get similar colors and set control value
%--

colors = similar_css_colors(color, prod(tiles));

set_control(pal, 'similar', 'value', colors); 

%--
% set parent axes properties
%--

base = get(ancestor(handles.obj, 'figure'), 'color');

set(ax, ...
	'xlim', [0, pos(3)], ...
	'ylim', [0, pos(4)], ...
	'ydir', 'reverse', ...
	'hittest', 'off', ...
	'xcolor', base, ...
	'ycolor', base, ...
	'color', 'none' ...
);

%--
% build color patches
%--

count = 1; handles = [];

for k = 1:tiles(1)
	
	for l = 1:tiles(2)
		
		pos = get_patch_position(k, l, space);
		
		xdata = tilesize * pos.xdata; ydata = tilesize * pos.ydata;
		
		handles(end + 1) = patch( ...
			'parent', ax, ...
			'clipping', 'off', ...
			'xdata', xdata, ... 
			'ydata', ydata, ... 
			'buttondownfcn', {@similar_callback, colors(count)}, ...
			'facecolor', colors(count).rgb ...
		);
	
		if count == 1
			
% 			set(handles(end), 'linewidth', 1, 'edgecolor', 0.25 * ([3 0 0] + base));
			
			% NOTE: the display below does not work well with the label
			
% 			offset = 3;
% 			
% 			xdata = xdata - offset; xdata(2:3) = xdata(2:3) + 2 * offset;
% 			
% 			ydata = ydata - offset; ydata(3:4) = ydata(3:4) + 2 * offset;
% 			
% 			handles(end + 1) = patch( ...
% 				'parent', ax, ...
% 				'clipping', 'off', ...
% 				'xdata', xdata, ... 
% 				'ydata', ydata, ... 
% 				'hittest', 'off', ...
% 				'edgecolor', 'red', ...
% 				'facecolor', 'none' ...
% 			);
			
		end 
		
		count = count + 1;
		
	end
	
end

% NOTE: this makes sure that the patches are visible along with parent axes

set(handles, 'visible', get(ax, 'visible'));


%----------------------------------
% SIMILAR_CALLBACK
%----------------------------------

function similar_callback(obj, eventdata, color)

% NOTE: this function releases the color name filter and sets the name control

pal = ancestor(obj, 'figure')

filter = get_control_value(pal, 'filter');

if ~isempty(filter)
	control = set_control(pal, 'filter', 'value', ''); dialog_callback(control.handles.obj);
end

control = set_control(pal, 'name', 'value', color.name); dialog_callback(control.handles.obj);


%----------------------------------
% GET_PATCH_POSITION
%----------------------------------

function pos = get_patch_position(k, l, space)

% NOTE: this function outputs patch positions in tile units

if nargin < 3
	space = 0.5;
end

offset = 1 + space;

pos.xdata = [0 1 1 0 0] + offset * (k - 1);

pos.ydata = [0 0 1 1 0] + offset * (l - 1);


