function control = slider_control(name, value, range, type, varargin)

% slider_control - shortcut to declare slider control
% ---------------------------------------------------
%
% control = slider_control(name, value, range, type, varargin)
%
% Input:
% ------
%  name - name or name and alias
%  value - value
%  range - min and max
%  type - 'real' (default), 'integer', or 'time'
%  field - field
%  value - value
%
% Output:
% -------
%  control - struct

%-----------------
% HANDLE INPUT
%-----------------

%--
% set type
%--

if nargin < 4 || isempty(type)
	type = 'real';
end

%--
% set range
%--

if nargin < 3 || isempty(range)
	range = [0, 1];
end

%--
% check name and alias input
%--

% TODO: factor this so that all shortcut functions can use it

if ~ischar(name) && ~(iscellstr(name) && (length(name) == 2))
	error('Name is a string or a cell array of two strings.'); 
end

if iscell(name)
	alias = name{2}; name = name{1};
else
	alias = name;
end

%--
% create control using provided information
%--

% NOTE: the way we pass the variable arguments tests that these are proper within 'control_create'

control = control_create( ...
	'style', 'slider', ...
	'type', type, ...
	'name', name, ...
	'alias', alias, ...
	'min', range(1), ...
	'max', range(2), ...
	'value', value, ...
	varargin{:} ...
);

