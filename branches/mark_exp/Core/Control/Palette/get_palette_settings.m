function value = get_palette_settings

% get_palette_settings - get name and tile and font settings
% ----------------------------------------------------------
%
% value = get_palette_settings
%
% Output:
% -------
%  value - palette settings

%-----------------------------------------------
% SETUP
%-----------------------------------------------

% TODO: environment variables should follow some conventions

%--
% get palette size environment variable
%--

pal_size = get_env('palette_size');

if isempty(pal_size)
	
	pal_size = set_env('palette_size', 'smaller');

end

%--
% get palette sounds environment variable
%--

pal_sounds = get_env('palette_sounds');

if isempty(pal_sounds)
	
	pal_sounds = set_env('palette_sounds', 'on');
	
end

%-----------------------------------------------
% PACK PALETTE SETTINGS
%-----------------------------------------------

%--
% pack palette size and sounds variables
%--

value.size = pal_size;

value.sounds = strcmpi(pal_sounds, 'on');

%--
% compute palette properties based on size
%--

% NOTE: the font and tilesize values below are hand selected

switch pal_size
	
	case {'smallest', -2}
		
		value.fontsize = 10; 
		
		value.tilesize = 16;
		
	case {'smaller', -1}
		
		value.fontsize = 11;
		
		value.tilesize = 19;
			
	case {'small', 0}

		value.fontsize = 12.5;
		
		value.tilesize = 21;
		
	case {'medium', 1}
		
		value.fontsize = 13;
		
		value.tilesize = 23;
		
	case {'large', 2}
		
		value.fontsize = 14;
		
		value.tilesize = 25;
		
	case {'larger', 3}
		
		value.fontsize = 15;
		
		value.tilesize = 27;
		
	otherwise
		
		value = [];
		
end
