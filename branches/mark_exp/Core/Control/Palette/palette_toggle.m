function palette_toggle(h, palette, control, value, data)

% palette_toggle - open and close sections of a control group
% -----------------------------------------------------------
%
% palette_toggle(h, palette, control, state, data)
%
% palette_toggle(pal, control, state)
% 
% Input:
% ------
%  h - parent figure of palette
%  pal - palette handle
%  palette - palette name
%  control - control name
%  value - desired state for toggle 'close' or 'open'
%  data - userdata of parent figure

% TODO: refactor this and add 'toggle_data' and 'toggle_move' functions

%------------------------------------------------------------------
% SET CONSTANTS
%------------------------------------------------------------------

% TODO: consider making these persistent

% TODO: make colors come from a color table in some way, configurable

%--
% color constants
%--

BLACK = [0, 0, 0];

MEDIUM_GRAY = [128, 128, 128] / 255;

LIGHT_GRAY  = [192, 192, 192] / 255;

FIGURE_COLOR = get(0,'Defaultuicontrolbackgroundcolor');

%--
% other constants
%--

HOFF = 10^6; 

TEXT_OFF = 1.1 * MEDIUM_GRAY; 

TEXT_ON = BLACK;

COLOR_OFF = (LIGHT_GRAY + FIGURE_COLOR) / 2;

%------------------------------------------------------------------
% HANDLE INPUT
%------------------------------------------------------------------

%----------------------------------
% GUI TOGGLE CALL
%----------------------------------

if nargin < 1
	
	%--
	% get container handles
	%--
	
	% NOTE: we get toggle axes, and palette handle
	
	parent = gcbo; pal = gcbf;
	
	%--
	% get state from toggle axes contents
	%--
	
	child = get(parent, 'children');
	
	toggle = findobj(child, 'tag', 'header_toggle');
		
	%---------------
	
	% NOTE: this is part of the 'hidden_header' hack
	
	hidden_toggle = findobj(toggle, 'userdata', 'hidden');
	
	toggle = setdiff(toggle, hidden_toggle);
	
	%---------------
	
	text = findobj(child, 'tag', 'header_text');
	
% 	image = findobj(child, 'tag', 'header_image');
			
	% TODO: separate text and image here, do this by tagging in 'control_group' perhaps
	
	state = get(toggle, 'string');
	
%----------------------------------
% PROGRAM TOGGLE CALL
%----------------------------------

elseif (nargin == 3)
	
	%--
	% rename input
	%--
	
	pal = h;
	
	toggle = palette;
	
	value = control;

	%--
	% get other relevant state information and handles
	%--
	
	% NOTE: most of these handles are used to modify objects for visual effect
	
	parent = get(toggle,'parent');
	
	child = get(parent,'children');
	
	text = findobj(child,'tag','header_text');
	
	image = findobj(child,'tag','header_image');
	
	state = get(toggle,'string');
	
	% NOTE: return if we are in desired specified state or if state is empty
	
	if isempty(value)
		return;
	end
			
	if ( ...
		(strcmp(value,'close') && strcmp(state,'+')) || ...
		(strcmp(value,'open') && strcmp(state,'-')) ...
	)
		return;
	end
	
else
	
	%--
	% get parent userdata if not provided
	%--
	
	if (nargin < 5) || isempty(data)
		data = get(h,'userdata');
	end
	
	if (nargin < 4) || isempty(value)
		value = '';
	end
	
	%--
	% get container handles
	%--
	
	pal = get_palette(h,palette,data);
	
	if isempty(pal)
		return;
	end
	
	parent = findobj(pal,'type','axes','tag',control);
	
	child = get(parent,'children');
	
	%--
	% get state indicator handles and state
	%--
	
	toggle = findobj(child,'tag','toggle');
	
	% return if toggle is not available
	
	if isempty(toggle)
		return;
	end
	
	% logical subscripting
	
	text = child(child ~= toggle);
		
	state = get(toggle, 'string');
	
	%--
	% return if we are in desired specified state
	%--
	
	if ~isempty(value)
		
		if ( ...
			(strcmp(value, 'close') && strcmp(state, '+')) || ...
			(strcmp(value, 'open') && strcmp(state, '-')) ...
		)
			return;
		end
		
	end
	
end

%--
% handle accordion toggle mode
%--

% FIXME: this does not work because of recursion

% TODO: setup accordion state and editing

if 0

	%--
	% get all, but current and make sure they are closed
	%--
	
	others = findobj(pal, 'tag', 'header_toggle');

	parents = get(others, 'parent'); tags = iterate(@get, parents, 'tag');
	
	tag = get(parent, 'tag');
	
	[ignore, index] = string_is_member(tag, tags);
	
	toggle = others(index); others(index) = [];
	
	% TODO: get palette state and only perform needed updates
	
	palette_toggle(pal, toggle, 'open');
	
	for k = 1:length(others)
		palette_toggle(pal, others(k), 'close'); 
	end
	
	drawnow; drawnow; return;
	
end

%--
% make sure that the resize function of the palette remains empty
%--

set(pal, 'resizefcn', '');

%----------------------------------
% CHANGE STATE OF TAB
%----------------------------------

switch state
	
	%--------------------------------------------------------------
	% CLOSE TAB
	%--------------------------------------------------------------
	
	case {'-', 'min', 'close'}
		
		%--------------------------------------------------------------
		% CHECK FOR TOGGLE DATA
		%--------------------------------------------------------------
		
		data = get(pal, 'userdata');
		
		if ~isfield(data, 'toggle')
			flag = 1;
		else
			flag = 0;
		end
		
		%--------------------------------------------------------------
		% CREATE TOGGLE DATA 
		%--------------------------------------------------------------

		if flag
			
			%----------------------------------
			% SET PIXEL UNITS FOR CHILDREN
			%----------------------------------
			
			% TODO: factor this into a function
			
			%--
			% get all objects and their type
			%--
			
			obj = findall(pal);
			
			type = get(obj, 'type');
			
			%--
			% remove objects for which units is not a property
			%--
			
			% NOTE: when did we create an 'hggroup' object?
			
			types = { ...
				'uicontextmenu', ...
				'uimenu', ...
				'line', ...
				'patch', ...
				'image', ...
				'rectangle', ...
				'hggroup' ...
			};
			
			ix = [];
			
			for k = 1:length(types)
				
				kix = find(strcmp(type, types{k}));
				
				if ~isempty(kix)
					ix = [ix; kix(:)];
				end
				
			end
		
			obj(ix) = [];
			
			%--
			% set remaining object units to pixels
			%--

			set(obj, 'units', 'pixels');
			
			%----------------------------------
			% SORT TOGGLES BY POSITION
			%----------------------------------
			
			%--
			% get parent axes positions
			%--
			
			tog = findobj(obj, 'flat', 'tag', 'header_toggle');
					
			tog = setdiff(tog, findobj(tog, 'userdata', 'hidden'));
			
			% NOTE: 'post' is the position of the 'toggle' parent axes
			
			if length(tog) > 1
				par = cell2mat(get(tog, 'parent')); post = get(par, 'position');
			else
				par = get(tog, 'parent'); post = {get(par, 'position')};
			end
			
			%--
			% sort in descending vertical position
			%--
			
			for k = 1:length(post)
				tmp(k) = post{k}(2);
			end
			
			[ignore, ix] = sort(tmp);
						
			ix = fliplr(ix);
		
			tog = tog(ix);
			
			par = par(ix);
			
			post = post(ix);
			
			%----------------------------------
			% GET TOGGLE CHILDREN INFO
			%----------------------------------
			
			pos = get(obj, 'position');
			
			nt = length(tog);
			
			for k = 1:length(tog)
			
				%--
				% get position of current and next toggle if it exists
				%--
				
				if k < nt
					
					% NOTE: the 'c' and 'n' stand for 'current' and 'next'
					
% 					posc = get(par(k), 'position'); posn = get(par(k + 1), 'position');
					
					posc = post{k}; posn = post{k + 1};
					
					data.toggle(k).toggle = tog(k);
					
					data.toggle(k).parent = par(k);
					
					data.toggle(k).head = posc(4);
					
					data.toggle(k).body = (posc(2) - posn(2)) - posn(4);
					
					data.toggle(k).color = get(par(k), 'color');
					
				else
					
% 					posc = get(par(k),'position');
					
					posc = post{k}; posn = [];	
					
					data.toggle(k).toggle = tog(k);
					
					data.toggle(k).parent = par(k);
					
					data.toggle(k).head = posc(4);
					
					data.toggle(k).body = posc(2);
					
					data.toggle(k).color = get(par(k), 'color');
					
				end
			
				%--
				% select objects between current and next toggle
				%--
			
				j = 1;
				
				if k < nt

					for i = 1:length(obj)
						
						if ( ...
							(strcmp(get(obj(i),'type'),'uicontrol') || strcmp(get(obj(i),'type'),'axes')) && ...
							(pos{i}(2) < posc(2)) && ...
							(pos{i}(2) > posn(2)) ...
						)	
					
							data.toggle(k).child(j) = obj(i);
							data.toggle(k).pos{j} = pos{i};
							j = j + 1;
							
						end
						
					end
								
				else

					for i = 1:length(pos)
						
						if ( ...
							(strcmp(get(obj(i),'type'),'uicontrol') || strcmp(get(obj(i),'type'),'axes')) && ...
							(pos{i}(2) < posc(2)) ...
						)
					
							data.toggle(k).child(j) = obj(i);
							data.toggle(k).pos{j} = pos{i};
							j = j + 1;
							
						end
						
					end
									
				end
			
			end
			
			%----------------------------------
			% RELATIVE CHILDREN POSITION
			%----------------------------------
		
			for k = 1:nt
				
				for j = 1:length(data.toggle(k).child)
					
					part = 0;
					
					for i = (k + 1):nt
						part = part + (data.toggle(i).body + data.toggle(i).head);
					end
															
					data.toggle(k).pos{j}(2) = data.toggle(k).pos{j}(2) - part;
										
				end
				
			end
			
			%----------------------------------
			% STORE TOGGLE DATA
			%----------------------------------
			
			set(pal, 'userdata', data);
						
		end
		
		%----------------------------------
		% CHANGE STATE
		%----------------------------------
		
		% NOTE: part of this is state the other part is visual
		
		set(toggle, 'string', '+');
						
		off_color = (2 * COLOR_OFF + get(parent, 'color')) / 3;
		
		set(parent, ...
			'color', off_color ...
		);
	
		set_header_image(parent, off_color);
				
		set(text, ...
			'color', TEXT_OFF, ...
			'hittest', 'off' ...
		);
	
		%----------------------------------
		% MOVE OBJECTS TO PRODUCE TOGGLE
		%----------------------------------
		
		%--
		% determine which toggle
		%--
		
		ixc = find(struct_field(data.toggle, 'toggle') == toggle);
		
		%--
		% shift objects that belong to this toggle
		%--
		
		for k = 1:length(data.toggle(ixc).child)
				
			posk = get(data.toggle(ixc).child(k),'position');
			
			posk(1) = posk(1) + HOFF;

			set(data.toggle(ixc).child(k), 'position', posk);
						
		end

		%----------------------------------
		% RESIZE FIGURE
		%----------------------------------
							
		posf = get(pal, 'position');

		posf(4) = posf(4) - data.toggle(ixc).body;
		
		posf(2) = posf(2) + data.toggle(ixc).body;
		
		set(pal, 'position', posf);
		
		%----------------------------------
		% POSITION VISIBLE OBJECTS
		%----------------------------------
		
		% NOTE: this means whether they belong to an open or closed toggles
		
		%--
		% get tab states
		%--
		
		nt = length(data.toggle);
		
		for k = 1:nt
			visible(k) = strcmp(get(data.toggle(k).toggle, 'string'), '-');
		end
						
		%--
		% reposition objects
		%--
		
		for k = 1:nt
			
			%--
			% reposition toggle parents
			%--
			
			part = visible(k) * data.toggle(k).body;
			
			for j = (k + 1):nt
				part = part + data.toggle(j).head + (visible(j) * data.toggle(j).body);
			end 
						
			posk = get(data.toggle(k).parent, 'position');
			
			posk(2) = part;
			
			set(data.toggle(k).parent, 'position', posk);
						
			%--
			% reposition visible children 
			%--
			
			if visible(k)
				
				for j = 1:length(data.toggle(k).child)
					
					part = 0;
					
					for i = (k + 1):nt
						part = part + data.toggle(i).head + (visible(i) * data.toggle(i).body);
					end 
					
					posj = data.toggle(k).pos{j};
					posj(2) = posj(2) + part;
					
					set(data.toggle(k).child(j),'position',posj);
										
				end
				
			end
			
		end
		
% 		center_toggle(toggle);
		
		%----------------------------------
		% PLAY SOUND
		%----------------------------------
		
		toggle_sound('close');
		
	%----------------------------------
	% OPEN TAB
	%----------------------------------
	
	case {'+', 'max', 'open'}
		
		%----------------------------------
		% GET TOGGLE DATA
		%----------------------------------
		
		data = get(pal,'userdata');
		
		%--
		% determine which toggle
		%--
		
		ixc = find(struct_field(data.toggle,'toggle') == toggle);
		
		%----------------------------------
		% RESIZE FIGURE
		%----------------------------------
		
		posf = get(pal,'position');
				
		posf(4) = posf(4) + data.toggle(ixc).body;
		
		posf(2) = posf(2) - data.toggle(ixc).body;
		
		% NOTE: this is test code to handle palettes that are too tall for the screen
		
		screen_size = get(0,'screensize');
		
		if posf(4) >= screen_size(4)
			
			% TODO: display message, use environment variable control
			
			rgb = get(parent, 'color');
			
			set_header_image(parent, ([1 0.5 0] + 2 * rgb) / 3); 
			 
			pause(0.1);
			
			set_header_image(parent ,rgb);
	
			return;
			
		end
		
		set(pal,'position',posf);
		
		%----------------------------------
		% CHANGE STATE
		%----------------------------------
		
		set(toggle,'string','-');
		
		on_color = data.toggle(ixc).color;
		
		set_header_image(parent,on_color);
		
		set(text, ...
			'color',TEXT_ON, ...
			'hittest','off' ...
		);
		
		set(parent,'color',on_color);
		
		%----------------------------------
		% MOVE OBJECTS TO PRODUCE TOGGLE
		%----------------------------------
		
		%--
		% shift objects that belong to this toggle
		%--
		
		for k = 1:length(data.toggle(ixc).child)
			
			posk = get(data.toggle(ixc).child(k),'position');
			
			posk(1) = posk(1) - HOFF;
			
			set(data.toggle(ixc).child(k),'position',posk);
			
		end
		
		%----------------------------------
		% POSITION VISIBLE OBJECTS
		%----------------------------------

		%--
		% get tab states
		%--
		
		nt = length(data.toggle);
		
		for k = 1:nt
			visible(k) = strcmp(get(data.toggle(k).toggle, 'string'), '-');
		end
				
		%--
		% reposition objects
		%--
		
		for k = 1:nt
			
			%--
			% reposition toggle parents
			%--
			
			part = visible(k) * data.toggle(k).body;
			
			for j = (k + 1):nt
				part = part + data.toggle(j).head + (visible(j) * data.toggle(j).body);
			end
			
			posk = get(data.toggle(k).parent,'position');
			
			posk(2) = part;
			
			set(data.toggle(k).parent,'position',posk);
						
			%--
			% reposition visible children
			%--
			
			if visible(k)
				
				for j = 1:length(data.toggle(k).child)
					
					part = 0;
					
					for i = (k + 1):nt
						part = part + data.toggle(i).head + (visible(i) * data.toggle(i).body);
					end 
									
					posj = data.toggle(k).pos{j};
					
					posj(2) = posj(2) + part;
					
					set(data.toggle(k).child(j),'position',posj);
				
										
				end
				
			end
			
		end
		
% 		center_toggle(toggle);
		
		%----------------------------------
		% PLAY SOUND
		%----------------------------------
		
		toggle_sound('open');
				
end

%----------------------------------
% CENTER_TOGGLE
%----------------------------------

function center_toggle(tog)

set(get(tog, 'parent'), 'ylim', [0, 1]);

set(tog, ...
	'fontname', 'courier', ...
	'units', 'normalized', ...
	'verticalalignment', 'middle' ...
);

pos = get(tog, 'position'); pos(2) = 0.5;

set(tog, ...
	'position', pos, ... 
	'units', 'pixels' ... 
);





