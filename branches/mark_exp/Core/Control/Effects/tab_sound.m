function tab_sound

% tab_sound - tab selection sound
% -------------------------------

%--
% return if palette sounds are not on
%--

if ~get_user_preference(get_active_user, 'palette_sounds')
	return;
end

%--
% create tab selection sound
%--

% TODO: make this configurable and load from file

n = 50;

X = 0.015 * (sort(randn(1, n)) + 0.1 * randn(1, n));

%--
% play tab selection sound
%--

% NOTE: blocking play avoids multiple simultaneous play requests

if ispc
    wavplay(X, 'sync');
else
    sound(X);
end
