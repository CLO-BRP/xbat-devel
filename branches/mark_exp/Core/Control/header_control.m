function control = header_control(label, collapse, varargin)

% header_control - create header control
% --------------------------------------
%
% control = header_control(label, collapse, varargin)
%
% Input:
% ------
%  label - label
%  collapse - indicator
% 
% Output:
% -------
%  control - control

%--
% set default collapse behavior
%--

if nargin < 2 || isempty(collapse)
	collapse = 1;
end

%--
% create control
%--

control = control_create( ...
	'string', label, ...
	'style', 'separator', ... 
	'type', 'header', ...
	varargin{:} ...
);

% NOTE: declaring a 'min' of 1 (the 'max' is also 1) means we don't collapse, we have no range

if ~collapse
	control.min = 1;
end
