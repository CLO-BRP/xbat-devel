function label = get_label(control, opt)

% get_label - compute control label
% ---------------------------------
%
% label = get_label(control, opt)
%
% Input:
% ------
%  control - control
%  opt - palette options 
%
% Output:
% -------
%  label - control label

%--
% typically label is either name or alias
%--

if isempty(control.alias)
	label = control.name;
else
	label = control.alias;
end

% NOTE: return if there is no label fun

if isempty(opt.label_fun)
	return;
end

%--
% perform locale replacement if needed
%--

% 	if (opt.get_text)
% 		label = get_text(label);
% 	end

%--
% prepare label for display if needed
%--

% NOTE: the default label function is 'title_caps'

switch control.style

	%--
	% separators and tabs are special cases
	%--

	case 'separator'

		control.string = opt.label_fun(control.string);

	case 'tabs'

		% NOTE: store the modified tab names in tab 'alias', the function handles cell arrays

		if isempty(control.alias)
			control.alias = opt.label_fun(control.tab);
		end

	%--
	% typical controls are labelled with label
	%--

	otherwise

		label = opt.label_fun(label);

end
