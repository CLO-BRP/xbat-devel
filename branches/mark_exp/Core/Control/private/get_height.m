function height = get_height(control)

% get_height - compute control height in tile units
% -------------------------------------------------
%
% height = get_height(control)
%
% Input:
% ------
%  control - control
%
% Output:
% -------
%  height - height in tile units

switch control.layout
	
	case 'compact'
		height = control.lines + control.space;
		
	otherwise
		height = control.label + control.lines + control.space;

end
