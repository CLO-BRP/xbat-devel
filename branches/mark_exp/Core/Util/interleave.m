function X = interleave(varargin)

for k = 1:numel(varargin)
	X(:, k) = varargin{k}(:);
end

X = vec(X');