function name = simple_computer

name = lower(computer); name = name(1:3);

switch name
	
	case 'pcw', name = 'windows';
		
	case {'mac', 'sol'}, name = 'unix';
		
	case 'gln', name = 'linux';
		
end