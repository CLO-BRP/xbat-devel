function installed = install_fast_md5

% install_fast_md5 - java jar
% ---------------------------
%
% installed = install_fast_md5
%
% Output:
% -------
%  installed - indicator

% NOTE: this is not a tool we download, it is small and GPL, we have included it, we just need to make sure it is in the classpath

jar = which('fast-md5.jar');

if isempty(jar)
	installed = 0; return;
end

if ~jar_in_classpath(jar)
	append_classpath(jar);
end

installed = 1;