function [handle, created] = create_line(par, tag, varargin)

% create_line - singleton determined by tag
% -----------------------------------------
%
% [handle, created] = create_line(par, tag, varargin)
%
% Input:
% ------
%  par - handle
%  tag - identifier
%  varargin - field and value pairs
%
% Output:
% -------
%  handle - to line
%  created - indicator

[handle, created] = create_obj('line', par, tag, varargin{:});