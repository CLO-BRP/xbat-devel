function tags = get_handle_tags(handle)

% NOTE: this function returns a cell array of strings not a tag string like the 'get'

if numel(handle) > 1
	tags = iterate(mfilename, handle); return;
end 

tags = str_split(get(handle, 'tag'), ',');