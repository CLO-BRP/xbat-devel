function handle = find_handles_with_tag(par, tag, varargin)

%--
% find candidate handles using input
%--

% NOTE: we accept a collection of candidates or a parent as handle input

if numel(par) > 1
	candidate = par;
else
	candidate = findobj(par, varargin{:});
end

%--
% get tag strings and search for tag
%--

tags = get(candidate, 'tag');

if ischar(tags)
	tags = {tags};
end

handle = zeros(size(candidate)); count = 0;

for k = 1:numel(tags)
	
	% NOTE: first a quick check then the complete check
	
	% NOTE: the second clause is not DRY with respect to 'get_handle_tags'
	
	if isempty(strfind(tags{k}, tag)) && ~string_is_member(tag, str_split(tags{k}, ','))
		continue;
	end 

	count = count + 1; handle(count) = candidate(k);
	
end

if count
	handle = handle(1:count);
else
	handle = [];
end
