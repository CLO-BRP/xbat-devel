function set_handle_tags(handle, tags)

if iscell(tags)
	tags = str_implode(tags, ', ');
end

set(handle, 'tag', tags);
