function [out,fail] = mat_load(in,varargin)

% mat_load - load variables from mat file
% ---------------------------------------
%
% [out,fail] = mat_load(in,'var_1', ... ,'var_n')
%
% Input:
% ------
%  in - file location
%  var_k - variable name description
%
% Output:
% -------
%  out - variable structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2196 $
% $Date: 2005-12-02 18:16:46 -0500 (Fri, 02 Dec 2005) $
%--------------------------------

% NOTE: we use this function to ignore file name and to wrap load and catch warnings

%--
% simple load
%--

names = varargin;

if (length(names) < 1)
	
	out = load('-mat',in);
	
%--
% load variables
%--

else
	
	%--
	% inspect for requested variables
	%--
	
	var = mat_inspect(in,names{:});
		
	%--
	% check for load failure and remove missing variables from load
	%--
	
	if (isempty(var))
		out = []; return;
	end
		
	found = names;
	
	for k = length(found):-1:1
		if (isempty(var.(names{k})))
			found(k) = [];
		end
	end
	
	fail = setdiff(names,found);
	
	%--
	% load found variables
	%--
	
	out = load('-mat',in,found{:});
	
end