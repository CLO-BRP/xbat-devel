function [root, content] = get_dir(key, note)

% get_dir - directory using dialog
% --------------------------------
%
% [root, content] = get_dir(key, note)
%
% Input:
% ------
%  key - to last location
%  note - to user
% 
% Output:
% -------
%  root - directory
%  content - of directory

%--
% handle input
%--

if nargin < 2
	note = 'Select folder';
end

if ~nargin || trivial(key)
	key = '';
end

%--
% get starting directory through key
%--

% NOTE: this creates a namespace for this function

key = ['get_dir::', key];

start = key_value(key);

if isempty(start)
	start = pwd;
end

%--
% present directory dialog
%--

root = uigetdir(start, note);

%--
% output result and update start location for key
%--

if isequal(root, 0)
	content = []; return;
end

if nargout > 1
	content = dir(root);
end

key_value(key, root);





