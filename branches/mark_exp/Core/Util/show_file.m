function in = show_file(in)

% show_file - show file
% ---------------------
%
% out = show_file(in)
%
% Input:
% ------
%  in - file or directory path to show
%
% Output:
% -------
%  out - file shown, empty is input does not exist

%--
% check input
%--

if ~ischar(in)
	error('File to show must be a string.');
end

% NOTE: return empty if we did not find something to show

if ~exist(in, 'file') && ~exist(in, 'dir')
	in = ''; return;
end

%--
% show file
%--

switch computer
	
	case 'PCWIN'
		
		% NOTE: we use the system call display for files so we can select
		
        if isdir(in)
			winopen(in); % eval(['!explorer /n,', path_parts(f), ' &']);
		else
			eval(['!explorer /n,/select,', in, ' &']);
        end
        
    case 'GLNX86'
 
        if ~isdir(in)
            in = fileparts(in);
        end
        
        commands = {'nautilus'};
                     
        for k = 1:length(commands) 
            
            [status, result] = system([commands{k}, ' ', in]); 
            
            if ~status
                break;
            end
              
        end    
		
end