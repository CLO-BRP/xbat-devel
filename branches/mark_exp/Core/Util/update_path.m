function dirs = update_path(type, root, rec)

% update_path - update path with tree or path
% -------------------------------------------
%
% dirs = update_path('append', root, rec)
%
%      = update_path('remove', root, rec)
%
% Input:
% ------
%  root - tree root (def: pwd)
%  rec - follow tree (def: 1)
%
% Output:
% -------
%  dirs - directories appended or removed from path

%--
% handle input
%--

% NOTE: default recursive append

if nargin < 3
	rec = 1;
end

% NOTE: default current directory starting point

if (nargin < 2) || isempty(root) 
	root = pwd;
end

% NOTE: check for proper type

if ~ismember(type, {'append', 'remove'})
	error('Path update type must be ''append'' or ''remove''.');
end

%--
% collect and filter directories to append
%--

if rec
	dirs = scan_dir(root);
else
	dirs = {root};
end

% NOTE: we filter paths that are 'private'

for k = length(dirs):-1:1
	
	[ignore, leaf] = fileparts(dirs{k});
	
	if strcmp(leaf, 'private')
		dirs(k) = []; continue;
	end 
	
	if strfind(dirs{k}, [filesep, 'private', filesep])
		dirs(k) = [];
	end
		
end

%--
% update path based on type
%--

switch type
	
	case 'append', addpath(dirs{:}, '-end');

	case 'remove', rmpath(dirs{:});
		
end