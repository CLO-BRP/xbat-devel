function str = get_string_from_java(obj)

if ~isjava(obj)
	error('Input must be java object.');
end

% if ~string_is_member('toString', methods(obj))
% 	error('Input object does not support ''toString'' conversion.');
% end

str = char(obj.toString());