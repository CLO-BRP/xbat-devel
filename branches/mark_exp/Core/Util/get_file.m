function [file, path] = get_file(key, varargin)

% get_file - using dialog
% -----------------------
%
% [file, path] = get_file(key, varargin)
%
% Input:
% ------
%  key - to last location
%  varargin - for 'uigetfile'
%
% Output:
% -------
%  root - parent directory
%  file - name

%--
% handle input
%--

if ~nargin || trivial(key)
	key = '';
end

%--
% get starting directory through key
%--

% NOTE: this creates a namespace for this function

key = ['get_file::', key];

start = key_value(key);

if isempty(start) || ~ischar(start)
	start = pwd;
end

%--
% present file dialog
%--

current = pwd; cd(start);

try
	[file, path] = uigetfile(varargin{:});
	
	if file
		key_value(key, path);
	end
catch
	cd(current); rethrow(lasterror);
end

cd(current);
