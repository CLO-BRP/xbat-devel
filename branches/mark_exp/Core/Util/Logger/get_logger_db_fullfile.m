function log_db_fullfile = get_logger_db_fullfile( logname )

log_db_fullfile = fullfile(fileparts(mfilename('fullpath')), 'logs', [logname, '.sqlite3']) ;