function level = logger_levels(level)

% FATAL     an unhandleable error that results in a program crash
% ERROR 	a handleable error condition 
% WARN      Use of deprecated APIs, poor use of API, 'almost' errors, other runtime situations that are undesirable or unexpected, but not necessarily "wrong". Expect these to be immediately visible on a status console.
% INFO      generic (useful) information about system operation 
% DEBUG     low-level information for developers 

levels = {'fatal', 'error', 'warn', 'info', 'debug'};

if ~nargin
    level = levels;
	return;
end

%--
% match and normalize level name
%--

match = find(iterate(@strcmpi, levels, level));

if isempty(match)
	level = '';
else
	level = levels{match(1)};
end