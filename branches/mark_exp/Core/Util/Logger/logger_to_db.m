function status = logger_to_db(logname, level, message)

db = get_logger_db(logname);

% filter out characters which will make the SQLite barf
if string_contains(message, '''')
    message = regexprep(message, '''', '"');
    message = regexprep(message, '*', '|');
end

sql = log_insert_sql(level, message);

try
    status = sqlite(db, sql);
    sql = log_insert_sql(level, message);
catch
    nice_catch
end
