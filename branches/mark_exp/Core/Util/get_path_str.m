function str = get_path_str(root, mode)

% get_path_str - add directories to path
% ----------------------------------
%
% str = get_path_str(root, 'flat')
%     = get_path_str(root, 'rec')
%
% Input:
% ------
%  root - initial directory
%
% Output:
% -------
%  str - string appended to path

%--
% set default mode
%--

if nargin < 2
	mode = 'flat'; 
end 

% NOTE: we have some hard-coded directory names to skip

EXCLUDE_DIRS = { ...
	'private', ...
	'CVS' ...
};

%--
% build path string
%--

% NOTE: you can add to the path calling 'addpath(path, str)'

out = what_ext(root);
		
str = out.path;

for k = 1:length(out.dir)

	%--
	% skip dot, method, and excluded directories
	%--

	if (out.dir{k}(1) == '.') || (out.dir{k}(1) == '@')
		continue;
	end

	if ~isempty(find(strcmp(out.dir{k}, EXCLUDE_DIRS), 1))
		continue;
	end

	%--
	% handle add mode
	%--
	
	switch mode

		case 'flat'
			part = [root, filesep, out.dir{k}];

		case 'rec'
			part = get_path_str([root, filesep, out.dir{k}], 'rec');

	end
	
	%--
	% append partial path string to path string
	%--
	
	str = [str, pathsep, part];

end