function parenthetical_menus(handle, labels)

% parenthetical_menus - disable and separate parenthetical menus
% --------------------------------------------------------------
%
% parenthetical_menus(handle, labels)
%
% Input:
% ------
%  handle - to menus
%  labels - declared parenthetical, along with those that are labelled such

%--
% set set of parenthetical labels to empty
%--

% NOTE: this label approach is of limited use because frequently menus do not have unique labels

if nargin < 2
	labels = {};
end 

%--
% check through collection of handles for parenthetical menus
%--

for k = 1:numel(handle)
	
	%--
	% get current handle to consider and label
	%--
	
	current = handle(k); label = get(current, 'label'); 
	
	%--
	% check label is actually or declared as parenthetical
	%--
	
	if label(1) == '(' || (~isempty(labels) && string_is_member(label, labels))
		
		set(current, 'enable', 'off');
		
		position = get(current, 'position');
		
		% NOTE: we set a separator whenever we are parenthetical, except at the top
		
		% TODO: consider making this an option
		
		if position > 1
			set(current, 'separator', 'on');
		end
		
	end

end