function content = get_extension_content(source, ext, pat, rec)

% get_extension_content - get directory contents by extension
% -----------------------------------------------------------
%
% content = get_extension_content(source, ext, pat, rec)
%
% Input:
% ------
%  source - directory
%  ext - extensions to scan for
%  pat - further pattern to match for name (def: '')
%  rec - recursive search (def: 0)
%
% Output:
% -------
%  content - directory contents

%--
% handle input
%--

if nargin < 4
	rec = 0;
end

if nargin < 3 
	pat = '';
end

%--
% initialize content and record initial directory
%--

init = pwd; content = [];

try
	
	%--
	% move to source directory
	%--
	
	if rec
		sources = scan_dir(source);
	else
		sources = {source};
	end 
	
	for j = 1:numel(sources)

		%--
		% loop over extensions
		%--

		% NOTE: pack single extension into cell if needed

		if ischar(ext)
			ext = {ext};
		end 

		for k = 1:length(ext)

			%--
			% get extension content
			%--
			
			ext_content = dir([sources{j}, filesep, strrep([pat, ext{k}], '**', '*')]);

			if isempty(ext_content)
				continue;
			end

			relative_source = [strrep(sources{j}, source, ''), filesep]; % relative_source(1) = [];
			
			if numel(relative_source) > 1
				
				for l = 1:numel(ext_content)
					ext_content(l).name = strcat(relative_source, ext_content(l).name);
				end
				
			end
			
			for l = 1:numel(ext_content)
				
				if ext_content(l).name(1) == filesep
					ext_content(l).name(1) = [];
				end
				
			end
			
			%--
			% append extension content
			%--

			if ~isempty(content)
				content = [content; ext_content];
			else
				content = ext_content;
			end

		end
	
	end
	
catch
	
	content = [];

end

%--
% return to initial directory 
%--

cd(init);
