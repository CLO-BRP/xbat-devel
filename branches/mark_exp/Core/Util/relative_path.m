function relative = relative_path(file, source)

% relative_path - computation for file based on source
% ----------------------------------------------------
%
% relative = relative_path(file, source)
%
% Input:
% ------
%  file - we want to address
%  source - location of reference
%
% Output:
% -------
%  relative - path string

prefix = string_prefix(file, source);

