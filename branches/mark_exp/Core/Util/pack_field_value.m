function out = pack_field_value(field, value, callback)

% pack_field_value - pack fields and values as struct
% ---------------------------------------------------
%
% out = pack_field_value(field, value)
%
% out = pack_field_value(args)
%
% Input:
% ------
%  field - fields
%  value - values
%  args - alternating fields and values
%
% Output:
% -------
%  out - packed fields and values

% TODO: make use of the builtin function 'cell2struct' when appropiate

%--
% handle input
%--

if nargin < 3
	callback = struct;
end

if nargin < 2 || isempty(value)
	
	[field, value] = get_field_value(field);
	
	if ~iscellstr(field)
		error('Contents of odd indexed cells must be strings.');
	end
	
else
	
	if ~iscellstr(field)
		error('Field name input must be a string cell array.');
	end

end

if numel(field) ~= numel(value)
	error('Field and value arrays must have matching elements.');
end 

%--
% apply field and value callbacks if available
%--

% NOTE: '@genvarname' is typically a useful field callback

% NOTE: using 'genvarname' directly, outside of iteration will also ensure that fields are unique, we don't want this

if ~trivial(callback)
	
	if isfield(callback, 'field')
		field = iteraten(@eval_callback, 2, callback.field, field);
	end 
	
	if isfield(callback, 'value')
		value = iteraten(@eval_callback, 2, callback.value, value);
	end

end

%--
% pack fields and values
%--

out = struct;

if iscell(value)
	
	for k = 1:numel(field)
		out.(field{k}) = value{k};
	end
	
else
	
	for k = 1:numel(field)
		out.(field{k}) = value(k);
	end
	
end
