function hlp(fun)

% hlp - better help
% -----------------
%
% hlp fun
%
% Input:
% ------
%  fun - function name

%--
% clear screen and set more on
%--

clc; more on;

%--
% display help
%--

try
	
	disp(' ');

	if ~nargin || isempty(fun)
		help;
	else
		help(fun);
	end

	disp(' ');
	
catch 
	
	more off;

end

%--
% reset more
%--

more off;
