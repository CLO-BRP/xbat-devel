function dirs = get_dirs(root, pat, fun)

% get_dirs - get directories starting from root matching a pattern
% ----------------------------------------------------------------
%
% dirs = get_dirs(root, pat)
%
% Input:
% ------
%  root - scan root
%  pat - name pattern
%  fun - pattern matching function (def: @strcmpi)
%
% Output:
% -------
%  dirs - matching dirs

%--
% handle input
%--

if nargin < 3
	fun = @strcmpi;
end

% NOTE: no pattern means select all

if (nargin < 2)
	pat = '';
end

if ~nargin || isempty(root)
	root = pwd;
end

%--
% scan and select on match
%--

if isempty(pat)
	dirs = scan_dir(root);
else
	dirs = scan_dir(root, {@match, pat, fun});
end


%--------------------
% MATCH
%--------------------

function file = match(file, pat, fun)

%--
% get directory name
%--

[ignore, name] = fileparts(file);

%--
% match with pattern using fun
%--

if ~fun(name, pat)
	file = [];
end