function out = cell_to_path(in)

% cell_to_path - concatenate cell strings into path string
% --------------------------------------------------------
%
% out = cell_to_path(in)
%
% Input:
% ------
%  in - input cell string
%
% Output:
% -------
%  out - path string

if ~iscellstr(in)
	error('Input must be cell array of strings.');
end

out = str_implode(in, pathsep);
