function value = set_env(name, value, verb)

% set_env - set environment variable
% ----------------------------------
%
% value = set_env(name, value, verb)
%
% Input:
% ------
%  name - variable name
%  value - variable value
%  verb - verbosity flag (def: 0)
%
% Output:
% -------
%  value - value set

% TODO: update the environment variable framework to use application data

% TODO: update framework to allow saving to and loading from file, for cross-session persistence

%--
% set display verb
%--

if (nargin < 3) || isempty(verb)
	verb = 0;
end

%--
% get root userdata
%--

data = get(0, 'userdata');

%--
% check for existing environment variable structure
%--

if isfield(data, 'env')
	
	if isfield(data.env, name)
		
		%--
		% update value of existing environment variable
		%--
		
		data.env.(name) = value;
		
	else
		
		%--
		% create new environment variable
		%--
		
		data.env.(name) = value;
		
		%--
		% report creation of new environment variable
		%--
		
		if verb
			disp(['Environment variable ''', name, ''' created.']);
		end
		
	end
	
else
	
	%--
	% create new environment variable
	%--
		
	data.env.(name) = value;
	
	%--
	% report creation of environment variable structure and new environment variable
	%--
	
	if verb
		disp(['Environment variable ''', name, ''' created.']);
	end
	
end

%--
% update root userdata
%--

set(0, 'userdata', data);
