function value = in_range(value, low, high)

% in_range - test whether value is in range
% -----------------------------------------
%
% result = in_range(value, low, high)
%
% Input:
% ------
%  value - to test
%  low - boundary
%  high - boundary
%
% Output:
% -------
%  result - test of range 

%--
% set default range to unit interval
%--

if nargin < 3
	high = 1; 
end

if nargin < 2
	low = 0;
end 

%--
% perform test
%--

value = (value >= low) & (value <= high);