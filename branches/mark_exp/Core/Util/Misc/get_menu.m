function [h, par] = get_menu(g, label, n)

% get_menu - get menu handle using label
% --------------------------------------
%
% h = get_menu(g, label, n)
%
% Input:
% ------
%  g - menu or parent hadles
%  label - menu label string
%  n - single or multiple handle option (def: 1)
%
% Output:
% -------
%  h - menu handle

%--
% set option
%--

if nargin < 3
	n = 1;
end

%--
% find handles
%--

h = findobj(g, 'label', label, 'type', 'uimenu');

%--
% return first handle found
%--

% NOTE: consider warning in the case of non-unique handles

if ~isempty(h) && (n == 1)
	h = h(1);
end

if nargout > 1
	par = get(h, 'parent');
end

	