function value = get_env(name, flag)

% get_env - get value of environment variable
% -------------------------------------------
%
% value = get_env(name, flag)
%
% Input:
% ------
%  name - variable name
%  flag - display flag (def: 0)
%
% Output:
% -------
%  value - variable value

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6971 $
% $Date: 2006-10-09 15:34:23 -0400 (Mon, 09 Oct 2006) $
%--------------------------------

% TODO: update the environment variable framework to use application data

%--
% set display flag
%--

if (nargin < 2) || isempty(flag)
	flag = 0;
end

%--
% get root userdata
%--

data = get(0, 'userdata');

%--
% return empty if we can't find environment variable
%--

if ~isfield(data, 'env')

	value = [];

	if flag
		disp('There is no environment variable structure available.');
	end

	return;
	
end

%--
% display named variable or existing environment variables
%--

if nargin && ~isempty(name)

	%--
	% return empty if we can't find named environment variable
	%--

	if ~isfield(data.env, name)
		
		value = [];

		if flag
			disp(['Environment variable ''' name ''' is not currently defined.']);
		end
		
		return;
		
	end
	
	%--
	% output value of environment variable
	%--

	value = data.env.(name);

else

	%--
	% output full contents of environment variable structure
	%--

	value = data.env;

end
