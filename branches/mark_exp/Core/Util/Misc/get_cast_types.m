function [types, fun] = get_cast_types

% get_cast_types - and functions available for casting
% ----------------------------------------------------
%
% [types, fun] = get_cast_types
%
% Output:
% -------
%  types - available for casting
%  fun - cast functions

%--
% create persistent table of cast types
%--

persistent PERSISTENT_CAST_TYPES PERSISTENT_CAST_HANDLES;

if isempty(PERSISTENT_CAST_TYPES)

	% NOTE: these are ordered from 'less' to 'more' numerical
	
	PERSISTENT_CAST_TYPES = { ...
		'char', ...
		'logical', ...
		'uint8', 'uint16', 'uint32', 'uint64', ...
		'int8', 'int16', 'int32', 'int64', ...
		'single', 'double' ...
	};

	%--
	% get cast functions from names
	%--

	for k = 1:length(PERSISTENT_CAST_TYPES)
		PERSISTENT_CAST_HANDLES{k} = eval(['@',PERSISTENT_CAST_TYPES{k}]);
	end
	
end

%--
% output types and function lists
%--

types = PERSISTENT_CAST_TYPES;

if nargout > 1
	fun = PERSISTENT_CAST_HANDLES;
end

