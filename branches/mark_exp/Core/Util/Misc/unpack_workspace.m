function unpack_workspace(out)

% unpack_workspace - unpack workspace structure into workspace
% ------------------------------------------------------------
%
% unpack_workspace(out)
%
% Input:
% ------
%  out - workspace structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% TODO: use a variable name filter in the unpacking

% TODO: use a class filter for the unpacking

% NOTE: consider implementing the above in 'struct_extract'

%--
% return empty struct on empty workspace
%--

if isempty(out)
	return;
end

struct_extract(out);
