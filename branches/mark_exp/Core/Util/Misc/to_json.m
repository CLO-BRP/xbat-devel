function str = to_json(in)

% to_json - get json representation of struct
% -------------------------------------------
%
% str = to_json(in)
%
% Input:
% ------
%  in - struct
%  flat - indicator
% 
% Output:
% -------
%  str - json representation

%--
% check for struct
%--

if ~isstruct(in) 
	error('Struct input is required.');
end

%--
% handle struct array
%--

if numel(in) > 1
	str = ['[', str_implode(iterate(@to_json, in), ', '), ']']; return;
end

%--
% convert struct to json
%--

field = fieldnames(in); part = cell(0);

for k = 1:numel(field)
	part{k} = [field{k}, ': ', value_to_json(in.(field{k}))];
end

str = ['{', str_implode(part, ', '), '}'];


%---------------------------
% VALUE_TO_JSON
%---------------------------

function str = value_to_json(value)
	
switch class(value)

	case 'cell'
		str = ['[', str_implode(value, ', ', @value_to_json), ']'];

	case 'char'
		str = ['"', value, '"'];

	case 'struct'
		str = to_json(value);

	% TODO: the representation of matrices is still not adequate in js
	
	% TODO: add logical scalar output as 'true' and 'false'
	
	otherwise
		if isnumeric(value) && isempty(value)
			str = 'null';
		else
			str = mat2str(value, 20);
		end
		
end
