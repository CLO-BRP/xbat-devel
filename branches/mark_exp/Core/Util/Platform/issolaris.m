function value = issolaris

value = ~isempty(findstr(computer, 'SOL'));