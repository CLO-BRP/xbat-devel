function interval = interval_union(interval)

% interval_union - find the union of a collection of intervals
% --------------------------------------------------------
%
% interval = interval_union(interval)
%
% Input:
% ------
%  interval - interval struct
% 
% Output:
% -------
%  interval - modified intervals


[edges, ix] = sort([interval.start; interval.stop]);

labels = [ones(size(interval.start)); -ones(size(interval.stop))];

labels = labels(ix);

%--
% get new block starts and stops
%--

interval.start = edges(diff([0; cumsum(labels) > 0]) > 0);

interval.stop = edges(diff([0; cumsum(labels) > 0]) < 0);
