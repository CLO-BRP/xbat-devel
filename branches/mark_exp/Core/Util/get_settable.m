function prop = get_settable(obj)

% get_settable - get settable properties and values for object
% ------------------------------------------------------------
%
% prop = get_settable(obj)
%
% Input:
% ------
%  obj - object handle
%
% Output:
% -------
%  prop - settable properties struct

%--
% get settable fields and corresponding values and pack into struct
%--

fields = fieldnames(set(obj)); values = get(obj,fields);

prop = cell2struct(values(:), fields(:));

%--
% remove fields we really can't set properly
%--

% NOTE: these were discovered using figure to figure out settable

improper = {'Children', 'CurrentCharacter'};

prop = remove_fields(prop, improper);
