function lines = file_readlines(in, fun, opt)

% file_readlines - load and possibly process lines of file
% --------------------------------------------------------
%
% lines = file_readlines(in, fun, opt)
%
%   opt = file_readlines
%
% Input:
% ------
%  in - input file or file identifier
%  fun - fun to process each line
%  opt - options
%
% Output:
% -------
%  lines - file lines
%  opt - options

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% set default options and output options if needed
%--

if (nargin < 3) || isempty(opt)
	
	opt = file_process;
	
	if ~nargin
		lines = opt; return;
	end
	
end

%--
% set no fun default
%--

if nargin < 2
	fun = [];
end

%--------------------------------------
% READ FILE LINES
%--------------------------------------

% NOTE: setting empty output indicates read to buffer behavior

lines = file_process([], in, fun, opt); 
