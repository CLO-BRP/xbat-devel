function name = dir_name(location)

% dir_name - get name of leaf of path string
% ------------------------------------------
%
% name = dir_name(location)
%
% Input:
% ------
%  location - directory
%
% Output:
% -------
%  name - of leaf 

if iscell(location)
	name = iterate(mfilename, location); return;
end

if isempty(location)
	name = ''; return;
end

if location(end) == filesep
	location(end) = '';
end

[ignore, name] = fileparts(location);