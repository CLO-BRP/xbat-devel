function out = filtered_file_dialog(root, type, scan, multiple)

% filtered_file_dialog - to select a file 
% ---------------------------------------
%
% out = filtered_file_dialog(root, type, scan, multiple)
%
% Input:
% ------
%  root - of listing
%  type - of file
%  scan - root recursively (def: false)
%  multiple - selection allowed (def: true)
% 
% Output:
% -------
%  out - from dialog

%--
% handle input
%--

if nargin < 4
	multiple = true;
end

if nargin < 3
	scan = true;
end

if nargin < 2
	type = '.m';
end

if ~nargin
	root = pwd;
end

%--
% create controls and display dialog
%--

control = empty(control_create);

control(end + 1) = header_control('Files', false);

control(end + 1) = edit_control({'query', 'filter'}, '', 'onload', 1);

control(end + 1) = control_create( ...
	'name', 'files', ...
	'style', 'listbox', ...
	'string', {}, ...
	'lines', 10, ...
	'value', 1, ...
	'max', 1 + multiple ...
);

opt = dialog_group; opt.text_menu = true;

out = dialog_group('Select file', control, opt, {@callback, root, type, scan});


% NOTE: this is where the magic actually happens

%-----------------------
% CALLBACK
%-----------------------

function callback(obj, eventdata, root, type, scan) %#ok<INUSL>

callback = get_callback_context(obj, 'pack'); pal = callback.pal;

switch callback.control.name
	
	case 'query'
		
		query = get_control_value(pal.handle, 'query');
		
		files = get_files(root, type, scan, query);
		
		set_control(pal.handle, 'files', 'label', ['Files (', int2str(numel(files)), ')']);
		
		set_control(pal.handle, 'files', 'string', files);
		
	case 'files'
		
end


%-----------------------
% GET_FILES
%-----------------------

function [files, content] = get_files(root, type, scan, query)

% TODO: consider a cache mechanism, the base list should not change

%--
% get filtered file content
%--

content = filtered_dir(root, {@filter, type, query}, scan);

%--
% build file name strings for interface
%--

% TODO: use the root somewhere in the interface so that we can know where we are

files = strcat({content.path}, filesep, {content.name});

files = strrep(files, root, '');

% NOTE: this removes a leading filesep, an annoyance

for k = 1:numel(files)
	files{k}(1) = [];
end


function select = filter(content, type, query)

% NOTE: this filter is vectorized

%--
% name based filters
%--

names = {content.name};

% NOTE: we initialize to select all

select = true(size(names));

if ~isempty(type)
	select = string_ends(names, type, @strcmpi);
end

if ~isempty(query)
	select = select & string_contains(names, query);
end

