function out = flatten(varargin)

% flatten - flatten struct
% ------------------------
%
% out = flatten(in, n, sep)
%
% Input:
% ------
%  in - scalar struct
%  n - number of levels to flatten (def: -1, flatten all)
%  sep - field separator (def: '__', double underscore)
%
% Output:
% -------
%  out - flat struct

% TODO: extend to struct arrays, this is a non-trivial extension

% TODO: stop recursive flattening if we hit 'namelengthmax'

out = flatten_struct(varargin{:});
