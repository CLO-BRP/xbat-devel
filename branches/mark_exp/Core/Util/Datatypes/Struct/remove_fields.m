function in = remove_fields(in, field)

% remove_fields - remove fields from struct
% -----------------------------------------
%
% out = remove_fields(in, field)
%
% Input:
% ------
%  in - input struct
%  field - names to remove
%
% Output:
% -------
%  out - updated struct

%--
% put string field in cell
%--

if ischar(field)
	field = {field};
end

%--
% determine fields available for removal and remove
%--

for k = numel(field):-1:1
	if ~isfield(in, field{k}), field(k) = []; end
end

% NOTE: if there are no remaining fields to remove we have nothing to do

if isempty(field)
	return; 
end

in = rmfield(in, field);
