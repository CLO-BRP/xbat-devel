function cellstr_check(str)

% NOTE: this triggers a recursive search on nested cell arrays, the leaves must be string cell arrays

if iscellcell(str)
	
	for k = 1:numel(str)
		cellstr_check(str{k}); 
	end

	return;
	
end

if ~iscellstr(str)
	error('Input must be a cell array of strings.')
end