function str = pluralize(str)

cellstr_check(str);

str = iterate(mfilename, str);
