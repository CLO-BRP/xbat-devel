function str = quote(str, type)

% NOTE: it is possible that there are internal double quotes

str = ['"', str, '"'];