function buf = buffer_add(buf, content)

% buffer_add - add entry to buffer
% --------------------------------
%
% buf = buffer_add(buf, content)
%
% Input:
% ------
%  buf - buffer
%  content - entry content
%
% Output:
% -------
%  buf - buffer

% update index and store content

buf.index = max(1, mod(buf.index + 1, buf.length + 1));

buf.content{buf.index} = content;

buf.updated(buf.index) = now;