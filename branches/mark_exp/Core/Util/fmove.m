function [moved, file] = fmove(name, target)

% fmove - move a file to be with another file
% -------------------------------------------
%
% fmove name target
%
% [moved, file] = fmove(name, target)
%
% Input:
% ------
%  name - function or file to move
%  target - function or file at target location
%
% Output:
% -------
%  moved - indicator
%  file - result

moved = 0;

%--
% get full source location and desired parent
%--

% NOTE: this is the file that the name maps to, we compute here and at the end

file = which(name);

if isempty(file)
	return;
end

par = fcd(target);

if isempty(par)
	return;
end

%--
% try to move source file to target location
%--

[ignore, name, ext] = fileparts(file);

result = [par, filesep, name, ext];

moved = movefile(file, result);

file = which(name);

%--
% display file for no output
%--

if ~nargout
	clear moved; disp(file);
end

