function plot_SPD_ax3(stdplusdiag_tag, stdplusdiag_name, corr_functions, cf_array_prs, max_lag, Fs);

%%
%% 'plot_SPD_ax3'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create 3rd axis of standard plus diagnostic figure
%%
%% syntax:
%% -------
%% plot_SPD_ax3(stdplusdiag_tag, stdplusdiag_name, corr_functions, cf_array_prs, max_lag, Fs);
%%
%% input:
%% ------
%% internaldiag_tag == figure tag
%% internaldiag_name == figure name
%% corr_functions == cross correlation functions
%% cf_array_prs == channel pairs used for the correlation functions
%% max_lag = max allowed lag for the correlation functions
%% Fs == data sampling rate
%%
%% output:
%% -------
%%


[figh_spd, axh] = create_stdplusdiag_fig(stdplusdiag_tag, stdplusdiag_name);

axh = axh(3);
set(axh, 'visible', 'on');


%% plot raw cross correlation functions
tmp_tag = get(axh, 'tag');
axes(axh);
strips(corr_functions);
set(axh, 'tag', tmp_tag);

th(1) = get(axh, 'title');
set(th(1), 'string', sprintf('N-1 XCorr Fun (Raw)'));

for inx = 1:size(cf_array_prs, 1)
  my_y_label{inx} = [num2str(cf_array_prs(inx, 1)), 'x', num2str(cf_array_prs(inx, 2))];
end
my_y_label = fliplr(my_y_label);
set(axh, 'yticklabelmode', 'manual', 'yticklabel', my_y_label);

th(2) = get(axh, 'ylabel');
set(th(2), 'string', 'Channel Pair');

tmp = get(axh, 'xticklabel');
tmp = num2str((str2num(tmp)-max_lag)/ Fs, '%.2f');
set(axh, 'xticklabel', tmp);

th(3) = get(axh, 'xlabel');
set(th(3), 'string', 'Lag (sec)');

yval = get(axh, 'ylim');
lh = line([max_lag, max_lag], [yval(1), yval(2)], 'parent', axh);
set(lh, 'color', 'r', 'linestyle', ':');


set([axh, th], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(th, 'fontsize', 10);


return;