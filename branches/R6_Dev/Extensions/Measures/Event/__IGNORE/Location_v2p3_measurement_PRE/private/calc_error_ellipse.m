function [ output_args ] = calc_error_ellipse(corr_fun, arr_geom, elem_pairs,...
  Fs, speed_of_sound, mean_type, coord_sys, location, e_max, sigma_e, corr_sum_best, confidence_level);

%calc_error_ellipse - Computes the statistical error ellipse of a given location
%   Assuming that the statistical error isosurface is an ellipse on which
%   the deviation of the chi squared statistic is a constant, find the
%   size and orientation of the ellipse.
%   Dean Hawthorne  4 Sept 2012
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Inputs:
%     Far too many inputs
% Outputs:
%     major_axis - a vector whose magnitude and direction describes the
%         major axis
%     minor_axis - a vector whose magnitude and direction describes the
%         minor axis (which should always be perpendicular to the major
%         axis)
%     sigma_x - standard error of the location's x coordinate for a given
%         confidence level
%     sigma_y - standard error of the location's y coordinate for a given
%         confidence level
%     area = the area of the elipse
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    %% Variance of the correlation sum
    sigma_e2 = sigma_e^2;
    chi_min2 = chi_squared(corr_sum_best);
    
    %% Start the search algorithm at the point where chi squared is a
    %% minimum (i.e., the computed location)
    point = location;
    
    %% Temporarily we will interpret "confidence_level" argument to be
    %% the desired delta chi square.  Ultimately it would be good if
    %% we could pass in a percentage and compute the corresponding
    %% delta chi squared by inverting the cummulative distribution
    %% function with one degree of freedom
    confidence_delta = confidence_level;
    
    %% Choose a "previous point" far away
    point_prev = point * 1e6;
    iter_num = 0;
    while iter_num < 100 && norm(point - point_prev, 2) > 1e-5 * norm(point, 2)
        %fprintf(1,'Loop #%d,  point = %f %f\n', iter_num, point(1), point(2));
        point_prev = point;
        %% Find the point along "vary" axis where delta chi squared equals
        %% the confidence_delta corresponding to the confidence level
        %% value passed in argument "confidence_delta"
        %% When the minimization is done, "point" will contain the
        %% coordinates of the intersection with the confidence isosurface
        vary = 'y';
        [coord, delta_chi_squared] = fminsearch( @dist_from_confidence_isosurface, location(2));

        %% Now vary the orthognal component and minimize chi squared
        %% When the minimization is done, "point" will contain the
        %% coordinates of the minimum
        vary = 'x';
        [coord, energy] = fminsearch( @chi_squared_vary_one_coordinate, point(1));
        iter_num = iter_num + 1;
    end
    
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Nested function
    %% [] = vary_point_along_one_axis
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% sets one coordinate of the parent function's "point" variable
    %% The coordinate is selected by setting the parent function's "vary"
    %% variable to 'x', 'y' or 'z'
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Inputs:
    %%     x: the new value of the coordinate
    %% Outputs:
    %%     none
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function [] = vary_point_along_one_axis(x)
        switch vary
            case 'x'
                point(1) = x;
            case 'y'
                point(2) = x;
            case 'z'
                %% Only update z coordinate if it exists
                if( point.length() > 2 )
                    point(3) = x;
                end
            otherwise
                %%
        end
    end
    
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Nested function
    %% [chi_squared] = chi_squared_vary_one_coordinate
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% varies one coordinate of the parent function's "point" variable
    %% and computes the correlation sum at that point.
    %% The coordinate is selected by setting the parent function's "vary"
    %% variable to 'x', 'y' or 'z'
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Inputs:
    %%     x: the new value of the coordinate
    %% Outputs:
    %%     chi_squared: the value of the chi squared statistic at the new 
    %%         point
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function chisquared = chi_squared_vary_one_coordinate(x)
        vary_point_along_one_axis(x);
        corr_sum = calc_corr_sum(point, corr_fun, arr_geom, elem_pairs,...
        Fs, speed_of_sound, mean_type, coord_sys);
        chisquared = chi_squared(corr_sum);
    end

    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Nested function
    %% chisquare = function chi_squared( corr_sum )
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Inputs:
    %%     corr_sum: a correlation sum, presumedly
    %%         obtained by calling "calc_corr_sum(...)"
    %% Outputs:
    %%     chisquared: the value of the chi squared statistic for the
    %%         given correlation sum
    %% Computes chi squared given a correlation sum
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function chisquared = chi_squared(corr_sum)
        %% Use abs here just in case we encounter a point with a lower
        %% chi squared than chi_min
        chisquared = abs(e_max - corr_sum)/sigma_e2;
    end

    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Nested function
    %% delta = dist_from_confidence_isosurface
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Computes the distance from a desired isosurface as describe by
    %% the parent function's "confidence_delta" variable
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Inputs:
    %%     x: the value of the coordinate to bet varied
    %%         The coordinate is selected by setting the parent function's 
    %%         "vary" variable to 'x', 'y' or 'z'
    %% Outputs:
    %%     delta: the distance from the desired isosurface in chi squared 
    %%         (dimensionless) units
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function delta = dist_from_confidence_isosurface(x)
        vary_point_along_one_axis(x);
        corr_sum = calc_corr_sum(point, corr_fun, arr_geom, elem_pairs,...
        Fs, speed_of_sound, mean_type, coord_sys);

        %% Use abs here just in case we encounter a point with a lower
        %% chi squared than chi_min
        delta = abs(chi_squared( corr_sum ) - chi_min2 - confidence_delta);
    end

end

