%% Numerical derivative using Ridder's method
%% See Numerical Recipes Third Edition, p231
%%
%% Usage - 
%% [derivative, error] = ridder(@f, x, hmax)
%% where - f is a single-variable function 
%%         x = the point at which to evaluate the derivative
%%         hmax = the maximum step size (starting value; the actual step size is dynamic)
%%         derivative = estimate of derivative at point x
%%         error = absolute error of the derivative estimate

function [deriv, error] = ridder( f, x, h )

    if( h == 0.0 ) 
	err = MException('BRP:IllegalValue', 'Step size must be non-zero');
	throw err;
    end

    NTAB = 10; % Maximum size of the Neville tableau
    CON = 1.4; CON2 = CON * CON; % Constant by which to reduce stepsize each trial
    BIG = realmax(); % Largest real number representable
    SAFE = 2.0;  % Constant to help detect too small step size
    a = zeros(NTAB, NTAB); % Storage for the Neville tableau
    
    hh = h;
    a(1,1) = (f(x+hh) - f(x-hh))/(2.0*hh);
    error = BIG;
    for ii=2:NTAB
	hh = hh/CON;
	a(1,ii) = (f(x+hh) - f(x-hh))/(2.0*hh);
	fac = CON2;
        for jj=2:NTAB
	    %% Compute extrapolations of various orders
            %% No additional function evaluations are required
	    a(jj,ii) = (a(jj-1,ii) * fac - a(jj-1,ii-1))/(fac-1.0);
	    fac = CON2 * fac;
            %% The error strategy is to compare each new extrapolation to one
	    %% order lower, both at the present step size and the previous one.
            errt = max(abs(a(jj,ii)-a(jj-1,ii)), abs(a(jj,ii)-a(jj-1,ii-1)));
	    %% If error decreased, save the new value of the derivative and its error
            if( errt <= error )
		error = errt;
		deriv = a(jj,ii);
	    end
	end
	%% If higher order estimate is worse by a significant factor SAFE, then return early
	if( abs(a(ii,ii) - a(ii-1,ii-1)) >= SAFE * error)
	    break;
	end
    end
end