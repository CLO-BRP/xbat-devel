function varargout = calc_corr_sum(spat_coord, corr_fun, arr_geom, elem_pairs,...
  Fs, speed_of_sound, mean_type, coord_sys);

%%
%% 'calc_corr_sum'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Calculate the correlation sum associated with the given points in space for
%% the given array geometry, speed of sound, and pairwise correlation functions;
%% based on the very compact and clever code in KJD 'corrSumNew', but generalized
%% to work in 2- or 3-space
%%
%% syntax:
%% -------
%% corr_sum = calc_corr_sum(spat_coord, corr_fun, arr_geom, elem_pairs, Fs, speed_of_sound, coord_sys, display_plots);
%%
%% input:
%% ------
%% spat_coord == spatial coordinates for the points to search over in (x, y, (z)) meters
%% corr_fun == all pairwise correlation functions in order matching elem_pairs
%% arr_geom == array geometry in (x, y, (z)) meters
%% elem_pairs == array element pairs actually used (and order actually used) in corr functions
%% Fs == sound sampling rate
%% speed_of_sound == speed of sound
%% mean_type == type of mean estimate to use
%% coord_sys == coordinate system type, either Cartesian or Spherical
%%
%% output:
%% -------
%% corr_sum == correlation sum surface for the search grid
%%
%%
%% Modifications:
%% June 2005-- to get rid of 'Canary' order use; now elem_pair indices provided in same order as corr funs;
%% plus of few more mods to tighten code
%%
%% July 2006-- 1) changes to require that both array and search points are either both 2D or 3D from the
%% start, and then to calculate accordingly; report an error if dimensions don't match (why? there is no
%% need to force everything to 3D and waste calculation time if only 2D is required)
%% 2) changes to move deep diagnostic plotting code somewhere else
%%




%% get the number of search points to explore and the dimensionality of the coordinate space
[num_pts, dim] = size(spat_coord);

%% make sure array and search point dimensions match
if (size(arr_geom, 2) ~= dim)
  error('Search points and sensor array have different dimensionality!  Quitting.');
end

dim_3D = 0;
if dim == 3
  dim_3D = 1;
end


%% get the number of array element pairs == number of correlation functions
num_pairs = size(elem_pairs, 1);

%% generate a 3D matrix of  xy(z)-positions for all of the array
%% element pairs used, in the order of elem_pairs-- this is the
%% same order that the corr functions are in;
%% dim_1 are all the pairs used (num_pairs)
%% dim_2 are the two elements of the pair
%% dim_3 are the xy(z) coordinates for each element
arr_pairs(:, 1, :) = arr_geom(elem_pairs(:, 1), :);
arr_pairs(:, 2, :) = arr_geom(elem_pairs(:, 2), :); %% (all prs 1:num_pairs) X (element 1 of pr, element 2 of pr) X (x, y, (z))


%% generate a matrix of ones, num_pairs long, to be reused
num_pr_ones = ones(num_pairs, 1);


switch (coord_sys)

  case ('Cartesian')
    %% repeat the (x, y, (z)) search point coordinates for each array element pair
    %% spat_coord(:, 1) == x
    %% spat_coord(:, 2) == y
    %% (spat_coord(:, 3) == z)

    X_coord = num_pr_ones * spat_coord(:, 1)';
    Y_coord = num_pr_ones * spat_coord(:, 2)';
    if dim_3D
      Z_coord = num_pr_ones * spat_coord(:, 3)';
    end

  case ('Spherical')
    %% convert sperical to cartesian coordinates
    %% and repeat the (x, y, z) search point coordinates for each array element pair
    %% spat_coord(:, 1) == radius (range)
    %% spat_coord(:, 2) == azimuth (bearing) == theta
    %% (spat_coord(:, 3) == zenith (elevation) == phi)
    %% NOTE: conversion is for map/compass/elevation orientation, with azimuth/bearing angle (theta) taken
    %% clockwise from y-axis (in xy-plane) and zenith/altitude (phi) taken counter-clockwise from xy-plane
    %% -- not normal spherical coordinate orientation

    if dim_3D
      X_coord = num_pr_ones * (spat_coord(:, 1)' .* sin(spat_coord(:, 2)') .* cos(spat_coord(:, 3)'));
      Y_coord = num_pr_ones * (spat_coord(:, 1)' .* cos(spat_coord(:, 2)') .* cos(spat_coord(:, 3)'));
      Z_coord = num_pr_ones * (spat_coord(:, 1)' .* sin(spat_coord(:, 3)'));
    else
      X_coord = num_pr_ones * (spat_coord(:, 1)' .* sin(spat_coord(:, 2)'));
      Y_coord = num_pr_ones * (spat_coord(:, 1)' .* cos(spat_coord(:, 2)'));
    end

  otherwise
    %% throw an error

    error('Invalid coordinate system used.  Quitting.')

end


%% free up some space
clear num_pr_ones;


%% generate a matrix of ones, num_pts long, to be reused
num_pt_ones = ones(1, num_pts);

%% repeat the array pair coordinates for each search point
arr_pairs_X1 = arr_pairs(:, 1, 1) * num_pt_ones;
arr_pairs_X2 = arr_pairs(:, 2, 1) * num_pt_ones;
arr_pairs_Y1 = arr_pairs(:, 1, 2) * num_pt_ones;
arr_pairs_Y2 = arr_pairs(:, 2, 2) * num_pt_ones;
if dim_3D
  arr_pairs_Z1 = arr_pairs(:, 1, 3) * num_pt_ones;
  arr_pairs_Z2 = arr_pairs(:, 2, 3) * num_pt_ones;
end


%% calculate the time delays associated with each point in space (X, Y, (Z)) for each of the array element pairs
%% delta_t has size num_pairs by num_pts, so each column gives the set of pairwise time of arrival differences
%% for the array prescribed by a particular point in space
if dim_3D
  delta_t = ( sqrt((X_coord - arr_pairs_X1).^2 + (Y_coord - arr_pairs_Y1).^2  + (Z_coord - arr_pairs_Z1).^2) - ...
    sqrt((X_coord - arr_pairs_X2).^2 + (Y_coord - arr_pairs_Y2).^2  + (Z_coord - arr_pairs_Z2).^2) ) / speed_of_sound;
else
  delta_t = ( sqrt((X_coord - arr_pairs_X1).^2 + (Y_coord - arr_pairs_Y1).^2) - ...
    sqrt((X_coord - arr_pairs_X2).^2 + (Y_coord - arr_pairs_Y2).^2) ) / speed_of_sound;
end

%% convert the time delays to correlation function lag bins
tot_num_lags = size(corr_fun, 1);
bin_offset = ceil(tot_num_lags / 2); %% == maxlag + 1 bins
bin_num = round(delta_t .* Fs) + bin_offset; %% bin_num has size num_prs x num_pts


%% convert the lag bin to advance indices into the corr_fun matrix
adv_offset = tot_num_lags .* [0:num_pairs-1]' * num_pt_ones;
adv_index = bin_num + adv_offset;


%% calculate the average all pairwise correlation function value for all points
%% first, get the correlation values at the prescribed delays
%% corr_val has size num_pairs by num_pts, so each column gives the set of corr fun values (pulled from the
%% set of all pairwise xcorr funs at the prescribed delays) for a particular point in space
corr_val = corr_fun(adv_index);


%% threshold any correlation values less than zero
%% there shouldn't be any though
corr_val(corr_val < 0) = 0;


%% make an estimate of the mean correlation value for each point in space
if strcmpi(mean_type, 'arithmetic')
  %% calculate the average correlation value for each point
  corr_mean = sum(corr_val) / num_pairs;
elseif strcmpi(mean_type, 'trimmed')
  %% calculate the average trimmed correlation value for each point
  %% use a trimmed mean 20% (so, throw out the highest 10% and 
  %% lowest 10% of the correlation values); this won't toss out any
  %% values unless there are at least 10 correlation functions
  trim_perc = 20;
  trim = floor(num_pairs * (trim_perc/200));
  corr_val = sort(corr_val);
  corr_val = corr_val((trim+1):(end-trim), :);
  corr_mean = sum(corr_val) / (num_pairs-2*trim);
end

%% leave as the positive corr sum
% %% convert to a negative correlation sum
% %% and transpose to column
% corr_mean = -corr_mean';

%% transpose to column
varargout{1} = corr_mean';


if nargout == 3
  %% get the search point which has the maximum correlation sum (mean) value
  [max_corr_value, point_inx] = max(corr_mean);

  %% get the correlation function lags (bins) for that point (for all
  %% correlation pairs)
  max_bin_inx = bin_num(:, point_inx);

  varargout{2} = max_corr_value;
  varargout{3} = max_bin_inx;
end




return;