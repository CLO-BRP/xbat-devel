function plot_DD_all_corr_fun(deepdiag_tag, deepdiag_name, corr_functions, cf_array_prs, Fs, plot_flag);


%%
%% 'plot_DD_all_corr_fun'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create deep diagnostic figure for sensor data
%%
%% syntax:
%% -------
%% plot_DD_all_corr_fun(deepdiag_tag, deepdiag_name,...
%%  corr_functions, cf_array_prs, xchan_event_inx, peak_threshold, snd_data, x_channels_kept, t_vec, Fs, plot_flag);
%%
%% input:
%% ------
%% deepdiag_tag == figure tag
%% deepdiag_name == figure name
%% corr_functions
%% cf_array_prs
%% Fs == sample rate
%% plot_flag == what's being plotted
%%
%% output:
%% -------
%%




figh = create_deepdiag_fig(deepdiag_tag, deepdiag_name);
if (plot_flag == 1)
  delete(get(figh, 'children'));
  ax_title = sprintf('All XCorr Fun (Raw)');
elseif (plot_flag == 2)
  ax_title = sprintf('All XCorr Fun (Smoothed)');
else
  ax_title = sprintf('All XCorr Fun\n(Smoothed & Weighted)');
end



ymax = max(max(corr_functions));

[num_lags, num_pairs] = size(corr_functions);
lagvec = (num_lags - 1) / 2;
lagvec = [-lagvec:1:lagvec] / Fs;


for inx = 1:num_pairs

  if (plot_flag == 1) sbplt_num = 3 * inx - 2;
  elseif (plot_flag == 2) sbplt_num = 3 * inx - 1;
  else sbplt_num = 3 * inx; end

  %% plot corr funs
  axh(inx) = subplot(num_pairs, 3, sbplt_num, 'parent', figh');

  lh = plot(lagvec, corr_functions(:, inx), 'parent', axh(inx));

  [max_val, max_bin] = max(corr_functions(:, inx));
  lh = line(lagvec(max_bin), max_val, 'parent', axh(inx));
  set(lh, 'marker', 'x', 'color', 'r');

  set(axh(inx), 'ylim', [0, ymax*1.05]);

  if (inx == 1) th(1) = title(ax_title, 'parent', axh(inx));
  elseif (inx == num_pairs) th(2) = xlabel('Lag (sec)', 'parent', axh(inx)); end
  if (plot_flag == 1)
    th(2+inx) = ylabel(sprintf('Pair %dx%d', cf_array_prs(inx, 1), cf_array_prs(inx, 2)), 'parent', axh(inx));
  end

end


set([axh, th], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(th, 'fontsize', 10);


return;