function c = fxcorr(a,maxlag,fparams,sRate,shoulder,ce,option)
%FXCORR Filtered cross-correlation function estimates in Canary order.
%   CCOR = FXCORR(SIGNAL,MAXLAG,FPARAMS,SRATE,SHOULDER,COMPLEX_ENV,OPTION)
%		Returns the cross-correlations for the columns of SIGNAL in Canary order
%		over the range of -MAXLAG to MAXLAG.  Correlations are filtered using
%       one of the following filters, depending on the number of values in
%       the FPARAMS vector.
%   If FPARAMS is a 2-element vector, the filter is
%	    FREQFILT (a Hanning window with the defined SHOULDER at the frequency 
%	    boundaries in Hz defined in FPARAMS-- see FREQFILT for more details).
%       SHOULDER is set as half the percentage of the centered window that lies
%	    outside of the filter band.  (i.e., default shoulder = 35 means each 
%       tail makes up 35 of the curve; the center 30% of the curve covers the 
%       filter band.) A smaller shoulder makes the filter "sharper" with 
%       shorter tails. 
%   If FPARAMS is a 5-element vector, the filter is SELFFILT.  
%   FISH WARLOCK WARNING: SELFFILT is not yet complete!!!      FPARAMS
%       contains the upper and lower frequency bounds, the channel of the
%       signal to build the filter from, the multipler to use and the number
%       of frequency bins to smooth over.  SHOULDER indicates the percentile
%       of the sorted frequency bins to subtract when building the filter.
%	SIGNAL is a matrix with at least 2 columns and a length greater than one.
%	MAXLAG is the maximum time offset to be correlated in seconds
%	SRATE is the sample rate in samples/second
%   COMPLEX_ENV is switch for calculating the complex envelope of the cross correlation.
%       1 is on; 0 is off.
%	
%	OPTION is a string variable indicating the desired normalization
%       biased   - scales the raw cross-correlation by 1/M.
%       unbiased - scales the raw correlation by 1/(M-abs(lags))
%       coeff    - normalizes the sequence so that the auto-correlations
%                  at zero lag are identically 1.0.
%       none     - no scaling (this is the default).
%
%   See also XCORR, FILTFREQ and SELFFILT.

% 12/00 KJD addition of SELFFILT as an option
% 8/00 KJD modification of MATLAB's XCORR to include filtering, to reduce
%		some of the memory use and to generally speed things up
%
%   Author(s): L. Shure, 1-9-88
%   	   L. Shure, 4-13-92, revised
%   	   J. McClellan, 9-13-95, revised for maxlag
%   Copyright (c) 1988-98 by The MathWorks, Inc.
%   $Revision: 1.5 $  $Date: 1998/09/04 14:44:10 $ 
%
%   References:
%     [1] J.S. Bendat and A.G. Piersol, "Random Data:
%         Analysis and Measurement Procedures", John Wiley
%         and Sons, 1971, p.332.
%     [2] A.V. Oppenheim and R.W. Schafer, Digital Signal 
%         Processing, Prentice-Hall, 1975, pg 539.

error(nargchk(7,7,nargin))
if  any(size(a)<=1),
   error('1st input argument must be a matrix.')
end
option = lower(option);

[ar,ac] = size(a);
La = ar;

if  isempty(maxlag)
   maxlag = ceil(size(a,1)/2)-1;
else
   maxlag = round(maxlag*sRate);
end

% check validity of option
switch option
case{'none'}
   nopt = 0;
case{'coeff'}
   nopt = 1;
case{'biased'}
   nopt = 2;
case{'unbiased'}
   nopt = 3;
otherwise
   error('Unknown OPTION')
end

% Set up parameters for filtering
switch length(fparams)
case{2}
    fType = 'freq';
    freq = fparams;
case{5}
    fType = 'self';
    freq = fparams(1:2);
    ch2use = fparams(3);
    mult = fparams(4);
    sBins = fparams(5);
    perc = shoulder;
otherwise
    error('Filter paramter vector length is invalid.')
end

[nr, nc] = size(a);
nsq  = nc^2;
mr = 2 * maxlag + 1;
nfft = 2^nextpow2(mr);
nsects = ceil(2*nr/nfft);
if nsects>4 & nfft<64
   nfft = min(4096,max(64,2^nextpow2(nr/4)));
end

pp = 1:nc;
n1 = pp(ones(nc,1),:);  n2 = n1';
aindx = n1(:)';   bindx = n2(:)';

c = zeros(nfft,nsq);
minus1 = (-1).^(0:nfft-1)' * ones(1,nc);
af_old = zeros(nfft,nc);
n1 = 1;
nfft2 = nfft/2;
while( n1 < nr )
   n2 = min( n1+nfft2-1, nr );
   af = fft(a(n1:n2,:), nfft);
   c = c + af(:,aindx).* conj( af(:,bindx) + af_old(:,bindx) );
   af_old = minus1.*af;
   n1 = n1 + nfft2;
end;
if  n1==nr
   af = ones(nfft,1)*a(nr,:);
   c = c + af(:,aindx).* conj( af(:,bindx) + af_old(:,bindx) );
end


% get filter and apply in frequency domain
switch fType
case{'freq'}
filt = freqfilt(nfft,freq,sRate,shoulder);
case{'self'}
filt = selffilt(a(:,ch2use),freq,nfft,sRate,mult,perc,sBins,1);
end
c = c.*(filt(:)*ones(1,nsq));

% Calculate the complex envelope if required
if(ce)
    c(2:nfft2,:) = c(2:nfft2,:)*2; % Double positive frequencies
    c(nfft2+1:end,:) = 0;   % Zero out negative frequencies
end

% Back to the time domain
c = ifft(c);
if(ce); c = abs(c); end % Take absolute value of analytic signal to get complex envelope

jkl = reshape(1:nsq,nc,nc)';
mxlp1 = maxlag+1;
c = [ conj(c(mxlp1:-1:2,:)); c(1:mxlp1,jkl(:))];

% Pull out auto correlations and cross correlations in Canary order
auto = sqrt(c(mxlp1,diag(jkl))); % auto correlations at zero lag
mno = triu(jkl',1);	% Pointer vector for columns in Canary order
mno = mno(:); mno(mno==0) = [];
c = c(:,mno); % reduce matrix to Canary pairs only

switch nopt
case{1}	% return normalized by sqrt of each autocorrelation at 0 lag
   % do column arithmetic to get correct autocorrelations
   N = length(auto);
   Nm1 = N-1;
   pr1 = triu((1:Nm1)'*ones(1,Nm1),0); %Pointer for 1st half of pair
   pr1 = pr1(:); pr1(pr1==0) = [];
   pr2 = triu(ones(N,1)*(1:N),1);	%Pointer for 2nd half of pair
   pr2 = pr2(:); pr2(pr2==0) = [];
   tmp = auto(pr1).*auto(pr2);
   c = c ./ (ones(mr,1)*tmp(:).');
case{2}	% biased result, i.e. divide by nr for each element
   c = c / nr;
case{3}	% unbiased result, i.e. divide by nr-abs(lag)
   c = c ./ ([nr-maxlag:nr (nr-1):-1:nr-maxlag]' * ones(1,size(c,2)));
end

if ~any(any(imag(a)))
   c = real(c);
end
%keyboard
return
