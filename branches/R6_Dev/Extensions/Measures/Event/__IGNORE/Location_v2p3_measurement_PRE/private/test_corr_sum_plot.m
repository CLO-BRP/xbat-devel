%% 21 Oct 2005
%% expore why using all 5 channels gives a location different from using
%% only 3

x1 = 9636;   
y1 = -6721;
delt1 = ( sqrt((x1 - arr_pairs_X1).^2 + (y1 - arr_pairs_Y1).^2  + (0 - arr_pairs_Z1).^2) - ...
  sqrt((x1 - arr_pairs_X2).^2 + (y1 - arr_pairs_Y2).^2  + (0 - arr_pairs_Z2).^2) ) / speed_of_sound;
bin_num1 = round(delt1(:,1) .* Fs) + bin_offset(:,1); 
corr_val1 = sum(corr_fun(bin_num1 + adv_offset(:,1)));

x2 = 10139;   
y2 = -6783;
delt2 = ( sqrt((x2 - arr_pairs_X1).^2 + (y2 - arr_pairs_Y1).^2  + (0 - arr_pairs_Z1).^2) - ...
  sqrt((x2 - arr_pairs_X2).^2 + (y2 - arr_pairs_Y2).^2  + (0 - arr_pairs_Z2).^2) ) / speed_of_sound;
bin_num2 = round(delt2(:,1) .* Fs) + bin_offset(:,1); 
corr_val2 = sum(corr_fun(bin_num2 + adv_offset(:,1)));



 figure;
 for i=1:size(corr_fun, 2)
    subplot(size(corr_fun, 2), 1, i);
    plot(corr_fun(:, i), '-k');
    ylabel(sprintf('%dx%d', elem_pairs(i,1), elem_pairs(i,2)));
    h=line(bin_num1(i), corr_fun(bin_num1(i), i));
    set(h, 'marker', 'o', 'color', 'r');
    h=line(bin_num2(i), corr_fun(bin_num2(i), i));
    set(h, 'marker', 'o', 'color', 'b');
end
