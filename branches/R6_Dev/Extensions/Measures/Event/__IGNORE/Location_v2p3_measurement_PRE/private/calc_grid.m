function varargout = calc_grid(varargin);

%%
%% 'calc_grid'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Calculate a rectangular search grid from the 2D center of the array
%%
%% syntax:
%% -------
%% [gridXY, x_range, y_range, grid_size] = calc_grid(arr_geom, search_radius, num_search_pts);
%%
%% input:
%% ------
%% arr_geom == array geometry in x, y, (z) meters
%% search_radius == radius of search area from the center of the array; generate a square based on this
%%  (containing the circle) 
%% num_search_pts == total number of search points to generate
%%
%% output:
%% -------
%% gridXY == search grid
%% x_range == x value vector for grid
%% y_range == y value vector for grid
%% grid_size == dimensions of search grid (num_y, num_x)
%%

%%
%% K.A. Cortopassi, July 2006, change calculation of the search area
%%


%% get inputs
arr_geom = varargin{1};
search_radius = varargin{2};
num_search_pts = varargin{3};

% %% get rectangular boundaries of array
% xmin = min(arr_geom(:, 1));
% xmax = max(arr_geom(:, 1));
% ymin = min(arr_geom(:, 2));
% ymax = max(arr_geom(:, 2));
% 
% %% from this, define search area
% xmin = xmin - search_pad;
% xmax = xmax + search_pad;
% ymin = ymin - search_pad;
% ymax = ymax + search_pad;

%% get the center of the array
arr_center = mean(arr_geom);

%% define the square search area
xmin = arr_center(1) - search_radius;
xmax = arr_center(1) + search_radius;
ymin = arr_center(2) - search_radius;
ymax = arr_center(2) + search_radius;

%% get interval for grid
search_area = abs((xmax - xmin) * (ymax - ymin));
search_interval = sqrt(search_area / num_search_pts);

%% generate the grid
x_range = xmin:search_interval:xmax;
y_range = ymin:search_interval:ymax;
num_x = length(x_range);
num_y = length(y_range);
x_values = ones(num_y, 1) * x_range;
y_values = (ones(num_x, 1) * y_range)';

gridXY = [x_values(:) y_values(:)];


%% return outputs
varargout{1} = gridXY; %% output the search grid
varargout{2} = x_range;
varargout{3} = y_range;
varargout{4} = [num_y, num_x]; %% output the dimensions of the search grid


return;