function [plot_text_diag, plot_standard_diag, plot_standard_plus_diag, plot_deep_diag, plot_deep_plus_diag]...
  = get_diag_plot_levels(display_plots);

%%
%% Kathryn Cortopassi
%% October 2005
%%
%% set flag for which diagnostics to show
%%


if strcmpi(display_plots, 'deep plus')
  plot_deep_plus_diag = 1;
  plot_deep_diag = 1;
  plot_standard_plus_diag = 1;
  plot_standard_diag = 1;
  plot_text_diag = 1;
elseif strcmpi(display_plots, 'deep')
  plot_deep_plus_diag = 0;
  plot_deep_diag = 1;
  plot_standard_plus_diag = 1;
  plot_standard_diag = 1;
  plot_text_diag = 1;
elseif strcmpi(display_plots, 'standard plus')
  plot_deep_plus_diag = 0;
  plot_deep_diag = 0;
  plot_standard_plus_diag = 1;
  plot_standard_diag = 1;
  plot_text_diag = 1;
elseif strcmpi(display_plots, 'standard')
  plot_deep_plus_diag = 0;
  plot_deep_diag = 0;
  plot_standard_plus_diag = 0;
  plot_standard_diag = 1;
  plot_text_diag = 1;
elseif strcmpi(display_plots, 'text only')
  plot_deep_plus_diag = 0;
  plot_deep_diag = 0;
  plot_standard_plus_diag = 0;
  plot_standard_diag = 0;
  plot_text_diag = 1;
else
  plot_deep_plus_diag = 0;
  plot_deep_diag = 0;
  plot_standard_plus_diag = 0;
  plot_standard_diag = 0;
  plot_text_diag = 0;
end

return;
