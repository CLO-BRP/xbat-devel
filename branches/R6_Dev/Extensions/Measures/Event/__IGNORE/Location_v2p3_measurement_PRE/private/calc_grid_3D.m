function varargout = calc_grid_3D(varargin);

%%
%% 'calc_grid_3D'
%%
%% Kathryn A. Cortopassi
%% June 2005
%%
%% Like 'calc_grid', but calculate a cuboidal search grid from the 3D center of the array
%%
%% syntax:
%% -------
%% [gridXYZ, x_range, y_range, z_range, grid_size] = calc_grid_3D(arr_geom, search_radius, num_search_pts);
%%
%% input:
%% ------
%% arr_geom == array geometry in x, y, (z) meters
%% search_radius == radius of search area from the center of the array; generate a cuboid based on this
%%  (containing the sphere) 
%% num_search_pts == total number of search points to generate
%%
%% output:
%% -------
%% gridXYZ == cuboidal search grid
%% x_range == x value vector for grid
%% y_range == y value vector for grid
%% z_range == z value vector for grid
%% grid_size == dimensions of search grid (num_y, num_x, num_z)
%%

%%
%% K.A. Cortopassi, July 2006, change calculation of the search area
%%


%% get inputs
arr_geom = varargin{1};
search_radius = varargin{2};
num_search_pts = varargin{3};

%% make sure array is 3D
if size(arr_geom, 2) < 3
  arr_geom(:, 3) = 0;
end

% %% get cuboidal boundaries of array
% xmin = min(arr_geom(:, 1));
% xmax = max(arr_geom(:, 1));
% ymin = min(arr_geom(:, 2));
% ymax = max(arr_geom(:, 2));
% zmin = min(arr_geom(:, 3));
% zmax = max(arr_geom(:, 3));
% 
% %% from this, define a search volume around
% %% the extent of the array
% xmin = xmin - search_range;
% xmax = xmax + search_range;
% ymin = ymin - search_range;
% ymax = ymax + search_range;
% zmin = zmin - search_range;
% zmax = zmax + search_range;

%% get the center of the array
arr_center = mean(arr_geom);

%% define the cuboidal search area
xmin = arr_center(1) - search_radius;
xmax = arr_center(1) + search_radius;
ymin = arr_center(2) - search_radius;
ymax = arr_center(2) + search_radius;
zmin = arr_center(3) - search_radius;
zmax = arr_center(3) + search_radius;

%% get interval for search grid
if (zmax-zmin)
  search_interval = nthroot(abs((xmax-xmin)*(ymax-ymin)*(zmax-zmin))/(num_search_pts), 3);
else
  search_interval = sqrt(abs((xmax-xmin)*(ymax-ymin))/(num_search_pts));
end

%% generate the grid
x_range = xmin:search_interval:xmax;
y_range = ymin:search_interval:ymax;
z_range = zmin:search_interval:zmax;
num_x = length(x_range);
num_y = length(y_range);
num_z = length(z_range);
x_values = ones(num_y * num_z, 1) * x_range;
y_values = ones(num_z, 1) * y_range;
y_values = repmat(y_values(:), 1, num_x);
z_values = z_range' * ones(1, num_x * num_y);


%% return outputs
varargout{1} = [x_values(:), y_values(:), z_values(:)]; %% output the search grid
varargout{2} = x_range;
varargout{3} = y_range;
varargout{4} = z_range;
varargout{5} = [num_y, num_x, num_z]; %% output the dimensions of the search grid


return;