function varargout = calc_hyperbolae(varargin);

%%
%% 'calc_hyperbolae'
%%
%% Kathryn A. Cortopassi
%% February/April 2005
%%
%% Calculate (and plot) hyperbolae for given array geometry and path differences
%% (path differences probably obtained from time delays)
%% Make the point spacing exponentially increasing from the foci of the hyperbolae
%%
%% syntax:
%% -------
%% [XY, hyper_XY, a, c, phi, X0] = calc_hyperbolae(arr_geom, elem_used, ref_chan, nminus1_pathdiff, search_radius, num_search_pts);
%%
%% input:
%% ------
%% arr_geom == array geometry in x,y meters
%% elem_used == array elements actually used
%% ref_chan == number of reference array element
%% nminus1_pathdiff == pairwise path differences of other elements with ref element in ascending
%%                     order; calculated as PrefCh - PxCh
%% %search_pad == range to extend length of hyperbolic arm beyond that defined by the array
%% search_radius == range to extend length of hyperbolic arm from the center of the array
%% num_search_pts == total number of hyperbolic search points to generate
%%
%% output:
%% -------
%% XY == (x,y) points of the hyperbolic search grid
%% hyper_XY == the coordinates of the individual hyperbolae
%% a ==  hyperbola parameter, distance from origin to curve
%% c ==  hyperbola parameter, distance from origin to focus
%% phi == change of basis parameter, rotation
%% X0 == change of basis parameter, translation
%%

%%
%% K.A. Cortopassi, July 2006, change calculation of the search area,
%% use a given radius from the center of the array
%%


%% get inputs
%% put into useable var names
arr_geom = varargin{1};
elem_used = varargin{2};
ref_chan = varargin{3};
nminus1_pathdiff = varargin{4};
search_radius = varargin{5};
num_search_pts = varargin{6};



nch = length(elem_used);
nchm1 = nch - 1;


%% calculate the distances between the reference channel (sensor) and
%% all other channels (sensors) (in meters)
%% assume only 2D arrays for now
dist = arr_geom(elem_used, 1:2) - ones(nch, 1) * arr_geom(ref_chan, 1:2);
dist = sqrt(dist(:, 1).^2 + dist(:, 2).^2);
ref_chan_inx = find(elem_used == ref_chan);
dist(ref_chan_inx) = [];

%% get parameters for the hyperbola function
a = nminus1_pathdiff/2; %% (PrefCh - PxCh) = 2a, so a negative if signal arrives at refCh 1st
c = dist/2;

%% make sure dimensions of a and c are the same
a = reshape(a, size(c));

%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%% is this what I want to do long-term?
%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%% make sure that |c| >= |a|
fix_inx = abs(a) > abs(c);
a(fix_inx) = c(fix_inx);
%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


%% hyperbolae ==> y.^2/a.^2 - x.^2/(c.^2-a.^2) = 1
%% with line connecting array elements (foci) defining the
%% y-axis

%% get coordinate transform parameters for the n-1 pairs

%% first, rotation angle
xR = arr_geom(elem_used, 1) - arr_geom(ref_chan, 1);
yR = arr_geom(elem_used, 2) - arr_geom(ref_chan, 2);
phi = atan2(yR, xR) - pi/2;
phi(ref_chan_inx) = [];

%% next, translation
x0 = (arr_geom(elem_used, 1) + arr_geom(ref_chan, 1)) / 2;
y0 = (arr_geom(elem_used, 2) + arr_geom(ref_chan, 2)) / 2;
X0 = [x0, y0];
X0(ref_chan_inx, :) = [];


%% set some values for xp (independent variable)
%% but make the spacing of the points expand out
%% exponentially around zero, 

% and make the length
% %% of each arm equal to hypotenuse of rectangular
% %% array bounds plus hypotenuse defined by search_pad
% 
% %% get rectangular boundaries of array
% xmin = min(arr_geom(:, 1));
% xmax = max(arr_geom(:, 1));
% ymin = min(arr_geom(:, 2));
% ymax = max(arr_geom(:, 2));
% 
% %% use this to define the arm-length of the hyperbolae
% arm_length = sqrt((xmax - xmin).^2 + (ymax - ymin).^2) + (sqrt(2) * search_pad);

%% make the arm_length simply equal to the search radius
%% plus the distance between the most extreme sensor elements
arm_length = search_radius + max(calc_dist(arr_geom(elem_used, 1:2)));


N = 2; %% base for exponent (though this won't matter here)
x1 = 1; %% first x value, just some non-zero offset value
%% compute last x value for all n-1 hyperbolae
xend = arm_length .* sqrt(c.^2 - a.^2) ./ c;
%% add the offset
xend = xend + x1;
half_npts = round(num_search_pts / nchm1 / 2); %% number of x points on each arm
%% so that total number of points used is ~num_search_pts
%% calculate exponents giving x1 & xend
%% save the offset
offset = x1;
x1 = log(x1)/log(N);
xend = log(xend)/log(N);
for loop_inx = 1:nchm1
  xp{loop_inx} = N.^(x1:(xend(loop_inx)-x1)/half_npts:xend(loop_inx)) - offset; %% x values on one arm
  xp{loop_inx} = [-fliplr(xp{loop_inx}(2:end)), xp{loop_inx}]; %% all x values
  ptsperhyp(loop_inx) = length(xp{loop_inx});
end


%% set up container for final (x, y) values
XY = zeros(sum(ptsperhyp), 2);

%% insert a spacer
ptsperhyp(2:end+1) = ptsperhyp;


for loop_inx = 1:nchm1

  %% get yp from xp (y values from x values)
  yp = sqrt(a(loop_inx).^2 + (xp{loop_inx}.^2 .* a(loop_inx).^2) / (c(loop_inx).^2 - a(loop_inx).^2));

  %% diagnostic line
  %fprintf(1, '# yp NAN''s = %d\n', sum(isnan(yp)));

  %% put into vector form
  Xp = [xp{loop_inx}; yp];
  Xp_flip = [xp{loop_inx}; -yp];

  %% generate change of basis transform
  T = [cos(phi(loop_inx)), -sin(phi(loop_inx)); sin(phi(loop_inx)), cos(phi(loop_inx))];

  %% transform to standard basis
  X = T * Xp + X0(loop_inx, :)' * ones(1, length(xp{loop_inx}));
  X_flip = T * Xp_flip + X0(loop_inx, :)' * ones(1, length(xp{loop_inx}));

  %% save values
  if a(loop_inx) > 0
    XY((1+ptsperhyp(loop_inx)*(loop_inx-1)):(ptsperhyp(loop_inx+1)+ptsperhyp(loop_inx)*(loop_inx-1)), :) = X';
    hyper_XY{loop_inx} = X';
  else
    XY((1+ptsperhyp(loop_inx)*(loop_inx-1)):(ptsperhyp(loop_inx+1)+ptsperhyp(loop_inx)*(loop_inx-1)), :) = X_flip';
    hyper_XY{loop_inx} = X_flip';
  end

end



%% diagnostic code
%% plot the XY search points
if 0
  h = findobj(0, 'tag', 'Hyperbolic_Search_Area_Plot');
  if isempty(h)
    h = figure;
    set(h, 'tag', 'Hyperbolic_Search_Area_Plot');
    set(h, 'numbertitle', 'off', 'name', 'Hyperbolic Search Area', 'units', 'pixels', 'position', [500, 500, 600, 600]);
  else
    figure(h);
  end
  plot(XY(:, 1), XY(:, 2), '.k');
  title('hyperbolic search points');
  xlabel('meters'); ylabel('meters');
end



%% return 2D coordinates only
% %% for now, Z is zero
% %% LATER: make this general to 3D
% XY = [XY, zeros(size(XY, 1), 1)];


%% return outputs
varargout{1} = XY; %% output the hyperbolic search grid
varargout{2} = hyper_XY; %% output the individual hyperbolae coordinates

varargout{3} = a; %% output the hyperbolae parameters
varargout{4} = c;
varargout{5} = phi; %% and change of basis parameters
varargout{6} = X0;

return;