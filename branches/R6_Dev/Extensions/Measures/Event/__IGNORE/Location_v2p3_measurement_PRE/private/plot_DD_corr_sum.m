function plot_DD_corr_sum(corr_fun, elem_pairs, max_corr_value, max_bin_inx, Fs, iteration, version_num);

%%
%% 'plot_DD_corr_sum'
%%
%% Kathryn A. Cortopassi
%% July 2006
%%
%% Plot the deep diagnostic plot for the correlation sum calculation
%%
%%
%%
%% syntax:
%% -------
%% plot_DD_corr_sum(corr_fun, elem_pairs, max_corr_value, max_bin_inx);
%%
%% input:
%% ------
%% corr_fun == all pairwise correlation functions in order matching elem_pairs
%% elem_pairs == array element pairs actually used (and order actually used) in corr functions
%% max_corr_value == correlation sum (mean) for the best point
%% max_bin_inx == associated lags of the pairwise correlation functions for the best point
%% Fs == sampling rate
%% iteration == search iteration number
%% version_num == location measure version number
%%
%% output:
%% -------
%%



deepdiag_tag = ['deep_diagnostics_for_location_', version_num, '_corr_sum_calc'];
deepdiag_name = ['Deep Diagnostics for Source Location Measurement ', version_num, ' : ''Corr Sum Calc'''];
figh = create_deepdiag_fig(deepdiag_tag, deepdiag_name);
if iteration == 1
  delete(get(figh, 'children'));
end


%% get number of correlation functions
num_corrs = size(corr_fun, 2);


ymax = 1.05 * max(max(corr_fun));

%% generate diagnostic plot
for inx = 1:num_corrs
  axh(inx) = subplot(num_corrs, 1, inx, 'parent', figh);
  lag_start = (length(corr_fun(:, inx)) - 1)/2;
  lagvec = [-lag_start:1:lag_start] / Fs;
  plot(lagvec, corr_fun(:, inx), '-k', 'parent', axh(inx));
  
 th(2+inx) = ylabel(sprintf('%dx%d', elem_pairs(inx, 1), elem_pairs(inx, 2)), 'parent', axh(inx));

  h = line(lagvec(max_bin_inx(inx)), corr_fun(max_bin_inx(inx), inx), 'parent', axh(inx));
  set(h, 'marker', 'o', 'color', 'b');

  [pkval, pkpt] = max(corr_fun(:, inx));
  h = line(lagvec(pkpt), pkval, 'parent', axh(inx));
  set(h, 'marker', 'x', 'color', 'r');

end

set(axh, 'ylim', [0, ymax]);
th(2) = xlabel('Lag (sec)', 'parent', axh(end));
th(1) = title(sprintf('Current Maximum Correlation Mean Value = %.2f\nIteration # %d', max_corr_value, iteration), 'parent', axh(1));


set([axh, th], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(th, 'fontsize', 10);


figure(figh);
drawnow;


return;