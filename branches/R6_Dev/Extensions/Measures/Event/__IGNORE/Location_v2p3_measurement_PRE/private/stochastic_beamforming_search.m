function varargout = stochastic_beamforming_search(varargin);

%%
%% 'stochastic_beamforming_search'
%%
%% Kathryn A. Cortopassi
%% April/May 2005
%%
%% Search for the best location, determined by maximizing the sum of all pairwise correlation functions
%% (a beam-forming approach); start the search over either a hyperbolic search trajectory (where the point
%% spacing is exponentially increasing from the foci of the hyperbolae) or a regular regtangular grid;
%% the grid search will work in both 2D (rectangular) or 3D (cuboidal), the hyperbolic search will
%% currently work in 2D only
%% New search points are generated as random deviations from the current top points (a stochastic search)
%% The search is terminated when the top points fall within a user-specified bound, or the maximum
%% number of iterations is reached
%%
%% syntax:
%% -------
%% [top_coords_final, top_corr_sum_final, med_cent_dist, iterations,...
%%   top_coords_init, top_corr_sum_init, coords_init, corr_sum_init] = stochastic_beamforming_search(search_info);
%%
%% input:
%% ------
%% search_info.search_type == search method to use
%% search_info.search_dim = dimensionality of search space to use
%% search_info.arr_geom == array geometry in x,y, (z) meters
%% search_info.elem_used == array elements actually used
%% search_info.elem_pairs == all element pairs actually used in order they were calculated
%%                           (same order as corr functions)
%% search_info.ref_elem == number of reference array element
%% search_info.event_ID === log event ID number
%% search_info.Fs == sound sampling rate
%% search_info.speed_of_sound == speed of sound
%% search_info.corr_fun == all pairwise correlation functions in the order they were calculated
%% search_info.nminus1_pathdiff == pairwise path differences of other elements with ref element in ascending order;
%%                                 calculated as PrefCh - PxCh
%% search_info.search_radius == radius of search area from the center of the array
%% search_info.term_crit == criterion for terminating the search as average centroid distance in meters
% %% search_info.jitter == amount of jitter to use for generation of new search points in meters
%% search_info.display_plots == flag to plot graphics or not
%% version_num == location measurement version number
%%
%% output:
%% -------
%% top_coords_final == coordinates (x,y,(z)) of the final top ~1000 (5%) locations
%% top_corr_sum_final == their associated correlation sums
%% med_cent_dist == median centroid distance for final top locations
%% iterations == iterations required
%% top_coords_init == coordinates (x,y,(z)) of the initital top ~1000 (5%) locations
%% top_corr_sum_init == their associated correlation sums
%% coords_init == coordinates (x,y,(z)) of the initital search points
%% corr_sum_init == their associated correlation sums
%%
%%
%% Modifications:
%% June 2005--
%%   get rid of 'Canary' order use; now 'elem_pairs' indices are provided in the same order as corr funs;
%%   add code so the grid search will work in either 2D or 3D
%%
%% July 2006--
%% 1) change search area calculation to a given radius from the center of
%% the array, and change parts of search code accordingly so that points jitter
%% spherically from where they are
%% 2) add choice of using only N-1 correlation functions for making location
%% estimate
%% 3) add version number variable to prevent typos
%% 4) have dealt consistently with location dimensions, if search is 2D,
%% array was lopped to have 2D dimensions, all other code chenged to expect array
%% and search points to have the same dimensionality to start
%%



%% get inputs
search_info = varargin{1}; %% all info
version_num = varargin{2};


%% make a flag for search type
if strcmpi(search_info.search_type, 'hyperbolic')
  hyper_search = 1;
else
  hyper_search = 0;
end

%% make a flag for search dimension
if strcmpi(search_info.search_dim, '2D')
  search_dim_2D = 1;
else
  search_dim_2D = 0;
end


%% put other variables into useable var names
arr_geom = search_info.arr_geom;
elem_used = search_info.elem_used;
elem_pairs = search_info.elem_pairs;
ref_elem = search_info.ref_elem;
event_ID = search_info.event_ID;
Fs = search_info.Fs;
speed_of_sound = search_info.speed_of_sound;
corr_fun = search_info.corr_fun;
nminus1_pathdiff = search_info.nminus1_pathdiff;
search_radius = search_info.search_radius;
term_crit = search_info.term_crit;
%jitter = search_info.jitter;
mean_type = search_info.mean_type;
display_plots = search_info.display_plots;

%% Den Hawathorne
%% Hack to test function
theta_min = search_info.theta_min;
theta_max = search_info.theta_max;
%%theta_min=0;
%%theta_max=180;

%% set flag to show diagnostic plots
[plot_text_diag, plot_standard_diag, plot_standard_plus_diag, plot_internal_diag, plot_deep_diag]...
  = get_diag_plot_levels(display_plots);


%% tighten up space and memory use a bit
clear search_info;
%pack; %% this no longer works within functions in MATLAB R2007a


%% define number of search points
num_search_pts = 50e3;


if hyper_search
  %% calculate the initial hyperbolic search grid

  if search_dim_2D
    %% calculate the 2D hyperbolic search grid
    if plot_text_diag
      fprintf('(performing 2D search with hyperbolic start)\n');
    end
    [coords_init, hyper_XY] = calc_hyperbolae(arr_geom, elem_used, ref_elem, nminus1_pathdiff, search_radius, num_search_pts);

  else
    %% calculate the 3D hyperboloidal search grid
    fprintf('3D Search with Hyperbolic Start Not Yet Implemented!!\n');
    %     if plot_text_diag
    %       fprintf('Performing 3D Hyperbolic Search.\n');
    %     end
    %[coords_init, hyper_XYZ] = calc_hyperboloids(arr_geom, elem_used, ref_elem, nminus1_pathdiff, search_radius, num_search_pts);
    fprintf('(performing 3D search with grid start)\n');
    search_dim_2D = 1;
    [coords_init, x_vec, y_vec, z_vec, grid_size] = calc_grid_3D(arr_geom, search_radius, num_search_pts);

  end

else %% do grid search
  %% calculate the initial regtangular search grid

  if search_dim_2D
    %% calculate the 2D rectangular search grid
    if plot_text_diag
      fprintf('(performing 2D search with grid start)\n');
    end
    [coords_init, x_vec, y_vec, grid_size] = calc_grid(arr_geom, search_radius, num_search_pts);

  else
    %% calculate the 3D cuboidal search grid
    if plot_text_diag
      fprintf('(performing 3D search with grid start)\n');
    end
    [coords_init, x_vec, y_vec, z_vec, grid_size] = calc_grid_3D(arr_geom, search_radius, num_search_pts);

  end

end



%% get the array center
arr_center = mean(arr_geom);

%% the number of search points returned is ~num_search_pts so get the exact value
num_search_pts = size(coords_init, 1);

%% check if initial grid points fall inside or outside the specified search radius
%% first, make array center (0,0,(0))
adj_coords_init = coords_init - ones(num_search_pts, 1) * arr_center;

%% find out which points are inside the search area
in_inx = (sqrt(sum(adj_coords_init .^ 2, 2)) <= search_radius);


%% calculate the average corr sum for these points; if points are outside
%% the search radius, their corr sum is zero (keep these outside points for
%% plotting the grid as an image below, then eliminate them)
%% 'calc_corr_sum' is generalized to 2D or 3D calculations
coord_sys = 'Cartesian';
corr_sum_init = zeros(num_search_pts, 1);

if plot_internal_diag
  [corr_sum_init(in_inx), max_corr_value, max_bin_inx] = calc_corr_sum(coords_init(in_inx, :), corr_fun, arr_geom, elem_pairs, Fs, speed_of_sound,...
    mean_type, coord_sys);
  plot_DD_corr_sum(corr_fun, elem_pairs, max_corr_value, max_bin_inx, Fs, 1, version_num);
else
  corr_sum_init(in_inx) = calc_corr_sum(coords_init(in_inx, :), corr_fun, arr_geom, elem_pairs, Fs, speed_of_sound,...
    mean_type, coord_sys);
end


if plot_standard_diag
  %% plot the array, initial search space, and corr sum
  standarddiag_tag = ['LOC_', version_num, '_standard_diag_plot'];
  standarddiag_name = ['Standard Diagnostics for Source Location Measurement ', version_num];

  if search_dim_2D
    if hyper_search
      plot_SD_array_hyperb(coords_init, hyper_XY, arr_geom, elem_used, ref_elem, event_ID, standarddiag_tag, standarddiag_name);
    else
      corr_sum_grid = reshape(corr_sum_init, grid_size);
      plot_SD_init_grid_corr_sum(corr_sum_grid, x_vec, y_vec, arr_geom, elem_used, ref_elem, event_ID, standarddiag_tag, standarddiag_name);
    end

  else
    %% search dim 3D

    fprintf('Diagnostic plots not yet available for 3D search\n');

    if 0 %% *** skip this for now

      if hyper_search
        %%plot_SD_array_hyperb3D(coords_init, hyper_XY, arr_geom, elem_used, ref_elem, event_ID, standarddiag_tag, standarddiag_name);
        %% for now, 3D hyper start is forced to a grid start
        corr_sum_grid = reshape(corr_sum_init, grid_size);
        plot_SD_init_grid_corr_sum3D(corr_sum_init, coords_init, corr_sum_grid, x_vec, y_vec, z_vec, arr_geom, elem_used, ref_elem, event_ID, standarddiag_tag, standarddiag_name);
      else
        corr_sum_grid = reshape(corr_sum_init, grid_size);
        plot_SD_init_grid_corr_sum3D(corr_sum_init, coords_init, corr_sum_grid, x_vec, y_vec, z_vec, arr_geom, elem_used, ref_elem, event_ID, standarddiag_tag, standarddiag_name);
      end

    end %% skip this for now

  end %% *** if search_dim 2D

end


%% now, get rid of the points outside the search radius
coords_init = coords_init(in_inx, :);
corr_sum_init = corr_sum_init(in_inx);

%% get the new total number of search points
num_search_pts = sum(in_inx);



%% set up some parameters for the search loop

%% set maximum number of iterations
num_iterations = 101;

%% criterion for termination-- median centroid distance in meters of top choices
%% XXX think about changing this to the movement of the peak position in meters
%% between iterations; if it moves less than a criterion ammount, terminate XXX
criterion_dist = term_crit;


% %% maximum deviation for point jitter in meters
% %% make this a user entered value instead
% dev_frac = 50; %% max deviation is 2% of search range
% max_dev = search_range / dev_frac;
% max_dev = jitter;

%% maximum deviation for point jitter in meters
%% make jitter start out as a certain percentage of the max extents
%% of the search area,
%% then decrease them at a steady rate from iteration to iteration
%% (note each dimension has it's own jitter amount based on that
%% dimension's search extent)

% %% set intial deviation (jitter)
% frac = 0.5;
% max_dev = frac * (max(coords_init) - min(coords_init));

%% set intial radial deviation (jitter) as a fraction of the search radius
frac = 0.5;
max_dev = frac * search_radius;

%% set fraction of current deviation (jitter) to use in next loop
dec_frac = 0.75;


%% number of points to take off the top
rep_num = 20; %% take top 5% of points;
trim_pts = round(num_search_pts / rep_num);

%% get the new total number of search points
num_search_pts = rep_num * trim_pts;

%% get the dimensions for jittering the points below,
%% jitter the replicates, leaving the top coordinates unaltered
dim1 = num_search_pts - trim_pts;

if search_dim_2D
  dim2 = 2;
else
  dim2 = 3;
end

%% expand arr_center accordingly for use in search loop
arr_center = ones(num_search_pts, 1) * arr_center;


%% set up starting coordinates and corr sums

%% find top 'trim_pts' coordinates from initial corr sum;
%% these will be the largest values since 'calc_corr_sum'
%% returns the positive correlation sum
[top_vals, top_inx] = sort(corr_sum_init); %% sort is ascending
top_coords_curr = flipud(coords_init(top_inx(end-trim_pts+1:end), :)); %% now, 'top_coords_curr' is descending
top_coords_init = top_coords_curr;

top_corr_sum_curr = flipud(top_vals(end-trim_pts+1:end)); %% now, 'top_corr_sum_curr' is descending
top_corr_sum_init = top_corr_sum_curr;

%% Calculate the spread of current top coordinates by calculating their median centroid distance
centroid = mean(top_coords_curr);
med_cent_dist = median(sqrt(sum((top_coords_curr - ones(trim_pts, 1) * centroid).^2, 2)));
%avg_cent_dist = mean(sqrt(sum((top_coords_curr - ones(trim_pts, 1) * centroid).^2, 2))); %% change from avg to med


%% set the search counter; intitial grid search counts as 1
loop_inx = 1;

if plot_standard_diag && search_dim_2D
  %% plot the array, initial search space, and corr sum for current top points
  coords_curr = coords_init;
  corr_sum_curr = corr_sum_init;
  plot_SD_3D_corr_sum_curr(coords_init, corr_sum_init, top_coords_init, top_corr_sum_init, coords_curr, corr_sum_curr,...
    top_coords_curr, top_corr_sum_curr, arr_geom, elem_used, ref_elem, event_ID, med_cent_dist, loop_inx,...
    standarddiag_tag, standarddiag_name);
  %% compare intial search coord to current search coords and top points
  plot_SD_2D_coords_curr(coords_init, top_coords_init, coords_curr, top_coords_curr, arr_geom, elem_used, ref_elem,...
    event_ID, loop_inx, num_search_pts, standarddiag_tag, standarddiag_name);
  %% plot corr functions and time delays associated with current top pick
  plot_SD_corr_fun(corr_fun, arr_geom, elem_pairs, speed_of_sound, Fs, event_ID, top_coords_curr(1, :), loop_inx,...
    standarddiag_tag, standarddiag_name);
end


% %%~~
% %% some snooping
% %%~~
% figure;
% if search_dim_2D
%   plot(coords_init(:,1), coords_init(:,2), 'b.');
%   hold on; plot(top_coords_init(:,1), top_coords_init(:,2), 'ro');
%   xlabel('x');
%   ylabel('y');
% else
%   plot3(coords_init(:,1), coords_init(:,2), coords_init(:,3), 'b.');
%   hold on; plot3(top_coords_init(:,1), top_coords_init(:,2), top_coords_init(:,3), 'ro');
%   xlabel('x');
%   ylabel('y');
%   zlabel('z');
%   rotate3d on;
% end



%%***
%% run the search loop
%%***
while (med_cent_dist > criterion_dist) && (loop_inx ~= num_iterations)

  %% increment the search counter; intitial grid search counts as 1
  loop_inx = loop_inx + 1;

  %% replicate the top coordinates
  coords_curr = repmat(top_coords_curr, rep_num, 1);


  %   %% jitter only those dimensions being searched
  %   coords_curr(trim_pts+1:end, 1:dim2) = sign(randn(dim1, dim2)) .* rand(dim1, dim2) .* (ones(dim1, 1)* max_dev(1:dim2))...
  %     + coords_curr(trim_pts+1:end, 1:dim2);

  %% jitter the replicates, leaving the top coordinates unaltered
  %% jitter only those dimensions being searched, and jitter points spherically around their current position
  dR = rand(dim1, 1) .* max_dev;
  d_theta = rand(dim1, 1) .* (theta_max - theta_min);
  if search_dim_2D
    d_phi = [];
  else
    %% Dean Hawthorne
    %% Change range of phi from 360 to 180
    d_phi = rand(dim1, 1) .* (180); 
  end
  jitter_offset =  spher2cart([dR, d_theta, d_phi]);

  coords_curr(trim_pts+1:end, 1:dim2) = coords_curr(trim_pts+1:end, 1:dim2) +  jitter_offset;

  %   %%~~
  %   %% some snooping
  %   %%~~
  %   plot(coords_curr(:,1), coords_curr(:,2), 'oy');


  %% should I allow points to go outside the search area?
  %% no, if the points go out of bounds, reflect them back in by flipping around the exceeded edges
  %% (first jitter shouldn't be too far out, so this should pop them back into search area, if it
  %% pops out on the other side, it will get it in the next pass)

  %% check if points fall outside the search radius, if they do, reflect
  %% back in
  %% first, make array center (0,0,(0))
  adj_curr_coords = coords_curr - arr_center;

  %% find out which points if any are outside the search area
  out_inx = (sqrt(sum(adj_curr_coords .^ 2, 2)) > search_radius);

  if sum(out_inx)
    %% get spherical coordinates (radius, azimuth, (zenith)) of outside
    %% points
    spher_coords = cart2spher(adj_curr_coords(out_inx, :));

    %% flip back into search area (around outer edge) by adjusting the radius,
    %% leave the angles unchanged
    spher_coords(:, 1) = 2 * search_radius - spher_coords(:, 1);

    %% now convert back to cartesian coordinates, in the original reference frame
    %% and replace the bad points
    coords_curr(out_inx, :) =  spher2cart(spher_coords) + arr_center(out_inx, :);
  end

  %% decrease the jitter for next loop
  max_dev = dec_frac * max_dev;


  %% calculate the average corr sum for these points
  if plot_internal_diag
    [corr_sum_curr, max_corr_value, max_bin_inx] = calc_corr_sum(coords_curr, corr_fun, arr_geom, elem_pairs, Fs, speed_of_sound,...
      mean_type, coord_sys);
    plot_DD_corr_sum(corr_fun, elem_pairs, max_corr_value, max_bin_inx, Fs, loop_inx, version_num);
  else
    corr_sum_curr = calc_corr_sum(coords_curr, corr_fun, arr_geom, elem_pairs, Fs, speed_of_sound,...
      mean_type, coord_sys);
  end


  %% find the top trim_pts points; these will be the largest values
  %% since 'calc_corr_sum' returns the positive correlation sum
  [top_vals, top_inx] = sort(corr_sum_curr); %% sort is ascending
  top_coords_curr = flipud(coords_curr(top_inx(end-trim_pts+1:end), :)); %% now, top_coords_curr is descending
  top_corr_sum_curr = flipud(top_vals(end-trim_pts+1:end)); %% now, top_corr_sum_curr is descending

  %% Calculate the spread of current top coordinates by calculating their median centroid distance
  centroid = mean(top_coords_curr);
  med_cent_dist = median(sqrt(sum((top_coords_curr - ones(trim_pts, 1) * centroid).^2, 2)));
  %avg_cent_dist = mean(sqrt(sum((top_coords_curr - ones(trim_pts, 1) * centroid).^2, 2))); %% change from avg to med

  if plot_standard_diag && search_dim_2D
    %% plot the array, initial search space, and corr sum for current top points
    plot_SD_3D_corr_sum_curr(coords_init, corr_sum_init, top_coords_init, top_corr_sum_init, coords_curr, corr_sum_curr,...
      top_coords_curr, top_corr_sum_curr, arr_geom, elem_used, ref_elem, event_ID, med_cent_dist, loop_inx,...
      standarddiag_tag, standarddiag_name);
    %% compare intial search coord to current search coords and top points
    plot_SD_2D_coords_curr(coords_init, top_coords_init, coords_curr, top_coords_curr, arr_geom, elem_used, ref_elem,...
      event_ID, loop_inx, num_search_pts, standarddiag_tag, standarddiag_name);
    %% plot corr functions and time delays associated with current top pick
    plot_SD_corr_fun(corr_fun, arr_geom, elem_pairs, speed_of_sound, Fs, event_ID, top_coords_curr(1, :), loop_inx,...
      standarddiag_tag, standarddiag_name);
  end

end
%%***
%% end the search loop
%%***


top_coords_final = top_coords_curr;
top_corr_sum_final = top_corr_sum_curr;



%% return outputs
varargout{1} = top_coords_final; %% coordinates (x,y,(z)) of the final top 'trim_pts' locations
varargout{2} = top_corr_sum_final; %% associated correlation sums

varargout{3} = med_cent_dist; %% median centroid distance for final top locations
varargout{4} = loop_inx; %% iterations required

varargout{5} = top_coords_init; %% coordinates (x,y,(z)) of the initital top 'trim_pts' locations
varargout{6} = top_corr_sum_init; %% associated correlation sums

varargout{7} = coords_init; %% coordinates (x,y,(z)) of the initital search points
varargout{8} = corr_sum_init; %% associated correlation sums


return;