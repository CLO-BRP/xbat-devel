function [cart_coords] = spher2cart(spher_coords);

%%
%% 'spher2cart'
%%
%% Kathryn A. Cortopassi
%% July 2006
%%
%% Convert spherical to cartesian coordinates; returns 2D or 3D coordinates
%% depending on dimensions of input coords 
%% (spherical coordinates are given in degrees)
%%
%% syntax:
%% -------
%% [cart_coords] = spher2cart(spher_coords);
%%
%% input:
%% ------
%% spher_coords == spherical spatial coordinates (radius, azimuth, (zenith))
%%                 where azimuth (bearing angle) is taken clockwise from the y-axis, and
%%                 zenith (altitude) is taken counter-clockwise from the xy-plane
%%                 (in degrees)
%%
%% output:
%% -------
%% cart_coords == cartesian spatial coordinates (x, y, (z))
%%



%% get the dimensionality of the coordinate space
dim = size(spher_coords, 2);

dim_3D = 0;
if dim > 2
  dim_3D = 1;
end

%% convert degrees to radians
spher_coords(:, 2:end) = spher_coords(:, 2:end) * (pi/180);

%% calculate cartesian coordinates
if dim_3D
  X_coord = spher_coords(:, 1) .* cos(spher_coords(:, 3)) .* sin(spher_coords(:, 2));
  Y_coord = spher_coords(:, 1) .* cos(spher_coords(:, 3)) .* cos(spher_coords(:, 2));
  Z_coord = spher_coords(:, 1) .* sin(spher_coords(:, 3));
else
  X_coord = spher_coords(:, 1) .* sin(spher_coords(:, 2));
  Y_coord = spher_coords(:, 1) .* cos(spher_coords(:, 2));
  Z_coord = [];
end

%% return cartesian coords
cart_coords = [X_coord, Y_coord, Z_coord];


return;