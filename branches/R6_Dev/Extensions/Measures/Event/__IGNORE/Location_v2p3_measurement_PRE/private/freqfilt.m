function	filt = freqfilt(bins,freq,sRate,shoulder)
%FREQFILT	Returns a filter for frequency domain use
%	FILTER = FREQFILT(BINS,FREQUENCIES,SRATE,SHOULDER)
%		Returns a filter for use in the frequency domain.
%	BINS is the number of frequency bins in the spectrum
%	FREQUENCIES is the lower and upper boundaries of the filter (Hz)
%	SRATE is the sample rate (samples/second)
%	SHOULDER (optional). The filter is just a simple Hanning window.
%	The shoulder is set as half the percentage of the centered window that lies
%	outside of the filter band.  (i.e., default shoulder = 35 means each tail makes up 35%
%	of the curve; the center 30% of the curve covers the filter band.) 
%	A smaller shoulder makes the filter "sharper" with shorter tails.

% 8/00 KJD excised from within filtcorrcf
% 11/99 KJD added coeff normalization
% 7/99 KJD

error(nargchk(3,4,nargin))

if(nargin==3 | isempty(shoulder))
shoulder = 35;	% Hanning filter "shoulder" (a percentage)
%NOTE:  This number determines the location of the edges of the designated filter
% with respect to the given frequency limits.  The "sharpness" of the filter depends
% on how small this number is: a larger shoulder means less attenuation within the
% filter band, but longer tails outside of it.  
end

% Generate filter
mid = bins/2;
filt = zeros(mid-1,1);

hzperb = sRate/bins;
sNo = round(freq/hzperb);	% indices for the frequencies (filt)
winLen = sNo(2)-sNo(1)+1;	% width of filter band in samples
wlPercent = (100-2*shoulder)/100;	% Percentage of hamming window that overlaps filter band
fwin = hanning(round(winLen/wlPercent));	% Length
sh = round(length(fwin)*(shoulder/100));	% Index of first shoulder in filter

if(sh <= sNo(1))	% Figure out where the first shoulder falls in the final filter
   startFilt = sNo(1) - sh + 1;
   startWin = 1;
else
   startFilt = 1;
   startWin = sh - sNo(1) + 1;
end
% Patch 'window' onto filter
fwinLen = length(fwin)-startWin + 1;
if(startFilt + fwinLen -1 > mid - 1)
   endFilt = mid - 1;
else
   endFilt = startFilt + fwinLen - 1;
end
endWin = startWin + endFilt - startFilt;
filt(startFilt:endFilt) = fwin(startWin:endWin);
filt = [0;filt;1;filt(end:-1:1)];	%Set DC offset to zero and aliasing bin to 1

return

