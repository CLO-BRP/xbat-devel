function plot_SD_3D_corr_sum_curr(varargin);

%%
%% 'plot_SD_3D_corr_sum_curr'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Plot the 3D correlation sum for the coordinates and corr sum values provided
%%
%% syntax:
%% -------
%%  plot_SD_3D_corr_sum_curr(coords_init, corr_sum_init, top_coords_init, top_corr_sum_init,...
%%    coords_curr, corr_sum_curr, top_coords_curr, top_corr_sum_curr,...
%%    arr_geom, elem_used, ref_chan, event_ID, cent_dist, iteration, fig_tag, fig_name);
%%
%% input:
%% ------
%% coords_init == spatial coordinates for the initital search points
%% corr_sum_init == correlation sum associated with initial search points
%% top_coords_init == spatial coordinates for the initital top search points
%% top_corr_sum_init == correlation sum associated with initial top search points
%% coords_curr == current search coordinates
%% corr_sum_curr == correlation sum for current search coordinates
%% top_coords_curr == spatial coordinates for current top points
%% top_corr_sum_curr == correlation sum associated with current top points
%% arr_geom == array geometry in (x, y, z) meters
%% elem_used == channels used in search
%% ref_chan == reference channel
%% event_ID == event ID
%% cent_dist == median or average centroid distance for current top points
%% iteration = number of iterations involved
%% fig_tag == diagnostic figure tag
%% fig_name == diagnostic figure name
%%
%% output:
%% -------
%%
%%



for_movie = 0;


%% get the input variables
i = 1;
coords_init = varargin{i}; i = i + 1;
corr_sum_init = varargin{i}; i = i + 1;
top_coords_init = varargin{i}; i = i + 1;
top_corr_sum_init = varargin{i}; i = i + 1;
coords_curr = varargin{i}; i = i + 1;
corr_sum_curr = varargin{i}; i = i + 1;
top_coords_curr = varargin{i}; i = i + 1;
top_corr_sum_curr = varargin{i}; i = i + 1;
arr_geom = varargin{i}; i = i + 1;
elem_used = varargin{i}; i = i + 1;
ref_chan = varargin{i}; i = i + 1;
event_ID = varargin{i}; i = i + 1;
cent_dist = varargin{i}; i = i + 1;
iteration = varargin{i}; i = i + 1;
fig_tag = varargin{i}; i = i + 1;
fig_name = varargin{i}; i = i + 1;



%% set up figure
[fh1, axh] = create_standarddiag_fig(fig_tag, fig_name);


%% plot 3D coordinates comparing all intitial search coordinates
%% to current search coordinates, in 2nd panel of big diag figure
axh = axh(2);
set(axh, 'visible', 'on');


%% update the titles
th1(1) = get(axh, 'title');
th1(2) = get(axh, 'xlabel');
th1(3) = get(axh, 'ylabel');
th1(4) = get(axh, 'zlabel');
set(th1(1), 'string', sprintf('3D Corr Sum : Ev #%d : Iter #%d\nCent Dist = %.2f (m)', event_ID, iteration, cent_dist));
set(th1(2), 'string', 'X (meters)');
set(th1(3), 'string', 'Y (meters)');
set(th1(4), 'string', 'Z (avg corr sum)');
set(th1, 'parent', axh, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);


%% get/set axis limits
xmin = min([coords_init(:, 1); coords_curr(:, 1); arr_geom(:, 1)]);
xmax = max([coords_init(:, 1); coords_curr(:, 1); arr_geom(:, 1)]);
ymin = min([coords_init(:, 2); coords_curr(:, 2); arr_geom(:, 2)]);
ymax = max([coords_init(:, 2); coords_curr(:, 2); arr_geom(:, 2)]);
zmin = 0;
zmax = top_corr_sum_curr(1);

set(axh, 'xlim', [xmin, xmax], 'ylim', [ymin, ymax], 'zlim', [zmin, zmax]);


%% plot 3D corr sum for initial coordinates
lh1 = line(coords_init(:, 1), coords_init(:, 2), corr_sum_init, 'parent', axh);
% %% decimate before plot
% lh1 = line(coords_init(1:10:end, 1), coords_init(1:10:end, 2), corr_sum_init(1:10:end), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.6 .75 .8]);

%% plot 3D corr sum for initial top coordinates
lh1 = line(top_coords_init(:, 1), top_coords_init(:, 2), top_corr_sum_init, 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.2,.35,.4]);

%% plot 3D corr sum for current coordinates
clear lh1;
lh1 = line(coords_curr(:, 1), coords_curr(:, 2), corr_sum_curr, 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [0, .5, 1]);

%% plot 3D corr sum for current top coordinates
clear lh1;
lh1 = line(top_coords_curr(:, 1), top_coords_curr(:, 2), top_corr_sum_curr, 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [1,.25,.25]);

%% plot current top point == location
if for_movie
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', 'p', 'markerfacecolor', [0,1,.5], 'markeredgecolor', [0,0,0],...
    'markersize', 26, 'linewidth', 3);
else
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', '*', 'color', 'k', 'markersize', 12, 'linewidth', 2.5);
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', '*', 'color', [0,1,.5], 'markersize', 8, 'linewidth', 1);
end
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', '*', 'color', 'k', 'markersize', 12, 'linewidth', 2.5);
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', '*', 'color', [0,1,.5], 'markersize', 10, 'linewidth', 1);
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), top_corr_sum_curr(1), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', 'o', 'markerfacecolor', [0,1,.5], 'markeredgecolor', 'k', 'markersize', 5, 'linewidth', 1);

%% plot point shadow
clear lh1;
lh1 = line(coords_init(:, 1), coords_init(:, 2), zmin*ones(1,size(coords_init,1)), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.5 .5 .5]);
clear lh1;
lh1 = line(coords_curr(:, 1), coords_curr(:, 2), zmin*ones(1,size(coords_curr,1)), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.5 .5 .5]);
% clear lh1;
% lh1 = line(top_coords_curr(:, 1), top_coords_curr(:, 2), zmin*ones(size(top_coords_curr,1),1), 'parent', axh);
% set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.5 .5 .5]);

%% plot array
clear lh1;
lh1 = line(arr_geom(:, 1), arr_geom(:, 2), zmax*ones(1,size(arr_geom,1)), 'parent', axh);
if for_movie
  set(lh1, 'marker', 'o', 'markersize', 14, 'markerfacecolor', 'y', 'markeredgecolor', 'k', 'linestyle', 'none', 'linewidth', 2);
else
  set(lh1, 'marker', 'p', 'markersize', 11, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');

  %% indicate the elements actually used
  clear lh1;
  elem_used(elem_used == ref_chan) = [];
  lh1 = line(arr_geom(elem_used, 1), arr_geom(elem_used, 2), zmax*ones(1,length(elem_used)), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none');
  clear lh1;
  lh1 = line(arr_geom(ref_chan, 1), arr_geom(ref_chan, 2), zmax, 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none', 'linewidth', 2);
end


drawnow;


return;