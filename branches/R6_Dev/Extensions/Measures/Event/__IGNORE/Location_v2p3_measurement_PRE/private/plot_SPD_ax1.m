function plot_SPD_ax1(stdplusdiag_tag, stdplusdiag_name, snd_data, chan_2_use, Fs, event_inx);

%%
%% 'plot_SPD_ax1'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create 1st axis of standard plus diagnostic figure
%%
%% syntax:
%% -------
%% plot_SPD_ax1(stdplusdiag_tag, stdplusdiag_name, snd_data, chan_2_use, Fs, event_inx);
%%
%% input:
%% ------
%% internaldiag_tag == figure tag
%% internaldiag_name == figure name
%% snd_data == sound data
%% chan_2_use == channels selected by user
%% Fs == data sampling rate
%% event_inx = indices of reference event
%%
%% output:
%% -------
%%


[figh_spd, axh] = create_stdplusdiag_fig(stdplusdiag_tag, stdplusdiag_name);
set(axh, 'visible', 'off');
for inx = 1:length(axh)
  delete(allchild(axh(inx)));
end


axh = axh(1);
set(axh, 'visible', 'on');


%% plot raw data on all channels
%% 'strips' obliterates the axis tag
%% so get and restore it
tmp_tag = get(axh, 'tag');
axes(axh);
strips(snd_data(:, chan_2_use));
set(axh, 'tag', tmp_tag);

th(1) = get(axh, 'title');
set(th(1), 'string', 'Snd Data (Raw)');

my_y_label = num2str(flipud(chan_2_use'));
my_y_label = mat2cell(my_y_label, ones(length(chan_2_use), 1), 1);
set(axh, 'yticklabelmode', 'manual', 'yticklabel', my_y_label);

th(2) = get(axh, 'ylabel');
set(th(2), 'string', 'Channel #');

tmp = get(axh, 'xticklabel');
tmp = num2str(str2num(tmp)/ Fs);
set(axh, 'xticklabelmode', 'manual', 'xticklabel', tmp);


th(3) = get(axh, 'xlabel');
set(th(3), 'string', 'Time (sec)');

yval = get(axh, 'ylim');
lh(1) = line([event_inx(1), event_inx(1)], [yval(1), yval(2)], 'parent', axh);
lh(2) = line([event_inx(end), event_inx(end)], [yval(1), yval(2)], 'parent', axh);
set(lh, 'color', 'r');


set([axh, th], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(th, 'fontsize', 10);


return;