function plot_DD_sensor_data(deepdiag_tag, deepdiag_name, snd_data, chan_2_use, ref_chan, t_vec, ev_bnd, order, raw_flag);

%%
%% 'plot_DD_sensor_data'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create deep diagnostic figure for sensor data
%%
%% syntax:
%% -------
%% plot_DD_sensor_data(deepdiag_tag, deepdiag_name, snd_data, chan_2_use, ref_chan, t_vec, ev_bnd, order, raw_flag);
%%
%% input:
%% ------
%% deepdiag_tag == figure tag
%% deepdiag_name == figure name
%% snd_data == sound data
%% chan_2_use == channels selected by user
%% ref_chan == reference channel number
%% t_vec = time vector
%% ev_bnd == event start and end time
%% order == bandpass filter order
%% raw_flag == flag if 1st or 2nd column of plot
%%
%% output:
%% -------
%%



figh = create_deepdiag_fig(deepdiag_tag, deepdiag_name);
if (raw_flag)
  delete(get(figh, 'children'));
  ax_title = sprintf('Snd Data (Raw)');
else
  ax_title = sprintf('Snd Data (Bndlim-Std)\n<Filt Ord = %d>', order);
end


n_ch = length(chan_2_use);

maxval = max(max(snd_data(:, chan_2_use)));
minval = min(min(snd_data(:, chan_2_use)));

for inx = 1:n_ch

  if (raw_flag) sbplt_num = 2 * inx - 1;
  else sbplt_num = 2 * inx; end
  axh(inx) = subplot(n_ch, 2, sbplt_num, 'parent', figh);
  plot(t_vec, snd_data(:, chan_2_use(inx)), 'parent', axh(inx));

  if (inx == 1) th(1) = title(ax_title, 'parent', axh(inx));
  elseif (inx == n_ch) th(2) = xlabel(sprintf('Time (sec)'), 'parent', axh(inx)); end
  if (raw_flag) th(2+inx) = ylabel(sprintf('Ch %d', chan_2_use(inx)), 'parent', axh(inx)); end

  if (chan_2_use(inx) == ref_chan)
    lh = line([ev_bnd(1), ev_bnd(1), ev_bnd(2), ev_bnd(2), ev_bnd(1)],...
      [minval, maxval, maxval, minval, minval], 'parent', axh(inx));
    set(lh, 'color', 'r', 'linewidth', 2);
  end

end

set(axh, 'ylim', 1.1*[minval, maxval], 'xlim', [t_vec(1), t_vec(end)]);


set([axh, th], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(th, 'fontsize', 10);


return;