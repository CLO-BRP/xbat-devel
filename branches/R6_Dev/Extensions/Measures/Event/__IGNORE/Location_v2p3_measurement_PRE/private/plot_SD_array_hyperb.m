function plot_SD_array_hyperb(varargin);

%%
%% 'plot_SD_array_hyperb'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Plot the array and hyperboli for the initial search
%%
%% syntax:
%% -------
%% plot_SD_array_hyperb(XY, hyper_XY, arr_geom, elem_used, ref_chan, event_ID, fig_tag, fig_name);
%%
%% input:
%% ------
%% XY ==  spatial coordinates for the points
%% hyper_XY == cell array of coordinates for the individual hyperbolae
%% arr_geom == array geometry in (x, y, z) meters
%% elem_used == array elements actually used
%% ref_chan == number of reference channel
%% event_ID == event ID
%% fig_tag == diagnostic figure tag
%% fig_name == diagnostic figure name
%%
%% output:
%% -------
%%
%%



%% get the input variables
i = 1;
XY = varargin{i}; i = i + 1;
hyper_XY = varargin{i}; i = i + 1;
arr_geom = varargin{i}; i = i + 1;
elem_used = varargin{i}; i = i + 1;
ref_chan = varargin{i}; i = i + 1;
event_ID = varargin{i}; i = i + 1;
fig_tag = varargin{i}; i = i + 1;
fig_name = varargin{i}; i = i + 1;


%% okay, I won't be mean...
%set(0, 'defaultFigureMenuBar', 'figure');


%% set up figure
[figh, axh] = create_standarddiag_fig(fig_tag, fig_name);
for (inx = 1:4) delete(get(axh(inx), 'children')); end
set(axh, 'visible', 'off');


%% plot the array and hyperbolae in 1st panel of a big diagnostic plot
%% 2D geometries only for now
axh = axh(1);
set(axh, 'visible', 'on');


%% set up titles
th1(1) = get(axh, 'title');
th1(2) = get(axh, 'xlabel');
th1(3) = get(axh, 'ylabel');
set(th1(1), 'string', sprintf('Array & Hyperbolae : event # %d\n', event_ID));
set(th1(2), 'string', 'X (meters)');
set(th1(3), 'string', 'Y (meters)');
set(th1, 'parent', axh, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);


%% plot array
lh1 = line(arr_geom(:, 1), arr_geom(:, 2), 'parent', axh);
set(lh1, 'marker', 'p', 'markersize', 11, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');

%% indicate the elements actually used
clear lh1;
lh1 = line(arr_geom(elem_used, 1), arr_geom(elem_used, 2), 'parent', axh);
set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none');

%% add array element numbers
clear th1;
nch = size(arr_geom, 1);
for i = 1:nch
  th1(i) = text(arr_geom(i,1), arr_geom(i,2), ['  ', num2str(i)], 'parent', axh);
end
set(th1, 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 12, 'horizontalalignment', 'left');
set(th1(ref_chan), 'color', 'r', 'fontsize', 14);


%% set up color cycle
%% (I need a better way to deal with cycling through colors
%% in the plot)
%% 'r', 'y', 'g', 'b', 'm', 'k', 'c', and lights and mediums of each
colors = [...
  {[1 0 0]},...
  {[1 1 0]},...
  {[0 1 0]},...
  {[0 0 1]},...
  {[1 0 1]},...
  {[0 0 0]},...
  {[0 1 1]},...
  {[1 .75 .75]},...
  {[1 1 .75]},...
  {[.75 1 .75]},...
  {[.75 .75 1]},...
  {[1 .75 1]},...
  {[.75 .75 .75]},...
  {[.75 1 1]},...
  {[1 .3525 .3525]},...
  {[1 1 .3525]},...
  {[.3525 1 .3525]},...
  {[.3525 .3525 1]},...
  {[1 .3525 1]},...
  {[.3525 .3525 .3525]},...
  {[.3525 1 1]},...
  ];

clear lh1;
for loop_inx = 1:length(elem_used)-1
  lh1 = line(hyper_XY{loop_inx}(:, 1), hyper_XY{loop_inx}(:, 2), 'parent', axh);
  set(lh1,...
    'markeredgecolor', colors{loop_inx},...
    'markerfacecolor', colors{loop_inx},...
    'linestyle', '-',...
    'linewidth', 1,...
    'color', colors{loop_inx},...
    'marker', '.');
end

%% finish plot
xmin = min([XY(:, 1); arr_geom(:, 1)]);
xmax = max([XY(:, 1); arr_geom(:, 1)]);
ymin = min([XY(:, 2); arr_geom(:, 2)]);
ymax = max([XY(:, 2); arr_geom(:, 2)]);

set(axh, 'xlim', [xmin, xmax], 'ylim', [ymin, ymax]);

clear lh1;
lh1(1) = line([0, 0], [ymin, ymax], 'parent', axh);
lh1(2) = line([xmin, xmax],[0, 0], 'parent', axh);
set(lh1, 'color', 'k', 'linestyle', ':');

drawnow;
%zoom on;

return;
