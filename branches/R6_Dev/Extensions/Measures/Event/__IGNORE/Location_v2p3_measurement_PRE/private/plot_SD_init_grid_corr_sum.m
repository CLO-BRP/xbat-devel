function plot_SD_init_grid_corr_sum(varargin);

%%
%% 'plot_SD_init_grid_corr_sum'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Plot the initial grid, corr sum and array in 3D
%%
%% syntax:
%% -------
%% plot_SD_init_grid_corr_sum(corr_sum_grid, x_vec, y_vec, arr_geom, elem_used, ref_chan, event_ID,...
%%   fig_tag, fig_name);
%%
%% input:
%% ------
%% corr_sum_grid == array of corr sum values for the intial search grid
%% x_vec == values over x range for the intial search grid
%% y_vec == values over y range for the intial search grid
%% arr_geom == array geometry
%% elem_used == channels used in search
%% ref_chan == reference channel
%% event_ID == event ID
%% fig_tag == diagnostic figure tag
%% fig_name == diagnostic figure name
%%
%% output:
%% -------
%%
%%


for_movie = 0;

%% get the input variables
i = 1;
corr_sum_grid = varargin{i}; i = i + 1;
x_vec = varargin{i}; i = i + 1;
y_vec = varargin{i}; i = i + 1;
arr_geom = varargin{i}; i = i + 1;
elem_used = varargin{i}; i = i + 1;
ref_chan = varargin{i}; i = i + 1;
event_ID = varargin{i}; i = i + 1;
fig_tag = varargin{i}; i = i + 1;
fig_name = varargin{i}; i = i + 1;


%% okay, I won't be mean...
%set(0, 'defaultFigureMenuBar', 'figure');


%% set up figure
[figh, axh] = create_standarddiag_fig(fig_tag, fig_name);
for (inx = 1:4) delete(get(axh(inx), 'children')); end
set(axh, 'visible', 'off');


%% plot the array and corr sum in 1st panel of a big diagnostic plot
%% only 2D geometries for now
axh = axh(1);
set(axh, 'visible', 'on');

%% plot corr sum grid as image
%% 'image' clears out the axes so must add tag back below
axh_tag = get(axh, 'tag');
ih1 = image('parent', axh);
set(ih1, 'cdata', corr_sum_grid, 'xdata', x_vec, 'ydata', y_vec, 'cdatamapping', 'scaled');
set(axh, 'clim', [min(corr_sum_grid(:)), max(corr_sum_grid(:))],...
  'xlim', [x_vec(1), x_vec(end)],...
  'ylim', [y_vec(1), y_vec(end)]);
%colorbar('peer', axh);

%% set up axes and titles, font is as we like, etc
%% 'image' clears out the tag so I have to reset it
%% also sits itself on top, so I need to push axis up
set(axh,...
  'tag', axh_tag,...
  'layer', 'top');


%% set titles
th1(1) = get(axh, 'title');
th1(2) = get(axh, 'xlabel');
th1(3) = get(axh, 'ylabel');
set(th1(1), 'string', sprintf('Intitial Grid Corr Sum : Event #%d\n', event_ID));
set(th1(2), 'string', 'X (meters)');
set(th1(3), 'string', 'Y (meters)');
set(th1, 'parent', axh, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);


%% plot array
if for_movie
  lh1 = line(arr_geom(:, 1), arr_geom(:, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 14, 'markerfacecolor', 'y', 'markeredgecolor', 'k', 'linestyle', 'none', 'linewidth', 2);
else
  lh1 = line(arr_geom(:, 1), arr_geom(:, 2), 'parent', axh);
  set(lh1, 'marker', 'p', 'markersize', 11, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');

  %% indicate the elements actually used
  clear lh1;
  lh1 = line(arr_geom(elem_used, 1), arr_geom(elem_used, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none');
  clear lh1;
  lh1 = line(arr_geom(ref_chan, 1), arr_geom(ref_chan, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none', 'linewidth', 2);
end


%% add array element numbers
clear th1;
nch = size(arr_geom, 1);
for i = 1:nch
  th1(i) = text(arr_geom(i,1), arr_geom(i,2), ['  ', num2str(i)], 'parent', axh);
end
set(th1, 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 12, 'horizontalalignment', 'left');
set(th1(ref_chan), 'color', 'r', 'fontsize', 14);


drawnow;


return;
