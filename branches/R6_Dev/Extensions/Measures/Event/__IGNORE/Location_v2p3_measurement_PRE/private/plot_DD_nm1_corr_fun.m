function plot_DD_nm1_corr_fun(deepdiag_tag, deepdiag_name,...
  corr_functions, cf_array_prs, xchan_event_inx, peak_threshold, snd_data, x_channels_kept, t_vec, Fs);


%%
%% 'plot_DD_nm1_corr_fun'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create deep diagnostic figure for sensor data
%%
%% syntax:
%% -------
%% plot_DD_nm1_corr_fun(deepdiag_tag, deepdiag_name,...
%%  corr_functions, cf_array_prs, xchan_event_inx, peak_threshold, snd_data, x_channels_kept, t_vec, Fs);
%%
%% input:
%% ------
%% deepdiag_tag == figure tag
%% deepdiag_name == figure name
%% corr_functions
%% cf_array_prs
%% xchan_event_inx
%% peak_threshold
%% snd_data == sound data
%% x_channels_kept == channels retained by algorithm
%% t_vec == time vector
%% Fs == sample rate
%%
%% output:
%% -------
%%



figh = create_deepdiag_fig(deepdiag_tag, deepdiag_name);
delete(get(figh, 'children'));


[num_lags, num_pairs] = size(corr_functions);
lagvec = (num_lags - 1) / 2;
lagvec = [-lagvec:1:lagvec];
lagvec1 = lagvec / Fs;


for inx = 1:num_pairs

  %% plot corr funs
  axh = subplot(num_pairs, 2, 2*inx-1, 'parent', figh, 'xticklabel', '');

  lh = plot(lagvec1, corr_functions(:, inx), 'parent', axh);

  if (inx == 1) th1(1) = title('XCorr Fun - Ref Ch x Cross Ch', 'parent', axh);
  elseif (inx == num_pairs) th1(2) = xlabel(sprintf('Lag (sec)'), 'parent', axh); end
  th1(2+inx) = ylabel(sprintf('Pair %dx%d', cf_array_prs(inx, 1), cf_array_prs(inx, 2)), 'parent', axh);

  curr_event_inx = [];
  for jinx = 1:size(xchan_event_inx{inx}, 2)
    curr_event_inx = [curr_event_inx, xchan_event_inx{inx}(1, jinx):xchan_event_inx{inx}(2, jinx)];
  end
  lagvec2 = [lagvec(curr_event_inx(1)):1:lagvec(curr_event_inx(1))+length(curr_event_inx)-1];
  lagvec2 = lagvec2 / Fs;

  lh = line(lagvec2, peak_threshold(inx)*ones(size(curr_event_inx)));
  set(lh, 'parent', axh, 'marker', '.', 'linestyle', 'none', 'color', 'r', 'linewidth', 2);


  %% plot cross channels
  axh = subplot(num_pairs, 2, 2*inx, 'parent', figh, 'xticklabel', '');

  lh = plot(t_vec, snd_data(:, x_channels_kept(inx)), 'parent', axh);

  if (inx == 1) th2(1) = title('X-Chan Data', 'parent', axh);
  elseif (inx == num_pairs) th2(2) = xlabel(sprintf('Time (sec)'), 'parent', axh); end
  th(2+inx) = ylabel(sprintf('Ch %d', x_channels_kept(inx)), 'parent', axh);

  lh = line(t_vec(curr_event_inx), zeros(size(curr_event_inx)));
  set(lh, 'parent', axh, 'marker', '.', 'linestyle', 'none', 'color', 'r', 'linewidth', 2);

end


set([axh, th1, th2], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set([th1, th2], 'fontsize', 10);


return;