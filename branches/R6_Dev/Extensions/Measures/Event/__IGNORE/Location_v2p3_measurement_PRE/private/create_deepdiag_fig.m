function figh = create_deepdiag_fig(deepdiag_tag, internaldiag_name);


%%
%% 'create_deepdiag_fig'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create deep diagnostic figure
%%
%% syntax:
%% -------
%% figh = create_deepdiag_fig(deepdiag_tag, internaldiag_name);
%%
%% input:
%% ------
%% deepdiag_tag == figure tag
%% internaldiag_name == figure name
%%
%% output:
%% -------
%% figh == figure handle
%%


figh = findobj(0, 'tag', deepdiag_tag);

if isempty(figh)
  figh = figure;
  lbwh = get(0, 'screensize');
  wid = round(.475*lbwh(3));
  hght = round(.875*lbwh(4));
  set(figh, 'tag', deepdiag_tag,...
    'name', internaldiag_name,...
    'numbertitle', 'off',...
    'menubar', 'none',...
    'position', [lbwh(3)-wid-4, lbwh(4)-30-hght, wid, hght]);
end

return;