function [ ] = plot_ellipse( a, b, phi )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
range = 1.2 * max( [a b] )
phi = phi * pi/180.0;
t= 0:0.001:2.0*pi;
X = a*cos(t)*cos(phi) - b * sin(t) * sin(phi);
Y = a*cos(t)*sin(phi) + b * sin(t) * cos(phi);
axis( [-range range -range range] )
axis square;
plot(X,Y);
end

