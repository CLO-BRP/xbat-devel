function [spher_coords] = cart2spher(cart_coords);

%%
%% 'cart2spher'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Convert cartesian to spherical coordinates; returns 2D or 3D coordinates
%% depending on dimensions of input coords 
%% (spherical coordinates are given in degrees)
%%
%% syntax:
%% -------
%% [spher_coords] = cart2spher(cart_coords);
%%
%% input:
%% ------
%% cart_coords == cartesian spatial coordinates (x, y, (z))
%%
%% output:
%% -------
%% spher_coords == spherical spatial coordinates (radius, azimuth, (zenith))
%%                 where azimuth (bearing angle) is taken clockwise from the y-axis, and
%%                 zenith (altitude) is taken counter-clockwise from the xy-plane
%%                 (in degrees)
%%



%% get the dimensionality of the coordinate space
dim = size(cart_coords);

dim_3D = 0;
if dim(2) > 2
  dim_3D = 1;
end


%% get range
range = sqrt(sum(cart_coords .^ 2, 2));

X_coord = cart_coords(:, 1);
Y_coord = cart_coords(:, 2);
if dim_3D
  Z_coord = cart_coords(:, 3);
end


%% get azimuth (bearing angle) in degrees clockwise from y-axis
for inx = 1:dim(1)
  if Y_coord(inx) > 0
    if X_coord(inx) >= 0
      bearing(inx) = (180/pi) * atan(X_coord(inx) ./ Y_coord(inx));
    else %% if X_coord < 0
      bearing(inx) = 360 + (180/pi) * atan(X_coord(inx) ./ Y_coord(inx));
    end
  elseif Y_coord(inx) < 0
    bearing(inx) = 180 + (180/pi) * atan(X_coord(inx) ./ Y_coord(inx));
  else %% if Y_coord == 0
    if X_coord(inx) == 0
      bearing(inx) = 0;
    elseif X_coord(inx) > 0
      bearing(inx) = 90;
    else %% if X_coord < 0
      bearing(inx) = 270;
    end
  end

  if dim_3D
    if range(inx)
      %% get zenith (altitude) in degrees counter-clockwise from xy-plane
      altitude(inx) = (180/pi) * asin(Z_coord(inx) ./ range(inx));
    else
      altitude(inx) = 0;
    end
  end

end

if ~dim_3D
  altitude = [];
end

%% return spherical coords
spher_coords = [range, bearing', altitude'];



return;