function plot_SD_2D_coords_curr(varargin);

%%
%% 'plot_SD_2D_coords_curr'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Plot the current points being searched
%%
%% syntax:
%% -------
%% plot_SD_2D_coords_curr(coords_init, top_coords_init, coords_curr, top_coords_curr,...
%%%  arr_geom, elem_used, ref_chan, event_ID, iteration, num_search_pts, fig_tag, fig_name);
%%
%% input:
%% ------
%% coords_init == spatial coordinates for the initital search points
%% top_coords_init == initial top coordinates in order
%% coords_curr == spatial coordinates for the current search points
%% top_coords_curr == current top coordinates in order
%% arr_geom == array geometry in (x, y, z) meters
%% elem_used == channels used in search
%% ref_chan == reference channel
%% event_ID == event ID
%% iteration == current iteration in loop
%% num_search_pts == number of points being searched
%% fig_tag == diagnostic figure tag
%% fig_name == diagnostic figure name
%%
%% output:
%% -------
%%
%%



for_movie = 0;


%% get the input variables
i = 1;
coords_init = varargin{i}; i = i + 1;
top_coords_init = varargin{i}; i = i + 1;
coords_curr = varargin{i}; i = i + 1;
top_coords_curr = varargin{i}; i = i + 1;
arr_geom = varargin{i}; i = i + 1;
elem_used = varargin{i}; i = i + 1;
ref_chan = varargin{i}; i = i + 1;
event_ID = varargin{i}; i = i + 1;
iteration = varargin{i}; i = i + 1;
num_search_pts = varargin{i}; i = i + 1;
fig_tag = varargin{i}; i = i + 1;
fig_name = varargin{i}; i = i + 1;


%% set up figure
[fh1, axh] = create_standarddiag_fig(fig_tag, fig_name);


%% plot 2D coordinates comparing all intitial search coordinates 
%% to current search coordinates, in 4th panel of big diag figure
axh = axh(4);
set(axh, 'visible', 'on');


%% set titles
th1(1) = get(axh, 'title');
th1(2) = get(axh, 'xlabel');
th1(3) = get(axh, 'ylabel');
set(th1(1), 'string', sprintf('Init & Curr Srch Pts (%d pts)\nEvent #%d : iteration #%d', num_search_pts, event_ID, iteration));
set(th1(2), 'string', 'X (meters)');
set(th1(3), 'string', 'Y (meters)');
set(th1, 'parent', axh, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);

%% plot initial coordinates
clear lh1;
lh1 = line(coords_init(:, 1), coords_init(:, 2), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color',  [.6 .75 .8]);

%% plot initial top coordinates
clear lh1;
lh1 = line(top_coords_init(:, 1), top_coords_init(:, 2), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [.2,.35,.4]);

%% plot current coordinates
clear lh1;
lh1 = line(coords_curr(:, 1), coords_curr(:, 2), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [0, .5, 1]);

%% plot current top coordinates
clear lh1;
lh1 = line(top_coords_curr(:, 1), top_coords_curr(:, 2), 'parent', axh);
set(lh1, 'marker', '.', 'linestyle', 'none', 'color', [1,.25,.25]);

%% plot current top point == location
if for_movie
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', 'p', 'markerfacecolor', [0,1,.5], 'markeredgecolor', [0,0,0],...
    'markersize', 26, 'linewidth', 3);
else
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', '*', 'color', 'k', 'markersize', 12, 'linewidth', 2.5);
  clear lh1;
  lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
  set(lh1, 'linestyle', 'none', 'marker', '*', 'color', [0,1,.5], 'markersize', 8, 'linewidth', 1);
end
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', '*', 'color', 'k', 'markersize', 12, 'linewidth', 2.5);
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', '*', 'color', [0,1,.5], 'markersize', 10, 'linewidth', 1);
% clear lh1;
% lh1 = line(top_coords_curr(1, 1), top_coords_curr(1, 2), 'parent', axh);
% set(lh1, 'linestyle', 'none', 'marker', 'o', 'markerfacecolor', [0,1,.5], 'markeredgecolor', 'k', 'markersize', 5, 'linewidth', 1);

%% plot array
if for_movie
  clear lh1;
  lh1 = line(arr_geom(:, 1), arr_geom(:, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 14, 'markerfacecolor', 'y', 'markeredgecolor', 'k', 'linestyle', 'none', 'linewidth', 2);
else
  clear lh1;
  lh1 = line(arr_geom(:, 1), arr_geom(:, 2), 'parent', axh);
  set(lh1, 'marker', 'p', 'markersize', 11, 'markerfacecolor', 'y', 'markeredgecolor', 'r', 'linestyle', 'none');

  %% indicate the elements actually used
  clear lh1;
  elem_used(elem_used == ref_chan) = [];
  lh1 = line(arr_geom(elem_used, 1), arr_geom(elem_used, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none');
  clear lh1;
  lh1 = line(arr_geom(ref_chan, 1), arr_geom(ref_chan, 2), 'parent', axh);
  set(lh1, 'marker', 'o', 'markersize', 13, 'markerfacecolor', 'none', 'markeredgecolor', 'b', 'linestyle', 'none', 'linewidth', 2);
end

xmin = min([coords_init(:, 1); coords_curr(:, 1); arr_geom(:, 1)]);
xmax = max([coords_init(:, 1); coords_curr(:, 1); arr_geom(:, 1)]);
ymin = min([coords_init(:, 2); coords_curr(:, 2); arr_geom(:, 2)]);
ymax = max([coords_init(:, 2); coords_curr(:, 2); arr_geom(:, 2)]);

set(axh, 'xlim', [xmin, xmax], 'ylim', [ymin, ymax]);

drawnow;


return;