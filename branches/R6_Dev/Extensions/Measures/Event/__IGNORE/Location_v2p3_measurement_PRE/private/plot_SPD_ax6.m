function plot_SPD_ax6(stdplusdiag_tag, stdplusdiag_name,...
  event_id, chan_2_use, ref_chan, abs_pk_thresh, all_unsm_corr_fun_peaks,...
  channels_kept, corr_num, peak_threshold, chan_wts, filter_len, pk_vals);

%%
%% 'plot_SPD_ax6'
%%
%% Kathryn A. Cortopassi
%% August 2006
%%
%% Create 3rd axis of standard plus diagnostic figure
%%
%% syntax:
%% -------
%% plot_SPD_ax6(stdplusdiag_tag, stdplusdiag_name,...
%%  event_id, chan_2_use, ref_chan, abs_pk_thresh, all_unsm_corr_fun_peaks,...
%%  channels_kept, corr_num, peak_threshold, chan_wts, filter_len, pk_vals);
%%
%% input:
%% ------
%% internaldiag_tag == figure tag
%% internaldiag_name == figure name
%% event_id == event ID number
%% chan_2_use == channels selected by user
%% ref_chan == reference channel
%% abs_pk_thresh == absolute correlation peak threshold
%% all_unsm_corr_fun_peaks == raw correlation peaks
%% channels_kept == channels retained by location process
%% corr_num == correlation functions used
%% peak_threshold == relative correlatoin peak threshold
%% chan_wts == correlation function weights
%% filter_len == filter lenght for correlation smoothing
%% pk_vals == smoothed correlation peaks
%%
%% output:
%% -------
%%


[figh_spd, axh] = create_stdplusdiag_fig(stdplusdiag_tag, stdplusdiag_name);

axh = axh(6);

inx = 1;
h(inx) = text(-.25, 1.05, sprintf('Run Information for Event #%d', event_id), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .9, sprintf('Ch Given = %s\n', num2str(chan_2_use, '%6d')), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .8, sprintf('Ref  Ch = %s\n', num2str(ref_chan, '%6d')), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .7, sprintf('Abs Pk Thresh = %s\n', num2str(abs_pk_thresh, '%6.2f')), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .6, sprintf('All Pk Hts (Raw) = %s\n', num2str(all_unsm_corr_fun_peaks, '%5.2f')), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .5, sprintf('Ch Used = %s\n', num2str(channels_kept, '%6d')), 'parent', axh);
inx = inx + 1;
if strcmpi(corr_num, 'N(N-1)/2');
  h(inx) = text(-.25, .4, sprintf('Rel Pk Thresh = %s\n', num2str(peak_threshold, '%6.2f')), 'parent', axh);
else
  h(inx) = text(-.25, .4, sprintf(''), 'parent', axh);
end
inx = inx + 1;
h(inx) = text(-.25, .3, sprintf('Ch Wts = %s\n', num2str(chan_wts, '%6.2f')), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .2, sprintf('Sm Filt Len = %6.2f', filter_len), 'parent', axh);
inx = inx + 1;
h(inx) = text(-.25, .1, sprintf('All Pk Hts (Sm-Wt) = %s\n', num2str(pk_vals, '%6.2f')), 'parent', axh);


set([axh, h], 'fontname', 'arial',...
  'fontsize', 9,...
  'fontweight', 'demi');
set(h(1), 'fontsize', 10);

drawnow;


return;

