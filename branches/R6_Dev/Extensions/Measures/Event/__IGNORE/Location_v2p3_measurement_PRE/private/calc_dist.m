function array_dist = calc_dist(arr_geom, array_prs);

%%
%% 'calc_dist'
%%
%% Kathryn A. Cortopassi
%% July 2006
%%
%% Calculate all pairwise distances between array elements for the given
%% array geometry (and specified element pairs); this code works for
%% 2D or 3D arrays
%%
%% syntax:
%% -------
%%  array_dist = calc_dist(arr_geom, array_prs);
%%
%% input:
%% ------
%% arr_geom == array geometry (x, y, (z)) in meters
%% array_prs == channels pairs to use in calculation (default: all channel  pairs)
%%
%% output:
%% -------
%% array_dist == all pairwise delays between array elements in seconds (in 'Canary' order)
%%
%%
%%



if ~exist('array_prs', 'var')
  %% create a list of all possible array pairs
  %% ('nchoosek' sorts by 1st column then by 2nd column)
  array_prs = nchoosek(1:size(arr_geom, 1), 2);
end


%% create a matrix of all channel pair positions (x, y, (z)) for the specified channel
%% pairs
pair_position(:, 1, :) = arr_geom(array_prs(:, 1), :);
pair_position(:, 2, :) = arr_geom(array_prs(:, 2), :);

%% calculate the distance between channel pairs
array_dist = sqrt(sum((pair_position(:, 2, :) - pair_position(:, 1, :)).^2, 3));



return;