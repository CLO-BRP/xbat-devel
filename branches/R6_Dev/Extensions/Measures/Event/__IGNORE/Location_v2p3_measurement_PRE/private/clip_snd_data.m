function [snd_data, event_inx] = clip_snd_data(sound, event, event_padpts);

%%
%% 'clip_snd_data'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Get the sound data across all channels associated with the given event; extent is
%% plus-minus the given pad around the reference event; return the sound data and
%% the indices to the reference event
%%
%% syntax:
%% -------
%% function [snd_data, event_inx] = clip_snd_data(sound, event, event_padpts);
%%
%% input:
%% ------
%% sound == sound data structure
%% event == event structure
%% event_padpts == amount of additional signal to grab around the reference event in points
%%
%% output:
%% -------
%% snd_data == multi-channel clip of appropriately padded sound data
%% event_inx == indices to the actual reference event in the sound clip
%%



%% get the sound label
if strcmpi(sound.type, 'file')
  sound_name = [sound.path, sound.file];
else
  sound_name = [sound.path, '*.*'];
end


%% get sound sample rate
fs = sound.samplerate;


%% get reference event channel
ref_chan = event.channel;


%% get position and duration in sound file (stream) of data to clip out
%% (remember, the first element in whole stream is zero)
event_startpt = floor(event.time(1) * fs);
event_durpts = ceil(event.duration * fs);
%% now add end padding
startpt = event_startpt - event_padpts;
totalpts = event_durpts + (2 * event_padpts);


%% check for zero length events
if totalpts == 0
  %% empty event!  skip it
  fprintf('Event #%d is empty for sound ''%s'' skipping...\n', event.id, sound_name);
  %% return empty output
  snd_data = [];
  event_inx = [];
  return;
end


%% check for out of bounds events; a potential problem with all this padding
if (startpt < 0) | (startpt + totalpts > sound.cumulative)
  %% skip this event for now and report
  %% (in future versions, get data needed by reflection at sound boundaries)
  fprintf('Not enough data to process event #%d \nfor sound ''%s'' skipping...\n', event.id, sound_name);
  %% return empty output
  snd_data = [];
  event_inx = [];
  return;
end


try
  %% get data from all channels and de-mean; channels returned in columns
  snd_data = sound_read(sound, 'samples', startpt, totalpts);
  snd_data = snd_data - ones(totalpts, 1) * mean(snd_data);

  %% get the event indices
  event_inx = [event_padpts+1:totalpts-event_padpts];

catch
  %% problem reading sound data; skip this event for now and report
  fprintf('Problem reading sound data for event #%d \nfor sound ''%s'' skipping...\n', event.id, sound_name);
  %% return empty output
  snd_data = [];
  event_inx = [];
  return;

end


%% some diagnostic code for debugging
plot_diag = 0;
%% plot the clipped data
if plot_diag
  figure;
  for inx = 1:sound.channels
    subplot(sound.channels, 2, (inx-1)*2+1);
    plot(snd_data(:, inx));
    subplot(sound.channels, 2, (inx-1)*2+2);
    specgram(snd_data(:, inx),512, fs);
  end
  drawnow;
  fprintf('Waiting... Press <return> to continue.\n');
  pause;
end


return;