function plot_SD_corr_fun(varargin);

%%
%% 'plot_SD_corr_fun'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Plot the corr functions and top location re: peaks (a la APP)
%%
%% syntax:
%% -------
%% plot_SD_corr_fun(corr_fun, arr_geom, elem_pairs, speed_of_sound, Fs, event_ID, source_coord, iteration, fig_tag, fig_name);
%%
%% input:
%% ------
%% corr_fun == all pairwise correlation functions in order calculated
%% arr_geom == array geometry in x,y,(z) meters
%% elem_pairs == all element pairs actually used in the order calculated (same order as corr functions)
%% speed_of_sound == speed of sound
%% Fs == sound sampling rate
%% event_ID === log event ID number
%% source_coord == spatial coordinates for source location
%% iteration == current iteration of search loop
%% fig_tag == diagnostic figure tag
%% fig_name == diagnostic figure name
%%
%% output:
%% ------
%%
%%
%% Modifications:
%% June 2005-- to get rid of 'Canary' order use; now elem_pair indices provided in same order as corr funs
%%
%%


for_movie = 0;


%% get the input variables
i = 1;
corr_fun = varargin{i}; i = i + 1;
arr_geom = varargin{i}; i = i + 1;
elem_pairs = varargin{i}; i = i + 1;
speed_of_sound = varargin{i}; i = i + 1;
Fs = varargin{i}; i = i + 1;
event_ID = varargin{i}; i = i + 1;
source_coord = varargin{i}; i = i + 1;
iteration = varargin{i}; i = i + 1;
fig_tag = varargin{i}; i = i + 1;
fig_name = varargin{i}; i = i + 1;


%% set up figure
[fh1, axh] = create_standarddiag_fig(fig_tag, fig_name);


%% Plot the corr functions and time delays associated with top
%% location re: peak locations, in 3rd panel of a big diagnostic plot
axh = axh(3);
set(axh, 'visible', 'on');


%% get lag vector on seconds
tot_num_lags = size(corr_fun, 1);
bin_offset = ceil(tot_num_lags / 2); %% == maxlag + 1 bins
lags = ((1:tot_num_lags) - bin_offset) / Fs;

%% get xcorr pair labels
num_prs = length(elem_pairs);
for inx = 1:num_prs
  pair_labels{inx} = [num2str(elem_pairs(inx, 1)), 'x', num2str(elem_pairs(inx, 2))];
end


%% plot corr functions as image
%% 'image' clears out the axes so must add tag back below
axh_tag = get(axh, 'tag');
ih1 = image('parent', axh);
set(ih1, 'cdata', flipud(corr_fun'), 'xdata', lags, 'ydata', 1:num_prs, 'cdatamapping', 'scaled');
set(axh, 'clim', [min(corr_fun(:)), max(corr_fun(:))],...
  'ytick', 1:num_prs,...
  'yticklabel', fliplr(pair_labels),...
  'ylim', [0.5, num_prs+0.5],...
  'xlim', [min(lags), max(lags)]);
%colorbar('peer', axh);

%% 'image' clears out the tag so I have to reset it
%% also sits itself on top, so I need to push axis up
set(axh,...
  'tag', axh_tag,...
  'layer', 'top');


%% set titles
th1(1) = get(axh, 'title');
th1(2) = get(axh, 'xlabel');
th1(3) = get(axh, 'ylabel');
set(th1(1), 'string', sprintf('Corr Fun Pks re: Curr Best Loc\nEvent #%d : Iteration #%d', event_ID, iteration));
set(th1(2), 'string', 'Corr Lag (sec)');
set(th1(3), 'string', 'Array Pair');
set(th1, 'parent', axh, 'color', 'k', 'fontname', 'arial', 'fontweight', 'demi', 'fontsize', 11);

%% mark corr fun peaks
[max_val, max_inx] = max(fliplr(corr_fun));
lh1 = line((max_inx - bin_offset) / Fs, 1:num_prs, 'parent', axh);
set(lh1, 'marker', 'x', 'markersize', 10, 'linestyle', 'none', 'color', 'r', 'linewidth', 2);
if for_movie
  set(lh1, 'markersize', 14, 'linewidth', 3);
end

%% mark time delays associated with current top location
delta_t = calc_delays(arr_geom, flipud(elem_pairs), source_coord, speed_of_sound);

%% mark current top location
clear lh1;
lh1 = line(delta_t, 1:num_prs, 'parent', axh);
set(lh1, 'marker', 'o', 'markersize', 10, 'linestyle', 'none', 'color', 'b', 'linewidth', 2);
if for_movie
  set(lh1, 'markersize', 14, 'linewidth', 3);
  %saveas(fh1, ['fig', datestr(now, 30), '.jpg'], 'jpg');
end


drawnow;


return;








%%*************************************************************************************
%%*************************************************************************************
%%*************************************************************************************




function delta_t = calc_delays(arr_geom, elem_pairs, source_coord, speed_of_sound);

%%
%% 'calc_delays'
%%
%% Kathryn A. Cortopassi
%% April 2005
%%
%% Calculate the pairwise time delays for the given array and source location;
%% return them in the order specified by elem_pairs
%%
%% syntax:
%% -------
%% delta_t = calc_delays_canord(arr_geom, elem_pairs, source_coord, speed_of_sound);
%%
%% input:
%% ------
%% arr_geom == array geometry, spatial coordinates, in x,y,(z) meters
%% elem_pairs == array element pairs and orders actually used
%% source_coord == spatial coordinates in x,y,(z) meters for source location
%% speed_of_sound == speed of sound in m/s
%%
%% output:
%% -------
%% delta_t = time of arrival differences (time delays, sec) between array
%%           element pairs specified for the given source location
%%



%% get spatial dimensions of array and source
if size(arr_geom, 2) >= 3 & size(source_coord, 2) >= 3
  dim_3D = 1;
else
  dim_3D = 0;
end


%% get XY(Z) coords of array element pairs in order specified
arr_pairs_geom(:, 1, :) = arr_geom(elem_pairs(:, 1), :);
arr_pairs_geom(:, 2, :) = arr_geom(elem_pairs(:, 2), :);


%% calculate the time delay in sec associated with the source for each of the array element pairs
if dim_3D
  delta_t = ( sqrt((source_coord(1)-arr_pairs_geom(:,1,1)).^2 + (source_coord(2)-arr_pairs_geom(:,1,2)).^2  + (source_coord(3)-arr_pairs_geom(:,1,3)).^2) - ...
    sqrt((source_coord(1)-arr_pairs_geom(:,2,1)).^2 + (source_coord(2)-arr_pairs_geom(:,2,2)).^2  + (source_coord(3)-arr_pairs_geom(:,2,3)).^2) ) / speed_of_sound;
else
  delta_t = ( sqrt((source_coord(1)-arr_pairs_geom(:,1,1)).^2 + (source_coord(2)-arr_pairs_geom(:,1,2)).^2) - ...
    sqrt((source_coord(1)-arr_pairs_geom(:,2,1)).^2 + (source_coord(2)-arr_pairs_geom(:,2,2)).^2) ) / speed_of_sound;
end


return;