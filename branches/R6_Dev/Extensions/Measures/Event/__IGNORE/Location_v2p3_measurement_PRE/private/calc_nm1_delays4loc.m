function delta_t = calc_nm1_delays4loc(arr_geom, ref_chan, chan_used, source_coord, speed_of_sound); 

%%
%% 'calc_nm1_delays4loc'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Calculate the ~N-1 pairwise time delays between the ref channel and cross channels
%% for the given array and source location; return them in ascending order
%% 
%% syntax:
%% -------
%% delta_t = calc_nm1_delays4loc(arr_geom, ref_chan, chan_used, source_coord, speed_of_sound);  
%%
%% input:
%% ------
%% arr_geom == array geometry, spatial coordinates, in x,y,(z) meters
%% ref_chan == reference array element number
%% chan_used == array elements to be used 
%% source_coord == spatial coordinates in x,y,(z) meters for source location
%% speed_of_sound == speed of sound in m/s
%%
%% output:
%% -------
%% delta_t = time of arrival differences (time delays, sec) between array
%%           elements and reference channel for the given source location;
%%     


%% make sure channels are in ascending order
chan_used = sort(chan_used);

%% remove ref channel
chan_used(chan_used == ref_chan) = [];


%% get spatial dimensions of array and source
if size(arr_geom, 2) >= 3 & size(source_coord, 2) >= 3
    dim_3D = 1;
else
    dim_3D = 0;
end


%% calculate the time delay in sec associated with the source for each of the array element pairs 
if dim_3D
    
    loc2ref_dist = sqrt((source_coord(1)-arr_geom(ref_chan,1)).^2 + (source_coord(2)-arr_geom(ref_chan,2)).^2  + (source_coord(3)-arr_geom(ref_chan,3)).^2); 
    loc2xchan_dist = sqrt((source_coord(1)-arr_geom(chan_used,1)).^2 + (source_coord(2)-arr_geom(chan_used,2)).^2  + (source_coord(3)-arr_geom(chan_used,3)).^2); 
    delta_t = (loc2ref_dist - loc2xchan_dist) / speed_of_sound;
    
else
    
    loc2ref_dist = sqrt((source_coord(1)-arr_geom(ref_chan,1)).^2 + (source_coord(2)-arr_geom(ref_chan,2)).^2); 
    loc2xchan_dist = sqrt((source_coord(1)-arr_geom(chan_used,1)).^2 + (source_coord(2)-arr_geom(chan_used,2)).^2); 
    delta_t = (loc2ref_dist - loc2xchan_dist) / speed_of_sound;
    
end


return;