function array_delay = calc_delays(mode, arr_geom, speed_of_sound, array_prs);

%%
%% 'calc_delays'
%%
%% Kathryn A. Cortopassi
%% May 2005
%%
%% Calculate all pairwise delays between array elements for the given
%% array geometry, speed of sound (and specified element pairs); depending
%% on the mode, return the straight delay values, or add a bit of slop (1%)
%% to account for possible ambiguity in array position; this code works for
%% 2D or 3D arrays
%%
%% syntax:
%% -------
%%  array_delay = calc_delays(mode, arr_geom, speed_of_sound);
%%  array_delay = calc_delays(mode, arr_geom, speed_of_sound, array_prs);
%%
%% input:
%% ------
%% mode == method of delay calc, 'straight' or 'fuzzy' (default: 'straight')
%%         fuzzy adds 1% of calculated values
%% arr_geom == array geometry (x, y, (z)) in meters
%% speed_of_sound == speed of sound in meters per second
%% array_prs == channels pairs to use in calculation (default: all channel  pairs)
%%
%% output:
%% -------
%% array_delay == all pairwise delays between array elements in seconds (in 'Canary' order)
%%
%%
%%
%% Modifications:
%% June 2005-- to get rid of 'Canary' order use; now array_prs provides the proper channel order
%%


if ~exist('array_prs', 'var')
  %% create a list of all possible array pairs
  %% ('nchoosek' sorts by 1st column then by 2nd column)
  array_prs = nchoosek(1:size(arr_geom, 1), 2);
end


%% create a matrix of all channel pair positions (x, y, (z)) for the specified channel
%% pairs
pair_position(:, 1, :) = arr_geom(array_prs(:, 1), :);
pair_position(:, 2, :) = arr_geom(array_prs(:, 2), :);

%% calculate the distance between channel pairs
array_dist = sqrt(sum((pair_position(:, 2, :) - pair_position(:, 1, :)).^2, 3));

%% convert this to a delay in seconds for each pair
array_delay = array_dist / speed_of_sound;


if strcmpi(mode, 'fuzzy')
  %% make delays a little longer to deal with possible ambiguity in
  %% array positioning; how much longer?
  %% for now, just add 1% of calculated values
  array_delay = array_delay * 1.01;
end


return;