function [corr_functions, array_prs, lag_vec] =...
  calc_time_corr(snd_data, event_inx, ref_chan, array_prs, array_lag, max_lag, max_ev_width, fs,...
  corr_type, display_plots, version_num);


%%
%% 'calc_time_corr'
%%
%% Kathryn A. Cortopassi
%% June 2005
%%
%% Calculate normalized (0-1) time correlation functions
%% (set up similarly to KJD/KMF 'normcorr', but with many changes)
%%
%% syntax:
%% -------
%% function [corr_functions, array_prs, raw_peaks, standard_dev, lag_vec] =...
%%  calc_time_corr(snd_data, event_inx, ref_chan, array_prs, array_lag, max_lag, max_ev_width, fs,...
%%  corr_type, display_plots, version_num);
%%
%%
%% input:
%% ------
%% snd_data == all time waveforms
%% event_inx == index vector to the reference event
%% ref_chan == reference channel number
%% array_prs == array of all channel pairs in same order as 'array_lag'
%% array_lag == all delays (in points) between channel pairs in same order as 'array_prs'
%% max_lag == max delay for all channel pairs (in points)
%% max_ev_width == max width of actual cross channel event (in points) based on global reference event
%% fs == sound sample rate
%% corr_type == type of correlation to perform
% %% scale == flag for whether xcorr funs should be scaled by xchan energy
% %% standardize == flag for whether xcorr funs should be standardized
%% display_plots == type of diagnostic display to show
%% version_num == location measure version number
%%
%% output:
%% -------
%% corr_functions == the standardized correlation functions (all correlations with ref chan 2nd)
%% array_prs == the channels pairs actually correlated (all others with reference channel)
%% with ref chan always second in order to match how correlations were done
%% raw_peaks == peak values of raw xcorr functions (before any possible scaling or standardization)
%% standard_dev == standard deviation of xcorr fun (with whatever scaling (or not) done)
%% lag_vec == lag vector for the correlation functions in seconds
%%

%%
%% modifications made by KAC Sept 2005
%% %% modifications made by KAC October 2005
%%



%% set flag to show diagnostic plots
[plot_text_diag, plot_standard_diag, plot_standard_plus_diag, plot_deep_diag, plot_deep_plus_diag]...
  = get_diag_plot_levels(display_plots);


if plot_deep_diag
  deepdiag_tag = ['deep_diagnostics_for_location_', version_num, '_calc_time_corr'];
  deepdiag_name = ['Deep Diagnostics for Source Location Measurement ', version_num, ' : ''Time Corr Calc'''];
  figh = create_deepdiag_fig(deepdiag_tag, deepdiag_name);
  lbwh = get(figh, 'position');
  lbwh_scr = get(0, 'screensize');
  set(figh, 'position', [4, lbwh(2), lbwh_scr(3)-4, lbwh(4)]);
  delete(allchild(figh));
end


%% get the index to those pairs that contain the reference channel
%% (so ultimately, do N-1 correlations)
pr_inx = find(array_prs(:, 1) == ref_chan | array_prs(:, 2) == ref_chan);

%% keep only those pairs containing the reference channel
array_prs = array_prs(pr_inx, :);


%% keep only delays for those pairs containing the reference channel
array_lag = array_lag(pr_inx);


%% get total max delay in seconds
max_delay = max_lag / fs;

%% calculate the lag vector (in sec) for the xcorr functions
lag_vec = [-max_lag:max_lag] / fs;


% if strcmpi(scale, 'yes')
%   %% calculate the sum of squares for the reference channel event
%   %% this is also equal to the peak value of the raw autocorr fun
%   %ref_chan_ss = sum(snd_data(event_inx, ref_chan).^2);
%
%   % %% calculate the square-root of the sum of squares for the reference channel event
%   ref_chan_srss = sqrt(sum(snd_data(event_inx, ref_chan).^2));
% end
% %% calculate the square-root of the sum of squares for the reference channel event
% ref_chan_srss = sqrt(sum(snd_data(event_inx, ref_chan).^2));
%% calculate the sum of squares for the reference channel event
ref_chan_ss = sum(snd_data(event_inx, ref_chan).^2);

%% get the total signal length
snd_data_len = size(snd_data, 1);
%% set up a template of zeros
tempsig = zeros(snd_data_len, 1);

%% get the reference channel data zeroed around event bounds
refsig = tempsig;
refsig(event_inx) = snd_data(event_inx, ref_chan);

%% and the binarized reference signal
bin_refsig = tempsig;
bin_refsig(event_inx) = 1;


%% perform the correlations for all pairs (all other channels with ref channel)
num_prs = size(array_prs, 1);

%% set up corr fun matrix
corr_fun_len = 2 * max_lag + 1;
corr_functions = zeros(corr_fun_len, num_prs);

% %% just to be sure it's what I think it is...
% fprintf(1, 'length of 2*max_lag+1 = %d\n', 2*max_lag+1);



if strcmpi(corr_type, 'time_ce')
  complex_envelope = 1;
  if (plot_text_diag) fprintf('(using time waveform correlation with complex envelope)\n'); end
else
  complex_envelope = 0;
  if (plot_text_diag) fprintf('(using time waveform correlation)\n'); end
end

if plot_deep_plus_diag
  fprintf(1, 'length of snd data = %d\n', snd_data_len);
  fprintf(1, 'length of padded corr fun = %d\n', size(corr_functions, 1));
  array_prs
  array_lag
  fprintf(1, 'max_lag+1 = %d\n', max_lag+1);
end


%% intialize peak and stddev arrays
raw_peaks = zeros(1, num_prs);
standard_dev = zeros(1, num_prs);



for loop_inx = 1:num_prs

  %% get the index for the cross-channel data
  x_inx = max(1, event_inx(1)-array_lag(loop_inx)):min(snd_data_len, event_inx(end)+array_lag(loop_inx));


  if array_prs(loop_inx, 1) == ref_chan
    %% the x-channel is in column 2
    %% get the x-channel signal zeroed beyond maxlag for the pair (so no zeroing need be done later)
    %% ==> okay, this isn't totally correct about the zeroing, because of the way MATLAB does the xcorr
    %% using FFT's, the parts that should be zero are not, rather they are small non-zero; think
    %% about whether or not this is a big deal XXX
    %% 14 Sept 2005: looking into a weird behavior that CWC observed with this version-- 'weird shadows'--
    %% these are due precisely to this close-to but not exactly zero value of the xcorr function and the
    %% funny scaling and standardizing that happens below; based on this, maybe I should go ahead and
    %% zero the xcorr function directly?; also, it wouldn't help speed-wise to just run the short
    %% ref event clip against the longer clip (as determined by array delay) since signals get zero-padded
    %% anyway in the xcorr function (since correlation is done in frequency domain); however, maybe I
    %% should run just the section required for the specific sensor pair, and insert the xcorr result
    %% where it belongs in a zero-sequence
    xsig = snd_data(x_inx, array_prs(loop_inx, 2));

    %% switch pair order around so ref chan is always second in the pair order
    array_prs(loop_inx, 1) = array_prs(loop_inx, 2);
    array_prs(loop_inx, 2) = ref_chan;

  else
    %% the x-channel is in column 1
    %% get the x-channel signal zeroed beyond maxlag for the pair (so no zeroing need be done later)
    %% ==> okay, this isn't totally correct about the zeroing, because of the way MATLAB does the xcorr
    %% using FFT's, the parts that should be zero are not, rather they are small non-zero; think
    %% about whether or not this is a big deal XXX
    %% 14 Sept 2005: looking into a weird behavior that CWC observed with this version-- 'weird shadows'--
    %% these are due precisely to this close-to but not exactly zero value of the xcorr function and the
    %% funny scaling and standardizing that happens below; based on this, maybe I should go ahead and
    %% zero the xcorr function directly?; also, it wouldn't help speed-wise to just run the short
    %% ref event clip against the longer clip (as determined by array delay) since signals get zero-padded
    %% anyway in the xcorr function (since correlation is done in frequency domain); however, maybe I
    %% should run just the section required for the specific sensor pair, and insert the xcorr result
    %% where it belongs in a zero-sequence
    xsig = snd_data(x_inx, array_prs(loop_inx, 1));

  end

  %% always correlate in order x-chan x ref-chan, so xcorr function indices can be
  %% used directly to find location of event match in x-channel (and no index
  %% flipping is needed)

  %% get the cross correlation function using KJD's modified 'xcorr' code
  %% XXX think about re-doing this code (cause filtering aspect does not seem to be used;
  %% maybe we could speed things up) XXX
  %% to save processing time, only correlate over the pair specific lag (this will generate
  %% varying size xcorr functions)
  corrfun = fxcorr([xsig, refsig(x_inx)], array_lag(loop_inx)/fs, [0 fs/2], fs, [], complex_envelope, 'none');

  %% generate index into the full-length corr function for where to insert this
  %% shorter corr fun-- i.e., to the part of the x-corr function within plus-minus
  %% the pair specific lag (the min/max error checking is unnecessary)
  corrfun_inx = max(1, max_lag+1-array_lag(loop_inx)):min(corr_fun_len, max_lag+1+array_lag(loop_inx));


  %% changed, see below this
  %   %% further, find the indices to where the corr function should be exactly zero
  %   %% (even though it should be, it may not be because of round-off error; this will
  %   %% be a problem later when all the funky scaling/standardizing happens); this may be at indices
  %   %% sooner than plus-minus the pair-specific lag if the ref event falls off the end
  %   %% of the sound and only zero segments are being correlated with the cross channel
  %   %   nonzero_inx = max(1, max_lag+1-min(array_lag(loop_inx),event_inx(end))):min(corr_fun_len, max_lag+1+min(array_lag(loop_inx),snd_data_len-event_inx(1)));
  %   %     zero_inx = [1:nonzero_inx(1), nonzero_inx(end):corr_fun_len];
  %   %% let's be even more restrictive, the ref event doesn't have to totally fall off the end, let's say it just has to start going off the end
  %   %% BUT WAIT!!! this only works for the original reference event, not for the subsequent all pairwise xcorr events which could have multiple peaks
  %   %% that need to be looked at-- OKAY, this needs to be redone
  %   %% okay, now we find the more restricted indices marking where the current ref event is just starting to fall off the edge
  %   %% we need this so that we can adjust the normalizing function, and stop it from asymptotically approaching zero and creating
  %   %% weird edge effects (i.e., blowing up the correlation functions at the ends)
  %   %   nonzero_inx = max(1, max_lag+1-min(array_lag(loop_inx),event_inx(1))):min(corr_fun_len, max_lag+1+min(array_lag(loop_inx),snd_data_len-event_inx(end)));
  %   %   zero_inx = [1:nonzero_inx(1), nonzero_inx(end):corr_fun_len];

  %% further, find the indices to where the corr function should be exactly zero
  %% (even though it should be, it may not be because of round-off error; this will
  %% be a problem later when all the normalization-scaling happens); this may be at indices
  %% sooner than plus-minus the pair-specific lag if the ref event falls off the end
  %% of the sound and only zero segments are being correlated with the cross channel
  %% in fact, be even more restrictive, the ref event doesn't have to totally fall off the end,
  %% let's say it just has to start going off the end, but not right at the first edge,
  %% but when it has gone off the end to the point where there is only max_ev_width length
  %% of the ref event left
  %% to take care if this, calculate new values of the event edge points that are themselves
  %% max_ev_width away from the actual extreme edge
  %% (NB: we don't need to do this for the original reference event, but we do need to do
  %% this for all the subsequent pairwise xcorr events which could have multiple peaks
  %% and thus event widths wider than the original ref event; for the original ref ev,
  %% ev1_prime = event_inx(1) and ev2_prime = event_inx(end), so this generalizes)
  ev1_prime = event_inx(end) - max_ev_width + 1;
  ev2_prime = event_inx(1) + max_ev_width - 1;
  nonzero_inx = max(1, max_lag+1-min(array_lag(loop_inx),ev1_prime)):min(corr_fun_len, max_lag+1+min(array_lag(loop_inx),snd_data_len-ev2_prime));
  zero_inx = [1:nonzero_inx(1), nonzero_inx(end):corr_fun_len];

  %% keep only the final non-zero part of the short corr fun to work with, insert into the full-length holder later
  short_corr_inx = max(1, 1+nonzero_inx(1)-corrfun_inx(1)):min(length(corrfun), length(corrfun)-corrfun_inx(end)+nonzero_inx(end));

  corrfun = corrfun(short_corr_inx);

  %% save the value of the peak before any further processing
  raw_peaks(loop_inx) = max(corrfun);


  %% always scale
  %   if strcmpi(scale, 'kurt''s way')
  %     %% get the function for scaling the correlation functions
  %     %% (this is the windowed energy in the x-channel at all lags, with the window equal
  %     %% to the binarized reference event)
  %     %% XXX think about a way to speed this up later XXX
  %     corrfun_scale = xcorr(xsig.^2, bin_refsig(x_inx), array_lag(loop_inx), 'none');
  %
  %     %% further adjust the correlation scaling function
  %     %% set any negative values to zero
  %     corrfun_scale(corrfun_scale < 0) = 0;
  %
  %     %% take square root, and scale by the sqr-root of the total energy in
  %     %% the ref event
  %     corrfun_scale = sqrt(corrfun_scale) * ref_chan_srss;
  %     %corrfun_scale = sqrt(corrfun_scale) * sqrt(ref_chan_ss);
  %
  %     %% limit how small the values of the scaling function can go
  %     %% (prevent the moat around the peak problem)
  %     allowed_multiple = 100;
  %     corrfun_scale_thresh = max(corrfun_scale) / allowed_multiple;
  %     corrfun_scale(corrfun_scale < corrfun_scale_thresh) = corrfun_scale_thresh;
  %
  %     corrfun_scale = corrfun_scale(short_corr_inx);
  %
  %   elseif strcmpi(scale, 'yes')
  %% get the function for scaling the correlation functions
  %% (this is the windowed energy in the x-channel at all lags, with the window equal
  %% to the binarized reference event)
  %% this normalizes the xcorr fun at each lag with the correct
  %% normalization factor, which equals the square-root of the sum-squared
  %% values in the cross channel over the event duration (i.e., over the
  %% part of the cross-channel overlapping with the ref ev) the result
  %% will be that if a signal exactly matching the shape of the ref ev is
  %% found in the xchan, the xcorr value will be 1)
  %% XXX think about a way to speed this up later XXX
  % corrfun_scale = xcorr(xsig.^2, bin_refsig(x_inx), array_lag(loop_inx), 'none');
  corrfun_scale_xchan = xcorr(xsig.^2, bin_refsig(x_inx), array_lag(loop_inx), 'none');

  %% get scaling for ref channel; it won't be just a constant here, since I allow
  %% the ref event to fall off the edges (partially) in the search for the right
  %% peak, the normalization value for the ref channel will decrease at the edges
  %% (but, should never go to zero since it stops before it totally falls off)
  tempsig = refsig(x_inx).^2;
  corrfun_scale_refchan = flipud(ref_chan_ss - cumsum(tempsig(1:min(array_lag(loop_inx), end))));
  tempsig = flipud(tempsig);
  corrfun_scale_refchan = [corrfun_scale_refchan; ref_chan_ss;...
    ref_chan_ss - cumsum(tempsig(1:min(array_lag(loop_inx), end)))];

  %% there should be no negative values

  %   %% take square root, and multiply by the sqr-root of the sum-squared
  %   %% values in ref ev
  %% take square root of both the xchan and refchan scaling functions
  %% and multiple together element-wise
  %% so now we have the normalization factor for the ref chan and the
  %% normalization factor for the cross channel at all lags that we
  %% can apply by simply dividing the raw xcorr fun
  %     corrfun_scale = sqrt(corrfun_scale) * ref_chan_srss;
  corrfun_scale = sqrt(corrfun_scale_xchan) .* sqrt(corrfun_scale_refchan);

  %% truncate to extend over short non-zero segment only
  corrfun_scale = corrfun_scale(short_corr_inx);

  %   else
  %     corrfun_scale = ones(size(corrfun));
  %
  %   end


  if plot_deep_diag
    num_col = 6;
    axh(1, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+1, 'parent', figh);
    plot(xsig, 'b-', 'parent', axh(1, loop_inx));
    line(1:length(refsig(x_inx)), refsig(x_inx), 'color', 'r', 'linestyle', ':', 'parent', axh(1, loop_inx));
    th(1, 2+loop_inx) = ylabel(sprintf('Ch %d & %d', array_prs(loop_inx, 1), ref_chan), 'parent', axh(1, loop_inx));
    if (loop_inx == 1) th(1, 1) = title(sprintf('XCh & RefCh'), 'parent', axh(1, loop_inx)); end
    if (loop_inx == num_prs) th(1, 2) = xlabel('Time (bins)', 'parent', axh(1, loop_inx)); end
    set(axh(1, loop_inx), 'xlim', [0, length(refsig(x_inx))]);

    axh(2, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+2, 'parent', figh);
    plot(xsig.^2, 'b-', 'parent', axh(2, loop_inx));
    line(1:length(refsig(x_inx)), max(xsig.^2)*bin_refsig(x_inx), 'color', 'r', 'linestyle', ':', 'parent', axh(2, loop_inx));
    %th(2, 2+loop_inx) = ylabel(sprintf('ch %d & %d', array_prs(loop_inx, 1), ref_chan), 'parent', axh(2, loop_inx));
    if (loop_inx == 1) th(2, 1) = title(sprintf('XCh^2 &\nBin RefCh'), 'parent', axh(2, loop_inx)); end
    if (loop_inx == num_prs) th(2, 2) = xlabel('Time (bins)', 'parent', axh(2, loop_inx)); end
    set(axh(2, loop_inx), 'xlim', [0, length(refsig(x_inx))]);

    axh(3, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+3, 'parent', figh);
    lagvec = (length(corrfun)-1)/2;
    lagvec = [-lagvec:1:lagvec];
    plot(lagvec, corrfun, 'b-', 'parent', axh(3, loop_inx));
    th(3, 2+loop_inx) = ylabel(sprintf('Pair %dx%d',  array_prs(loop_inx, 1), ref_chan), 'parent', axh(3, loop_inx));
    if (loop_inx == 1) th(3, 1) = title(sprintf('XCorr Fun\n(Raw)'), 'parent', axh(3, loop_inx)); end
    if (loop_inx == num_prs) th(3, 2) = xlabel('Lag (bins)', 'parent', axh(3, loop_inx)); end
    set(axh(3, loop_inx), 'xlim', [lagvec(1), lagvec(end)]);

    axh(4, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+4, 'parent', figh);
    plot(lagvec, corrfun, 'b-', 'parent', axh(4, loop_inx));
    line(lagvec, corrfun_scale, 'color', 'r', 'linestyle', ':', 'parent', axh(4, loop_inx));
    %     plot(corr_functions(:, loop_inx));
    %     hold on; plot(corrfun, ':r');
    %th(4, 2+loop_inx) = ylabel(sprintf('%dx%d',  array_prs(loop_inx, 1), ref_chan), 'parent', axh(4, loop_inx));
    if (loop_inx == 1) th(4, 1) = title(sprintf('XCorr (Raw) &\nScale Fun'), 'parent', axh(4, loop_inx)); end
    if (loop_inx == num_prs) th(4, 2) = xlabel('Lag (bins)', 'parent', axh(4, loop_inx)); end
    set(axh(4, loop_inx), 'xlim', [lagvec(1), lagvec(end)]);
    %set(axh(4, loop_inx), 'xticklabel', '');
  end

  if plot_deep_plus_diag
    %     %% just to be sure it's what I think it is...
    %     fprintf(1, 'length of corr fun = %d\n', size(corrfun, 1));
    fprintf(1, 'event_inx(1) = %d\n', event_inx(1));
    fprintf(1, 'event_inx(end) = %d\n', event_inx(end));
    fprintf(1, 'snd_data_len - event_inx(end) = %d\n', snd_data_len-event_inx(end));
    fprintf(1, 'array_lag(loop_inx) = %d\n', array_lag(loop_inx));
    fprintf(1, 'nonzero_inx = %d:%d\n', nonzero_inx(1), nonzero_inx(end));
  end


  %% scale the xcorr function by the root energy in the cross channel signal;
  %% this will normalize to give small peaks a boost
  %% duh! normalize the corr fun but only over the ref ev and part of xchan
  %% sig overlapping with the ref ev, so that if a perfect pattern match is
  %% found within the xchan, the xcorr fun will be 1 at that point
  corrfun  = corrfun  ./ corrfun_scale;

  if plot_deep_diag
    axh(5, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+5, 'parent', figh);
    plot(lagvec, corrfun, 'parent', axh(5, loop_inx));
    %th(5, 2+loop_inx) = ylabel(sprintf('%dx%d',  array_prs(loop_inx, 1), ref_chan), 'parent', axh(5, loop_inx));
    if (loop_inx == 1) th(5, 1) = title(sprintf('XCorr (Norm)\nShort'), 'parent', axh(5, loop_inx)); end
    if (loop_inx == num_prs) th(5, 2) = xlabel('Lag (bins)', 'parent', axh(5, loop_inx)); end
    set(axh(5, loop_inx), 'xlim', [lagvec(1), lagvec(end)]);
  end


  %% but don't standardize
  %   if strcmpi(standardize, 'yes')
  %     %% take the square-root of the xcorr function, preserving sign
  %     corrfun = sign(corrfun) .* sqrt(abs(corrfun));
  %
  %     %% subtract the median of the xcorr function over the non-zero portion
  %     corrfun = corrfun - median(corrfun);
  %
  %     %% calculate the median absolute deviation of the x-corr function from
  %     %% its median (MAD), and use the MAD to estimate the standard deviation
  %     sigma = 1.4826 * median(abs(corrfun));
  %
  %     %% now standardize the x-corr function to z-scores
  %     corrfun = corrfun / sigma;
  %
  %     %% save standard dev of xcorr fun
  %     standard_dev(loop_inx) = 1;
  %
  %   else
  %     %     %% take the square-root of the xcorr function, preserving sign
  %     %     corrfun = sign(corrfun) .* sqrt(abs(corrfun));
  %     %
  %     %     %% subtract the median of the xcorr function over the non-zero portion
  %     %     corrfun = corrfun - median(corrfun);
  %
  %     %     %% get standard dev of xcorr fun
  %     standard_dev(loop_inx) = 1.4826 * median(abs(corrfun - median(corrfun)));
  %     %% get standard dev of xcorr fun
  %     %standard_dev(loop_inx) = 1.4826 * median(abs(corrfun));
  %
  %   end


  %% and threshold any negative values to zero
  corrfun(corrfun < 0) = 0;

  %% insert the short corr fun into the full-length set of correlation functions
  corr_functions(nonzero_inx, loop_inx) = corrfun;


  if plot_deep_diag
    axh(6, loop_inx) = subplot(num_prs, num_col, num_col*(loop_inx-1)+6, 'parent', figh);
    lagvec = (length(corr_functions(:, loop_inx))-1)/2;
    lagvec = [-lagvec:1:lagvec];
    plot(lagvec, corr_functions(:, loop_inx), 'parent', axh(6, loop_inx));
    %th(6, 2+loop_inx) = ylabel(sprintf('%dx%d',  array_prs(loop_inx, 1), ref_chan), 'parent', axh(6, loop_inx));
    if (loop_inx == 1) th(6, 1) = title(sprintf('XCorr (Norm > 0)\nFull'), 'parent', axh(6, loop_inx)); end
    if (loop_inx == num_prs) th(6, 2) = xlabel('Lag (bins)', 'parent', axh(6, loop_inx)); end
    set(axh(6, loop_inx), 'xlim', [lagvec(1), lagvec(end)]);
  end

  %% check for bad values and throw a warning
  if sum(isnan(corr_functions(:, loop_inx))) > 0 | sum(isinf(corr_functions(:, loop_inx))) > 0
    fprintf(1, 'Correlation function for pair %d X %d contains NAN or INF values!\n', array_prs(loop_inx, 1), array_prs(loop_inx, 2));
  end

end



if plot_deep_diag
  axh(7, 1) = axes('parent', figh, 'units', 'normalized', 'position', [0, 0, 1, 1], 'visible', 'off');
  th(7, 1) = text(.02, .03, 'Paused. Press any key to continue...', 'parent', axh(7, 1));

  %% get rid of zero entries
  axh(axh == 0) = axh(7,1);
  th(th == 0) = th(7,1);


  set([axh(:); th(:)], 'fontname', 'arial',...
    'fontsize', 9,...
    'fontweight', 'demi');
  set(th, 'fontsize', 10);
  set(th(7, 1), 'fontsize', 16);
  figure(figh);
  pause;
end


return;