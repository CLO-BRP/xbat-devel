function eventinx = get_page_events(data, loginx)

%%
%% 'get_page_events'
%%
%% hack to get IDs of all events displayed on the current page
%% using code from 'browser_display' function
%%
%% K.A.Cortopassi, 2 September 2005
%%
%% syntax:
%%   eventinx = get_page_events(data, loginx)
%%
%% input:
%%   data == browser figure data
%%   loginx == log number (index)
%%
%% output:
%%   eventinx == event numbers (indices)
%%

%% 21 November 2006
%% updatefor PRE release to deal with 
%% time code info on sounds

%% from browser_display lines 91-99
%--
% copy basic page parameters
%--


%% update for PRE release
page.start = data.browser.time;
page.duration = data.browser.page.duration;
page.channel = get_channels(data.browser.channels);
sound = data.browser.sound;

eventinx = find(event_in_page(data.browser.log(loginx).event, page, sound));

return;




%% *** no longer called ***

t = data.browser.time;

dt = data.browser.page.duration;

ch = get_channels(data.browser.channels);


%% from browser_display line 107
nch = length(ch);


%% from browser_display lines 1416-1497
%--
% display page visible events from logs
%--

t1 = t;
t2 = t + dt;

%% KAC COMMENT OUT
%for k = 1:length(data.browser.log)
%% KAC ADDITION
k = loginx;

%--
% check log visibility
%--

%% KAC COMMENT OUT
%if (data.browser.log(k).visible && data.browser.log(k).length)

%--
% time selection
%--

if (data.browser.log(k).length > 1)
  t = struct_field(data.browser.log(k).event,'time');
else
  t = struct_field(data.browser.log(k).event(1),'time');
end

% NOTE: time selection considers the following possibilities: the
% start time is within the page, the end time is within the
% page, and the event stradles the page. all the conditions
% are strict inequalities, we should not be so unlucky as
% to have things appearing and disappearing due to this.

ix = find( ...
  ((t(:,1) > t1) & (t(:,1) < t2)) | ...
  ((t(:,2) > t1) & (t(:,2) < t2)) | ...
  ((t(:,1) < t1) & (t(:,2) > t2)) ...
  );

%--
% joint channel selection
%--

if (~isempty(ix))

  %--
  % this uses an integer look up table
  %--

  % convert channel indices to integers

  c = struct_field(data.browser.log(k).event(ix),'channel');
  c = uint8(c);

  % consider zero offset

  T = zeros(1,nch + 1);
  T(ch + 1) = 1;

  % select indices using look up table

  ix2 = find(lut_apply(c,T));

  % dereference indices

  if (~isempty(ix2))
    ix = ix(ix2);
  else
    ix = [];
  end

end

%% KAC COMMENT OUT
%     %--
%     % display events from log
%     %--
%
%     if (~isempty(ix))
%       event_view('sound',h,k,ix,ax,data);
%     end

%% KAC COMMENT OUT
%end

%% KAC COMMENT OUT
%end


%% KAC ADDITION
eventinx = ix;

return;
