function [ energy_peak_partial, sensor_pos_partial, clock_drift_partial, sound_speed_partial ] = calc_partials(corr_fun, arr_geom, elem_pairs,...
  Fs, speed_of_sound, mean_type, coord_sys, channels_kept, top_loc, top_corr_sum, delta, centroid_dist)

%CALC_PARTIALS computes the partial derivatives of the energy peak value,
% varying several different variables.
%
%   Computes the partial derivatives of the energy function with respect 
%   to several variable.  From these partials we can calculate the
%   estimated error (i.e. the total derivative).
%
%   The code employs Ridder's method for computing numerical derivatives.
%   See Press, W.H. et al., _Numerical Recipes Third Edition_, 2007 pp. 229
%
%   Dean Hawthorne  4 Sept 2012
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Inputs:
%     Far too many inputs
% Outputs:
%   energy_peak_partial - a 1x2 or 1x3 vector containing the slope of the 
%      energy vs. source position curve in each direction
%      evaluated at a point 1/2 of the centroid distance away from the
%      peak.  If we evaluate at the peak, we should get numbers close to
%      zero, which messes up the computation of the total derivative (done
%      by the caller).
%   sensor_pos_partial - a (# sensors)x2 or (# sensors)x3 vector containing 
%      the partial derivatives of the energy peak with repect to each 
%      coordinate of each sensor
%   clock_drift_partial - NOT YET IMPLEMENTED
%   sound_speed_partial - a scalar containing the partial derivatives of the
%      energy peak with repect to the (scalar) speed of sound
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Modification history:
% 4 Sep 2012 - Dean Hawthorne
%     Initial working revision using Ridder's method for computing
%     numerical derivatives.
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    num_sensors = size(arr_geom, 1);
    num_dim = size(arr_geom, 2);
    energy_peak_partial = zeros(1,num_dim);
    sensor_pos_partial = zeros(num_sensors, num_dim);
    clock_drift_partial = zeros(num_sensors);
    sound_speed_partial = 0.0;
    
    %% Save a copy of the original variable values
    top_loc_orig = top_loc;
    arr_geom_orig = arr_geom;
    speed_of_sound_orig = speed_of_sound;

    %%
    %% Partial derivatives of the correlation sum (energy) with respect to
    %% source location
    %%
    %% First find a meaningful distance scale to take the derivative
    deltas = 0.5 * centroid_dist;
    vary = 'source_location'
    for component_to_vary = 1:num_dim
        [energy_peak_partial(component_to_vary), error] = ridder(@corr_sum, top_loc(1,component_to_vary) + deltas, 0.25 * centroid_dist );
        if( abs(error) > abs( 0.01 * energy_peak_partial(component_to_vary)) )
            fprintf(1,'Warning: high error returned by ridder method (source location partial)');
        end
    end
    
    %%
    %% Partial derivatives of the correlation sum (energy) with respect to
    %% sensor positions
    %%
    vary = 'array_geom'
    for ii = 1:num_sensors
        sensor_to_vary = ii;
        for jj = 1:num_dim
            component_to_vary = jj;
            %% Reset the array geometry
            arr_geom = arr_geom_orig;
            [sensor_pos_partial(ii,jj), error] = ridder(@corr_sum, arr_geom(ii,jj), 20*delta); %% Perhaps we can absorb factor of 20 into the calling function
            if( abs(error) > abs( 0.01 * sensor_pos_partial(ii,jj)) )
                fprintf(1,'Warning: high error returned by ridder method (array geometry partial)');
            end
        end
    end

    %%
    %% Partial derivatives of the correlation sum (energy) with respect to
    %% speed of sound
    %%
    vary = 'speed_of_sound'
    speed_of_sound = speed_of_sound_orig;
    [speed_of_sound_partial, error] = ridder(@corr_sum, speed_of_sound, 0.2 * speed_of_sound);
    if( abs(error) > abs( 0.01 * speed_of_sound_partial) )
        fprintf(1,'Warning: high error returned by ridder method (speed of sound partial)');
    end

    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Nested function
    %% corr_sum = corr_sum( variable )
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Computes the correlation sum, varying on variable at a time
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    %% Inputs:
    %%     x: the value of the coordinate to bet varied
    %%         The coordinate is selected by setting the parent function's 
    %%         "vary" variable to 'array_geom' or 'sound_speed'
    %% Outputs:
    %%     corr_sum: the correlation sum at the requested point
    %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function [corr_sum] = corr_sum(variable)
        switch vary
            case 'array_geom';
                arr_geom(sensor_to_vary, component_to_vary) = variable;
            case 'source_location'
                top_loc(1,component_to_vary) = variable;
            case 'sound_speed'
                speed_of_sound = variable;
        end
        corr_sum = calc_corr_sum(top_loc(1,:), corr_fun, arr_geom, elem_pairs,...
            Fs, speed_of_sound, mean_type, coord_sys);
        top_loc = top_loc_orig;
        arr_geom = arr_geom_orig;
        speed_of_sound =  speed_of_sound_orig;
    end
end

