function varargout = Location_v2p2_measurement_exporter(calling_mode, varargin);

%%
%% 'Location_v2p2_measurement_exporter'
%%
%% Measurement export description for the Location_v2p2_measurement
%%
%% Kathryn A. Cortopassi, Oct 2005
%%
%% syntax:
%% -------
%%   column_headers = Location_v2p2_measurement_exporter('export_headers', browser_log);
%%
%%   values = Location_v2p2_measurement_exporter('export_values', browser_log, event_inx, meas_inx);
%%
%% input:
%% ------
%%  browser_log = XBAT log structure
%%  event_inx = index for specific event
%%  meas_inx = index for measurement for specific event
%%
%% output:
%% -------
%%  column_headers = column header label strings (as a cell array)
%%                     for the values to be exported
%%  values = measurement values for specific event in order specified
%%           by 'column_headers'
%%

switch (calling_mode)


  case ('export_headers')
    %% get column header labels and format control string
    %% for writing to comma-delimited output file

    browser_log = varargin{1};
    num_chan = browser_log.sound.channels;

    corr_peaks = [];
    for inx = 1:num_chan
      corr_peaks = [corr_peaks, {sprintf('Corr Pk %dxRef', inx)}];
    end

    sm_peaks = [];
    for inx = 1:num_chan
      sm_peaks = [sm_peaks, {sprintf('Pk Sm %dxRef', inx)}];
    end

    peak_lags = [];
    for inx = 1:num_chan
      peak_lags = [peak_lags, {sprintf('Pk Lag %dxRef', inx)}];
    end

    loc_lags = [];
    for inx = 1:num_chan
      loc_lags = [loc_lags, {sprintf('Loc Lag %dxRef', inx)}];
    end

    dist2sens = [];
    for inx = 1:num_chan
      dist2sens = [dist2sens, {sprintf('Dist to %d', inx)}];
    end


    column_headers = [...
      {'Decimal Date-Time'},...
      {'X Location'},...
      {'Y Location'},...
      {'Z Location'},...
      {'Loc Avg Corr Sum'},...
      {'All Locations'},...
      {'Med Cent Dist'},...
      {'Iterations'},...
      {'Channels'},...
      corr_peaks,...
      {'Channel Wts'},...
      sm_peaks,...
      peak_lags,...
      loc_lags,...
      {'Range'},...
      {'Bearing'},...
      {'Elevation'},...
      dist2sens
      ];

    %% make a column vector
    column_headers = column_headers';


    varargout{1} = column_headers;



  case ('export_values')
    %% get the values corresponding to the column header labels defined above

    browser_log = varargin{1};
    event_inx = varargin{2};
    meas_inx = varargin{3};

    num_chan = browser_log.sound.channels;
    ref_chan = browser_log.event(event_inx).channel;

    value_struc = browser_log.event(event_inx).measurement(meas_inx).value;


    values = [];

    
    if ~isempty(browser_log.sound.realtime)
      %% return decimal time with 1-Jan-1900, 00:00:00 as reference
      dectime = (browser_log.sound.realtime + (browser_log.event(event_inx).time(1)) / (86400)) - datenum('01-Jan-1900, 00:00:00');
      values = [values; {num2str(dectime, '%.3f')}];
    else
      values = [values; {'NA'}];
    end


    if ~isempty(value_struc.xlocation)
      values =  [values; {num2str(value_struc.xlocation, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.ylocation)
      values =  [values; {num2str(value_struc.ylocation, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.zlocation)
      values =  [values; {num2str(value_struc.zlocation, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.xyz_corr_sum)
      values =  [values; {num2str(value_struc.xyz_corr_sum, '%.3f')}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.all_locations)
      values =  [values; {'MAT'}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.cent_dist)
      values =  [values; {num2str(value_struc.cent_dist, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.iterations)
      values =  [values; {num2str(value_struc.iterations)}];
    else
      values = [values; {' '}];
    end


    if ~isempty(value_struc.channels_used)
      values =  [values; {num2str(value_struc.channels_used, '%d;')}];
    else
      values = [values; {' '}];
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_vals)
          values = [values; {num2str(value_struc.peak_vals(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    if ~isempty(value_struc.channels_wts)
      values =  [values; {num2str(value_struc.channels_wts, '%.3f;')}];
    else
      values = [values; {' '}];
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_vals_sm)
          values = [values; {num2str(value_struc.peak_vals_sm(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.peak_lags)
          values = [values; {num2str(value_struc.peak_lags(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx, value_struc.channels_used) & (inx ~= ref_chan)
        counter = counter + 1;
        if ~isempty(value_struc.loc_lags)
          values =  [values; {num2str(value_struc.loc_lags(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    if ~isempty(value_struc.range)
      values =  [values; {num2str(value_struc.range, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.bearing)
      values =  [values; {num2str(value_struc.bearing, '%.3f')}];
    else
      values = [values; {' '}];
    end

    if ~isempty(value_struc.elevation)
      values =  [values; {num2str(value_struc.elevation, '%.3f')}];
    else
      values = [values; {' '}];
    end


    counter = 0;
    for inx = 1:num_chan
      if ismember(inx,  value_struc.channels_used)
        counter = counter + 1;
        if ~isempty(value_struc.dist2channels)
          values = [values; {num2str(value_struc.dist2channels(counter), '%.3f')}];
        else
          values = [values; {' '}];
        end
      else
        values = [values; {' '}];
      end
    end


    varargout{1} = values;


end %% switch


return;