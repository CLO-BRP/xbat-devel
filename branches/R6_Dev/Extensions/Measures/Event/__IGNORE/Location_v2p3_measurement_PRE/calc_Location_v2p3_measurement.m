function value = calc_Location_v2p2_measurement(brows_h, sound, event, param, value)

%%
%% 'calc_Location_v2p2_measurement'
%%
%% compute the Location measurement (version 2.2)
%%
%% Kathryn A. Cortopassi, June 2005
%% -----------------------------------------
%%
%% syntax:
%% ------
%% value = calc_Location_v2p2_measurement(sound, event, param, value)
%%
%% input:
%% ------
%%  brows_h - handle to calling browser
%%  sound - sound file structure
%%  event - event structure
%%  param - measurement parameter structure
%%  value - measurement value structure
%%
%% output:
%% -------
%%  value - measurement value structure
%%
%% -----------------------------------------
%%
%% Location measurement based on Kurt Fristrup's 'delay-forming' location
%% method which exploits the relationship between delay-and-sum beamforming
%% and the delay-and-sum of pairwise sensor correlation functions
%% (delay-and-sum beamforming => find delays which maximize energy in the
%% delayed sum of sensor outputs
%% correlation delay-forming => find delays which maximize sum of delayed
%% pairwise correlation functions
%%
%% This is based on the original KATLAB code with some significant
%% algorithmic changes.  All code except for 'fxcorr' and 'freqfilt'
%% has been re-worked and re-written; code has been streamlined where
%% possible, and approaches altered to simplify calculation.  Many new
%% post measurement visuals and diagnostic plots have been added
%%
%% Main modifications to the location algorithm include:
%% 1) normalized correlation functions are returned, there is no more
%% working in standard deviation units; the normalization is over the
%% duration of the marked reference event only, so if there is an exact
%% match found in a cross channel, the correlation value is one there;
%% correlation functions are no longer standardized, the absolute
%% threshold is entered as a value from 0-1, the relative threshold
%% is entered as a percentage (0-100) down from the global peak of the
%% correlation function
%% (regarding the mote areound the peak problem, we could limit the
%% normalization scaling applied to the correlation functions to
%% have a divisor only as small as 100x down from the peak value
%% (i.e., limit the the scaling functions to have values no lower than
%% 100x down from their highest value); while this addresses the 'mote'
%% around the peak problem, it no longer provides for correlation
%% normalization in the mathematical sense, for now, we'll use the
%% scaling function as is to get true normalization)
%% 2) the search for location has been modified to use a stochastic
%% search instead of the previously used genetic search; the search still
%% relies on delay-sum beamforming of the correlation functions to find
%% the best location in space; two starting points for the search are
%% available:
%%   1. the search starts out along the hyperbolic trajectories defined
%%   by the peaks (i.e., pairwise time delays) of the correlation
%%   functions; points are more closely spaced near the foci of the
%%   hyperboli than out on the arms (the length of the arms are defined
%%   by the search range parameter)
%%   2. the search starts out on a regular grid defined by the search
%%   area (search area == search range meters beyond the rectangle
%%   defined by the extent of the array)
%% in both cases, new guesses are generated randomly from the top ~1000
%% points; the initial amount of point jitter is set based on the user
%% defined search area; the jitter is reduced geometrically with each
%% iteration of the search; the jitter defines the allowed deviation in
%% generating new search points from a single parent (x & y jitter
%% independently); the search points are not allowed outside of the
%% search area, and are reflected back in if they try to go outside
%% 3) FIR filter order for bandlimiting is adjusted adaptively
%% 4) the multiplier for correlation smoothing is fixed at 2 (times
%% the filter length defined by the bandwidth calculation)
%% 5) the correlation lags are extended to make sure that all possible
%% current ref channel events (when the cross-channels become the ref
%% channels, and there is ambiguity in the actual position of the signal
%% of interest) get run across the full length of the current cross-channel
%% event; before, the correlation was being cut off too early to make all
%% the necessary comparisons
%% 6) the correlation operation is speed up a bit by only correlating over
%% each sensor pair specific max lag rather than the global array max lag;
%% these shorter corr funs are inserted into the full length correlation
%% function matrix later
%% 7) remove autochannel discard switch, let this be determined simply by
%% height of the absolute threshold (thresh = 0 --> auto-discard off)
%% 8) remove channel weighting switch, now this is always on (I wonder
%% about this though...)
%%
%% By Kathryn A. Cortopassi
%% June 2005, initial conversion from KATLAB
%% October 2005, further modifications to algorithm and processes
%% July 2006
%% 1) change search area calculation to a given radius from the center of
%% the array (change parts of search code accordingly so that points jitter
%% spherically from where they are)
%% 2) add choice of using only N-1 correlation functions for making location
%% estimate
%% 3) add version number variable to prevent typos
%% 4) deal consistently with location dimensions, if search is 2D, just lop
%% off array dimensions to 2D, etc.; change all other code to expect array and
%% search points to have the same dimensionality to start; this will eliminate
%% unnecessary computations
%%




% set source location measurement version number
version_num = 'v2.2';

%% assign corr type name that is used through out aux code
%% (to decouple input dialog label from internal flag)
switch (lower(param.corr_type))
  case ('time waveform')
    corr_type = 'time';
  case ('time waveform w/ complex envelope')
    corr_type = 'time_ce';
  case ('spectrogram')
    corr_type = 'spec';
end

%% set flag to show diagnostic plots
[plot_text_diag, plot_standard_diag, plot_standard_plus_diag, plot_deep_diag, plot_deep_plus_diag]...
  = get_diag_plot_levels(param.display_plots);


%% get the sound label
if (strcmpi(sound.type, 'file')), sound_name = [sound.path, sound.file];
else sound_name = [sound.path, '*.*']; end


%% get information from the sound structure
%arr_geom = sound.geometry.local; %% use built-in function to get array geom
arr_geom = get_geometry(sound, 'local');
speed_of_sound = sound.speed;


%% make sure that speed of sound and array geometry were loaded
pause_time = 3;
%% check if any of the needed sound parameters are empty and throw an error appropriately
if (isempty(arr_geom) && isempty(speed_of_sound))
  fprintf(1, 'Sensor geometry and speed of sound are not available for sound ''%s'' \nMeasurement for event %d terminating...\n', sound_name, event.id);
  errordlg(sprintf('Sensor geometry and speed of sound are not available for sound ''%s'' \n\nMeasurement for event %d terminating...', sound_name, event.id));
  pause(pause_time);
  %% quit
  return;
elseif (isempty(arr_geom))
  fprintf(1, 'Sensor geometry is not available for sound ''%s'' \nMeasurement for event %d terminating...\n', sound_name, event.id);
  errordlg(sprintf('Sensor geometry is not available for sound ''%s'' \n\nMeasurement for event %d terminating...', sound_name, event.id));
  pause(pause_time);
  %% quit
  return;
elseif (isempty(speed_of_sound))
  fprintf(1, 'Speed of sound is not available for sound ''%s'' \nMeasurement for event %d terminating...\n', sound_name, event.id);
  errordlg(sprintf('Speed of sound is not available for sound ''%s'' \n\nMeasurement for event %d terminating...', sound_name, event.id));
  pause(pause_time);
  %% quit
  return;
end


%% make sure that array is proper size for search dimensionality requested
%% and get rid of unnecessary dimensions if needed (to speed calculations)
%% also, ensure that minimum number of channels is set appropriately
arr_dim = size(arr_geom, 2);
if strcmpi(param.search_dim, '2D')
  srch_dim_2d = 1;
  if (param.min_channels < 3), param.min_channels = 3;
    fprintf(1, 'Minimum number of channels is 3 for 2D search. Resetting...\n'); end
  if (arr_dim > 2), arr_geom(:, 3:end) = []; end %% 2D array geom for 2D calcs!
else
  srch_dim_2d = 0;
  if (param.min_channels < 4), param.min_channels = 4;
    fprintf(1, 'Minimum number of channels is 4 for 3D search. Resetting...\n'); end
  if (arr_dim < 3), error('3D search requested but array geometry is not 3D!  Quitting.');
  elseif (arr_dim > 3), arr_geom(:, 4:end) = []; end %% 3D array geom for 3D calcs!
end
clear('arr_dim');


%% get more information from the sound structure
Fs = sound.samplerate;
num_chan = sound.channels;

%% get information from event
ref_elem = event.channel;
flo = event.freq(1);
fhi = event.freq(2);

%% get information from param struct
chan_2_use = param.channels;


%% make sure the user didn't eliminate the reference channel
if (~ismember(ref_elem, chan_2_use))
  % report and quit
  fprintf(1, 'Reference event channel (ch %d) was excluded by user\nMeasurement for event %d terminating...\n', ref_elem, event.id);
  errordlg(sprintf('\nReference event channel (ch %d) was excluded by user                \n\nMeasurement for event %d terminating...\n', ref_elem, event.id));
  pause(pause_time);
  return;
end


%% set up wait bar
num_steps = 10;
waitbar_inx = 0;
wait_bar = waitbar(waitbar_inx/num_steps, '');
set(wait_bar, 'units', 'normalized');
wait_bar_pos = get(wait_bar, 'position');
set(wait_bar, 'position', [.5, .5, wait_bar_pos(3), wait_bar_pos(4)]);





%%~~
%% BEGIN THE MEASUREMENT...
%%~~





%%~~
%% Extract Sound Data for Measurement
%%~~

waitbar(waitbar_inx/num_steps, wait_bar, 'Extracting Sound Data...');
if (plot_text_diag), fprintf('Extracting Sound Data...\n'); end


%% create a matrix of all array element pairs in a standard order
%% ('nchoosek' sorts by column 1st then by column 2nd)
array_prs = nchoosek(1:num_chan, 2);

%% calculate the max delay for all array element pairs in the order
%% provided by array_prs
%% set flag to make the delays a little longer to deal with possible
%% ambiguity in array positioning ('fuzzy' adds 1% of calculated values)
array_delay = calc_delays('fuzzy', arr_geom, speed_of_sound, array_prs);

%% convert delays in seconds to delays in points
array_lag = round(array_delay * Fs);

%% get the max delay in points
%% this is the amount of data to pad around the reference event
%% (however, isn't it true that you don't need the max delay for
%% all pairs together, but for all pairs containing the reference
%% channel only-- this will grab enough of the data to include
%% all the possible signal positions across channels; perhaps this
%% can be updated later)
max_lag = max(array_lag);

%% extract the sound data based on the event bounds and max_lag, return padded,
%% de-meaned data and indices to the reference event (channels in columns)
%% (perhaps I can change this later to grab only the max lag over the pairs
%% including the reference channel)
[snd_data, event_inx] = clip_snd_data(sound, event, max_lag);

%% get length of the sound data chunk
sig_len = size(snd_data, 1);

%% get width of reference event in points
ref_event_width = length(event_inx);


if (isempty(snd_data))
  %% unable to extract necessary sound data, quit
  if (plot_text_diag), fprintf('Unable to extract sound data. Quitting.\n'); end
  close(wait_bar); return;
end

if (plot_text_diag), fprintf('Using channels: %s\n', num2str(chan_2_use)); end

if plot_standard_plus_diag
  stdplusdiag_tag = ['standardplus_diagnostics_for_location_', version_num];
  stdplusdiag_name = ['Standard Plus Diagnostics for Source Location Measurement ', version_num];
  plot_SPD_ax1(stdplusdiag_tag, stdplusdiag_name, snd_data, chan_2_use, Fs, event_inx);
end

if plot_deep_diag
  deepdiag_tag = ['deep_diagnostics_for_location_', version_num, '_sensor_data'];
  deepdiag_name = ['Deep Diagnostics for Source Location Measurement ', version_num, ' : ''Sensor Data'''];
  t_vec = ([1:sig_len] - event_inx(1))/Fs;
  ev_bnd = [t_vec(event_inx(1)), t_vec(event_inx(end))];
  plot_DD_sensor_data(deepdiag_tag, deepdiag_name, snd_data, chan_2_use, ref_elem, t_vec, ev_bnd, [], 1);
end

if plot_deep_plus_diag
  array_prs
  array_lag
  max_lag
end






%% get active signal filter (and its context) from browser and apply to sound if available
[filter_ext, junk_var, context] = get_active_extension('signal_filter', brows_h);

if ~isempty(filter_ext)

  if (plot_text_diag), fprintf('Filtering sound data using "%s"...\n', filter_ext.name); end

  %% set up some minimal context info
  pad_sec = max(array_delay);
  context.page.start = event.time(1) - pad_sec;
  context.page.duration = event.duration + 2*pad_sec;
  context.page.channels = chan_2_use;

  %% apply the signal filter to the selected channels
  snd_data(:, chan_2_use) = apply_signal_filter(snd_data(:, chan_2_use), filter_ext, context);

end





%% if time or time with complex envelope correlation do this--
%% how do I want to deal with the case of spectrogram correlation?
if strcmpi(corr_type, 'time') || strcmpi(corr_type, 'time_ce')


  %%~~
  %% Estimate Event Bandwidth
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Estimating Event Bandwidth...');
  if (plot_text_diag), fprintf('Estimating Event Bandwidth...\n'); end


  %% use the reference event data to estimate the signal bandwidth
  %% for calculation of appropriate smoothing of correlation functions

  %% set FFT size making sure that it does not exceed the ref event length
  fft_sz = min(ref_event_width, 1024);
  %% XXX why this default choice of FFT size? (using historical choice here)
  %% is there any particular reason??

  %% calculate the spectrum (power spectral density) of the reference event
  event_spc = pwelch(snd_data(event_inx, ref_elem), fft_sz);
  nfreqbins = length(event_spc);

  %% convert the event frequency range in Hz to spectrum bins
  freq_range = round([flo, fhi] * fft_sz / Fs);
  %% make sure frequency range is within allowed index bounds
  freq_range(1) = min(nfreqbins, max([freq_range(1), 1]));
  freq_range(2) = max(1, min([freq_range(2), nfreqbins]));
  %% get full indices
  freq_range = freq_range(1):freq_range(2);


  %% determine whether signal is BB or NB (either by it's nature or
  %% by the user defined freq limits); i.e., what frac of the fft_sz
  %% number of bins in the spectrum is occupied by signal of interest?
  %% frac_fft_bins ranges from 0 (NB) to 1 (BB)
  %% (by only looking at frac over the lo-hi freq_range, this
  %% essentially bandlimits the reference event first before making the
  %% bandwidth estimate)
  frac_spc_bins = sum(event_spc(freq_range) / max(event_spc(freq_range))) / fft_sz;

  %% use this to determine the length of the smoothing filter;
  %% narrowband signals (== broad impulse response) get more smoothing
  %% that broadband signals (== narrow impulse response)

  %% set min amount of smoothing for all signal types (i.e., minimum number of
  %% points to average in moving average filter
  min_filt_len = 3;
  filter_len = round(min_filt_len / frac_spc_bins);


  %% get rid of this historical code
  %   filter_len = round(fft_sz * max(event_spc(freq_range)) / sum(event_spc(freq_range)));
  %
  %   %% extend filter length by user specified multiplier
  %   filter_len = round(filter_len * param.filter_length);
  %   %% extend filter length using a fixed multiplier
  %   mult = 2;
  %   filter_len = round(filter_len * mult);


  %% so, how should this be done if the correlation functions are from spectrograms,
  %% this filter length probably isn't appropriate...







  %%~~
  %% Standardize Sound Data
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Standardizing Sound Data...');
  if (plot_text_diag), fprintf('Standardizing Sound Data...\n'); end


  %% standardize the sound data so the noise variance equals 1 on all channels;
  %% use the median abs deviation (MAD) (i.e. median of the absolute deviations
  %% from the median)) to estimate the standard dev of the noise-only (non-event data);
  %% the median of the noise-only data would be max_lag points into the sorted signal
  %% (since we've padded around the event on both sides by max_lag points, and
  %% assuming that the event data is higher amplitude than the non-event data);
  %% assume the mean equals the median, so the demeaned sound data can be used
  %% directly to get the MAD
  %% (BUT, this is the mean of the whole signal, not the noise only section,
  %% how does this affect our conversion of signal amplitude into z-scores?
  %% re: the noise mean and variance?)

  %% determine the order for the FIR bandlimiting (??or, should this go right into 'bandlimitsnd'??)
  data_len = size(snd_data, 1); %% length of data to be filtered
  fftsz = 512; %% # of bins for parsing the frequency range
  k = 5; %% # of frequency bins to use for transition band
  max_ord = 500; %% arbitrary upper limit on order
  c = 2; %% constant for FIR order formula M = c * Fs / delta_f_trans (determined empirically
  %% for a Hamming window FIR using MATLAB 'fdatool')
  %% set the width of the transition band in Hz
  delta_f_trans = k*Fs/fftsz;
  if (flo ~= 0)
    delta_f_trans = min([flo, delta_f_trans]);
  end
  if (fhi ~= Fs/2)
    delta_f_trans = min([(Fs/2-fhi), delta_f_trans]);
  end

  %% set the FIR filter order
  order = min([round(c*Fs/delta_f_trans), floor(data_len/3), max_ord]);

  %% save channels passed in for later reporting
  chan_given = chan_2_use;


  for loop_inx = chan_2_use

    %% first, filter the sound data in the time domain over the event frequency bounds
    snd_data(:, loop_inx) = bandlimsnd(snd_data(:, loop_inx), Fs, flo, fhi, order);

    %% find the median abs dev
    MAD = sort(abs(snd_data(:, loop_inx)));
    MAD = MAD(max_lag);
    %% use it to estimate std dev (of the noise background)
    sigma = 1.4826 * MAD;

    if sigma == 0
      %% this channel contains mostly zeros, remove it from consideration
      %% NOTE: while this is fine for real data which will never have zero values, what
      %% about ideal data which is zero everywhere except during an event; don't we
      %% want our algorithm to generalize to the ideal case?
      if plot_text_diag
        fprintf(1, 'Channel %s contains mostly zero samples. Removing...\n', num2str(loop_inx));
      end
      chan_2_use(chan_2_use == loop_inx) = [];
    else
      %% standardize the sound data (i.e., convert amplitude values to z-scores)
      %% (so that noise var = 1 on all channels; this should have the effect of bringing the data on
      %% all channels to the same "record" level)
      snd_data(:, loop_inx) = snd_data(:, loop_inx) / sigma;
    end

  end


  if plot_standard_plus_diag
    plot_SPD_ax2(stdplusdiag_tag, stdplusdiag_name, snd_data, chan_2_use, Fs, event_inx, order);
  end

  if plot_deep_diag
    plot_DD_sensor_data(deepdiag_tag, deepdiag_name, snd_data, chan_2_use, ref_elem, t_vec, ev_bnd, order, 0);
  end







  %%~~
  %% Correlate Sound Data
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Correlating N-1 Channel Pairs...');
  if (plot_text_diag), fprintf('Correlating N-1 Channel Pairs...\n'); end

  %% get index to the pairs to be used
  pr_inx = ismember(array_prs(:,1), chan_2_use) & ismember(array_prs(:,2), chan_2_use);

  %% trim the array_prs and array_lag matrix accordingly
  array_prs_trm = array_prs(pr_inx, :);
  array_lag_trm = array_lag(pr_inx);

  %% calculate the N-1 cross correlations with the reference channel and return the channel
  %% order in which the calculations were done (this code 'calc_time_corr' always places the
  %% ref chan 2nd in the xcorr order)
  [corr_functions, cf_array_prs] = calc_time_corr(...
    snd_data, event_inx, ref_elem, array_prs_trm, array_lag_trm, max_lag, ref_event_width, Fs,...
    corr_type, param.display_plots, version_num);


  if plot_standard_plus_diag
    plot_SPD_ax3(stdplusdiag_tag, stdplusdiag_name, corr_functions, cf_array_prs, max_lag, Fs);
  end

  %%~~
  %% Auto-Eliminate Channels
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Evaluating for Channel Elimination...');
  if (plot_text_diag), fprintf('Evaluating for Channel Elimination...\n'); end


  %% get peaks values of the normalized xcorr functions
  corr_fun_peaks = max(corr_functions);

  %% save them (raw == prior to smoothing) for later reporting
  unsm_corr_fun_peaks = corr_fun_peaks;
  % if plot_standard_plus_diag
  all_unsm_corr_fun_peaks = corr_fun_peaks;
  % end

  if (plot_text_diag)
    fprintf('N-1 correlation peak heights (before smoothing)\n= %s\n', num2str(unsm_corr_fun_peaks, '%8.3f'));
    fprintf('Correlation threshold = %s\n', num2str(param.abs_pk_thresh, '%8.3f'));
  end

  %% remove cross channels from consideration based on user entered corr fun threshold

  %% get the corr functions (x-channels) to keep based on user-specified fraction
  %% for normalized xcorr funs
  corr_fun_2keep = corr_fun_peaks > param.abs_pk_thresh;

  %% report if channels were removed
  if (length(corr_fun_2keep) < length(corr_fun_peaks))
    waitbar(waitbar_inx/num_steps, wait_bar, sprintf('Eliminating Channels...'));
    if (plot_text_diag), fprintf('Eliminating Channels...\n'); end
  end

  %% see if the number of kept channels is within max range
  num_chan_2keep = sum(corr_fun_2keep) + 1;

  if (num_chan_2keep > param.max_channels)
    %% throw out worst channels to get down to max allowed number
    if (plot_text_diag), fprintf('Removing channels to reach allowed maximum.\n'); end

    %% sort corr fun peaks in ascending order to get the inx of functions to keep
    [sorted_peaks, sort_inx] = sort(corr_fun_peaks);

    %% reset the corr_fun_2keep logical array appropriately
    corr_fun_2keep(:) = 0;
    corr_fun_2keep(sort_inx(end-param.max_channels+2:end)) = 1;
    corr_fun_2keep = logical(corr_fun_2keep);

  end

  %% keep only the associated array pairs
  cf_array_prs = cf_array_prs(corr_fun_2keep, :);

  %% report the channels these correspond to
  if size(cf_array_prs, 1) == 1
    channels_kept = sort(cf_array_prs);
  else
    channels_kept = unique(cf_array_prs)';
    %% 'unique' will order the result;
    %% notice the transpose to convert to row vector
  end

  if (plot_text_diag)
    fprintf('Channels provided: %s\n', num2str(chan_given));
    fprintf('Channels retained: %s\n', num2str(channels_kept));
  end

  if (num_chan_2keep < param.min_channels)
    %% not enough channels to do location, drop out
    if (plot_text_diag), fprintf('Not enough channels for location calculation. Quitting...\n'); end

    %% report the ~N-1 corr fun peaks (before smoothing)
    %% to see where the best thresh is
    value.peak_vals = all_unsm_corr_fun_peaks;
    
    %% if there is at least one good channel, save the lag...
    if (num_chan_2keep > 1)
      if (plot_text_diag), fprintf('First, saving lags...\n'); end

      value.channels_used = channels_kept;
      corr_functions = corr_functions(:, corr_fun_2keep);
      [num_lags, num_pairs] = size(corr_functions);

      %% first smooth xcorr functions (as below)
      %% check filter length compared to data length
      min_length = floor(num_lags / 3);
      if (filter_len > min_length)
        filter_len = min_length;
        fprintf(1,...
          'ALERT: Reducing smoothing filter length to %d\n       to work with correlation length of %d\n',...
          filter_len, num_lags);
      end
      b = hanning(filter_len) / sum(hanning(filter_len));
      %% add a trick to alleviate improper treatment of end points
      tmp = filtfilt(b, 1, [zeros(1, num_pairs); corr_functions; zeros(1, num_pairs)]);
      corr_functions = tmp(2:end-1, :);

      %% find peak positions for these correlation functions;
      %% (x-channels will be in ascending order)
      [pk_vals, pk_inx] = max(corr_functions);
      value.peak_vals_sm = pk_vals;

      %% use pk_inx to calculate corresponding time lags
      %% (max_lag + 1 = zero time lag)
      %% values are negative if signal arrives at ref chan before cross chan,
      %% (xcorrs done with ref chan second)
      %%
      value.peak_lags = -(pk_inx - (max_lag + 1)) / Fs;
    end

    close(wait_bar); return;
  end

  %% keep only the xcorr functions selected above
  corr_functions = corr_functions(:, corr_fun_2keep);

  %% and their peaks
  corr_fun_peaks = corr_fun_peaks(corr_fun_2keep);
  unsm_corr_fun_peaks = unsm_corr_fun_peaks(corr_fun_2keep);

  %% get the final size of the correlation functions kept
  [num_lags, num_pairs] = size(corr_functions);

  if strcmpi(param.corr_num, 'N(N-1)/2');

    %%~~
    %% Calculate Correlation Weightings
    %%~~

    waitbar_inx = waitbar_inx + 1;
    waitbar(waitbar_inx/num_steps, wait_bar,'Calculating Correlation Weights...');
    if (plot_text_diag), fprintf('Calculating Correlation Weights...\n'); end


    %% get the threshold for local peak saving in each xcorr function
    %% set threshold as a percentage of the global peak height, ranging
    %% from 0-100% of the global peak (sort of like dB down)
    peak_threshold = corr_fun_peaks * param.rel_pk_thresh / 100;

    %% get pointers to the local peaks within the xcorr functions
    %% (i.e., the peaks at least peak_threshold high);
    %% use this to point us to the likely positions of the x-chan event in the sound data

    %% find local peaks or non-changing parts of xcorr functions
    local_peak_inx = (diff(corr_functions(1:end-1, :)) >= 0) & (diff(corr_functions(2:end, :)) <= 0);

    %% pad back to full length (i.e., add back end points)
    local_peak_inx = [zeros(1, num_pairs); local_peak_inx; zeros(1, num_pairs)];

    %% find the parts of the xcorr functions within the peak height thresholds
    thresh_inx = corr_functions >= ones(num_lags, 1) * peak_threshold;

    %% get all acceptable local peaks
    local_peak_inx = local_peak_inx & thresh_inx;

    %% go through the xcorr functions and look at the region event-width wide around
    %% each local peak; consolidate any overlapping peak regions to find the total
    %% width of the peak region for the xcorr function; this determines the cross
    %% channel weighting

    %% get the event width
    event_width = length(event_inx);

    %% get the x-channels used
    x_channels_kept = channels_kept(channels_kept ~= ref_elem);

    %% set up a cell array to hold the final x-channel peak regions
    xchan_event_inx = cell(1, num_pairs);

    %% set up an array to hold the x-channel weights
    xchan_wts = zeros(1, num_pairs);


    %% loop through the xcorr functions kept
    for loop_inx = 1:num_pairs

      %% get the indices to the local corr function peaks; since ref chan is
      %% always 2nd in the xcorr calc, these mark the beginning of the putative
      %% event in the x-channel
      xchan_event_bounds = find(local_peak_inx(:, loop_inx));

      %% and convert these to a table of start-end indices to the event-width
      %% sections in the actual x-channel sound data; notice the transpose
      xchan_event_bounds = [xchan_event_bounds, xchan_event_bounds + event_width - 1]';

      %% make sure indices are in the allowed index range
      xchan_event_bounds(xchan_event_bounds > sig_len) = sig_len;

      %% unwrap the indices column-wise
      xchan_event_bounds = xchan_event_bounds(:);

      %% determine which of the edge indices overlap
      overlap_inx = find(diff(xchan_event_bounds) <= 1);
      overlap_inx = [overlap_inx; overlap_inx + 1];

      %% and get rid of them leaving only the extreme edges
      xchan_event_bounds(overlap_inx) = [];

      %% reshape to a start-end table
      xchan_event_bounds = reshape(xchan_event_bounds, 2, length(xchan_event_bounds)/2);

      %% and store these in the x-chan peak region cell array
      xchan_event_inx{loop_inx} = xchan_event_bounds;

      %% finally, calculate the x-chan weight based on the total width of the x-chan
      %% peak region compared to the ref chan event width (i.e., based on the SNR
      %% for the cross chan; ambiguity in where the signal of interest in contained
      %% in the x-chan will reduce it's overall weight)
      xchan_wts(loop_inx) = event_width / sum(diff(xchan_event_bounds) + 1);

    end

    %% make sure that no weight is > 1
    xchan_wts(xchan_wts > 1) = 1;

    %% create a vector of all channel weights in order for display
    chan_wts = [xchan_wts, 1];
    [sortvals, sort_inx] = sort([x_channels_kept, ref_elem]);
    chan_wts = chan_wts(sort_inx);

    %% set up a vector to hold the weights to apply to the actual xcorr functions;
    %% start with all crosses with ref chan, which get weight 1 (cause ref chan gets weight 1)
    corr_fun_wts = ones(1, num_pairs);


    if (plot_text_diag), fprintf('Weights (by channel) = %s\n', num2str(chan_wts, '%7.2f')); end

    if plot_deep_diag
      deepdiag_tag = ['deep_diagnostics_for_location_', version_num, '_n-1_xcorr'];
      deepdiag_name = ['Deep Diagnostics for Source Location Measurement ', version_num, ' : ''N-1 XCorr Fun'''];
      plot_DD_nm1_corr_fun(deepdiag_tag, deepdiag_name,...
        corr_functions, cf_array_prs, xchan_event_inx, peak_threshold, snd_data, x_channels_kept, t_vec, Fs);
    end







    %%~~
    %% Correlate Remaining Channel Pairs
    %%~~

    waitbar_inx = waitbar_inx + 1;
    waitbar(waitbar_inx/num_steps, wait_bar,'Correlating All Other Channel Pairs...');
    if (plot_text_diag), fprintf('Correlating All Other Channel Pairs...\n'); end


    %% determine the remaining channels that should now be cross-correlated
    channels2do = channels_kept(channels_kept ~= ref_elem);

    %% get index to these pairs
    pr_inx = ismember(array_prs(:,1), channels2do) & ismember(array_prs(:,2), channels2do);

    %% trim the array_prs and array_lag matrix accordingly
    array_prs_trm = array_prs(pr_inx, :);
    array_lag_trm = array_lag(pr_inx);


    for loop_inx = 1:num_pairs-1

      %% determine which of these channels becomes the current ref channel based on
      %% the xcorr peak values; x-chan with max peak becomes current ref; the corr
      %% funs will be in order by xchan so index will be fine
      [peak_val, peak_inx] = max(corr_fun_peaks);
      curr_ref_elem = channels2do(peak_inx);

      %% remove peak for the current ref chan so it's not used again
      corr_fun_peaks(peak_inx) = 0;

      %% get the event index for this channel determined above
      start_stop_inx = xchan_event_inx{peak_inx};
      curr_event_inx = [];
      for inx = 1:size(start_stop_inx, 2)
        curr_event_inx = [curr_event_inx, start_stop_inx(1, inx):start_stop_inx(2, inx)];
      end

      %% calculate the N-1 cross correlations with the current reference channel
      [curr_corr_functions, curr_cf_array_prs] = calc_time_corr(...
        snd_data, curr_event_inx, curr_ref_elem, array_prs_trm, array_lag_trm, max_lag, ref_event_width, Fs,...
        corr_type, param.display_plots, version_num);

      %% add this to the array of xcorr functions
      corr_functions = [corr_functions, curr_corr_functions];

      %% and channel pairs
      cf_array_prs = [cf_array_prs; curr_cf_array_prs];

      %% get the current ref channel weight determined above
      %% and add on to the xcorr function weighting vector
      corr_fun_wts = [corr_fun_wts, xchan_wts(peak_inx) .* ones(1, size(curr_cf_array_prs, 1))];

      %% remove pairs containing the current ref chan from the array_prs_trm and
      %% array_lags_trm arrays so they're not used again
      pr_inx = (array_prs_trm(:, 1) == curr_ref_elem) | (array_prs_trm(:, 2) == curr_ref_elem);
      array_prs_trm(pr_inx, :) = [];
      array_lag_trm(pr_inx, :) = [];

    end


  else %% do (N-1) corr functions only

    peak_threshold = [];

    %%~~
    %% Setup Ref Channel Weightings
    %%~~

    %% set up a vector to hold the weights to apply to the actual xcorr functions;
    %% all crosses with ref chan get weight 1 (cause ref chan gets weight 1)
    corr_fun_wts = ones(1, num_pairs);

    %% create a vector of ref channel weight for display
    chan_wts = [1];


    if (plot_text_diag), fprintf('Weights (by channel) = %s\n', num2str(chan_wts, '%7.2f')); end


  end %% if strcmpi(param.corr_num, 'N(N-1)/2');







  %%~~
  %% Smooth and Weight Corr Functions
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar,'Smoothing and Weighting Correlation Functions...');
  if (plot_text_diag), fprintf('Smoothing and Weighting Correlation Functions...\n'); end


  if plot_standard_plus_diag
    plot_SPD_ax4(stdplusdiag_tag, stdplusdiag_name, corr_functions, cf_array_prs, max_lag, Fs);
  end

  if plot_deep_diag
    deepdiag_tag = ['deep_diagnostics_for_location_', version_num, '_all_xcorr'];
    deepdiag_name = ['Deep Diagnostics for Source Location Measurement ', version_num, ' : ''All XCorr Fun'''];
    plot_DD_all_corr_fun(deepdiag_tag, deepdiag_name, corr_functions, cf_array_prs, Fs, 1);
  end


  %%~~ smooth the xcorr functions
  %% check filter length compared to data length
  min_length = floor(num_lags / 3);
  if (filter_len > min_length)
    filter_len = min_length;
    fprintf(1,...
      'ALERT: Reducing smoothing filter length to %d\n       to work with correlation length of %d\n',...
      filter_len, num_lags);
  end
  b = hanning(filter_len) / sum(hanning(filter_len));
  %% add a little trick to alleviate the improper treatment of end points
  %% by this method--> pad ends with zeros, smooth, then remove pad
  num_funs = size(corr_functions, 2);
  tmp = filtfilt(b, 1, [zeros(1, num_funs); corr_functions; zeros(1, num_funs)]);
  corr_functions = tmp(2:end-1, :);


  if plot_deep_diag
    plot_DD_all_corr_fun(deepdiag_tag, deepdiag_name, corr_functions, cf_array_prs, Fs, 2);
  end


  %%~~ weight the xcorr functions
  corr_functions = corr_functions .* (ones(num_lags, 1) * corr_fun_wts);


  if plot_standard_plus_diag
    plot_SPD_ax5(stdplusdiag_tag, stdplusdiag_name, corr_functions, cf_array_prs, max_lag, Fs);
  end

  if plot_deep_diag
    plot_DD_all_corr_fun(deepdiag_tag, deepdiag_name, corr_functions, cf_array_prs, Fs, 3);
  end







  %%~~
  %% Find N-1 Peak Time Delays
  %%~~

  %% find the ~N-1 time delays for all other channels with the reference channel
  %% using the peaks of the correlation functions

  %% get indices for the ~N-1 correlation pairs with the ref channel
  %% (NOTE: because of what I did with cf_array_prs order, i.e., always
  %% having the ref channel second, I only need to look at inx 2)
  nminus1_inx = (cf_array_prs(:, 2) == ref_elem);

  %% find the peak values for these correlation functions and their locations;
  %% the x-channels will be in ascending order because of how things were setup earlier
  [pk_vals, pk_inx] = max(corr_functions(:, nminus1_inx));

  %% use pk_inx to calculate the corresponding time lags, where
  %% max_lag + 1 = zero time lag
  peak_delays = (pk_inx - (max_lag + 1)) / Fs;

  %% make values negative if signal arrives at ref chan before cross chan,
  %% (recall that xcorrs are done with ref chan second always)
  peak_delays = -peak_delays;

  %% use the time delays to get the path length differences between all
  %% other channels and the reference channel
  %% calculated as (PrefCh - PxCh), so path length difference should be
  %% negative if signal arrives at refCh 1st
  nminus1_pathdiff = peak_delays * speed_of_sound;


  if plot_standard_plus_diag
    plot_SPD_ax6(stdplusdiag_tag, stdplusdiag_name, event.id, chan_given, ref_elem,...
      param.abs_pk_thresh, all_unsm_corr_fun_peaks, channels_kept, param.corr_num,...
      peak_threshold, chan_wts, filter_len, pk_vals);
  end

  %%~~
  %% Search for Location
  %%~~

  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Searching for Source...');
  if (plot_text_diag), fprintf('Searching for Source...\n'); end


  %% set up structure to pass into hyperbolic or grid search
  search_info.search_type = param.search_type; %% search method to use
  search_info.search_dim = param.search_dim; %% dimensionality of search space to use

  search_info.arr_geom = arr_geom; %% array geometry
  search_info.elem_used = channels_kept; %% array elements finally used
  search_info.elem_pairs = cf_array_prs; %% all element pairs in the order in which they were correlated (same as corr funs)
  search_info.ref_elem = ref_elem; %% reference array element

  search_info.event_ID = event.id; %% log event ID number
  search_info.Fs = Fs; %% sound sampling rate
  search_info.speed_of_sound = speed_of_sound; %% speed of sound

  search_info.corr_fun = corr_functions; %% all pairwise correlation functions in order in which they were calculated
  search_info.nminus1_pathdiff = nminus1_pathdiff; %% pairwise path differences of other elements with ref element in ascending order

  %search_info.search_pad = param.search_pad; %% extent to pad beyond array for search
  search_info.search_radius = param.search_radius; %% radius of search area from the center of the array
  search_info.term_crit = param.term_crit; %% termination criterion as median centroid distance in meters
  search_info.mean_type = param.mean_type; %% method for calculation of mean estimate
  
  search_info.theta_min = 0.0;
  search_info.theta_max = 360.0;
  
  search_info.display_plots = param.display_plots; %% flag for what type of diagnostic display to use

  %% perform the search
  [top_coords_final, top_corr_sum_final, med_cent_dist, iterations] = stochastic_beamforming_search(search_info, version_num);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% 30 May 2012, Dean Hawthorne
  %% 
  %% Calculate the maximum correlation sum and its variance.
  %% These will be used to compute chi-squared in order to 
  %% compute the confidence level contours.
  %%
  %% TODO:  Below we calculate the variance of the energy
  %% at the peak correlation lags.  That's not really correct.
  %% We need the variance at the final location's lags.
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  corr_global_peaks = max(corr_functions,[],1);
  corr_sum_max = sum(corr_global_peaks);
  corr_sum_best = sum(pk_vals);
  q = unsm_corr_fun_peaks' * unsm_corr_fun_peaks;
  corr_var = sum(sum(cov(q)));
%  corr_var = var(pk_vals)
%  corr_var=var(corr_global_peaks)
  sigma = sqrt(abs(corr_var));
  
  coord_sys = 'Cartesian';
  confidence_level = 1.0; % delta chi-squared = confidence_level

    %% Get the number of dimensions as an integer
    %% We will need it later to initials some arrays 
    if( search_info.search_dim == '3D')
        dimensions = 3;
    else
        dimensions = 2;
    end
    
    %% Set up the arrays which will contain the error information
    %% First of all, each error term has separate negative and positive deviations
    %% which get preserved until the final error report
    %%
    %% sensor_pos_error
    %% This is the error due to uncertainty in the positioning of the sensors
    %% It is a rank 3 array. The indices have the following meanings:
    %% Index 1: represents the channel for which the error is being calculated
    %% Index 2: represents the spatial dimension (x=1, y=2, z=3)
    %% Index 3: represents the negative and positive deviations (neg=1, pos=2)
    %%
    %% sound_speed_error
    %% This is the error due to uncertainty in the speed of sound
    %% It is a rank 1 array.  The indices have the following meanings:
    %% Index 1: represents the spatial dimension for which the error is being calculated (x=1, y=2, z=3)
    %% Index 2: represents the negative and positive deviations (neg=1, pos=2)
    sensor_pos_error = zeros(size(channels_kept,2), dimensions, 2);
    sound_speed_error = zeros(dimensions,2);

    %% Get the sensor geometry uncertainty from XBAT core
    sensor_deployment_error = get_sound_attribute(sound, 'sensor_geometry_uncertainty');
    
    %% Choose a delta x and y which is 0.1% of the minimum array spacing.
    %% This value of delta will be used to compute the partial derivatives
    %% of the energy with respect to the sensor locations.
    delta = 0.001 * min( calc_dist(arr_geom,channels_kept) ); 

    %% Calculate the partial derivative of the location with respect to
    %% the variables representing the error sources.
    [dEds, sensor_pos_partials, clock_drift_partials, sound_speed_partial] = calc_partials(corr_functions, arr_geom, cf_array_prs,...
    Fs, speed_of_sound, param.mean_type, coord_sys, channels_kept, top_coords_final, top_corr_sum_final, delta, med_cent_dist);

    %% Use the partial derivatives to compute the error due to uncertainty in the sensor position
    sensor_pos_error(:,:,1) = sensor_pos_partials(channels_kept,:) .* sensor_deployment_error.uncertainty(channels_kept,:,1);
    sensor_pos_error(:,:,2) = sensor_pos_partials(channels_kept,:) .* sensor_deployment_error.uncertainty(channels_kept,:,2);
    
    sensor_pos_error(:,1,:) = sensor_pos_error(:,1,:) / dEds(1); 
    sensor_pos_error(:,2,:) = sensor_pos_error(:,2,:) / dEds(2); 
    if( dimensions == 3)
        %% 3-D case
        sensor_pos_error(:,3,:) = sensor_pos_error(:,3,:) / dEds(3); 
    end
    
    %% Compute cummulative error due to the sensor location uncertainties
    %% This is the coherent sum of the errors due to uncertainty in sensor location coordinates
    cummulative_error_sensor_pos(1,:) = sum(abs(sensor_pos_error(:,1,:)),1); % x deviation
    cummulative_error_sensor_pos(2,:) = sum(abs(sensor_pos_error(:,2,:)),1); % y deviation
    if( dimensions == 3)
        %% 3-D case
        cummulative_error_sensor_pos(3,:) = sum(abs(sensor_pos_error(:,3,:)),1); % y deviation
    end

    % Get the sound speed uncertainty from the XBAT core
    sound_speed_uncertainty = get_sound_attribute(sound, 'sound_speed_uncertainty');
    
    %% Compute the error due to uncertainty of the speed of sound
    ss_uncertainty = sound_speed_uncertainty.uncertainty;
    sound_speed_error(1,:) = abs(sound_speed_partial / dEds(1) * ss_uncertainty.uncertainty(1)); 
    sound_speed_error(2,:) = abs(sound_speed_partial / dEds(2) * ss_uncertainty.uncertainty(2)); 
    if( dimensions == 3)
        %% 3-D case
        sound_speed_error(3,:) = abs(sound_speed_partial / dEds(3) * ss_uncertainty.uncertainty(3)); 
    end
    
    %% Calculate the error due to the noise on the data
    calc_error_ellipse(corr_functions, arr_geom, cf_array_prs,...
        Fs, speed_of_sound, param.mean_type, coord_sys, top_coords_final(1,:), corr_sum_max, sigma, corr_sum_best, confidence_level)
    noise_error = 3.0 * sigma; %% 95% confidence interval for Gaussian distribution
    
    %% Tally up all the error
    total_error = cummulative_error_sensor_pos + sound_speed_error + noise_error;
    
  %% return the calculated values
  waitbar_inx = waitbar_inx + 1;
  waitbar(waitbar_inx/num_steps, wait_bar, 'Search Complete. Saving Location Data...');
  if (plot_text_diag), fprintf('Search Complete. Saving Location Data...\n'); end

  %% top location and associated corr sum
  value.xlocation = top_coords_final(1, 1);
  value.ylocation = top_coords_final(1, 2);
  if srch_dim_2d
    value.zlocation = [];
  else
    value.zlocation = top_coords_final(1, 3);
  end
  value.xyz_corr_sum = top_corr_sum_final(1);

  %% top 5% of the locations and associated corr sums
  value.all_locations = [top_coords_final, top_corr_sum_final];

  %% median centroid distance of final top coordinates
  value.cent_dist = med_cent_dist;

  %% number of iterations used
  value.iterations = iterations;

  %% array elements (channels) finally used
  value.channels_used = channels_kept;

  %% channels weights used
  value.channels_wts = chan_wts;

  %% peak ~N-1 xcorr values (before smoothing)
  %value.peak_vals = unsm_corr_fun_peaks;
  %% return all peaks to see where best thresh is
  value.peak_vals = all_unsm_corr_fun_peaks;

  %% and after smoothing
  value.peak_vals_sm = pk_vals;

  %% lags for ~N-1 channel pairs with ref chan from xcorr peaks
  value.peak_lags = peak_delays;

  %% lags for ~N-1 channel pairs with ref chan from the top location
  value.loc_lags = calc_nm1_delays4loc(arr_geom, ref_elem, channels_kept, top_coords_final(1,:), speed_of_sound);


  
  %% get spherical coordinates for location
  [spher_coords] = cart2spher(top_coords_final(1,:));

  value.range = spher_coords(1);
  value.bearing = spher_coords(2);
  if srch_dim_2d
    value.elevation = [];
  else
    value.elevation = spher_coords(3);
  end

    %% Dean Hawthorne, 2 Aug 2012
    %% Project (x,y) errors onto (R, theta)
    radians = value.bearing * pi/180;
    final_error =  sum(total_error,2); % Sum up the positive and negative errors
    value.range_error = abs(final_error(1) * cos(radians) + final_error(2) * sin(radians));
    value.bearing_error = abs(atan2(final_error(1) * sin(radians) + final_error(2) * cos(radians), value.range)) * 180/pi;
    if( dimensions == 3)
        %% Print warning
        fprintf(1,'3D location error estimate is not complete');
    end
  %% get distances between array elements and source location for the channels used
  value.dist2channels = ...
    sqrt(sum((arr_geom(channels_kept, :) - ones(length(channels_kept), 1) * top_coords_final(1,:)).^2, 2));



end %% if strcmpi(corr_type, 'time') | strcmpi(corr_type, 'time_ce')


close(wait_bar);



return;