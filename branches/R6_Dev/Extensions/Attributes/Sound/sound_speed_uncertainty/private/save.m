function save(attribute, store, context)

% SENSOR_GEOMETRY - save

% Copyright (C) 2002-2007 Harold K. Figueroa (hkf1@cornell.edu)
% Copyright (C) 2005-2007 Matthew E. Robbins (mer34@cornell.edu)
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

    result = context.dialog_result;

    %--
    % check for toolbox
    %--

    lines{1} = 'c-, c+';

    lines{2} = 'm/s';

    %--
    % write geometry uncertainty to lines
    %--
    for col = 1:2
        value = attribute.unceratinty(col);
        line = [line, num2str(value), ', ']		
    end
	
	line(end-1:end) = [];
	
	lines{end + 1} = line;
	
    file_writelines(store, lines);

end

