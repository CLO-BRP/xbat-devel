function save(attribute, store, context)

% SENSOR_ALIGNMENT - save

% Copyright (C) 2002-2007 Harold K. Figueroa (hkf1@cornell.edu)
% Copyright (C) 2005-2007 Matthew E. Robbins (mer34@cornell.edu)
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

lines{1} = 'Sensor Alignment Data';

lines{2} = 'Time since last alignment, ARU clock drift ';
lines{3} = '(days), (sec/day) ';

%% Test code
clear attribute.alignment

attribute.alignment = [ ...
    1 -1.1 -2.2 -3.3 -4.4; ...
    2 1.1 2.2 3.3 4.4; ...
    3 -1.1 -2.2 -3.3 -4.4; ...
]

rows = size(attribute.alignment, 1);
cols = size(attribute.alignment, 2) - 1;
for alignment = 1:rows
    line = [num2str(attribute.alignment(alignment, 1)), ', '];
    for drift = 1:cols
        line = [line, num2str(attribute.alignment(alignment, drift+1))];
        if( drift ~= cols)
            line = [line, ', '];
        end
    end
    lines{alignment+3} = line;
end

file_writelines(store, lines');
