function result = parameter__control__callback(callback, context)

% DETECT - parameter__control__callback

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 16 Sep 2013
%		Add control for adding string tokens to output log names.
%		Enable controls for threshold and max log size wtih checkboxes.
%	msp2 - 2 Nov 2013
%		Add support for Raven selection table output.
%	msp2 - 6 Mar 2013
%		Remove control for disabling threshold reset.
%		Update control for Window Parameter.
%   msp2 - 13 May 2014
%       Fix bug active when multiple presets selected.
%   msp2 - 14 May 2014
%       Add support for a unique identifier in log names.
%   msp2 - 15 May 2014
%       Add multithreading support.

result = [];

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
switch callback.control.name
  case 'add_token'  
    %--
    % add token to log name
    %--
    if strcmp(callback.control.name, 'add_token')
      str = get_control(callback.pal.handle, 'output', 'string');
      value = get_control(callback.pal.handle, 'add_token', 'value');
      if strcmp(value{1}, 'Sound Name')
        value = '%SOUND_NAME%';
      elseif strcmp(value{1}, 'Detector Name')
        value = '%DETECTOR_NAME%';
      elseif strcmp(value{1}, 'Preset Name')
        value = '%PRESET_NAME%';
      elseif strcmp(value{1}, 'Time')
        value = '%TIME%';
      elseif strcmp(value{1}, 'Unique Identifier')
        value = '%UID%';
      end

      if isempty(str)
          str = value;
      else
          str = [str, '_', value];
      end

      set_control(callback.pal.handle, 'output', 'string', str);

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( callback.pal.handle, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( callback.pal.handle, 'OK', 'enable', 1 );
        else
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end

        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( callback.pal.handle, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end

    end
    
	case 'detector'

        %--
        % get detector extension
        %--		
        detector = get_control(callback.pal.handle, 'detector', 'value');		
        ext = get_extensions('sound_detector', 'name', detector{1});

        %--
        % get preset names
        %--		
        presets = {};

        if ~isempty( ext )
            presets = get_preset_names( ext );
        end

        % NOTE: this may happen under various conditions		
        if isempty(presets)
            presets = { '(No Presets Found)' };
        end

        %--
        % unselect preset
        %--		
        handles = get_control( callback.pal.handle, 'preset', 'handles' );		
        set( handles.obj, 'string', presets, 'value', [] );
        
        %--
        % disable OK button
        %--        
        set_control( callback.pal.handle, 'OK', 'enable', 0 );
		
	case 'preset'
		
		% TODO: this will update the preset info controls in the future

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( callback.pal.handle, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( callback.pal.handle, 'OK', 'enable', 1 );
        else
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end
		
        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( callback.pal.handle, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end
		
	case 'output'

    % NOTE: consider having a type validation callback and extending validation

        %--
        % enable OK button if preset is selected
        %--
        preset = get_control( callback.pal.handle, 'preset', 'value' );
        if ~isempty( preset ) && ~any( strcmp( preset, '(No Presets Found)' ) )
           set_control( callback.pal.handle, 'OK', 'enable', 1 );
        else
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end
		
        %--
        % disable OK button if output filename is improper
        %--
        handles = get_control( callback.pal.handle, 'output', 'handles' );
        fn = get( handles.obj, 'string' );

        %set state of OK button
        if ~proper_filename( fn )
           fail( sprintf( '"%s" is not a proper file name', fn ) );
           set_control( callback.pal.handle, 'OK', 'enable', 0 );
        end
        
    case 'output_type'  
        
        % ask user for output path if Raven selection table output selected
        output_type = get_control( callback.pal.handle, 'output_type', 'value' );

		if strcmp( output_type, 'selection table' )
            out_path = get_control( callback.pal.handle, 'out_path', 'string' );            
            out_path = uigetdir( out_path, 'Select folder for Raven selection tables');            
            if out_path == 0
                return;
            end
            set_control( callback.pal.handle, 'out_path', 'string', out_path );
            set_control( callback.pal.handle, 'out_path', 'enable', 1 );            
		else  %XBAT log
            set_control( callback.pal.handle, 'out_path', 'enable', 0 );
		end
	case 'win_type'
        
        win_type = get_control( callback.pal.handle, 'win_type', 'value' );
		param = window_to_fun( win_type, 'param' );
		
		if ~isempty( param )
            set_control( callback.pal.handle, 'win_param', 'min', param.min );
            set_control( callback.pal.handle, 'win_param', 'max', param.max );
            set_control( callback.pal.handle, 'win_param', 'value', param.value );
            set_control( callback.pal.handle, 'win_param', 'enable', 1 );
		else
            set_control( callback.pal.handle, 'win_param', 'value', 0 );
            set_control( callback.pal.handle, 'win_param', 'enable', 0 );
        end
end

% % enable threshold slider with checkbox
% 
% threshold_flag = get_control(callback.pal.handle, 'threshold_flag', 'value');
% 
% if threshold_flag
%     set_control(callback.pal.handle, 'threshold', 'enable', 1);  
% else
%     set_control(callback.pal.handle, 'threshold', 'enable', 0);
% end

%---
% enable maximum log size slider with checkbox
%---
chop_flag = get_control(callback.pal.handle, 'chop_flag', 'value');

if chop_flag
    set_control(callback.pal.handle, 'max_num_events', 'enable', 1);    
else
    set_control(callback.pal.handle, 'max_num_events', 'enable', 0);    
end

%---
% enable maximum log size slider with checkbox
%---
multithread = get_control(callback.pal.handle, 'multithread', 'value');

if multithread
    set_control(callback.pal.handle, 'poolsize', 'enable', 1);    
else
    set_control(callback.pal.handle, 'poolsize', 'enable', 0);    
end
