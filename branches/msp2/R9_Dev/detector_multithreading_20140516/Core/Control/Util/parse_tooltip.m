function [ logname, tag, mode, duration, freq ] = parse_tooltip( C )
% parse_tooltip: Unpacks tooltip from button in Mode palette
%
%   input
%       tooltip (char matrix)
%   output
%       logname (char)
%       tag (char)
%       mode (char)
%       duration (double)
%       freq (1x2 double)

% History
%   msp2 - 28 Mar 2014
%       Create.
%   msp2 - 28 Mar 2014
%       Remove output parameter for bandwidth.

% initializations
logname = '';
tag = '';
mode = '';
duration = [];
freq = [];

if ~iscellstr( C )
    return;
end

% unpack tooltip into individual parameters
C2 = regexp( C{ 1 }, ':', 'split' );
logname = strtrim( C2{ 2 } );

C2 = regexp( C{ 2 }, ':', 'split' );
tag = strtrim( C2{ 2 } );

C2 = regexp( C{ 3 }, ':', 'split' );
mode = strtrim( C2{ 2 } );

C2 = regexp( C{ 4 }, ':', 'split' );
duration = strtrim( C2{ 2 } );
duration = str2double( duration );

C2 = regexp( C{ 5 }, ':', 'split' );
freq1 = strtrim( C2{ 2 } );
freq1 = str2double( freq1 );

C2 = regexp( C{ 6 }, ':', 'split' );
freq2 = strtrim( C2{ 2 } );
freq2 = str2double( freq2 );
freq = [ freq1, freq2 ];

