function mode_button_callback( h_menu, ~, pal, control, type )
% mode_callback - callback for right mouse click on buttons in Browse palette
%
%   input
%       h_menu: handle of menu item selected
%       pal: handle of Browse palette
%       control: control struct for selected button

% History
%   msp2 - 29 Mar 2014
%       Create.
%	msp2 - 30 Mar 2014
%		Make button light green if ready.
%		Get callbacks for "Tag" mode working.
%		Remove references to "Delete" mode.
%   msp2 - 31 Mar 2014
%       Disable delete mode when editing button context menus.
%   msp2 - 27 Apr 2014
%       Prevent frequency from being set to less than zero.
%   msp2 - 29 Apr 2014
%       Fix minor bug in tag-setting code.
%   msp2 - 7 May 2014
%       Do not reset settings when changing log or mode in button.
%       Remove code converting tag "(No Tag)" to '', since not needed.

%---
% initializations
%---
h_button = get_control( pal, control.name, 'handles' );
par = get_active_browser;
data = get( par, 'userdata' );

%---
% Enable "Select" mode checkbox
%---
children = get( pal, 'Children' );
ch = findobj( children, 'Style', 'checkbox' );
set( ch, 'Value', 0 )
sh = findobj( ch, 'Tag', 'select_mode' );
set( sh, 'Value', 1 )
    
% Set selected button to normal font to indicate it is NOT selected
set( h_button.obj, 'FontWeight', 'normal' )

% Set browser to "select" mode
set_browser_mode( par, 'select', data )

%---
% Stop if no callback
%---
fun = get( h_menu, 'Callback' );
if isempty( fun )
    return;
end

%---
% menu callbacks
%---
switch type
    case { 'Log:', 'Mode:' }
        
        %---
        % add selection to parent menu label
        %---
        selection = get( h_menu, 'Label' );
        new_label = sprintf( '%s %s', type, selection );
        set( get( h_menu, 'Parent' ), 'Label', new_label )
            
        %---
        % find handles of first level uimenu members
        %---
        h_mode_menu = get( h_menu, 'Parent' );
        h_root_menu = get( h_mode_menu, 'Parent' );
        h_menus = get( h_root_menu, 'Children' );
        
        %---
        % Retrieve initial state of each menu member
        %---
        
%         % reset menus
%         reset_labels( h_menus )

%         % remove tag as button label
%         set( h_button.obj, 'String', '(No Tag)' )

        % make button same gray as window
        set( h_button.obj, 'BackgroundColor', [ 0.8814, 0.8657, 0.8343], 'ForegroundColor', [ 0, 0, 0 ] )

        % unset ready bit
        tmp.ready = 0;
        set( h_button.obj, 'Userdata', tmp )
        
        %---
        % enable other controls if log selected
        %---
        if strcmp( type, 'Log:' )
           
            % enable "Mode:" menu
            idx = ismember( get( h_menus, 'Tag' ), { 'Mode:' } );
            set( h_menus( idx ), 'Enable', 'on' )
        
        %---
        % disable uimenus not needed for selected mode
        %---
        elseif strcmp( type, 'Mode:' )
            
            % disable first level uimenu members
            set( h_menus, 'Enable', 'off' )
                    
            switch selection
                case 'Tag'
                    idx = ismember( get( h_menus, 'Tag' ), { 'Log:', 'Mode:', 'Tag:' } );
                    set( h_menus( idx ), 'Enable', 'on' )
                    
                case { 'Hand', 'Select' }
                    
                    % enable uimenu for Log and Mode
                    idx = ismember( get( h_menus, 'Tag' ), { 'Log:', 'Mode:' } );
                    set( h_menus( idx ), 'Enable', 'on' )
                    
                case 'Select + Log'
                    
                    % enable uimenu for Log and Mode
                    idx = ismember( get( h_menus, 'Tag' ), { 'Log:', 'Mode:', 'Tag:' } );
                    set( h_menus( idx ), 'Enable', 'on' )
                    
                case { 'One Click', 'One Click Frequency Tracking' }
                    
                    % enable all uimember items
                    set( h_menus, 'Enable', 'on' )               
            end    
        end
        
    case 'Tag:'
        
        %---
        % open edit control
        %---
        txt = inputdlg( '', type, 1, {''} );
%         if isempty( txt )
%             txt = { '(No Tag)' };
%         end
        
        %---
        % make tag the button label
        %---
        if ~isempty( txt )
            set( h_button.obj, 'String', txt )
        else
            return;
%             set( h_button.obj, 'String', '(No Tag)' )
        end
            

        %---
        % add text to menu
        %---
        new_label = sprintf( '%s %s', type, txt{ 1 } );
        set( h_menu, 'Label', new_label )
        
    case { 'Dur (s):', 'Min Freq (Hz):', 'Max Freq (Hz):' }
        
        %---
        % open edit control
        %---
        C = inputdlg( '', type, 1, {''} );
        if isempty( C )
            return;
        end
        
        %---
        % convert to input from text to number
        %---
        str = C{ 1 };
        num = str2double( str );
        if ~isfinite( num )
            fail( sprintf( '"%s" is not a number', str ), 'WARNING' );
            return;
        end
        
        %---
        % test for reasonable value
        %---
        switch type
            case 'Dur (s):'
                if num <= 0
                    fail( sprintf( '"%s" is not greater than zero', str ), 'WARNING' );
                    return;
                else
                    str = sprintf( '%.3f', num );
                end
                
            case { 'Min Freq (Hz):', 'Max Freq (Hz):' }
                
                %---
                % test for reasonable min and max frequency
                %---
                
                % find name of open log
                h_freq_menu = get( h_menu, 'Parent' );
                h_menus = get( h_freq_menu, 'Children' );
                C = get( h_menus, 'Label' );
                [ logname, ~, ~, ~, freq ] = parse_tooltip( C( end : -1 : 1 ) );
                
                
                
                % get open log names
                if ~isempty( data.browser.log )
                    LOGS = file_ext( struct_field( data.browser.log,'file' ) );
                else
                    LOGS = {};
                end
	
                % get copy of selected log
                m = find( strcmp( LOGS, logname ) );
                if isempty( m )
                    txt = sprintf( '%s not found open in browser.', logname );
                    fail( txt, 'WARNING' )
                    return;
                end
                log = data.browser.log( m );
                
                % find Nyquist limit for sound
                nyquist = log.sound.samplerate / 2;
                
                %---
                % Warn and return if either freq is above Nyquist limit or below zero
                %---
                if num > nyquist
                    fail( 'Entered frequency is greater than Nyquist limit for sound', 'WARNING' )
                    return;
                elseif num < 0
                    fail( 'Entered frequency is less than zero', 'WARNING' )
                    return;
                end
                
                %---
                % find what min and max freq would be after this change
                %---
                if strcmp( type, 'Min Freq (Hz):' )
                    freq_q = [ num, freq( 2 ) ];
                elseif strcmp( type, 'Max Freq (Hz):' )                    
                    freq_q = [ freq( 1 ), num  ];
                end
                
                %---
                % Warn and return if Max Freq less than Min Freq
                %---
                if diff( freq_q ) <= 0
                    fail( '"Max Freq (Hz):" is not greater than "Min Freq (Hz):"', 'WARNING' )
                    return;
                end
                
                %---
                % Convert duration or frequency to string
                %---                
                str = sprintf( '%d', num );
        end
            
        %---
        % add text to menu
        %---
        new_label = sprintf( '%s %s', type, str );
        set( h_menu, 'Label', new_label )

end

%---
% Add first level context menu contents to tooltips
% (Note: Left-click on button reads tooltips in browser_controls.)
%---

% get handles for first level of context menu
switch type
    case { 'Log:', 'Mode:' }
        h_log_menu = get( h_menu, 'Parent' );
        h_root_menu = get( h_log_menu, 'Parent' );
        h_menus = get( h_root_menu, 'Children' );
    otherwise
        h_root_menu = get( h_menu, 'Parent' );
        h_menus = get( h_root_menu, 'Children' );        
end

% make contents of first level of context menu into button tooltips
C = get( h_menus, 'Label' );
C = C( end : -1 : 1 );
tooltipstring = sprintf( '%s\n', C{ : } );
set( h_button.obj, 'TooltipString', tooltipstring )

%---
% set button as ready or unready to be pushed
%---

% % if log name is being set, button is unready
% if strcmp( type, 'Log:' )
%     isready = 0;
%     
% % if log name is not being set
% else
    
    % button is ready only if all parameters needed by current browsing mode are set
    [ logname, mode, tag, duration, freq ] = parse_tooltip( C );
    if isempty( logname )
        isready = 0;
    else
        switch mode
            case { 'Hand', 'Select', 'Select + Log' }        
                isready = 1;
            case { 'One Click', 'One Click Frequency Tracking' }
                isready = ~isnan( duration ) && ~any( isnan( freq ) );
            case 'Tag'
                isready = 1;
            otherwise
                isready = 0;
        end
    end
% end

% set button label font to indicate if button isready / ~isready
if isready
    
    % trip ready bit
    tmp.ready = 1;
    set( h_button.obj, 'Userdata', tmp )
    
    % make button light green
    set( h_button.obj, 'BackgroundColor', [ 0.85, 1, 0.85 ] )
else
    
    % untrip ready bit
    tmp.ready = 0;
    set( h_button.obj, 'Userdata', tmp )
    
    % make button same gray as window
    set( h_button.obj, 'BackgroundColor', [ 0.8814, 0.8657, 0.8343] )
end

%---
% function reset_labels( h_menus )
% % reset_labels - reset uimenu Label from uimenu Tag
% %   Input: 
% %       h_menus - vector of uimenu handles
% %       field_names - cell vector of uimenu labels to be replaced
%             
%     % get menu tags, which have menu labels in initial state
%     menu_tag = get( h_menus, 'Tag' );
%     
%     % reinitialize all items on first menu level
%     idx = find( ~ismember( menu_tag, { 'Log:', 'Mode:' } ) );
%     for i = idx'
%         set( h_menus( i ), 'Label', menu_tag{ i } )
%     end

% %------------------------
% function str = html( str, fsize, bold )
% % fontize - adds html to string so that it shows up in bigger font
% %
% %   input
% %       str   - string to be formatted
% %       fsize - fontsize
% %       bold  - 0/1 = normal/bold font weight
% %   output
% %       str - string with html font size tag
% 
% if bold
%     str = sprintf( '<html><b><font size=%.0f>%s</font></b></html>', fsize, str );
% else
%     str = sprintf( '<html><font size=%.0f>%s</font></html>', fsize, str );
% end
%
% -------------------------------
% Example of use:
%         tag1 = sprintf( '<html><font size=%.0f><b>', fsize );
%         tag2 = '</b>';
%         tag3 = '</font></html>';
%         selection = get( h_menu, 'Label' );
%         new_label = sprintf( '%s%s %s%s%s', tag1, type, tag2, selection, tag3 );
%         set( get( h_menu, 'Parent' ), 'Label', new_label )
% -------------------------------

% %------------------------
% function str = strip_html( str )
% 
% str = regexprep( str, '<.*?>', '' );
% % str = regexprep( str, '<[^>]*>', '' );

% %-----------------------------
% function answer = inputdlg2( dlg_title )
% 
% fh = figure( 'menubar', 'none', 'Name', dlg_title, 'NumberTitle', 'off' );
% set( fh, 'Units', 'inches' );
% set( fh, 'Position', [ 5, 5, 2.5, 1.2 ] );
% set( fh, 'Units', 'normal' )
% 
% eh = uicontrol( fh, 'Style', 'edit', 'Units', 'normal', 'FontSize', 14 );
% set( eh, 'Position', [ 0.2, 0.6, 0.6, 0.2 ], 'BackgroundColor', [ 1 1 1] );
% 
% bh = uicontrol( 