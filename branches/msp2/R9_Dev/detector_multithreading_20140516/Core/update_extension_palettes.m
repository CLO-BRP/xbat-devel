function pals = update_extension_palettes(par, data)

% update_extension_palettes - update extension palettes when logs change
% ----------------------------------------------------------------------
%
% pals = update_extension_palettes(par, data)
%
% Input:
% ------
%  par - parent handle
%  data - parent state
%
% Output:
% -------
%  pals - updated palette handles

% Copyright (C) 2002-2012 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%	msp2 - 29 Mar 2014
%		Add listener for "Browse" palette.

%----------------------
% HANDLE INPUT
%----------------------

%--
% check browser handle input
%--
if ~is_browser(par)
	error('Input handle is not browser handle.');
end

%--
% get browser state
%--
if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%----------------------
% TEST CODE
%----------------------

%--
% get listener control values
%--
none = isempty(data.browser.log);
if none
	list = {'(No Open Logs)'}; 
    ix = 1;
else
	list = file_ext(struct_field(data.browser.log, 'file')); 
    ix = data.browser.log_active;
end
		
%--
% get listener names and palette handles
%--
% NOTE: not all of these listeners are currently in use
listener = {'output_log', 'noise_log', 'source_log', 'template_log'}; 
browse_listener = { 'button_1', 'button_2', 'button_3', 'button_4', 'button_5', 'button_6' };

% NOTE: this is critical, 'get_palette' only gets registered palettes, is should use this function!
pal = get_xbat_figs('type', 'palette', 'parent', par);

%--
% look for listener controls in available palettes
%--
pals = [];
for k = 1:length(pal)
    
    update_flag = 0;
	
	for j = 1:length(listener)
		
		%--
		% look for listener in palette
		%--
		
		control = get_control(pal(k), listener{j});
		
		if isempty(control)
			continue;
		end
		
		%--
		% update listening control values and state
		%--
		
		old_value = get_control(pal(k), listener{j}, 'value');
		
		set(control.handles.obj, 'string', list, 'value', ix);
		
		%--
		% try to keep old value for noise log
		%--
		
		if strcmp(listener{j}, 'noise_log');
			set_control(pal(k), listener{j}, 'value', old_value);
		end
			
		set_control(pal(k), listener{j}, 'enable', ~none);
		
		% NOTE: these are ancillary listeners that are not well formalized
		
		set_control(pal(k), 'scan', 'enable', ~none);
		
		set_control(pal(k), 'use_log', 'enable', ~none);
        
        update_flag = 1;
		
	end
    
	for j = 1:length(browse_listener)
        
        %--
        % look for listener in Browse palette
        %--
        h_control = get_control( pal( k ), browse_listener{ j }, 'handles' );
        if isempty( h_control )
            continue;
        end
        
        update_flag = 1;

        %--
        % find handle of log list in button context menu
        %--
        h_button = h_control.obj;
        h_uimenu = get( h_button, 'UIContextMenu' );
        h_uimenu_level1 = get( h_uimenu, 'Children' );
        log_menu_idx = strcmp( get( h_uimenu_level1, 'Tag' ), 'Log:' );
        h_log = h_uimenu_level1( log_menu_idx );

        %--
        % delete current context menu log list
        %--
        delete( get( h_log, 'Children' ) );

        %--
        % update context menu log list
        %--
        control = get_control( pal( k ), browse_listener{ j },'control');
        control.name = browse_listener{ j };
        log_fun = { @mode_button_callback, pal( k ), control, 'Log:' };
        for i = 1 : length( list )
            label = list{ i };
            h_menu = uimenu( h_log, 'Label', label, 'Enable', 'on',  'callback', log_fun );
        end

        %--
        % if no logs are open, reinitialize "Log:" menu, disable context menu log list
        %--
        if any( strcmp( '(No Open Logs)', list ) )
            reset_menu( h_log, h_uimenu_level1, h_button )
            set( h_menu, 'Enable', 'off' )
            
        %--
        % reinitialize "Log:" menu if selected log is no longer open
        %--            
        else
            
            %find name of selected log
            log_label = get( h_log, 'Label' );
            tmp = regexp( log_label, ':', 'split' );
            selected_log = strtrim( tmp( 2 ) );
            
            %if selected log is not open
            if ~any( strcmp( list, selected_log ) )                
                reset_menu( h_log, h_uimenu_level1, h_button )
            end
        end
	end
		
    if update_flag
		%--
		% add palette to list of updated palettes
		%--
		pals(end + 1) = pal(k); 
    end
end

%---
function reset_labels( h_menus )
% reset_labels - reset uimenu Label from uimenu Tag
%   Input: 
%       h_menus - vector of uimenu handles
%       field_names - cell vector of uimenu labels to be replaced
            
    % get menu tags, which have menu labels in initial state
    menu_tag = get( h_menus, 'Tag' );
    
    % reinitialize Log: menu
    idx = find( ~ismember( menu_tag, 'Log:' ) );
    for i = idx'
        set( h_menus( i ), 'Label', menu_tag{ i } )
    end

%---
function reset_menu( h_log, h_uimenu_level1, h_button )

% reset Log: menu
set( h_log, 'Label', get( h_log, 'Tag' ) );

%reinitialize "Log:" menu
set( h_log, 'Label', get( h_log, 'Tag' ) );

% enable uimenu for Log and Mode
menu_tag = get( h_uimenu_level1, 'Tag' );
idx = ismember( menu_tag, { 'Log:', 'Mode:' } );
set( h_uimenu_level1, 'Enable', 'off' )
set( h_uimenu_level1( idx ), 'Enable', 'on' )

% reinitialize all items on first menu level
reset_labels( h_uimenu_level1 )

% remove tag as button label
set( h_button, 'String', ' ' )

% make button same gray as window
set( h_button, 'BackgroundColor', [ 0.8814, 0.8657, 0.8343], 'ForegroundColor', [ 0, 0, 0 ] )

% untrip ready bit
userdata.ready = 0;
set( h_button, 'Userdata', userdata )
