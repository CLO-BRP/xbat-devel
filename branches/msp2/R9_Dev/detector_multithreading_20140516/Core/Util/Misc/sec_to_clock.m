function str = sec_to_clock( t, sf )

% sec_to_clock - convert seconds to time string
% ---------------------------------------------
%
% str = sec_to_clock(t,n)
%
% Input:
% ------
%  t  - time in seconds
%  sf - fractional second digits (def: 3)  %%was def: 2%%
%
% Output:
% -------
%  str - time string

% Copyright (C) 2002-2012 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4794 $
% $Date: 2006-04-25 14:54:27 -0400 (Tue, 25 Apr 2006) $
%--------------------------------

% History
%   msp2 - 13 Mar 2014
%       Reduce precision of output time string to milliseconds for readibility.
%	msp2 - 18 Mar 2014
%		Undo reduction of precision because it introduced a bug.
%	msp2 - 20 Mar 2014
%		Add back input parameter for specifying fractional second digits.

%-------------------------------------------------------------------------
% MEX COMPUTATION - NOTE: we are not using precision right now
%-------------------------------------------------------------------------
str = sec_to_clock_(t);

%-------------------------------------------------------------------------
% Reduce precision of string to "n" significant digits
%-------------------------------------------------------------------------
if ~exist( 'sf', 'var' )
    sf = 3;
end
n = min( sf, 7 );
n = max( n, 0 );
if isequal( sf, 0 )
    n = n - 1;    %if no fractional second, get rid of decimal point
end
str = cellfun( @(x) x( 1 : end - 6 + n ), str, 'UniformOutput', false );

%--
% output string - NOTE: we output a string for a single time
%--
if (length(str) == 1)
	str = str{1};
end
