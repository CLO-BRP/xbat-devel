function root = xbat

% xbat - start xbat
% -----------------
%
% xbat
%
% NOTE: this function starts the XBAT environment

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 30 Oct 2013
%       Backup palette presets to ToBeBackedUp directory.
%	msp2 - 22 Mar 2014
%		Lighten background color of control palettes.
%		Turn on Windows recycle for delete function.
%       Make palette size bigger for vertical 1200 x 1920 monitors.
%   msp2 - 30 Mar 2014
%       Uncomment "install_mex", which was inadvertantly commented out.
%		Fix mistake in palette size lookup.
%		Change default font for figure axes, text, and uicontrols from Comic Sans 8 to Arial 10.
%   msp2 - 23 Apr 2014
%       Suppress display of "Warning: Duplicate directory name", which is 
%       produced by the faulty restoredefaultpath MATLAB 2012a produces on launch.
%   msp2 - 24 Apr 2014
%       Create ToBeBackedUp folder for palette presets, if it doesn't exist.
%   msp2 - 28 Apr 2014
%       Change background color of GUI figures per CPR recommendations for
%       readability.
%   msp2 - 29 Apr 2014
%		Change default font for figure axes, text, and uicontrols from Arial to Segoe UI.
%   msp2 - 6 May 2014
%       Revert background color committed 28 Apr 2014.  Lighter is better.
%   msp2 - 17 May 2014
%       Suppress warning about moving XBAT paths ahead of other paths.
%   msp2 - 22 May 2014
%       When launching XBAT, set search paths to MATLAB default paths +
%         XBAT paths + MATLAB startup folder.
%   msp2 - 5 May 2014
%       Turn recycling back off.  It's recycling many MEX files and SVN
%         Command Line files on every XBAT launch.
%   msp2 - 26 Nov 2014
%       Stop deleting all figures and restoring default search paths if
%         xbat is called from another script because it messes up Sedna.
%   msp2 - 17 Mar 2015
%       Revert back to restoring default search paths.  Sedna can handle it
%         now.

%-------------------------------
% SETUP
%-------------------------------

%--
% set matlab properties
%--

% NOTE: name warning currently triggers on private functions
warning('off', 'MATLAB:dispatcher:nameConflict');

%--
% take care of some settings for linux
%--

if isunix
    
	% NOTE: this is voodoo, it may not work in the future
	setappdata(0, 'UseNativeSystemDialogs', false);
	
    % NOTE: this color was empirically derived
	set(0, 'DefaultFigureColor', [0.91, 0.89, 0.85]);	
    set(0, 'DefaultUicontrolBackgroundColor', [0.91, 0.89, 0.85]);    
elseif ispc
	set(0, 'DefaultFigureColor', [ 0.8814, 0.8657, 0.8343 ] );	
    set(0, 'DefaultUicontrolBackgroundColor', [ 0.8814, 0.8657, 0.8343 ] );      
end

% NOTE: we can set delete to use the recycling bin
recycle('off');

%-------------------------------
% INITIALIZE PATH
%-------------------------------
root = initialize_path;
% [root, init_path] = initialize_path;
    
%--
% install platform and version appropiate MEX files 
%--

install_mex;

%--
% set XBAT root
%--

% NOTE: we wait to set the root until we append the path
update = ~isempty(get_env('xbat_root'));
set_env('xbat_root', root); 

% NOTE: we can effectively use many XBAT functions after this point

%-------------------------------
% SET GRAPHICS PROPERTIES
%-------------------------------

%--
% set figure properties
%--

set(0, 'DefaultFigureMenubar', 'none');

%--
% set text properties
%--
fonts = get_simple_fonts; 
font.name = fonts{ 7 }; %SegoeUI
font.size = 10;
set_text_properties( font );

%-------------------------------
% CONFIGURE PALETTES
%-------------------------------

%--
% palette size
%--

% NOTE: allowed values are: 'smaller', 'small', 'medium', 'large', and 'larger'

height = get( 0, 'screensize' ); 
height = height( 4 );

if height <= 768
	palette_size = 'smallest';
elseif height <= 1050
	palette_size = 'smaller';
else
    palette_size = 'small';
end

set_env('palette_size', palette_size);

%--
% other palette options
%--

% NOTE: allowed values are 'on' and 'off'

set_env('palette_gradient', 'on');

set_env('palette_sounds', 'on');

set_env('palette_tooltips', 'on');

% TODO: consider revealing the hover behavior here

%-------------------------------
% SHOW SPLASH 
%-------------------------------

%--
% create splash
%--

% NOTE: determine the number of displayed startup steps, these are the ticks

ticks = 2 * 6 + length(get_extension_names) + length(get_formats);

% NOTE: we do this here because splash uses palette properties

% TODO: use different images for initial load and refresh

if ~update
	splash = splash_wait('', ticks);
else
	clear('functions'); splash = splash_wait('', ticks);
end

%--
% display fictitious yet informative messages
%--

first_steps = {'computing path', 'appending path', 'setting root', 'configuring gui'};

for k = 1:length(first_steps)
	splash_wait_update(splash, [first_steps{k}, ' ...']);
end

%------------------------------------------
% CONSIDER ROOT CHANGE
%------------------------------------------

splash_wait_update(splash, 'updating users ...');

users_root_update;

%------------------------------------------
% REFRESH EXTENSIONS
%------------------------------------------

get_formats(1);

% NOTE: consider allowing number input for this function

get_extensions('!');

%------------------------------------------
% OPEN XBAT PALETTE
%------------------------------------------

splash_wait_update(splash, 'opening xbat palette ...');

% NOTE: this is where the extensions get loaded

xbat_palette; 

pause(0.5); close(splash);

set_env('palette_sounds', 'on');

%------------------------------------------
% MISC 
%------------------------------------------

%--
% PREFERENCES AND MODES
%--

% NOTE: this sets an environment variable used by other functions

%--
% user options
%--

show_desktop 1;

show_other_sounds 0;

%--
% developer options
%--

xbat_developer 1;

pcode_refresh('clear');

%--
% start palette daemon
%--

stop(palette_daemon); start(palette_daemon);

% NOTE: this suppresses output display

if ~nargout
	clear root;
end

%--
% check for updates
%--

% xbat_update('startup');

%--
% handle migration on first use, when system seems empty
%--

users = get_users;

% if (length(users) == 1) && (length(users(1).library) == 1) && isempty(get_library_sounds)
% 	
% 	result = quest_dialog( ...
%         {'Would you like to migrate ', 'Users and libraries from ', 'an old version of XBAT?'}, ...
%         'Import Old Version' ...
% 	);
% 	
% 	if strcmp(result, 'Yes')
% 		migrate_xbat;
% 	end
% 	
% end

%--
% try to initialize wavelab
%--

% TODO: figure out where to put this code

pi = pwd;

if ~isempty(toolbox_which('WavePath'))
	
	try
		cd(fullfile(toolbox_root('Wavelab'), 'Wavelab850')); startup;
	catch
		nice_catch(lasterror, 'Failed to initialize ''Wavelab''.');
	end
	
end

cd(pi);
	

%--------------------------------
% GET_PATH_STR
%--------------------------------

function str = get_path_str(root, mode)

% get_path_str - add directories to path
% ----------------------------------
%
% str = get_path_str(root, 'flat')
%     = get_path_str(root, 'rec')
%
% Input:
% ------
%  root - initial directory
%
% Output:
% -------
%  str - string appended to path

%-----------------
% HANDLE INPUT
%-----------------

if nargin < 2
	mode = 'flat'; 
end 

%-----------------
% SETUP
%-----------------

% NOTE: the order is MATLAB, XBAT, Tools, and MISC

EXCLUDE_DIRS = { ...
	'Patches', ...
	'private', ...
	'Presets', ...
	'Users', ...
	'RadRails', ...
	'apache', ...
	'iconv', ...
	'share', ...
	'rails_apps', ...
	'phpmyadmin', ...
	'ruby', ...
	'MSYS', ...
	'CVS' ...
};

%-----------------
% BUILD STRING
%-----------------

out = what_ext(root);
		
str = out.path;

for k = 1:length(out.dir)

	%--
	% skip dot and method, then excluded directories
	%--

	if (out.dir{k}(1) == '.') || (out.dir{k}(1) == '@')
		continue;
	end

	if ~isempty(find(strcmp(out.dir{k}, EXCLUDE_DIRS), 1))
		continue;
	end

	%--
	% handle add mode
	%--
	
	switch mode

		case 'flat'
			part = [root, filesep, out.dir{k}];

		case 'rec'
			part = get_path_str([root, filesep, out.dir{k}], 'rec');

	end
	
	%--
	% append partial path string to path string
	%--
	
	str = [str, pathsep, part];

end


%--------------------------------
% WHAT_EXT
%--------------------------------

function out = what_ext(source, varargin)

% what_ext - get directory content information using extensions
% -------------------------------------------------------------
%
% out = what_ext(source, ext1, ..., extN)
%
% Input:
% ------
%  source - source directory 
%  ext - desired file extensions
%
% Output:
% -------
%  out - structure with path, file extensions, and dir

%-----------------
% HANDLE INPUT
%-----------------

%--
% set directory
%--

if (nargin < 1) || isempty(source)
	source = pwd;
end

%--
% set extensions to search
%--

if length(varargin) < 1
	ext = [];
else
	ext = varargin;
end

%-----------------
% GET CONTENTS
%-----------------

%--
% output path field 
%--

out.path = source;

%--
% get directory contents
%--

content = dir(source); 

% NOTE: this removes self and parent directory references

content = content(3:end); 

%--
% get children directory contents
%--

D = {};

for k = length(content):-1:1
	
	if content(k).isdir
		D{end + 1} = content(k).name; content(k) = [];
	end
	
end

out.dir = flipud(D(:));

if isempty(ext)
	return;
end

%--
% get files with specified extensions
%--

for i = 1:length(ext)
	
	%--
	% create list of selected filenames
	%--
	
	L = {};
	
	for k = length(content):-1:1
		
		%--
		% get extension from name
		%--
		
		ix = findstr(content(k).name, '.');
		
		% NOTE: file has no extension in name
		
		if isempty(ix)
			continue;
		end
		
		r = content(k).name(ix(end) + 1:end);
		
		%--
		% select file based on extension
		%--
		
		% NOTE: consider making this case insensitive, at least optional
		
		if strcmp(r, ext{i})
			L{end + 1} = content(k).name; content(k) = [];
		end
		
	end
	
	L = flipud(L(:));
		
	%--
	% put cell array into field
	%--
	
	out.(ext{i}) = L;

end


%--------------------------------
% SET_TEXT_PROPERTIES
%--------------------------------

function set_text_properties(font)

% set_text_properties - set default text properties
% -------------------------------------------------
%
% set_text_properties(font)
%
% Input:
% ------
%  font - font structure

%--
% figure properties
%--

set(0, 'DefaultTextInterpreter', 'none');

%--
% axes properties
%--

set(0, ...
	'DefaultAxesFontName', font.name, ...
	'DefaultAxesFontSize', font.size ...
);

%--
% text properties
%--

set(0, ...
	'DefaultTextFontName', font.name, ...
	'DefaultTextFontSize', font.size ...
);

%--
% uicontrol properties
%--

set(0, ...
	'DefaultUicontrolFontName', font.name, ...
	'DefaultUicontrolFontSize', font.size ...
);


%--------------------------------
% GET_SIMPLE_FONTS
%--------------------------------

function fonts = get_simple_fonts

% get_simple_fonts - get available simple fonts
% ---------------------------------------------
%
% fonts = get_simple_fonts
%
% Output:
% -------
%  fonts - available simple fonts

%--
% declare simple fonts
%--

% NOTE: this is the preferred order

simple = { ...
	'Comic Sans MS', ...
	'comic sans ms', ...
	'Lucida Sans MS', ...
	'lucidasans', ...
	'Trebuchet MS', ...
	'trebuchet ms', ...
	'Century Gothic', ...
	'gothic', ...
	'Arial', ...
	'Lucida Console', ...
	'Tahoma', ...
	'Palatino Linotype', ...
	'Times New Roman', ...
    'Times', ...
    'Courier', ...
	'Verdana', ...
    'Segoe UI' ...
};

%--
% get available simple fonts
%--

[fonts, ix] = intersect(simple, listfonts); 

if isempty(ix)
	error('Unable to find any of the simple fonts.');
end


%-------------------------------
% INITIALIZE_PATH
%-------------------------------

function [root, start_path] = initialize_path

%--
% get xbat path
%--

root = fileparts(mfilename('fullpath'));

xbat_path = get_path_str(root, 'rec'); 

%--
% get initial path
%--

start_path = path;

% % init_path = strread(path, '%s', 'delimiter', pathsep);

% % init_path = setdiff(init_path, strread(xbat_path, '%s', 'delimiter', pathsep));

%--
% If XBAT is not called from another script, initialize MATLAB by deleting
% all figures and restoring default search paths.
%--

% % %Determine if xbat is being called by another script
% % stack = dbstack;
% % if length( stack ) > 1
% %     xbat_called = 1;
% % else
% %     xbat_called = 0;
% % end
% % if ~xbat_called
% %     
% %     % Close all figures - some of them might be XBAT palettes
% %     close all hidden
    
    % Restore default search paths
    if ~isempty( which( 'restoredefaultpath' ) )
        try
            %Suppress "Warning: Duplicate directory name", which the
            %faulty restoredefaultpath in MATLAB2012a produces.
            id = 'MATLAB:dispatcher:pathWarning';
            warning( 'off', id )
            restoredefaultpath;

            %Reactivate "Warning: Duplicate directory name"
            warning( 'on', id )
        catch
            disp( [ 'NOTE: Failed to restore default path, this is a minor problem in this version of MATLAB.' ] ); 
        end       
    end
% % end

% % default_path = strread(path, '%s', 'delimiter', pathsep);

%--
% get user path
%--

% % user_path_set = setdiff(init_path, default_path);
% % 
% % user_path = '';
% % 
% % for k = 1:length(user_path_set)
% % 	user_path = [user_path, user_path_set{k}, pathsep];
% % end

%--
% concatenate paths together, making sure that the user path is under xbat
% (get rid of "user path", since it results in name conflicts)
%--

path( path, xbat_path );
path( path, userpath );

%--
% This line disabled because it leads to namespace conflicts.
%--
% % path(path, user_path);

%--
% display message if path is changed
%--

% % if numel(user_path_set)
% % 	disp(' ');
% % 	disp(' To ensure proper execution, XBAT has moved the following to the end of the MATLAB path:');
% % 	disp(' ');
% %     
% % 	for k = 1:numel(user_path_set)
% % 		disp([' ', int2str(k), '. ', user_path_set{k}]);
% % 	end
% % 	
% % 	disp(' ');
% % 	disp(' To restore the previous path, restart MATLAB.');
% % 	disp(' ');	
% % end

%--
% create hard shortcut of palette presets, for purposes of backup
%--
if ispc
    if ~isdir( fullfile( xbat_root, 'Users', 'palette_presets' ) )
        if isdir( fullfile( xbat_root, 'Presets' ) )
            if exist( 'D:', 'dir' )
                drive = 'D:';
            elseif exist( 'C:', 'dir' )
                drive = 'C:';
            else
                drive = '';
            end
            if ~isempty( drive )
                presets_path = fullfile( xbat_root, 'Presets' );
                target_path = fullfile( drive, 'ToBeBackedUp', 'XBAT_palette_presets' );
                
                %delete existing backup
                if exist( target_path, 'dir' )
                    [ status, msg ] = rmdir( target_path, 's' );
                    if ~status
                        disp( ' ' )
                        disp( msg )
                    end
                end
                
                %create new backup
                [ status, msg ] = copyfile( presets_path, target_path, 'f' );
                if ~status
                    disp( ' ' )
                    disp( msg )
                end
            end
        end
    end
end