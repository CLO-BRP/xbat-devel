function [result, context] = compute(log, parameter, context)
% COUNT TAGS - compute

% History
%   19 Oct 2014 - Remove warning if preset.mat is missing.

result = struct;

%%
% Count Tags log action
%
% Makes a list of all tags used in selected log(s) in MATLAB Command Window
%
% Michael Pitzrick
% Cornell Lab of Ornithology
% msp2@cornell.edu

% % %
% % % Abort if log open
% % 
% % if log_is_open(log, 1)
% %   disp(' ')
% %   disp(' ')
% %   disp(['Please close log ', log_name(log)]); 
% %   disp(' ')
% %   return;
% % end
% % 
% % Find and display name of current log in Command Window
% % fn = log.file;
% % fn = fn(1:end-4);
% % disp(' ')
% % disp(' ')
% % disp(sprintf('%s %s%s', 'Tags for', fn))
% % disp('Number   Tag')
% % 
% % Determine list of tags used in log
% % TagList={'`'};   % set dummy tag
% % LenTagList=1;
% % count = 0;
% % len=log.length;
% % 
% % for i=1:len
% %   CurrentTag = log.event(i).tags;
% %   if ~iscell(CurrentTag)
% %     CurrentTag={CurrentTag};
% %   end
% %   
% %   if isempty(CurrentTag) || isempty(CurrentTag{1})    % Events not tagged
% %     CurrentTag = {'(NO TAG)'};
% %   end
% %   
% %   NumCurrentTags=length(CurrentTag);
% %   for j=1:NumCurrentTags
% %     k=1;
% %     CurrentTag{j}=CurrentTag{j};
% %     TagList{k}=TagList{k};
% % 
% % try
% %     while k<=LenTagList && ~strcmp(CurrentTag{j},TagList{k})
% %       k=k+1;
% %     end
% % catch
% %   disp(' ')
% % end    
% %     
% %     if k>LenTagList
% %       TagList=[TagList CurrentTag{j}];
% %       LenTagList=LenTagList+1;
% %       count = [count; 1];
% %     else
% %       count(k) = count(k) + 1;
% %     end
% %   end
% % end
% % 
% % if ~isequal(LenTagList,1)
% %   sort tags
% %   TagList = TagList(2:end);
% %   count = count(2:end);
% %   
% %   for i=1:LenTagList-1       % transpose cell array from 1 x n to n x 1
% %     TagListTrans{i,1}=TagList{i};
% %   end
% %   [TagListTrans IDX] = sort(TagListTrans);
% %   count = count(IDX(:));
% % 
% %   Display list of tags used in log in Command Window
% %   for i=1:LenTagList-1
% %     disp(sprintf('%6.0f   %s', count(i), TagListTrans{i}))
% %   end
% % else
% %   fprintf(2,'   No events in log.\n\n')
% % end