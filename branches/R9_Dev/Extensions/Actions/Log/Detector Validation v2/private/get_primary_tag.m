function tags = get_primary_tag(event, len)

tags = cell(len,1);

for i = 1:len
  
  curr_tag = event(i).tags;
  
  if ~isempty(curr_tag)
  
    if iscell(curr_tag)

      curr_tag = curr_tag{1};

    end

    tags{i} = curr_tag;
    
  end
  
end