function M = output_header(parameter, fn_truth, fn_test)

%set header
M{1} = 'Detection Validation';
M{2,1} = 'Time Run:';
M{2,5} = datestr(now, 21);

M{4,1} = 'Truth Log Suffix:';
M{4,5} = parameter.truth_suffix;

M{5,1} = 'Test Log Suffix:';
M{5,5} = parameter.test_suffix;

M{6,1} = 'Time Overlap (% test event duration):';
M{6,5} = parameter.overlap;

M{7,1} = 'True Negatives Explicit?';

if parameter.truth_flag
  
  M{7,5} = 'Yes';
  
else
  
  M{7,5} = 'No';
  
end

M{9,1} = 'Truth Log Name';

M{9,5} = fn_truth;

M{10,1} = 'Test Log Name';

M{10,5} = fn_test;


%% set column heads

%if no negative truth events (TN not defined explicitly)
if ~parameter.truth_flag
  
  M{12,1}  = 'TPR';
  M{12,2} = sprintf('TPR (%.0f%% CI lower)', parameter.plevel * 100);
  M{12,3} = sprintf('TPR (%.0f%% CI upper)', parameter.plevel * 100);
  
  M{12,4}  = 'TPR';
  M{12,5}  = 'FP/hr';
  M{12,6}  = 'PPV';

  M{12,7}  = 'Positive Truth Matches';
  M{12,8}  = 'Positive Truth Misses';
  M{12,9}  = 'Positive Truth Total';

  M{12,10}  = 'Test Matches';
  M{12,11}  = 'Test Misses';
  M{12,12}  = 'Test Total';
  
  M{12,13}  = 'Truth Log';
  M{12,14} = 'Test Log';

  M{13,7}  = 'TP';
  M{13,8}  = 'FN';
  M{13,11} = 'FP';
  

%if negative truth events (TN defined explicitly)
else

  M{12,1} = 'TPR';
  M{12,2} = 'TPR (CI lower)';
  M{12,3} = 'TPR (CI upper)';  
  
  M{12,4} = 'TPR(check)';
  M{12,5} = 'FPR';
  M{12,6} = 'FP/hr';
  M{12,7} = 'PPV';

  M{12,8} = 'Positive Truth Matches';
  M{12,9} = 'Positive Truth Misses';
  M{12,10} = 'Positive Truth Total';

  M{12,11} = 'Negative Truth Matches';
  M{12,12} = 'Negative Truth Misses';
  M{12,13} = 'Negative Truth Total';
  
  M{12,14} = 'Positive + Negative Truth Total';
  M{12,15} = 'Truth Total';

  M{12,16} = 'Test Matches Positive Truth';
  M{12,17} = 'Test Matches Negative Truth';
  M{12,18} = 'Test Matches Positive or Negative Truth';
  M{12,19} = 'Test Total';
  
  M{12,20} = 'Truth Log';
  M{12,21} = 'Test Log';

  M{13,8}  = 'TP';
  M{13,9}  = 'FN';
  M{13,11} = 'FP';
  M{13,12} = 'TN';

  M{13,16} = 'TP (test)';
  M{13,17} = 'FP (test)';

end
