function [result, context] = prepare(parameter, context)
result = struct;

% DETECTOR VALIDATION - prepare

% TO DO
%
% 1. option to count missing logs as zero events
%
% 2. filter by pos_truth_tag, if specified
%
% 3. filter by neg_truth_flag, if specified, and report FPR
%

%% user parameters

if ~any([parameter.xls_flag, parameter.roc_flag, parameter.annot_truth_log_flag, parameter.annot_test_log_flag])
  
  fprintf('WARNING:\nNo output selected. Action terminated.\n')
  
  h = warndlg('No output selected. Action terminated.', 'WARNING');
  
  movegui(h, 'center')
  
  return;
  
end

if isempty(parameter.neg_truth_tag)
  
  parameter.truth_flag = 0;
  
else
  
  parameter.truth_flag = 1;
  
end


%% make vector of selected log names, checking for errors

% make vector of selected log names
[status1, target, NumLogs, order_flag] = list_logs(context, parameter);

if status1
  
  return;
  
end

%check if any selected log is open in an XBAT browser
status2 = log_is_open_pitz(target, NumLogs);

if status2
  
  return;
  
end

%check if logs are paired correctly
status3 = logs_paired(target, NumLogs, parameter, order_flag); 

if status3
  
  return;
  
end


%% match log pairs

if parameter.roc_flag
  
  TEMP = zeros(20,1);
  
else
  
  TEMP = 0;
  
end

if parameter.truth_flag

  cum_count = struct('truth', struct('TP', TEMP, 'FN', TEMP, 'FP', TEMP, 'TN', TEMP, 'total', 0), ...
                     'test', struct('TP', TEMP, 'FP', TEMP, 'total', 0));

else

  cum_count = struct('truth', struct('TP', TEMP, 'FN', TEMP, 'total', 0), ...
                     'test', struct('TP', TEMP, 'FP', TEMP, 'total', 0));
  
end

cum_duration = 0;

%for each log pair
for i = 1:2:NumLogs
    
  %load truth log and test log
  if order_flag
    
    truth = target{i};
    
    test = target{i+1};

  else
    
    truth = target{i+1};
    
    test = target{i};

  end

  [sound_truth, fn_truth] = fileparts(truth);

  [sound_test, fn_test] = fileparts(test); 


  %% create headers for output XLS files
  if parameter.xls_flag || parameter.roc_flag

    %ask user where to put XLS output, if XLS output has been selected
    out_dir = uigetdir(pwd, 'Select XLS output folder.');

    if ischar(out_dir) && exist(out_dir, 'dir')

      cd(out_dir)

      if parameter.xls_flag

        out_name = fullfile(out_dir, [context.library.name, '_DetectorValidation_Summary_', datestr(now, 30)]);

        %header for xls
        M = output_header(parameter, fn_truth, fn_test);

      end

      if parameter.roc_flag

        roc_name = fullfile(out_dir, [context.library.name, '_DetectorValidation_ROC_', datestr(now, 30)]);

        %header for xls
        M_header = roc_header(parameter, fn_truth, fn_test);

      end

    else

      fprintf('\nUser terminated action.\n')

      return;

    end
  end
  
  
  
  
  
  
  %load truth log
  truth_log = get_library_logs('logs', [], sound_truth, fn_truth);
  
  %check if a single, valid log is specified
  if ~(ischar(sound_truth) && ischar(fn_truth) && isequal(length(truth_log),1))
  
    txt = sprintf('Action terminated.\nLog did not load.%s', [fn_truth, '\', sound_truth]);

    fprintf(2,'ERROR:\n%s\n\n', txt);

    h = warndlg(txt, 'ERROR');

    movegui(h, 'center')

    return;
    
  else
    
    % rename file field in log
    fn_truth_ext = [fn_truth '.mat'];
    truth_log.file = fn_truth_ext;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath sound_truth '\Logs\'];
    truth_log.path = LogPath;
    
    %save updated file name and path to log
    log_save(truth_log);   
    
  end
  
  %load test log
  test_log = get_library_logs('logs', [], sound_test, fn_test);
  
  %check if a single, valid log is specified
  if ~(ischar(sound_test) && ischar(fn_test) && isequal(length(test_log),1))
  
    txt = sprintf('Action terminated.\nLog did not load.%s', [fn_test, '\', sound_test]);

    fprintf(2,'ERROR:\n%s\n\n', txt);

    h = warndlg(txt, 'ERROR');

    movegui(h, 'center')

    return;
    
  else
    
    % rename file field in log
    fn_test_ext = [fn_test '.mat'];
    test_log.file = fn_test_ext;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath sound_test '\Logs\'];
    test_log.path = LogPath;
    
    %save updated file name and path to log
    log_save(test_log);
    
  end
  
  %check if all sound file names the same,
  %if user indicates sound files were renamed since logs last open in XBAT, 
  %just check if the time stamp of the first sound file is the same
  if (~parameter.sound_check_flag && ~isequal(truth_log.sound.file, test_log.sound.file)) ...
  || ( parameter.sound_check_flag && ~isequal(file_datenum(truth_log.sound.file{1}), file_datenum(test_log.sound.file{1})))
%   if ~isequal(truth_log.sound.file, test_log.sound.file) ...
%    || (parameter.sound_check_flag && ~strcmp(sound_name(truth_log.sound), sound_name(test_log.sound)))

    txt1 = sprintf('Action terminated.\nLog pair is not from the same sound.');

    txt2 = sprintf('Log: %s\nis from\nSound: %s\n', truth, sound_name(truth_log.sound));

    txt3 = sprintf('Log: %s\nis from\nSound: %s\n', test, sound_name(test_log.sound));

    txt =sprintf('%s\n%s\n%s', txt1, txt2, txt3);

    fprintf(2,'ERROR:\n%s\n\n', txt);

    h = warndlg(txt, 'ERROR');

    movegui(h, 'center')

    return;   
    
  end
  
  %check same log.sound.channels in truth log as previous logs
  if ~exist('num_chan', 'var')
    
    num_chan = truth_log.sound.channels;
    
  end
  
  if ~isequal(truth_log.sound.channels, num_chan)

    txt1 = sprintf('Action terminated.\nTruth log has different number of channels than previous logs.');

    txt2 = sprintf('Log: %s\n\n', sound_name(truth_log.sound));

    txt3 = sprintf('%.0f\n', truth_log.sound.channels);

    txt =sprintf('%s\n%s\n%s', txt1, txt2, txt3);

    fprintf(2,'ERROR:\n%s\n\n', txt);

    h = warndlg(txt, 'ERROR');

    movegui(h, 'center')

    return;   
    
  end
  
  %compare truth and test, truth events are TP/FN, test events are TP/FP
  [count, annotated_truth_log, annotated_test_log] = match(truth_log, test_log, parameter);
  
  %write annotated truth log
  if parameter.annot_truth_log_flag
    
    log_save(annotated_truth_log)
    
  end
  
  %write annotated test log
  if parameter.annot_test_log_flag
    
    log_save(annotated_test_log)
    
  end
  
  %accummulate summary XLS output, if user wants it
  if parameter.xls_flag

    %increment cumulative stats
    cum_count = sum_fields(cum_count, count, parameter);

    %increment cumulative sound duration
    cum_duration = cum_duration + truth_log.sound.duration;

    %output results for xls  
    M = out_matrix(M, count, truth_log.sound.duration, parameter, fn_truth, fn_test);
    
  end
  
  %write worksheet to ROC XLS, if user selected it
  if parameter.roc_flag || parameter.fig_flag

    M2 = roc_matrix(M_header, count, truth_log.sound.duration, parameter);
    
  end
    
  if parameter.roc_flag

    xlswrite(roc_name, M2, fn_test(1:min(31,length(fn_test)))); 
    
  end  
end

if parameter.fig_flag
  
  fh = figure('menubar', 'figure');
  
  Threshold = [M2{15:end,1}];
  
  TPR = [M2{15:end,2}];
  
  FPR = [M2{15:end,6}];
  
  plot(FPR, TPR)
  
  if parameter.truth_flag
  
    xlabel('FP/hr')
    
  else
  
    xlabel('FP/hr')
    
  end
  
  text(FPR + 0.01, TPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
  
  ylabel('TPR')
  
  title(sprintf('ROC Curve\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
  
  saveas(fh, roc_name)
  
end

%output XLS, if user has selected it
if parameter.xls_flag
  
  M = out_matrix(M, cum_count, cum_duration, parameter, 'TOTAL', 'TOTAL');

  xlswrite(out_name, M);
  
end

if parameter.roc_flag

  M2 = roc_matrix(M_header, cum_count, cum_duration, parameter);

  xlswrite(roc_name, M2, 'summary'); 
  
end

