function status = test_channels( logs )
%test if all sounds in the library have the same number of channels

status = 1;

%list names of XBAT sounds associated with each selected XBAT log
snd_name_logs = cellfun( @fileparts, logs, 'UniformOutput', false );

%retrieve sound structures for each sound
sounds = get_library_sounds( '', 'name', snd_name_logs );

%find number of channels in each sound
NumChans = [ sounds( : ).channels ]';

%stop execution if all sounds do not have the same number of channels
unique_chan = unique( NumChans );
if length( unique_chan ) > 1
  d1 = 'Sounds in XBAT library do not have the same number of channels.';
  d2 = 'Channels   Sounds';
  d3 = strcat( num2str( NumChans ), '         "', sound_name( sounds )' , '"' );
  d3 = sprintf( '  %s\n', d3{ : } );
  d = sprintf( '%s\n\n%s\n', d1, d2, d3 );
  fail( d, 'WARNING' )
  return;
end

%Stop execution if fewer than 3 channels in sounds
if unique_chan < 3
    fail( 'Sounds must have 3 or more channels to do locations', 'WARNING' )
    return;
end

status = 0; 