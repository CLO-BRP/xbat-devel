function [result, context] = prepare(parameter, context)

%% Make RL-TD Events and Guide Logs - prepare

result = struct;
context.state.kill = 0;

if isempty(parameter.KeepChannels)
  fprintf(2,'ERROR: No channels selected. Select at least one channel and try again.\n\n');
  context.state.kill = 1;
  return;
end

if ~isfield(context.sound, 'type')
  fprintf(2,'ERROR: No sound selected. Select a single sound in XBAT palette and try again.\n');
  context.state.kill = 1;
  return;
end
  
  

%% REFRESH LOG.FILE AND LOG.PATH
% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end

