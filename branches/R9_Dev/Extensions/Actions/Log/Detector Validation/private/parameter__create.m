function parameter = parameter__create(context)

% DETECTOR VALIDATION - parameter__create

parameter = struct;

parameter.test_suffix = 'BHWdet1c_T1';

parameter.truth_suffix = 'Bowhead';

parameter.overlap = 0;

parameter.pos_truth_tag = '';

parameter.neg_truth_tag = '';

parameter.truth_flag = 0;

parameter.sound_check_flag = 0;

parameter.xls_flag = 1;

parameter.annot_truth_log_flag = 0;

parameter.annot_test_log_flag = 0;

parameter.plevel = 0.9;
