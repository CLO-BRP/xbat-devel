function [ table_new, headers, selection_idx, status ] = migrate_table( table, sound )

% Migrate Raven Sound Selection Table by updating Begin Time and End Time 
% to make compatible with new sound stream
%
% Inputs
%   table = m x n character matrix containing original Sound Selection Table
%   sound = XBAT sound struct
%
% Outputs
%   table_new = m x n char matrix containing migrated Sound Selection Table
%   status = 0 if success, 1 if fail

% Note: Cannot use tdfread due to tabs at end of line.

% History
%   msp2  9 Apr 2015
%       Return index of "Selection" column.  
%       Expect "Begin File" values to have file extension.

%---
% Initializations
%---
status = 1;
tab = char( 9 );

%---
% Read headers
%---
line = table( 1, : );
line = strtrim( line );
line = strrep( line, [ tab, tab ], '' );
line = strrep( line, [ tab, tab ], '' );
headers = strsplit( line, '\t' );
num_col = length( headers );
body = table( 2:end, : );

[ m , ~ ] = size( body );
table_new = cell( m, num_col );
for i = 1:m
  line = body( i, : );
  line = strtrim( line );
  line = strrep( line, [ tab, tab ], '' );
  line = strrep( line, [ tab, tab ], '' );
  C = strsplit( line, '\t' );
  table_new( i, 1:length( C ) ) = C;
end

%---
% Convert "Begin Time", "End Time", "Begin File" and "File Offset (s)" columns into vectors
%---
begin_time_idx = strcmp( headers, 'Begin Time (s)' );
end_time_idx = strcmp( headers, 'End Time (s)' );
begin_file_idx = strcmp( headers, 'Begin File' );
file_offset_idx = strcmp( headers, 'File Offset (s)' );
selection_idx = strcmp( headers, 'Selection' );

begin_time_str = table_new( :, begin_time_idx );
begin_time = str2double( begin_time_str );
end_time_str = table_new( :, end_time_idx );
end_time = str2double( end_time_str );
begin_file = table_new( :, begin_file_idx );
file_offset_str = table_new( :, file_offset_idx );
file_offset = str2double( file_offset_str );

%---
% Make vectors of XBAT file stream and cumulative time for XBAT file stream
%---
xbat_file_stream = sound.file;
% % [~, xbat_file_stream ] = cellfun( @fileparts, sound.file, 'UniformOutput', 0 );
if length( sound.cumulative ) > 1
    
    % convert cumulative number of samples to cumulative number of seconds
    cumulative_file_time = sound.cumulative ./ sound.samplerate;
    
    % find number of seconds of recording before each file in stream
    time_before = [ 0 ; cumulative_file_time( 1:end-1 ) ];
else
    time_before = 0;
end

%---
% Calculate new "Begin Time" and "End Time" for each selection
%--
new_begin_time = zeros( m, 1 );
new_end_time = new_begin_time;
j = 0;
skipped_lines = [];
for i = 1:m
   
    % find begin file in XBAT sound file stream
    idx = strcmp( xbat_file_stream, begin_file{ i } );
    
    % if no matching sound file found, skip selection
    if ~any( idx )
        skipped_lines = [ skipped_lines ; i ];
        continue;
    end
    
    % find number of seconds in file stream before begin file
    file_time = time_before( idx );
    
    % calculate new Begin Time
    curr_begin_time = file_time + file_offset( i );
    curr_end_time = curr_begin_time + diff( [ begin_time( i ), end_time( i ) ] );
    
    % put times in vector
    j = j + 1;
    new_begin_time( j ) = curr_begin_time;
    new_end_time( j ) = curr_end_time;    
end

% trim skipped lines from "Begin Time" and "End Time" vectors
new_begin_time = new_begin_time( 1:j );
new_end_time = new_end_time( 1:j );

%---
% If any lines weren't skipped, replace "Begin Time" and "End Time" columns in table
%---
if ~isempty( new_begin_time )
    
    % If any lines were skipped, delete them from output table
    if ~isempty( skipped_lines )
        table_new( skipped_lines, : ) = [];
    end
    
    % replace "Begin Time" and "End Time" columns in table
    new_begin_time_str = num2str( new_begin_time, 17 );
    new_begin_time_cell = cellstr( new_begin_time_str );
    table_new( :, begin_time_idx ) = new_begin_time_cell;
    new_end_time_str = num2str( new_end_time, 17 );
    new_end_time_cell = cellstr( new_end_time_str );
    table_new( :, end_time_idx ) = new_end_time_cell;
    
%---
% If all lines were skipped, table is empty 
else
    table_new = {};
end

status = 0;
