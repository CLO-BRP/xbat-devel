function [flag, g] = log_open(h, p, opt)

% log_open - open log in browser figure
% -------------------------------------
%
% [flag, g] = log_open(h, p)
%
% Input:
% ------
%  h - handle to browser figure (def: gcf)
%  p - log path or full location or log
%  opt - warning option
%
% Output:
% -------
%  flag - open confirmation flag
%  g - handle of figure that has log open

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 7028 $
% $Date: 2006-10-16 12:35:31 -0400 (Mon, 16 Oct 2006) $
%--------------------------------
%
% History
%   msp2 - 28 Oct 2013
%
%       1. Revert REV 3932, which caused XBAT to "forget" where the
%       default path for logs for each sound is.
%
%       2. Improve reporting when user attempts to open log in
%       inappropriate sound.
%
%       3. Record history in log.userdata when use choses to open log in
%       inappropriate sound.
%
%       4. Enable multiselect when opening logs
%   msp2 - 10 Mar 2014
%       Stop enabling Annotate menu in browser when log opens.
%	msp2 - 22 Mar 2014
%		Make all logs visible when opened.
%		Stop log opening if path length too long.
%   msp2 - 24 Mar 2014
%       Adapt to test_path_length accommodation for multi-log input.
%   msp2 - 25 Apr 2014
%       Temporarily disable "Browse" and "Export" menu items in "Log" menu in browser.
%   msp2 - 28 May 2014
%       Fix bug introduced in REV 4030 (/branches/R8_DEV), "Enable
%         multiselect when opening logs".
%   msp2 - 26 May 2015
%       Remove check for sound type and sound attributes for oDeLMA compatibility.

% NOTE: this function can be improved through modularization, separating the code
% that opens the log from the code that generates and updates the log menu

%--------------------------------------------------------
% HANDLE INPUT
%--------------------------------------------------------

%--
% set warning option
%--

if (nargin < 3) || isempty(opt)
	opt = 1;
end

%--
% get figure handle and userdata
%--

if (nargin < 1) || isempty(h)
	h = get_active_browser;
end

data = get(h, 'userdata');

openflag = 1;

pi = pwd;

%--
% handle path or location input
%--

if (nargin < 2) || isempty(p)
	
	%--
	% set open to be interactive
	%--
	
	inter = 1;
	
	%--
	% build library appropiate path to start dialog
	%--
	
	info = parse_tag(get(h, 'tag'));
	
	lib = get_libraries([], 'name', get_library_file_name(info.library));
		
	% start looking for the log in the library appropiate place
	
	p = [lib.path, info.sound filesep, 'Logs'];
	
elseif isstruct(p)
		
	openflag = 0; inter = 0;
		
else
		
	%--
	% check for file or directory location to see if open is interactive
	%--

	switch exist(p)

		% file location, not interactive 

		case (2)
			inter = 0;

		% directory location, interactive

		case (7)
			inter = 1;

		% error condition, this needs some work

		otherwise

			%--
			% handle error condition depending on warn option
			%--

			% this function needs to be redesigned, figure out dependencies

			if opt
				error(['No ''', p, ''' directory or file exists.']);
			else
				flag = 0; return;
			end

	end

end

%--
% get file if needed
%--

if inter
	
	%--
	% try to get log file starting from specified directory
	%--
		
	cd(p);
	
	[f, p] = uigetfile( ...
		{ ...
			'*.mat','MAT-Files (*.mat)'; ...
			'*.zip','ZIP-Files (*.zip)'; ...
			'*.*','All Files (*.*)' ...
		}, ...
		'Select XBAT Log File: ', ...
        'Multiselect', 'On' ...
	);
	
	% return if cancel
	
	if ~iscell( f ) && ~all( f )
		return;
	end
	
	cd(pi); 
	
	%--
	% build log location string
	%--
	
	p = strcat( p, f );
	
end

if openflag
	
	%--
	% load log from file
	%--

	log = log_load(p);
	
	if isempty(log)
		flag = 0; g = []; cd(pi); return;
	end

	%--
	% separate location into path and filename
	%--
    
    if ~iscell( p )
        p = { p };
    end
    
    [ p, fn, ext ] = cellfun( @fileparts, p, 'un', 0 );
    
    fn = strcat( fn, ext );
    
    p = [ p{ 1 }, filesep ];

else
	
	log = p;
	
	p = log.path;
	
	fn = log.file;

end
	
%--------------------------------------------------------
% OPEN LOG
%--------------------------------------------------------

for i = 1:length( log )

	%--
	% check if log is open
	%--

	if log(i).open

		%--
		% check that log is in fact open
		%--

		g = log_is_open(log(i));

		%--
		% display warning dialog
		%--

		if ~isempty(g)

			%--
			% display warning according to warn option
			%--

			if opt

				%--
				% log is open in this browser
				%--

				if (g == h)

					% perhaps remove warning and turn on display

					tmp = warn_dialog( ...
						['Log ''' log(i).file ''' is already open in this browser.'], ...
						' XBAT Warning  -  Log Open', ...
						'modal' ...
					);

				%--
				% log is open in another browser
				%--

				else

					tmp = warn_dialog( ...
						['Log ''' log(i).file ''' is currently open in another browser.'], ...
						' XBAT Warning  -  Log Open', ...
						'modal' ...
					);

				end

				waitfor(tmp);

			end

			%--
			% output proper flag and return
			%--

			flag = 0; return;

		end

	else

		%--
		% set log flag to open
		%--

		log(i).open = 1;
	end

	%--
	% update path and file if log file has been moved or renamed
	%--
	if ~strcmp(log(i).path,p) || ~strcmp(log(i).file,fn{i})
		log(i).path = p; log(i).file = fn{i};
	end
    
	%--
    % stop if path too long
	%--
    flag = test_path_length( log ); %error if path too long
    if ~flag
        g = [];
        return;
    end
    
	%----------------------------------------------------
	% UPDATE/FIX
	%----------------------------------------------------
    log(i).visible = 1;

	% NOTE: make 'no patch' signature equal to zero not -1
	if (log(i).patch < 0)
		log(i).patch = 0;
	end

	%--
	% check that log corresponds to sound
	%--

	[ is_compatible, report ] = sound_compare( log(i).sound, data.browser.sound );

	if is_compatible == 0
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        % Change from 7-member report vector to 4-member report vector
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		fn = strrep( log(i).file( 1:end-4 ), '_', '\_' );
		snd_name = strrep( sound_name( data.browser.sound ), '_', '\_' );
		f11 = '\fontsize{12}\bf';
		f12 = '\fontsize{11}';
		
		summary_cell = '';
		if ~report( 4 )
			t1 = 'Channels';
			t2 = 'Channels reported for events may be incorrect.';
			summary_cell = [ summary_cell; { sprintf( '{%s -%s}\n{%s	 %s}\n', f11,  t1, f12, t2) } ];
		end
		if ~report( 3 )
			t1 = 'Sound File Duration';
			t2 = 'Date and time reported for events will be incorrect.';
			summary_cell = [ summary_cell; { sprintf( '{%s -%s}\n{%s	 %s}\n', f11,  t1, f12, t2) } ];
		end	
		if ~report( 1 )
			t1 = 'Number of Sound Files';
			t2 = 'Date and time reported for events will be incorrect.';
			summary_cell = [ summary_cell; { sprintf( '{%s -%s}\n{%s	 %s}\n', f11,  t1, f12, t2) } ];
		end
		if ~report( 2 )
			t1 = 'Sound File Names';
			t2 = 'Events will be displayed with wrong sound.';
			summary_cell = [ summary_cell; { sprintf( '{%s -%s}\n{%s	 %s}\n', f11,  t1, f12, t2) } ];
        end
		
		f1 = '\fontsize{22}\bf';
		f2 = '\fontsize{14}\bf';
		f3 = '\fontsize{11}\bf';
		d1 = 'Wrong sound for log!';
		d2 = 'Choose "No" to exit safely (strongly recommended!)';
		d3 = 'Choose "Yes" if you are sure you want to open this log in the wrong sound ' ;
		d4 = '(not recommended).  ';
		d5 = 'Please be advised that the following metadata in the log will be ';
		d6 = 'overwritten with incorrect information:';
		d10 = sprintf( '{%s%s}\n\n{%s%s}\n\n{%s%s%s%s%s}\n\n', f1, d1, f2, d2, f3, d3, d4, d5, d6 );
		
		f4 = '\fontsize{11}\rm';
		d6 = sprintf( '%s\n', summary_cell{ : } );
		d7 = sprintf( 'Log name: %s', fn );
		d8 = sprintf( 'Sound name: %s', snd_name );
		d = sprintf( '%s%s%s\n\n%s\n%s\n', d10, d6, f4, d7, d8 );

		t = ' XBAT Question > Open Log';

		option.Default = 'No';
		option.Interpreter = 'tex';
		tmp = questdlg( d, t, option );
		
		if ~strcmp( tmp, 'Yes' )
			flag = 0;
			return;
		else
			
			disp('************ update log(i).sound??? **************')
			
			%recover history of sound changes for log
			if isfield( log(i).userdata, 'sound_history' )
				sound_history = log(i).userdata.sound_history;
			else
				sound_history = [];
			end
			
			%create new sound change history entry
			curr_sound_history.previous = log(i).sound;
			curr_sound_history.new	  = data.browser.sound;
			curr_sound_history.date	 = now;
			
			%update sound change history in log
			sound_history = [ sound_history, curr_sound_history ];
			log(i).userdata.sound_history = sound_history;
		end

	end

	%--
	% append log to browser data structure
	%--

	if isempty(data.browser.log)

		data.browser.log = log(i);
		data.browser.log_active = 1;

	else

		m = length(data.browser.log);

		data.browser.log(m + 1) = log(i);
		data.browser.log_active = m + 1;

	end

	%--
	% save changes to log
	%--

	log_save(log(i));

	%--------------------------------------------------------
	% CREATE LOG RELATED MENUS
	%--------------------------------------------------------

	%--
	% update userdata
	%--

	set(data.browser.log_menu.log,'enable','on');
    
    % temporarily disable buggy menu items
    set( data.browser.log_menu.log( [ 4, 6 ] ), 'Enable', 'off' )

	%--
	% Log
	%--

	file = file_ext(log(i).file);

	L = { ...
		file, ...
		[file ' Options'] ...
	};

	n = length(L);

	S = bin2str(zeros(1,n));
	S{1} = 'on';

	g1 = menu_group(data.browser.log_menu.log(1),'browser_log_menu',L,S);

	set(g1,'tag',file);

	%--
	% Log information, annotation, and measurement
	%--

	L = { ...
		'Log', ...
		'Annotation', ...
		'Measurement' ...
	};

	n = length(L);

	S = bin2str(zeros(1,n));
	S{2} = 'on';

	g2 = menu_group(g1(1),'browser_log_menu',L,S);

	set(g2,'tag',file);

	%--
	% Log and Info
	%--

	if ((isempty(log(i).time) || isempty(log(i).freq) || isempty(log(i).channel)) && log(i).length)

		[time,freq,channel] = log_info_update(log(i));

		log(i).time = time;

		log(i).freq = freq;

		log(i).duration = diff(time);

		log(i).bandwidth = diff(freq);

		log(i).channel = channel;

	end

	[tstr,fstr] = log_labels(h,log(i));

	L = { ...
		['Path:  ' log(i).path], ...
		['File:  ' log(i).file], ...
		['Number of Events:  ' int2str(log(i).length)], ...
		['Current ID:  ' int2str(log(i).curr_id)], ...
		['Channels:'], ...
		['Start Time:  ' tstr{1}], ...
		['End Time:  ' tstr{2}], ...
		['Duration:  ' tstr{3}], ...
		['Min Freq:  ' fstr{1}], ...
		['Max Freq:  ' fstr{2}], ...
		['Bandwidth:  ' fstr{3}], ...
		['Log Info:'] ...
	};

	n = length(L);

	S = bin2str(zeros(1,n));
	S{3} = 'on';
	S{5} = 'on'; 
	S{6} = 'on';
	S{9} = 'on';
	S{end} = 'on';

	g3 = menu_group(get_menu(g2,'Log'),'',L,S);

	set(g3,'tag',file);

	if (~isempty(log(i).modified))
		L = { ...
			['Author:  ' log(i).author], ...
			['Created:  ' datestr(log(i).created)], ...
			['Modified:  ' datestr(log(i).modified)] ...
		};
	else
		L = { ...
			['Author:  ' log(i).author], ...
			['Created:  ' datestr(log(i).created)] ...
		};
	end

	n = length(L);

	S = bin2str(zeros(1,n));
	S{2} = 'on';

	tmp = menu_group(g3(end),'',L,S);

	set(tmp,'tag',file);

	%--
	% Channel
	%--

	n = length(log(i).channel);

	L = cell(1,n);
	for k = 1:length(log(i).channel)
		L{k} = ['Channel ' int2str(log(i).channel(k))];
	end

	menu_group(get_menu(g3,'Channels:'),'',L);

	%--
	% Annotation
	%--

	%--
	% Measurement
	%--

	%--
	% Log Options
	%--

	L = { ...
		'Color', ...
		'Line Style', ...
		'Line Width', ...
		'Opacity', ...
		'Auto Save Log', ...
		'Save Log', ...
		'Backup Log', ...
		'Close' ...
	};

	n = length(L);

	S = bin2str(zeros(1,n));
	S{5} = 'on'; 
	S{end} = 'on';

	g2 = menu_group(g1(2),'browser_log_menu',L,S);

	set(g2,'tag',file);

	if (log(i).autosave)
		set(get_menu(g2,'Auto Save Log'),'check','on');
	end

	if (log(i).saved)
		set(get_menu(g2,'Save Log'),'enable','off');
	end

	set(get_menu(g2,'Backup Log'),'enable','off');
	set(get_menu(g2,'Export Log ...'),'enable','off');

	%--
	% Color
	%--

	[L,S] = color_to_rgb;

	tmp = menu_group(get_menu(g2,'Color'),'browser_log_menu',L,S);

	set(tmp,'tag',file);

	ix = find(strcmp(rgb_to_color(log(i).color),color_to_rgb));
	if (~isempty(ix))
		set(tmp(ix),'check','on');
	end

	%--
	% Line Style
	%--

	[L,S] = linestyle_to_str('','strict');

	tmp = menu_group(get_menu(g2,'Line Style'),'browser_log_menu',L,S);		

	set(tmp,'tag',file);

	ix = find(strcmp(L,str_to_linestyle(log(i).linestyle)));
	if (~isempty(ix))
		set(tmp(ix),'check','on');
	end

	%--
	% Line Width
	%--

	L = {'1 pt','2 pt','3 pt','4 pt'};

	tmp = menu_group(get_menu(g2,'Line Width'),'browser_log_menu',L);

	set(tmp,'tag',file);

	ix = find(log(i).linewidth == [1, 2, 3, 4]);
	if (~isempty(ix))
		set(tmp(ix),'check','on');
	end

	%--
	% Opacity
	%--

	L = { ...
		'Transparent', ...
		'1/8 Alpha', ...
		'1/4 Alpha', ...
		'1/2 Alpha', ...
		'3/4 Alpha', ...
		'Opaque' ...
	};

	n = length(L);

	S = bin2str(zeros(1,n));
	S{2} = 'on';
	S{end} = 'on';

	tmp = menu_group(get_menu(g2,'Opacity'),'browser_log_menu',L,S);

	set(tmp,'tag',file);

	ix = find(log(i).patch == [-1, 1/8, 1/4, 1/2, 3/4, 1]);
	if (~isempty(ix))
		set(tmp(ix),'check','on');
	end

	%--------------------------------------------------------
	% UPDATE RELATED MENUS UPON OPENING LOG
	%--------------------------------------------------------

	L = file_ext(struct_field(data.browser.log,'file'));

	% try sorting these menus

	tmp = length(data.browser.log);

	if (tmp > 1)

		[L,ix] = sort(L);

		ms = find(data.browser.log_active == ix);

		v = struct_field(data.browser.log,'visible');
		v = v(ix);

	else

		ms = data.browser.log_active;

		v = data.browser.log.visible;

	end

	%--
	% update active menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Active');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_log_menu',L);
	data.browser.log_menu.active = tmp;

	% set(tmp(data.browser.log_active),'check','on');
	set(tmp(ms),'check','on');

	%--
	% update display menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Display');
	delete(get(tmp,'children'));

	S = bin2str(zeros(length(L) + 2,1));
	S{2} = 'on';
	S{end} = 'on';

	tmp = menu_group(tmp,'browser_log_menu',{'No Display',L{:},'Display All'},S);
	data.browser.log_menu.display = tmp;

	for k = 1:length(data.browser.log)
%	if (data.browser.log(k).visible)
		if (v(k))
			set(tmp(k + 1),'check','on');
		else
			set(tmp(k + 1),'check','off');
		end
	end

	%--
	% update browse menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Browse');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_log_menu',L);
	data.browser.log_menu.browse = tmp;

	%--
	% update export menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Export');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_log_menu',L);
	data.browser.log_menu.export = tmp;

	%--
	% update strip menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Strip');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_log_menu',L);
	data.browser.log_menu.strip = tmp;

	%--
	% update copy to workspace menu
	%--

	tmp = get_menu(data.browser.log_menu.log,'Workspace');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_log_menu',L);
	data.browser.log_menu.copy_to_workspace = tmp;

	%--
	% update log selection to menu
	%--

	tmp = get_menu(data.browser.edit_menu.edit,'Log Selection To');
	delete(get(tmp,'children'));

	tmp = menu_group(tmp,'browser_edit_menu',L);
	data.browser.edit_menu.log_to = tmp;

	% set(tmp(data.browser.log_active),'check','on');
	set(tmp(ms),'check','on');

	%--------------------------------------------------------
	% SORT LOG INFORMATION MENUS
	%--------------------------------------------------------

	%--
	% get parent log menu
	%--

	tmp = findobj(h,'type','uimenu','label','Log');

	ix = find(cell2mat(get(tmp,'parent')) == h);

	%--
	% get children and select the ones to sort
	%--

	% assume the children come out in reverse order

	ch = get(tmp(ix),'children');

	ch = flipud(ch(:));

	ch = ch(7:end);

	%--
	% sort the menus
	%--

	if (length(ch) > 2)

		[ignore,ix] = sort(get(ch(1:2:end),'label'));

		ch = reshape(ch,2,length(ch)/2)';
		ch = ch(ix,:)';
		ch = ch(:);

		% note that this depends on the number of static menu items in the log menu

		for k = 1:length(ch)
			set(ch(k),'position',k + 6);
		end

	end

	%--
	% update measure menu, make measurements available
	%--

	set(data.browser.measure_menu.measure,'enable','on');

	%--
	% update annotate menu, make annotations available
	%--

% 	set(data.browser.annotate_menu.annotate,'enable','on');

	%--
	% update renderer mode and menus, eventually get rid of the renderer menus
	%--

	update_renderer(h, [], data);

	%--
	% update userdata
	%-

	set(h, 'userdata', data);

	flag = 1;

	%--
	% perform log related palette updates
	%--

	% NOTE: this does not affect the state of the browser on open, only on close

	update_log_palette(h, data);

	update_extension_palettes(h, data);

	%--
	% update find events if needed
	%--

	update_find_events(h, [], data);

	%--
	% update display 
	%--

	browser_display(h, 'events', data);

end
