


CurrPorts v1.10
Copyright (c) 2004 - 2006 Nir Sofer
Web site: http://www.nirsoft.net



Description
===========

CurrPorts displays the list of all currently opened TCP/IP and UDP ports
on your local computer. For each port in the list, information about the
process that opened the port is also displayed, including the process
name, full path of the process, version information of the process
(product name, file description, and so on), the time that the process
was created, and the user that created it.
In addition, CurrPorts allows you to close unwanted TCP connections, kill
the process that opened the ports, and save the TCP/UDP ports information
to HTML file , XML file, or to tab-delimited text file.
CurrPorts also automatically mark with pink color suspicious TCP/UDP
ports owned by unidentified applications (Applications without version
information and icons)



Versions History
================


* Version 1.10:
  o A tooltip is displayed when a string in a column is longer than
    the column length.

* Version 1.09:
  o /close command-line parameter - Close a connection from
    command-line

* Version 1.08:
  o Fixed columns order bug.

* Version 1.07:
  o New option: Resolve the remote IP addresses.

* Version 1.06:
  o New column: Process Attributes - Display the file attributes of
    the process (H for hidden, R for read-only, and so on)
  o Added support for working with IPNetInfo utility

* Version 1.05:
  o Fixed bug: identify process path starting with '\??\'

* Version 1.04:
  o Added more accelerator keys.
  o Added support for Windows XP visual styles.

* Version 1.03:
  o New Option: Display Listening
  o New Option: Display Established
  o New Option: Display Items With Unknown State
  o New Option: Display Items Without Remote Address

* Version 1.02:
  o Fixed bug: "Exception C0000005" message when running CurrPorts on
    Windows NT/2000 without administrator rights.
  o New column: "Process Services" - Displays the list of services of
    a process.

* Version 1.01:
  o The 'Copy Selected Items' option now copies the ports data in
    tab-delimited format, so you can instantly paste the data into your
    Excel worksheet.
  o Improvment in ports to process binding under Windows 2000.
    Process information is now also displayed under Windows NT.

* Version 1.00: First release.



System Requirements
===================

This utility works perfectly under Windows NT, Windows 2000, and Windows
XP. If you want to use this utility on Windows NT, you should install
psapi.dll in your system32 directory.
You can also use this utility on previous versions of Windows (Windows
98/ME), but in these versions of Windows, the process information for
each port won't be displayed.



Using CurrPorts
===============

CurrPorts utility is a standalone executable, and it doesn't require any
installation process or additional DLLs. In order to start using it, just
copy the executable file (cports.exe) to any folder you like, and run it.

The main window of CurrPorts displays the list of all currently opened
TCP and UDP ports. You can select one or more items, and then close the
selected connections, copy the ports information to the clipboard, or
save it to HTML/XML/Text file. If you don't want to view all available
columns, or you want to change the order of the columns on the screen and
in the files you save, select 'Choose Column' from the View menu, and
select the desired columns and their order. In order to sort the list by
specific column, click on the header of the desired column.



The Options Menu
================

The following options are available under the Options menu:
* Display Listening: If this option is enabled, all listening ports are
  displayed.
* Display Established: If this option is enabled, all established
  connections are displayed.
* Display Closed: If this option is enabled, closed ports (with 'Time
  Wait', 'Close Wait', or 'Closed' state) are displayed.
* Display Items With Unknown State: If this option is enabled, items
  with unknown state (the state column is empty) are displayed.
* Display Items Without Remote Address: If this option is enabled,
  disconnected ports with no remote address are displayed.
* Display TCP Ports: If this option is disabled, TCP ports won't be
  displayed in the ports list.
* Display UDP Ports: If this option is disabled, UDP ports won't be
  displayed in the ports list.
* Mark Ports Of Unidentified Applications: If this option is enabled,
  all TCP/UDP ports that opened by applications with no version
  information and with no icons, are automatically marked with pink
  color. If you have on your system one or more ports marked with pink
  color, you should deeply check the processes that created these ports.
  It could be only an innocent application that simply doesn't contain
  any icons and version information (For example: the executables of
  MySQL and Oracle servers don't contain any icons or version info, so if
  you have MySQL/Oracle servers on your system, the ports they open will
  be marked.) , but it can also be a trojan or other unwanted application
  that infiltrated into your system.
* Mark New/Modified Ports: If this option is enabled, each time the
  ports list is refreshed, all newly added ports and existing ports with
  changes are marked with green color.
* Auto Refresh: Allows you to automatically refresh the opened ports
  list each 2, 4, 6, 8, or 10 seconds.
* Sort On Auto Refresh If this option is enabled, the entire ports list
  is sorted each time that the list is refreshed automatically.
  Otherwise, new/modified ports are added to the bottom of the list.



Integration with IPNetInfo utility
==================================

If you want to get more information about the remote IP address displayed
in CurrPorts utility, you can utilize the Integration with IPNetInfo
utility in order to easily view the IP address information from WHOIS
servers:
1. Download and run the latest version of IPNetInfo utility. (If you
   have IPNetInfo with version prior to v1.06, you must download the
   newer version.)
2. Select the desired connections, and then choose "IPNetInfo" from
   the File menu (or simply click Ctrl+I).
3. IPNetInfo will retrieve the information about remove IP addresses
   of the selected connections.



Command-Line Options
====================



/stext <Filename>
Save the list of all opened TCP/UDP ports into a regular text file.

/stab <Filename>
Save the list of all opened TCP/UDP ports into a tab-delimited text file.

/stabular <Filename>
Save the list of all opened TCP/UDP ports into a tabular text file.

/shtml <Filename>
Save the list of all opened TCP/UDP ports into HTML file (Horizontal).

/sverhtml <Filename>
Save the list of all opened TCP/UDP ports into HTML file (Vertical).

/sxml <Filename>
Save the list of all opened TCP/UDP ports to XML file.



Closing a Connection From Command-Line
======================================

Starting from version 1.09, you can close one or more connections from
command-line, by using /close parameter.
The syntax of /close command:
/close <Local Address> <Local Port> <Remote Address> <Remote Port>

For each parameter, you can specify "*" in order to include all ports or
addresses.
Examples:
* Close all connections with remote port 80 and remote address
  192.168.1.10:
  /close * * 192.168.1.10 80
* Close all connections with remote port 80 (for all remote addresses):
  /close * * * 80
* Close all connections to remote address 192.168.20.30:
  /close * * 192.168.20.30 *
* Close all connections with local port 80:
  /close * 80 * *



Translating CurrPorts To Another Language
=========================================

CurrPorts allows you to easily translate all menus, dialog-boxes, and
other strings to other languages.
In order to do that, follow the instructions below:
1. Run CurrPorts with /savelangfile parameter:
   cports.exe /savelangfile
   A file named cports_lng.ini will be created in the folder of CurrPorts
   utility.
2. Open the created language file in Notepad or in any other text
   editor.
3. Translate all menus, dialog-boxes, and string entries to the
   desired language.
4. After you finish the translation, Run CurrPorts, and all translated
   strings will be loaded from the language file.
   If you want to run CurrPorts without the translation, simply rename
   the language file, or move it to another folder.



License
=======

This utility is released as freeware. You are allowed to freely
distribute this utility via floppy disk, CD-ROM, Internet, or in any
other way, as long as you don't charge anything for this. If you
distribute this utility, you must include all files in the distribution
package, without any modification !



Disclaimer
==========

The software is provided "AS IS" without any warranty, either expressed
or implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not
be liable for any special, incidental, consequential or indirect damages
due to loss of data or any other reason.



Feedback
========

If you have any problem, suggestion, comment, or you found a bug in my
utility, you can send a message to nirsofer@yahoo.com
