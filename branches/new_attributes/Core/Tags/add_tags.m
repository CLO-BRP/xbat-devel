function obj = add_tags(varargin)

% add_tags - add tags to taggable objects
% ---------------------------------------
%
% obj = add_tags(obj, tags)
%
% Input:
% ------
%  obj - taggable objects
%  tags - tags to add
%
% Output:
% -------
%  obj - updated tagged objects

obj = update_tags('add', varargin{:});
