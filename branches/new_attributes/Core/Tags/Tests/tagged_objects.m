function obj = tagged_objects(n, tags)

% tagged_objects - create a set of randomly tagged objects
% --------------------------------------------------------
%
% [obj, tags] = tagged_objects(n, tags)
%
% Input:
% ------
%  n - number of objects
%  tags - tags used
%
% Output:
% -------
%  obj - tagged objects
%  tags - tags used

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% set default tags
%--

if (nargin < 2)
	tags = {'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'cat', 'dog', 'duck'};
end

%--
% check tags
%--

if ~is_tags(tags)
	error('Input tags are not valid.');
end

%--
% set default number of objects
%--

if (nargin < 1)
	n = 10;
end

%-----------------------
% TAGGED OBJECTS
%-----------------------

% NOTE: we create a 'base' taggable object, thin tags, and set tags

base.tags = {};

for k = 1:n
	obj(k) = set_tags(base, thin_tags(tags));
end
