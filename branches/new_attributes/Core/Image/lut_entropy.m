function Y = lut_entropy(X,Z)

% lut_entropy - fast entropy computation
% --------------------------------------
%
% Y = lut_entropy(X,Z)
%
% Input:
% ------
%  X - input image
%  Z - mask image (def: [])
%
% Output:
% -------
%  Y - entropy valued image

%--
% set mask
%--

if (nargin < 2)
	Z = [];
end

%--
% get limits
%--

c = fast_min_max(X);

if ((c(1) <= 0) || c(2) >= 1)
    c = [eps,(1 - eps)];
	X = lut_range(X,c);
end

%--
% create inline entropy function
%--

f = inline('(x - 1).*log2(1 - x) - x.*log2(x)');

%--
% apply lut
%--

Y = lut_apply(X,lut_fun(f,c,1024),c,Z);




