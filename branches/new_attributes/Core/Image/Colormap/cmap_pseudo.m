function C = cmap_pseudo(n,f,p)

% cmap_pseudo - pseudo color colormap
% -----------------------------------
% 
% C = cmap_pseudo(n,f,p)
% 
% Input:
% ------
%  n - number of levels (def: 256)
%  f - frequencies for colors (def: 2*pi*[0.4, 0.5, 0.6])
%  p - phase shifts for colors (def: [0, 0, 0])
%  
% Output:
% -------
%  C - colormap

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set phase shifts
%--

if (nargin < 3)
	p = 2*pi*[0, 0, 0];
end

%--
% set frequencies
%--

if (nargin < 2)
	f = 2*pi*[0.4, 0.47, 0.55];
end

%--
% set levels
%--

if (nargin < 1)
	n = 256;
end	

%--
% set colormap of current figure
%--

if (~nargout)
	colormap(cmap_pseudo(n,f,p));
end

%--
% create colormap
%--

x = linspace(0,1,n)';

C = [sin(f(1)*x + p(1)).^2, sin(f(2)*x + p(2)).^2, sin(f(3)*x + p(3)).^2];

C = C/max(C(:));
