function pixel_menu(h)

% pixel_menu - context menu for displaying pixel information
% ----------------------------------------------------------
%
% pixel_menu(h)
%
% Input:
% ------
%  h - handle to parent figure (def: gcf)

%--
% set figure
%--

if (~nargin)
	h = gcf;
end

%--
% get image handle
%--

hi = get_image_handles(h);

if (length(hi) == 1)
	hc = uicontextmenu;
	set(hi,'uicontextmenu',hc);
else 
	error('More than one displayed image in figure. No pixel menu attached.');
end

%--
% create pixel menu
%--

L = { ...
	'Location: ', ...
	'R: ', ...
	'G: ', ...
	'B: ' ...
}

n = length(L);

S = bin2str(zeros(1,n));
S{2} = 'on';

A = cell(1,n);

hg = menu_group(hc,'',L,S,A); 

