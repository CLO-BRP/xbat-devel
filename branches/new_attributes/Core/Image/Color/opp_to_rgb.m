function Y = opp_to_rgb(X)

% opp_to_rgb - opponent colors to rgb conversion
% ----------------------------------------------
%
% Y = opp_to_rgb(X)
%
% Input:
% ------
%  X - opponent colors image
%
% Output:
% -------
%  Y - rgb image

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% make double image
%--

X = double(X);

%--
% opponent colors to rgb transformation
%--

V = [ ...
	1, -1, 0; ...
	2/3, 2/3, -4/3; ...
	1, 1, 1 ...
];

%--
% apply transformation
%--

Y = rgb_reshape(rgb_vec(X)*V,m,n);

%--
% enforce positivity
%--

Y(find(Y < 0)) = 0;
