function Y = rgb_to_rgbnl(X)

% rgb_to_rgbnl - rgb to nonlinear rgb image
% -----------------------------------------
%
% Y = rgb_to_rgbnl(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - nonlinear rgb image

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	error('Input image is not a multiple channel image.');
end

%--
% compute conversion
%--

Y = X;

Y(:,:,1) = (Y(:,:,1) * (1/255)).^(0.4) * 255;
Y(:,:,2) = (Y(:,:,2) * (1/255)).^(0.4) * 255;
Y(:,:,3) = (Y(:,:,3) * (1/255)).^(0.4) * 255;
