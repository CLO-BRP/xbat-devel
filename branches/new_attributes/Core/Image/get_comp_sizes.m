function s = get_comp_sizes(X)

%--
% get label histogram
%--

M = max(X(:));

h = hist_1d(X, M + 1);

% NOTE: we don't remove the background component 

s = h(h > 0);
