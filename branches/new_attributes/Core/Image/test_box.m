function R = test_box(n,p,it)

% test_box - test box filtering code
% ----------------------------------
%
% R = test_box(n,p,it)
%
% Input:
% ------
%  n - size of image
%  p - box support parameters
%  it - number of iterations
%
% Output:
% -------
%  R - timing and accuracy results

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 295 $
% $Date: 2004-12-16 13:55:03 -0500 (Thu, 16 Dec 2004) $
%--------------------------------

%------------------------
% HANDLE INPUT
%------------------------

if ((nargin < 3) || isempty(it))
	it = 10;
end

if ((nargin < 2) || isempty(p))
	p = [20,40];
end

if ((nargin < 1) || isempty(n))
	n = [300,900];
end

%-------------------------
% RUN TESTS
%-------------------------

%--
% create box filter
%--

F = ones(2*p(1) + 1,2*p(2) + 1);

%--
% compute and compare filtering performance and accuracy
%--

for k = 1:it
	
	%--
	% create random image
	%--
	
	X = 100000 * rand(n(1),n(2));
	
	%--
	% filter with direct and sparse code
	%--
	
	tic; Y1 = linear_filter(X,F); t1(k) = toc;
	
	tic; Y2 = box_filter(X,F); t2(k) = toc;
			
	%--
	% compare output
	%--
	
	E = fast_min_max(Y1 - Y2);
	
	%--
	% pack results and display
	%--
	
	% NOTE: direct computation time, sparse time, speedup ratio, error bounds

	R(k,:) = [t1(k), t2(k), t1(k)/t2(k), E]
	
end
	