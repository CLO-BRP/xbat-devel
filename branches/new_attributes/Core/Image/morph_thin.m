function Y = morph_thin(X, SE1, SE2, Z)

% morph_thin - morphological thinning (binary)
% --------------------------------------------
% 
% Y = morph_thin(X, SE1, SE2, Z)
%
% Input:
% ------
%  X - input image
%  SE1 - element to fit 
%  SE2 - element to miss
%  Z - mask image (def: [])
%
% Output:
% -------
%  Y - thinned image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-01-24 18:41:38 -0500 (Mon, 24 Jan 2005) $
% $Revision: 458 $
%--------------------------------

%-------------------------
% HANDLE INPUT
%-------------------------

%--
% check for single plane binary image
%--

if (ndims(X) > 2) || ~is_binary(X)
	error('Input image must be single plane binary image.');
end

%--
% set mask
%--

if (nargin < 4)
	Z = [];
end

%-------------------------
% COMPUTE
%-------------------------

%--
% thin
%--

ix = find(morph_fit_miss(X, SE1, SE2, Z));

Y = X; Y(ix) = 0;

		




