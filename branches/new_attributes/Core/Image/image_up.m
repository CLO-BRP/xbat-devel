function Y = image_up(X,u)

% image_up - upsample an image
% ----------------------------
% 
% Y = image_up(X,u)
%
% Input:
% ------
%  X - input image
%  u - rates for rows and columns (def: [2,2])
%
% Output:
% -------
%  Y - upsampled image

%--
% set rates
%--

if (nargin < 2)
	u = [2,2];
end

%--
% color image
%--

if (ndims(X) > 2)
	
	[r,c,s] = size(X);
	
	for k = 1:s
		Y(:,:,k) = image_up_(X(:,:,k),u);
	end
	
%--
% scalar image
%--

else
		
	Y = image_up_(X,u);
	
	% Y = im_col_up(im_col_up(X,u(2))',u(1))';

end
	
%--
% local function
%--
		
function Y = im_col_up(X,u)

% im_col_up - image column upsample
% ---------------------------------
%
% Y = im_col_up(X,u)
%
% Input:
% ------
%  X - input image
%  u - upsampling rate
%
% Output:
% -------
%  Y - upsampled image
%

%--
% get size of image
%--

[m,n] = size(X);

%--
% upsample along columns
%--

Y = reshape([X; zeros((u-1)*m,n)],m,u*n);

%--
% remove last zero column
%--

Y = Y(:,1:u*n-1);










