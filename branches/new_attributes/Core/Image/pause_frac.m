function pause_frac(t)

% pause_frac - more accurate pause function
% -----------------------------------------
%
% pause_frac(t)
%
% Input:
% ------
%  t - time in seconds

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-07-06 13:36:07-04 $
%--------------------------------

pause(t);

return;

% NOTE: this code was last tested on version 6

%--
% set global variable
%--

global PAUSE_FRAC_LOOP;

if (isempty(PAUSE_FRAC_LOOP))

	tic;
	for k = 1:10^4
		exp(pi);
	end
	t = toc;
	
	PAUSE_FRAC_LOOP = (10^4)/t;
	
end

%--
% separate whole and fractional seconds
%--

if (t >= 0)
	n = floor(t);
	f = t - n;
else
	n = 0;
	f = -1;
end

%--
% pause for full seconds and fraction
%--

if (n)
	pause(n);
end

if (f >= 0)
	for k = 1:floor(f * PAUSE_FRAC_LOOP)
		exp(pi);
	end
else
	pause;
end
