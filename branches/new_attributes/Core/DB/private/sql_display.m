function sql_display(lines)

disp(' ') 

for k = 1:length(lines)
	disp(['  ', lines{k}]);
end

disp(' '); 