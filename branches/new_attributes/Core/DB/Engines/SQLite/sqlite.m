function [status, result] = sqlite(file, stmt, bind)

% sqlite - sqlite access
% ----------------------
%
% [status, result] = sqlite(file, stmt, bind)
%
% Input:
% ------
%  file - database file
%  stmt - statement
%  bind - values to bind
%
% Output:
% -------
%  status - status
%  result - result

%--
% get statement string
%--

if iscellstr(stmt)
	stmt = sql_string(stmt);
end

%--
% execute statement
%--

if nargin < 3
	status = sqlite_mex(file, stmt);
else
	status = sqlite_mex(file, stmt, bind);
end

result = [];