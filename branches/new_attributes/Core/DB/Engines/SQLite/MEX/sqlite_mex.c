#include "sqlite3.h"

#include "mex.h"

#include "matrix.h"

/*
 * SQL EXECUTE CALLBACK
 ----------------------------------------*/

static int callback (void *NotUsed, int argc, char **argv, char **azColName) {
	
	int i;
  
	for (i = 0; i < argc; i++) {
		mexPrintf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
  
	mexPrintf("\n");
  
	return 0;
	
}

/*
 * BIND TOKENS
 ----------------------------------------*/

void bind_tokens (sqlite3_stmt *statement, mxArray *source, int offset, int count) {
	
	mxArray *cell;
	
	int k, result;
	
	char *str; int numel;
	
	int scalar = 1;
	
	/*
	 * loop over parameters 
	 -----------------------------*/
	
	for (k = offset; k < offset + count; k++) {
		
		/*
		 * get cell and elements
		 -----------------------------*/
		
		cell = mxGetCell(source, k);
		
		numel = mxGetM(cell) * mxGetN(cell);
		
		mexPrintf("%d, %d ", k, numel);
		
		/*
		 * bind cell contents
		 -----------------------------*/
		
		switch (mxGetClassID(cell)) {
			
			case (mxCHAR_CLASS): {
				
				mexPrintf("string\n");
				
				/* get cell string */
				
				numel = numel + 1; 
				
				str = calloc(numel, sizeof(char));
				
				mxGetString(cell, str, numel);
				
				/* bind text */
				
				result = sqlite3_bind_text(statement, k + 1, (const char *) str, numel, SQLITE_TRANSIENT);
				
				free(str);
				
				break;
				
			}
			
			case (mxINT8_CLASS):
				
			case (mxUINT8_CLASS): 
				
			case (mxINT16_CLASS):
				
			case (mxUINT16_CLASS): 
				
			case (mxINT32_CLASS):
				
			case (mxUINT32_CLASS): {
				
				mexPrintf("integer\n");
				
				if ((numel == 1) || scalar) {
					result = sqlite3_bind_int(statement, k + 1, (int) mxGetScalar(cell));
				} else {
					result = sqlite3_bind_blob(statement, k + 1, (void *) mxGetPr(cell), numel * mxGetElementSize(cell), SQLITE_TRANSIENT);
				}
				
				break;
				
			}
			
			case (mxINT64_CLASS):
				
			case (mxUINT64_CLASS): {
				
				mexPrintf("long\n");
				
				if ((numel == 1) || scalar) {
					result = sqlite3_bind_int64(statement, k + 1, (long long int) mxGetScalar(cell));
				} else {
					result = sqlite3_bind_blob(statement, k + 1, (void *) mxGetPr(cell), numel * mxGetElementSize(cell), SQLITE_TRANSIENT);
				}
	
				break;
				
			}
	
			case (mxSINGLE_CLASS): 
				
			case (mxDOUBLE_CLASS): {
				
				mexPrintf("real\n");
				
				if ((numel == 1) || scalar) {
					result = sqlite3_bind_double(statement, k + 1, mxGetScalar(cell));
				} else {
					result = sqlite3_bind_blob(statement, k + 1, (void *) mxGetPr(cell), numel * mxGetElementSize(cell), SQLITE_TRANSIENT);
				}
				
				break;
				
			}
				
			default:
					
				mexPrintf("null\n");
				
				/* we don't know how to bind complex types */
				
				result = sqlite3_bind_null(statement, k + 1);
				
				// result = sqlite3_bind_blob(statement, k + 1, cell, mxGetElementSize(cell), SQLITE_TRANSIENT);
					
		}
		
	}
	
}

/*
 * MEX FUNCTION
 ----------------------------------------*/

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  
	char *file, *command; int len;
	
	sqlite3 *db; 
	
	sqlite3_stmt *statement = 0;	
	
	int status;
	
	int k, count = 0, sets = 0;
	
	/*
	 * HANDLE INPUT
	 ----------------------------------------*/
	
	if (nrhs < 2) {
		mexErrMsgTxt("File and command to execute input is required.\n");
	}
	
	/*
	 * get file string
	 */

	len = mxGetM(prhs[0]) * mxGetN(prhs[0]) + 1;

	file = mxCalloc(len, sizeof(char));

	mxGetString(prhs[0], file, len);  
	
	/*
	 * get statement string
	 */
		
	len = mxGetM(prhs[1]) * mxGetN(prhs[1]) + 1;

	command = mxCalloc(len, sizeof(char));

	mxGetString(prhs[1], command, len);
		
	/*
	 * check bind parameters
	 */
	
	if (nrhs > 2) {
		
		if (!mxIsCell(prhs[2])) {
			mexErrMsgTxt("Parameters to bind must be stored in cell array.\n");
		}
		
		count = mxGetM(prhs[2]); sets = mxGetN(prhs[2]);
		
	}
	
	/*
	 * ACCESS DATABASE
	 ----------------------------------------*/
	
	/*
	 * OPEN
	 --------------------*/
	
	status = sqlite3_open(file, &db);
	
	if (status) {
		mexPrintf("Can't open database: %s\n", sqlite3_errmsg(db)); goto done;
	}
	
	/*
	 * PREPARE
	 --------------------*/
	
	status = sqlite3_prepare(db, command, len, &statement, 0);
	
	if (status || statement == NULL) {
		mexPrintf("Problem with SQL statement: %s\n", sqlite3_errmsg(db)); goto done;
	}
	
	/*
	 * BIND AND EXECUTE
	 --------------------*/
		
	if (count && sets) {
		
		if (sqlite3_bind_parameter_count(statement) != count) {
			mexPrintf("Bind parameters and parameter count do not match.\n"); goto done;
		}	
		
		for (k = 0; k < sets; k++) {
			
			mexPrintf("Set %d -------------------\n", k + 1);
			
			/*
			 * reset and bind statement
			 */
			
			// sqlite3_reset(statement);

			bind_tokens(statement, (mxArray *) prhs[2], k * count, count);
			
			/*
			 * step to execute
			 */
			
			status = sqlite3_step(statement); mexPrintf("status %d\n");
			
			if (status == SQLITE_ERROR) {
				mexPrintf("Problem in step: %s\n", sqlite3_errmsg(db)); goto done;
			}
			
			while (status != SQLITE_DONE) {
				
				status = sqlite3_step(statement); mexPrintf("status %d\n");
				
				if (status == SQLITE_ERROR) {
					mexPrintf("Problem in step: %s\n", sqlite3_errmsg(db)); goto done;
				}
				
			}

		}
	
	/*
	 * EXECUTE
	 -----------------------*/
		
	} else {
		
		while ( (status = sqlite3_step(statement)) == SQLITE_ROW ) {
			
		}
		
	}
	
	/*
	 * CLOSE
	 --------------------*/

	/*
	 * finalize statement
	 */
	
	sqlite3_finalize(statement);
	
	/*
	 * close and clean up
	 */
	
	done:
		
	sqlite3_close(db);
	
	if (nlhs) {
		plhs[0] = mxCreateDoubleScalar((double) status);
	}
	
	mxFree(file); 
	
	mxFree(command);
	
}
