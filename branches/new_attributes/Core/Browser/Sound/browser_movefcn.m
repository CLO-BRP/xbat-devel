function browser_movefcn(h,str)

% browser_movefcn - mouse motion function
% ---------------------------------------


%--
% create persistent variables
%--

persistent DISPLAY_AXES;
persistent MOVING_TEXT;

%--
% set command string
%--

if (nargin < 2)
	str = 'initialize';
end

%--
% set figure
%--

if (nargin < 1)
	h = gcf;
end

%--
% main switch
%--

switch (str)

	%--
	% initialize figure properties for mouse motion display
	%--
	
	case ('initialize')
		
		%--
		% set button down and up callbacks
		%--
		
		set(h, ...
			'windowbuttondownfcn','browser_movefcn(gcf,''start'')', ...
			'windowbuttonupfcn','browser_movefcn(gcf,''stop'')' ...
		);
		
	%--
	% start motion
	%--
	
	case ('start')
		
		%--
		% set mouse motion callback
		%--
		
		set(h,'windowbuttonmotionfcn','browser_movefcn(gcf,''move'')');
		
		%--
		% create text
		%--
		
		pos = get(gca,'currentpoint');
		pos  = pos(1,1:2);
		
		MOVING_TEXT = text(pos(1),pos(2),mat2str(pos,4));
		
		set(MOVING_TEXT, ...
			'tag','moving_text', ...
			'erasemode','xor' ...
		);
		
	%--
	% mouse movement
	%--
	
	case ('move')
		
% 		delete(MOVING_TEXT);
% 		
% 		pos = get(gca,'currentpoint');
% 		pos  = pos(1,1:2);
% 		
% 		MOVING_TEXT = text(pos(1),pos(2),mat2str(pos,4));
% 		
% 		set(MOVING_TEXT, ...
% 			'tag','moving_text', ...
% 			'erasemode','normal' ...
% 		);
% 		
% 		drawnow;
		
		%--
		% update position and text
		%--
		
		pos = get(gca,'currentpoint');
		pos  = pos(1,1:2);
		
		set(MOVING_TEXT, ...
			'position',[pos 0], ...
			'string',mat2str(pos,4) ...
		);
	
		drawnow;
		
	%--
	% stop display of text
	%--
	
	case ('stop')

		%--
		% delete text
		%--
		
		delete(MOVING_TEXT);
		findobj(h,'tag','moving_text');
		
		%--
		% reset mouse motion callback
		%--
		
		set(h,'windowbuttonmotionfcn','');
		
end