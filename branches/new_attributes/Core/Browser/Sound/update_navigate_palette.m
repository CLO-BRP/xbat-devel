function update_navigate_palette(par, data)

% update_navigate_palette
% ----------------------
% 
% update_navigate_palette(par)
%
% Input:
% ------
%  par - browser handle

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% get browser state if needed
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end

%--
% check for navigate palette
%--

% NOTE: the palette is typically closed so this code is often skipped

pal = get_palette(par, 'Navigate', data);

if isempty(pal)
	return;
end

%----------------------------
% UPDATE NAVIGATE CONTROLS
%----------------------------

sound = data.browser.sound; 

%---------------
% TIME
%---------------

slider = get_time_slider(par);

set_control(pal, 'Time', 'value', slider.value);

%---------------
% FILE
%---------------

%--
% get current file
%--

% NOTE: we get the sound time using the slider time to get the file

time = map_time(sound, 'real', 'slider', slider.value);

[file, ix] = get_current_file(sound, time);

%--
% set file navigation controls
%--

set_control(pal, 'File', 'value', file);

if ix == 1
	set_control(pal, 'Prev File', 'enable', 'off');
else
	set_control(pal, 'Prev File', 'enable', 'on');
end

if ix == length(sound.file)
	set_control(pal, 'Next File', 'enable', 'off');
else
	set_control(pal, 'Next File', 'enable', 'on');
end
