function par = get_active_browser

% get_active_browser - get handle to current browser
% --------------------------------------------------
%
% par = get_active_browser
%
% Output:
% -------
%  par - active browser handle

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3084 $
% $Date: 2005-12-20 18:54:15 -0500 (Tue, 20 Dec 2005) $
%--------------------------------

%--
% get browser handles
%--

par = get_xbat_figs('type', 'sound'); 

%--
% no browser or a single browser
%--

if (isempty(par) || (numel(par) == 1))
	return;
end

%--
% return single visible browser
%--

visible = get(par, 'visible');

ix = find(strcmp(visible, 'on'));

if (numel(ix) == 1)
	par = par(ix); return;
end

%--
% browser is current figure
%--

current = gcf;

if ismember(current, par)
	par = current; return;
end

%--
% return browser parent of palette
%--

if is_palette(current)
	
	current = get_palette_parent(current);
	
	if ismember(current, par)
		par = current; return;
	end
	
end

% NOTE: return empty if all of the above failed

par = [];

