function D = dist_real(x,y)

% dist_real - distance matrix for reals
% -------------------------------------
%
% D = dist_real(x,y)
%
% Input:
% ------
%  x - real numbers (row index)
%  y - real numbers (column index)
%
% Output:
% ------
%  D - distance matrix

%--
% compute distance matrix
%--

% non-hermitian transpose

x = x.';

m = length(x);
n = length(y);

D = abs(repmat(x,[1,n]) - repmat(y,[m,1]));

