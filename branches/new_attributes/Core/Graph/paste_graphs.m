function N = (varargin)

% network_setup - setup network data structure
% --------------------------------------------
%
% A = network_setup(X,G,A1,G1,...,An,Gn)
%
% Input:
% ------
%  X - positions
%  G - position graph
%  Ak - attribute k 
%  Gk - attribute graph k
%
% Output:
% -------
%  A - network data structure
%

%--
% check nargin
%--

n = nargin;

if (mod(n,2))
	error('The number of input arguments is not even.');
end

%--
% pack input into network
%--

N.P = varargin{1};
N.G = varargin{2};

for k = 1
	N.A{k} = varargin{};
	N.G{k} = varargin{};
end
