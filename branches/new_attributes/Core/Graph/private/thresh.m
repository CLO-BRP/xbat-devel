function Y = double_thresh(X,b)

% thresh - threshold interval of values
% -------------------------------------
% 
% Y = thresh(X,b)
%
% Input:
% ------
%  X - input matrix
%  b - interval string
%
% Output:
% -------
%  Y - threshold matrix
%

%--
% parse interval string
%--

[b,t] = parse_interval(b);

%--
% compute threshold matrix
%--

switch (t)

	case 0
		Y = ((X > b(1)) & (X < b(2)));
		
	case 1
		Y = ((X > b(1)) & (X <= b(2)));
		
	case 2
		Y = ((X >= b(1)) & (X < b(2)));
		
	case 3
		Y = ((X >= b(1)) & (X <= b(2)));
		
end

