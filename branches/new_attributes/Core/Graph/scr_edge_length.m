
% scr_hist_edge_lengths - compute distributions of edge lengths
% -------------------------------------------------------------
%
% Input:
% ------
%  n - size of underlying lattices
%  m - number of graphs to compute
%  b - linking interval for color


%--
% compute over watts model
%--

ll = [];
for k = 1:m
	G = watts_example(n,b);
	[L,l] = edge_lengths(G{1}.X,G{2}.E);
	ll = [ll; l];
end

LW = ll;

fig; hist_1d(ll,n + 1)
	

%--
% compute over random function model
%--

ll = [];
for k = 1:m
	G = figueroa_example(n,b);
	[L,l] = edge_lengths(G{1}.X,G{2}.E);
	ll = [ll; l];
end

LF = ll;

fig; hist_1d(ll,n + 1)
