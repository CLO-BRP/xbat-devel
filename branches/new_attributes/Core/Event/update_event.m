function [events, cancel] = update_event(old_event)

% update_event - update event(s) to new structure form
% ----------------------------------------------------
% events = update_event(old_event)
%
% Input:
% ------
%  old_event - array of old events
%
% Output:
% -------
%  events - new events

cancel = 0;

opt = struct_update; opt.flatten = 0;

events = empty(event_create);

for k = 1:length(old_event)	
	
	event = struct_update(event_create, old_event(k), opt);
	
	if ~isempty(event.annotation) && isfield(event.annotation(1).value, 'code')
		event = set_tags(event, str_to_tags(event.annotation(1).value.code));
	end
	
	%--
	% write updated event to output array
	%--
	
	events(end + 1) = event;
	
	%--
	% update waitbar and check for cancellation
	%--
	
	[ignore, result] = migrate_wait('Events');

	if ~isempty(result)
		cancel = strcmp(result, 'cancel'); return;
	end

end

