function [h,n] = get_window(parameter)

% get_window - compute window from parameters
% -------------------------------------------
%
% [h,n] = get_window_and_overlap(parameter)
%
% Input:
% ------
%  parameter - spectrogram parameters
%
% Output:
% -------
%  h - window vector
%  n - actual window length, no padding

%------------------------------------
% COMPUTE WINDOW
%------------------------------------

%--
% rename fft and window length
%--

N = parameter.fft;

n = round(N * parameter.win_length); 

%--
% get window function and test for parameter
%--

fun = window_to_fun(parameter.win_type);

param = window_to_fun(parameter.win_type,'param');

%--
% compute base window
%--

if (isempty(param))
	h = fun(n);
else
	h = fun(n,parameter.win_param);
end

%--
% zero pad window to fft length if needed
%--

if (n < N)
	h((n + 1):N) = 0;
end



