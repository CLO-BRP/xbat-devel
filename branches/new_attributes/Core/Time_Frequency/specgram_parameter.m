function parameter = specgram_parameter

% specgram_parameter - create spectrogram parameters struct
% ---------------------------------------------------------
%
% parameter = specgram_parameter
%
% Output:
% -------
%  parameter - spectrogram parameters

%--
% fft length and advance
%--

parameter.fft = 512;

parameter.hop = 0.5;

parameter.hop_auto = 1;

%--
% window options
%--

parameter.win_type = 'Hanning';

parameter.win_param = [];

parameter.win_length = 1;

%--
% summary options
%--

parameter.sum_type = 'mean';

parameter.sum_quality = 'low';

parameter.sum_length = 1;

parameter.sum_auto = 1;
