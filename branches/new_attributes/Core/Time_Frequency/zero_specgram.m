function C = zero_specgram(opt,rate,dt,nch)

% zero_specgram - spectrogram of zeros
% ------------------------------------
%
% C = zero_specgram(opt,rate,dt,nch)
%
% Input:
% ------
% opt - spectrogram options
% rate - sample rate
% dt - duration
% ch - the number of channels
% 
% Output:
% -------
% C - spectrogram of zeros

%--
% get size of spectrogram image
%--

[f,t] = specgram_size(opt,rate,dt);

%--
% return array for single channel case
%--

if (nch < 2)
	C = zeros(f,t); return;
end

%--
% otherwise make cell array of channels like fast_specgram does
%--

C = cell(1,nch);

for ch = 1:nch
	C{ch} = zeros(f,t);
end



