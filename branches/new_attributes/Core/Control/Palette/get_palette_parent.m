function par = get_palette_parent(pal)

% get_palette_parent - get palette parent
% ---------------------------------------
%
% par = get_palette_parent(pal)
%
% Input:
% ------
%  pal - palette handle
%
% Output:
% -------
%  par - parent handle

par = get_palette_property(pal, 'parent');