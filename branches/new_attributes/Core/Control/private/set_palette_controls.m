function flag = set_palette_controls(pal,controls)

% set_palette_controls - set palette controls
% -------------------------------------------
%
% flag = set_palette_controls(pal)
%
% Input:
% ------
%  pal - palette handle (def: gcf) 
%
% Output:
% -------
%  flag - success indicator flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3408 $
% $Date: 2006-02-06 15:40:18 -0500 (Mon, 06 Feb 2006) $
%--------------------------------

% NOTE: this is a simple stand-in that will do the job for now

% NOTE: setting palette controls requires a complex update of the state

%------------------------------
% SET CONTROLS
%------------------------------

%--
% update control array state
%--

% NOTE: this is currently a VERY DANGEROUS FUNCTION !!!

data = get(pal,'userdata'); 

data.control = controls;

set(pal,'userdata',data);

flag = 1;

%--
% update related states
%--

%--
% render palette according to new state
%--

% TODO: extract renderer from code
