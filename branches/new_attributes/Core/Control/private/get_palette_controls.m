function controls = get_palette_controls(pal)

% get_palette_controls - get palette controls array
% -------------------------------------------------
%
% controls = get_palette_controls(pal)
%
% Input:
% ------
%  pal - palette handle (def: gcf) 
%
% Output:
% -------
%  controls - control array

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3408 $
% $Date: 2006-02-06 15:40:18 -0500 (Mon, 06 Feb 2006) $
%--------------------------------

%------------------------------
% GET CONTROLS
%------------------------------

data = get(pal,'userdata'); 

controls = data.control;