function [value,name] = is_dialog(h)

% is_dialog - determine whether figure is dialog
% ----------------------------------------------
%
% [value,name] = is_dialog(h)
%
% Input:
% ------
%  h - figure handle
%
% Output:
% -------
%  value - result of dialog test
%  name - name of dialog figure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 550 $
% $Date: 2005-02-17 23:49:03 -0500 (Thu, 17 Feb 2005) $
%--------------------------------

%--
% test for dialog
%--

% NOTE: this function assumes a convention of using 'DIALOG' in tag

value = ~isempty(findstr(get(h,'tag'),'DIALOG'));

%--
% return name for convenience
%--

if (nargout > 1)
	name = get(h,'name');
end
