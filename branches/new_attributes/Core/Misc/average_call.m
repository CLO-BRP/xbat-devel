function [M, sounds] = average_call(source, opt)

%--
% handle input
%--

if nargin < 2
	opt = fast_specgram;
end

if ~nargin 
	source = uigetdir;
end

%--
% get files from directory
%--

files = dir(source); files([files.isdir]) = []; 

files = {files.name};

%--
% create sounds from files
%--

opt.attributes = 0;

for k = 1:length(files)
	disp(['Creating sound for clip ''', files{k}, '''.']);
	sounds(k) = sound_create('file', [source, filesep, files{k}], opt);
end

%--
% create spectrograms
%--

M = sound_read(sounds(1));

for k = 2:length(files)
	M = M + detrend(sound_read(sounds(k)));
end

M = M ./ length(files);

%--
% display average spectrogram
%--

fig; imagesc(fast_specgram(M, opt)); axis('xy');
