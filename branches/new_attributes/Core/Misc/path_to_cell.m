function out = path_to_cell(str)

% path_to_cell - separate path into string cell array
% ---------------------------------------------------
%
% out = path_to_cell(str)
%
% Input:
% ------
%  str - path string
%
% Output:
% -------
%  out - cell array with path contents

out = str_split(str, pathsep);