function str = str_line(n, marker)

% str_line - create a line using a marker string
% ----------------------------------------------
%
% str = str_line(n, marker)
%
% Input:
% ------
%  n - length of line
%  marker - character to use as marker (def: '-')
%
% Output:
% -------
%  str - marker line string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

% TODO: modify this function to produce comment lines

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

%--
% set default marker
%--

if (nargin < 2) || isempty(marker)
	marker = '-';
end

% NOTE: we only use the first character

marker = marker(1);

%--
% get length from string if needed
%--

if ischar(n)
	n = length(n);
end
	
%--
% handle multiple inputs recursively
%--

if numel(n) > 1

	str = cell(size(n)); 

	for k = 1:numel(n)
		str{k} = str_line(n(k), marker);
	end
	
	return;

end

%-----------------------------------
% CREATE MARKER LINE
%-----------------------------------

%--
% use char double conversion and matrix multiplication to create char line
%--

str = char(double(marker) * ones(1,n));

%--
% display string if no output requested
%--

if ~nargout
	disp(str);
end