function S = simple_fonts

% simple_fonts - list of simple fonts
% -----------------------------------
%
% S = simple_fonts
%
% Output:
% -------
%  S - list of simple fonts

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%--
% create persistent list and output
%--

% NOTE: 'Times' does not seem different from 'Times New Roman'

% NOTE: 'Courier' does not seem different from 'Courier New'

persistent SIMPLE_FONTS;

if (isempty(SIMPLE_FONTS))
	SIMPLE_FONTS = { ...
		'Arial', ...
		'Book Antiqua', ...
		'Century Gothic', ...
		'Comic Sans MS', ...
		'Courier', ...
		'Georgia', ...
		'Helvetica', ...
		'Lucida Console', ...
		'Lucida Sans Unicode', ... 
		'Palatino Linotype', ...
		'Times', ...
		'Trebuchet MS', ...
		'Verdana' ...
	}';
end

S = SIMPLE_FONTS;
