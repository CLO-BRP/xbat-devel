function y = decibel(x)

% decibel - decibel function
% ---------------------
%
% y = decibel(x)
%
% Input:
% ------
%  x - points of evaluation
%
% Output:
% -------
%  y - decibel values

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-04-07 10:15:20 -0400 (Thu, 07 Apr 2005) $
% $Revision: 923 $
%--------------------------------

%--
% vectorized decibel computation
%--

% NOTE: this assumes 'norm' input, rather than 'power' input

y = 20 .* log10(x + eps);
