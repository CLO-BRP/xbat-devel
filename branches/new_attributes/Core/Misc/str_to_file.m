function flag = str_to_file(str,f)

% str_to_file - put string into text file
% ---------------------------------------
%
% flag = str_to_file(str,f)
%
% Input:
% ------
%  str - fprintf string
%  f - file handle or file name
%
% Output:
% -------
%  flag - write success flag

% TODO: this is an awful function and should be rewritten from scratch

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.2 $
% $Date: 2004-03-25 20:39:53-05 $
%--------------------------------

%-------------------------------------
% OPEN FILE
%-------------------------------------

%--
% set success flag
%--

flag = 1;

%--
% file name or file handle
%--

if (isstr(f))
	
	fid = fopen(f,'wt');
	
	if (fid == -1)
		flag = 0;
		disp(' ');
		error(['Error opening file ' f]);
	end
	
	name = 1;
	
else
	
	fid = f; name = 0;
	
end

%-------------------------------------
% HANDLE SPECIAL CHARACTERS
%-------------------------------------

%--
% new line to question mark
%--

str = strrep(str,'\n','$$'); 

%--
% two string new lines prefixed or suffixed escaped
%--

str = strrep(str,'$$$$''','\\n\\n'''); 

str = strrep(str,'''$$$$','''\\n\\n');

%--
% string new line prefixed or suffixed escaped
%--
 
str = strrep(str,'$$''','\\n''');

str = strrep(str,'''$$','''\\n');

str = strrep(str,'$$$$','\n$$');

str = strrep(str,'%','%%');

%-------------------------------------
% WRITE FILE
%-------------------------------------

while (~isempty(str))
	
	[tok,str] = strtok(str,'$$');
	
	fprintf(fid,[tok '\n']);
	
end
		
%-------------------------------------
% CLOSE FILE
%-------------------------------------

if (name)
	fclose(fid);
end


