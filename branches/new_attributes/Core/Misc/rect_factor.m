function R = rect_factor(n)

% rect_factor - compute rectangle factorizations
% ----------------------------------------------
%
% R = rect_factor(n)
%
% Input:
% ------
%  n - positive integer sequence
%
% Output:
% -------
%  R - rectangle factorizations for all elements of sequence

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 990 $
% $Date: 2005-04-28 20:06:31 -0400 (Thu, 28 Apr 2005) $
%--------------------------------

%-----------------------------------------
% HANDLE INPUT
%-----------------------------------------

%--
% consider integer sequences recursively
%--

if (length(n) > 1)
	
	R = [];
	
	for k = 1:length(n)
		
		Rk = rect_factor(n(k));
		
		if (~isempty(Rk))
			R = unique([R; Rk],'rows');
		end
		
	end
	
	return;
	
end

%-----------------------------------------
% COMPUTE RECTANGLE FACTORIZATIONS
%-----------------------------------------

%--
% factor number
%--

f = factor(n);

% NOTE: return when the number of factors is too large

if (length(f) > 7)
	disp(' ');
	error('There is a limit of 7 factors for this computation.');
end

%--
% compute permutations of factors
%--

pf = perms(f);

%--
% compute unique possible rectangles
%--

R = [];

% NOTE: for primes, this loop is never executed, and empty rectangles are output

for k = 1:(length(f) - 1)
	
	Rk = unique( ...
		[prod(pf(:,1:k),2), prod(pf(:,(k + 1):end),2)], 'rows' ...
	);

	R = unique([R; Rk],'rows');
	
end