function name = log_name(log) 

% log_name - get name of log
% --------------------------
%
% name = log_name(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  name - name of log

%----------------------
% HANDLE INPUT
%----------------------

% TODO: add 'is_log' check

%--
% handle multiple logs recursively
%--

if numel(log) > 1
	
	for k = 1:numel(log)
		name{k} = log_name(log(k));
	end

	return;
	
end

%----------------------
% GET NAME
%----------------------

name = file_ext(log.file);