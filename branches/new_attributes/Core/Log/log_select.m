function ix = log_select(log,varargin)

% log_select - select events from a log
% -------------------------------------
% 
%  [ix,id] = log_select(log,field_1,value_1,...,field_k,value_k)
%
% Input:
% ------
%  log - input log
%  field_k - field available for selection
%  value_k - value or interval for field selection
%
% Output:
% -------
%  ix - selected event indices
%  id - selected event id's

%--
% create persistent table of available fields
%--

persistent PERSISTENT_FIELDS;

% get all event fields

if (isempty(PERSISTENT_FIELDS))
	PERSISTENT_FIELDS = fieldnames(event_create);
end 


% get available annotations and measurements
% --
% (this will enable us to select events that have these events and
% annotations)


% get fields for annotations and measurements
% --
% this will enable us to select based on the field values of the
% annotations or measurement

%--
% parse fields and values
%--

