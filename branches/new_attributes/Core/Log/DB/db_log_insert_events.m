function count = db_log_insert_events(db_log, events)

% db_log_add_events - add events to db log
% ----------------------------------------
%
% count = db_log_insert_events(db_log, events)
%
% Input:
% ------
%  db_log - destination log
%  events - to insert
%
% Output: 
% -------
%  count - events updated

% TODO: we need to separate the event from the metadata and put each in its place!

%--
% get simple events from events
%--

% NOTE: this operation should be optimized

not_supported = {'level', 'children', 'parent'};

not_needed = {'annotation', 'detection', 'measurement', 'userdata'};

remove = {not_needed{:}, not_supported{:}};

simple = events;

for k = 1:length(remove)
	
	if isfield(simple, remove{k})
		simple = rmfield(simple, remove{k});
	end
	
end
	
%--
% insert events
%--

% TODO: build statement with variables and use binding, this is currently very slow!

disp(' ');
disp(['INSERT INTO events VALUES ', value_str(length(fieldnames(simple))), ';']);
disp(' ');

sqlite(db_log.events, ...
	['INSERT INTO events VALUES ', value_str(length(fieldnames(simple))), ';'], ...
	struct2cell(simple(:)) ...
);

% for k = 1:length(simple)
% 	
% 	sqlite(db_log.events, ...
% 		['INSERT INTO events VALUES ', value_str(simple(k)), ';'] ...
% 	);
% 
% end

%--
% insert measures
%--

% NOTE: we may have to create the corresponding databases here

% sqlite(db_log.measures{k}, str);

%--
% insert annotations
%--

% NOTE: we may have to create the corresponding databases here

% sqlite(db_log.annotations{k}, str);


%------------------------------
% VALUE_STR
%------------------------------

function str = value_str(obj, fields)

%--
% create parameter value string
%--

if ~isstruct(obj) && (numel(obj) == 1) && (obj == floor(obj))
	
	str = '(';
	
	for k = 1:obj
		str = [str, '?,'];
	end
	
	str(end) = ')';

	return;
	
end

%--
% get object fieldnames
%--

if nargin < 2
	fields = fieldnames(obj);
end

%--
% handle multiple inputs recursively
%--

if numel(obj) > 1
	
	str = cell(size(obj));
	
	for k = 1:numel(obj)
		str{k} = value_str(obj(k), fields);
	end
	
	return;
	
end

%--
% create insert statement for each object
%--

str = '(';

for k = 1:length(fields)

	%--
	% get column value
	%--
	
	value = obj.(fields{k});

	if ismember(fields{k}, {'time', 'freq'})
		value = value(1);
	end
	
	%--
	% get column value string
	%--
	
	if isempty(value)
		
		part = 'NULL';
	
	elseif isnumeric(value)
		
		if (value == floor(value))
			part = int2str(value);
		else
			part = num2str(value);
		end	
		
	% TODO: implement escaping of strings!
	
	elseif ischar(value)
		
		part = ['''', value, ''''];
		
	elseif iscellstr(value)	
		
		if length(value) > 1
			part = strcat(value, ';');
		else
			part = value;
		end 

		part = ['''', part{:}, ''''];
		
	end

	%--
	% concatenate value string
	%--
	
	str = [str, part, ','];

end

str(end) = ')';

