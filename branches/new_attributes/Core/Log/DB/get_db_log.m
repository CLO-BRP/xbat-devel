function db_log = get_db_log(root)

% get_db_log - build log struct from root directory
% -------------------------------------------------
%
% db_log = get_db_log(root)
%
% Input:
% ------
%  root - directory
%
% Output:
% -------
%  db_log - from root

% TODO: this should probably be private, also consider redundancy in store

%--
% create struct with empty fields
%--

db_log = db_log_create;

%--
% check and scan for log contents
%--

db_log.root = root;

events = [root, filesep, 'Events.db'];

db_log.events = events;

%--
% get measure and annotation databases
%--

% NOTE: we may get the name of the available measures and annotations with fileparts

measures = create_dir([root, filesep, 'Measures']);

if ~isempty(measures)

	content = what_ext(measures, 'db'); content = content.db;

	if ~isempty(content)
		db_log.measures = strcat(measures, filesep, content.db);
	end

end

annotations = create_dir([root, filesep, 'Annotations']);

if ~isempty(annotations)

	content = what_ext(annotations, 'db'); content = content.db;

	if ~isempty(content)
		db_log.annotations = strcat(annotations, filesep, content.db);
	end

end