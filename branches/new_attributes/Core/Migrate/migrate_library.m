function [lib, cancel] = migrate_library(source, user)

% migrate_library - migrate library to current version
% ----------------------------------------------------
%
% lib = migrate_library(source, user)
%
% Input:
% ------
%  source - source library directory
%  user - the user that will get the library
%
% Output:
% -------
%  lib - library

cancel = 0;

%--
% get user if needed
%--

% NOTE: the user helps locate the library, also someone should subscribe

if nargin < 2 || isempty(user)
	user = get_active_user;
end

%--
% get source library file
%--

lib = [];

if source(end) == filesep
	source(end) = '';
end

[root, name] = fileparts(source); file = get_library_file(root, name);

%--
% update waitbar
%--

[ignore, result] = migrate_wait('Libraries', [], name);

if ~isempty(result)
	cancel = strcmp(result, 'cancel'); return;
end

% NOTE: the library source is bogus, we can't find the library file

if ~exist(file, 'file')
	return;
end

%--
% load library file and check for 'lib' variable
%--

load(file);

if ~exist('lib', 'var')
	lib = []; return;
end

%--
% update data structure
%--

lib = struct_update(library_create, lib);

%--
% deal with library patrons past, present, and future
%--

user_names = user_name_list;

% NOTE: library author no longer exists, give 'Default' authorship

if ~ismember(lib.author, user_names)
	lib.author = 'Default';
end

for k = length(lib.user):-1:1
	
	% NOTE: library user no longer exists, remove from record
	
	if ~ismember(lib.user{k}, user_names)
		lib.user(k) = [];
	end
	
end

%--
% update path
%--

lib.path = [user_root(lib.author, 'lib'), filesep, lib.name, filesep];

%--
% subscribe user to library
%--

[root, created] = create_dir(lib.path);

if isempty(root)
	error('Unable to create library root directory.');
end

author = get_users('name', lib.author); user = get_users('name', user.name);

% NOTE: this saves library file

user_subscribe(lib, author);

user_subscribe(lib, user);

%--
% migrate sounds to the created library
%--

% NOTE: this is not the best test

if ~created && ~strcmp(dir_name(root), 'Default')
	return;
end

count = numel(library_folder_contents(source));

set_migrate_wait_ticks(count);

migrate_sounds(source, lib);
