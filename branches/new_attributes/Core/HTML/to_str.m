function str = to_str(value)

% to_str - convert simple values to string
% ----------------------------------------
%
% str = to_str(value)
%
% Input:
% ------
%  value - simple value
%
% Output:
% -------
%  str - string representation of simple value

% NOTE: this is a very primitive version, largely a placeholder

%--
% consider class of value for conversion
%--

switch class(value)
	
	case 'char', str = value;
		
	case 'cell', str = to_str(value{1});
		
	case 'struct'
		
		% NOTE: consider expanding struct as field value pairs
		
		str = 'PROBLEM EXPANDING STRUCT!';
		
	otherwise
		
		% NOTE: this should perform a recursive call to handle arrays
		
		if (numel(value) > 1)
			str = mat2str(value);
		else
			
			if (value == floor(value))
				str = int2str(value);
			else
				str = num2str(value);
			end
			
		end

end
