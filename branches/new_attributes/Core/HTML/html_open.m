function html_open(fid, title, style)

% html_open - output header for html file
% ---------------------------------------
%
% html_open(fid, title, style)
%
% Input:
% ------
%  fid - output file identifier
%  title - document title
%  style - style file to include

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

% TODO: add keywords and description

% TODO: consider stylesheets as well as style text

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% handle style input
%--

% NOTE: when the input style file is not available we get empty string

if nargin < 2
	style = '';
else
	style = which(style);
end

%--
% pack template data if available
%--

if ~isempty(title)
	data.title = title;
end

if ~isempty(style)
	data.style = style;
end

%--
% process template
%--

% NOTE: template processing removes markup when it does not find variables

if ~exist('data', 'var')
	html_template(fid, 'html_open.html');
else
	html_template(fid, 'html_open.html', data);
end
