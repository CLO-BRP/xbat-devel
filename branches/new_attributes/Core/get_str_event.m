function [event, name, m, ix] = get_str_event(par, str, data)

% get_str_event - get event corresponding to string
% -------------------------------------------------
%
% [event, name, m, ix] = get_str_event(par, str, data)
%
% Input:
% ------
%  par - browser handle
%  str - event string
%  data - browser state
%
% Output:
% -------
%  event - event
%  name - log name
%  m, ix - log and event index

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5656 $
% $Date: 2006-07-11 13:19:57 -0400 (Tue, 11 Jul 2006) $
%--------------------------------

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% get state of browser if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%--------------------------------------------
% PARSE EVENT STRING
%--------------------------------------------

%--
% get log name from string and get log index
%--

[name, str] = strtok(str, '#'); name = name(1:(end - 1));

m = get_browser_log_index(par, name, data);

% NOTE: return empty if we did not find log

if isempty(m)
	event = []; ix = []; return;
end

%--
% get event id and index
%--

[id, str] = strtok(str, ':'); id = eval(id(3:end));

ix = find(id == struct_field(data.browser.log(m).event, 'id'));

% NOTE: return empty if we did not find event

if isempty(ix)
	event = []; return;
end

%--
% get event
%--

event = data.browser.log(m).event(ix);
