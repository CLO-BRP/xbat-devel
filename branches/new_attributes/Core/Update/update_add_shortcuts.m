function update_add_shortcuts

% update_add_shortcuts - add shortcuts to sound data in library sounds
% --------------------------------------------------------------------
%
% update_add_shortcuts

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

% TODO: consolidated sounds, this seems to require significant sound updates

% NOTE: the main advantage of consolidated sounds is that the library can be moved easily

% NOTE: how about consolidated libraries, should libraries be held anywhere

% TODO: implement library aliases in some kind of user update

%--
% get libraries
%--

libs = get_unique_libraries;

%--
% add data directories to sounds
%--

for k = 1:length(libs)
	add_sound_shortcut(libs(k));
end