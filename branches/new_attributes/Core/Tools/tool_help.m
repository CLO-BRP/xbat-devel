function tool_help(tool)

% TODO: a 'tool' should really be an object

if isempty(tool.help)
	return;
end

disp(' ');

disp(tool.name);
str_line(tool.name);

for k = 1:length(tool.help)
	disp(['<a href="matlab:winopen(''', tool.help{k}, ''');">', tool.help{k}, '</a>']);
end

disp(' ');