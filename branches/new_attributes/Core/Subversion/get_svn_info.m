function info = get_svn_info(opt)

% get_svn_info - get current SVN info
% -----------------------------------
%
% info = get_svn_info
%
% info = get_svn_info('refresh')
% 
% NOTE: 'refresh' forces information to be collected, this is slow

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%-------------------
% HANDLE INPUT
%-------------------

%--
% set default option
%--

if (nargin < 1)
	opt = '';
end

%--
% check for info file
%--
	
if isempty(tsvn_root)
	info = []; return;
end

%-------------------
% GET INFO
%-------------------

%--
% get updated info if needed or requested
%--

update = isempty(which('svn_info.m')) || strcmp(opt, 'refresh');

if update
	tsvn_replace(xbat_root, 'svn_info.in', 'svn_info.m');
end

%--
% get svn info
%--

% NOTE: this can fail in various ways

try
	info = svn_info;
catch
	info = [];
end