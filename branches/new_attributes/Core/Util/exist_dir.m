function value = exist_dir(in)

% exist_dir - check existence of directory
% ----------------------------------------
%
% value = exist_dir(in)
%
% Input:
% ------
%  in - proposed directory
%
% Output:
% -------
%  value - existence indicator

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

% NOTE: try to move to a directory to test its existence

start = pwd;

try
	cd(in); value = 1;
catch
	value = 0;
end

cd(start);