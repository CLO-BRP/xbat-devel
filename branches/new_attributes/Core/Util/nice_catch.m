function nice_catch(info, str)

% nice_catch - typically used to produce warnings from catch blocks
% -----------------------------------------------------------------
%
% nice_catch(info, str)
%
% Input:
% ------
%  info - error info struct as provided by 'lasterror'
%  str - message string

% TODO: add some form of logging

%----------------
% HANDLE INPUT
%----------------

%--
% set default message
%--

% NOTE: the message really wants to be called from a catch!

if nargin < 2
	str = 'WARNING: Exception handled.';
end

%--
% pad message and create line
%--

str = [' ', str];

n = length(str) + 1; line = str_line(n, '_');

%----------------
% DISPLAY
%----------------

%--
% display warning
%--

disp(' '); 

disp(line);
disp(' '); 
disp(str);
disp(line);

disp(' '); 

disp(' MESSAGE:');
disp(' ');
if isempty(info.identifier)
	disp(['   ', info.message]);
else
	disp(['   ', info.message, ' (', info.identifier, ')']);
end

disp(' ');

disp(' STACK:');

% NOTE: try to get stack information if not available

full = 1;

if ~isfield(info, 'stack')
	try
		info.stack = dbstack('-completenames'); info.stack(1:2) = []; full = 0;
	catch
		info.stack = [];
	end
end

if ~isempty(info.stack)
	
	if ~full
		disp(' ');
		disp('   WARNING: Only partial stack information is available.');
	end

	disp(' ');

	for k = 1:length(info.stack)
		disp(['   ', int2str(k), '. ', stack_line(info.stack(k))]);
	end
	
else
	
	disp(' ');
	disp('   WARNING: No stack information is available.');
	
end
	
disp(' ');
disp(line);
disp(' ');


%----------------
% STACK_LINE
%----------------

function str = stack_line(stack) 

file = strrep(stack.file, xbat_root, '');

link = ~isequal(file, stack.file);

file = strrep(file, matlabroot, '(matlabroot)');

str = [file, ' at line ', int2str(stack.line)];

if link
	str = ['In <a href="matlab:opentoline(''', stack.file, ''',', int2str(stack.line), ')">', '(xbat_root)', str, '</a>'];
else
	str = ['In ', str];
end

	
