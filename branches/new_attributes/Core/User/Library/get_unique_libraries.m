function libs = get_unique_libraries(users)

% get_unique_libraries - get unique user libraries
% ------------------------------------------------
%
% libs  = get_unique_libraries(users)
%
% Input:
% ------
%  users - array of users (def: all users)
%
% Output:
% -------
%  libs - unique libraries linked to by users

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1161 $
% $Date: 2005-07-05 16:25:08 -0400 (Tue, 05 Jul 2005) $
%--------------------------------

% NOTE: consider an independent store for libraries hidden '__MASTER' user

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set default users to be all users
%--

if (nargin < 1)
	users = get_users;
end

% NOTE: return empty when there are no users

if (isempty(users))
	libs = []; return;
end

%-----------------------------
% GET UNIQUE LIBRARIES
%-----------------------------

%--
% get libraries linked to by users
%--

libs = [];

for user = users

	%--
	% get user libraries
	%--

	lib = get_libraries(user);
	
	if (isempty(lib))
		continue;
	end
	
	%--
	% append user libraries to list
	%--
	
	if (isempty(libs))
		libs = lib;
	else
		libs = [libs, lib];
	end
	
end

%--
% get unique libraries
%--

% NOTE: we get unique libraries based on unique paths

[ignore,ix] = unique({libs.path}');

libs = libs(ix);

