function path = find_library(path)

% find_library - find library that may have been moved
% ----------------------------------------------------
%
% lib = find_library(path)
%
% Input:
% ------
%  path - last known path for library
%
% Output:
% -------
%  path - likely new path for library

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4286 $
% $Date: 2006-03-15 19:04:03 -0500 (Wed, 15 Mar 2006) $
%--------------------------------

% TODO: the relocation may need some kind of time stamp

%--------------------------------
% HANDLE INPUT
%--------------------------------

% NOTE: consider relocating to default location for active user or author

%--
% return if no new root is provided
%--

if ((nargin < 2) || isempty(root))
	return;
end