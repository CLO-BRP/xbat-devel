function user = get_active_user

% get_active_user - get currently active user
% -------------------------------------------
%
% user = get_active_user
%
% Output:
% -------
%  user - currently active user

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get active user
%--

user = get_env('xbat_user');
	
%--
% consider no active user case
%--

if isempty(user)
	
	%--
	% check for available users
	%--

	users = get_users;

	if isempty(users)

		%--
		% create default user and set to active user when there are no users
		%--
		
		user = default_user;

		if isempty(user)
			error('Failed to create default user.');
		end

	else

		%--
		% set default user or first user
		%--

		user = get_users('name', 'Default');

		if isempty(user)
			user = users(1);
		end

	end

	%--
	% set active user
	%--

	set_env('xbat_user', user(1));
	
end
	
%--
% check user still exists
%--

if ~exist(user_root(user), 'dir')

	%--
	% report and clear missing active user
	%--

% 	disp(['WARNING: User ''' user.name ''' no longer exists.']); 
	
	rm_env('xbat_user', 0);

	%--
	% get active user make sure we have one, this always ends	
	%--

	user = get_active_user;

end
