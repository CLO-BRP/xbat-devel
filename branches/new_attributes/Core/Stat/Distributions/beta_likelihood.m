function logL = betalik1(params,ld,l1d,n)
%BETALIK1 is a helper function. It is the same as BETALIKE, except it
% directly computes the betapdf without calling BETAPDF, also, saved some
% error checking and size checking.

%   ZP You 3-8-99
%   Copyright 1993-2000 The MathWorks, Inc. 
%   $Revision: 1.0 $  $Date: 2003-09-16 01:32:08-04 $

a = max(params(1),0);
b = max(params(2),0);

logL = n*betaln(a,b)+(1-a)*ld+(1-b)*l1d;


