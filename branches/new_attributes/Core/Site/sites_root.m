function root = sites_root

% SITES_ROOT root for all sites
%
% root = sites_root(base)

% TODO: change 'base' to be an environment variable

base = xbat_root;

root = create_dir([base, filesep, 'Sites']);

if isempty(root)
	error('Unable to get sites root.');
end
