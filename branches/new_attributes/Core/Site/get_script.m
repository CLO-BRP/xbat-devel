function script = get_script(site, name)

% GET_SCRIPT get script file from site by name
%
% script = get_script(site, name)

script = get_asset(site, 'scripts', name);
