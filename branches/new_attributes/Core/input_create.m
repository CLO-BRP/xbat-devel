function out = input_create(k)

% input_create - create input description
% ---------------------------------------
%
% out = input_create(k)
%
% Input:
% ------
%  k - number of input elements
%
% Output:
% -------
%  out - input structure

%----------------------------------------
% create input structure
%----------------------------------------

%--
% set units
%--

out.unit = 9;

%--
% set margins
%--

margin.top = 1;
margin.bottom = 1;
margin.left = 1; 
margin.right = 1;
margin.sep = 1;

out.margin = margin;

%--
% set input elements
%--

elem.name = 'Input';
elem.style = 'slider';
elem.size = [12,1];
elem.min = 0;
elem.max = 1;
elem.step = [0.01, 0.1];
elem.list = cell(0);
elem.value = 0.5;

for j = 1:k
	out.input(j) = elem;
	out.input(j).name = ['Input ' int2str(k)];
end