function flag = file_settings(mode,h,in)

% file_settings - handle settings data file
% -----------------------------------------
% 
%     settings = file_settings('read',f)
%
%         flag = file_settings('load',h,f)
%              = file_settings('write',h,settings)
%
% Input:
% ------
%  f - name of file to read
%  h - handle of relevant figure
%  settings - settings to write in file
%
% Output:
% -------
%  settings - settings read from file
%  flag - execution success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get settings file
%--

try
			
	[fn,p] = uigetfile( ...
		{'*.mat','MAT-Files (*.mat)'; '*.*','All Files (*.*)'}, ...
		'Select XBAT Settings File: ' ...
	);  
	
	if (~fn)
		return;
	else
		tmp = load([p,fn]);
		field = fieldnames(tmp);
		if (length(field) > 1)
			disp(' ');
			warning('File does not contain a single settings variable.');
			return;
		end
		eval(['tmp = tmp.' field{1} ';']);
	end
	
catch
	
	disp(lasterr);
	return;
	
end

%--
% get userdata
%--

data = get(h,'userdata');

%--
% update settings and userdata
%--

data.browser.page = tmp.page;
data.browser.specgram = tmp.specgram;
data.browser.colormap = tmp.colormap;
data.browser.grid = tmp.grid;

set(h,'userdata',data);

%--
% update menus
%--



%--
% update display
%--

browser_display(h,'update',data);

% call grid color to update figure color

browser_sound_menu(h,rgb_to_color(tmp.grid.color));