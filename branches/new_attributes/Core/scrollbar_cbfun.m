function scrollbar_cbfun

% scrollbar_cbfun - callback function for scrollbar
% -------------------------------------------------

%--
% set figure handle
%--

h = gcf;

%--
% get scrollbar value and update time
%--

data = get(h,'userdata');
data.browser.time = get(gcbo,'value');
set(h,'userdata',data);

%--
% update display
%--

browser_display(h,'update',data);

%--
% enable and disable navigation menus
%--

if (data.browser.time <= data.browser.page.duration / 1000)
	state = 'off';
else
	state = 'on';
end 

set(get_menu(h,'First Page',2),'enable',state);
set(get_menu(h,'Previous Page',2),'enable',state);

if (~strcmp(data.browser.sound.type,'File'))
	set(get_menu(h,'Previous File',2),'enable',state);
end

if (data.browser.time + data.browser.page.duration == data.browser.sound.duration)
	state = 'off';
else
	state = 'on';
end

set(get_menu(h,'Last Page',2),'enable',state);
set(get_menu(h,'Next Page',2),'enable',state);

if (~strcmp(data.browser.sound.type,'File'))
	set(get_menu(h,'Next File',2),'enable',state);
end

