function [in, created] = create_dir(in)

% create_dir - create a directory and required parents
% ----------------------------------------------------
%
% [out, created] = create_dir(in)
%
% Input:
% ------
%  in - directory path
%
% Output:
% -------
%  out - path if directory exists, empty if not
%  created - creation indicator

%-----------------
% HANDLE INPUT
%-----------------

%--
% check for existence of directory
%--

if exist_dir(in)
	created = 0; return;
end

%-----------------
% CREATE DIR
%-----------------

%--
% separate path into parent and leaf
%--

[par, leaf] = path_parts(in); 

%--
% consider parent recursively
%--

par = create_dir(par);

% NOTE: return on failure to create parent

if isempty(par)
	in = []; created = -1; return;
end

%--
% create directory as leaf of parent
%--

mkdir(par, leaf); created = 1;
