function attributes = get_sound_attributes(sound)

attributes = {};

root = get_attributes_root(sound);

files = strcat(get_attribute_fields, attribute_file_ext);

contents = dir(root);

if isempty(contents)
	return;
end

for k = 1:length(contents)

	if ismember(contents(k).name, files)
		attributes{end + 1} = file_ext(contents(k).name);
	end
	
end
