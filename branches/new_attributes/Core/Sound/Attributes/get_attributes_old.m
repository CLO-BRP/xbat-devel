function out = get_attributes(in,flag)

% get_attributes - get sound attributes from sound files
% ------------------------------------------------------
%
% out = get_attributes(in,flag)
%
% Input:
% ------
%  in - parent directory
%  flag - search flag
%
% Output:
% -------
%  out - attributes structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2201 $
% $Date: 2005-12-06 08:15:05 -0500 (Tue, 06 Dec 2005) $
%--------------------------------

% TODO: pack attributes in structure and clean up code

% NOTE: most of this should be completely rewritten, not yet

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% set empty attributes
%--

out.realtime = [];

out.calibration = [];

out.geometry = [];

out.speed = [];

%--
% return empty if no search
%--

if (~flag)
	return; 
end

%------------------------------------------
% GET ATTRIBUTE FILES
%------------------------------------------

%--
% get text files in directory
%--

files = get_field(what_ext(in,'txt'),'txt');

% NOTE: return if there are no attribute files

if (isempty(files))
	return;
end

%------------------------------------------
% LOAD ATTRIBUTES
%------------------------------------------

%--
% get prefix of attribute files
%--

pre = in;

ix = find(pre == filesep);

nix = length(ix);

if (nix == 1)
	nix = 2;
end

pre = pre((ix(nix - 1) + 1):end - 1);

%--
% check for date and time
%--

% NOTE: this shows that realtime should be called datetime perhaps
 
aix = find(strcmpi(files,[pre '_DateTime.txt']));

if (~isempty(aix))
	out.realtime = file_datetime('read',[in, files{aix}]);
end

%--
% check for calibration
%--

aix = find(strcmpi(files,[pre '_Calibration.txt']));

if (~isempty(aix))
	out.calibration = file_calibration('read',[in, files{aix}]);
end

%--
% check for geometry
%--

aix = find(strcmpi(files,[pre '_Geometry.txt']));

if (~isempty(aix))
	out.geometry = file_geometry('read',[in, files{aix}]);
end

%--
% check for speed of sound
%--

aix = find(strcmpi(files,[pre '_Speed.txt']));

if (~isempty(aix))
	out.speed = file_speed('read',[in, files{aix}]);
end