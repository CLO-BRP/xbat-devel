function sound = sound_attribute_update(sound, lib)

% sound_attribute_update - update sound attributes
% ------------------------------------------------
%
% sound = sound_attribute_update(sound, mode)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  sound - updated sound

%---------------------------
% HANDLE INPUT
%---------------------------

if nargin < 2 || isempty(lib)
	lib = get_active_library;
end

%---------------------------
% GET ATTRIBUTES
%---------------------------

context.sound = sound; context.lib = lib;

attribute_names = get_sound_attributes(sound);

%--
% load attributes from files
%--

attr = struct();

for k = 1:length(attribute_names)
	
	ext = get_extensions('sound_attribute', 'name', attribute_names{k});
	
	if isempty(ext)
		continue;
	end
	
	file = get_attribute_file(sound, ext.name);
	
	if isempty(ext.fun.load)
		continue;
	end
	
	attr.(attribute_names{k}) = ext.fun.load(file, context);
	
end

%--
% copy attributes to sound
%--

sound.attributes = attr;

%--------------------------------------------
% OLD CODE
%--------------------------------------------

% NOTE: This is all for backwards compatability.  There needs to be some
% compromise between totally changing the sound structure and having this
% be dynamic.  for example, some subset of sound attributes could be
% considered "core" attributes, and live in normal sound fields, others
% could go under sound.attributes.(whatever).

% REALTIME

if isfield(attr, 'date_time')
	sound.realtime = attr.date_time;
end

% SENSOR GEOMETRY

if isfield(attr, 'sensor_geometry')
	sound.geometry = attr.sensor_geometry;
end

% SOUND SPEED

if isfield(attr, 'sound_speed')
	sound.speed = attr.sound_speed;
end

% TIME STAMPS

if isfield(attr, 'time_stamps')
	
	if isempty(sound.time_stamp)

		%--
		% fill in entire time stamps field if it doesn't exist.
		%--

		sound.time_stamp = attr.time_stamps;

	else

		%--
		% only update the table. Enable and collapse are config options.
		%--

		if isfield(attr.time_stamps, 'table')
			sound.time_stamp.table = attr.time_stamps.table;
		end

	end
	
end

% SENSOR CALIBRATION

if isfield(attr, 'sensor_calibration')
	sound.calibration = attr.sensor_calibration;
end



