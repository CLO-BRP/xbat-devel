function attribute = get_sound_attribute(sound, attribute_name)

% get_sound_attribute - get sound attribute by name
% -------------------------------------------------
%
% attribute = get_sound_attribute(sound, attribute_name)
%
% Input:
% ------
%  sound - sound
%  attribute_name - attribute name
%
% Output:
% -------
%  attribute - 'z' attribute

attribute = [];

if isfield(sound, attribute_name)
	attribute = sound.(attribute_name);
end

if ~isfield(sound, 'attributes')
	return;
end

if isfield(sound.attributes, attribute_name)
	attribute = sound.attributes.(attribute_name);
end
