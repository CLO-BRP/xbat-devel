%
% Geometry - microphone geometry for recording
% --------------------------------------------
%
% Sound:
% ------
%  Path - $PATH
%  Files - $FILE
%
% Description:
% ------------
%  This file contains a list of either 3 or 4 numbers
%  for each channel. The channel index along with the
%  planar or volume coordinates for the microphone
%  separated by commas.
%
% Format:
% -------
%  index_1, x_1, y_1 [, z_1]
%  index_2, x_2, y_2 [, z_2]
%  ...
%  index_n, x_n, y_1 [, z_n]
%
% Warning:
% --------
%  This file may be edited, however it is automatically
%  regenerated upon editing of the values, other
%  information included in this file through editing
%  will be lost.
%
