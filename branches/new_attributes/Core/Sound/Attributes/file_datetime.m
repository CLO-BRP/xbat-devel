function flag = file_datetime(mode,h,in)

% file_datetime - handle realtime data file
% -----------------------------------------
% 
%     t = file_datetime('read',f)
%
%  flag = file_datetime('load',h,f)
%       = file_datetime('write',h,t)
%
% Input:
% ------
%  f - name of file to read
%  h - handle of relevant figure
%  t - time to write in file
%
% Output:
% -------
%  t - time read from file
%  flag - execution success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set handle
%--

if ((nargin < 2) | isempty(h))
	h = gcf;
end

%--
% get userdata
%--

if (~strcmp(mode,'read'))
	data = get(h,'userdata');
end

%--
% read or write depending on mode
%--

switch (mode)
	
%--
% read date and time from file
%--

case ('read')
	
	%--
	% rename input
	%--
	
	in = h;
	
	%--
	% try to read values from file
	%--
		
	try
		
		t = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to read data from selected file ''' in '''.'], ...
			' XBAT Error  -  Date and Time Data','modal' ...
		);
		waitfor(tmp);
		
		flag = [];
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	if (any(size(t) ~= [1,6]) | any(t < 0) | any(t(1:3) == 0))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper date and time information.'], ...
			' XBAT Error  -  Date and Time Data','modal' ...
		);
		waitfor(tmp);
		
		flag = [];
		
		return;
		
	end
	
	%--
	% output time
	%--
	
	flag = datenum(t);
	
	return;
	
%--
% load date and time into browser from file
%--

case ('load')
	
	%--
	% get file if needed
	%--
	
	if ((nargin < 3) | isempty(in))

		%--
		% go to figure sound directory
		%--
		
		pi = pwd;
		
		cd(data.browser.sound.path);
		
		%--
		% get file to read using dialog
		%--
		
		[in,p] = uigetfile( ...
			{'*.txt','Text Files (*.txt)'; '*.*','All Files (*.*)'}, ...
			'Select Date and Time  File: ' ...
		);  
		
		%--
		% return if file dialog was cancelled
		%--
		
		if (~in)
			
			flag = 0;
			cd(pi);	
			return;
			
		else
			
			in = [p,in];
			cd(pi);
			
		end

	end
	
	%--
	% try to read values from file
	%--
	
	try
		
		t = load(in);
		
	catch
		
		tmp = error_dialog( ...
			['Unable to load data from selected file ''' in '''.'], ...
			' XBAT Error  -  Date and Time Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% simple check on the type of information contained in file
	%--
	
	if (any(size(t) ~= [1,6]) | any(t < 0) | any(t(1:3) == 0))
		
		tmp = error_dialog( ...
			['Selected file ''' in ''' does not contain proper date and time information.'], ...
			' XBAT Error  -  Date and Time Data','modal' ...
		);
		waitfor(tmp);
		
		flag = 0;
		
		return;
		
	end
	
	%--
	% update date and time in browser
	%--
	
	t = datenum(t);
	
	data.browser.sound.realtime = t;
	
	set(h,'userdata',data);
	
	%--
	% update date and time data file
	%--
	
	flag = file_datetime('write',h,t);
	
	if (~flag)
		
		tmp = error_dialog('Error updating date and time data file.', ...
			' XBAT Error  -  Date and Time Data','modal');
		waitfor(tmp);
		
		return;
		
	end
	
%--
% write date and time data from browser sound to file
%--

case ('write')
		
	%--
	% convert date number to date vector
	%--
	
	if ((nargin < 3) | isempty(in))
		t = datevec(data.browser.sound.realtime);
	else
		t = datevec(in);
	end
	
	%--
	% get template string to write to file and add sound data
	%--
	
	str = file_to_str('file_datetime.txt');
	
	str = strrep(str,'$PATH',strrep(data.browser.sound.path,'\','\\'));
	
	if (~strcmp(data.browser.sound.type,'File'))
		str = strrep(str, ...
			'$FILE',[data.browser.sound.file{1} ' , ... , ' data.browser.sound.file{end}]);
	else
		str = strrep(str,'$FILE',data.browser.sound.file);
	end	
	
	%--
	% add date and time data to string
	%--
	
	for k = 1:5
		str = [str, num2str(t(k)) ', '];
	end
	
	str = [str, num2str(t(6))];
	
	%--
	% write file
	%--

	if (~strcmp(data.browser.sound.type,'File'))
		pre = data.browser.sound.path;
		ix = findstr(pre,filesep);
		pre = pre(ix(end - 1):(end - 1));
	else
		pre = file_ext(data.browser.sound.file);
	end
	
	flag = str_to_file(str,[data.browser.sound.path pre '_DateTime.txt']);
	
	%--
	% update date and time related menus
	%--
	
	set(get_menu(data.browser.view_menu.time_labels,'Date and Time'),'enable','on');
	
	mg = data.browser.sound_menu.data(4);
	
	set(mg,'label','Date and Time:  (Available)');
	
	delete(get(mg,'children'));
	
	[tmp1,tmp2] = strtok(datestr(data.browser.sound.realtime,0),' ');
	tmp2 = tmp2(2:end);
		
	L = { ...
		['Date:  ' tmp1], ...
		['Time:  ' tmp2], ...
		'Edit Date and Time ...', ...
		'Import Date and Time ...' ...
	};
	
	S = bin2str([0 0 1 0]);
	
	tmp = menu_group(mg,'browser_sound_menu',L,S);
	
	set(tmp(1:end - 2),'callback','');
	
	%--
	% update display if time labels are clock
	%--
	
	if (strcmp(data.browser.grid.time.labels,'clock'))
		browser_view_menu(h,'Date and Time');
	end
	
end

%--
% set flag
%--

flag = 1;
	
	
	
	