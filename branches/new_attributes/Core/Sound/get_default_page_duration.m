function duration = get_default_page_duration(sound)

% NOTE: perhaps consider some machine parameters and page mode

%--
% estimate default page duration
%--

duration = (2^18) / get_sound_rate(sound);

%--
% ensure integer duration or full sound duration
%--

duration = round(duration);

if duration < 1
	duration = 1;
end

if duration > sound.duration
	duration = sound.duration;
end