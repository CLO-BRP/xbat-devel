function buffer = get_player_buffer

% get_player_buffer - access samples from current player
% ------------------------------------------------------
%
% buffer = get_player_buffer
%
% Output:
% -------
%  buffer - player buffer struct

% NOTE: consider renaming to 'get_play_buffer' and adding parent input

%--
% get player
%--

player = get_current_player;

if isempty(player)
	buffer = []; return;
end

%--
% get player data
%--

data = get(player, 'userdata');

%--
% pack buffer
%--

buffer.samples = data.buffer;

buffer.length = length(buffer.samples);

buffer.ix = get(player, 'currentsample');