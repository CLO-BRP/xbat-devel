function X = player_resample(X, player)

%--
% return quickly if we don't need to resample
%--

if isempty(player.resample)
	return;
end

%--
% resample if needed
%--

% NOTE: this should be available as a function

fun = player.resample{1}; args = player.resample(2:end);

X = fun(X, args{:});
