function [r, p, q] = get_player_resample_rate(r)

% get_player_resample_rate - get player rate and resample factors
% ---------------------------------------------------------------
%
% [r, p, q] = get_player_resample_rate(r)
%
% Input:
% ------
%  r - input samplerate
%
% Output:
% -------
%  r - player samplerate
%  p,q - resample factors

%--
% create list of typical play supported rates
%--

persistent TYPICAL_PLAY_RATES;

if isempty(TYPICAL_PLAY_RATES)
	TYPICAL_PLAY_RATES = [8000, 11025, 22050, 44100, 48000]; 
end

%--
% check if we are playing a supported rate
%--

if ismember(r, TYPICAL_PLAY_RATES)
	p = []; q = []; return;
end

%--
% select sufficient rates from available rates
%--

rates = TYPICAL_PLAY_RATES(TYPICAL_PLAY_RATES >= r);

if isempty(rates)
	rates = TYPICAL_PLAY_RATES(end);
end

%--
% select samplerate based on efficient resampling scheme 
%--

for k = 1:length(rates)
	[p(k), q(k)] = rat(rates(k) / r);
end

[ignore, ix] = min(p + q); 

r = rates(ix); p = p(ix); q = q(ix);
