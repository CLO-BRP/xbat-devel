function rate = get_sound_rate(sound)

% get_sound_rate - get effective sound sample rate
% ------------------------------------------------
%
% rate = get_sound_rate(sound)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  rate - current sample rate of sound

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3232 $
% $Date: 2006-01-20 18:00:37 -0500 (Fri, 20 Jan 2006) $
%--------------------------------

if isempty(sound.output.rate)
	rate = sound.samplerate;
else
	rate = sound.output.rate;
end