function sound = edit_sound_config(sound,lib)

% edit_sound_config - sound configuration editing
% -----------------------------------------------
%
% sound = edit_sound_config(sound,lib)
%
% Input:
% ------
%  sound - sound to edit
%  lib - parent library (def: active library)
%
% Output:
% -------
%  sound - edited sound

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

%---------------------------------
% HANDLE INPUT
%---------------------------------

%--
% set default active library
%--

if (nargin < 2)
	lib = get_active_library;
end

if (isempty(lib))
	error('Library to save sound in is required.');
end

%---------------------------------
% EDIT SOUND CONFIG
%---------------------------------

% TODO: check that sound is closed, otherwise prompt to close and reopen

%--
% present edit configuration dialog
%--

out = sound_config_dialog(sound);

if (isempty(out.values))
	return;
end

%--
% update properties
%--

sound.output.class = lower(out.values.class{1});

if (out.values.resample && (sound.samplerate ~= out.values.samplerate))
	sound.output.rate = out.values.samplerate;
else
	sound.output.rate = [];
end

sound.time_stamp.enable = out.values.enable_time_stamps;

sound.time_stamp.collapse = out.values.collapse_sessions;

%--
% save sound in library
%--

% NOTE: we may consider discarding parts of the state

out = sound_load(lib,sound_name(sound));

sound_save(lib,sound,out.state);
