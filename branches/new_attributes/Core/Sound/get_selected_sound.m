function [sound, lib] = get_selected_sound(pal)

% get_selected_sound - get sound selected in XBAT palette
% -------------------------------------------------------
%
% [sound, lib] = get_selected_sound(pal)
%
% Input:
% ------
%  pal - palette
%
% Output:
% -------
%  sound - sound
%  lib - library containing sound

if ~nargin || isempty(pal)	
	pal = get_palette(0, 'XBAT');
end

sound = []; lib = [];

if isempty(pal)
	return;
end

%--
% get selected sounds in XBAT palette
%--

name = get_control(pal, 'Sounds', 'value');

if numel(name) == 0
	return;
end

lib = get_active_library;

sound = get_library_sounds(lib, 'name', name{1});
