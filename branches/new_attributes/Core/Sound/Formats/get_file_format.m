function format = get_file_format(f) 

% get_file_format - get format from file
% --------------------------------------
%
% format = get_file_format(f)
%
% Input:
% ------
%  f - filename
%
% Output:
% -------
%  format - format to handle file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 587 $
% $Date: 2005-02-22 23:28:55 -0500 (Tue, 22 Feb 2005) $
%--------------------------------

%--
% try to get format handler using file extension
%--

[ignore,ext] = file_ext(f);

format = get_formats(0,'ext',ext);

%--
% display warning when format was not found
%--

% NOTE: this is the goal of this function, to handle failure, develop other ways

if (isempty(format))
	disp(' ');
	error(['Unable to get format handler for sound file with extension ''' ext '''.']);
end