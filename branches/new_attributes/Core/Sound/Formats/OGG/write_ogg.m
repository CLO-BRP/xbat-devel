function out = write_ogg(f,X,r,opt)

% write_ogg - write samples to sound file
% ----------------------------------------
%
%  opt = write_ogg(f)
%
% flag = write_ogg(f,X,r,opt)
%
% Input:
% ------
%  f - file location
%  X - samples to write to file
%  r - sample rate
%  opt - format specific write options
%
% Output:
% -------
%  opt - format specific write options
%  flag - success flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 587 $
% $Date: 2005-02-22 23:28:55 -0500 (Tue, 22 Feb 2005) $
%--------------------------------

%---------------------------------------
% SET PERSISTENT VARIABLES
%---------------------------------------

%--
% create single persistent temporary file name
%--

persistent OGG_WRITE_TEMP;

if (isempty(OGG_WRITE_TEMP))
	OGG_WRITE_TEMP = [tempdir, 'OGG_WRITE_TEMP.wav'];
end

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set default encoding options
%--

if ((nargin < 4) || isempty(opt))
	
	% NOTE: we get the options as if coming from WAV
	
	opt = encode_ogg('temp.wav');
	
end 

%--
% return default options
%--
	
if (nargin == 1)
	out = opt; return;
end

%---------------------------------------
% DECODE USING CLI HELPER
%---------------------------------------

%--
% create temporary file
%--

% NOTE: the temporary file is created using default encoding options

write_libsndfile(OGG_WRITE_TEMP,X,r);

%--
% encode temporary file to ogg
%--

out = sound_file_encode(OGG_WRITE_TEMP,f,opt);

out = ~isempty(out);