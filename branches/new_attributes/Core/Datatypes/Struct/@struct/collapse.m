function out = collapse(in, sep)

% collapse - collapse flattened struct
% ------------------------------------
%
% out = collapse(in, sep)
%
% Input:
% ------
%  in - flattened scalar struct
%  sep - field separator (def: '__', double underscore)
%
% Output:
% -------
%  out - collapsed struct

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set separator
%--

if nargin < 2
	sep = '__';
end

%--
% check for scalar struct
%--

if length(in) ~= 1
	error('Scalar struct input is required.');
end

%---------------------------------------
% COLLAPSE STRUCT
%---------------------------------------

% NOTE: the order of flattening and collapsing matters

field = fieldnames(in);

for k = 1:length(field)
	eval(['out.', strrep(field{k}, sep, '.'), ' = in.', field{k}, ';']);
end

