function target = get_action_target(type, callback)

% get_action_target - get targets for action from type and callback
% -----------------------------------------------------------------
%
% target = get_action_target(type, callback)
%
% Input:
% ------
%  type - type of action
%  callback - packed callback context
%
% Output:
% -------
%  target - action target array

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%-------------------------------------------------------------
% GET TARGET FOR ACTION TYPE
%-------------------------------------------------------------
	
%--
% get palette from callback context
%--

pal = callback.pal;

%--
% get targets from context
%--

switch type

	%-------------------------------
	% SOUND
	%-------------------------------
	
	case 'sound'

		%--
		% XBAT palette
		%--
		
		if strcmpi(pal.name, 'XBAT')
			
			%--
			% get selected sounds
			%--
			
			[target, lib] = get_selected_sound(pal.handle);
			
		end

	%-------------------------------
	% LOG
	%-------------------------------
	
	case 'log'

		%--
		% XBAT palette
		%--
		
		% NOTE: in this case we get log names not logs
		
		if strcmpi(pal.name, 'XBAT')
			
			target = get_control(pal.handle, 'Logs', 'value');
			
		end

	%-------------------------------
	% EVENT
	%-------------------------------
	
	% NOTE: these actions handle selections as well, they are unlogged events
	
	case 'event'
		
		%--
		% Event palette
		%--
		
		% NOTE: in this case we get event palette strings
		
		if strcmpi(pal.name, 'Event')
			
			target = get_control(pal.handle, 'event_display', 'value');
	
		end

	%-------------------------------
	% ERROR
	%-------------------------------
	
	otherwise, error(['Unrecognized action type ''', type, '''.']);

end
