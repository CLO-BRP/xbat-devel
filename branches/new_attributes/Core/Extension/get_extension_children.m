function ch = get_extension_children(par)

% get_extension_children - get extensions that are children
% ---------------------------------------------------------
%
% ch = get_extension_children(ext)
%
% Input:
% ------
%  ext - parent extension
%
% Output:
% -------
%  ch - children extensions

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% refresh extensions
%--

ext = get_extensions('!');

%--
% remove non-child extensions
%--

name = par.name;

for k = length(ext):-1:1
	if (isempty(ext(k).parent) || ~strcmp(ext(k).parent.name,name))
		ext(k) = [];
	end
end

% NOTE: return simple empty on empty

if (~length(ext))
	ch = [];
else
	ch = ext;
end
