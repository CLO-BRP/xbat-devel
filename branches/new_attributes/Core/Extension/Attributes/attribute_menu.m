function out = attribute_menu(par)

% attribute_menu - attach attribute menu to parent
% ------------------------------------------------
%
% h = attribute_menu(par)
%
% Input:
% ------
%  par - parent menu
%
% Output:
% -------
%  out - menu handles

%--------------------------------------
% CREATE MENU
%--------------------------------------

%--
% prepare parent by clearing children and callback
%--

delete(allchild(par)); set(par, 'callback', []);

%--
% get attributes
%--

ext = get_extensions('sound_attributes');

% NOTE: return early creating informative menu

if isempty(ext)
	
	uimenu(par, ...
		'label', '(No Attributes Found)', ...
		'enable', 'off' ...
	);

	return;

end

%--
% get selected sound
%--

sound = get_selected_sound;

if isempty(sound)
	
	uimenu(par, ...
		'label', '(No Souds Selected)', ...
		'enable', 'off' ...
	);

	return;	
	
end

%--
% create menus for existing sound attributes
%--

attribute_names = get_sound_attributes(sound);

addable = ext;

for k = length(ext):-1:1
	
	if ~ismember(ext(k).name, attribute_names)
		 continue;
	end
		
	addable(k) = [];
	
	uimenu(par, ...
		'label', ext(k).name, ...
		'tag', ext(k).name, ...
		'callback', {@attribute_dispatch, ext(k)} ...
	);

end

if isempty(addable)
	return;
end

add_menu = uimenu(par, ...
	'label', 'add ...', ...
	'tag', 'add ...' ...
);
	
for k = 1:length(addable)
	
	uimenu(add_menu, ...
		'label', addable(k).name, ...
		'tag', addable(k).name, ...
		'callback', {@attribute_dispatch, addable(k)} ...
	);
	
end

