function result = attribute_dispatch(obj, eventdata, ext)

result = [];

%--
% build context
%--

[sounds, lib] = get_selected_sound;

if length(sounds) > 1 || length(sounds) == 0
	return;
end

context.sound = sounds; context.lib = lib;

%--
% get attribute file for sound and type
%--

file = get_attribute_file(context.sound, ext.name);

%--
% load attribute from file
%--

try
	
	attribute = ext.fun.load(file, context);
	
catch
	
	error_str = ['Failed to load "', ext.name, '" for "', sound_name(context.sound), '".'];
	
	extension_warning(ext, error_str, lasterror);
	
	attribute = [];
	
end

context.attribute = attribute;
	
%--
% edit attribute with dialog
%--

result = attribute_dialog(ext, context);

if ~strcmp(result.action, 'ok')
	return;
end

if ~isfield(result.values, ext.name)
	attribute = result.values;
else
	attribute = result.values.(ext.name);
end

%--
% save attribute to file
%--

try
	
	ext.fun.save(attribute, file, context);
	
catch
	
	error_str = ['Failed to save "', ext.name, '" for "', sound_name(context.sound), '".'];
	
	extension_warning(ext, error_str, lasterror);
	
	attribute = [];
	
end


