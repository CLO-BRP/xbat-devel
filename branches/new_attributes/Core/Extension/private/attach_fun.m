function fun = attach_fun(main, type)

% attach_fun - attach defined functions to extension
% --------------------------------------------------
%
% fun = attach_fun(main, type)
%
% Input:
% ------
%  main - handle to main extension function
%  
% NOTE: return empty function structure when main is not available

%----------------------------------------
% HANDLE INPUT
%----------------------------------------

% NOTE: it is not clear what this exit condition means

if isempty(main)
	fun = clear_struct(feval(type)); return;
end

%----------------------------------------
% ATTACH FUNCTIONS
%----------------------------------------

%--
% get names of extension functions
%--

% NOTE: extension function names are computed from a flat fun structure

name = extension_signatures(type);

%--
% look for extension functions in private
%--

% NOTE: on read-only media, the extensions must have all their directories

root = create_dir([path_parts(which(func2str(main))), filesep, 'private']);

if isempty(root)
	error('Problem attaching extension function handles.');
end

curr = pwd; cd(root);

for k = 1:length(name)
			
	% NOTE: check for properly named file
	
	if ~exist([root, filesep, name{k}], 'file')
		handle{k} = []; continue;
	end
	
	% TODO: check function signature
	
	handle{k} = eval(['@', name{k}]);
	
end

cd(curr);

%--
% pack fun
%--

for k = 1:length(name)
	fun.(name{k}) = handle{k};
end

% NOTE: we unflatten this structure to get a proper fun structure

fun = unflatten_struct(fun);
