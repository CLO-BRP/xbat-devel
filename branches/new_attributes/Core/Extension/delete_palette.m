function pal = delete_palette(par, str)

% delete_palette - delete palette from screen and parent
% ------------------------------------------------------
%
% pal = delete_palette(par, str)
%
% Input:
% ------
%  par - parent handle
%  str - palette name
%
% Output:
% -------
%  pal - palette handle (no longer a handle)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6208 $
% $Date: 2006-08-17 23:24:44 -0400 (Thu, 17 Aug 2006) $
%--------------------------------

% NOTE: this function does a lot more than just close the palette figure

%-------------------------------------------------------------
% HANDLE INPUT
%-------------------------------------------------------------

%--
% get parent userdata
%--

data = get(par, 'userdata');

%--
% try to get palette handle
%--

pal = get_palette(par, str, data);

if isempty(pal)
	return;
end

%-------------------------------------------------------------
% PERFORM EXTENSION UPDATES
%-------------------------------------------------------------

%--
% check if this is an extension palette
%--

% NOTE: the extension returned here, if any does not contain current state

[test, ext] = is_extension_palette(pal);

%--
% update browser extension state
%--

if test
	
	[ext, eix] = get_browser_extension(ext.subtype, par, ext.name, data);

	if ~isempty(eix)
		data.browser.(ext.subtype).ext(eix) = ext;
	end

end

%-------------------------------------------------------------
% STORE PALETTE STATE
%-------------------------------------------------------------

%--
% get palette state
%--

% NOTE: the palette state includes: position, toggle, and tab selection states

state = get_palette_state(pal);

%--
% append or update palette state registry
%--

if isempty(data.browser.palette_states)
	
	data.browser.palette_states = state;
	
else
	
	names = struct_field(data.browser.palette_states, 'name');
	
	ix = find(strcmp(state.name, names));
	
	if ~isempty(ix)
		data.browser.palette_states(ix) = state;
	else
		data.browser.palette_states(end + 1) = state;
	end
	
end

%-------------------------------------------------------------
% UNREGISTER WITH PARENT AND DELETE
%-------------------------------------------------------------

% TODO: registering and unregistering need to be encapsulated

%--
% remove palette handle from list and update userdata
%--

ix = find(data.browser.palettes == pal);

data.browser.palettes(ix) = [];

set(par, 'userdata', data);

%--
% close palette figure
%--

closereq;

% NOTE: this was an attempt at decoupling the actual close of the palette

% if ~nargout
% 	closereq;
% else
% 	set(pal, ...
% 		'tag', '', ...
% 		'name', '', ...
% 		'closerequestfcn', 'closereq;' ...
% 	);
% end
