function presets = get_presets(ext, type, varargin)

% get_presets - get extension presets
% -----------------------------------
%
% presets = get_presets(ext, type, 'field', value, ...)
%
% Input:
% ------
%  ext - extension
%  type - preset type
%  field - field name
%  value - field value
%
% Output:
% -------
%  presets - presets

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1180 $
% $Date: 2005-07-15 17:22:21 -0400 (Fri, 15 Jul 2005) $
%--------------------------------

%--------------------------------------
% GET PRESET FILES
%--------------------------------------

%--
% set default type
%--

if (nargin < 2) || isempty(type)
	type = '';
end

%--
% initialize presets
%--

% NOTE: this is not currently used, in the non-trivial case

presets = empty(preset_create(ext));

%--
% get preset files and root 
%--

[files, root] = get_preset_files(ext, type);

if isempty(root) || isempty(files)
	return;
end

%--
% get full name of preset files
%--

files = strcat(root, filesep, files);

files = sort(files);

%--------------------------------------
% LOAD PRESETS
%--------------------------------------

for k = 1:length(files)
	
	% NOTE: this loads the variable named 'preset'
	
	load(files{k});
	
	% TODO: provide some checking and filtering based on extension that created preset
	
	% NOTE: this will not be required when we have stabilized presets
	
	if isempty(presets)
		presets = preset;
	else
		presets(end + 1) = preset;
	end
	
end

%---------------------------------------------------------------
% SELECT PRESETS
%---------------------------------------------------------------

% TODO: add name filtering to this function

% TODO: this kind of selection will be factored, expanded, and improved

%--
% consider selection when we have something to select and criteria
%--

if length(varargin) && ~isempty(presets)
	
	%--
	% get field value pairs from input
	%--
	
	[field, value] = get_field_value(varargin);
	
	%--
	% loop over fields
	%--
	
	for j = 1:length(field)
				
		%--
		% check preset field
		%--
				
		if isfield(presets, field{j})

			for k = length(presets):-1:1
								
				if ~isequal(presets(k).(field{j}), value{j})
					presets(k) = [];
				end
				
			end
			
		%--
		% check preset values field
		%--
		
		elseif isfield(presets(1).values, field{j})

			for k = length(presets):-1:1
				
				if ~isequal(presets(k).values.(field{j}), value{j})
					presets(k) = [];
				end
				
			end
			
		end
		
		
	end
	
end
