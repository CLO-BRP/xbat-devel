function preset = preset_load(ext, name, type, context)

% preset_load - load preset from file
% -----------------------------------
%
% preset = preset_load(ext, name, type, context)
%
% Input:
% ------
%  ext - extension
%  name - preset name
%  type - preset type
%  context - context
%
% Output:
% -------
%  preset - preset or name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1627 $
% $Date: 2005-08-23 10:01:37 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

%-------------------------
% HANDLE INPUT
%-------------------------

if nargin < 4
	context = struct;
end

if nargin < 3
	type = '';
end

%-------------------------
% LOAD PRESET
%-------------------------

% TODO: allow for custom preset load, consider different types: file (various 
% extensions) or directory

%--
% get preset file
%--

file = preset_file(ext, name, type);

if ~exist(file, 'file')
	error('Preset file does not seem to exist.');
end

%--
% load preset content based on type
%--

% NOTE: this loads the preset variable

load(file);

preset.name = name;


% NOTE: the context in this case is used to validate the content of the
% preset
