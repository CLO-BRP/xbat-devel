function preset = preset_create(ext, varargin)

% preset_create - create preset structure
% ---------------------------------------
% 
% preset = preset_create(ext, 'field', value, ...)
%
% Input:
% ------
%  ext - extension to create preset for
%  field - preset field name
%  value - preset field value
%
% Output:
% -------
%  preset - preset structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6208 $
% $Date: 2006-08-17 23:24:44 -0400 (Thu, 17 Aug 2006) $
%--------------------------------		

%--------------------------------
% CREATE PRESET
%--------------------------------

%--
% PRIMITIVE
%--

% NOTE: this is a volatile field, on load we set it and use it carefully

preset.name = '';

% NOTE: this is the content of the preset

preset.ext = ext;

% NOTE: tags and notes support simple annotation

preset.tags = {};

preset.notes = {};

%--
% CONTEXT
%--

preset.sound = []; % sound

preset.active = []; % active extensions

%--
% ADMINISTRATIVE
%--

preset.author = '';

preset.created = now;

preset.modified = [];

preset.userdata = [];

%--------------------------------
% SET PROVIDED FIELDS
%--------------------------------

if length(varargin)
	preset = parse_inputs(preset, varargin{:});
end


