function widget_close(widget, eventdata, par)

% widget_close - close widget
% ---------------------------
%
% widget_close(widget, eventdata)
%
% Input:
% ------
%  widget - widget

% NOTE: set as 'closerequestfcn' of widget figure using function handle 

%--
% save widget extension in parent store
%--

ext = get_widget_extension(widget);

set_browser_extension(par, ext);

%--
% close widget
%--

closereq;