function close_widgets(varargin)

% close_widgets - close parent widgets
% ------------------------------------
%
% close_widgets(par, event)
%
% Input:
% ------
%  par - parent
%  event - event

% NOTE: the argument list here is the same as 'get_widgets'

close(get_widgets(varargin{:}));