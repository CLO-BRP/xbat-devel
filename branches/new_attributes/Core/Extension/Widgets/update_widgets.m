function widgets = update_widgets(par, event, data)

% update_widgets - update parent widgets on event
% -----------------------------------------------
%
% widgets = update_widgets(par, event, data)
%
% Input:
% ------
%  par - widget parent
%  event - parent event
%  data - parent state
%
% Output:
% -------
%  widgets - widgets updated

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% get parent event widgets
%--

widgets = get_widgets(par, event);
		
if isempty(widgets)
	return;
end

%--
% get parent data if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%-----------------------
% UPDATE WIDGETS
%-----------------------

%--
% get widget data from parent data
%--

data = get_widget_data(par, event, data);

%--
% update widgets
%--

% TODO: time and time budget

% NOTE: data depends on parent and event, it is the same for all widgets

for k = length(widgets):-1:1
	
	%--
	% update widget and time
	%--
	
	start = clock;
	
	updated = widget_update(par, widgets(k), event, data);
	
	elapsed = etime(clock, start);
	
	% NOTE: removed widget from from the updated list if update failed
	
	if ~updated
		widgets(k) = [];
	end
	
end
