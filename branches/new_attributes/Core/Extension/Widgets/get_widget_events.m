function events = get_widget_events

% get_widget_events - get events widget may listen to
% ---------------------------------------------------
%
% events = get_widget_events
%
% Output:
% -------
%  events - widget event names

persistent WIDGET_EVENTS;

if isempty(WIDGET_EVENTS)
	fun = widget; WIDGET_EVENTS = fieldnames(flatten(fun.on));
end

events = WIDGET_EVENTS;