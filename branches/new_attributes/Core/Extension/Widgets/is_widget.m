function value = is_widget(ext)

% is_widget - test whether extension is widget
% --------------------------------------------
%
% value = is_widget(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - widget indicator

value = extension_isa(ext, 'widget');