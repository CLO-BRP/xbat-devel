function [Y, info] = apply_signal_filter(X, ext, context)

% apply_signal_filter - apply signal filter
% -----------------------------------------
% 
% [Y, info] = apply_signal_filter(X, ext, context)
%
% Input:
% ------
%  X - signal 
%  ext - filter extension
%  context - context
%
% Output:
% -------
%  Y - filtered signal
%  info - coarse profile information

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1917 $
% $Date: 2005-10-06 17:19:00 -0400 (Thu, 06 Oct 2005) $
%--------------------------------

% NOTE: signals are assumed columns, compute should handle multiple columns

%------------------------------------------
% HANDLE INPUT
%------------------------------------------

%--
% set empty struct default context
%--

if (nargin < 3) || isempty(context)
	context = struct([]);
end

%------------------------------------------
% SETUP
%------------------------------------------
	
%--
% get alpha from opacity if available
%--

% NOTE: no opacity parameter means pure filtered signal output

if ~isfield(ext.control, 'opacity')
	alpha = 1;
else
	alpha = ext.control.opacity;
end

%--
% get input form and possibly time stamp
%--

fx = get_form(X);

if (nargout > 1)
	t0 = clock; 
end

%--
% handle float to double conversion if needed
%--

% NOTE: consider extension flag to indicate native float support

if isfloat(X)
	X = double(X);
end

%------------------------------------------
% APPLY FILTER
%------------------------------------------

%--
% apply filter considering mixing
%--

switch alpha

	case 0, Y = X;

	case 1
				
		try
			Y = ext.fun.compute(X, ext.parameter, context);
		catch
			Y = X; extension_warning(ext, 'Error in compute function.', lasterror);
		end
	
	otherwise
		
		try
			Y = (1 - alpha) * X + alpha * ext.fun.compute(X, ext.parameter, context);
		catch
			Y = X; extension_warning(ext, 'Error in compute function.', lasterror);
		end
	
end

%--
% check output form
%--

try
	Y = set_form(Y, fx);
catch
	Y = X; extension_warning(ext, 'Incompatible output form.');
end

%--
% get coarse profile information if needed
%--

% TODO: put this into a function

if (nargout > 1)
	
	info.ext = [ext.subtype, '::', ext.name];
	
	info.time = etime(clock, t0);

	info.form = fx;

	info.rate = numel(X) / (info.time * 10^6); 

end
