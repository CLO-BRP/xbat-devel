function root = extension_root(ext, type)

% extension_root - root directory of extension
% --------------------------------------------
%
% root = extension_root(ext, type)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  root - extension root

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% TODO: extend to consider when we may have more than one extension root

%--
% get base extension root
%--

root = [extensions_root(ext.subtype), filesep, ext.name];

if (nargin < 2) || isempty(type)
	return;
end

%--
% append type 
%--

if ~ismember(type, get_extension_directories)
	error(['Unrecognized extension root directory type ''', type, '''.']);
end

if strcmp(type, 'base')
	return;
end

root = [root, filesep, type];
