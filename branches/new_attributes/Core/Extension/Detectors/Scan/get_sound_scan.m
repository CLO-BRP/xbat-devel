function scan = get_sound_scan(sound, duration, overlap)

% get_sessions_scan - get default sound scan
% ------------------------------------------
%
% scan = get_sound_scan(sound, duration, overlap)
%
% Input:
% ------
%  sound - sound
%  duration - page duration
%  overlap - page overlap
%
% Output:
% -------
%  scan - default scan for sound  

%-----------------------------------------
% HANDLE INPUT
%-----------------------------------------

%--
% set default scan page configuration
%--

if (nargin < 3) || isempty(overlap)
	overlap = 0;
end

if (nargin < 2) || isempty(duration)
	duration = get_default_page_duration(sound);
end

%-----------------------------------------
% GET SCAN BLOCKS FROM SOUND
%-----------------------------------------

%--
% get recording time session start and stop times
%--

sessions = get_sound_sessions(sound, 1);

%--
% get scan blocks
%--

if isempty(sessions)
	
	% NOTE: when there are no sessions scan spans whole sound
	
	start = 0; stop = sound.duration; interval = 0;

else

	% NOTE: interval value of three indicates start and stop are session boundaries
	
	start = [sessions.start]; stop = [sessions.end]; interval = 3 * ones(size(start));
	
end

%--
% build scan with interval information
%--

scan = scan_create(start, stop, duration, overlap, interval);

