function scan = get_page_scan(page,duration,overlap)

% get_page_scan - create scan from page
% -------------------------------------
%
% scan = get_page_scan(page,duration,overlap)
%
% Input:
% ------
%  page - page
%  duration - scan page duration
%  overlap - scan page overlap
%
% Output:
% -------
%  scan - page scan

%--
% handle input
%--

if ((nargin < 3) || isempty(overlap))
	overlap = 0;
end

if ((nargin < 2) || isempty(duration))
	duration = page.duration;
end

%--
% create scan
%--

start = page.start; stop = page.start + page.duration;

scan = scan_create(start, stop, duration, overlap);