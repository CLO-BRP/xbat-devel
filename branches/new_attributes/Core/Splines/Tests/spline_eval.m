function [t, yp] = spline_eval(y, x, N, A)

%-----------------------
% HANDLE INPUT
%-----------------------

if nargin < 4 || isempty(A)
	A = 1;
end

if (nargin < 3) || isempty(N)
	N = 100; 
end

if (nargin < 2)
	x = [];
end

%-----------------------
% EVALUATE SPLINE
%-----------------------

%--
% make parametric curve grid
%--

t = linspace(0, length(y) - 3, N);

%--
% evaluate y component of spline
%--

% NOTE: we consider y the default, because we think of a spline function

dys = A * (y(2) - y(1)); dye = A * (y(end) - y(end - 1));

y = y(2:end-1);

wy = bspline_weights([dys; y(:); dye]);

yp = eval_bspline(t, wy);

%--
% evaluate x component of spline if needed
%--

% NOTE: this maps the parametric curve grid from the interval onto a curve

if isempty(x) 
    return;
end

dxs = A * (x(2) - x(1)); dxe = A * (x(end) - x(end - 1));

x = x(2:end-1);

wx = bspline_weights([dxs; x(:); dxe]);

t = eval_bspline(t, wx);

