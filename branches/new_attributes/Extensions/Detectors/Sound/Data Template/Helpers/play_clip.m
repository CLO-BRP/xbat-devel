function y = play_clip(pal)

% play - create and play signal for sound using parameters
% --------------------------------------------------------
%
% y = play_clip(pal)
%
% Input:
% ------
%  pal - palette handle

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1000 $
% $Date: 2005-05-03 19:36:26 -0400 (Tue, 03 May 2005) $
%--------------------------------

% TODO: set proper 'xlim' on display axes so that we can draw the play line
% on the display

% TODO: check for availability of 'clip'

%--
% get parameters and sound
%--

[ignore, templates] = control_update([], pal, 'templates');

par = get_field(get(pal, 'userdata'), 'parent');

data = get(par, 'userdata');

%--
% get clip and other relevant data
%--

rate = data.browser.sound.samplerate;

speed = data.browser.play.speed;

clip = templates.clip(templates.ix);

%--
% play clip
%--

ax = findobj(pal, 'tag', 'templates', 'type', 'axes');

%--------------------------------------------

%--
% create temp sound from clip
%--

temp_file = [tempname, '.wav'];

sound_file_write(temp_file, clip.data, clip.samplerate); 

temp = sound_create('file', temp_file);

p = sound_player( ...
	temp, 'time', 0, clip.event.duration, [], data.browser.play.speed, [], [0, ax, 1] ...
);

start(p);

% NOTE: something could go wrong here
delete(temp_file);

%--------------------------------------------

