function parameter = parameter__create(context)

% TOP-HAT - parameter__create

%--
% get parent parameters
%--

fun = parent_fun;

parameter = fun(context);

%--
% add tophat type parameter
%--

% NOTE: the default type is coded in 'tophat_types'

[types,ix] = tophat_types;

parameter.type = {types{ix}};