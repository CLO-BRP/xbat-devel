function [Y, context] = compute(X, parameter, context)

% CENTER - compute

%--
% create structuring element
%--

SE = create_se(parameter);

%--
% compute alternating filters
%--

E = morph_open(morph_close(X, SE), SE);

D = morph_close(morph_open(X, SE), SE);

%--
% compute center
%--

% NOTE: a center is more general than what this function computes

Y = min(E, D);

Y = max(X, Y);

Y = min(Y, max(E, D));

% NOTE: the above is equivalent to

% Y = min(max(X, min(E, D)), max(E, D));