function parameter = parameter__create(context)

% IMPULSE-NOISE - parameter__create

[types, ix] = get_impulse_types;

% NOTE: using the parenthesis, keeps the value in cell, as returned by control

parameter.type = types(ix);

parameter.percent = 0.5;
