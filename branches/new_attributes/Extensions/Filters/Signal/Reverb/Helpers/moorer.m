function Y = moorer(X,param)

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set and possibly output default parameters
%--

if nargin < 2 || isempty(param)
	
	param.lengths = [.7, 5, 7.2, 2.7, 6] / 40;
	
	param.decay = .18;
	
	param.filt = [.5 .3 .2];
	
	param.fs = 44100;
	
	if (~nargin)
		Y = param; return;
	end
	
end

%--
% check shape of signal
%--

% TODO: extend to handle signal provided as one channel per column

if prod(size(X)) ~= length(X)
	error('single channel signals only');
end

X = X(:);

%--------------------------------
% COMPUTE FILTER
%--------------------------------

% TODO: make it fast! this is already pretty close to C anyway

Y = zeros(size(X));

tap_starts = floor(param.lengths * param.fs);

tap_length = length(param.filt);

for ix = 1:length(X)
	
	Y(ix) = X(ix);
	
	if (ix > max(tap_starts))
		
		for jx = 1:length(tap_starts)

			kx = ix - tap_starts(jx);

			Y(ix) = Y(ix) + param.decay * param.filt * Y(kx:(kx + tap_length - 1));

		end

	end
	
end
	
	



