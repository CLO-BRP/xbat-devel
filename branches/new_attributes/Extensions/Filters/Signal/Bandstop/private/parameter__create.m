function parameter = parameter__create(context)

% BANDSTOP - parameter__create

%------------------
% SETUP
%------------------

%--
% inherit basic parameters from parent
%--

fun = parent_fun; parameter = fun(context);

%--
% get nyquist from context
%--

nyq = get_sound_rate(context.sound) / 2;

%------------------
% PARAMETERS
%------------------

%--
% hidden parameters
%--

parameter.min_band = parameter.min_band / 2;

parameter.amplitude = [1, 0, 1];

%--
% band parameters
%--

parameter.min_freq = 0.45 * nyq;

parameter.max_freq = 0.55 * nyq;

%--
% design parameters
%--

parameter.stop_ripple = parameter.pass_ripple;