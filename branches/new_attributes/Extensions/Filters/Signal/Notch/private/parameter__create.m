function parameter = parameter__create(context)

% NOTCH - parameter__create

%--
% get initial parameters from parent
%--

fun = parent_fun; parameter = fun(context);

nyq = get_sound_rate(context.sound) / 2;

parameter.center_freq = 0.5 * nyq;

parameter.order = 48;
