function [X, context] = compute(X, parameter, context)

% CLIP - compute

%--
% clip using oversampling
%--

if parameter.oversample > 1
	
	X = resample(X, parameter.oversample, 1);

	X = sign(X) .* min(abs(X), 0.01 * parameter.percent_level);

	X = resample(X, 1, parameter.oversample);

%--
% clip directly
%--

else
	
	X = sign(X) .* min(abs(X), 0.01 * parameter.percent_level);
	
end