function [handles, context] = on__play__stop(widget, data, parameter, context)

% SPECTRUM - on__play__stop

handles = [];

%--
% delete play lines and time display
%--

ax = spectrum_axes(widget);

delete(play_line(ax));

set(get(ax, 'title'), 'string', '');
