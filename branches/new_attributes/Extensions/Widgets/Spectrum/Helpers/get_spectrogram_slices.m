function [slice, ix] = get_spectrogram_slices(im, time)

% get_spectrogram_time_index - get index for times
% ------------------------------------------------
%
% [slice, ix] = get_spectrogram_slices(im, time)
%
% Input:
% ------
%  im - spectrogram image handle
%  time - time or time interval

%--
% get spectrogram data
%--

X = get(im, 'cdata'); cols = size(X, 2); times = get(im, 'xdata');

%--
% handle time input
%--

% NOTE: if no selection criteria are proposed we get all slices

if (nargin < 2) || isempty(time)
	slice = X; ix = [1, cols]; return;
end

if length(time) > 2
	error('Time must be a single time or interval boundaries.');
end 

time = sort(time);

%--
% compute indices
%--

ix = round(cols * (time - times(1)) / diff(times));

% NOTE: in this case we don't want to infer start and end

if any(ix < 1) || any(ix > cols)
	slice = []; return;
end

%--
% get slices
%--

switch length(ix)
	
	case 1, slice = X(:, ix);
		
	case 2, slice = X(:, ix(1):ix(2));
		
end


