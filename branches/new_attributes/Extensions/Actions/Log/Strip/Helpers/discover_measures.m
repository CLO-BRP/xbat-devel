function measures = discover_measures(target, lib)

% discover_measures - get measure names from target log
% -----------------------------------------------------
%
% measures = discover_measures(target, lib)
%
% Input:
% ------
%  target - target list
%  lib - parent library
%
% Output:
% -------
%  measures - encountered measures

%--
% handle input
%--

% NOTE: we typically rely on the active library

if nargin < 2
	lib = get_active_library;
end

%--
% get log files from target names
%--

for k = 1:length(target)
	target{k} = [lib.path, strrep(target{k}, filesep, [filesep, 'Logs', filesep]), '.mat'];
end

%--
% discover measures in target files
%--

measures = {};

for k = 1:length(target)
	
	%--
	% load target log
	%--
	
	% NOTE: this is the costly step in this computation
	
	log = log_load(target{k});
	
	%--
	% get available measure names from events
	%--
	
	for j = 1:length(log.event)
		measures = union(measures, {log.event(j).measurement.name});
	end
	
end

% NOTE: this removes the empty measure name

for k = length(measures):-1:1
	
	if isempty(measures{k})
		measures(k) = [];
	end

end