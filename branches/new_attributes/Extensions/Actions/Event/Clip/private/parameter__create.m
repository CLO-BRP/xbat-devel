function parameter = parameter__create(context)

% CLIP - parameter__create

parameter.format = 'WAV';

parameter.output = get_clip_directory;

parameter.show_files = 1;
