function result = control__callback(callback, context)

% SENSOR_GEOMETRY - control__callback

result = [];


units = get_control(callback.pal.handle, 'units', 'value'); units = units{1};

switch units

	case 'lat-lon'
		position = {'Latitude', 'Longitude', 'Altitude'};
	case 'xyz'
		position = {'X', 'Y', 'Z'};

end


switch callback.control.name
	
	case 'units'
				
		for k = 1:3
			set_control(callback.pal.handle, ['position_', int2str(k)], 'label', position{k});	
		end
		
	case 'Position_1'
		
	case 'Position_2'
		
	case 'Position_3'
	
		
end
		
handles = get_control(callback.pal.handle, 'geometry_display', 'handles');

geometry = get(handles.obj, 'userdata');

if isempty(geometry)
	geometry = context.attribute;
end

geometry_plot(geometry.local, callback.pal.handle, handles.obj);

switch units
	
	case 'lat-lon'
		
end
		
		
