function save(attribute, store, context)

% SENSOR_GEOMETRY - save

%--
% check for toolbox
%--

if ~isfield(attribute, 'sensor_geometry')
	
	disp('This is not working yet.');
	return;
end

if isempty(which('m_ll2xy'))
	
	warning('m_map not found.  Unable to use lat-lon geometry.');
	
	attribute.global = [];
	
end

%--
% check for global geometry
%--

if ~isempty(attribute.global)
	
	lines{1} = 'lat, lon, elev';

	lines{2} = ['ll, ', attribute.ellipsoid];
	
	type = 'global';
	
else
	
	lines{1} = 'x, y, z';
	
	lines{2} = 'xyz';
	
	type = 'local';
	
end

%--
% write geometry to lines
%--

for k = 1:size(attribute.local, 1)
	
	line = '';
	
	if sum(attribute.local(k,:)) == 0
		line = '*';
	end
		
	for j = 1:3
	
		if numel(attribute.(type)(k, :)) >= j
			value = attribute.(type)(k, j);
		else
			value = 0;
		end
					
		line = [line, num2str(value), ', '];
		
	end
	
	line(end-1:end) = [];
	
	lines{end + 1} = line;
	
end
	
file_writelines(store, lines);


