function attribute = load(store, context)

% SOUND_SPEED - load

attribute = sound_speed(store);
