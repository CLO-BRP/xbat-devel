function save(attribute, store, context)

% SOUND_SPEED - save


lines{1} = 'Speed of Sound';

lines{2} = 'M/s';

lines{3} = num2str(attribute);

file_writelines(store, lines);
