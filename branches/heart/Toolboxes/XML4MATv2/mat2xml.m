function XML = mat2xml(MAT, VARNAME)

% MAT2XML converts structured variable MAT into XML string
%
%Syntax XML = mat2xml(MAT, VARNAME)
%
%Description
%  MAT : structured varable
%  VARNAME : variable name (string)
%  XML : xml version of structured variable (string)
%
% See Also: XML2MAT
%
% Jonas Almeida, almeidaj@musc.edu, 20 Aug 2002, XML4MAT Tbox

%-------------------
% HANDLE INPUT
%-------------------

% NOTE: if not provided make it a default the matlab answer variable

if nargin < 2
	VARNAME = 'ans';
end

%-------------------
% CONVERT TO XML
%-------------------

% NOTE: we get required variable information through 'whos', these are stored as attributes

w = whos('MAT'); w.name = VARNAME; 

XML = ['<', w.name, ' class="', w.class, '" size="', int2str(w.size), '">'];

%--
% string
%--

switch w.class
	
	case 'char'

		% NOTE: convert special characters to their XML entity codes

		XML = [XML, spcharin(MAT(:)')];

	case {'struct', 'function_handle'}
		
		% TODO: there is still something weird with the recovery of function handles
		
		if strcmp(w.class, 'function_handle')
			MAT = functions(MAT);
		end
		
		names = fieldnames(MAT);

		for i = 1:prod(w.size)
			for j = 1:length(names)
				XML = [XML, mat2xml(MAT(i).(names{j}), names{j})];
			end
		end

	case 'cell'

		for i = 1:prod(w.size)
			XML = [XML, mat2xml(MAT{i}, 'cell')];
		end
		
	otherwise

		XML = [XML, num2str(MAT(:)', 16)];

end

XML = [XML, '</', w.name, '>'];

XML = regexprep(XML, '\s{2,}', ' ');
