function [result, xml, recovered] = test_xml2mat(in)

%--
% serialize, recover and compare
%--

xml = mat2xml(in);

recovered = xml2mat(xml);

% TODO: from here on this can be factored

result = isequal(in, recovered);

%--
% display detailed results of struct comparison when it fails
%--

if ~result && isstruct(in)
	
	in1 = flatten(in); recovered1 = flatten(recovered);
	
	fields = fieldnames(in1);

	disp(' ');
	
	for k = 1:numel(fields)
		
		field = fields{k};
		
		if ~isfield(recovered1, field)
			disp([field, ': MISSING']); disp(' '); continue;
		end
		
		if ~isequal(in1.(field), recovered1.(field))
			disp([field, ': NOT-EQUAL']); disp(['  ', to_str(in1.(field)), ' ~= ', to_str(recovered1.(field))]); disp(' '); continue;
		end
		
% 		disp([field, ': Properly recovered.']); continue;
		
	end
	
end