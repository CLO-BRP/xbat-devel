module FilePathing


	#---------------------
	# FILES LINKS
	#---------------------

	def attachment_path(id)
		("%08d" % id).scan(/..../).join('/')
	end		

	# NOTE: this is the root directory for files related to an objects in the system
	# the path contains both the local identifier and global idenrifier. This
	# path is within the 'public' files path

	# NOTE: this is what we use to build a URL that accesses the file from the web

	def files_root_url(obj, file = '')
		url = File.join("files", obj.class.to_s.downcase, attachment_path(obj.id), obj.guid)

		url = File.join(url, file) unless file.empty?

		# NOTE: we need this to indicate that this is a root-based url

		url = '/' + url

		return url
	end

	alias_method :files_root_path, :files_root_url

	# NOTE: this gives us the full web-server system file path to get to the file

	def files_full_path(obj, file='')
		File.join(RAILS_ROOT, "public", files_root_url(obj, file))
	end

	alias_method :files_full_url, :files_full_path 


end