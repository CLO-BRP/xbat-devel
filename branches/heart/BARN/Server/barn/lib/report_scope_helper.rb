module ReportScopeHelper

	def self.included(base)

		base.extend(ClassMethods)
	end

	module ClassMethods

		def get_histogram( hist_params, params = {} )

			limits = self.with_parent_in_set( params ).report_limits( hist_params )

			hist_params[:binsize] = get_binsize( limits )

			scoped = self.with_parent_in_set( params ).find_histogram( hist_params )

			filtered = self.with_parent_in_set( params ).filtered_by( params ).find_histogram( hist_params )

			paginated = self.with_parent_in_set( params ).filtered_by( params ).order_by( params ).find_histogram( hist_params, pagination_params(params) )

			{
				:scoped => scoped,
				:filtered => filtered,
				:paginated => paginated
			}
		end
		

		def get_histogram_for_user( hist_params, user, params = {} )

#			if !hist_params[:bincount]
#				limits = self.with_parent_in_set( params ).readable_by( user ).report_limits( hist_params )
#			end

			if ( hist_params[:min] & hist_params[:max] & hist_params[:bins] )
				hist_params[:binsize] = ( hist_params[:max] - hist_params[:min] ) / hist_params[:bins]
			end

			scoped = self.with_parent_in_set( params ).readable_by( user ).find_histogram( hist_params )

			filtered = self.with_parent_in_set( params ).readable_by( user ).filtered_by( params, user ).find_histogram( hist_params )

			paginated = self.with_parent_in_set( params ).readable_by( user ).filtered_by( params, user ).order_by( params ).find_histogram( hist_params, pagination_params(params) )

			{
				:scoped => scoped,
				:filtered => filtered,
				:paginated => paginated
			}
		end

		#TODO This would be much nicer if will_paginate allowed a pass-through named scope interface.  Too entangled here.  find a way to get rid of limit args

		#NOTE: 'empty' bins will _NOT_ be returned from this function - for performance reasons, this will be the responsibility of the web client
		#NOTE: we could create a switch in the hist_params that asks this function to return the empty bins.
		def find_histogram( hist_params, pagination_args = {} )

			scope = scope(:find)


			scope[:joins]  = scope[:joins] || ""

			scope = convert_joins_dynamic( scope )
			
			
			scope[:select] = " CEIL(#{hist_params[:variable]} / #{hist_params[:binsize]}) as bin, start"

			innersql = self.send(:construct_finder_sql, {})

			if pagination_args != {}
				add_limit! innersql, :offset => pagination_args[:offset], :limit => pagination_args[:per_page]
			end

			sql  = "SELECT bin, COUNT(bin) as 'count', start FROM ( " + innersql + " ) AS FOO GROUP BY bin"

			binvals = self.find_by_sql(sql)
		end


		#TODO: this code is untested
		def report_limits( hist_params )

			scope = scope(:find)

			scope[:select] = " min( #{hist_params[:variable] } ) as minval, max( #{hist_params[:variable] } ) as maxval"

			scope[:group_by] = "id"

			sql = self.send(:construct_finder_sql, {})

			self.find_by_sql(sql)
		end



		def pagination_params( params )

			page = params[:page].to_i || 1

			if page < 1
				page = 1
			end

			per_page = params[:per_page].to_i || self.per_page.to_i

    		offset = (page - 1) * per_page

			{
				:offset => offset, :per_page => per_page
			}
		end

	end

end