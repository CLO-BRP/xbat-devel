class SimpleUUID
	def self.generate
		rand_uuid
	end
	
	def self.rand_hex_3(l)
		"%0#{l}x" % rand(1 << l*4)
	end
	
	def self.rand_uuid
		[8,4,4,4,12].map {|n| rand_hex_3(n)}.join('-')
	end
end