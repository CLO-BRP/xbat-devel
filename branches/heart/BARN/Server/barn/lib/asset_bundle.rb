# AssetBundle
#
# This is used to encapsulate functionality for dealing with assets for remote BARN resources

#--
# There are other options for dealing with assets here without creation of an active record object
# including OpenStructs and creating an Asset class, but for the moment, a Hash extention suffices
# since we eventually need it to respond to #to_xml
#--
require 'open-uri'
require 'hpricot'

#require 'zip/zip'
#require 'zip/zipfilesystem'

class AssetBundle < Hash
	extend ApplicationHelper
	
	attr_accessor :parent_class
	attr_accessor :parent_object
	
	#-------------
	# initialize
	#-------------
		
	def initialize(params = {})
		# initialize the keys
		self[:type] 	 = params[:type] || ""
		self[:guid] 	 = params[:guid] || ""
		self[:files] 	 = params[:files] || []
		self[:archive] = params[:archive] || {}
	end
	
	#-------------
	# to_xml
	#-------------
	
	def to_xml(options = {})
		# super here calls Hash.to_xml
		super({:root => "asset-bundle"}.merge(options))
	end
	
	#-------------
	# from_xml
	#-------------
	
	# Returns an AssetBundle (Hash) or an Array of AssetBundles from an XML document
	# AssetBundle.new.from_xml( xml )
	
	def from_xml(xml, options = {})
		# Make this an Hpricot doc if it is not already
		# TODO: fix this as it is not working! from_xml currenly only accepts a string
		xml = Hpricot(xml) unless xml.class.to_s.include?('Hpricot')
		
		selector = options[:selector] || "asset-bundle"
		bundles  = []
		
		xml.search(selector).each do |element|
			files, archive = [], {}
			
			# type and guid
			type  = element.search("> type").inner_html
			guid  = element.search("> guid").inner_html
			
			# files
			element.search("> files > file").each do |file|
				files << {
					:host => file.search("> host").inner_html,
					:path => file.search("> path").inner_html,
					:name => file.search("> name").inner_html
				}
			end
			
			# archive
			archive[:name] = element.search("> archive > name").inner_html
			archive[:path] = element.search("> archive > path").inner_html
			
			# Create the AssetBundle object
			bundles << AssetBundle.new(
				:type => type,
				:guid => guid, 
				:files => files, 
				:archive => archive
			)
		end		
		
		return (bundles.size > 1) ? bundles : bundles.first
	end

	#-------------
	# from_resource
	#-------------
	
	# Returns an AssetBundle (Hash) or an Array of AssetBundles from a local ActiveRecord instance
	# AssetBundle.new.from_resource( ar )	
	
	def from_resource(resources)
		resources = [resources] if !resources.is_a?( Array )
		bundles 	= []
		
		resources.each do |resource|
			begin
				# Create the AssetBundle object
				bundle  = AssetBundle.new(
					:type => resource.class.to_s, 
					:guid => resource.guid
				)
			
				# Now try and get the files
				directory = AssetBundle.files_system_path(resource)
				files 		= Dir.entries(directory).reject!{|f| ['.','..','.DS_Store'].include?(f) }
				
				if files.size > 0
					# create an archive of the files
					archive_name = 'archive.zip'#"archive-#{Time.now.strftime("%Y-%m-%d-%H%M%S")}.zip"
					
					created = create_archive(
						:source => directory, 
						:target => File.join(directory, archive_name)
					)
					
					# Add the archive hash to the AssetBundle instance
					bundle[:archive] = {
						:path => AssetBundle.files_server_root(resource),
						:name => archive_name
					} if created
					
					# Lets also pack the individual files ...
					file_array = []
					files.each do |file|
						file_array << {
							:host => AssetBundle.local_ip,
							:path => File.join( AssetBundle.files_server_root(resource), file),
							:name => file 
						}
					end
 
					bundle[:files] = file_array
				end
				
				bundles << bundle
			
			rescue Errno::ENOENT => e
				# chances are a file could not be found as advertised
			end
			# begin
		end
		# resources.each
		
		return (bundles.size > 1) ? bundles : bundles.first
	end

	

	#-------------
	# retrieve
	#-------------

	def retrieve(options = {})
		# We must have a host to retrieve from
		return false if options[:host].blank?
		
		set_local_parent_object
		
		# We also need the parent object set to target the extraction, etc ...
		return false if @parent_object.nil?
		
		# Create the local directories where the files should reside
		create_local_directory(@parent_object)

		# Here is the meat. 
		# If there is an archive, go get it and decompress it
		
		unless self[:archive].blank?
			saved = download_and_extract_archive( 
				:source => File.join( 'http://', options[:host], self[:archive][:path], self[:archive][:name] ),
				:target => File.join( AssetBundle.files_system_path(@parent_object), self[:archive][:name] )
			)
				 
			return true if saved
		end
				
		# No archive was found, 
		# so lets try to get the files individually
		
		return false if self[:files].size == 0
		
		self[:files].each{ |file|
			host = file[:host] if options[:host].blank?
			
			AssetBundle.save_file_from_remote(
				:source => File.join("http://", host, file[:path]),
				:target => File.join( AssetBundle.files_system_path(@parent_object), file[:name])
			)
		}
		
		return true
	end


protected

	def download_and_extract_archive(options)
		source = options[:source]
		target = options[:target]

		saved = AssetBundle.save_file_from_remote(
			:source => source,
			:target => target 
		)
		
		# It is possible that the file is already there...
		if saved || File.file?(target)
			AssetBundle.extract_archive(
				:archive => target, 
				:extract_to => AssetBundle.files_system_path(@parent_object))
				
			return true			
		end
		
		return false
	end
	
	#-------------
	# self.save_file_from_remote
	#-------------
	
	# This is now a stubbable method...
	def self.save_file_from_remote(options)
		begin			
			open(options[:target],"w").write( open(options[:source]).read )
			
			return true
		rescue OpenURI::HTTPError => e
			return false
		end
	end
	
	#-------------
	# self.extract_archive
	#-------------
	
	def self.extract_archive(options)
		archive = options[:archive]
		path = options[:extract_to]
		RAILS_DEFAULT_LOGGER.info(archive)
		RAILS_DEFAULT_LOGGER.info(path)
		# TODO: should probably remove destination_file first
		begin
			Zip::ZipFile.open(archive) { |zipfile|
				zipfile.entries.each do |file|
					destination_path = File.join(path, file.name)
		
					# Check to see if the file exists already, and if it does, delete it.
					if File.file?(destination_path)
						File.delete(destination_path)
					end
				
					file.extract(destination_path)
				end
			}
			
			# cleanup
			if File.file?(archive)
				File.delete(archive)
			end
			
		rescue Zip::ZipError => e
			RAILS_DEFAULT_LOGGER.error("ERROR: #{e}\nThere was an error extracting the ZIP file. Chances are you are running it from the same server.")
			# If you are doing this from localhost, it will error!
		end
	end
	
	#-------------
	# create_archive
	#-------------
	
	def create_archive(options = {})
		source = options[:source]
		target = options[:target]
		archive_name = target.split('/').last
		
		raise "Cannot create a ZIP with source => #{source} and target => #{target}" if source.blank? || target.blank?

		# Check to see if the file exists already, and if it does, delete it.
		if File.file?(target)
			File.delete(target)
		end 
		
		# If the source is a directory, get the files, otherwise assume the source *is* a file
		if File.directory?(source)
			files = Dir.entries(source).reject!{|f| ['.','..','.DS_Store', archive_name].include?(f) }
		else
			raise "Dude, seriously, what is this - because its not a file => #{source}." if !File.file?(source)
			a = source.split('/')
			files = [a.pop]
			source = a.join('/')
		end
		
		# open or create the zip file
		Zip::ZipFile.open(target, Zip::ZipFile::CREATE) { |zipfile|
			files.collect { |file|
				zipfile.add( file, File.join(source, file) )
			}
		}

		# set read permissions on the file
		File.chmod(0644, target)

		return true
	end
	
	#-------------
	# utilities
	#-------------
	
	# This method is a class method for better mocking and stubbing
	def self.files_system_path(resource = nil, name = "")
		files_full_path(resource, name)
	end

	# This method is a class method for better mocking and stubbing	
	def self.files_server_root(resource = nil, name = "")
		files_root_path(resource, name)
	end

	# This method is a class method for better mocking and stubbing
	def create_local_directory(resource)
		FileUtils.mkdir_p AssetBundle.files_system_path(resource)
	end

	def set_local_parent_object
		if !self[:type] || !self[:guid]
			raise "Could not create an AssetBundle without a proper type or guid."
		end
		@parent_class 	= self[:type].classify.constantize
		@parent_object 	= @parent_class.find_by_guid(self[:guid])
	end
	
end
