# --------------
# NamedScopeFilter encapsulates the filter query scope and its attendant helper functions
# --------------

module NamedScopeFilter

	def self.included(base)

		base.extend(ClassMethods)

		base.class_eval do

			named_scope :filtered_by, lambda { |*args|
				#--
				# handle input
				#--
				
				params = args.first || {}
				
				if args.length > 1
					user = args[1]
				else
					user = nil
				end
				
				#--
				# setup
				#--
				
				query = params[:query]
				
				confirm = params[:confirm]

				return {} if query.blank?

				rawtokens = raw_to_tokens(query)

				# if the confirm param is not blank, then the confirm param will specify the reserved key for the first tag
				# if the confirm param is not blank, then only the first _tag_ clause will be used, but other filters can be used


				# TODO the special cases for tags and name suggest that there is a better way to handle secondary associations and string compares

				conditions = ""
				firstTagFound = false

				tokens = Array.new

				rawtokens.each do |token|

					key, modifier, value = extract_key_value(token)

					if ( !params[:confirm].blank? && key == 'tag' ) then
						if firstTagFound == false then
							tokens << { :key => key, :modifier => modifier, :value => value }
							firstTagFound = true
						end
					else
						tokens << { :key => key, :modifier => modifier, :value => value }
					end


					# if the key is tag, then a separate clause handles it below

					if key != nil && key != "tag" && ( key_filterable?( key ) || key == 'name' ) then

						if key == 'name'
							modifier = "LIKE"
							value = render_target( value )
						end

						if conditions != "" then
							conditions += " AND "
						end

						conditions += " #{table_name}.#{key} #{modifier} '#{value}' "

					end

				end

				joins = assemble_joins( tokens, conditions, table_name, user )

				{
					:conditions => conditions,
					:select => "DISTINCT `#{table_name}`.*",
					:joins => joins.join(" "),
				}
			}
		end
		
	end

	module ClassMethods

		def strip_tag_tokens( raw_input )

			out = Array.new

			rawtokens = raw_to_tokens(query)

			rawtokens.each do |token|

				key, modifier, value = extract_key_value(token)

				if key != 'tag' then out << token end

			end

			out

		end

		def raw_to_tokens( raw_input )

			# This accounts for a comma separated list coming from the tags input autocompleter,
			# in effect, ignoring all the previous entries - but could

			return raw_input.split(',').each {|part| part.strip!};
		end

		def key_filterable?( key )
			
			# define filterable keys in model
			# filterable_keys = %w[ channels samplerate duration ]
			
			return filterable_keys.include?( key )
		end

		def key_reserved?( key )
			return Set.new(['tag:', 'all:', 'name:', 'accepted:', 'rejected:', 'unconfirmed:']).include?(key) || key_filterable?(key.chop)
		
			#return ( key == 'tag:' || key == 'all:' || key == 'name:' || key == 'unconfirmed:' || key_filterable?( key.chop ) )
		end

		def assemble_joins( tokens, conditions, table_name, user )

			# first order parsing

			# reserved keywords
			# like: - returns items with tag or name like search term inclusive
			# tag: - returns items with tag
			# name: - returns items with name

			# nouns and modifiers
			# target - exact match
			# *target - match with wild prefix
			# target* - match with wild suffix
			# *target* - match 'like'

			clauses = []
			
			unique_id = 0

			#preassemble tokens for conditions

			tokens.each do |token|

				# reserved tag:value
				# property:value
				# other:value

				clause = ""
				target = render_target( token[:value] )
				key = token[:key]

# WARNING!  please observe these SQL rules to remain in a harmonious state of non-annoyance:  ' single quote around string value in SQL  ` angled quote around table names


				clause =  "INNER JOIN tag t#{unique_id} ON t#{unique_id}.name LIKE '#{target}' "
				clause += "INNER JOIN tagging tg#{unique_id} ON tg#{unique_id}.tag_id = t#{unique_id}.id AND tg#{unique_id}.taggable_type = '#{table_name}' AND tg#{unique_id}.taggable_id = #{table_name}.id "


				case key

					when nil
						and_conditions = ""

						if conditions != ""
							and_conditions =  "AND (#{conditions})"
						end

						clause += " INNER JOIN `#{table_name}` n#{unique_id} ON n#{unique_id}.name LIKE '#{target}' AND n#{unique_id}.id = `#{table_name}`.id"


					when "tag", "all"

						clause += ""


					when "name"

						clause = ""
						# this will be handled in the includes
						#clause = " INNER JOIN `#{table_name}` n#{unique_id} ON n#{unique_id}.name LIKE '#{target}' AND n#{unique_id}.id = `#{table_name}`.id"


					when "accepted"

            			userid = user.nil? ? -1 : user.id

						clause += " AND tg#{unique_id}.user_id = #{userid} AND tg#{unique_id}.vote = 1 "


					when "rejected"

            			userid = user.nil? ? -1 : user.id

						clause += " AND tg#{unique_id}.user_id = #{userid} AND tg#{unique_id}.vote = -1 "

						
					when "unconfirmed"
												
						# TODO: the subselect should work on its own with a last 'AND t2.taggable_id = NULL', this is not working
						userid = user.nil? ? -1 : user.id

						clause += " AND tg#{unique_id}.user_id <> #{userid} "

					else
						clause = ""

				end

				clauses.push( clause )

				unique_id += 1
			end

			return clauses
		end

		def extract_key_value( token )

			#keyword: <=> value
			# includes modifier variants such as => <> <=

			key, colon, modifier, value = token.scan(/(\*?[A-Za-z\d\-_.]+\*?)(:?)\s*([<=>]{1,2})?\s*(\*?[A-Za-z\d\-_.:]+\*?)*/)[0]

			if modifier == "==" || !colon.blank?
				modifier = "="
			end

			if value.nil? || value.blank?
				value = key
				key = "tag"
				modifier = "LIKE"
			end

			return key, modifier, value
		end

		def render_target( value )

			# *value* -> '%value%'

			value.gsub("*", "%")
		end
		
	end

end