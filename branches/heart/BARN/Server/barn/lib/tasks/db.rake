# http://blog.craigambrose.com/articles/2007/03/01/a-rake-task-for-database-backups
# http://edge.matrixprojects.com/2008/5/21/backing-up-your-rails-mysql-db-via-a-rake-task

# CRONTAB
# 0 3 * * * cd /apps/barn.xbat.org/current && /usr/bin/rake db:backup DIR=/apps/backups/barn.xbat.org RAILS_ENV=production >/dev/null 2>&1

require 'find' 

namespace :db do
	
	desc "Backup the database to a file. Options: DIR=base_dir RAILS_ENV=production MAX=20"
	
	task :backup => [:environment] do
	
		datestamp     = Time.now.strftime("%Y-%m-%d_%H-%M-%S")
		
		base_path     = ENV["DIR"] || "db"
		
		backup_base   = base_path #File.join(base_path, 'backup')
		
		backup_folder = File.join(backup_base, datestamp)
		
		backup_file   = File.join(backup_base, "#{RAILS_ENV}_dump.sql.gz")    
		
		db_config     = ActiveRecord::Base.configurations[RAILS_ENV]   
		  
		# FileUtils.mkdir_p(backup_folder)    
		
		sh "/usr/bin/mysqldump -u #{db_config['username'].to_s} #{'-p' if db_config['password']}#{db_config['password'].to_s} --opt #{db_config['database']}"
		puts "Created backup: #{backup_file}" 
		
		sh 'cd /apps/backups/barn.xbat.org && /usr/bin/svn commit -m "automated update of the BARN production database"'    
		
		max_backups      = ENV["MAX"] || 20
		
		all_backups      = (Dir.entries(backup_base) - ['.', '..']).sort.reverse
		
		unwanted_backups = all_backups[max_backups.to_i..-1] || []
		
		# for unwanted_backup in unwanted_backups
		#   FileUtils.rm_rf(File.join(backup_base, unwanted_backup))
		#   puts "deleted #{unwanted_backup}"
		# end
		
		# puts "Deleted #{unwanted_backups.length} backups, #{all_backups.length - unwanted_backups.length} backups available"
	end
end
