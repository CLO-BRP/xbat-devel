# NOTE: these volume tasks work with the volumes.yml file

#---------------------
# HELPERS
#---------------------

def load_volumes
	volumes = []
	
	configs = YAML::load(File.open("#{RAILS_ROOT}/config/volumes.yml"))
	
	configs.each do |volume|
		volumes << Volume.new(volume)
	end
	
	return volumes
end

#---------------------
# TASKS
#---------------------

namespace :volumes do
	
	desc "Mount the drives specified in the config/volumes.yml"
	
	task :mount => [:environment] do
		volumes = load_volumes
		
		# NOTE: we mount each of volume, and save them to the DB
		
		volumes.each { |v| 
			puts "Mounting #{v.mount_point}"
			
			# Try to mount 
 
			if v.mount
				puts "Success."
			else 
				puts "Error mounting #{v.mount_point}, check the logs for details."
			end
			
			volume = Volume.find(:all, :conditions => {:name => v.name})
			if volume.blank? && v.mounted?
				v.save
			end
		}
	end
	
	desc "Unmount the drives specified in the config/volumes.yml"
	
	task :unmount => [:environment] do
		volumes = load_volumes
		
		# NOTE: unmount each of the volumes
		
		volumes.each { |v| 
			puts "Unmounting #{v.mount_point}"
			v.unmount 
			# Removing volumes from the DB could be bad...
			# Volume.delete_all(:name => v.name)
		}
	end
end