require 'digest'

class FileHash
	
	def self.md5(path)
		d = Digest::MD5.new
		
		File.open(path,'rb') do |f|
			d.update(f.read(8192)) until f.eof?
		end
		
		return d.hexdigest
	end
	
	def self.sha1(path)
		d = Digest::SHA1.new
		
		File.open(path,'rb') do |f|
			d.update(f.read(8192)) until f.eof?
		end
		
		return d.hexdigest
	end
	
end

