# TagMonster will eat your offending methods
# This class is currently mixed-in to NamedScopeHelper, who is in turn mixed into ActiveRecord models.
# It depends upon activerecord_extentions.rb for the convert_includes_to_joins method.

module TagMonster

	def self.included(base)

		base.extend(ClassMethods)

	end

	module ClassMethods
		
		#-----------
		# tag_counts_rescoped
		#-----------
		
		# This method is used as a replacement for the acts_as_taggable tag_counts class method
		# When applied at the end of a scoping chain, it transforms the scope to a Tag finder,
		# returning a set of tags and their counts for the class and its conditions.
		
		# A primary reason that tag_counts does not work is because it cannot (or does not) convert :include to :joins
		
		def tag_counts_rescoped(options = {})
			scope = scope(:find)


			# SELECT

			tag_select = "tag_id, taggable_type, taggable_id"


			if scope[:select].nil?
				scope[:select] = tag_select
			else
				scope[:select] += "," + tag_select
			end

			# CONDITIONS

			conditions = "tagging.taggable_type = #{quote_value(name)}"

			scope[:conditions] = self.send(:merge_conditions, conditions, scope[:conditions])

			if ( !options[:user] == nil )
				scope[:conditions] = self.send(:merge_conditions, "tagging.user_id = #{options[:user].id}", scope[:conditions])
			end


      #scope[:alias] = "Tagging AS t0"


			# GROUP
			scope[:group]	= "tag_id, taggable_type, taggable_id HAVING COUNT(*) > 0"

			# JOINS
			#barn_logger :info, scope[:joins]
			scope[:joins] = scope[:joins].join(" ") if scope[:joins].is_a?(Array)
			scope[:joins] = "" if scope[:joins].nil?

			tagging_joins = "INNER JOIN #{self.table_name} ON #{self.table_name}.id = taggable_id "


			contains_tagging = !scope[:joins].blank? && scope[:joins].is_a?(String) && scope[:joins].match(/INNER JOIN tagging/)

			if contains_tagging
				scope[:joins] = tagging_joins
			else
				scope[:joins] = self.send(:merge_joins, tagging_joins, scope[:joins])
			end

			#barn_logger :info, "scope[:joins] #{scope[:joins]}"

			# Convert the includes to joins
			if !scope[:include].nil?
				converted_joins = convert_includes_to_joins(scope[:include])
				scope[:joins] = self.send(:merge_joins, scope[:joins], converted_joins) unless converted_joins.blank?
				scope[:include] = nil
			end

			# Flatten the joins
			scope[:joins] = scope[:joins].join(" ") if scope[:joins].is_a?(Array)

			# Create the SQL from the new scoping and execute it

      scope[:order] = nil

			sql  = Tagging.send(:construct_finder_sql, scope)

      sql.sub!( "taggable_id FROM `tagging`", "taggable_id FROM `tagging` as t0")
      sql.sub!( "tagging.taggable_type", "t0.taggable_type")
      sql.gsub!( " tag_id", " t0.tag_id")
      sql.gsub!( " taggable_type", " t0.taggable_type")
      sql.gsub!( " taggable_id", " t0.taggable_id")

      out =  "SELECT tag_id, tag.name, COUNT(taggable_id) AS count FROM (" + sql +
             ") AS t1 INNER JOIN tag ON tag.id = t1.tag_id " +
             "GROUP BY tag_id ORDER BY tag.name"


#			if ( !options[:user] == nil )
#				scope[:conditions] = self.send(:merge_conditions, "tagging.user_id = #{options[:user].id}", scope[:conditions])
#			end
#
#			out = "SELECT `tag`.id, `tag`.name, COUNT( t1.id ) AS count FROM tag " +
#				"INNER JOIN #{name} t0 ON t0.user_id = #{options[:user].id} " +
#				"INNER JOIN tagging t1 ON t1.taggable_type = '#{name}' AND t1.taggable_id = t0.id AND t1.tag_id = `tag`.id " +
#
#				#"JOIN "
#
#				"GROUP BY 'tag'.id " +
#				"ORDER BY 'tag'.name "


			tags = Tag.find_by_sql(out)
			
			return tags
		end		

	end
	
end