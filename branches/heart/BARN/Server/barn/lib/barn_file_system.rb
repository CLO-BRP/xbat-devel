# NOTE: this module is a collection of methods that interact with the files and folders specific to BARN

# IMPORTANT: please be sure that the USER_FTP_ROOT constant is set correctly in the environment.rb file.

module BarnFileSystem
	
	# NOTE: this includes functions for file partitioning
	
	extend ApplicationHelper
	
	def self.update_user_data_files
		each_user do |user|
			user.update_data_files
		end
	end
	
	def self.cleanup_user_data_files
		each_user do |user|
			user.cleanup_data_files
		end
	end
	
	def self.remove_user_data_files
		each_user do |user|
			user.remove_missing_data_files
		end
	end

	def self.copy_user_data_files
		
		data_files = DataFile.find(:all, :conditions => {
			:copied => false, 
			:project_id => nil, 
			:directory => false, 
			:in_ftp => true
		})
		
		data_files.each do |data_file|			
			destination = files_full_url(data_file)
			
			puts "Copying #{data_file.source} to #{destination}"
			
			data_file.copy(destination)
		end
	end
	
	def self.copy_project_data_files
		
		# TODO: this will probably use the project.volume concept, we are not ready for this yet
	
	end

	#----------------
	# UTILITY METHODS
	#----------------
	
	def self.user_directories
		
		Dir.glob("#{USER_FTP_ROOT}/*")
	end
	
	def self.each_user
		
		user_directories.each do |directory|
			puts "Working in #{directory} ..."
			
			user = User.find_by_directory(directory)			
			
			next if user.blank? 
			
			yield user
		end		
	end
		
end