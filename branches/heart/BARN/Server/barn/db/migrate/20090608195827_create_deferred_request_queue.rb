class CreateDeferredRequestQueue < ActiveRecord::Migration
  def self.up
		create_table :deferred_request do |t|
			t.text 			:request
			t.text 			:response
			t.string 		:silo_id
			t.string 		:user_id
			t.datetime 	:submitted_at
			t.datetime 	:started_at
			t.datetime 	:finished_at
		end
  end

  def self.down
		drop_table :deferred_request
  end
end