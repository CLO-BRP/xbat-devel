class RemoveIdFromRoleJoinTable < ActiveRecord::Migration
  def self.up
    begin drop_table "role_user" rescue true end
    create_table :role_user, :id => false do |t|
      t.column :role_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.timestamps
    end
    
    # we assume this is here already!
    admin_role = Role.find_by_name('administrator')
    admin = User.find_by_login('default')
    admin.roles << admin_role if admin_role
  end

  def self.down
    begin drop_table "role_user" rescue true end
    create_table :role_user do |t|
      t.column :role_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.timestamps
    end
    
    # we assume this is here already!
    admin_role = Role.find_by_name('administrator')
    admin = User.find_by_login('default')
    admin.roles << admin_role if admin_role
  end
end
