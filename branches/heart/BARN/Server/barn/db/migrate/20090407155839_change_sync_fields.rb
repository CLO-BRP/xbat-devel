class ChangeSyncFields < ActiveRecord::Migration
  def self.up
		change_column :sync, :added, :text
		change_column :sync, :updated, :text
		change_column :sync, :deleted, :text
		
		change_column :sync, :added_count, :integer
		change_column :sync, :updated_count, :integer
		change_column :sync, :deleted_count, :integer
  end

  def self.down
		change_column :sync, :added, :string
		change_column :sync, :updated, :string
		change_column :sync, :deleted, :string
		
		change_column :sync, :added_count, :string
		change_column :sync, :updated_count, :string
		change_column :sync, :deleted_count, :string
  end
end
