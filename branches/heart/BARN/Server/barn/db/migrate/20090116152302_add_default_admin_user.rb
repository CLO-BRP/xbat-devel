class AddDefaultAdminUser < ActiveRecord::Migration
  def self.up
    user = User.create :login => "default", :email => "default@email.com", :password => "password", :password_confirmation => "password"
    user.register
    user.activate
    user.save
  end

  def self.down
    User.find_by_login("default").destroy
  end
end
