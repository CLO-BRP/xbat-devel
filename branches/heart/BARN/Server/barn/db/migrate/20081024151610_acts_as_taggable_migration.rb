class ActsAsTaggableMigration < ActiveRecord::Migration
  def self.up
    begin drop_table "tag" rescue true end
    
    create_table :tag do |t|
      t.column :name, :string
    end
    
    begin drop_table "tagging" rescue true end
    
    create_table :tagging do |t|
      t.column :tag_id, :integer
      t.column :taggable_id, :integer
      
      # You should make sure that the column created is
      # long enough to store the required class names.
      t.column :taggable_type, :string
      
      t.column :created_at, :datetime
    end
    
    add_index :tagging, :tag_id
    add_index :tagging, [:taggable_id, :taggable_type]
  end
  
  def self.down
    drop_table :tagging
    drop_table :tag
  end
end
