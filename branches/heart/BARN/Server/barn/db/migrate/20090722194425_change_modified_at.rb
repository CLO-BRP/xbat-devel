class ChangeModifiedAt < ActiveRecord::Migration
  def self.up
		rename_column :data_file, :updated_at, :modified_at
  end

  def self.down
		rename_column :data_file, :modified_at, :updated_at
  end
end
