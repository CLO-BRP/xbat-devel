class AddCumulativeToRecordingSounds < ActiveRecord::Migration
  def self.up
    add_column :recording_sound, :cumulative_samples, :integer 
  end

  def self.down
    remove_column :recording_sound, :cumulative_samples
  end
end
