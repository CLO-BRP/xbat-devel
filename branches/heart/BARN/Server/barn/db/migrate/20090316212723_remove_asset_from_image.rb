class RemoveAssetFromImage < ActiveRecord::Migration
  def self.up
		remove_column :image, :asset_id
		remove_column :image, :asset_type
  end

  def self.down
		add_column :image, :asset_type, :string
		add_column :image, :asset_id, :integer
  end
end
