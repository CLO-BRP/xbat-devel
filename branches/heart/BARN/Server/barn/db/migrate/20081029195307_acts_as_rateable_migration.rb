class ActsAsRateableMigration < ActiveRecord::Migration
  def self.up
    begin drop_table "rating" rescue true end
    
    create_table :rating do |t|
      t.column :rating, :integer
      t.column :rateable_type, :string
      t.column :rateable_id, :integer
      t.column :user_id, :integer
      t.timestamps
    end
  end
  
  def self.down
    drop_table :rating
  end
end
