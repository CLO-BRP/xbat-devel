class DataFile < ActiveRecord::Migration
  def self.up
    create_table :data_file do |t|
      t.column :guid, :string
      t.column :location, :string
      t.column :name, :string
      t.column :ext, :string
      t.column :directory, :boolean
      t.column :content_hash, :string
      t.column :parent_id, :integer
      t.column :size, :integer
      t.column :user_id, :integer
      t.timestamps
    end
  end

  def self.down
  end
end
