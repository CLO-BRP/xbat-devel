class AddUserToPeer < ActiveRecord::Migration
  def self.up
    add_column :peer, :user_id, :integer
  end

  def self.down
    remove_column :peer, :user_id
  end
end
