class AddGuidToDeferredRequest < ActiveRecord::Migration
  def self.up
		add_column :deferred_request, :guid, :string
  end

  def self.down
		remove_column :deferred_request, :guid
  end
end
