class RenameAnnotatedToNotings < ActiveRecord::Migration
  def self.up
		rename_table :annotation, :noting
  end

  def self.down
		rename_table :noting, :annotation
  end
end
