class AddPubicFlagToSound < ActiveRecord::Migration
  def self.up
    add_column :sound, :public, :boolean, :default => true
  end

  def self.down
    remove_column :sound, :public
  end
end
