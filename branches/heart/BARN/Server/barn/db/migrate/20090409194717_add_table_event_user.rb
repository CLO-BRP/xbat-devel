class AddTableEventUser < ActiveRecord::Migration
  def self.up
		create_table :event_user, :id => false do |t|
      t.string 		:event_id
			t.string 		:user_id
      t.datetime 	:created_at
    end
  end

  def self.down
		drop_table :event_user
  end
end
