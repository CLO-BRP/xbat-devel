class ShareFuMigration < ActiveRecord::Migration
  def self.up
    create_table :shared, :force => true do |t|
      t.column :shareable_id,       :integer
      t.column :shareable_type,     :string, :limit => 30
      t.column :forwardable,        :boolean
      t.column :created_at,         :timestamp
      t.column :modified_at,        :timestamp
    end
    
    create_table :sharing, :force => true do |t|
      t.column :user_id,            :integer
      t.column :shared_id,          :integer
      t.column :recipient_id,       :integer
      t.column :created_at,         :timestamp
    end
  end

  def self.down
    drop_table :sharing
    drop_table :shared
  end
end
