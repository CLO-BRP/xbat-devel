class Shared < ActiveRecord::Base
  
  include AuthorizationApocalypse::Keymaster
  
  belongs_to :shareable, :polymorphic => true
  has_many :sharings
  
  
  def self.find_shares_by_user(user)
    find(:all,
      :conditions => ["user_id = ?", user.id],
      :order => "created_at DESC"
    )
  end
  
  def self.find_by_shared_to(object)
    to = ActiveRecord::Base.send(:class_name_of_active_record_descendant, object.class).to_s
    find(:all, :conditions=>["shared_to_type=? and shared_to_id=?", to, object.id])
  end
  
  def self.find_by_shareable_and_shared_to(shareable, object)
    share = ActiveRecord::Base.send(:class_name_of_active_record_descendant, shareable.class).to_s
    to = ActiveRecord::Base.send(:class_name_of_active_record_descendant, object.class).to_s
    Share.find(:all, :conditions=>["shareable_type = ? and shareable_id = ? and shared_to_type = ? and shared_to_id = ?",
                    share, shareable.id, to, object.id])
  end
  
  #------------------
	# RESTful ACL methods
	#------------------
	
	# THESE ARE NOT DONE YET
	
	def self.indexable_by?(user, parents = nil)
    (user)? true : false
  end

	def self.creatable_by?(user, parent = nil)
    (user)? true : false
  end

  def readable_by?(user, parents = nil)
    true
  end

  def updatable_by?(user, parent = nil)
		(user && user.id == self.user.id) ? true : false
  end

  def deletable_by?(user, parent = nil)
		(user && self.sharings.any?{|s| s.user_id == user.id}) ? true : false
  end
  
end