module SoundKeymaster

  module ClassMethods
    # NOTE: the second condition works to prevent project sound access
    def indexable_by?(user, parents = [])
    	parents.blank? || parents_readable_by?(user, parents)
    end

    # NOTE: any logged in user can create a sound
    def creatable_by?(user, parents = [])
      user ? true : false
    end
  end
    
  def readable_by?(user, parents = [])
    # NOTE: the parent condition allows sound access through a project
  	public? || owned_by?(user) || parents_readable_by?(user, parents) && in_project?(parents.first)
  end

  def updatable_by?(user, parents = [])
  	owned_by?(user)
  end

  def deletable_by?(user, parents = [])
  	owned_by?(user)
  end

protected
  
  def owned_by?(user)
    # NOTE: these are helpers to the access control helpers
    return false if user.blank?  
    (user_id == user.id) || sound_users.any?{|relation| relation.user_id == user.id}
  end

  def in_project?(project)
    return false if project.blank?

    project.project_sounds.any? { |relation| relation.asset_id == id }
  end
  
end