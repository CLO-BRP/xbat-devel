module TagSetKeymaster

  module ClassMethods
    # NOTE: the second condition works to prevent project tag_set access
    def indexable_by?(user, parents = [])
    	parents.blank? || parents_readable_by?(user, parents)
    end

    # NOTE: any logged in user can create a tag_set
    def creatable_by?(user, parents = [])
      user ? true : false
    end
  end
    
  def readable_by?(user, parents = [])
    # NOTE: the parent condition allows tag_set access through a project
  	public? || owned_by?(user) || parents_readable_by?(user, parents) && in_project?(parents.first)
  end

  def updatable_by?(user, parents = [])
  	#owned_by?(user)
    true
  end

  def deletable_by?(user, parents = [])
  	#owned_by?(user)
    true
  end

protected

  def in_project?(project)
    return false if project.blank?

    project.project_tag_sets.any? { |relation| relation.asset_id == id }
  end
  
end