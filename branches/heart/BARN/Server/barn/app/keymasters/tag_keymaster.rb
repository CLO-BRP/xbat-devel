module TagKeymaster

  module ClassMethods
    def indexable_by?(user, parents = [])
    	parents.blank? || parents_readable_by?(user, parents)
    end

    def creatable_by?(user, parents = [])
      user ? true : false
    end
  end

  def readable_by?(user, parents = [])
  	public? || owned_by?(user) || parents_readable_by?(user, parents) && in_project?(parents.first)
  end

  def updatable_by?(user, parents = [])
  	owned_by?(user)
  end

  def deletable_by?(user, parents = [])
  	owned_by?(user)
  end

protected


# TODO: define correct implementation for tags

  def owned_by?(user)
#    return false if user.blank?
#    (user_id == user.id) || sound_users.any?{|relation| relation.user_id == user.id}
	  true
  end

  def in_project?(project)
#    return false if project.blank?
#
#    project.project_sounds.any? { |relation| relation.asset_id == id }
	true
  end

end