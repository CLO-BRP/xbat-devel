class DeferredRequest < ActiveRecord::Base
	belongs_to :user
	
	belongs_to :silo
	
	before_save GUIDGenerator.new
end