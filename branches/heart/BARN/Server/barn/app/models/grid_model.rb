
class GridModel

	attr_accessor :type, :object_path, :options, :heading_markup, :heading_selector, :container_id,
				  :total_entries, :paginate_markup, :expansion_url, :columns	

	def initialize( caller, params, options, local_assigns )
	
		#--
		# get the type that we are listing and some convenience variables
		#--

		@type = params[:controller]

		@object_path = type.singularize + '_path'

		@options = options || {}

		@heading_markup = local_assigns[:heading_markup] ? local_assigns[:heading_markup] : "<h2> #{caller.title_caps(type)} (#{@total_entries}) </h2>"

		@heading_selector = params.include?(:heading_selector) ? params[:heading_selector] : ''

		@container_id = local_assigns[:container_id] ? local_assigns[:container_id] : caller.rand_id

		@total_entries = @metadata[:total_entries]

		@paginate_markup = objects.is_a?(WillPaginate::Collection) ? caller.will_paginate(objects) : nil;

		@expansion_url = ""

		@columns = caller.columns_default



		# NOTE: the default expansion url is the show for the object without layout

		if options[:expansion] == 1 && (!local_assigns[:expansion_url] || expansion_url.blank?)

			expansion_url = Proc.new {|object|
				send(type.singularize + '_path', object, :format => 'html', :layout => false)
			}

		end

	end



end

