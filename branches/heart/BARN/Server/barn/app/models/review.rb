class Review < ActiveRecord::Base

	include AuthorizationApocalypse::Keymaster	

	include WillPaginateHelper

	include NamedScopeHelper

	belongs_to :parent, :polymorphic => true	

	belongs_to :tag_proxy, :foreign_key => "tag_id"

	alias_method :tag, :tag_proxy

	validates_uniqueness_of :tag_id, :scope => [:parent_id, :parent_type, :object_type]

	#acts_as_annotated

	#----------
	# with_parent
	#----------

	named_scope :with_parent, lambda { |*args|
		# Setup variables
		params 	 = args.first || {}
		project_id = params[:project_id]

		# Lets not even bother if we dont have the information we need
		return {} if project_id.blank?


		#TODO this is not polymorphic!!
		conditions = "review.parent_id = #{project_id} AND review.parent_type = 'Project'"

		{
			:conditions => conditions
		}

	}


	def events
		if @events.nil?
			@events = Event.in_project( :parent_id )
		end
		@events
	end



end

class TagProxy < ActiveRecord::Base

	set_table_name "tag"
	
end