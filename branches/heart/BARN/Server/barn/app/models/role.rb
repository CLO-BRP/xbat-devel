class Role < ActiveRecord::Base
  
  # has_many :role_users
  
  # has_many :users, :through => :role_users
  
  has_and_belongs_to_many :users
  
  has_and_belongs_to_many :project_users
  
  def self.default_roles
	
    %w{administrator member guest watcher}
  end

  # NOTE: this is how we ask ActiveRecord to make some default values available
  
  def self.DEFAULTS
	
	  default_roles
  end
end
