class PeerUser < ActiveRecord::Base
 	belongs_to :peer
 	belongs_to :user
	has_many :syncs
	
  validates_presence_of :user_id, :peer_id
end