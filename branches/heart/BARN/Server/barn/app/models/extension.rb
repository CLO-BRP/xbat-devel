class ExtensionType
  
  SOUND_DETECTOR = "sound_detector"
end

class Extension < ActiveRecord::Base
  
  set_inheritance_column :ruby_type

  has_many :extension_presets

  # getter for the "type" column
  
  def extension_type
	self[:type]
  end
  
  # setter for the "type" column
  
  def extension_type=(s)
	self[:type] = s
  end

end

