class DataFile < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	acts_as_tree :order => "name"
	
	belongs_to :user
	
	def self.indexable_by?(user, parents = nil)
		return (user.blank?) ? false :true
	end

	# TODO: this is bad, what are we supposed to have here
	
	named_scope :readable_by, lambda { |*args| 
		return {}
	}
	
	def soft_delete
		update_attribute(:in_ftp, false)
	end
	
	# TODO: this should be changed to concatenate the volume and relative location when relevant
	
	def source
		location
	end
	
	def copy(destination)
		FileUtils.mkdir_p(destination)
		
		FileUtils.copy(source, destination)
		
		update_attribute(:copied, true) # NOTE: we update the datafile record to reflect the copy
	end
	
end