require 'httparty'
require 'httparty_harder'

class Silo < Peer
	include HTTParty
	include HTTPartyHarder
	
	attr_accessor :username, :password
	
	validates_presence_of :host, :port
	
	# Default ActiveRecord attributes
	# http://www.jroller.com/obie/entry/default_values_for_activerecord_attributes
	
	def host
		self[:host] || 'localhost'
	end
  
	def port
		self[:port] || 80
	end
  
	# Initialize faux-AR accessors
	# http://blog.hasmanythrough.com/2007/1/22/using-faux-accessors-to-initialize-values
	
	def after_initialize
		# it's possible that the username and password are not the owner's
		
		self.username = @username || user.login
		
		@auth = {:username => self.username, :password => @password}
	end
  
	# Implemented from HTTPartyHarder
  
	def base_uri
		# lazy initialization (and caching) with the base_uri
		
		if @base_uri.nil? || host_changed? || port_changed?
			@base_uri = File.join('http://', "#{host}:#{port}") 
		end
		
		return @base_uri
	end
  
end