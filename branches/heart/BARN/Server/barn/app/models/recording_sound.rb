class RecordingSound < ActiveRecord::Base
  belongs_to :recording
  belongs_to :sound
end