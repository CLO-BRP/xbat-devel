class Project < ActiveRecord::Base
	
	include AuthorizationApocalypse::Keymaster
	
	include WillPaginateHelper
	
	include NamedScopeHelper
	
	before_save GUIDGenerator.new
	
	class AssetTypeError < StandardError; end
	
	#---------------
	# associations
	#---------------
	
	belongs_to :volume
		
	# members
	
	has_many :project_users
	
	has_many :users, :through => :project_users

	has_many :project_assets

	has_many :reviews, :as => :parent

	# NOTE: the project asset relations is polymorphic, we alias the various types as associations
	# NOTE: These are used when adding things like tags, notes, and ratings to a resource in context of a project

	%w{sound image}.each do |association|
		
		has_many "project_#{association.pluralize}", 
			:class_name => "ProjectAsset",
			:conditions => "asset_type = '#{association.capitalize}'"
			
		has_many association.pluralize.to_sym, 
			:through => :project_assets, 
			:source => :asset, 
			:source_type => association.capitalize, 
			:class_name => association.capitalize
	end


	has_many :logs,
			 :finder_sql => "SELECT log.* FROM log INNER JOIN project_asset p ON p.asset_type = 'Sound' AND p.asset_id = log.sound_id AND p.project_id = " + '#{ self.id }',
			:counter_sql => "SELECT count( DISTINCT( log.id )) AS count_all FROM log INNER JOIN project_asset p ON p.asset_type = 'Sound' AND p.asset_id = log.sound_id AND p.project_id = " + '#{ self.id }'




	
	has_many :file_resources, :as => :attachable

	has_many :tag_sets, :as => :context
	
	#--
	# active record extensions
	#--
	
	acts_as_taggable
	
	acts_as_annotated
	
	acts_as_rateable 

	def self.filterable_keys
		%w[]		
	end

	def files_root
		"#{VOLUME_ROOT}"
		#"#{volume.mount_point}/#{files_path}"
	end
	
	#---------------
	# validations
	#---------------
	
	validates_presence_of :name
	
	#------------------
	# Access Control overwrites
	#------------------
	
	# User should be logged in if this method is called	
	def check_user_roles
		@project = Project.find(params[:id])
		
		base_conditions = logged_in? && ((@project.owner == User.current) || User.current.is_admin?)

		case params[:action]
		# for the 'show' action, we allow anyone to see it if public
		when 'show'	
			unless base_conditions || @project.public? || @project.user_has_role?(User.current, ['administrator', 'member', 'guest'])
				flash[:notice] = "Sorry, that project is private."
				access_denied#resource_access_denied # defined in application.rb
			end
		 
		when 'edit'
			unless base_conditions || @project.user_has_role?(User.current, ['administrator'])
				flash[:notice] = "Nice try."				
				access_denied #resource_access_denied # defined in application.rb
			end

		end # case
	end
	
	#--------- 
	# assets_writeable_by?
	#---------
	
	# Special project access control method
	
	def assets_updatable_by?(user)
		self.updatable_by?(user) || self.has_member?(user)
	end

	#------------------
	# NAMED SCOPES
	#------------------

	#--------- 
	# owned_by	
	#---------
		
	named_scope :owned_by, lambda { |*args| 
		{
			:conditions => ["#{ProjectUser.table_name}.user_id = ? AND #{ProjectUser.table_name}.role_id = 1", user.id ]
			#:conditions => ["#{table_name}.user_id = ?", args.first ]
		} 
	}

	#--------- 
	# with_role	
	#---------

	named_scope :with_role, lambda { |*args| 
		roles = (args.first)? Role.find_all_by_name(args.first) : nil
		{
			:include => :users, 
			:conditions => ["#{ProjectUser.table_name}.role_id IN (?)", (roles || Role.default_roles) ]
		} 
	} 
	
	#--------- 
	# with_asset	
	#---------
	
	named_scope :with_asset, lambda { |*args| 
		raise "You need at least one project asset instance as an argument" if args.first.blank?
		# this finder works with one or many assets
		asset_instance = ( args.first.is_a?(Array) ) ? args.first : [args.first]
		asset_ids			= asset_instance.collect{|a| a.id }.join(',')
		asset_class		= asset_instance.first.class

		# magic block used by named_scope ...
		{
			:include		=> asset_class.name.tableize,
			:conditions => "#{asset_class.table_name}.id IN (#{asset_ids})"
		}
	}



	named_scope :watched_by_user, lambda { |*args|
		return {} if args.nil? || args.first.nil?
		user = User.find(args.first)
		role = args[1]
		{
			:include => [:project_users],
			:conditions => ["project_user.user_id = #{user.id} AND project_user.role_id = #{role.id}"]
		}
	}

	#---------
	# readable_by
	#---------

	named_scope :readable_by, lambda { |*args|
		user = (args.first.is_a?(User))? args.first : User.find(args.first)
		{
			:include => :users,
			:conditions => ["#{ProjectUser.table_name}.user_id = ?", user.id ]
			#:conditions => ["#{table_name}.user_id = ? OR #{ProjectUser.table_name}.user_id = ?", user.id, user.id ]
		}
	}


	#------------------
	# CLASS METHODS
	#------------------
	
	# none yet

	#------------------
	# INSTANCE METHODS
	#------------------
	
	def private?
		!self.public
	end

	def members
		users
	end

	def owner
		#NOTE the project.user_id field is _not_ used here - it exists only for deletion rights, not any other display.
		if @owner.nil?
			@owner = User.find(:first, :joins => :project_users, :conditions => "#{ProjectUser.table_name}.role_id = 1")
		end

		@owner
	end



	#--------- 
	# attach_images	
	#---------
	
	# This will silently fail if you try to attach non-image files as images...
	
	def attach_images(imgs)
		return false if imgs.blank?
		
		imgs.each do |image|
			i = Image.create(:uploaded_data => image)
			 if i.errors.blank?
				self.images << i
			end
		end unless imgs.blank?
	end



	#---------
	# update_assets
	#---------
	
	# This function acts as a proxy to add_assets and remove_assets
	
	def update_assets(params)	
		id		 = params[:asset_id]
		type	 = params[:asset_type]
		action = params[:asset_action]
		
		return false if id.nil? || type.nil? || action.nil?
		
		case action 
			when 'add'
				add_assets(type, id)
			when 'remove'
				remove_assets(type, id)
		end
	end
	
	#---------
	# add_assets
	#---------
	
	# Returns the assets added if any, or an empty array if nothing was done
	
	def add_assets(type, id)	
		# We are making the assumption that type is either a string (more likely) or a constant
		association = (type.is_a? String) ? type.tableize : type.class_name.tableize
		added = []
		
		sanitize_assets(type, id).each do |asset|
			unless Set.new(self.send(association)).member?(asset)
				self.send(association) << asset
				added << asset
			end
		end
		
		return added
	end
	
	#---------
	# remove_assets
	#---------
	
	# Returns the assets removed if any, or an empty array if nothing was done
	# This functions much like Array#delete
	
	def remove_assets(type, id)

		# We are making the assumption that type is either a string (more likely) or a constant
		association = (type.is_a? String) ? type.tableize : type.class_name.tableize

		self.send(association).delete( sanitize_assets(type, id) )

	end
	
	#---------
	# sanitize_assets
	#---------
	
	# For the moment, this assumes that the items to be removed exist... 
	# if they do not it will throw a not found error!
	
	def sanitize_assets(type, assets)
		begin
			klass = (type.is_a? String) ? type.classify.constantize : type
		rescue NameError => error
			raise AssetTypeError, "Asset type #{type.classify} is not defined."
		end
		
		if assets.is_a?(Array)
			assets.map!{ |a| a.to_i if a.is_a?(String) }
		else
			assets = assets.to_i if assets.is_a?(String)
			assets = [assets] # make this a collection
		end
		
		# now that we know its an enumerable with a Klass or Integers
		assets.map!{ |asset| (asset.is_a? klass)? asset : klass.find(asset) }
	end





	#---------
	# update_memberships
	#---------
	
	def update_memberships(params)
		result = {:notice => "", :error => ""}
		
		# add users
		
		if !params[:add_users].blank?

			users_to_add = Project.parse_users(params[:add_users])

			added, errors	=	add_users(users_to_add, params[:role_id])
			result[:notice] = "#{added.size} user(s) added." if added && added.size > 0
			result[:error]	= "Could not add '#{errors.join(', ')}' to the project. Does the user exist?" if errors && errors.size > 0
		end

		# remove users as necessary
		
		if !params[:remove_users].blank?

			#users_to_remove = Project.parse_users(params[:remove_users])

			removed, errors	=	remove_users(params[:remove_users])
			result[:notice] = "#{removed.size} user(s) removed." if removed && removed.size > 0
			result[:error] = "Could not remove '#{errors.join(', ')}' from the project." if errors && errors.size > 0
		end

		return result
	end


	def self.parse_users( users )
		matches = users.scan(/(\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b)/i).flatten!
	end


	#--------- 
	# add_users	
	#---------
	
	def add_users(users = [], role = nil)
		added	= []
		errors = []

		#TODO enforce uniqueness for admin user

		users = [users] if !users.is_a? Array
		
		# we are only interested in the ID of the role
		role = (role.is_a?(Integer)) ? Role.find(role) : Role.find_by_name(role)

		# ... and if something still went terribly wrong	
		if role.blank?
			logger.error "ERROR: You have add a user with a role that does not exist. We have chosen something logical for you."
			role = Role.find_by_name(Role.default_roles[1]) # member
		end

		users.each do |user|
			user = (user.is_a? User)? user : User.find_by_login_or_email(user)
			#user = (Float(user) != nil rescue false)? User.find_by_id(user) : User.find_by_login_or_email(user)
			if user.blank?
				errors << user

			#elsif !self.has_user?(user)
			elsif !self.user_has_role?(user, role.name)
				# is there a better way to do this?
				ProjectUser.create(:user_id => user.id, :role_id => role.id, :project_id => self.id)
				added << user
			end
		end unless users.blank?
		
		return added, errors
	end

	#---------
	# remove_users 
	#---------
	
	def remove_users(users = [])
		removed	= []
		errors = []
				
		unless users.blank? # its possible that the caller passes in a nil value
			users.each do |user|
				if Integer(user)
					user = User.find(user)
				else
					user = User.find_by_login_or_email(user)
				end


				if user.blank? || user.id == self.user_id
					errors << user
				end				

				if self.has_user?(user)
					self.users.delete( user )
					removed << user
				end
			end # users.each 
		end # users.blank?
		
		return removed, errors
	end # remove_users


	def remove_user_role( users = [], role = nil )
		removed	= []
		errors = []

		users = [users] if !users.is_a? Array

		# we are only interested in the ID of the role
		role = (role.is_a?(Integer)) ? Role.find(role) : Role.find_by_name(role)

		# ... and if something still went terribly wrong
		if role.blank?
			logger.error "ERROR: You have tried to remove a role that does not exist."
		end

		if role == 1
			logger.error "ERROR: you cannot remove the owner of a project."
			users = []
		end

		users.each do |user|
			user = (user.is_a? User)? user : User.find_by_login_or_email(user)
			if user.blank?
				errors << user
			else
				@targets = ProjectUser.find(:all, :conditions => ["user_id = ? AND role_id = ? AND project_id = ?",user.id, role.id, self.id])
				ProjectUser.delete(@targets.map(&:id))
				removed << user
			end
		end unless users.blank?

		return removed, errors
	end

	#--------- 
	 # has_user?
	#---------	
	
	def has_user?(user)
		users.any?{|u| u == user }
	end
	alias_method :has_member?, :has_user?
	
	#---------
	# user_can_edit?	
	#---------
	
	def user_can_edit?(user)
		# logged_in? is not defined for the Model!, 
		# plus logged in state should be check well before this point
		!user.blank? and (user.is_admin? or self.owner == user)
	end
	
	#---------
	# user_has_role?
	#---------
	
	def user_has_role?(user, rolename)
		roles = Role.find_all_by_name(rolename)
		
		if roles.blank? # there is no such role
			raise "No such Role: \"#{rolename.join(',')}\""
		end
		
		usership = ProjectUser.find(:all, :conditions => "project_id = #{self.id} AND user_id = #{user.id} AND role_id IN (#{roles.collect{|r| r.id }.join(',')})")
		
		return (usership.blank?)? false : true
	end
	
	#---------
	# user_is_admin?
	#---------
	
	def user_is_admin?(user)
		self.user_has_role?(user, ['administrator'])
	end
	
	alias_method :has_admin?, :user_is_admin?
	
	#---------
	# user_is_owner?
	#---------
	
	def user_is_owner?(user)
		(self.owner == user)
	end
	
	alias_method :has_owner?, :user_is_owner?

end
