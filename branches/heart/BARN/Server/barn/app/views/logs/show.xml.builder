# The following XML Builder template was distilled from this original code:

# events = @events.to_xml(:except => :rating, :dasherize => false, :include => {:tags => {:only => ['name', 'id']}}, :methods => :rating) do |xml|
#   @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :builder => xml, :dasherize => false)
# end

# By coding this by hand, we have far more control over the format of the XML

# This template hands either a single event from the 'show' action or multiple events
# via the index action and show action called with a set notation

xml.instruct! :xml, :version=>"1.0"

# NOTE: a single log is represented in a simple way, a set of logs is a result set packed with some metadata

unless @logs.is_a?(Enumerable)
  xml << render(:partial => 'log', :locals => {:log => @logs})
else
  xml.tag!(:result) {
    # metadata
    @metadata = {} if @metadata.blank?
    xml << @metadata.to_xml(:skip_instruct => true, :root => 'metadata', :dasherize => false)
    
    # data
    xml.tag!(:logs, :type => 'array') {
      xml << render(:partial => 'log', :collection => @logs)
    }
  }
end