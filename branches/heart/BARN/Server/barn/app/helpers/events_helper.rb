module EventsHelper
  
  def columns_default

	  	#NOTE: we will not require expansion or selection columns
      	#columns = super
		columns = ListingColumn.assemble([]);


		if params[:partial] == 'events/listing'
			
			#NOTE: these are listing table columns

            columns_list = %w[event time freq rating tags]

			if params.include? :sound_id
              columns_list = %w[event log time freq rating tags]
            end

            columns.concat ListingColumn.assemble(columns_list)
		else	
			#NOTE: these are 'sort by' fields in clips display
			
			if params.include? :sound_id
				columns.concat ListingColumn.assemble(%w[score time duration freq rating])
				
			elsif params.include? :log_id
				columns.concat ListingColumn.assemble(%w[score time duration freq rating])
				
			else
				columns.concat ListingColumn.assemble(%w[score time duration freq rating]) # TODO: include a sound/log display column, not yet defined
			end
		end
			
  end
  
  def column_markup(row, column, event)

    if params[:partial] == 'events/listing'

      case column.name

      #--
      # event displays
      #--

      #TODO: develop small, medium, large event displays

      when 'event'

        #--
        # setup
        #--

        tip_id = "event_#{event.id}_tip" #rand_id

        if event.duration.blank?
          time = sec_to_clock(event.start)
        else
          time = sec_to_clock(event.start) + '&mdash;' + sec_to_clock(event.start + event.duration)
        end

        if params.include? :log_id
          title = "#{event.log.name} &mdash; ##{event.id}"
        else
          # TODO: this fails in the pure events view, the sound is 'nil'
          #title = "#{event.log.sound.name} &mdash; #{event.log.name} &mdash; ##{event.id}"
          title = "#{event.log.name} &mdash; ##{event.id}"
        end

        #--
        # create base event markup for the different views
        #--

        html = title.to_s()
#        "
#        #{link_to(title, event_url(event), :class => 'load-local-tip', :rel => '#' + tip_id, :title => title)}
#
#        <div class='local-tip render-inline-player element' id='#{tip_id}'>
#          <a href='#{files_root_url(event, 'clip.mp3')}'>
#            <div class='progress' style='height: 129'></div>
#            <img src='#{files_root_url(event, 'thumb.256.gray-light.png')}'/>
#          </a>
#        </div>
#        "
      #--
      # time displays
      #--

      when 'start'
        sec_to_clock(event.start)

      when 'duration'
        sec_to_clock(event.duration)

      when 'time'
        if event.duration.blank?
          sec_to_clock(event.start)
        else
          sec_to_clock(event.start) + '&mdash;' + sec_to_clock(event.start + event.duration)
        end

      #--
      # frequency displays
      #--

      when 'low', 'high'
        rate_to_hertz(event.send(column))

      when 'freq'
        if event.low.blank?
          ''
        else
          rate_to_hertz(event.low) + '&mdash;' + rate_to_hertz(event.high)
        end

      when 'log'
        link_to(event.log.name, log_path(event.log))

      # NOTE: we use the application helper default

      else
        super

      end

    else

      p = params.merge({:id => event.id, :action => 'show', :partial => 'events/details_hover'})

      html = render(:partial => "widgets/inline_player", :locals => {
        :resource => event,
        :tip_location => url_for(p),
        :tip_title => title,
        :user => User.current
      })


    end
		
  end
  
  def column_sorting(column)
				
		case column.name

		when 'event'
			{:order => 'duration', :direction => toggle_sort_direction("duration")}
			
		when 'time'
		  {:order => 'start', :direction => toggle_sort_direction("start")}
		
		when 'freq'
		  {:order => 'low', :direction => toggle_sort_direction("low")}
		  
		when 'log'
		  {:order => 'log.name', :direction => toggle_sort_direction("log.name"), :include => :log}
		
		# NOTE: sorting information is provided by application helper
		
		when 'score'
			{:order => 'score', :direction => toggle_sort_direction("score", 'desc')}					

		else
			super
			
		end
		
  end
  
	def sub_menu
		
		# TODO: what is the difference between 'path' and 'address'
		return []
#		return [
#			{:title => "Clips", :path => "/new"},
#			{:title => "List", :address => "/delete"},
#			{:title => "Hybrid", :path => "/some-place"}
#		]
	end	

end
