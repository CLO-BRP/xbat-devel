module LogsHelper
  
  def columns_default
	columns = super
		        
	if params[:sound_id].blank?
		columns.concat ListingColumn.assemble([
			{:name => 'log_sound', :width => '20%'}
		])
    end
      
	columns.concat ListingColumn.assemble([
		{:name => 'log', :width => '20%'},
		{:name => 'events', :width => '4%'},
		{:name => 'rating', :width => '12%'},
		{:name => 'tags', :width => ''}
	]);		
  end
  
  def header_markup ( column, params )
		
		case column.name
		  
		when 'log_sound'
		  "<th class='#{column.name}' width='#{column.width}'>Sound</th>"
		  
		else
		  super
		  
		end	
  end
  
  def column_markup (row, column, log)
	
		case column.name
			
		when 'log_sound'
			link_to(log.sound.name, sound_path(log.sound))
			
		when 'log'
			link_to(log.name, log_path(log))
			
		when 'events'
			log.events_count
			
		else
			super
			
		end
  end
  
  def column_sorting(column)
	
		case column.name
			
		when 'log_sound'
			{}
			
		when 'log'
			{:order => 'name', :direction => toggle_sort_direction('name')}
		
		when 'events'
			{:order => 'log.events_count', :direction => toggle_sort_direction('log.events_count', 'desc')}
			
		when 'rating'
			{:order => 'rating', :direction => toggle_sort_direction('rating', 'desc')}
		
		else
			super
		
		end
  end
  
  def sub_menu
	  element = [];
	  
	#  case params[:action]
	#  
	#  when 'show'
	#	# TODO: the 'show' action should show a different menu
	#	
	#  else
	#	element.push({:title => "New Log", :path => "logs/new"});
	#	
	#  end
  
	  return element;
  end	
  
end
