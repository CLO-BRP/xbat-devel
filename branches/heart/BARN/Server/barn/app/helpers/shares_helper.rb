module SharesHelper
  
  def columns_default
		
      columns = super

      columns.concat ListingColumn.assemble(%w[position file bytes format channels samplerate duration created])
		
  end
  
  def column_markup (row, column, recording)
	
		case column.name
		
		when 'position'
			recording.recording_sounds.detect{|r| r.recording_id == recording.id }.position.to_s
		
		when 'samplerate'
			rate_to_hertz(recording.samplerate)
			
		when 'duration'
			sec_to_clock(recording.samples.to_f / recording.samplerate.to_f) unless recording.samples.blank? || recording.samplerate.blank?
		
		else
			super
			
		end
	
  end
  
  def column_sorting(column)
	
		case column.name
			
		when 'duration'
			{:order => 'samples', :direction => toggle_sort_direction('samples')}

		else
			super
		
		end
		
  end
  
end