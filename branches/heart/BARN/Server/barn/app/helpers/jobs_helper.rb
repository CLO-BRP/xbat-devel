module JobsHelper

	def columns_default

		columns = super

		#if logged_in?
		#	columns.concat ListingColumn.assemble(%w[name priority progress started_at])
		#else
		#	columns.concat ListingColumn.assemble(%w[name priority progress started_at])
		#end

		columns.concat ListingColumn.assemble([
			{:name => 'name', :width => '25%'},
			{:name => 'priority', :width => '7%'},
			{:name => 'progress', :width => '45%'},
			{:name => 'started_at', :width => '15%'}
		]);
	end

	def column_markup(row, column, job)

		case column.name

		when 'started_at'
			job.started_at.nil? ? "Not started yet" : "Started #{time_ago_in_words(job.started_at)} ago."

		when 'progress'
			job.progress.nil? ? "No progress." : progress_display(job.progress)

		else
			super

		end
	end

	def column_sorting (column)
		params = {}
		
		case column.name 
		
		when 'log'
			params = {:order => 'log.name', :direction => toggle_sort_direction('log.name')}
		
		else
			return super
		end
		
		return params
	end

	#def post_row( options, columns, row, object )
	#	
	#	html = ""
	#	
	#	if options[:expansion] == 1
	#		
	#		object.children.each { |child|
	#			html << "<tr style='display: none; border-top: none;' class='#{row[:handle]}  #{row[:state]}'>"
	#			html << "<td></td><td></td>"
	#			html << "<td class='name'>" + child.name.to_s + "</td><td>"
	#			html << "<td>"
	#			html << progress_display ( child.progress.nil? ? 0 : child.progress )
	#			html << "</td>"
	#			html << "<td></td>"
	#			html << "</tr>"
	#		}
	#		
	#		html
	#	end
	#end

	def progress_display ( progress )
	
		# TODO: this could also use color to give an indication of 'doneness' at a glance
		
		progress_display = progress * 3
		
		"<div class='progressbar'><div class='progressbar_complete' style='width:#{progress_display}px;'><span class='progressbar_info'>#{progress}&nbsp;%</span></div></div>"
	end
  
	def sub_menu
		
		return [];
	end	
  
end