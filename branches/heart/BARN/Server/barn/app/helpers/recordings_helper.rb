module RecordingsHelper
  
  def columns_default

	columns = ListingColumn.assemble([
		{ :name => 'selection', :width => '20px', :header => true },
	])	
	
	if params.include? :sound_id
	  columns.concat ListingColumn.assemble([
		{:name => 'position', :width => '4%'}
	  ]);
	end
	
	columns.concat ListingColumn.assemble([
	  {:name => 'name', :width => '25%'},
	  {:name => 'events', :width => '4%'},
	  {:name => 'duration', :width => '12%'},
	  {:name => 'samplerate', :width => '12%'},
	  {:name => 'channels', :width => '4%'},
	  {:name => 'size', :width => ''}
	])
	
  end
  
  def column_markup (row, column, recording)
		
		case column.name
		  
		when 'sounds'
			html = ""
			
			recording.sounds.map{ |sound| 
				positions 		= recording.recording_sounds.find_all{|rs| rs.sound_id == sound.id}.map{|rs| rs.position}
				positions_txt = (positions.blank?) ? ": (#{positions.join(', ')})" : ""
				#sound.name
				html << link_to( sound.name, sound_url(sound) ) << positions_txt
			}
			
			return html
			
		when 'name'
			link_to(recording.name, recording_path(recording))
			
		when 'events'
			recording.events.count
			
		when 'file'
			html = link_to(recording.file, (recording.location || recording.public_filename))
				
			#html << "<ul class='graphic'><li>" + link_to('PLAY', recording.location, :class => 'render-inline-player') + "</li></ul>"
			
		when 'position'
			recording.recording_sounds.detect{|r| r.recording_id == recording.id }.position.to_s
			
		when 'bytes', 'size'
			size_to_bytes recording.bytes
			
		when 'samplerate'
			rate_to_hertz(recording.samplerate)
			
		when 'duration'
			sec_to_clock(recording.samples.to_f / recording.samplerate.to_f) unless recording.samples.blank? || recording.samplerate.blank?
			
		else
			super
		end
		
  end
  
  def column_sorting(column)
		
		case column.name
			when 'position'
				{:order => 'recording_sound.position', :direction => toggle_sort_direction('recording_sound.position')}
				
			when 'size'
				{:order => 'bytes', :direction => toggle_sort_direction('bytes')}
				
			when 'duration'
				{:order => 'samples', :direction => toggle_sort_direction('samples')}	
				
			else
				super
		end
		
  end
  
  def sub_menu
	  return []
	#  return [
	#	  {:title => "New Recording", :path => "recordings/new"}
	#  ]
  end	
  
end