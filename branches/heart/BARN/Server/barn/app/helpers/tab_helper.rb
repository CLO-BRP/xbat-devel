module TabHelper

	def renderCountLabel( title, count )
		title + " (<span class='tab_count_filtered'>#{count} of </span>#{count})"
	end

	def addTab ( label, link, options )
		html = "<li>"
		html << link_to ( label, link, options )
		html << "</li>"
	end

end