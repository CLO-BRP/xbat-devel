module FileResourcesHelper

	def columns_default
      columns = super
      
      columns.concat ListingColumn.assemble(%w[file caption size])
	end


	# TODO: add number of members column to project

	def column_markup (row, column, fileresource)

		case column.name

		when 'file'
			link_to(fileresource.resource_file_name, fileresource.resource.url )
			
		when 'bytes', 'size'
			size_to_bytes fileresource.resource_file_size
			
		when 'caption'
			if fileresource.caption.blank?
				'No caption available'
			else
				fileresource.caption
			end

#			str	= '<b>%s %s</b> %s<br/><div style="display:none;margin: 0.75em 0em 1em"></div>'
#
#			filename = link_to(h(fileresource.resource_file_name), fileresource.resource_file_name)
#
#			return sprintf(str, filename )

		else
			super

		end
	end


	def column_sorting (column)

		case column.name

		when 'file'
			{:order => 'resource_file_name', :direction => toggle_sort_direction('resource_file_name')}

		when 'bytes', 'size'
			{:order => 'resource_file_size', :direction => toggle_sort_direction('resource_file_size')}
			
		when 'caption'
			{:order => 'caption', :direction => toggle_sort_direction('caption')}

		else
			return super

		end
	end

end
