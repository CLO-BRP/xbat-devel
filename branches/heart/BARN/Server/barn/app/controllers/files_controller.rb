require 'fileutils'
require 'filebrowser'

class FilesController < ApplicationController
  before_filter :login_required
	#before_filter :has_permission?
	
  def import
  end
  
	# def index
	# 		dirname = params[:dirname] || ''
	# 		
	# 		page = params[:page] = params[:page] || 1
	# 		
	# 		per_page = params[:per_page] = params[:per_page] || 10
	# 		
	# 		
	# 		begin
	# 			file_browser = FileBrowser.new(File.join(User.current.ftp_root, dirname), params)
	# 		rescue Errno::ENOENT => e
	# 		  # Return a 404 if the dirname is invalid
	# 			resource_access_denied :status => 404
	# 			return
	# 		end
	# 		
	# 		# Filter, sort, and page the collection
	# 				
	# 		files = file_browser.filter( params[:query] ).sort( params[:order], params[:direction] ).page( page )
	# 		
	# 		# Wrap the files in a WillPaginate::Collection instance for display
	# 		
	# 		@files = WillPaginate::Collection.create(page, file_browser.per_page, file_browser.total_entries) do |pager|
	# 			pager.replace(files)
	# 		end
	# 		
	# 		@files.each{|f| f.id = "x" }
	# 		
	# 		@metadata = {
	# 			:page => page, 
	# 			:per_page => file_browser.per_page, 
	# 			:total_entries => file_browser.total_entries
	# 		}
	# 						
	# 		render_action_or_partial
	# 	end
	# 	
	
	def index		
		dirname = params[:dirname] || ''
		page = params[:page] = params[:page] || 1
		per_page = params[:per_page] = params[:per_page] || 10
		
		begin
			if params[:project_id]
				@project = Project.find(params[:project_id])
				file_browser = FileBrowser.new(File.join(@project.files_root, dirname), params)
			else
				file_browser = FileBrowser.new(File.join(User.current.ftp_root, dirname), params)
			end

			# Filter, sort, and page the collection
			files = file_browser.filter( params[:query] ).sort( params[:order], params[:direction] ).page( page )

			# Wrap the files in a WillPaginate::Collection instance for display

			@files = WillPaginate::Collection.create(page, file_browser.per_page, file_browser.total_entries) do |pager|
				pager.replace(files)
			end

			@files.each{|f| f.id = "x" }

			@metadata = {
				:page => page, 
				:per_page => file_browser.per_page, 
				:total_entries => file_browser.total_entries
			}
		
			render_action_or_partial #response_for( @projects, {:metadata => @metadata}.merge(params) )	
			
		rescue Errno::ENOENT => e
			barn_logger e
			render :text => "Sorry, #{e}"
			# Return a 404 if the dirname is invalid
			#resource_access_denied :status => 404
			#return
		end
	end
	
end
