# NOTE: The 'edit' action has been removed since it has not relevance in the context of the api
# additionally, all requests for HTML responses have been removed

class EventsController < ApplicationController
	
include AuthorizationApocalypse::Gatekeeper

before_filter :login_required, :except => [:index, :show]

before_filter :has_permission?

# GET /events

# TODO: document the family of requests handled here
  
def index
    
    # user = (logged_in? && params[:public].blank?) ? User.current : nil
		
	# NOTE: we check for a report partial and carry out required query before moving on
	
	case params['partial']
	
	when 'widgets/histogram'

		if !params[:sound_id].nil?
			
			target_sound_id = params[:sound_id]

		elsif !params[:log_id].nil?
			
			log = Log.find(params[:log_id])

			target_sound_id = log.sound_id
		end

		if !target_sound_id.nil?

			sound = Sound.find( target_sound_id )

			# TODO: we should be able to send specific histogram parameters here!!
			#-----------------------------------------------------------------------------------------
			
			hist_params = { :variable => 'start', :min => 0, :max => sound.duration,  :bins => 100, :query => params[:query] }

			histogram = Event.get_histogram_for_user( hist_params, User.current, params )

			params[:metadata] = hist_params
			
			params[:name] = 'histogram'
			
			response_for(histogram, params)
		end

    when 'tags/tag_cloud'

		@tags = Event.paginate_by_user_with_metadata( User.current, params )[:tags]

		response_for( @tags, params )

	else
		if params[:order].blank?
			
			params[:order] = 'score'; params[:direction] = 'desc';
		end

		results = Event.paginate_by_user_with_metadata( User.current, params )

		@events = results[:collection]
		
		@metadata = results[:metadata]

		# NOTE: our named-scope computes these for us so that we can display a tag-cloud
		
		@tags = results[:tags]
		
		#---------------------
		
		if !params[:confirm].blank?
			
			filter_tag = get_filter_tags(params[:query]);
			
			# NOTE: we probably don't have to compute everything, however this is very simple
			
			['unconfirmed', 'accepted', 'rejected'].each do |key|
				
				@metadata[key.to_sym] = Event.with_parent_in_set(params).filtered_by(params.merge({:query => "#{key}:#{filter_tag}"}), current_user).count;
			end
		end
	
	
		if TagSet.count == 0
		  
			@metadata['tag_set'] = nil;
		else
			# TODO: we need to figure out what to do here
			
			sets = TagSet.find(:all)
			
			if !params[:tag_set].blank?
				tag_set = TagSet.find( params[:tag_set] )
			else
				tag_set = TagSet.find(:first)
			end
		  
			@metadata['tag_set'] = { :name => tag_set.name, :tags => tag_set.tag_list, :all => sets };
		end
		
		response_for( @events, {:metadata => @metadata}.merge(params) )	
	end

    return
    
    # NOTE: the code below, is 'traditional' code for a controller like this

    #--
    # handle include, sorting, and nested resource conditions
    #--
    
    # NOTE: the above are linked to the extent that some sorting requires includes
    
    if params[:include].blank?
      include = Set.new;
      
    else
      include = Set.new params[:include].split(',').map!{|s| s.strip}
      
    end
    
    if params[:order].blank?
	  params[:order] = 'score'; params[:direction] = 'desc'; order = "score desc"
      
    elsif params[:order].split('.').size > 1
      order = "#{params[:order]} #{params[:direction]}"
      
    else
      order = "#{Event.table_name}.#{params[:order]} #{params[:direction]}"
      
    end
    
    conditions = {}; joins = {}
    
    if !params[:sound_id].blank?
      conditions = 'log.sound_id =' + params[:sound_id]; include.add 'log'
      
      joins = 'INNER JOIN log ON log.id = event.log_id'
            
    elsif !params[:log_id].blank?
      conditions = 'event.log_id = ' + params[:log_id];
      
    end

    #--
    # find events with pagination
    #--
    
    include = (include.length == 0)? nil : include.to_a
    
    if params[:format] == "csv"
      
      @events = Event.readable_by( User.current || nil ).filtered_by(params).find(
        :all, :order => order, :conditions => conditions, :include => include
      )
       
      @metadata = {
        :total_entries => Event.readable_by( User.current || nil ).filtered_by(params).count(:all, :conditions => conditions, :include => include)
      }
        
    else
            
      # TODO: remove non-associated includes
 
      @events = Event.readable_by( User.current || nil ).filtered_by(params).paginate(
        :page => params[:page],
        :per_page => params[:per_page] || Event.per_page,
        :order => order,
        :conditions => conditions,
        :include => include
      )
            
      @metadata = {
        :page => params[:page],
        :per_page => params[:per_page] || Event.per_page,
        :total_entries => Event.readable_by( User.current || nil ).filtered_by(params).count(:all, :conditions => conditions, :include => include)
      }
                 
      @tags = Event.tag_counts(
        :conditions => conditions,
        :joins => joins
      )
    end
    
    #--
    # respond to request according to format
    #--
    
    if !params[:log_id].blank?
      locals = {
        :log => Log.find(params[:log_id])
      }
    
    elsif !params[:sound_id].blank?
      locals = {
        :sound => Sound.find(params[:sound_id])
      }
      
    end

    response_for(@events, {:metadata => @metadata, :locals => locals}.merge(params))
      
  end


  # GET /events/1

  def show
	@event = Event.find(params[:id])
	
	response_for( @event, params )    
  end


  # GET /events/new
  
  def new
    @event = Event.new

    respond_to do |format|
      format.html { render :text => "I'm a teapot.", :status => 418 }
      format.xml { render :text => @event.to_xml( :dasherize => false ) }
      format.any(:js, :json)  { render :json => @event }
    end
  end

  
  # POST /events
  
  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        # XML
        format.xml do 
          render :xml => @event, :status => :created, :location => @event
        end
        # JS/JSON
        format.any(:js, :json) do 
          render :json => @event, :status => :created, :location => @event 
        end
        format.html { render :text => "I'm a teapot.", :status => :im_a_teapot }
      else
        format.xml { render :xml => @event.errors, :status => :unprocessable_entity }
        format.any(:js, :json){ render :json => @event.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # PUT /events/1
  
  def update
    @events = Event.in_set(params[:id])

	success = false
	@events.each do |event|
		success = event.update_attributes(params[:event])
	end

	statusText=""

	# if :noview is not specified
	if (params[:noview] && params[:noview] == 'true')
		 render :text => statusText
	else
		if success
			response_for( @events, params )
			
			#format.xml { render :xml => @events }
			#format.any(:js, :json) { render :json => @events,  }
		else
			
			respond_to do |format|
				format.xml { render :xml => @events.errors, :status => :unprocessable_entity }
				format.any(:js, :json) { render :json => @events.errors, :status => :unprocessable_entity }				
			end
		end
	end
  end

	#PUT /events/{id}/review
	def update_review
		@event = Event.find_by_id( params[:id])
		@event.update_attributes(params[:event])

		render :partial => "event_review", :locals => { :event => @event, :user => User.current }
	end

  
  # DELETE /event/1
  
  def destroy
    @events = Event.in_set(params[:id])

	@events.each do |event|
		event.destroy
	end

    #@events.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.xml { head :ok }
      format.any(:js, :json)  { head :ok }
    end
  end
  
end
