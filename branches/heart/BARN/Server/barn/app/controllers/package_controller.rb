require 'zip/zipfilesystem'

class PackageController < ApplicationController

  def upload
    # this will receive a package uploaded by the user.
    # it will also receive the user id as part of the session info.
    # the package is typically a zip file, containing a manifest called package.yml
    # package.zip includes information about what kind of package it is.
    # once this is determined, a file location will be assigned, and the file will be saved there.
    # any appropriate records will also be created in the database.

    targetfile = RAILS_ROOT + "/public/files/" + "mypackage.zip"

    xyzzy = nil

    Zip::ZipFile.open( targetfile ) { |zipfile|
        raw =  zipfile.file.read("package.yml")
        xyzzy = YAML.load( raw )
    }


    x = 1

  end

end