class ReviewsController < ApplicationController

	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required, :except => [:index, :show]

	before_filter :has_permission?



	# GET /parent/1/reviews

	def index
		# NOTE: we do not limit the sounds to user-owned, we may have project access

        user_id = (logged_in? && params[:public].blank?) ? User.current.id : 0

#		results = if requested_through_parents?
			results = Review.paginate_with_metadata( params )
#		else
#			results = Review.paginate_by_user_with_metadata( User.current, params )
#		end

		@reviews  = results[:collection]

		@metadata = results[:metadata]

		@tags     = results[:tags]


		response_for( @reviews, {:metadata => @metadata}.merge(params) )
	end


	def show
		@review = Review.find(params[:id])

		response_for( @review, params )
	end

	# GET /review/:id/edit

	def edit
		@review = Review.find(params[:id])

		respond_to do |format|
			format.html
		end
	end


	def create

		parent_type, parent_id = getParentFromParamType
		object_type = "Event"

		tag = Tag.new( :name => params["add_review"] )
		tag.save
		tag = Tag.find( :first, :conditions => { :name => params["add_review"] } )
		

		@review = Review.find(:first, :conditions => { :tag_id => tag.id, :parent_id => parent_id, :parent_type => parent_type, :object_type => object_type })

		if ( @review.nil? ) then
			@review = Review.new( params[:review] )
			@review.parent_type = parent_type.to_s.capitalize
			@review.parent_id = parent_id
			#TODO this must be made dynamic in the future.
			@review.object_type = object_type
			@review.tag_id = tag.id

		end

		@review.description = params["review_short"]
		
		@review.long_description = params["review_long"]

		@review.save

#		@review.notes.each() { |n|
#			n.destroy
#		}

#		if ( @review.id.nil? ) then
#		    @review = Review.find(:first, :conditions => { :tag_id => tag.id, :parent_id => parent_id, :parent_type => parent_type, :object_type => object_type })
#		end

#		@review.add_notes_by_user( {:title => tag.name, :body => params["review_text"]}, User.current )

		flash[:notice] = "Review Saved"

		respond_to do |format|
			format.html{
				redirect_back_or_default("/#{parent_type.pluralize}/#{parent_id}/reviews")
			}

			format.xml{ render :xml => @review }

			format.js{ render :json => @review }
		end

		return
	end

	# PUT /review/:id
	# PUT /review/{:id}


#	def update
#		@reviews = Review.in_set(params[:id])
#
#		success = false
#
#		@review.each do |review|
#			success = review.update_attributes(params[:review])
#		end
#
#		respond_to do |format|
#			if success
#				format.html {
#					flash[:notice] = "Review saved sucessfully"
#
#					redirect_to review_path(@reviews.first)
#				}
#
#				format.xml { render :template => 'review/show.xml.builder', :status => :ok }
#
#				format.js { render :json => @reviews, :status => :ok }
#
#				format.json { render :json => @reviews, :status => :ok }
#			else
#				format.html {
#					flash[:error] = "The was a problem saving your record"
#
#					render :action => 'edit' # presumably we came from the edit action
#				}
#
#				format.xml { render :xml => @reviews.map{|s| s.errors}, :status => :unprocessable_entity }
#
#				format.js { render :json => @reviews.map{|s| s.errors}, :status => :unprocessable_entity }
#
#				format.json { render :json => @reviews.map{|s| s.errors}, :status => :unprocessable_entity }
#			end
#		end # respond_to
#
#	end # update

	# DELETE /review/:id
	# DELETE /review/{:id}

	def destroy
		# NOTE: we only destroy the user sound association not the sound resource

		@reviews = Review.in_set(params[:id])

		@reviews.each do |review|
			review.destroy
		end

		respond_to do |format|
			format.html { redirect_to reviews_url }

			format.xml { head :ok }

			format.any(:js, :json)	{ head :ok }
		end
	end

end
