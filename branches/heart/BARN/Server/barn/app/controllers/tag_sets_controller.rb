class TagSetsController < ApplicationController
	
	include AuthorizationApocalypse::Gatekeeper

	before_filter :login_required, :except => [:index, :show]

	before_filter :has_permission?

    def index
		# NOTE: we do not limit the tag_sets to user-owned, we may have project access
		
		# TODO: there are two contexts for index, a user's and a project's
		
		results = TagSet.paginate_with_metadata(params)
		
		@tag_sets = results[:collection]
		
		@metadata = results[:metadata]
		
		@tags = results[:tags]
		
		response_for( @tag_sets, {:metadata => @metadata}.merge(params) )
    end


#	def new
#		@tag_set = TagSet.new
#
#		response_for( @tag_set, params )
#	end

	def create
		@tag_set = TagSet.new(params[:tag_set])

    @tag_set.context_id = (params[:type] == 'project') ? params[:project_id] : current_user.id

    @tag_set.context_type = params[:type].capitalize

		@tag_set.user = current_user

    @tag_set.save


    render :nothing => true
#		respond_to do |format|
#      format.html { redirect_back_or_default("#{@tag_set.context_type}/#{@tag_set.context_id}/tag_sets/") }
#      format.html { redirect_to("/#{params[:type]}s/#{@tag_set.context_id}/tag_sets/") }

      #format.xml	{ render :template => 'tag_sets/show.xml.builder', :status => :created }

#      format.any(:js, :json) { render :json => @tag_set, :status => :created, :location => @tag_set }
#		end
  end

	# GET /tag_set/:id/edit

	def edit
		@tag_set = TagSet.find(params[:id])

		respond_to do |format|
			format.html
			# is this method really needed in other formats??
		end
	end


	# PUT /tag_set/:id
	# PUT /tag_set/{:id}

	def update
		@tag_sets = TagSet.in_set(params[:id])

		success = false

		@tag_sets.each do |tag_set|
			tag_set.user = current_user
			
			success = tag_set.update_attributes(params[:tag_set])
		end

		respond_to do |format|
			
			if success
				format.html {
					flash[:notice] = "Tag Set saved sucessfully"

					redirect_to tag_set_path(@tag_sets.first)
				}

				#format.xml { render :template => 'tag_sets/show.xml.builder', :status => :ok }

				format.js { render :json => @tag_sets, :status => :ok }

				format.json { render :json => @tag_sets, :status => :ok }
			else
				format.html {
					flash[:error] = "The was a problem saving your record"

					render :action => 'edit' # presumably we came from the edit action
				}

				format.xml { render :xml => @tag_sets.map{|s| s.errors}, :status => :unprocessable_entity }

				format.js { render :json => @tag_sets.map{|s| s.errors}, :status => :unprocessable_entity }

				format.json { render :json => @tag_sets.map{|s| s.errors}, :status => :unprocessable_entity }
			end
		end # respond_to

	end # update

	# DELETE /tag_set/:id
	# DELETE /tag_set/{:id}

	def destroy

		@tag_sets = TagSet.in_set(params[:id])

		respond_to do |format|
			format.html { redirect_to tag_sets_url }

			format.xml { head :ok }

			format.any(:js, :json)	{ head :ok }
		end
	end

end