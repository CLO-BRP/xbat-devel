# NOTE: The 'edit' action has been removed since it has not relevance in the context of the api
# additionally, all requests for HTML responses have been removed

class AppsController < ApplicationController

  # GET /apps
  
  # We should not allow access to all apps as a single recordset as a call to index would imply. Bad.
  # Instead, lets just callthe show method with some reasonable recordset since paging is done there.
  
  def index  
    params[:id] = '1-20'
    show
  end

  def yui
    render :layout => false
  end
  
  def iphone
  end
  
end
