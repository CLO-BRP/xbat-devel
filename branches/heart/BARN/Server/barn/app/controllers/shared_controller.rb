class SharedController < ApplicationController
	include AuthorizationApocalypse::Gatekeeper
	
	before_filter :login_required
	before_filter :has_permission?

	# GET /shares
	
	def index	
		params[:id] = 'inbox'
		show
	end
	
	def show
		if parent_resource_class
			object = parent_resource_class.find(parent_id)
			# get the instance of the shared object
			@shared	 = Shared.find(:first, :conditions => ["shareable_id = ? and shareable_type = ?", object.id, object.class.to_s])			
			@sharings = Sharing.find(:all, :conditions => "shared_id = #{@shared.id} and user_id = #{User.current.id}", :include => :user) if @shared
			#@sharings = @shared.sharings # -> this causes some weird stack level too deep
			shares = @sharings.collect{ |sharing| sharing.user }
		else
			if params[:id] == 'inbox'
				@sharings = Sharing.find(:all, :conditions => "recipient_id = #{User.current.id}", :include => :user)
			end
			if params[:id] == 'outbox'
				@sharings = Sharing.find(:all, :conditions => "user_id = #{User.current.id}", :include => :user)
			end
			shares = @sharings.collect{ |sharing| sharing.shared }
		end
		

		
		respond_to do |format|
			format.xml { render :xml => shares.to_xml(:root => 'shares') }
			format.js	{ render :json => @sharings }
		end
	end
	
	
	def new
		if parent_resource_class
			@shareable = parent_resource_class.find(parent_id)
		else
			flash[:error] = "For the moment, sharing is done through a parent object."
			redirect_to "/"
		end
	end
	
	# POST shares
	
	def create
		if parent_resource_class
			@shareable = parent_resource_class.find(parent_id)
		else
			flash[:error] = "For the moment, sharing is done through a parent object."
			redirect_to "/" and return
		end
		
		# user to share this with
		@user = User.find_by_login_or_email(params[:login_or_email])
		if @user.blank? or @user == User.current
			flash[:error] = "Sorry, no user by that name." if @user.blank?
			flash[:error] = "You want to share with yourself? nicer." if @user == User.current
			render :action => "new" and return
		end
		
		# SHAREABLE
		if @shareable.is_shareable_by_user?(User.current)
			@sharing = @shareable.share :with => @user, :from => User.current

			flash[:notice] = "This #{@shareable.class.to_s} has been shared. Thanks for sharing."
			respond_to do |format|
				format.html { redirect_to polymorphic_url(@shareable) }
				format.xml	{ render :xml => @sharing, :status => :created }
				format.any(:js, :json) { render :json => @sharing, :status => :created }
			end
			return
		end
		
		# NOT SHAREABLE
		flash[:error] = 'Error'
		respond_to do |format|
			format.html { redirect_to request.request_uri }
			format.xml	{ render :status => :unprocessable_entity }
			format.any(:js, :json) { render :text => '', :status => :unprocessable_entity }
		end
	end
	
	def destroy
		if parent_resource_class
			@shareable = parent_resource_class.find(parent_id)
		else
			flash[:error] = "For the moment, sharing is done through a parent object."
			redirect_to "/" and return
		end
		
		# SHAREABLE
		if @shareable.is_shareable_by_user?(User.current)
			@sharing = Sharing.find(params[:id])
			@sharing.destroy
		
			respond_to do |format|
				format.html { redirect_to polymorphic_url(@shareable) }
				format.xml	{ render :xml => @sharing, :status => :created }
				format.any(:js, :json) { render :json => @sharing, :status => :created }
			end
			return
		end
		
		# NOT SHAREABLE
		flash[:error] = 'Error'
		respond_to do |format|
			format.html { redirect_to polymorphic_url(@shareable) }
			format.xml	{ render :status => :unprocessable_entity }
			format.any(:js, :json) { render :text => '', :status => :unprocessable_entity }
		end
	end
	
	
	
end