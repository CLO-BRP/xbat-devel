require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SyncController do

	describe "routes" do
		it "should generate params for #pull" do
	    params_from(:get, "/sync/request").should == {:controller => "sync", :action => "pull_requester"}
	  end
	
		it "should generate params for #push" do
	    params_from(:put, "/sync/request").should == {:controller => "sync", :action => "push_requester"}
	  end

		it "should generate params for #pull" do
	    params_from(:get, "/sync").should == {:controller => "sync", :action => "pull_responder"}
	  end
	
		it "should generate params for #push" do
	    params_from(:put, "/sync").should == {:controller => "sync", :action => "push_responder"}
	  end
	end
	
	describe "GET" do
		
		# before :all do
		# 	User.destroy_all
		# 	@user = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
		# 	@sound = default_sound
		# end
		# 
		# before :each do
		# 	request.env["HTTP_HOST"] = "localhost"
		# 	login_as @user
		# end
		# 
		# it "should log the requesting peer" do
		# 	Peer.count.should == 0
		# 	get :pull
		# 	
		# 	response.headers["Status"].should =~ /200/ # Success
		# 	Peer.count.should == 1
		# 	PeerUser.count.should == 1
		# 	Sync.count.should == 1
		# end
	end
	
end