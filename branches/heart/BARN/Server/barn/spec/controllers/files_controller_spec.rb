require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe FilesController do
	
	before(:all) do
		@@path = File.join(RAILS_ROOT, 'spec', 'files', 'browser')
  end

	before :each do 
		@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
		login_as @bill
	end
	
	describe "filesystem" do
		before :each do
			mock_file_browser = FileBrowser.new(@@path)
			FileBrowser.stub!(:new).and_return(mock_file_browser)
		end
		
		it "should have a folder of files" do
			get :index
			response.headers["Status"].should =~ /200/ # OK
      
			# Make sure all the values are assigned as expected.

			assigns[:files].size.should == 10	# default paging
			assigns[:metadata][:per_page].should == 10
			assigns[:metadata][:total_entries].should == 13
		end
	end
	
end