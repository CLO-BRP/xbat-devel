require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe LogsController do

	fixtures :user, :sound, :log
	
	describe "GET" do 
		integrate_views
		
		before :each do
			login_as(:default)
		end
		
		%w{json xml html}.each do |format|
		
			it "should be success as a resource" do
				get :index, :format => format
				
				response.headers["Status"].should =~ /200/ # OK
			end
			
			it "should be success as a nested Sound resource" do
				get :index, :sound_id => sound(:sound_1).id, :format => format
				
				response.headers["Status"].should =~ /200/ # OK
			end
		
		end
	end

# describe "responding to DELETE destroy" do
# 
#   it "should destroy the requested event" do
#     Event.should_receive(:find).with("37").and_return(mock_event)
#     mock_event.should_receive(:destroy)
#     delete :destroy, :id => "37"
#   end
# 
#   it "should redirect to the event list" do
#     Event.stub!(:find).and_return(mock_event(:destroy => true))
#     delete :destroy, :id => "1"
#     response.should redirect_to(events_url)
#   end
# 
# end

end
