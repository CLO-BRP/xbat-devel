=begin

  Tests for the SoundsController class.
  It uses fixtures to test against data in the database. 
  RSpec recommends doing this sort of testing in isolation, using mocks, 
  but for the moment, this is easy and reliable for testing conditions of use.
  
  In the future if this gets difficult to maintain, it may well be worth looking at Mocks

=end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SoundsController do

  
  # Tests for GET requests to the SoundsController 
  describe "GET" do 
    integrate_views
    
    before(:each) do
      #Sound.destroy_all
      SoundUser.destroy_all
      
			@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
			@ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
			
			# Make 3 sounds upon which to test
			@bills_public_sound  = default_sound(:name => "Bill's Public Sound", :public => 1, :user_id => @bill.id)
			@bills_private_sound = default_sound(:name => "Bill's Private Sound", :public => 0, :user_id => @bill.id)
	    @teds_private_sound   = default_sound(:name => "Ted's Private Sound", :public => 0, :user_id => @ted.id)
	    
	    login_as @bill
		end
    
    %w{js xml html}.each do |format| # loop through all the expected response formats
      # Test all of the basic GET accessible methods to make sure nothing is broken.
      it "/sounds be success" do
        get :index, :format => format
        response.headers["Status"].should =~ /200/ # OK
      end
      
      it "/sounds/{id} should be success" do
        get :show, :id => @bills_public_sound.id, :format => "html"
        response.headers["Status"].should =~ /200/ # OK
      end

      it "/sounds/new should be success" do
        get :new, :format => format
        response.headers["Status"].should =~ /200/ # OK
      end
      
      it "/sounds/{id}/edit should be success" do
        get :edit, :id => @bills_public_sound.id, :format => format
        if format == "html"
          response.headers["Status"].should =~ /200/ # OK
        else
          response.headers["Status"].should =~ /406/ # Unacceptable - there is no reason to respond to anything but HTML
        end
      end
    end
    
  end

=begin
      # Test the specifics of the GET accessible methods
      # it "/sounds should return only the sounds that the logged in user owns" do 
      #   get :index, :format => format
      #   assigns[:sounds].each{|sound| sound.user_id.should == user(:default).id} 
      #   assigns[:sounds].size
      # end
      
      it "/sounds should paginate correctly" do 
        Sound.per_page = 1
        get :index, :format => format
        assigns[:sounds].size.should equal(1)
        
        get :index, :format => format, :page => 2
        assigns[:sounds].size.should equal(1)
        
        Sound.per_page = 10
        get :index, :format => format, :page => 1
        # default user only has 2 sounds created
        assigns[:sounds].size.should equal(2)
        get :index, :format => format, :page => 2
        assigns[:sounds].size.should equal(0)
      end
      
      it "/sounds should sort correctly" do 
        get :index, :format => format, :order => "name", :direction => "asc"
        
        # We are logged in as Quentin... thus we only get his sounds...
        assigns[:sounds].collect{|s| s.name}
        assigns[:sounds][0].name.should == sound(:two).name

        get :index, :format => format, :order => "name", :direction => "desc"
        # this is the second sound owned by default - if the fixture changes, it could break this
        assigns[:sounds][0].name.should == sound(:four).name
        
        # RATING!
        get :index, :format => format, :order => "rating", :direction => "desc"
      end
      
    end
  end # GET
  
  # PUT 
  describe "PUT" do
    
    %w{json xml}.each do |format|
      it "/sounds/{id} should always require a login!" do
        put :update, :id => sound(:one).id, :sound => {:name => 'another name'}, :format => format
        response.headers["Status"].should =~ /401/ # Unauthorized
      end
    end
    
    describe "/sounds/{id} with valid params" do
      before :each do
        login_as(:quentin)
      end
      
      %w{json xml html}.each do |format|
        it "should update the requested sound" do
          # TODO: stub the request with an xml data body to simulate cURL requests
          # request.stub!( :raw_post ).and_return @xml
          put :update, :id => sound(:two).id, :sound => {:name => 'something else'}, :format => format
          assigns[:sounds].first.name.should == 'something else'
          
          put :update, :id => sound(:two).id, :sound => {:name => 'again'}, :format => format
          assigns[:sounds].first.name.should == 'again'
          
          if format == 'html'
            response.headers["Status"].should =~ /302/ # Moved (Redirect)
          else
            response.headers["Status"].should =~ /200/ # Ok
          end
        end
  
        it "should return the updated representation of itself" do
          #Event.stub!(:find).and_return(mock_sound(:update_attributes => true))
          put :update, :id => sound(:two).id, :sound => {:name => 'something else'}, :format => format
          # we are not using the 'integrate_views' command so the view is not rendered..
          # perhaps we can test for the expected content of the view later
          response.body.should == "sounds/show.xml.builder" if format == "xml"
          # TODO: test of JSON response view

          if format == 'html'
            response.should redirect_to(sound_url(sound(:two))) if format == 'html'
            response.headers["Status"].should =~ /302/ # Moved (Redirect)
          else
            response.headers["Status"].should =~ /200/ # Ok
          end
        end
        
        it "should update a user's rating" do
          @sound = sound(:two)
          # This test should maybe be put into the acts_as_rateable tests
          put :update, :id => sound(:two).id, :sound => {:rating => '5.0'}, :format => format
          @sound.reload
          @rating = @sound.rating_by_user(user(:quentin))
          @rating.rating.should == 5
          
          put :update, :id => sound(:two).id, :sound => {:rating => '4.0'}, :format => format
          @sound.reload
          @rating = @sound.rating_by_user(user(:quentin))
          @rating.rating.should == 4
        end
      end # each format...
      
    end # "/sounds/{id} with valid params"
    
    describe "with invalid params" do
      # Write this
    end
  end
=end
  # DELETE
  describe "DELETE" do
		
		before(:each) do
      Sound.destroy_all
      SoundUser.destroy_all
      
			@bill  = default_user(:login => 'bill', :email => 'bill.s.preston.esquire@gmail.com')
			@ted   = default_user(:login => 'ted', :email => 'ted.theodore.logan@gmail.com')
			
			# Make 3 sounds upon which to test
			@bills_public_sound  = default_sound(:name => "Bill's Public Sound", :public => 1, :user_id => @bill.id)
			@bills_private_sound = default_sound(:name => "Bill's Private Sound", :public => 0, :user_id => @bill.id)
	    @teds_private_sound   = default_sound(:name => "Ted's Private Sound", :public => 0, :user_id => @ted.id)
	    
	    login_as @bill
		end
		
		
		%w{js json xml html}.each do |format| 
			it "should allow for deletion of a single sound" do
				SoundUser.count.should == 3
				
				delete :destroy, :id => @bills_public_sound.id, :format => format
				
				response.headers["Status"].should =~ /200/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
				# Make sure we delete the relation, not the sound
				SoundUser.count.should == 2
				Sound.count.should == 3
			end
			
			it "should allow for deletion of a set of sounds" do
				delete :destroy, :id => "{#{@bills_public_sound.id},#{@bills_private_sound.id}}", :format => format
				
				response.headers["Status"].should =~ /200/ if format != "html"# OK
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
				
				SoundUser.count.should == 1
				Sound.count.should == 3
			end
			
  		it "should NOT allow for deletion of a set of sounds with mixed access" do
				delete :destroy, :id => "{#{@bills_public_sound.id},#{@teds_private_sound.id}}", :format => format
				# Sound.count.should == 4
				response.headers["Status"].should =~ /409/ if format != "html"# Conflict
				response.headers["Status"].should =~ /302/ if format == "html"# Redirect
				
				SoundUser.count.should == 3
				Sound.count.should == 3
			end
		end
		
	end

end
