require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe "tag extensions" do


	# for the sake of expedience, I'm recruiting the event model for our little test.
	
	before :each do
		@userAlpha  = default_user(:login => 'alpha', :email => 'alpha@gmail.com')
		@userBeta  = default_user(:login => 'beta', :email => 'beta@gmail.com')		
		@event = default_event(:user => @userAlpha)
		#@event.recording = mock_model(Recording)
		#@event.log = mock_model(Log)
		#@event.update_attributes!(:tag_list => "xyzzy, review:yes") 
		#@event = mock_model(Event)
		#@event.taggings.current_user = default_user
	end

	it "should save single add" do
		@event.tag_add = "hello"

		#@event.taggings.should_not be_empty

		#@event.taggings.map { |tagging| tagging.tag.to_s }.should include "hello"

	end

	it "should add to existing list" do

	end

	it "should add same tag for different user" do

	end

	it "should allow removal of a tag" do

	end

	it "should allow only one tag of a constrained set for each user" do

	end

	it "should allow removal of a tag of a constrained set" do

	end











end





  def test_new_tags
    assert_equivalent ["Very good", "Nature"], posts(:jonathan_sky).tag_list
    posts(:jonathan_sky).update_attributes!(:tag_list => "#{posts(:jonathan_sky).tag_list}, One, Two")
    assert_equivalent ["Very good", "Nature", "One", "Two"], posts(:jonathan_sky).tag_list
  end

  def test_remove_tag
    assert_equivalent ["Very good", "Nature"], posts(:jonathan_sky).tag_list
    posts(:jonathan_sky).update_attributes!(:tag_list => "Nature")
    assert_equivalent ["Nature"], posts(:jonathan_sky).tag_list
  end

  def test_change_case_of_tags
    original_tag_names = photos(:jonathan_questioning_dog).tag_list
    photos(:jonathan_questioning_dog).update_attributes!(:tag_list => photos(:jonathan_questioning_dog).tag_list.to_s.upcase)

    # The new tag list is not uppercase becuase the AR finders are not case-sensitive
    # and find the old tags when re-tagging with the uppercase tags.
    assert_equivalent original_tag_names, photos(:jonathan_questioning_dog).reload.tag_list
  end

  def test_remove_and_add_tag
    assert_equivalent ["Very good", "Nature"], posts(:jonathan_sky).tag_list
    posts(:jonathan_sky).update_attributes!(:tag_list => "Nature, Beautiful")
    assert_equivalent ["Nature", "Beautiful"], posts(:jonathan_sky).tag_list
  end

  def test_tags_not_saved_if_validation_fails
    assert_equivalent ["Very good", "Nature"], posts(:jonathan_sky).tag_list
    assert !posts(:jonathan_sky).update_attributes(:tag_list => "One, Two", :text => "")
    assert_equivalent ["Very good", "Nature"], Post.find(posts(:jonathan_sky).id).tag_list
  end

  def test_tag_list_accessors_on_new_record
    p = Post.new(:text => 'Test')

    assert p.tag_list.blank?
    p.tag_list = "One, Two"
    assert_equal "One, Two", p.tag_list.to_s
  end

  def test_clear_tag_list_with_nil
    p = photos(:jonathan_questioning_dog)

    assert !p.tag_list.blank?
    assert p.update_attributes(:tag_list => nil)
    assert p.tag_list.blank?

    assert p.reload.tag_list.blank?
  end

  def test_clear_tag_list_with_string
    p = photos(:jonathan_questioning_dog)

    assert !p.tag_list.blank?
    assert p.update_attributes(:tag_list => '  ')
    assert p.tag_list.blank?

    assert p.reload.tag_list.blank?
  end

  def test_tag_list_reset_on_reload
    p = photos(:jonathan_questioning_dog)
    assert !p.tag_list.blank?
    p.tag_list = nil
    assert p.tag_list.blank?
    assert !p.reload.tag_list.blank?
  end