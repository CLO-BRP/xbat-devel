#require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
#
#describe AssetBundle do
#  before(:all) do
#		Kernel.stub!(:open)
#  end
#
#	before :each do
#		# Lets stub out some methods that have some implications for the local filesystem
#		path = File.join(RAILS_ROOT, 'spec', 'files')
#		AssetBundle.stub!(:files_server_root).and_return(path)
#		AssetBundle.stub!(:files_system_path).and_return(path)
#	end
#
#  it "should create a new instance" do
#		AssetBundle.new
#    #Event.create!(@valid_attributes)
#  end
#
#
#	#-------------
#	# from_xml
#	#-------------
#
#	describe "#from_xml" do
#		it "should create an AssetBundle instance" do
#			xml = File.open(File.join(RAILS_ROOT, 'spec', 'docs', 'asset_bundle.xml'))
#			bundle = AssetBundle.new.from_xml(xml)
#
#			bundle.should be_a_kind_of(AssetBundle)
#		end
#
#		it "should create an Array of AssetBundle instances" do
#			xml = File.open(File.join(RAILS_ROOT, 'spec', 'docs', 'asset_bundles.xml'))
#			bundle = AssetBundle.new.from_xml(xml)
#
#			bundle.should be_a_kind_of(Array)
#			bundle.first.should be_a_kind_of(AssetBundle)
#
#			# There are 6 asset bundle definitions in the XML
#			bundle.size.should equal(6)
#		end
#
#		describe "resulting object" do
#			before :each do
#				@bundle = AssetBundle.new.from_xml( File.open(File.join(RAILS_ROOT, 'spec', 'docs', 'asset_bundle.xml')) )
#			end
#
#			it "should have the correct keys" do
#				test_keys(@bundle)
#			end
#
#			it "file key should have an array" do
#				@bundle[:files].should be_a_kind_of(Array)
#			end
#
#			it "file array should have the correct keys" do
#				@bundle[:files].each { |file| test_file_keys(file) }
#			end
#		end
#		# describe "resulting object"
#	end
#	# describe "#from_xml"
#
#
#	#-------------
#	# from_xml
#	#-------------
#
#	describe "#from_resource" do
#		before :all do
#			@event = default_event
#		end
#
#		it "should create an AssetBundle instance" do
#			bundle = AssetBundle.new.from_resource(@event)
#			test_keys(bundle)
#			# the returned structure here will have an empty files array
#			bundle.should be_a_kind_of(AssetBundle)
#		end
#	end
#	# describe "#from_resource"
#
#
#	#-------------
#	# retrieve
#	#-------------
#
#	describe "#retrieve" do
#		before :each do
#			@sound 	= default_sound
#			AssetBundle.stub!(:set_local_parent_object).and_return(@parent_class = Sound, @parent_object = @sound)
#			AssetBundle.stub!(:save_file_from_remote).and_return(true)
#			# finally, create the test bundle
#			@bundle = AssetBundle.new.from_resource(@sound)
#		end
#
#		it "should return true since all methods are stubbed" do
#			@bundle.retrieve(:host => 'localhost').should equal(true)
#		end
#
#		it "should return false if there are no files to retrieve" do
#			@bundle[:files] = []
#			@bundle[:archive] = ''
#			@bundle.retrieve.should equal(false)
#		end
#	end
#
#	#-------------
#	# to_xml
#	#-------------
#
#	describe "#to_xml" do
#		before :each do
#			@sound 	= default_sound
#			@bundle = AssetBundle.new.from_resource(@sound)
#		end
#
#		it "should respond and return an XML string" do
#			xml = @bundle.to_xml
#			xml.should be_a_kind_of(String)
#			xml.should match(/xml/)
#		end
#	end
#
#
#protected
#
#	def test_keys(bundle)
#		[:guid, :type, :files].each { |key| bundle.should have_key(key) }
#	end
#
#	def test_file_keys(file)
#		[:host, :name, :path].each { |key| file.should have_key(key) }
#	end
#end
