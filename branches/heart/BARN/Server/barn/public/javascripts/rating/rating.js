var rating = function(id, value)
{
	this.construct.apply(this, arguments);
};

rating.prototype =
{
	construct: function(element, stars, value, rerate, hinput)
	{
		this.__timeout = -1;
		this.__listeners = {};
		this.__timeouts = {};
		this.element = element;
		this.id = element.id;
		this.imageOff = '/javascripts/rating/assets/blank.gif';
		this.imageOn = '/javascripts/rating/assets/gSelected.gif';
		this.imageOut = '/javascripts/rating/assets/bSelected.gif';
		this.timeout = 0;
		this.stars = stars;
		this.rerate = rerate; 
		this.hinput = hinput;

		this.setValue(value);

		var outsideEl = element;
        
        this.star_images = [];
        
		for(i=1; i<=this.stars; i++)
		{
			var imgID = 'img-' + i + '-' + this.id;
			var newimg = document.createElement('img');
			newimg.id = imgID;
			newimg.src = this.imageOff;

			this.star_images.push(newimg);
			outsideEl.appendChild(newimg);
			
			YAHOO.util.Event.addListener(newimg, "mouseover", curry(this.mouseOver, this, i));							
			YAHOO.util.Event.addListener(newimg, "click", curry(this.clickMethod, this, i));

		}

		this.addMethodListener("mouseOut", this.id, "mouseout");
		
		this.renderStars(this.value, false);
	},
	
	addMethodListener: function(method, el, event)
	{
		var that = this;
	
		this.__listeners["method:" + name + ":" + el.id + ":" + event] = function()
		{
			that[method].apply(that, arguments);
		};
	
		YAHOO.util.Event.addListener(el, event, this.__listeners["method:" + name + ":" + el.id + ":" + event]);
	},

	mouseOver: function(rating)
	{
		if(this.rerate)
		{
			this.clearTimeout(this.__timeout);
			this.__timeout = -1;

			this.renderStars(rating, true);
		}
	},
	
	clickMethod: function(rating)
	{
		this.onClick(rating);
	},

	mouseOut: function()
	{
		
		if(this.rerate)
		{
			this.clearTimeout(this.__timeout);
			this.__timeout = this.setTimeout('onTimeOut',this.timeout);
		}
	},
	
	onTimeOut: function()
	{	
		if(this.__timeout != -1)
		{
			this.renderStars(this.value, false);
		}
	},

	renderStars: function(units, startColor)
	{

		for (var i = 0; i < units; i++)
		{
		
			if(startColor == true)
			{
				this.star_images[i].src = this.imageOn
				//document.getElementById("img-" + i + '-'+this.id).src = this.imageOn;
			}
			else
			{
				this.star_images[i].src = this.imageOut
				//document.getElementById("img-" + i + '-'+this.id).src = this.imageOut;
			}
		}

		for (i = Number(units)+1; i <= this.star_images.length; i++)
		{
		    this.star_images[Number(i)-1].src = this.imageOff;

		    // document.getElementById(id).src = this.imageOff;
		}
	},

	setTimeout: function(method, period)
	{
		this.clearTimeout(method);

		var that = this;
		var args = Array.prototype.slice.call(arguments, 2) || [];

		this.__timeouts[method] = setTimeout(function()
		{
			that[method].apply(that, args);
		}, period);
	},

	clearTimeout: function(method)
	{
		if (this.__timeouts[method] > 0)
		{
			clearTimeout(this.__timeouts[method]);
			this.__timeouts[method] = 0;
		}
	},

	onClick: function(value)
	{
		this.setValue(value);
		this.hinput.value = this.value;

        var name_array = this.hinput.id.split('-');
        var name = name_array.slice(1, name_array.length)[0];
        var id = name_array.slice(1, name_array.length)[1];
        

        RatingsController.save({rateable_type:name, rateable_id:id, rating:value});


		if(!this.rerate)
		{
			for(i=0; i<=this.stars; i++)
			{
				YAHOO.util.Event.removeListener(this.star_images[i], "mouseover");
				YAHOO.util.Event.removeListener(this.star_images[i], "click");
			}
			
			YAHOO.util.Event.removeListener(document.getElementById(this.id), "mouseout");
			
			this.renderStars(this.value, false);

		}	
	},
	
	setValue: function(value)
	{
		this.value = value;
	},
	
	getValue: function()
	{
		return this.value;	
	}
}