function curry(fn, scope)
{
	var scope = scope || window;
	var args = Array.prototype.slice.call(arguments, 2) || [];

	return function()
	{
		fn.apply(scope, args);
	};
};
