// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

// Establish a BARN namespace to play nice with others

var BARN = {};

BARN.Application = {
    /**
        Initialize the application
    */
    initialize: function(data){
        // TODO: Make this meaningful!
        this.user = {id:1}
        this.renderTreeView();
        var yui_console = new YAHOO.widget.LogReader('yui_console', {isCollapsed:true, footerEnabled:true, height:'350px'});
        yui_console.hideSource("LogReader");
        yui_console.hideSource("source");
        yui_console.collapse();
    },
    
    /**
        Render a tree view of the BARN
    */
    renderTreeView: function(){

        // Create new tree and get root, the latter we attach stuff to
        
        var tree = new YAHOO.widget.TreeView("tree_browser");

        // create sound node and set dynamic load behavior to make Sound REST call
        
        var sounds_node = new YAHOO.widget.TextNode("Sounds", tree.getRoot(), true);
        sounds_node.setDynamicLoad( SoundsController.renderTreeNodes );

        // Refresh nodes each time it is expanded (no caching)
        
        tree.subscribe("expand", function(node) { 
            tree.removeChildren(node);
        }); 

        // Finally, render the tree; all descendants of these nodes
        // will be generated as needed by the dynamic loader.
        
        tree.draw();
    }
    
};


// HELPERS

YAHOO.widget.DataTable.formatTags = function(elCell, oRecord, oColumn, oData){

    // if(!oData || oData.tag.length == 0) return '';
    // 
    // var tags = oData.tag;
    // 
    // // var str  = tags[0].name;
    // 
    // for(i=1; i < tags.length; i++){
    //     str += ', ' + tags[i].name 
    // }
    var str = oData || ''
    elCell.innerHTML = str;
}

YAHOO.widget.DataTable.formatRating = function(elCell, oRecord, oColumn, oData){

    var el = document.createElement('div');
    el.id = 'rater-'+ oRecord.getData().id;
    
    var hinput = document.createElement('input')
    hinput.type = 'hidden'
    hinput.id = 'rater-event-'+ oRecord.getData().id;
    
    var avg_rating = oData || 0;

    //elCell.innerHTML = '<div id="rater-'+ oRecord.getData().id + '" class="rater"></div>'
    var r = new rating(el, 5,  Math.round(oData) || 0 , true, hinput);

    elCell.appendChild(r.element)

    //console.log(oRecord.getData().id)

}

