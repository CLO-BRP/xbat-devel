//-----------------------
// TAGGING
//-----------------------

// Form submission script
function bind_tag_form( context ){

    $("#ok").click( function() { $('#new-tag-form').submit(); });
    $("#cancel").click( function() { closeDialog('#verb-form'); });


	$('#new-tag-form').submit( function() {

		var form = $(this);
		var tag = $("#tag_entry", this).attr("value");

		if(tag == ""){
			alert("Please enter a tag");
			return false;
		}

        var curraction = $("#tag_action option:selected").attr("value");
        $(".tag_dialog_input").attr('name', curraction);

        //closeDialog('#verb-form');

		// submit the form
		$(this).ajaxSubmit({
			url: $(this).attr("action") + ".js", // this should be specifiable with dataType, but its not working
			success: function(response){
				try{
					// refresh the existing control



					// for some reason, this script cannot find "#verb-form"- maybe document.ready
					var dialog_div = form.parent();
					// close the dialog
					if(dialog_div.dialog('isOpen')) dialog_div.dialog('close');
				} catch(e) {
					console.log(e)
				}
			}
		});

		// return false to prevent normal browser submit and page navigation
		return false;
	});
}


function enable_editable_tags(logged_in) {
	
	if (!logged_in) {
		
		$('.render-editable-tags').livequery( function () {
			$(this).removeClass('render-editable-tags');
			
			wrap_tags(this, logged_in);
		});
		
		return;
	}
	
	var submit_tags = function(value, settings) {

		// parse the container id to get singular type taggable and object id
	
		var taggable_type = this.id.split("-")[0];
	
		var taggable_id   = this.id.split("-")[1];
	
		// pack post request parameters and post
	
		var params = {};
	
		params[taggable_type + '[tag_list]'] = value;
	
		params['_method'] = 'put';

        var localthis = this;
	
		// this is making an assumption about the controller name and is very brittle (why? see below)
	
		// NOTE: this updates a taggable object via the conventional Rails REST interface eg. PUT /events/123?event[tag_list]=red,green,blue
	
		// NOTE: the only concern here would be the pluralization of the 'taggable_type'
	
		$.post( '/' + taggable_type + 's/' + taggable_id, params, function( data ){ update_tag_cloud( get_container_context_id( localthis ) );  }, 'json');
	
		return(value);
	}

	$('.render-editable-tags').livequery( function() {
	
		// NOTE: livequery is necessary here since the ui element will disappear and reappear regularly
	
		$(this).focus(
			function() {
				BARN.gridTextEdit = true;
			}
		);
	
		$(this).blur(
			function() {
				BARN.gridTextEdit = false;
			}
		);
	
		$(this).removeClass('render-editable-tags');
	
		var taggable_type = this.id.split("-")[0];
	
		var taggable_id   = this.id.split("-")[1];
	
		$(this).editable( submit_tags, {
			type: 'text',
			width: 'auto',
			event: 'dblclick',
			tooltip: '',
	
			// NOTE: when it's time to edit we get the form values from this function
	
			data: function (value, settings) {
				var id = '#' + this.id + '-hidden';
				return $(id).text().strip();
			},
	
			// NOTE: after submitting the tags we call this function
	
			callback: function (value, settings) {
			    wrap_tags(this, logged_in);
			},
	
			onblur: function (value, settings) {
				// NOTE: 'reset' seems to do everything we need it to do
	
				this.reset();
	
				// NOTE: once we have reset the tags we can reattach the click event handler
	
				bindTagTips( this, 'mine' );
	
				$('.delete', this).dblclick( function(){
                    delete_tag(this);
                    return false; }).click( function () { return false; });
	
				BARN.gridTextEdit = false;
			}
	
			//autocomplete: {
			//	url: "/" + taggable_type + "s/tags?autocomplete=true",
			//	options: {
			//		multiple: true,
			//		minChars:2,
			//		matchContains:true,
			//		matchSubset:true,
			//		max:50,
			//		cacheLength:20,
			//		width: 260,
			//		selectFirst: false
			//	}
			//}
		});
	
		wrap_tags(this, logged_in);
	});
}

function wrap_tags (context, logged_in) {

//	if (arguments.length == 1) {
//		logged_in = true;
//	}
	
    // NOTE: we may wrap through a selector or for a particular element (the latter is used in the callback

    if (context) {
        targets = context;
    } else {
        targets = $('.tags');
    }

    $(targets).each( function () {

		// NOTE: we get the 'tag_list' string, split the list into a string 'tag' array, and wrap in appropriate markup

        var tag_list = $(this).html();

		var tag = tag_list.split(",");

		var delete_control = logged_in ? '<span class="delete">x</span>' : '';
		
        output = '<ul class="tag_list">';

        for (k = 0; k < tag.length; k++) {

			var current = tag[k].strip(); var part = current.split(":");

			if(current == '') continue;

			if (part.length == 2) {
				// NOTE: this is an experiment with the markup, we are creating a namespace element and adding the namespace as a class

				// NOTE: including the namespace part as a class in the outer element should easily allow for namespace specific markup

				output += '<li class="namespace tag ' + part[0] + '" rel="' + current + '"><span class="namespace">' + part[0] + ':</span><span>' + part[1] + '</span>' + delete_control + '</li>';

			} else {

				output += '<li class="tag" rel="' + current + '"><span>' + current + '</span>' + delete_control + '</li>';
			}
        }

        output += '</ul>';

        // NOTE: we store the tag list string in the hidden element, this is the editing representation
		
        if (logged_in) {
            if (tag_list != 'Double-click to edit') {
                key = '#' + this.id + '-hidden';

                $(key).html(tag_list);
            } else {
                    output = '<ul class="tag_hint"><li>Double-click to edit</li></ul>';
            }
        } else {
            if (tag_list.strip() == '') {
                output = '<ul class="tag_hint"><li>No tags available</li></ul>';
            }
        }
		
		// NOTE: finally display the wrapped version of the tags

        $(this).html(output);
		
		if (logged_in) {
			
			bindTagTips( this, 'mine' );
			
			$('span.delete', this).dblclick( function(){
                delete_tag(this);
                return false; } ).click( function () {return false;});
		}
		// HACK: to cleanup the auto-complete

//		ac_cleanup();
	});
}


// NOTE: we want to send the tag to the filter control, as 'set' or 'append'

// NOTE: the markup we rely on here is defined in /views/tags/_tag_cloud.html.erb

// TODO: there are various edge-case and set-operation improvements possible here

function bind_cloud_tags (context) {

	// NOTE: these values available to the callbacks set below, closures are beautiful

	var target = $('#filter-' + context);

	var filter = $('#query', target);

	var current = $(filter).val();

	// NOTE: this callback sets the filter

	$('.tag-cloud span.tag', '#' + context).click( function () {

		$(filter).val($(this).attr('target'));

		$(target).submit();
	});

	// NOTE: this callback appends the filter

	$('.tag-cloud span.add', '#' + context).click( function() {

		// NOTE: we need to get the 'html', not the 'text' from the element for the 'split' to work

		var append = $(this).attr('target');

		var current = $(filter).val();

		if (current.length) {

			$(filter).val(current + ', ' + append);
		} else {
			$(filter).val(append);
		}

		$(target).attr('action', $(target).attr('action').replace(/page=[0-9]*&/, "" ) ); 

		$(target).submit();

		return false; // this stops the bubbling of the event
	});
}

function bindTagTips( taglist, mode ) {
	$("li.tag", taglist).each( function(){

		if ( mode == 'mine' ) {
//			var content = '<ul class="unadorned tag_buttons">';
//			content += '<li title="maybe" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-help"></span></li>';
//			//content += '<li title="delete" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-close"></span></li>';
//			content += '</ul>';
			
			var content = '<span class="doubt">Doubt</span>';
		} else {
//			var content = '<ul class="unadorned tag_buttons">';
//			content += '<li title="yes" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-triangle-1-n"></span></li>';
//			content += '<li title="maybe" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-help"></span></li>';
//			content += '<li title="no" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-triangle-1-s"></span></li>';
//			content += '</ul>';
			
			var content = '<span class="accept">Accept</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="reject">Reject</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="doubt">Doubt</span>';
		}

		$(this).qtip({
			content: content,
			position:	{
				corner: {
				 tooltip: 'bottomLeft',
				 target: 'topRight'
			  }
			},
			show: {
				when: { event: 'click' },
				solo: $('.vote-popup')
			},
			hide: {
				fixed: true,
				when: { event: 'inactive' },
				delay: 2000
			},			
			style: {
			  name: 'blue',
			  classes: {
				tooltip: 'vote-popup'
			  },	
			  tip: true
		   	}
		});

		var qapi = $(this).qtip("api");		

		qapi.beforeShow = function() {
			$('.vote-popup').qtip("hide");
		};

		qapi.onShow = function() {
			$('.qtip-active').css('opacity', 1);
		};		

		qapi.onRender = function() {

			var target = this.elements.target;
			var content = this.elements.content;

			if (mode == 'mine') {

				bind_tag_rated( content, target, "span.doubt", 0);

			} else {

				bind_tag_rated( content, target, "span.accept", 1);

				bind_tag_rated( content, target, "span.doubt", 0);

				bind_tag_rated( content, target, "span.reject", -1);

			}
		};

	});
}

function bind_tag_rated( content, target, filter, value ) {
	$(content).find(filter).click( function() {

		send_tag_rating( target, value );
		reload_tag_parent( target );
	});
}

function reload_tag_parent( target ){
		//reload the parent tip
		var tapi = $(target).closest('.qtip').qtip("api");
		$(target).qtip("hide");
		tapi.loadContent( tapi.options.content.url, {}, "get");
	
		return false;
}

function send_tag_rating( target, rating ){

	element = $( target ).closest('div').attr('id');

	tag = $( target ).attr('rel');

	post = get_post_struct( element, '[tag_rated]', tag + "," + rating);

	$.ajax({
		type: 'POST', url: post['url'], data: post['data'],

		success: function () {
			return false;
		}
	});
}

function delete_tag( target ){

	var element = $( target ).closest('div').attr('id');

    //TODO - refactor - this pattern is used in a number of places on the client side.

    var elclass = element.split('-')[0] + "s";

	var siblings = $( 'ul', $( target ).closest('div') ).children();

	var tag_selected = $( target ).closest('li').attr('rel');

	var tags = '';

	siblings.each( function() {
		if ($(this).attr('rel') != tag_selected){
			tags += $(this).attr('rel') + ", ";
		}
	});

	if ( tags.length > 0 ) { tags = tags.substr(0, tags.length - 2); }

	post = get_post_struct( element, '[tag_list]', tags );

	$.ajax({
		type: 'POST', url: post['url'], data: post['data'], dataType: 'json',

		success: function ( data ) {

            var hidden = "";
            $.each(data[elclass][0]['tag_list'], function( index, value ){
                hidden += value + ",";
            });

            $('#' + element + '-hidden').html( hidden );

            update_tag_cloud( get_container_context_id( target ) );

            $(target).closest('div.tags').html(hidden).addClass('render-editable-tags');

			return false;
		}
	});

	return false;
}


function get_post_struct( target, action, tags ) {
	// target is expected as 'type-id-tags'

	// NOTE: this is really ugly, a simpler way should be possible

	url = target.split('-'); var type = url[0];

	url[0] = url[0] + 's'; url.pop(); url = '/' + url.join('/') + '.js';

	var data = {};

	data['_method'] = 'put';

	data[type + action] = tags;

	var out = {};

	out['url'] = url;

	out['data'] = data;

	return out;
}


