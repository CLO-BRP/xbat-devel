# {
#   "server": {
#     "guid": "86193c18-8a48-dd21-b885-d6929c2c64c2",
#     "port": 4567,
#     "database": "files.sqlite3",
#     "host": "localhost"
#   },
#   "scout": {
#     "resolution": 1,
#     "port": 80,
#     "host": "barn.xbat.org",
#     "user": "david.michael@gmail.com",
#     "password": "passwords"
#   }
# }
require 'rubygems'
require 'json'

module Silo
	class Configuration < Hash
		attr_accessor :file
		
		def initialize(filename = 'config.json', options = {})
			@file = filename
			
			# Attempt to load the configuration file
			begin
				if File.exist?(@file)
					load 
					return self
				end
			rescue Exception => e
				# Probably a parse error... reverting to the default config
			end
			# default configuration
			FileUtils.touch(@file) # should this be here?
			set_defaults(options)
			save
		end
		
		def set_defaults(options = {})
			self["silo"] = {
				"guid" => nil,
				"port" => 4567,
				"database" => "files.sqlite3",
				"host" => "localhost",
				"local_ip" => options[:local_ip] || nil,
				"external_ip" => nil
			}				
			
			self["scout"] = {
				"resolution" => 1, # in seconds
				"port" => 80, 
				"host" => "dev.barn.xbat.org",
				"user" => nil,
				"password" => nil
			}
		end
		
		def save(settings = {})			
			open(@file, 'w') { |file| 
				file.puts JSON.pretty_generate(self)
			}
			return self
		end
		
		# If you edit the file by hand and 
		def load
			config = JSON.parse(open(@file) { |f| f.read })
			config.each do |key, value|
				self[key] = config[key]
			end
			return self
		end
		
		def reload
		end
	end
end

