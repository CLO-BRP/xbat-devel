#----------
# Usage
#----------

# To boot from the command line:
# $ RACK_ENV=development rackup config.ru
# http://localhost:9292

# When running this server as a rackup script, ENV['RACK_ENV'] will be nil
# The consequences of this is the the environment variables will not be run.
# For windows deployment this will take some care as it will need to be run as a service.
# http://rubyforge.org/docman/view.php/85/595/service.html
# http://github.com/raggi/ruby_service_helper/tree/master

# We are currently packaging all necessary libraries to run this server in the vendor folder
# require 'rubygems'

root = File.expand_path(File.dirname(__FILE__));

vendor_root = File.join(root, 'vendor')

$LOAD_PATH.unshift root

%w{rack sinatra platform json_pure}.each do |name|
	$LOAD_PATH.unshift File.join(vendor_root, name, 'lib')
end

require 'server'

#----------
# Logging needs setup
#----------

FileUtils.mkdir_p 'log' unless File.exists?('log')
log = File.new("log/sinatra.log", "a")
$stdout.reopen(log)
$stderr.reopen(log)
 
#----------
# Run the application
#----------

run Sinatra::Application
#run Silo::Application


