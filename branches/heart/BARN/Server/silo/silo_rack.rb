#--
# setup
#--

app_file = File.expand_path(__FILE__);

root = File.dirname(app_file)

vendor_root = File.join(root, 'vendor');

libs = %w{activerecord activesupport platform rack sinatra}

libs.each do |name|
	$LOAD_PATH.unshift File.join(vendor_root, name, 'lib')
end

$LOAD_PATH << File.join(root, 'lib') << File.join(root, 'lib', 'worm')

require 'worm'

#--
# configure
#--

set :app_file => app_file, :reload => true

set :root, root

set :run, false

set :environment, :development

set :raise_errors, true

configure do 
  $worm = Worm.new(File.join(root, 'files.sqlite3'))
end

configure :development do
  $stdout << "development"
end

configure :production do
  $stdout << "production"
end

#--
# helpers
#--

helpers do
	def disp(obj, head = nil)
		if head.nil?
			"<pre>" + obj.to_yaml + "</pre>"
		else
			"<h1>" + head.to_s + "</h1><pre>" + obj.to_yaml + "</pre>"
		end
	end
	
	def known_resource_check
		throw :halt, [404, "Unknown resource type."] unless ['asset', 'audio', 'image'].include?(resource)
	end
	
	def debug_info
		disp(request, 'request') << disp(ENV, 'ENV')
	end
end

#--
# routes
#--

get '/' do
  disp(request, 'request') << disp(ENV, 'ENV')
end

#get '/audio' do
#   @audio = Audio.find(:all)
#   
#   @audio.to_json
#end

# NOTE: the routes below handle resource description, listing, and display

# NOTE: the 'known resource' constraint would be nice as a filter

get '/:resource/info/?' do
	known_resource_check
	
	disp( $worm.table_info(params[:resource]), params[:resource].to_s + ' info' ) << debug_info
end

get '/:resource/?' do
	known_resource_check
	
	disp( $worm.find_all(params[:resource]), params[:resource].to_s ) << debug_info
end

get '/:resource/:id/?' do
	known_resource_check
	
	disp( $worm.find(params[:resource], params[:id]).first, params[:resource].to_s + ' ' + params[:id].to_s ) << debug_info
end

  
