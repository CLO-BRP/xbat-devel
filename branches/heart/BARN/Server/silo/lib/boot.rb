class File 
  def self.here(string)
    join(expand_path(dirname(__FILE__)), string)
  end
end

$LOAD_PATH << File.here(File.join('..', 'vendor', 'platform', 'lib'))
$LOAD_PATH << File.here(File.join('..', 'vendor', 'rack', 'lib'))
$LOAD_PATH << File.here(File.join('..', 'vendor', 'sinatra', 'lib'))
$LOAD_PATH << File.here(File.join('..', 'vendor', 'activerecord', 'lib'))
$LOAD_PATH << File.here(File.join('..', 'vendor', 'activesupport', 'lib'))

require 'platform'
require 'rack'

if Platform.is_mac?
  $LOAD_PATH << File.here(File.join('..', 'vendor', 'sqlite3-ruby-1.2.4-osx-10.5', 'lib'))
elsif Platform.is_windows?
  $LOAD_PATH << File.here(File.join('..', 'vendor', 'sqlite3-ruby-1.2.3-x86-mswin32', 'lib'))  
else
  raise "Sorry, Linux is not ready."
end

require 'activerecord'

# require 'initialize_file_database'
# 
# initialize_file_database
# 
# ActiveRecord::Base.establish_connection(  
#   :adapter  => 'sqlite3',   
#   :database => 'files.sqlite3'
# )


# require 'logger'
# ActiveRecord::Base.logger = Logger.new(STDERR)

# ActiveRecord::Base.connection.execute("CREATE TABLE customer
# (First_Name char(50),
# Last_Name char(50),
# Address char(50),
# City char(50),
# Country char(25),
# Birth_Date date)")
