helpers do	
	def head(text, n = 1)
		return '' if text.nil?

		"<h#{n}>" + text + "</h#{n}>"
	end
	
	def disp(obj, title = nil)
		
		params[:format] = 'html' if params[:format].nil?
		
		case params[:format]
			
		when 'html'
			head(title) + "<pre>" + obj.to_yaml + "</pre>"
			
		when 'js'
			obj.to_json
			
		else 
			throw :halt, [404, head("Unknown response format '#{params[:format]}'.")]
			
		end
	end
	
	def check_resource_type
		puts options
		known = options.types.include?(params[:type])
		
		throw :halt, [404, "<h1>Unknown resource type '#{params[:type]}'.</h1>"] unless known 
	end
	
	def query
		request.env['rack.request.query_hash']
	end
	
	def register_with_barn(config)

	  # If there is a GUID, then nothing to do
	  #return true if !config['server']['guid'].empty?  

	  puts "Registering with the BARN ..."

	  remote_host = config['scout']['host'] 
	  remote_port = config['scout']['port']
	  remote_user = config['scout']['user'] 
	  remote_pass = config['scout']['password']    
	  host = config['silo']['host'] 
	  port = config['silo']['port']  
		
		begin
			uri = URI.parse(File.join("http://#{remote_host}:#{remote_port}", "silos.js"))
			# Create a Net::HTTP::Post request instance
			request = Net::HTTP::Post.new(uri.path)
			
	    # Set the authorization headers
			request.basic_auth(remote_user, remote_pass)
			
			# Set the form data
	    request.set_form_data({"host" => host, "port"=> port})
	    
			# Finally, make the request
			response = Net::HTTP.new(uri.host, uri.port).start {|http| 
				http.request(request) 
			}
	    
			#response = Net::HTTP.post_form(URI.parse(uri), {"host" => host, "port"=> port})

			if response.header.code == "201"
				puts response.body
				this_silo = JSON.parse(response.body)
				
				puts this_silo.inspect
				
			  config['silo']['guid'] = this_silo["silo"]["guid"]
				config['silo']['external_ip'] = this_silo["silo"]["host"]

				config['silo']['local_ip'] = options.local_ip
				
			  config.save
				puts "success"
	      return "This Silo has been successfully registered."
	    else
				puts "failure"
				puts response.body      
				return "Sorry, this Silo could not be registered! #{decode_http_status(response.header.code)}. #{response.header.code}"
	      
		  end
		rescue URI::InvalidURIError => error
			return "Could not construct a valid URL. Please review your configuration."
		rescue Errno::ETIMEDOUT, Errno::ECONNREFUSED => error
			return "#{error} => Failed connection to #{uri} at #{Time.now.strftime("%X")}"
		rescue JSON::ParserError => error
			return "Sorry... I couldn't understand what the server said!"
		end
	end
	
	def decode_http_status(code)
		case code.to_i
		when 401 
			"Authorization Failed"
		when 500
			"Internal Server Error"
		else
			"Something has gone wrong"
		end
	end
end