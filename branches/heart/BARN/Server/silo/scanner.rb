#--
# require
#--

require 'yaml'

require 'json'

# NOTE: appending the load path allows simple 'require' of the various 'lib' files

# TODO: consider automating this and the use of 'unshift' (prepend) rather than append

root = File.expand_path(File.dirname(__FILE__));

lib_root = File.join(root, 'lib'); 

$LOAD_PATH << lib_root << File.join(lib_root, 'sndfile') << File.join(lib_root, 'worm')

vendor_root = File.join(root, 'vendor')

$LOAD_PATH << File.join(vendor_root, 'platform', 'lib') << File.join(vendor_root, 'mp3info', 'lib')

puts $LOAD_PATH.to_yaml

require 'directory_scanner'

require 'sndfile'

require 'mp3info'

require 'worm'

#--
# load silo config
#--

config = open('config.json') { |f| f.read }; config = JSON.parse(config)

#--
# setup database and helper
#--

# TODO: add basic licensing and eventually provenance data to schema

# NOTE: we initialize a database connection and make it global

database = File.join(File.expand_path(File.dirname(__FILE__)), config['server']['database'])

$db = Worm.new(database, true)

$db.execute "
BEGIN;
	CREATE TABLE IF NOT EXISTS recording (
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		file VARCHAR(255),
		format VARCHAR(255),
		channels INTEGER,
		samples INTEGER,
		samplerate INTEGER,
		samplesize INTEGER,
		bitrate INTEGER,
		length REAL,
		time DATETIME,
		time_standard VARCHAR(255),
		gps_lat REAL,
		gps_lon REAL,
		gps_alt REAL,
		location TEXT(65535),
		cache TEXT(65535),
		date VARCHAR(255),
		bytes INTEGER,
		content_hash VARCHAR(255),
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	);
	CREATE TABLE IF NOT EXISTS asset (
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		asset_type VARCHAR(255),
		asset_id INTEGER,
		location TEXT(65535),
		date VARCHAR(255),
		bytes INTEGER,
		content_hash VARCHAR(255),
		time DATETIME,
		time_standard VARCHAR(255),
		gps_lat REAL,
		gps_lon REAL,
		gps_alt REAL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	);
	CREATE TABLE IF NOT EXISTS audio (
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		file VARCHAR(255),
		format VARCHAR(255),
		samplerate INTEGER,
		samplesize INTEGER,
		bitrate INTEGER,
		channels INTEGER,
		frames INTEGER,
		length REAL
	);
	CREATE TABLE IF NOT EXISTS image (
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		file VARCHAR(255),
		format VARCHAR(255),
		height INTEGER,
		width INTEGER,
		pixeldepth INTEGER
	);
COMMIT;
"

# NOTE: we get and store media file info

def update_file_info(file)

	format = file.split('.').last.downcase; info = {}
	
	case format
		
	when 'aif', 'aifc', 'aiff', 'au', 'fla', 'flac', 'mp3', 'ogg', 'wav'
		
		# TODO: check whether the file has been modified, in that case we must update
		
		found = $db.find_by('asset', 'location', file)
		
		if found.size == 0
			
			#--
			# get format-specific and file-related info
			#--
			
			if format != 'mp3'
				info = Sndfile.new(file).info
				
				info['length'] = info['frames'].to_f / info['samplerate'].to_f
			else
				begin
					mp3 = Mp3Info.open(file)
				rescue Mp3InfoError => e
					puts ''
					puts "Failed to process #{file}"
					puts e.message
					puts ''
					
					return
				end
				
				info['length'] = mp3.length
				
				info['samplerate'] = mp3.samplerate
				
				# TODO: check whether this is correct, we are truncating
				
				info['frames'] = (mp3.samplerate * mp3.length).to_i
				
				info['bitrate'] = mp3.bitrate
				
				info['channels'] = (mp3.channel_mode == 'Single Channel') ? 1 : 2
			end
						
			info['location'] = file
		
			info['file'] = file.split('/').last
			
			info['format'] = format
		
			require 'file_hash'
			
			info['content_hash'] = FileHash.md5(file)
			
			info['date'] = File.mtime(file)
			
			info['bytes'] = File.size(file)
			
			#--
			# add audio asset
			#--
			
			info['asset_id'] = $db.insert('audio', info)
			
			info['asset_type'] = 'audio'
			
			$db.insert('asset', info)
			
			$db.insert('recording', info)
		end
	
	when 'jpg', 'tiff'
		
	end
	
end

#--
# configure and scan
#--

# NOTE: get directories to scan from file

roots = File.join(Dir.pwd, 'roots.txt')

if File.exists? roots
	roots = open(roots) do |file|
		file.readlines
	end

	roots.map! {|root| root.chomp}
else
	roots = Dir.pwd
end

scanner = DirectoryScanner.new(roots, Proc.new {|file| update_file_info(file)})

scanner.scan