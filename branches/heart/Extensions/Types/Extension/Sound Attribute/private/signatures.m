function [fun, info] = sound_attribute

% NOTE: consider reuse of the 'param_fun' api structure, attributes are
% sort of parameters, susceptible to: creation, compilation, and control,
% the controls can take options and have a callback that represents the
% attribute model. What is the relation between preset store and load with
% read and write?

% NOTE: the context will contain the sound, which will probably provide
% some indication of where the file is and help to validate its contents

% NOTE: it is not clear that we need to specify the file, sort of like
% presets where we don't need to specify the location only the name, except
% in this case we may not even need a name

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun = param_fun('attribute');

fun.read = {{'attr', 'options'}, {'sound', 'start', 'duration', 'channels', 'options'}};

