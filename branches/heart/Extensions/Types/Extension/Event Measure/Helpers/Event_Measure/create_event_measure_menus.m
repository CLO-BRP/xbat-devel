function handles = create_event_measure_menus(par, event, exts, contexts)

% create_event_measure_menus - create measure menus for event
% -----------------------------------------------------------
%
% handles = create_event_measure_menus(par, event, exts, contexts)
%
% Input:
% ------
%  par - parent for measure menus
%  event - event to display menus for
%  exts - extensions to consider
%  contexts - respective contexts
%
% Output:
% -------
%  handles - menu handles
%
% See also: event_menu, struct_menu

% TODO: consider creating a parameter menu, these may have non-trivial callbacks!

% TODO: develop callback conventions for these menus

% NOTE: what are callbacks used for in this context?

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% get extensions and contexts if needed
%--

if nargin < 4
	
	[id, exts, data] = get_event_extension_data(event);
	
	parent = ancestor(par, 'figure'); data = get_browser(parent);
	
	for k = 1:numel(exts)
		[exts(k), ignore, contexts(k)] = get_browser_extension(exts(k).subtype, parent, exts(k).name, data);
	end
end

%----------------------------
% CREATE MENUS
%----------------------------

handles = cell(size(exts));

for k = 1:numel(exts)

	%--
	% get current extension and context and check for value
	%--
	
	ext = exts(k); context = contexts(k);

	% NOTE: if the measure value is missing we should not try to create the value menu
	
	% TODO: consider creating a menu that indicates, perhaps explains, failure
	
	if ~isfield(event.measure, ext.fieldname)
		continue;
	end
	
	%--
	% create menu
	%--
	
	% NOTE: make sure that we have a single parent
	
	root = get_menu(par, ext.name);
	
	if ~isempty(root)
		delete(get(root, 'children'));
	else
		root = uimenu(par, 'label', ext.name);
	end
	
	if isempty(ext.fun.value.menu.create)
		
		% TODO: should this be an option? building menus here can be dangerous
		
% 		handles{k} = struct_menu(root, event.measure.(ext.fieldname).value);
	else
		try
			handles{k} = ext.fun.value.menu.create(root, event.measure.(ext.fieldname).value, context);
		catch
			handles{k} = []; extension_warning(ext, 'Failed to create value menu.', lasterror);
		end
	end

end

% NOTE: it seems like we could have orientation problems when concatenating

handles = [handles{:}];

