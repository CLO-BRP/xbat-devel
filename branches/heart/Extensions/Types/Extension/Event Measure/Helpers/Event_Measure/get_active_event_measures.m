function [ext, context] = get_active_event_measures(par, data, fast)

% get_active_event_measures - what it says
% ----------------------------------------
%
% [ext, context] = get_active_event_measures(par, data)
%
% Input:
% ------
%  par - browser
%  data - state
%
% Output:
% -------
%  ext - active event measure extensions
%  context - matching contexts

if nargin < 3
	fast = 0; 
end

%--
% initialize trivial output and handle input
%--

ext = []; context = []; 

if ~nargin
	par = get_active_browser;
end 

if isempty(par)
	return; 
end

if nargin < 2
	data = get_browser(par);
end

%--
% get active event measures and ensure proper contexts
%--

[ext, ignore, context] = get_active_extension('event_measure', par, data, fast);

% NOTE: we only do this if the context output was requested

if nargout > 1
	
	active = get_active_extension(get_extension_types, par, data, fast);

	% NOTE: we have to add the active context extensions separately to avoid pathological recursion

	for k = 1:numel(context)
		context(k).active = active;
	end
	
end
