function log = compute_log_event_measures(log, id, exts, contexts)

% compute_log_event_measures - compute various event measures on log
% ------------------------------------------------------------------
%
% log = compute_log_event_measures(log, id, exts, contexts)
%
% Input:
% ------
%  log - log
%  id - ids for events to measure
%  exts - event measure extensions
%  contexts - contexts
%
% Output:
% -------
%  log - log

% TODO: update to handle multiple logs and id arrays, it would be nice to iterate over 'log' and 'id' together

% NOTE: this is the equivalent of 'detector_scan' 

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% get all events in log
%--

if isempty(id)
	id = get_log_event_field(log, 'id');
end

% NOTE: there are no events and nothing to do

if isempty(id)
	return; 
end

%-----------------------
% SETUP
%-----------------------

%--
% discard extensions that do not yet compute
%--

for k = numel(exts):-1:1
	
	if isempty(exts(k).fun.compute), exts(k) = []; end
	
end

% NOTE: there is nothing to compute, we are done

if isempty(exts), return; end 

%--
% create waitbar and pack description
%--

wait.start = 0; wait.total = numel(id) * numel(exts);

wait.handle = measure_waitbar(contexts(1));

%-----------------------
% COMPUTE
%-----------------------

% NOTE: we page event measure computations

% TODO: test paging parameters in terms of performance

page_size = 200;

page = get_pages(id, page_size);

for k = 1:numel(page)
	
	%--
	% get page events from log
	%--
	
	event = log_get_events(log, page{k});

	%--
	% compute measures on page events
	%--
	
	% TODO: this function should perhaps update the waitbar in some way
	
	wait.start = (k - 1) * page_size * numel(exts);
	
	event = compute_event_measures(event, exts, contexts, wait);

	%--
	% save page events to log
	%--
	
	waitbar_update(wait.handle, 'PROGRESS', 'message', 'Saving measurement results ...');
	
	% TODO: in the case of multiple extensions we are not saving the extension as user
	
	if numel(exts) == 1
		request.save = 'measure'; request.user = exts;
	else
		request = 'measure';
	end

	log_save_events(log, event, request);
	
end

%--
% close waitbar if needed
%--

if get_control(wait.handle, 'close_after_completion', 'value')
	close(wait.handle); 
end

%--
% update parent displays if needed
%--

par = unique([contexts.par]);

for k = 1:numel(par)
	
	if is_browser(par(k)), browser_display(par(k), 'events'); end
	
end


%----------------------
% GET_PAGES
%----------------------

% TODO: consider a richer representation of pages that can simplify simple pages

function page = get_pages(id, n)

%--
% consider simple case first
%--

if numel(id) <= n
	page = {id}; return;
end

%--
% compute number of full pages and whether there is a partial page
%--

pages = numel(id) / n; full = floor(pages); partial = pages ~= full; 

%--
% build pages 
%--

page = cell(1, full + partial);

stop = 0;

for k = 1:full
	start = stop + 1; stop = start + n - 1; page{k} = id(start:stop);
end

if partial
	page{full + 1} = id(stop + 1:end);
end 


%----------------------
% MEASURE_WAIT
%----------------------

function pal = measure_waitbar(context)

%--
% get waitbar handle
%--

% NOTE: this implements a singleton condition on the waitbar

name = ['Measuring Log Events'];

pal = find_waitbar(name);

if ~isempty(pal)
	return;
end

%-------------------------------------------------
% WAITBAR CONTROLS
%-------------------------------------------------

control = control_create( ...
	'name', 'PROGRESS', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'lines', 1.15, ...
	'space', 2 ...
);

control(end + 1) = control_create( ...
	'name', 'close_after_completion', ... 
	'style', 'checkbox', ...
	'space', 0.75, ...
	'value', 1 ...
);

control(end + 1) = control_create( ...
	'string', 'Details', ...
	'style', 'separator', ...
	'type', 'header' ...
);

% NOTE: serves as text display for results of detection

control(end + 1) = control_create( ...
	'name', 'events', ...
	'lines', 8, ...
	'space', 1, ...
	'confirm', 0, ...
	'style', 'listbox' ...
);
	
%-------------------------------------------------
% CREATE WAITBAR
%-------------------------------------------------

%--
% setup waitbar
%--

opt = waitbar_group; opt.show_after = 0;

if ~isempty(context.par)
	par = context.par; pos = 'bottom right';
else
	par = 0; pos = 'center';
end

%--
% create waitbar
%--

pal = waitbar_group(name, control, par, pos, opt);



