function [value, ext, context] = has_active_event_measures(varargin)

% has_active_event_measures - test this
% -------------------------------------
%
% [value, ext, context] = has_active_event_measures(par, data)
%
% Input:
% ------
%  par - browser
%  data - state
%
% Output:
% -------
%  value - indicator
%  ext - active event measure extensions
%  context - matching contexts

%--
% initialize trivial output and handle input
%--

switch nargout
	
	case {0, 1}
		value = numel(get_active_event_measures(varargin{:}));

	case 2
		ext = get_active_event_measures(varargin{:}); value = numel(ext);
		
	case 3
		[ext, context] = get_active_event_measures(varargin{:}); value = numel(ext);
		
end