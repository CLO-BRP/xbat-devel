function control = event_measure_shared_controls(ext, context)

% event_measure_shared_controls - shared controls
% -----------------------------------------------
%
% control = event_measure_shared_controls(ext, context)
%
% Input:
% ------
%  ext - extension
%  context - context
%
% Output:
% -------
%  control - shared controls

% NOTE: when we get type extensions the signature will be (parameter, context)

%--
% handle input
%--

% TODO: compute this from context

active = 0;

%--
% create controls
%--

% NOTE: active features essentially provide a view to the user

control = empty(control_create);

tabs = {'Active', 'Logs'};

control(end + 1) = control_create( ...
	'style', 'tabs', ...
	'tab', tabs ...
);

control(end + 1) = control_create( ...
	'name', 'active', ...
	'alias', 'Selection', ...
	'tab', tabs{1}, ...
	'style', 'checkbox', ...
	'initialstate', initial_enable(can_compute(ext)), ...
	'value', active, ...
	'callback', {@active_state_callback, context.par, context.ext}, ...
	'tooltip', 'Measure selection' ...
);

control(end + 1) = control_create( ...
	'name', 'view', ...
	'tab', tabs{1}, ...
	'style', 'checkbox', ...
	'initialstate', initial_enable(has_view(ext)), ...
	'value', 0, ...
	'tooltip', 'Display event measure view' ...
);

[names, active, initial] = get_open_log_names(context.par);

% NOTE: naming this control 'output_log' provides control updating on log open and close

control(end + 1) = control_create( ...
	'name', 'output_log', ...
	'alias', 'Target', ...
	'tab', tabs{2}, ...
	'style', 'popup', ...
	'string', names, ...
	'value', active, ...
	'tooltip', 'Select measure-computation target-log' ...
);

if ~isempty(initial)
	control(end).initialstate = initial;
end

control(end + 1) = control_create( ...
	'name', 'start', ...
	'tab', tabs{2}, ...
	'style', 'buttongroup', ...
	'lines', 1.75, ...
	'tooltip', 'Start measuring log events' ...
);


% TODO: factor the following to real functions, check if any already exists

%---------------------------
% GET_OPEN_LOG_NAMES
%---------------------------

function [names, active, initial] = get_open_log_names(par)

data = get_browser(par);

% NOTE: this function is too intimate

initial = '';

try
	names = file_ext({data.browser.log.file}); active = data.browser.log_active;
catch
	names = {};
end

if isempty(names)
	names = {'(No Open Logs)'}; active = 1; initial = '__DISABLE__';
end


