function [result, context] = dispatch(request, parameter, context)

% EVENT MEASURE - dispatch

result = struct;

switch request
	
	% NOTE: 'active' and 'open' typically initiate in the browser extension menus
	
	case 'active'
		
		activate_event_measure(context.ext, context.par);
		
	case 'open'
		
		result.pal = open_extension_palette(context.ext, context, context.par);
		
	% NOTE: should this be an extension warning?
	
	otherwise, error('XBAT:ExtensionType:UnrecognizedRequest', ['Unrecognized request ''', request, '''.']);
		
end
