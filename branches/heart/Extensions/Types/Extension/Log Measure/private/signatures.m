function [fun, info] = log_measure

% log_measure - function handle structure
% ---------------------------------------
%
% [fun, info] = log_measure
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

fun.compute = {{'value'},{'log','param'}};

fun.parameter = param_fun;

fun.value = value_fun;