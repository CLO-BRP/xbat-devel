function [result, context] = dispatch(request, parameter, context) %#ok<INUSL>

% SOUND BROWSER PALETTE - dispatch

result = struct;

switch request
	
	%------------------------
	% OPEN
	%------------------------
	
	case 'open'
		
		%--
		% setup
		%--
		
		ext = context.ext; data = get_browser(context.par);
		
		%--
		% check for existing palette and center if found
		%--
		
		% TODO: it seems like this should be factored as it is the behavior of all extension palettes
		
		out = get_xbat_figs('parent', context.par, 'type', 'palette');

		% NOTE: further check the type and position and return if palette exists

		for k = 1:length(out)
			
			info = parse_tag(get(out(k), 'tag'), '::', {'ignore', 'type', 'name'});

			if ~strcmpi(ext.type, info.type)
				continue;
			end
			
			position_palette(out(k), context.par, 'center'); return;

		end
		
		%--
		% get controls and options to build palette
		%--
		
		fun = ext.fun.parameter.control.create;
		
		% NOTE: a palette is essentially controls, if we don't have these return
		
		if isempty(fun)
			return; 
		end

		try
			control = ext.fun.parameter.control.create(ext.parameter, context);
		catch
			extension_warning(ext, 'Control options failed.', lasterror);
		end
		
		fun = ext.fun.parameter.control.options;
		
		if ~isempty(fun)
			try
				opt = struct_update(control_group, fun(context));
			catch
				extension_warning(ext, 'Control options failed.', lasterror);
			end
		end
		
		opt.header_color = get_extension_color('sound_browser_palette');
				
		opt.ext = ext;
		
		%--
		% create new control group (palette)
		%--
		
		% TODO: think about how this approach references the variables
		
		pal = control_group(context.par, {@callback_helper, context}, ext.name, control, opt);
		
		%--
		% set palette tag, key press, and close request function
		%--

		% NOTE: control values are updated in parent as part of close request
		% function

		set(pal, ...
			'visible','off', ...
			'tag', build_widget_tag(context.par, ext), ...
			'keypressfcn', {@browser_keypress_callback, context.par}, ...
			'closerequestfcn', {@delete_palette, context.par} ... 
		);

		%--
		% register palette with parent and set parent windowbuttondown function
		%--

		n = length(data.browser.palettes);

		data.browser.palettes(n + 1) = pal;

		set(context.par, ...
			'userdata', data, ...
			'buttondown', 'browser_palettes(gcf,''Show'');' ...
		);

		%--
		% position palette relative to parent
		%--

		position_browser_palette(context.par, pal, data);
	
end


%------------------------
% CALLBACK_HELPER
%------------------------

function callback_helper(obj, eventdata, context) %#ok<INUSL>

%--
% check for callback
%--

fun = context.ext.fun.parameter.control.callback;

if isempty(fun)
	return;
end 

%--
% pack context and try to callback
%--

callback = get_callback_context(obj, eventdata);

try
	result = fun(callback, context); %#ok<NASGU>
catch
	extension_warning(context.ext, 'Parameter control callback failed.', lasterror); 
	
	result.keep_focus = true; % NOTE: it makes sense to focus on the cause of the error? how about the related error?
end

%--
% consider result
%--

% TODO: interpret result to consider update, this is used in other places, here we are talking about the browser refresh

if isfield(result, 'keep_focus') && result.keep_focus
	
	figure(callback.pal.handle);
end


