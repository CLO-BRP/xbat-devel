function [fun, info] = sound_format

% sound_format - function handle structure
% -----------------------------------------
%
% [fun, info] = sound_format
%
% Output:
% -------
%  fun - structure for extension type API

%--------------
% INFO
%--------------

info.version = '0.1';

info.short_description = '';

%--------------
% API
%--------------

% TODO: this proably needs parameters in a few places

%--
% sound creation and configuration
%--

fun.create = {{'sound'}, {}};

fun.parameter = param_fun;

%--
% sound info, reading, and possibly writing
%--

% NOTE: read uses time

fun.info = {{'info'}, {'resource', 'context'}};

fun.read = {{'samples', 'rate'}, {'sound', 'start', 'duration', 'channels'}};

% fun.write = {{}, {}};

