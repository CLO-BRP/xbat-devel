function pal = detector_waitbar(context)

% detector_waitbar - create detection waitbar
% --------------------------------------
%
% pal = detector_detect_wait(context)
%
% Input:
% ------
%  context - detector context
%
% Output:
% -------
%  pal - waitbar palette handle

%--
% get waitbar handle
%--

% NOTE: this implements a singleton condition for the waitbar

name = [context.ext.name, ' - ', sound_name(context.sound)];

pal = find_waitbar(name);

if ~isempty(pal)
	return;
end

%-------------------------------------------------
% WAITBAR CONTROLS
%-------------------------------------------------

control = control_create( ...
	'name', 'PROGRESS', ...
	'style', 'waitbar', ...
	'units', context.scan.duration, ...
	'confirm', 1, ...
	'lines', 1.15, ...
	'space', 2 ...
);

control(end + 1) = control_create( ...
	'name', 'close_after_completion', ... 
	'style', 'checkbox', ...
	'space', 0.75, ...
	'value', 1 ...
);

control(end + 1) = control_create( ...
	'string', 'Details', ...
	'style', 'separator', ...
	'type', 'header' ...
);

% NOTE: serves as text display for results of detection

control(end + 1) = control_create( ...
	'name', 'events', ...
	'lines', 8, ...
	'space', 1, ...
	'confirm', 0, ...
	'style', 'listbox' ...
);
	
%-------------------------------------------------
% CREATE WAITBAR
%-------------------------------------------------

%--
% setup waitbar
%--

opt = waitbar_group; opt.show_after = 1;

if ~isempty(context.par)
	par = context.par; pos = 'bottom right';
else
	par = 0; pos = 'center';
end

%--
% create waitbar
%--

pal = waitbar_group(name, control, par, pos, opt);

