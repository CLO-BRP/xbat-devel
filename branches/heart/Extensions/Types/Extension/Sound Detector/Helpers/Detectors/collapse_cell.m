function out = collapse_cell(in)

%--
% check for cell input
%--

if ~iscell(in)
	error('Input must be cell array');
end

if numel(in) == 0
    out = []; return;
end

%--
% remove empty cells and align content arrays
%--

for k = numel(in):-1:1
	
	if isempty(in{k})
		in(k) = []; continue;
	end

	% NOTE: make contents row vector to permit simple collapse
	
	in{k} = in{k}(:)';

end

%--
% collapse content arrays and output
%--

% NOTE: why are we using the transpose here?

out = [in{:}]';