function pages = get_scan_pages(scan)

[page, scan] = get_scan_page(scan); pages = empty(page);

while ~isempty(page)
	pages(end + 1) = page; [page, scan] = get_scan_page(scan);
end
