function fun = signatures

% sound_feature - function handle structure
% -----------------------------------------
%
% fun = signatures
%
% Output:
% -------
%  fun - structure for extension type API

fun.prepare = {{'result'}, {'parameter', 'context'}};

fun.compute = {{'feature', 'context'}, {'page', 'parameter', 'context'}};

fun.conclude = {{'result'}, {'parameter', 'context'}};

fun.reduce = {{'feature', 'context'}, {'feature', 'parameter', 'context'}};

fun.parameter = param_fun;

% NOTE: the feature data should be a widget data field

fun.view = get_extension_type_signatures('widget');

%--
% rendering of features to the filesystem
%--

fun.render = {{'result'}, {'parameter', 'context'}};

% NOTE: this is a projected direction for the API

% fun.render.prepare = {{'result'}, {'parameter', 'context'}};
% 
% fun.render.compute = {{'result'}, {'parameter', 'context'}};
% 
% fun.render.prepare = {{'result'}, {'parameter', 'context'}};

