function feature_refresh(context)

% feature_refresh - computation when needed
% -----------------------------------------
% 
% feature_refresh(context)
%
% Input:
% ------
%  context - of refresh

% NOTE: we can get all we need from the context provided the extension is updated and correct

%--
% return when feature is not active
%--

if ~extension_is_active(context.ext, context.par)
	return;
end

%--
% recompute feature
%--

[ext, ignore, context] = get_browser_extension(context.ext.type, context.par, context.ext.name);

compute_features(context.ext, context.sound, context.page, context);

%--
% update feature display
%--

widget = find_widget(context.par, context.ext);

if isempty(widget)
	return;
end

browser_display(context.par, 'events');

% NOTE: the commented function is quite far from being implemented, is there any gain?

% display_events_widget_update(widget, context);

widget_update(context.par, widget, 'page');

set_browser_marker(context.par);

selection_update(context.par);