function result = clear_previous_widget_display(widget, target)

% clear_previous_widget_display - what it says
% ---------------------------------------------
%
% result = clear_previous_widget_display(widget, target)
%
% Input:
% ------
%  widget - widget handle
%  target - 'all' or 'event' (def: 'all')
%
% Output:
% -------
%  result - result struct

%--
% set default target all
%--

if nargin < 2
	target = 'all';
end

%--
% handle multiple parents through iteration
%--

if numel(widget) > 1
	result = iterate(mfilename, widget, target); return; 
end

%--
% delete previous display handles
%--

% NOTE: this uses managed handle tagging conventions established in 'event_view', 'event_tag', and 'compute_features'

switch target
	
	case 'all'
		tags = delete_obj(widget, 'previous-display', @string_contains);
		
	case 'event'
		tags = delete_obj(widget, 'EVENT::', @string_contains);
		
end

%--
% pack result if needed
%--

% TODO: tags may be different, so there is some information, consider if we want it?

if nargout
	result.deleted = numel(tags);
end
