function [value, context, page, key] = get_page_feature(page, ext, context)

% get_page_feature - get feature for page
% ---------------------------------------
%
% [value, context, page, key] = get_page_feature(page, ext, context)
%
% Input:
% ------
%  page - scan page
%  ext - feature extension
%  context - feature context
%
% Output:
% -------
%  value - feature value
%  context - possibly updated context
%  page - possibly updated page
%  key - cache key

%--
% try to get feature from cache
%--

[feature, key] = get_cache_feature(page, ext, context);

if ~trivial(feature)
	value = feature.value; context = feature.context; return;
end

%--
% compute feature 
%--

try
	[value, context] = ext.fun.compute(page, ext.parameter, context);
catch
	value = []; extension_warning(ext, 'Error in compute function.', lasterror);
end

%--
% cache feature in page
%--

if (nargout > 1) && ~isempty(value)
	feature.value = feature; feature.context = context; page.cache.(key) = feature;
end


%------------------------------
% GET_CACHE_FEATURE
%------------------------------

function [feature, key] = get_cache_feature(page, ext, context)

%--
% compute key
%--

% TODO: make sure key is sufficient!

% TODO: allow scheme to take advantage of partial channel computation

key.page.start = page.start; key.page.duration = page.duration; key.page.channels = page.channels;

key.ext.name = ext.name; key.ext.type = ext.subtype; key.ext.parameters = ext.parameters;

key.context.par = context.par;

key = ['FEAT_', md5(key)];

%--
% retrieve feature
%--

if isfield(page, 'cache') && isfield(page.cache, key)
	feature = page.cache.(key);
else
	feature = struct;
end
