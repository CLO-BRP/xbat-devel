function listen = get_listener_string(ext)

% TODO: we must clear this cache when a widget extension is modified

%--
% create and check persistent store
%--

persistent LISTEN;

if isempty(LISTEN) 
	LISTEN = struct;
end

if isfield(LISTEN, ext.fieldname)
	listen = LISTEN.(ext.fieldname); return;
end

%--
% get events and initialize listen
%--

event = get_widget_events; listen = '';

%--
% get prefix based on extension type
%--

% TODO: consider other widget parent extension types

switch ext.subtype
	
	case {'widget', 'sound_browser_palette'}, prefix = 'ext.fun.on.';
	
	case {'sound_feature', 'sound_detector'}, prefix = 'ext.fun.view.on.';
		
	otherwise
		
		% TODO: consider other possible locations
		
		try
			test = ext.fun.view.on; prefix = 'ext.fun.view.on.'; %#ok<NASGU>
		catch
			extension_warning(ext, 'Unable to get widget function handles.');
		end
		
end

%--
% test if we know how to listen to each event
%--

for k = 1:length(event)
	
	% NOTE: this is effectively a struct 'collapse' on a string
	
	fun = eval([prefix, strrep(event{k}, '__', '.')]);
	
	if isempty(fun)
		listen(end + 1) = '0';
	else
		listen(end + 1) = '1';
	end
	
end

%--
% pack listener string in cache
%--

LISTEN.(ext.fieldname) = listen;
