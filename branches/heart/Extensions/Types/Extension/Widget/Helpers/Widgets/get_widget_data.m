function data = get_widget_data(par, event, data)

% get_widget_data - get data for widget that handles event
% --------------------------------------------------------
%
% data = get_widget_data(par, event, data)
%
% Input:
% ------
%  par - widget parent
%  event - event data
%  data - parent state
%
% Output:
% -------
%  data - parent event data

%--------------------
% HANDLE INPUT
%--------------------

%--
% get parent data if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%--------------------
% GET DATA
%--------------------

% NOTE: we try to provide easy to use browser state data for widget

% NOTE: some of these ideas will be integrated into state

%--
% sound fields
%--

sound = data.browser.sound;

% NOTE: is this currently done during context construction? in any case, it should!

sound.rate = get_sound_rate(sound);

%--
% page fields
%--

page.start = data.browser.time;

page.duration = data.browser.page.duration;

page.channels = get_channels(data.browser.channels);

% NOTE: page we use in most parts of system does not include this field

page.freq = data.browser.page.freq;

if isempty(page.freq)
	page.freq = [0, 0.5 * sound.rate];
end

%--
% player fields
%--

% NOTE: the player is singleton, we expect that the parent is in context

% TODO: consider checking the parent condition on the player

[buffer, player] = get_player_buffer;

%--
% selection and marker fields
%--

selection = data.browser.selection;

marker = data.browser.marker;

%--
% event-specific fields
%--

if ~isempty(event)

	if ~ismember(event, get_widget_events)
		error('Unrecognized widget event.');
	end
	
	%--
	% collect and pack event specific data
	%--

	% NOTE: it seems like this happens in unique ways for some events in the event generator code

end

%--------------------
% PACK DATA
%--------------------

% NOTE: currently we are only adding fields to the parent userdata

% NOTE: access to the browser state is currently used in the 'Spectrum' widget extension

data.sound = sound;

% NOTE: this is the browser display page, 'on__compute' calls get a compute scan page

data.page = page;

data.player = player;

data.buffer = buffer;

data.selection = selection;

data.marker = marker;

%--------------------
% ADD DEPRECATED
%--------------------

% NOTE: this information will change, impacting any widgets that use this data

% TODO: provide structured spectrogram data, handles and options
		
data.spectrogram = data.browser.images;

