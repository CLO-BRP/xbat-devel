function result = is_widget_event(event)

% is_widget_event - test for string
% ---------------------------------
%
% result = is_widget_event(event)
%
% Input:
% ------
%  event - proposed
% 
% Output:
% -------
%  result - of test

result = string_is_member(event, get_widget_events); 