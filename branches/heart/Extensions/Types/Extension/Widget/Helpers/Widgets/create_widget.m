function [widget, created] = create_widget(par, ext, context)

% create_widget - create widget figure
% ------------------------------------
%
% [widget, created] = create_widget(par, ext, context)
%
% Input:
% ------
%  par - parent figure
%  ext - widget extension
%  context - context
%
% Output:
% -------
%  widget - widget figure
%  created - indicator

%-------------------
% HANDLE INPUT
%-------------------

%--
% check for widget and enforce singleton condition
%--

[widget, tag] = find_widget(par, ext);

if ~isempty(widget)
	
	pos = get(widget, 'position');
	
	if any(pos < 0)
		pos(pos < 0) = 0; set(widget, 'position', pos);
	end
	
	created = 0; return;

end

%--
% get context (and updated extension) if needed
%--

% NOTE: it is not clear that we want to overwrite the extension?

if (nargin < 3) || isempty(context)
	[ext, ignore, context] = get_browser_extension(ext.subtype, par, ext.name);
end

%-------------------
% CREATE WIDGET
%-------------------

%--
% create figure, name and tag
%--

if strcmp(ext.type, 'widget')
	name = [ext.name, ' Widget'];
else	
	switch ext.type
		case 'sound_feature', part = ' Feature';
			
		case 'sound_detector', part = ' Detector';
			
		otherwise, part = '';
			
	end
	
	name = [ext.name, part]; % , ' View'];	
end

widget = figure;

set(widget, ...
	'name', name, ...
	'numbertitle', 'off', ...
	'dockcontrols', 'off', ...
	'renderer', 'painters', ...
	'backingstore', 'on', ...
	'doublebuffer', 'on', ...
	'tag', tag ...
);

% NOTE: parsing the tag here add it to the parsed cache, at this point we know we have a widget

parse_widget_tag(tag);

%--
% store extension state in figure
%--

% NOTE: this is compatible with control palette extensions (this is a duck)

% TODO: the parameters in this extension can become stale, careful use until this is solved

data.opt.ext = ext;

data.parent = par;

% NOTE: this could become stale, context should be used carefully in widgets

data.context = context;

data.created = clock;

set(widget, ...
	'userdata', data, 'closerequestfcn', {@widget_close, par} ...
);

%--
% get widget handles branch from extension
%--

% NOTE: the 'widget' handles are part of other extension 'views'

if strcmp(ext.type, 'widget')
	fun = ext.fun;
else
	fun = ext.fun.view;
end

%--
% set keypress callback if available, otherwise we delegate to parent
%--

if isempty(fun.on.keypress)
	set(widget, 'keypressfcn', {@browser_keypress_callback, par});
else
	set(widget, 'keypressfcn', {@widget_keypress, ext, par});
end

%--
% set resize callback if available
%--

% NOTE: this may belong in 'widget_update', that way we check if layout clears resize, and we may add this again

if ~isempty(fun.on.resize)
	
	append_resize(widget, {@widget_resize, ext, par});
	
end

%--
% add extension menus
%--

extension_menus(widget, ext, context);

% NOTE: this is no longer required after patching 'is_extension_palette', still not clean

% % NOTE: we need to restore tag clobbered during menu creation
% 
% set(widget, 'tag', tag);

%--
% set widget position, possibly using state
%--

data = get_browser(par);

if ~isempty(data.browser.widget_states)
	state = data.browser.widget_states(strcmp(name, {data.browser.widget_states.name}));
else
	state = [];
end

if ~isempty(state)
	
	set_widget_state(widget, state);
	
else
	
	if ~isempty(fun.position)		
		try
			fun.position(widget, ext.parameters.view, context);
		catch
			extension_warning(ext, 'Positioning failed.', lasterror);
		end
	end
	
end

%-------------------
% UPDATE WIDGET
%-------------------

% NOTE: relevant events may have occured before widget was created 

% NOTE: widget data depends on parent and event, it is different for each update

%--
% page update
%--

% NOTE: surely we are on some page, also the last argument forces layout

widget_update(par, widget, 'page', [], 1); 

%--
% marker update
%--

% TODO: generate marker create event, add marker to general widget data!

%--
% selection update
%--

% NOTE: we may have missed when a selection was made

[ignore, count] = get_browser_selection(par);

if count
	widget_update(par, widget, 'selection__create');
end

%--
% play started update
%--

% NOTE: we may have missed when a current player started

if ~isempty(get_player_buffer)
	widget_update(par, widget, 'play__start');
end

created = 1;


%---------------------------
% WIDGET_KEYPRESS
%---------------------------

function widget_keypress(obj, eventdata, ext, par)

% TODO: this is relatively expensive, can we cache this slowly changing data

[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name);

if strcmp(ext.type, 'widget')
	fun = ext.fun; parameter = ext.parameter;
else
	fun = ext.fun.view; parameter = ext.parameters.view;
end

fun.on.keypress(obj, eventdata, parameter, context);


%---------------------------
% WIDGET_RESIZE
%---------------------------

function widget_resize(obj, eventdata, ext, par)

%--
% keep persistent list of positions
%--

persistent LAST;

if isempty(LAST) 
	LAST.widget = obj; LAST.position = get(obj, 'position'); return
end 

%--
% check for last position
%--

ix = find([LAST.widget] == obj, 1);

if isempty(ix)
	LAST(end + 1).widget = obj; LAST(end).position = get(obj, 'position'); return;
end

%--
% get and pack the resize event data
%--

current = get(obj, 'position');

change = current - LAST(ix).position;

% NOTE: the tolerance here is somewhat conmesurate with the tolerance used in 'get_resize_type'

if sum(abs(change(3:4))) < 16
	return; 
end

type = get_resize_type(change);

data.position = current; data.change = change; data.type = type;

% NOTE: a valid and typical source for this is translation, we need to update our position

if isempty(type)
	LAST(ix).position = get(obj, 'position'); return;  
end

%--
% call resize event handler
%--

% TODO: this is relatively expensive, can we cache this slowly changing data

[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name);

if strcmp(ext.type, 'widget')
	fun = ext.fun; parameter = ext.parameter; 
else
	fun = ext.fun.view; parameter = ext.parameters.view;
end

try
	fun.on.resize(obj, data, parameter, context);
catch
	extension_warning(ext, 'Widget ''resize'' callback failed.', lasterror);
end

%--
% update last position 
%--

% NOTE: we get the position again, in case the callback modified it

LAST(ix).position = get(obj, 'position');


%---------------------------
% GET_RESIZE_TYPE
%---------------------------

function type = get_resize_type(change)

% TODO: reconsider and refine the use of the tolerance

% NOTE: the tolerance was empirically determined, ideally related values should be zero

tol = 4; type = ''; 

% TRBL

if all(abs(change(1:3)) < tol)
	type = 'top'; return;
end

if all(abs(change([1,2,4])) < tol)
	type = 'right'; return;
end

if all(abs(change([1,3])) < tol) && sum(change([2,4])) == 0
	type = 'bottom'; return;
end

if all(abs(change([2,4])) < tol) && sum(change([1,3])) == 0
	type = 'left'; return;
end 

% MIXED

if abs(change(2)) < tol && abs(sum(change([1,3]))) < tol
	type = 'top-left'; return;
end

if abs(sum(change([1,3]))) < tol && abs(sum(change([2,4]))) < tol
	type = 'bottom-left'; return;
end

if abs(sum(change(1:2))) < tol
	type = 'top-right';  return;
end

if abs(change(1)) < tol && abs(sum(change([2,4]))) < tol
	type = 'bottom-right'; return;
end

% TODO: 'top-right' and 'bottom-right'





