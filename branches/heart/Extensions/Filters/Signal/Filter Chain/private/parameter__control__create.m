function control = parameter__control__create(parameter, context)

% FILTER CHAIN - parameter__control__create

% control = empty(control_create);

parameter = parameter__compile(parameter, context);

control = extension_chain_controls('signal_filter', parameter, context);