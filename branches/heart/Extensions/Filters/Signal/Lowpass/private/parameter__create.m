function parameter = parameter__create(context)

% LOWPASS - parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

nyq = 0.5 * context.sound.rate;

parameter.min_freq = 0;

parameter.max_freq = 0.5 * nyq;
