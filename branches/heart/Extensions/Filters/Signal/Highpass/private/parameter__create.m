function parameter = parameter__create(context)

% HIGHPASS - parameter__create

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

nyq = context.sound.rate / 2;

parameter.min_freq = 0.5 * nyq;

parameter.max_freq = nyq;
