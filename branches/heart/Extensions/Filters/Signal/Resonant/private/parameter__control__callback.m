function result = parameter__control__callback(callback, context)

% RESONANT - parameter__control__callback

%--
% consider selection configuration
%--

switch callback.control.name

	case 'sel_config'

		[selection, count] = get_browser_selection(callback.par.handle);

		if count
			
			set_control(callback.pal.handle, ...
				'center_freq', 'value', mean(selection.event.freq) ...
			);
		
			set_control(callback.pal.handle, ...
				'width', 'value', diff(selection.event.freq) ...
			);
		
		end

end

%--
% update display
%--

fun = parent_fun(mfilename('fullpath')); result = fun(callback, context);
