function parameter = parameter__create(context)

% RESONANT - parameter__create

%--
% get parent parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% get nyquist from context
%--

nyq = 0.5 * context.sound.rate;

%--
% filter parameters
%--

parameter.center_freq = 0.5 * nyq;

parameter.width = 0.2 * nyq;
