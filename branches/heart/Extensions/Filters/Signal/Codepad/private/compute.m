function [X, context] = compute(X, parameter, context)

% CODEPAD - compute

%--
% copy and clear parameter
%--

input = parameter; clear parameter;

%--
% evaluate filter code
%--

% TODO: consider multi-channel computation

% TODO: factor string transformation and test into an eval utility

str = get_eval_string(input.code);

if ~isempty(str)
	eval(str);
end


%-------------------------
% GET_EVAL_STRING
%-------------------------

function str = get_eval_string(in)

opt = file_readlines; opt.pre = '%';

str = str_implode(file_readlines(in, [], opt));
