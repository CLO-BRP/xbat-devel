function result = parameter__control__callback(callback, context)

% WAVESHRINK - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'method', return;
		
	case 'wavelet' 
		
		value = cellfree(callback.value);
		
		%--
		% enable or disable 'order' control
		%--
		
		set_control(callback.pal.handle, 'index', ...
			'enable', ~string_is_member(value, {'Beylkin', 'Haar', 'Vaidyanathan'}) ...
		);
	
		%--
		% update 'index' control properties to reflect family
		%--
		
		switch value
			
			case 'Battle'
				
			case 'Coiflet'
				
			case 'Daubechies'
				
			case 'Symmlet'
				
		end
		
end

%--
% update wavelet display 
%--

update_wavelet_plot(callback.pal.handle);


%-----------------------------
% UPDATE_WAVELET_PLOT
%-----------------------------

function update_wavelet_plot(pal)

%--
% get relevant values
%--

wavelet = cellfree(get_control_value(pal, 'wavelet'));

index = cellfree(get_control_value(pal, 'index'));

%--
% get axes control handles
%--

handles = get_control_handles(pal, 'plot'); ax = handles.obj;

% NOTE: various numbers here are magic numbers from toon0111.m

time = (1:1024)./1024; wave = MakeWavelet(4, 8, wavelet, index, 'Mother', 1024); start = 300; stop = 800; 

time = time(start:stop); wave = wave(start:stop);

[handle, created] = create_line(ax, 'wavelet');

if created
	set(ax, 'xlim', time([1, end]));
end

set(ax, 'ylim', 1.1 * fast_min_max(wave));

set(handle, 'xdata', time, 'ydata', wave);



