function result = parameter__control__callback(callback, context)

% IMPULSE-NOISE - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'generate'
		
		set_control(callback.pal.handle, 'seed', 'value', int2str(rand_ab(1, 0, 10^8)));
		
end