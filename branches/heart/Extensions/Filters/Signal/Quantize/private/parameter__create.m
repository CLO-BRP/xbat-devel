function parameter = parameter__create(context)

% QUANTIZE - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.map = @quantize;

parameter.bits = 4;


function X = quantize(X, parameter)

% TODO: improve this function, it has a large step between negative and positive

N = 2^(parameter.bits - 1);

X = N * X; 

X(X < 0) = floor(X(X < 0));

X(X >= 0) = ceil(X(X >= 0));

X = X / N;
