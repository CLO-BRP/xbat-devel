function [X, context] = compute(X, parameter, context)

% LOWPASS-BUTTERWORTH - compute

fun = parent_fun(mfilename('fullpath')); [X, context] = fun(X, parameter, context);
