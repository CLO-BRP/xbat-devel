function ext = root_distortion_signal_filter

ext = extension_inherit(mfilename, point_operation_base_signal_filter);

ext.short_description = 'Distort signal through root transformation';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'c03b53dd-f219-427a-83e1-719b15948977';

% ext.author = '';

% ext.email = '';

% ext.url = '';

