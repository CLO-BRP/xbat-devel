function control = parameter__control__create(parameter, context)

% DIFFERENCE - parameter__control__create

%--
% get parent controls
%--

fun = parent_fun(mfilename('fullpath')); control = fun(parameter, context);

control(end).space = 1;

% TODO: remove configure with selection button here rather than disable in callback!

%--
% add order and normalize controls
%--

control(end + 1) = control_create( ...
	'name', 'order', ...
	'style', 'slider', ...
	'type', 'integer', ...
	'min', 1, ...
	'max', 16, ...
	'value', parameter.order, ... 
	'space', 0.75 ...
);

% TODO: consider renaming parameter field

control(end + 1) = control_create( ...
	'name', 'normalize', ...
	'alias', 'unit gain', ...
	'style', 'checkbox', ...
	'value', parameter.normalize, ... 
	'space', 1 ...
);
