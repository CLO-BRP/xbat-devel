function result = parameter__control__callback(callback, context)

% TEMPORAL_FILTER_BASE - parameter__control__callback

result = struct;

%--
% return if there is no callback fun to be had
%--

fun = context.ext.parameter.ext.fun.parameter.control.callback;

if isempty(fun)
	return;
end

%--
% compile hosted extension parameters
%--

% NOTE: typically filters want to know the rate when creating parameters or controls

% NOTE: we have done this when creating parameters, this is paranoia

context.sound.rate = get_specgram_rate(context.sound.specgram, get_sound_rate(context.sound));

result = fun(callback, context);