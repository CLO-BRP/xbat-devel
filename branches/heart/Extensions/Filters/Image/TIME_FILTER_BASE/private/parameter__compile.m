function [parameter, context] = parameter__compile(parameter, context)

% TEMPORAL_FILTER_BASE - parameter__compile

%--
% return if we have no hosted extension
%--

if trivial(parameter.ext)
	return;
end

%--
% return if there is no compile fun to be had
%--

fun = parameter.ext.fun.parameter.compile;

if isempty(fun)
	return;
end

%--
% compile hosted extension parameters
%--

% NOTE: typically filters want to know the rate when creating parameters or controls

% NOTE: we have done this when creating parameters, this is paranoia

context.sound.rate = get_specgram_rate(context.sound.specgram, get_sound_rate(context.sound));

[parameter, context] = fun(parameter, context);