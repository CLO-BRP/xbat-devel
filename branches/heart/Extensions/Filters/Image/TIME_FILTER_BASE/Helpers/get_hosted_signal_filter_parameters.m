function parameter = get_hosted_signal_filter_parameters(parameter, context)

%--
% initialize hosted extension using hacked context
%--

% NOTE: set context sound rate to spectrogram rate

ext = get_extensions('signal_filter', 'name', parameter.hosted);

context.sound.rate = get_specgram_rate(context.sound.specgram, get_sound_rate(context.sound));

parameter.ext = extension_initialize(ext, context);

%--
% copy signal parameter fields to parameters
%--

% TODO: consider using the new parameter store

fields = fieldnames(parameter.ext.parameter);

for k = 1:numel(fields)
	parameter.(fields{k}) = parameter.ext.parameter.(fields{k});
end