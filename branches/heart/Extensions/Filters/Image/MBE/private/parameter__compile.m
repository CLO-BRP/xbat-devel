function [parameter, context] = parameter__compile(parameter, context)

% MBE - parameter__compile

% NOTE: we convert the desired duration into a number of spectrogram slices

delta = specgram_resolution(context.sound.specgram, get_sound_rate(context.sound));

parameter.length = ceil(parameter.duration / delta);

if parameter.length == 1
	parameter.length = 2;
end

parameter.duration = delta * parameter.length;

if isfield(context, 'pal') && ~isempty(context.pal)
	set_control(context.pal, 'duration', 'value', parameter.duration);
end
