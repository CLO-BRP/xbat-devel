function parameter = parameter__create(context)

% GRADIENT (MORPHOLOGICAL) - parameter__create

%--
% get parent parameters
%--

fun = parent_fun(mfilename('fullpath'));

parameter = fun(context);

%--
% add gradient type parameter
%--

[types,ix] = gradient_types;

parameter.type = {types{ix}};
