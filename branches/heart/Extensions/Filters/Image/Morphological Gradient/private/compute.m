function [X, context] = compute(X, parameter, context)

% GRADIENT (MORPHOLOGICAL) - compute

SE = create_se(parameter);

% NOTE: output from listbox is contained in cell

code = gradient_type_code(parameter.type{1});

X = morph_gradient(X, SE, code);