function ext = median_image_filter

ext = extension_inherit(mfilename, morphological_base_image_filter);

ext.short_description = 'Local median';

ext.category = {ext.category{:}, 'Blur', 'Statistical'};

% ext.version = '';

ext.guid = 'ba3f01ba-239d-455d-863e-25fcd606707a';

% ext.author = '';

% ext.email = '';

% ext.url = '';

