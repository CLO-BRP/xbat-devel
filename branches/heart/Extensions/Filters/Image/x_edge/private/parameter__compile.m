function parameter = parameter__compile(parameter,context)

% X_EDGE - parameter__compile

parameter.filter = filt_binomial(3, 2 * round(parameter.edge_scale) + 1, [0, 1]);
