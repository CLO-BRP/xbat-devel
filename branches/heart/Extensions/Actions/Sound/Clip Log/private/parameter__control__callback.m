function result = parameter__control__callback(callback, context)

% CLIP LOG - parameter__control__callback

result = struct;

switch callback.control.name
	
	case 'name'
		
		set_control(callback.pal.handle, 'OK', 'enable', proper_filename(get(callback.obj, 'string')));
		
end