function ext = detect_sound_action

ext = extension_create(mfilename);

ext.short_description = 'Scan a sound with an existing detector';

% ext.category = {};

ext.version = '0.1';

ext.guid = '9f80a1fe-18e3-48fb-b5e7-a59f3c506f20';

ext.author = 'Harold Figueroa';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

