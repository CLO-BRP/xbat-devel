function [result, context] = compute(sound, parameter, context)

% SET SPECTROGRAM - compute

result = struct;

%--
% check for open sound and close if needed
%--

[open, par] = sound_is_open(sound, context.library);

if open
	close(par); reopen.sound = sound; reopen.library = context.library;
end

%--
% update spectrogram parameters and save sound
%--

% TODO: make sure that these make sense for the current sound

if context.debug
	db_disp before; sound.specgram
end

parameter = struct_iterate(parameter, @(value)(cellfree(value)));

sound.specgram = struct_update(sound.specgram, parameter);

if context.debug
	db_disp after; sound.specgram
end

sound_save(context.library, sound);

%--
% update reopen structure
%--

% NOTE: we will re-open any closed sounds during conclude

if open
	
	if ~isfield(context.state, 'reopen')
		context.state.reopen = struct;
	end

	context.state.reopen(end + 1) = reopen;
end