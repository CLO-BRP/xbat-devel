function [result, context] = compute(sound, parameter, context)

% GET DATE TIME - compute

result = struct;

if iscell(sound.file)
	file = sound.file{1};
else
	file = sound.file;
end

value = file_datenum(file, parameter.pattern);

if ~isempty(value)
	% NOTE: the context helper is the 'date_time' extension, we set this in 'prepare'
	
	helper = context.helper; helper.context.sound = sound;
	
	attribute.datetime = value; file = get_attribute_file(helper.context, helper.ext.name);
	
	try
		helper.ext.fun.save(attribute, file, helper.context);
	catch
		extension_warning(helper.ext, 'Failed to save attribute.', lasterror);
	end
end