function parameter = parameter__create(context)

% GET DATE TIME - parameter__create

parameter = struct;

parameter.pattern = 'yyyymmdd_HHMMSS';