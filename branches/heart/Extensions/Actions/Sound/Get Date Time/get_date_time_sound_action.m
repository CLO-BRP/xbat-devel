function ext = get_date_time_sound_action

ext = extension_create(mfilename);

ext.short_description = 'Get date and time information from files';

ext.category = {'BASIC'};

ext.version = '0.1';

ext.guid = '5351d0b8-3f32-40b3-a90f-08bee1452dd6';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = '';

