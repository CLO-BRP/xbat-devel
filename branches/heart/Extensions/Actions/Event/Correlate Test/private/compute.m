function [result, context] = compute(event, parameter, context)

% CORRELATE TEST - compute

result = struct;

%--
% read page samples and compute spectrograms for page and event
%--

page = context.page; nch = numel(page.channels);

page.samples = sound_read(context.sound, 'time', page.start, page.duration, page.channels);

page.spectrogram = fast_specgram(page.samples, context.sound.rate, 'norm', context.sound.specgram);

[event.spectrogram, time, freq] = fast_specgram(event.samples, context.sound.rate, 'norm', context.sound.specgram);

if ~mod(size(event.spectrogram, 2), 2)
	event.spectrogram(:, end) = [];
end 

%--
% correlate event spectrogram with page spectrogram
%--

% TODO: we should not have to multiply to get Hertz here, fix this

% NOTE: select rows to work with considering event frequency bounds

freq = 1000 * freq; ix = find(freq > event.freq(1) & freq < event.freq(2));

% NOTE: we need to configure the image correlation code for this special case, consider encapsulating

opt = image_corr; opt.pad_row = 0; opt.pad_col = 1;
	
for k = 1:nch
	result.correlation{k} = image_corr(event.spectrogram(ix, :), page.spectrogram{k}(ix, :), opt);
end

%--
% create figure and display correlation
%--

% TODO: this could be greatly improved, also consider getting the top hits and lags

figure; 

for k = 1:nch
	subplot(nch, 1, k); title(['channel ', int2str(page.channels(k))]); plot(result.correlation{k});
end


