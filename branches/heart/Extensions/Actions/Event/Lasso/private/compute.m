function [result, context] = compute(event, parameter, context)

% LASSO EVENT - compute

% TODO: make the various parameters parameters

result = struct;

event = event_specgram(event, context.sound);

[back, N] = background_estimate(event);

% NOTE: a fraction 'alpha' of the bins will be mapped to zero

clean = max(0, event.specgram.value - repmat(back, [1, N]));

event.freq = band_estimate(clean, event.specgram.freq);

event.time = time_estimate(event);

event.level = 1; % NOTE: this should not be needed!

% NOTE: this action will modify the event!

result.event = event;


%--------------------------
% BACKGROUND ESTIMATE
%--------------------------

function [back, N] = background_estimate(event)

% NOTE: estimate the background using a fraction the lowest-valued bins

alpha = 0.25; 

N = size(event.specgram.value, 2); n = ceil(alpha * N);

value = sort(event.specgram.value, 2);

back = sum(value(:, 1:n), 2);


%--------------------------
% BAND_ESTIMATE
%--------------------------

function [freq, center] = band_estimate(clean, freq)

alpha = 0.0125;

center = mean(clean, 2);

% NOTE: the factor of two is a heuristic value to compensate for remaining background

edge = get_cdf_interval(center, 2 * alpha, alpha);

freq = freq(edge)';


%--------------------------
% TIME_ESTIMATE
%--------------------------

function time = time_estimate(event)

% TODO: filter signal using band estimate before computing this time estimate

alpha = 0.0125;

energy = event.samples.^2;

edge = get_cdf_interval(energy, alpha);

% time = linspace(event.time(1), event.time(2), numel(event.samples));

% time = time(edge);

time = event.time(1) + edge / event.rate;


%--------------------------
% GET_CDF_INTERVAL
%--------------------------

% TODO: this function should be factored, it should be useful elsewhere

function [edge, cdf] = get_cdf_interval(x, alpha, beta)

%--
% handle input
%--

if nargin < 2 || isempty(alpha)
	alpha = 0.05;
end

if nargin < 3
	beta = alpha; 
end

%--
% compute cumulative distribution and edge indices
%--

cdf = cumsum(x(:)); cdf = cdf ./ cdf(end);

ix1 = find(cdf < alpha, 1, 'last'); ix2 = find(cdf > 1 - beta, 1);

if isempty(ix1)
	ix1 = 1;
end

if isempty(ix2)
	ix2 = numel(cdf);
end

edge = [ix1, ix2];


