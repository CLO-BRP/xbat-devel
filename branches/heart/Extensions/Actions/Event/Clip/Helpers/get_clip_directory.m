function output = get_clip_directory

try
	output = get_windows_desktop;
catch
	output = app_root;
end