function time_stamp = create_log_clips(log, location, ext, pad)

if nargin < 4 || isempty(pad)
    pad = 0;
end

%--
% initialize time stamp table
%--

time_stamp = [0, 0];

%--
% get intervals from log events and padding parameter
%--

interval = get_log_intervals(log, pad);

%--
% traverse log intervals
%--

record_time = 0;

for k = 1:length(interval.start)
    
    time = [interval.start(k), interval.stop(k)]; duration = diff(time);
    
    %--
    % name files according to standard date_time
    %--
    
    if ~isempty(log.sound.realtime)
        timestr = datestr((time(1) / 86400) + log.sound.realtime, 30);
    else
        timestr = datestr(time(1) / 86400, 'HHMMSS');
    end
    
    filename = fullfile(location, ['file_', timestr, '.' ext]);
    
    %--
    % create clips
    %--
    
    % NOTE: sound_read requires slider time
    
    if ~isempty(log.sound.time_stamp)
        log.sound.time_stamp.collapse = 0;
    end
    
    slider_time = map_time(log.sound, 'slider', 'real', time);
    
    X = sound_read(log.sound, 'time', slider_time(1), duration);
    
    %--
    % write clip to file
    %--
    
    sound_file_write(filename, X, get_sound_rate(log.sound));
    
    %--
    % write time stamp for this interval
    %--
      
    time_stamp(k, :) = [record_time, interval.start(k)];
    
    record_time = record_time + duration;
    
end

%----------------------------------------
% GET_LOG_INTERVALS
%----------------------------------------

function interval = get_log_intervals(log, pad)

if nargin < 2
    pad = 0;
end

%--
% get union of event intervals
%--

% NOTE: everything gets mapped out to real time

for k = 1:length(log.event)

    interval.start(k) = map_time( ...
        log.sound, 'real', 'record', log.event(k).time(1) - pad ...
    );
    
    interval.stop(k) = map_time( ...
        log.sound, 'real', 'record', log.event(k).time(2) + pad ...
    );
    
end

%--
% expand intervals to nearest second boundaries
%--

interval.start = floor(interval.start(:)); interval.stop = ceil(interval.stop(:));

interval = interval_union(interval);

%--
% intersect with sound scan
%--

scan = get_sound_scan(log.sound);

interval.start = [scan.start(:);interval.start(:)]; 

interval.stop = [scan.stop(:);interval.stop(:)];

interval = interval_intersect(interval);




