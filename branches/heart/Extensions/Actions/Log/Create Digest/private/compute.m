function [result, context] = compute(log, parameter, context)

% DIGEST - compute

result = struct;

%--
% create location
%--

name = [log_name(log), '_', context.ext.name];

location = fullfile(parameter.location, name);

create_dir(fullfile(location, 'Data', name));

%--
% create sound files and time stamps
%--

attribute.table = create_log_clips( ...
    log, fullfile(location, 'Data', name), parameter.extension, parameter.pad ...
);

attribute.enable = 1; attribute.collapse = 0;

%--
% create a new sound
%--

new_sound = sound_create('file stream', fullfile(location, 'Data', name));

%--
% write time stamp attribute for new sound
%--

att_context.sound = new_sound; att_context.library = [];

ext = get_extensions('sound_attribute', 'name', 'time_stamps');

[file, exists] = get_attribute_file(att_context, ext.name);

%--
% save attribute to file
%--

if ~exists
    create_dir(fileparts(file)); 
end
    
try
	ext.fun.save(attribute, file, att_context);
catch
	extension_warning(ext, 'Save failed.', lasterror);
end

%--
% save sound
%--

new_sound.time_stamp = attribute;

new_sound.consolidated = 1;

sound_save(location, new_sound);

%--
% create new log
%--

% TODO: this should call a 'new_log' function 

new_log = log;

new_log.sound = new_sound;

new_log.path = fullfile(location, 'Logs', filesep);

create_dir(new_log.path);

%--
% map event times
%--

for k = 1:length(log.event)
   
    %--
    % map out to real time, then back to record time in new sound
    %--
    
    real_time = map_time(log.sound, 'real', 'record', log.event(k).time);
    
    new_time = map_time(new_sound, 'record', 'real', real_time);
    
    new_log.event(k).time = new_time;
    
end

log_save(new_log);



