function ext = strip_log_action

ext = extension_create(mfilename);

ext.short_description = 'Strip event measurements from logs';

% ext.category = {};

ext.version = '0.1';

ext.guid = '7fed7b5a-defb-428d-8159-521bd0704f94';

ext.author = 'Harold Figueroa';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

