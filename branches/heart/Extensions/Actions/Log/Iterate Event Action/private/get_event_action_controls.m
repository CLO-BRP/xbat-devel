function control = get_event_action_controls(action, context)

% TODO: the callback for the event action controls needs to be routed through the event action callback

% TODO: preserve the action parameters through changes in the selected action, by updating the extension parameters

%--
% get event action
%--

if isempty(context.par) 
	context.par = 0;
end

[ext, ignore, context2] = get_browser_extension('event_action', context.par, action);

%--
% get event action controls
%--

if isempty(ext.fun.parameter.control.create)
	
	control = empty(control_create); return;
	
else
	
	try
		control = ext.fun.parameter.control.create(ext.parameter, context2);
	catch
		extension_warning(ext, 'Control creation failed.', lasterror);
	end
	
end
