function parameter = parameter__create(context)

% CORRELATE EVENTS - parameter__create

parameter = struct;

%--
% get base spectrogram feature and parameters
%--

parameter.feature = get_extension_using_context('sound_feature', 'Spectrogram', context);

parameter.spectrogram = parameter.feature.parameters;

%--
% correlation specific parameters
%--

parameter.bandwidth = 100;

parameter.threshold = 0.6;

parameter.distortion = 1.5;