function [handles, context] = on__selection__create(widget, data, parameter, context)

% EVENT 2 - on__selection__create

handles = [];

% TODO: this suggests a generalization of the widget signature, output 'result' typically including 'handles' field

% db_disp 'the code below does not belong here, this is part of testing'

selection = data.selection;

%--
% determine whether annotation controls are enabled and update accordingly
%--

enabled = ~trivial(selection.log);
	
% TODO: fix enable and disable for 'rating' controls

toggle = {'tags', 'rating', 'notes'};

for k = 1:numel(toggle)
	
	set_control(widget, toggle{k}, 'enable', enabled);

end

%--
% display values on enabled controls
%--

if enabled
	
	set_control(widget, 'known_tags', 'string', setdiff(get_log_tags(selection.log), selection.event.tags));
	
	set_control(widget, 'tags', 'value', tags_to_str(selection.event.tags));

	set_control(widget', 'rating', 'value', selection.event.rating);
	
else
	
	set_control(widget, 'tags', 'value', '');

	set_control(widget', 'rating', 'value', 0);
	
	set_control(widget, 'notes', 'value', '');
	
end