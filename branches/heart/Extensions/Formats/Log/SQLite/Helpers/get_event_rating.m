function user = get_event_rating(varargin)

% result = get_from_view(file, view, id, field, where, order)

user = get_from_view(varargin{1}, mfilename, varargin{2:end}); 