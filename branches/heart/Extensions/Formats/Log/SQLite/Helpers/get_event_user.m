function user = get_event_user(varargin)

% result = get_event_user(file, id, field, where, order)

% result = get_from_view(file, view, id, field, where, order)

start = clock;

user = get_from_view(varargin{1}, mfilename, varargin{2:end}); 

db_disp; elapsed = etime(clock, start)

% TODO: there are typically few users, get relevant ones and 'join' in MATLAB

% NOTE: this function may show a larger problem regarding 'VIEWS'!!