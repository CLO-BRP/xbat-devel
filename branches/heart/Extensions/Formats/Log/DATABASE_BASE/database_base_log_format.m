function ext = database_base_log_format

ext = extension_create(mfilename);

ext.short_description = 'Base for Database Backed Log Formats';

% ext.category = {};

% ext.ext = {};

ext.version = '0.1';

ext.guid = '9126d167-a3cf-4bf0-a890-cd1e72bdd8e7';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

