function [sql, lines] = create_note_table(force)

% create_note_table - create table for notes
% ------------------------------------------
%
% [query, lines] = create_note_table(force)
%
% Input:
% ------
%  force - drop existing table if needed
%
% Output:
% -------
%  query - statement string
%  lines - statement lines 

%--
% set no force default
%--

opt = create_table; 

if ~nargin || isempty(force)
	force = 0; 
end 

opt.force = force;

%--
% create note prototype
%--

hint = column_type_hints;

note.id = hint.integer; 

note.title = hint.string; note.body = hint.text; 

note.user_id = hint.integer;

note.created_at = hint.timestamp; note.modified_at = hint.timestamp;

%--
% generate create query
%--

% NOTE: the table name is inferred from the 'note' variable name

if nargout
	[sql, lines] = create_table(note, [], opt);
else
	create_table(note, [], opt);
end
