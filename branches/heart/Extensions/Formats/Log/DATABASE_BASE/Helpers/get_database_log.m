function log = get_database_log(file, field, value, like)

if nargin < 4
	like = 0; 
end

if nargin < 2
	field = 'id'; value = [];
end 

opt = get_database_objects_by_column; opt.like = like;

log = get_database_objects_by_column(file, 'log', field, value, opt);