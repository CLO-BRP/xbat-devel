function [query, lines] = create_event_table(force)

% create_event_table - create table for events
% --------------------------------------------
%
% [query, lines] = create_event_table(force)
%
% Input:
% ------
%  force - drop existing table if needed
%
% Output:
% -------
%  query - statement string
%  lines - statement lines 

%--
% set no force default
%--

opt = create_table; 

if ~nargin || isempty(force)
	force = 0;
end 

opt.force = force;

%--
% create prototype event and generate create table query
%--

event = build_prototype(event_table_fields);

% NOTE: when no output is requested we display the query

if ~nargout
	create_table(event, [], opt); return;
end

[query, lines] = create_table(event, [], opt);
