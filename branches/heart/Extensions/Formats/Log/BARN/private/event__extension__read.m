function [data, id] = event__extension__read(store, id, ext, context)

% BARN - event__extension__read

data = [];

%--
% get extension table names
%--

table = extension_table_names(ext);

%-------------------------------
% GET EXTENSION DATA
%-------------------------------

%------------------
% VALUES
%------------------

%--
% build value select query
%--

sql = ['SELECT * FROM ', table.value, ' AS value'];

if ~isempty(id)
    sql = [sql, ' WHERE value.event_id IN (', str_implode(id, ', ', @int2str), ')'];
end

sql = [sql, ' ORDER BY value.event_id;'];

%--
% execute query and extract retrieved event identifiers from result
%--

[status, value] = query(store, sql); 

if isempty(value)
	data = []; id = []; return;
end

id = [value.event_id];

%------------------
% PARAMETERS
%------------------

%--
% get expanded parameter values
%--

parameter = struct;

if has_barn_table(store, table.parameter)

	% NOTE: get needed parameter identifiers, get such parameters, later we join in MATLAB
	
	sub = ['SELECT DISTINCT parameter_id FROM ', table.value, ' WHERE event_id IN (', str_implode(id, ', ', @int2str), ')'];
	
	sql = ['SELECT * FROM ', table.parameter, ' AS parameter WHERE parameter.id IN (', sub, ');'];
	
	[status, parameter] = query(store, sql);
	
end

%------------------
% JOIN AND OUTPUT
%------------------

%--
% setup for join and normalization
%--

remove.parameter = {'id', 'content_hash'}; remove.value = intersect(fieldnames(value), {'event_id', 'parameter_id'});

if ~trivial(parameter)
	parameter_id = [parameter.id];
end

%--
% join, normalize, and pack output
%--

% NOTE: what we are calling normalization consists or removal of auxiliary fields and struct collapse

for k = 1:length(value)
   
    data(k).value = collapse(rmfield(value(k), remove.value));
    
	if trivial(parameter)
		data(k).parameter = struct;
	else
		data(k).parameter = collapse(rmfield(parameter(parameter_id == value(k).parameter_id), remove.parameter));
	end
	
end
	