function users = list_barn_users(store, id)

% list_barn_users - from store, possibly filtered
% ----------------------------------------------
%
% users = list_barn_users(store, id)
%
% Input:
% ------
%  store - for users
%  id - list
%
% Output:
% -------
%  users - selected

%--
% build query
%--

sql = 'SELECT * FROM user';

if nargin > 1 && ~isempty(id)
	
	id = unique(id); % NOTE: we make sure we have no duplicates
	
	if numel(id) > 1
		sql = [sql, ' WHERE id IN (', str_implode(id, ', ', @int2str), ')'];
	else
		sql = [sql, ' WHERE id = ', int2str(id)];
	end
	
end

sql(end + 1) = ';'; 

%--
% execute query
%--

[status, users] = query(store, sql);

if ~nargout
	disp(users); clear users;
end



