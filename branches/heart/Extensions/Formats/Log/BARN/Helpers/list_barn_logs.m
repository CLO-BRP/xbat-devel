function logs = list_barn_logs(store, duck)

%--
% handle input
%--

if nargin < 2
	duck = 1; 
end

if ~nargin
	store = local_barn;
end

logs = empty(log_create);

if isempty(store)
	return;
end

%--
% get logs from store and possibly wrap them to look like MATLAB logs
%--

obj = get_database_objects_by_column(store, 'log');

% TODO: we can call 'log_get_events' on the duck logs, however the log sound is missing so we can't read samples

if duck
	for k = 1:numel(obj)
		logs(end + 1).name = obj(k).name;
		
		logs(end).format = 'BARN'; 
		
		logs(end).store = store;
		
		logs(end).store.log = obj(k);
	end
else
	logs = obj;
end




