function parameter = store__parameter__create(context)

% BARN-REST - store__parameter__create

parameter = struct;

parameter.server = 'localhost:3000';

parameter.login = context.user.email;

parameter.password = context.user.password;