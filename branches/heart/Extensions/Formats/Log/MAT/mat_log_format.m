function ext = mat_log_format

ext = extension_create(mfilename);

ext.short_description = 'MAT file based log format';

% ext.category = {};

% ext.ext = {};

ext.version = '0.1';

ext.guid = '38860c97-79d5-41bd-b84b-224f704cc578';

ext.author = 'Default';

ext.email = '';

ext.url = '';

ext.enable = false;
