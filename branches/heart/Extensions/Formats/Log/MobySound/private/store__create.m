function store = store__create(name, parameter, context)

% MOBYSOUND - store__create

store = struct;

root = log_root(name, context.sound, context.library);

root = create_dir(root);

store.file = [root, filesep, 'events.box'];

store.parameter = parameter;

%--
% create event table
%--

create_file(store.file);