function event = event__read(store, id, order)

% MOBYSOUND - event__read

lines = file_readlines(store.file);

if nargin < 2 || isempty(id)
    id = 1:length(lines);
end

event = empty(event_create);

for k = 1:length(id)
     
    line = lines{id(k)};
    
    %--
    % parse string using strread
    %--
    
    new = event_create;
    
    try 
        [new.time(1), ...
        new.time(2), ...
        new.freq(1), ...
        new.freq(2), ...
        new.measure.snr.value ...
        ] = strread(line, '%f%f%f%f%f');
    
        new.id = id(k);
    
        new.channel = 1;
    
        new.level = 1;
        
    catch
        continue;
    end

    event(end + 1) = new;
    
end


