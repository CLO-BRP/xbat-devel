function ext = nhp_sound_file_format

ext = extension_create(mfilename);

ext.short_description = 'NOAA HydroPhone';

% ext.category = {};

ext.ext = {'nhp'};

ext.version = '0.1';

ext.guid = '4b523a73-f29e-435e-bcf1-249b09c29be5';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://www.pmel.noaa.gov/vents/acoustics/ftp-files/NHPdataFormat.html';

