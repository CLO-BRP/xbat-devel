function ext = aiff_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Audio Interchange File Format (Apple)';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'aif', 'aifc', 'aiff'};

% ext.version = '';

ext.guid = '0ea0a2cc-d7e5-4ace-baaf-b203534ec8bc';

% ext.author = '';

% ext.email = '';

% ext.url = '';

