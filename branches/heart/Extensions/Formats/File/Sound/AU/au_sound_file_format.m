function ext = au_sound_file_format

ext = extension_inherit(mfilename, libsndfile_base_sound_file_format);

ext.short_description = 'Audio File Format (Sun)';

% ext.category = {ext.category{:}};

ext.ext = {ext.ext{:}, 'au', 'snd'};

% ext.version = '';

ext.guid = '2d030f17-00fd-414c-b7f6-ea721fe11f32';

% ext.author = '';

% ext.email = '';

% ext.url = '';

