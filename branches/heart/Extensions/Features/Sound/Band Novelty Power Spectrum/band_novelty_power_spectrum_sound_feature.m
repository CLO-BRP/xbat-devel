function ext = novelty_power_spectrum_sound_feature

ext = extension_inherit(mfilename, band_novelties_sound_feature);

ext.short_description = 'Computes a spectrogram from novelty curves, revealing periodic element repetitions';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '7de28912-5e15-4ebd-95c3-ed68527f187d';

ext.author = 'Daniel Wolff';

ext.email = 'wolffd.mail@googlemail.com';

ext.url = 'www.uni-bonn.de/~wolffd';

