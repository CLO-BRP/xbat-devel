function [feature, context] = compute(page, parameter, context)

% SURPRISINGLY LOUD - compute

%--
% compute spectrogram
%--

% TODO: eventually replace with call to 'get_page_feature'

% NOTE: work with filtered samples if available

if isempty(page.filtered)
	samples = page.samples;
else
	samples = page.filtered;
end

[X, time, freq] = fast_specgram(samples, context.sound.rate, 'norm', context.sound.specgram);
	
% NOTE: work with filtered image if image filter is active

[ext, ignore, context2] = get_active_extension('image_filter', context.par);

if ~isempty(ext)
	X = apply_image_filter(X, ext, context2);
end

%--
% binarize based on pixel Z-score and label components
%--

% NOTE: the label operation here uses the 'sieve' parameter

if ~iscell(X)
	[D, L] = binarize_and_label(X, parameter);
else
	for k = 1:length(X)
		[D{k}, L{k}] = binarize_and_label(X{k}, parameter);
	end
end

%--
% extract and select components
%--

component = pack_components(L, D, parameter, context);

if parameter.coalesce
	component = coalesce_components(component);
end

if parameter.threshold_on
	component([component.mean] < parameter.threshold) = [];
end

%--------------------

% NOTE: some of this could be done by a detection co-processor

component([component.duration] < 1 & [component.bandwidth] < 100) = [];

% component([component.bandwidth] < 5000) = []; 

%--------------------

%--
% pack results
%--

% NOTE: the time is essential for all signal features, the frequency is particular here

feature.time = time; feature.freq = freq;

feature.spectrogram.value = D; 

% NOTE: setting the empty time field allows reduce to skip this feature field

feature.component.value = component;

feature.component.time = [];


%--------------------------
% BINARIZE
%--------------------------

function [D, L] = binarize_and_label(X, parameter, context)

%--
% compute mean, deviation estimate, and loud pixel output
%--

n = size(X, 2);

% NOTE: we sort each frequency independently and average the lower energy bins

E = sort(X, 2); n0 = floor(parameter.alpha * n); M = sum(E(:, 1:n0), 2) ./ n0;

D = sqrt(sum((X - repmat(M, 1, n)).^2, 2) ./ n);

L = X > repmat(M + parameter.score * D, 1, n);

% NOTE: this test passed, and shows the threshold computation rationale

% L2 = ((X - repmat(M, 1, n)) ./ repmat(D, 1, n)) > parameter.scale; test = isequal(L, L2)

%--
% select deviation values and label components
%--

L = label_components(uint8(L), parameter);

% NOTE: these values have survived the sieve as well

D = ((X - repmat(M, 1, n)) ./ repmat(D, 1, n)) .* (L > 0);

% fast_min_max(D(D > 0))


%--------------------------
% LABEL_COMPONENTS
%--------------------------

function X = label_components(X, parameter, context)

%--
% label components
%--

% TODO: there is a problem when labelling with a non-trivial structuring element

X = comp_label(X);

%--
% sieve to remove noise
%--

% NOTE: this should depend on scale

if parameter.sieve
	X = comp_sieve(X, '[0, 11]');
end


%--------------------------
% PACK_COMPONENTS
%--------------------------

function component = pack_components(L, D, parameter, context)

%--
% setup
%--

[dt, df] = specgram_resolution(context.sound.specgram, context.sound.rate);

page = context.page;

start = context.scan.last_page.start;

nyq = 0.5 * context.sound.rate;

%--
% create components
%--

component = empty(create_component);

if numel(page.channels) == 1
	
	key = unique(L); S = size(L);
	
	% NOTE: the first key element is zero, we ignore this one
	
	for k = 2:length(key)
		
		[i, j] = find(L == key(k)); ix = [i(:), j(:)];
		
		T = [-1, 1] + fast_min_max(j) - 0.5; time = start + dt * T; 
		
		F = [-1, 1] + fast_min_max(i) - 1; freq = df * F;
		
		component(end + 1) = create_component( ...
			page.channels, time, freq, ix, D(sub2ind(S, i, j)) ...
		);
	
	end
	
else
	
	for l = 1:length(page.channels)
		
		key = unique(L{l}); S = size(L{l});
		
		% NOTE: the first key element is zero, we ignore this one
		
		for k = 2:length(key)
			
			[i, j] = find(L{l} == key(k)); ix = [i(:), j(:)];

			T = [-1, 1] + fast_min_max(j) - 0.5; time = start + dt * T; 
		
			F = [-1, 1] + fast_min_max(i) - 1; freq = df * F;

			component(end + 1) = create_component( ...
				page.channels(l), time, freq, ix, D{l}(sub2ind(S, i, j)) ...
			);
			
		end
		
	end
	
end



