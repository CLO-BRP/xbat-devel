function X = teager_kaiser(X)

n = size(X, 1); 

X = X.^2 - X([1, 1:n-1], :) .* X([2, 2:n], :);