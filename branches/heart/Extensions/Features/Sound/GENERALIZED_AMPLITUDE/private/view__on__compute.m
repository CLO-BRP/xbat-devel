function [handles, context] = view__on__compute(widget, data, parameter, context)

% GENERALIZED_AMPLITUDE - view__on__compute

% TODO: fix to work with multiple channels

if trivial(data)
	handles = []; return;
end

%------------------------------------------
% SIMPLE GENERALIZED AMPLITUDE
%------------------------------------------

% NOTE: in the case of simple generalized amplitudes we simply reuse the amplitude view

fun = parent_fun(mfilename('fullpath')); 

if numel(data) == 1	
	
	[handles, context] = fun(widget, data, parameter, context); return;

end

%------------------------------------------
% MULTIPLE GENERALIZED AMPLITUDES
%------------------------------------------

% NOTE: in the case of multiple generalized amplitudes we do a 'strips' like display

%--
% get number of displays needed and setup for grid display
%--

count = numel(data); 

points = linspace(0, 1, 2 * count + 1);

breaks = points(3:2:end - 1); offsets = points(2:2:end);

%--
% compute amplitude display for each signal
%--

for k = 1:numel(data)

	% NOTE: the 'scale' and 'offset' parameters define an affine map for the display
	
	% TODO: move affine map to 'context'
	
	% TODO: figure out how to pass range information to amplitude, this should affect various things
	
	% TODO: figure out a way to compute a reasonable scale, we need to access previous page handles
	
	current = data(k); parameter.scale = 0.75; parameter.offset = offsets(k);
	
	% TODO: allow passing color information for the various signals
	
	[handles{k}, context] = fun(widget, current, parameter, context);
	
end

%--
% set various display axes properties, to provide 'grid' and 'labels'
%--

% NOTE: one iteration of the previous loop visits all channels, we can get all display axes handles below

ax = iterate(@ancestor, handles{1}, 'axes'); 

if iscell(ax) 
	ax = [ax{:}]; 
end

ax = unique(ax);

set(ax, ... 
	'ylim', [0, 1], 'ytick', offsets, 'ygrid', 'on' ...
);

% NOTE: this is a convention being established, we expect a label for each signal

if isfield(context.state.info, 'labels')
	set(ax, 'yticklabel', context.state.info.labels);
end 

% NOTE: we would like this to happen on the page callback, however we may not know the number of signals there

page = context.page;

for k = 1:numel(breaks)
	
	create_line(ax, ['guide::', int2str(k)], ...
		'xdata', [page.start, page.start + page.duration], ...
		'ydata', breaks(k) * ones(1, 2), ...
		'color', context.display.grid.color ...
	);

end

%--
% output all handles as a single array
%--

handles = [handles{:}];




