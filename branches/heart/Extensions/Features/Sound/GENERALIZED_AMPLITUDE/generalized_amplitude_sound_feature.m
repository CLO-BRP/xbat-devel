function ext = generalized_amplitude_sound_feature

ext = extension_inherit(mfilename, amplitude_sound_feature);

ext.short_description = 'Base for various amplitude type features';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = 'df2fb346-ce49-4368-bf80-006c51fb0de8';

% ext.author = '';

ext.email = 'hkf1@cornell.edu';

% ext.url = '';

