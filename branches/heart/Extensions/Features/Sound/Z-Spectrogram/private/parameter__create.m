function parameter = parameter__create(context)

% Z-SPECTROGRAM - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

%--
% binarization parameters
%--

parameter.alpha = 0.25;

parameter.score = 2;

%--
% component processing parameters
%--

parameter.sieve = 1;

parameter.coalesce = 1;

parameter.threshold = 3;

parameter.threshold_on = 0;

