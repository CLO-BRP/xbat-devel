function widget = view__position(widget, parameter, context)

% SIGNAL_FEATURE - view__position

pos_par = get(context.par, 'position');

% NOTE: we size widget using parent as reference

pos = get(widget, 'position');

pos(3:4) = [pos_par(3), pos_par(4)];

set(widget, 'position', pos);

% TODO: consider other views and parent position when positioning relative to parent

position_palette(widget, context.par, 'bottom right');