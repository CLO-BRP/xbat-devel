function [handles, context] = view__on__selection__delete(widget, feature, parameter, context)

% SIGNAL_FEATURE - view__on__selection__delete

delete(findobj(widget, 'tag', 'selection_handles'));

handles = [];
