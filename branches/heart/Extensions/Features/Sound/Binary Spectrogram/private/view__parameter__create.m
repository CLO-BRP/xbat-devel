function parameter = view__parameter__create(context)

% BINARY SPECTROGRAM - view__parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.invert = 0;

parameter.adapt_contrast = 0; 