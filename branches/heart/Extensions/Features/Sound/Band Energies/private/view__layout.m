function handles = view__layout(widget, parameter, context)

% BAND ENERGY - view__layout

% handles = [];

fun = parent_fun(mfilename('fullpath')); handles = fun(widget, parameter, context);

% NOTE: update right margin of layout so that we may display band scaling factors

data = harray_data(widget); data.layout.margin(2) = 1.75;

harray_data(widget, data);