function [feature, context] = compute(page, parameter, context)

% Band Energy - compute

%--
% compute parent feature (spectrogram)
%--
fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context);


%--
% compute band energies
%--

nyquist=context.sound.samplerate/2;

if ~iscell(feature.spectrogram.value)
	[band_energy.max band_energy.value] = filter_genwindow(feature.spectrogram.value,feature.freq./nyquist,parameter);	
else
	
	for k = 1:numel(feature.spectrogram.value)
		[band_energy.max{k} band_energy.value{k}] = filter_genwindow(feature.spectrogram.value{k},feature.freq./nyquist,parameter);
	end
end

band_energy.time = feature.time';

feature.band_energy = band_energy;


%-----------------------
% GENERIC BAND ENERGY
%-----------------------

function [valmax, out]= filter_genwindow(specf, freqs, parameter) %#ok<INUSL>

% NOTE: apply filter bank matrix to get band energies

out = parameter.bank' * specf.^2;

valmax = max(out,[],2);

