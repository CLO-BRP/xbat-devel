function C = triangle(N, low, center, high, p)

% triangle - create triangle sequence column
% ------------------------------------------
%
% C = triangle(N, low, center, high, p)
%
% Input:
% ------
%  N - length of sequence
%  low, center, high - low, center, and high elements
%  p - normalization type
%
% Output:
% -------
%  C - triangle sequence

%--
% set default normalization
%--

if nargin < 5
	p = 2;
end

%--
% compute triangle sequence
%--

C = zeros(N, 1); 

C((low:center) + 1) = linspace(0, 1, center - low + 1);

C((center:high) + 1) = linspace(1, 0, high - center + 1);

%--
% normalize sequence
%--

if p < 0
	return;
end

C = C ./ norm(C, p);