function [handles, context] = view__on__keypress(widget, data, parameter, context)

% SPECTROGRAM - view__on__keypress

handles = [];

switch data.Key
	
	% TODO: implement invert and contrast adaptation keypress callbacks
	
	case {'i', 'a'}
		
		% NOTE: this sets the parameter field based on the key pressed
		
		field = ternary(data.Key == 'i', 'invert', 'adapt_contrast');
		
		ext = context.ext; current = ext.parameters.view.(field);
		
		% NOTE: if the palette is closed we update the browser extension, otherwise we update the control
		
		if isempty(context.pal)
			ext.parameters.view.(field) = ~current; set_browser_extension(context.par, ext);
		else
			set_control(context.pal, field, 'value', ~current);
		end
	
	otherwise
		
		browser_keypress_callback(widget, data, context.par); return;
		
end

%--
% update spectrogram colormap
%--

% NOTE: here we obtain an up to date extension

context.ext = get_browser_extension(ext.type, context.par, ext.name);

set_spectrogram_colormap(widget, context.ext.parameters.view);

