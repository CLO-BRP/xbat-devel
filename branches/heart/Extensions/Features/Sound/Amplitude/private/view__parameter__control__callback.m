function result = view__parameter__control__callback(callback, context)

% AMPLITUDE - view__parameter__control__callback

result = struct;

% TODO: how do we call this from the menu callback?

% use a 'callback' structure adapter in the menu callback 

% we should simply regenerate the menu from here

% db_disp; flatten(callback)

%--
% check for view widget
%--

% NOTE: if we are called while the view is off there is nothing to do

% TODO: is there an analogous need in the case of computation? why or why not?

if isempty(context.widget)
	return;
end

%--
% dispatch callback
%--

switch callback.control.name
	
	case {'color', 'background', 'alpha'}
		
		% NOTE: this helper consumes the parameter logic, here we update handles
		
		color = get_amplitude_display_colors(context.ext.parameters.view);

		%--
		% update waveform display handles
		%--
		
		% NOTE: the non-uniform naming is quirky 
		
		tag = {'max', 'rms', 'abs'}; field = tag; field{3} = 'mean';

		for k = 1:numel(tag)
			
			handle = find_handles_with_tag(context.widget, tag{k}, 'type', 'patch');

			if isempty(handle)
				continue;
			end
			
			set(handle, ...
				'facecolor', color.(field{k}).facecolor, ...
				'edgecolor', color.(field{k}).edgecolor ...
			);
		end
		
		%--
		% update axes background if needed
		%--
		
		if ~strcmpi(callback.control.name, 'color')
			ax = get(handle, 'parent');
			
			if iscell(ax)
				ax = [ax{:}];
			end
			
			set(unique(ax), 'color', color.background);
		end
		
		%--
		% update event display if needed
		%--
		
		% TODO: look at code for changing log display!
		
		% NOTE: we could also use a simple function to show us the events in a browser page
		
	case 'max'
		
		set(find_handles_with_tag(context.widget, 'max'), 'visible', ternary(callback.value, 'on', 'off'));
		
end
