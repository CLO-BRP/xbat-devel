function [feature, context] = compute(page, parameter, context)

% AMPLITUDE - compute

% db_disp; stack_disp; disp(' '); flatten(parameter)

%--
% work with filtered samples if available
%--

if ~isempty(page.filtered)
	page.samples = page.filtered;
end

%--
% compute various amplitude envelope estimates 
%--

% NOTE: the time scale parameter should inspect the rate to set bounds

n = round(context.sound.rate * parameter.block); m = round(n * parameter.overlap);

%-------------------

% TODO: consider removal of padding

% NOTE: pad to allow last block to be computed

N = size(page.samples, 1);

if m
	N0 = m * floor((N - n) / m) + n;
else
	N0 = n * floor(N / n);
end

pad = n - (N - N0);

% page.samples = [page.samples; zeros(pad, M)];

% NOTE: we pad with some of the last samples trying to reduce discontinuity

append = flipud(page.samples(end - pad + 1:end, :));

if norm(page.samples(end,:) - append(1,:)) < norm(page.samples(end,:) + append(1,:))
	alpha = 1;
else
	alpha = -1;
end

page.samples = [page.samples; alpha * append];

%-------------------

% NOTE: we could get time from the MEX, but this is less flexible
	
A1 = fast_amplitude(page.samples, n, m, 'rms');

[A2, A3] = fast_amplitude(page.samples, n, m, 'abs');

feature.rms.value = A1;

feature.abs.value = A2;

feature.max.value = A3;

%--
% compute common feature time
%--

feature.time = linspace(page.start, page.start + page.duration, length(A1) + 1)'; feature.time(end) = [];

feature.time = feature.time + 0.5 * diff(feature.time(1:2));

% NOTE: these are here for convenience, they are cheap to compute and store

feature.time_step = diff(feature.time(1:2));

feature.rate = 1 / feature.time_step;

%--
% store actual amplitude and time
%--

% NOTE: we only pack samples when the scale is reduced and we may want to use or display them

% TODO: consider controlling this packing through a parameter

if length(page.samples) <  4 * parameter.reduce.target
	
	feature.amplitude.value = page.samples;

	% NOTE: for large pages this 'linspace' call is the most costly line in this function

	feature.amplitude.time = linspace(page.start, page.start + page.duration, size(page.samples, 1));

end
