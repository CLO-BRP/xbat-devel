function result = view__parameter__menu__callback(callback, context)

% AMPLITUDE - view__parameter__menu__callback

% result = struct;

result = scaffold_menu_callback('compute', callback, context);

% result = view__parameter__control__callback(callback, context);