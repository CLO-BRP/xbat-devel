function [parameter, context] = parameter__compile(parameter, context)

% BAND AMPLITUDE - parameter__compile

fun = parent_fun(mfilename('fullpath')); [parameter, context] = fun(parameter, context);

%--
% DESIGN FILTER
%--

%--
% get normalized frquency specification
%--

band = [parameter.min_freq, parameter.max_freq] / (0.5 * context.sound.rate); 

%--
% no filtering
%--

% NOTE: when both band edges are extremes no filtering is needed, no filter is appended to parameters

if isequal(band, [0, 1])
	return;
end 

%--
% low or high pass
%--

% NOTE: when one of the band edges is an extreme we have a special case

if band(1) == 0	
	[parameter.filter.b, parameter.filter.a] = butter(parameter.order, band(2), 'low'); return;
end

if band(2) == 1	
	[parameter.filter.b, parameter.filter.a] = butter(parameter.order, band(1), 'high'); return; 
end

%--
% general case
%--

% HACK: somewhere we are losing our structured 'filter' field

if ~isstruct(parameter.filter)
	parameter.filter = struct; parameter.filter.a = 1; parameter.filter.b = 1;
end
	
% NOTE: this may currently fail for some 'band' values, these values should be limited

[parameter.filter.b, parameter.filter.a] = butter(parameter.order, band);