function [handles, context] = view__on__compute(widget, data, parameter, context)

% MEL BAND AUTOCORRELATION - view__on__compute

%--
% unpack feature for convenience
%--

R = data.novelty_power_spec.value;

time = data.novelty_power_spec.time;

lags = data.novelty_power_spec.lags;

if iscell(R{1})
	num_channels = numel(R);
	num_bands = numel(R{1});
else
	num_channels = 1;
	num_bands = numel(R);
end

page = get_browser_page(context.par);

if ~isequal(context.page.channels, page.channel)
	page.channel = context.page.channels;
end

handles = [];

for k = 1:num_channels

	%--
	% setup
	%--
	
	if num_channels == 1
		val = R;
	else
		val = R{k};
	end

	ax = get_channel_axes(widget, page.channel(k));

	if isempty(ax)
		continue;
	end
	
	%--
	% pack spectrograms as single image
	%--
	
	% TODO: we should eventually reveal the scaling as a view option, not there yet
	
	for i = 1:numel(val)
		% val{i} = val{i} ./ max(val{i}(1,:));
		
		val{i} = val{i} ./ repmat(val{i}(1,:), size(val{i},1), 1);
	end

	val = cell2mat(val');

	%--
	% display nps image
	%--
	
	% TODO: note that we are not ready to resolve the lag axes display with this view strategy
	
	handles(end + 1) = image( ...
		'parent', ax, ...
		'xdata', time, ...
		'ydata', 1:size(val, 1), ...
		'cdata', val, ...
		'cdatamapping', 'scaled' ...
	); 

	context.view.paging(k) = 1;

	set(ax, ...
		'ydir', 'normal', ...
		'xlim', [time(1), time(end)], ...
		'ylim', [1, size(val, 1)] ...
	);
end


