function [handles, context] = view__on__page(widget, data, parameter, context)

% MEL BAND AUTOCORRELATION - view__on__page

% handles = [];

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

set(widget, 'colormap', jet);