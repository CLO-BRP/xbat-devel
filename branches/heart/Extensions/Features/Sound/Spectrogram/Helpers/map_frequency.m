function values = map_frequency(values, parameter, context, forward)

% map_frequency - values for spectrogram feature display
% ------------------------------------------------------
%
% values = map_frequency(values, parameter, context, forward)
%
% Input:
% ------
%  values - to map
%  parameter - struct for view
%  context - for extension
%  forward - indicator
%
% Output:
% -------
%  values - mapped to right domain
%
% NOTE: forward means from unit interval to frequency

%--
% handle input
%--

if nargin < 4
	forward = true;
end

%--
% map values
%--

switch lower(cellfree(parameter.scale))
	
	case 'linear'
		
		scale = 0.5 * context.sound.rate;
		
		if forward
			values = scale * values;
		else
			values = values / scale;
		end
		
	case 'log'
		
		if parameter.min_freq == 0
			start = 0;
		else
			start = log2(parameter.min_freq);
		end

		stop = log2(parameter.max_freq);

		g = 2.^linspace(start, stop, 1024 + 1)'; g(1) = [];
		
		fixed = values == 0;
		
		if forward
			values = interp1q(linspace(0, 1, 1024)', g(:), values(:)); % lut_apply(1024 * values, g);
		else
			values = interp1q(g(:), linspace(0, 1, 1024)', values(:));
		end
		
		values(fixed) = 0;
		
	case 'linear-log'

		split = cellfree(parameter.split); split = eval(split(1:2)) / 100;

		n1 = floor(split * 1024); n2 = 1024 - n1;

		g1 = linspace(parameter.min_freq, parameter.split_freq, n1);

		g2 = 2.^linspace(log2(parameter.split_freq), log2(parameter.max_freq), n2 + 1); g2(1) = [];

		g = [g1, g2];
		
		fixed = values == 0;
		
		if forward
			values = interp1q(linspace(0, 1, 1024)', g(:), values(:)); % lut_apply(1024 * values, g);
		else
			values = interp1q(g(:), linspace(0, 1, 1024)', values(:));
		end
		
		values(fixed) = 0;
		
	otherwise

		error(['Unrecognized scale option ''', parameter.scale, '''.']);
		
end






