function set_spectrogram_colormap(widget, parameter)

% TODO: modify to make use of the 'get_spectrogram_colormap'

%--
% check widget and displayed image
%--

% NOTE: 'cmap_scale' fails when there are no displayed images in figure, check for image

if isempty(widget) || isempty(findobj(widget, 'type', 'image'))
	return;
end

%--
% compute and update colormap according to view parameters
%--

fun = colormap_to_fun(parameter.colormap);

if parameter.adapt_contrast

	%--
	% adaptive contrast
	%--
	
	map = eval(fun);

	if parameter.invert
		map = flipud(map);
	end

	set(widget, 'colormap', map);

	% TODO: update 'cmap_scale' signature to take target figure as input, various other updates as well

	figure(widget); 
	
	if strcmpi(cellfree(parameter.scale), 'linear')
		cmap_scale;
	else
		p = cmap_scale; cmap_scale(p.^2);
	end

else

	%--
	% brightness contrast controlled map
	%--
	
	map = brightness_contrast_map(fun, parameter.brightness, parameter.contrast, parameter.invert);

	set(widget, 'colormap', map);

end
