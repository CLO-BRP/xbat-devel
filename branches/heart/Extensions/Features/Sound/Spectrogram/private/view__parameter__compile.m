function [parameter, context] = view__parameter__compile(parameter, context)

% SPECTROGRAM - view__parameter__compile

screen = get(0, 'screensize'); width = screen(3);

parameter.reduce.target = width;

% if ~isfield(context, 'pages')
% 	return;
% end

% NOTE: this should only be used for display, consider adding these to the compute parameters and adapting them

% db_disp 'trying to alter summary computation, we may need to repeat first page'
% 
% count = floor(size(context.page_value.spectrogram.value, 2) * numel(context.pages) / width)
% 
% parameter.old.sum_length = count;
% 
% context.repeat_page = 1;