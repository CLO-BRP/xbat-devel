function [parameter, context] = parameter__compile(parameter, context)

% SPECTROGRAM - parameter__compile

%--
% handle common spectrogram parameters
%--

% NOTE: this is a service that this extension provides for it's children

if ~strcmpi(context.ext.name, 'spectrogram') && parameter.use_common_spectrogram
	
	ext = get_browser_extension('sound_feature', context.par, 'Spectrogram');
	
	% NOTE: we make sure not to override the value of the 'use_common_spectrogram' meta-parameter
	
	ext.parameters.compute.use_common_spectrogram = 1;
		
	parameter = struct_update(parameter, ext.parameters.compute);
	
else
	
	% TODO: in this case we are updating the spectrogram feature itself, check for children who care and update them
	
% 	disp(' '); db_disp 'updating children of spectrogram';
% 	
% 	stack_disp;
% 	
% 	start = clock;
% 	
% 	%----------------------
% 	
% 	child = get_extension_children(context.ext);
% 
% 	% NOTE: in this case we are not only fast, but we also avoid a recursion problem
% 
% 	active = get_active_extensions(context.par, [], 1);
% 	
% 	%----------------------
% 	
% 	elapsed = etime(clock, start); disp(' '); disp(['   elapsed = ', num2str(elapsed)]);
% 	
% 	disp(' '); disp('PARENT'); 
% 	
% 	disp(['   ', context.ext.subtype, '::', context.ext.name]);
% 	
% 	disp(' '); disp('CHILDREN'); 
% 	
% 	if ~isempty(child)
% 	
% 		iterate(@disp, strcat({'   '}, {child.subtype}, '::', {child.name})); disp(' ');
% 	
% 	end
% 	
% 	disp('ACTIVE');
% 	
% 	if ~isempty(active)
% 		
% 		field = fieldnames(active);
% 
% 		for k = 1:numel(field)
% 			iterate(@disp, strcat({'   '}, {active.(field{k}).ext.subtype}, '::', {active.(field{k}).ext.name}));
% 		end
% 	
% 	end
% 	
% 	disp(' ');
	
end

%--
% get old spectrogram parameters from feature parameters
%--

% NOTE: make sure we adapt feature parameters to spectrogram parameters for now

old = fast_specgram;

old.fft = parameter.fft;

parameter.samples = round(parameter.frame * context.sound.rate);

% NOTE: the parameter overlap has been set to produce an integer hop

% NOTE: we are converting hop from fraction of the frame to fraction of the transform 

old.hop = parameter.samples * parameter.advance / parameter.fft;

old.hop_auto = parameter.adapt;

% TODO: what is the role of the 'hop_auto' field at this point

old.win_type = parameter.window;

% NOTE: this window length should always be smaller than 1

old.win_length =  parameter.samples / parameter.fft;

%--
% possibly adapt time resolution through 'advance'
%--

if parameter.adapt
	
	old = update_time_resolution(context.sound, context.page.duration, old, parameter.slices);
	
	parameter.advance = parameter.fft * old.hop / parameter.samples;
	
	if ~isempty(context.pal)
		
		set_control(context.pal, 'advance', 'value', parameter.advance);
		
		parameter_plot(context.pal, parameter, context);
		
	end
	
end

%--
% pack old spectrogram parameters in feature parameters
%--

parameter.old = old;

