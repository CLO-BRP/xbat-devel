function parameter = parameter__create(context)

% SVD-SPECTROGRAM - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.rank = 4;

parameter.normalize = 1;

parameter.residual = 0;