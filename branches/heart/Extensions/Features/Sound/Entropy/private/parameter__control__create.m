function control = parameter__control__create(parameter, context)

% ENTROPY - parameter__control__create

% control = empty(control_create);

fun = parent_fun(mfilename('fullpath')); control = fun(parameter, context);

control(1:2) = [];