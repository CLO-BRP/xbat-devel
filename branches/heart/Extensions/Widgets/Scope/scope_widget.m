function ext = scope_widget

ext = extension_create(mfilename);

ext.short_description = 'Waveform scope display';

% ext.category = {};

ext.version = '0.3';

ext.guid = 'b619185d-f290-4425-a56a-d91d04ade943';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

