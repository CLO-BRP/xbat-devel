function parameter = parameter__create(context)

% HISTOGRAM - parameter__create

parameter = struct;

parameter.normalize = 1;

parameter.scale = 'log'; 

parameter.grid = 'off';
