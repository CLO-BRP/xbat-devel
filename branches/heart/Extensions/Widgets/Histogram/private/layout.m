function handles = layout(widget, parameter, context)

% HISTOGRAM - layout

%--
% layout axes and get handles
%--

layout = layout_create(2, 1); layout.margin(1) = 0.5;

layout.row.frac = [0.7 , 0.3]; layout.row.pad = 1;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

set(handles(1), 'tag', '2d_histogram');

set(handles(2), 'tag', '1d_histogram');

xlabel(handles(2), 'dB');

