function [parameter, context] = parameter__compile(parameter, context)

% PHOTO STREAM - parameter__compile

% NOTE: this will change when we consider URL based sources

parameter.files = get_field(what_ext(parameter.source, 'jpg'), 'jpg', {});