function parameter = parameter__create(context)

% PHOTO STREAM - parameter__create

parameter = struct;

parameter.source = get_photo_root(context);


%-----------------------
% GET_PHOTO_ROOT
%-----------------------

% TODO: try to get source from a properly devised sound attribute

function source = get_photo_root(context)

% NOTE: for now we will hack code this to work with the PALAOA photo streams

% NOTE: the conventional mapping for the drive containing the data is 'P'

root = 'C:\PALAOA\WEBCAM';

source = fullfile(root, sound_name(context.sound));

