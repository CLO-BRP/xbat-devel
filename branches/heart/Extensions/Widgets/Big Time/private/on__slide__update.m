function [handles, context] = on__slide__update(widget, data, parameter, context)

% BIG TIME - on__slide__update

page = context.page; page.start = data.slide.time;

% TODO: fix something in the context durng slide, it currently displays 'clock'

handles = display_big_time(widget, page.start, context);

set(widget, 'name', [context.ext.name, ' (Page)']);