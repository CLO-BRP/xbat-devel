function ext = big_selection_widget

ext = extension_create(mfilename);

ext.short_description = 'Text display for selection properties';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'd9ab8fce-e1a9-4c67-b437-2052fb3e4e5e';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

