function [handles, context] = on__selection__create(widget, data, parameter, context)

% AR SPECTRUM - on__selection__create

handles = [];

data.marker.samples = data.selection.event.samples;

data.marker.color = data.selection.color;

[handles, context] = on__marker__create(widget, data, parameter, context);