function handle = spectrum_marker_line(ax, channel, varargin)

handle = create_line(ax, ['marker_line::', channel], varargin{:});