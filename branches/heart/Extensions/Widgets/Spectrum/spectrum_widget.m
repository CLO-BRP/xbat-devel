function ext = spectrum_widget

ext = extension_create(mfilename);

ext.short_description = 'Spectrogram slice spectrum';

% ext.category = {};

ext.version = '0.2';

ext.guid = '0a7dc0da-746d-4996-a048-9d4c4f87ae3a';

ext.author = 'Harold and Matt';

ext.email = 'harold.figueroa@gmail.com';

ext.url = 'http://xbat.org';

