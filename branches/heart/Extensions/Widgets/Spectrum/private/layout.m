function handles = layout(widget, parameter, context)

% SPECTRUM - layout

%--
% clear existing widget axes
%--

delete(findobj(widget, 'type', 'axes'));

%--
% layout axes and get handles
%--

% NOTE: this layout matches the current row arrangement of channels

channels = context.page.channels; 

layout = layout_create(length(channels), 1); 

layout.margin(1) = 0.5; layout.margin(4) = 1;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag axes
%--

for k = 1:length(handles)
	
	str = int2str(channels(k));
	
	set(handles(k), ...
		'tag', ['spectrum_axes::', str] ...
	);
	
	set(get(handles(k), 'ylabel'), 'string', ['Ch ', str]);
	
end

%-------------
% TEST CODE
%-------------

%--
% set display options
%--

% TODO: this should happen at a higher level

color = context.display.grid.color;

set(handles, 'xcolor', color, 'ycolor', color);
