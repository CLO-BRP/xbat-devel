function [handles, context] = on__marker__delete(widget, data, parameter, context)

% SPECTRUM - on__marker__delete

handles = [];

%--
% delete selection lines and frequency guides
%--

channels = context.page.channels;

for k = 1:length(channels)
	
	channel = channels(k);
	
	ax = spectrum_axes(widget, channel);

	delete(spectrum_marker_line(ax, channel));

end