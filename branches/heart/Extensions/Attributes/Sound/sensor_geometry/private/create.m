function attribute = create(context)

% SENSOR_GEOMETRY - create

% TODO: move away from doing this, it creates real problems when the network is intermittent

if ~has_toolbox('M_Map')   
	
    install_toolbox('M_Map');    
end

attribute = struct;

channels = context.sound.channels;

attribute.local = default_geometry(channels);

attribute.global = [];

attribute.offset = [];

attribute.ellipsoid = [];