function table = sound_speed(file)

% sound_speed - get speed of sound attribute
% ------------------------------------------
% table = sound_speed(file)
%
% Inputs:
% -------
% file - file with path
%
% Outputs:
% --------
% table - table of (possibly time-varying) sound speed

lines = file_readlines(file);

if isempty(lines)
	return
end

%--
% get number of columns from first line
%--

header = lines{1};

t = {};

while (1)
	
	[t{end + 1} header] = strtok(header, ',');
	
	if isempty(header)
		break;
	end
	
end

%--
% get units from second line
%--


%--
% create table by reading lines
%--

cols = length(t); rows = length(lines) - 2;

table = zeros(rows, cols);

for k = 1:rows
	
	table(k,:) = strread(lines{k + 2}, '', cols, 'delimiter', ',');
	
end

