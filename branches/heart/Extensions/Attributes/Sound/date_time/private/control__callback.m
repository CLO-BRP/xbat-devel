function result = control__callback(callback, context)

% DATE_TIME - control__callback

result = [];

switch callback.control.name

	case 'datetime'
	
		value = get_control(callback.pal.handle, 'datetime', 'value');

		% NOTE: we enforce validity of date string by testing its conversion
				
		try
			datevec(value);
		catch
			% NOTE: what is this 'context.attribute' field? 
			
			set_control(callback.pal.handle, 'datetime', 'string', datestr(context.attribute.datetime, 0));
		end
		
	case 'get_from_files'
		
		% NOTE: in the case of a file stream we assume that the first file name contains the relevant information
		
		file = context.sound.file;
		
		if iscell(file)
			file = file{1};
		end
		
		value = file_datenum(file);
		
		if ~isempty(value)
			set_control(callback.pal.handle, 'datetime', 'string', datestr(value));
		end
end