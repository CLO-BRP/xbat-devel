function save(attribute, store, context)

% DATE_TIME - save

lines{1} = 'Date and Time';

lines{2} = '(MATLAB date vector)';

vec = datevec(attribute.datetime);

line = '';

for k = 1:length(vec)
	line = [line, num2str(vec(k)), ', '];
end

line(end-1:end) = '';

lines{3} = line;

file_writelines(store, lines);

%--
% barn store
%--

% NOTE: this is mostly general for all sound attributes, factor

if isempty(local_barn)
	return;
end

% SETUP

% NOTE: first we prepare the data to store, we call it 'prototype'

% TODO: we need to start passing a 'context.barn' rather than calling the 'local_barn'

sound = set_barn_sound(local_barn, context.sound);

attribute.datetime = datestr(attribute.datetime, 31);

prototype = attribute;

prototype.sound_id = sound.id;

% NOTE: here we make sure store is established

establish_extension_table(local_barn, context.ext, 'value', prototype);

table = extension_table_names(context.ext);

% STORE

% NOTE: here we make sure we have a single date_time for each sound

% TODO: there is something insufficient about the schema

query(local_barn, ['DELETE FROM ', table.value, ' WHERE sound_id = ', int2str(sound.id)]);

set_barn_objects(local_barn, prototype, [], [], table.value);
