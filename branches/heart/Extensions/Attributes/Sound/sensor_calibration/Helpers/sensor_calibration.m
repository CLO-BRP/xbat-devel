function table = sensor_calibration(file)

% sensor_calibration - read sensor_calibration file
% ----------------------------------
% table = sensor_calibration(file)
%
% Inputs:
% -------
% file - file with path
%
% Outputs:
% --------
% table - sensor geometry table 

lines = file_readlines(file);

if isempty(lines)
	return
end

%--
% get units from second line
%--

units = lines{2};

%--
% create table by reading lines
%--

rows = length(lines) - 2;

table = zeros(rows, 1);

for k = 1:rows
	
	line = lines{k + 2};
	
	table(k) = strread(line, '', 1, 'delimiter', ',');
	
end

