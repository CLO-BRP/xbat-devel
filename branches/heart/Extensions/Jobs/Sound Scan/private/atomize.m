function [job, context] = atomize(job, parameter, context) %#ok<INUSL>

% SOUND SCAN - atomize

% NOTE: it is not clear that everything here belongs here, perhaps the name is 'prepare'

%--
% get detector and preset referenced
%--

% NOTE: the availability of the detector is a matter of installed software

% NOTE: extension distribution is managed through source-control

ext = get_extensions('sound_detector', 'guid', job.job.detector);

if isempty(ext)
	% TODO: try to install extension using BARN database representation
	
	% TODO: to include source-control address in BARN extension representation
	
	error(['Detector is not available. (', job.job.detector, ')']);
end

preset = get_barn_preset(context.store, job.job.preset); 

%--
% get sounds referenced
%--

% NOTE: how we parse the different sounds depends on the web-client, we could have gotten a cell array to start with

guid = str_split(job.job.sound, ' ');

for k = 1:numel(guid)
	
	sound(k) = get_database_objects_by_column(context.store, 'sound', 'guid', guid{k}); %#ok<AGROW>
end

db_disp; disp(sound)

%--
% create children jobs
%--

% NOTE: we are certainly splitting by sound, then we may split a single sound scan for long sounds

child = empty(job);

for k = 1:numel(sound)
	
	% NOTE: we start working on a current job focused on one sound
	
	current = job;
	
	current.description = ['Scan ''', sound.name, ''' using ''', ext.name, ''' with ''', preset.name, '''.'];
	
	current.job.sound = sound(k).guid;
	
	% NOTE: in what follows we will ensure producing atomic jobs
	
	current.atomic = true;
	
	% NOTE: here we consider whether the scan job should be split, this is controlled via a job parameter
	
	if get_sound_duration(sound(k)) < parameter.atomic_scan_length
		
		child(end + 1) = job; %#ok<*AGROW>
	else
		scan = get_sound_scan(sound(k), parameter.atomic_scan_length);
		
		[page, scan] = get_scan_page(scan);
		
		while ~isempty(page)
			child(end + 1) = current; child(end).job.scan = page;
			
			[page, scan] = get_scan_page(scan);
		end
	end
end

job = child;


%                  id: []
%                guid: []
%                name: []
%         description: []
%                job: []
%            priority: []
%             user_id: []
%          project_id: []
%              atomic: []
%           parent_id: []
%     parent_fraction: []
%              daemon: []
%          started_at: []
%             attempt: []
%            progress: []
%              report: []
%         last_update: []
%        completed_at: []
%          created_at: []
%         modified_at: []
