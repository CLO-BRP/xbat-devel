function [job, context] = atomize(job, parameter, context)

% TEST - atomize

% db_disp parameter; parameter

parent = job;

fprintf('ATOMIZE\n%s\n%s\n%s\n\n', ...
	['name: ', parent.name], ...
	['daemon: ', parent.daemon_guid], ... 
	['transaction: ', parent.daemon_transaction] ...
);

% NOTE: the number of atoms from a non-atomic job will be random from 2 to 4

parts = round(rand_ab(1, 2, 4));

for k = 1:parts
	
	% NOTE: since atomicity is random we will have job trees, since the fraction is uniform they will be balanced
	
	job(k) = create_child_job(parent, ...
		'name', [parent.name, ' Child-', int2str(k)], ...
		'parent_fraction', 1 / parts, ...
		'atomic', double(rand > 0.1) ...
	); 
end