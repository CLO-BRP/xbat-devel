function parameter = parameter__create(context)

% TEST - parameter__create

parameter = struct;

parameter.branching = [2, 4];

parameter.atomic_fraction = 0.9;

parameter.delay = 1;