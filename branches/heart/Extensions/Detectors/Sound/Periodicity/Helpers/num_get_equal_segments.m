function [out, class] = num_get_equal_segments(in)
% 
% generic method for segmentation and grouping of integer series
% successive integers are grouped 
%

out = [];
class = [];

aktseg = 0;
start = 0;
i = 1;
while i <= numel(in)
    
    aktseg = in(i);
    start = i;
    while i < numel(in) && in(i+1) == aktseg;
        i = i + 1;
    end
    
    % save segment
    out(end+1,:) = [start i];
    class(end+1) = aktseg;
    
    i = i + 1;
end