function ext = periodicity_sound_detector

ext = extension_inherit(mfilename, band_novelty_power_spectrum_sound_feature);

ext.short_description = 'Detects periodic repetitions of elements within the signal';

% ext.category = {ext.category{:}};

% ext.version = '';

ext.guid = '64e237dd-782b-4198-aa55-c7aedeb07327';

ext.author = 'Daniel Wolff';

ext.email = 'wolffd.mail@googlemail.com';

ext.url = 'www.uni-bonn.de/~wolffd';

