function [event, value, context] = compute(page, parameter, context)

% PERIODICITY - compute

event = empty(event_create); value = struct;

% ---
% check if we got the features via the context variable
% ---
if ~isfield(context, 'feature')
    fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.view.data = feature;
else
    feature = context.feature;
    context = rmfield(context, 'feature');
end

% ---
% TODO: parameters considered for gui access
% ---
pitch_mem = 5;
max_bad_pitches = parameter.max_err; % percent/100

% ---
% get (sampled) parameters
% ---
ri = feature.novelty_power_spec.rel_idx;

% ref_energy = feature.novelty_power_spec.ref_energy;
     
freq = feature.novelty_power_spec.freq;

% bins per Hz 
bph = 1/diff(freq(1:2));

% we have to get the tolerance level in bins
pitch_tolerance = ceil(parameter.pitch_tolerance * bph);

% nyquist frequency for event information
nyq = 0.5 * context.sound.rate;

% we deal with page-relative time, here!
time = feature.novelty_power_spec.time - page.start;

% frames(s) per second
fps = 1 / diff(time(1:2));

% minimum segment size in frames
min_segsize = parameter.min_segsize * fps;

% ---
% maximal pitch gradient
% twice the treshold might be nice
% ---
max_pitch_dev = 2 * (parameter.pitch_tolerance / parameter.min_segsize) * ... % Hz / s;
    bph / fps ;


% --
% deal with multiple channels: select active
% --
if length(page.channels) > 1
    nps = feature.novelty_power_spec.value{page.channel};

    ref_energy = feature.novelty_power_spec.ref_energy{page.channel};
else
    nps = feature.novelty_power_spec.value;
    
    ref_energy = feature.novelty_power_spec.ref_energy;
end

% ---
% here, we cutoff the irrelevant frequencies from the novelty power
% spectrum
% ---
% [nps, freq] = cutoff(nps_full, freq, rel_idx);

% this is for the case of just one frequency band
if ~iscell(nps) 
    nps = {nps};
    ref_energy = {ref_energy};
end

% --
% deal with multiple frequency bands
% --
fixed_pitches = zeros(numel(nps),size(nps{1},2));
fp_dom = zeros(numel(nps),size(nps{1},2));
outli_dom = zeros(numel(nps),size(nps{1},2));

for j = 1:numel(nps)
    segs{j} = [];
    
    % ---
    % calculate pitch candidates
    % ---
    for k=1:size(nps{j},2)
        
        % ---
        % TODO: use a peak picking algo for the selection of several
        % candidate peaks. this would make it more easy to discard segments
        % having no peak in the given range of repetition frequencies
        % ---
        
        % the max version is intended as first attempt
        % we want to use more sophisticated peak picking
        % algorithms for this
        
        % ---
        % NOTE: using pitches_max, we accept almost every tiny peak in the given
        % range :(
        % --- 
        % pitches{j}{k} = pitches_max(nps{j}(ri(1):ri(2),k)) + ri(1) - 1;
        
        pitches{j}{k} = pitches_peaks(nps{j}(:,k));
      
        % measure domination values for all candidates
        for l = 1:numel(pitches{j}{k}) 
            [dominations{j}{k}(l) pitches{j}{k}(l)] = domination(nps{j}(:,k), ref_energy{j}(k),...
                pitches{j}{k}(l), round(pitch_tolerance/2));
        end
        
        % ---
        % these outlying pitches have been detected for comparing the 
        % relative domination measurements 
        % ---
        outlip = find((pitches{j}{k} < ri(1)) | (pitches{j}{k} > ri(2))); 
        
        % try to avoid a constant zero-freq peak
        % outlip = outlip(pitches{j}{k}(outlip) > 3);
        
        if ~isempty(outlip)
            outli_dom(j,k) = max(dominations{j}{k}(outlip));
        end
        
        % ---
        % exclude pitches not in the specified range
        % ---
        idx = find((pitches{j}{k} >= ri(1)) & (pitches{j}{k} <= ri(2))); 
        
        if ~isempty(pitches{j}{k})
            pitches{j}{k} = pitches{j}{k}(idx);
            dominations{j}{k} = dominations{j}{k}(idx);
            
            % sort pitches according to domination measurements
            [dominations{j}{k}, idx] = sort(dominations{j}{k},'descend');
            pitches{j}{k} = pitches{j}{k}(idx);
        end
    end
    
    % ---
    % make pitch decisions
    % ---
    
    for k=1:size(nps{j},2)
        
        % we look at our old decisisons and
        % try to stay somewhat near them
        last_pitches = fixed_pitches(j,max(1,k-pitch_mem):k-1);
        
        % note: zero-pitches overlooked at this stage
        last_pitches = last_pitches(last_pitches > 0);
        
        if ~isempty(last_pitches) && ~isempty(pitches{j}{k})
            
            % compare the new candidates to history
            lastmat = repmat(last_pitches,numel(pitches{j}{k}),1);
            newmat = repmat(pitches{j}{k}',1,numel(last_pitches));
            
            distances = abs(lastmat - newmat);
            
            % and sum over past times
            distances = sum(distances,2);

            % sort according to history conformity
            [distances, idx] = sort(distances);
            
            % test if one of the candidates fits history
            if distances(1) <= pitch_tolerance
                
                % save the pitch with smallest historical distance
                % and the corresponding domination value
                fixed_pitches(j,k) = pitches{j}{k}(idx(1));
                fp_dom(j,k) = dominations{j}{k}(idx(1));
                
            else % save the pitch with biggest domination
                
                % OK, we stay with the peak having 
                % superior domination
                fixed_pitches(j,k) = pitches{j}{k}(1);
                fp_dom(j,k) = dominations{j}{k}(1);
            end
            
        elseif ~isempty(pitches{j}{k})
            % ---
            % TODO: check this case
            % ---
            
            % OK, we stay with the peak having 
            % superior domination
            fixed_pitches(j,k) = pitches{j}{k}(1);
            fp_dom(j,k) = dominations{j}{k}(1);
            
        else
            % there is no pitch to save
            fp_dom(j,k) = 0;
        end
    end
    
    % --- 
    % segments of almost constant periods are extracted
    % TODO: note zero-pitch segments and use this information for segment
    % intersection / classification
	% ---
    
    % ---
    % new approach: we use a regression function based on the minlen number
    % of elements as a start. 
    % this line is prolonged and adapted successively as long as the
    % maximum number of outliers is exceeded. 
    % if so, the line is redrawn from the last fitting position.
    % each line exceeding minlen corresponds to a pitch segment
    % ---
    startpos = 1;
    aktpos = round(min_segsize) + 1;
    misfits = [];
    is_seg = 0;
    segctr = 0;
    
    while aktpos <= size(nps{j},2)
    
        % ---
        % get relevant indices
        % ---
        seg_idx = startpos:aktpos;
        
        ok_idx = seg_idx;
        ok_idx = setdiff(ok_idx, misfits);
        
        % we also don't want the tested position within our line data
        ok_idx(ok_idx == aktpos) = [];

        % generate segments pitch fun(regression line)
        li = getfun(ok_idx, fixed_pitches(j,ok_idx));

        % count misfits  
        [fits, misfits] = check_points...
            (seg_idx, fixed_pitches(j,seg_idx), li, pitch_tolerance,...
            fp_dom(j,seg_idx), parameter.min_domination);
        
        % access percentage of fitting pitches
        bperr = numel(misfits) / numel(seg_idx);
        if aktpos < size(nps{j},2) && check_fun(li, max_pitch_dev) && (bperr < max_bad_pitches)
            % ---
            % ok, so the line actually fits :)
            % ---

            % prolong / start segment
            aktpos = aktpos + 1;
            is_seg = 1;
           
        else
            
            % ---
            % ok, this segments start time is over
            % go forward to last fit and start new segment if neccessary
            % ---
            if is_seg
                
                a = startpos;
                o = fits(end);
                
                % ---
                % test segment on credibility ( time / domination / energy )
                % TODO: energy
                % ---
                domsum = mean(fp_dom(j, a:o));
                if 1 % (checked in check_points:)domsum > parameter.min_domination
                    
                    % save seg and line
                    segctr = segctr +1 ;
                    
                    segs{j}(segctr,:) = [a o];
                    seggrad{j}(segctr,:) = li(1);
                    
                    repfreq{j}(segctr) = median(fixed_pitches(j, fits)) / ...
                        bph;
                    segdom{j}(segctr) = domsum;
                    segerr{j}(segctr) = bperr;
                end

                startpos = o + 1;
                aktpos = startpos + round(min_segsize) + 1;
                
                is_seg = 0;
                
            else
                % just go forward a bit and try again
                
                startpos = startpos +1;
                aktpos = startpos + round(min_segsize) + 1;
            end
            
            % ---
            % NOTE: this has to be reset every time startpos is touched!
            % ---
            misfits = []; 
        end
    end
       
%              old_rels = fp_dom(j,a:o) ./ outli_dom(j,a:o);
%              old_rels(old_rels == Inf) = 1;
%              meanor = mean(old_rels);
%              
%              domsum = mean(fp_dom(j,a:o));
%              if (diff(time([a o])) > parameter.min_segsize) && ...
%                     meanor > parameter.rel_domination;
%                     %domsum  > parameter.min_domination && ...
%                  % save new segment
%                  segctr = segctr+1;
%                  segs{j}(segctr,:) = [a o];
%                  repfreq{j}(segctr) = median(fixed_pitches(j,a:o));
%                  segdom{j}(segctr) = domsum;
%                  segreldom{j}(segctr) = meanor;

end

% --
% now we save our segments into an event struct array
% -- 
for j = 1:numel(nps)
    min_freq = parameter.band_details(1,j) * 1.05 * nyq;
    max_freq = parameter.band_details(3,j) * 0.95 * nyq;
    for k=1:size(segs{j},1)
        
        etime = time(segs{j}(k,:))';
        efreq = [min_freq max_freq];
        erepfreq = repfreq{j}(k);
        
        % create event
        event(end+1) = event_create( ...
		'channel', page.channels, 'time', etime, 'freq', efreq, ...
        'score', (1 - segerr{j}(k)).*segdom{j}(k), ...
        'tags', str_to_tags(sprintf('repfreq:%3.1fHz domination:%1.2f segerr:%1.3f',...
        repfreq{j}(k), segdom{j}(k), segerr{j}(k))));
    end
end
% auto-fill the segment's duration

if ~isempty(event)
    event = update_event_duration(event);
end


% ----------------------------------------------------------------------
% private funs
% -

function lout = getfun(x, y)
% generates line
lout = polyfit(x, y ,1);

function bool = check_fun(li, dmax)
bool = abs(li(1)) < dmax;

function [fits, outliers] = check_points(x, y, lin, tmax, dom, dmin)
% checks certain poins on 
% - detection of any pitch 
% - fit of given line, 
% - domination treshold

% predict function values
fvals = lin(1) * x + lin(2);

% ---
% get errors and outliers
% zero pitches are counted as outliers, too
% ---
err = abs(fvals - y);

% binary decision on each point
ok = (err < tmax) & (y ~= 0) & (dom > dmin);

fits = x(ok) ;
outliers = x(~ok);


