function ext = so_ext

% so_ext - return the file extension for native shared object libraries
% ---------------------------------------------------------------------
%
% ext = so_ext
%
% Output: 
% -------
%  ext - for shared object files, 'dll' for windows, otherwise 'so'

if ispc
    ext = 'dll';
else
    ext = 'so';
end

