function control = parameter__control__create(parameter, context)

% ZC-DETECTOR - parameter__control__create

%--
% get parent controls
%--

fun = parent_fun(mfilename('fullpath')); inherited = fun(parameter, context);

% NOTE: to understand this code you should look at the parent code

%--
% update tab organization
%--

control = empty(control_create);

control(end + 1) = inherited(1); inherited(1) = [];

tabs = {'Duration', control(end).tab{:}};

control(end).tab = tabs;

%--
% create duration controls
%--

% TODO: perhaps there is a reasonable minimum based on the rate and block size

control(end + 1) = control_create( ... 
	'name', 'min_duration', ...
	'style', 'slider', ... 
	'tab', tabs{1}, ...
	'min', 0.05, ... 
	'max', 10, ...
	'value', parameter.min_duration ...
);
	
%--
% reattach parent controls to control array
%--

control = [control, inherited];

