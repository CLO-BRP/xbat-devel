function parameter = parameter__create(context)

% ZC-DETECTOR - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.min_duration = 2;
