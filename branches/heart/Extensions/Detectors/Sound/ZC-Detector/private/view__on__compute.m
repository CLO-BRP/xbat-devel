function [handles, context] = view__on__compute(widget, data, parameter, context)

% ZC-DETECTOR - view__on__compute

% handles = [];

%--
% produce parent feature display
%--

fun = parent_fun(mfilename('fullpath')); [handles, context] = fun(widget, data, parameter, context);

%--
% produce decision related displays
%--

page = get_browser_page(context.par); feature = context.view.data;

for k = 1:length(page.channel)
	
	%--
	% get display axes
	%--
	
	ax = get_channel_axes(widget, page.channel(k));
	
	if isempty(ax)
		continue; 
	end
	
	%--
	% display detection information
	%--
	
% 	ylim = get(ax, 'ylim'); center = mean(ylim); % offset = 0.25 * diff(ylim);
	
	y = feature.rms.value .* feature.test.down; y(~feature.test.down) = nan;
	
	handles(end + 1) = line( ... 
		'color', 'red', ...
		'linewidth', 3, ...
		'parent', ax, ... 
		'xdata', feature.time, ...
		'ydata', y ...
	);

	y = feature.rms.value .* feature.test.constant; y(~feature.test.constant) = nan;
	
	handles(end + 1) = line( ... 
		'color', 'green', ...
		'linewidth', 6, ...
		'parent', ax, ... 
		'xdata', feature.time, ...
		'ydata', y ...
	);

	y = feature.rms.value .* feature.test.up; y(~feature.test.up) = nan;
	
	handles(end + 1) = line( ... 
		'color', 'blue', ...
		'linewidth', 3, ...
		'parent', ax, ... 
		'xdata', feature.time, ...
		'ydata', y ...
	);

	
% 	
% 	handles(end + 1) = line( ... 
% 		'color', 'red', ...
% 		'linewidth', 2, ...
% 		'parent', ax, ... 
% 		'xdata', feature.score.time, ...
% 		'ydata', scale * feature.score.value ...
% 	);
% 	
% 	uistack(handles(end), 'top'); 
% 	
% 	ylim = get(ax, 'ylim'); ylim(2) = max(ylim(2), 1.25 * max(y(:))); set(ax, 'ylim', ylim);

end


