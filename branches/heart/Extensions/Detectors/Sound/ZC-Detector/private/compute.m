function [event, context] = compute(page, parameter, context)

% ZC-DETECTOR - compute


%--
% compute parent feature
%--

% TODO: this code should be produced by the code generator

% NOTE: we output an empty event array, compute the parent feature, and make it the view data

event = empty(event_create);

fun = parent_fun(mfilename('fullpath')); [feature, context] = fun(page, parameter, context); context.view.data = feature;

% END OF CODE TO GENERATE


%--
% compute amplitude feature
%--

% [ext, ignore, context2] = get_browser_extension('sound_feature', context.par, 'Amplitude');

% NOTE: we can use the same parameters as the ZC feature because the latter is a child of the first

% NOTE: the above means that both features are computed on the same time grid

[b, a] = butter(parameter.order, [parameter.min_freq, parameter.max_freq] / parameter.nyq, 'band');

page.filtered = filter(b, a, page.samples);

amplitude = ext.fun.compute(page, parameter, context2);

%--
% compute change in RMS value over time
%--

value = feature.rms.value; value(end + 1, :) = value(end, :);
	
% NOTE: the smoothing and the tolerance below interact reconsider the design

% NOTE: we should also carefully consider the scale for smoothing as well as simplification

% value = linear_filter(value, filt_binomial(5, 1));

change = diff(value, 1, 1); 

change = change ./ feature.rms.value;

if context.debug
	
	handle = findobj(0, 'tag', 'ZCR_DEBUG_1');
	
	if isempty(handle)
		handle = figure; set(handle, 'tag', 'ZCR_DEBUG_1');
	end
	
	figure(handle); hist(change, 101); % hist_view(change);
	
end

%--
% determine change direction considering tolerance
%--

tol = 0.05; 
	
down = change < tol; constant = abs(change) < tol; up = change > -tol;

test.up = simplify(up); 

test.constant = simplify(constant); 

test.down = simplify(down);

% NOTE: we pack the change direction signals for display

context.view.data.test = test;

%--
% compute interesting events
%--

dt = diff(feature.time(1:2));

%--
% get atomic event intervals
%--

ix = get_fragment_indices(test.constant);

%--
% build and score events
%--

% NOTE: this loop is over the channels

for k = 1:numel(ix)

	current = ix{k};

	%--
	% consider joining atomic constant intervals
	%--

	for j = size(current, 1):-1:2

		time_gap = dt * (current(j,1) - current(j - 1, 2));

		last = get_fragment_description(value, current(j, :));

		previous = get_fragment_description(value, current(j - 1, :));

		value_gap = abs(last.median - previous.median);

		if (time_gap < 2 && value_gap < 0.01) || time_gap < 1
			current(j - 1, 2) = current(j, 2); current(j, :) = [];
		end

	end
	
	%--
	% build and score events
	%--

	for j = 1:size(current, 1)

		%--
		% compute duration and discard short events based on minimum duration
		%--
		
		time = dt * current(j, :);

		if diff(time) < parameter.min_duration
			continue;
		end 

		%--
		% compute event score
		%--
		
		% TODO: factor as a function
		
		zcr = get_fragment_description(value, current(j, :));
		
% 		amp = get_fragment_description(amplitude.rms.value, current(j, :));
		
		if ~in_range(zcr.mean, 0.05, 0.07)
		
			score = 0;
		
		elseif abs(zcr.start - zcr.stop) / min(zcr.start, zcr.stop) > 0.3
			
			score = 0;
			
		else

			score = clip_to_range(1 - 5 * zcr.variation / 100, [0, 1]);

		end

		%--
		% compute frequency bounds depending on configuration
		%--
		
		if parameter.max_freq_active	
			freq = [parameter.min_freq, parameter.max_freq];
		else
			freq = [parameter.min_freq, 0.5 * context.sound.rate];
		end
		
		%--
		% create event
		%--
		
		event(end + 1) = event_create( ...
			'channel', page.channels(k), 'score', score, 'time', time, 'freq', freq ... 
		);

	end

end

%-------------------------------------
% GET_CONSTANT_EVENTS
%-------------------------------------


%-------------------------------------
% GET_FRAGMENT_INDICES
%-------------------------------------

function ix = get_fragment_indices(value)

%--
% compute change in fragment indicator sequence
%--

pad = zeros(1, size(value, 2));

% NOTE: we 'end' edge events with the padding, this ensures the find step finds index pairs

change = diff([pad; value; pad], 1, 1);

%--
% get fragment start and stop indices
%--

% TODO: consider when to discard the edge events in our collection 

% NOTE: the cell packing works over channels

ix = {};

for k = 1:size(value, 2)
	current = [find(change > 0), find(change < 0) - 1]; ix{k} = current;
end


%-------------------------------------
% GET_FRAGMENT_DESCRIPTION
%-------------------------------------

function stat = get_fragment_description(value, ix)

% TODO: this function will compute many of the properties we will also use in the score

value = value(ix(1):ix(2));

stat.mean = mean(value);

stat.deviation = std(value);

stat.variation = 100 * (stat.deviation / stat.mean);

stat.quartiles = fast_rank(value, [0.05, 0.25, 0.5, 0.75, 0.95]);

stat.median = stat.quartiles(3);

stat.start = value(1); stat.stop = value(end);


%------------------------
% SCORE_EVENT
%------------------------

function value = score_event(event)

% TODO: compute score considering range, monotone purity, and duration


%------------------------
% SIMPLIFY
%------------------------

function value = simplify(value, scale)

if nargin < 2
	scale = 5;
end

if islogical(value)
	value = double(value); 
end

% NOTE: use simple morphological filter to simplify binary change signal

se = ones(scale, 1); value = morph_close(morph_open(value, se), se);



