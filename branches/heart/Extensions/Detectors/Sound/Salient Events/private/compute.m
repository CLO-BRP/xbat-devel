function [event, context] = compute(page, parameter, context)

% MEANINGFUL - compute

%--
% get surprisingly loud components
%--

[ext, ix, context2] = get_browser_extension('sound_feature', context.par, 'Surprisingly Loud');

[value, ignore, ignore, key] = get_page_feature(page, ext, context); 

%--
% convert components to events
%--

component = value.component.value;

event = empty(event_create); nyq = [0, 0.5 * context.sound.rate];

for k = 1:numel(component)
	
	current = component(k);
	
	% TODO: develop these tests, and the reasonable correspoding parameters, use more physics
	
	time = clip_to_range(current.time - page.start, [0, inf]); 
	
	freq = clip_to_range(current.freq, nyq);

	test(1) = parameter.censor_duration && (diff(time) < parameter.min_duration);

	test(2) = parameter.censor_bandwidth && (diff(freq) < parameter.min_bandwidth);
	
	if all(test == 1)
		continue; 
	end 
		
	% NOTE: we should make this a meaningfulness computation
	
	if size(current.indices, 1) < 64
		continue;
	end
	
	event(end + 1) = event_create( ...
		'score', component_score(current), ...
		'channel', current.channel, ...
		'time', time, ...
		'freq', freq ...
	);
	
end


%----------------------------
% COMPONENT_SCORE
%----------------------------

function score = component_score(component)

score = 0.5 * (tanh(0.5 * component.mean) + 1);

