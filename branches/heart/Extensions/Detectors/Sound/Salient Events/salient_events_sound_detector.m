function ext = salient_events_sound_detector

ext = extension_create(mfilename);

ext.short_description = 'Detect meaningful occurrences ';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'd82ac506-5719-446a-b30f-0076322d3f58';

ext.author = 'Harold';

ext.email = 'hkf1@cornell.edu';

ext.url = 'http://xbat.org';

