function parameter = parameter__create(context)

% AMPLITUDE DETECTOR I - parameter__create

% parameter = struct;

%--
% get and modify default parent parameters
%--

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

% NOTE: we do not use adaptive frame size as default for detection

parameter.auto = 0;

%--
% append detector parameters
%--

parameter = amplitude_detection_parameters(parameter, context);

