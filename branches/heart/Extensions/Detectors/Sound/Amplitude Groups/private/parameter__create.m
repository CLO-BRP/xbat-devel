function parameter = parameter__create(context)

% AMPLITUDE DETECTOR III - parameter__create

% parameter = struct;

fun = parent_fun(mfilename('fullpath')); parameter = fun(context);

parameter.min_chain_length = 2;

% TODO: figure out a way to set this considering other parameters

parameter.similar = 0.4;

parameter.stretch = 1.5;
