function value_struc = STFT_value_struc

%%
%% 'STFT_value_struc'
%%
%% create the STFT measurement value structure
%%
%% K.A. Cortopassi March 2008
%% --------------------------------------------
%%
%%
%% syntax:
%% -------
%%   value_struc = STFT_value_struc;
%%
%%
%% input:
%% ------
%%
%% output:
%% -------
%%   value = measurement value structure
%%



%% create STFT value structure
%% measures from short-time FFT spectrogram representation

%% notation:
%% p == user-specified energy fraction
%% p1 == like Q1 except using p% instead of 50%
%% p2 == like Q2 except using p% instead of 50%
%% med == median


%%~~
%% measurements made on aggregate power envelope (derived from spectrogram)
value_struc.agg_envelope = []; %% aggregate power envelope

value_struc.p1_time = []; %% event start time defined by p1 of the aggregate power envelope
%value_struc.p2_time = []; %% event end time defined by p2 of the aggregate power envelope
%value_struc.med_time = []; %% event center time defined by median of the aggregate power envelope (energy center of envelope)
value_struc.mpr_time = []; %% time to event center defined by med_time-p1_time
value_struc.ipr_time = []; %% event duration defined by p2_time-p1_time
value_struc.ipr_skew_time = []; %% event temporal skewness defined by (p2_time-med_time)-(med_time-p1_time)/(p2_time-p1_time)

value_struc.conc_time = [];  %% event duration defined as width needed to accumulate p% of signal energy in sorted envelope
value_struc.lower_time = []; %% minimum time encountered in concentration
%value_struc.upper_time = []; %% maximum time encountered in concentration
value_struc.spread_time = []; %% event duration defined by upper_time-lower_time
value_struc.spread_skew_time = []; %% event temporal skewness defined by (upper_time-med_time)-(med_time-lower_time)/(upper_time-lower_time)

%value_struc.peak_time = []; %% time of event energy peak (energy peak of envelope)
value_struc.ppr_time = []; %% time to event energy peak defined by peak_time-p1_time
%value_struc.peak_energy_val_time = []; %% value of event energy peak (energy peak of envelope)

value_struc.entropy_time = []; %% entropy of aggregate power envelope
%value_struc.max_entropy_time = []; %% theoretical maximum of temporal entropy (i.e., value for a uniform distribution)
value_struc.rel_entropy_time = []; %% entropy divided by theoretical maximum entropy



%%~~
%% measurement made on aggregate power spectrum (derived from spectrogram)
value_struc.agg_spectrum = []; %% aggregate power envelope

value_struc.p1_freq = [];  %% event start freq defined by p1 of the aggregate power spectrum
%value_struc.p2_freq = [];  %% event end freq defined by p2 of the aggregate power spectrum
value_struc.med_freq = [];  %% event center freq defined by median of the aggregate power spectrum (energy center of spectrum)
%value_struc.mpr_freq = []; %% freq range to event center defined by med_freq-p1_freq
value_struc.ipr_freq = [];  %% event bandwidth defined by p2_freq-p1_freq
value_struc.ipr_skew_freq = [];  %% event spectral skewness defined by (p2_freq-med_freq)-(med_freq-p1_freq)/(p2_freq-p1_freq)

value_struc.conc_freq = [];   %% event bandwidth defined as the width needed to accumulate p% of signal energy in sorted spectrum
value_struc.lower_freq = [];  %% minimum freq encountered in concentration
%value_struc.upper_freq = [];  %% maximum freq encountered in concentration
value_struc.spread_freq = [];  %% event bandwidth defined by upper_freq-lower_freq
value_struc.spread_skew_freq = []; %% event spectral skewness defined by (upper_freq-med_freq)-(med_freq-lower_freq)/(upper_freq-lower_freq)

value_struc.peak_freq = []; %% frequency of event energy peak (energy peak of spectrum)
%value_struc.ppr_freq = []; %% freq range to event energy peak defined by peak_freq-p1_freq
%value_struc.peak_energy_val_freq = []; %% value of event energy peak (energy peak of spectrum)

value_struc.entropy_freq = [];  %% entropy of aggregate power spectrum
%value_struc.max_entropy_freq = []; %% theoretical maximum of spectral entropy (i.e., value for a uniform distribution)
value_struc.rel_entropy_freq = []; %% entropy divided by theoretical maximum entropy


%%~~
%% measurements made on entire spectrogram
%value_struc.peak_time_spec = []; %% time of spec peak
value_struc.ppr_time_spec = []; %% p1 to peak time
value_struc.peak_freq_spec = []; %% freq of spec peak
%value_struc.peak_energy_val_spec = []; %% energy at spec peak
%value_struc.total_energy_spec = []; %% total spec energy



%%~~
%% time and freq vectors of the ST and NB contours
value_struc.tvec = [];
value_struc.fvec = [];



%%~~
%% sequence of measurements from narrow-band power envelopes, i.e., various contours along frequency
value_struc.NBp1_time_cont = []; %% vector holding p1-time of narrow-band power envelopes
value_struc.NBlower_time_cont = []; %% vector holding lower-time of narrow-band power envelopes

value_struc.NBipr_time_cont = []; %% vector holding ipr-based duration of narrow-band power envelopes
value_struc.NBconc_time_cont = []; %% vector holding concentration-based duration of narrow-band power envelopes
value_struc.NBspread_time_cont = []; %% vector holding spread-based duration of narrow-band power envelopes

value_struc.NBipr_skew_time_cont = []; %% vector holding ipr-based temporal skewness of narrow-band power envelopes
value_struc.NBspread_skew_time_cont = []; %% vector holding spread-based temporal skewness of narrow-band power envelopes

value_struc.NBmpr_time_cont = []; %% vector holding time to center of narrow-band power envelopes
value_struc.NBppr_time_cont = []; %% vector holding time to peak of narrow-band power envelopes
value_struc.NBentropy_time_cont = [];  %% vector holding entropy of narrow-band power envelopes

value_struc.med_NBipr_time = []; %% median of all narrow-band ipr durations
value_struc.med_NBconc_time = []; %% median of all narrow-band concentration durations
value_struc.med_NBspread_time = []; %% median of all narrow-band spread durations

value_struc.med_NBipr_skew_time = []; %% median of all narrow-band ipr skews
value_struc.med_NBspread_skew_time = []; %% median of all narrow-band spread skews

value_struc.med_NBmpr_time = []; %% median of all narrow-band center times
value_struc.med_NBppr_time = []; %% median of all narrow-band peak times
value_struc.med_NBentropy_time = []; %% median of all narrow-band entropies



%%~~
%% sequence of measurements from short-time power spectra, i.e., various contours with time
value_struc.STp1_freq_cont = []; %% vector holding p1-freq of short-time power spectrum
value_struc.STlower_freq_cont = []; %% vector holding lower-freq of short-time power spectrum

value_struc.STipr_freq_cont = []; %% vector holding ipr-based bandwith of short-time ffts
value_struc.STconc_freq_cont = []; %% vector holding concentration-based bandwith of short-time ffts
value_struc.STspread_freq_cont = []; %% vector holding spread-based bandwith of short-time ffts

value_struc.STipr_skew_freq_cont = []; %% vector holding ipr-based spectral skewness of short-time ffts
value_struc.STspread_skew_freq_cont = []; %% vector holding spread-based spectral skewness of short-time ffts

value_struc.STmed_freq_cont = []; %% vector holding center frequency of short-time ffts
value_struc.STpeak_freq_cont = []; %% vector holding peak frequency of short-time ffts
value_struc.STentropy_freq_cont = [];  %% vector holding entropy of short-time ffts

value_struc.med_STipr_freq = []; %% median of all short-time ipr bandwidths
value_struc.med_STconc_freq = []; %% median of all short-time concentration bandwidths
value_struc.med_STspread_freq = []; %% median of all short-time spread bandwidths

value_struc.med_STipr_skew_freq = []; %% median of all short-time ipr skews
value_struc.med_STspread_skew_freq = []; %% median of all short-time spread skews

value_struc.med_STmed_freq = []; %% median of all short-time center frequencies
value_struc.med_STpeak_freq = []; %% median of all short-time peak frequencies
value_struc.med_STentropy_freq = []; %% median of all short-time entropies


%%~~
%% more measures of the median and peak frequency contours with time
value_struc.STmed_freq_cont_cumabsder = []; %% cumulative absolute derivative of median frequency contour
value_struc.STmed_freq_cont_medabsder = []; %% median absolute derivative of median frequency contour
%value_struc.STmed_freq_cont_avgabsder = []; %% average absolute derivative of median frequency contour
value_struc.STmed_freq_cont_cumder = []; %% cumulative derivative of median frequency contour
value_struc.STmed_freq_cont_medder = []; %% median derivative of median frequency contour
%value_struc.STmed_freq_cont_avgder = []; %% average derivative of median frequency contour
value_struc.STmed_freq_cont_extrm = []; %% extremum count of median frequency contour
value_struc.STmed_freq_cont_inflx = []; %% inflexion count of median frequency contour

value_struc.STpeak_freq_cont_cumabsder = []; %% cumulative absolute derivative of peak frequency contour
value_struc.STpeak_freq_cont_medabsder = []; %% median absolute derivative of peak frequency contour
%value_struc.STpeak_freq_cont_avgabsder = []; %% average absolute derivative of peak frequency contour
value_struc.STpeak_freq_cont_cumder = []; %% cumulative derivative of peak frequency contour
value_struc.STpeak_freq_cont_medder = []; %% median derivative of peak frequency contour
%value_struc.STpeak_freq_cont_avgder = []; %% average derivative of peak frequency contour
value_struc.STpeak_freq_cont_extrm = []; %% extremum count of peak frequency contour
value_struc.STpeak_freq_cont_inflx = []; %% inflexion count of peak frequency contour

value_struc.STmed_freq_cont_start = []; %% median frequency contour start freq (contour start time == p1_time)
value_struc.STmed_freq_cont_end = []; %% median frequency contour end freq (contour end time == p2_time)
value_struc.STmed_freq_cont_min = []; %% median frequency contour min freq
value_struc.STmed_freq_cont_max = []; %% median frequency contour max freq
value_struc.STmed_freq_cont_minpr = []; %% time to min freq defined by min_time-p1_time
value_struc.STmed_freq_cont_maxpr = []; %% time to max freq defined by max_time-p1_time

value_struc.STpeak_freq_cont_start = []; %% peak frequency contour start freq (contour start time == p1_time)
value_struc.STpeak_freq_cont_end = []; %% peak frequency contour end freq (contour end time == p2_time)
value_struc.STpeak_freq_cont_min = []; %% peak frequency contour min freq
value_struc.STpeak_freq_cont_max = []; %% peak frequency contour max freq
value_struc.STpeak_freq_cont_minpr = []; %% time to min freq defined by min_time-p1_time
value_struc.STpeak_freq_cont_maxpr = []; %% time to max freq defined by max_time-p1_time



%%~~
%% correlation measures
%% Dur and time contours with frequency
value_struc.NBipr_time_contXfreq_corr = []; %% correlation of NB ipr duration and freq
value_struc.NBconc_time_contXfreq_corr = []; %% correlation of NB concentration duration and freq
value_struc.NBspread_time_contXfreq_corr = []; %% correlation of NB spread duration and freq

value_struc.NBipr_skew_time_contXfreq_corr = []; %% correlation of the NB ipr-skew and freq
value_struc.NBspread_skew_time_contXfreq_corr = []; %% correlation of the NB spread-skew and freq

value_struc.NBmpr_time_contXfreq_corr = []; %% correlation of NB center time and freq
value_struc.NBppr_time_contXfreq_corr = []; %% correlation of NB peak time and freq
value_struc.NBentropy_time_contXfreq = [];  %% correlation of NB entropy and freq

%% BW and frequency contours with time
value_struc.STipr_freq_contXtime_corr = []; %% correlation of the ST ipr-BW and time
value_struc.STconc_freq_contXtime_corr = []; %% correlation of the ST conc-BW and time
value_struc.STspread_freq_contXtime_corr = []; %% correlation of the ST spread-BW and time

value_struc.STipr_skew_freq_contXtime_corr = []; %% correlation of the ST ipr-skew and time
value_struc.STspread_skew_freq_contXtime_corr = []; %% correlation of the ST spread-skew and time

value_struc.STmed_freq_contXtime_corr = []; %% correlation of the ST CF and time
value_struc.STpeak_freq_contXtime_corr = []; %% correlation for the ST PF and time
value_struc.STentropy_freq_contXtime_corr = []; %% correlation of ST entropy and time

%% looking and signal power across time and frequency
value_struc.agg_envXtime_corr = []; %% correlation of signal power and time
value_struc.agg_spcXfreq_corr = []; %% correlation of signal power and frequency



return;