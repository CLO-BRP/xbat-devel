function c = getcolors;

%%
%% 'getcolors'
%% K.A. Cortopassi, July 2003
%% returns rgb colors in intuitve
%% structure
%%

%% red
c.r = [.85 0 0];
%% light red
c.lr = [1 .2 0];

%% orange
c.o = [1 .5 .5];
%% light orange
c.lo = [1 .75 0];

%% yellow
c.y = [1 .99 0];
%% light yellow
c.ly = [1 1 .5];

%% green
c.g = [0 .75 0];
%% light green
c.lg = [0 1 0];

%% blue
c.b = [0 0 1];
%% light blue
c.lb = [.5 .5 1];

%% indigo
c.i = [.5 .5 .75];

%% purple
c.p = [.45 0 .75];
%% light purple
c.lp = [.65 0 .95];

%% violet
c.v = [.55 0 .55];
%% light violet
c.lv = [.75 0 .75];

%% magenta
c.m = [1 0 1];

%% cyan
c.cy = [0 .65 .65];
%% light cyan
c.lcy = [.2 1 1];

%% black
c.k = [0 0 0];

%% white
c.w = [1 1 1];

%% gray
c.gy = [.5 .5 .5];


