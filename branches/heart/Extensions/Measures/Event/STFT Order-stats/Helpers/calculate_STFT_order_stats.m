function [value, param] = calculate_STFT_order_stats(sound, event, param)

%
% 'calculate_STFT_order_stats'
%
% compute the actual measurement
%
% --------------------------------------------
% 'STFT_order_stats_measurement'
%
% Calculate signal energy distribution measurements
% from STFTs of the sound events using robust statistics;
% do parallel short-time and narrow-bandwidth measurements,
% include a number of contour measurements
%
% By K.A. Cortopassi, March 2008
% --------------------------------------------
%
%
% syntax:
% -------
%   value = calculate_STFT_order_stats(sound, event, param)
%
%
% input:
% ------
%   sound = sound associated with event
%   event = event to measure, a single event
%   param = measurement parameter structure
%
%
% output:
% -------
%   value = measurement value structure
%


%~~  print line to peek in ~~%
% fprintf(1, 'Processing event #%d...\n', event.id);


% set up empty value structure
value = STFT_value_struc;


% rename sample rate
% deal with the possibility of resampling being configured
fs = sound.output.rate;
if isempty(fs)
	fs = sound.samplerate; end
nyquist = fs / 2;

% max time
max_time = sound.duration;

% total samples
max_samp = sound.cumulative(end);



%~~~~~
% set up data structure for using fast_specgram function
%~~~~~
fastspc_param = fast_specgram;

fastspc_param.fft = param.fftsz; % in points

% win_length is given as a fraction of FFT size
fastspc_param.win_length = param.datawin;

% hop is given as a fraction of FFT size;
% data windows are zero padded to fft size
% and data increment = fftsz * hop
fastspc_param.hop = (1 - param.overlap) * param.datawin;

% window type is given as a string
fastspc_param.win_type = param.winfunc;

% not sure this even works in 'fast_specgram', but make sure
% that spectra are not aggregated
fastspc_param.sum_length = 1;




%~~~~~
% compute grid resolution for spectrogram
df = fs / param.fftsz;
dt = round((1 - param.overlap) * param.datawin * param.fftsz) / fs;

half_df = df / 2;
half_dt = dt / 2;



%~~~~~
% get event data, demean, and filter
%~~~~~

% get event start and duration
ev_starttm = event.time(1); % in seconds
ev_dur = event.time(2) - event.time(1); % in seconds

% check for zero length events
if (ev_dur == 0)
	% empty event!  skip it
	fprintf('Event %d is empty. Skipping...\n', event.id);
	return;
end

% get <event + pad> start and duration
starttm = ev_starttm - param.ev_pad; % in seconds

if (starttm < 0)
	% not enough data for event!  skip it
	fprintf('Not enough data to process event %d. Skipping...\n', event.id);
	return;
end

startpt = floor(starttm * fs);
totalpts = ceil((2*param.ev_pad + ev_dur) * fs);

if (startpt + totalpts > max_samp)
	% not enough data for event!  skip it
	fprintf('Not enough data to process event %d. Skipping...\n', event.id);
	return;
end

try
	% try getting <event + pad> data
	data4spc = sound_read(sound, 'samples', startpt, totalpts, event.channel);
	% de-mean the data
	data4spc = data4spc - mean(data4spc);
	
catch
	% problem reading data for <event + pad>!  skip it
	fprintf('Problem reading data to process event %d. Skipping...\n', event.id);
	return;
	
end


%~~
% apply waveform denoising from browser
if (param.filter_wav) && ~strcmpi(param.filter_ext, 'none')
	% set up some minimal context info
	context.sound = sound;
	% filter
	data4spc = apply_signal_filter(data4spc, param.filter_ext, context);
end




%~~~~~
% calculate FFT-based time-frequency spectrogram
%~~~~~

% get time-frequency spectrograms with power values
[sigspc, fvecspc, tvecspc] = fast_specgram(data4spc, fs, 'power', fastspc_param);

% apply correct offset to time vector
tvecspc = tvecspc + starttm;

fvecspc = fvecspc'; % make these row vectors
tvecspc = tvecspc';



%~~~~~
% bandlimit the spectrogram
%~~~~~

if ~param.std_frange
	% use event extent to bandlimit
	param.flo = event.freq(1);
	param.fhi = event.freq(2);
end

% 'flo' always >= 0; 'fhi' always <= Nyquist;
% fftsz always even
% so won't need any error checking for indices...
startInx = param.flo/df + 1;
endInx = param.fhi/df + 1;

% check for non-integer indices
if (mod(startInx,1) == 0)
	startWt = 1;
else
	% do a little interpolation-like weighting of first freq bin
	startWt = ceil(startInx) - startInx;
	startInx = floor(startInx);
end

if (mod(endInx,1) == 0)
	endWt = 1;
else
	% do a little interpolation-like weighting of last freq bin
	endWt = endInx - floor(endInx);
	endInx = ceil(endInx);
end

% pull out the frequency range of the spectrogram
sigspc = sigspc(startInx:endInx, :);
fvecspc = fvecspc(startInx:endInx);

% weight the first and last freq bins
sigspc(1, :) = startWt .* sigspc(1, :);
sigspc(end, :) = endWt .* sigspc(end, :);



%~~~~~
% denoise the spectrogram
%~~~~~
rawsigspc = sigspc;
if param.denoise_spc
	sigspc = denoisespc_v2(sigspc, param.denoise_type, param.denoise_perc,...
		'Power', 'Bias');
end

%~~~~~
% plot spec before and after denoising
if param.plot_spc
	
	% get index to event part of the spectrogram
	inx1 = find(tvecspc <= ev_starttm);
	if isempty(inx1), inx1 = 1;
	else inx1 = inx1(end); end
	
	inx2 = find(tvecspc >= (ev_starttm + ev_dur));
	if isempty(inx2), inx2 = length(tvecspc);
	else inx2 = inx2(1); end
	
	param.plot_spc = STFT_spec_display('create_ui', rawsigspc, sigspc,...
		tvecspc, fvecspc, event.id, tvecspc(inx1), tvecspc(inx2));
	
end

% *** use the padded event for making measurements!!! ***%
% % now trim to event width
% tvecspc = tvecspc(inx1:inx2);
% sigspc = sigspc(:, inx1:inx2);




%~~~~~
% make estimates of duration and bandwidth
%~~~~~

% compute the power vs time envelope function
pow_env = sum(sigspc, 1);
value.agg_envelope = pow_env;

% compute the power vs frequency spectrum function
pow_spc = sum(sigspc, 2)'; % notice the transpose
value.agg_spectrum = pow_spc;


%~~~~~
% IPR (inter-percentile) calculations
%~~~~~
% find percentile time and frequency values--
% (i.e., those time and frequency values which mark the (user-entered)
% fractions p1 and p2 for the cumulative power of the time envelope or
% frequency spectrum)
% use the inter-percentile range to obtain bandwidth/duration estimates
% return skewness of the event in time and frequency as well

% convert to fraction
param.meas_perc = param.meas_perc / 100;

p1 = (1 - param.meas_perc) / 2;
p2 = param.meas_perc + p1;
p = [p1, p2, 0.5];

%~~~~~
% calculate for time; use 'get_percentiles' with interpolation
ptinx = get_percentiles(pow_env, p);
ptinx_interp_int = floor(ptinx);
ptinx_interp_frac = ptinx - ptinx_interp_int;

% get actual times; buffer values with half_dt to deal with discretization
% so that all durations will be at least dt
if ptinx_interp_int(1) == 0
	value.p1_time = min(max(0, tvecspc(1) - dt +...
		(ptinx_interp_frac(1) * dt) - half_dt), max_time);
else
	value.p1_time = min(max(0, tvecspc(ptinx_interp_int(1)) +...
		(ptinx_interp_frac(1) * dt) - half_dt), max_time);
end

if ptinx_interp_int(2) == 0
	p2_time = max(min(max_time, tvecspc(1) - dt +...
		(ptinx_interp_frac(2) * dt) + half_dt), 0);
else
	p2_time = max(min(max_time, tvecspc(ptinx_interp_int(2)) +...
		(ptinx_interp_frac(2) * dt) + half_dt), 0);
end

if ptinx_interp_int(3) == 0
	med_time = max(min(max_time, tvecspc(1) - dt +...
		(ptinx_interp_frac(3) * dt)), 0);
else
	med_time = max(min(max_time, tvecspc(ptinx_interp_int(3)) +...
		(ptinx_interp_frac(3) * dt)), 0);
end

value.mpr_time =  med_time - value.p1_time;
value.ipr_time = p2_time - value.p1_time;
value.ipr_skew_time = (p2_time + value.p1_time - 2*med_time)/value.ipr_time;
% 0 == no skew; positive == right skew; negative == left skew


%~~~~~
% calculate for frequency; use 'get_percentiles' with interpolation
pfinx = get_percentiles(pow_spc, p);
pfinx_interp_int = floor(pfinx);
pfinx_interp_frac = pfinx - pfinx_interp_int;

% get actual frequencies; buffer values with half_df to deal with discretization
% so that all bandwidths will be at least df
if pfinx_interp_int(1) == 0
	value.p1_freq = min(max(0, fvecspc(1) - df +...
		(pfinx_interp_frac(1) * df) - half_df), nyquist);
else
	value.p1_freq = min(max(0, fvecspc(pfinx_interp_int(1)) +...
		(pfinx_interp_frac(1) * df) - half_df), nyquist);
end

if pfinx_interp_int(2) == 0
	p2_freq = max(min(nyquist, fvecspc(1) - df +...
		(pfinx_interp_frac(2) * df) + half_df), 0);
else
	p2_freq = max(min(nyquist, fvecspc(pfinx_interp_int(2)) +...
		(pfinx_interp_frac(2) * df) + half_df), 0);
end

if pfinx_interp_int(3) == 0
	value.med_freq = max(min(nyquist, fvecspc(1) - df +...
		(pfinx_interp_frac(3) * df)), 0);
else
	value.med_freq = max(min(nyquist, fvecspc(pfinx_interp_int(3)) +...
		(pfinx_interp_frac(3) * df)), 0);
end

%value.mpr_freq = value.med_freq - value.p1_freq;
value.ipr_freq = p2_freq - value.p1_freq;
value.ipr_skew_freq = (p2_freq + value.p1_freq - 2*value.med_freq)/value.ipr_freq;
% 0 == no skew; positive == right skew; negative == left skew




%~~~~~
% concentration/spread calculations
%~~~~~
% find the time and frequency concentration:
% -- the number of bins needed to accumulate the user-specified percentage
% of the total signal energy in the sorted aggregate envelope, converted
% to time
% -- the number of bins needed to accumulate the user-specified percentage
% of the total signal energy in the sorted aggregate spectrum, converted
% to frequency

[sort_pow_env, env_inx] = sort(pow_env);
[sort_pow_spc, spc_inx] = sort(pow_spc);

% get descending values
sort_pow_env = fliplr(sort_pow_env);
env_inx = fliplr(env_inx);
sort_pow_spc = fliplr(sort_pow_spc);
spc_inx = fliplr(spc_inx);

% calculate for the user specified percentile
% with interpolation; for CDF this returns number of bins required
[ptinx2] = get_percentiles(sort_pow_env, param.meas_perc);
[pfinx2] = get_percentiles(sort_pow_spc, param.meas_perc);

% think about whether this should be reported as a time/freq or number of
% bins; if a time/freq, should this be pinx-1 so that conc is always less
% than or equal to spread?? -- no, I don't think so
value.conc_time = ptinx2 * dt;
value.conc_freq = pfinx2 * df;

%~~~~~
% find the highest and lowest values encountered in the concentration bins
% -- upper is the largest time or frequency value found in the set of bins
% needed to accumulate p% of the total signal energy, and
% -- lower is the smallest time or frequency value found in the set of bins
% needed to accumulate p% of the total signal energy

% buffer values with half_dt/_df to deal with discretization so all durations
% will be at least dt/df
value.lower_time = min(max(0, tvecspc(min(env_inx(1:ceil(ptinx2)))) -...
	half_dt), max_time);
upper_time = max(min(max_time, tvecspc(max(env_inx(1:ceil(ptinx2)))) +...
	half_dt), 0);

value.lower_freq = min(max(0, fvecspc(min(spc_inx(1:ceil(pfinx2)))) -...
	half_df), nyquist);
upper_freq = max(min(nyquist, fvecspc(max(spc_inx(1:ceil(pfinx2)))) +...
	half_df), 0);

% find the spread and the associated skewness
value.spread_time = upper_time - value.lower_time;
value.spread_skew_time = (med_time - value.lower_time) / value.spread_time;

value.spread_freq = upper_freq - value.lower_freq;
value.spread_skew_freq = (value.med_freq - value.lower_freq) / value.spread_freq;



%~~~~~
% calculate peak values
%~~~~~
% get peaks for the aggregates
% find the peak time and frequency values -- time or frequency bin
% containing the most signal energy; remember the half_dt/half_df padding
[pk_val, pk_inx] = max(pow_env);
%value.peak_time = tvecspc(pk_inx);
value.ppr_time = tvecspc(pk_inx) - half_dt - value.p1_time;

[pk_val, pk_inx] = max(pow_spc);
value.peak_freq = fvecspc(pk_inx) - half_df;
%value.ppr_freq = value.peak_freq - value.p1_freq;


% and for the whole spectrogram
N = length(fvecspc);
[pk_val, pk_inx] = max(sigspc(:));
% convert advanced inx to row/column values
r = mod(pk_inx, N);
if (r == 0), r = N; end
c = (pk_inx - r + N) / N;
%value.peak_time_spec = tvecspc(c);
value.ppr_time_spec = tvecspc(c) - half_dt - value.p1_time;
value.peak_freq_spec = fvecspc(r) - half_df;



%~~~~~
% calculate entropy values for aggregate power spectrum and envelope using
% Shannon's entropy measure H = -sum[p(xi)log2(p(xi))]
%~~~~~
% just go with the binning we already have from the spectrogram
prob_dist_env = pow_env / sum(pow_env);
prob_dist_spc = pow_spc / sum(pow_spc);

% remove any zero probability values
prob_dist_env = prob_dist_env((prob_dist_env ~= 0));
prob_dist_spc = prob_dist_spc((prob_dist_spc ~= 0));

% calculate the Shannon entropy
value.entropy_time = -sum(prob_dist_env .* log2(prob_dist_env));
value.entropy_freq = -sum(prob_dist_spc .* log2(prob_dist_spc));

% % compute the maximum entropy value for the distribution
% value.max_entropy_time = log2(length(pow_env));
% value.max_entropy_freq = log2(length(pow_spc));

% calculate the relative entropy
value.rel_entropy_time = value.entropy_time / log2(length(pow_env));
value.rel_entropy_freq = value.entropy_freq / log2(length(pow_spc));




%~~~~~
% get NB (narrow-band) contours
%~~~~~

% get spectrogram over IPR freq range
reduced_frange = max(1, floor(pfinx(1))):ceil(pfinx(2));
freq_reduced_sigspc = sigspc(reduced_frange, :)'; % notice the transpose
% get the freq vector over the IPR range
freq_reduced_fvecspc = fvecspc(reduced_frange);
value.fvec = freq_reduced_fvecspc;


% get NB median, ipr, and skew contours
[pinx] = get_percentiles(freq_reduced_sigspc, p);
pinx_interp_frac = pinx - floor(pinx);
pinx = floor(pinx);

if size(pinx, 1) == 1
	if pinx(1) == 0
		p1_time_cont = min(max(0, tvecspc(1) - dt + (pinx_interp_frac(1) * dt) - half_dt), max_time);
	else
		p1_time_cont = min(max(0, tvecspc(pinx(1)) + (pinx_interp_frac(1) * dt) - half_dt), max_time);
	end
	if pinx(2) == 0
		p2_time_cont = max(min(max_time, tvecspc(1) - dt + (pinx_interp_frac(2) * dt) + half_dt), 0);
	else
		p2_time_cont = max(min(max_time, tvecspc(pinx(2)) + (pinx_interp_frac(2) * dt) + half_dt), 0);
	end
	if pinx(3) == 0
		med_time_cont = max(min(tvecspc(1) - dt + (pinx_interp_frac(3) * dt), max_time), 0);
	else
		med_time_cont = max(min(tvecspc(pinx(3)) + (pinx_interp_frac(3) * dt), max_time), 0);
	end
else
	if any(pinx(1, :) == 0)
		p1_time_cont = min(max(0, (tvecspc(1) - dt) + (pinx(1, :) * dt) + (pinx_interp_frac(1, :) * dt) - half_dt), max_time);
	else
		p1_time_cont = min(max(0, tvecspc(pinx(1, :)) + (pinx_interp_frac(1, :) * dt) - half_dt), max_time);
	end
	if any(pinx(2, :) == 0)
		p2_time_cont = max(min(max_time, (tvecspc(1) - dt) + (pinx(2, :) * dt) + (pinx_interp_frac(2, :) * dt) + half_dt), 0);
	else
		p2_time_cont = max(min(max_time, tvecspc(pinx(2, :)) + (pinx_interp_frac(2, :) * dt) + half_dt), 0);
	end
	if any(pinx(3, :) == 0)
		med_time_cont = max(min((tvecspc(1) - dt) + (pinx(3, :) * dt) + (pinx_interp_frac(3, :) * dt), max_time), 0);
	else
		med_time_cont = max(min(tvecspc(pinx(3, :)) + (pinx_interp_frac(3, :) * dt), max_time), 0);
	end
end

value.NBp1_time_cont = p1_time_cont;
value.NBmpr_time_cont = med_time_cont - p1_time_cont;
value.NBipr_time_cont = p2_time_cont - p1_time_cont;
value.NBipr_skew_time_cont = (p2_time_cont + p1_time_cont - 2*med_time_cont) ./ value.NBipr_time_cont;


% get NB conc, spread, and skew
[sort_freq_reduced_sigspc, sort_inx] = sort(freq_reduced_sigspc);

% get descending values
sort_freq_reduced_sigspc = flipud(sort_freq_reduced_sigspc);
sort_inx = flipud(sort_inx);

% calculate for the user specified percentile
[pinx] = get_percentiles(sort_freq_reduced_sigspc, param.meas_perc);

num_val = length(pinx);
lower_time_cont(num_val) = 0;
upper_time_cont(num_val) = 0;
for loop_inx = 1:num_val
	lower_time_cont(loop_inx) = min(max(0, tvecspc(min(sort_inx(1:ceil(pinx(loop_inx)), loop_inx))) - half_dt), max_time); % buffer values with half_dt to
	upper_time_cont(loop_inx) = max(min(max_time, tvecspc(max(sort_inx(1:ceil(pinx(loop_inx)), loop_inx))) + half_dt), 0); % deal with discretization
	% so all durations will be at least dt
end

value.NBlower_time_cont = lower_time_cont;
value.NBconc_time_cont = pinx * dt;
value.NBspread_time_cont = upper_time_cont - lower_time_cont;
value.NBspread_skew_time_cont = (med_time_cont - lower_time_cont) ./ value.NBspread_time_cont;


% get NB peak cont
[pk_val, pk_inx] = max(freq_reduced_sigspc);
value.NBppr_time_cont = tvecspc(pk_inx) - p1_time_cont;


% get NB entopy
warning off MATLAB:divideByZero
% if one whole narrowband envelope is zero-valued, a nan-valued probability distribution is returned
% deal with this at the end
prob_dist_env = freq_reduced_sigspc ./ (ones(size(freq_reduced_sigspc, 1), 1) * sum(freq_reduced_sigspc, 1));
warning on MATLAB:divideByZero

num_val = size(prob_dist_env, 2);
temp_entropy_cont(num_val) = 0;
for loop_inx = 1:num_val
	prob_dist = prob_dist_env(:, loop_inx);
	prob_dist = prob_dist((prob_dist ~= 0));
	temp_entropy_cont(loop_inx) = -sum(prob_dist .* log2(prob_dist));
end

% set the nan and nan+nani values to just nan
temp_entropy_cont(isnan(temp_entropy_cont)) = nan;
value.NBentropy_time_cont = temp_entropy_cont;
% get median of only non-nan values (i.e., non-zero envelopes)
value.med_NBentropy_time = median(temp_entropy_cont(~isnan(temp_entropy_cont)));


% get other contour medians
value.med_NBmpr_time = median(value.NBmpr_time_cont);
value.med_NBipr_time = median(value.NBipr_time_cont);
value.med_NBipr_skew_time = median(value.NBipr_skew_time_cont);

value.med_NBconc_time = median(value.NBconc_time_cont);
value.med_NBspread_time = median(value.NBspread_time_cont);
value.med_NBspread_skew_time = median(value.NBspread_skew_time_cont);

value.med_NBppr_time = median(value.NBppr_time_cont);


%***
% is there any more to be done with the contours?
% what can I measure and summarize???




%~~~~~
% get ST (short-time) contours
%~~~~~

% get spectrogram over IPR time range
reduced_trange = max(1, floor(ptinx(1))):ceil(ptinx(2));
time_reduced_sigspc = sigspc(:, reduced_trange);
% get the time vector over the IPR range
time_reduced_tvecspc = tvecspc(reduced_trange);
value.tvec = time_reduced_tvecspc;


% get ST median, ipr, and skew cont
[pinx] = get_percentiles(time_reduced_sigspc, p);
pinx_interp_frac = pinx - floor(pinx);
pinx = floor(pinx);

if size(pinx, 1) == 1
	if pinx(1) == 0
		p1_freq_cont = min(max(0, fvecspc(1) - df + (pinx_interp_frac(1) * df) - half_df), nyquist);
	else
		p1_freq_cont = min(max(0, fvecspc(pinx(1)) + (pinx_interp_frac(1) * df) - half_df), nyquist);
	end
	if pinx(2) == 0
		p2_freq_cont = max(min(nyquist, fvecspc(1) - df + (pinx_interp_frac(2) * df) + half_df), 0);
	else
		p2_freq_cont = max(min(nyquist, fvecspc(pinx(2)) + (pinx_interp_frac(2) * df) + half_df), 0);
	end
	if pinx(3) == 0
		med_freq_cont = max(min(fvecspc(1) -df + (pinx_interp_frac(3) * df), nyquist), 0);
	else
		med_freq_cont = max(min(fvecspc(pinx(3)) + (pinx_interp_frac(3) * df), nyquist), 0);
	end
else
	if any(pinx(1, :) == 0)
		p1_freq_cont = min(max(0, (fvecspc(1) - df) + (pinx(1, :) * df) + (pinx_interp_frac(1, :) * df) - half_df), nyquist);
	else
		p1_freq_cont = min(max(0, fvecspc(pinx(1, :)) + (pinx_interp_frac(1, :) * df) - half_df), nyquist);
	end
	if any(pinx(2, :) == 0)
		p2_freq_cont = max(min(nyquist, (fvecspc(1) - df) + (pinx(2, :) * df) + (pinx_interp_frac(2, :) * df) + half_df), 0);
	else
		p2_freq_cont = max(min(nyquist, fvecspc(pinx(2, :)) + (pinx_interp_frac(2, :) * df) + half_df), 0);
	end
	if any(pinx(3, :) == 0)
		med_freq_cont = max(min((fvecspc(1) - df) + (pinx(3, :) * df) + (pinx_interp_frac(3, :) * df), nyquist), 0);
	else
		med_freq_cont = max(min(fvecspc(pinx(3, :)) + (pinx_interp_frac(3, :) * df), nyquist), 0);
	end
end

value.STp1_freq_cont = p1_freq_cont;
value.STmed_freq_cont = med_freq_cont;
value.STipr_freq_cont = p2_freq_cont - p1_freq_cont;
value.STipr_skew_freq_cont = (p2_freq_cont + p1_freq_cont - 2*med_freq_cont) ./ value.STipr_freq_cont;


% get ST conc, spread, and skew
[sort_time_reduced_sigspc, sort_inx] = sort(time_reduced_sigspc);

% get descending values
sort_time_reduced_sigspc = flipud(sort_time_reduced_sigspc);
sort_inx = flipud(sort_inx);

% calculate for the user specified percentile
[pinx] = get_percentiles(sort_time_reduced_sigspc, param.meas_perc);

num_val = length(pinx);
lower_freq_cont(num_val) = 0;
upper_freq_cont(num_val) = 0;
for loop_inx = 1:num_val
	lower_freq_cont(loop_inx) = min(max(0, fvecspc(min(sort_inx(1:ceil(pinx(loop_inx)), loop_inx))) - half_df), nyquist); % buffer values with half_df to
	upper_freq_cont(loop_inx) = max(min(nyquist, fvecspc(max(sort_inx(1:ceil(pinx(loop_inx)), loop_inx))) + half_df), 0); % deal with discretization
	% so all bandwidths will be at least df
end

value.STlower_freq_cont = lower_freq_cont;
value.STconc_freq_cont = pinx * df;
value.STspread_freq_cont = upper_freq_cont - lower_freq_cont;
value.STspread_skew_freq_cont = (med_freq_cont - lower_freq_cont) ./ value.STspread_freq_cont;


% get ST peak cont
[pk_val, pk_inx] = max(time_reduced_sigspc);
value.STpeak_freq_cont = fvecspc(pk_inx);


% get ST entopy
warning off MATLAB:divideByZero
% if one whole spectrum is zero-valued, a nan-valued probability distribution is returned
% deal with this at the end
prob_dist_spc = time_reduced_sigspc ./ (ones(size(time_reduced_sigspc, 1), 1) * sum(time_reduced_sigspc, 1));
warning on MATLAB:divideByZero

num_val = size(prob_dist_spc, 2);
spc_entropy_cont(num_val) = 0;
for i = 1:size(prob_dist_spc, 2)
	prob_dist = prob_dist_spc(:, i);
	prob_dist = prob_dist((prob_dist ~= 0));
	spc_entropy_cont(i) = -sum(prob_dist .* log2(prob_dist));
	%figure; plot(prob_dist, 'o'); title(sprintf('entropy = %f',spc_entropy_cont(i))); % a little snoop line
end

% set the nan and nan+nani values to just nan
spc_entropy_cont(isnan(spc_entropy_cont)) = nan;
value.STentropy_freq_cont = spc_entropy_cont;
% get median of only non-nan values (i.e., non-zero spectra)
value.med_STentropy_freq = median(spc_entropy_cont(~isnan(spc_entropy_cont)));


% get other contour medians
value.med_STmed_freq = median(value.STmed_freq_cont);
value.med_STipr_freq = median(value.STipr_freq_cont);
value.med_STipr_skew_freq = median(value.STipr_skew_freq_cont);

value.med_STconc_freq = median(value.STconc_freq_cont);
value.med_STspread_freq = median(value.STspread_freq_cont);
value.med_STspread_skew_freq = median(value.STspread_skew_freq_cont);

value.med_STpeak_freq = median(value.STpeak_freq_cont);




%***
% is there any more to be done with the contours?
% what can I measure and summarize???




%~~~~~
% get additional measures on the STmed_freq_cont and STpeak_freq_cont
%~~~~~

% return derivative measures in Hz/sec for the smoothed STmed_freq_cont
% (3-point smoothing)
mfc_der = diff(smooth(value.STmed_freq_cont, 3))/dt;
mfc_2ndder = diff(mfc_der)/dt;
abs_mfc_der = abs(mfc_der);
sign_mfc_der = sign(mfc_der);
sign_mfc_2ndder = sign(mfc_2ndder);
if isempty(mfc_der)
	fprintf(1, 'Warning: No Median Frequency Contour...\n');
else
	value.STmed_freq_cont_cumabsder = sum(abs_mfc_der);
	value.STmed_freq_cont_medabsder = median(abs_mfc_der);
	%value.STmed_freq_cont_avgabsder = mean(abs_mfc_der);
	value.STmed_freq_cont_cumder = sum(mfc_der);
	value.STmed_freq_cont_medder = median(mfc_der);
	%value.STmed_freq_cont_avgder = mean(mfc_der);
	value.STmed_freq_cont_extrm = length(find((sign_mfc_der(1:end-1) == 1 & sign_mfc_der(2:end) == -1) |...
		(sign_mfc_der(1:end-1) == -1 & sign_mfc_der(2:end) == 1)));
	value.STmed_freq_cont_inflx = length(find((sign_mfc_2ndder(1:end-1) == 1 & sign_mfc_2ndder(2:end) == -1) |...
		(sign_mfc_2ndder(1:end-1) == -1 & sign_mfc_2ndder(2:end) == 1)));
end

% start and end points for STmed_freq_cont
value.STmed_freq_cont_start = value.STmed_freq_cont(1);
value.STmed_freq_cont_end = value.STmed_freq_cont(end);
[value.STmed_freq_cont_min, inx] = min(value.STmed_freq_cont);
value.STmed_freq_cont_minpr = time_reduced_tvecspc(inx) - value.p1_time;
[value.STmed_freq_cont_max, inx] = max(value.STmed_freq_cont);
value.STmed_freq_cont_maxpr = time_reduced_tvecspc(inx) - value.p1_time;


% return derivative measures in Hz/sec for the smoothed STpeak_freq_cont
% (3-point smoothing)
pfc_der = diff(smooth(value.STpeak_freq_cont, 3))/dt;
pfc_2ndder = diff(pfc_der)/dt;
abs_pfc_der = abs(pfc_der);
sign_pfc_der = sign(pfc_der);
sign_pfc_2ndder = sign(pfc_2ndder);
if isempty(pfc_der)
	fprintf(1, 'Warning: No Peak Frequency Contour...\n');
else
	value.STpeak_freq_cont_cumabsder = sum(abs_pfc_der);
	value.STpeak_freq_cont_medabsder = median(abs_pfc_der);
	%value.STpeak_freq_cont_avgabsder = mean(abs_pfc_der);
	value.STpeak_freq_cont_cumder = sum(pfc_der);
	value.STpeak_freq_cont_medder = median(pfc_der);
	%value.STpeak_freq_cont_avgder = mean(pfc_der);
	value.STpeak_freq_cont_extrm = length(find((sign_pfc_der(1:end-1) == 1 & sign_pfc_der(2:end) == -1) |...
		(sign_pfc_der(1:end-1) == -1 & sign_pfc_der(2:end) == 1)));
	value.STpeak_freq_cont_inflx = length(find((sign_pfc_2ndder(1:end-1) == 1 & sign_pfc_2ndder(2:end) == -1) |...
		(sign_pfc_2ndder(1:end-1) == -1 & sign_pfc_2ndder(2:end) == 1)));
end

% start and end points for STpeak_freq_cont
value.STpeak_freq_cont_start = value.STpeak_freq_cont(1);
value.STpeak_freq_cont_end = value.STpeak_freq_cont(end);
[value.STpeak_freq_cont_min, inx] = min(value.STpeak_freq_cont);
value.STpeak_freq_cont_minpr = time_reduced_tvecspc(inx) - value.p1_time;
[value.STpeak_freq_cont_max, inx] = max(value.STpeak_freq_cont);
value.STpeak_freq_cont_maxpr = time_reduced_tvecspc(inx) - value.p1_time;



%***
% is there any more to be done with the med and peak freq contours?
% what can I measure and summarize???



%%****** DO I REALLY WANT THESE CORRELATION MEASURES????? *****

%~~~~~
% calculate correlation measures
%~~~~~

% NB contours with freq
if length(freq_reduced_fvecspc) > 1
	% contours have at least 2 members
	
	if std(value.NBipr_time_cont(1, :))
		Rvalue = corrcoef(value.NBipr_time_cont(1, :), freq_reduced_fvecspc);
		value.NBipr_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBipr_time_contXfreq_corr = 0;
	end
	
	if std(value.NBconc_time_cont(1, :))
		Rvalue = corrcoef(value.NBconc_time_cont(1, :), freq_reduced_fvecspc);
		value.NBconc_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBconc_time_contXfreq_corr = 0;
	end
	
	if std(value.NBspread_time_cont(1, :))
		Rvalue = corrcoef(value.NBspread_time_cont(1, :), freq_reduced_fvecspc);
		value.NBspread_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBspread_time_contXfreq_corr = 0;
	end
	
	if std(value.NBipr_skew_time_cont)
		Rvalue = corrcoef(value.NBipr_skew_time_cont, freq_reduced_fvecspc);
		value.NBipr_skew_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBipr_skew_time_contXfreq_corr = 0;
	end
	
	if std(value.NBspread_skew_time_cont)
		Rvalue = corrcoef(value.NBspread_skew_time_cont, freq_reduced_fvecspc);
		value.NBspread_skew_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBspread_skew_time_contXfreq_corr = 0;
	end
	
	if std(value.NBmpr_time_cont(1, :))
		Rvalue = corrcoef(value.NBmpr_time_cont(1, :), freq_reduced_fvecspc);
		value.NBmpr_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBmpr_time_contXfreq_corr = 0;
	end
	
	if std(value.NBppr_time_cont(1, :))
		Rvalue = corrcoef(value.NBppr_time_cont(1, :), freq_reduced_fvecspc);
		value.NBppr_time_contXfreq_corr = Rvalue(1, 2);
	else
		value.NBppr_time_contXfreq_corr = 0;
	end
	
	% correlate only over non-infinity and non-nan values
	inx = ~isinf(value.NBentropy_time_cont) & ~isnan(value.NBentropy_time_cont);
	if sum(inx) > 1
		% non-nan -inf track has at least 2 members
		if std(value.NBentropy_time_cont(inx))
			Rvalue = corrcoef(value.NBentropy_time_cont(inx), freq_reduced_fvecspc(inx));
			value.NBentropy_time_contXfreq = Rvalue(1, 2);
		else
			value.NBentropy_time_contXfreq = 0;
		end
	end
	
	if std(pow_spc(reduced_frange))
		Rvalue = corrcoef(pow_spc(reduced_frange), freq_reduced_fvecspc);
		value.agg_spcXfreq_corr = Rvalue(1, 2);
	else
		value.agg_spcXfreq_corr = 0;
	end
	
end


% ST contours with time
if length(time_reduced_tvecspc) > 1
	% contours have at least 2 members
	
	% 1st check that standard deviation is non-zero (with zero stddev corrcoeff returns nans)
	if std(value.STipr_freq_cont(1, :))
		Rvalue = corrcoef(value.STipr_freq_cont(1, :), time_reduced_tvecspc);
		value.STipr_freq_contXtime_corr = Rvalue(1, 2);
	else
		% if zero, then correlation is zero
		value.STipr_freq_contXtime_corr = 0;
	end
	
	if std(value.STconc_freq_cont(1, :))
		Rvalue = corrcoef(value.STconc_freq_cont(1, :), time_reduced_tvecspc);
		value.STconc_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STconc_freq_contXtime_corr = 0;
	end
	
	if std(value.STspread_freq_cont(1, :))
		Rvalue = corrcoef(value.STspread_freq_cont(1, :), time_reduced_tvecspc);
		value.STspread_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STspread_freq_contXtime_corr = 0;
	end
	
	if std(value.STipr_skew_freq_cont)
		Rvalue = corrcoef(value.STipr_skew_freq_cont, time_reduced_tvecspc);
		value.STipr_skew_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STipr_skew_freq_contXtime_corr = 0;
	end
	
	if std(value.STspread_skew_freq_cont)
		Rvalue = corrcoef(value.STspread_skew_freq_cont, time_reduced_tvecspc);
		value.STspread_skew_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STspread_skew_freq_contXtime_corr = 0;
	end
	
	if std(value.STmed_freq_cont(1, :))
		Rvalue = corrcoef(value.STmed_freq_cont(1, :), time_reduced_tvecspc);
		value.STmed_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STmed_freq_contXtime_corr = 0;
	end
	
	if std(value.STpeak_freq_cont(1, :))
		Rvalue = corrcoef(value.STpeak_freq_cont(1, :), time_reduced_tvecspc);
		value.STpeak_freq_contXtime_corr = Rvalue(1, 2);
	else
		value.STpeak_freq_contXtime_corr = 0;
	end
	
	% correlate only over non-infinity and non-nan values
	inx = ~isinf(value.STentropy_freq_cont) & ~isnan(value.STentropy_freq_cont);
	if sum(inx) > 1
		% non-nan -inf track has at least 2 members
		if std(value.STentropy_freq_cont(inx))
			Rvalue = corrcoef(value.STentropy_freq_cont(inx), time_reduced_tvecspc(inx));
			value.STentropy_freq_contXtime_corr = Rvalue(1, 2);
		else
			value.STentropy_freq_contXtime_corr = 0;
		end
	end
	
	if std(pow_env(reduced_trange))
		Rvalue = corrcoef(pow_env(reduced_trange), time_reduced_tvecspc);
		value.agg_envelopeelopeelopeelopeXtime_corr = Rvalue(1, 2);
	else
		value.agg_envelopeelopeelopeelopeXtime_corr = 0;
	end
	
end
