function varargout = STFT_order_stats_measurement_exporter(calling_mode, varargin);

%%
%% 'STFT_order_stats_measurement_exporter'
%%
%% Measurement export description for the STFT_order_stats_measurement
%%
%% Kathryn A. Cortopassi, July 2005
%%
%% syntax:
%% -------
%%   column_headers = STFT_order_stats_measurement_exporter('export_headers', browser_log);
%%
%%   values = STFT_order_stats_measurement_exporter('export_values', browser_log, event_inx, meas_inx);
%%
%% input:
%% ------
%%  browser_log = XBAT log structure
%%  event_inx = index for specific event
%%  meas_inx = index for measurement for specific event
%%
%% output:
%% -------
%%  column_headers = column header label strings (as a cell array)
%%                     for the values to be exported
%%  values = measurement values for specific event in order specified
%%           by 'column_headers'
%%

switch (calling_mode)


  case ('export_headers')
    %% get column header labels and format control string
    %% for writing to comma-delimited output file

    browser_log = varargin{1};
    num_chan = browser_log.sound.channels;


    column_headers = [...
      {'P1 to Median (Time)'},...
      {'IPR (Time)'},...
      {'IPR Skew (Time)'},...
      {'Concentration (Time)'},...
      {'Spread (Time)'},...
      {'Spread Skew (Time)'},...
      {'P1 to Peak (Time)'},...
      {'Temporal Entropy'},...
      {'Max Temp Entropy'},...
      {'Median Freq'},...
      {'IPR (Freq)'},...
      {'IPR Skew (Freq)'},...
      {'Concentration (Freq)'},...
      {'Spread (Freq)'},...
      {'Spread Skew (Freq)'},...
      {'Peak Freq'},...
      {'Spectral Entropy'},...
      {'Max Spec Entropy'},...
      {'Median NB IPR'},...
      {'Median NB Conc'},...
      {'Median NB Spread'},...
      {'Median NB IPR Skew'},...
      {'Median NB Spread Skew'},...
      {'Median NB P1 to Median'},...
      {'Median NB P1 to Peak'},...
      {'Median NB Temp Entropy'},...
      {'Median ST IPR'},...
      {'Median ST Conc'},...
      {'Median ST Spread'},...
      {'Median ST IPR Skew'},...
      {'Median ST Spread Skew'},...
      {'Median ST Median Freq'},...
      {'Median ST Peak Freq'},...
      {'Median ST Spec Entropy'},...
      {'MFC Cum Abs Der'},...
      {'MFC Med Abs Der'},...
      {'MFC Cum Der'},...
      {'MFC Med Der'},...
      {'MFC Inflex Count'},...
      {'PFC Cum Abs Der'},...
      {'PFC Med Abs Der'},...
      {'PFC Cum Der'},...
      {'PFC Med Der'},...
      {'PFC Inflex Count'},...
      {'MFC Start Freq'},...
      {'MFC End Freq'},...
      {'MFC Min Freq'},...
      {'MFC Max Freq'},...
      {'MFC P1 to Min (Time)'},...
      {'MFC P1 to Max (Time)'},...
      {'PFC Start Freq'},...
      {'PFC End Freq'},...
      {'PFC Min Freq'},...
      {'PFC Max Freq'},...
      {'PFC P1 to Min (Time)'},...
      {'PFC P1 to Max (Time)'},...
      {'IPR (Time) Cont X Freq Corr'},...
      {'Conc (Time) Cont X Freq Corr'},...
      {'Spread (Time) Cont X Freq Corr'},...
      {'IPR Skew (Time) Cont X Freq Corr'},...
      {'Spread Skew (Time) Cont X Freq Corr'},...
      {'P1 to Med (Time) Cont X Freq Corr'},...
      {'P1 to Peak (Time) Cont X Freq Corr'},...
      {'Temp Entropy Cont X Freq Corr'},...
      {'IPR (Freq) Cont X Time Corr'},...
      {'Conc (Freq) Cont X Time Corr'},...
      {'Spread (Freq) Cont X Time Corr'},...
      {'IPR Skew (Freq) Cont X Time Corr'},...
      {'Spread Skew (Freq) Cont X Time Corr'},...
      {'Med Freq Cont X Time Corr'},...
      {'Peak Freq Cont X Time Corr'},...
      {'Spec Entropy Cont X Time Corr'},...
      {'Agg Pow Env X Time Corr'},...
      {'Agg Pow Spec X Freq Corr'},...
      {'P1 to Peak (Time) Spec'},...
      {'Peak Freq Spec'},...
      ];

    %% make a column vector
    column_headers = column_headers';


    varargout{1} = column_headers;



  case ('export_values')
    %% get the values corresponding to the column header labels defined above

    browser_log = varargin{1};
    event_inx = varargin{2};
    meas_inx = varargin{3};

    num_chan = browser_log.sound.channels;
    ref_chan = browser_log.event(event_inx).channel;

    value_struc = browser_log.event(event_inx).measurement(meas_inx).value;


    values = [];


    values = add_value(value_struc.mpr_time, values);
    values = add_value(value_struc.ipr_time, values);
    values = add_value(value_struc.ipr_skew_time, values);
    values = add_value(value_struc.conc_time, values);
    values = add_value(value_struc.spread_time, values);
    values = add_value(value_struc.spread_skew_time, values);
    values = add_value(value_struc.ppr_time, values);
    values = add_value(value_struc.entropy_time, values);
    values = add_value(value_struc.max_entropy_time, values);

    values = add_value(value_struc.med_freq, values);
    values = add_value(value_struc.ipr_freq, values);
    values = add_value(value_struc.ipr_skew_freq, values);
    values = add_value(value_struc.conc_freq, values);
    values = add_value(value_struc.spread_freq, values);
    values = add_value(value_struc.spread_skew_freq, values);
    values = add_value(value_struc.peak_freq, values);
    values = add_value(value_struc.entropy_freq, values);
    values = add_value(value_struc.max_entropy_freq, values);

    values = add_value(value_struc.median_NBipr_time, values);
    values = add_value(value_struc.median_NBconc_time, values);
    values = add_value(value_struc.median_NBspread_time, values);
    values = add_value(value_struc.median_NBipr_skew_time, values);
    values = add_value(value_struc.median_NBspread_skew_time, values);
    values = add_value(value_struc.median_NBmpr_time, values);
    values = add_value(value_struc.median_NBppr_time, values);
    values = add_value(value_struc.median_NBentropy_time, values);

    values = add_value(value_struc.median_STipr_freq, values);
    values = add_value(value_struc.median_STconc_freq, values);
    values = add_value(value_struc.median_STspread_freq, values);
    values = add_value(value_struc.median_STipr_skew_freq, values);
    values = add_value(value_struc.median_STspread_skew_freq, values);
    values = add_value(value_struc.median_STmed_freq, values);
    values = add_value(value_struc.median_STpeak_freq, values);
    values = add_value(value_struc.median_STentropy_freq, values);

    values = add_value(value_struc.STmed_freq_contour_cumabsder, values);
    values = add_value(value_struc.STmed_freq_contour_medabsder, values);
    values = add_value(value_struc.STmed_freq_contour_cumder, values);
    values = add_value(value_struc.STmed_freq_contour_medder, values);
    values = add_value(value_struc.STmed_freq_contour_inflex, values);

    values = add_value(value_struc.STpeak_freq_contour_cumabsder, values);
    values = add_value(value_struc.STpeak_freq_contour_medabsder, values);
    values = add_value(value_struc.STpeak_freq_contour_cumder, values);
    values = add_value(value_struc.STpeak_freq_contour_medder, values);
    values = add_value(value_struc.STpeak_freq_contour_inflex, values);

    values = add_value(value_struc.STmed_freq_contour_start, values);
    values = add_value(value_struc.STmed_freq_contour_end, values);
    values = add_value(value_struc.STmed_freq_contour_min, values);
    values = add_value(value_struc.STmed_freq_contour_max, values);
    values = add_value(value_struc.STmed_freq_contour_minpr, values);
    values = add_value(value_struc.STmed_freq_contour_maxpr, values);

    values = add_value(value_struc.STpeak_freq_contour_start, values);
    values = add_value(value_struc.STpeak_freq_contour_end, values);
    values = add_value(value_struc.STpeak_freq_contour_min, values);
    values = add_value(value_struc.STpeak_freq_contour_max, values);
    values = add_value(value_struc.STpeak_freq_contour_minpr, values);
    values = add_value(value_struc.STpeak_freq_contour_maxpr, values);

    values = add_value(value_struc.NBipr_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBconc_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBspread_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBipr_skew_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBspread_skew_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBmpr_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBppr_time_contourXfreq_corr, values);
    values = add_value(value_struc.NBentropy_time_contourXfreq, values);

    values = add_value(value_struc.STipr_freq_contourXtime_corr, values);
    values = add_value(value_struc.STconc_freq_contourXtime_corr, values);
    values = add_value(value_struc.STspread_freq_contourXtime_corr, values);
    values = add_value(value_struc.STipr_skew_freq_contourXtime_corr, values);
    values = add_value(value_struc.STspread_skew_freq_contourXtime_corr, values);
    values = add_value(value_struc.STmed_freq_contourXtime_corr, values);
    values = add_value(value_struc.STpeak_freq_contourXtime_corr, values);
    values = add_value(value_struc.STentropy_freq_contourXtime_corr, values);

    values = add_value(value_struc.agg_envXtime_corr, values);
    values = add_value(value_struc.agg_spcXfreq_corr, values);

    values = add_value(value_struc.ppr_time_spec, values);
    values = add_value(value_struc.peak_freq_spec, values);



    varargout{1} = values;


end %% switch


return;








%%~~~~~~~~~~~~~~~~~~~
%% 'add_value'
%% add a scalar value to the value array
%%~~~~~~~~~~~~~~~~~~~

function values_array = add_value(value, values_array);

if ~isempty(value)
  values_array =  [values_array; {num2str(value, '%.3f')}];
else
  values_array = [values_array; {' '}];
end

return;
