function [pinx_interp, N] = get_percentiles(x, p)

%% get_percentiles
%%
%% gets percentiles, p, of x
%% where x contains counts (or any probability
%% distribution), either a vector of counts,
%% or a matrix where each column is a vector of counts;
%% the interpolated index of the percentile
%% location is returned, either a vector of
%% indices if x is a vector, or a matrix of
%% indices if x is a matrix
%%
%% syntax:
%% -------
%%  [pinx_interp, N] = get_percentiles(x, p)
%%
%% input:
%% ------
%%  x = frequency distribution (counts)
%%  p = array of percentiles (fraction 0-1)
%%
%% output:
%% -------
%%  pinx_interp = interpolated indices of percentile values
%%  N = total count
%%
%% K.A. Cortopassi, Aug 2003
%%
%% Sept 2003, added return of
%% N, and extention to matrix
%% of counts
%%
%% April 2006, add interpolation
%%



%% turn divide by zero warning off temporarily
warning off MATLAB:divideByZero

%% make sure that the percentiles are
%% between 0-1 and ordered
if any(p > 1 | p < 0)
  error('Values in ''p'' must be between 0 and 1');
end
[r, c] = size(p);
if r == 1 || c == 1
  num_p = length(p);
else
  error('''p'' must be a vector');
end
[p, sort_inx] = sort(p);

[r, c] = size(x);
if r == 1
  x = x';
  lx = c;
  lN = 1;
else
  lx = r;
  lN = c;
end

%midpoint = round(lx/2);


if lN == 1

  x = cumsum(x);
  N = x(end);
  %% deal with possible zero input from denoising
  %% treat as a uniform distribution
  if N == 0
    x = cumsum(ones(lx, 1));
    N = lx;
  end

  x = x / N;

  j = 1;
  pinx(num_p) = 0;
  for i = 1:num_p
    %pinx(i) = 0;
    while pinx(i) == 0 && j <= lx
      if x(j) >= p(i)
        pinx(i) = j;
        j = j - 1;
      end
      j = j + 1;
    end
  end
  %% make pinx a column vector
  pinx = pinx';

else

  x = cumsum(x);
  N = x(end, :);
  %% deal with possible zero columns from denoising
  %% treat as a uniform distribution
  zero_inx = find(N == 0);
  for inx = zero_inx
    x(:, inx) = cumsum(ones(lx, 1));
    N(inx) = lx;
  end

  x = x ./ (N' * ones(1, lx))';

  pinx(num_p, lN) = 0;
  for k = 1:lN
    j = 1;
    for i = 1:num_p
      %pinx(i,k) = 0;
      while pinx(i,k) == 0 && j <= lx
        if x(j,k) >= p(i)
          pinx(i,k) = j;
          j = j - 1;
        end
        j = j + 1;
      end
    end
  end

end



%% now, add some simple linear interpolation

%% get initial bounding indices
pinx1 = pinx - 1;
%% check for zero values of pinx1
zero_inx = find(pinx1 == 0);


if lN ~= 1
  %% get advanced indices into x for intital and terminal
  %% bounding indices
  pinx1 = pinx1 + ones(num_p, 1) * lx*(0:lN-1);
  pinx2 = pinx + ones(num_p, 1) * lx*(0:lN-1);
else
  %% copy terminal bounding indices
  pinx2 = pinx;
end

%% replace neg/zero inx temporarily
pinx1(zero_inx) = 1;

%% get the cum values at the bounding indices
x2 = x(pinx2);
x1 = x(pinx1);
%% deal with possible zeros indices by setting cumsum to 0
x1(zero_inx) = 0;

%% expand desired percentiles, make p a column vector
if size(p, 1) == 1
  p = p';
end
p = p * ones(1, lN);

%% find the fractional index using
%% linear interpolation
pinx_interp = pinx + ((p - x2) ./ (x2 - x1));



%% return the values in the original order of p
if lN > 1
  pinx_interp(sort_inx, :) = pinx_interp;
else
  pinx_interp(sort_inx) = pinx_interp;
end


%% turn divide by zero warning on
warning on MATLAB:divideByZero

return;