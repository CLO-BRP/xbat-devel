function varargout = slider_synch(cb_slider, slider_type);

%%
%% 'slider_synch'
%%
%% - function to associate and properly control slider parts
%% (slider control and text edit box) based on the HKF function
%% 'slider_sync' found in his March 2004 version of the template
%% detector
%%
%%
%%
%% syntax:
%% -------
%%   slider_synch(cb_slider, slider_type);
%%
%% input:
%% ------
%%   cb_slider == handle to callback slider
%%   slider_type == numeric class of slider (for now, integer or float)
%%
%% output:
%% -------
%%
%%
%%
%% Kathryn A. Cortopassi, March 2004
%%




%% get info about the slider object
%% triggering the callback
cb_slider_name = get(cb_slider, 'tag');
cb_slider_style = get(cb_slider, 'style');
cb_slider_parent = get(cb_slider, 'parent');
cb_slider_objs = findobj(cb_slider_parent, 'tag', cb_slider_name);


if ~exist('slider_type')
  slider_type = [];
end



%%~~
%% synchronize all parts of the callback slider (slider control and text edit box)
%%~~


%% width for number display
precision = 6;


%% synchronize slider and edit box
%% use values based on data type given

switch (cb_slider_style)


  case ('slider')
    %% slider moved, get its value
    %% and use it to set edit box

    edit_obj = findobj(cb_slider_objs, 'style', 'edit');

    value = get(cb_slider, 'value');

    if strcmpi(slider_type, 'integer')
      %% make integer value
      value = round(value);

    elseif strcmpi(slider_type, 'even')
      %% make an even integer value
      value = round(value);
      value = value + mod(value, 2);

    elseif strcmpi(slider_type, 'binary')
      %% make binary value
      value =  2^round(log2(value));
      %% this turns out to be a goofy option

      %% else, leave alone

    end

    %% update slider
    set(cb_slider, 'value', value);

    %% update edit box
    set(edit_obj, 'string', num2str(value, precision));



  case ('edit')
    %% edit box changed, get its value
    %% and use it to set slider

    slider_obj = findobj(cb_slider_objs, 'style', 'slider');

    value = str2num(get(cb_slider, 'string'));

    if isempty(value)
      %% improper value entered in text field! don't update
      %% reset to value of slider object

      %% get value of slider
      value = get(slider_obj, 'value');

    else
      %% value is valid, update

      if strcmpi(slider_type, 'integer')
        %% make integer value
        value = round(value);

      elseif strcmpi(slider_type, 'even')
        %% make an even integer value
        value = round(value);
        value = value + mod(value, 2);

      elseif strcmpi(slider_type, 'binary')
        %% make binary value
        value =  2^round(log2(value));
        %% this turns out to be a goofy option

        %% else, leave alone
      end

      %% check that value is within slider min/max range
      minval = get(slider_obj, 'min');
      maxval = get(slider_obj, 'max');

      if value > maxval
        %% change value to equal max
        value = maxval;

      elseif value < minval
        %% change value to equal min
        value = minval;

      end

    end

    %% update edit box
    set(cb_slider, 'string', num2str(value, precision));

    %% update slider
    set(slider_obj, 'value', value);

end


return;