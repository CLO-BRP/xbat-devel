function Spec = dBThresh(Spec, dBthresh, data_form, adjustment);

%**
%** ".m" 
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Syntax: Spec = dBThresh(Spec, dBthresh, data_form, adjustment);
%**
%** A function to zero the data a given number of dB below the peak value;
%** various adjustments can be made to the masked (unthresholded) values 
%**
%** 'Spec' == an MxN spectrogram array (or any array of values)
%** 'dBthresh' == some positive number of dB
%** 'data_form' == a flag telling how the values are stored,
%**                Amplitude, Power, or dB
%** 'adjustment' == a flag telling how to deal with masked values after thresholding,
%**                 None, Bias, Demean, or Binarize
%**
%** NOTE: when you want to accomplish true additive noise correction,
%**       data_form should be power, and adjustment should be bias
%**

%** 
%** by Kathryn A. Cortopassi
%** created 2-Oct-2001 
%** modified 
%** 22-Oct-2001
%** 23-May-2002
%**
%** modifications March 2005
%** 



%% check for the correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));


%% make sure that thresh is a positive number of dB
dBthresh = abs(dBthresh);


switch (data_form)
  
  case {'Amp'; 'Amplitude'}
    %% get absolute threshold value in amplitude
    %% (the value dBthresh below peak)
    divisor = 10^(dBthresh/20);
    abs_thresh = max(Spec(:)) / divisor;
    
    %   %% OKAY, now, which way should I do this?
    %   
    %   %% 1) this way? 
    %   %% so that the amplitude values are scaled and the dynamic 
    %   %% range in amplitude matches the dB range given
    %   Spec = Spec / abs_thresh;
    %   Spec(Spec < 1) = 0;
    %   %% -or- 
    %   Spec
    %   Spec = Spec - 1;
    %   Spec(Spec < 0) = 0;
    %   
    %   %% 2) or this way? 
    %   %% so that the absolute amplitude values are roughly preserved
    %   Spec = Spec - abs_thresh;
    %   Spec(Spec < 0) = 0;
    
    
  case {'Pow'; 'Power'}
    %% get absolute threshold value in power
    %% (the value dBthresh below peak)
    divisor = 10^(dBthresh/10);
    abs_thresh = max(Spec(:)) / divisor;
    
    %   %% OKAY, now, which way should I do this?
    %   
    %   %% 1) this way? 
    %   %% so that the amplitude values are scaled and the dynamic 
    %   %% range in amplitude matches the dB range given
    %   Spec = Spec / abs_thresh;
    %   Spec(Spec < 1) = 0;
    %   %% -or- 
    %   Spec
    %   Spec = Spec - 1;
    %   Spec(Spec < 0) = 0;
    %   
    %   %% 2) or this way? 
    %   %% so that the absolute amplitude values are roughly preserved
    %   Spec = Spec - abs_thresh;
    %   Spec(Spec < 0) = 0;
    
    
  case {'dB';  'dBpow'}
    %% get absolute threshold value in dB
    %% (the value dBthresh below peak)
    abs_thresh = max(Spec(:)) - dBthresh;
    
    
  otherwise
    error(sprintf('Data format flag ''%s'' not recognized!', data_form));
    
    
end


%% find indices of values equal to or below absolute threshold value
noise_inx = (Spec - abs_thresh) <= 0;
mask_inx = ~noise_inx;

%% zero out the spectrogram values below abs_threshold (considered noise)
Spec(noise_inx) = 0;

%% adjust the masked (unthresholded) spectrogram values
switch (adjustment)
  
  case {'None'}
    %% do nothing 
    
  case {'Bias'}
    Spec(mask_inx) = Spec(mask_inx) - abs_thresh; 
    
  case {'Demean'}
    Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
    
  case {'Binarize'}
    Spec(mask_inx) = 1; 
    
  otherwise
    error(sprintf('Mask adjustment flag ''%s'' not recognized!', data_form));
    
end


%% end function
return;