function [value, context] = compute(event, parameter, context)

% actual computation for the STFT_order_stats measurement
% K.A. Cortopassi, March 2008

%--
% setup
%--

if context.debug
	fprintf('event id == %d\n', event.id);
	
	fprintf('plot flag == %d\n', parameter.plot_spc);
end

% NOTE: we get handle of calling palette and use it to store spec plotting flag so it can be changed mid-run

udat = get(context.pal, 'userdata');

if udat.rtdata.plot_spc
	parameter.plot_spc = 1;
else
	parameter.plot_spc = 0; 
end

if context.debug
	fprintf('plot flag == %d\n', parameter.plot_spc);
end

if (parameter.filter_wav) && ~strcmpi(parameter.filter_ext, 'none')
	
	% NOTE: we get the active signal filter from browser to pass into compute function
	
	parameter.filter_ext = get_active_extension('signal_filter', context.par);
end

%--
% compute the measurements
%--

[value, parameter] = calculate_STFT_order_stats(context.sound, event, parameter);

if ~isfield(value, 'agg_envelopeelopeelopeelopeXtime_corr')
	
	value.agg_envelopeelopeelopeelopeXtime_corr = 0;
end

if context.debug
	fprintf('plot flag == %d\n', parameter.plot_spc);
end

if parameter.plot_spc
	udat.rtdata.plot_spc = 1;
else
	udat.rtdata.plot_spc = 0; 
end

set(context.pal, 'userdata', udat);



