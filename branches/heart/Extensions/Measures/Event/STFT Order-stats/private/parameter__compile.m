function [parameter, context] = parameter__compile(parameter, context)

%% chance to alter parameter and context information for the
%% STFT_order_stats measurement prior to computation loop
%% K.A. Cortopassi, March 2008

%% start clean
clc;


%% get handle of calling palette
%% use it to store spec plotting flag so it can be changed mid-run
udat = get(context.pal, 'userdata');
if (parameter.plot_spc), udat.rtdata.plot_spc = 1;
else udat.rtdata.plot_spc = 0; end
set(context.pal, 'userdata', udat);



%% update some parameter fields for storage
if ~(parameter.std_frange)
  parameter.flo = ''; parameter.fhi = ''; end


if (parameter.filter_wav)
  %% get active signal filter from browser and store
  ext = get_active_extension('signal_filter', context.par);
  if (isempty(ext)), parameter.filter_ext = 'none';
    %% for now, just save name, otherwise saving SQL DB log breaks
    %% when this is fixed, go back to saving entire extension
    %% (?? or some essential subset of the extention information ??)
    % else parameter.filter_ext = ext;
  else parameter.filter_ext = ext.name;
    fprintf('Filtering sound data using "%s"...\n', ext.name);
  end
else parameter.filter_ext = '';
end


if ~(parameter.denoise_spc)
  parameter.denoise_type = ''; parameter.denoise_perc = ''; end



return;