% quartile_indices - get quartile indices from normalized cumulative
% ------------------------------------------------------------------
%
% ix = quartile_indices(F)
%
% Input:
% ------
%  F - cumulative distribution
%
% Output:
% -------
%  ix - quartile indices

% NOTE: this is a MEX file
