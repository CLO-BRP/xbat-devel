function ext = replaygain_event_measure

ext = extension_create(mfilename);

ext.short_description = 'Based on the Replay-gain algorithm';

% ext.category = {};

ext.version = '0.1';

ext.guid = 'e2971a53-55b4-4fde-84de-dfe22ed024bb';

ext.author = 'Harold';

ext.email = 'harold.figueroa@gmail.com';

ext.url = '';

