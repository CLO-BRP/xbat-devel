function [parameter, context] = parameter__compile(parameter, context)

% REPLAY-GAIN - parameter__compile

% NOTE: in the case of an adaptive window we cannot precompute the window

if parameter.adapt
	return;
end

parameter.window = ones(1, ceil(parameter.frame * context.sound.rate));