function handles = value__menu__create(par, value, context)

% REPLAY-GAIN - value__menu__create

value.info = keep_fields(value, {'id', 'created_at', 'modified_at'});

handles = flat_struct_menu(par, value);