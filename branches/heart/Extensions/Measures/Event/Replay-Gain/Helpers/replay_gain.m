function factor = replay_gain(samples, rate, frame, level)

% replay_gain - factor computation
% --------------------------------
%
% factor = replay_gain(samples, rate, frame, level)
%
% Input:
% ------
%  samples - for play
%  frame - for computation
%  level - to normalize to 
%
% Output:
% -------
%  factor - to use for normalization

% TODO: we should add equal-loudness filtering

%--
% handle input
%--

if nargin < 4
	level = 0.05;
end

if nargin < 3 || isempty(frame)
	frame = 50 / 1000;
end

if nargin < 2
	rate = 44100;
end

%--
% adapt the frame to half if we don't have many bins
%--

len = ceil(frame * rate);

if (numel(samples) / len) < 20
	len = ceil(0.5 * len);	
end

window = ones(1, len);

%--
% compute frame amplitudes and current level
%--

a = sort(fast_amplitude(samples, window, 0, 'rms'));

if isempty(a)
	current = nan;
else
	current = a(round(0.95 * numel(a)));
end

factor = level / current;

%--
% if output is not captured, play input samples and gain adjusted samples
%--

if ~nargout
	disp(' '); disp(['Playing input. (level = ', num2str(current), ')']);
	
	wavplay(samples, rate)
	
	disp(['Playing scaled. (gain factor = ', num2str(factor), ')']);
	
	if ~isnan(factor)
		wavplay(factor * samples, rate);
	end
end

