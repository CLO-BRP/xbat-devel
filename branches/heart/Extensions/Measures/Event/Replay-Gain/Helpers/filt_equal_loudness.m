function Y = filt_equal_loudness(X, rate)

% filt_equal_loudness - filter for equal loudness
% -----------------------------------------------
%
% Y = filt_equal_loudness(X, rate)
%
% Input:
% ------
%  X - input signal
%  rate - of signal (def: 44100)
%
% Output:
% -------
%  Y - filtered signal
%
% See also: design_equal_loudness

%--
% handle input
%--

% NOTE:  the user hasn't specified a sampling frequency, use the CD default

if nargin < 2
	rate = 44100;
end

%--
% get filter and filter
%--

[b1, a1, b2, a2] = design_equal_loudness(rate);

Y = filter(filter(b1, a1, X), b2, a2);
