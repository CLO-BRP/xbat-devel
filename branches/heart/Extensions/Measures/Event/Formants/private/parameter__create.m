function parameter = parameter__create(context)

% TEST - parameter__create

parameter = struct;

%--
% frame
%--

% NOTE: this is the frame length in seconds
parameter.frame = 0.02;

% NOTE: this is the frame overlap as a fraction of the frame
parameter.overlap = 0.5;

%--
% LPC
%--

parameter.model_order = 19;

parameter.threshold = 0.05;

parameter.keep = 3;