function [value, context] = compute(event, parameter, context)

% TEST - compute

value = struct;

%--
% frame samples
%--

frame = round(parameter.frame * context.sound.rate); overlap = floor(parameter.overlap * frame);

F = buffer(event.samples(:), frame, overlap);

%--
% compute LPC for each frame
%--

for k = 1:size(F, 2)

	a = lpc(F(:, k), parameter.model_order); p = roots(a);

	p = p(imag(p) > parameter.threshold);

	if parameter.keep == 1
		f(k) = min(angle(p) * context.sound.rate/(2*pi));
	else
		temp = sort(angle(p) * context.sound.rate/(2*pi)); f(:, k) = temp(1:parameter.keep);
	end
	
end

value.time = 0.5 * (parameter.frame * (0:size(F, 2) - 1) + 0.5 * parameter.frame);

value.freq = f;

