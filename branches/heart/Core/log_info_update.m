function [time, freq, channel] = log_info_update(varargin)

% log_info_update - update log extent information fields
% ------------------------------------------------------
%
% [time, freq, channel] = log_info_update(log)
%                       = log_info_update(log, event)
%
% Input:
% ------
%  event - log event array
%
% Output:
% -------
%  time - time span of log
%  freq - freq span of log
%  channel - channel span of log

%--
% handle input
%--

if (length(varargin) == 1)
	log = varargin{1}; mode = 'full';
else
	log = varargin{1}; event = varargin{2}; mode = 'update';
end

%--
% compute according to mode
%--

switch mode
	
	%--
	% full computation of time, frequency, and channel span
	%--
	
	% this computation is used when an event is deleted and the first time
	% that this information is requested and does not exist
	
	case 'full'
		
		%--
		% compute time span
		%--
		
		tmp = struct_field(log.event, 'time');
		
		time = [min(tmp(:, 1)), max(tmp(:, 2))];
		
		%--
		% compute freq span
		%--
		
		tmp = struct_field(log.event,'freq');
		
		freq = [min(tmp(:, 1)), max(tmp(:, 2))];
		
		%--
		% compute channel span
		%--
		
		tmp = struct_field(log.event, 'channel'); 
		tmp = unique(tmp);
		channel = tmp(:)';

	%--
	% update computation of time frequency, and channel span
	%--
	
	% this update approach works upon editing of an event and addition of
	% an event. in the case of event deletions, the full update must be
	% computed
	
	case 'update'
		
		%--
		% check for empty time and frequency
		%--
		
		if isempty(log.time)
			[time, freq, channel] = log_info_update(log); return;
		end
		
		%--
		% update time span
		%--
		
		tmp = [log.time; event.time];
		
		time = [min(tmp(:, 1)), max(tmp(:, 2))];
		
		%--
		% update frequency span
		%--
		
		tmp = [log.freq; event.freq];
		
		freq = [min(tmp(:, 1)), max(tmp(:, 2))];
		
		%--
		% update channel span
		%--
		
		tmp = [log.channel, event.channel];
		tmp = unique(tmp);
		channel = tmp(:)';
		
end
