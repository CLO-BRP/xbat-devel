function files = build_all_cuda(level, opt)

% build_all_cuda - MEX files
% --------------------------
% 
% files = build_all_cuda(level, opt)
%
% Input:
% ------
%  level - of CUDA to build for 
%  opt - for 'build_cuda_mex'
%
% Output:
% -------
%  files - built
%
% See also: build_cuda_mex

%--
% handle input
%--

if nargin < 2
	opt = build_cuda_mex; 
end 

if ~nargin || isempty(level)
	
	level = get_cuda_capability; 
end

% NOTE: this is ugly, however it is correct for now, the 'compute' flag is used by 'nvcc'

if ~ischar(level)
	
	opt.arch = ['compute_', int2str(10 * level)];
else 
	if string_begins(level, 'compute_')
		opt.arch = level;
	else 
		opt.arch = ['compute_', strrep(level, '.', '')];
	end
end

%--
% setup
%--

if isempty(cc_root)
	error('Compiler is not available.');
end

if isempty(cuda_root);
	error('CUDA SDK is not available.');
end 

% NOTE: here we check availability of tools perhaps: compiler, drivers, sdk ... remember some of the tools we have 'cuda_root'

%--
% build
%--

% NOTE: this is a build script, which means some 'hard-coded' stuff is fair game

files = {};

disp 'SPECTROGRAM';

try
	fcd fast_specgram; cd MEX; cd private; 
	
	current = build_cuda; files = [files, current];
catch
	% NOTE: this expects that we've run some tests and that these throw errors
	
	nice_catch(lasterror, 'Problems building correct spectrogram code.'); 
end

if opt.test
    passed = test_cuda_specgram;
    
    if ~passed
        disp('Tests of CUDA specgram FAILED!');
    end
end

disp 'AMPLITUDE';

try
    fcd fast_amplitude; cd MEX;
    
    build_cuda_mex('amplitude', opt);

catch
	nice_catch(lasterror, 'Problems building correct amplitude code.'); 
end

if opt.test
    passed = test_cuda_amplitude;
    
    if ~passed
        disp('Tests of CUDA amplitude FAILED!');
    end
end

disp 'IMAGE-FILTERS'

try
    
    fcd linear_filter; cd MEX;
    
    build_cuda_image_all(opt);

catch
	nice_catch(lasterror, 'Problems building correct image-filters code.'); 
end

if opt.test
    fcd linear_filter; cd Tests;
    
    passed = test_cuda_image_all;
    
    if ~passed
        disp('Tests of CUDA image filters FAILED!');
    end
end

disp 'cuSVM'

try
    fcd svm_train; cd MEX; cd cuSVM; cd private;
    
    build_cuda_mex;
catch
    nice_catch(lasterror, 'Problems building cuda SVM code.');
end
