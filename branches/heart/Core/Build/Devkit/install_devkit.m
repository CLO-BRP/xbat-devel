function install_devkit(platform)

% install_devkit - Installs the necessary development environment for
% building the external interfaces to XBAT
% -------------------------------------------------------------
%
% install_devkit(platform)
%
% INPUTS:
% -------
% platform - if other than current arch, default is 'mexext'
% 
% NOTE: the required location of the devkit file is
%               xbat_root/DEVKIT_mexext.zip
%
% See also: create_devkit

%-
% Handle inputs
%-
if ~nargin
    platform = mexext;
end

devkit_path = fullfile(app_root, ['/DEVKIT_', platform '.zip']);

if ~exist(devkit_path, 'file')
    error(['Devkit file ''' devkit_path ''' does not exist']);
end

%-
% Install environment
%-

% Find and uncompress the master devkit file

unzip(devkit_path, app_root);

% Unzip LIBS

libzip = fullfile(app_root, 'devkit_LIBS.zip');

unzip(libzip, libs_root);

addpath(genpath(libs_root));

% Unzip Tools

toolzip = fullfile(app_root, 'devkit_Tools.zip');

unzip(toolzip, fullfile(app_root, 'Tools'));

addpath(genpath(fullfile(app_root, 'Tools')));

% Clean up intermediate files

delete(libzip);

delete(toolzip);