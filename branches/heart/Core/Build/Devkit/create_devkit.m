function create_devkit(platform)

% create_devkit - pack MEX build environment for architecture
%
% create_devkit(platform)
%
% Input:
% ------
%  platform - MEX extension (def: mexext)
%
% See also: install_devkit

%--
% handle input and setup
%--

if ~nargin
	platform = mexext;
end

temp = tempdir; % a bit or paranoia

%--
% package
%--

% NOTE: we compress the external libs and includes directory, this is the previous location of the LIBS

cd(fullfile(app_root, 'Core/Build/LIBS', platform));

libzip = fullfile(temp, 'DEVKIT_LIBS.zip');

zip(libzip, '.');

% NOTE: we compress the needed tools, compiler and pexports

if isempty(mingw_root)
	error('Install MinGW and set ''mingw_root'' first.'); 
end 

toolzip = fullfile(temp, 'DEVKIT_Tools.zip');

zip(toolzip, {mingw_root, fullfile(app_root, 'Tools', 'pexports')});

% Zip together the pieces into one file

output = fullfile(temp, ['DEVKIT_', platform, '.zip']);

zip(output, {libzip, toolzip});

movefile(output, app_root, 'f');

% Clean up temp files

delete(toolzip);

delete(libzip);


