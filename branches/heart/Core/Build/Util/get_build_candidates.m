function file = get_build_candidates(root, type, filter)

% get_build_candidates - files that may be built
% ----------------------------------------------
%
% file = get_build_candidates(root, type, filter)
%
% Input:
% ------
%  root - directory to search 
%  type - of candidate, allowed types are .c, .cc, .cpp, and .template files
%  filter - pattern
%
% Output:
% -------
%  file - list of candidates

%--
% handle input
%--

if nargin < 3
	filter = '';
end

types = {'c', 'cc', 'cpp', 'template'};

if nargin < 2 || trivial(type)
	type = types;
end

if any(~string_is_member(type, types))
	error('Unrecognized candidate type.');
end

if ~nargin || isempty(root)
	root = pwd;
end

%--
% get candidates
%--

file = scan_dir_for(type, root);

if ~isempty(filter)
	file = file(string_contains(file, filter));
end

if ~nargout
	% TODO: this display can be improved by grouping the files by directory
	
	disp(' '); iterate(@disp, file); disp(' ');

	clear file;
end
