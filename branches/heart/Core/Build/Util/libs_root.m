function root = libs_root

% libs_root - directory, where 'build_mex' will find libs
% -------------------------------------------------------
%
% root = libs_root;
%
% Output:
% -------
%  root - for library files

% NOTE: remember that things named all caps are for the attention of developers

if ispc
    root = fullfile(devkit_root, 'LIBS', mexext);
else
    root = fullfile(app_root, 'Core', 'Build', 'LIBS', mexext);
end
