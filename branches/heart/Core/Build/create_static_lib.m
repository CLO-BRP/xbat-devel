function [def, static] = create_static_lib(dll, destination, clean)

% create_static_lib - from DLL so that we may statically link
% -----------------------------------------------------------
%
% [def, static] = create_static_lib(dll)
%
% Input:
% ------
%  dll - file or name
%  destination - for static library files (def: where libs go)
%  clean - regenerate (def: false)
%
% Output:
% -------
%  def - defined symbols
%  static - static library

%--
% handle input
%--

if nargin < 3
	clean = false;
end

if nargin < 2 || isempty(destination)
	
    destination = create_dir(fullfile(libs_root, 'lib'));
	
	if isempty(destination)
		error(['Failed to create destination directory ''', destination, '''.']);
	end
end

% NOTE: handle dll file or name input, we need the full filename below

if ~string_ends(dll, '.dll')
	dll = [dll, '.dll'];
end

dll = which(dll);

if isempty(dll);
	error(['Unable to find DLL file ''', dll, '''.']);
end

disp(' '); disp(dll); disp(destination); disp(' ');

[ignore, name, ignore] = fileparts(dll); %#ok<NASGU,ASGLU>

%----------------
% SETUP
%----------------

switch computer
	case {'PCWIN', 'PCWIN32'}, prefix = '';
		
	case 'PCWIN64', prefix = 'x86_64-w64-mingw32-';
end

%--
% get tools
%--

% NOTE: this tool extracts symbols from 'dll' files, it is part of 'mingw-utils'

pexports = get_tool('pexports.exe');

if isempty(pexports)	
	error('Unable to create static libs, missing ''pexports'' utility. Install ''mingw-utils''.');
end

% NOTE: this tool creates the actual static libraries using the extracted symbols

dlltool = get_tool([prefix, 'dlltool.exe']);

if isempty(dlltool)
    dlltool = get_tool('dlltool.exe');
end

if isempty(dlltool)
	error('Unable to create static libs, missing ''dlltool'' utility. MinGW install needs repair.');
end

%--
% output file names
%--

destination = create_dir(destination);

if isempty(destination)
	error('Unable to create destination directory for static library.');
end

def = fullfile(destination, [name, '.def']);

static = fullfile(destination, [name, '.a']);

%----------------
% BUILD STATIC
%----------------

%--
% extract symbols and create 'def' files
%--

if ~exist(def, 'file') || clean

	str = ['"', pexports.file, '" -v "', dll, '"'];
	
	[status, result] = system(str); disp(str);
	
	if status
		error(result);
	else
		disp(result);
	end
	
	symbols = file_readlines(result);
	
	% NOTE: we are not currently ignoring symbols, look for an example
	
	for j = length(symbols):-1:1
		parts = str_split(symbols{j}, ';');
		
		if isempty(parts{1})
			symbols(j) = [];
		else
			symbols{j} = parts{1};
		end
	end
	
	file_writelines(def, symbols);
	
	disp([def, ' (', int2str(numel(symbols) - 2), ')']); disp(' ');
end

%--
% build static lib
%--

str = ['"', dlltool.file, '" --def "', def, '" --output-lib "', static];

[status, result] = system(str); disp(str); 

if status
	error(result);
else
	disp(result);
end

disp(static); disp(' ');

%--
% supress output
%--

if ~nargout
	clear def;
end

