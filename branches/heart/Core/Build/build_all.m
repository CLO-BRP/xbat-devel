function build_all(location)

% build_all - find and compile all mex code in the XBAT project
% -------------------------------------------------------------
%
% build_all
%
% See also: build, build_mex, update_install

% NOTE: be paranoid about not being able to move files

clear functions;

%--
% scan the xbat folder by default
%--

if ~nargin
    location = fullfile(app_root, 'Core');
end

%--
% scan the file system and build for each MEX directory
%--

scan_dir(location, {@build_cb, pwd});

disp(' ');


%-----------------------------------
% BUILD_CB
%-----------------------------------

function result = build_cb(path, parent)

%--
% check we are in a MEX directory
%--

result = [];

if ~strcmp(path(end-2:end), 'MEX')
    return;
end

%--
% if it is a MEX directory, run build
%--

% NOTE: the build function returns the files built as output

try
    cd(path); files = build; update_install(files); cd(parent);
catch 
    cd(parent); nice_catch(lasterror, ['Build failed on ''', path, '''.']);
end
