function private_test

here = fileparts(mfilename('fullpath'));

root_a = fullfile(here, 'A', 'private');

root_b = fullfile(here, 'B', 'private');

cd(root_a); private_test; cd(here);

cd(root_b); private_test; cd(here);