function device = get_playrec_devices_by_type(type, varargin) 

%--
% check type and get selection field from type
%--

switch type
	
	case 'input'
		field = 'inputChans';
		
	case 'output'
		field = 'outputChans';
		
	otherwise
		error('Device type must be ''input'' or ''output''.');
		
end

%--
% select devices and keep only specified type
%--

device = get_playrec_devices(varargin{:}); device([device.(field)] == 0) = [];

