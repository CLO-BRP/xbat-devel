function device = get_play_device

id = playrec('getPlayDevice');

if id == -1
	device = []; return;
end

device = get_playrec_output_devices; device = device([device.deviceID] == id);

if ~nargout
	disp(device); clear device;
end