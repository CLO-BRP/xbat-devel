function device = get_playrec_output_devices(varargin)

% get_playrec_output_devices - what it says
% -----------------------------------------
%
% device = get_playrec_output_devices(varargin)
%
% Input:
% ------
%  same as 'get_playrec_devices'
%
% Output:
% -------
%  device - output devices

device = get_playrec_devices_by_type('output', varargin{:}); 

if ~nargout
	disp(device); clear device;
end