function [tree, event] = test_on_page(par)

%--
% get active browser
%--

if ~nargin
	par = gab;
end 

if isempty(par)
	return;
end

%--
% get current page events
%--

log = get_browser_logs(par); page = get_browser_page; sound = sound_name(get_browser(par, 'sound'))

event = get_page_events(log, page);

if iscell(event)
	event = [event{:}];
end

%--
% create event vectors
%--

duration = 1000 * struct_field(event, 'duration'); freq = struct_field(event, 'freq') / 1000;

% f1 = freq; 
% tree1 = metric_tree(f1);
% plot_metric_tree(tree1); xlabel('Min Freq'); ylabel('Max Freq');

f2 = [freq(:, 1), [event.bandwidth]' / 1000];

tree2 = metric_tree(f2); 

plot_metric_tree(tree2); xlabel('Min Freq'); ylabel('Bandwidth'); title(sound);

f3 = [duration, [event.bandwidth]' / 1000];

tree3 = metric_tree(f3); 

plot_metric_tree(tree3); xlabel('Duration'); ylabel('Bandwidth'); title(sound);

