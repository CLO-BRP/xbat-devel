function tree = test_metric_tree(points, opt)

if nargin < 2
	opt = metric_tree;
end

%--
% create test points to process if needed
%--

% TODO: add various interesting random generation options, then factor

if ~nargin || isempty(points)
	points = randn(500, 2);
else
	if numel(points) == 1
		points = randn(points, 2);
	end
end

%--
% build tree
%--

tree = metric_tree(points, opt);

if nargout
	return;
end

%--
% visualize
%--

plot_metric_tree(tree);

