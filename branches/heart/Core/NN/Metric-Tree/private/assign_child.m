function value = assign_child(tree, id, points)

% NOTE: the goal of this function is to select the next child to search

%--
% get node and left child pivot
%--

% NOTE: the left child pivot is used in the decision function computation

node = get_node(tree, id);

child1 = get_node(tree, node.child.left); p1 = child1.pivot;

%--
% compute child assignment
%--

points = points - repmat(p1, size(points, 1), 1);

[vector, separation] = normalize(p2 - p1);

% NOTE: this creates a binary assignment vector

ix1 = points * decision.vector(:) <= decision.threshold;

% NOTE: this updates the true part to the left child and the complement to the right

value = node.child.left * ix1; value(~ix1) = node.child.right;