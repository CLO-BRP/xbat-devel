function points = get_points(ix, data)

% NOTE: points are stored as rows of the data matrix

points = data(ix, :);