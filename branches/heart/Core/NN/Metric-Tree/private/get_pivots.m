function [ix1, ix2] = get_child_pivots(ix, data)

% NOTE: this is low-cost heuristic to compute reasonable pivots, the cost is 2*N versus N^2

ix0 = get_random_point(ix);

ix1 = get_furthest_point(ix0, ix, data);

ix2 = get_furthest_point(ix1, ix, data);