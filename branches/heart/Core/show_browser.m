function show_browser(par, str)

% show_browser - move to another browser
% --------------------------------------
%
% show_browser(par, str)
%
% Input:
% ------
%  par - browser figure handle
%  str - command 'first', 'previous', 'next', 'last'

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

%--
% set default command
%--

if (nargin < 2) || isempty(str)
	str = 'next';
end

%--
% set default browser
%--

if nargin < 1
	par = get_active_browser;
end

if isempty(par)
	return;
end

%-----------------------------------
% SHOW BROWSER
%-----------------------------------

%--
% get sound browser figure handles
%--

other = get_xbat_figs('type', 'sound');

% NOTE: return if there are no other browsers

if length(other) < 2
	return; 
end
			
%--
% sort handles by tag to determine order
%--

% NOTE: this could be confusing in the case of sounds from multiple libraries

tags = get(other, 'tag');

[tags, ix] = sort(tags);  other = other(ix);

%--
% find position of current browser among open browsers
%--

if ~isempty(par)
	
	tag = get(par, 'tag');

	ix = find(strcmp(tag, tags));

	% NOTE: return if we are not a browser

	if isempty(ix)
		return;
	end 
	
	if length(ix) > 1
		ix = ix(1); 
	end
	
end

%--
% select browser based on command string
%--

switch str
	
	case 'first', figure(other(1));
		
	case 'previous'

		if ix > 1
			figure(other(ix - 1));
		else
			figure(other(end));
		end

	case 'next'

		if ix < length(other)
			figure(other(ix + 1));
		else
			figure(other(1));
		end
		
	case 'last', figure(other(end));

end
