function html = build_nav(model, k, type, fun)

% BUILD_NAV build top of page navigation
%
% html = build_nav(model, k, type, fun)

%--
% set default label fun
%--

% TODO: this should allow for cell callbacks

if nargin < 4
	fun = @prepare_label;
end

if nargin < 3
	type = 'menu';
end

%--
% get page and family
%--

page = model.pages(k);

family = get_family(model, page);

%--
% build navigation code
%--

switch type

	case 'menu'

		%--
		% create multiple level menu navigation
		%--

		html = ''; member = [];

		for level = 0:min(page.level + 1, max([model.pages.level]))

			%--
			% get all visible pages from this level and ancestor
			%--

			if isempty(member)
				pages = model.pages([model.pages.level] == level);
			else
				ix = intersect(member.child, find([model.pages.level] == level)); pages = model.pages(ix);
			end

			if isempty(pages)
				continue;
			end

			% TODO: when a page is missing from the 'order.txt' list, we have a problem here, we should detect this earlier
			
			% NOTE: the problem is in 'model_site' and its been noted as a TODO there
			
% 			db_disp pages; pages, {pages.name}, {pages.position}
			
			pages([pages.position]) = pages;

			member = family([family.level] == level);

			%--
			% build level navigation list
			%--

            topnav = '';
            
            if (level < 2) 
                topnav = ' topnav';
            end
            
			html = [html, '<div id="nav-', int2str(level), '" class="screen', topnav ,'">\n<ul>\n'];

			prefix = [pages_root(model.site), filesep];

			for k = 1:length(pages)

				href = strrep(pages(k).file, prefix, '');

				href = [level_prefix(page.level), strrep(href, filesep, '/')];

				if isempty(member)
					html = [html, '\t<li><a href="', href, '">', fun(pages(k).name), '</a></li>\n'];
				else
					if ~strcmp(pages(k).name, member.name)
						html = [html, '\t<li><a href="', href,'">', fun(pages(k).name), '</a></li>\n'];
					else
						html = [html, '\t<li class="here"><a href="', href, '">', fun(pages(k).name), '</a></li>\n'];
					end
				end
			end

			html = [html, '</ul>\n</div>\n'];

% 			if level > 1
% 				html = [html, '<div class="clear"></div>\n'];
% 			end
			
		end
		

	case 'bread-crumbs'

		%--
		% build bread-crumbs
		%--

		html = '';

		if length(family) > 1
			
			html = [html, '<div id="bread-crumbs" class="screen"><ul>\n'];

			% NOTE: consider making the bread-crumbs a list as well

			html = [html, '<li>', page_link(model.site, page, family(end)), '</li>'];

			for k = length(family) - 1:-1:1
				html = [html, '<li>', page_link(model.site, page, family(k)), '</li>'];
			end

			html = [html, '</ul></div>\n'];
		end
end

html = sprintf(html);


%--------------------------------
% GET_FAMILY
%--------------------------------

function family = get_family(model, page)

%--
% add self to family
%--

family = page;

%--
% move up the ancestor tree
%--

while page.parent
	ancestor = model.pages(page.parent); family(end + 1) = ancestor; page = ancestor;
end


%--------------------------------
% GET_CHILDREN
%--------------------------------

function pages = get_children(model, page)

%--
% return if there are no children
%--

if isempty(page.child)
	pages = []; return;
end

%--
% get children, and possibly order them
%--

pages = model.pages(page.child);

% NOTE: we get the children in position order

pos = [pages.position];

if ~isempty(pos)
	pages(pos) = pages;
end




