function tok = inject(tok, data)

% INJECT inject data into tokens
%
% tok = inject(tok, data)

if ischar(tok)
	tok = {tok}; extract = 1;
else
	extract = 0;
end 

for k = 1:length(tok)
	
	if strcmp(tok{k}(1), '$')
		tok{k} = to_str(get_field(data, tok{k}(2:end), []));
	end
	
end

if extract
	tok = tok{1};
end


%-------------------
% TO_STR
%-------------------

function str = to_str(in)

if isnumeric(in)
	
	if round(in) == in
		str = int2str(in);
	else
		str = num2str(in);
	end
	
else
	
	if ischar(in)
		str = in;
	else
		error('Unable to simply convert value to string.');
	end
	
end
