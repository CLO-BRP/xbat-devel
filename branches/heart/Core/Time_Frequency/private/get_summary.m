function opt = get_summary(parameter)

% get_summary - get mex codes for summary options
% -----------------------------------------------
%
% opt = get_summary(parameter)
%
% Input:
% ------
%  parameter - spectrogram parameters
%
% Output:
% -------
%  opt - summary mex codes
% 
% See also: fast_specgram

% HACK: we should not need this, however we make sure that values coming from a control are not celled

parameter = struct_iterate(parameter, @(value)(cellfree(value)));

%---------------------
% SUMMARY TYPE
%---------------------

sum_type = lower(parameter.sum_type);

switch sum_type

	case {'mean', 'average'}
		opt.type = 0;

	case 'max'
		opt.type = 1;

	case 'min'
		opt.type = 2;

	case 'decimate'
		opt.type = 3;

	otherwise
		error(['Unknown summary type ''', sum_type, '''.']);
end

%---------------------
% SUMMARY LENGTH
%---------------------

opt.length = parameter.sum_length;

%---------------------
% SUMMARY QUALITY
%---------------------

% NOTE: the default quality value is coded in two places, here and create

if isfield(parameter, 'sum_quality')
	
	sum_quality = lower(parameter.sum_quality);
else
	sum_quality = 'medium';
end

switch sum_quality
	
	case 'low'
		opt.quality = 1;
		
	case 'medium'
		opt.quality = 2;
	
	case 'high'
		opt.quality = 3;
		
	case 'highest'
		opt.quality = 4;
		
	otherwise
		error(['Unknown summary quality ''', sum_quality, '''.']);
end