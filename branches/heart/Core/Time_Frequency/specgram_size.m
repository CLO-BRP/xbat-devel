function [m,n] = specgram_size(opt,r,t)

% specgram_size - compute size of spectrogram image
% -------------------------------------------------
%
% [m,n] = specgram_size(opt,r,t)
%
% Input:
% ------
%  opt - spectrogram parameters
%  r - sample rate
%  t - page duration
%
% Output:
% -------
%  m - number of rows
%  n - number of columns

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4407 $
% $Date: 2006-03-28 19:01:25 -0500 (Tue, 28 Mar 2006) $
%--------------------------------

%--
% compute number of rows and columns
%--

m = floor(opt.fft / 2) + 1;

n = (t * r) / (opt.hop * opt.fft);

n = floor(n) + 1;

if ~isempty(opt.sum_length)
	n = round(n / opt.sum_length);
end


