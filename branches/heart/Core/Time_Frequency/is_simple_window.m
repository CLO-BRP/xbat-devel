function [value, code] = is_simple_window(window)

% is_simple_window - check that a window is unimodal
% --------------------------------------------------
%
% [value, code] = is_simple_window(window)
%
% Input:
% ------
%  window - window sequence
%
% Output:
% -------
%  value - result indicator
%  code - run-length code computed as part of test

%--
% check window is unimodal
%--

up = window(1:end - 1) < window(2:end);

% NOTE: we compute a run-length code for the window change and check it starts up and it has two parts

code = rle_encode(up);

% NOTE: the total length of the two part code will be five [row, col, initial, run1, run2]

value = code(3) && (numel(code) == 5);
