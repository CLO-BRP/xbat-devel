//------------------------------------
// Spectragram on overlapping windows.
//------------------------------------

#include <cuda.h> 
#include <cufft.h>
#include "cuda_specgram.h"

// NOTE: max threads is hardcoded to 256 for current generation of hardware
#define MAX_THREADS 256

#ifdef USE_CONSTANT_MEMORY

#ifndef FILTER_CONSTANT
#define FILTER_CONSTANT

// Constant memory for filter values
#if __CUDA_ARCH__ >= 130
__device__ __constant__ __align__(8) double c_func[4096];
#else
__device__ __constant__ __align__(8) float c_func[4096];
#endif

#endif /* FILTER_CONSTANT */

#endif /* FOO */

//--------------------------------------------------------------
// Copy data into windows for transform in place (single thread)
//--------------------------------------------------------------

__global__ void windowKernel_serial(CUFFT_TYPE *d_fft, C_TYPE *d_data, unsigned int numTransforms, unsigned int transformSize, unsigned int w, unsigned int o)
{
	unsigned int i, j;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < transformSize; j++)
		{
			d_fft[i*transformSize + j].x = d_data[i*(w-o) + j];
			d_fft[i*transformSize + j].y = 0.0;
		}
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//---------------------------------------------------------------

__global__ void windowKernel(CUFFT_TYPE *d_fft, C_TYPE *d_data, unsigned int N, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
	}
}

//---------------------------------------------------------------
// Copy data into windows for transform in place (multi threaded)
//   and apply window function
//---------------------------------------------------------------

__global__ void windowKernelFunc(CUFFT_TYPE *d_fft, C_TYPE *d_data, C_TYPE *d_func, unsigned int N, unsigned int w, unsigned int o, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
	unsigned int didx = blockIdx.x * (w-o) + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
    int bound = (transformSize - inc + MAX_THREADS - 1) / MAX_THREADS;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_fft[fidx].x = d_data[didx] * d_func[inc];
		d_fft[fidx].y = 0.0;
		fidx += MAX_THREADS;
		didx += MAX_THREADS;
		inc += MAX_THREADS;
	}
}

//----------------------------------------------------
// Compute magnitude of complex values (single thread)
//----------------------------------------------------

__global__ void magnitudeKernel_serial(C_TYPE *d_mag, CUFFT_TYPE *d_fft, unsigned int numTransforms, unsigned int transformSize)
{
	unsigned int i, j;
	unsigned int hop = (transformSize >> 1) + 1;
	
	for (i = 0; i < numTransforms; i++)
	{
		for (j = 0; j < hop; j++)
		{
			d_mag[i * hop + j] = sqrt(d_fft[i * transformSize + j].x * d_fft[i * transformSize + j].x +
					                  d_fft[i * transformSize + j].y * d_fft[i * transformSize + j].y) / transformSize;
		}
	}
}

//-----------------------------------------------------
// Compute magnitude of complex values (multi threaded)
//-----------------------------------------------------

__global__ void magnitudeKernel(C_TYPE *d_Mag, CUFFT_TYPE *d_FFT, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
    unsigned int resultSize = (transformSize >> 1) + 1;
	unsigned int midx = blockIdx.x * resultSize + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = (resultSize - inc + blockDim.x - 1) / blockDim.x;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Mag[midx] = sqrt(d_FFT[fidx].x * d_FFT[fidx].x + d_FFT[fidx].y * d_FFT[fidx].y) / (C_TYPE) transformSize;
		midx += blockDim.x;
		fidx += blockDim.x;
	}
}

//-----------------------------------------------------
// Compute power of complex values (multi threaded)
//-----------------------------------------------------

__global__ void powerKernel(C_TYPE *d_Pow, CUFFT_TYPE *d_FFT, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
    unsigned int resultSize = (transformSize >> 1) + 1;
	unsigned int pidx = blockIdx.x * resultSize + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = (resultSize - inc + blockDim.x - 1) / blockDim.x;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Pow[pidx] = (d_FFT[fidx].x * d_FFT[fidx].x + d_FFT[fidx].y * d_FFT[fidx].y) / (C_TYPE) transformSize / (C_TYPE) transformSize;
		pidx += blockDim.x;
		fidx += blockDim.x;
	}
}

//-----------------------------------------------------
// Change interleave of complex values (multi threaded)
//-----------------------------------------------------

__global__ void complexKernel(C_TYPE *d_Rea, C_TYPE *d_Img, CUFFT_TYPE *d_FFT, unsigned int transformSize)
{
	unsigned int inc = threadIdx.x;
    unsigned int resultSize = (transformSize >> 1) + 1;
	unsigned int idx = blockIdx.x * resultSize + inc;
	unsigned int fidx = blockIdx.x * transformSize + inc;
	int i;
	int bound = (resultSize - inc + blockDim.x - 1) / blockDim.x;
	
	#pragma unroll 8
	for (i = 0; i < bound; i++)
	{
		d_Rea[idx] = d_FFT[fidx].x;
		d_Img[idx] = d_FFT[fidx].y;
		idx += blockDim.x;
		fidx += blockDim.x;
	}
}

//---------------------------------------------
// Compute FFT of windowed data
//---------------------------------------------

cudaError_t cuda_specgram(C_TYPE *d_real, C_TYPE *d_imag, C_TYPE *d_data, unsigned int N, C_TYPE *d_func, unsigned int w, unsigned int o, unsigned int transformSize, int comp_type, int maxGridSizeX)
{
    cufftHandle fp;
	CUFFT_TYPE *d_fft = NULL;
	unsigned int numTransforms, totalTransforms, transformCount;
    cudaError_t cudaErr = cudaSuccess;
    cufftResult cufftErr = CUFFT_SUCCESS;
	
    // Clear last error
    cudaGetLastError();

    // Number of windows on which we will do transforms
	totalTransforms = (N - w) / (w - o) + 1;

	// Initialize FFT library
    if (cudaErr == cudaSuccess && cufftErr == CUFFT_SUCCESS)
    {
        cufftErr = cufftPlan1d(&fp, transformSize, CUFFT_PLAN_TYPE, totalTransforms);
    }
    
	// Allocate temporary result buffer
    if (cudaErr == cudaSuccess && cufftErr == CUFFT_SUCCESS)
    {
        cudaErr = cudaMalloc((void **) &d_fft, totalTransforms * transformSize * sizeof(CUFFT_TYPE));
    }
    
	// Copy data into fft buffer (parallel)
    if (cufftErr == CUFFT_SUCCESS)
    {
        transformCount = 0;
        while (transformCount < totalTransforms && cudaErr == cudaSuccess)
        {
            numTransforms = min(maxGridSizeX, totalTransforms - transformCount);
            if (d_func == NULL)
            {
            	windowKernel<<<numTransforms, MAX_THREADS>>>(d_fft + transformCount *transformSize, d_data + transformCount * (w-o), N, w, o, transformSize);
            }
            else
            {
                windowKernelFunc<<<numTransforms, MAX_THREADS>>>(d_fft + transformCount * transformSize, d_data + transformCount * (w-o), d_func, N, w, o, transformSize);
            }
            cudaErr = cudaGetLastError();
            transformCount += numTransforms;
        }
    }

	// Copy data into fft buffer (serial)
	// windowKernel_serial<<<1, 1>>>(d_fft, d_data, numTransforms, transformSize, w, o);
    // cudaErr = cudaGetLastError();
    
	// Transform

    if (cudaErr == cudaSuccess && cufftErr == CUFFT_SUCCESS)
    {
        CUFFT_EXECUTE(fp, d_fft, d_fft, CUFFT_FORWARD);
        cudaErr = cudaGetLastError();
    }

	// Calculate result
    
    if (cufftErr == CUFFT_SUCCESS)
    {
        transformCount = 0;
        while (transformCount < totalTransforms && cudaErr == cudaSuccess)
        {
            numTransforms = min(maxGridSizeX, totalTransforms - transformCount);
            
            if (comp_type == 0)
            {
                // Compute magnitude of complex result (serial)
                // magnitudeKernel_serial<<<1, 1>>>(d_real, d_fft, numTransforms, transformSize);
                
                // Compute magnitude of complex result (parallel)
                magnitudeKernel<<<numTransforms, MAX_THREADS>>>(d_real + transformCount * (transformSize/2 + 1), d_fft + transformCount * transformSize, transformSize);
            }
            else if (comp_type == 1)
            {
                // Compute power of complex result (parallel)
                powerKernel<<<numTransforms, MAX_THREADS>>>(d_real + transformCount * (transformSize/2 + 1), d_fft + transformCount * transformSize, transformSize);
            }
            else // if (comp_type == 2)
            {
                // Transform interleave of complex result (parallel)
                complexKernel<<<numTransforms, MAX_THREADS>>>(d_real + transformCount * (transformSize/2 + 1), d_imag + transformCount * (transformSize/2 + 1), d_fft + transformCount * transformSize, transformSize);
            }
            cudaErr = cudaGetLastError();
            transformCount += numTransforms;
        }
    }
    
	// Free device memory
	if (d_fft != NULL) { cudaFree(d_fft); }
	
	// CUFFT cleanup
    cufftDestroy(fp);
    
    // Return status
    if (cudaErr == cudaSuccess && cufftErr != CUFFT_SUCCESS)
    {
        cudaErr = (cudaError_t) cufftErr;
    }
    return cudaErr;
}