function [R, lags, time] = fast_autocorr(X, rate, parameter)

% fast_autocorr - fast running autocorrelation (cyclic) based on 'fast_specgram'
% ------------------------------------------------------------------------------
%
% [R, lags, time] = fast_autocorr(X, rate, parameter)
%
% Input:
% ------
%  X - samples
%  rate - of samples in Hz
%  parameter - configures 'fast_specgram'
%
% Output:
% -------
%  R - running autocorrelation
%  lags - for each autocorrelation in seconds
%  time - grid for running computation

%--
% handle input
%--

if nargin < 3
	parameter = fast_specgram;
end

% NOTE: we are afraid of summarization, we do not understand it

parameter.sum_auto = 0;

% NOTE: we are afraid of auto hopping , daniel does not understand it

parameter.hop_auto = 0;

if ~nargin
	R = parameter; return;
end

if (nargin < 2) || isempty(rate)
	rate = 2;
end

%--
% compute cyclic autocorrelation using fft method
%--

% NOTE: compute power spectrum for each windowed section

[B, freq, time] = fast_specgram(X, rate, 'power', parameter);

% TODO: handle multiple-channel case, when we get cell output

if ~iscell(B)
        % NOTE: recover full spectrum using symmetry conditions
        F = [B; flipud(B(2:end-1,:))];

        % NOTE: compute inverse fft to obtain autocorrelation, we only keep the
        % non-redundant part
        R = real(ifft(F, size(F, 1), 1));

        R = R(1:size(B), :);
else
    for k=1:numel(B)
        % NOTE: recover full spectrum using symmetry conditions
        F = [B{k}; flipud(B{k}(2:end-1, :))];

        % NOTE: compute inverse fft to obtain autocorrelation, we only keep the
        % non-redundant part
        G = real(ifft(F, size(F, 1), 1));

        R{k} = G(1:size(B{1}), :);
    end
end

lags=[0:parameter.fft/2]./rate;


