function [B, freq, time, t] = matlab_specgram(X, rate, out, param)

%--
% extract parameters
%--

fn = param.fft;

overlap = round(fn * (1 - param.hop));

%--
% generate window
%--

[h, hn] = get_window(param);

% if (isempty(window_to_fun(param.win_type,'param')))
% 	h = feval(window_to_fun(param.win_type),hn);
% else
% 	h = feval(window_to_fun(param.win_type),hn,param.win_param);
% end

% NOTE: zero pad window to fft length if needed

if (hn < fn)
	h((hn + 1):fn) = 0; hn = fn;
end 

%--
% compute spectrogram and time
%--

fun = [];

switch (out)
    
    case 'norm'
        fun = @abs;
        
    case 'power'
        fun = @mag_sqr;
        
    case 'complex'
        fun = [];

end

tic; [B, freq, time] = spectrogram(X, h, overlap, fn, rate); t = toc;

if ~isempty(fun)
    B = fun(B);
end


%--
% mag-sqr - take the point-wise squared magnitude of a complex vector
%--

function x = mag_sqr(x)

x = x.*conj(x);

return;

