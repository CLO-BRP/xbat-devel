function add_log_event_count(store)

% add_log_event_count - migration
% -------------------------------
%
% add_log_event_count(store)
%
% Input:
% ------
%  store - database

% TODO: we could refactor further as for example 'establish_counter_cache' and 'refresh_counter_cache'

% NOTE: we inspect and possibly update the schema, then we update relation count

if ~is_table_column(store, 'log', 'events_count')
	add_column(store, 'log', 'events_count', 'sql', 'INTEGER NOT NULL');
end

update_relation_count(store, 'log', 'event');

% TODO: consider setting a trigger to manage the count column

