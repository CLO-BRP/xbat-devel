function preset = get_barn_preset(store, guid)

% get_barn_preset - by guid
% -------------------------
%
% preset = get_barn_preset(store, guid)
%
% Input:
% ------
%  store - barn
%  guid - for preset
%
% Output:
% -------
%  preset - from barn

%--
% get preset object
%--

preset = get_database_objects_by_column(store, 'extension_preset', 'guid', guid);
	
if isempty(preset)
	error(['Unable to retrieve extension preset. (', guid, ')']);
end

%--
% get preset extension parameters
%--

ext = get_database_objects(store, 'extension', preset.extension_id);

if isempty(ext)
	error('Preset extension reference failed to retrieve extension.');
end

table = extension_table_names(ext);

preset.preset = unflatten(get_database_objects(store, table.parameter, preset.parameter_id));

%--
% create local representation of preset
%--

% TODO: create local representation of preset

% TODO: consider how this behaves with respect to preset custom 'save' and 'load'


