function table = set_barn_master(store, type, table, sql)

% set_barn_master - entry for table
% ---------------------------------
%
% table = set_barn_master(store, type, table, sql)
%
% Input:
% ------
%  store - database
%  type - of table
%  table - name
%  sql - used to create table
%
% Output:
% -------
%  table - representation in master

%--
% pack input
%--

barn_master.type = type;

barn_master.name = table;

% NOTE: the trailing semicolon was causing problems in the interpretation of the statement

if sql(end) == ';', sql(end) = []; end

barn_master.create_sql = sql;

%--
% store in 'barn_master'
%--

% NOTE: we output the master table entry

if ~nargout
	set_database_objects(store, barn_master);
else
	table = set_database_objects(store, barn_master);
end