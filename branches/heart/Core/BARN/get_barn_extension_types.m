function types = get_barn_extension_types(filter)

% get_barn_extension_types - types that store data
% ------------------------------------------------
%
% types = get_barn_extension_types(filter)
%
% Input:
% ------
%  filter - string (def: '', no filtering, all types)
%
% Output:
% -------
%  types - of extension that store data
%
% NOTE: the types output here have corresponding data tables in the BARN schema

%--
% set no filter default
%--

if ~nargin
	filter = '';
end

%--
% filter list of extension types using BARN relevance and input filter
%--

types = get_extension_types; 

for k = numel(types):-1:1

	% NOTE: the types filtered here are not represented by tables in the BARN schema
	
	if string_contains(types{k}, {'widget', 'format', 'palette', 'action', 'filter', 'extension', 'source'})
		types(k) = []; continue;
	end

	if ~isempty(filter) && ~string_contains(types{k}, filter)
		types(k) = [];
	end
end