function [obj, description] = get_barn_prototype(table)

% get_barn_prototype - barn object description
% --------------------------------------------
%
% [obj, description] = get_barn_prototype(table)
%
% Input:
% ------
%  table - name
% 
% Output:
% -------
%  obj - prototype
%  description - of constraints

% TODO: consider packing the output into a 'model'

% NOTE: the prototype includes strcture fields and type hints

hint = column_type_hints; obj.id = hint.integer; obj.guid = hint.string;

description = empty(column_description);

switch table
	
	%-------------------------
	% DATA TABLES
	%-------------------------
	
	case 'user'

		% NOTE: this is the original simple user
		
% 		obj.name = hint.string; 
% 		
% 		obj.email = hint.string;
% 
% 		opt.constraints = {'UNIQUE (name, email)'};
		
		% NOTE: this is the server capable user 
		
		obj.login = hint.string;
		
		obj.name = hint.string; 
      
		obj.email = hint.string;
		
		% NOTE: this makes sure that the first timestamp is 'created_at', a MySQL thing
		
		obj.created_at = hint.timestamp; obj.modified_at = hint.timestamp;
		
		obj.crypted_password = hint.string;
		
      	obj.salt = hint.string;
		
		% NOTE: 'created_at' and 'modified_at' are appended to all tables
		
		% t.column :created_at, :datetime
		
% 		obj.updated_at = hint.timestamp;
		
		obj.remember_token = hint.string;
		
		obj.remember_token_expires_at = hint.timestamp;
		
		obj.activation_code = hint.string;
		
		obj.activated_at = hint.timestamp;
		
		% t.column :state, :string, :null => :no, :default => 'passive'
		
		% TODO: we currently do not have a way of describing contraints here
		
		
		obj.state = hint.string;
		
		description(end + 1) = struct('name', 'state', 'type', 'VARCHAR(255)', 'constraints', 'DEFAULT "passive"');
		
		obj.deleted_at = hint.timestamp;
		
	case 'role'
		
		obj.name = hint.string;
		
		obj = rmfield(obj, 'guid');
		
	case 'library'
		
		obj.name = hint.string;

		obj.user_id = hint.integer;
		
	case 'project'
		
		obj.name = hint.string;

		obj.description = hint.text;
		
		obj.long_description = hint.text;
		
		obj.visible = hint.integer;
		
		obj.public = hint.integer;
		
		obj.data_file_root = hint.string;
		
		obj.user_id = hint.integer;

	case 'project_asset'
		
		% NOTE: this does not comfortably fit here, or with the relation or 'tagging' like frameworks. why?
		
		obj = rmfield(obj, 'guid');
		
		obj.project_id = hint.integer;
		
		obj.asset_id = hint.integer;
		
		obj.asset_type = hint.string;
		
	case 'sound' 
		
		obj.name = hint.string;

		obj.description = hint.text;
		
		obj.long_description = hint.text;
		
		obj.user_id = hint.integer;
		
		obj.public = hint.logical;
		
		% NOTE: the below summary properties are computed from the 'recordings'
		
		% NOTE: these leave open the possibility for mixed formats and for dropping and adding files
		
		obj.channels = hint.integer;
		
		obj.samplerate = hint.real;
		
		% NOTE: these limit the adding and dropping of files
		
		obj.duration = hint.real;
		
		obj.samples = hint.real;
		
		obj.content_hash = hint.string;
		
		obj.events_count = hint.integer;
		
		description(end + 1) = struct('name', 'public', 'type', 'BOOLEAN', 'constraints', 'DEFAULT true');
		
	case 'recording'
		
		% NOTE: currently we have 'name' and 'file' with the same content, the first does not need an extension
		
		% NOTE: this 'info_create' is from the 'sound_file_info' world;
		
		obj = struct_merge(obj, sound_file_info('sample_audio_file.wav'));
		
		obj.bytes = hint.real;
		
		obj.samplerate = hint.real;
		
		obj.duration = hint.real;
		
		obj.samples = hint.real;
		
		obj.data_file_id = hint.integer;
		
		% NOTE: if the data file is available, these are not needed
		
        obj.content_hash = hint.string;
        
		obj.location = hint.text;
		
		description(end + 1) = struct('name', 'data_file_id', 'type', 'INTEGER', 'constraints', '');
		
% 		obj.path = hint.string;
	
	case 'log'
		
		obj.name = hint.string;
		
		obj.user_id = hint.integer;
		
		% NOTE: this would make certain things simpler
		
		obj.sound_id = hint.integer;
		
		obj.public = hint.logical;
		
		obj.events_count = hint.integer;
% 		
		description(end + 1) = struct('name', 'public', 'type', 'BOOLEAN', 'constraints', 'DEFAULT true');
		
% 		opt.constraints = {'UNIQUE (name, user_id, sound_id)'};

		nulled = {'sound_id'};
		
		for k = 1:numel(nulled)
			description(end + 1) = struct('name', nulled{k}, 'type', 'INTEGER', 'constraints', '');
		end
		
	case 'event' 
		
		obj = rmfield(obj, {'id', 'guid'});
		
		obj = struct_merge(obj, build_prototype(event_table_fields));
		
		% NOTE: the 'log_id' is added to the SQLite table for BARN logs
		
		obj.log_id = hint.integer;
		
		obj.recording_id = hint.integer;
		
		obj.recording_start = hint.real;
		
		obj.sound_id = hint.integer;
		
		% NOTE: this may support UTC or TAI or some other standard
		
		% REF: http://en.wikipedia.org/wiki/Time_standard
		
		% TODO: determine whether we can use a real here
		
		obj.standard_start = hint.string;
		
		obj.standard_type = hint.string;
		
	%-------------------------
	% METADATA TABLES
	%-------------------------
		
	case 'barn_master'
		
		obj.type = hint.string; 
		
		obj.name = hint.string;
		
		obj.create_sql = hint.text;
		
		obj = rmfield(obj, 'guid');
		
	case 'job'
		
		obj = rmfield(obj, 'guid');
		
		% job organization fields
		
		obj.parent_id = hint.integer; 
		
		obj.parent_fraction = hint.real; 
		
		% description fields
		
		obj.atomic = hint.boolean; % NOTE: we make this explicit for all tasks, rather than rely on the job task description
		
		obj.name = hint.string;
		
		obj.description = hint.string;
		
		obj.handler_type = hint.string;
		
		obj.handler_guid = hint.string;
		
		obj.task = hint.text;
		
		% management and execution fields
		
		obj.priority = hint.integer;
		
		obj.user_id = hint.integer;
		
		obj.project_id = hint.integer;
		
		% NOTE: this helps us handle distributed transactions
		
		obj.daemon_guid = hint.string;
		 
		obj.daemon_transaction = hint.string;
		
		obj.run_at = hint.timestamp;
		
		obj.started_at = hint.timestamp;
				
		obj.attempt = hint.integer;
		
		obj.progress = hint.real;
		
		obj.report = hint.text;
		
		obj.completed_at = hint.timestamp;
		
		obj.failed_at = hint.timestamp;
		
		% NOTE: this is the update related to job execution, 'modified_at' relates to job modifications?
		
		obj.last_update = hint.timestamp;
		
		% description
		
		% NOTE: these are foreign keys that may be empty
		
		nulled = {'parent_id', 'project_id'};
		
		for k = 1:numel(nulled)
			description(end + 1) = struct('name', nulled{k}, 'type', 'INTEGER', 'constraints', ''); %#ok<*AGROW>
		end
		
		zeroed = {'attempt', 'progress'};
		
		for k = 1:numel(zeroed)
			description(end + 1) = struct('name', zeroed{k}, 'type', 'INTEGER', 'constraints', 'DEFAULT 0');
		end
		
		% NOTE: these are time stamps that may be empty
		
		nulled = {'started_at', 'completed_at', 'failed_at'};
		
		for k = 1:numel(nulled)
			description(end + 1) = struct('name', nulled{k}, 'type', 'DATETIME', 'constraints', 'DEFAULT NULL');
		end
		
	case 'worker'
		
		obj.ip = hint.string;
		
		obj.os = hint.string;
				
		obj.hardware_description = hint.string;
		
	case 'image'
		
		obj.filename = hint.string;
		
		obj.size = hint.integer;
		
		obj.content_type = hint.string;
		
		obj.thumbnail = hint.string;
		
		obj.parent_id = hint.integer;
		
		obj.height = hint.integer;
		
		obj.width = hint.integer;
		
		obj.user_id = hint.integer;
		
		obj.created_at = hint.datetime;
		
		description(end + 1) = struct('name', 'parent_id', 'type', 'INTEGER', 'constraints', 'DEFAULT NULL');
		
	case 'data_file'
		
		obj = add_fields(obj, {'location', 'name', 'ext', 'ctime', 'mtime'});

		obj.size = hint.integer;
		
		obj.directory = hint.boolean;
		
		obj.in_ftp = hint.boolean;
		
		obj.parent_id = hint.integer;
		
		obj.project_id = hint.integer;
		
		obj.user_id = hint.integer;
		
		obj.content_hash = hint.string;
		
		nulled = {'ctime', 'mtime'};

		for k = 1:numel(nulled)
			description(end + 1) = struct('name', nulled{k}, 'type', 'DATETIME', 'constraints', '');
		end
		
		% TODO: make a shortcut for nullable foreign keys
		
		description(end + 1) = struct('name', 'parent_id', 'type', 'INTEGER', 'constraints', '');
		
		description(end + 1) = struct('name', 'project_id', 'type', 'INTEGER', 'constraints', '');
		
	case 'peer'
		
		obj = rmfield(obj, 'guid');
		
		obj.host = hint.string;
		
	case 'sync'
		
		obj = rmfield(obj, 'guid');
		
		obj = add_fields(obj, {'http_method', 'http_user_agent', 'http_request_headers', 'added', 'updated', 'deleted'});
		
		% TODO: we want these to have zero default value
		
		obj = add_fields(obj, strcat({'added', 'updated', 'deleted'}, '_count'), 'integer');
		
		% NOTE: this is a foreign key to a peer user combination, we have to establish this relation
		
		obj.peer_user_id = hint.integer;

	case 'repository'
		
		% DESCRIPTION
		
		% NOTE: with this information we know where to install the code
		
		% TODO: THIS IS NOT THE SAME CONCEPT AS IN BARN PROJECT! consider name?
		
		obj.project_name = hint.string; 
		
		obj.project_type = hint.string; % XBAT for now, mat be R or Python in the future
				
		obj.description = hint.text;
		
		obj.long_description = hint.text;
		
		% ACCESS 
		
		obj.url = hint.string;
		
		% NOTE: with this information we know what client to use to retrieve the code
		
		obj.type = hint.string; % SVN for now, may be GIT or HG in the future
		
		obj.user = hint.string; 
		
		obj.password = hint.string;
		
	case 'extension'
		
		obj = rmfield(obj, 'guid');
		
		% TODO: extensions have category labels, consider using machine tags for these
		
		% NOTE: extensions may be public, access through access to a repository is also possible
		
		obj.public = hint.logical;
		
		obj.repository_id = hint.integer;
		
		obj.repository_location = hint.string;
		
		% NOTE: systems like 'git' and 'hg' use a hash for their revision numbers, we need a string here
		
		obj.repository_revision = hint.string;
		
		obj = add_fields(obj, {'type', 'name', 'guid', 'input_type', 'output_type', 'tablename__parameter', 'tablename__value'}, 'string');
		
		% NOTE: currently not in use, they describe the generative, transformative relations between various models
		
		obj = rmfield(obj, {'input_type', 'output_type'});
		
		description(end + 1) = struct('name', 'repository_id', 'type', 'INTEGER', 'constraints', '');
		
	case 'extension_preset'
		
		% NOTE: the idea here is that the preset is essentially a parameter (configuration), with some additional annotations
		
		obj.name = hint.string;		
		
		obj.description = hint.string;
		
		obj.long_description = hint.text;
		
		obj.extension_guid = hint.integer;
		
		% NOTE: the pure serialization of parameters is not always convenient, we are moving towards moving preset files
		
		obj.parameter_id = hint.integer; % NOTE: this selects a row within the extension parameter table
		
		% NOTE: this is a configuration file and corresponding hash for the compute handler application
		
		obj.file = hint.string;
		
		obj.file_hash = hint.string;
		
		obj.user_id = hint.integer;
		
		description(end + 1) = struct('name', 'parameter_id', 'type', 'INTEGER', 'constraints', '');
		
	case 'file_resource'
		
		obj.resource_file_name = hint.string;
		
		obj.resource_content_type = hint.string;
		
		obj.resource_file_size = hint.integer;
		
		obj.caption = hint.string;
		
		obj.attachable_id = hint.integer;
		
		obj.attachable_type = hint.string;
		
		obj.user_id = hint.integer;
		
		description(end + 1) = struct('name', 'attachable_id', 'type', 'INTEGER', 'constraints', '');
		
% 		CREATE TABLE IF NOT EXISTS `file_resource` (
% 		  `id` int(11) NOT NULL AUTO_INCREMENT,
% 		  `guid` varchar(255) DEFAULT NULL,
% 		  `fileresource_type` varchar(255) DEFAULT NULL,
% 		  `fileresource_id` int(11) DEFAULT NULL,
% 		  `resource_file_name` varchar(255) NOT NULL,
% 		  `resource_content_type` varchar(255) NOT NULL,
% 		  `resource_file_size` int(11) NOT NULL,
% 		  `title` varchar(255) DEFAULT NULL,
% 		  `notes` text,
% 		  `user_id` int(11) DEFAULT NULL,
% 		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
% 		  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
% 		  PRIMARY KEY (`id`)
% 		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
    case 'file_server'
        
        obj = rmfield(obj, 'guid');
        
        obj = add_fields(obj, {'server', 'username', 'password', 'initial_path', 'protocol'});
        
        obj = add_fields(obj, {'port', 'user_id'}, 'integer');
		
	case 'review'
		
		obj = rmfield(obj, 'guid');
		
		obj = add_fields(obj, {'parent_type', 'object_type', 'description', 'long_description'}, 'string');
        
		% NOTE: if we declare a review to be majority ruled, then we can
		% call something reviewed as soon as a majority of people have
		% accept or reject consensus, and shortcut the review process
		
        obj = add_fields(obj, {'parent_id', 'tag_id', 'votes', 'majority'}, 'integer');
		
		obj.long_description = hint.text;
		
	%-------------------------
	% UNKNOWN
	%-------------------------
		
	otherwise
		
		% NOTE: we do not throw an error but return all trivial
		
		query = ''; lines = {}; obj = struct; 
		
		if ~nargout
			disp(' '); disp(['  Unknown table ''', table, '''.']); disp(' '); clear query; 
		end

		return;
end

obj.created_at = hint.timestamp; obj.modified_at = hint.timestamp;


%--------------------------
% ADD_FIELDS
%--------------------------

function obj = add_fields(obj, field, type)

%--
% handle input and get type hint
%--

if nargin < 3
	type = 'string';
end

% NOTE: this checks that the type is correct through the hint fields

hint = column_type_hints; hint = hint.(type);

%--
% add fields
%--

for k = 1:numel(field)
	obj.(field{k}) = hint;
end
		

