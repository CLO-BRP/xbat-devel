function root = local_barn_files(obj, type)

% local_barn_files - root directory
% ---------------------------------
%
% root = local_barn_files(obj, type)
%
% Input:
% ------
%  obj - from barn
%  type - of object, if not available from object variable name
% 
% Output:
% -------
%  root - for object related files

%--
% set generic files root
%--

root = files_root;

if ~nargin
	return;
end

%--
% handle input to determine further elements of root
%--

if nargin < 2
	type = inputname(1);
end

type = normalize_type(type);

if isempty(type)
	error('Unable to determine type from input.');
end

switch type
	
	otherwise

		if ~isfield(obj, 'guid') || isempty(obj.guid)
			error('Unable to determine leaf directory name, ''guid'' is missing.');
		end
		
		attach = get_attachment_location(obj);
		
		leaf = obj.guid;
		
end

root = fullfile(root, type, attach, leaf);


%----------------------------------
% NORMALIZE_TYPE
%----------------------------------

function type = normalize_type(type)

type = string_singular(lower(type));

if ~string_is_member(type, {'event', 'sound', 'image'})
	
	error(['Unrecognized resource type ''', type, '''.']);
end




