function [model, lines] = parse_rails_schema(root)

% parse_rails_schema - the current schema
% ---------------------------------------
%
% [model, lines] = parse_rails_schema(root)
%
% Input:
% ------
%  root - of rails application
% 
% Output:
% -------
%  model - collection, each contains 'table', 'prototype', and 'description'
%
% See also: get_barn_prototype

% TODO: this function contains a parse_migration function

%--
% handle input
%--

if ~nargin
	root = rails_root;
end 

model = empty(struct('table', [], 'prototype', [], 'description', [])); lines = {};

if isempty(root)
	return;
end

%--
% get relevant schema lines 
%--

schema = fullfile(root, 'db', 'schema.rb');

opt = file_readlines; opt.skip = 1; opt.pre = '#';

lines = file_readlines(schema, @strip_blanks, opt); 

% NOTE: the first and last lines wrap the migration definition

lines([1, end]) = [];

%--
% parse schema
%--

hint = column_type_hints;

state = '';

for k = 1:numel(lines)
	
	current = lines{k};
	
	if strmatch('create_table', current)

		state = 'create_table';

		model(end + 1).table = 'table';

		model(end).prototype = struct;

		model(end).description = struct;
		
		continue;

	elseif strmatch('add_index', current)

		% NOTE: this is a single line statement in the canonical migration files

	elseif strmatch('end', current)

		state = '';
				
	end
			
	switch state
		
		case 'create_table'
			
			part = str_split(current, ','); 
			
			basic = part{1};
			
			if numel(part) > 1
				constraints = part{2};
			else
				constraints = '';
			end
			
			part = str_split(basic, ' '); field = part{2}(2:end - 1); value = part{1}(3:end);
			
			model(end).prototype.(field) = hint.(value);

	end
	
end

%--
% display if output not captured
%--

if ~nargout
	disp(model); clear model;
end

