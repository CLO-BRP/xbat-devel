function root = user_files_root(user, store, server)

% user_files_root - where user files are kept on server
% -----------------------------------------------------
%
% root = user_files_root(user, store, server)
%
% Input:
% ------
%  user - file owner
%  store - for users
%  server - description
% 
% Output:

%--
% handle input
%--

% NOTE: we expect the last two inputs to come in together

if nargin < 3
	[store, server] = local_barn;
end

if ~nargin
	user = get_active_user; 
end 

try
	user = set_barn_user(store, user);
catch
	% NOTE: in this case the user was a barn user already, update this
end 

%--
% get user files root
%--

% TODO: decide what to call this field

if isfield(user, 'data_files_root') && ~isempty(user.data_files_root)
	root = user.data_files_root;
else
	root = fullfile(server_users_root(server), user.email);
end
