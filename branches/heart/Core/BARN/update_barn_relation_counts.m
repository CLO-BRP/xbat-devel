function update_barn_relation_counts(store, event, varargin)

% update_barn_relation_counts - these are various count caches
% ------------------------------------------------------------
%
% update_barn_relation_counts(store, event, varargin)
%
% Input:
% ------
%  store - database (def: local_barn)
%  event - requesting update
%  varargin - data to support update
% 
% See also: update_relation_count

% TODO: this goes with other maintanence scripts

%--
% handle input
%--

if nargin < 2
	event = '';
end

if ~nargin
	store = local_barn;
end

if isempty(store)
	return;
end

%--
% update relations, considering event if possible
%--

disp(' '); disp(['Updating relation counts on BARN store at ''', store.hostname, ''' ...']);

if ~isempty(event)
	disp(['Trigger: ', ternary(isempty(event), '(null)', event), '']);
end

start = clock;

switch event
	
	case 'manual-event-log'
		% NOTE: we may integrate this code into the general solution when we understand it well
		
	otherwise
		% NOTE: the 'update_relation_count' function is general and updates all objects, hence it is slow
		
		update_relation_count(store, 'sound', 'event');
		
		update_relation_count(store, 'log', 'event');
end

disp(['(elapsed: ', sec_to_clock(etime(clock, start)), ')']); disp(' ');
