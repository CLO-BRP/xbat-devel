function removed = cleanup_barn_resources(store, server)

% cleanup_barn_resources - from filesystem
% ----------------------------------------
%
% removed - cleanup_barn_resources(store, server)
%
% Input:
% ------
%  store - database
%  server - description
%
% Output:
% -------
%  removed - resource directories
%
% See also: render_barn_resources, local_barn

if ~nargin
	[store, server] = local_barn;
end

if isempty(store) || isempty(server)
	return; 
end

root = files_root(server); 

events = fullfile(root, 'event');

removed = scan_dir(events, {@cleanup_callback, store});

if ~nargout
	disp(' '); iterate(@disp, removed); disp(' '); clear removed; 
end


%------------------------------
% CLEANUP_CALLBACK
%------------------------------

function removed = cleanup_callback(current, store)

disp(current);