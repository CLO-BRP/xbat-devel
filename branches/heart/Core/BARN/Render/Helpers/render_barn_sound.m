function result = render_barn_sound(sound, render)

% render_barn_sound - for view display
% ------------------------------------
%
% render = render_barn_sound
%
% result = render_barn_sound(sound, render)
%
% Input:
% ------
%  sound - to render
%  render - options
%
% Output:
% -------
%  render - options default
%  result - of render
%
% See also: render_barn_resources, render_barn_events

%--
% handle input
%--

if nargin < 2
	render = struct;
	
	if ~nargin
		result = render; return;
	end
end

if ~nargin
	sounds = get_library_sounds(gal);
end

if numel(sound) > 1
	result = iterate(mfilename, sound, render); return;
end

%--
% render sounds
%--

disp ' '; disp 'rendering sounds'; disp ' ';

for k = 1:numel(sounds)
	
	% NOTE: when adding the sound for the first time we probably do this
	
	sound = set_barn_sound(local_barn, sounds(k));
	
	disp(local_barn_files(sound))
end

disp ' ';


