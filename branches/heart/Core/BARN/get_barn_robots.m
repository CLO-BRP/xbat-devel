function robot = get_barn_robots(type)

% get_barn_robots - extensions that annotate and store data
% ---------------------------------------------------------
%
% robot = get_barn_robots(type)
%
% Input:
% ------
%  type - of robot (def: barn_robot_types, all known robot types)
%
% Output:
% -------
%  robot - extensions of given type

%--
% handle input 
%--

if ~nargin
	type = barn_robot_types;
else
	if ~string_is_member(type, barn_robot_types)
		error(['Unrecognized robot type ''', type, '''.']);
	end
end

%--
% get robot extensions
%--

robot = get_extensions(type);

if iscell(robot)
	robot = collapse_cell(robot);
end
