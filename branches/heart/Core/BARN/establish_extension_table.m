function table = establish_extension_table(store, ext, type, prototype)

% establish_extension_table - in BARN store
% -----------------------------------------
%
% establish_extension_table(store, ext, type, prototype)
%
% Input:
% ------
%  store - database
%  ext - extension
%  type - of table 'parameter' or 'value'
%  prototype - for value to store
%
% Output:
% -------
%  table - row from master
%
% See also: extension_table_names, generate_barn_extension

%--
% handle input
%--

if nargin < 3
	type = 'parameter';
end

%--
% setup
%--

hint = column_type_hints;

table = extension_table_names(ext);

%--
% establish extension table of required type
%--

generate_barn_extension( ext, store, false );

switch type
	
	case 'parameter'	
		
		if has_barn_table(store, table.parameter)
			return;
		end
		
		if nargin < 4
			parameter = ext.parameter;

			% NOTE: the 'prototype' code below just helps our view of the table placing the identifier columns first

			prototype = struct; field = fieldnames(parameter);

			prototype.id = 1; prototype.content_hash = 'string';

			prototype.created_at = []; prototype.modified_at = [];

			for k = 1:numel(field)
				prototype.(field{k}) = parameter.(field{k});
			end

			% NOTE: the 'user_id' is the foreign key that links users, parameters, and values through parameters

			prototype.user_id = hint.integer;
		end
		
		table = establish_barn_table(store, table.parameter, 'extension', create_table(prototype, table.parameter));
		
	case 'value'	
		
		if has_barn_table(store, table.value)
			return;
		end	
		
		input = prototype; prototype = struct; field = fieldnames(input);
		
		prototype.id = hint.integer;
		
		% TODO: do we need a 'guid' or a 'content_hash' here?
		
		prototype.guid = hint.string;
		
		for k = 1:numel(field) 
			prototype.(field{k}) = input.(field{k});
		end
		
		prototype.created_at = []; prototype.modified_at = [];
				
		table = establish_barn_table(store, table.value, 'extension', create_table(prototype, table.value));
end
