function setup_barn_test(user, password)

% setup_barn_test - clean database
% --------------------------------
%
% setup_barn_test
%
% See also: establish_barn_schema

% TODO: we need to create users with different GRANT privileges, we don't want accidental DROP of a database

% TODO: test this with other DB engines, develop for SQLite that should use a different approach

%--
% handle input
%--

if nargin < 2
	password = '';
end

if ~nargin
	user = '';
end

%--
% use the local barn connection to setup test database
%--

if isempty(local_barn)
	error('Local BARN connection is not available.');
end 

name = 'barn_test';

% TODO: we need to update the 'user' and 'password' for the 'local_barn' connection 

% NOTE: in tune with the top TODO, the typical 'local_barn' connection uses a limited privilege user

query(local_barn, ['DROP DATABASE IF EXISTS ', name]);

query(local_barn, ['CREATE DATABASE ', name]);

%--
% establish schema in test database
%--

store = create_database_config('adapter', 'mysql', 'database', 'barn_test');

establish_barn_schema(store);