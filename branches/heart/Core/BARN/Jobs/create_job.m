function [job, field] = create_job(varargin)

% create_job - struct
% --------------------
% 
% job = create_job(field, value, ... )
%
% Input:
% ------
%  field - of job
%  value - for field
% 
% Output:
% -------
%  job - struct

persistent PROTOTYPE;

if isempty(PROTOTYPE)
	PROTOTYPE = struct_empty(get_barn_prototype('job'));
end

% NOTE: when the first input is a struct we update the prototype, then we process the remaining pairs

% TODO: this object creation pattern could be used for other objects

if numel(varargin) && isstruct(varargin{1})
	job = struct_update(PROTOTYPE, varargin{1}); varargin(1) = [];
else
	job = PROTOTYPE;
end

job.attempt = 0; % we should not need this!

[job, field] = parse_inputs(job, varargin{:});
