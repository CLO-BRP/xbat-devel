function establish_jobs(store, force)

% establish_jobs - make sure that we have a job table
% -----------------------------------------------------
%
% establish_jobs(store, force)
%
% Input:
% ------
%  store - to update
%  force - creation of job table, drop current if it exists
%
% Output:
% -------
%  status - of query
%  result - from query

%--
% set no force default
%--

if nargin < 2
	force = 0;
end

%--
% drop if requested, then make sure we have a job table
%--

if force
	query(store, 'DROP TABLE job;');
end

query(store, create_barn_table('job'));