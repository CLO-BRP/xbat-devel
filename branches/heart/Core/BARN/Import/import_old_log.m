function log = import_old_log(file, lib, store, server, render)

% import_old_log - file into barn
% -------------------------------
%
% log = import_old_log(file, store, server)
%
% Input:
% ------
%  file - log
%  store - for barn
%  server - for barn

% NOTE: eventually we can create a dialog

% TODO: consider adding a project input

%--
% handle input
%--

if nargin < 5
	render = []; 
end

% NOTE: this is the BARN database and server where the logs will live

[store0, server0] = local_barn;

if nargin < 4 || isempty(server)
	server = server0;
end

if nargin < 3 || isempty(store)
	store = store0;
end

% NOTE: this is the XBAT library where the sounds will live

if nargin < 2 || isempty(lib)
	
	lib = get_active_library; 
end

% NOTE: by default we scan the current directory for logs to import

if ~nargin
	source = pwd;
elseif  ~iscell(file) && exist_dir(file)
	source = file;
end

if ~nargin || isempty(file)	|| exist('source', 'var')
	
	content = what(source);
	
	if isempty(content.mat)
		log = []; return;
	end 
	
	file = iteraten(@fullfile, 2, content.path, content.mat);
end 

%--
% handle multiple files
%--

if iscell(file) && numel(file) > 1
	
	for k = 1:numel(file)
		log = import_old_log(file{k}, lib, store, server, render); 
	end
	
	return;
end

%--
% check old log and sound
%--

% NOTE: load log from MAT file

log = struct_pop(load(file)); [ignore, log.file] = fileparts(file);

db_disp(['loaded-log: ', log.file]);

% NOTE: reconnect log sound 

location = find_sound_files(log.sound);

if isempty(location)
	
	warning(['Failed to find sound files for log ''', log.file, '''.']);
end

log.sound.path = location;

db_disp updated-log-sound; disp(keep_fields(log.sound, {'path', 'file', 'duration'}));
 
% create BARN sound if needed

add_sounds([log.sound], lib);

% create BARN log

old_log = log;

log = new_log(old_log.sound, old_log.file, 'BARN', store);

% add events to BARN log, this renders the logs

event = old_log.event; 

for k = 1:numel(event)
	event(k).id = [];
end

if string_contains(upper(log.name), 'ISRAT') 
	
	% NOTE: we update the event to be centered as the in the ISRAT log, but with a duration of 5s
	
	for k = 1:numel(event)
		event(k).time = mean(event(k).time) + [-2.5, 2.5];
		
		event(k).duration = diff(event(k).time);
		
		% NOTE: events from an ISRAT log are putatively right-whales
		
		event(k).tags = union(event(k).tags, 'NRW');
	end
	
	user = get_extensions('sound_detector', 'name', 'ISRAT');
else
	user = get_active_user;
end

% TODO: we need to set render options based on project or sound

% TODO: we should create a user for the detector extension, look up how we create robot users

% user = set_barn_user;

request = log_save_events; 

request.user = user;

request.render = render_whales;

log_save_events(log, event, request);



%---------------------------------
% FUNCTIONS
%---------------------------------

% TODO: get some old log function and put them here



