function [files, sound, location] = get_old_log_files(path, first)

% get_old_log_files - return valid log files
% ------------------------------------------
%
% files = get_old_log_files(location, first)
%
% Input:
% ------
%  location - directory
%  first - find return (def: false)
%
% Output:
% -------
%  files - found
%  sound - available indicator
%  location - of sound data

%--
% handle input
%--

if nargin < 2
	first = false;
end

if iscellstr(path) && numel(path) > 1
	
	[files, sound, location] = iterate(mfilename, path, first); return;
end

path = cellfree(path); % NOTE: we should not need this

%--
% find log files in directory
%--

content = dir(path); content = content(~[content.isdir]);

files = {};

for j = 1:numel(content)
	
	if ~string_ends(content(j).name, '.mat')
		continue;
	end
	
	candidate = fullfile(path, content(j).name);
	
	if old_log_valid(candidate)
		
		if first
			files = candidate; [sound, location] = check_log_sound(files); return;
		else
			files{end + 1} = candidate; %#ok<AGROW>
		end
	end
	
end

if nargout > 1
	[sound, location] = check_log_sound(files);
end


