function found = find_sound_files(sound)

% find_sound_files - through names and search
% -------------------------------------------
%
% found = find_sound_files(sound, sources)
%
% Input:
% ------
%  sound - containing files
%  sources - directories to search
%
% Output:
% -------
%  found - new location
%
% See also: sound_file_sources

% TODO: this code may go somewhere else, it could be more generally useful

%--
% handle input
%--

if nargin < 2
	sources = sound_file_sources;
end

%--
% find files
%--

found = ''; % NOTE: as we start we have not found the new location

switch sound.type
	
	case 'file'
		
		[ignore, name, ext] = fileparts(cellfree(sound.file));
		
		for k = 1:numel(sources)
			% TODO: we could use a filter here to reduce the number of candidates
			
			content = scan_dir_for(ext, sources{k});
			
			for j = 1:numel(content)
				[ignore, candidate, ext] = fileparts(content{j});
				
				if strcmp(name, candidate)
					% NOTE: if we maintain a content-hash for the file we can have a more stringent test here
					
					found = fileparts(content{j}); break;
				end
			end
		end
						
	case 'file stream'
		
		% NOTE: this was causing problems with the use of 'fileparts' to get the leaf directory
		
		if sound.path(end) == filesep
			sound.path(end) = [];
		end
		
		[ignore, root] = fileparts(sound.path); [ignore, ignore, ext] = fileparts(sound.file{1});
		
		for k = 1:numel(sources)
			
			content = cached_scan_dir(sources{k});
						
			for j = 1:numel(content)
				
				[ignore, candidate] = fileparts(content{j});
				
				if strcmp(root, candidate)
					% NOTE: the new location may include files not previously available, but we want all known files to be available
					
					files = what_ext(content{j}, ext(2:end)); files = files.(ext(2:end));
					
					available = string_is_member(sound.file, files);
					
					if all(available)
						found = content{j}; break;
					else
						disp(' ');
						disp(['Some files are unavailable at candidate location ''', content{j}, '''.']);
						iterate(@disp, sound.file(~available));
						disp(' ');						
					end
				end
				
			end
		end	
end




