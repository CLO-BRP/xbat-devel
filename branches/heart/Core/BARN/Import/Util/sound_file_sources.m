function sources = sound_file_sources(op, varargin)

% sound_file_sources - directory list
% -----------------------------------
%
% sources = sound_file_sources(op, root, ... )
%
% Input:
% ------
%  op - operation 'add', 'del', 'set'
%  root - location for sound files
%
% Output:
% -------
%  sources - currently registered
%
% See also: find_sound_files

persistent SOURCES;

if isempty(SOURCES)
	SOURCES = {};
end

if ~nargin
	if isempty(SOURCES)
		sources = {pwd};
	else
		sources = SOURCES;
	end
	
	return;
end

if ~iscellstr(varargin)
	error('Sources must be a collection of path strings.');
end

switch op
	case {'add'}
		SOURCES = union(SOURCES, varargin);
		
	case {'del', 'delete', 'rem', 'remove'}
		SOURCES = setdiff(SOURCES, varargin);
		
	case 'set'
		SOURCES = varargin;
end

for k = numel(SOURCES):-1:1
	
	if ~exist_dir(SOURCES{k})
		disp(['Source ', SOURCES{k}, ' not found and removed.']);
		
		SOURCES(k) = [];
	end 
end

sources = SOURCES;