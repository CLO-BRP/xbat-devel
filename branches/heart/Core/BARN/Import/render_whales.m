function render = render_whales

render = render_barn_events;

render.clip.speed = [0.5, 1, 2 ,4];

% NRW
%-------------------------
% a.       FFT: 512
% b.      Auto advance: on
% c.       Page duration: 60 s
% d.      Page overlap: 0.05
% e.      Frequency range: 10-400
% f.        Colormap: grey
% g.       Inverse: checked
% h.      Autoscale: checked
% i.         Screen orientation: vertical, with all channels with sound displayed. 

% FIN
%-------------------------
% a.       FFT: 2048
% b.      Auto advance: on
% c.       Page duration: 120 s
% d.      Page overlap: 0.05
% e.      Frequency range: 10-100
% f.        Colormap: grey
% g.       Inverse: checked
% h.      Autoscale: checked
% i.         Screen orientation: vertical, with all channels with sound displayed.