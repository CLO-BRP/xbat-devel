function old_render_barn_resources(type, varargin)

% render_barn_resources - assets, images and sound clips
% ------------------------------------------------------
%
% render_barn_resources(type, varargin)
%
% Input:
% ------
%  type - of resource to render

if isempty(local_barn)
	return;
end

%--
% handle input
%--

if ~nargin
	type = 'all';
end

type = lower(string_singular(type));

switch type
	case 'all'
		disp 'Rendering all resources not currently implemented.'
		
	case 'log'
		render_barn_events(varargin{:});
	
	case 'sound'
		render_barn_sounds(varargin{:});
end


%----------------------------------
% RENDER_BARN_EVENTS
%----------------------------------

function render_barn_events(log, event, specgram)

%----------------------
% HANDLE INPUT
%----------------------

%--
% set default specgram and event
%--

if nargin < 3
	specgram = [];
end

if nargin < 2
	event = [];
end

%--
% get all logs in active library
%--

if ~nargin
	log = get_logs(gal);
end

if isempty(log)
	return;
end

%--
% render all events for input logs
%--

if numel(log) > 1
	
	iterate(@render_barn_events, log, event, specgram); return;
end


%--
% make sure that we have proper events
%--

% NOTE: we assume that a single complete event, no events, or a set of incomplete event are input

if numel(event) == 1 && isfield(event, 'guid') && ~isempty(event.guid)
	render = event;
	
elseif isempty(event)
	%---------------------------------------
	
	log_id = int2str(log.store.log.id); limit = 200;
	
	[ignore, result] = query(log.store, ...
		['SELECT COUNT(id) AS total FROM event WHERE log_id = ', log_id] ...
	); %#ok<*ASGLU>
	
	pages = ceil(result.total / limit);
	
	for k = 1:pages
		disp(['Rendering page ', int2str(k), ' of ', int2str(pages)]);
		
		[ignore, page] = query(log.store, ...
			['SELECT id FROM event WHERE log_id = ', log_id, ' ORDER BY score DESC LIMIT ', int2str(limit), ' OFFSET ', int2str((k - 1) * limit)] ...
		);
				
		event = log_get_events(log, [page.id], 0);
		
		start = clock;
		
		render_barn_events(log, event, specgram);
		
		disp(sec_to_clock(etime(clock, start)));
	end
	
	return;
	%---------------------------------------
else
	% NOTE: this is a bit of paranoia, in case we have ducks or resources have changed
	
% 	render = log_get_events(log, [event.id], 0);
	
	render = event;
end




%----------------------
% SETUP
%----------------------

%--
% configure clips
%--

ext = {'.wav', '.mp3'};

clip = create_event_clip; clip.rate = 44100;

%--
% configure thumbs
%--

opt = create_event_thumb;

if isempty(specgram)
	
	rate = get_sound_rate(log.sound); time = specgram_resolution(log.sound.specgram, rate);
	
	% TODO: we should adapt the display parameters maintaining 'aspect ratio'
	
	specgram = fast_specgram;

	% NOTE: these are the images currently on display in the BARN interface, the '0.75' increases the time resolution
	
% 	specgram.fft = 256; specgram.win_length = 1; specgram.hop = 0.75 * (time * rate) / 256;

	specgram.fft = 256; specgram.win_length = 1; specgram.hop = 0.25 * 0.75 * (time * rate) / 256;
	
	specgram(end + 1) = specgram;
	
% 	specgram(end).fft = 512; specgram(end).win_length = 0.5; specgram(end).hop = (time * rate) / 512;
	
% 	db_disp; disp(specgram)
end

filt = {filt_binomial(3, 3), filt_binomial(5, 5)};

map.name = 'gray-light'; map.scale = 1; map.matrix = flipud(gray(512));

% map(end + 1) = map;
% 
% map(end).name = 'gray-dark'; map(end).matrix = gray(512);

% map(end + 1).name = 'jet'; map(end).matrix = jet(512); map(end).scale = 0;
% 
% map(end + 1).name = 'sunset'; map(end).matrix = cmap_pseudo(512); map(end).scale = 0;

%----------------------
% RENDER
%----------------------

% db_disp 'rendering events';

%--
% render events
%--

thumb = create_event_thumb;

for k = 1:numel(render)

	% HACK: this should not be needed eventually
	
	if isempty(render(k).guid)
		db_disp 'THIS SHOULD NOT HAPPEN, when we get here the event should have a GUID!'; stack_disp;
		
% 		render(k).guid = generate_uuid;
% 		
% 		log_save_events(log, render(k));
		
		continue; % NOTE: since we save the clips along with the update, this should change
	end 

	disp(['Rendering event ', render(k).guid]);
	
	%--
	% determine destination
	%--
	
	destination = create_dir(local_barn_files(render(k), 'event'));

% 	disp(destination);

	%--
	% render clips
	%--
	
	for j = 1:numel(ext)
		file = create_event_clip(log.sound, render(k), fullfile(destination, ['clip', ext{j}]), clip);

% 		disp(file);
	end

	%--
	% render thumbs
	%--
	
	for j1 = 1:numel(specgram)
		
		% TODO: pass an array of maps to the create thumb function
		
		for j2 = 1:numel(map)

			file = fullfile(destination, str_implode({'thumb', int2str(specgram(j1).fft), map(j2).name, 'png'}, '.'));
			
			thumb.specgram = specgram(j1); thumb.filter = filt{j1}; thumb.map = map(j2);
			
			file = create_event_thumb(log.sound, render(k), file, thumb);

% 			disp(file);
		end
	end
	
% 	disp ' ';
end


%----------------------------------
% RENDER_BARN_SOUNDS
%----------------------------------

function render_barn_sounds(sounds)

%--
% handle input
%--

if ~nargin
	sounds = get_library_sounds(gal);
end

%--
% render sounds
%--

disp ' '; disp 'rendering sounds'; disp ' ';

for k = 1:numel(sounds)
	
	% NOTE: when adding the sound for the first time we probably do this
	
	sound = set_barn_sound(local_barn, sounds(k));
	
	disp(local_barn_files(sound))
end

disp ' ';


