function child = create_child_job(parent, varargin)

% create_child_job - from parent and further description
% -------------------------------------------------------
%
% child = create_child_job(parent, field, value, ... )
%
% Input:
% ------
%  parent - job
%  field, value - pairs describing child
%
% Output:
% -------
%  child - job
%
% See also: create_job

% NOTE: these are the parent fields typically shared by the child

fields = { ...
	'name', ...
	'description', ...
	'handler_type', ...
	'handler_guid', ...
	'priority', ...
	'user_id', ...
	'project_id', ...
};

child = create_job(keep_fields(parent, fields), 'parent_id', parent.id, varargin{:});


