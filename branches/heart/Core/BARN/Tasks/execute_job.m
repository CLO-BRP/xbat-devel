function result = execute_job(job, store, server)

% execute_job - manager for jobs
% --------------------------------
%
% result = execute_job(job, store, server)
%
% Input:
% ------
%  job - to execute
%  store, server - connection information, store holds queue
%
% See also: job_daemon, pick_job

[handler, ignore, context] = get_browser_extension('job', 0, job.handler_guid); %#ok<ASGLU>

context.store = store; context.server = server;

%--
% delegate job execution to handler
%--

helper = handler.fun.execute;

result = struct; result.started = clock;
 
if ~isempty(helper)
% 	db_disp; disp(functions(helper));
	
	try
		[job, context] = helper(job, handler.parameter, context); %#ok<NASGU>
	catch
		extension_warning(handler, 'Failed to execute job.', lasterror);
		
		result.lasterror = lasterror;
	end
else
	% NOTE: this updates the job to be completed

	job.daemon = ''; job.completed_at = datestr(now, 31);
end

%--
% update job and pack results
%--

job = update_job(store, job, 'completed_at', job.completed_at);

% job = set_barn_objects(store, job);

result.job = job;

result.failed = isfield(result, 'lasterror');

result.elapsed = etime(clock, result.started);

% TODO: we want to rely on 'get_ip', the current module for this is very obscure


