function user = update_user(user)

% update_user - 'modernize' user structure
% ----------------------------------------
% user = update_user(user)
%
% Input:
% ------
%  user - user
%
% Output:
% -------
%  user - updated user

if isstruct(user.library)
	user.library = {user.library.path}';
end

user = struct_update(user_create, user);
