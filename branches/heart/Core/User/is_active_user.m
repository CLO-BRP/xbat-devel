function value = is_active_user(user)

% is_active_user - check user is active user
% ------------------------------------------
%
% value = is_active_user(user) 
%
% Input:
% ------
%  user - user
%
% Output:
% -------
%  value - active indicator

%--
% check input is a user struct
%--

% NOTE: this could become an 'is_user' function

value = isequal(empty(user), empty(user_create));

if ~value
	return;
end

%--
% check user and active user have same id 
%--

% NOTE: this might allow us to change the name of a user 

active = get_active_user; value = (active.id == user.id);