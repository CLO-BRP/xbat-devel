function user = new_user_dialog(user)

% new_user_dialog - dialog to create new users
% --------------------------------------------
%
% user = new_user_dialog(user)
%
% Input:
% ------
%  user - user to edit
%
% Output:
% -------
%  user - new user created

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set new user state
%--

new = ~nargin;

%----------------------------------
% CREATE CONTROLS AND DIALOG
%----------------------------------

%--
% get controls, these are dynamic with respect to the 'new_log_format'
%--

if new
	control = get_user_controls;
else
	control = get_user_controls(user);
end

%--
% configure dialog options
%--

opt = dialog_group;

if new
	opt.width = 16;
else
	opt.width = 16;
end

opt.header_color = get_extension_color('root');

opt.text_menu = 1;

%--
% create dialog
%--

if new
	name = 'New User ...';
else 
	name = 'Edit User ...';
end

try
	out = dialog_group(name, control, opt, {@new_user_callback, new});
catch
	out.values = []; nice_catch(lasterror);
end


% NOTE: return empty on cancel

if isempty(out.values)
	user = []; return;
end

%----------------------------------
% CREATE AND SUBSCRIBE USER
%----------------------------------

values = out.values;

%--
% create and add user
%--

if new
	user = user_create;
else
	user = get_active_user;
end

opt = struct_update; opt.flatten = 0; 

user = struct_update(user, values, opt);


% curl -H "Authorization:ZGVmYXVsdDpwYXNzd29yZA==" -X POST http://localhost:3000/users -d 'user[login]=xxx&user[password]=xxxxxx&user[password_confirmation]=xxxxxx&user[email]=x@x.com'
 

% NOTE: this creates the default user library and subscribes the user to it

if new
	user = add_user(user);
end 

%--
% set user preferences
%--

values = unflatten_struct(values); prefs = values.prefs;

field = fieldnames(prefs);

for k = 1:length(field)
	
	value = values.prefs.(field{k});
	
	if iscell(value) && (length(value) == 1)
		value = value{1};
	end 
	
	user = set_user_preference(user, field{k}, value);
	
end

user_save(user);


%----------------------------------
% NEW_USER_CALLBACK
%----------------------------------

function new_user_callback(obj, eventdata, new) %#ok<INUSD>

%--
% get callback context
%--

callback = get_callback_context(obj, eventdata); 

control = callback.control; pal = callback.pal;

%--
% get lists
%--

available = get_control(pal.handle, 'available_libraries', 'value');

subscribed = get_control(pal.handle, 'user_libraries', 'value');

%--
% toggle button enabled-ness
%--

set_control(pal.handle, 'add', 'enable', ~isempty(available));

set_control(pal.handle, 'rem', 'enable', ~isempty(subscribed));

%--
% process callback request
%--

switch control.name
		
	%--
	% name, email, and password
	%--
	
	case {'name', 'email', 'confirm_email', 'password', 'confirm_password'}
		
		result = validate_basic(pal.handle);

		set_control(pal.handle, 'OK', 'enable', result);
		
		% NOTE: in this case we are editing the email, make sure we confirm
		
		if strcmp(control.name, 'email') && ~result	
			
			set_control(pal.handle, 'confirm_email', 'enable', 1, 'value', '');
			
		end
		
		if strcmp(control.name, 'password') && ~result	
			
			set_control(pal.handle, 'confirm_password', 'enable', 1, 'value', '');
			
		end
		
	%--
	% add (subscribe to) library
	%--
	
	case 'add'
		
		for k = 1:length(available)
			user_subscribe(get_library_from_name(available{k}));
		end

		han = get_control(pal.handle, 'user_libraries', 'handles');

		set(han.obj, ...
			'string', library_name_list(get_active_user), 'value', [] ...
		);			
		
	%--
	% remove (unsubscribe from) library
	%--		
		
	case 'rem'
		
		for k = 1:length(subscribed)
			user_unsubscribe(get_library_from_name(subscribed{k}));	
		end	
	
		han = get_control(pal.handle, 'user_libraries', 'handles');

		set(han.obj, ...
			'string', library_name_list(get_active_user), 'value', [] ...
		);			
	
	%--
	% locate library
	%--
		
	case 'locate ...'
		
		%--
		% look for library starting in this user's root
		%--
		
		user = get_active_user;
		
		di = pwd; cd(user_root(user, 'lib'));
		
		[lib_file, lib_path] = uigetfile( ...
			{'*.mat', 'MAT-files (*.mat)'}, ...
			'Please select an existing library file.' ...
		);
		
		cd(di);

		if isnumeric(lib_file) || ~is_library(lib_path, lib_file) 
			set_control(pal.handle, 'OK', 'enable', 1); return;
		end
		
		%--
		% get library
		%--
			
		lib = load_library(fullfile(lib_path, lib_file));
		
		%--
		% subscribe user to library if needed
		%--
		
		user_subscribe(lib, get_active_user);	
		
		%--
		% update available libraries control
		%--		
	
		han = get_control(pal.handle, 'available_libraries', 'handles');
		
		set(han.obj, ...
			'string', library_name_list(get_users), 'value', [] ...
		);		
	
		han = get_control(pal.handle, 'user_libraries', 'handles');

		set(han.obj, ...
			'string', library_name_list(get_active_user), 'value', [] ...
		);	
	
	%--
	% set new log format
	%--
	
	case 'prefs__new_log_format'
		
		%--
		% get figure name and value of control set select control
		%--
		
		name = get(pal.handle, 'name');

		value = cellfree(control.value);
		
		% TODO: the user may be empty
		
		if ~new
			user = get_active_user; 
		else 
			user = [];
		end
		
		control = get_user_controls(user, value);

		% NOTE: this call can preserve control values for similarly named controls

		control = update_control_values(control, get_control_values(pal.handle));
			
		%--
		% reload dialog controls
		%--
	
		dialog_group(name, control, [], {@new_user_callback, new}, [], pal.handle);
		
		tab_select([], pal.handle, 'Logs');
		
end


%-------------------------------------------
% VALIDATE_BASIC
%-------------------------------------------

function result = validate_basic(pal)

name = get_control_value(pal, 'name');

email = get_control_value(pal, 'email');

confirm_email = get_control_value(pal, 'confirm_email');

% password = get_control_value(pal, 'password'); 
% 
% confirm_password = get_control_value(pal, 'confirm_password');

test(1) = proper_filename(name);

test(2) = proper_email(email) && strcmp(email, confirm_email);

% test(3) = proper_password(password) && strcmp(password, confirm_password);

result = all(test);


%-------------------------------------------
% PROPER_PASSWORD
%-------------------------------------------

function result = proper_password(password)

result = length(password) > 6;


%-------------------------------------------
% IS_LIBRARY
%-------------------------------------------

function out = is_library(path, file)

out = 0;

fullfile = [path, file];

contents = load(fullfile, 'lib');

if ~isfield(contents, 'lib')
	return
end

lib = contents.lib;

if ~isequal(fieldnames(library_create), fieldnames(lib));
	return
end

out = 1;


%-------------------------------------------
% GET_PREFERENCE_CONTROLS
%-------------------------------------------

function control = get_preference_controls(tab, control)

if nargin < 2
	control = empty(control_create);
end 

% TODO: we could also consider appending the controls

preference = {'palette_sounds', 'palette_tooltips', 'image_buttons', 'sep', 'DEBUG'};

for k = 1:length(preference)

	if strcmp(preference{k}, 'sep')
		control(end + 1) = control_create('style', 'separator', 'tab', tab); continue;
	end
	
	%--
	% get preference value
	%--
	
	value = get_user_preference([], preference{k});

	if isempty(value)
		value = 0;
	end

	%--
	% add preference control
	%--
	
	control(end + 1) = control_create( ...
		'style', 'checkbox', ...
		'name', ['prefs__', preference{k}], ...
		'alias', preference{k}, ...
		'value', value, ...
		'tab', tab ...
	);

end


%-------------------------------------------
% GET_USER_CONTROLS
%-------------------------------------------

function control = get_user_controls(user, format)

%--
% handle input
%--

if nargin < 2
	format = '';
end

new = ~nargin || isempty(user);

%--
% create controls
%--

control = empty(control_create);

%-----------------
% BASIC
%-----------------

if ~new
	str = ['User  (', user.name, ')'];
else
	str = 'User';
end

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'space', .1, ...
	'string', str ...
);
	
if new
	tabs = {'Profile', 'Logs'};
else
	tabs = {'Profile', 'Logs', 'Libraries'};
end

control(end + 1) = control_create( ...
	'style', 'tabs', ...
	'tab', tabs ...
);

%--
% name
%--

width2 = 0.69;

control(end + 1) = control_create( ...
	'name', 'name', ...
	'space', 1, ...
	'width', width2, ...
	'onload', 1, ...
	'space', 0.75, ...
	'style', 'edit', ...
	'tab', tabs{1}, ...
	'type', 'filename' ...
);

if ~new
	control(end).string = user.name; control(end).initialstate = '__DISABLE__';
end

%--
% email, password, and url
%--

% TODO: consider setting the validator as default given name

control(end + 1) = control_create( ...
	'name', 'email', ...
	'type', 'email', ...
	'space', 0.75, ...
	'tab', tabs{1}, ...
	'style', 'edit' ...
);

if ~new
	control(end).string = user.email;
end

control(end + 1) = control_create( ...
	'name', 'confirm_email', ...
	'type', 'email', ...
	'space', 1.25, ...
	'tab', tabs{1}, ...
	'style', 'edit' ...
);

if ~new
	control(end).string = user.email; control(end).initialstate = '__DISABLE__';
end

% control(end + 1) = control_create( ...
% 	'name', 'password', ...
% 	'style', 'password', ...
% 	'space', 0.75, ...
% 	'width', 0.75, ...
% 	'tab', tabs{1} ...
% );
% 
% if ~new
% 	control(end).string = user.password; db_disp; control(end)
% end
% 
% control(end + 1) = control_create( ...
% 	'name', 'confirm_password', ...
% 	'style', 'password', ...
% 	'space', 1.25, ...
% 	'width', 0.75, ...
% 	'tab', tabs{1} ...
% );
% 
% if ~new
% 	control(end).string = user.password; control(end).initialstate = '__DISABLE__'; db_disp; control(end)
% end
% 
% control(end + 1) = control_create( ...
% 	'name', 'url', ...
% 	'alias', 'URL', ...
% 	'space', 0.75, ...
% 	'tab', tabs{1}, ...
% 	'style', 'edit' ...
% );
% 
% if ~new
% 	control(end).string = user.url;
% end

developer = get_user_preference([], 'developer');

if isempty(developer)
	developer = 0;
end

control(end + 1) = control_create( ...
	'style', 'checkbox', ...
	'name', 'prefs__developer', ...
	'alias', 'DEVELOPER', ...
	'value', developer, ...
	'tab', tabs{1} ...
);

control(end).space = 1.5;

%--------------------------
% LOGS
%--------------------------

% NOTE: we take the default format from the helper unless we have a proper input format

[names, ix0] = get_log_format_names;

if isempty(format)
	ix = ix0;
else
	[found, ix] = string_is_member(format, names);
	
	if ~found
		ix = ix0;
	end
end

control(end + 1) = control_create( ...
	'name', 'prefs__new_log_format', ...
	'alias', 'format', ...
	'style', 'popup', ...
	'tab', tabs{2}, ...
	'string', names, ...
	'value', ix ...
);

% NOTE: here we load the controls for the log format

[format, context] = get_log_format(names{ix});

if has_any_presets(format)
	
	% NOTE: here we try to create controls for any controllable parameters
	
	% TODO: make sure we can perform callbacks
	
	try
		% TODO: perhaps the control create can actually use the context prefix!
		
		context.prefix = 'prefs__new_log_parameter__';
		
		specific = get_extension_controls(format, context); specific(1) = [];
		
		if ~isempty(specific)
			% NOTE: we are making sure that we pack the log default parameters as a preference
			
			for k = 1:numel(specific)
				specific(k).alias = specific(k).name;
				
				specific(k).name = ['prefs__new_log_parameter__', specific(k).name];
			end
			
			control(end).space = 1.5; control(end + 1) = control_separator([], [], 'tab', tabs{2});
			
			specific(end).space = 2;
		end 
		
		for k = 1:numel(specific)
			specific(k).tab = tabs{2};

			control(end + 1) = specific(k);
		end
		
	catch
		
		db_disp; nice_catch;	
	end

end

% NOTE: this is the current working code

% [adapters, ix] = get_barn_adapters;
% 
% control(end + 1) = control_create( ...
% 	'name', 'adapter', ...
% 	'style', 'popup', ...
% 	'tab', tabs{2}, ...
% 	'string', adapters, ... 
% 	'value', ix ...
% );
% 
% control(end + 1) = control_create( ...
% 	'name', 'database', ...
% 	'style', 'edit', ...
% 	'tab', tabs{2}, ...
% 	'string', 'barn' ... 
% );

%--------------------------
% LIBRARIES
%--------------------------

% TODO: make the layout or the interaction more intuitive

if ~new

	control(end + 1) = control_create( ...
		'name', 'user_libraries', ...
		'alias', 'current', ...
		'style', 'listbox', ...
		'string', library_name_list(user), ...
		'width', 0.825, ...
		'value', [], ...
		'tab', tabs{3}, ...
		'lines', 3, ...
		'min', 1, ...
		'max', 3 ...
	);

	offset = 4; control(end).space = -(offset + 1);
	
	control(end + 1) = control_create( ...
		'style', 'buttongroup', ...
		'name', 'rem', ...
		'alias', 'Del', ...
		'space', offset + 0.5, ...
		'width', 0.15, ...
		'align', 'right', ...
		'tab', tabs{3}, ...
		'lines', 1.5 ...
	);

	control(end + 1) = control_create( ...
		'name', 'available_libraries', ...
		'alias', 'available', ...
		'style', 'listbox', ...
		'width', 0.825, ...
		'lines', 3, ...
		'string', library_name_list(get_users, 'full'), ...
		'value', [], ...
		'onload', 1, ...
		'tab', tabs{3}, ...
		'min', 1, ...
		'max', 3 ...
	);

	offset = 4; control(end).space = -(offset + 1);
	
	control(end + 1) = control_create( ...
		'style', 'buttongroup', ...
		'name', 'add', ...
		'space', offset, ...
		'width', 0.15, ...
		'align', 'right', ...
		'tab', tabs{3} ...
	);

	control(end + 1) = control_create( ...
		'style', 'buttongroup', ...
		'name', 'locate ...', ...
		'width', 0.825, ...
		'tab', tabs{3}, ...
		'space', 1.5 ...
	);

end

