function user = set_user_preference(user, field, value)

% set_user_preference - set user preference
% -----------------------------------------
%
% user = set_user_preference(user, field, value)
%
% Input:
% ------
%  user - user
%  field - preference name
%  value - preference value
%
% Output:
% -------
%  user - updated user

%--
% handle input
%--

if nargin < 1 || isempty(user)
	user = get_active_user;
end

if nargin < 2
	return; 
end

%--
% set user preference
%--

if ~isvarname(field) 
	field = genvarname(field);
end

change = ~isfield(user.prefs, field) || ~isequal(user.prefs.(field), value);

if ~change
	return;
end

user.prefs.(field) = value;

%--
% save user and update active user if needed
%--
	
user_save(user);

if is_active_user(user)
	set_active_user(user);
end

%--
% perform updates to reflect state
%--

% TODO: this is like a callback for a user preference, develop a mini-framework for it 

switch field
	
	case 'new_log_format'
		
		%--
		% get and loop over open browsers
		%--
		
		par = get_xbat_figs('type', 'sound');
		
		for k = 1:length(par)
			
			%--
			% get parent log palette
			%--
			
			% TODO: move this to 'get_palette' and possibly to 'get_xbat_figs', this handles even orphaned palettes efficiently
			
			pals = get_xbat_figs('parent', par(k), 'type', 'palette'); pal = [];
			
			for l = 1:length(pals)
				
				tag = get(pals(l), 'tag');
				
				if isempty(strfind(tag, '::CORE::Log'))
					continue;
				end

				pal = pals(l); break;
				
			end
			
			%--
			% update palette if needed
			%--
			
			if isempty(pal)
				continue;
			end
			
			set_control(pal, 'new_log_format', 'value', user.prefs.new_log_format);
			
		end
		
end


