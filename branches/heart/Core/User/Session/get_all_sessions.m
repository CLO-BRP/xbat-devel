function sessions = get_all_sessions(varargin)

% get_all_sessions - get all sessions for a user
% ----------------------------------------------
%
% sessions = get_all_sessions(user)
%
% Input:
% ------
%  user - user (def: active user)
%
% Output:
% -------
%  sessions - all session names

sessions = get_sessions('', varargin{:});