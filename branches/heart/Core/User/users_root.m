function root = users_root(parent, create)

% users_root - get all users root directory
% -----------------------------------------
%
% root = users_root(parent, create)
%
% Input:
% ------
%  parent - parent (def: app_root)
%  create - option to create root if it does not exist (def: 0)
%
% Output:
% -------
%  root - users root

%--
% handle input
%--

if nargin < 2 
	create = 0;
end

if ~nargin || isempty(parent)
	parent = app_root;
end

% if ~exist_dir(parent)
% 	error('Users parent does not seem to exist.');
% end

%--
% get users root
%--

root = [parent, filesep, 'Users'];

if ~create
	return;
end
	
% TODO: make this a set and get to allow other users roots

root = create_dir(root);

if ~exist_dir(root)
	error('Failed to create users root.');
end
	


