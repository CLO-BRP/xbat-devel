function flag = add_sounds(sound, lib, opt, copy)

% add_sounds - add sound to library
% --------------------------------
%
% flag = add_sounds(sound, lib)
%
% Input:
% ------
%  sound - sound to add system
%  lib - library to add sounds to (def: active library)
%
% Output:
% -------
%  flag - save confirmation flag

%-----------------------------------
% HANDLE INPUT
%-----------------------------------

if nargin < 4
    copy = 0;
end

%--
% set default option to refresh
%--

if nargin < 3 || isempty(opt)
	opt = 1;
end

%--
% set library if needed
%--

if (nargin < 2) || isempty(lib)
	lib = get_active_library;
end

%--
% handle multiple sounds recursively
%--

if length(sound) > 1
	
	h = add_sounds_wait(lib); count = numel(sound); flag = zeros(size(sound));
	
	for k = 1:count
		
		% NOTE: we try to add sound to library and update the waitbar
		
		flag(k) = add_sounds(sound(k), lib, 0, copy);
		
		%--
		% update waitbar
		%--

		if flag(k)
			msg = ''' added to ''';
		else
			msg = ''' already exists in ''';
		end
		
		% NOTE: if waitbar has been deleted, just return canceling the add
		
		try
			waitbar_update(h, 'PROGRESS', ...
				'value', k/count, 'message', ['''', sound_name(sound(k)), msg, lib.name, ''''] ...
			);
		catch
			return;
		end
	end

	%--
	% report sounds added
	%--
		
	str = [int2str(sum(flag == 1)), ' sounds added to ''', lib.name, ''' library'];
	
	waitbar_update(h, 'PROGRESS', ...
		'value', 1, 'message', str ...
	);

	%--
	% refresh library cache and delete waitbar
	%--
	
	get_library_sounds(lib, 'refresh');
	
	pause(0.5); delete(h); return;
end

%-----------------------------------
% ADD SOUND TO LIBRARY
%-----------------------------------
	
%--
% get sound name
%--

name = sound_name(sound);

%--
% return if sound seems to exist
%--

% NOTE: return if sound seems to exist

if exist([lib.path, name], 'dir')
	flag = 0; return;
end

%--
% create required directories
%--

% NOTE: create all by creating the deepest nested directory

% TODO: this will create spurious directiories in the case of sounds with periods in their name, this stems from the use of 'fileparts'!

backup = create_dir([lib.path, filesep, name, filesep, 'Logs', filesep, '__BACKUP']);

if isempty(backup)
	error('Failed to create required library directories.');
end

% TODO: create '__XBAT' directory if needed, otherwise create shortcut to it in the library sound


% NOTE: hide backup on PC, consider making this is an environment variable

% if (ispc)
% 	fileattrib(backup,'+h');
% end

%--
% copy data or create sound shortcut
%--

if copy
	sound = consolidate_sound(sound, lib); 
else
    add_sound_shortcut(lib, sound);
end

%--
% save sound
%--

sound_save(lib, sound, [], opt);

% NOTE: add sound to local barn if available

if ~trivial(local_barn)
	
	set_barn_sound(local_barn, sound);
	
	% TODO: the files have to moved to the uploaded or put in an accesible place that we point to
	
	% NOTE: creating the latter space would allow us to scan the location for new sounds
end

%--
% set addition indicator
%--

flag = 1;
	

%------------------------------------------------------------------------
% ADD_SOUNDS_WAIT
%------------------------------------------------------------------------

function wait = add_sounds_wait(lib)

% add_sounds_wait - create add sounds waitbar
% -------------------------------------------
%
% wait = add_sounds_wait(lib)
%
% Input:
% ------
%  lib - structure

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', ['Add Sounds to ''', lib.name,''' ...'], ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'lines', 1.1, ...
	'space', 0.5 ...
);

wait = waitbar_group('Add Folder ...', control);
