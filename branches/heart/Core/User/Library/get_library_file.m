function file = get_library_file(varargin)

% get_library_file - get library file name
% ----------------------------------------
%
% file = get_library_file(lib)
%
%      = get_library_file(root, name)
%
% Input:
% ------
%  lib - library
%  root - parent directory
%  name - library name
%
% Output:
% -------
%  file - library file

%--
% compute depending on number of arguments
%--

switch nargin
	
	%--
	% build library file name from library
	%--
	
	case 1
		
		% NOTE: the library path should not include the trailing filesep, but it does

		lib = varargin{1};
		
		file = [lib.path, lib.name, '.mat'];
	
	%--
	% get library file from path and name information
	%--
		
	case (2)
		
		%--
		% handle input
		%--
		
		root = varargin{1}; name = varargin{2};
	
		if ~proper_filename(name)
			error(['Input name ''', name, ''' is not proper filename.']);
		end
		
		%--
		% build library filename
		%--
		
		file = [root, filesep, name, filesep, name, '.mat'];
		
		% NOTE: is this better than the line above?
		
% 		file = fullfile(root, name, [name, '.mat']);
			
	%--
	% error
	%--
	
	otherwise, error('Input must be library, or parent directory and name.');
		
end