function [libs, user] = get_libraries(user, varargin)

% get_libraries - get and select libraries from user
% --------------------------------------------------
%
% libs = get_libraries(user)
%
%      = get_libraries(user, 'field', value, ...)
%
% Input:
% ------
%  user - input user (def: active user)
%  field - library field name
%  value - library field value
%
% Output:
% -------
%  libs - selected libraries

% TODO: libraries should be cached, do they have any fast changing content?

%------------------------------------
% HANDLE INPUT
%------------------------------------

%--
% set default active user
%--

if (nargin < 1) || isempty(user)
	user = get_active_user;
end

active = isequal(user, get_active_user);

% NOTE: return quickly on empty user

if isempty(user)
	libs = []; return;
end

%------------------------------------
% GET USER LIBRARIES
%------------------------------------

%--
% get library paths from user
%--

lib_path = user.library;

% NOTE: return quickly on no library paths

if isempty(lib_path)
	libs = []; return;
end

%--
% get libraries from paths
%--

libs = {}; update = 0;

for k = 1:numel(lib_path)
	
	%--
	% load library if possible
	%--
		
	[root, name] = path_parts(lib_path{k});
	
	file = get_library_file(root, name); 
		
	lib = load_library(file);
				
	%--
	% try to find missing libraries
	%--
	
	if isempty(lib)	
		
		str = {...
			['Unable to find library.''' name ''', '], ...
			'Would you like to find it?' ...
		};

		out = quest_dialog(double_space(str),' Missing Library','Cancel');
		
		if strcmp(out, 'Yes')
			[lib, user] = library_relocate(user, lib_path{k}); update = 1;
		end	
			
	end	

	libs{end + 1} = lib;	
	
end

libs = [libs{:}];

% TODO: check if library content changed, use this evaluate 'update'

user.library = {libs.path};

%--
% update user if needed
%--

if update
	
	user_save(user);

	if active
		set_env('xbat_user', user);
	end

end

%--
% return if there are no selection criteria
%--

if isempty(varargin)
	return;
end

%---------------------------------------------------------------
% SELECT FROM AVAILABLE LIBRARIES
%---------------------------------------------------------------

%--
% extract selection field value pairs
%--

[field, value] = get_field_value(varargin);

%--
% loop over selection fields
%--

for j = 1:length(field)
	
	%--
	% use field if it is in fact a library field
	%--
	
	if isfield(libs(1), field{j})
		
		%--
		% check library fields for value match
		%--
		
		for k = length(libs):-1:1
			
			if ~isequal(libs(k).(field{j}), value{j})
				libs(k) = [];
			end
			
		end
		
	end
end
