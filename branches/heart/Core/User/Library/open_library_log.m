function open_library_log(log, opt, lib)

% open_library_log - open a log in a library
% ------------------------------------------
%
% open_library_log(log, opt, lib)
%
% Input:
% ------
%  log - log or name
%  opt - whether to open in new window (def: 0)
%  lib - library (def: gal)

%--
% set default active library 
%--

if nargin < 3
    lib = get_active_library;
end

%--
% set new window option
%--

% TODO: reinstate this, turned off due to an XBAT window management bug

if (nargin < 2) || isempty(opt)
	opt = 0;
end

%--
% get log from name if necesssary
%--

if ischar(log)
    log = get_log_from_name(log, lib);
end

%--
% bring browser to front if it's open already
%--

par = log_is_open(log);

if ~isempty(par), figure(par); return; end
	
%--
% open log sound in new browser
%--

par = open_library_sound(sound_name(log.sound), lib, opt);

log_open(par, log);
    

