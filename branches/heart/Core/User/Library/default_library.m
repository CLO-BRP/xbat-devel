function lib = default_library(user)

% default_library - defines default library location
% --------------------------------------------------
%
% lib = default_library(user)
%
% Output:
% -------
%  lib - default library

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% return if xbat root is not defined
%--

root = app_root;

%--
% get or set user if needed
%--

if ((nargin < 1) || isempty(user))
	user = get_active_user;
end

%--
% get or create default library
%--

if (isempty(user.default))

	%--
	% set default default library
	%--
	
	fs = filesep;
	
	lib = library_create( ...
		'id',0, ...
		'name','Default', ...
		'path',[root 'Users' fs user.name fs 'Default' fs], ...
		'author',user.name ...
	);

else
	
	%--
	% user has a set default library
	%--
	
	lib = user.library(user.default);
	
end
