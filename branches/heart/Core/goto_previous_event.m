function [prev, event, log] = goto_previous_event(varargin)

[prev, event, log] = goto_closest_event(-1, varargin{:});