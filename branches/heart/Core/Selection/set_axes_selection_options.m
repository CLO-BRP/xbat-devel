function opt = set_axes_selection_options(ax, opt)

% set_axes_selection_options - set default selection options for axes
% -------------------------------------------------------------------
%
% opt = set_axes_selection_options(ax)
%
% Input:
% ------
%  ax - axes handles
%
% Output:
% -------
%  opt - selection options set

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

if (nargin < 2) || isempty(opt)
	opt = selection_axes;
end

selection_axes(ax, opt.name, opt);

refresh_axes_selections(ax);
