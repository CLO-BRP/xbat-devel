function [whois, response] = get_whois(domain)

% get_whois - information 
% -----------------------
%
% [whois, response] = get_whois(domain)
%
% Input:
% ------
%  domain - name
%
% Output:
% -------
%  whois - info
%  response - from service
% 

% TODO: add caching

%--
% handle input
%--

% known = {'net', 'poc', 'org', 'asn', 'customer'};
% 
% if ~string_is_member(resource, known)
% 	error(['Unknown ARIN resource type ''', resource, '''.']);
% end

%--
% get date from service
%--

% NOTE: this is for a much older service 

% OLD
%-------------------------------------

% url = ['http://ws.arin.net/whois/?queryinput=', ip]; % 128.253.113.118

% CURRENT, not the same
%-------------------------------------

% NOTE: look at https://www.arin.net/resources/whoisrws/whois_api.html#whoisrws
%
% url = ['http://whois.arin.net/rest/', resource];
% 	
% if nargin > 1 && ~isempty(query)
% 	url = [url, '/', query];
% end
% 
% url = [url, '.txt'];

response = curl_rest('get', url);

%--
% get relevant lines from response content
%--

lines = {}; state = 0;

for k = 1:numel(response.content.lines)
	
	current = strtrim(response.content.lines{k});
	
	if state
		if strcmpi(current, '</pre>')
			break;
		else
			if ~isempty(current) && current(1) ~= '#'
				lines{end + 1} = current;
			end
		end
	else
		state = strcmpi(current, '<pre>');
	end
end

%--
% parse lines to get whois info struct
%--

% NOTE: the names used in this format are proper field names

% NOTE: the format may have multiple entries per field, these appear on separate lines

whois = struct;

for k = 1:numel(lines)
	% TODO: the split function should handle a single split!
	
	disp(lines{k})
	
	part = str_split(lines{k}, ':'); 
	
	field = part{1}; 
	
	if numel(part) < 2
		part{2} = '';
	else
		value = part{2};
	end
	
	% NOTE: handle field creation, first append to field, and further appends
	
	if ~isfield(whois, field)
		
		whois.(field) = value;	
	else	
		if ischar(whois.(field))
			whois.(field) = {whois.(field), value};
		else
			whois.(field){end + 1} = value;
		end	
	end
end