function url = build_yahoo_map_url(opt)

%--
% set service base url
%--

base = 'http://local.yahooapis.com/MapsService/V1/mapImage';

%--
% build request part of url using options struct
%--

fields = fieldnames(opt);

% TODO: add some code to remove non-API fields, this will impact the 'skip_cache' clause

part = {};

for k = 1:numel(fields)
	
	value = opt.(fields{k});
	
	if isempty(value) || strcmp(fields{k}, 'skip_cache')
		continue;
	end
	
	if ~ischar(value)
		if value == round(value)
			value = int2str(value);
		else
			value = num2str(value, 20);
		end
	end
	
	% NOTE: this replaces any number of spaces with a single plus sign
	
	value = str_implode(strread(value, '%s', -1), '+');
	
	part{end + 1} = [fields{k}, '=' value];
	
end

%--
% build url
%--

url = [base, '?', str_implode(part, '&')];

