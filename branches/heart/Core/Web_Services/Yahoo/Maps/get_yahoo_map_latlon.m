function [file, result, request] = get_yahoo_map_latlon(lat, lon, radius, opt)

% get_yahoo_map_latlon - get map from yahoo map service by lat-lon
% ---------------------------------------------------------
%
% [file, result, request] = get_yahoo_map_latlon(lat, lon, radius, opt)
%
% Input:
% ------
%  lat - latitude 
%  lon - longitude
%  radius - display radius in miles
%  opt - map request options
%
% Output:
% -------
%  file - map image file
%  result - url from result of map request
%  request - request url

%--
% handle input
%--

if nargin < 4
	opt = yahoo_map_opt;
end

% NOTE: we store all the inputs into an options struct

if exist('lat', 'var')
	opt.latitude = lat; 
end

if exist('lon', 'var')
	opt.longitude = lon;
end

if exist('radius', 'var');
	opt.radius = radius;
end

%--
% get file name and check cache
%--

file = get_map_file(lat, lon, radius, opt);

if exist(file, 'file') && opt.skip_cache
	result = ''; request = ''; return;
end 

%--
% get map image file from yahoo
%--

request = build_yahoo_map_url(opt);

% TODO: add exception handling to network requests

result = parse_yahoo_result(urlread(request));

urlwrite(result, file);

