function field = header_field(header)

field = lower(strrep(header, '-', '_'));
