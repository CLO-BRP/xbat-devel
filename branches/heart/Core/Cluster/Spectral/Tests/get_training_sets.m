function sets = get_training_sets(logs)

% get_training_sets
% -----------------

%--
% get available species tags
%--

% NOTE: we should consider the thrushes separately

tags = get_log_tags(logs);

if isempty(tags)
	disp(' '); error('Tags are required for the extraction of training sets.'); 
end

len = iterate(@length, tags);

tags(len ~= 4) = [];

%--
% build sets by performing queries
%--

% NOTE: we are discarding the coincidence information, right now we don't have such anyway

disp(' ');

for k = 1:numel(tags)
	
	sets(k).tag = tags{k}; 
	
	[sets(k).event, ignore, sets(k).log] = get_events_by_tag(logs, tags{k});
	
	disp([sets(k).tag, ' samples ', int2str(count_examples(sets(k)))]);

end


%--------------------------
% COUNT_EXAMPLES
%--------------------------

% TODO: this function is not working for the case of multiple logs

function count = count_examples(set)

% set.event

if iscell(set.event)
	count = numel(set.event{:});
else
	count = numel(set.event);
end

