function event_menu_bdfun(obj, eventdata, mode, par, log, id) %#ok<INUSL>

% event_menu_bdfun - create event menu upon request
% -------------------------------------------------
%
% event_menu_bdfun(obj, eventdata, 'sound', par, m, ix)
%
% event_menu_bdfun(obj, eventdata, 'log', par, m, ix)
%
% Input:
% ------
%  obj - callback object
%  eventdata - not used at the moment
%  par - parent browser handle
%  m - log index in sound browser
%  ix - event index in log


switch mode
	
	%------------------------
	% SOUND BROWSER
	%------------------------
	
	case 'sound'

		%--
		% create contextual menu if needed
		%--

		if isempty(get(obj, 'uicontextmenu'))
						
			%--
			% create contextual menu, tag, and attach to current event display
			%--
			
			top = uicontextmenu( ...
				'parent', par, 'tag', event_tag(id, log) ...
			);
			
			set(obj, 'uicontextmenu', top);
			
			%--
			% create event menu
			%--
			
			event_menu(top, 'Initialize', par, log, id);
			
			% DEBUG: display menu tags for children of context-menu
			
% 			iterate(@disp, get(findobj(top, 'type', 'uimenu'), 'tag'))
			
		end
		
		%--
		% execute button down function for event display
		%--
		
		% NOTE: the last argument asks for event palette update ??
		
		event_bdfun(par, log, id);
		
		%--
		% play on double click
		%--
		
		double_click_play(obj, []);
		
	%------------------------
	% LOG BROWSER
	%------------------------
	
	% TODO: this code may be broken now that the signature has changed
	
	case 'log'
				
		%--
		% execute button down function for event display
		%--
		
		event_bdfun;
		
		%--
		% create contextual menu if needed
		%--
		
		if isempty(get(obj,'uicontextmenu'))
		
			%--
			% create contextual menu, tag, and attach to current event display
			%--
			
			c = uicontextmenu;
			
			set(c, ...
				'tag', event_tag(id, log) ...
			);
		
			set(obj, 'uicontextmenu', c);
			
			%--
			% create event menu
			%--
			
			event_menu(c,'','',log,id);
			
		end
		
end


%------------------------
% DOUBLE_CLICK_PLAY
%------------------------

function double_click_play(obj, eventdata)

% double_click_play - play selection on double click
% --------------------------------------------------

if double_click(obj)
	
	browser_sound_menu(gcbf, 'Play Event');

end
