function handle = gab

% gab - get active browser handle
% -------------------------------
%
% handle = gab
%
% Output:
% -------
%  handle - active browser

handle = get_active_browser;