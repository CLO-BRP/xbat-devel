function [page, sound] = get_browser_page(par, data)

% get_browser_page - get standard page struct from browser
% --------------------------------------------------------
%  
% page = get_browser_page(par, data)
%
% Input:
% ------
%  par - browser handle
%  data - browser data
%
% Output:
% -------
%  page - the page

%-------------------
% HANDLE INPUT
%-------------------

%--
% get active browser
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

% NOTE: return trivial page if not browser is available

if isempty(par)
	page = struct; return;
end

%--
% get data from browser
%--

if nargin < 2 || isempty(data)
	data = get_browser(par);
end

%-------------------
% GET PAGE
%-------------------

%--
% populate page from browser
%--

slider = get_time_slider(par);

page.start = slider.value;

page.duration = get_browser(par, 'page__duration', data);

page.channel = get_channels(data.browser.channels);

% TODO: consider adding channels to this

if nargout > 1
	sound = data.browser.sound;
end


