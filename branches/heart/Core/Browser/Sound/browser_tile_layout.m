function [layout, tile] = browser_tile_layout(status, color, slider)

% browser_layout - give browser layout sizes in tiles
% ---------------------------------------------------
%
% [layout, tile] = browser_tile_layout(status, color, slider)
%
% Inputs:
% -------
% status - status bar indicator
% color - color bar indicator
% slider - slider indicator
%
% Outputs:
% --------
% layout - layout structure
% tile - tile size in pixels

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1935 $
% $Date: 2005-10-14 16:58:12 -0400 (Fri, 14 Oct 2005) $
%--------------------------------

%-------------------
% HANDLE INPUT
%-------------------

if nargin < 3
	slider = 1;
end

%-------------------
% GET PARAMETERS
%-------------------

param = get_palette_settings;

%-------------------
% CREATE LAYOUT
%-------------------

tile = param.tilesize * 1.2; 

%--
% axes array spacing
%--

% NOTE: these are in tile units

layout.yspace = 0.5;

layout.xspace = 0.5;

layout.top = 2;

layout.left = 2.5;

layout.right = 1.5;

%--
% modify margins based on layout elements
%--

layout.bottom = 3 + status + slider;

layout.colorbar = 2.5 * color;

