function set_browser_colormap(par, data)

% set_browser_colormap - set browser colormap
% -------------------------------------------
%
% set_browser_colormap(par, data)
%
% Input:
% ------
%  par - browser
%  data - browser state

%-------------------
% HANDLE INPUT
%-------------------

%--
% get active browser
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

if isempty(par)
	return;
end

%--
% get parent state
%--

if (nargin < 2) || isempty(data)
	data = get_browser(par);
end
	
%-------------------
% SETUP
%-------------------

%--
% get colormap state from browser
%--

map = data.browser.colormap;

%-------------------
% UPDATE COLORMAP
%-------------------

switch str

	%--
	% label colormap
	%--

	case 'label', cmap_label;

	%--
	% contrast and scale maps
	%--

	otherwise

		if map.auto_scale

			%--
			% create base map
			%--

			C = feval(colormap_to_fun(str), 256);

			if map.invert
				C = flipud(C);
			end

			%--
			% set map
			%--

			set(par, 'colormap', C);

			%--
			% automatically scale map
			%--

			% NOTE: this should happen before not after set the colormap currently this is the way we pass the data

			cmap_scale([], data.browser.images);

		else

			C = brightness_contrast_map( ...
				colormap_to_fun(str), map.brightness, map.contrast, map.invert, 256 ...
			);

			set(par, 'colormap', C);

		end

end

%--
% update menu
%--

if ~par_flag

	% NOTE: this menu handle storage will be eliminated

	% NOTE: this should be done with tagging

	set(data.browser.view_menu.colormap, 'check', 'off');

	set(get_menu(data.browser.view_menu.colormap,str), 'check', 'on');

end

%--
% update palette if needed
%--

pal = get_palette(par, 'Colormap', data);

if ~isempty(pal)

	set_control(pal, 'Colormap', 'value', str);

	set(pal, 'colormap', feval(colormap_to_fun(str), 256));

end
	
	
