function [selection, count] = get_active_selection

% get_active_selection - from active browser
% ------------------------------------------
%
% [selection, count] = get_browser_selection;
%
% NOTE: this is meant for command-line use
%
% See also: get_browser_selection, get_active_browser

par = get_active_browser;

[selection, count] = get_browser_selection(par);

sound = get_browser(par, 'sound');

for k = 1:count
	selection.event(k) = get_event_samples(selection.event(k), sound);
end