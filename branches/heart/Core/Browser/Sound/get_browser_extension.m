function [ext, ix, context] = get_browser_extension(type, par, name, data, fast)

% get_browser_extension - get extension with up to date browser state
% -------------------------------------------------------------------
%
% [ext, ix, context] = get_browser_extension(type, par, identifier, data)
%
% Input:
% ------
%  type - extension type
%  par - parent browser
%  identifier - name, fieldname, or guid
%  data - browser state
%
% Output:
% -------
%  ext - extension
%  ix - index of extension in browser store
%  context - context used

% db_disp; stack_disp;

%-------------------------
% HANDLE INPUT
%-------------------------

if nargin < 5
	fast = 0;
end

%--
% try to get browser
%--

% TODO: do we get the root browser by leaving the parent empty? evaluate this carefully

if isempty(par)
	par = get_active_browser;
end

%--
% check browser and get state if needed
%--

if ~is_browser(par)
	error('Input is not browser handle.');
end

% NOTE: the data will not be needed for the root browser

if (nargin < 4) || (par && isempty(data))
	data = get_browser(par);
end

%--
% check extension type and existence of browser store for type
%--

if ~is_extension_type(type)
	error(['Unrecognized extension type ''', type, '''.']);
end

% TODO: reconsider the statement below, this only when the browser state persists over an update that adds extensions

% NOTE: we set empty outputs because we do not want to throw errors here

ext = []; ix = []; context = [];

if par

	if ~isfield(data.browser, type)
		return;
	end

	if isempty(data.browser.(type).ext)
		return;
	end
	
end

%-------------------------
% GET EXTENSION
%-------------------------

%--
% get extension from browser or system extension cache by type and name, fieldname, or guid
%--

if par	
	ix = find(strcmp(name, {data.browser.(type).ext.name}));

	if isempty(ix)
		ix = find(strcmp(name, {data.browser.(type).ext.fieldname}));
	end
	
	if isempty(ix)
		ix = find(strcmp(name, {data.browser.(type).ext.guid}));
	end
	
	if isempty(ix)
		ext = [];
	else
		ext = data.browser.(type).ext(ix);
	end
else 
	
	ext = get_extensions(type, 'name', name);
	
	if isempty(ext)
		ext = get_extensions(type, 'fieldname', name);
	end
	
	if isempty(ext)
		ext = get_extensions(type, 'guid', name);
	end
end

% NOTE: we return fast here if no context output was requested

if isempty(ext) || (fast && nargout < 3)
	return;
end

%--
% get initial extension context, palette handle and control values
%--

% NOTE: the context is used by various extension methods to do their jobs

context = get_extension_context(ext, par, data);

pal = get_extension_palette(ext, par);

context.pal = pal;

if ~isempty(pal)

	ext.control = get_control_values(pal);

	% NOTE: if we have a debug control, it will be used to set the context

	if isfield(ext.control, 'debug')
		context.debug = ext.control.debug;
	end

end

% NOTE: it may not be worthwhile to do this here

if has_widget(ext)	
	context.widget = find_widget(par, ext);
end
	
% NOTE: we return fast here saving the creation and compilation of parameters for the extension

% NOTE: the following code considers updates due to parameter or context changes, in the fast case these are missed

if fast 
	return; 
end

%--
% get current compiled parameters for all required types
%--

[value, types] = has_any_parameters(ext);

for k = 1:length(types)
	
	%--
	% get type, parameter function branch, and currently stored parameters
	%--
	
	type = types{k};

	if strcmp(type, 'compute')
		fun = ext.fun.parameter; parameter = ext.parameter;
	else
		fun = ext.fun.(type).parameter; parameter = ext.parameters.(type);
	end 
	
	% NOTE: in what follows we do what we need to get current parameters

	%--
	% we have non-trivial parameters and no palette to update them with, compile with fresh context
	%--
	
	if ~trivial(parameter) && isempty(pal)

		% NOTE: if we compile, we compile to ensure fresh context

		if ~isempty(fun.compile)

			try
				[parameter, context] = fun.compile(parameter, context);
			catch
				extension_warning(ext, warning_message(type, 'compilation'), lasterror);
			end

		end

		% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
		if strcmp(type, 'compute')
			ext.parameter = parameter; ext.parameters.(type) = parameter;
		else
			ext.parameters.(type) = parameter;
		end

		continue;

	end

	%--
	% we have trivial parameters, create default parameters given context
	%--

	if trivial(parameter)

		try
			parameter = fun.create(context);
		catch
			extension_warning(ext, warning_message(type, 'creation'), lasterror); continue;
		end
		
		% NOTE: trivial parameters need no update, we assume they need no compilation

		if trivial(parameter)
			continue;
		end

	end

	%--
	% update parameters from matching controls if we have a palette
	%--
	
	% NOTE: this may fail if we have name conflicts

	if ~isempty(pal)

		% NOTE: update must be configured to be shallow

		opt = struct_update; opt.flatten = 0;
		
		parameter = struct_update(parameter, ext.control, opt);
		
% 		db_disp; stack_disp; parameter, ext.control

	end

	%--
	% compile current parameters if needed
	%--
	
	if ~isempty(fun.compile)

		try
			[parameter, context] = fun.compile(parameter, context);
		catch
			extension_warning(ext, warning_message(type, 'compilation'), lasterror);
		end

	end
	
	%--
	% store current compiled parameters of type in extension
	%--
	
	% NOTE: we have duplicate 'compute' parameters for backward-compatibility
		
	if strcmp(type, 'compute')
		ext.parameter = parameter; ext.parameters.(type) = parameter;
	else
		ext.parameters.(type) = parameter;
	end

end

%--
% put current extension in context
%--

context.ext = ext;


%------------------------
% WARNING_MESSAGE
%------------------------

function str = warning_message(type, failed)

str = ['''', title_caps(type), ''' parameter $API failed.'];

str = strrep(str, '$API', failed);
