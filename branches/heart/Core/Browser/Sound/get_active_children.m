function child = get_active_children(par, ext, data)

% get_active_children - get active children extensions
% ----------------------------------------------------
%
% child = get_active_children(par, ext, data) 
%
% Input:
% ------
%  par - browser
%  ext - parent
%  data - browser state
%
% Output:
% -------
%  child - active

%--
% get browser state
%--

if nargin < 3
	data = get_browser(par);
end

%--
% get children and active extensions
%--

child = get_extension_descendants(ext);

if isempty(child)
	return; 
end

type = unique({child.subtype});

active = [];

% NOTE: the 'fast' flag helps with speed and solves a recursion problem

for k = 1:numel(type)
	active = [active, get_active_extension(type{k}, par, data, 1)];
end

if isempty(active)
	return;
end

%--
% select active children
%--

% NOTE: this is an intersection of the sets using a string signature

child = child(string_is_member(sig(child), sig(active)));


%--------------------
% SIG
%--------------------

function str = sig(ext)

str = strcat({ext.subtype}, {'::'}, {ext.name})';


	