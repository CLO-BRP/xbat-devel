function out = browser_feature_menu(par, str)

% browser_feature_menu - create menus for features extensions
% ---------------------------------------------------------
%
% out = browser_feature_menu(par, str)
%
% Input:
% ------
%  par - browser handle
%  str - command string
% 
% Output:
% -------
%  out - command dependent output

%--
% get extensions
%--

ext = get_extensions('sound_feature');

if isempty(ext)
	return;
end

%--
% create top menu and extensions category menu
%--

top = uimenu(par, 'label', 'Feature');  set(top, 'position', 6);

out = extension_category_menu(top, ext);
