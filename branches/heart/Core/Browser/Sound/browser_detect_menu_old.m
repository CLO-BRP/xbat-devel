function browser_detect_menu(h)

% browser_detect_menu - load menus of available detectors
% -------------------------------------------------------
%
% browser_detect_menu(h)
%
% Input:
% ------
%  h - handle to browser figure

%--
% set figure
%--

if (nargin < 1)
	h = gcf;
end

%--
% check for existing detector menu
%--

if (get_menu(h,'Detect'))
	return;
end

%--
% get menu functions for available detectors
%--

fun = what('detectors'); 

if (isempty(fun.m))
	fun = fun.p;
else
	fun = fun.m;
end

j = 1;
for k = 1:length(fun)
	if findstr(fun{k},'_menu')
		tmp = findstr(fun{k},'.');
		menu{j} = fun{k}(1:tmp(end) - 1);
		j = j + 1;
	end
end

%--
% Create parent
%--

tmp = uimenu(h,'label','Detect');

set(tmp,'position',5);

if (exist('menu','var'))
	for j = 1:length(menu)
		if (~isempty(menu{j}))
			feval(menu{j},h,'Initialize');
		end
	end
end

