function [cursor, handles] = set_cursor(par, eventdata, opt)

cursor = []; opt = [];

%--
% make sure we have a proper parent
%--

if nargin < 1
	par = get_active_browser;
end

if isempty(par)
	return;
end

if isempty(get(par, 'windowbuttonmotionfcn'))
	set(par, 'windowbuttonmotionfcn', {@set_cursor, opt});
end

%--
% check we are hovering over axes in parent
%--

over = overobj('axes');

if isempty(over)
	return;
end

if get(over, 'parent') ~= par
	return;
end

%--
% set cursor
%--

get_axes_position(par, over);

% TODO: the way we set the cursor will depend on cursor options, yet to be defined

handles = draw_cursor(over, cursor);


function pos = get_axes_position(par, ax)

point = get(par, 'currentpoint')

pos = get_size_in(ax, 'pixels', 'pack')

xlim = get(ax, 'xlim')

ylim = get(ax, 'ylim')






