function marker = get_browser_marker(par, data)

if (nargin < 1) || isempty(par)
	par = get_active_browser;
end

if nargin < 2
	data = get_browser(par);
end

marker = data.browser.marker;
