function marker = display_marker(par, ax, time, data)

% display_marker - display marker in time axes
% --------------------------------------------
%
% marker = display_marker(par, ax, time, data)
%
% Input:
% ------
%  par - parent
%  ax - axes
%  time - time
%  data - parent browser data
%
% Output:
% -------
%  marker - marker

% TODO: clean-up and develop further to use in other places, for example to display the play line

% NOTE: to achieve the above we need tagging and display configuration

%--
% handle input
%--

if nargin < 4
	data = [];
end

color = [0 0.6 0.1];

%--
% consider GUI problems
%--

% NOTE: double click was causing problems during capture, it was probably put in to resolve some earlier problem?

if isempty(time) % || double_click(ax)
	marker = []; return;
end 

%--
% create or update display
%--

ylim = get(ax, 'ylim'); y = [ylim(1), mean(ylim), ylim(2)]; yrange = diff(ylim);

% NOTE: we first try to get the marker handles from the axes, if it fails the figure 

handles = get_marker_handles(ax);

if isempty(handles)
	handles = get_marker_handles(par);
end

if isempty(handles)
	
	%--
	% create marker
	%--
	
	handles(1) = line( ...
		'xdata', time * ones(1, 3), ...
		'ydata', y + yrange * [0.01, 0, -0.01], ...
		'color', color, ...
		'linestyle', '-', ...
		'marker', 's' ...
	);

	%--
	% set callback and create label
	%--

	if (nargin > 3) && ~isempty(data)
		
		set(handles(1), ...
			'buttondownfcn', {@marker_callback, 'start', data} ...
		);

		handles(2) = display_label(ax, time, data, ylim);
		
	end

	set(handles, 'tag', 'marker-handles');
	
else
	
	set(handles(1), ...
		'xdata', time * ones(1, 3), ...
		'ydata', y + yrange * [0.01, 0, -0.01] ...
	);

	if (nargin > 3) && ~isempty(data)

		display_label(handles(2), time, data, ylim);
		
	end
	
end 

set(handles, 'parent', ax);

%--
% pack marker
%--

marker = marker_create;

marker.time = time;

marker.channel = str2double(get(ax, 'tag'));

marker.color = color;

marker.axes = ax;

marker.handles = handles;

%--
% display marker on top of everything
%--

% NOTE: we turn the handle vector into a column, this resolves a bug in earlier MATLAB versions

uistack(marker.handles(:), 'top');


%-----------------------------
% DISPLAY_LABEL
%-----------------------------

% TODO: 'sound_player' should use 'display_label_text', so should the selection time display

function [handle, label] = display_label(handle, time, data, ylim)
				
%--
% get browser time string
%--

% NOTE: context duck to adapt for to signature

context.sound = data.browser.sound; context.display.grid = data.browser.grid;

label = get_browser_time_string([], time, context);

%--
% display marker label text
%--

handle = display_label_text(handle, label, time, ylim);


