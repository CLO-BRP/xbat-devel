function marker = marker_create(varargin)

%--
% initialize marker
%--

marker.time = [];

marker.channel = [];

marker.tags = {};

marker.rating = []; 

marker.notes = struct;

marker.color = [];

% TODO: this field should indicate whether the marker is crosshair-like, this is not implemented

% NOTE: also consider adding custom elements to the browser grid, or in some features

marker.grid = [];

marker.handles = [];

%--
% set marker fields from input
%--

marker = parse_inputs(marker, varargin{:});