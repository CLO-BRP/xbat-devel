function obj = apply_tags(varargin)

% apply_tags - add tags to taggable objects
% -----------------------------------------
%
% obj = apply_tags(obj, tags)
%
% Input:
% ------
%  obj - taggable objects
%  tags - tags to add
%
% Output:
% -------
%  obj - updated tagged objects
%
% NOTE: this is the same as 'add_tags'

obj = update_tags('add', varargin{:});
