function result = mapf(value, range, fun)

% mapf - scalars using parametric function
% ----------------------------------------
%
% result = mapf(value, range, fun, limits)
% 
% Input:
% ------
%  value - to map
%  range - of colors
%  mode - for color interpolation 'hsv' (default) or 'rgb'
%  limits - for input values
%
% Output:
% -------
%  result - colors