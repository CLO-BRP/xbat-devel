value = linspace(0, 1, 64);

ranges = { ...
	[1 0 0; 0 1 0], ... % red to green
	[1 0 0; 0 0 1], ... % red to blue
	[0 1 0; 0 0 1], ... % green to blue
	[0 0 1; 1 1 0] ... % blue to yellow
};

for k = 1:numel(ranges)
	
	range = ranges{k};

	color = mapc(value, range, 'hsv');

	fig; colormap(color); ax = axes; colorbar(ax); title HSV

	color = mapc(value, range, 'rgb');

	fig; colormap(color); ax = axes; colorbar(ax); title RGB

end