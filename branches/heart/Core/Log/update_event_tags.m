function result = update_event_tags(log, event, tag, mode)

% update_event_tags - generate query for
% --------------------------------------
%
% result = update_event_tags(log, event, tag, mode)
%
% Input:
% ------
%  log - log
%  event - list
%  tag - matching
%  mode - 'apply', 'remove', or 'set'
%
% Output:
% -------
%  result - query

%-----------------
% HANDLE INPUT
%-----------------

%--
% set default mode and check mode input
%--

if nargin < 4
	mode = 'set';
end

known = string_is_member(mode, {'apply', 'remove', 'set'});
	
if ~known
	error(['Unrecognized update mode ''', mode, '''.']);
end

%--
% handle tag input
%--

% NOTE: a single tag is wrapped in a cell to indicate tag set

if ischar(tag)
	tag = {tag};
end

%--
% handle multiple event input
%--

if numel(event) > 1
	
	% NOTE: a tag set is wrapped in a cell to indicate tag set collection
	
	if iscellstr(tag)
		tag = {tag}; wrapped = 1;
	end

	%--
	% check that tags match or that we have a single set of tags
	%--
	
	% NOTE: the tag input should be a cell of string cell arrays, we wrap other cases above
	
	if numel(event) ~= numel(tag) && numel(tag) ~= 1
		
		error('Event and tag input must match.');
	
	end
	
	%--
	% collect queries for each update operation and coalesce so that we may use a single transaction
	%--
	
	if numel(tag) == 1
		
		for k = 1:numel(event)
			sql{k} = update_event_tags(log, event(k), tag{1}, mode);
		end
		
	else
		
		for k = 1:numel(event)
			sql{k} = update_event_tags(log, event(k), tag{k}, mode);
		end
		
	end
	
	% NOTE: this removes the transaction 'BEGIN' and 'COMMIT' from every individual update
	
	for k = 1:numel(event)
		
		if ~isempty(sql{k})
			sql{k}([1, end]) = [];
		end
		
	end
	
	sql = aggregate(sql); sql = {'BEGIN;', sql{:}, 'COMMIT;'};
	
	%--
	% output query or actually perform update
	%--
	
	% NOTE: it may not be intuitive that the query may not execute when output is requested 
	
	if nargout 
		result = sql; return;
	end
	
	[status, result] = sqlite(log.store.file, sql); result = status;
	
	return;
	
end

% NOTE: the code below handles a single event

%--
% update event tags
%--

current = get_tags(event); 

% NOTE: if output is requested we generate query but defer evaluation

file = ternary(nargout, '', log.store.file);

user = get_log_user(log); value.user_id = user.id;

result = {};

switch mode
	
	case 'apply'
		
		% NOTE: see that we are actually appending something, update log tags and insert relation
		
		append = setdiff(tag, current);
		
		if isempty(append)
			return;
		end
		
		% NOTE: we update the database tags so that we have tag identifiers
		
		[ignore, ignore, id] = set_database_tags(log.store.file, append);
		
		% NOTE: the updating of the relation may be deferred if we updating many edges
		
		[ignore, result] = insert_relation(file, 'event', event.id, 'tag', id, 'value', value);
		
	case 'remove' 
		
		% NOTE: see that we are actually removing something, update log tags and delete relation
		
		remove = intersect(tag, current);
		
		if isempty(remove)
			return; 
		end
		
		[ignore, id] = get_database_tags(log.store.file, 'in', remove);
		
		% NOTE: the updating of the relation may be deferred if we updating many edges
		
		[ignore, result] = delete_relation(file, 'event', event.id, 'tag', id);
		
	case 'set'
		
		% NOTE: the set operation may incur in both insertion and deletion of relations
		
		remove = setdiff(current, tag);
		
		if ~isempty(remove)
			[ignore, id] = get_database_tags(log.store.file, 'in', remove);

			[ignore, result] = delete_relation(file, 'event', event.id, 'tag', id); result([1, end]) = [];
		end
		
		append = setdiff(tag, current);

		% NOTE: we update the database tags so that we have tag identifiers
		
		if ~isempty(append)
			[ignore, ignore, id] = set_database_tags(log.store.file, append);

			% NOTE: the updating of the relation may be deferred if we updating many edges
			
			[ignore, result2] = insert_relation(file, 'event', event.id, 'tag', id, 'value', value); result2([1, end]) = [];
			
			result = {result{:}, result2{:}};
		end
		
		result = {'BEGIN;', result{:}, 'COMMIT;'};
		
end
		
		
		