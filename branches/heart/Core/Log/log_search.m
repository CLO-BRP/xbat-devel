function id = log_search(log, search)

sql = [ ...
    'SELECT id FROM events WHERE ', ...
    '(SELECT (''', log_name(log), ' # '' || id || '': '' ||', ...
    'IFNULL((round(score*1000)/10 || ''%''), '''') ||', ...
    'IFNULL(tags, '''') ||', ...
    'IFNULL(notes, '''') ||', ...
    '''T = '' || time((time/86400)-0.5)) info) ', ...
    'LIKE ''%', search, '%'';' ...  
];

[status, result] = sqlite(log_cache_file(log), 'prepared', sql);

id = [result(:).id];