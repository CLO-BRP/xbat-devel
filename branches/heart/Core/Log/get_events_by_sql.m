function event = get_events_by_sql(log, sql)

% get_events_by_sql - get events from a log by specifying an SQL query
% --------------------------------------------------------------------
%
% event = get_events_by_sql(log, sql)
%
% input:
% ------
%  log - log 
%  sql - query to execute (must return an id column)

%--
% handle multiple logs with automagical iterator
%--

if numel(log) > 1
    event = iterate(mfilename, log, sql); return;
end

%--
% get ids using query
%--

[status, result] = sqlite(log_cache_file(log), 'prepared', sql);
    
if ~isfield(result, 'id')
    error('SQL query must return a list of ids');
end

%--
% early return if there are no ids
%--

if isempty(result)
    event = empty(event_create); return;
end

%--
% get events from actual log
%--

event = log_get_events(log, [result.id], 0);
