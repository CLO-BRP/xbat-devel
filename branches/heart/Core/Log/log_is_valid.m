function [valid, reason] = log_is_valid(file)

% log_is_valid - check log is valid
% ---------------------------------
%
% [value, reason] = log_is_valid(file)
%
% Input:
% ------
%  file - log file
%
% Output:
% -------
%  value - valid indicator
%  reason - if not valid, reason why

%--
% check log connection file
%--

% NOTE: a valid log file will load a log struct

% TODO: update this to use the 'struct' MAT storage

try
	load(file); log(end + 1) = log_create; valid = 1; reason = [];
catch
	valid = 0; reason = lasterror;
end

%--
% check log data
%--

% TODO: there may be other things we do to ensure validity
