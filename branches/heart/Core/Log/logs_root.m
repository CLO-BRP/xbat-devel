function [root, created] = logs_root(sound, lib)

% logs_root - get root of library sound logs
% ------------------------------------------
%
% [root, created] = logs_root(sound, lib)
%
% Input:
% ------
%  sound - name or sound
%  lib - parent library
%
% Output:
% -------
%  root - directory
%  created - creation indicator

%--
% set active library default and get library if needed
%--

if nargin < 2
    lib = get_active_library;
end

if ischar(lib)
	lib = get_library_from_name(lib);
end

if isempty(lib)
	error('Library is required to compute logs root.');
end

%--
% get sound
%--

if ischar(sound)
    sound = get_library_sounds(lib, 'name', sound);
end

if isempty(sound)
    error('Sound is required to compute logs root.');
end

%--
% get root and ensure it exists
%--

[root, created] = create_dir([sound_root(sound, lib), filesep, 'Logs']);
    

