function tags = get_log_tags(log)

% get_log_tags - get tags from log
% --------------------------------
%
% tags = get_log_tags(log)
%
% Input:
% ------
%  log - logs
%
% Output:
% -------
%  tags - tags known to each log

% TODO: consider other possible options

%--
% handle multiple logs
%--

if numel(log) > 1
	tags = iterate(mfilename, log); return;
end

%--
% dispatch based on format
%--

% TODO: consider a 'log_format' method, it could do more 

switch log.format
	
	case 'BARN'
		% TODO: this actually gets all tags using in BARN, not just a particular log
		
		tags = get_taggings(log.store); tags = {tags.name};
		
	case 'SQLite'
		% NOTE: the first line should not be required
		
		store = log_store(log); 
		
		tags = get_database_tags(store.file);
		
	otherwise
		
		tags = {};
end