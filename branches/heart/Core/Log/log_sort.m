function Y = log_sort(X,field,d)

% log_sort - sort log events using specified field
% ------------------------------------------------
%
%  Y = log_sort(X,field,d)
%
% Input:
% ------
%  X - input log
%  field - event field used for sorting
%  d - sorting order, descending order -1 (def: 1)
%
% Output:
% -------
%  Y - sorted log

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% set sorting order
%--

if (nargin < 3)
	d = 1;
end

%--
% extract specified field
%--

if (isfield(X.event,field))
	F = struct_field(X.event,field);
else
	disp(' ');
	error(['Desired sorting field ''' field ''' is not an event field.']);
end

%--
% sort field and get indices
%--

[F,ix] = sortrows(F);

if (d == -1)
	ix = flipud(ix);
end

%--
% sort events and put in log
%--

Y = X;
Y.event = X.event(ix);