function log = test_db_log

log = log_create_in('db_log', ...
    'path', this_path, ...
    'file', 'test_log.db' ...
);


function s = this_path()

s = fileparts(mfilename('fullpath'));