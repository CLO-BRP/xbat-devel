function event = db_log_get_events(log, varargin)

event = empty(event_create);

if ~strcmp(log.type, 'db_log')
    error('This function only supports db logs.');
end

log_file = fullfile(log.path, log.file);

%--
% build sql string
%--

sql = 'SELECT * FROM events';

select = '';

if numel(varargin)
    
    sql = [sql, ' WHERE '];

    %--
    % build select clause from field/value pairs
    %--   

    [field, value] = get_field_value(varargin{:});

    for k = 1:length(field)

        switch field{k}

            case 'page'
                
                page = value{k};
                
                sql = [sql, 'time > ', num2str(page.start), ' AND time < ', num2str(page.stop)];

            case 'id'

            case 'channel'

            case ''

        end

    end

end

%--
% add selection to sql
%--

if ~isempty(select)
    sql = [sql, select];
end

sql = [sql, ';'];

[status, base_event] = sqlite(log_file, 'prepared', sql);

for k = 1:length(base_event)
    event(k) = struct_update(event_create, base_event(k));
end

%--
% get measurements
%--

sql = 'SELECT * FROM sqlite_master'; 

[status, tables] = sqlite(log_file, 'prepared', sql);

names = {tables.name};

%--
% get measurement table names
%--

names = names(strmatch('measure', names));

for k = 1:length(names)
   
    sql = ['SELECT * FROM ', names{k}];
    
    if ~isempty(select)
        sql = [sql, ' AND ', select];
    end
    
    [status, measure] = sqlite(log_file, 'prepared', sql); 
    
    for j = 1:length(measure)
        
        event_id = measure(j).event_id;
        
        measurement = rmfield(measure(j), 'event_id');
               
        event([event.id] == event_id).measurement(end + 1) = unflatten_struct(measurement);
        
    end
    
    
end





