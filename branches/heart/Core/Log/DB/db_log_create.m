function log = db_log_create(name, sound, lib)

% db_create_log - create sqlite based log
% ---------------------------------------
%
% log = db_log_create(name, sound, lib)
%
% Input:
% ------
%  name - log name
%  sound - parent sound
%  lib - container library
%
% Output:
% -------
%  log - db_log struct

% NOTE: return if there is nothing to do

if ~nargin
	return;
end

%--
% handle input
%--

% TODO: consider which conditions should result in errors

if nargin < 3
	lib = get_active_library;
end 

if isempty(lib)
	return;
end

if nargin < 2
	sound = get_active_sound;
end

if ischar(sound)
	sound = get_library_sound(lib, 'name', sound);
end

if isempty(sound)
	return;
end

if ~proper_filename(name)
	error('Log name must be proper filename.');
end

% NOTE: the name should not have excess spaces

name = strtrim(name);

%--
% get logs root directory
%--

root = fullfile(lib.path, sound_name(sound), 'Logs');

%--
% create events database
%--

file = fullfile(root,  [name, '.db']);

if exist(file, 'file')
    return;
end

%--
% create log 
%--

log = log_create_in('db_log', ...
    'path', root, ...
    'file', file, ...
    'sound', sound ...
);
   
% NOTE: this is the only call to the database layer

% TODO: create a higher level function that creates the full events database

log_save(log);

