function file = log_cache_file(log)

% log_cache_file - create log cache file and db tables
% ----------------------------------------------------
% 
% file = log_cache_file(log)
%
% Input:
% ------
%  log - log
%
% Output:
% -------
%  file - log cache file

%--
% handle multiple logs
%--

if numel(log) > 1
	file = iterate(mfilename, log); return;
end

%--
% get full file location and check for file
%--

file = fullfile(log_path(log), 'cache.db');

if exist(file, 'file')
    return;
end

%--
% create new file
%--

% NOTE: the cache file contains the basic event information for display and navigation

sqlite(file, create_event_table);

store = log.store;

% NOTE: the cache file also contains store information and event 'guid' information, this may allow some kind of sync in the future

sqlite(file, create_table(store)); 

sqlite(file, sqlite_array_insert('store', flatten(store)));

% establish_tags(file, {'event', 'tag'});

%--
% sync file with existing log
%--

log_cache_update(log);
