function [event, partial] = get_log_events(log, id)

% NOTE: the page should contain time and channel information




function [id, position, log] = get_events_by_page(logs, page)

% get_events_by_tag - select events from logs by page
% ---------------------------------------------------
%
% [events, position, log] = get_events_by_tags(logs, page)
%
% Input:
% ------
%  logs - logs
%  page - page
%
% Output:
% -------
%  events - events
%  position - in-page indicator matrix for event boundaries
%  log - log index array

% NOTE: the in-page indicator matrix is binary with event row index and start and stop column indices

% NOTE: when start is not in-page, the event starts before; when stop is not, the event ends after

% NOTE: relate and possibly reconsider 'event_in_page'




function [id, tag, log] = get_events_by_tag(logs, tags)

% get_events_by_tag - select events from logs by tags
% ---------------------------------------------------
%
% [id, tag, log] = get_events_by_tags(logs, tags)
%
% Input:
% ------
%  logs - logs
%  tags - tags
%
% Output:
% -------
%  events - events
%  tag - tag indicator matrix
%  log - log index array

% NOTE: the tag indicator matrix is binary with event row index and tag column index
