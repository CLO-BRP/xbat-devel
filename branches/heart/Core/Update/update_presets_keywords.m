function update_presets_keywords

% update_presets_keywords - add keywords field to extension presets
% -----------------------------------------------------------------

% TODO: add other preset updates to this function and rename

%--
% create current new empty preset
%--

% NOTE: this should contain the keywords field

temp = preset_create;

%--
% get and loop over extensions
%--

ext = get_extensions;

for i = 1:length(ext)
	
	% NOTE: skip over disabled extensions

	if (~ext(i).enable)
		continue;
	end
	
	%--
	% get extension presets location
	%--
	
	info = functions(ext(i).fun.main);
	
	p = [path_parts(info.file), filesep, 'Presets'];
	
	%--
	% get extension presets
	%--
		
	presets = get_presets(ext(i));
	
	if (isempty(presets))
		continue;
	end
	
	%--
	% loop over and update presets
	%--
	
	str = [upper(ext(i).subtype), ': ', ext(i).name, ' Presets'];
	
	disp(' '); 
	disp(str_line(length(str)));
	disp(str);
	disp(str_line(length(str)));
	
	for j = 1:length(presets)
				
		%--
		% update preset if needed
		%--
		
		% NOTE: we add the keywords field and update the order
		
		% TODO: develop this into a separate function that harmonizes structures
		
		preset = presets(j);

		if (isfield(preset,'keywords'))
			continue;
		end
		
		preset.keywords = cell(0);
		
		preset = orderfields(preset,temp); 
		
		%--
		% save preset
		%--
		
		out = [p, filesep, preset.name, '.mat'];
		
		save(out,'preset');
		
		disp(out);
		
	end
	
end