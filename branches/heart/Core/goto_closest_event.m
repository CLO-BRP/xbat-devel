function [next, event, log] = goto_closest_event(direction, par, time)

%--
% handle input
%--

% NOTE: set active browser to allow use from the command-line

if nargin < 2 || isempty(par)
	par = get_active_browser;
end 

% NOTE: default time is center of the current page, we move to center the next event

if nargin < 3
	page = get_browser_page(par); time = page.start + 0.5 * page.duration;
else
	page = get_browser_page(par);
end

%--
% find closest event
%--

log = get_browser_logs(par);

% NOTE: no logs, no next event

if isempty(log)
	next = []; event = []; log = []; return;
end

if direction > 0
	sql = ['SELECT * FROM event WHERE start > ', num2str(time), ' ORDER BY start LIMIT 1;']; next = inf;
else
	sql = ['SELECT * FROM event WHERE start < ', num2str(time), ' ORDER BY start DESC LIMIT 2;']; next = -inf;
end

for k = 1:numel(log)
	
	[status, candidate{k}] = sqlite(log_cache_file(log(k)), sql); %#ok<NASGU>
	
	if ~isempty(candidate{k})
		
		start = candidate{k}(end).start;
		
		if direction > 0
			if start < next
				next = start; event = candidate{k}; log = log(k);
			end
		else
			if start > next
				next = start; event = candidate{k}(end); log = log(k);
			end
		end

	end 
	
end

% NOTE: we have not found a next event

if ~isfinite(next)
	next = []; event = []; log = []; return;
end

%--
% go to next event
%--

% TODO: check that the page duration and event duration play well

time = next - 0.5 * page.duration + 0.5 * event.duration;

set_browser_time(par, time);

marker = get_browser_marker(par); marker.time = time; set_browser_marker(par, marker);
