function cache_clear(label)

% cache_clear - clear cache entry
% -------------------------------
%
% cache_clear(label)
%
% Input:
% ------
%  label - label of cache entry (def: clear all cache entries)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% clear a single or all cache entries
%--

if (nargin < 1)
	
	data = get(0,'userdata');
	
	if (isfield(data,'cache'))
		data.cache = [];
		set(0,'userdata',data);
	end 

else 
	
	data = get(0,'userdata');
	
	if (isfield(data,'cache') & length(data.cache))
		labels = struct_field(data.cache,'label');
		ix = find(label == labels);
		if (~isempty(ix))
			data.cache(ix) = [];
		end
		set(0,'userdata',data);
	end
	
end
		