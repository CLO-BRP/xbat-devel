function event_menu(g, str, par, log, id)

% event_menu - menu for displayed events
% --------------------------------------
%
% event_menu(g, str, par, log, id)
%
% Input:
% ------
%  g - handle to parent
%  str - command string
%  par - handle to browser figure (def: gcf)
%  log - log (or log name) (def: saved in parent tag)
%  id - event id (def: saved in parent tag)

%----------------------
% HANDLE INPUT
%----------------------

%--
% set command string
%--

if nargin < 2 || isempty(str)
	str = 'Initialize';
end

%--
% set figure and get userdata
%--

if nargin < 3 || isempty(par)
	par = get_active_browser;
end

data = get(par, 'userdata');

%--
% set mode and parent if needed
%--

if isempty(data.browser.parent)
	mode = 'sound';
else
	mode = 'log'; parent = get(data.browser.parent, 'userdata');
end

%--
% get log and event from tag
%--

if nargin < 5

	if is_browser(g)
		patch = gco;
	else
		patch = g;
	end

	tag = get(patch, 'tag');

	[log, id] = parse_event_tag(tag);

end

if isstruct(log)
	log = log_name(log);
end

%--
% get event
%--

log_struct = get_browser_log(par, log);

event = log_get_events(log_struct, id);

%--
% get available annotations and measurements
%--

% [ANNOT, ANNOT_NAME] = get_annotations;
% 
% [MEAS, MEAS_NAME] = get_measurements(par);

%----------------------
% MAIN SWITCH
%----------------------

switch str

	%----------------------
	% Initialize
	%----------------------

	case 'Initialize'

		head = [log ' # ' int2str(event.id)];
		
		if ~isempty(event.score)
			score = ['Score:  ', num2str(100 * event.score, 5), ' %'];
		else
			score = ['Score:  None'];
		end
		
		%--
		% sound browser mode
		%--

		if strcmp(mode, 'sound')

			L = { ...
				head, ...
				score, ...
				'(Basic)', ...
				'Event', ...
				'Hierarchy', ...
				'(Annotation)', ...
				'Tags', ...
				'Rating', ...
				'Annotation', ...
				'Measurement', ...
				'Classification', ...
				'(Action)', ...
				'Play Event', ...
				'Center Event', ...
				'Copy Event To', ...
				'(Edit)', ...
				'Cut Selection', ...
				'Copy Selection', ...
				'Delete Selection', ...
				'Delete Event ...' ...
			};

			n = length(L);

			S = bin2str(zeros(1, n)); S{3} = 'on';

		%--
		% log browser mode
		%--

		else

% 			disp('we are in log_browser mode');

			L = { ...
				head, ...
				score, ...
				'Event', ...
				'Hierarchy', ...
				'Annotation', ...
				'Measure', ...
				'Template', ...
				'View in Sound', ...
				'Play Event', ...
				'Play Clip', ...
				'Delete Event ...' ...
			};

			%--
			% add option to add event to other logs
			%--

			if (length(parent.browser.log) > 1)

				L{9} = 'Copy Event To';
				L{10} = 'Delete Event ...';

				n = length(L);

				S = bin2str(zeros(1, n));
				S{2} = 'on';
				S{4} = 'on';
				S{7} = 'on';
				S{end} = 'on';

			else

				n = length(L);

				S = bin2str(zeros(1, n));
				S{2} = 'on';
				S{4} = 'on';
				S{7} = 'on';
				S{end} = 'on';

			end

		end

		%--
		% attach event menu to parent (typically 'uicontextmenu')
		%--

		h1 = menu_group(g, 'event_menu', L, S);

		set(h1(1:2), 'enable', 'off');

		parenthetical_menus(h1);

		%--
		% set callbacks to other functions
		%--

		set(h1(1), 'callback', '');

		set(get_menu(h1, 'Cut Selection'), ...
			'callback', 'browser_edit_menu(gcf, ''Cut Selection'')' ...
			);

		set(get_menu(h1, 'Copy Selection'), ...
			'callback', 'browser_edit_menu(gcf, ''Copy Selection'')' ...
			);

		set(get_menu(h1, 'Delete Selection'), ...
			'callback', 'browser_edit_menu(gcf, ''Delete Selection'')' ...
			);

		%--
		% compute event labels
		%--

		event.time = map_time(data.browser.sound, 'real', 'record', event.time);

		opt = event_labels;

		opt.time = data.browser.grid.time.labels;

		opt.freq = data.browser.grid.freq.labels;

		[time_label, freq_label] = event_labels( ...
			event, ...
			data.browser.sound, ...
			opt ...
			);

		%--
		% build menu
		%--

		% TODO: eventually include attribute information for channel here, specifically location and calibration

		% NOTE: we may also have channel names!

		
		% TODO: author display should contain human and robot
		
		author = event_author_string(event); 
			
		L = { ...
			'(Channel)', ...
			['Channel:  ', int2str(event.channel)], ...
			'(Time)', ...
			['Start Time:  ', time_label{1}], ...
			['End Time:  ', time_label{2}], ...
			['Duration:  ', time_label{3}], ...
			'(Freq)', ...
			['Min Freq:  ', freq_label{1}], ...
			['Max Freq:  ', freq_label{2}], ...
			['Bandwidth:  ', freq_label{3}], ...
			'(Info)', ...
			['Author:  ', author] ...
		};

		% TODO: make 'created' content same as modified, an SQLite compatible timestamp

		% 	db_disp 'time-stamp problems in menu display'

		if isempty(event.modified)
			L{end + 1} = ['Created:  ', event.created];
		else
			L{end + 1} = ['Modified:  ', event.modified];
			L{end + 1} = ['Created:  ', event.created];
		end

		n = length(L);

		S = bin2str(zeros(1, n));

		tmp = menu_group(get_menu(h1, 'Event'), '', L, S);

		parenthetical_menus(tmp);

		%--
		% create tags menu
		%--

		top = get_menu(h1, 'Tags');

		tags = get_tags(event); known = get_log_tags(log_struct);

		create_tags_menu(top, tags, known, {@tags_menu_callback, log_struct, event});

		%--
		% create rating menu
		%--

		top = get_menu(h1, 'Rating');

		create_rating_menu(top, event.rating, 5, {@rating_menu_callback, log_struct, event});

		%--
		% create required measure menus
		%--

		top = get_menu(h1, 'Measurement');

		if ~isempty(top)
			create_event_measure_menus(top, event);
		end

		%--
		% tag all menus using standard event tag
		%--

		% 	set(findall(g), 'tag', event_tag(event, log));

	%----------------------
	% Play Event
	%----------------------

	case {'Play Event', 'Play Clip'}

		if strcmp(mode, 'sound')
			browser_sound_menu(par,str);
		else
			log_sound_menu(par,str);
		end

		return;

	%----------------------
	% Center Event
	%----------------------

	case 'Center Event'

		selection = get_browser_selection(par);

		%--
		% reset display time to center selection
		%--

		t = (sum(selection.event.time) - data.browser.page.duration) / 2;

		set_browser_time(par, t, 'slider');

	%----------------------
	% View in Sound
	%----------------------

	case 'View in Sound'


	%----------------------
	% Delete Event ...
	%----------------------

	case 'Delete Event ...'

		% TODO: factor this as a function and improve dialog to use new dialogs

		%--
		% confirmation deletion
		%--

		options = {'Yes', 'No', 'Cancel'};

		% NOTE: we add leading spaces to the title for better layout

		ans_dialog = quest_dialog( ...
			['Delete event ''' log ' # ' num2str(id) ''' ?'], ...
			'  Delete Event ...', ...
			options{:}, 'No' ...
			);

		if ~strcmp(ans_dialog, 'Yes')
			return;
		end

		%--
		% delete selection and event, then refresh display
		%--

		% TODO: the display here is not as clean as it should be, it contrasts strongly with the logging display

		data = delete_selection(par, data);

		log_delete_events(log_struct, id);

		browser_refresh(par, data);

% 	%----------------------
% 	% Annotation Schemes
% 	%----------------------
% 
% 	case strcat(ANNOT_NAME, ' ...')
% 
% 		%--
% 		% get log and event indices
% 		%--
% 
% 		[m, ix] = strtok(get(gco,'tag'), '.');
% 
% 		m = str2double(m); ix = str2double(ix(2:end));
% 
% 		%--
% 		% get annotation extension
% 		%--
% 
% 		ixa = strcmp(ANNOT_NAME, str(1:end - 4));
% 
% 		ext = ANNOT(ixa);
% 
% 		%--
% 		% call annotation scheme function and update event palette
% 		%--
% 
% 		if ~strcmp(mode, 'sound')
% 			par = data.browser.parent;
% 		end
% 
% 		process_browser_events(par, ext, m, ix);
% 
% 	%----------------------
% 	% Measurements
% 	%----------------------
% 
% 	case strcat(MEAS_NAME, ' ...')
% 
% 		%--
% 		% get log and event indices
% 		%--
% 
% 		[m, ix] = strtok(get(gco,'tag'), '.');
% 
% 		m = str2double(m); ix = str2double(ix(2:end));
% 
% 		%--
% 		% get measurement extension
% 		%--
% 
% 		ixa = strcmp(MEAS_NAME, str(1:end - 4));
% 
% 		ext = MEAS(ixa);
% 
% 		%--
% 		% call annotation scheme function
% 		%--
% 
% 		if ~strcmp(mode, 'sound')
% 			par = data.browser.parent;
% 		end
% 
% 		process_browser_events(par, ext, m, ix);

	otherwise

		% NOTE: this is now an action

end


%-----------------------------------
% TAGS_MENU_CALLBACK
%-----------------------------------

function tags_menu_callback(obj, eventdata, log, event) %#ok<INUSL>

%--
% update tags
%--

part = str_split(get(obj, 'tag'), '::'); action = part{1}; tag = part{2};

switch action

	case 'apply', event.tags = union(event.tags, tag);

	case 'remove', event.tags = setdiff(event.tags, tag);

	otherwise, return;

end

log_save_events(log, event, 'tags');

%--
% update display
%--

edit_update(obj, log, event)


%-----------------------------------
% RATING_MENU_CALLBACK
%-----------------------------------

function rating_menu_callback(obj, eventdata, log, event) %#ok<INUSL>

%--
% update rating if needed
%--

rating = sum(get(obj, 'label') == '*');

if event.rating == rating
	return;
end

if ~rating
	event.rating = [];
else
	event.rating = rating;
end

log_save_events(log, event, 'rating');

%--
% update display
%--

edit_update(obj, log, event)


%-----------------------------------
% EDIT_UPDATE
%-----------------------------------

function edit_update(obj, log, event)

%--
% get browser handle
%--

par = ancestor(obj, 'figure');

if ~is_browser(par)
	return;
end

%--
% update display of events and selection
%--

browser_display(par, 'events');

% NOTE: we know we are the selection when this happens

set_browser_selection(par, event, log);


