function event_delete(par, log, id, data)

if nargin < 4 || isempty(data)
    data = get_browser(par);
end

if isstruct(log)
    log = log_name(log);
end

