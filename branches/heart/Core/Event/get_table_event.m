function basic = get_table_event(event)

% get_table_event - get part of event represented as single table
% ---------------------------------------------------------------
%
% basic = get_table_event(event)
%
% Input:
% ------
%  event - event
%
% Output:
% -------
%  basic - table event

%--
% get table field description
%--

field = event_table_fields;

%--
% get table events
%--

timestamp = get_current_timestamp;

for k = 1:numel(event)

	%--
	% perform field name to column conversion
	%--
	
	% TODO: create functions to perform conversion in each direction
	
	basic(k) = build_prototype(field, event(k));

	% NOTE: the code here is OBSCURE outside of the 'event_table_fields' context

	%--
	% update time and frequency fields stored
	%--
	
	basic(k).start = basic(k).start(1);

	% NOTE: the two following checks consider the event representation of a marker
	
	if ~isempty(basic(k).low)
		basic(k).high = basic(k).low(2); basic(k).low = basic(k).low(1);
	end

	if basic(k).duration == 0
		basic(k).duration = [];
	end
	
	%--
	% update dates in case of previously stored events
	%--
	
	% NOTE: added during import
	
	if isnumeric(basic(k).created_at) && ~isempty(basic(k).created_at)
		basic(k).created_at = datestr(basic(k).created_at, 31);
	end
	
	if ~isempty(basic(k).id)
		basic(k).modified_at = timestamp;
	end
	
end

% NOTE: added during import

if ~string_is_member('guid', fieldnames(basic))
	
	basic(1).guid = '';
end

