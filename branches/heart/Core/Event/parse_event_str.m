function [name, id] = parse_event_str(str)

% parse_event_str - parse an event info string to get log name and event id
% -------------------------------------------------------------------------
%
% [name, id] = parse_event_str(str)

%--
% handle multiple strings
%--

if iscellstr(str)
	[name, id] = iterate(mfilename, str); return;
end

%--
% get log name and event identifier
%--

[name, str] = strtok(str, '#'); name = name(1:(end - 1));

id = strtok(str, ':'); id = eval(id(3:end));

