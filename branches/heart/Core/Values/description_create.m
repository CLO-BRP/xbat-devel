function value = description_create(varargin)

% description_create - create value description
% ---------------------------------------------
%
% value = description_create
%
% Output:
% -------
%  value - value description

%---------------------------
% CREATE DESCRIPTION
%---------------------------
	
value.alias = '';

% NOTE: types are 'numerical', 'categorical', and 'ordinal'

value.type = '';

% NOTE: units must be a string, eventually we may support known units

value.units = '';

if ~isempty(varargin)
	value = parse_inputs(value,varargin{:});
end

%---------------------------
% SOME VALIDATION
%---------------------------

%--
% validate and normalize type
%--

if ~isempty(value.type)
	
	if ~ischar(value.type)
		error('Description type must be character string.');
	end
	
	types = {'numerical', 'categorical', 'ordinal'};

	switch lower(value.type(1))
	
		case 'n', value.type = types{1};
		
		case 'c', value.type = types{2};
			
		case 'o', value.type = types{3};
			
		otherwise, error('Unrecognized description type.');
			
	end
	
end

%--
% validate interval type
%--

if ~isempty(value.interval)
	
	% TODO: change this to use 'ismember'
	
	switch value.interval
		
		case {0,1,2,3}
			
		otherwise, error('Improper interval type code.');
		
	end
	
end

