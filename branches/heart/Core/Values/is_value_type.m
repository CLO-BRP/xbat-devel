function [value, type] = is_value_type(type)

%--
% iterate over multiple inputs
%--

if iscellstr(type) && numel(type) > 1
	[value, type] = iterate(mfilename, type); return;
end

%--
% check for proper input
%--

if ~ischar(type)
	value = 0; type = ''; return;
end

%--
% check input string is value type
%--

% TODO: consider a more lenient evaluation

types = get_value_types;

ix = find(strcmpi(types, type));

value = ~isempty(ix);

%--
% output normalized type
%--

if nargout > 1
	if value
		type = types{ix};
	else
		type = '';
	end
end
