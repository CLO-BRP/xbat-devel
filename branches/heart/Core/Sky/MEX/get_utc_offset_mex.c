/* TODO: generalize to other systems, this only works on windows */

/* TODO: consider developing this code for UTC time handling, rather than the more complex external toolbox */

#include "windows.h"

#include "mex.h"

#include "matrix.h"

/* from http://lists.gnupg.org/pipermail/gnupg-devel/2000-September/016630.html, Hideki Saito hideki at allcity.net  */

float get_utc_offset(void) {

	float offset;
	
	/* declare and get system times */
	
	SYSTEMTIME utc; SYSTEMTIME local;

	GetSystemTime(&utc); GetLocalTime(&local);

	/* compute offset */
	
	offset = (float)(utc.wHour - local.wHour);
	
	if (local.wDay < utc.wDay) {
		
		offset = (float)(((24 - local.wHour) + utc.wHour) * -1);
	
	} else if (local.wDay > utc.wDay) {
	
		offset = (float)(((24 + local.wHour) - utc.wHour) * -1);
	
	} else {
		
		offset = (float)(utc.wHour - local.wHour);
	
	} 
	
	/* NOTE: what is this correction? */
	
	if (utc.wMinute - local.wMinute == 30) { 
		offset += 0.5; 
	}
	
	return offset;

}

/* from http://code.google.com/p/matlab-directx/source/browse/trunk/display/dllmain.cpp?r=52, William.McIlhagga */

/* NOTE: the author of this function calls it a missing MX function, that seems accurate */

mxArray* mxCreateScalar(double x) {
	
    mxArray* p = mxCreateDoubleMatrix(1, 1, mxREAL);
	
    double*  ptr = mxGetPr(p);
    
	ptr[0] = x;
    
	return p;

}

/* NOTE: the MEX function in this case is trivial, just call the above two functions */

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	plhs[0] = mxCreateScalar(get_utc_offset());

}
