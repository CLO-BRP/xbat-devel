function sound = new_sound_dialog(lib)

% new_sound_dialog - create a new sound
% -------------------------------------
% sound = new_sound_dialog(lib)
%
% Input:
% ------
%  lib - the library to which to add the sound
%  
% Output:
% -------
%  sound - the new sound

%--
% handle input
%--

if ~nargin
	lib = get_active_library;
end

%--
% create controls
%--

control = empty(control_create);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'min', 1, ...
	'string', 'Sound' ...
);

types = title_caps(union(sound_types, 'folder'));

control(end + 1) = control_create( ...
	'name', 'type', ...
	'style', 'popup', ...
	'string', types, ...
	'value', 1, ...
	'onload', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'info', ...
	'style', 'listbox', ...
	'lines', 4, ...
	'value', [], ...
	'min', 0, ...
	'max', 5 ...
);

control(end + 1) = control_create( ...
	'name', 'add_files_to_database', ...
	'width', 0.5, ...
	'space', 0.75, ...
	'style', 'checkbox', ...
	'initialstate', '__DISABLE__', ...
	'value', 1 ...
);

control(end + 1) = control_create( ...
	'name', 'copy_data', ...
	'alias', 'copy_files_to_library', ...
	'width', 0.5, ...
	'style', 'checkbox', ...
	'space', 1.5, ...
	'value', 0 ...
);

opt = dialog_group;

opt.width = 14;

opt.header_color = get_extension_color('root');

%--
% create dialog
%--

result = dialog_group('New ...', control, opt, @new_sound_callback);
	
if ~strcmp(result.action, 'ok')
	return;
end

type = lower(result.values.type{1});

copy = result.values.copy_data;

err = [];

switch type

	case {'file', 'file stream', 'playlist', 'variable', 'recording'}
		
		try
			sound = sound_create(type);	
		catch
			sound = []; nice_catch(lasterror, ['Failed to create ''', upper(type), ''' sound.']);
		end
		
		% NOTE: what follows is the former catch
		
% 			err = lasterr; 
% 			
% 			if strcmp(type, 'file stream')
% 				
% 				err = { ...
% 					err(strfind(err, sprintf('\n')) + 1:end), ...
% 					'Try adding these files as a ''folder''' ...
% 				};		
% 			
% 			end
% 			
% 		end	
		
		%--
		% add sound to library
		%--		

		if ~isempty(sound)
			add_sounds(sound, lib, [], copy);
		end


	case 'folder'
		
		%--
		% get library root directory
		%--
		
		% NOTE: we try to remember the last place we looked for sound files
		
		base = get_env('xbat_sound_data_path'); 
		
		if isempty(base) || ~exist(base,'dir')
			base = set_env('xbat_sound_data_path', pwd);
		end
	
		base = uigetdir(base, 'Select folder containing sound files');
	
		if ~base
			return;
		end
		
		% NOTE: consider filesep at the end of the drive selection

		while base(end) == filesep
			base = base(1:end - 1);
		end

		set_env('xbat_sound_data_path', base);

		%--
		% get sounds in folder and add them to the library
		%--

		get_folder_sounds(base, 1, {@add_sounds, lib, [], copy});	
		
		get_library_sounds([], 'refresh');
		
	otherwise
		
		error(['Unrecognized sound type ''', upper(type), '''.']);
			
end

if ~isempty(err)
	warn_dialog(err, ['Can''t create ', type, ' sound.'], 'modal');			
end

%--
% update palette to display available sounds
%--

xbat_palette('find_sounds');
		

%---------------------------------------
% NEW_SOUND_CALLBACK
%---------------------------------------

function result = new_sound_callback(obj, eventdata)

%--
% get callback context
%--

callback = get_callback_context(obj, eventdata);

control = callback.control; pal = callback.pal;

pal = pal.handle;

type = get_control(pal, 'type', 'value'); type = lower(type{1});

str = '';

switch type
	
	case 'file'
		
		str = [ ...
			'Single-file sound' ...
		];
	
		set_control(pal, 'name', 'enable', 0);

	case 'file stream'
		
		str = [ ...
			'Multi-file sound contained in a directory.  All files must have ', ...
			'matching sample rate and number of channels.' ...
		];
		
		set_control(pal, 'name', 'enable', 0);
		
	case 'folder'
		
		str = [ ...
			'Multiple Sounds.  The directory is recursively scanned for files ', ...
			'and subfolders are added as multiple sounds, or as file-streams if possible.' ...
		];
	
		set_control(pal, 'name', 'enable', 0);
		
	case 'variable' 
		
		str = [ ...
			'A workspace variable.  If you have sound data in a variable, ', ...
			'you may browse it as if it were a sound file on disk.' ...
		];
	
		set_control(pal, 'name', 'enable', 0);
				
	case 'synthetic'
		
		str = [ ...
			'Create an empty sound to which data may be added by applying ', ...
			'''source'' and ''filter'' extensions.' ...
		];
	
		set_control(pal, 'name', 'enable', 1);
	
	case 'recording'
		
		str = [ ...
			'Create an empty sound to which data may be added by acquiring ', ...
			'it using the MATLAB data aquisition toolbox.' ...
		];
	
		set_control(pal, 'name', 'enable', 1);
			
end

%--
% update information display control
%--

if isempty(str)
	str = {'(No Description Available)'};
else
	str = str_wrap(str, 40);
end

han = get_control(pal, 'info', 'handles');

set(han.obj, 'string', str, 'value', []);



