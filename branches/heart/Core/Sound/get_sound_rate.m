function rate = get_sound_rate(sound)

% get_sound_rate - get effective sound sample rate
% ------------------------------------------------
%
% rate = get_sound_rate(sound)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  rate - current sample rate of sound

%--
% iterate over sound arrays
%--

if numel(sound) > 1
	rate = iterate(mfilename, sound); rate = [rate{:}]; return;
end 

%--
% get native or resampled rate
%--

if isempty(sound.output.rate)
	rate = sound.samplerate;
else
	rate = sound.output.rate;
end
