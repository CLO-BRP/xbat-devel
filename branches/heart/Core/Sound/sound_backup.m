function sound_backup(lib,name)

% sound_backup - backup sound from library
% ----------------------------------------
%
% sound_backup(lib,name)
%
% Input:
% ------
%  lib - library containing sound
%  name - name of sound to backup

if isstruct(name)
    name = sound_name(name);
end

%--
% check for the existence of the backup directory. create if needed
%--

backup_dir = [lib.path, '__BACKUP'];

if ~exist(backup_dir,'dir')
	mkdir(lib.path,'__BACKUP');
end

% NOTE: block on the directory creation, this may not be needed

while ~exist(backup_dir,'dir')
	pause(0.1);
end

%--
% zip sound directory backup folder with time stamp
%--

in = [lib.path, name]; % location of sound directory

out = [lib.path, '__BACKUP', filesep, name, ' ', datestr(now,30), '.zip']; % location of backup zip file
	
zip(out,in);