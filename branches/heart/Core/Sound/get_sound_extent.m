function [ix, n, ch] = get_sound_extent(sound, mode, x, dx, ch)

% get_sound_extent - get sound extent from input
% ----------------------------------------------
%
% [ix, n, ch] = get_sound_extent(sound, mode, x, dx, ch)
%
% Input:
% ------
%  sound - sound 
%  mode - reading mode
%  x - starting index or time
%  dx - number of samples or duration
%  ch - channels to read

%--
% set default read mode
%--

if isempty(mode)
	mode = 'samples';
end

if ~ischar(mode)
	error('Read mode must be string.');
end

if ~string_is_member(mode, {'samples', 'time'})
	error(['Unrecognized read mode ''', mode, '''.']);
end

%--
% set starting index
%--

if isempty(x)
	
	ix = 0;

else

	if strcmpi(mode, 'time')
		ix = x * sound.samplerate;
	else
		ix = x;
	end
	
	ix = floor(ix);
	
end

%--
% set number of samples to read
%--

if isempty(dx)

	%--
	% set remaining samples as default
	%--
	
	if strcmpi(sound.type, 'file')
		n = sound.samples - ix;
	else
		n = sound.cumulative(end) - ix;
	end
	
else

	if strcmpi(mode, 'time')
		n = dx * sound.samplerate;
	else
		n = dx;
	end

	n = floor(n);
	
	%--
	% check that we have requested number of samples in sound
	%--
	
% 	if (strcmpi(sound.type, 'file'))
% 		
% 		if (n > (sound.samples - ix))
% 			disp(' ');
% 			error('Number of desired samples exceeds available samples.');
% 		end
% 		
% 	else
% 		
% 		if (n > (sound.cumulative(end) - ix))
% 			disp(' ');
% 			error('Number of desired samples exceeds available samples.');
% 		end
% 		
% 	end

end

%--
% set and check channels
%--

% NOTE: default is to select all available channels, reconsider this

if isempty(ch)

	ch = 1:sound.channels;

else

	%--
	% check that we have requested channels
	%--
	
	% NOTE: channels requested may not be unique
	
	flag = numel(unique(ch)) > numel(intersect(1:sound.channels, ch));
	
	if flag
		error('Selected channels are unavailable.');
	end

end