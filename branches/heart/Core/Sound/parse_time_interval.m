function interval = parse_time_interval(str)

% parse_time_interval - parse an interval string using clock_to_sec
% -----------------------------------------------------------------
%
% interval = parse_time_interval(str)
%
% Input:
% ------
%  str - string
%
% Output:
% -------
%  interval - interval struct

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

if ~iscell(str)
	str = {str};
end

for k = 1:length(str)
	
	session_str = str{k}; session_str([1, end]) = '';
	
	start_str = strtok(session_str); stop_str = fliplr(strtok(fliplr(session_str)));
	
	start(k) = clock_to_sec(start_str); stop(k) = clock_to_sec(stop_str);
	
end

interval.start = start; interval.stop = stop; interval.end = stop;
	
	
	
	