function name = sound_name(sound)

% sound_name - compute name of sound
% ----------------------------------
%
% name = sound_name(sound)
%
% Input:
% ------
%  sound - sound structure
%
% Output:
% -------
%  name - string used as name for sound

% NOTE: this function encodes sound file naming conventions used

%--------------------
% HANDLE INPUT
%--------------------

%--
% handle special input cases, not sound struct input
%--

if iscellstr(sound)
    name = sound{1}; return;
end

if ischar(sound)
    name = sound; return;
end

if isempty(sound)
	name = ''; return;
end

%--
% iterate over sound arrays
%--

if numel(sound) > 1
	name = iterate(mfilename, sound); return;
end

%--------------------
% COMPUTE NAME
%--------------------

% TODO: make this coherent with the extension framework, make this is a method that we can dispatch

switch lower(sound.type)

	%--
	% file
	%--
	
	case 'file'

		% TODO: handle file names with channel indices in special way
		
		% NOTE: the sound name the filename without the extension

		name = file_ext(sound.file);

	%--
	% file stream
	%--
	
	case 'file stream'

		% NOTE: the sound name is the immediate parent directory name
		
		name = file_parent(sound.path, 0);
		
		%--
		% check for channel naming convention 
		%--
		
		% TODO: create a function to handle more complex conventions
		
% 		channels = 0;
% 		
% 		if strcmpi(name(1:2), 'ch')
% 			channels = 1; 
% 		end
% 		
% 		if channels
% 			name = [file_parent(sound.path), ' Ch ', strtrim(name(3:end))];
% 		end
		
% 		%--
% 		% handle name length
% 		%--
% 		
% 		if length(name) < 10
% 			
% 			if channels		
% 				name = [file_parent(sound.path,2), ', ', name];
% 			else
% 				name = [file_parent(sound.path), ', ', name];
% 			end
% 			
% 		end

	%--
	% playlist
	%--
	
	case 'playlist'
		
		% NOTE: the name of the playlist sound is the name of the playlist file
		
		[p1, p2, p3] = fileparts(sound.path); name = p2; %#ok<NASGU>
	

		
	% NOTE: these 'types' are not currently implemented, it is not clear that they will be
	
	case 'stack', name = 'stacked sound';
		
	case {'variable', 'synthetic'}, name = sound.file;
		
		
	%--
	% error condition
	%--
	
	otherwise, error(['Unrecognized sound type ''', sound.type, '''.']);

end
