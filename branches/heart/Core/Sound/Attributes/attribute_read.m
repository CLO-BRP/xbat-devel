function attr = attribute_read(sound, name, start, duration, channels, options)

% attribute_read - read sound attributes for page
% -----------------------------------------------
%
% attr = attribute_read(sound, name, start, duration, channels, options)
%
% Input:
% ------
%  sound - sound to read attributes of
%  name - attribute name
%  start - start time of page
%  duration - duration of page
%  channels - page channels
%  options - options

% NOTE: we would like many of these attributes to be read through a web service interface

% TODO: return simple empty when called with no page and attribute does not exist

%------------------------
% HANDLE INPUT
%------------------------

attr = [];