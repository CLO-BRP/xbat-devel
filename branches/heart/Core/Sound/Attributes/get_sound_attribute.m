function value = get_sound_attribute(sound, name, page, store)

% get_sound_attribute - get sound attribute value from name
% ---------------------------------------------------------
%
% value = get_sound_attribute(sound, name, page, store)
%
% Input:
% ------
%  sound - sound
%  name - attribute name
%  page - to get values for
%  store - for barn
%
% Output:
% -------
%  value - attribute value

% TODO: figure out how to integrate this code into the new framework

%--
% handle input
%--

if nargin < 4
	store = local_barn;
end

%--
% get sound attribute
%--

% TODO: check for available store and tables before trying anything funny

% TODO: use get_barn_table or has_barn_table, whatever it's called

try

	%--
	% attribute from barn store
	%--
	
	ext = get_attribute_extension(name); table = extension_table_names(ext);

	% NOTE: we don't abuse the 'sound' variable, in case there's a catch
	
	user = get_active_user; obj = set_barn_sound(store, sound);

	sql = ['SELECT * FROM ', table.value, ' WHERE sound_id = ', int2str(obj.id), ';'];
	
	% NOTE: consider the available page in the case of dynamic attributes

	% TODO: verify the page will typically be given in recording time?

	% TODO: add 'dynamic' field to extension

	if isfield(ext.dynamic) && ext.dynamic && nargin > 3

		sql = [' AND WHERE time >= ', to_str(page.start), ' AND time <= ', to_str(page.start + page.duration)];

	end

	sql = [sql, ';'];
	
	[status, value] = query(store, sql);

	value = rmfield(value, {'id', 'sound_id', 'user_id'});
	
catch
	
	% NOTE: we use old code when barn 'get' fails, in any way. the goal is backward compatibility for now
	
	value = old_sound_attribute(sound, name);

end


%-------------------------------
% OLD_SOUND_ATTRIBUTE
%-------------------------------

function value = old_sound_attribute(sound, name)

% NOTE: in this case we would load attributes from file and pick them from where we packed them

value = [];

ix = find(strcmp(name, {sound.attributes.name}));

if isempty(ix)
	return;
end

value = sound.attributes(ix).value;
