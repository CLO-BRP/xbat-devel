function ext = get_attribute_extension(name)

% get_attribute_extension - by name or fieldname
% ----------------------------------------------
% 
% ext = get_attribute_extension(name)
%
% Input:
% ------
%  name - or fieldname of extension
%
% Output:
% -------
%  ext - for attribute
%
% See also: get_extensions

ext = get_extensions('sound_attribute', 'name', name);

if isempty(ext)
	ext = get_extensions('sound_attribute', 'fieldname', name);
end