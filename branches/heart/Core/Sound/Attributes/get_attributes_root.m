function root = get_attributes_root(context)

% get_attributes_root - get attributes directory relative to input path
% ---------------------------------------------------------------------
%
% root = get_attributes_root(context)
% 
% Input:
% ------
%  context - context (includes sound and container library)
%
% Output:
% -------
%  root - attributes path
% 
% See also: get_attribute_file

% NOTE: also check that we can write with the sound? it may exist in non-writeable media, which if it contains means we cannot edit

%--
% handle sound input
%--

% NOTE: placing attributes with the data provides universal access

if strcmpi(context.sound.type, 'file stream')	
	
	root = fullfile(context.sound.path, '__XBAT', 'Attributes');
else	
	if ischar(context.library)
		context.library = get_library_from_name(context.library);
	end
	
	root = fullfile(context.library.path, sound_name(context.sound), 'Attributes');
end
