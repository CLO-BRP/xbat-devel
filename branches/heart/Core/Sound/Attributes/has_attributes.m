function name = has_attributes(sound)

% has_attributes - check which attributes a sound has
% ---------------------------------------------------
% 
% name = has_attributes(sound)
%
% Input:
% ------
%  sound - sound
%
% Output:
% -------
%  name - available attribute names

names = get_attribute_names; name = {};

for k = 1:numel(names)

	% NOTE: the double index allows us to grow a column rather than a row
	
	if has_attribute(sound, names{k})
		name{end + 1, 1} = names{k};
	end
end


