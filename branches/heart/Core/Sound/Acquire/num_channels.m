function [N names IDs] = num_channels(ai_obj)

%--------------------------------------------
% [N names IDs] = num_channels(ai_obj)
%
% ai_obj - a standard data acquisition toolbox analog input object
%
% N - the number of available channels
%
% names - the names of each channel, in a cell array
%
% IDs - channel identifiers, the channel IDs are guarateed to be unique and
% have similar indexing properties for all analog input adapter types.
%
% Author: Matt Robbins
% $Date$
% $Revision$

info = daqhwinfo(ai_obj);

N = info.TotalChannels;

IDs = info.SingleEndedIDs;

if strcmpi(get(ai_obj, 'InputType'), 'differential');

	N = N/2;
	
	IDs = info.DifferentialIDs;
	
end

names = cell(N, 1);

for ix = 1:N
	
	names{ix} = ['channel ' num2str(IDs(ix))];
	
end
	