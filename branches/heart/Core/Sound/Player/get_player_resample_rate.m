function [rate, p, q] = get_player_resample_rate(rate, lowest)

% get_player_resample_rate - get player rate and resample factors
% ---------------------------------------------------------------
%
% [rate, p, q] = get_player_resample_rate(rate, lowest)
%
% Input:
% ------
%  rate - of input
%  lowest - rate desired
%
% Output:
% -------
%  rate - for typically reliable play
%  p, q - resample factors
%
% See also: resample_for_play, buffered_player

%--
% create list of candidate typical play supported rates
%--

persistent TYPICAL_PLAY_RATES;

if isempty(TYPICAL_PLAY_RATES)
	
	TYPICAL_PLAY_RATES = [8000, 11025, 22050, 44100, 48000]; 
end

if nargin > 1 && ~isempty(lowest)
	
	candidates = TYPICAL_PLAY_RATES(TYPICAL_PLAY_RATES >= lowest);
else
	candidates = TYPICAL_PLAY_RATES;
end

%--
% check if we are playing a supported rate
%--

if ismember(rate, candidates)
	p = []; q = []; return;
end

%--
% select sufficient rates from available rates
%--

rates = candidates(candidates >= rate);

if isempty(rates)
	rates = candidates(end);
end

%--
% select samplerate based on efficient resampling scheme 
%--

p = zeros(size(rates)); q = p;

for k = 1:length(rates)
	[p(k), q(k)] = rat(rates(k) / rate);
end

[ignore, ix] = min(p + q); %#ok<ASGLU>

rate = rates(ix); p = p(ix); q = q(ix);
