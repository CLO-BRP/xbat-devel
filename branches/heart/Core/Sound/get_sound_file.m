function file = get_sound_file(sound, position)

% get_sound_file - by position
% ----------------------------
%
% file = get_sound_file(sound, position)
%
% Input:
% ------
%  sound - to query
%  position - of file
% 
% Output:
% -------
%  file - location

% NOTE: this function will be impacted by updating sound stores to be database files

if ischar(sound.file)
	file = sound.file;
else
	if position < 1 || position > numel(sound.file)
		error('File index out of bounds.');
	end
	
	file = sound.file{position};
end 

file = fullfile(sound.path, file);