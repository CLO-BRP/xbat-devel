function [file, ix] = get_current_file(sound, time)

% get_current_file - get file at specific time
% --------------------------------------------
%
% [file, ix] = get_current_file(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - slider time
%
% Output:
% -------
%  file - file at sound time
%  ix - index of file in sound files

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% check input time to be non-negative
%--

if time < 0
	file = ''; ix = []; return; % error('Sound times are always positive.');
end

%--
% single file sounds are special
%--

if get_file_count(sound) == 1
	
	ix = 1; file = get_file_from_index(sound, ix); return;
end

% if ischar(sound.file)
% 	file = sound.file; ix = 1; return;
% end
% 
% if iscell(sound.file) && numel(sound.file) == 1
% 	file = sound.file{1}; ix = 1; return;
% end

%---------------------------
% GET CURRENT FILE
%---------------------------

%--
% get file start sound times
%--

% NOTE: get_file_times.m was written to accept 'real' time

time = map_time(sound, 'real', 'slider', time);

start = get_file_times(sound);

%--
% locate time within file start times
%--

ix = find(time >= start, 1, 'last'); 

file = get_file_from_index(sound, ix);


