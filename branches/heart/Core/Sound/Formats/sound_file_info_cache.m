function [out, missing] = sound_file_info_cache(in, fields)

% sound_file_info_cache - set and get sound file info to and from cache
% ---------------------------------------------------------------------
%
% [info, missing] = sound_file_info_cache(file, fields)
%
%                 = sound_file_info_cache(info)
%
% Input:
% ------
%  file - file to consider
%  fields - desired info fields
%  info - info to set
%
% Output:
% -------
%  info - info from cache
%  missing - files missing from cache in relation to input

%--------------------
% SETUP
%--------------------

% TODO: this is one possibility for avoiding the insert of single items into the database

persistent SET_QUEUE; %#ok<NUSED>

% NOTE: the page size should be carefully considered, perhaps exposed as an environment variable

% TODO: we should present some kind of waitbar when we are iterating over pages

page_size = 64;

%--------------------
% HANDLE INPUT
%--------------------

%--
% output location of cache file
%--

% NOTE: this is not revealed in the help, it might be useful to do this

if ~nargin
	out = cache_file; return;
end

%--
% get mode from simple input examination
%--

if isstruct(in)
	
	% NOTE: input consists of sound file info structs that we want to store
	
	mode = 'set';
	
	if nargout && (nargin < 2)
		fields = {'*'};
	end
	
else
	
	% NOTE: the input should consist of file names that we want to get info for
	
	mode = 'get';
	
	% NOTE: get all fields by default
	
	if nargin < 2
		fields = {'*'};
	end
	
end

%--------------------
% SET OR GET
%--------------------

switch mode

	case 'set'
		
		%--
		% create and initialize cache file if needed
		%--
		
		if ~exist(cache_file, 'file')
			cache_file_init(in);
		end

		%--
		% set cache info
		%--
		
		set_cache_info(in);
		
		if numel(in) <= page_size
			
			set_cache_info(in);
			
		else
			
			[page, pages] = get_index_pages(numel(in), page_size);

			for k = 1:pages
				set_cache_info(in(page(k).start:page(k).stop));
			end

		end
		
		%--
		% retrieve cache info if asked for
		%--
		
		% NOTE: this is an honest retrieval, we are not passing the input as output
		
		if nargout
			
			out = sound_file_info_cache({in.file}, fields);
			
			% NOTE: this serves as a test of whether 'set' was fully successful
			
			if nargout > 1
				missing = setdiff({in.file}, {out.file});
			end
			
		end

	case 'get'
		
		%--
		% return empty info when it is not available
		%--
		
		% NOTE: in this case we are also missing information for all request files
		
		if ~exist(cache_file, 'file')
			
			out = empty(info_create); missing = in; return;
			
		end
		
		%--
		% retrieve cache info
		%--
		
		if numel(in) <= page_size
			
			out = get_cache_info(in, fields);
		
		else

			[page, pages] = get_index_pages(numel(in), page_size);
			
			% TODO: consider the transposition, it should not be needed
			
			for k = 1:pages
				out{k} = get_cache_info(in(page(k).start:page(k).stop), fields)';
			end
			
			out = [out{:}];

		end
		
		missing = setdiff(in, {out.file});
		
end


%--------------------------
% CACHE_FILE
%--------------------------

function file = cache_file

% TODO: this kind of file should have a special place

persistent CACHE_FILE;

if isempty(CACHE_FILE)
	CACHE_FILE = fullfile(fileparts(which('sound_file_info')), 'sound_file_info_cache.db');
end

file = CACHE_FILE;


%--------------------------
% CACHE_FILE_INIT
%--------------------------

function cache_file_init(info)

% NOTE: we do not flatten info, so the schema may tolerate variable format-specific info

opt = create_table; opt.flatten = 0;

opt.column(end + 1) = column_description('file', 'VARCHAR', 'UNIQUE NOT NULL');
	
sqlite(cache_file, 'prepared', create_table(info, 'sound_file_info', opt));


%--------------------------
% SET_CACHE_INFO
%--------------------------

function set_cache_info(info)

% NOTE: the 'info' struct containd the file information

sqlite(cache_file, 'exec', sqlite_array_insert('sound_file_info', info));


%--------------------------
% GET_CACHE_INFO
%--------------------------

function info = get_cache_info(file, fields, loose)

%--
% set default loose retrieval mode
%--

if nargin < 3
	loose = 0; 
end

%--
% get names from files in the loose case, also set the retrieval field
%--

% NOTE: the 'loose' mode strongly depends on the integrity of file names

if loose
	[p1, p2, p3] = iterate(@fileparts, file); file = strcat(p2, p3);
end

field = ternary(loose, 'name', 'file');

%--
% build retrieval sql and get info
%--

% TODO: extend to use file modification date as well

fields = str_implode(fields, ', ');

% NOTE: we need to use the encoding as part of the SQL generation, this is strange

set = strcat('("', str_implode(file, '", "', @spcharin), '")');

% NOTE: we want to get info for all proposed files

sql = ['SELECT ', fields, ' from sound_file_info WHERE ', field, ' in ', set, ' ORDER BY ', field, ';'];

[status, info] = sqlite(cache_file, 'prepared', sql);


