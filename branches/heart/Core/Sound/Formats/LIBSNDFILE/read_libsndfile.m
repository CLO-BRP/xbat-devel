function [samples, options] = read_libsndfile(file, start, duration, channels, options)

% read_libsndfile - read samples from sound file
% ----------------------------------------------
%
% [samples, options] = read_libsndfile(file, start, duration, channels, options)
%
% Input:
% ------
%  file - file info
%  start - initial sample
%  duration - number of samples
%  channels - channels to select
%  options - conversion options
%
% Output:
% -------
%  samples - samples from sound file
%  options - updated conversion options

%------------------------------------
% READ SAMPLES FROM FILE
%------------------------------------

%--
% get file from file info
%--

% NOTE: in this case we only need the file name, we clobber file

file = file.file;

%--
% convert file string to C string
%--

% NOTE: this string replacement handles network files

file = strrep(file, '\\', '??');

file = strrep(file, '\', '\\');

file = strrep(file, '??', '\\');

%--
% create sample class prototype for configuring MEX, if needed
%--

if nargin < 5
	type = [];
else
	type = get_class_prototype(options);
end

%--
% get samples using mex
%--

% NOTE: we read each requested channel once in sorted, then we sort and replicate if needed

[channels, ignore, reconstruct] = unique(channels); %#ok<ASGLU>

samples = sound_read_(file, start, duration, int16(channels), type)'; % note the transpose

samples = samples(:, reconstruct);


%------------------------------------
% GET_CLASS_PROTOTYPE
%------------------------------------

function type = get_class_prototype(options)

if isempty(options) || isempty(options.class)
	type = []; return;
end 
	
switch options.class

	case 'double', type = double([]);

	case 'single', type = single([]);

	case 'int16', type = int16([]);

	case 'int32', type = int32([]);

	otherwise, error(['Unsupported sample type ''', options.class, '''.']);
end
