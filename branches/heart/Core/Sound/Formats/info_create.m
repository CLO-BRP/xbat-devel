function info = info_create(varargin)

% info_create - create info structure
% -----------------------------------
%
%  info = info_create
%
% Output:
% -------
%  info - info structure

%--------------------------------
% CREATE INFO STRUCTURE
%--------------------------------

persistent INFO_PERSISTENT;

if isempty(INFO_PERSISTENT)
	
	%--------------------------------
	% SYSTEM INFORMATION
	%--------------------------------
	
	info.name = [];			% file name
	
	info.file = [];			% file location (full path)
	
	info.date = [];			% last modification date
	
	info.bytes = [];		% size of file in bytes
	
	info.format = [];		% simple, human readable format name (match to available formats)
	
	%--------------------------------
	% FILE CONTENT INFORMATION
	%--------------------------------
	
	info.channels = [];		% number of channel in file
	
	info.samplerate = [];	% samplerate in hertz
	
	info.samplesize = [];	% number of bits per sample
	
	info.samples = [];		% number of samples in file
	
	info.duration = [];		% duration of file (convenience field)
	
	% TODO: this hides quite a bit of complexity, the recorder is a relevantly complex device
	
	% NOTE: what we mean is that we probably need to at least describe microphone, A/D, and encoder in the corresponding table
	
	% info.recorder_id = [];
	
	%--------------------------------
	% TIME-SPACE METADATA
	%--------------------------------
	
	% NOTE: we expect this to be simple data for static recording systems
	
	% TODO: consider how to store this information for dynamic recording systems
	
	% TODO: include a foreign key to timing and locator devices, there should be tables describing these, for example their precision
	
	% TODO: consider also how to structure these values in a sufficient way
	
% 	info.time = []; info.clock_id = [];
% 	
% 	info.place = []; info.locator_id = [];
	
	%--------------------------------
	% FORMAT INFORMATION
	%--------------------------------
	
% 	info.info = [];	% format specific file information
	
	INFO_PERSISTENT = info;
	
else
	
	info = INFO_PERSISTENT;
		
end

%--------------------------------
% SET FIELDS IF PROVIDED
%--------------------------------

%--
% try to get field value pairs from input
%--

if ~isempty(varargin)
	
	info = parse_inputs(info, varargin{:});

end


