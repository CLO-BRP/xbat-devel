function [X, opt] = read_ogg(file, ix, n, ch, opt)

% read_ogg - read samples from sound file
% ---------------------------------------
%
% [X, opt] = read_ogg(file, ix, n, ch, opt)
%
% Input:
% ------
%  file - file location
%  ix - initial sample
%  n - number of samples
%  ch - channels to select
%  opt - conversion request options
%
% Output:
% -------
%  X - samples from sound file
%  opt - updated conversion options

file.file = get_ogg_cache_file(file.file);

%--
% load data from temporary sound file
%--
	
X = read_libsndfile(file, ix, n, ch);

%--
% delete temporary file
%--

% TODO: periodically enforce a limit on OGG cache size