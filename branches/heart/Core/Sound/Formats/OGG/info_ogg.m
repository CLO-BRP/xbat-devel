function info = info_ogg(file)

% info_ogg - get sound file info
% ------------------------------
%
% info = info_ogg(file)
%
% Input:
% ------
%  file - file location
%
% Output:
% -------
%  info - format specific info structure

%--
% get OGG file info through decoding
%--

file = get_ogg_cache_file(file);

if isempty(file)
	error('Unable to get OGG info through decoding.');
end

info = sound_file_info(file);

%--
% discard cache file
%--

% delete(file);



%--------------------------------------------------
% ACTUAL_OGG_INFO
%--------------------------------------------------

% TODO: this code is very old and should be reviewed

function info = actual_ogg_info(file) %#ok<DEFNU>

% NOTE: consider putting precise file information using vorbis comments when we encode, so that we can retrieve it here

% NOTE: this might mean that we only handle files that are ogg encoded using xbat

%--------------------
% GET INFO
%--------------------

persistent INFO;

if isempty(INFO)

	INFO = get_tool('ogginfo.exe');
	
	if isempty(INFO)
		error('Unable to get OGG info helper.');
	end

end

%--
% use helper to get info string
%--

str = ['"', INFO.file, '" -v "', file, '"'];

[status, result] = system(str);

result

%--------------------
% PARSE RESULT
%--------------------

%--
% separate lines of 'ogginfo' output
%--

% NOTE: 10 is the code for the newline character

ix = [0, find(double(result) == 10)];

for k = 2:length(ix);
	lines{k} = result(ix(k - 1) + 1:ix(k) - 1);
end

%--
% find lines containing information and parse the lines
%--

% info = lines; 
% 
% return;

%--
% find lines containing information, parse, and pack into info structure
%--

% SAMPLERATE

for k = 1:length(lines)
	
	if (~isempty(findstr('Rate',lines{k})))
		
		[t1,t2] = strtok(lines{k},':');
		
		info.samplerate = eval(t2(2:end));
		break;	
		
	end
	
end

% info.samplerate = eval(lines{5});

% CHANNELS

for k = 1:length(lines)
	
	if (~isempty(findstr('Channels',lines{k})))
		
		[t1,t2] = strtok(lines{k},':');
		
		info.channels = eval(t2(2:end));
		break;
		
	end
	
end

% NOMINAL BITRATE

% NOTE: for this line we remove the label and the units

for k = 1:length(lines)
	
	if (~isempty(findstr('Nominal bitrate',lines{k})))
		
		[t1,t2] = strtok(lines{k},':');
		[t2,t1] = strtok(t2(2:end),' ');
		
		info.nominal_bitrate = eval(t2);
		break;
		
	end
	
end

% AVERAGE BITRATE

% NOTE: for this line we remove the label and the units

for k = 1:length(lines)
	
	if (~isempty(findstr('Average bitrate',lines{k})))
		
		[t1,t2] = strtok(lines{k},':');
		[t2,t1] = strtok(t2(2:end),' ');
		
		info.average_bitrate = eval(t2);
		break;
		
	end
	
end

