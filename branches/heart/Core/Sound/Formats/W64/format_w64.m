function format = format_w64

format = format_libsndfile;

format.name = 'Sony Pictures Digital Wave 64 (W64)';

format.ext = {'w64'};