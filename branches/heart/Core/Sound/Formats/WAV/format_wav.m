function format = format_wav

format = format_libsndfile;

format.name = 'Waveform Audio Format (WAV)';

format.ext = {'wav'};