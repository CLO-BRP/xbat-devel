function format = format_flac

% format_flac - create format structure
% -------------------------------------
%
% format = format_flac
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% create format structure
%--

% NOTE: this is inheritance in the extension framework!

format = format_libsndfile;

%--
% fill format structure (set)
%--

% NOTE: in the extension framework this might be the short description

format.name = 'Free Lossless Audio Codec (FLAC)';

format.ext = {'fla','flac'};

format.home = 'http://flac.sourceforge.net/';

% NOTE: we are now reading this format natively

% format.info = @info_flac;
% 
% format.read = @read_flac;

format.write = @write_flac; 

format.encode = @encode_flac;

format.decode = @decode_flac;

format.seek = 1;

format.compression = 1;
