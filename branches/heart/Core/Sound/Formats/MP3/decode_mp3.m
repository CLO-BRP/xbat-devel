function out = decode_mp3(f1, f2)

% decode_mp3 - decode mp3 to wav
% ------------------------------
%
% out = decode_mp3(f1, f2)
%
% Input:
% ------
%  f1 - input file
%  f2 - output file
%
% Output:
% -------
%  out - command to execute to perform decoding

%--
% persistently  store location of command-line helper
%--

persistent LAME;

if isempty(LAME)
	LAME = get_tool('lame.exe');
end

%--
% build full command string
%--

out = ['"', LAME.file, '" --decode "', f1, '" "', f2, '"'];
