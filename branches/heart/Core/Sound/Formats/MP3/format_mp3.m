function format = format_mp3

% format_mp3 - create format structure
% ------------------------------------
%
% format = format_mp3
%
% Output:
% -------
%  format - format structure

%--
% create format structure
%--

format = format_create;

%--
% fill format structure
%--

format.name = 'MPEG-1 Layer 3 (MP3)';

format.home = 'http://www.underbit.com/products/mad/';

format.ext = {'mp3'};

format.info = @info_mp3;

format.read = @read_mp3;

format.write = @write_mp3;

format.encode = @encode_mp3;

format.decode = @decode_mp3;

format.seek = 1;

format.compression = 1;

