function ip = get_ip(force)

% get_ip - get own IP address
% ---------------------------
%
% ip = get_ip(force)
%
% Input:
% ------
%  force - refresh, otherwise the address is cached
%
% Output:
% -------
%  ip - address

% TODO: create server script to get this information remotely as well, add 'method' parameter 'local' versus 'remote'

if ~nargin
	force = 0; 
end

persistent IP;

if ~isempty(IP) && ~force
	ip = IP; return; 
end

%--
% call system helper to get ip info
%--

% TODO: generalize to other operating systems

if ispc
	[status, result] = system('ipconfig');
else
	[status, result] = system('ifconfig');
end

status

opt = file_readlines; opt.skip = 1;

lines = file_readlines(result, @strtrim, opt);

% db_disp lines; iterate(@disp, lines)

%--
% parse result to get ip
%--

if ispc
	ip = parse_pc(lines);
else
	ip = parse_unix(lines);
end

IP = ip; % NOTE: store result persistently


%--------------------------
% PARSERS
%--------------------------

% PARSE_PC

function ip = parse_pc(lines)

% TODO: add some fault-tolerance to the parsing

for k = 1:numel(lines)
	% NOTE: we get the first displayed IP
	
	if strcmp('IP', lines{k}(1:2))
		part = str_split(lines{k}, ':'); ip = strtrim(part{2}); break;
	end
end

% PARSE_UNIX

function ip = parse_unix(lines)

db_disp implement-unix-parse

ip = '';

