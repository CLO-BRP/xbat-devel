function info = get_mac

if ispc
	[status, result] = system('getmac /nh /v'); %#ok<ASGLU>
	
	opt = file_readlines; opt.skip = true;
	
	lines = file_readlines(result, [], opt);
	
	info = struct; info.lines = lines; device = cell(numel(lines), 1);

	db_disp; iterate(@disp, lines);
	
	for k = 1:numel(lines)
		[parts, match] = regexp(lines{k}, '\s((?:[0-9A-F]{2}[:-]){5}(?:[0-9A-F]{2}))\s', 'split', 'match')
		
		device{k} = struct( ...
			'description', strtrim(parts{1}), ... 
			'mac', strtrim(match), ... 
			'status', strtrim(parts{2}) ...
		);
	end
	
	info.device = [device{:}];
else
	
end