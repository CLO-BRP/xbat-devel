function on__startup(config)

% on__startup - helper for local configuration
% --------------------------------------------
%
% on__startup(config)
%
% Input:
% ------
%  config - name
%
% See also: local_barn, get_barn_store, rails_server

%------------------------------------
% SHARED configuration
%------------------------------------

% NOTE: this turns off CUDA related code

cuda_enabled false;

%------------------------------------
% NAMED configuration
%------------------------------------

% NOTE: return quickly if no named configuration is requested

if ~nargin
	return;
end

switch config
	
	case 'default'
		
		%--
		% database connection
		%--

		% NOTE: this is the default database configuration, uses MySQL 'localhost' database with 'root' user and no password
		
		store = get_barn_store('barn', 'MySQL'); 
		
% 		store = get_barn_store('barn', 'MySQL', 'localhost', 'username', '<% user %>', 'password', '<% password %>');
		
		%--
		% web server interaction
		%--
		
		% NOTE: the command below creates the default server configuration
		
		server = rails_server;
		
        % NOTE: below are hints to remote server configuration
		
% 		server.root = '\\server\BARN';
% 		
% 		server.files =  '\\server\BARN\public\files';
				
		%--
		% set environment variables 
		%--
		
		local_barn(store, server);
		
	% TODO: fill-in some typical options here?
		
	case 'localhost'
		
		on__startup default;
		
	case 'remotehost-1'
		
	case 'remotehost-2'
		
	otherwise
		
		error(['Unknown configuration ''', config, '''.']);
end



