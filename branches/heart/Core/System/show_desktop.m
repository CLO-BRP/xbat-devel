function [value, status, result] = show_desktop(value)

% show_desktop - set visibility of windows desktop
% ------------------------------------------------
% 
% state = show_desktop
%
% show_desktop(1) - show the desktop
%
% show_desktop(0) - hide the desktop

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% return value if no input
%--

if nargin < 1
	
	value = get_env('show_desktop'); 
	
	% NOTE: handle empty value here in case 'startup' fails to set it
	
	if ~isempty(value)
		return;
	else
		value = 1;
	end
	
end 

%--------------------------------
% BUILD AND EXECUTE COMMAND
%--------------------------------

% TODO: figure out if there is some linux equivalency

if ispc
	
	%--
	% get tool file
	%--

	tool = nircmd;
    
    if isempty(tool)
        return;
    end

	% NOTE: this allows the command syntax

	if ischar(value)
		value = eval(value);
	end

	if value

		[status, result] = system(['"', tool.file, '" win show class progman']);

		set_env('show_desktop', 1);

	else

		[status, result] = system(['"', tool.file, '" win hide class progman']);

		set_env('show_desktop', 0);

	end
	
end