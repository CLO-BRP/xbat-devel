function out = from_str(str)

% NOTE: this is an experiment in retrieving a struct from a string

%--
% get field-value parts from input string
%--

part = strread(str, '%s', -1, 'delimiter', '|');

%--
% initialize struct and get field values by parsing and evaluating parts
%--

out = struct;

for k = 1:length(part)
	
	[field, value] = strtok(part{k}, ':'); 
	
	% NOTE: 'strtok' leaves the delimiter with the second part
	
	if length(value)
		value(1) = []; 
	end
	
	%--
	% try to get value and reconstruct
	%--
	
	try	
		if isempty(value)
			out.(field) = [];
		else
			out.(field) = eval(value);
		end	
	catch	
		nice_catch(lasterror, ['Failed to reconstruct field ''', field, '''.']);
	end
	
end

%--
% collapse struct
%--

out = unflatten_struct(out);
