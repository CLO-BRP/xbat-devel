function parsed = parse_css(file, verb)

% parse_css - rules from file
% ---------------------------
%
% parsed = parse_css(file, verb)
%
% Input:
% ------
%  file - input
%  verb - indicator
%
% Output:
% -------
%  parsed - rules

%--
% handle input
%--

if nargin < 2
	verb = true;
end

%--
% load and group lines
%--

lines = file_readlines(file);

group = zeros(0, 2);

for k = 1:numel(lines)
	
	if string_contains(lines{k}, '{')
		group(end + 1, 1) = k;
	end
	
	if string_contains(lines{k}, '}')
		group(end, 2) = k;
		
		if verb
			disp(['GROUP-', int2str(size(group, 1)), ':']); 
			
			iterate(@disp, lines(group(end, 1):group(end, 2)));
			
			disp(' ');
		end
	end
end

%--
% collapse groups into normal form
%--

normal = cell(size(group, 1), 1);

for k = 1:size(group, 1)
	
	normal{k} = str_implode(iterate(@strtrim, lines(group(k, 1):group(k, 2))));
	
	normal{k} = remove_comments(normal{k});
	
	if verb
		disp(['NORMAL-', int2str(k), ':']);
		
		disp(normal{k});
		
		disp(' ');
	end
end

%--
% parse normalized group into selectors and rules
%--

parsed = repmat(struct('selectors', [], 'rules', []), numel(normal), 1);

for k = 1:numel(normal) 
	
	db_disp normal; disp(normal{k});
	
	cut = find(normal{k} == '{', 1, 'first');
	
	% TODO: we are currently getting a parsed rule with empty selector
	
	parsed(k).selectors = str_split(normal{k}(1:cut - 1), ',');
	
	parsed(k).rules = parse_rules(normal{k}(cut + 1:end - 1));
end

if verb
	disp('PARSED:');
	
	iterate(@(x)(disp(flatten(x))), parsed);
end

%--
% analyze selectors
%--


%--
% analyze rules
%--


%-----------------------
% PARSE_RULES
%-----------------------

function rules = parse_rules(str)

%--------------

% HACK: one last bit of paranoia

str = strtrim(str); 

if isempty(str)
	rules.props = struct; return;
end

if str(end) == '}'
	str(end) = []; 
end

%--------------

rule = str_split(str, ';'); props = struct;

db_disp rule-str; str, rule, iterate(@disp, rule);

for k = 1:numel(rule)
	
	% NOTE: each rule is a field value pair
	
	part = str_split(rule{k}, ':');
	
	props.(prop_field(part{1})) = part{2};
end

rules.props = props;


%-----------------------
% REMOVE_COMMENTS
%-----------------------

% NOTE: this function removes comments from a group in normal form

function normal = remove_comments(normal)

start = strfind(normal, '/*'); stop = strfind(normal, '*/'); 

if numel(start) ~= numel(stop)
	
	% TODO: consider improving this to include problem lines, for each group there are a number of source lines
	
	error('Unable to remove comments, they must span more than one rule group.');
end

for k = numel(start):-1:1
	
	normal(start(k):stop(k) + 1) = [];
end


%-----------------------
% RULE_NAME
%-----------------------

function field = prop_field(field)

field = strrep(field, '-', '_');

% NOTE: for agent-specific properties we have names that start with a '-'

if field(1) == '_'
	field = ['A', field];
end


