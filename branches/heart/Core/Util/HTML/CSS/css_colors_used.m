function [parsed, full] = css_colors_used(in)

% css_colors_used - in styles
% ---------------------------
%
% [parsed, full] = css_colors_used(in)
%
% Input:
% ------
%  in - file or parsed rules
%
% Output:
% -------
%  parsed - rules using color
%  full - set of input rules
%
% See also: parse_css

[parsed, full] = filter_css_rules(in, 'field', 'color');

