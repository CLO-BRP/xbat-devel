function str = to_str(value, sep, n)

% to_str - convert a vector of values to string
% ---------------------------------------------
%
% str = to_str(value)
%
% Input:
% ------
%  value - value
%
% Output:
% -------
%  str - string

% TODO: refactor to use options struct, add option to quote strings as well

%-------------------
% HANDLE INPUT
%-------------------

% NOTE: default precision and implode separators are not revealed in help

if nargin < 3
	n = 16;
end

if nargin < 2
	sep = '|';
end 

%-------------------
% GET STRING
%-------------------

%--
% create row value vector
%--

value = value(:)';

%--
% consider class of value for conversion
%--

str = '';

switch class(value)
	
	case 'char'
		
		str = value; return; 
		
% 		str = ['''', value, '''']; return;
		
	case 'cell'
		
		% NOTE: this only properly handles row cell arrays 
				
		for k = 1:length(value)
			str{k} = to_str(value{k}, ';');
		end
	
		str = ['{', str_implode(str, ','), '}']; return;
		
	case 'struct'
		
		if length(value) > 1
			error('Only scalar structs are supported.');
		end
		
		value = flatten_struct(value); field = fieldnames(value);
		
		for k = 1:length(field)		
			str{k} = [field{k}, ':', to_str(value.(field{k}), ';')];
		end
		
	case 'logical'
		
		for k = 1:length(value)
			str{k} = int2str(value(k));
		end
		
	otherwise
		
		if all(value == floor(value))
			% NOTE: the MEX underlying this function fails for large values
			
% 			str = int_to_str(value);
			
			for k = 1:length(value)
				str{k} = int2str(value(k)); 
			end
		else
			str = cell(size(value));
		
			for k = 1:length(value)
				str{k} = num2str(value(k), n);
			end
			
		end
end

%--
% implode cell arrays to get string
%--

% NOTE: it is not clear why we need the 'while' here

while iscell(str)
	str = str_implode(str, sep);
end
