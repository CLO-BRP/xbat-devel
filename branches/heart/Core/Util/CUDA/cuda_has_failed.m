function [result, failure] = cuda_has_failed(fun, state, error)

% cuda_has_failed - persistent memory
% -----------------------------------
%
% [previous, failure] = cuda_has_failed(fun, state)
%
% failure = cuda_has_failed(fun, state, error)
%
% history = cuda_has_failed(fun)
%
% cuda_has_failed fun
%
% Input:
% ------
%  fun - name
%  state - description, should include 'data' field may include 'key'
%  error - description, as provided by 'lasterror'
%
% Output:
% -------
%  previous - failure indicator
%  failure - description
%  history - of failures
%
% See also: cuda_enabled
%
% NOTE: this function is provided to allow efficient development and robust use of CUDA acceleration

% NOTE: typical use of this function involves a block analogous to the one below

% if cuda_enabled && ~cuda_has_failed(mfilename, struct('data', state))
%    try
%        cuda_function(data, state); return;
%    catch
%        cuda_has_failed(mfilename, struct('data', state), error)
%    end
% end

%--
% setup failure store
%--

persistent FAILURE;

if isempty(FAILURE)
	FAILURE = struct;
end

%--
% dispatch based on signauture
%--

switch nargin
	
	case 0
		% output all failures or display failure functions
		
		if nargout
			result = FAILURE;
		else
			display_failure(FAILURE);
		end
		
	case 1 % (fun)
		
		% display failures for specific function
		
		if nargout
			if isfield(FAILURE, fun)
				result = FAILURE.(fun);
			else 
				result = [];
			end
		else
			display_failure(FAILURE, fun)
		end
		
	case 2 % (fun, state)
		
		% check for and retrieve failure for function given state
		
		key = get_state_key(state);
		
		result = isfield(FAILURE, fun) && isfield(FAILURE.(fun), key);
		
		if result && nargout > 1
			failure = FAILURE.(fun).(key);
		else
			failure = [];
		end
		
	case 3 % (fun, state, error)
		
		% register failure
		
		failure = pack_failure(error, state);
		
		FAILURE.(fun).(failure.key) = failure;
end


%-----------------------------
% PACK_FAILURE
%-----------------------------

function failure = pack_failure(error, state)

if nargin < 2
	state = {};
end

failure.state = state;

failure.error = error;

failure.key = get_state_key(state);


%-----------------------------
% DISPLAY_FAILURE 
%-----------------------------

function display_failure(store, fun)

if nargin < 2
	fun = fieldnames(store);
end

if ischar(fun)
	fun = {fun};
end

disp(' ');

if isempty(fun)
	disp('No CUDA failures recorded for any function this session.');
end

for k = 1:numel(fun)
	disp(upper(fun{k}));
	disp(str_line(length(fun{k})));
	
	if ~isfield(store, fun{k})
		disp('  No CUDA failure recorded.'); continue;
	end
	
	state = fieldnames(store.(fun{k}));
	
	for l = 1:numel(state)
		% TODO: remove line-breaks from error, use indentation
		
		failure = store.(fun{k}).(state{l});
		
		disp(['STATE: (', state{l}, ')']);
		disp(to_json(failure.state));
		
		disp('ERROR:');
		disp(failure.error.message);
		
		disp('STACK:');
		stack_disp(failure.error.stack);
		disp(' ');
	end
end

disp(' ');

%-----------------------------
% GET_STATE_KEY
%-----------------------------

function key = get_state_key(state)

if isfield(state, 'key')
	key = state.key;
	
	if ~ischar(key)
		error('State ''key'' field should contain a string.');
	end
	
	% NOTE: this makes sure that the key is a proper fieldname
	
	key = genvarname(key);
else
	% NOTE: in this case we compute a state key based on the hashing the state data
	
	key = ['k_', hash(state.data)];
end

