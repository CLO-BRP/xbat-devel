function current = get_matlab_version

% get_matlab_version - info
% -------------------------
%
% current = get_matlab_version
%
% Output:
% -------
%  current - version info
% 
% NOTE: this is an adapter for the built-in 'ver' and 'version'
%
% See also: ver, version

% NOTE: get and repack version info from built-in functions

current = struct;

%--
% get release name and date
%--

full = ver('matlab'); 

current.release = full.Release(2:end - 1);

current.version = full.Version; 

current.date = full.Date;

%--
% get version sequence elements
%--

% http://en.wikipedia.org/wiki/Software_versioning

[current.major, rest] = strtok(version, '.'); current.major = eval(current.major);

[current.minor, rest] = strtok(rest, '.'); current.minor = eval(current.minor);

[current.revision, rest] = strtok(rest, '.'); current.revision = eval(current.revision);

try
	current.build = strtok(rest(2:end), ' '); current.build = eval(current.build);
catch
	current.build = [];
end

%--
% get architecture 32 or 64 bit
%--

% NOTE: this works when MATLAB is installed in a conventional location

% TODO: we might want to factor this

if strcmp(computer, 'PCWIN64') || (string_contains(matlabroot, '(x86)') && strcmp(computer, 'PCWIN'))
	arch = 64;
else
	arch = 32;
end

% NOTE: this code does not offer a difference from 'mexext'

% os = computer;
% 
% if strcmp(os(end - 1:end), '64')
% 	arch = 64;
% else
% 	arch = 32;
% end

current.arch = arch;

ext = mexext;

if strcmp(ext(end - 1:end), '64')
	mex = 64;
else
	mex = 32;
end

current.mex = mex;
