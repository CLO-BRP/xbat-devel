function [str, uuid] = generate_uuid

% generate_uuid - generate random universal identifier
% ----------------------------------------------------
%
% [str, uuid] = generate_uuid
%
% Output:
% -------
%  str - representation of identifier
%  uuid - java object

% TODO: implement other UUID functions not just the random version

import java.util.*

% NOTE: we typically want the MATLAB string representation, perform conversion, it is the first output

uuid = UUID.randomUUID(); str = char(uuid.toString());