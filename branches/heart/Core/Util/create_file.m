function [file, created] = create_file(file)

% create_file - create file with extension
% ----------------------------------------
% 
% [file, created] = create_file(file)
%
% Input:
% ------
%  file - to create
%
% Output:
% -------
%  file - created
%  created - indicator

% TODO: consider using 'get_fid' when we need to create a file

%--
% check file input
%--

[p1, p2, p3] = fileparts(file);

if isempty(p3)
	error('File must have an extension.');
end

%--
% create directory
%--

% TODO: update this function to indicate the number of directories created

[p1, created] = create_dir(p1);

% NOTE: failed to create parent directory

if isempty(p1)
	file = ''; return;
end

%--
% check for file, create if needed
%--

if exist(file, 'file')
	return;
end

% TODO: handle various file types properly, create a valid file

try
	switch p3
		case '.mat'
			save(file); created = true;
			
		otherwise
			fclose(fopen(file, 'w')); created = true;
	end
catch %#ok<CTCH>
	file = ''; created = false
end

