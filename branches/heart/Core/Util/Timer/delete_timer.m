function count = delete_timer(name)

% delete_timer - by name
% ----------------------
%
% count = delete_timer(name)
%
% Input:
% ------
%  name - of timer
%
% Output:
% -------
%  count - of deleted timers

obj = timerfind('name', name);

count = numel(obj);

if ~count
	return;
end

stop(obj); delete(obj);