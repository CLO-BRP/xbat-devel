function [value, ix] = in_path(dirs)

% in_path - check for directories in path
% ---------------------------------------
%
% [value, ix] = in_path(dirs)
%
% Input:
% ------
%  dirs - candidate directories
%
% Output:
% -------
%  value - indicator
%  ix - position in path

if ~nargin
	dirs = scan_dir(pwd);
end

[value, ix] = string_is_member(dirs, path_to_cell); 

if ~nargout
	disp(' ');
	
	for k = 1:numel(dirs)
		disp([int2str(value(k)), ' ', dirs{k}]);
	end
	
	disp(' ');
	
	clear value;
end