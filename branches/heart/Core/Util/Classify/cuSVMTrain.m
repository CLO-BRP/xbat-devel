function [model, info] = cuSVMTrain(data, value, opt)

% cuSVMTrain - CUDA implementation of SVM trainer
% ------------------------------------------------------
%[model, info] = cuSVMTrain(value, data, opt);
%
% Inputs:
% label - single-precision vector of training outputs. If you are classifying,
%     these must be either 1 or -1. In regression, these are generally continuously
%     valued.
% data - single-precision matrix of training data corresponding to y.
% opt.C - scalar SVM regularization parameter.
% opt.kernel - scalar Gaussian kernel parameter.
% opt.eps - eps in eps-Support Vector Regression. If you want to classify rather than
%     regress, set eps to empty, i.e [ ].
% opt.stoppingcrit - optional scalar argument that one can use to specify the
%     optimization stopping criterion. Default is 0.001
%
% Outputs:
% model.cusvm.alphas - single-precision vector of the support vector coefficients.
% model.cusvm.beta - single-precision scalar, the offset b in the SVM prediction function.
% model.cusvm.svs - single-precision matrix of the support vectors corresponding to
%       alphas, i.e. the support vector found in row i of svs has the coefficient in
%       the SVM prediction function found in row i of alphas.
% info - placeholder for suplemental info

% --
% handle input
% --

if ~isfield(opt, 'C')
	opt.C = 10;
end

if ~isfield(opt, 'kernel')
	opt.kernel = 0.05;
end

if ~isfield(opt, 'eps')
	opt.eps = [];
end

if ~isfield(opt, 'stoppingcrit')
	opt.stoppingcrit = 0.001;
end

% --
% Train
% --

[model.cusvm.alphas, model.cusvm.beta, model.cusvm.svs] = cuda_SVMTrain_mex(single(value), single(data), opt.C, opt.kernel, opt.eps, opt.stoppingcrit);

% Save the corresponding prediction function for this training function

model.predict = @libSVMPredict;

info = struct;