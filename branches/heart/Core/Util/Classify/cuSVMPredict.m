function [value, info] = cuSVMPredict(data, model, opt)

% cuSVMPredict - CUDA implementation of SVM predictor
% ------------------------------------------------------
%
% value = cuSVMPredict(data, model, opt)
%
% Inputs:
%--------
% data - single-precision matrix of test data.
% model.cusvm.svs - single-precision matrix of support vectors output by cuSVMTrain.
% model.cusvm.alphas - single-precision vector of support vector coeficients output by cuSVMTrain.
% model.cusvm.beta - single-precision scalar offset output by cuSVMTrain.
% model.cusvm.kernel - the same scalar Gaussian kernel parameter value previously used in cuSVMTrain.

% opt - placeholder for optional parameters
% opt.regind - scalar indicator variable that tells cuSVMPredict whether you
%          are classifying or regressing. Set regind to 0 if the former and
%          1 if the later. Default is classify.

% Outputs:
%---------
% value  - single-precision vector of predictions
% info - suplemental info

% --
% handle input
% --

if nargin < 2
    error('Required arguments missing');
end

if nargin < 3 || ~isfield(opt, 'regind')
    regind = 0;
else
    regind = opt.regind;
end

% --
% predict
% --

value = cuda_SVMPredict_mex(data, model.cusvm.svs, model.cusvm.alphas, model.cusvm.beta, model.cusvm.kernel, regind);

info = struct;