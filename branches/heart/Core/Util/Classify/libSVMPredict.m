function [value, info] = libSVMPredict(data, model)

% libSVMPredict - Matlab interface to LIBSVM predictor
% ------------------------------------------------------
%
% [value, info] = libSVMPredict(data, model)
%
% Inputs:
%--------
% data - to find values for
% model.libsvm.model - SVM model description output by libSVMTrain
%
% Outputs:
%---------
% values - vector of predictions
% info.libsvm.accuracy
% info.libsvm.dec_values

% TODO: document why we do this.

label = floor(2 * rand(size(data, 1), 1)) * 2 - 1;

[value, info.libsvm.accuracy, info.libsvm.dec_values] = libSVMPredict_mex(label, data, model.libsvm.model);