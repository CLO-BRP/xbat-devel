function x = vec_row(X)

% vec_row - concatenate rows of matrix
% ------------------------------------
%
% x = vec_row(X)
% 
% Input:
% ------
%  X - input matrix
% 
% Output:
% -------
%  x - column vector obtained from matrix

x = vec_col(X')';
