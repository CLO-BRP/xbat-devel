function root = jruby_path

% jruby_path - in system
% ----------------------
%
% root = jruby_path
%
% Output:
% -------
%  root - path
%
% See also: jruby

% NOTE: here we get the 'bin' path

if ispc
	path = get_windows_path;
	
	found = find(~cellfun('isempty', strfind(get_windows_path, 'jruby')));
	
	if found
		root = path{found}; 
	end
else 
	[status, result] = system('which jruby'); %#ok<ASGLU>
	
	if ~isempty(result)
		root = result;
	end
end

if ~isempty(root) 
	root = fileparts(root);
end 