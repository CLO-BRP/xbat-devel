function file = get_logger_db(logname)

file = get_logger_db_fullfile( logname );

if ~exist(file)
    disp(['Creating a new SQLite log: ', file])
    sqlite(file, 'CREATE TABLE logs (id integer primary key autoincrement, level text, name text, message text, time datetime);');
end
