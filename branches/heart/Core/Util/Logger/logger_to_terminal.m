function logger_to_terminal(level, message)

str = sprintf('[%s] %s \n%s \n', upper(level), datestr(now), message);

disp(str)