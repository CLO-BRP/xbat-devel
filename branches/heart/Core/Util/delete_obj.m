function tags = delete_obj(par, tag, fun, varargin)

% delete_obj - delete objects by focusing on matching tags
% --------------------------------------------------------
%
% tags = delete_obj(par, tag, fun, field, value, ... )
%
% Input:
% ------
%  par - parent
%  tag - tag or tag pattern
%  fun - matching function (def: @string_contains)
%  field - field name
%  value - field value
%
% Output:
% -------
%  tags - tags of deleted handles

%--
% handle input
%--

% NOTE: the default is case sensitive string comparison

if (nargin < 3) || isempty(fun)
	fun = @string_contains;
end

%--
% find objects based on tag matching and provided attributes
%--

obj = find_obj(par, tag, fun, varargin{:});

%--
% carefully delete
%--

% NOTE: when we fail to delete an object we remove its tag from the list

% NOTE: the meaning of this output is tricky due to parental relations

tags = get(obj, 'tag');

for k = numel(obj):-1:1

	try
		delete(obj(k));
	catch
		tags(k) = [];
	end
	
end
