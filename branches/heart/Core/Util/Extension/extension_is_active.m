function value = extension_is_active(ext, par)

% extension_is_active - check if extension is active in browser
% -------------------------------------------------------------
%
% value = extension_is_active(ext, par)
%
% Input:
% ------
%  ext - extension
%  par - browser
%
% Output:
% -------
%  value - active indicator

% TODO: checking all browsers might be useful

%--
% handle input
%--

if nargin < 2
	par = get_active_browser;
end 

if isempty(par)
	value = 0; return;
end

%--
% check that extension is active
%--

% NOTE: we do not need a fresh extension to test whether an extension is active

% active = get_active_extension(ext.type, par);

% TODO: factor this?

data = get(par, 'userdata');

if ~isfield(data.browser, ext.type)
	value = 0; return;
end

value = string_is_member(ext.name, data.browser.(ext.type).active);

% TODO: consider how to extend this to have multiple instances of an extension

% value = ~isempty(active) && ismember(ext.name, {active.name});
