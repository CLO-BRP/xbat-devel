function descendant = get_extension_descendants(ext)

% get_extension_descendants - get all descendant extensions
% ---------------------------------------------------------
%
% descendant = get_extension_descendants(ext)
%
% Input:
% ------
%  ext - parent
%
% Output:
% -------
%  descendant - list

%--
% get immediate descendants, children
%--

child = get_extension_children(ext); 

descendant = child;

%--
% get further descendants through recursion
%--

for k = 1:numel(child)
	descendant = [descendant, get_extension_descendants(child(k))];
end