function lines = generate_callback_code(control)

% generate_callback_code - generate callback skeleton from controls
% -----------------------------------------------------------------
%
% lines = generate_callback_code(control)
%
% Input:
% ------
%  control - control array
%
% Output:
% -------
%  lines - code lines for control callback skeleton

%--------------------------------------
% GENERATE CODE
%--------------------------------------

%--
% callbacks have a standard header
%--

% NOTE: this relies on input to callback being names 'obj'

lines = cell(0);

lines{end + 1} = 'callback = get_callback_context(obj, eventdata);';
lines{end + 1} = ' ';
lines{end + 1} = 'result = []; control = callback.control; pal = callback.pal; par = callback.par;';
lines{end + 1} = ' ';
lines{end + 1} = 'switch callback.control.name';
lines{end + 1} = ' ';

%--
% add control switch cases
%--

for k = 1:length(control)

	% TODO: develop code for buttongroups
	
	switch control.style
		
		case {'separator', 'tabs'}
			continue;
			
		case 'buttongroup'
			continue;
			
		otherwise
			lines{end + 1} = ['    case ''', control.name, '''']; 
			lines{end + 1} = ' ';
			
	end
	
end

%--
% close switch and return
%--

lines{end + 1} = 'end';
lines{end + 1} = ' ';

% NOTE: output column cell array, readable in command line

lines = lines(:);
