function types = get_allowed_parent_types(in)

% get_allowed_parent_types - get allowed parent types for extension type
% ----------------------------------------------------------------------
%
% types = get_allowed_parent_types(in)
%
% Input:
% ------
%  in - extension type name or extension
%
% Output:
% -------
%  types - allowed parent types

%--
% handle input
%--

% NOTE: allowed input is an extension type name or an extension

if iscell(in)
	in = in{1};
end

if ischar(in)
	type = in;
else
	type = in.subtype; 
end

%--
% ask extension type extension about allowed parents
%--

% TODO: it is strange that 'get_extensions' uses the human-readable version of this value

ext = get_extensions('extension_type', 'name', title_caps(type));

% NOTE: the output is not the human-readable version, but the proper fieldname version

% NOTE: the first element of the list is the same type

if isempty(ext.fun.parents)
	types = type;
else
	others = unique(ext.fun.parents()); if ~isempty(others), types = {type, others{:}}; end
end

