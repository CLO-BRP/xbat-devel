function extension_control_examples(style, type)

% extension_examples - for controls type and extensions of a given type
% ---------------------------------------------------------------------
%
% extension_control_examples(control, type)
%
% Input:
% ------
%  style - of control
%  type - of extension

%--
% setup
%--

if nargin < 2
	exts = get_extensions;
else
	exts = get_extensions(type);
end

if ~nargin
	style = 'slider';
end

par = get_active_browser;

if isempty(par)
	par = 0;
end

%--
% get control examples
%--

for k = 1:numel(exts)
	
	%--
	% get current extension and see if it has controls
	%--
	
	% TODO: there is a helper for this already
	
	ext = exts(k); 
	
	if ~isfield(ext.fun, 'parameter')
		continue;
	end
	
	fun = ext.fun.parameter.control.create;
	
	if isempty(fun)
		continue; 
	end
	
	%--
	% create and examine controls
	%--
	
	[ext, ignore, context] = get_browser_extension(ext.type, par, ext.name);
	
	try
		control = fun(ext.parameter, context);
	catch
		disp(['Failed to create controls for ''', ext.name, '''.']); continue;
	end
	
	db_disp(ext.name); style, {control.style}'
	
end


