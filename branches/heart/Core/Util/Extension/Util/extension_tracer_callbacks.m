function extension_tracer_callbacks(value, type, when)

% extension_tracer_callbacks - set and unset
% ------------------------------------------
%
% extension_tracer_callbacks(active, type, when)
%
% Input:
% ------
%  active - state 
%  type - of extensions to trace
%  when - to trigger
%
% See also: extension_callbacks, extension_call

%--
% handle input
%--

if nargin < 3
	when = {'before', 'after', 'error'};
end

if nargin < 2 || isempty(type)
	type = get_extension_types;
end

if ~nargin
	value = true;
end 

%--
% setup
%--

if value
	callback = @tracer;
else
	callback = [];
end

%--
% set tracer callbacks for all types
%--

for k = 1:numel(type)	
	
	for l = 1:numel(when)
		
		extension_callbacks(when{l}, type{k}, [], callback);
	end
end


%-------------------------
% TRACER
%-------------------------

function tracer(call, varargin)

% TODO: add context used 'extension_type' or 'extension', include in 'call'

% db_disp tracing; call, varargin

switch call.match
	case 'extension_type'
		match = 'type';
		
	case 'extension'
		match = 'specific';
end

disp(['TRACER: ''', call.ext.name, ''' ', match, ' match callback ', call.when, ' ''', func2str(call.fun), '''.']);
