function callbacks = extension_callbacks(when, ext, fun, callback)

% extension_callback - get and set
% --------------------------------
%
% callback = extension_callback(when, ext, fun, callback)
%
% Input:
% ------
%  when - to callback 'before', 'after', or 'error'
%  ext - context
%  fun - call
%  callback - to set
%
% Output:
% -------
%   - set, or struct for context
%
% See also: extension_call

%--
% handle input
%--

if nargin && ~string_is_member(when, {'before', 'after', 'error'})
	
	error(['Unrecognized callback trigger ''', when, '''.']);
end

if nargin > 1 && trivial(ext)
	
	error('Extension-type or extension context must be available.');
end

if nargin < 3
	fun = '';
end

if ~trivial(fun) && isa(fun, 'function_handle')
		
	fun = func2str(fun); % NOTE: this should give us the double underscore separated name
end
	
%--
% setup persistent callback stores
%--

% NOTE: the name is short for 'callback registry'

persistent CBR

if isempty(CBR)
	
	CBR = struct('extension_type', struct, 'extension', struct);
end

% NOTE: we return all known callbacks if no input is provided

if nargin < 2
	callbacks = CBR; return;
end

%--
% handle various input forms
%--

% NOTE: this holds event-relevant callbacks for extension_type, for extension, and for particular function call in each of the previous contexts

callbacks = struct('when', when, 'extension_type', [], 'extension', [], 'extension_type_fun', [], 'extension_fun', []);

%--
% get callbacks
%--

if nargin < 4
	
	% NOTE: determine whether we have a type or a full extension as context
	
	if ischar(ext)
		type = ext; name = [];
	else
		type = ext.type; name = ext.fieldname;
	end
	
	% NOTE: first we retrieve extension-type callbacks
	
	if isfield(CBR.extension_type, type) && isfield(CBR.extension_type.(type), when)
						
		if isfield(CBR.extension_type.(type).(when), 'all')
			
			callbacks.extension_type = CBR.extension_type.(type).(when).all; % extension type
		end
		
		if ~isempty(fun) && isfield(CBR.extension_type.(type).(when), fun)
			
			callbacks.extension_type_fun = CBR.extension_type.(type).(when).(fun); % extension type fun
		end
	end
	
	% NOTE: now we retrieve extension-specific callbacks if we can
	
	if ~isempty(name)
				
		if isfield(CBR.extension, name) && isfield(CBR.extension.(name), when)
			
			if isfield(CBR.extension.(name).(when), 'all')
				
				callbacks.extension = CBR.extension.(name).(when).all; % extension
			end
			
			if ~isempty(fun) && isfield(CBR.extension.(name).(when), fun)
				
				callbacks.extension_fun = CBR.extension.(name).(when).(fun); % extension fun
			end
		end
	end
	
%--
% set callback
%--

else
	% NOTE: in this case all inputs are provided we just have to see how the callback context is described
	
	code = ~ischar(ext) + 2 * ~isempty(fun);
	
	switch code
		
		% NOTE: the first two are used for all function calls in the context
		
		case 0 % extension type
			
			CBR.extension_type.(ext).(when).all = callback;
			
		case 1 % extension
			
			CBR.extension.(ext.fieldname).(when).all = callback;
			
		% NOTE: these are called when a given function call happens in context
		
		case 2 % extension type fun
			
			CBR.extension_type.(ext).(when).(fun) = callback;
			
		case 3 % extension fun
			
			CBR.extension.(ext.fieldname).(when).(fun) = callback;
	end
end
