function root = extension_root(ext, type)

% extension_root - root directory of extension
% --------------------------------------------
%
% root = extension_root(ext, type)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  root - extension root

% TODO: extend to consider when we may have more than one extension root

%--
% handle multiple extension input
%--

if numel(ext) > 1 && nargin < 2
	root = iterate(mfilename, ext); return;
end

%--
% get base extension root
%--

root = [extensions_root(ext.subtype, ext.project), filesep, ext.name];

if (nargin < 2) || isempty(type)
	return;
end

%--
% append type 
%--

% TODO: be case-insensitive in this test

if ~ismember(type, get_extension_directories)
	error(['Unrecognized extension root directory type ''', type, '''.']);
end

if strcmp(type, 'base')
	return;
end

root = [root, filesep, type];
