function value = is_extension_type(type)

% is_extension_type - check whether proposed string is extension type
% -------------------------------------------------------------------
%
% value = is_extension_type(type)
%
% Input:
% ------
%  type - proposed type string
%
% Output:
% -------
%  value - result of test

%--
% handle multiple types using iterator
%--

if iscell(type)
	value = iterate(mfilename, type); return;
end

%--
% sanity check
%--

if ~ischar(type)
	value = 0; return;
end

%--
% check string is an extension type
%--

% NOTE: this test resolves a possible recursion problem

value = strcmp(type, 'extension_type');

if ~value
	value = string_is_member(type, get_extension_types);
end