function ecd(name, type) 

% ecd - move to extension directory
% ---------------------------------
%
% ecd name type
%
% Input:
% ------
%  name - extension name
%  type - extension type

%--
% set empty type if not provided
%--

if nargin < 2
	type = '';
end

%--
% move to all extensions root for no input
%--

if ~nargin
	cd(extensions_root); return;
end

%--
% get extension indicated and move to root if found
%--

if isstruct(name)
	ext = name;
else
	ext = get_extensions(type, 'name', name);
end

if ~isempty(ext)
	root = extension_root(ext);
	
	if iscellstr(root);
		disp('Extension name is ambiguous, indicate type as well.'); disp(' '); return;
	end
	
	disp(['Moving to ''', root, ''' ...']); disp(' ');
	
	cd(root);
end
