function ext = get_able_extensions(in, level)

% get_able_extensions - get enabled extensions that compute
% ---------------------------------------------------------
%
% ext = get_able_extensions(type, level)
%
% Input:
% ------
%  type - extension type (def: '', get all extension types)
%  ext - extension array
%
% Output:
% -------
%  ext - able extensions

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% set default to check all extensions
%--

if nargin < 1
	in = '';
end

%--
% get or rename extensions
%--

% NOTE: 'get_extensions' will check the extension type string

if ischar(in)
	ext = get_extensions(in);
else
	ext = in;
end

%--
% get developer level
%--

if nargin < 2
	level = xbat_developer;
end

%--------------------------------------------
% SELECT ABLE EXTENSIONS
%--------------------------------------------

for k = length(ext):-1:1
	
	%--
	% remove disabled
	%--
	
	% NOTE: the non-computing test is deprecated
	
	if ~ext(k).enable
		ext(k) = [];
	end

	%--
	% remove base extensions if we are not developers
	%--
	
	% NOTE: base extensions have the pattern '_BASE' in their name
	
	if (level < 1) && ~isempty(strfind(ext(k).name, '_BASE'))
		ext(k) = [];
	end
	
end