function types = get_parameter_types(ext)

% get_parameter_types - get parameter types for extension
% -------------------------------------------------------
%
% types = get_parameter_types(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  types - parameter types

% TODO: consider caching the results here, these are slowly changing

%--
% get api function names
%--

% NOTE: these signatures are cached

fun = extension_signatures(ext.subtype, ext.fun.main);

%--
% get parameter types from function names
%--

% NOTE: don't we need controls in order to have presets?

types = cell(0);

for k = 1:numel(fun)
	
	%--
	% check for a parameter create type method
	%--
	
	stop = strfind(fun{k}, 'parameter__create');
	
	% NOTE: we need the match to be unique
	
	if length(stop) ~= 1
		continue;
	end
	
	%--
	% get parent name of above parameter create
	%--
	
	if stop == 1
		types{end + 1} = 'compute'; continue;
	end
	
	start = strfind(fun{k}, '_');
	
	% TODO: this could be done using a 'find' with modifier
	
	for j = numel(start):-1:1
		
		if (start(j) < stop - 2)
			types{end + 1} = fun{k}(start(j) + 1:stop - 3); continue;
		end
		
	end
	
	% TODO: consider it this ever happens
	
	types{end + 1} = fun{k}(1:stop - 3);

end
