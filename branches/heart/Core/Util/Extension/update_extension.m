function update_extension(varargin)

% TODO: the goal of this function is to update system extension

switch length(varargin)
	
	case 1, ext = varargin{1};
		
	case 2, ext.subtype = varargin{1}; ext.name = varargin{2};
		
end

%--
% update system
%--

% NOTE: discover and update extensions cache

ext = discover_extensions(ext.subtype, ext.name); extensions_cache(ext);

%--
% update browsers
%--

% NOTE: we update the browser stores and possibly any palettes?

par = get_xbat_figs('type', 'sound');

for k = 1:length(par)
	
% 	refresh_extension(callback, context)
	
end