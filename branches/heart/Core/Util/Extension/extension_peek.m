function extension_peek(ext)

%--
% display extension info
%--

str = [' ', upper(ext.name)];

line = str_line(max(70,length(str) + 1));
		
disp(' ');
disp(line); 
disp(str); 
disp(line);

%--
% get extension function names and handles
%--

fun = flatten_struct(ext.fun);

names = fieldnames(fun);

handles = struct2cell(fun);

%--
% display extension function info
%--

disp(' ');

for k = 1:length(names)
	
	str = [' ', upper(names{k})]; 
	line = str_line(length(str) + 2);
		
	disp(line);
	disp(str); 
	disp(line);
	disp(' ');
	
	% NOTE: the xml conversion introduces a newline at the end
	
	if (~isempty(handles{k}))
		info = strrep(sprintf(to_xml(functions(handles{k}))),app_root,'$app_root');
		disp(info);	
	else
		disp('__UNDEFINED__');
		disp(' ');
	end
	
	disp(' ');

end
