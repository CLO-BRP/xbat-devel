function [value, names, store] = has_presets(ext, type)

% has_presets - whether extension may have presets
% ------------------------------------------------
%
% [value, names, store] = has_presets(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - preset type to consider (def: 'compute')
%
% Output:
% -------
%  value - indicator
%  names - names
%  store - preset storage type

%--
% set default type
%--

if nargin < 2
	types = get_parameter_types(ext); type = types{1}; % type = 'compute';
end

% TODO: efficiently check type using 'isfield' rather than get and 'ismember'

%--
% check if extension may have presets
%--

% TODO: consider how we may efficiently use an exist type test

if strcmp(type, 'compute')
	value = ~isempty(ext.fun.parameter.create) && ~isempty(ext.fun.parameter.control.create);
else
	value = ~isempty(ext.fun.(type).parameter.create) && ~isempty(ext.fun.(type).parameter.control.create);
end

if nargout < 2
	return;
end 

%--
% get preset names if needed
%--

% NOTE: we have a function 'get_preset_names', but we are not using it here

if value
	[names, store] = get_preset_files(ext, type);
else
	names = {}; store = '';
end

% TODO: we could check for both 'save' and 'load' methods and produce mismatch warning
