function ext = extension_edit_dialog(ext)

% extension_edit_dialog - edit various extension properties
% ---------------------------------------------------------
%
% ext = extension_edit_dialog(ext)
%
% Input:
% ------
%  ext - extension to edit 
%
% Output:
% -------
%  ext - result extension

%--
% handle string input
%--

% NOTE: we convert the tag into a duck

if ischar(ext)	
	ext = parse_tag(ext, '::', {'subtype', 'name', 'parent'});
end 

%--
% get and edit extension
%--

ext = get_extensions(ext.subtype, 'name', ext.name);

if isempty(ext)
	return;
end

% NOTE: clear main function, to get a fresh extension

clear(func2str(ext.fun.main));

ext = new_extension_dialog(ext.fun.main());

%--
% regenerate main
%--

regenerate_main(ext);

%--
% regenerate related menus
%--

% NOTE: we have only changed metadata, this may affect category type menus

% TODO: certainly factor this code

% TODO: this code should be more dynamic, make sure that menu creation is only that not refreshing of extension cache

par = get_xbat_figs('type', 'sound'); 

for k = 1:numel(par)
	
	current = par(k);
	
	switch ext.subtype
		
		case {'signal_filter', 'image_filter'}
			browser_filter_menu(current); 
			
		case 'sound_feature'
			browser_feature_menu(current);
			
		case 'sound_detector'
			browser_detect_menu(current);
			
		case 'event_measure'
			browser_measure_menu_2(current);
			
	end
	
end






