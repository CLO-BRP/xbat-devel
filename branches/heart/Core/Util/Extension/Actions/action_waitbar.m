function pal = action_waitbar(context, duration)

% action_waitbar - create multiple target action waitbar
% ------------------------------------------------------
%
% pal = action_waitbar(context)
%
% Input:
% ------
%  context - action context
%  duration - duration of target in time
%
% Output:
% -------
%  pal - waitbar handle

%-----------------------------
% HANDLE INPUT
%-----------------------------

if nargin < 2
	duration = [];
end

%-----------------------------
% WAITBAR CONTROLS
%-----------------------------

%--
% progress waitbar
%--

control = control_create( ...
	'name', 'PROGRESS', ...
	'alias', 'Perform Action ...', ...
	'style', 'waitbar', ...
	'confirm', 1, ...
	'lines', 1.15, ...
	'space', 2 ...
);

% NOTE: this displays time realtime for sound actions

if ~isempty(duration)
	control.units = duration;
end

% TODO: make the default value into a user option

control(end + 1) = control_create( ...
	'name', 'close_after_completion', ... 
	'style', 'checkbox', ...
	'value', 1 ...
);

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'string', 'Details' ...
);

control(end + 1) = control_create( ...
	'name', 'results', ...
	'alias', 'Results', ...
	'lines', 6, ...
	'space', 0, ...
	'confirm', 0, ...
	'style', 'listbox' ...
);

%-----------------------------
% CREATE WAITBAR
%-----------------------------

% TODO: put some string in here to indicate what we are doing



name = active_name(context);

opt = waitbar_group; % opt.show_after = 1;

pal = waitbar_group(name, control, [], [], opt);


%---------------------------
% ACTIVE_NAME
%---------------------------

function name = active_name(context)

%--
% declare special keywords to use when activating
%--

% NOTE: we capitalize to reduce hits

keyword = title_caps({'create', 'export', 'produce', 'extract'});

name = context.ext.name; active = 0;

%--
% make name active
%--

for k = 1:length(keyword)
	
	if strfind(name, [keyword{k}, ' '])
		name = strrep(name, keyword{k}, string_ing(keyword{k})); active = 1;
	end
	
end

% NOTE: when the name is all caps we do not add the 'ing'

if ~active && ~strcmp(upper(name), name)
	name = string_ing(name);
end

%--
% append object
%--

name = [name, ' ', title_caps(string_plural(context.type)), ' ...'];
