function result = log_action(varargin)

% log_action - from command line
% ------------------------------
%
% result = log_action(name, target)
%
% Input:
% ------
%  name - of action
%  target - array
%
% Output:
% -------
%  result - of action
%
% NOTE: sugar for 'run_action'

%--
% handle input
%--

if nargin < 2
	target = get_selected_log;
end

result = struct;

if isempty(target)
	return;
end

%--
% run action
%--

result = run_action('log', varargin{:});