function pal_menu(pal, str)

% pal_menu - developer menu for extension palettes
% ------------------------------------------------
% 
% pal_menu(pal, str)
%
% Input:
% ------
%  pal - palette handle
%  str - command string

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% set default command
%--

if (nargin < 2) || isempty(str)
	str = 'Initialize';
end

%--------------------------
% GET EXTENSION INFO
%--------------------------

%--
% get extension using palette tag
%--

info = parse_extension_tag(get(pal, 'tag'));

ext = get_extensions(lower(info.type), 'name', info.name);

if length(ext) > 1
	error('Unable to select unique extension by name.');
end

%--
% get main info
%--

info = functions(ext.fun.main);

ext_main_name = info.function;

ext_main = info.file;

ext_dir = extension_root(ext);

%--
% get function names and flatten extension fun structure
%--

name = extension_signatures(ext.subtype);

fun = flatten_struct(rmfield(ext.fun, 'main'));

%--
% get functions info
%--

ADD_LIST = cell(0); 

EDIT_LIST = {ext_main_name}; 

EDIT_FILE = {ext_main};

for k = 1:length(name)

	%--
	% check for availability of function by looking at handle
	%--
	
	% NOTE: this is not a normal condition and we should consider rethrow
	
	try
		f = fun.(name{k});
	catch
		extension_warning(ext, 'Extension signature mismatch.', lasterror); continue;
	end

	%--
	% unavailable function
	%--
	
	% NOTE: handle is empty, function neither exists or is inherited
	
	if isempty(f)
		ADD_LIST{end + 1} = name{k}; continue;
	end
	
	%--
	% inherited function
	%--
	
	% NOTE: function is not in extension directory, it is inherited
	
	file = get_field(functions(f), 'file');
	
	if isempty(strmatch(ext_dir, file))
		ADD_LIST{end + 1} = name{k}; continue;
	end
	
	%--
	% available function
	%--
	
	% NOTE: a non-empty handle exists in our directory
	
	EDIT_LIST{end + 1} = name{k};
	
	EDIT_FILE{end + 1} = file;
	
end

% NOTE: add when there are files to add

if ~isempty(ADD_LIST)
	ADD_LIST{end + 1} = 'Add All ...';
end

EDIT_LIST{end + 1} = 'Edit All ...';

%--
% get helper functions info
%--

% TODO: add support for multiple editable types 

% NOTE: get names of m-files in 'Helpers' directory 

HELPER_LIST = get_helpers(ext); 

private = get_helpers(ext, 'private');

if ~isempty(private)
	PRIVATE_LIST = strcat(get_helpers(ext, 'private'), {' (private)'});
else
	PRIVATE_LIST = {};
end

HELPER_LIST = sort({HELPER_LIST{:}, PRIVATE_LIST{:}});

% NOTE: add when there are multiple files to edit

if length(HELPER_LIST) > 1
	HELPER_LIST{end + 1} = 'Edit All ...';
end

%--
% get svn info
%--

% NOTE: we use a fast unreliable test here ... oh well

in_svn = is_working_copy(extension_root(ext), 1);

%--------------------------
% COMMANDS
%--------------------------

switch str
	
	%--------------------------
	% INITIALIZE
	%--------------------------
	
	case 'Initialize'
		
		%--
		% set top level names
		%--
		
		name_1 = 'EXT';
		
		name_2 = 'SVN';
		
		%--
		% check for exising menu
		%--
		
		% NOTE: we assume this indicates presence of all menus
		
		temp = findobj(pal, 'type', 'uimenu', 'label', name_1);
		
		if ~isempty(temp)
			return;
		end
		
		%------------------------
		% FILE MENU
		%------------------------
		
		%--
		% create main file menu
		%--
		
		% TODO: add help menu, this should include an 'About Extension ...'
		
		L = { ...
			name_1, ...
			'Edit ...', ...
			'(API)', ...
			'Edit', ... 
			'Diff', ... 
			'Add', ...  
			'M-Lint ...', ...
			'(Helpers)', ...
			'Edit', ...
			'Diff', ...
			'Add Helper ...', ...
			'M-Lint ...', ...
			'Refresh', ...
			'Show Files ...', ...
		};
	
		n = length(L);
		
		S = bin2str(zeros(1, n));  
		
		S{3} = 'on';
		S{8} = 'on'; 
		S{end - 1} = 'on';
		
		g = menu_group(pal, 'pal_menu', L, S);
	
		set(g(2), 'accelerator', 'E');
		
		% NOTE: disable diff when subversion is not available
		
		if isempty(tsvn_root)
			set(g([5,10]), 'enable', 'off');
		end
		
		% NOTE: add an accelerator key to refresh!
		
		set(g(13), 'accelerator', 'R');
		
		%--
		% special  menu handling
		%--
				
		% NOTE: disable header menus
		
		set(get_menu(g, '(API)'), 'enable', 'off');
		
		set(get_menu(g, '(Helpers)'), 'enable', 'off');

		% NOTE: alias menus for clarity
		
		ix = 4;
		
		API_EDIT = g(ix); 
		
		API_DIFF = g(ix + 1); 
		
		API_ADD = g(ix + 2);
		
		API_LINT = g(ix + 3);
		
		ix = 9;
		
		HELPER_EDIT = g(ix); 
		
		HELPER_DIFF = g(ix + 1);
		
		HELPER_LINT = g(ix + 3);
						
		% NOTE: tag menus to deal with label ambiguities
		
		set(API_EDIT,'tag','api edit');
		
		set(API_DIFF,'tag','api diff');
		
		set(API_LINT,'tag','api lint');
		
		set(HELPER_EDIT,'tag','helper edit');
				
		set(HELPER_DIFF,'tag','helper diff');

		set(HELPER_LINT,'tag','helper lint');
		
		%------------------------
		% API
		%------------------------
		
		%--
		% edit and diff
		%--
		
		par = [API_EDIT, API_DIFF];
		
		for k = 1:length(par)
			
			% TODO: consider a hierarchical menu here
			
			temp = menu_group(par(k), 'pal_menu', EDIT_LIST); 

			% NOTE: this separates the main function from the rest
			
			if length(temp) > 1
				set(temp(2), 'separator', 'on');
			end

			% NOTE: this tags the edit all and separates it
			
			set(temp(end), ...
				'tag', 'api', ...
				'separator', 'on' ...
			); 
		
		end
	
		delete(temp(end)); % NOTE: discard edit all from diff children
		
		%--
		% add
		%--
		
		if isempty(ADD_LIST)
			
			set(API_ADD, 'enable', 'off');
			
		else
			
			temp = menu_group(API_ADD, 'pal_menu', ADD_LIST);
			
			% NOTE: this separates the add all
			
			set(temp(end), ...
				'separator', 'on' ...
			); 
			
		end
		
		%------------------------
		% HELPER
		%------------------------
			
		%--
		% edit and diff
		%--
		
		if isempty(HELPER_LIST)
			
			set([HELPER_EDIT, HELPER_DIFF, HELPER_LINT], 'enable', 'off');
			
		else
			
			par = [HELPER_EDIT, HELPER_DIFF];

			for k = 1:length(par)
				
				temp = menu_group(par(k), 'pal_menu', HELPER_LIST);

				% NOTE: this tags the edit all and separates it
				
				set(temp(end), ...
					'tag', 'helper' ...
				);
			
				if length(temp) > 1
					set(temp(end), 'separator', 'on');
				end

			end
			
			% NOTE: discard edit all from diff children
			
			if length(HELPER_LIST) > 1
				delete(temp(end)); 
			end
			
		end
		
		%------------------------
		% TOOLS MENU
		%------------------------
		
		% TODO: consider packaging functions 
		
		% NOTE: the package is a zip with a manifest file, then we should
		% have an install function, eventually an update function for the
		% extension
		
		% NOTE: under version control what we need is to call status and
		% then update if desired, otherwise consider some version string
		% comparison
		
		% TODO: documentation generation, editing, and publishing
		
		% NOTE: the documentation generation should at least include nice
		% publishing of the code
		
		%------------------------
		% SVN MENU
		%------------------------
		
		% TODO: present choice of repository, then set external
		
		L = { ...
			name_2, ...
			'Update ...', ...
			'Status ...', ...
			'Commit ...', ...
			'Add ...', ...
			'About TSVN ...', ...
		};
	
		n = length(L);
		
		S = bin2str(zeros(1, n));
		
		S{3} = 'on';
		S{end} = 'on';
		
		g = menu_group(pal, 'pal_menu', L, S);
	
		% NOTE: disable SVN menus when subversion is not installed
		
		if isempty(tsvn_root)
			set(g, 'enable', 'off');
		else
			if ~in_svn
				set(g, 'enable', 'off'); set(g([1, n - 1, n]), 'enable', 'on');
			end
		end
		
		set(g(2), 'accelerator', 'U');
		
	%--------------------------
	% FILE COMMANDS
	%--------------------------
		
	%--------------------------
	% EDIT
	%--------------------------
	
	case 'Edit ...'
		
		% TODO: the tag on the 'action' dialogs or 'widget' displays does not conform to this approach
		
		%--
		% get required context to get extension
		%--
		
		call = get_callback_context(gcbo);
		 
		info = parse_tag(get(call.pal.handle, 'tag'), '::', {'figure', 'subtype', 'name'});
		
		%--
		% get and edit extension
		%--
		
		% NOTE: info is an extension 'duck'
		
		extension_edit_dialog(info);
		
	%------------------------
	% REFRESH
	%------------------------
	
	case 'Refresh'
		
		callback = get_callback_context(gcbo);
		
		[value, ext] = is_extension_palette(callback.pal.handle);
		
		if ~value
			return;
		end
		
		% NOTE: duck the context, really we only need to duck the extension
		
		context.ext = ext;
		
		refresh_extension(callback, context);
		
	%------------------------
	% SHOW FILES
	%------------------------
	
	case 'Show Files ...'
		
		% NOTE: we disable file selection
		
		showf(ext_main, 0);
		
	%------------------------
	% EDIT ALL
	%------------------------
	
	% TODO: extend this behavior to handle private helpers
	
	case 'Edit All ...'
		
		%--
		% get option related files
		%--
		
		% NOTE: we get the 'api' or 'helper' option context from menu tag
		
		con = get(gcbo, 'tag');
		
		switch con
			
			case 'api'
				p = [ext_dir, filesep, 'private'];
				
			case 'helper'
				p = [ext_dir, filesep, 'Helpers'];
				
			% NOTE: we simply return if we don't have an option context
			
			otherwise, return;
				
		end

		f = strcat(p, filesep, get_field(what(p), 'm'));
		
		%--
		% build and execute command string to edit files
		%--
				
		str = 'edit ';
		
		% NOTE: add main extension file to list in the 'api' option
		
		if strcmp(con, 'api')
			str = [str, '''', ext_dir, filesep, ext_main_name, ''' '];
		end
		
		for k = 1:length(f)
			str = [str, '''', f{k}, ''' '];
		end
				
		eval(str);
		
	%------------------------
	% API EDIT AND DIFF
	%------------------------
	
	case EDIT_LIST
		
		%--
		% get command from tag
		%--
		
		[ignore, com] = parse_context(get(get(gcbo, 'parent'), 'tag'));
				
		%--
		% build full filename
		%--
		
		if strcmp(str, ext_main_name)
			
			file = [ext_dir, filesep, ext_main_name, '.m'];
			
		else
			
			% NOTE: this should provide some fun in the future
			
			info = functions(fun.(str)); file = info.file;
			
% 			f = [ext_dir, filesep, 'private', filesep, str, '.m'];
			
		end
		
		%--
		% execute command
		%--
		
		switch com
			
			case 'edit', edit(file);
		
			case 'diff', tsvn('diff', file);
		
		end

	%------------------------
	% API ADD
	%------------------------
	
	%--
	% add all
	%--
	
	% NOTE: order matters here in the switch
	
	% NOTE: in this case we do not open generated files for editing
	
	case 'Add All ...'

		%--
		% generate remaining functions
		%--
		
		disp(' '); generate_function(ext); disp(' ');

		%--
		% close palette or widget
		%--
		
		par = get_xbat_figs('child', pal);
		
		if ~isempty(par)
			close(pal);
		end
		
		%--
		% update extension
		%--
		
		% NOTE: first update system cache, then browser store
		
		extensions_cache(discover_extensions(ext.subtype, ext.name));
		
		% NOTE: we only update the browser store for this browser, reconsider
		
		update_extension_store(par, ext.subtype, ext.name);
		
		%--
		% reopen palette or widget
		%--
		
		if ~isempty(par)
			extension_palettes(par, ext.name, ext.subtype);
		end
		
	%--
	% add
	%--
	
	% TODO: consider adding editing on single add
	
	case ADD_LIST
		
		%--
		% generate specific function
		%--
		
		file = generate_function(ext, str);

		%--
		% close palette or widget
		%--
		
		par = get_xbat_figs('child', pal);
		
		if ~isempty(par)
			close(pal);
		end
		
		%--
		% update extension
		%--
		
		% NOTE: first update system cache, then browser store
		
		extensions_cache(discover_extensions(ext.subtype, ext.name));
		
		% NOTE: we only update the browser store for this browser, reconsider
		
		update_extension_store(par, ext.subtype, ext.name);
		
		%--
		% reopen palette or widget
		%--
		
		if ~isempty(par)
			extension_palettes(par, ext.name, ext.subtype);
		end
		
		%--
		% open file for editing
		%--
		
		% NOTE: the pause allows us to see the re-opened palette before the editor
		
		if ~isempty(file) 
			pause(1); edit(file);
		end
		
	%--
	% api add helper
	%--
	
	case 'update_api'

		%--
		% delete and update edit, diff, and add items
		%--
		
		% EDIT
		% ----
		
		g = findobj(pal, 'tag', 'api edit');
		
		if ~isempty(g)

			delete(allchild(g));

			temp = menu_group(g, 'pal_menu', EDIT_LIST);

			if (length(temp) > 1)
				set(temp(2), 'separator', 'on');
			end

		end
		
		set(temp(end), 'separator', 'on');
		
		% DIFF
		% ----
		
		g = findobj(pal, 'tag', 'api diff');
		
		if ~isempty(g)

			delete(allchild(g));

			temp = menu_group(g, 'pal_menu', EDIT_LIST);

			if (length(temp) > 1)
				set(temp(2), 'separator', 'on');
			end

		end
		
		delete(temp(end)); % NOTE: discard edit all from diff children
		
		% ADD
		%----
		
		g = get_menu(pal, 'Add');

		if ~isempty(g)

			delete(allchild(g));

			if ~isempty(ADD_LIST)
				set(g, 'enable', 'on'); menu_group(g, 'pal_menu', ADD_LIST);
			else
				set(g, 'enable', 'off');
			end

		end

	%------------------------
	% HELPER EDIT AND DIFF
	%------------------------
	
	case HELPER_LIST
		
		%--
		% get command option
		%--
		
		[ignore, com] = parse_context(get(get(gcbo, 'parent'), 'tag'));

		%--
		% build full filename
		%--
		
		start = strtok(str, ' ');

		% NOTE: this will change if we allow other editable types
		
		if strcmp(start, str)
			f = [ext_dir, filesep, 'Helpers', filesep, start, '.m'];
		else
			f = [ext_dir, filesep, 'private', filesep, start, '.m'];
		end
				
		%--
		% execute command
		%--
		
		switch com
			
			case 'edit', edit(f);
				
			case 'diff', tsvn('diff', f);
		
		end
		
	%------------------------
	% ADD HELPER
	%------------------------
	
	%--
	% add helper
	%--
	
	case 'Add Helper ...'

		% NOTE: the condition test involves the creation of the helper, the body updates the helper menu if needed
		
		if ~isempty(new_helper_dialog(ext))
			
			pal_menu(pal, 'update_helper_edit');
			
		end
		
	%--
	% add helper helper
	%--
	
	case 'update_helper_edit'

		%--
		% update edit and diff children
		%--
		
		% EDIT
		%-----
		
		g = findobj(pal, 'tag', 'helper edit');
		
		if ~isempty(g)
			
			delete(allchild(g));
			
			set(g, 'enable', 'on');
			
			temp = menu_group(g, 'pal_menu', HELPER_LIST); 

			if length(temp) > 2
				set(temp(end), 'separator', 'on');
			end
			
		end	
		
		% DIFF
		%-----
		
		g = findobj(pal, 'tag', 'helper diff');
		
		if ~isempty(g)
			
			delete(allchild(g));

			set(g, 'enable', 'on');
			
			temp = menu_group(g, 'pal_menu', HELPER_LIST); 

			if length(temp) > 2
				set(temp(end), 'separator', 'on');
			end
			
		end	
		
		% TODO: handle edit all ...
		
	%------------------------
	% MLINT
	%------------------------
		
	case 'M-Lint ...'
		
		%--
		% get context from menu tag
		%--
		
		con = parse_context(get(gcbo, 'tag'));

		%--
		% build path 
		%--
		
		p = extension_root(ext);
		
		switch con
			
			case 'api', p = [p, filesep, 'private'];
				
			case 'helper', p = [p, filesep, 'Helpers'];
				
		end
		
		%--
		% produce lint report
		%--
			
		mlintrpt(p, 'dir');
	
	%-----------------------------------------------------------
	% SVN COMMANDS
	%-----------------------------------------------------------
	
	% TODO: this operation needs to refresh extension
	
	case 'Update ...', tsvn('update', ext_dir);
	
	case 'Status ...', tsvn('status', ext_dir);
		
	% NOTE: this operation is better as a background operation
	
	case 'Add ...', [status, result] = svn('add', ext_dir); reopen_palette(pal); %#ok<NASGU>
	
	case 'Commit ...', tsvn('commit', ext_dir);
		
	case 'Revert ...', tsvn('revert', ext_dir); reopen_palette(pal);
		
	%--
	% about tortoise
	%--
	
	case 'About TSVN ...', tsvn('about');
		
end

%-------------------------------------------
% REOPEN PALETTE
%-------------------------------------------

% NOTE: we may only need to recreate the SVN menu

function reopen_palette(pal)

%--
% get parent
%--

par = get_palette_parent(pal);

% NOTE: we will not know how to re-open in this case, return

if isempty(par)
	return;
end

%--
% parse tag to know how to re-open
%--

tag = get(pal, 'tag'); info = parse_tag(tag, '::', {'type', 'type', 'name'});

close(pal);

extension_palettes(par, info.name, info.type);


%-------------------------------------------
% PARSE_CONTEXT
%-------------------------------------------

function [con, com] = parse_context(tag)

% parse_context - get context and command from menu tag
% -----------------------------------------------------
%
% [con, com] = parse_context(tag)
%
% Input:
% ------
%  tag - menu tag
%
% Output:
% -------
%  con - context
%  com - command

%--
% parse string
%--

% NOTE: tags are of the form 'context command' we only need to split

out = str_split(tag);

%--
% output context and command if needed
%--

con = out{1};

if (nargout > 1)
	if (length(out) > 1)
		com = out{2};
	else
		com = '';
	end
end


%-------------------------------------------
% UPDATE_RELEVANT_EXTENSIONS
%-------------------------------------------

% TODO: the various update functions could be unified

function update_relevant_extensions(ext) %#ok<DEFNU>

%--
% update system extensions
%--

get_extensions('!');

%--
% update interface and extension registries
%--

% NOTE: this will zap state for all extensions of relevant type 

switch ext.subtype
	
	case {'image_filter', 'signal_filter'}
		
		update_filter_menu;
		
	case 'sound_detector'
		
		update_detect_menu;
		
	case 'widget'
		
		% NOTE: this updates stores of all open browsers
		
		update_extension_store([], 'widget'); browser_window_menu;
		
	otherwise
		
		% NOTE: the extensions will be fresh, the menus might be stale
		
		update_extension_store([], ext.subtype);
		
end
		

% function toggle_debug(obj, eventdata)
% 
% state = strcmp(get(obj, 'check'), 'on');
% 
% if state
% 	set(obj, 'check', 'off', 'label', 'DEBUG (Off)');
% else
% 	set(obj, 'check', 'on', 'label', 'DEBUG (On)');
% end


%-------------------------------------------
% HIERARCHICAL_MENU
%-------------------------------------------

function handles = hierarchical_menu(par, callback, label) 

for k = 1:numel(label)
	
	parts = str_split(label, '__');
	
	current = create_menu(par, parts{1}, 'label', title_caps(parts{1}));
	
	for j = 2:numel(parts)
		current = create_menu(current, parts{j}, 'label', title_caps(parts{j}));
	end
	
	set(current, 'tag', 'label', 'callback', @pal_menu_callback);
	
end
	
	




