function value = can_compute(ext)

% can_compute - indicate extension can compute
% --------------------------------------------
%
% value = can_compute(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  value - compute indicator

value = ~isempty(ext.fun.compute);
