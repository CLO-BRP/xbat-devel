function pal = open_extension_palette(ext, context, par, widget)

% open_extension_palette - open palette for extension
% ---------------------------------------------------
%
% pal = open_extension_palette(ext, context, par)
%
% Input:
% ------
%  ext - extension
%  context - context
%  par - browser
%
% Output:
% -------
%  pal - handle

%----------------
% HANDLE INPUT
%----------------

%--
% get parent from context if possible, otherwise default to active browser
%--

% NOTE: the context parent will be overridden by an input parent

if nargin < 3
	par = ternary(isfield(context, 'par') && ~isempty(context.par), context.par, get_active_browser);
end 

if isempty(par)
	return;
end

if nargin < 4
	widget = par;
end 

%--
% get extension from context if needed
%--

% NOTE: the context extension will be overridden by an input extension

if isempty(ext)
	ext = ternary(isfield(context, 'ext') && ~isempty(context.ext), context.ext, []);
end

if isempty(ext)
	return;
end 

%----------------
% OPEN PALETTE
%----------------

% TODO: currently we always enforce a singleton condition, make it optional

%--
% see if palette exists, ensure singleton condition and position
%--

pal = get_extension_palette(ext, par);

if ~isempty(pal)
	position_palette(pal, widget); return;
end

%--
% get palette ingredients using extension and context
%--

control = get_extension_controls(ext, context);

opt = get_extension_control_options(ext, context);

%--
% create and tag palette
%--

pal = control_group(par, '', ext.name, control, opt);

% NOTE: the tag used for the palette is important in various places

set(pal, ...
	'tag', get_extension_tag(ext, par), ... 
	'keypressfcn', {@browser_keypress_callback, par}, ...
	'closerequestfcn', {@delete_palette, par} ...
);

%--
% position using browser state
%--

position_browser_palette(par, pal);

% TODO: this should be reimplemented as a widget event

update_selection_buttons(par);
