function varargout = extension_call(ext, name, varargin)

% extension_call - dispatcher
% ---------------------------
%
% varargout = extension_call(ext, name, varargin)
%
% Input:
% ------
%  ext - extension
%  name - of function
%  varargin - inputs required for function
%
% Output:
% -------
%  varargout - outputs provided by method
%
% See also: extension_callbacks

%--
% get function handle for call by name
%--

% TODO: consider how to handle failure to get handle

[fun, field] = get_extension_fun(ext, name);

%--
% call extension method
%--

% TODO: would these callbacks like to communicate something?

try
	execute_callbacks('before', ext, fun, varargin{:});
	
	% TODO: consider gathering elapsed time data here and passing it along to the callback 
	
	[varargout{1:nargout}] = fun(varargin{:});
	
	execute_callbacks('after', ext, fun, varargin{:});
catch
	execute_callbacks('error', ext, fun, varargin{:});
	
	extension_warning(ext, warning_string(field), lasterror); %#ok<LERR>
end


%-----------------------------
% GET_EXTENSION_FUN
%-----------------------------

function [fun, field] = get_extension_fun(ext, name)

%--
% get hierarchy fields
%--

field = parse_fun_name(name);

%--
% get handle from structure
%--

switch numel(field)
	
	case 1
		fun = ext.fun.(field{1});
		
	case 2
		fun = ext.fun.(field{1}).(field{2});
		
	case 3
		fun = ext.fun.(field{1}).(field{2}).(field{3});
		
	case 4
		fun = ext.fun.(field{1}).(field{2}).(field{3}).(field{4});
		
	case 5
		fun = ext.fun.(field{1}).(field{2}).(field{3}).(field{4}).(field{5});
		
	otherwise
		current = ext.fun;
		
		for k = 1:length(field)
			current = current.(field{k});
		end
end


%-----------------------------
% EXECUTE_CALLBACKS
%-----------------------------

function execute_callbacks(when, ext, fun, varargin)

%--
% pack call data for callback
%--

% NOTE: this is where we define the interface for extension callbacks
	
call.when = when; call.ext = ext; call.fun = fun;
	
switch when
	
	case 'error'
		% NOTE: we rely on MATLAB to properly set 'lasterror'
		
		call.failure = lasterror; %#ok<LERR>
end

% NOTE: callbacks may be registered for specific extensions or extension types and considering specific calls or all

callbacks = extension_callbacks(when, ext, fun);

% fields = fieldnames(callbacks);

fields = {'extension_type', 'extension_type_fun', 'extension', 'extension_fun'};

for k = 1:numel(fields)
	
	field = fields{k};
	
	if isempty(callbacks.(field))
		continue;
	end
	
	call.match = field;
	
	% TODO: should we handle exceptions here?
	
	eval_callback(callbacks.(field), call, varargin);
end


%-----------------------------
% WARNING_STRING
%-----------------------------

function str = warning_string(field)

% NOTE: this should provide something sufficiently close to english

str = [str_implode(field), ' failed.']; str(1) = upper(str(1));


%-----------------------------
% PARSE_FUN_NAME
%-----------------------------

function field = parse_fun_name(name)

% TODO: we probably should only handle dot separated names

%--
% handle dot and space separated names
%--

% NOTE: this is the typical case, another typical case is the simple one

if has_dot(name)
	
	field = textscan(name, '%s', 'delimiter', '.'); field = field{1}; return;
end

% if has_space(name)
% 	
% 	field = textscan(name, '%s'); field = field{1}; return;
% end
% 
% %--
% % handle double underscore separated names
% %--
% 
% if has_underscore(name)
% 		
% 	field = textscan(strrep(name, '__', ' '), '%s'); field = field{1}; return;
% end

% NOTE: this is the simple name case

field = {name};


% NOTE: both of the functions below scan the string until they find the first symbol occurence



