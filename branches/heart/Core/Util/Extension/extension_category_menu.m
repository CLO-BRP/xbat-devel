function handles = extension_category_menu(par, ext, opt)

% extension_category_menu - create category menus for extensions
% --------------------------------------------------------------
%
% handles = extension_category_menu(par, ext, opt)
%
% Input:
% ------
%  par - parent for menu
%  ext - extensions array
%  opt - options
%
% Output:
% -------
%  handles - extension menu handles

% TODO: allow ordering of menus in alphabetical order while it makes sense

%------------------
% HANDLE INPUT
%------------------

%--
% set and possibly output default options
%--

if (nargin < 3) || isempty(opt)
	
	opt = struct; opt.active = 1;
	
	opt.callback_helper = @extension_dispatcher;
	
	if ~nargin
		handles = opt; return;
	end
end

%--
% get extensions by type if needed
%--

if ischar(ext)
	
	type = ext;
	
	if ~is_extension_type(type)
		error('Unrecognized extension type.');
	end
	
	ext = get_extensions(type);
	
elseif iscellstr(ext)
	
	types = ext; ext = [];
	
	for k = 1:length(types)
		ext = [ext, get_extensions(types{k})];
	end
	
	opt.active = 0; handles = extension_category_menu(par, ext, opt); return;
	
else
	
	% TODO: extend to do handle multiple extension types, use options
	
	if length(unique({ext.subtype})) > 1	
		opt.active = 0; handles = multiple_type_menu(par, ext, opt); return;	
	end
	
end

%------------------
% SETUP
%------------------

handles = [];

%--
% get extension categories
%--

category = get_extension_categories(ext);

% TODO: use options to control the creation of these 'active' like menus

%--
% create active menu
%--

active = strcmpi(category(1).name, 'all') && (isfield(opt, 'active') && opt.active);

base_head = 0;

if active

	cat = uimenu(par, 'label', 'Active'); 
	
	% NOTE: in the case of the active menu we should check the extensions active flag
	
	for j = 1:length(category(1).children)
		
		name = category(1).children{j}; type = category(1).types{j};
		
		if is_base_name(name) && ~base_head
			
			handles(end + 1) = uimenu(cat, 'label', '(DEV)', 'enable', 'off', 'separator', 'on'); base_head = 1;
		end
		
		current = get_extensions(type, 'name', name);
		
		handles(end + 1) = uimenu(cat, ...
			'label', name, 'callback', {@menu_callback, opt.callback_helper, type, 'active', name} ...
		); %#ok<AGROW>

		% NOTE: this is an experiment, we are not checking whether this is clobbering an important accelerator, whether it is already taken
		
		current = get_extensions(type, 'name', name);
		
		if ~isempty(current.accelerator)
			set(handles(end), 'accelerator', current.accelerator);
		end
	end
	
end

%--
% create category menus
%--

head = uimenu(par, ...
	'label', ['(', title_caps(strtok(category(1).types{1}, '_')), ')'], ...
	'enable', 'off' ...
);

% head = uimenu(par, ...
% 	'label', ['(', title_caps(category(1).types{1}), ')'], ...
% 	'enable', 'off', ...
% 	'separator', 'on' ...
% );


if active % && exist('head', 'var')
	set(head, 'separator', 'on');
end
	
for k = 1:length(category)
	
	%--
	% attach category parent to parent
	%--
	
	cat = uimenu(par, 'label', [category(k).name, '    ']); 
	
% 	% NOTE: separate the category menus from the active menu
% 	
% 	if (k == 1) && active
% 		set(cat, 'separator', 'on');
% 	end
	
	%--
	% attach children to category parent
	%--
	
	base_head = 0;
	
	for j = 1:length(category(k).children)
		
		name = category(k).children{j}; type = category(k).types{j};
		
		if is_base_name(name) && ~base_head
			handles(end + 1) = uimenu(cat, 'label', '(DEV)', 'enable', 'off', 'separator', 'on'); base_head = 1;
		end
		
		handles(end + 1) = uimenu(cat, ...
			'label', [name, ' ...'], 'callback', {@menu_callback, opt.callback_helper, type, 'open', name} ...
		); %#ok<AGROW>
	end
	
end


%------------------------------------
% MENU_CALLBACK
%------------------------------------

function menu_callback(obj, eventdata, helper, type, request, name) %#ok<INUSL>

% NOTE: 'type, request, name' is analogous to 'controller/action/parameter'

par = ancestor(obj, 'figure'); 

% TODO: we call the general dispatcher first and it may delegate to the type dispatcher

helper(type, name, request, par)

% extension_dispatcher(type, name, request, par);


%------------------------------------
% MULTIPLE_TYPE_MENU
%------------------------------------

% TODO: this is just one possibility, use options to allow various types

function handles = multiple_type_menu(par, ext, opt)

%--
% order types in verb noun order
%--

types = unique({ext.subtype});

for k = 1:length(types) 
	part = str_split(types{k}, '_'); order{k} = [part{2}, part{1}];
end

[ignore, ix] = sort(order); types = types(ix);

%--
% create menus
%--

handles = []; last_verb = '';

for k = 1:length(types) 
	
	%--
	% create type verb header
	%--
	
	type = types{k}; 
	
	part = str_split(type, '_'); verb = part{2};
	
	if ~strcmp(verb, last_verb)
	
		label = upper(string_plural(verb));

		handles(end + 1) = uimenu(par, ...
			'label', ['(', title_caps(label), ')'], ...
			'enable', 'off', ...
			'separator', 'on' ...
		); %#ok<AGROW>
	
		last_verb = verb;
	end

	%--
	% create extension category menu for extensions of type
	%--
	
	% NOTE: we should probably supress the 'active'-like menus
	
	handles = [handles, extension_category_menu(par, ext(strcmp({ext.subtype}, type)), opt)];	 %#ok<AGROW>
end


