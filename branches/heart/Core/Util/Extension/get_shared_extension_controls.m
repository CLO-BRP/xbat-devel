function control = get_shared_extension_controls(ext, context)

% get_shared_extension_controls - get controls shared by type
% -----------------------------------------------------------
%
% control = get_shared_extension_controls(ext, context)
%
% Input:
% ------
%  ext - extension
%  context - context
%
% Output:
% -------
%  control - shared controls array

%--
% set header
%--

control = empty(control_create);

% NOTE: the name used for this header is often a shortened version of the type

control(end + 1) = control_create( ...
	'style', 'separator', ...
	'type', 'header', ...
	'string', get_extension_action_name(ext) ...
);

%--
% get parent type controls, these are shared by all extensions of type
%--

type = get_extensions('extension_type', 'name', title_caps(ext.subtype));

part = empty(control_create);

if has_any_presets(type)
	
	%--
	% get type parameters in context and create controls
	%--
	
	% TODO: we should be storing the computed parameters in the type extension
	
	try
		parameter = type.fun.parameter.create(context);
	catch
		parameter = struct; extension_warning(type, 'Failed to create parameters', lasterror);
	end
	
	% NOTE: these controls are for the consumption of the children
	
	try
		part = type.fun.parameter.control.create(parameter, context);
	catch
		extension_warning(type, 'Failed to create controls.', lasterror);
	end

	%--
	% adjust spacing, concatenate, and set callbacks
	%--
	
	if ~isempty(part)

		control(end) = adjust_control_space(control(end), part(1));

		for k = 1:length(part)

			part(k).callback = {@type_callback_router, ext, type};

			control(end + 1) = part(k); %#ok<AGROW>

		end

	end

end

%--
% add debug controls if needed
%--

if xbat_developer
		
	offset = 0.5 * 0.75;

	% TODO: this looks a bit unfinished when there are no shared controls
	
	control(end + 1) = control_create( ...
		'style', 'separator', ...
		'type', 'hidden_header' ...
	);

	control(end + 1) = control_create( ...
		'style', 'separator', ...
		'space', 1 + offset ...
	);

	control(end + 1) = control_create( ...
		'name', 'debug', ...
		'alias', 'DEBUG', ...
		'style', 'checkbox' ...
	);

% 	'callback', {@debug_callback, context} ...

	control(end).space = -(1 + offset);

	control(end + 1) = control_create( ...
		'name', 'refresh', ...
		'alias', 'REFRESH', ...
		'style', 'buttongroup', ...
		'align', 'right', ...
		'lines', 1.75, ...
		'width', 0.5, ...
		'callback', {@refresh_callback, context} ...
	);

end


%---------------------------------
% TYPE_CALLBACK_ROUTER
%---------------------------------

function result = type_callback_router(obj, eventdata, ext, type) %#ok<INUSL>

result = struct;

%--
% pack callback
%--

callback = get_callback_context(obj, eventdata);

%--
% get fresh context and extension
%--

par = callback.par.handle; 

[ext, ignore, context] = get_browser_extension(ext.subtype, par, ext.name);

%--
% execute type control callback
%--

try
	result = type.fun.parameter.control.callback(callback, context);
catch
	extension_warning(type, ['Type control callback failed for ''', ext.name, '''.'], lasterror);
end


%---------------------------------
% REFRESH_CALLBACK
%---------------------------------

function refresh_callback(obj, eventdata, context) %#ok<INUSL>

%--
% duck callback context for refresh extension
%--

callback.pal.handle = ancestor(obj, 'figure');

callback.par.handle = context.par;

%--
% refresh extension
%--

% NOTE: this functions is designed for use in an extension callback

refresh_extension(callback, context);


%---------------------------------
% DEBUG_CALLBACK
%---------------------------------

% function debug_callback(obj, evendata, context)

% NOTE: currently we have no use for this method, but we may in the future

