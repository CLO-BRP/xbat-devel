function fun = attach_fun(main, type)

% attach_fun - attach defined functions to extension
% --------------------------------------------------
%
% fun = attach_fun(main, type)
%
% Input:
% ------
%  main - handle to main extension function
%  type - extension type
%  
% NOTE: return empty function structure when main is not available

%------------------------
% HANDLE INPUT
%------------------------

% NOTE: if we have a type but no main, we output the struct with no handles

% TODO: figure out if this happens anywhere, perhaps put some trace code

if (nargin < 2) || isempty(main)
	
	fun = clear_struct(get_extension_type_signatures(type)); fun.helper = struct; return;
	
end

%------------------------
% ATTACH FUNCTIONS
%------------------------

%--
% get names of extension methods
%--

% NOTE: extension function names are computed from a flat fun structure

name = extension_signatures(type, main);

%--
% look for all extension functions in private 
%--

% NOTE: on read-only media, the extensions must have all their directories

root = create_dir([fileparts(which(func2str(main))), filesep, 'private']);

if isempty(root)
	error('Problem attaching extension function handles.');
end

curr = pwd; cd(root);

% NOTE: this code talks to the filesystem 'once' to get the contents of the root

content = what(root); 

% files = file_ext({content.m{:}, content.p{:}});

files = file_ext([content.m, content.p]);

% [ignore, ix] = intersect(name, files); ix = sort(ix);

ix = find(string_is_member(name, files));

%--
% attach method handles
%--

for k = 1:length(name)
	
	if isempty(ix)
		
		fun.(name{k}) = [];	
	else	
		if k == ix(1)
			fun.(name{k}) = str2func(name{k}); ix(1) = [];
		else
			fun.(name{k}) = [];
		end
	end
end

%--
% attach helper handles
%--

ix = find(~string_is_member(files, name));

fun.helper = struct;

for k = 1:length(ix)
	helper = files{ix(k)}; fun.helper.(helper) = str2func(helper);
end

%--
% collapse to get hierarchical structure
%--

cd(curr);

% NOTE: we collapse this structure to get a proper fun structure

fun = collapse(fun);
