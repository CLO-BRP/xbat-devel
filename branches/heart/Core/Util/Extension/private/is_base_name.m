function value = is_base_name(name)

% NOTE: a base name is all upper, with some underscores, and no spaces

value = strcmp(upper(name), name) && sum(name == '_') && ~sum(name == ' ');