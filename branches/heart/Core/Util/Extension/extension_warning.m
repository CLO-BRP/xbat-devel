function extension_warning(ext, str, info)

% extension_warning - produce warning for extension failure
% ---------------------------------------------------------
%
% extension_warning(ext, str, info)
%
% Input:
% ------
%  ext - extension producing message
%  str - message string (def: none);
%  info - error info struct as provided by 'lasterror' (def: lasterror)

% TODO: use nice catch logging, consider local and centralized stores

% return;

%--
% get error info if needed
%--

% NOTE: this explains the order of the arguments

if nargin < 3
	info = lasterror;
end

if nargin < 2
	str = '';
end

% TODO: can we have an active extension? perhaps, through an API calling layer

%--
% put warning together using extension
%--

type_str = upper(strrep(ext.subtype, '_', ' ')); 

name_str = upper(ext.name);

str = ['WARNING: In ', type_str, ' extension ''', name_str, '''. ', str];

%--
% call nice catch
%--

nice_catch(info, str);
	
