function [name, args, sig, out] = extension_signatures(type, main)

% extension_signatures - generate extension function declarations
% ---------------------------------------------------------------
%
% [name, args, sig, out] = extension_signatures(type, main)
%
% Input:
% ------
%  type - extension type
%  main - main of 'extension_type' extension
%
% Output:
% -------
%  name - function names
%  args - function arguments
%  sig - function declarations
%  out - statements to produce trivial output

%------------------------
% HANDLE INPUT
%------------------------

%--
% normalize and check extension type
%--

type = type_norm(type);

%------------------------
% GET SIGNATURES
%------------------------

%--
% get function names and argument descriptions
%--

% NOTE: function names are computed from the structure of the type fun

% NOTE: input and output names are the type fun content

persistent TYPE_NAMES TYPE_ARGS;

if isempty(TYPE_NAMES) || ~isfield(TYPE_NAMES, type)
	
	%--
	% get type signatures from extension type
	%--
	
	fun = flatten(get_extension_type_signatures(type));
	
	name = fieldnames(fun); args = struct2cell(fun); 
	
	%--
	% add type function names and argument descriptions to cache
	%--
	
	TYPE_NAMES.(type) = name; TYPE_ARGS.(type) = args;
	
else
	
	%--
	% get function names and argument descriptions from cache
	%--
	
	name = TYPE_NAMES.(type); args = TYPE_ARGS.(type);
	
end

%--
% get action descriptions more dynamically
%--

% NOTE: we get actions as well in the case of 'extension_type' extensions
	
if strcmp(type, 'extension_type')

	[actions, arg] = get_extension_type_actions(main); 
	
	if numel(actions)
		
		actions = strcat('on__', actions);

		name = {name{:}, actions{:}}'; args = {args{:}, arg{:}}'; 
		
		[name, ix] = sort(name); args = args(ix);
		
	end
	
end

if nargout && nargout < 3
	return;
end

%--
% build function declarations using names and argument descriptions
%--
	
sig = cell(size(name));

for k = 1:length(name)

	if isempty(args{k})
		sig{k} = name{k}; continue;
	end

	% NOTE: if argument information is available it must contain output

	if isempty(args{k}{1})
		sig{k} = name{k};
	else
		sig{k} = [args_to_str(args{k}{1}, 'out'), ' = ', name{k}];
	end
	
	if length(args{k}) > 1
		sig{k} = [sig{k}, args_to_str(args{k}{2}, 'in')];
	end

	%--
	% generate trivial body for function
	%--

	out{k} = args_to_out(args{k});
	
end

%--
% display signatures
%--

if ~nargout
	
	disp(' '); 
	
	for k = 1:numel(name)
		disp(name{k}); disp(['    ', sig{k}]); disp(' ');
	end
	
	clear name;
	
end


%----------------------------------------------------------
% ARGS_TO_STR
%----------------------------------------------------------

function str = args_to_str(args, type)

%--
% return empty on empty
%--

str = '';

if isempty(args)
	return;
end

if ischar(args)
	args = {args};
end

%--
% put arguments names into comma separated list
%--

for k = 1:(length(args) - 1)
	str = [str, args{k}, ', '];
end

str = [str, args{end}];

%--
% add brackets based on type of arguments
%--

switch type
	
	case 'out'
		if length(args) > 1
			str = ['[', str, ']'];
		end
		
	case 'in'
		str = ['(', str, ')'];
		
end


%----------------------------------------------------------
% ARGS_TO_OUT
%----------------------------------------------------------

function out = args_to_out(args)

%--
% get input and output
%--

input = args{2}; output = args{1};

%--
% return empty on empty output
%--

out = '';

if isempty(output)
	return;
end

%--
% state something for each output
%--

for k = 1:length(output)
	
	%--
	% pass through inputs that appear as outputs
	%--
	
	% NOTE: these should not be emptied without provocation
	
	if string_is_member(output{k}, input)
		continue;
	end
	
	%--
	% handle known output arguments
	%--
	
	switch output{k}
		
		%--
		% recognized objects
		%--
		
		% NOTE: this should develop as basic objects develop
		
		case 'event'
			
			part = 'event = empty(event_create)';
			
		case 'control'
			
			part = 'control = empty(control_create)';
			
		case {'opt', 'parameter', 'attribute', 'result', 'feature', 'fun', 'store', 'value'}
			
			% NOTE: options and parameters are structs so they can be merged
			
			part = [output{k}, ' = struct'];
			
		%--
		% generic outputs
		%--
		
		% NOTE: the generic empty is the empty array
		
		otherwise

			part = [output{k}, ' = []'];

	end
	
	out = [out, part, '; '];
	
end

% NOTE: remove trailing space

if ~isempty(out)
	out(end) = []; 
end
