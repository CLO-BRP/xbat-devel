
// System
#include "math.h"

// Mathworks
#include "mex.h"

// Nvidia
#include "cuda_runtime.h"
#include "cuda_amplitude.h"

// Convert float to double on host
void float2double(double *d, float *f, int count) {
	int i;
	
	for (i = 0; i < count; i++) {
		d[i] = (double) f[i];
	}
}

// Convert double to float on host
void double2float(float *f, double *d, int count) {
	int i;
	
	for (i = 0; i < count; i++) {
		f[i] = (float) d[i];
	}
}

//--------------------------------------
// MEX FUNCTION
//--------------------------------------

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	
	// result, means and extremes
	mwSize			nM;
	double			*hM;
	float			*dM = NULL;
	
	mwSize			nE;
	double			*hE;
	float			*dE = NULL;
	
	// input
	mwSize			nX;
	void			*hX;
	float			*dX = NULL;
	
	// window
	mwSize			nH;
	double			Sh, O; // NOTE: these are not currently used
	mwSize			block_size, block_overlap;
	
	cudaError_t		cudaErr = cudaSuccess;
	
	//--
	// HANDLE INPUT
	//--
	
	if (nlhs < 1) {
		return;
	}
	
	// Signal
	
	hX = (void *) mxGetData(prhs[0]);
	
	nX = (mwSize) (mxGetM(prhs[0]) * mxGetN(prhs[0]));
	
	// Window size and overlap
	
	nH = (mwSize) mxGetScalar(prhs[1]);
	
	block_size = nH;
	
	block_overlap = nrhs > 2 ? (mwSize) mxGetScalar(prhs[2]) : 0; // NOTE: get block overlap or set no overlap default
	
	//--
	// ALLOCATE OUTPUT
	//--
	
	// NOTE: compute the number of output points, the expressions are equivalent
	
	nM = nE = (mwSize) ((nX - block_size) / (block_size - block_overlap) + 1);
	
	plhs[0] = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
	mxSetM(plhs[0], nM); mxSetN(plhs[0], 2);
	hM = (double *) mxMalloc(nM * 2 * sizeof(double));
	
	if (nlhs > 1) {
		plhs[1] = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
		mxSetM(plhs[1], nE); mxSetN(plhs[1], 2);
		hE = (double *) mxMalloc(nE * 2 * sizeof(double));
	}
	
	// Allocate device memory for result
	
	if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dM, nM * 2 * sizeof(float)); }
	
	if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dE, nE * 2 * sizeof(float)); }
	
	// Allocate device memory for input, copy to device, and compute
	
	if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS) {
		if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dX, nX * sizeof(float)); }
		
		if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(dX, hX, nX * sizeof(float), cudaMemcpyHostToDevice); }
		
		cuda_amplitude_single(dM, dE, (float *) dX, nX, block_size, block_overlap);
	}
	else /* if (mxGetClassID(prhs[0] == mxDOUBLE_CLASS) */ {
		float *tempX = (float *) malloc(nX * sizeof(float));
		
		double2float(tempX, (double *) hX, nX);
		
		if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **) &dX, nX * sizeof(float)); }
		
		if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(dX, tempX, nX * sizeof(float), cudaMemcpyHostToDevice); }
		
		free(tempX);
		
		cuda_amplitude_single(dM, dE, dX, nX, block_size, block_overlap);
	}
	
	// Copy result from device
	
	if (cudaErr == cudaSuccess) {
		float *tempM = (float *) malloc(nM * 2 * sizeof(float));
		
		cudaErr = cudaMemcpy(tempM, dM, nM * 2 * sizeof(float), cudaMemcpyDeviceToHost);
		
		float2double(hM, tempM, nM * 2);
		
		free(tempM);
	}
	
	if (cudaErr == cudaSuccess && nlhs > 1) {
		float *tempE = (float *) malloc(nE * 2 * sizeof(float));
		
		if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(tempE, dE, nE * 2 * sizeof(float), cudaMemcpyDeviceToHost); }
		
		float2double(hE, tempE, nE * 2);
		
		free(tempE);
	}
	
	// Assign to Matlab variables
	
	mxSetData(plhs[0], hM);
	
	if (nlhs > 1) { mxSetData(plhs[1], hE); }
	
	// Free device memory
	
	if (dX != NULL) { cudaFree(dX); }
	
	if (dE != NULL) { cudaFree(dE); }
	
	if (dM != NULL) { cudaFree(dM); }
	
}
