#include "math.h"

#include "mex.h"

#include "cuda_runtime.h"

#include "cuda_pamplitude.h"

// #define DEBUG
#ifdef DEBUG

#ifndef MIN
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif /* MIN */

#endif /* DEBUG */

//--------------------------------------
// MEX FUNCTION
//--------------------------------------

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	// result, means and extremes
	unsigned int	nM;
	float			*hM;
	float			*dM;

	unsigned int	nE;
	float			*hE;
	float			*dE;

	// input
	unsigned int	nX;
    unsigned char	*hX;
	unsigned char	*dX;
    
	// window
	unsigned int	nH;
	double			Sh, O; // NOTE: these are not currently used
	unsigned int	block_size, block_overlap;
	
#ifdef DEBUG
	unsigned int maxSamplesPerBlock;
	unsigned int windowsPerBlock;
	unsigned int samplesPerBlock;
	unsigned int smemPerBlock;
	unsigned int gridSize;
	unsigned int threadsPerBlock;
#endif /* DEBUG */
	
	//--
	// HANDLE INPUT
	//--
	
	if (nlhs < 1) {
        return;    
	}
	
	// Signal

	hX = (unsigned char *) mxGetData(prhs[0]); 
	
	nX = mxGetM(prhs[0]) * mxGetN(prhs[0]);

	// Window size and overlap

	nH = (int) mxGetScalar(prhs[1]);
    
	block_size = nH;

	block_overlap = nrhs > 2 ? (int) mxGetScalar(prhs[2]) : 0; // NOTE: get block overlap or set no overlap default

	//--
	// ALLOCATE OUTPUT
	//--
	
	// NOTE: compute the number of output points, the expressions are equivalent

	nM = nE =  (nX - block_size) / (block_size - block_overlap) + 1;
	
	hM = (float *) mxGetData(plhs[0] = mxCreateNumericMatrix(nM, 2, mxSINGLE_CLASS, mxREAL));
		
	hE = (float *) mxGetData(plhs[1] = mxCreateNumericMatrix(nE, 2, mxSINGLE_CLASS, mxREAL));
		
	//--
	// COMPUTE
	//--
	
	switch (mxGetClassID(prhs[0]))
	{
		// BEGIN-EXPAND-TYPES

		case MEX_TYPE_CLASS:
#ifdef DEBUG
			maxSamplesPerBlock = 16384 / sizeof(MEX_TYPE);
			windowsPerBlock = MIN(16, (maxSamplesPerBlock - block_size) / (block_size - block_overlap) + 1);
			samplesPerBlock = (windowsPerBlock - 1) * (block_size - block_overlap) + block_size;
			smemPerBlock = samplesPerBlock * sizeof(MEX_TYPE);			
			gridSize = (nL + windowsPerBlock - 1) / windowsPerBlock;
			threadsPerBlock = windowsPerBlock;
	
			printf("Max samples per block %d\n", maxSamplesPerBlock);
			printf("Windows per block %d\n", windowsPerBlock);
			printf("Samples per block %d\n", samplesPerBlock);
			printf("Shared mem per block %d\n", smemPerBlock);
			printf("Grid size %d\n", gridSize);
			printf("Threads per block %d\n", threadsPerBlock);
#endif /* DEBUG */
			
			// Allocate device memory

			cudaMalloc((void **) &dX, nX * sizeof(MEX_TYPE));
			
			cudaMalloc((void **) &dM, nM * 2 * sizeof(float));
	
			cudaMalloc((void **) &dE, nE * 2 * sizeof(float));
	
			// Copy signal data to device

			cudaMemcpy(dX, hX, nX * sizeof(MEX_TYPE), cudaMemcpyHostToDevice);

			// Compute
	
			cuda_pamplitude_MEX_TYPE_NAME(dM, dE, (MEX_TYPE *) dX, nX, block_size, block_overlap);
	
			// Copy result from device
	
			cudaMemcpy(hM, dM, nM * 2 * sizeof(float), cudaMemcpyDeviceToHost);
			
			cudaMemcpy(hE, dE, nE * 2 * sizeof(float), cudaMemcpyDeviceToHost);

			// Free device memory
			
			cudaFree(dX);
			
			cudaFree(dE);

			cudaFree(dM);
			
			break;
			
		// END-EXPAND-TYPES
	}
}
