function X = spline_resample(X, ratio, ix, n)

% spline_resample - 

X = X(:);

if nargin < 4
    n = size(X, 1);
end

if nargin < 3
    ix = 1;
end

if nargin < 2
    return;
end

%--
% get the range of converted samples
%--

range = ceil([ratio*(ix - 1), ratio*(ix - 1 + n)]); range(1) = range(1) + 1;

N = diff(range);

%--
% compute offset (in samples) of the first sample at the new rate
%--

offset = (ceil((ix - 1)*ratio) - ((ix - 1)*ratio)) / ratio;

%--
% resample using fast_spline_resample
%--

new_grid = offset:(1/ratio):(offset + (N/ratio)) + 1;

X = fast_spline_resample(X(1:n,:), new_grid(:), 5);
