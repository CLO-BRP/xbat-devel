#include "mex.h"

#include "fast_spline.c"

#include "fast_spline_eval.c"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    double * s, * b, * t, * x; 
    
    int nX, nT, k_0;
    
    /*
     * get input signal and length
     */
    
    x = (double *) mxGetPr(prhs[0]);
    
	nX = (int) (mxGetM(prhs[0]) * mxGetN(prhs[0]));
       
    /*
     * get input time grid and length
     */
    
    t = (double *) mxGetPr(prhs[1]);
    
	nT = (int) (mxGetM(prhs[1]) * mxGetN(prhs[1])); 
    
    /*
     * get precision for recursion initialization
     */
    
    k_0 = (int) mxGetScalar(prhs[2]);
    
    /*
     * allocate coefficient storage and output vector
     */
    
    s = mxCalloc(nT, sizeof(double));
    
    b = mxCalloc(nX, sizeof(double)); 
      
    /*
     * compute spline representation
     */
    
    fast_spline(x, nX, b, k_0);
    
    /*
     * compute interpolated values using LUT.
     */
    
    fast_spline_eval(s, t, nT, b, nX);
    
    /*
     * free spline coefficient storage
     */
    
    mxFree(b);
    
    /*
     * write output data
     */
    
    plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);
    
    mxSetM(plhs[0],nT); mxSetN(plhs[0],1);
    
    mxSetPr(plhs[0],s);
    
}
