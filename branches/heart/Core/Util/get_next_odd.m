function n = get_next_odd(n, strict)

% get_next_odd - get next odd number
% ----------------------------------
% 
% n = get_next_odd(n, strict)
%
% Input:
% ------
%  n - numbers
%  strict - requires next not current odd in such case
%
% Output:
% -------
%  n - next odd numbers

if nargin < 2
	strict = 0;
end

n = ceil(n); 

if strict
	n = n + ~mod(n, 2) + 2 * mod(n, 2);
else
	n = n + ~mod(n, 2); 
end