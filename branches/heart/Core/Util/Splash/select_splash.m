function file = select_splash(ratio, root)

% select_splash - select splash image file considering screen size
% ----------------------------------------------------------------
%
% file = select_splash(ratio)
%
% Input:
% ------
%  ratio - desired screen to image width ratio (def: 2.5)
% 
% Output:
% -------
%  file - splash image file

%--
% set default screen to image ratio
%--

if nargin < 2
	root = splash_root;
end

if isempty(root) || ~exist(root, 'dir')
	error('Unable to find splash image root.');
end

if ~nargin
	ratio = 2.5;
end

%--
% get screen size
%--

screen = get_size_in(0, 'pixels', 1);

%--
% get candidate images and widths
%--

files = what_ext(root, 'png'); files = files.png;

widths = zeros(size(files));

for k = 1:length(files)
	info = imfinfo([root, filesep, files{k}]); widths(k) = info.Width;
end

%--
% select file closest to desired ratio
%--

[ignore, ix] = min(abs(widths - (screen.width / ratio))); %#ok<ASGLU>

file = [root, filesep, files{ix}];