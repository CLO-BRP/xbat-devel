function [T, R, A] = fig_psfrag(h, T, R)

% fig_psfrag - tool for creating psfrag annotated fig
% ---------------------------------------------------
%
% [T, R, A] = fig_psfrag(h)
%           = fig_psfrag(h, T, R)
%
% Input:
% ------
%  h - figure handle
%  T - tags
%  R - text to replace
%
% Output:
% -------
%  T - tags
%  R - replaced text
%  A - alignment of replacements

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.1 $
% $Date: 2004-02-11 06:56:07-05 $
%--------------------------------

%--
% set figure
%--

if nargin < 1
	h = gcf;
end

%--
% handle variable input
%--

font = get(0,'DefaultAxesFontName'); ax = findobj(h, 'type', 'axes');
		
switch nargin

	case 1
		% NOTE: replace text by psfrag tags
	
		%--
		% replace text with tags and set font (conversion to PDF problem)
		%--

		n = 1; % tag counter
		
		% loop over axes
		
		for k = 1:length(ax)
				
			%--
			% title
			%--
			
			g = get(ax(k),'title');			
			s = get(g,'string');
			
			if ~isempty(s)
				T{n} = ['T' n2s(k)];
				R{n} = s;
				A{n} = '[][]';
				set(g,'string',T{n},'fontname',font);
				n = n + 1;
			end
			
			drawnow;
			
			% ylabel and yticklabel
			
			g = get(ax(k),'ylabel');
			s = get(g,'string');
			
			if ~isempty(s)
				T{n} = ['YL' n2s(k)];
				R{n} = s;
				A{n} = '[][]';
				set(g,'string',T{n},'fontname',font);
				n = n + 1;
			end
			
			% xlabel and xticklabel
				
			g = get(ax(k),'xlabel');
			s = get(g,'string');
			
			if ~isempty(s)
				T{n} = ['XL' n2s(k)];
				R{n} = s;
				A{n} = '[][]';
				set(g,'string',T{n},'fontname',font);
				n = n + 1;
			end
		end
		
		% text objects
		
		g = findobj(h,'type','text');
		
		c = 65;
		
		for k = 1:length(g)
		
			s = get(g(k),'string');
		
			if ~isempty(s)
				T{n} = char(c);
				R{n} = s;
				A{n} = '[][]';
				set(g(k),'string',T{n},'fontname',font);
				n = n + 1;
				c = c + 1;		
			end	
		end
	
	case 3
		% NOTE: replace psfrag tags by text
		
		%--
		% replace tags with text
		%--
		
		% loop over axes
		
		for k = 1:length(ax)
		
			% title
			
			g = get(ax(k),'title');			
			s = get(g,'string');
			
			if ~isempty(s)
				ix = find(strcmp(T,s));
				set(g,'string',R{ix},'fontname',font);
			end
			
			% ylabel and yticklabel
			
			g = get(ax(k),'ylabel');
			s = get(g,'string');
			
			if ~isempty(s)
				ix = find(strcmp(T,s));
				set(g,'string',R{ix},'fontname',font);
			end
			
			% xlabel and xticklabel
			
			g = get(ax(k),'xlabel');
			s = get(g,'string');
			
			if ~isempty(s)
				ix = find(strcmp(T,s));
				set(g,'string',R{ix},'fontname',font);
			end
	
		end
		
		% text objects
			
		g = findobj(h,'type','text');
		
		for k = 1:length(g)
		
			s = get(g(k),'string');
		
			if ~isempty(s)
				ix = find(strcmp(T,s));
				set(g,'string',R{ix},'fontname',font);
			end
		end			
end

% n2s - two digit number strings
% -------------------------------
% 
% s = n2s(n)
%
% Input:
% ------
%  n - integer > 0 and < 100
%
% Output:
% -------
%  s - two digit string

function s = n2s(n)

% check range

if (n < 1) || (n > 99)
	error('Number to convert must be > 0 and < 100.');
end

% convert padding small numbers

if n < 10
	s = ['0' num2str(n)];
else
	s = num2str(n);
end


