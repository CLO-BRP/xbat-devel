function figure_menubar(h)

% figure_menubar - toggle figure menubar
% --------------------------------------
%
% figure_menubar(h)
%
% Input:
% ------
%  h - figure handles (def: gcf)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% set figure handle
%--

if (nargin < 1)
	h = gcf;
end

%--
% toggle menubar
%--

for k = 1:length(h)
	tmp = get(h(k),'menubar');
	switch (tmp)
	case ('none')
		set(h(k),'menubar','figure');
	case ('figure')
		set(h(k),'menubar','none');
	end
end