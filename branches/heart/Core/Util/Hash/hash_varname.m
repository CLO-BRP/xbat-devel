function varname = hash_varname(candidate)

% hash_varname - generate valid varname through hash
% --------------------------------------------------
%
% varname = hash_varname(candidate)
%
% Input:
% ------
%  candidate - name
%  
% Output:
% -------
%  varname - valid variable name
%
% See also: genvarname, hash

varname = ['h_', hash(candidate, 'sha1')];