function root = miktex_root(type, root)

% miktex_root - get miktex root
% -----------------------------
%
% root = miktex_root(type, root)
%
% Input:
% ------
%  type - root type 'bin' or 'doc'
%  root - directory to set
%
% Output:
% -------
%  root - current
%
% See also: miktex_tools

%--
% setup
%--

if ~ispc
	root = ''; return;
end

persistent ROOT

if isempty(ROOT)
	ROOT = struct;
end

%--
% set default root type
%--

if ~nargin || isempty(type)
	type = 'bin';
end

%--
% get or set root
%--

if nargin < 2
	
	if isfield(ROOT, type)
		root = ROOT.(type); return;
	end
			
	root = ''; base = {};

	% NOTE: the cell container for the strings in 'strcat' prevent trimming of whitespace
	
	programs = strcat({'C:\'}, {'Program Files', 'Program Files (x86)'}, {'\MikTex '});
	
	for k = 1:numel(programs)
		base = [base, strcat(programs(k), {'3.0', '2.9', '2.8', '2.7', '2.6', '2.5'})]; %#ok<AGROW>
	end
	
% 	db_disp; iterate(@disp, base);
	
	switch type
		case 'bin'
			candidate = strcat(base, '\miktex\bin');
			
		case 'doc'
			candidate = strcat(base, '\doc');
	end
	
	for k = 1:length(candidate)
		
		if exist_dir(candidate{k}), root = candidate{k}; return; end
	end
else
	
	if exist_dir(root)
		ROOT.(type) = root;
	end
end

