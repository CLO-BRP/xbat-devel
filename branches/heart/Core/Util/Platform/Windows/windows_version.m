function [ver, status, result] = windows_version

% windows_version - get windows version description
% -------------------------------------------------
%
% [ver, status, result] = windows_version
%
% Output:
% -------
%  ver - version description struct
%  status - of system call
%  result - of system call

% NOTE: we return empty and no problem status for non-windows case

if ~ispc
	ver = ''; status = 0; result = ''; return;
end
	
[status, result] = system('ver'); 

% TODO: learn what the output in various systems is, what fields are contained, and how to parse

% NOTE: two interesting places to look: http://www.ss64.com/nt/ver.html and http://commandwindows.com/index.html

ver.result = strrep(result, sprintf('\n'), ''); ver.short = '';

if ~nargout, disp(ver); clear ver; end