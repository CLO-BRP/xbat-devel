function value = islinux

value = ~isempty(findstr(computer, 'LNX'));