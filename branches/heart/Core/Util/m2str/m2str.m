function str = m2str(value, format, compress)

% m2str - compact matrix to string
% --------------------------------
%
% str = m2str(value, format, compress)
%
% Input:
% ------
%  value - matrix
%  format - for numbers
%  compress - zeros in string
%
% Output:
% -------
%  str - representation

%--
% handle input
%--

if ~isnumeric(value)
	error('Input must be a matrix.')
end

if nargin < 3
	compress = 1;
end

if nargin < 2
	format = '%0.16f';
end

%--
% build string
%--

[start, stop] = bracket_str(value);

str = [start, data_str(value, format, compress), stop];


%--------------------
% BRACKET_STR
%--------------------

function [start, stop] = bracket_str(value)

if ~issparse(value)
	
	if isreal(value)
		start = ['<matrix type="full" class="real" size="', size_str(value), '">'];
	else
		start = ['<matrix type="full" class="complex" size="', size_str(value), '">'];
	end
	
else
	
	if isreal(value)
		start = ['<matrix type="sparse" class="real" size="', size_str(value), '">'];
	else
		start = ['<matrix type="sparse" class="complex" size="', size_str(value), '">'];
	end

end

stop = '</matrix>';


%--------------------
% SIZE_STR
%--------------------

function str = size_str(value)

str = sprintf('%d,', size(value)); str(end) = [];


%--------------------
% DATA_STR
%--------------------

function str = data_str(value, format, compress)

% NOTE: handle special case of empty matrix

if numel(value) < 1
	str = ''; return;
end

%--
% set default compression and format
%--

if nargin < 3
	compress = 1;
end

if nargin < 2
	format = '%0.16f';
end

%--
% build data string according to format and compress if needed
%--

if ~issparse(value)
	
	if isreal(value)
		str = sprintf([format, ' '], value(:));
	else
		str = sprintf([format, ' '], real(value(:)), imag(value(:)));
	end

else
	
	[row, col, value] = find(value);
	
	if isreal(value)
		str = [sprintf('%d ', row, col), sprintf([format, ' '], value(:))];
	else
		str = [sprintf('%d ', row, col), sprintf([format, ' '], real(value(:)), imag(value(:)))];
	end
	
end

if compress
	str = regexprep(str, '(0+)\s', '0 '); str = strrep(strrep(str, '0 ', ' '), '. ', ' ');
end

if str(end) == ' '
	str(end) = [];
end