function value = str2m(str)

% m2str - recover matrix from compact string
% ------------------------------------------
%
% value = str2m(str)
%
% Input:
% ------
%  str - representation
%
% Output:
% ------
%  value - matrix

%--
% get type, size, and data from string
%--

tag.str = 'matrix'; tag.length = numel(tag.str);

% NOTE: we only scan a short prefix of length 150 if available, this should be enough

ix = find(str(1:min(150, numel(str))) == '>');

attr = str_split(str(tag.length + 2:ix - 1), ' '); 

for k = 1:numel(attr)
	
	part = str_split(attr{k}, '='); part{2} = part{2}(2:end - 1);

	switch part{1}
		
		% NOTE: we get 'sparse' and 'real' indicators, analogs of 'issparse' and 'isreal'
		
		case 'type'
			out.sparse = (part{2}(1) == 's');
			
		case 'class'
			out.real = (part{2}(1) == 'r');
			
		case 'size'
			out.size = sscanf(strrep(part{2}, ',', ' '), '%d')';
			
	end

end

% NOTE: handle special case of empty matrix

if sum(out.size) == 0
	
	if ~out.sparse
		value = [];
	else
		value = sparse(0, 0);
	end
	
	return;
	
end

% NOTE: in the case of sparse matrices the head values are integers, but this works anyway

out.data = sscanf(str(ix + 1:end - (tag.length + 3)), '%f');

%--
% recover value from parsed string representation
%--

if ~out.sparse
	
	if out.real
		value = reshape(out.data, out.size);
	else
		value = reshape(out.data(1:end/2) + i * out.data(end/2 + 1:end), out.size);
	end
	
else
	
	if out.real
		value = sparse(out.data(1:end/3), out.data(end/3 + 1:(2/3)*end), out.data((2/3)*end + 1:end), out.size(1), out.size(2));
	else
		value = sparse(out.data(1:end/4), out.data(end/4 + 1:(2/4)*end), out.data((2/4)*end + 1:(3/4)*end) + i * out.data((3/4)*end + 1:end), out.size(1), out.size(2));
	end
	
end

