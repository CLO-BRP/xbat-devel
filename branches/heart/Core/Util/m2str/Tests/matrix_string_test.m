function matrix_string_test

% FULL REAL

for k = 1:4
	disp('-----------------------');
	A = randn(5 * ones(1, k)); str = m2str(A); B = str2m(str); E = A - B; max(E(:))
end

% FULL COMPLEX

for k = 1:4
	disp('-----------------------');
	A = sqrt(rand(5 * ones(1, k))); str = m2str(A); B = str2m(str); E = A - B; max(E(:))
end

% NOTE: vectors and higher dimensional arrays are not available as sparse

% SPARSE REAL

for k = 5:10
	disp('-----------------------');
	A = randn(k); A(abs(A) < 0.5) = 0; A = sparse(A); str = m2str(A); B = str2m(str); str, E = A - B
end

% SPARSE COMPLEX

for k = 5:10
	disp('-----------------------');
	A = sqrt(randn(k)); A(abs(A) < 0.5) = 0; A = sparse(A); str = m2str(A); B = str2m(str); str, E = A - B
end
