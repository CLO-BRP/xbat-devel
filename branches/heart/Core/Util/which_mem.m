function out = which_mem(ignore)

% which_mem - get full location of functions in memory
% ----------------------------------------------------
% 
% out = which_mem(ignore)
%
%
% Input:
% ------
%  ignore - set paths to ignore in listing
%
% Output:
% ------
%  out - path location strings for called functions

%--
% handle input
%--

if ~nargin
	% NOTE: ignore functions in basic MATLAB
	
	ignore = {fullfile(matlabroot, 'toolbox', 'matlab')};
end

% NOTE: get ignore string lengths for efficient comparison later

len = zeros(size(ignore));

for k = 1:numel(ignore)
	len(k) = numel(ignore{k});
end

%--
% get m and mex functions in memory
%--

[fun, mex] = inmem;

fun = {fun{:}, mex{:}}';

%--
% display location of functions in memory
%--

i = 0;

for k = 1:length(fun)
	
	%--
	% get location string if available
	%--
	
	str = which(fun{k}); 
	
	%--
	% ignore functions from certain paths
	%--
	
	for j = 1:length(ignore)
		
		if length(str) > len(j) && strncmp(ignore{j},str,len(j))
			str = []; break;
		end
	end 
	
	%--
	% display locations not ignored
	%--
	
	if ~isempty(str)
		i = i + 1; out{i} = str;
	end
end

% NOTE: return with no display on empty

if (i == 0)
	out = cell(0);
end

%--
% sort and display
%--

out = sort(out(:)); total = numel(out);

disp(' '); disp(['  ', int2str(total), ' functions in memory.']); disp(' ');

for k = 1:numel(out)
	disp(['  ', int_to_str(k, numel(out), ' ') '. ' out{k}]);
end

disp(' ');

if ~nargout
	clear out;
end
