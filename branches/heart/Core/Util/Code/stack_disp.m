function lines = stack_disp(stack, depth)

% stack_disp - display linked stack information
% ---------------------------------------------
%
% lines = stack_disp(stack, depth)
%
% Input:
% ------
%  stack - array
%  depth - to display
%
% Output:
% -------
%  lines - to display (no display if output requested)

%--
% handle input
%--

if ~nargin || isempty(stack)
	stack = dbstack(1, '-completenames');
end

if nargin < 2
	depth = length(stack);
else
	depth = min(depth, length(stack));
end

%--
% display stack
%--

lines = {};

for k = 1:depth
	lines{end + 1} = ['  ', ternary(k < 10, ' ', ''), int2str(k), '. ', stack_line(stack(k))];
end

if ~nargout
	for k = 1:numel(lines)
		disp(lines{k});
	end
	
	clear lines;
end

