function [called, caller] = get_called(source, pack, scan)

% get_called - functions for file or directory
% --------------------------------------------
%
% [called, caller] = get_called(source, pack, scan)
%
% Input:
% ------
%  source - file or directory
%  pack - as struct (def: false)
%  scan - directory source (def: false)
%
% Output:
% -------
%  called - functions
%  caller - corresponding
%
% NOTE: this function relies on the undocumented 'getcallinfo'
% 
% See also: get_required, getcallinfo, deprpt

% TODO: keep all calls and separate called into various types 'built-in', 'toolbox', 'other'

%--
% handle input
%--

% TODO: add 'verbose' and 'detailed' (related to above TODO) options, create options struct

if nargin < 3
	scan = true;
end

if nargin < 2
	pack = true; 
end

if ~nargin
	source = pwd;
end

%--
% get call info
%--

switch exist(source) %#ok<EXIST>
	
	case 2, type = 'file';
		
	case 7, type = 'directory';
		
	otherwise
		error('Source input is not available as file or directory.');
end

switch type
	
	case 'file'
		info = getcallinfo(source, 'flat'); 

		called = cell(size(info.calls));
		
		for k = numel(info.calls):-1:1
			called{k} = which(info.calls{k});
			
			% NOTE: this typically happens when the call is to a private MEX, beware
			
			if isempty(called{k})
				called(k) = [];
			end
		end
		
		called(strmatch(fullfile(matlabroot, 'toolbox'), called)) = [];
		
		called(strmatch('built-in', called)) = [];
		
		called = sort(called); caller = source;
		
	case 'directory'
		if scan
			% NOTE: when we 'scan' we scan the root directory for children an pool all m-file content
			
			sources = scan_dir(source); source = {};
			
			for k = 1:numel(sources)
				content = what(sources{k}); 
				
				if ~isempty(content.m)
					source = [source; strcat(sources{k}, filesep, content.m)]; %#ok<AGROW>
				end
			end
		else 
			content = what(source); source = strcat(source, filesep, content.m);
		end
		
		% NOTE: here we get called functions for each file
		
		called = cell(size(source)); caller = source;
		
		for k = 1:numel(source)
			called{k} = get_called(source{k}, false); % NOTE: we disable packing here
		end
		
		[rcalled, rcaller] = reverse(caller, called);
end

%--
% pack if needed
%--

if ~pack
	return;
end

out = repmat(struct('file', '', 'caller', {}), size(called));

if ischar(caller)
	caller = {caller};
end

for k = 1:numel(called)
	out(k).file = called{k}; out(k).caller = caller{k};
end

called = out;

if ~nargout
	disp(called); clear called; 
end


%-----------------
% REVERSE
%-----------------

function [called, caller] = reverse(caller, called)

% reverse - called for caller relation
% ------------------------------------
%
% [called, caller] = reverse(caller, called)
%
% Input:
% ------
%  caller - list
%  called - list of list
%
% Output:
% -------
%  called - list
%  caller - list of list

% NOTE: reversing this list would be easier with a proper hash

store = struct;

for j = 1:numel(caller)
	
	for k = 1:numel(called{j})
		% NOTE: called is hashed to get a struct field bucket, we then update the bucket
		
		key = get_key(called{j}{k});
		
		if isfield(store, key)
			store.(key).caller{end + 1} = caller{j};
		else
			store.(key).called = called{j}{k}; store.(key).caller = {caller{j}};
		end
	end
	
end

% NOTE: here we unravel the hash bucket struct to a couple of arrays

keys = fieldnames(store); called = cell(size(keys));

for k = 1:numel(keys)
	called{k} = store.(keys{k}).called; caller{k} = store.(keys{k}).caller;
end

[called, index] = sort(called); caller = caller(index);


%-----------------
% GET_KEY
%-----------------

function key = get_key(str)

% NOTE: the leading character ensures we have a proper field name

key = ['k', md5(str)];
