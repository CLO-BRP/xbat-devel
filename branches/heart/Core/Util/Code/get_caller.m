function [caller, str] = get_caller(fun)

% get_caller - get caller context
% -------------------------------
%
% [caller, str] = get_caller(fun)
%
%
% Input:
% ------
%  fun - function called
%
% Output:
% -------
%  caller - caller, decorated 'dbstack' entry
%  str - linked caller string

% TODO: figure out what the meaning and intended use of this function is

%-----------------
% HANDLE INPUT
%-----------------

%--
% get caller from stack
%--

if (nargin < 1) || isempty(fun)
	
	% NOTE: this function and actual caller are the first two stack elements

	stack = dbstack('-completenames');

	% NOTE: return if we were called from the command line

	if length(stack) < 3
		caller = []; return;
	end

	caller = stack(3);
	
	caller.stack = stack(3:end);
	
%--
% set caller
%--

else
	
	% NOTE: this could change if the stack object entries change
	
	caller.file = which(fun);
	
	caller.name = fun;
	
	caller.line = 0;
	
	caller.stack = []; 

end

%--
% output caller string
%--

if nargout > 1
	str = stack_line(caller);
end

%-----------------
% DECORATE
%-----------------

%--
% parse caller file to get extension location
%--

% NOTE: caller is an extension if it exists within an 'Extensions' directory

% TODO: the following code works on windows, encapsulate to avoid this

loc = strread(caller.file, '%s', 'delimiter', '/');

ix = find(strcmp(loc, 'Core')); 

if isempty(ix)
	caller.loc = []; return;
end

caller.loc = loc((ix + 1):(end - 1));



