function nice_catch_error(info, short)

% NOTE: this function displays the unframed error display for 'nice_catch'

%---------------------
% HANDLE INPUT
%---------------------

if nargin < 2
	short = 0;
end

if ~nargin
	info = lasterror; %#ok<LERR>
end 

%---------------------
% DISPLAY
%---------------------

% TODO: parse info.message for Java exception messages and remove useless stack, lines begin with 'at'

disp(' MESSAGE:');
disp(' ');

% TODO: do something to display a compressed string or a single line

if short
	opt = file_readlines; opt.skip = true;
	
	message = file_readlines(info.message, [], opt);
	
	if numel(message) > 1
		info.message = message{1};
	end
end

if ~isfield(info, 'identifier') || isempty(info.identifier)
	disp(['   ', info.message]);
else
	disp(['   ', info.message, ' (', info.identifier, ')']);
end

disp(' ');

disp(' STACK:');

% NOTE: try to get stack information if not available

full = 1;

if ~isfield(info, 'stack')
	try
		info.stack = dbstack('-completenames'); info.stack(1:2) = []; full = 0;
	catch %#ok<CTCH>
		info.stack = [];
	end
end

if ~isempty(info.stack)
	
	if ~full
		disp(' ');
		disp('   WARNING: Only partial stack information is available.');
	end

	disp(' ');

	stack_disp(info.stack);
	
else
	
	disp(' ');
	disp('   WARNING: No stack information is available.');
	
end
