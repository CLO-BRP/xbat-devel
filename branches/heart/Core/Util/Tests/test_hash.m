function test_hash(in)

% NOTE: this test is obsolete, you must change 'md5' to use the MEX for it to make sense

if ~nargin
	in = 'we are testing whether we can hash using these two functions';
end

types = {'char', 'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64', 'single', 'double'}; 

for k = 1:numel(types)
	
	% NOTE: this keeps the output of 'hash' constant
	
	try
		bytes = typecast(uint8(in), types{k});
	catch
		result(k).cast = types{k}; continue;
	end
	
	% NOTE: this keeps the output of 'md5' constant
	
% 	bytes = feval(types{k}, uint8(in)); 
	
	result(k).cast = types{k}; % result.bytes = bytes; 
	
	result(k).hash = hash(bytes, 'md5'); result(k).md5 = md5(bytes);
	
end

disp(result);