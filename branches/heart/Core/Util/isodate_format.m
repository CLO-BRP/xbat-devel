function format = isodate_format(fractional)

% isodate_format - isodate format string
% --------------------------------------
%
% format = isodate_format(fractional)
%
% Input:
% ------
%  fractional - indicate the use of fractional seconds (def: 0)
%
% Output:
% -------
%  format - format string

if ~nargin
	fractional = 0;
end

if fractional
	format = 'yyyy-mm-ddTHH:MM:SS.FFF';
else
	format = 'yyyy-mm-ddTHH:MM:SS';
end

