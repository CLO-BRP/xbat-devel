function toolbox = Wavelab_toolbox

%------------------------
% DATA
%------------------------

toolbox.name = 'Wavelab';

toolbox.home = 'http://www-stat.stanford.edu/~wavelab';

toolbox.url = 'http://www-stat.stanford.edu/~wavelab/Wavelab_850/WAVELAB850.ZIP';

toolbox.install = @install;

%------------------------
% INSTALL
%------------------------

function install

%--
% copy patched files
%--

source = fullfile(toolbox_data_root('Wavelab'), 'Files', '*.m');

destination = real_toolbox_root('Wavelab');

copyfile(source, destination, 'f');

%--
% run startup
%--

p1 = pwd;

cd(destination);

startup;

cd(p1);
