function [value, local, info] = has_toolbox(name)

% has_toolbox - check for toolbox availability
% --------------------------------------------
%
% [value, local, info] = has_toolbox(name)
%
% Input:
% ------
%  name - toolbox name
%
% Output:
% -------
%  value - result
%  local - not mathworks toolbox indicator
%  info - version info for mathworks toolboxes

%--
% consider local toolboxes
%--

% NOTE: value is the 'exists' output of 'toolbox_root'

[root, value] = toolbox_root(name);

% NOTE: for a local toolbox we do not have versio info

if value
	local = 1; info = []; return;
end

%--
% consider built-in toolboxes
%--

% NOTE: we do not use the toolbox root to make this determination

info = ver(name); value = ~isempty(info);

% NOTE: the local value is empty (undefined) if we don't have the toolbox

local = ternary(value, 0, []);

