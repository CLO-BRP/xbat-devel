function [file, root] = toolbox_data_file(name)

% toolbox_data_file - file providing toolbox data
% -----------------------------------------------
%
% [file, root] = toolbox_data_file(name)
%
% Input:
% ------
%  name - toolbox name
%
% Output:
% -------
%  file - file providing toolbox data
%  root - toolbox data root

root = toolbox_data_root(name);

name = [genvarname(name), '_toolbox']; 

file = fullfile(root, [name, '.m']);