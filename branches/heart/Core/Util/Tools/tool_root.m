function root = tool_root(name)

% tool_root - directory
% ---------------------
%
% root = tool_root(name)
%
% Input:
% ------
%  name - of tool
%
% See also: tools_root

if ~nargin
	error('Tool name input is required.');
end

root = create_dir(fullfile(tools_root, name));
