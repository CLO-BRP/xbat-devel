function [status, result] = nmap(varargin)

% nmap - use network mapper
% -------------------------
%
% [status, result] = nmap(varargin)
%
% NOTE: this is a shortcut to putty, arguments are imploded as strings

% TODO: develop a 'latest_nmap_url' function that scrapes the 'http://nmap.org/dist/' listing

name = 'nmap'; file = 'nmap.exe'; url = 'http://download.insecure.org/nmap/dist/nmap-4.20-win32.zip';

[status, result] = generic_tool(name, file, url, 'curl', varargin{:});