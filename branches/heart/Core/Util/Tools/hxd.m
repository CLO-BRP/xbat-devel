function [status, result] = hxd(file)

%--
% get hex editor tool
%--

tool = get_tool('HxD.exe'); 

if isempty(tool) && install_tool('HxD', 'ftp://wa651f5:anonymous@mh-nexus.de/HxDen.zip');
	
	tool = get_tool('HxD.exe');
end

% NOTE: possibly return tool

if ~nargin
	status = tool; return;
end

%--
% open file using hex editor
%--
	
[status, result] = system(['"', tool.file, '" "', file, '" &']);
