function [status, result] = pstools(name, varargin)

%--
% check for tools, possibly install
%--

root = fullfile(tools_root, 'PsTools');

if ~exist(root, 'dir')
	
	% NOTE: this 'tool' is more like a toolbox, there are others
	
	if ~install_tool('PsTools', 'http://download.sysinternals.com/Files/PsTools.zip')
		error('Failed to install ''PsTools''.');
	end
	
end

%--
% get all tools
%--

tools = get_tools(root);

% NOTE: return all tools if no input

if ~nargin
	status = tools; return;
end

%--
% get named tool
%--

ix = find(strcmpi(name, {tools.name}));

if isempty(ix)
	tool = [];
else
	tool = tools(ix);
end
	
% NOTE: return a named tool if not input beyond name

if isempty(varargin)
	status = tool; return;
end

%--
% use tool
%--
	
[status, result] = system(['"', tool.file, '" ', str_implode(varargin, ' ')]);
