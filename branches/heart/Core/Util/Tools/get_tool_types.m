function types = get_tool_types

% NOTE: we are allowing for executables and other language scripts

types = {'*.bat', '*.exe', '*.php', '*.pl', '*.py', '*.rb'};
