function result = add_callback_function(handle, callback, fun)

% add_callback_function - add function to callback
% ------------------------------------------------
%
% result = add_callback_function(handle, callback, fun)
%
% Input:
% ------
%  handle - handle
%  callback - callback 
%  fun - callback function
%
% Output:
% -------
%  result - length of callback array

% TODO: consider what would be a more useful or meaningful output

%---------------------
% HANDLE INPUT
%---------------------

%--
% check function input
%--

if (nargin < 2) || isempty(fun)
	return;
end 

if ~is_callback(fun)
	error('Callback function must be a proper callback.');
end

%--
% check handle input
%--

if any(~ishandle(handle))
	error('First input must be handles.');
end 

%--
% handle multiple handles recursively
%--

if length(handle) > 1
	
	result = zeros(size(handle));
	
	for k = 1:length(handle) 
		result(k) = add_callback_function(handle, callback, fun);
	end 
	
	return;

end

%--
% check callback type
%--

% NOTE: the above recursion and this error do not seem compatible, result is key

callback = lower(callback); implemented = lower(get_callback_fields(handle));

if ~ismember(callback, implemented)
	error(['Callback ''', callback, ''' is not implemented by type ''', get(handle, 'type')]);
end

%---------------------
% UPDATE CALLBACK
%---------------------

%--
% get current callback function from handle
%--

current = get(handle, callback);

%--
% set manager and add input function
%--

if isempty(current)
	set(handle, callback, {@callback_manager, callback, fun}); result = 1; return;
end 

%--
% set manager as main function and add existing resize and input function
%--

if ~iscell(current) || ~isequal(current{1}, @callback_manager)
	set(handle, callback, {@callback_manager, callback, current, fun}); result = 2; return;
end

%--
% add a new resize to the managed list of functions
%--

new = {current{:}, fun}; result = length(new) - 2;

set(handle, callback, new);


%------------------------
% CALLBACK_MANAGER
%------------------------

function callback_manager(obj, eventdata, callback, varargin)

%--
% get callback function array from callback
%--

fun = get(obj, callback);

%--
% loop over stored resize functions
%--

% NOTE: the handleameters for this resize function are callbacks

for k = 3:length(fun)
	
	% TODO: consider catching exceptions here
	
	eval_callback(fun{k}, obj, eventdata);
	
end


