function type = get_handle_type(handle)

type = 'UNKNOWN';

try 
	type = get(handle, 'type');
catch
	type = get(handle, 'UIClassID');
end