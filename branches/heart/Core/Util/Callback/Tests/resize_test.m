function result = resize_test

par = figure;

result = [];

result(end + 1) = append_resize(par, 'set(gcbf, ''name'', ''Resizing!'');');

result(end + 1) = append_resize(par, {@change_color, 0.8 * [1 0 0]});

result(end + 1) = append_resize(par, {@change_color, 0.8 * [0 1 0]});

result(end + 1) = append_resize(par, @change_color);

result(end + 1) = append_resize(par, {@change_color, 0.8 * [1 1 1]});

result(end + 1) = append_resize(par, 'set(gcbf, ''name'', ''Resize done!'');');


function change_color(obj, eventdata, color) %#ok<INUSL>

if nargin < 3
	color = 0.8 * [0 0 1];
end

% NOTE: the pause is supposed to flush display requests

current = get(obj, 'color'); tick = linspace(0, 1, 20);

for k = 1:length(tick)
	set(obj, 'color', (1 - tick(k)) * current + tick(k) * color); pause(0.01);
end
