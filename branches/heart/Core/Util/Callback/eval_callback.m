function result = eval_callback(callback, varargin)

% eval_callback - evaluate callback
% ---------------------------------
%
% result = eval_callback(callback, varargin)
%
% Input:
% ------
%  callback - callback
%  varargin - call arguments
%
% Output:
% -------
%  result - callback result

% TODO: consider result structure and add some profiling

%--
% check for empty callback
%--

result = [];

if isempty(callback)
	return;
end

%--
% check callback input
%--

[value, type] = is_callback(callback);

if ~value
	error('Callback input is not callback.');
end

%--
% evaluate callback based on type
%--

switch type
	
	case 'string'
		
		eval(callback); return;
		
	case 'chain'
		% NOTE: the multiple output case is tricky with callback chains! it may not be possible
		
		for k = 1:length(callback)
			result(k) = eval_callback(callback{k}, varargin{:});
		end
		
		return;
		
	case 'simple' % in this case the callback is a 'function_handle'
		
		fun = callback; args = varargin;
		
	case 'parametrized' % in this case the callback is a cell array with a leading function handle and parameters
		
		fun = callback{1}; parameters = callback(2:end); args = {varargin{:}, parameters{:}};
	
end

% NOTE: this section evaluates 'simple' and 'parametrized' callbacks

switch nargout(fun)
	
	case 0
		result = []; fun(args{:});

	case 1
		result = fun(args{:});
		
	% NOTE: just evaluate, develop a way to warn about this once!
	
	otherwise
		result = fun(args{:});
		
		% TODO: fix this using code found in 'iterate'
		
% 		error('Callback functions must have at most a single output.');
		
end

