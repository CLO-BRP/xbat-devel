function [files, content] = editor_open_files

% editor_open_files - list
% ------------------------
%
% [files, content] = editor_open_files
% 
% Output:
% -------
%  files - open
%  content - of MATLAB preferences file
%
% See also: prefdir, xml2struct

% TODO: interpret times, can we find out who is currently open?

%--
% load content from preference file
%--

source = fullfile(prefdir, 'MATLAB_Editor_State.xml');

content = xml2struct(source);

%--
% process parsed file contents
%--

content = content.Children(strcmp({content.Children.Name}, 'File'));

files = cell(numel(content), 1);

for k = 1:numel(content)
	attr = content(k).Attributes; packed = struct; 

	% NOTE: we could skip packing, however this makes for simpler more robust code

	for j = 1:numel(attr)
		packed.(attr(j).Name) = attr(j).Value;
	end
	
	files{k} = fullfile(packed.absPath, packed.name);
end

