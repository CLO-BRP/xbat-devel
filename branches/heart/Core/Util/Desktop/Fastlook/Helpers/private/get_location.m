function location = get_location(store, location)

% get_location - representation from store
% ----------------------------------------
%
% location = get_location(store, location)
%
% Input:
% ------
%  store - database
%  location - string
%
% Output:
% -------
%  location - from store

value = location;

[status, location] = query(store, ['SELECT * FROM location WHERE location = ''', value, ''';']); %#ok<ASGLU>

if ~isempty(location)
	return; 
end

content = dir(value);

% NOTE: this relies on the output of 'dir' containing '.' as the first element

location = keep_fields(content(1), {'date', 'datenum'}); location.location = value;

location = set_database_objects(store, location); 