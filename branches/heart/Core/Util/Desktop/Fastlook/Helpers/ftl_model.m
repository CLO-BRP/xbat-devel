function obj = ftl_model(name)

% ftl_model - objects
% -------------------
%
% obj = ftl_model(name)
%
% Input:
% ------
%  name - of object
%
% Output:
% -------
%  obj - model

% NOTE: all our objects have an 'id' primary-key

obj.id = 1;

hint = column_type_hints;

switch name
	
	case 'location'
		obj.location = 'location';
		
		% TODO: use modification date information to help streamline updates
		
		obj.date = hint.date;
		
		obj.datenum = hint.real;
		
	case 'file'
		obj.location_id = 1;
		
		here = fileparts(mfilename('fullpath')); file = rmfield(no_dot_dir(here), 'isdir');
		
		obj = struct_merge(obj, file(1)); 
		
		obj.type = '.ext'; 
		
		obj.md5 = 'hash'; 

	case 'line'
		obj.file_id = 1;
		
		obj.number = 1;
				
		obj.comment = 0;
		
		obj.content = 'content';
end
