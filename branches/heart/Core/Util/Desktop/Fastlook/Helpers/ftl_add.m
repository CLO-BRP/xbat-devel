function added = ftl_add(location, verb)

% ftl_add - location content to database
% --------------------------------------
%
% added = ftl_add(location, verb)
%
% Input:
% ------
%  location - string
%  verb - flag
%
% Output:
% -------
%  added - info
%
% See also: ftl, ftl_clean, ftl_del, ftl_sync

% TODO: this code and model are not DRY, fix this

% TODO: consider handling of query failures

if nargin < 2
	verb = false;
end 

store = ftl_store;

%--
% make sure location is in database
%--

location = get_location(store, location);

if verb
	disp(' '); disp(['(', location.location, ')']);
end

%--
% get and add location content to store
%--

%----------------

% TODO: factor the next few lines as 'get_files'

content = no_dot_dir(location.location, -1); % NOTE: the -1 flag indicates only files

content = rmfield(content, 'isdir');

for k = numel(content):-1:1
	
	name = content(k).name; [ignore, ignore, type] = fileparts(name); %#ok<*ASGLU>
	
	% TODO: add other file extensions and do this well
	
	% NOTE: this means that we will need to describe prefix comments for each type
	
	% TODO: gather statistics on file types and order set favorably,  most common first
	
	if ~string_is_member(type, {'.m', 'c', '.template', '.h', '.html', '.css', '.js', '.rb', '.erb', '.builder', '.yml', '.txt'})
		content(k) = [];
	else
		content(k).type = type;
	end
end

%----------------

% TODO: the prefix comment indicator is a function of the file type

% NOTE: for non-prefix comments those lines will appear as source

% NOTE: conventionally the project prefers prefix comments when these are available

% NOTE: conventinally we also prefer comments on their own lines

opt = file_readlines; opt.pre = '%'; opt.skip = true; opt.no_end = true;

line_count = 0;

% TODO: flip list, then flip 'for' so that we can skip files already in store and keep alphabetical order of added files

for k = 1:numel(content)
	
	%--
	% check for file availability and possible change
	%--
	
	file = fullfile(location.location, content(k).name);

	content(k).md5 = file_md5(file);
		
	content(k).location_id = location.id;
	
	[ignore, available] = query(store, ['SELECT * FROM file WHERE name = ''', content(k).name, ''' AND location_id = ', int2str(location.id)]); %#ok<ASGLU>
	
	% TODO: consider a force refresh mode
		
	if isempty(available) 
		if verb
			disp(['Adding ', content(k).name, ' ...']);
		end
		
		% NOTE: here we add a location file, with location foreign-key
		
		files(k) = set_database_objects(store, content(k), [], [], 'file'); %#ok<*AGROW>
	else
		files(k) = available;
		
		% NOTE: when file is available and the content has not changed, as verified by the hash, continue
		
		if strcmp(content(k).md5, available.md5)
			if verb > 1
				disp(['Skipped ', content(k).name]);
			end
			
			continue;
		else
			if verb
				disp(['Updating ', content(k).name, ' ...']);
			end
			
			% TODO: we need some update helpers as part of the ORM, perhaps integrate into 'set_database_objects'
			
			query(store, ['UPDATE file SET md5 = ''', content(k).md5, ''' WHERE id = ', int2str(available.id)]);
			
			[status, result] = query(store, ['DELETE FROM line WHERE file_id = ', int2str(available.id)]); %#ok<NASGU,ASGLU>
		end
	end
	
	%--
	% add file lines to index
	%--
		
	% TODO: develop more extensive line filtering, for example no 'only punctuation' lines
	
	[lines, nos, comment] = file_readlines(file, [], opt);
	
	% NOTE: if the file is empty there are no lines to store
	
	if isempty(lines)
		continue;
	end
	
	line_count = line_count + numel(lines); 
	
	clear line; % TODO: there should be a better way to do this!
		
	for j = 1:numel(lines)
		line(j).file_id = files(k).id;
		
		line(j).comment = 0;
		
		line(j).number = nos(j);
		
		line(j).content = lines{j};
	end
	
	for j = 1:numel(comment.lines)
		line(end + 1).file_id = files(k).id;
		
		line(end).comment = 1;
		
		line(end).number = comment.nos(j);
		
		line(end).content = comment.lines{j};
	end
	
	% NOTE: here we add the file lines, with file foreign-key

	set_database_objects(store, line);
end

if verb	
	disp([int2str(line_count), ' lines added.']);
end

%--
% report some possibly useful output
%--

added.location = location;

if ~exist('files', 'var')
	files = [];
end

added.files = files;

% NOTE: the line count only considers updated lines

added.line_count = line_count;

if ~nargout
	clear added;
end
