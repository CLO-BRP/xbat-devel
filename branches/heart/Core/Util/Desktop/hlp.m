function hlp(fun)

% hlp - better help
% -----------------
%
% hlp fun
%
% Input:
% ------
%  fun - function name

% NOTE: we clear screen, set more on, display help, and reset more

% TODO: maintain 'more' state rather than always set to off at the end

clc; more on;

try
	disp(' ');

	if ~nargin || isempty(fun)
		help;
	else		
		if string_contains(which(fun), matlabroot)
			help(fun);
		else
			lines = get_hlp_lines(fun);

			for k = 1:numel(lines)
				disp(lines{k});
			end

			disp(' ');
		end
	end
catch 
	more off;
end

more off;


%---------------------
% GET_HLP_LINES
%---------------------

function lines = get_hlp_lines(fun)

lines = file_readlines(help(fun));

link = find(string_contains(lines, 'see also:', false));

for k = 1:numel(link)
	ix = link(k);
	
	lines{ix} = link_see_also(lines{ix});
end


%---------------------
% LINK_SEE_ALSO
%---------------------

function line = link_see_also(line)

% TODO: allow this to work with no semi-colon

part = str_split(line, ':');

calls = str_split(part{2}, ',');

for k = 1:numel(calls)
	calls{k} = ['<a href="matlab:hlp(''', calls{k}, ''');">', calls{k}, '</a>'];
end

part{2} = str_implode(calls, ', ');

% TODO: the number of leading spaces should not be hard-coded

line = ['  ', part{1}, ': ', part{2}];

