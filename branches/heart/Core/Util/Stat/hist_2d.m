function [H, c, v] = hist_2d(X, Y, n, b, Z)

% hist_2d - 2 dimensional histogram
% ---------------------------------
%
% [H, c, v] = hist_2d(X, Y, n, b, Z)
%
%           = hist_2d(X, p, n, b, Z)
%
% Input:
% ------
%  X,  Y - input data
%  p - planes or frames to use
%  n - number of bins (def: 128)
%  b - value bounds (def: value range)
%  Z - computation mask (def: [])
%
% Output:
% -------
%  H - bin counts
%  c - bin centers
%  v - bin breaks
%
% See also: hist_1d

%--
% set mask
%--

if nargin < 5
	Z = [];
end

%--
% get planes or frames
%--

% NOTE: for multi-dimensional images and image stacks the second input is a set of indices

if ndims(X) == 3
	p = Y; Y = X(:, :, p(2)); X = X(:, :, p(1));
	
elseif iscell(X)
	p = Y; Y = X{p(2)}; X = X{p(1)};
end

%--
% set bounds
%--

if (nargin < 4) || isempty(b)
	b = [fast_min_max(X); fast_min_max(Y)];
	
elseif all(size(b) == [1, 2])
	b = [b; b];
end

%--
% set number of bins
%--

if (nargin < 3) || isempty(n)
	n = [128, 128];
	
elseif (length(n) == 1)
	n = [n, n];
end

%--
% compute histogram using mex depending on data type
%--

if strcmp(class(X), class(Y))
	
	[H, c.x, c.y, v.x, v.y] = hist_2d_mex(X, Y, n(1), n(2), b(1, :), b(2, :), uint8(Z));
else
	% TODO: consider casting only to the greater type rather than the ultimate type
	
	% NOTE: in the case of mixed data we cast to double
	
	[H, c.x, c.y, v.x, v.y] = hist_2d_mex(double(X), double(Y), n(1), n(2), b(1, :), b(2, :), uint8(Z));
end

%--
% display if output not captured
%--

if ~nargout
	
	hist_2d_view(H, c, v); clear H;
end
