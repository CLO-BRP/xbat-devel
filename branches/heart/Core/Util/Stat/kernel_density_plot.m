function [handles, density, grid] = kernel_density_plot(ax, data, grid, varargin)

% kernel_density_plot - for a set of data points
% ----------------------------------------------
%
% [handles, density, grid] = kernel_density_plot(ax, data, grid, field, value, ... )
%
% Input:
% ------
%  ax - parent
%  data - points
%  grid - to evaluate density on
%  field, value - options for 'ksdensity'
%
% Output:
% -------
%  handles - displayed
%  density - computed
%  grid - used

%--
% handle input
%--

if ~nargin
	ax = gca;
end

if nargin < 3
	range = fast_min_max(data); 
	
	grid = linspace(range(1), range(2), 128);
end 

density = ksdensity(data, grid, varargin{:});

handles = line( ...
	'parent', ax, ... 
	'xdata', grid, ...
	'ydata', density ... 
);

handles(end + 1) = data_points_plot(ax, data);


%----------------------
% DATA_POINTS_PLOT
%----------------------

% TODO: factor as it's own function, create centered marker display as well

function handle = data_points_plot(ax, data, varargin)

xdata = repmat(data(:)', 2, 1); xdata(3, :) = nan;

ylim = get(ax, 'ylim'); ylim = [ylim(1), ylim(1) + 0.1 * diff(ylim)];

ydata = repmat(ylim(:), 1, numel(data)); ydata(3, :) = nan;

handle = line( ...
	'parent', ax, ...
	'xdata', xdata(:), ...
	'ydata', ydata(:), ...
	varargin{:} ...
);