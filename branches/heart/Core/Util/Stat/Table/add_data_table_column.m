function table = add_data_table_column(table, name, value, replace)

% add_table_data_column - given name and values or computation
% ------------------------------------------------------------
%
% table = add_data_table_column(table, name, value, replace)
%
% Input:
% ------
%  table - to update
%  name - of column
%  value - or function 
%  replace - indicator, allows update of existing column
%
% Output:
% -------
%  table - updated
%
% See also: create_data_table, get_data_table_column

%--
% check input
%--

if nargin < 4
	replace = 0;
end 

[clash, index] = string_is_member(name, table.column.names);

if ~replace && clash
	error(['Table already has a ''', name, ''' column.']);
end

rows = size(table.data, 1);

if numel(value) ~= rows
	error(['Number of values (', numel(value), ') must match number of table rows (', rows, ').']);
end

%--
% append column to table
%--

if ~clash
	index = size(table.data, 2) + 1;
end

table.column.names{index} = name;

if iscellstr(value)
	
	if isnumeric(table.data)
		table.data = num2cell(table.data);
	end
	
	table.data(:, index) = value(:);

	table.column.types{index} = 'string';
	
else
	
	table.data(:, index) = value(:);
	
	table.column.types{index} = 'numeric';
	
end

