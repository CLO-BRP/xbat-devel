function table = arff_to_table(file)

% arff_to_table - load arff file as table
% ---------------------------------------
%
% table = arff_to_table(file)
%
% Input:
% ------
%  file - to load
%
% Output:
% -------
%  table - representation of file

%--
% read file as lines
%--

opt = file_readlines; opt.pre = '%'; opt.skip = 1;

lines = file_readlines(file, [], opt);

%--
% parse header
%--

% NOTE: in the header we get the table name (arff 'relation') and column description (arff 'attributes')

name = ''; columns = {}; types = {};

for k = 1:numel(lines)
	
	current = lines{k};
	
	if current(1) ~= '@'
		continue;
	end
	
	% TODO: use regular expressions with capture to parse lines, categorical attributes in particular
	
	part = str_split(current(2:end), ' ');
	
	% HACK: sometimes string names are quoted in ARFF, if it starts with punctuation 'unquote'

	if numel(part) > 1 && isstrprop(part{2}(1), 'punct')
		part{2} = part{2}(2:end - 1);
	end
	
	% NOTE: coalesce over-partitioned line
	
	if numel(part) > 3
		part{3} = [part{3:end}]; part(4:end) = [];
	end
	
	switch lower(part{1})
		case 'relation'
			name = part{2};
			
		case 'attribute'
			columns{end + 1} = part{2}; types{end + 1} = part{3}; %#ok<AGROW>
			
		case 'data'
			break;
	end
	
end

table = create_data_table('name', name, 'column__names', columns, 'column__types', types);

%--
% parse data
%--

row = 1; data = {};

for j = k + 1:numel(lines)
	current = str_split(lines{j}, ',', @str_value);
	
	if isnumeric(current)
		current = num2cell(current);
	end
	
	data(row, :) = current; row = row + 1; %#ok<AGROW>
end

% NOTE: we do not always have a pure numeric matrix, but when we do ...

try %#ok<TRYNC>
	data = cell2mat(data);
end

%--
% create table
%--

table = create_data_table( ...
	'name', name, ...
	'column__names', columns, ...
	'column__types', types, ...
	'data', data ...
);


%--------------------
% STR_VALUE
%--------------------

function value = str_value(str)

% NOTE: ARFF uses the question to indicate missing values

if str == '?'
	value = nan; return; 
end

% NOTE: try to convert to number, otherwise we are dealing with a categorical

value = str2double(str);

if isnan(value)
	value = str;
end 


