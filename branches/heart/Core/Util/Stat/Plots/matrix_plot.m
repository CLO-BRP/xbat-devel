function [par, ax, handles] = matrix_plot(table, vars, display, par)

% matrix_plot - of table data
% ---------------------------
%
% [par, ax, handles] = matrix_plot(table, vars, display, par)
% 
% Input:
% ------
%  table - with data
%  vars - to use for 'x', 'y', 'size', 'color' and 'group'
%  display - to produce in 'plot', 'hover', 'brush'
%
% Output:
% -------
%  par - figure
%  ax - axes in matrix
%  handles - of plotted objects

% TODO: what are the assumptions, common axes per column and row

%--
% handle input
%--

if nargin < 4
	par = [];
end

if nargin < 3
	display.plot = @matrix_scatter;
	
	%  TODO: implement some default hover and brush behaviors
	
	display.hover = []; display.brush = [];
end

% NOTE: allow simple input for the simple case

if nargin < 2 || trivial(vars) || ~isfield(vars, 'x')
	
	if isempty(table.column.types)
		names = table.column.names;
	else
		names = table.column.names(iterate(@strcmp, table.column.types, 'numeric'));
	end
	
	vars.x = iteraten(@create_var, 2, 'column', names);

elseif iscellstr(vars.x)
	
	% NOTE: this is simplified input for the not simplest case
	
	vars.x = iteraten(@create_var, 2, 'column', vars.x);

end

% NOTE: full matrix plot is default

if ~isfield(vars, 'y')
	vars.y = vars.x;
end

% NOTE: no use of 'size', 'color', and 'group' variables default

for type = {'size', 'color', 'group'}
	
	if ~isfield(vars, cellfree(type))
		vars.(cellfree(type)) = empty(create_var);
	end
	
end

%--
% check input values
%--

xvars = {vars.x.column};

xmiss = xvars(~string_is_member(xvars, table.column.names));

if ~isempty(xmiss)
	error(['Requested ''x'' variables ''', str_implode(xmiss, ''', '''), ''' are not available.']);
end

yvars = {vars.y.column};

ymiss = yvars(~string_is_member(yvars, table.column.names));

if ~isempty(ymiss)
	error(['Requested ''y'' variables ''', str_implode(ymiss, ''', '''), ''' are not available.']);
end

if numel(vars.group)
	gvar = vars.group.column;
	
	if ~isempty(gvar) && ~string_is_member(gvar, table.column.names)
		error(['Requested grouping variable ''', gvar, ''' is not available.']);
	end
else
	gvar = [];
end

% TODO: add checking for 'color' and 'size' variables

%--
% layout matrix and plot table data display
%--

if isempty(gvar)
	data.group = [];
else
	data.group = get_data_table_column(table, gvar);
	
	% NOTE: in the case of a categorical grouping variable we pass the index representation to the helper
	
	if iscellstr(data.group)
		data.group = cat2index(data.group);
	end
end

% TODO: allow for some layout configuration

[par, ax, base] = layout_matrix_plot(par, vars); handles = cell(size(ax));

% NOTE: the matrix is used to display the collection of 'x' and 'y' vars

for row = 1:numel(yvars)
	
	for col = 1:numel(xvars)

		current = ax(row, col); 
		
		%--
		% get plot data
		%--
		
		if iscell(table.data)
			data.x = [table.data{:, col}]; data.y = [table.data{:, row}];
		else
			data.x = table.data(:, col); data.y = table.data(:, row);
		end
		
		%--
		% plot using callback
		%--
		
		% NOTE: the plot helper handles the self plotting case
		
		if strcmp(xvars{col}, yvars{row});
			data.y = [];
		end
		
		% NOTE: the display plot helper handles the various types of data available
		
		if iscell(display.plot)
			handles{row, col} = display.plot{1}(current, data, display.plot{:});
		else
			handles{row, col} = display.plot(current, data);
		end
		
	end 
	
end 

% TODO: this definitely belongs in the layout functions

%--
% display title and setup display behaviors
%--

handle = title(base, table.name); 

set(handle, 'visible', 'on', 'verticalalignment', 'top', 'tag', 'matrix_plot::title');

set(par, 'windowbuttonmotionfcn', {@hover, ax, display.hover});

%--
% store plot data
%--

userdata = get(par, 'userdata');

userdata.matrix_plot.table = table; 

userdata.matrix_plot.vars = vars;

userdata.matrix_plot.display = display;

% userdata.matrix_plot.handles.ax = ax;
% 
% userdata.matrix_plot.handles.base = base;


%--------------------------
% HOVER
%--------------------------

% TODO: the 'ax' input should probably be a 'data' input

function hover(par, eventdata, ax, callback) %#ok<INUSL>

%--
% determine current hover axes, if available set as current axes
%--

point = get(par, 'currentpoint'); current = [];

for k = 1:numel(ax)
	
	pos = get_size_in(ax(k), 'pixels');
	
	if point(1) > pos(1) && point(1) < pos(1) + pos(3) && point(2) > pos(2) && point(2) < pos(2) + pos(4)
		current = ax(k); break;
	end
	
end

set(ax, 'linewidth', 0.5);

if isempty(current)
	return;
end

set(par, 'currentaxes', current);

%--
% update display to indicate hover
%--

set(current, 'linewidth', 2);

% TODO: tag axes labels so we can select? or simply pack and pass!

%--
% perform user specified callback
%--

% TODO: develop structure for callback, we should pack relevant computed stuff

if nargin > 3 && ~isempty(callback)
	
	try
		if iscell(callback)
			callback{1}(ax, current, callback{2:end});
		else
			callback(ax, current);
		end
	catch
		nice_catch(lasterror, 'User hover callback failed.');
	end
	
end



