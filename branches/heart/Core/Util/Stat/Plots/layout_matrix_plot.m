function [par, ax, base] = layout_matrix_plot(par, vars) 

% layout_matrix_plot - axes for two way plotting of many variables
% ----------------------------------------------------------------
%
% [par, ax] = layout_matrix_plot(par, vars)
%
% Input:
% ------
%  par - figure
%  vars - to use
%
% Output:
% -------
%  par - figure
%  ax - axes for matrix plot

%--
% handle input
%--

if isempty(par)
	par = fig;
else
	clf(par);
end

%--
% create harray base
%--

m = numel(vars.y); n = numel(vars.x); 

layout = layout_create(m, n);

layout.margin(3:4) = 1.25; 

layout.row.pad = 0.25 * ones(1, m - 1); 

layout.col.pad = 0.25 * ones(1, n - 1);

ax = harray(par, layout); base = ax(1); ax(1) = []; ax = reshape(ax, m, n);

%--
% set conventional tags for finding matrix axes
%--

for row = 1:m
	for col = 1:n
		set(ax(row, col), 'tag', fields2tag(vars.x(col).column, vars.y(row).column));
	end
end

%--
% update matrix axes in various ways
%--

% NOTE: add axes labelling along the left and bottom of the matrix

% TODO: add tags to these labels?

for k = 1:numel(vars.y)
	ylabel(ax(k, 1), vars.y(k).label);
end

for k = 1:numel(vars.x)
	xlabel(ax(end, k), vars.x(k).label);
end

% NOTE: display ticks only on peripheral axes, alternate display of ticks in rows and columns

set(ax(1, :), 'xaxislocation', 'top');

set([ax(1, 2:2:end), ax(end, 1:2:end)], 'xticklabel', []);

set(ax(2:end - 1, :), 'xticklabel', []);

set(ax(:, end), 'yaxislocation', 'right');

set([ax(2:2:end, 1); ax(1:2:end, end)], 'yticklabel', []);

set(ax(:, 2:end - 1), 'yticklabel', []);

% TODO: consider setting the various axes limits

% NOTE: the 'diagonal' of the matrix deserves special attention


