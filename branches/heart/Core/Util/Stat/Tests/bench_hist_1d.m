function R = bench_hist_1d(X, n)

% bench_hist_1d - benchmark cuda accelerated histogram code
% ---------------------------------------------------------
%
% R = bench_hist_1d(X,n)
%
% Input:
% ------
%  X - input image
%  n - number of iterations, default 10
%
% Output:
% -------
%  R - timing and accuracy results

if ((nargin < 3) || isempty(n))
	n = 10;
end

for k = 1:n

	tic; [Y1, ignore, ignore] = hist_1d(X, 256);  t1(k) = toc;
	tic; Y2 = cuda_histogram256_mex(X); t2(k) = toc;

	E(k,:) = fast_min_max(Y1 - double(Y2));
end

disp(' ');
disp('TIME, CUDA TIME, SPEEDUP, ERROR');

R = [t1', t2', (t1./t2)', E]

