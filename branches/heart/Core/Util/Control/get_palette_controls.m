function controls = get_palette_controls(pal, varargin)

% get_palette_controls - get palette controls array
% -------------------------------------------------
%
% controls = get_palette_controls(pal, varargin)
%
% Input:
% ------
%  pal - palette handle (def: gcf) 
%  varargin - control field value pairs
%
% Output:
% -------
%  controls - control array

%------------------------------
% GET CONTROLS
%------------------------------

% NOTE: this sometimes fails as the handle is 'beingdeleted', the exception is more reliable than checking the handle

try
    data = get(pal, 'userdata');
catch
    data = [];
end

if isempty(data) || ~isfield(data, 'control')
	controls = empty(control_create);
else
	controls = data.control;
end

if isempty(controls)
	return;
end

%------------------------------
% SELECT CONTROLS
%------------------------------

if isempty(varargin)
	return;
end

[field, value] = get_field_value(varargin);

for k = 1:numel(field) 
	
	if ~isfield(controls, field{k})
		continue; 
	end 
	
	match = iterate(@isequal, {controls.(field{k})}, value{k});
	
	controls(~match) = [];
	
	if isempty(controls)
		break;
	end
	
end



