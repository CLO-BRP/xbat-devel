function control = popup_control(name, list, value, varargin)

% popup_control - simpler popup menu control creation
% ---------------------------------------------------
%
% control = popup_control(name, list, value, varargin)
% 
% Input:
% ------
%  name - and possibly alias for control
%  list - of values
%  value - current
%  varargin - control field value pairs
%
% Output:
% -------
%  control - structure

%--
% get alias from name
%--

if iscell(name)
	alias = name{2}; name = name{1};
else
	alias = name;
end

%--
% get value index from list and value if needed
%--

if nargin < 3 || isempty(value)
	ix = 1;
end

% NOTE: we are lax about matching a value from the list, tolerate space and case problems

if ischar(value)
	ix = find(strcmpi(strtrim(list), strtrim(value)));
else
	ix = value;
end

if isempty(ix) 
	error('Value is not found in value list.'); 
end

if ix < 1 || ix > numel(list)
	error('Value index is out of range of list.');
end

%--
% create control
%--

control = control_create( ...
	'name', name, ...
	'alias', alias, ...
	'style', 'popup', ...
	'string', list, ...
	'value', ix, ... 
	varargin{:} ...
);
