function value = is_tabs_control(control)

% is_tabs_control - answer this question using callback context control
% ---------------------------------------------------------------------
%
% value = is_tabs_control(control) 
%
% Input:
% ------
%  control - from callback context
%
% Output:
% -------
%  value - indicator

value = isfield(control, 'handle') && strcmp(get(control.handle, 'type'), 'rectangle');