function [updated, toggle] = set_toggle_by_label(pal, str, state)

% set_toggle_by_label - set header toggle state using label
% ---------------------------------------------------------
% 
% [updated, toggle] = set_toggle_by_label(pal, str, state)
%
% Input:
% ------
%  pal - palette
%  str - label string
%  state - desired state
%
% Output:
% -------
%  updated - update indicator
%  toggle - toggle handle

%--
% get toggle to set
%--

[toggle, current] = get_toggle_by_label(pal, str);

if isempty(toggle)
	updated = 0; return;
end

%--
% return if there is nothing to do
%--

if (nargin < 3) || strcmp(state, current)
	updated = 0; return; 
end

%--
% set toggle state
%--

palette_toggle(pal, toggle, state);

