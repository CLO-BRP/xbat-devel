function close_palettes(obj,eventdata,fun)

% close_palettes - close parent palettes
% --------------------------------------
% 
% close_palettes(obj,event,fun)
%
% Input:
% ------
%  obj - parent handle
%  event - reserved
%  fun - clean fun

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

%--
% set close fun
%--

if (nargin < 3)
	fun = @closereq;
end

%--
% get palette handles
%--

data = get(obj,'userdata');

pal = data.browser.palette.handle;

%--
% close all palettes
%--

for k = 1:length(pal)	
	close(pal(k),'force');
end

%--
% yield to fun
%--

if (~isempty(fun))
	fun();
end
