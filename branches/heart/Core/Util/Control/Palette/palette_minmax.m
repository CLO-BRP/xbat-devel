function palette_minmax(state, pal)

% palette_minmax - minimize or maximize palette
% ---------------------------------------------
%
% palette_minmax(state, pal)
%
% Input:
% ------
%  state - 'min' or 'max'
%  pal - palette figure handle

% TODO: problems stopping and starting timers

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% set handle
%--

if (nargin < 2) || isempty(pal)
	pal = gcf;
end 

%--
% check handle
%--

if ~ishandle(pal) || ~strcmpi(get(pal, 'type'), 'figure')
	error('Input is not figure handle.');
end

%--
% check for properly tagged figure
%--

% NOTE: a palette figure has 'PALETTE', 'WAITBAR' or 'DIALOG' in the tag

% NOTE: a fully collapsed dialog is strange, action buttons are hidden 

tag = get(pal, 'tag');

if ( ...
	isempty(findstr(tag, 'PALETTE')) && ...
	isempty(findstr(tag, 'WAITBAR')) && ...
	isempty(findstr(tag, 'DIALOG')) ...
)

	disp('WARNING: Handle does not point to a palette.'); return;

end

%--
% check state
%--

switch lower(state)
	
	case 'min', state = 'close';
		
	case 'max', state = 'open';
		
	otherwise, error(['Unrecognized palette state ''', state, '''.']);
		
end

%-----------------------------
% HANDLE INPUT
%-----------------------------

%--
% get all palette toggles
%--

toggle = findobj(pal, 'tag', 'header_toggle');

%--
% apply state to toggles
%--

% NOTE: timers off and forced display queue flush seems to work

% stop(timerfind);

for k = 1:length(toggle)
	
	palette_toggle(pal, toggle(k), state); 
	
	% NOTE: put this as optional in palette toggle

	drawnow; drawnow; % pause(0.025);
	
end

% start(timerfind); 
