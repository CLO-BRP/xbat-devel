function [value, name, type] = is_palette(pal)

% is_palette - determine whether figure is palette
% ------------------------------------------------
%
% [value, name, type] = is_palette(pal)
%
% Input:
% ------
%  pal - figure handle
%
% Output:
% -------
%  value - result of palette test
%  name - name of palette figure
%  type - palette type

%--
% check for figure handles
%--

if ~is_handles(pal, 'figure')
	value = 0; return;
end

%--
% test for palette tag
%--

% NOTE: uses convention of using 'DIALOG, 'PALETTE', 'WAITBAR', or 'WIDGET' in tag

types = {'DIALOG', 'PALETTE', 'WAITBAR', 'WIDGET'}; value = 0;

tag = get(pal, 'tag'); 

for k = 1:length(types)
	
	if ~isempty(strfind(tag, types{k}))
		value = 1; break;
	end
	
end

%--
% return name and type for convenience
%--

if nargout > 1
	name = get(pal, 'name');
end

if nargout > 2
	
	if value
		type = lower(types{k});
	else
		type = '';
	end
	
end

