function root = palette_preset_root(user)

if ~nargin
	user = get_active_user;
end

root = fullfile(user_root(user, 'preset'), 'Palette');