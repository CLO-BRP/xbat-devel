function value = get_palette_property(pal, field)

% get_palette_property - get palette property
% -------------------------------------------
%
% value = get_palette_parent(pal, field)
%
% Input:
% ------
%  pal - palette handle
%  field - property field name
%
% Output:
% -------
%  value - parent handle

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% check input is palette
%--

if ~is_palette(pal)
	error('Input handle is not a palette.');
end

%--
% get state data from palette
%--

data = get(pal, 'userdata');

% NOTE: return palette state for no field input

if nargin < 2
	value = data; return;
end

%--
% check field name
%--

if ~ischar(field)
	error('Palette field name must be a string.');
end

fields = {'name', 'parent', 'children', 'control', 'opt', 'created'};

field = lower(field);

if ~ismember(field, fields)
	error('Input field is not a palette property.');
end

%-----------------------
% GET PROPERTY
%-----------------------

switch field
	
	case 'parent'
		
		value = get_field(data, 'parent', []);
		
		% NOTE: at the moment a screen parent means no parent
		
		if value == 0
			value = [];
		end
		
	otherwise
		
		% NOTE: we silently default to empty values when we can't find the field
		
		value = get_field(data, field, []);
		
end
