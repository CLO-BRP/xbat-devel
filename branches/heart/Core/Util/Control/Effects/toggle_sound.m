function toggle_sound(action)

% toggle_sound - produce sound to indicate palette toggle action
% --------------------------------------------------------------
%
% toggle_sound(action)
%
% Input:
% ------
%  action - toggle action, 'open' or 'close'

% TODO: consider where to check for audio devices

%---------------------------
% HANDLE INPUT
%---------------------------

return;

%--
% return if palette sounds are not on
%--

% TODO: the user preference retrieval should be faster, otherwise we don't need this

if ~get_user_preference(get_active_user, 'palette_sounds')
	return;
end

%---------------------------
% PRODUCE SOUND
%---------------------------

%--
% create waveform and persist
%--

persistent TOGGLE_SIGNAL;

if ~isfield(TOGGLE_SIGNAL, action)

	switch action

		case {'open', 'close'}

			n = 128; 
			
			X = linspace(0, 0.1, n);
						
			X = 0.5 * X .* (1 - X) .* filter(ones(1, 9) ./ 9, [0.5, zeros(1, 16), 0.1, 0, 0.1], rand(1, n));

		otherwise, return;

	end
	
	TOGGLE_SIGNAL.(action) = X;
	
end

%--
% play sound
%--

if ispc
    wavplay(TOGGLE_SIGNAL.(action), 'sync');
else
    sound(TOGGLE_SIGNAL.(action));
end
