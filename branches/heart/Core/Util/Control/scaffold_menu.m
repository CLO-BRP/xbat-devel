function handle = scaffold_menu(par, type, context, skip)

%--
% initialize output and handle input
%--

handle = [];

if nargin < 4
	skip = {};
end 

%--
% get extension controls for type if possible
%--

control = get_extension_controls_of_type(type, context.ext, context);

if isempty(control)
	return;
end

%--
% select controls we may represent and filter with skip list
%--

for k = numel(control):-1:1
	
	if ~string_is_member(control(k).style, {'checkbox', 'popup'})
		control(k) = []; continue;
	end
	
	if ~isempty(skip) && string_is_member(control(k).name, skip)
		control(k) = []; continue;
	end
	
end

%--
% build menu for remaining controls
%--

% TODO: develop and use 'menu_control' to build these menus

handle = []; 

for k = 1:numel(control)
	
	current = control(k);
	
	%--
	% create tab menu if needed
	%--
	
	if isempty(current.tab)
		top = par;
	else
		top = create_menu(par, current.tab, 'label', [current.tab, '   ']);
	end
	
	%--
	% create control menus based on control style
	%--
	
	switch current.style
		
		case 'checkbox'

			% NOTE: the representation of the checkbox control is straightforward
			
			handle(end + 1) = uimenu(top, ...
				'label', get_menu_label(current), 'tag', current.name, 'check', ternary(current.value, 'on', 'off') ...
			);
		
		case 'popup'
			
			% NOTE: for the popup menu we create a parent menu with the control name, then children with the options
			
			top = uimenu(top, ...
				'label', [get_menu_label(current), '   '], 'tag', current.name ...
			);
		
			for j = 1:numel(current.string)
				
				handle(end + 1) = uimenu(top, ...
					'label', current.string{j}, 'tag', current.name, 'check', ternary(current.value == j, 'on', 'off') ...
				);
			
			end
			
	end

end


%------------------------------
% GET_MENU_LABEL
%------------------------------

function label = get_menu_label(control)

if ~isempty(control.alias) && ~string_is_member(control.alias, {'on'})
	label = control.alias;
else
	label = control.name;
end

label = title_caps(label);



