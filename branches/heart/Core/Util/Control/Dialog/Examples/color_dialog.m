function out = color_dialog(name, head, color)

% color_dialog - color selection dialog
% -------------------------------------
%
% out = color_dialog(name, head, color)
%
% Input:
% ------
%  name - dialog name
%  head - header message
%  color - initial color
%
% Output:
% -------
%  out - dialog output structure

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set default color
%--

if (nargin < 3) || isempty(color)
	color = get_color_by_name('Sea Green');
end

%--
% set default head
%--

if (nargin < 2) || isempty(head)
	head = 'Color';
end

%--
% set default name
%--

if (nargin < 1) || isempty(name)
	name = 'Select Color';
end

%----------------------------------
% CREATE DIALOG
%----------------------------------

%--
% create controls and configure dialog
%--

control = color_dialog_controls(color, head);

opt = dialog_group;

opt.width = 10;

%--
% create dialog
%--

out = dialog_group(name, control, opt, @color_dialog_callback);

if ~isempty(out.values)
	
	flatten(out)
	
	out.values.name = cellfree(out.values.name);
end
