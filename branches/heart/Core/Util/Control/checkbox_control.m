function control = checkbox_control(name, value, varargin)

% checkbox_control - shortcut to create a checkbox control
% --------------------------------------------------------
%
% control = checkbox_control(name, value, varargin)
%
% Input:
% ------
%  name - and alias if cell input
%  value - of control
%  varargin - field value pairs for control creation =
%
% Output:
% -------
%  control - description

%--
% get alias from name
%--

if iscell(name)
	alias = name{2}; name = name{1};
else
	alias = name;
end

%--
% set value
%--

if nargin < 2 || isempty(value)
	value = 0;
end

%--
% create control
%--

control = control_create( ...
	'name', name, ...
	'alias', alias, ...
	'style', 'checkbox', ...
	'value', value, ...
	varargin{:} ...
);