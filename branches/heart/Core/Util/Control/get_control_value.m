function value = get_control_value(pal, name, controls)

% get_control_value - shortcut to get control value
% -------------------------------------------------
%
% value = get_control_value(pal, name)
%
% Input:
% ------
%  pal - handle
%  name - of control
%
% Output:
% -------
%  value - of control

if nargin < 3
	controls = get_palette_controls(pal);
end

value = get_control(pal, name, 'value', controls);
