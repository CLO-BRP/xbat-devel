function control = control_separator(label, align, varargin)

% control_separator - create separator control
% --------------------------------------------
%
% control = control_separator(label, align, varargin)
%
% Input:
% ------
%  label - for separator
%  align - label
%  varargin - control field value pairs
%
% Output:
% -------
%  control - structure

%--
% set default no label and left alignment
%--

if ~nargin
	label = '';
end

if nargin < 2 || isempty(align)
	align = 'left';
end

%--
% create control considering field value input
%--

control = control_create( ...
	'string', label, ...
	'align', align, ...
	'style', 'separator', ...
	varargin{:} ...
);