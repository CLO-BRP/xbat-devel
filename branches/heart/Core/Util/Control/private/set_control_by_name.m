function updated = set_control_by_name(pal, control)

% set_control_by_name - update control using name
% -----------------------------------------------
%
% updated = set_control_by_name(pal, control)
%
% Input:
% ------
%  pal - palette handle
%  control - array to set
%
% Output:
% -------
%  updated - indicator

%--
% update controls
%--

controls = get_palette_controls(pal); updated = zeros(size(control));

for k = 1:numel(control)

	%--
	% find control by name 
	%--
	
	[ignore, ix] = get_control_by_name(pal, control(k).name, controls);

	%--
	% update control if found
	%--
	
	if ~isempty(ix)
		controls(ix) = control(k); updated(k) = 1;
	end

end

%--
% set palette controls
%--

% NOTE: set function will render palette according to the new controls

flag = set_palette_controls(pal, controls);


