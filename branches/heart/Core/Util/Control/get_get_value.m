function value = get_get_value(varargin)

% get_get_value - get value of control possibly considering 'get' behavior
% ------------------------------------------------------------------------
% 
% value = get_get_value(control, value)
%
% Input:
% ------
%  control - to get value for
%  value - to 'get'
%
% Output:
% -------
%  value - after 'get'

value = get_value_through_callback('get', varargin{:});
