function handle = pcp(X, label, opt)

% pcp - parallel coordinates plot
% -------------------------------
%
% handle = pcp(X, label, opt)
%
%    opt = pcp;
%
% Input:
% ------
%  X - data
%  label - strings
%  opt - options
%
% Output:
% -------
%  handle - struct
%  opt - defaults

% NOTE: these plots are complex and interactive, based on harray they occupy a figure

%--
% handle input and setup
%--

if nargin < 3
	opt.range = true;
	
	opt.par = [];
	
	if ~nargin && nargout
		handle = opt; return;
	end
end

if isempty(opt.par)
	opt.par = fig;
end

%--
% create plot
%--

layout = layout_create(1, 1);

ax = harray(opt.par, layout); ax = ax(end);
