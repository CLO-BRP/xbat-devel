function clear_radial_grid(ax, prefix)

% clear_radial_grid - from axes considering prefix
% ------------------------------------------------
%
% clear_radial_grid(prefix)
%
% Input:
% ------
%  ax - handle to parent
%  prefix - for grid (def: same as 'set_radial_grid')
%
% See also: set_radial_grid

if nargin < 2
	opt = set_radial_grid; prefix = opt.prefix;
end

delete(find_prefixed_handles(ax, prefix));