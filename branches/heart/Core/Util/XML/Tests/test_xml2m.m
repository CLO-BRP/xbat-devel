function [result, xml, recovered] = test_xml2m(in)

%--
% serialize, recover and compare
%-- 

xml = m2xml(in);

recovered = xml2m(xml);

% TODO: from here on this can be factored

result = isequal(in, recovered);