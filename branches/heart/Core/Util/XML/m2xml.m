function xml = m2xml(varargin)

% m2xml - generates invertible xml representation of matlab value
% ---------------------------------------------------------------
%
% xml = m2xml(value, name)
%
% Input:
% ------
%  value - matlab value
%  name - variable name
%
% Output:
% -------
%  xml - xml representation
%
% NOTE: for non-numeric input this is part of a very simple wrapper for the XML4MAT toolbox

% NOTE: we call the XML4MAT function and add a prefix

if isnumeric(varargin{1})
	xml = ['<!--M2SML-->', m2str(varargin{1})];
else
	xml = ['<!--M2XML-->', mat2xml(varargin{:})];
end

xml = strrep(xml, 'NaN', 'NULL');