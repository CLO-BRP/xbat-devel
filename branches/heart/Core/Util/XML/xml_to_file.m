function flag = xml_to_file(str,out,xsl)

% xml_to_file - create xml file
% -----------------------------
%
% flag = xml_to_file(str,out,xsl)
%
% Input:
% ------
%  str - input xml string
%  out - output file
%  xsl - xsl transformation to attach
%
% Output:
% -------
%  flag - success flag

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set xsl
%--

if (nargin < 3)
	xsl = '';
end

%--------------------------------
% CREATE XML FILE
%--------------------------------

%--
% open file, create if needed
%--

fid = fopen(out,'wt');

%--
% append header
%--

fprintf(fid,'<?xml version = "1.0" encoding = "UTF-8" ?>\n');

%--
% append style if available
%--

if (~isempty(xsl))
	fprintf(fid,['<?xml:stylesheet type = "text/xsl" href = "' xsl '.xsl" ?>\n']);
end

%--
% append string
%--

% this is assumed to be valid xml

fprintf(fid,str);

%--
% close file
%--

fclose(fid);