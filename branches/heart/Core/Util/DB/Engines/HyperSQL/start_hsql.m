function start_hsql(name)

% start_hsql - database server
% ----------------------------
%
% start_hsql(name)
%
% Input:
% ------
%  name - of database
%
% See also: install_hsql

% TODO: we may want something a bit more specific

tool = get_tool('runServer.bat');

if isempty(tool)
	error('You must first install HyperSQL by calling ''install_hsql''.');
end

% NOTE: we change directory for the sake of the 'runServer.bat' script

start = pwd; cd(tool.root);

eval(['!"', tool.file '" --database.0 file:', name, ' --dbname.0 ', name, '&']);

cd(start);