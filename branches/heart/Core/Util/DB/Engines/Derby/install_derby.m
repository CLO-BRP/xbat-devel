function installed = install_derby

% install_derby - java database engine and JDBC drivers
% ------------------------------------------------------
%
% installed = install_jdbc
%
% Output:
% -------
%  installed - indicator
%
% See: start_derby

name = 'Derby';

%--
% check for jars in tool directory
%--

% NOTE: the 'create_dir' filter ensures that the directory exists

root = create_dir(tool_root(name));

% NOTE: we assume that jars in the driver directory are drivers, no need to download

jars = get_jars(root);

if ~isempty(jars)
	
	append_classpath(jars); installed = true; return;
end

%--
% install tool if needed
%--

if install_tool(name, 'http://xbat.org/downloads/installers/db-derby-10.7.1.1-bin.zip');
	
	jars = get_jars(root);

	if ~isempty(jars)
		
		append_classpath(jars); installed = true;
	end
end
