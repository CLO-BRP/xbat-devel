function root = derby_driver_root

% derby_driver_root - JDBC driver location
% ----------------------------------------
%
% root = derby_driver_root
% 
% Output:
% -------
%  root - location of jar containing driver (and server)
%
% See also: jdbc_driver_root

% NOTE: we examine the dynamic java classpath for the presence of the jar 

candidate = javaclasspath('-dynamic'); root = '';

for k = 1:numel(candidate)
	
	if string_contains(candidate{k}, 'derbyclient.jar')
		
		root = candidate{k}; break;
	end
end

if ~isempty(root)
	root = fileparts(root);
end