function start_derby

% start_derby - database server
% -----------------------------
%
% start_derby
%
% See also: install_derby

tool = get_tool('startNetworkServer.bat');

if isempty(tool)
	error('You must first install Derby using ''install_derby''.');
end

eval(['!"', tool.file, '" &']);