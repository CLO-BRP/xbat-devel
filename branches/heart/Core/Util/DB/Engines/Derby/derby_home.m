function home = derby_home

% derby_home - installation directory
% -----------------------------------
%
% home = derby_home 
%
% Output:
% -------
%  home - directory
%
% See also: derby_driver_root

home = fileparts(derby_driver_root);