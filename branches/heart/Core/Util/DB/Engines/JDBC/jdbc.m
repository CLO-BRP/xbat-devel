function [status, result] = jdbc(config, sql, opt)

% jdbc - use JDBC to connect to a database
% ----------------------------------------
%
% [status, result] = jdbc(config, sql, opt)
%
% Input:
% -------
%  config - all the configs needed for JDBC to work
%  sql - statements to be executed
%
% Output:
% -------
% status - status code?
% result - resultset in a struct

%-----------------
% HANDLE INPUT
%-----------------

%--
% create and possibly output default options
%--

% NOTE: we may have some options

if nargin < 3
	opt = struct; 
	
	opt.trace = 0; 
	
	opt.log = 0; 
	
	opt.action = 'get'; 
	
	opt.retry = 0;
end

if ~nargin
	status = opt; return;
end

%--
% compile config and create connection, possibly output compiled config
%--

[connection, problem] = jdbc_connection(config, opt.action);

if nargin < 2 || isempty(sql)
	status = config; return;
end

%--
% check for failed connection
%--

% TODO: include config info and error info

if isempty(connection)
	nice_catch(problem); 
	
	error('Failed to establish JDBC connection.');
end

%-----------------
% EXECUTE QUERY
%-----------------

% logger('info', sprintf('Executing:\n%s', sql_string(sql)));

%--
% consider minor adapter differences in query syntax
%--

% TODO: we should have a 'rewrite' method just as we have a 'query' method

switch lower(config.adapter)

	case 'mysql'

		% TODO: consider doing the 'INSERT OR REPLACE' to 'REPLACE' conversion here

		% TODO: add the FOREIGN_KEY to FOREIGN KEY conversion here as well
		
		if ~iscell(sql)
			sql = strrep(sql, 'AUTOINCREMENT', 'AUTO_INCREMENT');
		else
			sql = iterate(@strrep, sql, 'AUTOINCREMENT', 'AUTO_INCREMENT');
		end
		
	case 'hypersql'
		
		if ~iscell(sql)
			sql = strrep(sql, 'AUTOINCREMENT', ''); 
			
			sql = strrep(sql, 'PRIMARY KEY', 'IDENTITY'); 
		else
			sql = iterate(@strrep, sql, 'AUTOINCREMENT', '');
			
			sql = iterate(@strrep, sql, 'PRIMARY KEY', 'IDENTITY');
		end
		
	case 'derby'
		if ~iscell(sql)
			sql = strrep(sql, 'IF NOT EXISTS', ''); 
		else
			sql = iterate(@strrep, sql, 'IF NOT EXISTS', '');
		end
end

% timer = start_logging;

%--------------------------
% EXECUTE BATCH STATEMENTS
%--------------------------

% TODO: this transformation is too stupid, figure out how to this efficiently and smarter

if iscell(sql)
	queries = sql;
else
	queries = str_split(sql, ';');
end

switch lower(config.adapter)
	
	case 'hypersql'
		% TODO: why is this the case?
		
		if strcmpi(queries{1}, 'BEGIN;') && strcmpi(queries{end}, 'COMMIT;')
			queries = queries(2:end - 1);
		end
end

try	
	%--
	% consider batch queries, these may be complex transactions
	%--
	
	% NOTE: the return here assumes that no result is available
	
	if numel(queries) > 1
		[status, result] = jdbc_execute_batch(connection, queries); return;
	end
	
	%--
	% call execution helper based on whether query returs result
	%--
	
	if ischar(sql) && sql(end) == ';'
		sql(end) = [];
	end
	
	% NOTE: no result queries include nearly everything but SELECT statements
	
	if ~query_returns_result(sql)
		[status, result] = jdbc_execute_update(connection, sql); return;
	end
	
	[status, result] = jdbc_execute_query(connection, sql);
	
catch

	e = lasterror; status = []; result = [];

	% NOTE: here the connection is stale, try to reconnect a few times
	
	if string_contains(e.message, 'CommunicationsException')

		logger('info', 'Connection is stale. Trying to reconnect ...');

		if opt.retry < 3
			opt.action = 'reset'; opt.retry  = opt.retry + 1; [status, result] = jdbc(config, sql, opt);
		else
			nice_catch; logger('error', 'Tried to reconnect too many times, JDBC connection problem.');
		end

	% NOTE: failed connection altogether, return
	
	elseif string_contains(e.message, 'java.net.ConnectException')
		
		nice_catch; 
		
		logger('error', ...
			sprintf('Connection failed: \n%s\nPerhaps the server is offline or you are not connected to the internet.', config.jdbc) ...
		)
		
		return;
	
	% NOTE: syntax errors should be thrown, there is something we need to fix
	
	elseif string_contains(e.message, 'MySQLSyntaxErrorException')
			
		% TODO: this does not belong here! this patches problems when we fail to create 'extension' tables
		
		if  string_contains(e.message, 'doesn''t exist')
			try %#ok<TRYNC>
				get_barn_tables(local_barn, 'extension', 1);
			end
		end
		
		rethrow(e);
		
	% NOTE: unexamined exception, everything else ... keep going until we fail

	else
		
		db_disp; sql_display(queries); nice_catch;
	
	end

end

%---------------------
% RECOVER FROM M2XML
%---------------------

if isempty(result)
	return;
end

if isstruct(result)
	result = recover_values(result);
end

%--
% return result as single output
%--

if nargout < 2
	status = result;
end

%---------------------
% LOGGING
%---------------------

function start = start_logging %#ok<DEFNU>

start = clock;


function stop_logging(sql, start) %#ok<DEFNU>

if iscell(sql)
	sql = sprintf('%s|', sql{:});
end

str = sprintf('Query complete.\n%s\nElapsed time is %f seconds.', sql, etime(clock, start));

logger('debug', str);
