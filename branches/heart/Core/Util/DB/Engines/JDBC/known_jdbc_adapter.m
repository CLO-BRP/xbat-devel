function adapter = known_jdbc_adapter(adapter)

% known_jdbc_adapter - list known adapters or test name against list
% ------------------------------------------------------------------
%
% adapter = known_jdbc_adapter(adapter)
%
% Input:
% ------
%  adapter - name
%
% Output:
% -------
%  adapter - list or normalized name
%
% See also: get_jdbc_driver_info

%--
% get adapter names from info of known drivers
%--

info = get_jdbc_driver_info; adapters = {info.name};

% NOTE: we may return all known names 

if ~nargin
	adapter = adapters; return;
end

%--
% match and normalize adapter name
%--

match = find(iterate(@strcmpi, adapters, adapter));

if isempty(match)
	adapter = '';
else
	adapter = adapters{match(1)};
end
