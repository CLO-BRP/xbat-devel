function result = install_sqlite3

% install_sqlite3 - install sqlite command-line application and analyzer
% ----------------------------------------------------------------------
%
% result = install_sqlite3
%
% Output:
% -------
%  result - install indicator

%--
% download and unpack command-line client, analyzer 
%--

% TODO: also download 'dll'

url = { ...
	'http://www.sqlite.org/sqlite-3_5_7.zip', ...
	'http://www.sqlite.org/sqlite3_analyzer-3_5_4.zip' ...
};

for k = 1:numel(url)
	result = install_tool('SQLite', url{k});
end

%--
% add to windows path
%--

% TODO