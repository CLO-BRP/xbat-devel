function name = sqlite_database_name(str)

% sqlite_database_name - create valid database name
% -------------------------------------------------
%
% name = sqlite_database_name(str)
%
% Input:
% ------
%  str - candidate name
%
% Output:
% -------
%  name - real name

str = lower(str);

keywords = {'main', 'temp', 'is'};

if string_is_member(str, keywords)
    str(end + 1) = '_';
end

name = strrep(str, ' ', '_');
