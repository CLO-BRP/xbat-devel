function value = is_sqlite_transaction(proposed)

% is_sqlite_transaction - test proposed value
% -------------------------------------------
%
%        value = is_sqlite_transaction(proposed)
%
% transactions = is_sqlite_transaction
%
% Input:
% ------
%  proposed - transaction name
%
% Output:
% -------
%  value - result of proposed test
%  transactions - types

% NOTE: the empty string and 'DEFERRED' are equivalent types

transactions = {'', 'DEFERRED', 'IMMEDIATE', 'EXCLUSIVE'};

if ~nargin
	value = transactions; return;
end

value = string_is_member(proposed, transactions);