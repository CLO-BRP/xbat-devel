function [status, result] = query(store, sql)

% query - database server or file-based database
% ----------------------------------------------
%
% [status, result] = query(store, sql)
%
% Input:
% ------
%  store - configuration or file-based database
%  sql - query
%
% Output:
% -------
%  status - of query
%  result - from query

if trivial(store)
	% NOTE: we hope for a cell array representation in 'sql', but this is not guaranteed

	status = sql_string(sql); result = sql;

	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear status;
	end
else
	% NOTE: quickly check if we are tracing and start clock
	
	[trace, state] = query_trace;

	% TODO: this is something that 'query_trace' could also do, for now we have it here and there are two functions involved
	
	trace = trace && trace_needed(state, sql);
	
	if trace
		disp(' '); sql_display(sql); disp(' ');
		
		if trace > 1
			stack_disp([], 3);
			disp(' ');
		end
		
		if trace > 2
			disp(store);
			disp(' ');
		end
		
		start = clock;
	end
	
	%--
	% perform query
	%--
	
	if isstruct(store)
		[status, result] = store.query(store, sql);
	else
		[status, result] = sqlite(store, sql);
	end

	%--
	% trace display
	%--
	
	if trace
		elapsed =  etime(clock, start); rate = round(1 / elapsed);
		
		% TODO: create 'query_trace_message' to display information about the higher level interaction being represented by the trace
		
		if isinf(rate)
			speed = 'Completed instantly';
		else
			speed = ['Completed in ', num2str(elapsed, 6), ' (', int2str(rate), ' reqs/sec)'];
		end
		
		if isfield(store, 'jdbc')
			server = store.jdbc;
		else
			server = [store.adapter, ': ', store.database];
		end
		
		disp(['   ', speed, ' @ ', server]);
		
		str_line(72, '_');
	end
end


%-----------------------------
% TRACE_NEEDED
%-----------------------------

function needed = trace_needed(state, sql)

% trace_needed - test given filter state and query
% ------------------------------------------------
%
% needed = trace_needed(state, sql)
%
% Input:
% ------
%  state - of trace filter
%  sql - query
%
% Output:
% -------
%  needed - indicator
%
% See also: query, query_trace

% TODO: consider 'from-function', 'not-from-function' to handle fine control that considers sub-functions contained in a file

needed = true;

if isfield(state, 'not_from')
	
	stack = dbstack(1, '-completenames'); 
	
	if numel(stack) < 2
		caller = 'command-line';
	else
		[ignore, caller] = fileparts(stack(2).file); %#ok<ASGLU>
	end

	% NOTE: this allows us to exclude a set of files (or functions eventually)
	
	if ischar(state.not_from)
		needed = ~strcmp(state.not_from, caller);
	else
		needed = ~any(iterate(@strcmp, state.not_from, caller));
	end
		
	if ~needed
		return;
	end
end

% NOTE: multiple conditions are combined with AND

% TODO: we can have both 'and' and 'or' here by having multiple conditions held in a cell of cells

% NOTE: we do 'or' over the set of conditions, and 'and' within each condition

if isfield(state, 'contains')
	sql = sql_string(sql);
	
	% NOTE: this allows us to include an additive set of queries
	
	if ischar(state.contains)
		needed = string_contains(sql, state.contains);
	else
		needed = any(iteraten(@string_contains, 2, sql, state.contains));
	end
end

if isfield(state, 'from')
	
	stack = dbstack(1, '-completenames');
	
	if numel(stack) < 2
		caller = 'command-line';
	else
		[ignore, caller] = fileparts(stack(2).file); %#ok<ASGLU>
	end
	
	% NOTE: this allows us to include a set of callers, excluding everything else
	
	if ischar(state.from)
		needed = strcmp(state.from, caller);
	else
		needed = any(iterate(@strcmp, state.from, caller));
	end
end

% TODO: implement conditions below!

% if isfield(state, 'does-not-contain')
% 	sql = sql_string(sql);
% end
% 
% if isfield(state, 'matches')
% 	sql = sql_string(sql);
% end




