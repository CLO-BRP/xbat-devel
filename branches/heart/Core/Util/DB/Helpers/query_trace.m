function [level, state] = query_trace(level, varargin)

% query_trace - state set and get
% -------------------------------
%
% [level, state] = query_trace(level, field, value, ... )
%
% Input:
% ------
%  level - to set, integer 0, 1, 2
%  field, value - filter state descriptions
%
%    - query 'contains' or 'does-not-contain' a string
%    - query comes 'from' or 'not-from' a caller
%    - query 'matches' regular expression
%
%    NOTE: we can clear the state description (and level) through 'query_trace clear'
% 
% Output:
% -------
%  level - current
%  filter - struct for trace
%
% See also: query

% TODO: allow tracing of a specific connection, log trace to file

% NOTE: half of the above task is implemented here, the other half in 'query'

%--
% setup and defaults
%--

persistent LEVEL STATE;

if isempty(LEVEL)
	LEVEL = 0;
end

if isempty(STATE)
	STATE = struct;
end

%--
% set trace variables
%--

if nargin 

	% NOTE: we may want to update the state without updating the level
	
	if ~isempty(level)
		
		if ischar(level)
			% NOTE: we can simply clear the query trace state with a command
			
			if strcmp(level, 'clear')
				clear query_trace; return;
			end
			
			level = str2int(level);
		end

		LEVEL = level;
	end
	
	if numel(varargin)
		% NOTE: we update a single state field at a time
		
		state = parse_state(STATE, varargin); STATE = state;
	else
		state = STATE;
	end
	
%--
% get trace variables
%--

else
	level = LEVEL;
	
	if nargout > 1
		state = STATE;
	end
end


%-----------------------------
% PARSE_STATE
%-----------------------------

function state = parse_state(state, args)

[field, value] = get_field_value(args, {'clear-state', 'from', 'not-from', 'contains', 'does-not-contain', 'matches'});

% NOTE: the associated value does not matter, typically we can just put a 'true'

if string_is_member('clear-state', field)
	state = struct;
end

for k = 1:numel(field)
	% NOTE: typically the 'genvarname' will not be required. the hyphen is more readable, however it creates a naming gap
	
	current = genvarname(strrep(field{k}, '-', '_'));
	
	if isempty(value{k}) && isfield(state, current)
		% NOTE: when we set a field to empty we clear it from the filter
		
		state = rmfield(state, current); continue;
	end
	
	if isfield(state, current)
		
		if ~iscell(state.(current))
			state.(current) = {state.(current)};
		end
		
		state.(current){end + 1} = value{k}; state.(current) = unique(state.(current));
	else
		% NOTE: we pack in a cell for safe-keeping, otherwise the adding could become confused
		
		state.(current) = {value{k}};
	end
end




