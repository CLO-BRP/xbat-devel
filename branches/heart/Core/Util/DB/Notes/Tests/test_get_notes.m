function test_get_notes

% This is really just a place-holder for a better test, sorry bout that

[store, object, note, user] = setup_test_notes;

[notes, notings] = get_notes(store, object);



% Make sure that the note is nontrivial and has something

assert(~trivial(notes) && numel(notes{1}) == 1);



% right now the notes do not return ids... should they?
% assert(note.id == note.id)