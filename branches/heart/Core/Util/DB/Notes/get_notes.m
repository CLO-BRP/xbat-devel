function [note, relation] = get_notes(store, obj, user, table)

% get_notes - get object notes from database
% ----------------------------------------------
%
% rating = get_notes(store, obj, user, table)
%
% Input:
% ------
%  store - database
%  obj - tagged
%  user - tagger
%  table - name
%
% Output:
% -------
%  rating - for objects

%-----------------------
% HANDLE INPUT
%-----------------------

%--
% return all notes
%--

% NOTE: this may be impractical, consider some refinement, notes require a 'find_notes'

if nargin < 2
    note = get_database_objects_by_column(store, 'note'); return;
end

%--
% typically reflect table from input
%--

if nargin < 4
	table = inputname(2);
end

if isempty(table)
	error('Unable to determine table name from input.');
end

%-----------------------
% GET ANNOTATION INFO
%-----------------------

%--
% get note and annotation information so that we can properly join
%--

sql = { ...
	'SELECT annotation.*, note.title, note.body, note.content_hash', ...
	' FROM annotation', ...
	' JOIN note ON annotation.note_id = note.id', ...
	[' WHERE annotation.annotated_type = ''', table, ''' AND'] ...
};

% NOTE: user input means we only retrieve notes created by that user

if nargin > 2 && ~trivial(user)
	sql{end + 1} = [' user_id = ', int2str(user.id), ' AND '];
end

if numel(obj) > 1
    sql{end + 1} = [' annotated_id IN (', str_implode([obj.id], ', ', @int2str), '); '];
else
    sql{end + 1} = [' annotated_id = ', int2str(obj.id) '; '];
end

% NOTE: return the query if we have no database to query

if trivial(store)
	
	note = sql;

	if ~nargout
		disp(' '); sql_display(sql); disp(' '); clear note;
	end
	
	return;
	
end

[status, result] = query(store, sql_string(sql));

%--
% extract notes from the query result
%--

note = cell(size(obj)); relation = cell(size(obj));

annotated_id = [result.annotated_id];

note_fields = {'title', 'body', 'content_hash'};

for k = 1:numel(obj)
  
	selected = result(annotated_id == obj(k).id);
	
	for j = 1:numel(selected)
		
        note{k}(j) = keep_fields(selected(j), note_fields);

		relation{k}(j) = rmfield(selected(j), note_fields);
		
	end
	
end

