function [status, result] = establish_notes(store)

% establish_notes - add tables for notes
% --------------------------------------
%
%     [sql, lines] = establish_notes
%
% [status, result] = establish_notes(store)
%
%                  = establish_notes(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  sql - string
%  lines - of query
%  status - of query request
%  result - of query

%--
% build query to create note and annotation tables
%--

note = note_prototype;

[str1, part1] = create_table(note);

noting = noting_prototype;

[str2, part2] = create_table(noting);

sql = {'BEGIN;', str1, str2, 'COMMIT;'};

%--
% output or execute query
%--

if ~nargin
	store = []; 
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql);
end
