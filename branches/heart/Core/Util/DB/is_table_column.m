function result = is_table_column(store, table, column)

% is_table_column - test
% ----------------------
%
% result = is_table_column(store, table, column)
%
% Input:
% ------
%  store - database
%  table - name
%  column - names
%
% Output:
% -------
%  result - of tests
%
% See also: get_table_columns

result = string_is_member(column, get_table_columns(store, table));