function [name, info] = get_database_indices(file)

% get_database_indices - get database indices and some information
% ----------------------------------------------------------------
%
% [name, info] = get_database_indices(file)
%
% Input:
% ------
%  file - database
%
% Output:
% -------
%  name - of indices
%  info - for indices

if nargout < 2
	name = get_database_elements(file, 'index'); 
else
	[name, info] = get_database_elements(file, 'index'); 
end