function update = rename_database_tags(file, current, desired)

% rename_database_tags - rename tags from database
% ------------------------------------------------
%
% update = rename_database_tags(file, current, desired)
%
% Input:
% ------
%  file - file
%  current - name
%  desired - name
%
% Output:
% -------
%  update - indicator
% 
% See also: get_database_tags, cleanup_database_tags

%--
% handle input
%--

if ischar(current)
	current = {current};
end

if ischar(desired)
	desired = {desired}; 
end

if numel(current) ~= numel(desired)
	error('Current and desired name arrays must have matching elements.');
end

%--
% get known tags and rename current tags
%--

% NOTE: this is not efficient, however currently it is concise and correct

[tags, id] = get_database_tags(file); update = zeros(size(current));

skip = false;

for k = 1:numel(current) 
	
	[update(k), ix] = string_is_member(current{k}, tags);
	
	if ~update(k)
		if ~skip, disp(' '); end
		
		disp(['Skipping ''', current{k}, ''' replacement, tag not found.']);
		
		skip = true; continue;
	end 
	
	if ~valid_tag(desired{k})
		
		if ~skip, disp(' '); end
		
		disp(['Skipping ''', current{k}, ''' rename, proposed replacement ''', desired{k}, ''' is not a valid tag.']);
	
		skip = true; continue;
	end
	
	% NOTE: above we obtained the identifier index, here we must dereference
		
	query(file, ['UPDATE tag SET name = ''', desired{k}, ''' WHERE id = ', int2str(id(ix))]);
end

if skip
	disp(' ');
end

if ~nargout
	clear update;
end
