function [tag, id, created] = get_tag_tags(file)

% get_tag_tags - get tags currently used for tags
% -----------------------------------------------
%
% [tag, id, created] = get_tag_tags(file)
%
% Input:
% ------
%  file - file
%
% Output:
% -------
%  tag, id, created - tag, identifier, and time stamp matching arrays

%--
% build and execute query
%--

% NOTE: this is a subset of the database tags

if nargout > 1
	sql = 'SELECT * FROM tag JOIN tag_tag ON tag.id = tag_tag.tag_id ORDER BY name;';
else
	sql = 'SELECT name FROM tag JOIN tag_tag ON tag.id = tag_tag.tag_id ORDER BY name;';
end

[status, result] = sqlite(file, sql);

%--
% produce output
%--

if trivial(result)
	tag = {}; id = []; created = {}; return;
end

% NOTE: this could be factored by making the various functions have variable output arguments

tag = {result.name};

if nargout > 1
	id = [result.id];
end

if nargout > 2
	created = {result.created_at};
end

