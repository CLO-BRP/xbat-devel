function test_load_database_config

% SETUP
%------------------

file = fullfile(fileparts(mfilename('fullpath')), 'test_database.yml');

% TEST
%------------------
% load_database_config should return a struct of structs if no environment
% is specified

[config, yaml] = load_database_config(file);

assert( isstruct(config.test) );
assert( isstruct(config.production) );
assert( isstruct(config.development) );
assert( isstruct(yaml) );

% TEST
%------------------

config = load_database_config(file, 'test');

assert( isfield(config, 'adapter') );
assert( isfield(config, 'root') );
assert( isfield(config, 'file') );
assert( isfield(config, 'query') );
assert_equal( class(config.query), 'function_handle' );

% TEST
%------------------

config = load_database_config(file, 'development');

assert( isfield(config, 'adapter') );
assert( ~isfield(config, 'root') );
assert( ~isfield(config, 'file') );
assert( isfield(config, 'query') );
assert( isfield(config, 'jdbc') );
assert_equal( class(config.query), 'function_handle' );

% loaded_fields  = fieldnames(config);
% empty_fields   = fieldnames(create_database_config);

% 
% assert_equal( numel(loaded_fields), numel(empty_fields) );

% TEST
%------------------
% The field names should also match and be in order

% for i=1:numel(empty_fields)
%    x = (empty_fields{i} == loaded_fields{i});
% end
% 
% assert_true(x);
