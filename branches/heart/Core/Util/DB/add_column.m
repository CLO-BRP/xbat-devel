function added = add_column(store, table, column, type, description)

% add_column - to database table
% ------------------------------
%
% added = add_column(store, table, column, type, description)
%
% Input:
% ------
%  store - database
%  table - name
%  column - name
%  type - 'hint' or 'sql'
%  description - of type
%
% Output:
% -------
%  added - column indicator
%
% See also: remove_column, is_table_column 

if is_table_column(store, table, column)
	
	added = false;
else
	switch type
		case 'hint'
			description = get_type_and_constraints(column, description);
		
			type = [description.type , ' ', description.constraints];
			
		case 'sql'
			type = description;
			
		otherwise 
			error(['Unrecognized columns description type ''', type, '''.']);
	end
	
	query(store, ['ALTER TABLE ', table, ' ADD COLUMN ', column, ' ', type]);
	
	added = true;
end