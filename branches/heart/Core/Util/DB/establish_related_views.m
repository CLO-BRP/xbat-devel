function view = establish_related_views(store, table)

% establish_related_views - create views for typical conventional joins 
% ---------------------------------------------------------------------
% 
% view = establish_related_views(store, table)
%
% Input:
% ------
%  store - database
%  table - focus
%
% Output:
% -------
%  view - struct containing queries

% NOTE: we get known related attribute select queries and turn them into views

%--
% compute view select queries
%--

if nargin < 2
	view = select_related(store); 
else
	view = select_related(store, table);
end

%--
% select prefix based on engine
%--

switch lower(store.adapter)
	case 'sqlite'
		prefix = 'CREATE VIEW IF NOT EXISTS ';
		
	otherwise
		prefix = 'CREATE OR REPLACE VIEW ';
end

%--
% slightly modify select query and execute to create views
%--

field = fieldnames(view);

for k = 1:numel(field)
	
	% NOTE: remove final semi-colon in select query
	
	view.(field{k}).sql{end}(end) = []; 
	
% 	view.(field{k}).sql = { ...
% 		[prefix, field{k}, ' AS '], ...
% 		view.(field{k}).sql{:}, ...
% 		[' ORDER BY ', view.(field{k}).focus, '.id;'] ...
% 	};
	
	view.(field{k}).sql = [ ...
		[prefix, field{k}, ' AS '], ...
		view.(field{k}).sql, ...
		[' ORDER BY ', view.(field{k}).focus, '.id;'] ...
	];
	
	try
		query(store, sql_string(view.(field{k}).sql));
	catch
		nice_catch(lasterror);
	end
end

%--
% display result during development
%--

if ~nargout
	
	field = fieldnames(view);

	disp(' ');

	for k = 1:numel(field)

		disp(field{k});
		disp(' ');
		sql_display(sql_string(view.(field{k}).sql));
		disp(' ');
		sql_display(view.(field{k}).sql);
		disp(' ');
	end

	clear view;
end

