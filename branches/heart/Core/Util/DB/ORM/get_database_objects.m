function obj = get_database_objects(store, table, id, opt)

% get_database_objects - through identifier
% -----------------------------------------
%
% obj = get_database_objects(store, table, id, opt)
%
% opt = get_database_objects
%
% Input:
% ------
%  store - for objects
%  table - name
%  id - identifier(s)
%  opt - options for query
%
% Output:
% -------
%  obj - objects
%  opt - default options
%
% See also: delete_database_objects

%--
% set and possibly return default options
%--

if nargin < 4
	opt = get_database_objects_by_column;

	opt.primary_key = 'id';
	
	opt.load_relations = false;
	
	% NOTE: this implies a contract on the part of the convention functions

	opt.foreign_key_convention = @rails_convention;
	
	if ~nargin
		obj = opt; return;
	end 
end 

%--
% get all by default
%--

if nargin < 3
	id = [];
end

%--
% get objects through identifier
%--

obj = get_database_objects_by_column(store, table, opt.primary_key, id, opt);

if opt.load_relations
	
	[ignore, relations] = opt.foreign_key_convention(fieldnames(obj));  %#ok<ASGLU>
	
	for relation = relations
		
		% NOTE: passing the options struct here means we recursively load relations
		
		child = get_database_objects(store, relation.table, unique([obj.(relation.foreign_key)]), opt);
		
		% NOTE: where we hang the child to the parent could be further conventional, here it is the table
		
		cid = [child.(relation.column)]; % NOTE: this is a minor optimization for the inner loop
		
		for j = 1:numel(obj)
			obj(j).(relation.table) = child(cid == obj(j).(relation.foreign_key));
		end
	end
	
end 

%--
% display if output not captured
%--

% NOTE: the called function 'get_database_objects_by_column' has a more elaborate version of this functionality

if ~nargout
	disp(obj); clear obj;
end

