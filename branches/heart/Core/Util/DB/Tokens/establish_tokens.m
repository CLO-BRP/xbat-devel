function [status, result] = establish_tokens(store)

% establish_tokens - table
% ------------------------
% 
% [status, result] = establish_tokens(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  status, result - of create query
%
% See also: get_user_tokens

%--
% create token prototype and from it create table query
%--

% NOTE: this is taken from the Redmine schema

hint = column_type_hints;

token.id = hint.integer;

token.user_id = hint.integer;

token.action = hint.string;

token.value = hint.string;

token.created_at = hint.timestamp;

sql = create_table(token);

%--
% output query
%--

if ~nargin
	store = []; 
end

if ~nargout
	query(store, sql);
else
	[status, result] = query(store, sql); 
end 