function initialize_tokens(store, actions)

% initialize_tokens - for all users and for each action
% -----------------------------------------------------
%
% initialize_tokens(store, actions)
%
% Input:
% ------
%  store - database
%  actions - list
%
% Output:
% -------
%  token - list created

%--
% handle input
%--

if ~has_table(store, 'token')
	error('Store does not have ''token'' table.');
end

if ~has_table(store, 'user')
	error('Store does not have ''user'' table.');
end

if ischar(actions)
	actions = str_split(actions, ',');
end

if ~iscellstr(actions)
	error('Actions must a comma-separated string or string cell array of actions.');
end

%--
% create token objects for users
%--

users = get_database_objects(store, 'user');

token = repmat(struct('user_id', [], 'action', [], 'value', []), numel(users) * numel(actions), 1); ix = 1;

for k = 1:numel(users)
	user = users(k);
	
	for j = 1:numel(actions)
		action = actions{j};

		token(ix) = struct('user_id', user.id, 'action', action, 'value', generate_uuid); ix = ix + 1;
	end
end

%--
% clear previous tokens and set new ones 
%--

disp(token)

query(store, 'DELETE FROM token');

set_database_objects(store, token, [], fieldnames(token));

