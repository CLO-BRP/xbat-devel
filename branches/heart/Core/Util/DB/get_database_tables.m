function [tables, info] = get_database_tables(store)

% get_database_tables - names and some information
% ------------------------------------------------
%
% [tables, info] = get_database_tables(store)
%
% Input:
% ------
%  store - database
%
% Output:
% -------
%  tables - names
%  info - for tables
%
% NOTE: table info is not yet implemented

if nargout < 2
	tables = get_database_elements(store, 'table'); 
else
	[tables, info] = get_database_elements(store, 'table'); 
end