function [status, config] = save_database_config(file, config, name)

% save_database_config - to YAML file
% -----------------------------------
%
% [status, config] = save_database_config(file, config, name)
%
% Input:
% ------
%  file - destination
%  config - to save
%  name - of config
%
% Output:
% -------
%  status - of write
%  config - saved

%--
% try to infer name for scalar config case
%--

if nargin < 3
	name = inputname(2);
end

if isempty(name)
	error('Unable to determine config name from input.');
end

%--
% for an existing file we update the named config, otherwise initialize empty document
%--

if exist(file, 'file')
    container = load_yaml(file);
else
	container = struct;
end 

%--
% normalize config and pack into document
%--

config = struct_update(create_database_config, config);

% NOTE: this is the field use in a 'rails' context

config.host = config.hostname;

container.(name) = config;

status = save_yaml(file, container);


