function shared = get_shared_objects(store, obj, type)

% get_shared_objects - get shared table entry for objects
% -------------------------------------------------------
%
% shared = get_shared_objects(store, obj, type)
%
% Input:
% ------
%  store - to consider
%  obj - to share
%  type - of object
%
% Output:
% -------
%  shared - entry for object

%--
% handle input
%--

if nargin < 3
	type = inputname(2);
end

if isempty(type)
	error('Unable to determine object type from input.');
end

if numel(obj) > 1
	shared = iteraten(mfilename, 2, store, obj, type); return;
end

%--
% check for shared entry, otherwise create one
%--

sql = ['SELECT * FROM shared WHERE shareable_type = ''', type, ''' AND shareable_id = ', int2str(obj.id), ';'];

[status, shared] = query(store, sql);

if ~isempty(shared)
	return;
end

shared = struct('shareable_type', type, 'shareable_id', obj.id, 'forwardable', 1);

shared = set_database_objects(store, shared);



