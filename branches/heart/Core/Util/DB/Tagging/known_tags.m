function tags = known_tags(store, like, alpha)

% known_tags - list, perhaps like a pattern
% -----------------------------------------
% 
% tags = known_tags(store, like, alpha)
%
% Input:
% ------
%  store - database
%  like - filter
%  alpha - order (def: true)
%
% Output:
% -------
%  tags - known
%
% NOTE: like uses the '%' wildcard, '%it' matches 'wit', '%it%' matches 'with'
%
% See also: tag_counts

%--
% handle input
%--

if nargin < 3
	alpha = true;
end

if nargin < 2
	like = []; 
end

%--
% get known tags, possibly order
%--

sql = 'SELECT id, name FROM tag';

if like
	% TODO: remove the '%' characters, these should come with the input
	
	sql = [sql, ' WHERE name LIKE ''', like, ''''];
end

[ignore, tags] = query(store, sql); %#ok<ASGLU>

if alpha
	% TODO: consider being case insensitive
	
	[ignore, ix] = sort({tags.name}); tags = tags(ix); %#ok<ASGLU>
end

%--
% display if output is not captured
%--

if ~nargout
	disp(' '); str_wrap(str_implode({tags.name}, ', ')); disp(' '); clear tags;
end