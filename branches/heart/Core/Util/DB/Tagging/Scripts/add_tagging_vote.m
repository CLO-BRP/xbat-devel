function [status, result] = add_tagging_vote(store)

[status, result] = query(store, 'ALTER TABLE tagging ADD COLUMN vote INTEGER DEFAULT 1');