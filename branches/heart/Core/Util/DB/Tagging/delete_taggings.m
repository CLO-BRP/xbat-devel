function tagging = delete_taggings(store, taggable, type)

% delete_taggings - for taggable objects
% --------------------------------------
%
% tagging = delete_taggings(store, taggable, type)
%
% Input:
% ------
%  store - database
%  taggable - objects or identifiers
%  type - of taggable objects
%
% Output:
% -------
%  tagging - objects deleted
%
% NOTE: the taggable type may be inferred from the identifier variable name
%
% See also: set_taggings, establish_tagging

% NOTE: there nothing to do in this case

if isempty(taggable)
	tagging = []; return;
end

%--
% handle input
%--

if nargin < 3
	type = inputname(2);
end

if isempty(type) && isstruct(taggable) && isfield(taggable, 'type')
	
	type = taggable(1).type;
end
	
if isempty(type)
	error('Unable to determine taggable type from input.'); 
end

% NOTE: this is the classname convention used in the polymorphic tagging schema

type = capitalize(type);

if isstruct(taggable)
	% TODO: if we choose to pass objects we should pass the 'primary_key' field name

	taggable = [taggable.id];
end

%--
% delete taggings
%--

suffix = ['FROM tagging WHERE taggable_type = ''', type, ''' AND taggable_id IN (', str_implode(taggable, ', ', @int2str), ')'];
	
if nargout	
	[status, tagging] = query(store, ['SELECT * ', suffix]); %#ok<ASGLU>
end

query(store, ['DELETE ', suffix]);
	

