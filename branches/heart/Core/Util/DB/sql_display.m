function sql_display(lines, prefix)

% sql_display - reasonable display of sql statement
% -------------------------------------------------
%
% sql_display(lines)
%
% Input:
% ------
%  lines - sql statement, possibly a cell array

if nargin < 2
	prefix = '   ';
end 

if ischar(lines)
	disp([prefix, lines]);
else
	for k = 1:length(lines)
		disp([prefix, lines{k}]);
	end
end

