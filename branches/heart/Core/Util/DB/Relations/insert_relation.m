function [status, result] = insert_relation(store, varargin)

% insert_relation - insert or update relations
% --------------------------------------------
%
% [count, sql] = insert_relation(store, table1, id1, table2, id2, opt)
%
% Input:
% ------
%  store - database
%  table1 - table
%  id1 - identifier
%  table2 - table
%  id2 - identifier
%  opt - options
%
% Output:
% -------
%  status, result - of query

% db_disp relation-insert; varargin

%---------------------
% HANDLE INPUT
%---------------------

%--
% get options from input or default, possibly output default options
%--

if is_odd(numel(varargin))
	opt = varargin{end}; varargin(end) = []; 
end

% NOTE: simple option merging works because fields are unique

if ~exist('opt', 'var')
	opt = struct_merge(establish_relation, sqlite_array_insert);
end 

if ~nargin
	count = opt; return; 
end

%---------------------
% SETUP
%---------------------

%--
% determine and check for relation using input
%--

% NOTE: the relation fields are all but the 'value' field

[field, value] = get_field_value(varargin);

related = setdiff(field, 'value');

% NOTE: the relation table name is determined by what we learn from the input as well as the options

relation = relation_table_name(related{:}, opt);

% NOTE: we can't update a relation that does not exist

if ischar(store) && ~isempty(store) && ~database_has_table(store, relation) 
	error(['Proposed relation to update ''', relation, ''' is not available.']);
end

%--
% pack input as prototype
%--

% NOTE: the field callback will append an '_id' to all non 'value' fields

callback.field = @append_id; data = pack_field_value(field, value, callback);

% NOTE: when we have been provided with a value structure, we merge it with the foreign key identifiers

% NOTE: the 'value' field itself will not be part of the table

if ~isfield(data, 'value') 
	field = fieldnames(data);
else
	data = struct_merge(data, data.value); field = setdiff(fieldnames(data), 'value');
end

% NOTE: properly matching data

for k = 1:numel(field)
	given(k) = numel(data.(field{k}));
end

if numel(unique(given)) > 2 
	error('Identifiers and values must match or at most one input can be plural.');
end

% NOTE: this is not the most economical solution

count = max(given);

if min(given) == 1
		
	for k = 1:numel(field)
		if given(k) == 1
			data.(field{k}) = repmat(data.(field{k}), count, 1);
		end
	end	
end

% TODO: make this into a function, subsume the leading code as well

for j = 1:numel(field) 
	field_is_cell(j) = iscell(data.(field{j}));
end

for k = 1:count

	for j = 1:numel(field)
		
		if field_is_cell(j)
			content(k).(field{j}) = data.(field{j}){k};
		else
			content(k).(field{j}) = data.(field{j})(k);
		end	
	end
end

%--
% build update query
%--

sql = sqlite_array_insert(relation, content, fieldnames(content), opt);		

% db_disp insert-relation-query; iterate(@disp, sql); disp(' '); disp(sql_string(sql))

%--
% output or possibly execute query
%--

% TODO: update help and other parts to reflect signature, count is not really count

[status, result] = query(store, sql);


%---------------------
% APPEND_ID
%---------------------

% NOTE: this is a little helper to turn table or object names into foreign key names

function in = append_id(in)

if strcmp(in, 'value')
	return;
end

in = [in, '_id'];

