function str = camelize(str, sep)

% camelize - string
% -----------------
%
% str = camelize(str, sep)
%
% Input:
% ------
%  str - to camelize
%  sep - word separator (def: '_' or ' ', depends on string inspection)
%
% Output:
% -------
%  str - camel string
%
% See also: str_uncamel, capitalize

if nargin < 2
	
	if any(str == '_')
		sep = '_';
	else
		sep = ' ';
	end
end

str = str_implode(str_split(str, sep), '', @capitalize);