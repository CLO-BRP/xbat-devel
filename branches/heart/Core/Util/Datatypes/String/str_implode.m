function str = str_implode(parts, sep, fun, trim)

% str_implode - implode cell array into separated string
% ------------------------------------------------------
%
% str = str_implode(parts, sep, fun, trim)
%
% Input:
% ------
%  parts - string parts cell array
%  sep - separator (def: space)
%  fun - to string helper or transformation (def: none)
%  trim - indicator (def: true)
%
% Output:
% -------
%  str - implosion result string

%-------------------
% HANDLE INPUT
%-------------------

%--
% cell string if needed
%--

if ischar(parts)
	parts = {parts};
end

%--
% set trim and no helper default
%--

if nargin < 4
	trim = true;
end

if nargin < 3
	fun = [];
end

%--
% set and check separator
%--

if nargin < 2
	sep = ' ';
end

if ~ischar(sep)
	error('Separator must be string.');
end

%--
% check and prepare cell array for implosion
%--

if isempty(parts)
	str = ''; return;
end

if ~isempty(fun)
	
	% TODO: consider exception handling for helper, and check output to be a cellstr
	
	if iscell(fun)
		parts = iterate(fun{1}, parts, fun{2:end});
	else
		parts = iterate(fun, parts);
	end
end

if ~iscellstr(parts)
	
	if isempty(fun)
		error('First input must be string cell array, unless helper is available.');
	else
		error('Input parts cell array is not a string cell array, helper may be the problem.');
	end	
end

if trim
	for k = 1:numel(parts)
		parts{k} = strtrim(parts{k});
	end
end

%--
% implode string cell array
%--

% NOTE: in the case of a single element there is no implosion

if numel(parts) == 1
	str = parts{1}; return;
end

parts = parts(:)'; parts(2, :) = {sep}; parts = parts(:)'; parts(end) = [];

str = [parts{:}]; 

% NOTE: the separator used to be wrapped in a further cell, this should not be needed

if iscell(str)
	str = char([str{:}]);
end

% NOTE: the above two statement line is much faster than 'strcat' applied to the cell array

% str = strcat(parts{:});
% 
% if iscell(str)
% 	str = str{1};
% end

