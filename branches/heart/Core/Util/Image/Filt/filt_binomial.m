function f = filt_binomial(m, n, d)

% filt_binomial - create binomial filters
% ---------------------------------------
%
% f = filt_binomial(m, n, d)
%
% Input:
% ------
%  m - row length of filter
%  n - column length of filter
%  d - derivative indicator
%
% Output:
% -------
%  f - binomial filter

%--
% handle input
%--

if nargin < 3
	d = [0, 0];
end

if nargin < 2
	n = 1;
end

if (m < 1) || (n < 1)
  error('Length of filter must be larger than or equal to 1');
end

%--
% create filter and normalize
%--

f = binomial(m, d(1)) * binomial(n, d(2))'; 

f = f ./ sum(abs(f(:)));


%------------------------------
% BINOMIAL
%------------------------------

function f = binomial(m, d)

%--
% create column filter
%--

f = 1;

for k = 1:(m - 1)
	f = conv([0.5 0.5]', f);
end
  
%--
% compute derivative if needed
%--

if d	
	ix = length(f) / 2;

	if mod(length(f), 2)
		ix = floor(ix); f = f(1:ix); f = [-f; 0; flipud(f)];
	else
		f = f(1:ix); f = [-f; flipud(f)];
	end
end
