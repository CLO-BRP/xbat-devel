function B = cuda_intper2(A, NR, NC, METHOD)

%  Fast 2-D interpolation
%   B = CUDAINTERP2(A, NR, NC) performs bilinear interpolation
%   on the matrix A so that the output B has NR rows and NC cols.
%   
%   Class support for input A: 
%      float (real): double
%
%   Class support for inputs NR, NC: 
%      real
%   
%   See also: TEST_CUDA_INTERP2, CONV2
    
%   By Alexander Huth, California Institute of Technology, 2008

if nargin < 4
    METHOD = 'linear';
end

if ~ismember(METHOD, {'nearest','linear','spline','bicubic','*nearest','*linear','*spline','*bicubic'})
    error(['Invalid interpolation type''' METHOD '''.']);
end

if strcmpi(METHOD, 'nearest') || strcmpi(METHOD, '*nearest')
    interp_mode = 0;
end
if strcmpi(METHOD, 'linear') || strcmpi(METHOD, '*linear')
    interp_mode = 1;
end
if strcmpi(METHOD, 'spline') || strcmpi(METHOD, '*spline')
    interp_mode = 2;
end
if strcmpi(METHOD, 'bicubic') || strcmpi(METHOD, '*bicubic')
    interp_mdoe = 3;
end

B = cuda_interp2_mex(A, NR, NC, interp_mode);
