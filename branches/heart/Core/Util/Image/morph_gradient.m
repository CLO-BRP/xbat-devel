function Y = morph_gradient(X,SE,t,Z)

% morph_gradient - morphological gradient
% ---------------------------------------
%
% Y = morph_gradient(X,SE,t,Z)
%
% Input:
% ------
%  X - input image or handle to parent figure
%  SE - structuring element
%  t - type of gradient
%
%   -1 - inner (dilation)
%    0 - symmetric (dilation - erosion) (default)
%    1 - outer (erosion)
%
%  Z - computation mask image (def: [])
%
% Output:
% -------
%  Y - gradient image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 473 $
% $Date: 2005-01-27 17:04:34 -0500 (Thu, 27 Jan 2005) $
%--------------------------------

% this function currently does not support image stacks

% given optimization of the erosion and dilation code consider using these

%--
% check for handle input
%--

[X, N, tag, flag, g, h] = handle_input(X,inputname(1));

%--
% set mask
%--

if (nargin < 4)
    Z = [];
end

%--
% set type of gradient
%--

if (nargin < 3)
    t = 0;
end

%--
% color image
%--

if (ndims(X) > 2)
    
    [rr,cc,ss] = size(X);
    
    for k = 1:ss
        Y(:,:,k) = morph_gradient(X(:,:,k),SE,t,Z);
    end
    
    %--
    % scalar image
    %--
    
else
    
    %--
    % structuring element
    %--
    
    B = se_mat(SE);
    pq = se_supp(SE);
    
    %--
    % full operator
    %--
    
    if (isempty(Z))
        
        % TODO: the mex version of this code needs debugging
        
        % 		%--
        % 		% compute gradient using mex
        % 		%--
        %
        % 		X = image_pad(X,pq,-1);
        %
        % 		Y = morph_gradient_(X,uint8(B),t);
        
        % invoke CUDA implementation if possible
        
        % NOTE: the code below will remember where CUDA morph gradient failed and not delegate
        
        B = se_mat(SE); state.data = {class(X), size(B)};
        
        B = uint8(B);
        
        if isempty(Z) && cuda_enabled && ~cuda_has_failed(mfilename, state)
            try
                switch t
                    case -1
                        Y = cuda_morph_filter('gradient-inner', X, B, b); return;
                    case 0
                        Y = cuda_morph_filter('gradient', X, B, b); return;
                    case 1
                        Y = cuda_morph_filter('gradient-outer', X, B, b); return;
                end
            catch
                cuda_has_failed(mfilename, state, lasterror);
                
                clear cuda_morph_filter_mex; % TODO: could this be integrated into 'cuda_has_failed'? probably not simple
            end
        end
        
        %--
        % compute gradient using erosion and dilation
        %--
        
        switch (t)
            
            %--
            % inner gradient
            %--
            
            case (-1)
                Y = morph_dilate(X,SE) - X;
                
            %--
            % symmetric gradient
            %--
                
            case (0)
                Y = morph_dilate(X,SE) - morph_erode(X,SE);
                
            %--
            % outer gradient
            %--
                
            case (1)
                Y = X - morph_erode(X,SE);
                
        end
        
        %--
        % mask operator
        %--
        
    else
        
        % TODO: there are problems with the mex, so this is probably wrong
        
        X = image_pad(X,pq,-1);
        Z = image_pad(Z,pq,0);
        Y = morph_gradient_(X,uint8(B),t,uint8(Z));
        
    end
    
end

%--
% display output
%--

if (flag & view_output)
    
    switch(view_output)
        
        case (1)
            
            figure(h);
            set(hi,'cdata',Y);
            set(gca,'clim',fast_min_max(Y));
            
        otherwise
            
            fig;
            image_view(Y);
            
    end
    
end