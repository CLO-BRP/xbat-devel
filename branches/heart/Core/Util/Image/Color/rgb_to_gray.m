function Y = rgb_to_gray(X)

% rgb_to_gray - rgb to grayscale image
% ------------------------------------
%
% Y = rgb_to_gray(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - grayscale image

%--
% check and get size of input
%--

[value, m, n] = is_rgb(X);

if ~value
	error('Input is not RGB image.');
end

%--
% make double image
%--

X = double(X);

%--
% define and apply linear transform
%--

% NOTE: this is what the MATLAB rgb2gray does

% 0.2989 * R + 0.5870 * G + 0.1140 * B 

V = [ ...
	0.393  0.365  0.192; ...
	0.212  0.701  0.087; ...
	0.019  0.112  0.958 ...
];

Y = reshape(rgb_vec(double(X)) * V(2, :)', m, n);
