function Y = xyz_to_rgb(X, A)

% xyz_to_rgb - xyz to rgb conversion
% ----------------------------------
%
% Y = xyz_to_rgb(X, A)
%
% Input:
% ------
%  X - rgb image
%  A - colorspace conversion matrix
%
% Output:
% -------
%  Y - xyz image
%
% See also: rgb_to_xyz

%--
% check size of input
%--

[value, m, n] = is_tricolor(X);

if ~value
	error('Input is not tri-color image.');
end

%--
% set and apply colorspace transform
%--

if nargin < 2
	A = [ ...
		0.393  0.365  0.192; ...
		0.212  0.701  0.087; ...
		0.019  0.112  0.958 ...
	];
end

% NOTE: we invert and scale colorspace transformation matrix

A = inv(A) .* 255;

Y = rgb_reshape(rgb_vec(double(X)) * A, m, n);

