function Y = eig_to_rgb(X, V, c) 

% eig_to_rgb - eigencolor to rgb conversion
% -----------------------------------------
%
%  Y = eig_to_rgb(X, V, c) 
%
% Input:
% ------
%  X - eigencolor image
%  V - eigencolor basis
%  c - mean color
%
% Output:
% ------
%  X - rgb image
%
% See also: rgb_to_eig

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:51-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% get size of input
%--

[value, m, n] = is_tricolor(X);

if ~value
	error('Input is not tri-color image.');
end

%--
% apply affine transform and enforce positivity
%--

% TODO: replace multiplication below with a 'repmat' and 'inv' with '\'

Y = rgb_reshape(rgb_vec(double(X)) * inv(V) + ones(m * n, 1) * c, m, n);

Y(Y < 0) = 0;

