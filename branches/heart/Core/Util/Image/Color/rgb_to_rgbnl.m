function Y = rgb_to_rgbnl(X)

% rgb_to_rgbnl - rgb to nonlinear rgb image
% -----------------------------------------
%
% Y = rgb_to_rgbnl(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - nonlinear rgb image

%--
% check input
%--

if ~is_rgb(X)
	error('Input is not RGB image.');
end

%--
% convert
%--

Y = double(X);

for k = 1:3
	Y(:, :, k) = (Y(:, :, k) * (1/255)).^0.4 * 255;
end