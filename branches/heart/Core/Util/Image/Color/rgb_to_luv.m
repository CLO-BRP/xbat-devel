function Y = rgb_to_luv(X)

% rgb_to_luv - rgb to luv conversion
% ----------------------------------
%
% Y = rgb_to_luv(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - lab image

%--
% check size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% compute conversion
%--

Y = xyz_to_luv(rgb_to_xyz(X));
