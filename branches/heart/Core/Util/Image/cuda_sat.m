function Y = cuda_sat(X)

% cuda_sat - summed area table
% -----------------------------------
% 
% Y = cuda_sat(X)
% 
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  Y - integral image

%--
% handle input
%--

%--
% process gray and color images
%--

switch ndims(X)

	case 2
		
		[Y, status] = cuda_sat_mex(X);
				
		handle_cuda_failure(status)
        
	case 3
		Y = X;
		
		for k = 1:3
			[Y(:, :, k), status] = cuda_sat(X(:, :, k));
			
			handle_cuda_failure(status)
		end
end
