function compare_gabor()

% Create a test image to obtain filter responses

X = zeros(21);
X(11, 11) = 1;

% Pre-allocate results arrays

Y1 = zeros(21, 21, 3, 3);
Y2 = zeros(21, 21, 3, 3);

% Loop over orientations and wavelengths

scale = 3;
omega = 0:30:150;
wavelength = [0.5, 1.0, 2.0];
melapsed = zeros(numel(omega), numel(wavelength));
celapsed = zeros(numel(omega), numel(wavelength));

for o = 1:numel(omega)
	
	for w = 1:numel(wavelength)

		% Explicitly create a Gabor filter and apply to test image
		
        mstart = tic;
		F = filt_gabor(scale, omega(o), wavelength(w));
		Y1(:,:,o,w) = linear_filter(X, F);
        melapsed(o,w) = toc(mstart);
		
		% Invoke iirgabor_mex on the same data with the same parameters
		
        cstart = tic;
		Y = fast_gabor(X, scale, omega(o), wavelength(w));
		Y2(:,:,o,w) = double(Y.gabor.data(:,:,1));
        celapsed(o,w) = toc(cstart);
    end
    
end

Y2 = real(Y2);

%--
% Visually compare results
%--

par = fig; layout = layout_create(6, 6);

harray(par, layout); handle = harray_select(par, 'level', 1);

handle = reshape(handle, 6, 6);

for o = 1:numel(omega)
	imagesc(Y1(:,:,o,1), 'parent', handle(1, o));
	imagesc(Y1(:,:,o,2), 'parent', handle(2, o));
	imagesc(Y1(:,:,o,3), 'parent', handle(3, o));
	imagesc(Y2(:,:,o,1), 'parent', handle(4, o));
	imagesc(Y2(:,:,o,2), 'parent', handle(5, o));
	imagesc(Y2(:,:,o,3), 'parent', handle(6, o));

	xlabel(handle(6, o), num2str(omega(o)));
end

for w = 1:numel(wavelength)
	ylabel(handle(w, 1), num2str(wavelength(w)));
	ylabel(handle(w+numel(wavelength), 1), num2str(wavelength(w)));
	
end

colormap(handle(1), gray);

ax = findobj(par, 'type', 'axes'); axis(ax, 'image');

%--
% Visually compare cross section of results
%--

G1 = Y1(11,:,1,3);
G2 = Y2(11,:,1,3);

fig; plot(G1); title('XBAT')

fig; plot(G2); title('IIRGABOR');

