#pragma once

#include <complex>
using namespace std;
//
//Values of the pole locations for the iir recursive gaussian derivative filters of scale s = 2.
//From "Recursive Gaussian Derivative Filters", L.van.Vliet,Ian.T.Young and Piet.W.Verbeek
//

extern const complex<double> d0_N3_L2[];
extern const complex<double> d0_N4_L2[];
extern const complex<double> d0_N5_L2[];
extern const complex<double> d0_N3_Linf[];
extern const complex<double> d0_N4_Linf[];
extern const complex<double> d0_N5_Linf[];
extern const complex<double> d1_N3_Linf[];
extern const complex<double> d1_N4_Linf[];
extern const complex<double> d1_N5_Linf[];
extern const complex<double> d2_N3_Linf[];
extern const complex<double> d2_N4_Linf[];
extern const complex<double> d2_N5_Linf[];

void calc_poles(int taps, const double scale, const complex<double> oldpoles[], complex<double> newpoles[] );
void calc_coeffs(int taps, const complex<double> poles[], const double s, float *coeffs);
void calc_coeffs(int taps, const complex<double> poles[], float *coeffs);
double calc_dcvalue_1D(float *coeffs, double w);
double calc_dcvalue(float *coeffs, double wx, double wy);
void calc_frequency_response(float *filter_coeffs, double freq, double *magnitude, double *phase);
void calc_real_horizontal_backward_initial_conditions(float *filter_coeffs, double *filter_poles, int bord_loc, float bord_val, double phase0, double freq, float *init_cond );
void calc_imag_horizontal_backward_initial_conditions(float *filter_coeffs, double *filter_poles, int bord_loc, float bord_val, double phase0, double freq, float *init_cond );
void calc_real_vertical_backward_initial_conditions(float *filter_coeffs,double *filter_poles,float real_bord,float imag_bord,double freq,float *init_cond );
void calc_imag_vertical_backward_initial_conditions(float *filter_coeffs,double *filter_poles,float real_bord,float imag_bord,double freq,float *init_cond );

void _iir_gaussfilt3_horz(float * in, float *tempbuf, float * out, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step);
void _iir_gaussfilt3_vert(float * inout, float *tempbuf, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step);
void _iir_gaussfilt3(float * in, float * out, float *tempbuf, int width, int height, int stridepix, float *coeffs, float *resid_ic, float *resid_step );
void _iir_gaborfilt3(float * in, float * even, float * odd, float * real, float * imag, float *tempbuf, 
		                int width, int height, int stridepix, float *coeffs, double wx, double wy, 
						double magx, double magy, double phasex, double phasey, 
		                float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx, float *resid_cosy, float *resid_siny,
		                float *up_bound, float *low_bound);

void _iir_gaborfilt3_real_horz(float * in, float * even, float *tempbuf, float * real, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx);
void _iir_gaborfilt3_imag_horz(float * in, float * odd, float *tempbuf, float * imag, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosx, float *resid_sinx);
void _iir_gaborfilt3_real_vert(float * real, float * imag, float *tempbuf, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosy, float *resid_siny, float *up_bound, float *low_bound);
void _iir_gaborfilt3_imag_vert(float * real, float * imag, float *tempbuf, int width, int height, int stridepix, float *coeffs, double wx, double wy, double mag, double phase, float *resid_ic, float *resid_step, float *resid_cosy, float *resid_siny, float *up_bound, float *low_bound);

void _compute_gauss3_resids( complex<double> poles[], float *coeffs, float *resid_ic, float *resid_step );
void _compute_gabor3_horz_resids(complex<double> poles[], float *coeffs, double f, float *resid_cosx, float *resid_sinx);
void _compute_gabor3_vert_resids(complex<double> poles[], float *coeffs, double f, float *resid_cosy, float *resid_siny);

void compute_step_forward_ic(float bord_val, float *coeffs, float *i0);
void compute_real_horz_forward_ic(double filtermag, double filterphase, double carrierfreq, double carrierphase, float bord_val, float *i0) ;
void compute_imag_horz_forward_ic(double filtermag, double filterphase, double carrierfreq, double carrierphase, float bord_val, float *i0);
void compute_real_vert_forward_ic(double filtermag, double filterphase, double carrierfreq, float bord_real_val, float bord_imag_val, float *i0); 
void compute_real_vert_forward_ic(double filtermag, double filterphase, double carrierfreq, float bord_real_val, float bord_imag_val, float *i0);

void compute_natural_backward_ic( float *resid_ic, float *i0 );
void add_step_backward_ic( float *resid_step, float val, float *i0 );
void add_horz_real_backward_ic(float *resid_cosx, float *resid_sinx, int bord_loc, float bord_val, double phase0, double freq, float *init_cond );
void add_horz_imag_backward_ic(float *resid_cosx, float *resid_sinx, int bord_loc, float bord_val, double phase0, double freq, float *init_cond );
void add_vert_real_backward_ic(float *resid_cosy, float *resid_siny, float real_bord, float imag_bord, double freq, float *init_cond );
void add_vert_imag_backward_ic(float *resid_cosy, float *resid_siny, float real_bord, float imag_bord, double freq, float *init_cond );




