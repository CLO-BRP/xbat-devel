function [files, destination] = build

clear iirgabor_mex;

% What type of machine are we on?

if ispc
    objext = '.obj';
else
    objext = '.o';
end

% add -largeArrayDims on 64-bit machines

mex -largeArrayDims -O -c ../iirfilt.cpp
mex -largeArrayDims -O -c ../iirgausderiv.cpp
mex -largeArrayDims -O -c ../IIRGabor.cpp
mex('-largeArrayDims', '-O', '../iirgabor_mex.cpp', ['iirfilt' objext], ['iirgausderiv' objext], ['IIRGabor' objext])

movefile(['iirgabor_mex.', mexext], ['../../private/iirgabor_mex.', mexext]);

delete(['iirfilt' objext]);
delete(['iirgausderiv' objext]);
delete(['IIRGabor' objext]);

%--
% report created files and destination
%--

% TODO: the destination is redundant, it should not be needed as output

par = fileparts(fileparts(pwd));

destination = [par, filesep, 'private'];

% NOTE: the files to consider are files after they've been installed

files = { ...
	[destination, filesep, 'iirgabor_mex.', mexext], ...
};
