// 
// MEX wrapper for IIRGABOR Gabor filtering code
//

#include "mex.h"

#include "IIRGabor.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// Input image
	float   *img;
	int     lines;
	int     cols;
	mwSize  dims[3];
	
    // Filter parameters
	int     levels;
	double  *scales;
	int     norients;
	double  *orients;    // In degrees
	int     nwavs;
	double  *wavs;
    mxLogical   *mat_sw_tab;
    bool        *sw_tab;
    
    // Check for input parameters
    if (nrhs < 5)
    {
        return;
    }
    
	// Get image and dimensions
	img = (float *) mxGetPr(prhs[0]);
	cols = mxGetM(prhs[0]); lines = mxGetN(prhs[0]);

    // Get scales
    scales = (double *) mxGetPr(prhs[1]);
    levels = mxGetM(prhs[1]) * mxGetN(prhs[1]);
    
    // Get orientations
    orients = (double *) mxGetPr(prhs[2]);
    norients = mxGetM(prhs[2]) * mxGetN(prhs[2]);
    
    // Get wavelengths
    wavs = (double *) mxGetPr(prhs[3]);
    nwavs = mxGetM(prhs[3]) * mxGetN(prhs[3]);
    
    // Table of scale/wavelength combinations to actually compute
    if (mxGetM(prhs[4]) * mxGetN(prhs[4]) != nwavs * levels)
    {
        return;
    }
    mat_sw_tab = (mxLogical *) mxGetPr(prhs[4]);
    
    sw_tab = (bool *) malloc(nwavs * levels * sizeof(bool));
    int sw_count = 0;
    for (int i = 0; i < nwavs; i++)
    {
        for (int j = 0; j < levels; j++)
        {
            sw_tab[i * levels + j] = mat_sw_tab[i * levels + j];
            if (sw_tab[i * levels + j]) { sw_count++; }
        }
    }

    int nfilters = sw_count*norients;
    
	// Create and configure CIIRGabor object
	CIIRGabor *filter = new CIIRGabor();
	filter->AllocateResources(lines, cols, levels, scales, norients, orients, 
                                     nwavs, wavs, sw_tab);
	// Process the image
	filter->ProcessImage(img);

	// Return results
	int stride = filter->GetStridePix();
	float *iptr, *optr;
    float *iiptr, *oiptr;
    double *params;

    // Gabor filter values
	if (nlhs > 0)
	{
        // Filter outputs
		dims[0] = cols;
		dims[1] = lines;
		dims[2] = nfilters;
		optr = (float *) mxGetPr(plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxCOMPLEX));
        oiptr = (float *) mxGetPi(plhs[0]);
        
        // Filter parameters
        dims[0] = nfilters;
        dims[1] = 3;
        params = (double *) mxGetPr(plhs[1] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL));
        
        int filt_number = 0;
        for (int o = 0; o < norients; o++)
        {
            for (int w = 0; w < nwavs; w++)
            {	
                for (int s = 0; s < levels; s++ )
                {
                    if (sw_tab[w * levels + s])
                    {
                        iptr = filter->AccessGaborRealLevel(filt_number);
                        iiptr = filter->AccessGaborImagLevel(filt_number);
                        for (int j = 0; j < lines; j++)
                        {
                            for (int k = 0; k < cols; k++)
                            {
                                *optr++ = *iptr++;
                                *oiptr++ = *iiptr++;
                            }
                            iptr += stride - cols;
                            iiptr += stride - cols;
                        }
                        
                        params[0 * nfilters + filt_number] = orients[o];
                        params[1 * nfilters + filt_number] = wavs[w];
                        params[2 * nfilters + filt_number] = scales[s];
                        
                        filt_number++;
                    }
                }
            }
        }
        
//         for(int i = 0; i < nfilters; i++)
//         {
// 			iptr = filter->AccessGaborRealLevel(i);
//             iiptr = filter->AccessGaborImagLevel(i);
// 			for (int j = 0; j < lines; j++)
// 			{
// 				for (int k = 0; k < cols; k++)
// 				{
// 					*optr++ = *iptr++;
//                     *oiptr++ = *iiptr++;
// 				}
// 				iptr += stride - cols;
//                 iiptr += stride - cols;
// 			}
// 		}
	}

    // Gaussian at each scale level
	if (nlhs > 1)
	{
		dims[0] = cols;
		dims[1] = lines;
		dims[2] = levels;
		optr = (float *) mxGetPr(plhs[2] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL));
        
        dims[0] = levels;
        params = (double *) mxGetPr(plhs[3] = mxCreateNumericArray(1, dims, mxDOUBLE_CLASS, mxREAL));
        
		for(int i = 0; i < levels; i++)
		{
            params[i] = scales[i];
			iptr = filter->AccessGaussianLevel(i);
			for (int j = 0; j < lines; j++)
			{
				for (int k = 0; k < cols; k++)
				{
					*optr++ = *iptr++;
				}
				iptr += stride - cols;
			}
		}
	}
	
    // Laplacian at each scale level
	if (nlhs > 2)
	{
		dims[0] = cols;
		dims[1] = lines;
		dims[2] = levels;
		optr = (float *) mxGetPr(plhs[4] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL));
        
        dims[0] = levels;
        params = (double *) mxGetPr(plhs[5] = mxCreateNumericArray(1, dims, mxDOUBLE_CLASS, mxREAL));
        
		for(int i = 0; i < levels; i++)
		{
            params[i] = scales[i]; 
			iptr = filter->AccessLaplacianLevel(i);
			for (int j = 0; j < lines; j++)
			{
				for (int k = 0; k < cols; k++)
				{
					*optr++ = *iptr++;
				}
				iptr += stride - cols;
			}
		}
	}
	
	// Clean up and exit
    
    free(sw_tab);
    
	delete filter;
}

