// test_atrousgabor.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <conio.h>

#include "IIRGabor.h"

//helper function
//rescales image values for the whole range 0-255 and convert to 8-bit

void convert_for_display(float *ptr, unsigned char *buffer, int lines, int cols, int stride)
{
	//compute global maxima
	int i,j;
	float max = ptr[0];
	float min = ptr[0];
	for(i=0; i < lines; i++)
	{
		for(j=0; j < lines; j++)
		{
			if(ptr[i*stride+j]>max)
				max = ptr[i*stride+j];
			if(ptr[i*stride+j]<min)
				min = ptr[i*stride+j];
		}
	}
	float slope, intercept;
	
	if(max>min)
		slope= 255/(max-min);
	else
		slope = 0;

	intercept = -min*slope;


	for(i=0; i < lines; i++)
		for(j=0; j<cols; j++)
			buffer[i*cols+j] = (unsigned char)(ptr[i*stride+j]*slope+intercept);

}


int main(int argc, char* argv[])
{
	int i;
	char filename[40];
	int lines = 128;
	int cols = 128;
	int levels = 4;
	int norients = 12;
	double orients[12] = {0,15,30,45,60,75,90,105,120,135,150,165};
	int nwavs = 4;
	double scales[4] = {1, 2, 4, 8};
	double wavs[4]={3.7, 7.4, 14.8, 29.6};
	bool sw_tab[]	 = {	1,0,0,0,
							1,1,0,0,
							1,1,1,0,
							0,1,1,1 };

	//total number of filters = number of ones in sw_tab times the number of orientations
	int nfilters = 9*norients; 

	//opening cartesian image file
	FILE *fp;
	fp = fopen("lena8bit128x128.raw","rb");
	if(fp == NULL)
	{
		printf("image file not found ...");
		return -1;
	}
	//read image bytes to buffer
	unsigned char *buffer = (unsigned char*)calloc(lines*cols,sizeof(unsigned char));
	int bytes_read = fread(buffer, sizeof(unsigned char), lines*cols, fp); 
	printf("Read %d bytes.\n", bytes_read);
	if( bytes_read != lines*cols)
	{
		printf("invalid image file ...");
		fclose(fp);
		return -1;
	};
	fclose(fp);

	//convert to floating point
	float *img = (float*)calloc(lines*cols,sizeof(float));
	for( i = 0; i < lines*cols; i++)
		img[i] = (float)buffer[i];


	//create and configure CIIRGabor object
	CIIRGabor *filter = new CIIRGabor();
	filter->AllocateResources(lines, cols, levels, scales, norients, orients, 
                                     nwavs, wavs, sw_tab);
	//Process the image
	clock_t start, finish;
    double  duration;
	start = clock();
	filter->ProcessImage(img);
	finish = clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    printf( "Computed %d filters on a %dX%d image in %2.1f seconds\n", nfilters, lines, cols, duration );

	//writing to file
	int stride = filter->GetStridePix();
	float *ptr;
	printf("Writing output to files...\n");
	for(i = 0; i < nfilters; i++)
	{
		ptr = filter->AccessGaborEnergyLevel(i);
		//rescale image values for the whole range 0-255 and convert to 8-bit.
		convert_for_display(ptr, buffer, lines, cols, stride);
		//send to file
		sprintf(filename, "lena8bit128x128_energylevel_%03d.raw", i);
		fp = fopen(filename,"wb");
		fwrite(buffer,sizeof(unsigned char),lines*cols,fp);
		fclose(fp);
	}
	for(i = 0; i < levels; i++)
	{
		ptr = filter->AccessGaussianLevel(i);
		//rescale image values for the whole range 0-255 and convert to 8-bit.
		convert_for_display(ptr, buffer, lines, cols, stride);
		//send to file
		sprintf(filename, "lena8bit128x128_gausslevel_%03d.raw", i);
		fp = fopen(filename,"wb");
		fwrite(buffer,sizeof(unsigned char),lines*cols,fp);
		fclose(fp);
	}
	for(i = 0; i < levels; i++)
	{
		ptr = filter->AccessLaplacianLevel(i);
		//rescale image values for the whole range 0-255 and convert to 8-bit.
		convert_for_display(ptr, buffer, lines, cols, stride);
		//send to file
		sprintf(filename, "lena8bit128x128_laplacianlevel_%03d.raw", i);
		fp = fopen(filename,"wb");
		fwrite(buffer,sizeof(unsigned char),lines*cols,fp);
		fclose(fp);
	}
	printf("Done.\n");
	free(buffer);
	free(img);
	delete filter;
	printf("Press any key to exit ...\n");
	getch();
	return 0;
}

