// AUTHOR:  Alexandre Bernardino - ISR/IST
// FILE:    IIRGabor.cpp
// VERSION: 1.0
// DATE:    30/06/05
// CONTACT: alex@isr.ist.utl.pt

//This is windows stuff
//#include "stdafx.h"

#include <stdlib.h>
#include <string.h>

#include "IIRGabor.h"
#include "iirgausderiv.h"

/*********************************************************************************
Constructor():
	Sets all variables to NULL		
**********************************************************************************/
CIIRGabor::CIIRGabor(void)
{
	//Initialization of constants and variables
	pi = 3.14159265358979323846;
	m_i_scales = 0;
	m_i_wavelengths = 0;
	m_i_orientations = 0;
	m_i_kernels = 0;
	m_i_lines = 0;
	m_i_cols = 0;
	stride = 0;
	m_i_stridepix = 0;
	sine = NULL;
	cosine = NULL;
	gausslevel = NULL;
	laplevel = NULL;
	reallevel = NULL;
	imaglevel = NULL;
	gaborlevel = NULL;
	even = NULL;
	odd = NULL;
	temp = NULL;
	input = NULL;
	m_scales = NULL;
	m_wavelengths = NULL;
	m_orientations = NULL;
	m_dc_gain = NULL;
	m_magnitudex_kernel = NULL;
	m_phasex_kernel = NULL;
	m_magnitudey_kernel = NULL;
	m_phasey_kernel = NULL;
	m_scale_wav_table = NULL;
	m_wx = NULL;
	m_wy = NULL;
	filt_poles = NULL;
	filt_coeffs = NULL;
	m_bAllocated = false;
	//m_bFastDyadic = FALSE;
	upper_boundary = NULL;
	lower_boundary = NULL;
	m_resid_step_level = NULL;
	m_resid_ic_level = NULL;
	m_resid_cosx_kernel = NULL;
	m_resid_sinx_kernel = NULL;
	m_resid_cosy_kernel = NULL;
	m_resid_siny_kernel = NULL;
#ifdef USE_IPP
//	InitStatic(); //initialize Intel Performance Primitives Library
#endif
}

/*********************************************************************************
Destructor():
	Releases all allocated resources and exits
**********************************************************************************/
CIIRGabor::~CIIRGabor(void)
{
	FreeResources();
}

/*********************************************************************************
IsAllocated():
	Check if memory resources have already been allocated		
**********************************************************************************/
bool CIIRGabor::IsAllocated()
{
	return m_bAllocated;
}


/*********************************************************************************
GetLines():
	returns the number of lines for which the object was allocated		
**********************************************************************************/
long CIIRGabor::GetLines()
{
	return m_i_lines;
}

/*********************************************************************************
GetCols():
	returns the number of columns for which the object was allocated		
**********************************************************************************/
long CIIRGabor::GetCols()
{
	return m_i_cols;
}

/*********************************************************************************
GetStridePix():
	returns the number of pixels of one line of the internally allocated buffers
**********************************************************************************/
long CIIRGabor::GetStridePix()
{
	return m_i_stridepix;
}
/*********************************************************************************
AllocateResources():
	Creates internal structures and memory space for the required computations
	lines : number of lines of the image to process
	cols : number of columns of the image to process
	nlevels : number of scales in the gabor filter bank
	scales : vector containing the scale values
	norients : number of orientations in the gabor filter bank
	orients : vector containing the orientation values (in degrees)
	nwav : number of wavelenghths in the gabor filter bank 
	wavs : vector containing the wavelenght values (in pixels)
	sw_table : boolean matrix of "nlevels" lines and "nwavs" columns indicating 
	           which combinations scale/wavelength are to process
**********************************************************************************/
bool CIIRGabor::AllocateResources(long lines, long cols, 
     long nlevels, double *scales, long norients, double *orients, 
     long nwavs, double *wavs, bool *sw_table )
{
	int i,j,m,n;
	double angle;

	if( norients < 0 )
		throw "Invalid number of orientations";
	if( nwavs < 0 )
		throw "Invalid number of wavelengths";
	if( nlevels < 1 )
		throw "Invalid number of levels";

	if(IsAllocated())
		FreeResources();

	m_i_lines = lines;
	m_i_cols = cols;
	m_i_orientations = norients; 
	m_i_scales = nlevels;
	m_i_wavelengths = nwavs;


	//initialization of wavelength orientation and scale values
	m_scales = (double*)malloc(m_i_scales*sizeof(double));
	for( i = 0; i < m_i_scales; i++ )
	{
		if(scales[i] < 0.5)
		{
			free(m_scales);
			throw "Scale values lower than 0.5 are not alowed";
		}
		if(scales[i] > 100)
		{
			free(m_scales);
			throw "Danger: Erroneous results for scale values higher than 100";
		}
		m_scales[i] = scales[i];
	}

	if(m_i_orientations != 0)
	{
		m_orientations = (double*)malloc(m_i_orientations*sizeof(double));
		for( i = 0; i < m_i_orientations; i++ )
			m_orientations[i] = orients[i]*pi/180.0f;
	}

	if(m_i_wavelengths != 0)
	{
		m_wavelengths = (double*)malloc(m_i_wavelengths*sizeof(double));
		for( i = 0; i < m_i_wavelengths; i++ )
			m_wavelengths[i] = wavs[i];
	}

	m_i_kernels = 0;
	if((m_i_wavelengths != 0) && (m_i_orientations != 0))
	{
		m_scale_wav_table = (bool**)malloc(m_i_scales*sizeof(bool*));
		for( i = 0; i < m_i_scales; i++ )
			m_scale_wav_table[i] = (bool*)calloc(m_i_wavelengths,sizeof(bool));
		for(i=0;i<m_i_wavelengths;i++)
			for(j=0;j<m_i_scales;j++) 
				m_scale_wav_table[j][i] = (bool)sw_table[i*m_i_scales+j];
		
		for(i=0;i<m_i_wavelengths;i++)
			for(j=0;j<m_i_scales;j++)
				if(m_scale_wav_table[j][i])
					m_i_kernels++;
	}


	//allocation of level structures -  filter poles, coefficients, residues
	filt_poles = new complex<double>*[m_i_scales];
	filt_coeffs = (float**)malloc(m_i_scales*sizeof(float*));
	m_resid_step_level = (float**)malloc(m_i_scales*sizeof(float*));
	m_resid_ic_level = (float**)malloc(m_i_scales*sizeof(float*));
	for(i = 0; i < m_i_scales; i++)
	{
		filt_poles[i] = new complex<double>[5]; //max 5 complex poles 
		filt_coeffs[i] = (float*)malloc(6*sizeof(float)); //max 6 coeffs : 1 gain + 5 autoregressive terms
		m_resid_step_level[i] = (float*)malloc(3*sizeof(float));
		m_resid_ic_level[i] = (float*)malloc(9*sizeof(float));
	}

	//allocation of kernel structures - dc gain, frequencies, residues, magnitude and phase, 
	if((m_i_wavelengths != 0) && (m_i_orientations != 0))
	{
		m_wx = (double**)malloc(m_i_orientations*sizeof(double*));
		m_wy = (double**)malloc(m_i_orientations*sizeof(double*));
		for( i = 0; i < m_i_orientations; i++ )
		{
			m_wx[i] = (double*)malloc(m_i_wavelengths*sizeof(double));
			m_wy[i] = (double*)malloc(m_i_wavelengths*sizeof(double));
		}
		
		m_resid_cosx_kernel = (float**)malloc(m_i_orientations*m_i_kernels*sizeof(float*));
		m_resid_sinx_kernel = (float**)malloc(m_i_orientations*m_i_kernels*sizeof(float*));
		m_resid_cosy_kernel = (float**)malloc(m_i_orientations*m_i_kernels*sizeof(float*));
		m_resid_siny_kernel = (float**)malloc(m_i_orientations*m_i_kernels*sizeof(float*));
		m_magnitudex_kernel = (double*)malloc(m_i_orientations*m_i_kernels*sizeof(double));
		m_phasex_kernel = (double*)malloc(m_i_orientations*m_i_kernels*sizeof(double));
		m_magnitudey_kernel = (double*)malloc(m_i_orientations*m_i_kernels*sizeof(double));
		m_phasey_kernel = (double*)malloc(m_i_orientations*m_i_kernels*sizeof(double));
		m_dc_gain = (double*)malloc(m_i_orientations*m_i_kernels*sizeof(double));
		for( i = 0; i < m_i_orientations*m_i_kernels; i++)
		{
			m_resid_cosx_kernel[i] = (float*)malloc(3*sizeof(float));
			m_resid_sinx_kernel[i] = (float*)malloc(3*sizeof(float));
			m_resid_cosy_kernel[i] = (float*)malloc(3*sizeof(float));
			m_resid_siny_kernel[i] = (float*)malloc(3*sizeof(float));
		}		
	}

	//allocation of image buffers for computations
	gausslevel = (IMAGE_PTR_VEC)malloc(m_i_scales*sizeof(IMAGE_PTR));
	if(m_i_orientations != 0)
		laplevel = (IMAGE_PTR_VEC)malloc(m_i_scales*sizeof(IMAGE_PTR));
	if((m_i_orientations != 0 )&&(m_i_wavelengths != 0))
	{
		sine = (IMAGE_PTR_VEC)malloc(m_i_orientations*m_i_wavelengths*sizeof(IMAGE_PTR));
		cosine = (IMAGE_PTR_VEC)malloc(m_i_orientations*m_i_wavelengths*sizeof(IMAGE_PTR));
		reallevel = (IMAGE_PTR_VEC)malloc(m_i_orientations*m_i_kernels*sizeof(IMAGE_PTR));
		imaglevel = (IMAGE_PTR_VEC)malloc(m_i_orientations*m_i_kernels*sizeof(IMAGE_PTR));
		gaborlevel = (IMAGE_PTR_VEC)malloc(m_i_orientations*m_i_kernels*sizeof(IMAGE_PTR));
	}
#ifdef USE_IPP
	input = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride);
	m_i_stridepix = stride/4;

	temp = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
	for( i = 0; i < m_i_scales; i++ )
		gausslevel[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
	if(m_i_orientations != 0)
		for( i = 0; i < m_i_scales; i++ )
			laplevel[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
	
	if((m_i_orientations != 0)&&(m_i_wavelengths != 0))
	{
		even = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
		odd = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
	
		
		for( i = 0; i < m_i_orientations*m_i_wavelengths; i++ )
		{
			sine[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
			cosine[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
		}

		for( i = 0; i < m_i_orientations*m_i_kernels; i++ )
		{
			reallevel[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
			imaglevel[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
			gaborlevel[i] = ippiMalloc_32f_C1(m_i_cols, m_i_lines, &stride); 
		}
	}
#else

	m_i_stridepix = m_i_cols;
	stride = m_i_cols*4;

	input = (float*)malloc(m_i_lines*m_i_cols*sizeof(float)); 
	temp = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
	for( i = 0; i < m_i_scales; i++ )
		gausslevel[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));		

	if(m_i_orientations != 0)
		for( i = 0; i < m_i_scales; i++ )
			laplevel[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));		

	if((m_i_orientations != 0)&&(m_i_wavelengths != 0))
	{
		even = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
		odd = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));

		for( i = 0; i < m_i_orientations*m_i_kernels; i++ )
		{
			reallevel[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
			imaglevel[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
			gaborlevel[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
		}
		for( i = 0; i < m_i_orientations*m_i_wavelengths; i++ )
		{
			sine[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
			cosine[i] = (float*)malloc(m_i_lines*m_i_cols*sizeof(float));
		}
	}
#endif
	upper_boundary = (float*)malloc(m_i_cols*sizeof(float));
	lower_boundary = (float*)malloc(m_i_cols*sizeof(float));
    //compute filter poles, coefficients and boundary gaussian residues for each scale
	for(i = 0; i < m_i_scales; i++)
	{
		calc_poles(3,m_scales[i],d0_N3_Linf, filt_poles[i]);
		calc_coeffs(3, filt_poles[i], filt_coeffs[i]);
		_compute_gauss3_resids(filt_poles[i], filt_coeffs[i], m_resid_ic_level[i], m_resid_step_level[i]);
	}

	//computation of kernel horizontal, vertical frequencies and dc gain.
	if((m_i_orientations != 0 )&&(m_i_wavelengths != 0))
	{
		for( j = 0; j < m_i_orientations; j++ )
		{					
			for( i = 0; i < m_i_wavelengths; i++ )
			{
				m_wx[j][i] = 2*pi/m_wavelengths[i]*cos(m_orientations[j]);
				m_wy[j][i] = 2*pi/m_wavelengths[i]*sin(m_orientations[j]);
			}
		}
	}

	//initialization of carrier images
	if((m_i_orientations != 0)&&(m_i_wavelengths != 0))
	{
		for( j = 0; j < m_i_orientations; j++ )
		{
			for( i = 0; i < m_i_wavelengths; i++ )
			{
				for( m = 0; m < m_i_lines; m++)
				{
					for( n = 0; n < m_i_cols; n++ )
					{
						angle = n*m_wx[j][i] - m*m_wy[j][i];
						cosine[j*m_i_wavelengths+i][m*m_i_stridepix+n] = (float)cos(angle);
						sine[j*m_i_wavelengths+i][m*m_i_stridepix+n] = (float)sin(angle);
					}
				}
			}
		}
	}

	m = 0;
	for(int j = 0; j < m_i_orientations; j++)
	{
		for(int n = 0; n < m_i_wavelengths;  n++ )
		{
			for(int i = 0; i < m_i_scales; i++)
			{
				if(m_scale_wav_table[i][n])
				{
					m_dc_gain[m] = calc_dcvalue(filt_coeffs[i], m_wx[j][n], m_wy[j][n]);
					_compute_gabor3_horz_resids(filt_poles[i], filt_coeffs[i], m_wx[j][n], m_resid_cosx_kernel[m], m_resid_sinx_kernel[m]);
					calc_frequency_response(filt_coeffs[i], m_wx[j][n], &m_magnitudex_kernel[m], &m_phasex_kernel[m]);
					_compute_gabor3_vert_resids(filt_poles[i], filt_coeffs[i], m_wy[j][n], m_resid_cosy_kernel[m], m_resid_siny_kernel[m]);					
					calc_frequency_response(filt_coeffs[i], m_wy[j][n], &m_magnitudey_kernel[m], &m_phasey_kernel[m]);
					m++;
				}
			}
		}
	}
	m_bAllocated = true;
	return true;
}


/*********************************************************************************
FreeResources():
	Deletes internal structures and memory space allocated by AllocateResources
	Careful: Depends on the parameters 'm_i_orientations', 'm_i_scales',
				'm_i_kernels', 'm_i_wavelengths'. Do not change these values
				after calling AllocateResources
**********************************************************************************/
bool CIIRGabor::FreeResources()
{
	int i;
	m_bAllocated = false;
	if(m_resid_step_level != NULL)
	{
		for(i = 0; i < m_i_scales; i++ )
			free(m_resid_step_level[i]);
		free(m_resid_step_level);
		m_resid_step_level = NULL;
	}

	if(m_resid_ic_level != NULL)
	{
		for(i = 0; i < m_i_scales; i++ )
			free(m_resid_ic_level[i]);
		free(m_resid_ic_level);
		m_resid_ic_level = NULL;
	}

	if(m_resid_cosx_kernel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++ )
			free(m_resid_cosx_kernel[i]);
		free(m_resid_cosx_kernel);
		m_resid_cosx_kernel = NULL;
	}
	if(m_resid_sinx_kernel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++ )
			free(m_resid_sinx_kernel[i]);
		free(m_resid_sinx_kernel);
		m_resid_sinx_kernel = NULL;
	}

	if(m_resid_cosy_kernel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++ )
			free(m_resid_cosy_kernel[i]);
		free(m_resid_cosy_kernel);
		m_resid_cosy_kernel = NULL;
	}
	if(m_resid_siny_kernel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++ )
			free(m_resid_siny_kernel[i]);
		free(m_resid_siny_kernel);
		m_resid_siny_kernel = NULL;
	}


	if(filt_coeffs != NULL)
	{
		for(i = 0; i < m_i_scales; i++ )
			free(filt_coeffs[i]);
		free(filt_coeffs);
		filt_coeffs = NULL;
	}
	if(filt_poles != NULL)
	{
		for(i = 0; i < m_i_scales; i++ )
			delete[] filt_poles[i];
		delete[] filt_poles;
		filt_poles = NULL;
	}
	if(m_scale_wav_table != NULL)
	{
		for(i = 0; i < m_i_scales; i++ )
			free(m_scale_wav_table[i]);
		free(m_scale_wav_table);
		m_scale_wav_table = NULL;
	}

	if(m_wx != NULL)
	{
		for(i = 0; i < m_i_orientations; i++ )
			free(m_wx[i]);
		free(m_wx);
		m_wx = NULL;
	}

	if(m_wy != NULL)
	{
		for(i = 0; i < m_i_orientations; i++ )
			free(m_wy[i]);
		free(m_wy);
		m_wy = NULL;
	}

	if(m_magnitudex_kernel != NULL)
	{
		free(m_magnitudex_kernel);
		m_magnitudex_kernel = NULL;
	}
	if(m_phasex_kernel != NULL)
	{
		free(m_phasex_kernel);
		m_phasex_kernel = NULL;
	}

	if(m_magnitudey_kernel != NULL)
	{
		free(m_magnitudey_kernel);
		m_magnitudey_kernel = NULL;
	}
	if(m_phasey_kernel != NULL)
	{
		free(m_phasey_kernel);
		m_phasey_kernel = NULL;
	}

	if(m_scales != NULL)
	{
		free(m_scales);
		m_scales = NULL;
	}
	if(m_orientations != NULL)
	{
		free(m_orientations);
		m_orientations = NULL;
	}
	if(m_wavelengths != NULL)
	{
		free(m_wavelengths);
		m_wavelengths = NULL;
	}
	if(m_dc_gain != NULL)
	{
		free(m_dc_gain);
		m_dc_gain = NULL;
	}
	if(upper_boundary != NULL)
	{
		free(upper_boundary);
		upper_boundary = NULL;
	}
	if(lower_boundary != NULL)
	{
		free(lower_boundary);
		lower_boundary = NULL;
	}
#ifdef USE_IPP
	if(gausslevel != NULL)
	{
		for(i = 0; i < m_i_scales; i++)
			ippiFree(gausslevel[i]);
		free(gausslevel);		
	}
	
	if(laplevel != NULL)
	{
		for(i = 0; i < m_i_scales; i++)
			ippiFree(laplevel[i]);
		free(laplevel);		
	}

	if(reallevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			ippiFree(reallevel[i]);
		free(reallevel);		
	}

	if(imaglevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			ippiFree(imaglevel[i]);
		free(imaglevel);
	}

	if(gaborlevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			ippiFree(gaborlevel[i]);
		free(gaborlevel);
	}

	if(sine != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_wavelengths; i++)
			ippiFree(sine[i]);
		free(sine);		
	}

	if(cosine != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_wavelengths; i++)
			ippiFree(cosine[i]);
		free(cosine);		
	}

	if(input != NULL) 
		ippiFree(input);
	
	if(even != NULL) 
		ippiFree(even);
	
	if(odd != NULL) 
		ippiFree(odd);
		
	if(temp != NULL) 
		ippiFree(temp);	
#else
	if(gausslevel != NULL)
	{
		for(i = 0; i < m_i_scales; i++)
			free(gausslevel[i]);
		free(gausslevel);		
	}
	
	if(laplevel != NULL)
	{
		for(i = 0; i < m_i_scales; i++)
			free(laplevel[i]);
		free(laplevel);		
	}

	if(reallevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			free(reallevel[i]);
		free(reallevel);		
	}

	if(imaglevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			free(imaglevel[i]);
		free(imaglevel);
	}

	if(gaborlevel != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_kernels; i++)
			free(gaborlevel[i]);
		free(gaborlevel);
	}

	if(sine != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_wavelengths; i++)
			free(sine[i]);
		free(sine);		
	}

	if(cosine != NULL)
	{
		for(i = 0; i < m_i_orientations*m_i_wavelengths; i++)
			free(cosine[i]);
		free(cosine);		
	}

	if(input != NULL) 
		free(input);
	
	if(even != NULL) 
		free(even);
	
	if(odd != NULL) 
		free(odd);
		
	if(temp != NULL) 
		free(temp);	
#endif
	gausslevel = NULL;
	laplevel = NULL;
	reallevel = NULL;
	imaglevel = NULL;
	gaborlevel = NULL;
	sine = NULL;
	cosine = NULL;
	input = NULL;
	even = NULL;
	odd = NULL;
	temp = NULL;
	return true;
}

/*********************************************************************************
ProcessImage():
	Computes the response of the gabor filters (real part, imaginary part and energy) 
	for all configured scales, orientations and wavelengths. 
	As a by product also computes the gaussian and laplacian levels
	in : pointer to the input floating point image
**********************************************************************************/
bool CIIRGabor::ProcessImage(IMAGE_PTR in)
{	
	if(!IsAllocated())
		throw "Resources not allocated";

	int i,j,w,s,k;

	//copy image to internal buffer
	for( i = 0; i < m_i_lines; i++ )
		//memcpy(input + (m_i_border+i)*m_i_stridepix + m_i_border, in + m_i_cols*i, m_i_cols*sizeof(float) );
		memcpy(input + i*m_i_stridepix, in + m_i_cols*i, m_i_cols*sizeof(float) );

	

	//computing the gaussian levels
	for( i = 0; i < m_i_scales; i++ )
		_iir_gaussfilt3(input, gausslevel[i], temp, m_i_cols, m_i_lines, m_i_stridepix, filt_coeffs[i], m_resid_ic_level[i], m_resid_step_level[i] );

	if( m_i_orientations > 0) //compute the laplacian levels
	{
		_image_sub(input, gausslevel[0],laplevel[0]);
		for( i = 1; i < m_i_scales; i++)
		{
			_image_sub(gausslevel[i-1],gausslevel[i],laplevel[i]);
		}
	}
	if( m_i_orientations == 0 )
		return true;
	if( m_i_wavelengths == 0 )
		return true;

	//computing the even and odd levels
	k = 0;
	for( j = 0; j < m_i_orientations; j++ )
	{
		for(w = 0; w < m_i_wavelengths; w++)
		{
			//computing the even image
			_image_prod(input,cosine[j*m_i_wavelengths+w],even);
			//computing the odd image
			_image_prod(input,sine[j*m_i_wavelengths+w],odd);

			for(s=0;s<m_i_scales;s++)
			{
				if(m_scale_wav_table[s][w])
				{
					_iir_gaborfilt3(input, even, odd, reallevel[k], imaglevel[k], temp, 
						            m_i_cols, m_i_lines, m_i_stridepix, filt_coeffs[s], m_wx[j][w], m_wy[j][w], 
									m_magnitudex_kernel[k], m_magnitudey_kernel[k], m_phasex_kernel[k], m_phasey_kernel[k],
									m_resid_ic_level[s], m_resid_step_level[s], 
									m_resid_cosx_kernel[k], m_resid_sinx_kernel[k], m_resid_cosy_kernel[k], m_resid_siny_kernel[k],
									upper_boundary, lower_boundary);

					k++;
				}
			}
		}
	}

	//computing real, imag and gabor energy levels
	k = 0;
	for( j = 0; j < m_i_orientations; j++ )
	{
		for(w = 0; w < m_i_wavelengths; w++)
		{	
			for( s = 0;  s < m_i_scales; s++ )
			{
				if(m_scale_wav_table[s][w])
				{
					_image_prod(reallevel[k],cosine[j*m_i_wavelengths+w], even );
					_image_prod(imaglevel[k],sine[j*m_i_wavelengths+w], odd );	
					_image_prod(reallevel[k],sine[j*m_i_wavelengths+w], input );
					_image_prod(imaglevel[k],cosine[j*m_i_wavelengths+w], temp );

					_image_sub(input,temp,imaglevel[k]);
					_const_prod(gausslevel[s],temp,(float)m_dc_gain[k]);
			
					_image_add(even,odd,reallevel[k]);
					_image_sub(reallevel[k],temp,reallevel[k]);

					_image_prod(reallevel[k],reallevel[k],even);
					_image_prod(imaglevel[k],imaglevel[k],odd);
					_image_add(even,odd,temp);
					_image_sqrt(temp,gaborlevel[k]);

					k++;
				}
			}
		}
	}

	return true;
}

/*********************************************************************************
GaussFiltOnce():
	Filters an input image with a gaussian filter.
	Static function - Does not require object initialization.
	in : pointer to the input floating point image
	out : pointer to the output image
	width : number of columns of the input and output images
	height : number of lines of the input and output images
	stridepix : size (in pixels) one line of the image buffers containing the input and output images
	scale : the standard deviation of the gaussian filter
**********************************************************************************/
void CIIRGabor::GaussFiltOnce(IMAGE_PTR in, IMAGE_PTR out, int width, int height, int stridepix, double scale)
{
	complex<double> poles[5];
	float coeffs[6];
	float resid_step[3];
	float resid_nat[9];
	int bufsz = max(width,height);
	float *buf = new float[bufsz];

	if(scale < 0.5)
	{
		throw "Scale values lower than 0.5 unreliable";
	}
	if(scale> 100)
	{
		throw "Scale values higher than 100 are unreliable";
	}

	calc_poles(3,scale,d0_N3_Linf,poles);
	calc_coeffs(3,poles,coeffs);
	_compute_gauss3_resids(poles, coeffs, resid_nat, resid_step);

	_iir_gaussfilt3(in, out, buf, width, height, stridepix, coeffs, resid_nat, resid_step );

	delete [] buf;
}

void CIIRGabor::GaussFiltHorz(IMAGE_PTR in, IMAGE_PTR out, int width, int height, int stridepix, double scale)
{
	complex<double> poles[5];
	float coeffs[6];
	float resid_step[3];
	float resid_nat[9];
	int bufsz = max(width,height);
	float *buf = new float[bufsz];

	if(scale < 0.5)
	{
		throw "Scale values lower than 0.5 unreliable";
	}
	if(scale > 100)
	{
		throw "Scale values higher than 100 are unreliable";
	}

	calc_poles(3,scale,d0_N3_Linf,poles);
	calc_coeffs(3,poles,coeffs);
	_compute_gauss3_resids(poles, coeffs, resid_nat, resid_step);

	_iir_gaussfilt3_horz(in, buf, out, width, height, stridepix, coeffs, resid_nat, resid_step );

	delete [] buf;
}

/*********************************************************************************
GetScales():
	Returns the scale values configured for this object	
	scales : pointer to a user defined buffer with appropriate size
**********************************************************************************/
void CIIRGabor::GetScales(double *scales)
{
	for(int i = 0; i < m_i_scales; i++)
		scales[i] = m_scales[i];
}
/*********************************************************************************
GetWavelengths():
	Returns the wavelenght values configured for this object	
	wavelengths : pointer to a user defined buffer with appropriate size
**********************************************************************************/
void CIIRGabor::GetWavelengths(double *wavelengths)
{
	for(int i = 0; i < m_i_wavelengths; i++)
		wavelengths[i] = m_wavelengths[i];
}
/*********************************************************************************
GetOrientations():
	Returns the orientation values configured for this object	
	orientations : pointer to a user defined buffer with appropriate size
**********************************************************************************/
void CIIRGabor::GetOrientations(double *orientations)
{
	for(int i = 0; i < m_i_orientations; i++)
		orientations[i] = 180.0/pi*m_orientations[i];
}

/*********************************************************************************
GetScaleWavTable():
	Returns the table with computable scale/wavelenght combinations
	scalewavtable : pointer to a user defined buffer with appropriate size
**********************************************************************************/
void CIIRGabor::GetScaleWavTable(bool *scalewavtable)
{
	for(int i = 0; i < m_i_scales; i++)
		for(int j = 0; j < m_i_wavelengths; j++)
			scalewavtable[i*m_i_wavelengths+j] = m_scale_wav_table[i][j];
}

/*********************************************************************************
AccessGaussianLevel():
	Returns a pointer to the internal buffer containing the computed gaussian levels
	level : index of the level to access
**********************************************************************************/
IMAGE_PTR CIIRGabor::AccessGaussianLevel(int level)
{	
	if(!IsAllocated())
		throw "Resources not allocated";

	if( (level < 0) || (level >= m_i_scales))
		throw "Invalid Argument";

	return gausslevel[level];
}

/*********************************************************************************
AccessLaplacianLevel():
	Returns a pointer to the internal buffer containing the computed laplacian levels
	level : index of the level to access
**********************************************************************************/
IMAGE_PTR CIIRGabor::AccessLaplacianLevel(int level)
{
	if(!IsAllocated())
		throw "Resources not allocated";

	if(m_i_orientations == 0)
		throw "Object not configured for laplacian computations";

	if( (level < 0) || (level >= m_i_scales))
		throw "Invalid Argument";

	return laplevel[level];
}

/*********************************************************************************
AccessGaborRealLevel():
	Returns a pointer to the internal buffer containing the computed gabor real levels
	level : index of the level to access
**********************************************************************************/
IMAGE_PTR CIIRGabor::AccessGaborRealLevel(int level)
{
	if((m_i_orientations == 0)||(m_i_wavelengths == 0) )
		throw "Object not configured for gabor computations";

	if(!IsAllocated())
		throw "Resources not allocated";

	if( (level < 0) || (level >= m_i_orientations*m_i_kernels))
		throw "Invalid Argument";

	return reallevel[level];
}

/*********************************************************************************
AccessGaborImagLevel():
	Returns a pointer to the internal buffer containing the computed gabor imag levels
	level : index of the level to access
**********************************************************************************/
IMAGE_PTR CIIRGabor::AccessGaborImagLevel(int level)
{
	if((m_i_orientations == 0)||(m_i_wavelengths == 0) )
		throw "Object not configured for gabor computations";

	if(!IsAllocated())
		throw "Resources not allocated";

	if( (level < 0) || (level >= m_i_orientations*m_i_kernels))
		throw "Invalid Argument";

	return imaglevel[level];
}

/*********************************************************************************
AccessGaborEnergyLevel():
	Returns a pointer to the internal buffer containing the computed gabor real levels
	level : index of the level to access
**********************************************************************************/
IMAGE_PTR CIIRGabor::AccessGaborEnergyLevel(int level)
{
	if((m_i_orientations == 0)||(m_i_wavelengths == 0) )
		throw "Object not configured for gabor computations";

	if(!IsAllocated())
		throw "Resources not allocated";

	if( (level < 0) || (level >= m_i_orientations*m_i_kernels))
		throw "Invalid Argument";

	return gaborlevel[level];
}

/**********************************************************************
IMAGE PROCESSING FUNCTIONS
***********************************************************************/
void CIIRGabor::_image_prod(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out)
{
#ifdef USE_IPP		
	IppiSize roiSize = {m_i_cols, m_i_lines};
	ippiMul_32f_C1R(i1, stride, i2, stride, out, stride, roiSize);
#else
	int i, j;
	for(i = 0; i < m_i_lines; i++)
		for(j = 0; j < m_i_cols; j++)
			out[i*m_i_stridepix+j] = i1[i*m_i_stridepix+j]*i2[i*m_i_stridepix+j];
#endif
}
void CIIRGabor::_image_add(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out)
{
#ifdef USE_IPP		
	IppiSize roiSize = {m_i_cols, m_i_lines};
	ippiAdd_32f_C1R(i1, stride, i2, stride, out, stride, roiSize);
#else
	int i, j;
	for(i = 0; i < m_i_lines; i++)
		for(j = 0; j < m_i_cols; j++)
			out[i*m_i_stridepix+j] = i1[i*m_i_stridepix+j] + i2[i*m_i_stridepix+j];
#endif
}
void CIIRGabor::_image_sub(IMAGE_PTR i1, IMAGE_PTR i2, IMAGE_PTR out)
{
#ifdef USE_IPP		
	IppiSize roiSize = {m_i_cols, m_i_lines};
	ippiSub_32f_C1R(i2, stride, i1, stride, out, stride, roiSize);
#else
	int i, j;
	for(i = 0; i < m_i_lines; i++)
		for(j = 0; j < m_i_cols; j++)
			out[i*m_i_stridepix+j] = i1[i*m_i_stridepix+j] - i2[i*m_i_stridepix+j];
#endif
}
void CIIRGabor::_const_prod(IMAGE_PTR in, IMAGE_PTR out, float gain)
{
#ifdef USE_IPP		
	IppiSize roiSize = {m_i_cols, m_i_lines};
	ippiMulC_32f_C1R(in, stride, gain, out, stride, roiSize);
#else
	int i, j;
	for(i = 0; i < m_i_lines; i++)
		for(j = 0; j < m_i_cols; j++)
			out[i*m_i_stridepix+j] = gain*in[i*m_i_stridepix+j];
#endif
}
void CIIRGabor::_image_sqrt(IMAGE_PTR in, IMAGE_PTR out)
{
#ifdef USE_IPP		
	IppiSize roiSize = {m_i_cols, m_i_lines};
	ippiSqrt_32f_C1R(in, stride, out, stride, roiSize);
#else
	int i, j;
	for(i = 0; i < m_i_lines; i++)
		for(j = 0; j < m_i_cols; j++)
			out[i*m_i_stridepix+j] = (float)sqrt(in[i*m_i_stridepix+j]);
#endif
}
