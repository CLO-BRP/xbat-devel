function g_dst = get_gabor_slices(g_src, slice_type, slice_value)

% get_gabor_slices - for given value(s) across given dimension
% ------------------------------------------------------------
%
% g_dst = get_gabor_slices(g_src, slice_type, slice_value)
% 
% Input:
% ------
%  g_src - struct (output of fast_gabor function)
%  slice_type - one of 'scale', 'orientation', 'wavelength', or 'frequency'
%  slice_value - desired value(s) for requested type
%
% Output:
% -------
%  g_dst - struct
%
% See Also: fast_gabor


if nargin < 3
    error('Missing parameters');
end


switch slice_type
    case 'orientation'
        indices = ismember(g_src.gabor.parameters(:,1), slice_value);
        g_dst.gabor.data = g_src.gabor.data(:, :, indices);
        g_dst.gabor.parameters = g_src.gabor.parameters(indices,:);
        g_dst.gaussian.data = g_src.gaussian.data;
        g_dst.gaussian.parameters = g_src.gaussian.parameters;
        g_dst.laplacian.data = g_src.laplacian.data;
        g_dst.laplacian.parameters = g_src.laplacian.parameters;

    case {'wavelength', 'frequency'}
        if strcmp(slice_type, 'frequency')
            slice_value = pi ./ slice_value;
        end
        indices = ismember(g_src.gabor.parameters(:,2), slice_value);
        g_dst.gabor.data = g_src.gabor.data(:, :, indices);
        g_dst.gabor.parameters = g_src.gabor.parameters(indices,:);
        g_dst.gaussian.data = g_src.gaussian.data;
        g_dst.gaussian.parameters = g_src.gaussian.parameters;
        g_dst.laplacian.data = g_src.laplacian.data;
        g_dst.laplacian.parameters = g_src.laplacian.parameters;
                
    case 'scale'
        indices = ismember(g_src.gabor.parameters(:,3), slice_value);
        g_dst.gabor.data = g_src.gabor.data(:, :, indices);
        g_dst.gabor.parameters = g_src.gabor.parameters(indices,:);
        
        indices = ismember(g_src.gaussian.parameters, slice_value);
        g_dst.gaussian.data = g_src.gaussian.data(:, :, indices);
        g_dst.gaussian.parameters = g_src.gaussian.parameters(indices,:);
        
        indices = ismember(g_src.laplacian.parameters, slice_value);
        g_dst.laplacian.data = g_src.laplacian.data(:, :, indices);
        g_dst.laplacian.parameters = g_src.laplacian.parameters(indices,:);
        
    otherwise
        error(['Unknown gabor slice type ''', slice_type, '''.']);
        
end
