function Y = image_tile(X, m, n)

% image_tile - tile image
% -----------------------
%
% Y = image_tile(X, m, n)
% 
% Input:
% ------
%  X - image to tile
%  m - number of rows or size of image
%  n - number of columns
%
% Output:
% -------
%  Y - tiled image

%--
% parse size parameters
%--

if nargin < 3
	switch length(m)
		case 1
			n = m;
		
		case 2
			n = m(2); m = m(1);	
	end
end

%--
% tile image
%--

switch ndims(X)
	case 2
		Y = repmat(X, m, n);

	case 3
		for k = 1:3
			Y(:, :, k) = repmat(X(:, :, k), m, n);
		end
end
