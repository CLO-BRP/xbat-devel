#include "stdlib.h"
#include "math.h"
#include "mex.h"
#include "matrix.h"
void morph_erode_geo_double (
	double *Y, unsigned char *A,
	double *X, double *M, int m, int n, unsigned char *Z);
void morph_erode_geo_double (
	double *Y, unsigned char *A,
	double *X, double *M, int m, int n, unsigned char *Z)
{
	 int i, j, ij, k, l;
	 double B, L, t; 
	if (Z == NULL) {
	  	for (j = 1; j < (n - 1); j++) {
	  	for (i = 1; i < (m - 1); i++) {
	  		ij = i + (j * m);
			L = *(X + ij);
			B = *(M + ij);
			if (B < L) {
				for (l = (j - 1); l < (j + 2); l++) {
				for (k = (i - 1); k < (i + 2); k++) {
	      			t = *(X + k + (l * m));
	      			if (t < L) {
						L = t;
					}
					if (B >= L) {
						L = B;
						break;
					}
				}
				} 
				if (A) {
					if (*(X + ij) == L) {							
						*(A + (i - 1) + ((j - 1) * (m - 2))) = 0;
					} else {
						*(A + (i - 1) + ((j - 1) * (m - 2))) = 1;
					}
				}
				*(Y + (i - 1) + ((j - 1) * (m - 2))) = L;
			} else {
				if (A) {							
					*(A + (i - 1) + ((j - 1) * (m - 2))) = 0;
				}
				*(Y + (i - 1) + ((j - 1) * (m - 2))) = B;			
			}
		} 
		}
	} else {
	  	for (j = 1; j < (n - 1); j++) {
	  	for (i = 1; i < (m - 1); i++) {
	  		ij = i + (j * m);
	  		if (*(Z + ij)) {
				L = *(X + ij);
				B = *(M + ij);
				if (B < L) {
					for (l = (j - 1); l < (j + 2); l++) {
					for (k = (i - 1); k < (i + 2); k++) {
		      			t = *(X + k + (l * m));
		      			if (t < L) {
							L = t;
						}
						if (B >= L) {
							L = B;
							break;
						}
					}
					} 
					if (A) {
						if (*(X + ij) == L) {							
							*(A + (i - 1) + ((j - 1) * (m - 2))) = 0;
						} else {
							*(A + (i - 1) + ((j - 1) * (m - 2))) = 1;
						}
					}
					*(Y + (i - 1) + ((j - 1) * (m - 2))) = L;
				} else {
					if (A) {							
						*(A + (i - 1) + ((j - 1) * (m - 2))) = 0;
					}
					*(Y + (i - 1) + ((j - 1) * (m - 2))) = B;			
				}
			} else {
				if (A) {							
					*(A + (i - 1) + ((j - 1) * (m - 2))) = 0;
				}
				*(Y + (i - 1) + ((j - 1) * (m - 2))) = *(X + i + (j * m));
			}
		} 
		}
	}
}
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	 double *Y;
	 unsigned char *Y8;
	 unsigned char *A;  int d[2];
	 double *X, *M;
	 unsigned char *X8, *M8;
	 int m, mm, n, nn;
	 unsigned char *Z;
  	if (mxGetClassID(prhs[0]) == mxUINT8_CLASS) {
  	} else {
	  	X = mxGetPr(prhs[0]);
	  	m = mxGetM(prhs[0]); n = mxGetN(prhs[0]);
	  	M = mxGetPr(prhs[1]);
	  	mm = mxGetM(prhs[1]); nn = mxGetN(prhs[1]);
	  	if ((m != mm) || (n != nn)) {
			mexErrMsgTxt("Input images must be of the same size.");
		}
		if (nrhs > 2) {
			if (mxIsEmpty(prhs[2])) {
		  		Z = NULL;	
		  	} else {
			  	if (mxIsUint8(prhs[2])) {
			  		Z = (unsigned char *) mxGetPr(prhs[2]);
			  	} else {
			  		mexErrMsgTxt("Mask must be of class uint8.");
			  	}
			  	if ((m != mxGetM(prhs[2])) || (n != mxGetN(prhs[2]))) {
					mexErrMsgTxt("Images and mask must be of the same size.");
				}
			}
		} else {
			Z = NULL;	
		}
  		*d = (m - 2); *(d + 1) = (n - 2);
  		Y = mxGetPr(plhs[0] = mxCreateDoubleMatrix(m - 2, n - 2, mxREAL));
  		A = (unsigned char *) mxGetPr(plhs[1] = mxCreateNumericArray(2, d, mxUINT8_CLASS, mxREAL));
  		morph_erode_geo_double (Y, A, X, M, m, n, Z);
  	}
}
