#include "mex.h"

#include <stdlib.h>

#include "cuda_runtime.h"

#include "cuda_anisodiff.h"

// create a Matlab scalar value

mxArray* mxCreateScalar(double x) {
	mxArray* p = mxCreateDoubleMatrix(1, 1, mxREAL);
	double*  ptr = mxGetPr(p);
	ptr[0] = x;
	return p;
}

//------------------------------------------------------
// MEX FUNCTION
//------------------------------------------------------

// cuda_ansidiff - CUDA ansiotropic diffusion filter
// -----------------------------------------------------
//
// Implements ansiotropic diffusion filter
//

//
// [Y, e] = cuda_ansiodiff_mex(X,k,l,t,n)
//
// Input:
// ------
//  X - input data
//  k - kappa parameter, default 0.015
//  l - lambda parameter, default 0.25
//  t - function type, Perona Malik diffusion equation 1 or 2
//  n - iteration count, default 100
//
// Output:
// -------
//  Y - Filtered image
//  e - CUDA status

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	
	unsigned int	mX, nX;			// Source dimensions
	unsigned int	dSize;			// Source sample count
	
	void			*h_Data;		// Host source
	void			*d_Data = NULL;	// Device source
	
	void			*h_Result;		// Host result
	void			*d_Result = NULL;	// Device result
	
    double          scale = 20.0;   // Scale parameter
    double          step = 0.25;    // Step size parameter
	unsigned int    t = 1;          // Diffusion equation 
	unsigned int    n = 100;        // Number of iterations
	
	cudaError_t		cudaErr = cudaSuccess;	// CUDA status
	
	
	//--
	// get input
	//--
	
	if (nrhs < 1)
	{
		cudaErr = cudaErrorInvalidValue;	// Missing required inputs
	}
	else 
	{
		// Get input data
		
		mX = (unsigned int) mxGetM(prhs[0]); nX = (unsigned int) mxGetN(prhs[0]);
		
		dSize = mX * nX;
		
		h_Data = (void *) mxGetPr(prhs[0]);
		
	}
	
	// scale parameter
    
	if (nrhs > 1)
	{
		scale = mxGetScalar(prhs[1]);
	}
    
	// step parameter
    // NOTE: step size is divided by the number of neighbors
    
	if (nrhs > 2)
	{
		step = 0.25 * mxGetScalar(prhs[2]);
	}
    
    // type parameter
    
	if (nrhs > 3)
	{
		t = (unsigned int) mxGetScalar(prhs[3]);
	}
	
	// iteration parameter
    
	if (nrhs > 4)
	{
		n = (unsigned int) mxGetScalar(prhs[4]);
	}
	
    //--
	// compute
	//--
	
	switch (mxGetClassID(prhs[0]))
	{
		// BEGIN-EXPAND-TYPES
		
		case MEX_TYPE_CLASS:
			// Create result variable
			h_Result = (void  *) mxGetPr(plhs[0] = mxCreateNumericMatrix(mX, nX, MEX_TYPE_CLASS, mxREAL));
			
			// Allocate GPU global memory for input data
			if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **)&d_Data, dSize * sizeof(MEX_TYPE)); }
			
			// Allocating GPU global memory for result
			if (cudaErr == cudaSuccess) { cudaErr = cudaMalloc((void **)&d_Result, dSize * sizeof(MEX_TYPE) ); }
			
			// Copy input data from host to GPU
			if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(d_Data, h_Data, dSize * sizeof(MEX_TYPE), cudaMemcpyHostToDevice); }
			
			// Compute filter on GPU
			if (cudaErr == cudaSuccess)
			{
				cudaErr = cuda_anisodiff_MEX_TYPE_NAME((MEX_TYPE *) d_Result, (MEX_TYPE *) d_Data, mX, nX,
						scale, step, t, n);
			}
			
			// Copy results data from GPU to host
			if (cudaErr == cudaSuccess) { cudaErr = cudaMemcpy(h_Result, d_Result, dSize * sizeof(MEX_TYPE), cudaMemcpyDeviceToHost); }
			
			break;
			
			// END-EXPAND-TYPES
	}
	
	// Return CUDA error if requested
	if (nlhs > 1)
	{
		plhs[1] = mxCreateScalar((double) cudaErr);
	}
	
	// Free GPU global memory
	if (d_Result != NULL) { cudaFree(d_Result); }
	if (d_Data != NULL) { cudaFree(d_Data); }
}
