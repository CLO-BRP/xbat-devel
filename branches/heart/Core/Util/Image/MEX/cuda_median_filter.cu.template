#include <cuda.h>

#include "cuda_image_pad.h"

// median_filter - median filter computation
// ---------------------------------------------
// 
//  *Y - output image
//  *X - input image
//  T - structuring element size
//  NOTE: T is ignored, 3x3 square window for first implementation
//

/*
  * Find median on a 3x3 input box of integers.
  * b1, b2, b3 are pointers to the left-hand edge of three
  * parallel scan-lines to form a 3x3 spatial median.
  * Rewriting b2 and b3 as b1 yields code which forms median
  * on input presented as a linear array of nine elements.
  */

#define s2(a,b) {register int t; if ((t=b-a)<0) {a+=t; b-=t;}}
#define mn3(a,b,c) s2(a,b); s2(a,c);
#define mx3(a,b,c) s2(b,c); s2(a,c);
#define mnmx3(a,b,c) mx3(a,b,c); s2(a,b);
#define mnmx4(a,b,c,d) s2(a,b); s2(c,d); s2(a,c); s2(b,d);
#define mnmx5(a,b,c,d,e) s2(a,b); s2(c,d); mn3(a,c,e); mx3(b,d,e);
#define mnmx6(a,b,c,d,e,f) s2(a,d); s2(b,e); s2(c,f); mn3(a,b,c); mx3(d,e,f);

// Median filter in global memory
// X and Y are padded image

__global__ void median_filter_CUDA_TYPE_NAME (
	CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, unsigned int rM, unsigned int rN
)

{
    unsigned int rM2 = (rM << 1);
    
    unsigned int im = blockDim.x * blockIdx.x + threadIdx.x;
    unsigned int in = blockDim.y * blockIdx.y + threadIdx.y;
    unsigned int i = (in + rN) * (M + rM2) + rM + im;
	
    // If we are off the edge of the array then do no work
    if (im >= M + rM || in >= N + rN) { return; }
	
    // NOTE: Hardcoded 3x3 neighborhood
	CUDA_TYPE *b1 = X + i - (M + rM2) - 3/2 /* - rN */;
	CUDA_TYPE *b2 = b1 + M + rM2;
	CUDA_TYPE *b3 = b2 + M + rM2;
	
    register int r1, r2, r3, r4, r5, r6;

    r1 = *b1++; r2 = *b1++; r3 = *b1++;
    r4 = *b2++; r5 = *b2++; r6 = *b2++;
    mnmx6(r1, r2, r3, r4, r5, r6);
    r1 = *b3++;
    mnmx5(r1, r2, r3, r4, r5);
    r1 = *b3++;
    mnmx4(r1, r2, r3, r4);
    r1 = *b3++;
    mnmx3(r1, r2, r3);
	
    Y[i] = r2;
}

// Filter each block of pixels in shared memory
// Input and output image include padding

// Tile local to this thread-block of pixels, including apron
extern __shared__ CUDA_TYPE tile_CUDA_TYPE_NAME[];

__global__ void median_filter_tiled_CUDA_TYPE_NAME (
	CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, unsigned int rM, unsigned int rN
)

{
    // Twice apron width for computing offsets
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);
    
    // Offset for addressing sorting buffer
    unsigned int off = (N + rN2) * (M + rM2);
    
    // -----------------------------
    // Fill shared memory pixel tile
    // -----------------------------
    
    // Use the first blockDim.y + rN2 threads to load tile

    unsigned int bi = blockDim.x * blockIdx.x;
	unsigned int bj = blockDim.y * blockIdx.y;
    unsigned int ti = threadIdx.y * blockDim.x + threadIdx.x;
    
    if (ti < blockDim.y + rN2)
    {
        CUDA_TYPE *t = tile_CUDA_TYPE_NAME + ti * (blockDim.x + rM2);
        CUDA_TYPE *d = X + (bj + ti) * (M + rM2) + bi;
        for (int l = 0; l < blockDim.x + rM2; l++)
        {
            *t++ = *d++;
        }
    }

    // Synchronize tile load for thread block
    
    syncthreads();
	
    // --------------
    // Compute result
    // --------------
    
    // Get pixel location for this thread
    unsigned int im = blockDim.x * blockIdx.x + threadIdx.x;
    unsigned int in = blockDim.y * blockIdx.y + threadIdx.y;
    	
    // If we are off the edge of the array then do no work
    if (im >= M || in >= N) { return; }
    
    //------------------------------------------
    // Arbitrary neighborhood size min algorithm
    //------------------------------------------
    /*
    CUDA_TYPE *p, z;
    
    z = tile_CUDA_TYPE_NAME[threadIdx.y * (blockDim.x + rM2) + threadIdx.x];
    for (int n = 0; n <= rN2; n++)
    {
        p = tile_CUDA_TYPE_NAME + (threadIdx.y + n) * (blockDim.x + rM2) + threadIdx.x;
        for (int m = 0; m <= rM2; m++)
        {
            if (*p < z) { z = *p; }
            p++;
        }
    }
    Y[(in + rN) * (M + rM2) + rM + im] = z;
    */
    //---------------------------------------------
    // Arbitrary neighborhood size median algorithm
    //---------------------------------------------

    // Load search array
    
    CUDA_TYPE *tp;
    int count = 0;
    
    for (int n = 0; n <= rN2; n++)
    {
        tp = tile_CUDA_TYPE_NAME + (threadIdx.y + n) * (blockDim.x + rM2) + threadIdx.x;
        for (int m = 0; m <= rM2; m++)
        {
            tile_CUDA_TYPE_NAME[off + count++] = *tp++;
        }
    }
    
    // Find kth smallest of n values in a[]
    
    int i, j, l, m ;
    CUDA_TYPE x ;
    CUDA_TYPE temp ;
    int n = (rM2 + 1) * (rN2 + 1);
    int k = n / 2;
    
    l = 0 ; m = n-1 ;
    while (l<m) {
        x = tile_CUDA_TYPE_NAME[off + k] ;
        i = l ;
        j = m ;
        do {
            while (tile_CUDA_TYPE_NAME[off + i] < x) i++ ;
            while (x < tile_CUDA_TYPE_NAME[off + j]) j-- ;
            if (i<=j)
            {
                temp = tile_CUDA_TYPE_NAME[off + i];
                tile_CUDA_TYPE_NAME[off + i] = tile_CUDA_TYPE_NAME[off + j];
                tile_CUDA_TYPE_NAME[off + j] = temp;
                i++ ; j-- ;
            }
        } while (i<=j) ;
        if (j<k) l=i ;
        if (k<i) m=j ;
    }
    
    Y[(in + rN) * (M + rM2) + rM + im] = tile_CUDA_TYPE_NAME[off + k] ;
}


// ---------------------------------------------------------
// cuda_median_filter
// ---------------------------------------------------------

extern "C" void cuda_median_filter_CUDA_TYPE_NAME (CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, unsigned int rM, unsigned int rN, int iter);

void cuda_median_filter_CUDA_TYPE_NAME (
		CUDA_TYPE *Y, CUDA_TYPE *X, unsigned int M, unsigned int N, unsigned int rM, unsigned int rN, int iter
)

{
	// Double structuring element radius for dimension calculations
    unsigned int rM2 = (rM << 1);
    unsigned int rN2 = (rN << 1);
    
    // Threads per block, arbitrary but NVIDIA's code suggests that this order of threads works well
    dim3 threadsPerBlock(16,16);

    // Block dimensions for processing the image
    dim3 numBlocks;
    
    // Shared memory tile size
	unsigned int shMemSize;
    
    // Old, very simple implementation
    
    // median_filter_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(Y, X, M, N, SE);
    // return;
    
	// Grid size for median filtering
	numBlocks.x = (M + threadsPerBlock.x - 1) / threadsPerBlock.x;
	numBlocks.y = (N + threadsPerBlock.y - 1) / threadsPerBlock.y;

    // Size of shared memory tile buffer
	shMemSize = ( (threadsPerBlock.x + rM2) * (threadsPerBlock.y + rN2) + (rM2 + 1) * (rN2 + 1) ) *
            sizeof(CUDA_TYPE);
    
    // Global memory buffer for image with apron
	CUDA_TYPE *in, *out, *temp;
    
    // Allocate padded image
    cudaMalloc((void **)&in, (M + rM2) * (N + rN2) * sizeof(CUDA_TYPE));
	cudaMalloc((void **)&out, (M + rM2) * (N + rN2) * sizeof(CUDA_TYPE));
    
	// Copy image into center of padded image
	image_pad_center_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(in, X, M, N, rM, rN);
	
	// Iterate filter swapping buffers and recomputing padding as needed
	for (int n = 0; n < iter; n++)
	{
		// Pad image (in place)
		cuda_image_pad_only_CUDA_TYPE_NAME(in, M, N, rM, rN);
    
		// Compute the median filter
		median_filter_tiled_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock, shMemSize>>>(out, in, M, N, rM, rN);
		// median_filter_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(out, in, M, N, rM, rN);

		// Swap input and output buffers
		if (n < iter - 1)
		{
			temp = in;
			in = out;
			out = temp;
		}
	}

    // Copy data out of center of padded image into result
    image_unpad_center_CUDA_TYPE_NAME<<<numBlocks, threadsPerBlock>>>(Y, out, M, N, rM, rN);
	
    // Deallocate padded image buffers
    cudaFree(in);
	cudaFree(out);
}
