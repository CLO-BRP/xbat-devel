// Matlab

#include "mex.h"
#include "matrix.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
	
	double *start, *L, *X, *Y, value, last;
	
	int m, n, i, j, N;
	
	//--
	// INPUT
	//--
	
	// NOTE: we use various pointers, they all start at the start of the data
	
	start = L = X = mxGetPr(prhs[0]); m = mxGetM(prhs[0]); n = mxGetN(prhs[0]);
	
	last = 0.0;
	
	for (j = 0; i < n; j++) {
		
		for (i = 0; i < m; i++) {
			
			value = *(X++);
			
			if (value != last) {
				*(L++) = last = value; N++;
			}
			
		}
		
	}
	
	//--
	// OUTPUT
	//--
	
	Y = mxGetPr(plhs[0] = mxCreateDoubleMatrix(1, N, mxREAL));
	
	for (j = 0; j < N; j++) {
		
	}
	
}


