function [Y, Z] = cuda_mean_dev(X, F, b)

% cuda_mean_dev_filter - implementation
% -----------------------------------
% 
% Y = cuda_mean_dev(X, F, b)
% 
% Input:
% ------
%  X - input image
%  F - filter
%  b - boundary behavior, default reflecting
%
%   -2 - cyclic boundary
%   -1 - reflecting boundary
%    n - n padding for n >= 0
%
% Output:
% -------
%  Y - mean image
%  Z - deviation image

% NOTE: Only reflective boundary is implemented

%--
% handle input
%--

if nargin < 3
    b = -1;
end

%--
% process gray and color images
%--

switch ndims(X)

	case 2
		fdims = size(F);
		
		[Y, Z, status] = cuda_mean_dev_mex(X, F, fdims(1), fdims(2), b);
				
		handle_cuda_failure(status)
		
	case 3
		Y = X;
		Z = X;
		
		for k = 1:3
			[Y(:, :, k), Z(:, :, k), status] = cuda_mean_dev(X(:, :, k), F, b);
			
			handle_cuda_failure(status)
		end
end
