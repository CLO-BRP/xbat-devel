function [Y, S] = comp_label(X, SE, nn)

% comp_label - connected component labelling (binary)
% ---------------------------------------------------
% 
% [Y, S] = comp_label(X, nn)
%        = comp_label(X, SE, nn)
%
% Input:
% ------
%  X - input image
%  nn - number of neighbors
%  SE - connectivity element
%
% Output:
% -------
%  Y - label image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
% $Revision: 5169 $
%--------------------------------

%--------------------------
% HANDLE INPUT
%--------------------------

%--
% check for single plane binary image
%--

if (ndims(X) > 2) || ~is_binary(X)
	error('Input image must be single plane binary image.');
end

%--
% handle variable input
%--

switch (nargin)

	case (1)
		
		SE = []; nn = 4;
		
	case (2)
		
		if ~isempty(se_rep(SE))
			nn = 4;
		else
			SE = []; nn = 4;
		end
		
end

%-------------------------------------------
% LABEL COMPONENTS
%-------------------------------------------

S = [];

%--
% simple connected components
%--

if isempty(SE)
	
	Y = comp_label_(uint8(X), nn);

%--
% connectivity element components
%--
	
else
	
	Y = morph_dilate(uint8(X), SE);	
	
	Y = comp_label_(Y, nn);
	
	if nargout ~= 1
		S = Y; Y = mask_apply(Y, X);
	end
	
	if ~nargout
		fig; image_view({Y, S});
	end
	
end
