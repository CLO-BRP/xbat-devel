function SE = se_sym_diff(SE1,SE2)

% se_sym_diff - symmetric difference of structuring elements
% ----------------------------------------------------------
%
% SE = se_sym_diff(SE1,SE2)
%
% Input:
% ------
%  SE1, SE2 - structuring elements
%
% Output:
% -------
%  SE - points in SE1, and not in SE2

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% match supports
%--

[SE1,SE2] = se_match(SE1,SE2);

%--
% compute symmetric difference
%--

SE = ((SE1 - SE2) > 0) | ((SE2 - SE1) > 0);

if (sum(SE(:)))
	SE = se_mat(se_vec(SE));
else
	SE = [];
end
 
