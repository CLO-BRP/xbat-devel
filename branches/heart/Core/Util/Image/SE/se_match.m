function [SE1,SE2] = se_match(SE1,SE2)

% se_match - match suport of structuring elements
% -----------------------------------------------
%
% [SE1,SE2] = se_match(SE1,SE2)
%
% Input:
% ------
%  SE1, SE2 - structuring elements
%
% Output:
% -------
%  SE1, SE2 - structuring elements with matched support

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% match supports
%--

pq1 = se_supp(SE1);
pq2 = se_supp(SE2);

pq = max(pq1,pq2);

dpq1 = pq - pq1;
dpq2 = pq - pq2;

SE1 = se_loose(SE1,dpq1);
SE2 = se_loose(SE2,dpq2);
 
