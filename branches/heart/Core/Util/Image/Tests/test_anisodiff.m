function [result] = test_anisodiff(varargin)

% test_anisodiff
% -------------------------------------------------------------------------
%
% Input:
% ------
%  'quick' - run fast benchmark instead of extensive benchmark
%  'full' - run extensive benchmark instead of fast benchmark
%  'display' - display CUDA padded image for each test
%  'verbose' - print status output
%  'silent' - print nothing
%  anything else - assumed to be type conversion function handle ie. 'double'
%
% Output
% ------
%  result = 1 if passed, 0 if failed

% NOTE: default is fast bench on double precision data with no messages
% and no display, equivalent to
% 'test_cuda_linear_filter('quick', 'silent', 'double')'

%----------------------
% HANDLE INPUT
%----------------------

fast = 1; float = 0; verbose = 0; display = 0;
type_func = @double;

for i = 1:length(varargin)
    
    switch varargin{i}
        case 'quick', fast = 1;
            
        case {'exhaustive' 'full'}, fast = 0;
            
        case 'silent', verbose = 0;
            
        case 'verbose', verbose = 1;
            
        case 'display', display = 1;
            
        otherwise, type_func = str2func(varargin{i});
    end
end

%----------------------
% SETUP
%----------------------

%--
% initialize output
%--

result = 1;

%--
% load test data
%--

% NOTE: we load data and cast to type we are testing

load clown; % NOTE: this creates the 'X' variable below

X = [X, X; X, X]; X = [X; X]; %#ok<NODEF>

Xt = type_func(X);

%--
% check for compatible hardware before running CUDA test
%--

if ~has_cuda
    
    warning('CUDA hardware not available, test not run.'); return; %#ok<*WNTAG>
else
    level = get_cuda_capability;
    
    if isa(Xt, 'double') && level < 1.3
        
        warning('CUDA hardware capability not available, test not run.'); return;
    end
end

%--
% set parameter ranges
%--

kernel = {'tukey'; 'lorentz'; 'huber'; 'linear'};

%----------------------
% RUN TEST
%----------------------

%--
% Compare CUDA filter function to MEX implementation over range of parameters
%--

if display
    par = fig;
end

for i = 1:numel(kernel)
    
    for j = 5:20:45
        
        for k = 50:50:150
            
            if verbose
                fprintf('Type %s, scale %d, iter = %d:', kernel{i}, j, k);
            end
            
            try
                tic; i0 = cuda_anisodiff(Xt, kernel{i}, j, k); cuda_time = toc;
            catch
                result = 0; nice_catch(lasterror, 'CUDA anisotropic diffusion failed.');
            end
            
            if display
                figure(par);
                
                image_view(double(i0));
            end
            
            tic; i1 = aniso_filter(Xt, kernel{i}, j, k); ml_time = toc;
            
            % Set return code if we have an error
            
            err = max(max(i0 - i1));
            
            if err > 10^-6
                result = 0;
                if verbose
                    fprintf(' FAILED! Error = %8.5f', err);
                end
            else
                if verbose
                    fprintf(' Passed, Error = %8.5f', err);
                end
            end
            
            % Report speedup
            if verbose
                fprintf(' Speedup = %3.2f\n', ml_time / cuda_time);
            end
            
        end % for k
        
    end % for j
    
end % i
