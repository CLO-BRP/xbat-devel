function [E, X] = test_image_extrema(X)

%--
% handle input
%--

if ~nargin
	X = random_smooth_image;
end

%--
% compute and display extrema
%--

E = image_extrema(X);

%--
% display result
%--

fig; imagesc(X); axis image; colormap(flipud(gray(256))); ax = gca;

[r, c] = find(E == 1); 

props = {'linestyle', 'none', 'marker', 'o', 'markersize', 4};

line('parent', ax, 'xdata', c, 'ydata', r, 'color', 'blue', 'markerfacecolor', 'blue', props{:});

hold on;

% tri = delaunay(c, r); h = triplot(tri, c, r); set(h, 'color', 'blue', 'linestyle', '-', 'marker', 'none');

[r, c] = find(E == -1); 

line('parent', ax, 'xdata', c, 'ydata', r, 'color', 'red', 'markerfacecolor', 'red', props{:});

tri = delaunay(c, r); h = triplot(tri, c, r); set(h, 'color', 'red', 'linestyle', ':', 'marker', 'none');
