function [value, fractional] = is_isodate(str)

% is_isodate - check whether a string is isodate
% ----------------------------------------------
%
% [value, fractional] = is_isodate(str)
%
% Input:
% ------
%  str - proposed date string
%
% Output:
% -------
%  value - numerical date or non-isodate indicator
%  fractional - fractional isodate string indicator

%--
% handle multiple values
%--

if iscell(str) && numel(str) > 1
	value = iterate(mfilename, str); return;
end

%--
% check for string
%--

% NOTE: when test fails fractional is not defined, perhaps use nan?

if ~ischar(str)
	value = 0; fractional = []; return;
end

%--
% check proposed string, extracting fractional indicator as well
%--

fractional = ~isempty(find(str == '.', 1));


try
	value = datenum(str, isodate_format(fractional));
catch
	value = 0; fractional = [];
end


