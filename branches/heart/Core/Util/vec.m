function x = vec(X)

% vec - convert array to column vector
% ------------------------------------
%
% x = vec(X)
%
% Input:
% ------
%  X - input array
%
% Output:
% -------
%  x - column vector obtained from array

% NOTE: this allows better access to a simple built-in operation

x = X(:);
