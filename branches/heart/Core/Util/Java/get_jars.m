function jars = get_jars(root, verb)

% get_jars - scan directories looking for jars
% --------------------------------------------
%
% jars = get_jars(root, verb)
%
% Input:
% ------
%  root - of scan
%  verb - flag
%
% Output:
% -------
%  jars - found
%
% See also: append_classpath

%--
% handle input
%--

if nargin < 2
	verb = false;
end

if ~nargin
	root = pwd;
end 

%--
% scan for jars
%--

dirs = scan_dir(root); jars = {};

if verb, disp(' '); end

for k = 1:numel(dirs)
	if verb
		disp(['Scanning ''', dirs{k}, ''' ...']);
	end
	
	content = what_ext(dirs{k}, 'jar');
	
	if ~isempty(content.jar)
		content.jar = strcat(content.path, filesep, content.jar); jars = {jars{:}, content.jar{:}};
		
		if verb
			iterate(@disp, content.jar); disp(' ');
		end
	end
end

% NOTE: display results if output is not captured

if ~nargout
	disp(' '); iterate(@disp, jars); disp(' '); clear jars;
end

