function jars = append_classpath(jars)

% append_classpath - add jars to dynamic classpath if needed
% ----------------------------------------------------------
%
% jars = append_classpath(jars) 
%
% Input:
% ------
%  jars - locations
%
% Output:
% -------
%  jars - appended

if isempty(jars)
	return;
end

% NOTE: we check the full (not just the dynamic) classpath to determine whether a jar should be added

% NOTE: this step saves some warnings

if ischar(jars)
    jars = {jars};
end
    
jars = jars(~jar_in_classpath(jars));


% NOTE: this means that all files were already found in classpath

if isempty(jars)
	return;
end

% NOTE: here we add to the dynamic classpath

javaaddpath(jars); 