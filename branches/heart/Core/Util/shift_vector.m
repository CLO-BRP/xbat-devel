function [out, tail] = shift_vector(in, offset)

% shift_vector - shift vector
% ---------------------------
%
% [out, tail] = shift(in, offset)
%
% Input:
% ------
%  in - vector
%  offset - offset
%
% Output:
% -------
%  out - shifted vector
%  tail - lost entries

%--
% handle input
%--

dim = size(in);

if numel(dim) > 2 || min(dim) > 1
	error('Only vector input is supported.');
end

if offset ~= floor(offset)
	error('Offset must be an integer.');
end

%--
% shift vector
%--

out = zeros(size(in));

if offset > 0
	out(offset + 1:end) = in(1:end - offset); tail = in(end - offset + 1:end);
else
	out(1:end + offset) = in(-offset + 1:end); tail = in(1:-offset);
end


