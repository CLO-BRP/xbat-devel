function top = project_menu(par)

% project_menu - create or update project menu
% --------------------------------------------
% 
% top = project_menu(par)
%
% Input:
% ------
%  parent - of menu
%
% Output:
% -------
%  top - menu handle

% TODO: consider project context extension loading, this is not directly related

%----------------------
% HANDLE INPUT
%----------------------

if ~nargin
	par = get_xbat_figs('type', 'sound'); par(end + 1) = get_xbat_palette;
end

if numel(par) > 1
	top = iterate(mfilename, par); return;
end

%----------------------
% SETUP
%----------------------

%--
% get current project
%--

project = get_current_project;

%--
% check for and delete previous project menu
%--

top = findobj(par, 'type', 'uimenu', 'tag', 'PROJECT_MENU_TOP');

if ~isempty(top)
	delete(findall(top)); 
end

%----------------------
% CREATE MENU
%----------------------

%--
% create top project menu
%--

% NOTE: these are the top level menus

all_top = findobj(par, 'type', 'uimenu', 'parent', par);

label = 'PRJ';

top = uimenu(par, 'label', label, 'tag', 'PROJECT_MENU_TOP', 'position', numel(all_top) + 1);

%--
% create project switching menus
%--

head = uimenu(top, 'label', 'Set Active Project  ');

uimenu(head, 'label', '(Add)', 'enable', 'off');

uimenu(head, 'label', 'Repository ...', 'callback', @new_repository_callback);

uimenu(head, 'label', 'New Project ...', 'callback', @new_project_callback);

uimenu(head, 'label', 'From Repository ...', 'callback', @install_project_callback);

projects = all_projects; 

% TODO: remove current project from this list

uimenu(head, 'label', '(Projects)', 'enable', 'off', 'separator', 'on');

for k = 1:numel(projects)
	current = projects(k);
	
	uimenu(head, 'label', current.name, 'callback', {@set_project_callback, current.name, current.author});
end

% NOTE: there is nothing more to do if there is no active project

if isempty(project)
	return;
end

%--
% active project information and access menus
%--

uimenu(top, 'label', '(Active)', 'enable', 'off', 'separator', 'on');

field = fieldnames(project);

for k = 1:numel(field) 
	
	if strcmp(field{k}, 'repository') || strcmp(field{k}, 'fun')
		continue;
	end
	
	value = project.(field{k}); 
	
	if isempty(value);
		continue;
	end
	
	uimenu(top, 'label', [title_caps(field{k}), ':  ', value]);
	
end

uimenu(top, 'label', 'Show Files ...', 'callback', @show_files_callback);

%--
% subversion integration menus
%--

uimenu(top, 'label', '(SVN)', 'separator', 'on', 'enable', 'off');

[versioned, info] = project_in_repository(project);

if versioned
	
	uimenu(top, 'label', ['Repository:  ', project.repository], 'enable', 'off');
	
	command = {'status', 'update', 'commit'}; label = strcat(title_caps(command), {' ...'});
		
	for k = 1:numel(command)
		uimenu(top, 'label', label{k}, 'callback', {@svn_client_callback, command{k}});
	end
	
else 
	
	head = uimenu(top, 'label', 'Add To Repository');
	
	repository = get_project_repositories; 
	
	for k = 1:numel(repository)
		uimenu(head, 'label', repository{k}, 'callback', @add_to_repository_callback);
	end
	
	uimenu(head, 'label', 'New Repository ...', 'callback', @new_repository_callback, 'separator', 'on');
	
end

%--
% project extension menus
%--

uimenu(top, 'label', '(EXT)', 'separator', 'on', 'enable', 'off');

% NEW PROJECT EXTENSION

head = uimenu(top, 'label', 'New');

uimenu(head, 'label', 'Extension ...', 'callback', @new_project_extension);

type = get_project_extension_types;

uimenu(head, 'label', '(Types)', 'separator', 'on', 'enable', 'off');

for k = 1:numel(type)
	uimenu(head, 'label', [title_caps(type{k}), ' ...'], 'callback', {@new_project_extension, type{k}});
end

% CURRENT PROJECT EXTENSION

% TODO: some of these do not work, specifically action menus

head = uimenu(top, 'label', 'Project Extensions');

ext = get_project_extensions(project.name);

if isempty(ext)
	uimenu(head, 'label', '(No Project Extensions)', 'enable', 'off');
else
	handles = extension_category_menu(head, ext); set(handles(1), 'separator', 'off');
end 


%-----------------------------
% SET_PROJECT_CALLBACK
%-----------------------------

function set_project_callback(obj, eventdata, name, author)

%--
% get project from menu label
%--

% TODO: put all information in tag, then we gain freedom for the label

project = get_project(name, author);

if isempty(project)
	return;
end

%--
% update active project and menu
%--

set_current_project(project);

project_menu;


%-----------------------------
% NEW_PROJECT_CALLBACK
%-----------------------------

function new_project_callback(obj, eventdata)

%--
% create new project
%--

project = new_project_dialog;

if isempty(project)
	return; 
end

%--
% set current project and update menu
%--

set_current_project(project);

project_menu;


%-----------------------------
% INSTALL_PROJECT_CALLBACK
%-----------------------------

function project = install_project_callback(obj, eventdata)

%--
% try to install project
%--

[added, project] = install_project;

if ~added
	return; 
end 

%--
% make installed project current
%--

% NOTE: we convert the name to a project structure, consider throwing an error if this fails

project = get_project(project);

if isempty(project)
	return;
end

set_current_project(project); 

project_menu;


%-----------------------------
% SHOW_FILES_CALLBACK
%-----------------------------

function show_files_callback(obj, eventdata)

show_file(project_root(get_current_project));


%-----------------------------
% SVN_CLIENT_CALLBACK
%-----------------------------

function svn_client_callback(obj, eventdata, command) %#ok<INUSL>

%--
% get current project and effect subversion command
%--

project = get_current_project;

tsvn(command, project_root(project));

%--
% update extension cache in the case of 'update' command
%--

if strcmp(command, 'update')

	% TODO: update path to include possibly new extension directories
	
	% TODO: make it possible to only discover project extensions
	
	ext = discover_extensions('', '', project.name); extensions_cache(ext);

	project_menu;
	
end


%-----------------------------
% ADD_TO_REPOSITORY_CALLBACK
%-----------------------------

function add_to_repository_callback(obj, eventdata)

%--
% get project and set repository
%--

project = get_current_project;

project.repository = get(obj, 'label');

%--
% add to repository and update menu
%--

[added, info] = add_project_to_repository(project);

if added < 1
	info, error('Failed to add project to repository.'); %#ok<NOPRT>
end

% NOTE: we update the current project as it now has an associated repository

set_current_project(project);

project_menu;


%-----------------------------
% NEW_REPOSITORY_CALLBACK
%-----------------------------

function new_repository_callback(obj, eventdata)

%--
% consider adding new repository
%--

% TODO: new repository dialog, should create a local repository with ease

repo = new_repository;

if isempty(repo)
	return;
end

%--
% try to add project to new repository if one was created
%--

project = get_current_project; 

[added, info] = add_project_to_repository(project, repo);

if ~added
	info; error('Failed to add project to repository.'); 
end

%--
% update current project and menus
%--

project.repository = repo;

set_current_project(project);

project_menu;



%-----------------------------
% INSTALL_PROJECT_CALLBACK
%-----------------------------

function new_project_extension(obj, eventdata, type)

%--
% handle input and setup
%--

if nargin < 3
	type = '';
end

par = ancestor(obj, 'figure'); 

project = get_current_project;

%--
% present dialog for new extension
%--

ext = new_extension_dialog(type, par, project.name);

if isempty(ext)
	return;
end

% TODO: refresh extensions cache to include new project extension

project_menu;




