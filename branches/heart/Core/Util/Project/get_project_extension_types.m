function type = get_project_extension_types

type = setdiff(get_extension_types, {'extension_type', 'source'});