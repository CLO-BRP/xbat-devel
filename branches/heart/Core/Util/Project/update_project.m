function [updated, name] = update_project(name)

% update_project - update project by name
% ---------------------------------------
%
% [updated, name] = update_project(name)
%
% Input:
% ------
%  name - of projects (def: all projects)
%
% Output:
% -------
%  updated - indicator
%  name - of respective project

% TODO: the project description may have a stale root, we can always use 'svn info' to get the current root

% TODO: sometimes 'localhost' external references really want to point to the same host

%--
% get all projects or project structs from names
%--

if ~nargin
	project = all_projects;
else
	project = get_project(name);
end

% NOTE: in the 'else' clause we may have failed to retrieve some projects

name = {project.name};

%--
% update projects
%--

% NOTE: organize in case author name has changed

organize_projects;

if ~nargout, disp(' '); end
	
updated = false(size(project));

for k = 1:numel(project)
	
	current = project(k); fun = functions(current.fun);
	
	if ~project_in_repository(current)
		continue;
	end
	
	if ~nargout, disp(['Updating ''', current.name, ''' project ...']); start = clock; end
	
	% NOTE: this is the actual location of the project, not the conventional location
	
	[status, result] = svn('up', fileparts(fun.file));
	
	updated(k) = ~status;
	
	if ~nargout
		if updated(k)
			disp(['Done.  (', sec_to_clock(etime(clock, start)), ')']);
		else
			disp('Failed to update.'); disp(['  ', result]);
		end
		
		disp(' ');	
	end
	
end

% NOTE: organize in case author name has changed

organize_projects;

if ~nargout 
	clear updated;
end

