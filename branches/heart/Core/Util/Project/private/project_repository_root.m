function root = project_repository_root(project, repository) 

% NOTE: in the case of struct input we get the fields, otherwise the input is string input

%--
% handle input
%--

if isstruct(project)

	% NOTE: if the repository is not provided as input we get it from the project
	
	if nargin < 2
		repository = project.repository; 
	end

	project = project.name;

end

if isempty(repository)
	error('Unable to determine repository address.');
end

%--
% get repository project root
%--

root = [projects_repository_root(repository), '/', encode(project)];


%---------------------
% ENCODE
%---------------------

function url = encode(url) 

url = strrep(strrep(url, ' ', '%20'), '&', '%26');