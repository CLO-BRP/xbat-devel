function info = get_screen_info

% get_screen_info - get screen properties from root object
% --------------------------------------------------------
%
% info = get_screen_info
%
% Output:
% -------
%  info - screen properties from root object

% TODO: this type of get using a fieldname selector can be factored and applied elsewhere

%--
% get screen relevant fields
%--

all = fieldnames(get(0)); ix = strmatch('Screen', all); field = all(ix);

%--
% get properties from root object
%--

% TODO: consider a version of iterate that populates a struct, outputs a struct

info = struct;

for k = 1:numel(field)
	info.(field{k}) = get(0, field{k});
end
