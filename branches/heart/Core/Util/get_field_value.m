function [field, value] = get_field_value(args, allow)

% get_field_value - pairs from cell array
% ----------------------------------------
%
% [field, value] = get_field_value(args, allow)
%
% Input:
% ------
%  args - cell array of field value pairs
%  allow - field names allowed
%
% Output:
% -------
%  field - field names cell array
%  value - values cell array

%---------------------------------------------
% HANDLE INPUT
%---------------------------------------------

%--
% allow anything by default
%--

if nargin < 2
	allow = {};
end

%--
% handle empty input
%--

if isempty(args)
	field = {}; value = {}; return;	
end

%--
% check for cell input of even length
%--

if ~iscell(args)
	error('Input must be of type cell.');
end

if mod(length(args), 2)
	error('Input cell array must be of even length.');
end

%---------------------------------------------
% GET FIELDS AND VALUES
%---------------------------------------------

%--
% separate putative fields and values
%--

field = args(1:2:end); value = args(2:2:end);

%--
% check that field names are strings
%--

if ~iscellstr(field)
	error('Contents of odd indexed cells must be strings.');
end

%--
% check for allowed fields
%--

% NOTE: consider ignoring the not allowed field

if ~isempty(allow)
	
	for k = 1:length(field)
		
		if ~ismember(field{k}, allow)
			error(['Field ''' field{k} ''' is not an allowed field.']);
		end

	end

end

%--
% sort the field value pairs by field names
%--

[field, ix] = sort(field);

value = value(ix);
	
