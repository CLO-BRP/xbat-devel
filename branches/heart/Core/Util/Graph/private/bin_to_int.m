function x = bin_to_int(b)

% bin_to_int - binary to integer representation
% ------------------------------------------
%
% x = bin_to_int(b)
%
% Input:
% ------
%  b - binary representation
%
% Output:
% -------
%  x - integers
%

%--
% get size of binary matrix
%--

[m,n] = size(b);

%--
% compute integers
%--

x = 2.^(0:m-1) * b;

