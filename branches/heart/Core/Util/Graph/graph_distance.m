function G = graph_distance(V, fun, b)

% graph_distance - create distance graph
% --------------------------------------
%
% G = graph_distance(V, fun, b)
%
% Input:
% ------
%  V - values
%  fun - distance function
%  b - distance-link activity interval
%
% Output:
% -------
%  G - distance network

%--
% compute and threshold distance matrix to get adjacency matrix
%--

% NOTE: we threshold using an activity interval

A = thresh(feval(fun, V, V), b);

%--
% pack graph
%--

G.V = V; G.E = sparse_to_edge(A);




