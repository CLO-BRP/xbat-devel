function result = test_pack_unpack(n)

if ~nargin
	n = 10;
end

% NOTE: a distance matrix is symmetric and has a zero diagonal

A = rand(n); S = A + A'; 

for k = 1:n
	S(k, k) = 0;
end

s = pack_distance(S);

R = unpack_distance(s); % result of pack then unpack

% NOTE: this test is not a thorough as the order could change

r = pack_distance(unpack_distance(s)); % result of unpack then pack

% db_disp; S, R, s, r

result = all(R(:) == S(:)) && all(r(:) == s(:));