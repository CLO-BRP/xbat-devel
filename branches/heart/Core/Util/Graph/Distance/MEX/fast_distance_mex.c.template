//----------------------------------------------
// INCLUDE FILES
//----------------------------------------------

#include "mex.h"

#include "math.h"

//----------------------------------------------
// FUNCTIONS
//----------------------------------------------

// NOTE: this computes the full distance matrix

// TODO: make use of 'self' flag to halve the computation

void fast_distance (double *D, double *X, double *Y, int M, int N, int d, int f, int self);

void fast_distance (double *D, double *X, double *Y, int M, int N, int d, int f, int self) {
	
	int m, n, k; double *x, *y, t, z;

	for (m = 0; m < M; m++) {
		x = X + m * d;
				
		for (n = 0; n < N; n++) {
			y = Y + n * d;
			
			switch (f) {
				case 0:
					for (k = 0, z = 0.0; k < d; k++) {
						z += (*(x + k) - *(y + k) == 0.0) ? 0.0 : 1.0; 
					}
					*(D + m + n * M) = z;
					break;
					
				case 1:
					for (k = 0, z = 0.0; k < d; k++) {
						z += fabs(*(x + k) - *(y + k));
					}
					*(D + m + n * M)  = z;
					break;
					
				case 2:
					for (k = 0, z = 0.0; k < d; k++) {
						t = *(x + k) - *(y + k); z += t * t;
					}
					*(D + m + n * M) = sqrt(z);
					break;
			}
		}
	}
}   

// NOTE: this computes the full smallest (eventually largest distance)

// TODO: consider generalization to smallest-k, largest-k, mean and deviation

// TODO: initialize the D values to something infinity, this reduces logic below

// TODO: unroll loops? interesting code-generation exercise, look at wikipedia article

void fast_nearest (double *D, double *X, double *Y, int M, int N, int d, int f, int k, int self);

void fast_nearest (double *D, double *X, double *Y, int M, int N, int d, int f, int k, int self) {
	
	int m, n, j; double *x, *y, t, z, dm, dn;

	for (m = 0; m < M; m++) {
		x = X + m * d; 
				
		for (n = 0; n < N; n++) {
			y = Y + n * d; 

			dm = *(D + m); dn = *(D + M + n);
			
			// NOTE: when computing the self distance, compute the smallest non-trivial (to another point) distance
			
			if (self && (m == n)) continue;
			
			// TODO: we only accumulate while we have a chance of being the smallest, this does not work for largest!
			
			switch (f) {
				case 0:
					for (j = 0, z = 0.0; j < d; j++) {
						z += (*(x + j) - *(y + j) == 0.0) ? 0.0 : 1.0; 
					}
					break;
					
				case 1:
					for (j = 0, z = 0.0; j < d; j++) {
						z += fabs(*(x + j) - *(y + j));
					}
					break;
					
				case 2:
					for (j = 0, z = 0.0; j < d; j++) {
						t = *(x + j) - *(y + j); z += t * t;
						
						// if ((dn > 0.0) && (z > dn) && (dm > 0.0) && (z > dm)) break;
					}
					break;
			}
			
			// TODO: here is where we will consider the value of k
						
			// NOTE: here we update the smallest distance and neighbor point index to the m-th and n-th points if needed
			
			if ((dm == 0.0) || (z < dm)) {				
				*(D + m) = z; *(D + (M + N) + m) = n + 1;
			}

			if ((dn == 0.0) || (z < dn)) {
				*(D + M + n) = z; *(D + (M + N) + M + n) = m + 1;
			}
		}
	}
	
	// NOTE: in the case of the 'euclidean' distance we wait until the end to compute root
	
	if (f == 2) {
		for (j = 0; j < M + N; j++) {
			*(D + j) = sqrt(*(D + j));
		}
	}
}   


//----------------------------------------------
// MEX FUNCTION
//----------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
		
	double *X, *Y, *D; int M, N, d, f, k;
    
    int self;

	// INPUT
	
	X = mxGetPr(prhs[0]); M = mxGetN(prhs[0]);
	
	Y = mxGetPr(prhs[1]); N = mxGetN(prhs[1]);
	
	d = mxGetM(prhs[0]);

	if (d != mxGetM(prhs[1])) {
		mexErrMsgTxt("Vector collections have different dimensionality.");
	}
	
	// NOTE: check for self comparison
	
	self = ((M == N) && (X == Y));
		
	// NOTE: by default we compute the 'euclidean' distance
	
	if (nrhs < 3) {
		f = 2;
	} else {
		f = (int) mxGetScalar(prhs[2]);
	}
	
	// NOTE: by default we compute the distance matrix
	
	if (nrhs < 4) {
		k = 0;
	} else {
		k = (int) mxGetScalar(prhs[3]);
	}
	
	// ALLOCATE OUTPUT AND COMPUTE
		
	// NOTE: the meaning of the integer flag 'f' is defined in 'fast_nearest' and 'fast_distance'
	
	switch (k) {
		case 0:
			D = mxGetPr(plhs[0] = mxCreateDoubleMatrix((mwSize) M, (mwSize) N, mxREAL));
			
			fast_distance(D, X, Y, M, N, d, f, self);
			break;
			
		default:
			D = mxGetPr(plhs[0] = mxCreateDoubleMatrix((mwSize) (M + N), (mwSize) 2, mxREAL));
			
			fast_nearest(D, X, Y, M, N, d, f, k, self); 
	}
}
