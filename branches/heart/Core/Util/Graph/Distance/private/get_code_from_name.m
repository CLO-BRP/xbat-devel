function code = get_code_from_name(name)

% get_code_from_name - helper for 'fast_distance_mex'
% ---------------------------------------------------
%
% code = get_code_from_name(name)
%
% Input:
% ------
%  name - for distance
%
% Output:
% -------
%  code - in MEX
%
% See also: fast_distance, fast_nearest

switch name
	
	case {0, 'l0', 'hamming'}
		code = 0;
		
	case {1, 'l1', 'absolute'}
		code = 1;
	
	case {2, 'l2', 'euclidean'}
		code = 2;
		
	otherwise
		error(['Unrecognized distance type ''', name, '''.']);
end