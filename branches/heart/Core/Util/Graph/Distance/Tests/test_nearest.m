function test_nearest

%--
% get and display point set
%--

X = random_smooth_image(256); E = image_extrema(X);

fig; imagesc(X); axis image; colormap(flipud(gray(256))); ax = gca;

[r, c] = find(E == 1); 

props = {'linestyle', 'none', 'marker', 'o', 'markersize', 4};

line('parent', ax, 'xdata', c, 'ydata', r, 'color', 'blue', 'markerfacecolor', 'blue', props{:});

%--
% compute nearest distance
%--

D = fast_nearest([r, c]')