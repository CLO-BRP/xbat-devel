function G = graph_degree(n,p)

% graph_degree - create graph with specified degree distribution
% --------------------------------------------------------------
%
% G = graph_degree(n,p)
%
% Input:
% ------
%  n - size of graph
%  p - degree distribution
%
% Output:
% -------
%  G - graph with specified degree distribution
%    .E - edges
%

%--
% compute degrees
%--

K = length(p);

d = [];

for k = 1:K
	d = [d k*ones(ceil(p(k)*n))];
end

m = length(d);

d = d(randperm(m));

d = d(1:n);

%--
% match edges
%--


%--
% create graph
%--



