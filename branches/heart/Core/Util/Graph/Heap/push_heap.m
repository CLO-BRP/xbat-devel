function [heap, ix] = push_heap(heap, items)

%--
% get length of heap and count items to push 
%--

n = size(heap, 2); count = numel(items);

%--
% allocate resulting heap, and push items
%--

% NOTE: we add indices to keep track of object movement during heaping

heap = [heap, zeros(1, count)]; heap(2, :) = 1:numel(heap);

for k = 1:count
	heap(1, n + k) = items(k); heap = upheap(heap, n + k);
end

ix = heap(2, :); heap = heap(1, :);