function flag = proper_filename(str)

% proper_filename - determine whether a string may be a filename
% --------------------------------------------------------------
%
% flag = proper_filename(str)
%
% Input:
% ------
%  str - proposed filename
%
% Output:
% -------
%  flag - proper flag

% NOTE: this is a simple check for a proper filename

% NOTE: there is a better way of implementing this using regular expressions,

%--------------------------------------
% CHECK FOR EMPTY
%--------------------------------------

if isempty(str)
	flag = 0; return;
end

%--------------------------------------
% CHECK FOR CHARACTERS NOT ALLOWED
%--------------------------------------

%--
% improper characters
%--

not_allowed = double('\/:*"<>|');

%--
% convert string to double and check
%--

str = double(str);

for k = 1:length(not_allowed)
	
	if ~isempty(find(str == not_allowed(k), 1))
		flag = 0; return;
	end 
	
end

flag = 1;
