function url = fullurl(protocol, host, varargin)

% fullurl - build full url from parts
% -----------------------------------
%
% url = fullurl(protocol, host, part_1, ... , part_k)
%
% Input:
% ------
%  protocol - name
%  host - name
%  part_j - path element
%
% Output:
% -------
%  url - string

%--
% normalize elements
%--

psep = '://'; sep = '/';

% NOTE: remove protocol separator if needed

if strcmp(protocol(end - 2:end), psep)
	protocol(end - 2:end) = [];
end

% NOTE: remove trailing separator from host

if host(end) == sep
	host(end) = [];
end

% NOTE: remove separators from path elements

part = varargin;

if iscellstr(part)
	error('Elements of path must be strings.');
end

for k = 1:numel(part) - 1
	
	if part{k}(1) == sep
		part{k}(1) = [];
	end
	
	if part{k}(end) == sep
		part{k}(end) = []; 
	end
	
end

if part{end}(1) == sep
	part{end}(1) = [];
end

% NOTE: if the last element is an extension join before imploding

if part{end}(1) == '.'
	part{end - 1} = [part{end - 1}, part{end}]; part(end) = [];
end

%--
% join elements with proper separators
%--

url = [protocol, psep, host, sep, str_implode(part, sep)]; 

