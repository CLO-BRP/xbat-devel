function [ax, h] = harray(par, layout, base)

% harray - create hierarchical axes array
% ---------------------------------------
%
% [ax, h] = harray(par, layout, base)
%
% Input:
% ------
%  par - parent figure
%  layout - hierarchical array layout
%  base - base layout
%
% Output:
% -------
%  ax - created axes
%  h - corresponding harray elements

%---------------------------------------------------------------
% HANDLE INPUT
%---------------------------------------------------------------

%--
% set base layout
%--

% TODO: check base layout

if (nargin < 3) || isempty(base)
	base = layout_create(0); base.margin(1) = 0.5;
end

%--
% check for layout
%--

if (nargin < 2) || isempty(layout)
	error('Layout input is required.');
end

%--
% check layouts
%--

[flag, str] = layout_check(layout); %#ok<NASGU>

% if any(~flag)
% 		
% 	if length(layout) == 1
% 		error(str);
% 	end
% 		
% 	for k = 1:length(layout)
% 		disp(str{k});
% 	end
% 
% 	error('All layouts are not valid.');
% 	
% end

%--
% set default parent
%--

if isempty(par)
	par = figure;
end

%---------------------------------------------------------------
% CREATE HIERARCHICAL ARRAY
%---------------------------------------------------------------

%--
% get parent position
%--

par_pos = get_size_in(par, base.units);

%--
% get some colors
%--

color.control = get(0, 'defaultuicontrolbackgroundcolor');

color.figure = get(0, 'defaultfigurecolor');

% NOTE: slightly darker color than mean

color.line = 0.25 * (color.control + color.figure);

%--
% create toolbar axes
%--

% TODO: add capability for multiple toolbars

if base.tool.on
	pos = [ ...
		0, par_pos(4) - base.tool.size, par_pos(3), base.tool.size ...
	];

	tax = axes( ...
		'parent', par, ...
		'tag', 'HARRAY_TOOLBAR', ...
		'box', 'on', 'xtick', [], 'ytick', [], ...
		'xlim', [0, 1], 'ylim', [0, 1], ...
		'color', color.control, ...
		'xcolor', color.line, ...
		'ycolor', color.control, ...
		'units', base.units, 'position', pos ...
	);
end

%--
% create statusbar axes
%--
	
if base.status.on
	pos = [ ...
		0, 0, par_pos(3), base.status.size ...
	];

	sax = axes( ...
		'parent', par, ...
		'tag', 'HARRAY_STATUSBAR', ...
		'box', 'on', 'xtick', [], 'ytick', [], ...
		'xlim', [0, 1], 'ylim', [0, 1], ...
		'color', color.control, ...
		'xcolor', color.line, ...
		'ycolor', color.control, ...
		'units', base.units, 'position', pos ...
	);

	str = text(0, 0.5, ''); set(str, 'parent', sax, 'tag', 'HARRAY_STATUS_LEFT');
end

%--
% create colorbar axes
%--

% NOTE: we use a single colorbar since a figure has a single colormap

pos(1) = par_pos(3) - (base.color.size + base.margin(2));

pos(2) = base.margin(3) + ...
	(base.status.on * base.status.size) + ...
	layout(1).margin(3) ...
; 

pos(3) = base.color.size;

pos(4) = par_pos(4) - ( ...
	base.margin(1) + base.margin(3) + ...
	(base.tool.on * base.tool.size) + (base.status.on * base.status.size) + ...
	layout(1).margin(1) + layout(1).margin(3) ...
);

cax = axes( ...
	'parent', par, ...
	'tag','HARRAY_COLORBAR', ...
	'box', 'on', 'xtick', [], 'ytick', [], ...
	'color', 'none', ...
	'xcolor', color.line, ...
	'ycolor', color.line, ...
	'units', base.units, 'position', pos ...
);

%--
% create base axes
%--

pos(1) = base.margin(4);

pos(2) = base.margin(3) + (base.status.on * base.status.size); 

pos(3) = par_pos(3) - ( ...
	base.margin(2) + base.margin(4) + ...
	(base.color.on * (base.color.size + base.color.pad)) ...
);

pos(4) = par_pos(4) - ( ...
	base.margin(1) + base.margin(3) + ...
	(base.tool.on * base.tool.size) + (base.status.on * base.status.size) ...
);

ax = axes( ...
	'parent', par, ...
	'tag', 'HARRAY_BASE', ...
	'units', base.units, 'position', pos ...
);

h(1) = harray_create;

h(1).level = 0;

h(1).axes = ax;

%--
% create axes for layout levels
%--

for k = 1:length(layout)

	%--
	% find parent axes for this level
	%--
		
	lev = cell2mat({h.level}');
	
	ix = find(lev == (k - 1));
	
	for j = 1:length(ix)
	
		%--
		% compute positions of children relative to this parent
		%--
		
		pos = pos_rel(h(ix(j)).axes, layout(k));
		
		%--
		% create children axes and hierarchical array structures
		%--
		
		% TODO: consider a tagging formalism that uses a layout level, row,
		% and column tags, say LEVEL(ROW,COL)::LEVEL(ROW,COL):: ...
		
		for i2 = 1:layout(k).col.size
			
			%--
			% get column tag string
			%--
			
			if ~isempty(layout(k).col.label)
				col_tag = layout(k).col.label{i2};
			else
				col_tag = '';
			end
			
		for i1 = 1:layout(k).row.size

			%--
			% get row tag string
			%--
			
			if ~isempty(layout(k).row.label)
				row_tag = layout(k).col.label{i1};
			else
				row_tag = '';
			end
			
			%--
			% build tag string
			%--
			
			switch (~isempty(col_tag) + ~isempty(row_tag))
				
				case 0, tag = '';
					
				case 1, tag = [row_tag, col_tag];
					
				case 2, tag = [row_tag, '::', col_tag];
					
			end
			
			%--
			% create axes
			%--

			% NOTE: the max protects us from non-positive width and height
			
			ax = axes( ...
				'parent', par, ...
				'tag', tag, ...
				'box', 'on', ...
				'units', layout(k).units, 'position', max(pos{i1, i2}, eps) ...
			);
		
			%--
			% add created axes to parent's children
			%--

			h(ix(j)).children(i1, i2) = ax;

			%--
			% add new element to harray array
			%--

			h(end + 1) = harray_create;

			h(end).level = k;

			h(end).index = [h(ix(j)).index, i1, i2];

			h(end).axes = ax;
			
			h(end).parent = h(ix(j)).axes;

		end
			
		end
				
	end
	
end

%--
% pad harray indices 
%--

nix = length(h(end).index);

% NOTE: we can break because the index lengths are increasing

for k = 1:length(h)
	
	ixl = length(h(k).index);
	
	if (ixl < nix)
		h(k).index = [h(k).index, zeros(1, nix - ixl)];
	else
		break;
	end
	
end

%--
% store harray data in tagged axes and make these invisible
%--

data.type = 'harray';

data.base = base;

data.layout = layout;

data.harray = h;

set(h(1).axes, ...
	'tag', 'HARRAY_BASE_AXES', ...
	'userdata', data, ...
	'visible', 'off', ...
	'hittest', 'off' ...
);

%--
% set resize function
%--

% NOTE: this introduces a dependency on the 'append_resize', and various other 'callback' functions

append_resize(par, @harray_resize);

% set(par, ...
% 	'resizefcn', @harray_resize ...
% );

%--
% set tool, status, and color bar visibility
%--

if ~base.tool.on
	harray_toolbar(par, 'off');
end 

if ~base.status.on
	harray_statusbar(par, 'off');
end

if ~base.color.on
	harray_colorbar(par, 'off');
end

%--
% create bar tabs
%--

% TEST CODE: create toggle tab using patch

create_tab(par, 'status');

create_tab(par, 'tool');

%--
% output axes handles
%--

ax = [h.axes]';








