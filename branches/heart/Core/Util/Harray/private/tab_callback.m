function tab_callback(obj, eventdata, par, bar, marker)

%--
% check we are using markers
%--

if ~isequal(get(marker, 'type'), 'line')
	marker = 0;
end

%--
% toggle bar display
%--

switch bar

	case 'tool'
		
		state = harray_toolbar(par);

		if marker
			if isequal(state, 'on')
				set(marker, 'marker', '^');
			else
				set(marker, 'marker', 'v');
			end
		end

	case 'status'
		
		state = harray_statusbar(par);

		if marker
			if isequal(state, 'on')
				set(marker, 'marker', 'v');
			else
				set(marker, 'marker', '^');
			end
		end

end