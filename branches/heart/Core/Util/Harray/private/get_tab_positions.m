function [pos, marker] = get_tab_positions(ax, bar, wide, data)

%--
% handle input
%--

par = ancestor(ax, 'figure');

if nargin < 4
	data = harray_data(par);
end

if nargin < 3
	wide = 2;
end

%--
% get base layout units and size of parent
%--

base = data.base;

par_pos = get_size_in(par, base.units);

%--
% compute tab element positions
%--

% NOTE: the 'patch' position is not for a patch, is functions as a patch

switch bar
	
	case 'status'
		
		width = wide * base.status.size / par_pos(3);
		
		off = 0.49 - width/2;
		
		pos.tab = [off + 0.5 - width/2, 0.5, width, 1.25];
		
		width = 1.5 * width;
		
		pos.patch = [off + 0.5 - width/2, 0.4, width, 0.55];
		
		pos.marker = [off + 0.5, 1.35]; marker = 'v';
		
	case 'tool'
		
		width = wide * base.tool.size / par_pos(3);
		
		off = 0.49 - width/2;
		
		pos.tab = [off + 0.5 - width/2, -0.75, width, 1.25];
		
		width = 1.5 * width;
		
		pos.patch = [off + 0.5 - width/2, 0.0, width, 0.55];
		
		pos.marker = [off + 0.5, -0.35]; marker = '^';
		
end