function out = pretty_json(out)

% pretty_json - string output
% ---------------------------
%
% See also: to_json

%--
% compute pretty print version
%--

out = strrep(out, '{', '{\n'); out = strrep(out, '[', '[\n');

out = strrep(out, '}', '\n}'); out = strrep(out, ']', '\n]');

out = strrep(out, ',', ',\n');

out = sprintf(out);

out = strtrim(file_readlines(out));

depth = ...
	cumsum(string_ends(out, '{') + string_ends(out, '[')) - ...
	(string_ends(out, '{') + string_ends(out, '[')) - ...
	cumsum(string_begins(out, '}') + string_begins(out, ']')) ...
;

tab = sprintf('\t');

for k = 1:numel(out)
	out{k} = [repmat(tab, 1, depth(k)), out{k}];
end

%--
% display if output is not captured
%--

if ~nargout
	for k = 1:numel(out), disp(out{k}); end; disp(' ');
	
	clear out;
end