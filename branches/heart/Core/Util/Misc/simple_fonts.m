function fonts = simple_fonts

% simple_fonts - list of simple fonts
% -----------------------------------
%
% fonts = simple_fonts
%
% Output:
% -------
%  fonts - list of simple, common fonts

%--
% create persistent list and output
%--

% NOTE: 'Times' does not seem different from 'Times New Roman'

% NOTE: 'Courier' does not seem different from 'Courier New'

persistent SIMPLE_FONTS;

if isempty(SIMPLE_FONTS)
	
	SIMPLE_FONTS = { ...
		'Arial', ...
		'Bitstream Vera Sans', ...
		'Bitstream Vera Serif', ...
		'Book Antiqua', ...
		'Century Gothic', ...
		'Comic Sans MS', ...
		'Courier', ...
		'Courier New', ...
		'Futura', ...
		'Georgia', ...
		'Helvetica', ...
		'Lucida Console', ...
		'Lucida Grande', ...
		'Lucida Sans Unicode', ... 
		'Palatino Linotype', ...
		'Times', ...
		'Times New Roman', ...
		'Trebuchet MS', ...
		'Verdana' ...
	}';

end

fonts = SIMPLE_FONTS;
