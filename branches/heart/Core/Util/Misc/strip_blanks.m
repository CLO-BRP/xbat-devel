function str = strip_blanks(str)

% strip_blanks - remove leading and trailing spaces from a string
% ---------------------------------------------------------------
%
% str = strip_blanks(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  str - string without leading or trailing spaces

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% this approach to stripping blanks is not very efficient and works on short strings
%--

str = deblank(fliplr(deblank(fliplr(str))));