function [value, type] = alternate_click(obj)

% alternate_click - indicate not normal click
% -------------------------------------------
%
% [value, type] = alternate_click(obj)
%
% Input:
% ------
%  obj - object clicked
%
% Output:
% -------
%  value - alternate click indicator
%  type - type of click

%--
% check for not normal click
%--

type = get(ancestor(obj, 'figure'), 'selectiontype');

value = ~strcmp(type, 'normal');