function dirs = find_dirs(root, indicator, verbose)

% find_dirs - with the help of an indicator
% -----------------------------------------
%
% dirs = find_dirs(root, indicator, verbose)
%
% Input:
% ------
%  root - of search
%  indicator - function
%  verbose - indicator (def: false)
%
% Output:
% -------
%  dirs - found

% NOTE: the 'scan_dir' callback uses the current directory name as it first input, and outputs a selected directory

if nargin < 3
	verbose = false;
end

dirs = scan_dir(root, {@filter, indicator, verbose});

%--------------------
% FILTER
%--------------------

function p = filter(p, indicator, verbose)

% NOTE: the indicator function 'indicates' selection

if iscell(indicator)
	value = indicator{1}(p, indicator{2:end});
else
	value = indicator(p);
end

% NOTE: display directories being scanned to allow for some feedback, improve

if ~value
	if verbose
		disp(p);
	end
	
	p = [];
else
	if verbose
		disp(' '); disp('MATCH');
		disp(p);
		disp(' ');
	end
end

