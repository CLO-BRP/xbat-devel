function p = axes_transpose(h)

% axes_transpose - transpose axes
% -------------------------------
%
% p = axes_transpose(h)
%
% Input:
% ------
%  h - handle to axes
%
% Output:
% -------
%  p - properties changed in transposed axes

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-07-06 13:36:01-04 $
%--------------------------------

%--
% get axes properties
%--

q = get(h);

%--
% get names
%--

str = fieldnames(q);

%--
% get indexes to X and Y properties
%--

xix = strmatch('X',str);
xix = xix(6:end);

yix = strmatch('Y',str);
yix = yix(6:end);

%--
% exchange property values
%--

for k = 1:length(xix)

	eval(['p.' str{xix(k)} ' = q.' str{yix(k)} ';']);
	eval(['p.' str{yix(k)} ' = q.' str{xix(k)} ';']);

end
	
