function out = pack_workspace

% pack_workspace - pack workspace variables into structure
% --------------------------------------------------------
%
% out = pack_workspace
%
% Output:
% -------
%  out - workspace as structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% TODO: use a variable name filter in the packing

% TODO: use a class filter for the packing

%--
% get variable names
%--

work = evalin('caller', 'whos');

%--
% return empty struct on empty workspace
%--

out = struct;

if isempty(work)
	return;
end

%--
% pack workspace variables into structure
%--

for k = 1:length(work)
	out.(work(k).name) = evalin('caller', work(k).name);
end
