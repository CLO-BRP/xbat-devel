function [in, field, value] = parse_inputs(in, varargin)

% parse_inputs - put field value pairs into a structure
% -----------------------------------------------------
%
% [out, field, value] = parse_inputs(in, 'field', value, ...)
%
% Input:
% ------
%  in - struct to update
%  field - field name
%  value - field value
%
% Output:
% -------
%  out - updated struct
%  field - fields
%  value - values

% TODO: consider refactor to use 'get_field_value'

% TODO: consider renaming this function, make sure to consider all places where it's called, too many

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% check input is scalar struct
%--

% TODO: the length condition is not required, we could update multiple structures in an array

if ~isstruct(in) || (length(in) > 1)
	error('Input must be scalar struct.');
end

%--
% check pairing of field and value pairs
%--

n = length(varargin);

if n < 1
	field = {}; value = {}; return;
end

% NOTE: the last input may be indicate we should flatten the struct

flat = 0;

if mod(n, 2)
	if strcmp(varargin{end}, 'flatten')
		flat = 1; varargin(end) = []; n = n - 1;
	else
		error('Input fields and values are not properly paired.');
	end
end

%--
% indicate build through trivial input
%--

build = trivial(in); 

%--------------------------------
% PARSE AND UPDATE
%--------------------------------

%--
% separate and check field value pairs
%--

n = n / 2; field = cell(1, n); value = cell(1, n);

for k = 1:n

	field{k} = varargin{(2 * k) - 1}; value{k} = varargin{2 * k};
	
	if ~ischar(field{k})
		error('Field names must be strings.');
	end
	
end

%--
% update input struct fields 
%--

% NOTE: we allow that hierarchical field names be expressed using periods

if flat
	in = flatten(in); field = iterate(@strrep, field, '.', '__');
end

for k = 1:n

	%--
	% skip unrecognized fields
	%--
	
	% TODO: this could be greatly improved by displaying caller information
	
	% NOTE: consider the compilation problem with stack inspection
	
	if ~build && ~isfield(in, field{k})
		disp(['WARNING: Non-matching input field ''', field{k}, ''' ignored.']); continue;
	end
	
	%--
	% update fields
	%--
	
	switch field{k}
		
		% NOTE: implement 'notes' as a decoration, similar to tags
		
		case 'tags'
			try
				in = set_tags(in, value{k});
			catch
				nice_catch(lasterror, 'Failed to set tags.');
			end
			
		otherwise
			in.(field{k}) = value{k};
	
	end

end

if flat
	in = unflatten_struct(in);
end