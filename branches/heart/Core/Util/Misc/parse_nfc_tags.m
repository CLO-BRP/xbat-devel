function tags = parse_nfc_tags(str)

%--
% handle cell array of strings input
%--

if iscell(str)
	tags = iterate(str); return;
end

%--
% set regular expressions to use for extraction
%--

% NOTE: all caps are used for the species code

rex{1} = '([A-Z]*)';

% NOTE: number sequences are used for band numbers

% rex{2} = '([0-9]*[_\-][0-9]*)';

%--
% get tags from string
%--

tags = {};

for k = 1:length(rex)

	[start, stop] = regexp(str, rex{k});

	% NOTE: we should also be checking that the length of the pattern is proper
	
	for j = 1:length(start)
		
		if (stop - start) ~= 3
			continue; 
		end
		
		tags{end + 1} = str(start(j):stop(j));
	
	end

end

seasons = {'winter', 'spring', 'summer' , 'fall'}; str = lower(str);

for k = 1:length(seasons)
	
	if ~isempty(strfind(str, seasons{k}))
		tags{end + 1} = title_caps(seasons{k});
	end
	
end
