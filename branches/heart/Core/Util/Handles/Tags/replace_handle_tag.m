function replace_handle_tag(handle, current, replace)

if numel(handle) > 1
	iterate(mfilename, handle, current, replace); return;
end 

tags = get_handle_tags(handle); 

if isempty(tags) 
	return;
end

[found, ix] = string_is_member(current, tags);

if ~found
	return; 
end

tags{ix} = replace; set_handle_tags(handle, tags);