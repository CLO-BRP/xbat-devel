function [handle, created] = create_patch(par, tag, varargin)

[handle, created] = create_obj('patch', par, tag, varargin{:});