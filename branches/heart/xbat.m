function root = xbat(config)

% xbat - start xbat
% -----------------
%
% xbat
%
% NOTE: this function starts the XBAT environment

% NOTE: if opening the palette works we have been initialized 

try
	xbat_palette; return;
end 

%-------------------------------
% SETUP
%-------------------------------

%--
% set matlab properties
%--

% NOTE: name warning currently triggers on private functions

warning('off', 'MATLAB:dispatcher:nameConflict');

warning('off', 'MATLAB:divideByZero');

%--
% take care of some settings for linux
%--

if isunix
	
	% NOTE: this is voodoo, it may not work in the future
	
	setappdata(0, 'UseNativeSystemDialogs', false);
	
	% NOTE: this color was empirically derived
	
	set(0, 'DefaultFigureColor', [0.91, 0.89, 0.85]);
	
	set(0, 'DefaultUicontrolBackgroundColor', [0.91, 0.89, 0.85]);
end

% NOTE: we can set delete to use the recycling bin if we want

recycle('off');

%-------------------------------
% INITIALIZE PATH
%-------------------------------

%--
% initialize path
%--

disp(' '); disp('Updating MATLAB path ...'); start = clock;

root = initialize_path;

elapsed = etime(clock, start);

disp(['Done. (', num2str(elapsed), ' sec)']);

%--
% install platform and version appropiate MEX files 
%--

% NOTE: the MEX install makes things already on disk accesible

install_mex;

%--
% set XBAT application root
%--

% NOTE: we wait to set the root until we append the path

update = ~isempty(app_root);

app_root(root);

splash_root(fullfile(app_root, 'Core', 'Resources', 'Splash'));

% NOTE: we can effectively use many XBAT functions after this point

%--
% patch path
%--

% NOTE: we wait until 'app_root' has been set, we need it to determine whether we are clobbered

% NOTE: a function 'create_widget' has been recently added to Simulink 2009b, remove from path

clobbered = {'create_widget'};

for k = 1:numel(clobbered)
	
	if isempty(strmatch(app_root, which(clobbered{k})))
		
		remove_path(fileparts(which(clobbered{k})), false);
	end
end

%--
% install various tools and drivers
%--

% NOTE: for this MD5 install, a .jar is already on disk

install_fast_md5;

% NOTE: the installs below all rely on the network right now

get_curl;

% TODO: consider checking for the availability of the server on the machine

% TODO: consider a dialog asking which drivers to install

install_jdbc_driver mysql; 

install_jdbc_driver postgresql;

% NOTE: this installs a database engine along with corresponding JDBC drivers

% TODO: consider bundling this with the release, it's too large for convenient on-demand install

install_derby; 

install_hsql;

% install_jdbc_driver sqlite;

% install_jdbc_driver sqlserver;

% NOTE: these install command-line helpers for FLAC and MP3

% NOTE: while each of these installers check for PC, they produce a warning message in the non-PC case

if ispc	
	install_lame;
	
	install_flac;
end

% NOTE: this installs a command-line Subversion client

get_svn;

%-------------------------------
% SET GRAPHICS PROPERTIES
%-------------------------------

%--
% set figure properties
%--

set(0, 'DefaultFigureMenubar', 'none');

%--
% set text properties
%--

fonts = get_simple_fonts; font.name = fonts{1}; font.size = 8;

set_default_text_properties(font);

%-------------------------------
% CONFIGURE PALETTES
%-------------------------------

%--
% palette size
%--

% NOTE: allowed values are: 'smaller', 'small', 'medium', 'large', and 'larger'

height = get(0, 'screensize'); height = height(4);

palette_size = 'small';

if height < 1100
	palette_size = 'smaller';
end
	
if height <= 768
	palette_size = 'smallest';
end

set_env('palette_size', palette_size);

%--
% other palette options
%--

% NOTE: allowed values are 'on' and 'off'

set_env('palette_gradient', 'on');

set_env('palette_sounds', 'on');

set_env('palette_tooltips', 'on');

% TODO: consider revealing the hover behavior here

%-------------------------------
% SHOW SPLASH 
%-------------------------------

%--
% create splash
%--

% NOTE: determine the number of displayed startup steps, these are the ticks

ticks = 6 + get_total_extension_count;

% NOTE: we do this here because splash uses palette properties

% TODO: use different images for initial load and refresh

if ~update
	splash = splash_wait(select_splash, ticks);
else
	clear('functions'); splash = splash_wait(select_splash, ticks);
end

%--
% display fictitious yet informative messages
%--

first_steps = {'computing path', 'appending path', 'setting root', 'configuring gui'};

for k = 1:length(first_steps)
	splash_wait_update(splash, [first_steps{k}, ' ...']);
end

%------------------------------------------
% CONSIDER ROOT CHANGE
%------------------------------------------

splash_wait_update(splash, 'updating users ...');

users_root_update;

%------------------------------------------
% REFRESH EXTENSIONS
%------------------------------------------

get_formats(1);

% NOTE: consider allowing number input for this function

get_extensions('!');

%------------------------------------------
% OPEN XBAT PALETTE
%------------------------------------------

splash_wait_update(splash, 'opening xbat palette ...');

% NOTE: this is where the extensions get loaded

xbat_palette; 

pause(0.5); close(splash);

set_env('palette_sounds', 'on');

%------------------------------------------
% MISC 
%------------------------------------------

%--
% PREFERENCES AND MODES
%--

% NOTE: this sets an environment variable used by other functions

%--
% user options
%--

show_desktop 1;

show_other_sounds 0;

%--
% developer options
%--

pcode_refresh('clear');

%--
% start palette daemon
%--

try
	stop(palette_daemon); start(palette_daemon);
catch
% 	nice_catch;
end

% NOTE: this suppresses output display

if ~nargout
	clear root;
end

%--
% check for updates
%--

% TODO: move this way up ... use 'svn' client when we've developed and indeterminate waitbar

% NOTE: we should first check for, show, and confirm whether the user wants the updates, no fine grained control yet

% NOTE: we also have a section on the waitbar where we can display what was updated

% xbat_update('startup');

%--
% handle migration on first use, when system seems empty
%--

% users = get_users;

% if (length(users) == 1) && (length(users(1).library) == 1) && isempty(get_library_sounds)
% 	
% 	result = quest_dialog( ...
%         {'Would you like to migrate ', 'Users and libraries from ', 'an old version of XBAT?'}, ...
%         'Import Old Version' ...
% 	);
% 	
% 	if strcmp(result, 'Yes')
% 		migrate_xbat;
% 	end
% 	
% end

%--
% set user preference
%--

set_user_preference(get_active_user, 'image_buttons', 0);

%--
% try to initialize wavelab
%--

% TODO: figure out where to put this code

% pi = pwd;
% 
% if ~isempty(toolbox_which('WavePath'))
% 	
% 	try
% 		cd(fullfile(toolbox_root('Wavelab'), 'Wavelab850')); startup;
% 	catch
% 		nice_catch(lasterror, 'Failed to initialize ''Wavelab''.');
% 	end
% 	
% end
% 
% cd(pi);

%--
% setup default and consider startup
%--

if ~nargin
	config = 'default';
end

current = which('on__startup');

if isempty(current) || ~strcmp(fileparts(current), app_root)
	% NOTE: we copy the template startup file from the resource location
	
	copyfile(fullfile(app_root, 'Core', 'Resources', 'private', 'on__startup.m'), fullfile(app_root, 'on__startup.m'));
end

call_startup(config);

%--
% create launcher
%--

create_launcher;

%------------------------------------------
% CREATE HOME BARN
%------------------------------------------

establish_barn_schema(get_barn_store);


%--------------------------------
% GET_PATH_STR
%--------------------------------

function str = get_path_str(root, mode)

% get_path_str - add directories to path
% ----------------------------------
%
% str = get_path_str(root, 'flat')
%     = get_path_str(root, 'rec')
%
% Input:
% ------
%  root - initial directory
%
% Output:
% -------
%  str - string appended to path

%-----------------
% HANDLE INPUT
%-----------------

if nargin < 2
	mode = 'flat'; 
end 

%-----------------
% SETUP
%-----------------

% NOTE: the order is MATLAB, XBAT, Tools, and MISC

EXCLUDE_DIRS = { ...
	'rails', ...
	'Patches', ...
	'private', ...
	'public', ...
	'Presets', ...
	'Users', ...
	'RadRails', ...
	'apache', ...
	'iconv', ...
	'share', ...
	'rails_apps', ...
	'phpmyadmin', ...
	'ruby', ...
	'MSYS', ...
	'CVS' ...
};

%-----------------
% BUILD STRING
%-----------------

out = what_ext(root);
		
str = out.path;

for k = 1:length(out.dir)

	%--
	% skip dot and method, then excluded directories
	%--

	if (out.dir{k}(1) == '.') || (out.dir{k}(1) == '@')
		continue;
	end

	if ~isempty(find(strcmp(out.dir{k}, EXCLUDE_DIRS), 1))
		continue;
	end

	%--
	% handle add mode
	%--
	
	switch mode

		case 'flat'
			part = [root, filesep, out.dir{k}];

		case 'rec'
			part = get_path_str([root, filesep, out.dir{k}], 'rec');

	end
	
	%--
	% append partial path string to path string
	%--
	
	str = [str, pathsep, part];

end


%--------------------------------
% WHAT_EXT
%--------------------------------

function out = what_ext(source, varargin)

% what_ext - get directory content information using extensions
% -------------------------------------------------------------
%
% out = what_ext(source, ext1, ..., extN)
%
% Input:
% ------
%  source - source directory 
%  ext - desired file extensions
%
% Output:
% -------
%  out - structure with path, file extensions, and dir

%-----------------
% HANDLE INPUT
%-----------------

%--
% set directory
%--

if (nargin < 1) || isempty(source)
	source = pwd;
end

%--
% set extensions to search
%--

if length(varargin) < 1
	ext = [];
else
	ext = varargin;
end

%-----------------
% GET CONTENTS
%-----------------

%--
% output path field 
%--

out.path = source;

%--
% get directory contents
%--

content = dir(source); 

% NOTE: this removes self and parent directory references

content = content(3:end); 

%--
% get children directory contents
%--

D = {};

for k = length(content):-1:1
	
	if content(k).isdir
		D{end + 1} = content(k).name; content(k) = [];
	end
	
end

out.dir = flipud(D(:));

if isempty(ext)
	return;
end

%--
% get files with specified extensions
%--

for i = 1:length(ext)
	
	%--
	% create list of selected filenames
	%--
	
	L = {};
	
	for k = length(content):-1:1
		
		%--
		% get extension from name
		%--
		
		ix = findstr(content(k).name, '.');
		
		% NOTE: file has no extension in name
		
		if isempty(ix)
			continue;
		end
		
		r = content(k).name(ix(end) + 1:end);
		
		%--
		% select file based on extension
		%--
		
		% NOTE: consider making this case insensitive, at least optional
		
		if strcmp(r, ext{i})
			L{end + 1} = content(k).name; content(k) = [];
		end
		
	end
	
	L = flipud(L(:));
		
	%--
	% put cell array into field
	%--
	
	out.(ext{i}) = L;

end


%-------------------------------
% INITIALIZE_PATH
%-------------------------------

function [root, start_path] = initialize_path

%--
% get xbat path
%--

root = fileparts(mfilename('fullpath'));

% NOTE: caching path does not gain much and causes problems during development

if 1
	
	xbat_path = get_path_str(root, 'rec');
	
else
	
	% NOTE: check whether we have cached the path string
	
	cache = fullfile(root, 'path-cache.txt');

	if ~exist(cache, 'file') 
		fid = -1;
	else
		fid = fopen(cache);
	end

	% NOTE: the path is stored as a single line text file

	if fid ~= -1
		xbat_path = fgetl(fid); 
	else
		xbat_path = get_path_str(root, 'rec'); fid = fopen(cache, 'w'); fprintf(fid, '%s', xbat_path);
	end

end

%--
% get initial path
%--

start_path = path;

init_path = strread(path, '%s', 'delimiter', pathsep);

init_path = setdiff(init_path, strread(xbat_path, '%s', 'delimiter', pathsep));

%--
% restore default if possible
%--

if ~isempty(which('restoredefaultpath'))
	restoredefaultpath;
end

default_path = strread(path, '%s', 'delimiter', pathsep);

%--
% get user path
%--

user_path_set = setdiff(init_path, default_path);

user_path = '';

for k = 1:length(user_path_set)
	user_path = [user_path, user_path_set{k}, pathsep];
end

%--
% concatenate paths together, making sure that the user path is under xbat
%--

path(path, xbat_path);

path(path, user_path);

%--
% display message if path is changed
%--

% NOTE: this 'warning' does not produce a good impression, best to document this elsewhere

if 0 % numel(user_path_set)
	
	str = ' WARNING: XBAT has modified the MATLAB path.';
	
	n = max(64, length(str) + 1); line = str_line(n, '_');

	disp(' ')
	
	disp(line);
	disp(' ');
	disp(str);
	disp(line);
	
	disp(' ');

	disp(' MESSAGE:'); 
	disp(' ');
	disp(' The following non-default elements of path');
	disp(' have been moved to the end of the MATLAB path');
	disp(' to ensure the proper execution of XBAT.');
	disp(' ');
    
	for k = 1:numel(user_path_set)
		disp([' ', int2str(k), '. ', user_path_set{k}]);
	end
	
	disp(' ');
	disp(' To restore the previous path, restart MATLAB.');
	disp(' ');
	
	disp(line);
	disp(' ');
	
	disp(' ');
	
end


%-------------------------------
% CALL_STARTUP
%-------------------------------

% NOTE: the startup.m script runs at the end of the xbat.m startup process

function call_startup(config)

%--
% check for official startup script
%--

candidate = which('on__startup.m'); official = fullfile(app_root, 'on__startup.m');

if ~strcmp(candidate, official)
	return;
end

%--
% try to run startup script
%--

% NOTE: the startup 'script' is actually a function, we do not rely on the workspace as a configuration repository

try
	on__startup(config);
catch
	nice_catch(lasterror, ['Local startup script failed with ''', config, ''' configuration.']);
end


%-------------------------------
% CREATE_LAUNCHER
%-------------------------------

% TODO: this will be factored, we are waiting for some further 'Core' organization

function create_launcher

% NOTE: nothing to do if we are not a PC

if ~ispc
	return;
end

%--
% create launcher script if needed
%--

% TODO: consider which of the launchers is the 'marked' launcher, in the 64-bit system the 32-bit applications are marked (x86)

current = get_matlab_version; 

if current.arch ~= current.mex
	extra = [' (', int2str(current.mex), ')'];
else
	extra = '';
end

file = ['XBAT-', current.version, extra, '.bat'];

launcher = fullfile(get_desktop, file);

if exist(launcher, 'file')
	return;
end

cmd = {['"', fullfile(matlabroot, 'bin', 'matlab.exe'), '" -sd "', app_root, '" -r xbat']};

file_writelines(launcher, cmd);


